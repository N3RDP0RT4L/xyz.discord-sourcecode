package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: TrackSendMessage.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u000b\n\u0002\u0010 \n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b+\n\u0002\u0010\r\n\u0002\b\u0019\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0013\u0010\u0015R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0010\u001a\u0004\b\u0017\u0010\u0012R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0014\u001a\u0004\b\u0019\u0010\u0015R!\u0010\u001b\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR!\u0010\u001f\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u001c\u001a\u0004\b \u0010\u001eR\u001b\u0010!\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u0010\u001a\u0004\b\"\u0010\u0012R\u001b\u0010#\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u0010\u001a\u0004\b$\u0010\u0012R\u001b\u0010%\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u0010\u001a\u0004\b&\u0010\u0012R$\u0010(\u001a\u0004\u0018\u00010'8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b(\u0010)\u001a\u0004\b*\u0010+\"\u0004\b,\u0010-R\u001b\u0010.\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010\u0010\u001a\u0004\b/\u0010\u0012R\u001b\u00100\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010\u0010\u001a\u0004\b1\u0010\u0012R\u001b\u00102\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010\u0010\u001a\u0004\b3\u0010\u0012R\u001b\u00104\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010\u0010\u001a\u0004\b5\u0010\u0012R\u001b\u00106\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010\u0010\u001a\u0004\b7\u0010\u0012R\u001b\u00108\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010\u0014\u001a\u0004\b8\u0010\u0015R!\u00109\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010\u001c\u001a\u0004\b:\u0010\u001eR\u001b\u0010;\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010\u0010\u001a\u0004\b<\u0010\u0012R\u001b\u0010=\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b=\u0010\u0010\u001a\u0004\b>\u0010\u0012R\u001b\u0010?\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010\u0010\u001a\u0004\b@\u0010\u0012R\u001b\u0010A\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bA\u0010\u0014\u001a\u0004\bB\u0010\u0015R\u001b\u0010C\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bC\u0010\u0014\u001a\u0004\bD\u0010\u0015R\u001b\u0010E\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bE\u0010\u0010\u001a\u0004\bF\u0010\u0012R\u001b\u0010G\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bG\u0010\u0014\u001a\u0004\bG\u0010\u0015R\u001b\u0010H\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bH\u0010\u0014\u001a\u0004\bI\u0010\u0015R\u001b\u0010J\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bJ\u0010\u0010\u001a\u0004\bK\u0010\u0012R\u001c\u0010L\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\bL\u0010M\u001a\u0004\bN\u0010\u0005R!\u0010O\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u001a8\u0006@\u0006¢\u0006\f\n\u0004\bO\u0010\u001c\u001a\u0004\bP\u0010\u001eR\u001b\u0010Q\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bQ\u0010\u0010\u001a\u0004\bR\u0010\u0012R\u001b\u0010T\u001a\u0004\u0018\u00010S8\u0006@\u0006¢\u0006\f\n\u0004\bT\u0010U\u001a\u0004\bV\u0010WR\u001b\u0010X\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bX\u0010\u0010\u001a\u0004\bY\u0010\u0012R\u001b\u0010Z\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bZ\u0010\u0010\u001a\u0004\b[\u0010\u0012R\u001b\u0010\\\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\\\u0010\u0010\u001a\u0004\b]\u0010\u0012R\u001b\u0010^\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b^\u0010\u0010\u001a\u0004\b_\u0010\u0012R\u001b\u0010`\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b`\u0010\u0010\u001a\u0004\ba\u0010\u0012R\u001b\u0010b\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bb\u0010\u0010\u001a\u0004\bc\u0010\u0012R!\u0010d\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u001a8\u0006@\u0006¢\u0006\f\n\u0004\bd\u0010\u001c\u001a\u0004\be\u0010\u001eR\u001b\u0010f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bf\u0010\u0010\u001a\u0004\bg\u0010\u0012R\u001b\u0010h\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bh\u0010\u0010\u001a\u0004\bi\u0010\u0012R\u001b\u0010j\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bj\u0010\u0014\u001a\u0004\bk\u0010\u0015¨\u0006l"}, d2 = {"Lcom/discord/analytics/generated/events/TrackSendMessage;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "referenceMessageId", "Ljava/lang/Long;", "getReferenceMessageId", "()Ljava/lang/Long;", "isFirstMessage", "Ljava/lang/Boolean;", "()Ljava/lang/Boolean;", "emojiManaged", "getEmojiManaged", "emojiOnly", "getEmojiOnly", "", "components", "Ljava/util/List;", "getComponents", "()Ljava/util/List;", "stickerIds", "getStickerIds", "messageType", "getMessageType", "referenceMessageChannel", "getReferenceMessageChannel", "numAttachments", "getNumAttachments", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "activityAction", "getActivityAction", "server", "getServer", "webhookId", "getWebhookId", "emojiCustomExternal", "getEmojiCustomExternal", "clientApplicationId", "getClientApplicationId", "isFriend", "mentionIds", "getMentionIds", "emojiUnicode", "getEmojiUnicode", "emojiManagedExternal", "getEmojiManagedExternal", "channel", "getChannel", "private", "getPrivate", "mentionEveryone", "getMentionEveryone", "numEmbeds", "getNumEmbeds", "isGreeting", "hasSpoiler", "getHasSpoiler", "replyAgeSeconds", "getReplyAgeSeconds", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "recipientIds", "getRecipientIds", "applicationId", "getApplicationId", "", "activityPartyPlatform", "Ljava/lang/CharSequence;", "getActivityPartyPlatform", "()Ljava/lang/CharSequence;", "channelType", "getChannelType", "maxAttachmentSize", "getMaxAttachmentSize", "length", "getLength", "wordCount", "getWordCount", "referenceMessageGuild", "getReferenceMessageGuild", "messageId", "getMessageId", "attachmentIds", "getAttachmentIds", "emojiCustom", "getEmojiCustom", "emojiAnimated", "getEmojiAnimated", "probablyHasMarkdown", "getProbablyHasMarkdown", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackSendMessage implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Long messageId = null;
    private final Long messageType = null;
    private final Long channel = null;
    private final Long channelType = null;
    private final Boolean isFriend = null;

    /* renamed from: private  reason: not valid java name */
    private final Boolean f1private = null;
    private final Long server = null;
    private final Long numAttachments = null;
    private final Long maxAttachmentSize = null;
    private final List<Long> recipientIds = null;
    private final List<Long> mentionIds = null;
    private final Long length = null;
    private final Long wordCount = null;
    private final Boolean mentionEveryone = null;
    private final Long emojiUnicode = null;
    private final Long emojiCustom = null;
    private final Long emojiCustomExternal = null;
    private final Long emojiManaged = null;
    private final Long emojiManagedExternal = null;
    private final Long emojiAnimated = null;
    private final Boolean emojiOnly = null;
    private final Long numEmbeds = null;
    private final List<Long> components = null;
    private final Long clientApplicationId = null;
    private final Long applicationId = null;
    private final List<Long> attachmentIds = null;
    private final Long activityAction = null;
    private final CharSequence activityPartyPlatform = null;
    private final Boolean hasSpoiler = null;
    private final Boolean probablyHasMarkdown = null;
    private final Long referenceMessageId = null;
    private final Long referenceMessageChannel = null;
    private final Long referenceMessageGuild = null;
    private final List<Long> stickerIds = null;
    private final Long replyAgeSeconds = null;
    private final Long webhookId = null;
    private final Boolean isGreeting = null;
    private final Boolean isFirstMessage = null;
    private final transient String analyticsSchemaTypeName = "send_message";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackSendMessage)) {
            return false;
        }
        TrackSendMessage trackSendMessage = (TrackSendMessage) obj;
        return m.areEqual(this.messageId, trackSendMessage.messageId) && m.areEqual(this.messageType, trackSendMessage.messageType) && m.areEqual(this.channel, trackSendMessage.channel) && m.areEqual(this.channelType, trackSendMessage.channelType) && m.areEqual(this.isFriend, trackSendMessage.isFriend) && m.areEqual(this.f1private, trackSendMessage.f1private) && m.areEqual(this.server, trackSendMessage.server) && m.areEqual(this.numAttachments, trackSendMessage.numAttachments) && m.areEqual(this.maxAttachmentSize, trackSendMessage.maxAttachmentSize) && m.areEqual(this.recipientIds, trackSendMessage.recipientIds) && m.areEqual(this.mentionIds, trackSendMessage.mentionIds) && m.areEqual(this.length, trackSendMessage.length) && m.areEqual(this.wordCount, trackSendMessage.wordCount) && m.areEqual(this.mentionEveryone, trackSendMessage.mentionEveryone) && m.areEqual(this.emojiUnicode, trackSendMessage.emojiUnicode) && m.areEqual(this.emojiCustom, trackSendMessage.emojiCustom) && m.areEqual(this.emojiCustomExternal, trackSendMessage.emojiCustomExternal) && m.areEqual(this.emojiManaged, trackSendMessage.emojiManaged) && m.areEqual(this.emojiManagedExternal, trackSendMessage.emojiManagedExternal) && m.areEqual(this.emojiAnimated, trackSendMessage.emojiAnimated) && m.areEqual(this.emojiOnly, trackSendMessage.emojiOnly) && m.areEqual(this.numEmbeds, trackSendMessage.numEmbeds) && m.areEqual(this.components, trackSendMessage.components) && m.areEqual(this.clientApplicationId, trackSendMessage.clientApplicationId) && m.areEqual(this.applicationId, trackSendMessage.applicationId) && m.areEqual(this.attachmentIds, trackSendMessage.attachmentIds) && m.areEqual(this.activityAction, trackSendMessage.activityAction) && m.areEqual(this.activityPartyPlatform, trackSendMessage.activityPartyPlatform) && m.areEqual(this.hasSpoiler, trackSendMessage.hasSpoiler) && m.areEqual(this.probablyHasMarkdown, trackSendMessage.probablyHasMarkdown) && m.areEqual(this.referenceMessageId, trackSendMessage.referenceMessageId) && m.areEqual(this.referenceMessageChannel, trackSendMessage.referenceMessageChannel) && m.areEqual(this.referenceMessageGuild, trackSendMessage.referenceMessageGuild) && m.areEqual(this.stickerIds, trackSendMessage.stickerIds) && m.areEqual(this.replyAgeSeconds, trackSendMessage.replyAgeSeconds) && m.areEqual(this.webhookId, trackSendMessage.webhookId) && m.areEqual(this.isGreeting, trackSendMessage.isGreeting) && m.areEqual(this.isFirstMessage, trackSendMessage.isFirstMessage);
    }

    public int hashCode() {
        Long l = this.messageId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.messageType;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.channel;
        int hashCode3 = (hashCode2 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.channelType;
        int hashCode4 = (hashCode3 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Boolean bool = this.isFriend;
        int hashCode5 = (hashCode4 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.f1private;
        int hashCode6 = (hashCode5 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Long l5 = this.server;
        int hashCode7 = (hashCode6 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.numAttachments;
        int hashCode8 = (hashCode7 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.maxAttachmentSize;
        int hashCode9 = (hashCode8 + (l7 != null ? l7.hashCode() : 0)) * 31;
        List<Long> list = this.recipientIds;
        int hashCode10 = (hashCode9 + (list != null ? list.hashCode() : 0)) * 31;
        List<Long> list2 = this.mentionIds;
        int hashCode11 = (hashCode10 + (list2 != null ? list2.hashCode() : 0)) * 31;
        Long l8 = this.length;
        int hashCode12 = (hashCode11 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.wordCount;
        int hashCode13 = (hashCode12 + (l9 != null ? l9.hashCode() : 0)) * 31;
        Boolean bool3 = this.mentionEveryone;
        int hashCode14 = (hashCode13 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        Long l10 = this.emojiUnicode;
        int hashCode15 = (hashCode14 + (l10 != null ? l10.hashCode() : 0)) * 31;
        Long l11 = this.emojiCustom;
        int hashCode16 = (hashCode15 + (l11 != null ? l11.hashCode() : 0)) * 31;
        Long l12 = this.emojiCustomExternal;
        int hashCode17 = (hashCode16 + (l12 != null ? l12.hashCode() : 0)) * 31;
        Long l13 = this.emojiManaged;
        int hashCode18 = (hashCode17 + (l13 != null ? l13.hashCode() : 0)) * 31;
        Long l14 = this.emojiManagedExternal;
        int hashCode19 = (hashCode18 + (l14 != null ? l14.hashCode() : 0)) * 31;
        Long l15 = this.emojiAnimated;
        int hashCode20 = (hashCode19 + (l15 != null ? l15.hashCode() : 0)) * 31;
        Boolean bool4 = this.emojiOnly;
        int hashCode21 = (hashCode20 + (bool4 != null ? bool4.hashCode() : 0)) * 31;
        Long l16 = this.numEmbeds;
        int hashCode22 = (hashCode21 + (l16 != null ? l16.hashCode() : 0)) * 31;
        List<Long> list3 = this.components;
        int hashCode23 = (hashCode22 + (list3 != null ? list3.hashCode() : 0)) * 31;
        Long l17 = this.clientApplicationId;
        int hashCode24 = (hashCode23 + (l17 != null ? l17.hashCode() : 0)) * 31;
        Long l18 = this.applicationId;
        int hashCode25 = (hashCode24 + (l18 != null ? l18.hashCode() : 0)) * 31;
        List<Long> list4 = this.attachmentIds;
        int hashCode26 = (hashCode25 + (list4 != null ? list4.hashCode() : 0)) * 31;
        Long l19 = this.activityAction;
        int hashCode27 = (hashCode26 + (l19 != null ? l19.hashCode() : 0)) * 31;
        CharSequence charSequence = this.activityPartyPlatform;
        int hashCode28 = (hashCode27 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Boolean bool5 = this.hasSpoiler;
        int hashCode29 = (hashCode28 + (bool5 != null ? bool5.hashCode() : 0)) * 31;
        Boolean bool6 = this.probablyHasMarkdown;
        int hashCode30 = (hashCode29 + (bool6 != null ? bool6.hashCode() : 0)) * 31;
        Long l20 = this.referenceMessageId;
        int hashCode31 = (hashCode30 + (l20 != null ? l20.hashCode() : 0)) * 31;
        Long l21 = this.referenceMessageChannel;
        int hashCode32 = (hashCode31 + (l21 != null ? l21.hashCode() : 0)) * 31;
        Long l22 = this.referenceMessageGuild;
        int hashCode33 = (hashCode32 + (l22 != null ? l22.hashCode() : 0)) * 31;
        List<Long> list5 = this.stickerIds;
        int hashCode34 = (hashCode33 + (list5 != null ? list5.hashCode() : 0)) * 31;
        Long l23 = this.replyAgeSeconds;
        int hashCode35 = (hashCode34 + (l23 != null ? l23.hashCode() : 0)) * 31;
        Long l24 = this.webhookId;
        int hashCode36 = (hashCode35 + (l24 != null ? l24.hashCode() : 0)) * 31;
        Boolean bool7 = this.isGreeting;
        int hashCode37 = (hashCode36 + (bool7 != null ? bool7.hashCode() : 0)) * 31;
        Boolean bool8 = this.isFirstMessage;
        if (bool8 != null) {
            i = bool8.hashCode();
        }
        return hashCode37 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackSendMessage(messageId=");
        R.append(this.messageId);
        R.append(", messageType=");
        R.append(this.messageType);
        R.append(", channel=");
        R.append(this.channel);
        R.append(", channelType=");
        R.append(this.channelType);
        R.append(", isFriend=");
        R.append(this.isFriend);
        R.append(", private=");
        R.append(this.f1private);
        R.append(", server=");
        R.append(this.server);
        R.append(", numAttachments=");
        R.append(this.numAttachments);
        R.append(", maxAttachmentSize=");
        R.append(this.maxAttachmentSize);
        R.append(", recipientIds=");
        R.append(this.recipientIds);
        R.append(", mentionIds=");
        R.append(this.mentionIds);
        R.append(", length=");
        R.append(this.length);
        R.append(", wordCount=");
        R.append(this.wordCount);
        R.append(", mentionEveryone=");
        R.append(this.mentionEveryone);
        R.append(", emojiUnicode=");
        R.append(this.emojiUnicode);
        R.append(", emojiCustom=");
        R.append(this.emojiCustom);
        R.append(", emojiCustomExternal=");
        R.append(this.emojiCustomExternal);
        R.append(", emojiManaged=");
        R.append(this.emojiManaged);
        R.append(", emojiManagedExternal=");
        R.append(this.emojiManagedExternal);
        R.append(", emojiAnimated=");
        R.append(this.emojiAnimated);
        R.append(", emojiOnly=");
        R.append(this.emojiOnly);
        R.append(", numEmbeds=");
        R.append(this.numEmbeds);
        R.append(", components=");
        R.append(this.components);
        R.append(", clientApplicationId=");
        R.append(this.clientApplicationId);
        R.append(", applicationId=");
        R.append(this.applicationId);
        R.append(", attachmentIds=");
        R.append(this.attachmentIds);
        R.append(", activityAction=");
        R.append(this.activityAction);
        R.append(", activityPartyPlatform=");
        R.append(this.activityPartyPlatform);
        R.append(", hasSpoiler=");
        R.append(this.hasSpoiler);
        R.append(", probablyHasMarkdown=");
        R.append(this.probablyHasMarkdown);
        R.append(", referenceMessageId=");
        R.append(this.referenceMessageId);
        R.append(", referenceMessageChannel=");
        R.append(this.referenceMessageChannel);
        R.append(", referenceMessageGuild=");
        R.append(this.referenceMessageGuild);
        R.append(", stickerIds=");
        R.append(this.stickerIds);
        R.append(", replyAgeSeconds=");
        R.append(this.replyAgeSeconds);
        R.append(", webhookId=");
        R.append(this.webhookId);
        R.append(", isGreeting=");
        R.append(this.isGreeting);
        R.append(", isFirstMessage=");
        return a.C(R, this.isFirstMessage, ")");
    }
}
