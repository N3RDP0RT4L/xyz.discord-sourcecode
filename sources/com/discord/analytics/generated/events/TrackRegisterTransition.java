package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: TrackRegisterTransition.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0007\n\u0002\u0010\r\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010 \n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0013\u0010\u0015R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u0018\u001a\u0004\b\u001c\u0010\u001aR\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u0018\u001a\u0004\b\u001e\u0010\u001aR\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u0010\u001a\u0004\b \u0010\u0012R\u001c\u0010!\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010\u0005R\u001b\u0010$\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u0018\u001a\u0004\b%\u0010\u001aR$\u0010'\u001a\u0004\u0018\u00010&8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*\"\u0004\b+\u0010,R\u001b\u0010-\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u0018\u001a\u0004\b.\u0010\u001aR\u001b\u0010/\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u0010\u001a\u0004\b0\u0010\u0012R\u001b\u00101\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010\u0010\u001a\u0004\b2\u0010\u0012R#\u00104\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0016\u0018\u0001038\u0006@\u0006¢\u0006\f\n\u0004\b4\u00105\u001a\u0004\b6\u00107R!\u00108\u001a\n\u0012\u0004\u0012\u00020\u0016\u0018\u0001038\u0006@\u0006¢\u0006\f\n\u0004\b8\u00105\u001a\u0004\b9\u00107¨\u0006:"}, d2 = {"Lcom/discord/analytics/generated/events/TrackRegisterTransition;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "inviteInviterId", "Ljava/lang/Long;", "getInviteInviterId", "()Ljava/lang/Long;", "isUnclaimed", "Ljava/lang/Boolean;", "()Ljava/lang/Boolean;", "", "step", "Ljava/lang/CharSequence;", "getStep", "()Ljava/lang/CharSequence;", "actionType", "getActionType", "identityType", "getIdentityType", "inviteChannelId", "getInviteChannelId", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "inviteCode", "getInviteCode", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "registrationSource", "getRegistrationSource", "inviteChannelType", "getInviteChannelType", "inviteGuildId", "getInviteGuildId", "", "actionTypeDetails", "Ljava/util/List;", "getActionTypeDetails", "()Ljava/util/List;", "actionDetails", "getActionDetails", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackRegisterTransition implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final CharSequence step = null;
    private final CharSequence actionType = null;
    private final List<CharSequence> actionTypeDetails = null;
    private final List<CharSequence> actionDetails = null;
    private final CharSequence identityType = null;
    private final CharSequence registrationSource = null;
    private final Boolean isUnclaimed = null;
    private final CharSequence inviteCode = null;
    private final Long inviteGuildId = null;
    private final Long inviteChannelId = null;
    private final Long inviteChannelType = null;
    private final Long inviteInviterId = null;
    private final transient String analyticsSchemaTypeName = "register_transition";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackRegisterTransition)) {
            return false;
        }
        TrackRegisterTransition trackRegisterTransition = (TrackRegisterTransition) obj;
        return m.areEqual(this.step, trackRegisterTransition.step) && m.areEqual(this.actionType, trackRegisterTransition.actionType) && m.areEqual(this.actionTypeDetails, trackRegisterTransition.actionTypeDetails) && m.areEqual(this.actionDetails, trackRegisterTransition.actionDetails) && m.areEqual(this.identityType, trackRegisterTransition.identityType) && m.areEqual(this.registrationSource, trackRegisterTransition.registrationSource) && m.areEqual(this.isUnclaimed, trackRegisterTransition.isUnclaimed) && m.areEqual(this.inviteCode, trackRegisterTransition.inviteCode) && m.areEqual(this.inviteGuildId, trackRegisterTransition.inviteGuildId) && m.areEqual(this.inviteChannelId, trackRegisterTransition.inviteChannelId) && m.areEqual(this.inviteChannelType, trackRegisterTransition.inviteChannelType) && m.areEqual(this.inviteInviterId, trackRegisterTransition.inviteInviterId);
    }

    public int hashCode() {
        CharSequence charSequence = this.step;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        CharSequence charSequence2 = this.actionType;
        int hashCode2 = (hashCode + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        List<CharSequence> list = this.actionTypeDetails;
        int hashCode3 = (hashCode2 + (list != null ? list.hashCode() : 0)) * 31;
        List<CharSequence> list2 = this.actionDetails;
        int hashCode4 = (hashCode3 + (list2 != null ? list2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.identityType;
        int hashCode5 = (hashCode4 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.registrationSource;
        int hashCode6 = (hashCode5 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        Boolean bool = this.isUnclaimed;
        int hashCode7 = (hashCode6 + (bool != null ? bool.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.inviteCode;
        int hashCode8 = (hashCode7 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        Long l = this.inviteGuildId;
        int hashCode9 = (hashCode8 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.inviteChannelId;
        int hashCode10 = (hashCode9 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.inviteChannelType;
        int hashCode11 = (hashCode10 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.inviteInviterId;
        if (l4 != null) {
            i = l4.hashCode();
        }
        return hashCode11 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackRegisterTransition(step=");
        R.append(this.step);
        R.append(", actionType=");
        R.append(this.actionType);
        R.append(", actionTypeDetails=");
        R.append(this.actionTypeDetails);
        R.append(", actionDetails=");
        R.append(this.actionDetails);
        R.append(", identityType=");
        R.append(this.identityType);
        R.append(", registrationSource=");
        R.append(this.registrationSource);
        R.append(", isUnclaimed=");
        R.append(this.isUnclaimed);
        R.append(", inviteCode=");
        R.append(this.inviteCode);
        R.append(", inviteGuildId=");
        R.append(this.inviteGuildId);
        R.append(", inviteChannelId=");
        R.append(this.inviteChannelId);
        R.append(", inviteChannelType=");
        R.append(this.inviteChannelType);
        R.append(", inviteInviterId=");
        return a.F(R, this.inviteInviterId, ")");
    }
}
