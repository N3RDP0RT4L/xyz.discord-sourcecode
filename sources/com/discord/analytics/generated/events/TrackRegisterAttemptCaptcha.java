package com.discord.analytics.generated.events;

import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackGiftCodeMetadata;
import com.discord.analytics.generated.traits.TrackGiftCodeMetadataReceiver;
import com.discord.analytics.generated.traits.TrackGuildTemplate;
import com.discord.analytics.generated.traits.TrackGuildTemplateReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: TrackRegisterAttemptCaptcha.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0010 \n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u001a\u0010\u000e\u001a\u00020\r2\b\u0010\f\u001a\u0004\u0018\u00010\u000bHÖ\u0003¢\u0006\u0004\b\u000e\u0010\u000fR\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0012\u001a\u0004\b\u0016\u0010\u0014R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u0012\u001a\u0004\b\u001d\u0010\u0014R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u0012\u001a\u0004\b\u001f\u0010\u0014R\u001b\u0010 \u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0012\u001a\u0004\b!\u0010\u0014R$\u0010#\u001a\u0004\u0018\u00010\"8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b#\u0010$\u001a\u0004\b%\u0010&\"\u0004\b'\u0010(R\u001b\u0010)\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010*\u001a\u0004\b+\u0010,R\u001b\u0010-\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u0019\u001a\u0004\b.\u0010\u001bR$\u00100\u001a\u0004\u0018\u00010/8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b0\u00101\u001a\u0004\b2\u00103\"\u0004\b4\u00105R$\u00107\u001a\u0004\u0018\u0001068\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b7\u00108\u001a\u0004\b9\u0010:\"\u0004\b;\u0010<R\u001b\u0010=\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b=\u0010\u0012\u001a\u0004\b>\u0010\u0014R\u001c\u0010?\u001a\u00020\u00058\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b?\u0010@\u001a\u0004\bA\u0010\u0007R\u001b\u0010B\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010\u0019\u001a\u0004\bC\u0010\u001bR\u001b\u0010D\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\bD\u0010*\u001a\u0004\bE\u0010,R\u001b\u0010F\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\bF\u0010\u0012\u001a\u0004\bG\u0010\u0014R\u001b\u0010H\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\bH\u0010*\u001a\u0004\bI\u0010,R\u001b\u0010J\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\bJ\u0010\u0019\u001a\u0004\bK\u0010\u001bR!\u0010M\u001a\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010L8\u0006@\u0006¢\u0006\f\n\u0004\bM\u0010N\u001a\u0004\bO\u0010PR\u001b\u0010Q\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\bQ\u0010*\u001a\u0004\bR\u0010,¨\u0006S"}, d2 = {"Lcom/discord/analytics/generated/events/TrackRegisterAttemptCaptcha;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackGiftCodeMetadataReceiver;", "Lcom/discord/analytics/generated/traits/TrackGuildTemplateReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "phone", "Ljava/lang/CharSequence;", "getPhone", "()Ljava/lang/CharSequence;", "identityType", "getIdentityType", "", "inviteGuildId", "Ljava/lang/Long;", "getInviteGuildId", "()Ljava/lang/Long;", NotificationCompat.CATEGORY_EMAIL, "getEmail", "phoneCarrierName", "getPhoneCarrierName", "phoneCountry", "getPhoneCountry", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "full", "Ljava/lang/Boolean;", "getFull", "()Ljava/lang/Boolean;", "inviteChannelType", "getInviteChannelType", "Lcom/discord/analytics/generated/traits/TrackGuildTemplate;", "trackGuildTemplate", "Lcom/discord/analytics/generated/traits/TrackGuildTemplate;", "getTrackGuildTemplate", "()Lcom/discord/analytics/generated/traits/TrackGuildTemplate;", "setTrackGuildTemplate", "(Lcom/discord/analytics/generated/traits/TrackGuildTemplate;)V", "Lcom/discord/analytics/generated/traits/TrackGiftCodeMetadata;", "trackGiftCodeMetadata", "Lcom/discord/analytics/generated/traits/TrackGiftCodeMetadata;", "getTrackGiftCodeMetadata", "()Lcom/discord/analytics/generated/traits/TrackGiftCodeMetadata;", "setTrackGiftCodeMetadata", "(Lcom/discord/analytics/generated/traits/TrackGiftCodeMetadata;)V", "registrationSource", "getRegistrationSource", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "inviteInviterId", "getInviteInviterId", "forceBadCaptcha", "getForceBadCaptcha", "inviteCode", "getInviteCode", "instantInvite", "getInstantInvite", "inviteChannelId", "getInviteChannelId", "", "ipBlacklists", "Ljava/util/List;", "getIpBlacklists", "()Ljava/util/List;", "hasInvalidFingerprint", "getHasInvalidFingerprint", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackRegisterAttemptCaptcha implements AnalyticsSchema, TrackBaseReceiver, TrackGiftCodeMetadataReceiver, TrackGuildTemplateReceiver {
    private TrackBase trackBase;
    private TrackGiftCodeMetadata trackGiftCodeMetadata;
    private TrackGuildTemplate trackGuildTemplate;
    private final CharSequence registrationSource = null;
    private final Boolean full = null;
    private final Boolean instantInvite = null;
    private final CharSequence inviteCode = null;
    private final Long inviteGuildId = null;
    private final Long inviteChannelId = null;
    private final Long inviteChannelType = null;
    private final Long inviteInviterId = null;
    private final List<CharSequence> ipBlacklists = null;
    private final Boolean hasInvalidFingerprint = null;
    private final Boolean forceBadCaptcha = null;
    private final CharSequence email = null;
    private final CharSequence phone = null;
    private final CharSequence identityType = null;
    private final CharSequence phoneCarrierName = null;
    private final CharSequence phoneCountry = null;
    private final transient String analyticsSchemaTypeName = "register_attempt_captcha";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackRegisterAttemptCaptcha)) {
            return false;
        }
        TrackRegisterAttemptCaptcha trackRegisterAttemptCaptcha = (TrackRegisterAttemptCaptcha) obj;
        return m.areEqual(this.registrationSource, trackRegisterAttemptCaptcha.registrationSource) && m.areEqual(this.full, trackRegisterAttemptCaptcha.full) && m.areEqual(this.instantInvite, trackRegisterAttemptCaptcha.instantInvite) && m.areEqual(this.inviteCode, trackRegisterAttemptCaptcha.inviteCode) && m.areEqual(this.inviteGuildId, trackRegisterAttemptCaptcha.inviteGuildId) && m.areEqual(this.inviteChannelId, trackRegisterAttemptCaptcha.inviteChannelId) && m.areEqual(this.inviteChannelType, trackRegisterAttemptCaptcha.inviteChannelType) && m.areEqual(this.inviteInviterId, trackRegisterAttemptCaptcha.inviteInviterId) && m.areEqual(this.ipBlacklists, trackRegisterAttemptCaptcha.ipBlacklists) && m.areEqual(this.hasInvalidFingerprint, trackRegisterAttemptCaptcha.hasInvalidFingerprint) && m.areEqual(this.forceBadCaptcha, trackRegisterAttemptCaptcha.forceBadCaptcha) && m.areEqual(this.email, trackRegisterAttemptCaptcha.email) && m.areEqual(this.phone, trackRegisterAttemptCaptcha.phone) && m.areEqual(this.identityType, trackRegisterAttemptCaptcha.identityType) && m.areEqual(this.phoneCarrierName, trackRegisterAttemptCaptcha.phoneCarrierName) && m.areEqual(this.phoneCountry, trackRegisterAttemptCaptcha.phoneCountry);
    }

    public int hashCode() {
        CharSequence charSequence = this.registrationSource;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        Boolean bool = this.full;
        int hashCode2 = (hashCode + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.instantInvite;
        int hashCode3 = (hashCode2 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.inviteCode;
        int hashCode4 = (hashCode3 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l = this.inviteGuildId;
        int hashCode5 = (hashCode4 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.inviteChannelId;
        int hashCode6 = (hashCode5 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.inviteChannelType;
        int hashCode7 = (hashCode6 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.inviteInviterId;
        int hashCode8 = (hashCode7 + (l4 != null ? l4.hashCode() : 0)) * 31;
        List<CharSequence> list = this.ipBlacklists;
        int hashCode9 = (hashCode8 + (list != null ? list.hashCode() : 0)) * 31;
        Boolean bool3 = this.hasInvalidFingerprint;
        int hashCode10 = (hashCode9 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        Boolean bool4 = this.forceBadCaptcha;
        int hashCode11 = (hashCode10 + (bool4 != null ? bool4.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.email;
        int hashCode12 = (hashCode11 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.phone;
        int hashCode13 = (hashCode12 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.identityType;
        int hashCode14 = (hashCode13 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        CharSequence charSequence6 = this.phoneCarrierName;
        int hashCode15 = (hashCode14 + (charSequence6 != null ? charSequence6.hashCode() : 0)) * 31;
        CharSequence charSequence7 = this.phoneCountry;
        if (charSequence7 != null) {
            i = charSequence7.hashCode();
        }
        return hashCode15 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackRegisterAttemptCaptcha(registrationSource=");
        R.append(this.registrationSource);
        R.append(", full=");
        R.append(this.full);
        R.append(", instantInvite=");
        R.append(this.instantInvite);
        R.append(", inviteCode=");
        R.append(this.inviteCode);
        R.append(", inviteGuildId=");
        R.append(this.inviteGuildId);
        R.append(", inviteChannelId=");
        R.append(this.inviteChannelId);
        R.append(", inviteChannelType=");
        R.append(this.inviteChannelType);
        R.append(", inviteInviterId=");
        R.append(this.inviteInviterId);
        R.append(", ipBlacklists=");
        R.append(this.ipBlacklists);
        R.append(", hasInvalidFingerprint=");
        R.append(this.hasInvalidFingerprint);
        R.append(", forceBadCaptcha=");
        R.append(this.forceBadCaptcha);
        R.append(", email=");
        R.append(this.email);
        R.append(", phone=");
        R.append(this.phone);
        R.append(", identityType=");
        R.append(this.identityType);
        R.append(", phoneCarrierName=");
        R.append(this.phoneCarrierName);
        R.append(", phoneCountry=");
        return a.D(R, this.phoneCountry, ")");
    }
}
