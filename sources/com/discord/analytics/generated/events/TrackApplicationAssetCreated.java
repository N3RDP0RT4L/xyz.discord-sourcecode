package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackApplicationAssetCreated.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\r\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R$\u0010\u0018\u001a\u0004\u0018\u00010\u00178\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001dR\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u0010\u001a\u0004\b\u001f\u0010\u0012R\u001b\u0010 \u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0010\u001a\u0004\b!\u0010\u0012R\u001b\u0010#\u001a\u0004\u0018\u00010\"8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010$\u001a\u0004\b%\u0010&R\u001c\u0010'\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b)\u0010\u0005R\u001b\u0010*\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010\u0014\u001a\u0004\b+\u0010\u0016R\u001b\u0010,\u001a\u0004\u0018\u00010\"8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010$\u001a\u0004\b-\u0010&R\u001b\u0010.\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010\u0010\u001a\u0004\b/\u0010\u0012¨\u00060"}, d2 = {"Lcom/discord/analytics/generated/events/TrackApplicationAssetCreated;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "type", "Ljava/lang/Long;", "getType", "()Ljava/lang/Long;", "hasBot", "Ljava/lang/Boolean;", "getHasBot", "()Ljava/lang/Boolean;", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "botId", "getBotId", "applicationId", "getApplicationId", "", "applicationName", "Ljava/lang/CharSequence;", "getApplicationName", "()Ljava/lang/CharSequence;", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "hasRedirectUri", "getHasRedirectUri", ModelAuditLogEntry.CHANGE_KEY_NAME, "getName", ModelAuditLogEntry.CHANGE_KEY_ID, "getId", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackApplicationAssetCreated implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;

    /* renamed from: id  reason: collision with root package name */
    private final Long f2004id = null;
    private final Long applicationId = null;
    private final Long botId = null;
    private final CharSequence applicationName = null;
    private final Boolean hasBot = null;
    private final Boolean hasRedirectUri = null;
    private final Long type = null;
    private final CharSequence name = null;
    private final transient String analyticsSchemaTypeName = "application_asset_created";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackApplicationAssetCreated)) {
            return false;
        }
        TrackApplicationAssetCreated trackApplicationAssetCreated = (TrackApplicationAssetCreated) obj;
        return m.areEqual(this.f2004id, trackApplicationAssetCreated.f2004id) && m.areEqual(this.applicationId, trackApplicationAssetCreated.applicationId) && m.areEqual(this.botId, trackApplicationAssetCreated.botId) && m.areEqual(this.applicationName, trackApplicationAssetCreated.applicationName) && m.areEqual(this.hasBot, trackApplicationAssetCreated.hasBot) && m.areEqual(this.hasRedirectUri, trackApplicationAssetCreated.hasRedirectUri) && m.areEqual(this.type, trackApplicationAssetCreated.type) && m.areEqual(this.name, trackApplicationAssetCreated.name);
    }

    public int hashCode() {
        Long l = this.f2004id;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.applicationId;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.botId;
        int hashCode3 = (hashCode2 + (l3 != null ? l3.hashCode() : 0)) * 31;
        CharSequence charSequence = this.applicationName;
        int hashCode4 = (hashCode3 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Boolean bool = this.hasBot;
        int hashCode5 = (hashCode4 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.hasRedirectUri;
        int hashCode6 = (hashCode5 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Long l4 = this.type;
        int hashCode7 = (hashCode6 + (l4 != null ? l4.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.name;
        if (charSequence2 != null) {
            i = charSequence2.hashCode();
        }
        return hashCode7 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackApplicationAssetCreated(id=");
        R.append(this.f2004id);
        R.append(", applicationId=");
        R.append(this.applicationId);
        R.append(", botId=");
        R.append(this.botId);
        R.append(", applicationName=");
        R.append(this.applicationName);
        R.append(", hasBot=");
        R.append(this.hasBot);
        R.append(", hasRedirectUri=");
        R.append(this.hasRedirectUri);
        R.append(", type=");
        R.append(this.type);
        R.append(", name=");
        return a.D(R, this.name, ")");
    }
}
