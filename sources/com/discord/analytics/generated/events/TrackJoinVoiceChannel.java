package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackNetworkInformation;
import com.discord.analytics.generated.traits.TrackNetworkInformationReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackJoinVoiceChannel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\r\n\u0002\b\u0011\n\u0002\u0018\u0002\n\u0002\b\u0019\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\u0010\u0010\u0005\u001a\u00020\u0004HÖ\u0001¢\u0006\u0004\b\u0005\u0010\u0006J\u0010\u0010\b\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\r\u001a\u00020\f2\b\u0010\u000b\u001a\u0004\u0018\u00010\nHÖ\u0003¢\u0006\u0004\b\r\u0010\u000eR$\u0010\u0010\u001a\u0004\u0018\u00010\u000f8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001fR\u001b\u0010 \u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u001d\u001a\u0004\b!\u0010\u001fR\u001c\u0010\"\u001a\u00020\u00048\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010\u0006R\u001b\u0010%\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u0018\u001a\u0004\b&\u0010\u001aR\u001b\u0010'\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010\u001d\u001a\u0004\b(\u0010\u001fR\u001b\u0010)\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010\u001d\u001a\u0004\b*\u0010\u001fR\u001b\u0010+\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010\u0018\u001a\u0004\b,\u0010\u001aR$\u0010.\u001a\u0004\u0018\u00010-8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b.\u0010/\u001a\u0004\b0\u00101\"\u0004\b2\u00103R\u001b\u00104\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010\u0018\u001a\u0004\b5\u0010\u001aR\u001b\u00106\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010\u0018\u001a\u0004\b7\u0010\u001aR\u001b\u00108\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010\u0018\u001a\u0004\b9\u0010\u001aR\u001b\u0010:\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010\u0018\u001a\u0004\b;\u0010\u001aR\u001b\u0010<\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b<\u0010=\u001a\u0004\b>\u0010?R\u001b\u0010@\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b@\u0010\u0018\u001a\u0004\bA\u0010\u001aR\u001b\u0010B\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010\u0018\u001a\u0004\bC\u0010\u001aR\u001b\u0010D\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\bD\u0010\u0018\u001a\u0004\bE\u0010\u001a¨\u0006F"}, d2 = {"Lcom/discord/analytics/generated/events/TrackJoinVoiceChannel;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackNetworkInformationReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "", "channelId", "Ljava/lang/Long;", "getChannelId", "()Ljava/lang/Long;", "", "nonce", "Ljava/lang/CharSequence;", "getNonce", "()Ljava/lang/CharSequence;", "gameName", "getGameName", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "customStatusCount", "getCustomStatusCount", "gamePlatform", "getGamePlatform", "rtcConnectionId", "getRtcConnectionId", "videoStreamCount", "getVideoStreamCount", "Lcom/discord/analytics/generated/traits/TrackNetworkInformation;", "trackNetworkInformation", "Lcom/discord/analytics/generated/traits/TrackNetworkInformation;", "getTrackNetworkInformation", "()Lcom/discord/analytics/generated/traits/TrackNetworkInformation;", "setTrackNetworkInformation", "(Lcom/discord/analytics/generated/traits/TrackNetworkInformation;)V", "guildScheduledEventId", "getGuildScheduledEventId", "stageInstanceId", "getStageInstanceId", "channelBitrate", "getChannelBitrate", "guildId", "getGuildId", "videoEnabled", "Ljava/lang/Boolean;", "getVideoEnabled", "()Ljava/lang/Boolean;", "voiceStateCount", "getVoiceStateCount", "gameId", "getGameId", "channelType", "getChannelType", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackJoinVoiceChannel implements AnalyticsSchema, TrackBaseReceiver, TrackNetworkInformationReceiver {
    private TrackBase trackBase;
    private TrackNetworkInformation trackNetworkInformation;
    private final Long channelId = null;
    private final Long channelType = null;
    private final Long channelBitrate = null;
    private final Long guildId = null;
    private final CharSequence nonce = null;
    private final CharSequence rtcConnectionId = null;
    private final Long voiceStateCount = null;
    private final Long videoStreamCount = null;
    private final Boolean videoEnabled = null;
    private final CharSequence gameName = null;
    private final CharSequence gamePlatform = null;
    private final Long gameId = null;
    private final Long customStatusCount = null;
    private final Long stageInstanceId = null;
    private final Long guildScheduledEventId = null;
    private final transient String analyticsSchemaTypeName = "join_voice_channel";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackJoinVoiceChannel)) {
            return false;
        }
        TrackJoinVoiceChannel trackJoinVoiceChannel = (TrackJoinVoiceChannel) obj;
        return m.areEqual(this.channelId, trackJoinVoiceChannel.channelId) && m.areEqual(this.channelType, trackJoinVoiceChannel.channelType) && m.areEqual(this.channelBitrate, trackJoinVoiceChannel.channelBitrate) && m.areEqual(this.guildId, trackJoinVoiceChannel.guildId) && m.areEqual(this.nonce, trackJoinVoiceChannel.nonce) && m.areEqual(this.rtcConnectionId, trackJoinVoiceChannel.rtcConnectionId) && m.areEqual(this.voiceStateCount, trackJoinVoiceChannel.voiceStateCount) && m.areEqual(this.videoStreamCount, trackJoinVoiceChannel.videoStreamCount) && m.areEqual(this.videoEnabled, trackJoinVoiceChannel.videoEnabled) && m.areEqual(this.gameName, trackJoinVoiceChannel.gameName) && m.areEqual(this.gamePlatform, trackJoinVoiceChannel.gamePlatform) && m.areEqual(this.gameId, trackJoinVoiceChannel.gameId) && m.areEqual(this.customStatusCount, trackJoinVoiceChannel.customStatusCount) && m.areEqual(this.stageInstanceId, trackJoinVoiceChannel.stageInstanceId) && m.areEqual(this.guildScheduledEventId, trackJoinVoiceChannel.guildScheduledEventId);
    }

    public int hashCode() {
        Long l = this.channelId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.channelType;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.channelBitrate;
        int hashCode3 = (hashCode2 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.guildId;
        int hashCode4 = (hashCode3 + (l4 != null ? l4.hashCode() : 0)) * 31;
        CharSequence charSequence = this.nonce;
        int hashCode5 = (hashCode4 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.rtcConnectionId;
        int hashCode6 = (hashCode5 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l5 = this.voiceStateCount;
        int hashCode7 = (hashCode6 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.videoStreamCount;
        int hashCode8 = (hashCode7 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Boolean bool = this.videoEnabled;
        int hashCode9 = (hashCode8 + (bool != null ? bool.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.gameName;
        int hashCode10 = (hashCode9 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.gamePlatform;
        int hashCode11 = (hashCode10 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        Long l7 = this.gameId;
        int hashCode12 = (hashCode11 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Long l8 = this.customStatusCount;
        int hashCode13 = (hashCode12 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.stageInstanceId;
        int hashCode14 = (hashCode13 + (l9 != null ? l9.hashCode() : 0)) * 31;
        Long l10 = this.guildScheduledEventId;
        if (l10 != null) {
            i = l10.hashCode();
        }
        return hashCode14 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackJoinVoiceChannel(channelId=");
        R.append(this.channelId);
        R.append(", channelType=");
        R.append(this.channelType);
        R.append(", channelBitrate=");
        R.append(this.channelBitrate);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", nonce=");
        R.append(this.nonce);
        R.append(", rtcConnectionId=");
        R.append(this.rtcConnectionId);
        R.append(", voiceStateCount=");
        R.append(this.voiceStateCount);
        R.append(", videoStreamCount=");
        R.append(this.videoStreamCount);
        R.append(", videoEnabled=");
        R.append(this.videoEnabled);
        R.append(", gameName=");
        R.append(this.gameName);
        R.append(", gamePlatform=");
        R.append(this.gamePlatform);
        R.append(", gameId=");
        R.append(this.gameId);
        R.append(", customStatusCount=");
        R.append(this.customStatusCount);
        R.append(", stageInstanceId=");
        R.append(this.stageInstanceId);
        R.append(", guildScheduledEventId=");
        return a.F(R, this.guildScheduledEventId, ")");
    }
}
