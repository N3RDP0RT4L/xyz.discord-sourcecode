package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackOverlayClientMetadata;
import com.discord.analytics.generated.traits.TrackOverlayClientMetadataReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackOverlayPerfInfo.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\r\n\u0002\b\b\n\u0002\u0010\t\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b \b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\u0010\u0010\u0005\u001a\u00020\u0004HÖ\u0001¢\u0006\u0004\b\u0005\u0010\u0006J\u0010\u0010\b\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\r\u001a\u00020\f2\b\u0010\u000b\u001a\u0004\u0018\u00010\nHÖ\u0003¢\u0006\u0004\b\r\u0010\u000eR$\u0010\u0010\u001a\u0004\u0018\u00010\u000f8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u001b\u0010\u001b\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u001b\u0010 \u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#R\u001b\u0010$\u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010!\u001a\u0004\b%\u0010#R\u001b\u0010&\u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010!\u001a\u0004\b'\u0010#R\u001b\u0010(\u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010!\u001a\u0004\b)\u0010#R\u001b\u0010*\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010\u0018\u001a\u0004\b+\u0010\u001aR\u001b\u0010,\u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010!\u001a\u0004\b-\u0010#R$\u0010/\u001a\u0004\u0018\u00010.8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b/\u00100\u001a\u0004\b1\u00102\"\u0004\b3\u00104R\u001b\u00105\u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010!\u001a\u0004\b6\u0010#R\u001b\u00107\u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010!\u001a\u0004\b8\u0010#R\u001c\u00109\u001a\u00020\u00048\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b9\u0010:\u001a\u0004\b;\u0010\u0006R\u001b\u0010<\u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b<\u0010!\u001a\u0004\b=\u0010#R\u001b\u0010>\u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b>\u0010!\u001a\u0004\b?\u0010#R\u001b\u0010@\u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b@\u0010!\u001a\u0004\bA\u0010#R\u001b\u0010B\u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010!\u001a\u0004\bC\u0010#R\u001b\u0010D\u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\bD\u0010!\u001a\u0004\bE\u0010#R\u001b\u0010F\u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\bF\u0010!\u001a\u0004\bG\u0010#R\u001b\u0010H\u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\bH\u0010!\u001a\u0004\bI\u0010#R\u001b\u0010J\u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\bJ\u0010!\u001a\u0004\bK\u0010#R\u001b\u0010L\u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\bL\u0010!\u001a\u0004\bM\u0010#¨\u0006N"}, d2 = {"Lcom/discord/analytics/generated/events/TrackOverlayPerfInfo;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackOverlayClientMetadataReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/analytics/generated/traits/TrackOverlayClientMetadata;", "trackOverlayClientMetadata", "Lcom/discord/analytics/generated/traits/TrackOverlayClientMetadata;", "getTrackOverlayClientMetadata", "()Lcom/discord/analytics/generated/traits/TrackOverlayClientMetadata;", "setTrackOverlayClientMetadata", "(Lcom/discord/analytics/generated/traits/TrackOverlayClientMetadata;)V", "", "apiName", "Ljava/lang/CharSequence;", "getApiName", "()Ljava/lang/CharSequence;", "fullscreen", "Ljava/lang/Boolean;", "getFullscreen", "()Ljava/lang/Boolean;", "", "frameTimeMinUs", "Ljava/lang/Long;", "getFrameTimeMinUs", "()Ljava/lang/Long;", "captureTimeAvgUs", "getCaptureTimeAvgUs", "frameCount", "getFrameCount", "height", "getHeight", "frameBufferSource", "getFrameBufferSource", "captureTimeMaxUs", "getCaptureTimeMaxUs", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "drawTimeMinUs", "getDrawTimeMinUs", "width", "getWidth", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "captureTimeMinUs", "getCaptureTimeMinUs", "drawTimeAvgUs", "getDrawTimeAvgUs", "durationUs", "getDurationUs", "lockFailures", "getLockFailures", "drawTimeMaxUs", "getDrawTimeMaxUs", "framesReceived", "getFramesReceived", "frameTimeAvgUs", "getFrameTimeAvgUs", "frameTimeMaxUs", "getFrameTimeMaxUs", "framesCaptured", "getFramesCaptured", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackOverlayPerfInfo implements AnalyticsSchema, TrackBaseReceiver, TrackOverlayClientMetadataReceiver {
    private TrackBase trackBase;
    private TrackOverlayClientMetadata trackOverlayClientMetadata;
    private final Long width = null;
    private final Long height = null;
    private final CharSequence apiName = null;
    private final Boolean fullscreen = null;
    private final CharSequence frameBufferSource = null;
    private final Long durationUs = null;
    private final Long frameCount = null;
    private final Long framesReceived = null;
    private final Long framesCaptured = null;
    private final Long lockFailures = null;
    private final Long frameTimeMinUs = null;
    private final Long frameTimeMaxUs = null;
    private final Long frameTimeAvgUs = null;
    private final Long drawTimeMinUs = null;
    private final Long drawTimeMaxUs = null;
    private final Long drawTimeAvgUs = null;
    private final Long captureTimeMinUs = null;
    private final Long captureTimeMaxUs = null;
    private final Long captureTimeAvgUs = null;
    private final transient String analyticsSchemaTypeName = "overlay_perf_info";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackOverlayPerfInfo)) {
            return false;
        }
        TrackOverlayPerfInfo trackOverlayPerfInfo = (TrackOverlayPerfInfo) obj;
        return m.areEqual(this.width, trackOverlayPerfInfo.width) && m.areEqual(this.height, trackOverlayPerfInfo.height) && m.areEqual(this.apiName, trackOverlayPerfInfo.apiName) && m.areEqual(this.fullscreen, trackOverlayPerfInfo.fullscreen) && m.areEqual(this.frameBufferSource, trackOverlayPerfInfo.frameBufferSource) && m.areEqual(this.durationUs, trackOverlayPerfInfo.durationUs) && m.areEqual(this.frameCount, trackOverlayPerfInfo.frameCount) && m.areEqual(this.framesReceived, trackOverlayPerfInfo.framesReceived) && m.areEqual(this.framesCaptured, trackOverlayPerfInfo.framesCaptured) && m.areEqual(this.lockFailures, trackOverlayPerfInfo.lockFailures) && m.areEqual(this.frameTimeMinUs, trackOverlayPerfInfo.frameTimeMinUs) && m.areEqual(this.frameTimeMaxUs, trackOverlayPerfInfo.frameTimeMaxUs) && m.areEqual(this.frameTimeAvgUs, trackOverlayPerfInfo.frameTimeAvgUs) && m.areEqual(this.drawTimeMinUs, trackOverlayPerfInfo.drawTimeMinUs) && m.areEqual(this.drawTimeMaxUs, trackOverlayPerfInfo.drawTimeMaxUs) && m.areEqual(this.drawTimeAvgUs, trackOverlayPerfInfo.drawTimeAvgUs) && m.areEqual(this.captureTimeMinUs, trackOverlayPerfInfo.captureTimeMinUs) && m.areEqual(this.captureTimeMaxUs, trackOverlayPerfInfo.captureTimeMaxUs) && m.areEqual(this.captureTimeAvgUs, trackOverlayPerfInfo.captureTimeAvgUs);
    }

    public int hashCode() {
        Long l = this.width;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.height;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        CharSequence charSequence = this.apiName;
        int hashCode3 = (hashCode2 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Boolean bool = this.fullscreen;
        int hashCode4 = (hashCode3 + (bool != null ? bool.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.frameBufferSource;
        int hashCode5 = (hashCode4 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l3 = this.durationUs;
        int hashCode6 = (hashCode5 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.frameCount;
        int hashCode7 = (hashCode6 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.framesReceived;
        int hashCode8 = (hashCode7 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.framesCaptured;
        int hashCode9 = (hashCode8 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.lockFailures;
        int hashCode10 = (hashCode9 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Long l8 = this.frameTimeMinUs;
        int hashCode11 = (hashCode10 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.frameTimeMaxUs;
        int hashCode12 = (hashCode11 + (l9 != null ? l9.hashCode() : 0)) * 31;
        Long l10 = this.frameTimeAvgUs;
        int hashCode13 = (hashCode12 + (l10 != null ? l10.hashCode() : 0)) * 31;
        Long l11 = this.drawTimeMinUs;
        int hashCode14 = (hashCode13 + (l11 != null ? l11.hashCode() : 0)) * 31;
        Long l12 = this.drawTimeMaxUs;
        int hashCode15 = (hashCode14 + (l12 != null ? l12.hashCode() : 0)) * 31;
        Long l13 = this.drawTimeAvgUs;
        int hashCode16 = (hashCode15 + (l13 != null ? l13.hashCode() : 0)) * 31;
        Long l14 = this.captureTimeMinUs;
        int hashCode17 = (hashCode16 + (l14 != null ? l14.hashCode() : 0)) * 31;
        Long l15 = this.captureTimeMaxUs;
        int hashCode18 = (hashCode17 + (l15 != null ? l15.hashCode() : 0)) * 31;
        Long l16 = this.captureTimeAvgUs;
        if (l16 != null) {
            i = l16.hashCode();
        }
        return hashCode18 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackOverlayPerfInfo(width=");
        R.append(this.width);
        R.append(", height=");
        R.append(this.height);
        R.append(", apiName=");
        R.append(this.apiName);
        R.append(", fullscreen=");
        R.append(this.fullscreen);
        R.append(", frameBufferSource=");
        R.append(this.frameBufferSource);
        R.append(", durationUs=");
        R.append(this.durationUs);
        R.append(", frameCount=");
        R.append(this.frameCount);
        R.append(", framesReceived=");
        R.append(this.framesReceived);
        R.append(", framesCaptured=");
        R.append(this.framesCaptured);
        R.append(", lockFailures=");
        R.append(this.lockFailures);
        R.append(", frameTimeMinUs=");
        R.append(this.frameTimeMinUs);
        R.append(", frameTimeMaxUs=");
        R.append(this.frameTimeMaxUs);
        R.append(", frameTimeAvgUs=");
        R.append(this.frameTimeAvgUs);
        R.append(", drawTimeMinUs=");
        R.append(this.drawTimeMinUs);
        R.append(", drawTimeMaxUs=");
        R.append(this.drawTimeMaxUs);
        R.append(", drawTimeAvgUs=");
        R.append(this.drawTimeAvgUs);
        R.append(", captureTimeMinUs=");
        R.append(this.captureTimeMinUs);
        R.append(", captureTimeMaxUs=");
        R.append(this.captureTimeMaxUs);
        R.append(", captureTimeAvgUs=");
        return a.F(R, this.captureTimeAvgUs, ")");
    }
}
