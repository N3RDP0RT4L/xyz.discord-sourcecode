package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: TrackUpdateUserSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u000f\n\u0002\u0010\r\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010 \n\u0002\b-\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u001c\u0010\u0017\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u0005R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u000f\u001a\u0004\b\u001b\u0010\u0011R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u000f\u001a\u0004\b\u001d\u0010\u0011R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u0014\u001a\u0004\b\u001f\u0010\u0016R\u001b\u0010 \u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u000f\u001a\u0004\b!\u0010\u0011R\u001b\u0010#\u001a\u0004\u0018\u00010\"8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010$\u001a\u0004\b%\u0010&R\u001b\u0010'\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010\u000f\u001a\u0004\b(\u0010\u0011R\u001b\u0010)\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010\u0014\u001a\u0004\b*\u0010\u0016R\u001b\u0010+\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010\u0014\u001a\u0004\b,\u0010\u0016R$\u0010.\u001a\u0004\u0018\u00010-8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b.\u0010/\u001a\u0004\b0\u00101\"\u0004\b2\u00103R\u001b\u00104\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010\u000f\u001a\u0004\b5\u0010\u0011R!\u00107\u001a\n\u0012\u0004\u0012\u00020\u0012\u0018\u0001068\u0006@\u0006¢\u0006\f\n\u0004\b7\u00108\u001a\u0004\b9\u0010:R\u001b\u0010;\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010\u000f\u001a\u0004\b<\u0010\u0011R\u001b\u0010=\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b=\u0010\u000f\u001a\u0004\b>\u0010\u0011R\u001b\u0010?\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010\u0014\u001a\u0004\b@\u0010\u0016R\u001b\u0010A\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bA\u0010\u000f\u001a\u0004\bB\u0010\u0011R\u001b\u0010C\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bC\u0010\u000f\u001a\u0004\bD\u0010\u0011R\u001b\u0010E\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bE\u0010\u000f\u001a\u0004\bF\u0010\u0011R!\u0010G\u001a\n\u0012\u0004\u0012\u00020\u0012\u0018\u0001068\u0006@\u0006¢\u0006\f\n\u0004\bG\u00108\u001a\u0004\bH\u0010:R\u001b\u0010I\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\bI\u0010\u0014\u001a\u0004\bJ\u0010\u0016R\u001b\u0010K\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bK\u0010\u000f\u001a\u0004\bL\u0010\u0011R\u001b\u0010M\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bM\u0010\u000f\u001a\u0004\bN\u0010\u0011R\u001b\u0010O\u001a\u0004\u0018\u00010\"8\u0006@\u0006¢\u0006\f\n\u0004\bO\u0010$\u001a\u0004\bP\u0010&R\u001b\u0010Q\u001a\u0004\u0018\u00010\"8\u0006@\u0006¢\u0006\f\n\u0004\bQ\u0010$\u001a\u0004\bR\u0010&R\u001b\u0010S\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bS\u0010\u000f\u001a\u0004\bT\u0010\u0011R\u001b\u0010U\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bU\u0010\u000f\u001a\u0004\bV\u0010\u0011R\u001b\u0010W\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bW\u0010\u000f\u001a\u0004\bX\u0010\u0011R\u001b\u0010Y\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bY\u0010\u000f\u001a\u0004\bZ\u0010\u0011R\u001b\u0010[\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b[\u0010\u000f\u001a\u0004\b\\\u0010\u0011R\u001b\u0010]\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b]\u0010\u000f\u001a\u0004\b^\u0010\u0011R\u001b\u0010_\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b_\u0010\u000f\u001a\u0004\b`\u0010\u0011R\u001b\u0010a\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\ba\u0010\u000f\u001a\u0004\bb\u0010\u0011¨\u0006c"}, d2 = {"Lcom/discord/analytics/generated/events/TrackUpdateUserSettings;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "animateEmoji", "Ljava/lang/Boolean;", "getAnimateEmoji", "()Ljava/lang/Boolean;", "", "explicitContentFilter", "Ljava/lang/Long;", "getExplicitContentFilter", "()Ljava/lang/Long;", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "streamNotificationsEnabled", "getStreamNotificationsEnabled", "allowAccessibilityDetection", "getAllowAccessibilityDetection", "numServerFolders", "getNumServerFolders", "passwordless", "getPasswordless", "", "status", "Ljava/lang/CharSequence;", "getStatus", "()Ljava/lang/CharSequence;", "developerMode", "getDeveloperMode", "friendSourceFlags", "getFriendSourceFlags", "timezoneOffset", "getTimezoneOffset", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "showCurrentGame", "getShowCurrentGame", "", "restrictedGuilds", "Ljava/util/List;", "getRestrictedGuilds", "()Ljava/util/List;", "defaultGuildsRestricted", "getDefaultGuildsRestricted", "inlineAttachmentMedia", "getInlineAttachmentMedia", "animateStickers", "getAnimateStickers", "gifAutoPlay", "getGifAutoPlay", "disableGamesTab", "getDisableGamesTab", "renderEmbeds", "getRenderEmbeds", "guildPositions", "getGuildPositions", "afkTimeout", "getAfkTimeout", "messageDisplayCompact", "getMessageDisplayCompact", "convertEmoticons", "getConvertEmoticons", "locale", "getLocale", "theme", "getTheme", "detectPlatformAccounts", "getDetectPlatformAccounts", "hasCustomStatus", "getHasCustomStatus", "contactSyncEnabled", "getContactSyncEnabled", "nativePhoneIntegrationEnabled", "getNativePhoneIntegrationEnabled", "viewNsfwGuilds", "getViewNsfwGuilds", "renderReactions", "getRenderReactions", "enableTtsCommand", "getEnableTtsCommand", "inlineEmbedMedia", "getInlineEmbedMedia", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackUpdateUserSettings implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Boolean convertEmoticons = null;
    private final Boolean developerMode = null;
    private final Boolean enableTtsCommand = null;
    private final Long friendSourceFlags = null;
    private final List<Long> guildPositions = null;
    private final Boolean inlineAttachmentMedia = null;
    private final Boolean inlineEmbedMedia = null;
    private final Boolean gifAutoPlay = null;
    private final CharSequence locale = null;
    private final Boolean messageDisplayCompact = null;
    private final Boolean renderEmbeds = null;
    private final Boolean renderReactions = null;
    private final Boolean animateEmoji = null;
    private final List<Long> restrictedGuilds = null;
    private final Boolean showCurrentGame = null;
    private final CharSequence theme = null;
    private final Boolean detectPlatformAccounts = null;
    private final CharSequence status = null;
    private final Boolean defaultGuildsRestricted = null;
    private final Long explicitContentFilter = null;
    private final Long afkTimeout = null;
    private final Long timezoneOffset = null;
    private final Boolean disableGamesTab = null;
    private final Long numServerFolders = null;
    private final Boolean streamNotificationsEnabled = null;
    private final Boolean hasCustomStatus = null;
    private final Boolean allowAccessibilityDetection = null;
    private final Boolean contactSyncEnabled = null;
    private final Boolean nativePhoneIntegrationEnabled = null;
    private final Long animateStickers = null;
    private final Boolean viewNsfwGuilds = null;
    private final Boolean passwordless = null;
    private final transient String analyticsSchemaTypeName = "update_user_settings";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackUpdateUserSettings)) {
            return false;
        }
        TrackUpdateUserSettings trackUpdateUserSettings = (TrackUpdateUserSettings) obj;
        return m.areEqual(this.convertEmoticons, trackUpdateUserSettings.convertEmoticons) && m.areEqual(this.developerMode, trackUpdateUserSettings.developerMode) && m.areEqual(this.enableTtsCommand, trackUpdateUserSettings.enableTtsCommand) && m.areEqual(this.friendSourceFlags, trackUpdateUserSettings.friendSourceFlags) && m.areEqual(this.guildPositions, trackUpdateUserSettings.guildPositions) && m.areEqual(this.inlineAttachmentMedia, trackUpdateUserSettings.inlineAttachmentMedia) && m.areEqual(this.inlineEmbedMedia, trackUpdateUserSettings.inlineEmbedMedia) && m.areEqual(this.gifAutoPlay, trackUpdateUserSettings.gifAutoPlay) && m.areEqual(this.locale, trackUpdateUserSettings.locale) && m.areEqual(this.messageDisplayCompact, trackUpdateUserSettings.messageDisplayCompact) && m.areEqual(this.renderEmbeds, trackUpdateUserSettings.renderEmbeds) && m.areEqual(this.renderReactions, trackUpdateUserSettings.renderReactions) && m.areEqual(this.animateEmoji, trackUpdateUserSettings.animateEmoji) && m.areEqual(this.restrictedGuilds, trackUpdateUserSettings.restrictedGuilds) && m.areEqual(this.showCurrentGame, trackUpdateUserSettings.showCurrentGame) && m.areEqual(this.theme, trackUpdateUserSettings.theme) && m.areEqual(this.detectPlatformAccounts, trackUpdateUserSettings.detectPlatformAccounts) && m.areEqual(this.status, trackUpdateUserSettings.status) && m.areEqual(this.defaultGuildsRestricted, trackUpdateUserSettings.defaultGuildsRestricted) && m.areEqual(this.explicitContentFilter, trackUpdateUserSettings.explicitContentFilter) && m.areEqual(this.afkTimeout, trackUpdateUserSettings.afkTimeout) && m.areEqual(this.timezoneOffset, trackUpdateUserSettings.timezoneOffset) && m.areEqual(this.disableGamesTab, trackUpdateUserSettings.disableGamesTab) && m.areEqual(this.numServerFolders, trackUpdateUserSettings.numServerFolders) && m.areEqual(this.streamNotificationsEnabled, trackUpdateUserSettings.streamNotificationsEnabled) && m.areEqual(this.hasCustomStatus, trackUpdateUserSettings.hasCustomStatus) && m.areEqual(this.allowAccessibilityDetection, trackUpdateUserSettings.allowAccessibilityDetection) && m.areEqual(this.contactSyncEnabled, trackUpdateUserSettings.contactSyncEnabled) && m.areEqual(this.nativePhoneIntegrationEnabled, trackUpdateUserSettings.nativePhoneIntegrationEnabled) && m.areEqual(this.animateStickers, trackUpdateUserSettings.animateStickers) && m.areEqual(this.viewNsfwGuilds, trackUpdateUserSettings.viewNsfwGuilds) && m.areEqual(this.passwordless, trackUpdateUserSettings.passwordless);
    }

    public int hashCode() {
        Boolean bool = this.convertEmoticons;
        int i = 0;
        int hashCode = (bool != null ? bool.hashCode() : 0) * 31;
        Boolean bool2 = this.developerMode;
        int hashCode2 = (hashCode + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Boolean bool3 = this.enableTtsCommand;
        int hashCode3 = (hashCode2 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        Long l = this.friendSourceFlags;
        int hashCode4 = (hashCode3 + (l != null ? l.hashCode() : 0)) * 31;
        List<Long> list = this.guildPositions;
        int hashCode5 = (hashCode4 + (list != null ? list.hashCode() : 0)) * 31;
        Boolean bool4 = this.inlineAttachmentMedia;
        int hashCode6 = (hashCode5 + (bool4 != null ? bool4.hashCode() : 0)) * 31;
        Boolean bool5 = this.inlineEmbedMedia;
        int hashCode7 = (hashCode6 + (bool5 != null ? bool5.hashCode() : 0)) * 31;
        Boolean bool6 = this.gifAutoPlay;
        int hashCode8 = (hashCode7 + (bool6 != null ? bool6.hashCode() : 0)) * 31;
        CharSequence charSequence = this.locale;
        int hashCode9 = (hashCode8 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Boolean bool7 = this.messageDisplayCompact;
        int hashCode10 = (hashCode9 + (bool7 != null ? bool7.hashCode() : 0)) * 31;
        Boolean bool8 = this.renderEmbeds;
        int hashCode11 = (hashCode10 + (bool8 != null ? bool8.hashCode() : 0)) * 31;
        Boolean bool9 = this.renderReactions;
        int hashCode12 = (hashCode11 + (bool9 != null ? bool9.hashCode() : 0)) * 31;
        Boolean bool10 = this.animateEmoji;
        int hashCode13 = (hashCode12 + (bool10 != null ? bool10.hashCode() : 0)) * 31;
        List<Long> list2 = this.restrictedGuilds;
        int hashCode14 = (hashCode13 + (list2 != null ? list2.hashCode() : 0)) * 31;
        Boolean bool11 = this.showCurrentGame;
        int hashCode15 = (hashCode14 + (bool11 != null ? bool11.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.theme;
        int hashCode16 = (hashCode15 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Boolean bool12 = this.detectPlatformAccounts;
        int hashCode17 = (hashCode16 + (bool12 != null ? bool12.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.status;
        int hashCode18 = (hashCode17 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Boolean bool13 = this.defaultGuildsRestricted;
        int hashCode19 = (hashCode18 + (bool13 != null ? bool13.hashCode() : 0)) * 31;
        Long l2 = this.explicitContentFilter;
        int hashCode20 = (hashCode19 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.afkTimeout;
        int hashCode21 = (hashCode20 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.timezoneOffset;
        int hashCode22 = (hashCode21 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Boolean bool14 = this.disableGamesTab;
        int hashCode23 = (hashCode22 + (bool14 != null ? bool14.hashCode() : 0)) * 31;
        Long l5 = this.numServerFolders;
        int hashCode24 = (hashCode23 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Boolean bool15 = this.streamNotificationsEnabled;
        int hashCode25 = (hashCode24 + (bool15 != null ? bool15.hashCode() : 0)) * 31;
        Boolean bool16 = this.hasCustomStatus;
        int hashCode26 = (hashCode25 + (bool16 != null ? bool16.hashCode() : 0)) * 31;
        Boolean bool17 = this.allowAccessibilityDetection;
        int hashCode27 = (hashCode26 + (bool17 != null ? bool17.hashCode() : 0)) * 31;
        Boolean bool18 = this.contactSyncEnabled;
        int hashCode28 = (hashCode27 + (bool18 != null ? bool18.hashCode() : 0)) * 31;
        Boolean bool19 = this.nativePhoneIntegrationEnabled;
        int hashCode29 = (hashCode28 + (bool19 != null ? bool19.hashCode() : 0)) * 31;
        Long l6 = this.animateStickers;
        int hashCode30 = (hashCode29 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Boolean bool20 = this.viewNsfwGuilds;
        int hashCode31 = (hashCode30 + (bool20 != null ? bool20.hashCode() : 0)) * 31;
        Boolean bool21 = this.passwordless;
        if (bool21 != null) {
            i = bool21.hashCode();
        }
        return hashCode31 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackUpdateUserSettings(convertEmoticons=");
        R.append(this.convertEmoticons);
        R.append(", developerMode=");
        R.append(this.developerMode);
        R.append(", enableTtsCommand=");
        R.append(this.enableTtsCommand);
        R.append(", friendSourceFlags=");
        R.append(this.friendSourceFlags);
        R.append(", guildPositions=");
        R.append(this.guildPositions);
        R.append(", inlineAttachmentMedia=");
        R.append(this.inlineAttachmentMedia);
        R.append(", inlineEmbedMedia=");
        R.append(this.inlineEmbedMedia);
        R.append(", gifAutoPlay=");
        R.append(this.gifAutoPlay);
        R.append(", locale=");
        R.append(this.locale);
        R.append(", messageDisplayCompact=");
        R.append(this.messageDisplayCompact);
        R.append(", renderEmbeds=");
        R.append(this.renderEmbeds);
        R.append(", renderReactions=");
        R.append(this.renderReactions);
        R.append(", animateEmoji=");
        R.append(this.animateEmoji);
        R.append(", restrictedGuilds=");
        R.append(this.restrictedGuilds);
        R.append(", showCurrentGame=");
        R.append(this.showCurrentGame);
        R.append(", theme=");
        R.append(this.theme);
        R.append(", detectPlatformAccounts=");
        R.append(this.detectPlatformAccounts);
        R.append(", status=");
        R.append(this.status);
        R.append(", defaultGuildsRestricted=");
        R.append(this.defaultGuildsRestricted);
        R.append(", explicitContentFilter=");
        R.append(this.explicitContentFilter);
        R.append(", afkTimeout=");
        R.append(this.afkTimeout);
        R.append(", timezoneOffset=");
        R.append(this.timezoneOffset);
        R.append(", disableGamesTab=");
        R.append(this.disableGamesTab);
        R.append(", numServerFolders=");
        R.append(this.numServerFolders);
        R.append(", streamNotificationsEnabled=");
        R.append(this.streamNotificationsEnabled);
        R.append(", hasCustomStatus=");
        R.append(this.hasCustomStatus);
        R.append(", allowAccessibilityDetection=");
        R.append(this.allowAccessibilityDetection);
        R.append(", contactSyncEnabled=");
        R.append(this.contactSyncEnabled);
        R.append(", nativePhoneIntegrationEnabled=");
        R.append(this.nativePhoneIntegrationEnabled);
        R.append(", animateStickers=");
        R.append(this.animateStickers);
        R.append(", viewNsfwGuilds=");
        R.append(this.viewNsfwGuilds);
        R.append(", passwordless=");
        return a.C(R, this.passwordless, ")");
    }
}
