package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackChannel;
import com.discord.analytics.generated.traits.TrackChannelReceiver;
import com.discord.analytics.generated.traits.TrackGuild;
import com.discord.analytics.generated.traits.TrackGuildReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackFileUploadAlertViewed.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u0007\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u001a\u0010\u000e\u001a\u00020\r2\b\u0010\f\u001a\u0004\u0018\u00010\u000bHÖ\u0003¢\u0006\u0004\b\u000e\u0010\u000fR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R\u001b\u0010\u0014\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0011\u001a\u0004\b\u0015\u0010\u0013R\u001c\u0010\u0016\u001a\u00020\u00058\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0007R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00198\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u001dR\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"R$\u0010$\u001a\u0004\u0018\u00010#8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'\"\u0004\b(\u0010)R\u001b\u0010*\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010\u0011\u001a\u0004\b+\u0010\u0013R\u001b\u0010,\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010\u0011\u001a\u0004\b-\u0010\u0013R\u001b\u0010.\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010 \u001a\u0004\b/\u0010\"R\u001b\u00101\u001a\u0004\u0018\u0001008\u0006@\u0006¢\u0006\f\n\u0004\b1\u00102\u001a\u0004\b3\u00104R$\u00106\u001a\u0004\u0018\u0001058\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b6\u00107\u001a\u0004\b8\u00109\"\u0004\b:\u0010;R$\u0010=\u001a\u0004\u0018\u00010<8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b=\u0010>\u001a\u0004\b?\u0010@\"\u0004\bA\u0010BR\u001b\u0010C\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\bC\u0010\u0011\u001a\u0004\bC\u0010\u0013R\u001b\u0010D\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\f\n\u0004\bD\u0010 \u001a\u0004\bE\u0010\"¨\u0006F"}, d2 = {"Lcom/discord/analytics/generated/events/TrackFileUploadAlertViewed;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackGuildReceiver;", "Lcom/discord/analytics/generated/traits/TrackChannelReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "hasVideo", "Ljava/lang/Boolean;", "getHasVideo", "()Ljava/lang/Boolean;", "previewEnabled", "getPreviewEnabled", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "", "alertType", "Ljava/lang/CharSequence;", "getAlertType", "()Ljava/lang/CharSequence;", "", "maxAttachmentSize", "Ljava/lang/Long;", "getMaxAttachmentSize", "()Ljava/lang/Long;", "Lcom/discord/analytics/generated/traits/TrackChannel;", "trackChannel", "Lcom/discord/analytics/generated/traits/TrackChannel;", "getTrackChannel", "()Lcom/discord/analytics/generated/traits/TrackChannel;", "setTrackChannel", "(Lcom/discord/analytics/generated/traits/TrackChannel;)V", "imageCompressionSettingEnabled", "getImageCompressionSettingEnabled", "hasImage", "getHasImage", "numAttachments", "getNumAttachments", "", "imageCompressionQuality", "Ljava/lang/Float;", "getImageCompressionQuality", "()Ljava/lang/Float;", "Lcom/discord/analytics/generated/traits/TrackGuild;", "trackGuild", "Lcom/discord/analytics/generated/traits/TrackGuild;", "getTrackGuild", "()Lcom/discord/analytics/generated/traits/TrackGuild;", "setTrackGuild", "(Lcom/discord/analytics/generated/traits/TrackGuild;)V", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "isPremium", "totalAttachmentSize", "getTotalAttachmentSize", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackFileUploadAlertViewed implements AnalyticsSchema, TrackBaseReceiver, TrackGuildReceiver, TrackChannelReceiver {
    private TrackBase trackBase;
    private TrackChannel trackChannel;
    private TrackGuild trackGuild;
    private final CharSequence alertType = null;
    private final Long numAttachments = null;
    private final Long maxAttachmentSize = null;
    private final Long totalAttachmentSize = null;
    private final Boolean hasImage = null;
    private final Boolean hasVideo = null;
    private final Boolean isPremium = null;
    private final Float imageCompressionQuality = null;
    private final Boolean imageCompressionSettingEnabled = null;
    private final Boolean previewEnabled = null;
    private final transient String analyticsSchemaTypeName = "file_upload_alert_viewed";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackFileUploadAlertViewed)) {
            return false;
        }
        TrackFileUploadAlertViewed trackFileUploadAlertViewed = (TrackFileUploadAlertViewed) obj;
        return m.areEqual(this.alertType, trackFileUploadAlertViewed.alertType) && m.areEqual(this.numAttachments, trackFileUploadAlertViewed.numAttachments) && m.areEqual(this.maxAttachmentSize, trackFileUploadAlertViewed.maxAttachmentSize) && m.areEqual(this.totalAttachmentSize, trackFileUploadAlertViewed.totalAttachmentSize) && m.areEqual(this.hasImage, trackFileUploadAlertViewed.hasImage) && m.areEqual(this.hasVideo, trackFileUploadAlertViewed.hasVideo) && m.areEqual(this.isPremium, trackFileUploadAlertViewed.isPremium) && m.areEqual(this.imageCompressionQuality, trackFileUploadAlertViewed.imageCompressionQuality) && m.areEqual(this.imageCompressionSettingEnabled, trackFileUploadAlertViewed.imageCompressionSettingEnabled) && m.areEqual(this.previewEnabled, trackFileUploadAlertViewed.previewEnabled);
    }

    public int hashCode() {
        CharSequence charSequence = this.alertType;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        Long l = this.numAttachments;
        int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.maxAttachmentSize;
        int hashCode3 = (hashCode2 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.totalAttachmentSize;
        int hashCode4 = (hashCode3 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Boolean bool = this.hasImage;
        int hashCode5 = (hashCode4 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.hasVideo;
        int hashCode6 = (hashCode5 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Boolean bool3 = this.isPremium;
        int hashCode7 = (hashCode6 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        Float f = this.imageCompressionQuality;
        int hashCode8 = (hashCode7 + (f != null ? f.hashCode() : 0)) * 31;
        Boolean bool4 = this.imageCompressionSettingEnabled;
        int hashCode9 = (hashCode8 + (bool4 != null ? bool4.hashCode() : 0)) * 31;
        Boolean bool5 = this.previewEnabled;
        if (bool5 != null) {
            i = bool5.hashCode();
        }
        return hashCode9 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackFileUploadAlertViewed(alertType=");
        R.append(this.alertType);
        R.append(", numAttachments=");
        R.append(this.numAttachments);
        R.append(", maxAttachmentSize=");
        R.append(this.maxAttachmentSize);
        R.append(", totalAttachmentSize=");
        R.append(this.totalAttachmentSize);
        R.append(", hasImage=");
        R.append(this.hasImage);
        R.append(", hasVideo=");
        R.append(this.hasVideo);
        R.append(", isPremium=");
        R.append(this.isPremium);
        R.append(", imageCompressionQuality=");
        R.append(this.imageCompressionQuality);
        R.append(", imageCompressionSettingEnabled=");
        R.append(this.imageCompressionSettingEnabled);
        R.append(", previewEnabled=");
        return a.C(R, this.previewEnabled, ")");
    }
}
