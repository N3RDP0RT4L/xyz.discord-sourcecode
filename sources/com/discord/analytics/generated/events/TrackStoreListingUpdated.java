package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: TrackStoreListingUpdated.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010 \n\u0002\b\b\n\u0002\u0010\t\n\u0002\b\u0012\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u000e\u0010\u0010R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R$\u0010\u0017\u001a\u0004\u0018\u00010\u00168\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u0013\u001a\u0004\b\u001e\u0010\u0015R!\u0010 \u001a\n\u0012\u0004\u0012\u00020\u0011\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#R\u001b\u0010$\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u0013\u001a\u0004\b%\u0010\u0015R\u001b\u0010&\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010\u0013\u001a\u0004\b'\u0010\u0015R!\u0010)\u001a\n\u0012\u0004\u0012\u00020(\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010!\u001a\u0004\b*\u0010#R\u001c\u0010+\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b+\u0010,\u001a\u0004\b-\u0010\u0005R\u001b\u0010.\u001a\u0004\u0018\u00010(8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010/\u001a\u0004\b0\u00101R\u001b\u00102\u001a\u0004\u0018\u00010(8\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010/\u001a\u0004\b3\u00101R\u001b\u00104\u001a\u0004\u0018\u00010(8\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010/\u001a\u0004\b5\u00101R\u001b\u00106\u001a\u0004\u0018\u00010(8\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010/\u001a\u0004\b7\u00101R\u001b\u00108\u001a\u0004\u0018\u00010(8\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010/\u001a\u0004\b9\u00101¨\u0006:"}, d2 = {"Lcom/discord/analytics/generated/events/TrackStoreListingUpdated;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "isPublished", "Ljava/lang/Boolean;", "()Ljava/lang/Boolean;", "", "updateFrom", "Ljava/lang/CharSequence;", "getUpdateFrom", "()Ljava/lang/CharSequence;", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "updateType", "getUpdateType", "", "carouselItems", "Ljava/util/List;", "getCarouselItems", "()Ljava/util/List;", "tagline", "getTagline", "flavorText", "getFlavorText", "", "childSkuIds", "getChildSkuIds", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "guildId", "Ljava/lang/Long;", "getGuildId", "()Ljava/lang/Long;", "storeListingId", "getStoreListingId", "previewVideoAssetId", "getPreviewVideoAssetId", "thumbnailAssetId", "getThumbnailAssetId", "skuId", "getSkuId", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackStoreListingUpdated implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Long storeListingId = null;
    private final Long skuId = null;
    private final CharSequence updateFrom = null;
    private final CharSequence updateType = null;
    private final Long guildId = null;
    private final List<Long> childSkuIds = null;
    private final Boolean isPublished = null;
    private final CharSequence tagline = null;
    private final CharSequence flavorText = null;
    private final Long thumbnailAssetId = null;
    private final Long previewVideoAssetId = null;
    private final List<CharSequence> carouselItems = null;
    private final transient String analyticsSchemaTypeName = "store_listing_updated";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackStoreListingUpdated)) {
            return false;
        }
        TrackStoreListingUpdated trackStoreListingUpdated = (TrackStoreListingUpdated) obj;
        return m.areEqual(this.storeListingId, trackStoreListingUpdated.storeListingId) && m.areEqual(this.skuId, trackStoreListingUpdated.skuId) && m.areEqual(this.updateFrom, trackStoreListingUpdated.updateFrom) && m.areEqual(this.updateType, trackStoreListingUpdated.updateType) && m.areEqual(this.guildId, trackStoreListingUpdated.guildId) && m.areEqual(this.childSkuIds, trackStoreListingUpdated.childSkuIds) && m.areEqual(this.isPublished, trackStoreListingUpdated.isPublished) && m.areEqual(this.tagline, trackStoreListingUpdated.tagline) && m.areEqual(this.flavorText, trackStoreListingUpdated.flavorText) && m.areEqual(this.thumbnailAssetId, trackStoreListingUpdated.thumbnailAssetId) && m.areEqual(this.previewVideoAssetId, trackStoreListingUpdated.previewVideoAssetId) && m.areEqual(this.carouselItems, trackStoreListingUpdated.carouselItems);
    }

    public int hashCode() {
        Long l = this.storeListingId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.skuId;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        CharSequence charSequence = this.updateFrom;
        int hashCode3 = (hashCode2 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.updateType;
        int hashCode4 = (hashCode3 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l3 = this.guildId;
        int hashCode5 = (hashCode4 + (l3 != null ? l3.hashCode() : 0)) * 31;
        List<Long> list = this.childSkuIds;
        int hashCode6 = (hashCode5 + (list != null ? list.hashCode() : 0)) * 31;
        Boolean bool = this.isPublished;
        int hashCode7 = (hashCode6 + (bool != null ? bool.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.tagline;
        int hashCode8 = (hashCode7 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.flavorText;
        int hashCode9 = (hashCode8 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        Long l4 = this.thumbnailAssetId;
        int hashCode10 = (hashCode9 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.previewVideoAssetId;
        int hashCode11 = (hashCode10 + (l5 != null ? l5.hashCode() : 0)) * 31;
        List<CharSequence> list2 = this.carouselItems;
        if (list2 != null) {
            i = list2.hashCode();
        }
        return hashCode11 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackStoreListingUpdated(storeListingId=");
        R.append(this.storeListingId);
        R.append(", skuId=");
        R.append(this.skuId);
        R.append(", updateFrom=");
        R.append(this.updateFrom);
        R.append(", updateType=");
        R.append(this.updateType);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", childSkuIds=");
        R.append(this.childSkuIds);
        R.append(", isPublished=");
        R.append(this.isPublished);
        R.append(", tagline=");
        R.append(this.tagline);
        R.append(", flavorText=");
        R.append(this.flavorText);
        R.append(", thumbnailAssetId=");
        R.append(this.thumbnailAssetId);
        R.append(", previewVideoAssetId=");
        R.append(this.previewVideoAssetId);
        R.append(", carouselItems=");
        return a.K(R, this.carouselItems, ")");
    }
}
