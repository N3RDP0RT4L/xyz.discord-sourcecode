package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackUserAccountUpdated.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\n\n\u0002\u0010\t\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u001d\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0010\u001a\u0004\b\u0014\u0010\u0012R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0010\u001a\u0004\b\u0016\u0010\u0012R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0010\u001a\u0004\b\u0018\u0010\u0012R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00198\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u001dR\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010!R\u001b\u0010\"\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u001f\u001a\u0004\b#\u0010!R$\u0010%\u001a\u0004\u0018\u00010$8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b%\u0010&\u001a\u0004\b'\u0010(\"\u0004\b)\u0010*R\u001b\u0010+\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010\u001f\u001a\u0004\b,\u0010!R\u001c\u0010-\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b-\u0010.\u001a\u0004\b/\u0010\u0005R\u001b\u00100\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010\u001f\u001a\u0004\b1\u0010!R\u001b\u00102\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010\u001f\u001a\u0004\b2\u0010!R\u001b\u00103\u001a\u0004\u0018\u00010\u00198\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010\u001b\u001a\u0004\b4\u0010\u001dR\u001b\u00105\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010\u001f\u001a\u0004\b6\u0010!R\u001b\u00107\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010\u001f\u001a\u0004\b8\u0010!R\u001b\u00109\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010\u001f\u001a\u0004\b:\u0010!R\u001b\u0010;\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010\u001f\u001a\u0004\b<\u0010!R\u001b\u0010=\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b=\u0010\u001f\u001a\u0004\b>\u0010!R\u001b\u0010?\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010\u001f\u001a\u0004\b@\u0010!¨\u0006A"}, d2 = {"Lcom/discord/analytics/generated/events/TrackUserAccountUpdated;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "oldUsername", "Ljava/lang/CharSequence;", "getOldUsername", "()Ljava/lang/CharSequence;", "oldEmail", "getOldEmail", "newUsername", "getNewUsername", "newEmail", "getNewEmail", "", "oldDiscriminator", "Ljava/lang/Long;", "getOldDiscriminator", "()Ljava/lang/Long;", "hasBannerColor", "Ljava/lang/Boolean;", "getHasBannerColor", "()Ljava/lang/Boolean;", "updatedBio", "getUpdatedBio", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "updatedBanner", "getUpdatedBanner", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "hasBanner", "getHasBanner", "isUserSetDiscriminator", "newDiscriminator", "getNewDiscriminator", "hasPremium", "getHasPremium", "hasAnimatedAvatar", "getHasAnimatedAvatar", "updatedAvatar", "getUpdatedAvatar", "hasBio", "getHasBio", "hasAvatar", "getHasAvatar", "updatedBannerColor", "getUpdatedBannerColor", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackUserAccountUpdated implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Boolean isUserSetDiscriminator = null;
    private final Long oldDiscriminator = null;
    private final Long newDiscriminator = null;
    private final CharSequence oldUsername = null;
    private final CharSequence newUsername = null;
    private final CharSequence oldEmail = null;
    private final CharSequence newEmail = null;
    private final Boolean hasBanner = null;
    private final Boolean hasBannerColor = null;
    private final Boolean hasBio = null;
    private final Boolean hasPremium = null;
    private final Boolean hasAvatar = null;
    private final Boolean hasAnimatedAvatar = null;
    private final Boolean updatedBio = null;
    private final Boolean updatedBanner = null;
    private final Boolean updatedBannerColor = null;
    private final Boolean updatedAvatar = null;
    private final transient String analyticsSchemaTypeName = "user_account_updated";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackUserAccountUpdated)) {
            return false;
        }
        TrackUserAccountUpdated trackUserAccountUpdated = (TrackUserAccountUpdated) obj;
        return m.areEqual(this.isUserSetDiscriminator, trackUserAccountUpdated.isUserSetDiscriminator) && m.areEqual(this.oldDiscriminator, trackUserAccountUpdated.oldDiscriminator) && m.areEqual(this.newDiscriminator, trackUserAccountUpdated.newDiscriminator) && m.areEqual(this.oldUsername, trackUserAccountUpdated.oldUsername) && m.areEqual(this.newUsername, trackUserAccountUpdated.newUsername) && m.areEqual(this.oldEmail, trackUserAccountUpdated.oldEmail) && m.areEqual(this.newEmail, trackUserAccountUpdated.newEmail) && m.areEqual(this.hasBanner, trackUserAccountUpdated.hasBanner) && m.areEqual(this.hasBannerColor, trackUserAccountUpdated.hasBannerColor) && m.areEqual(this.hasBio, trackUserAccountUpdated.hasBio) && m.areEqual(this.hasPremium, trackUserAccountUpdated.hasPremium) && m.areEqual(this.hasAvatar, trackUserAccountUpdated.hasAvatar) && m.areEqual(this.hasAnimatedAvatar, trackUserAccountUpdated.hasAnimatedAvatar) && m.areEqual(this.updatedBio, trackUserAccountUpdated.updatedBio) && m.areEqual(this.updatedBanner, trackUserAccountUpdated.updatedBanner) && m.areEqual(this.updatedBannerColor, trackUserAccountUpdated.updatedBannerColor) && m.areEqual(this.updatedAvatar, trackUserAccountUpdated.updatedAvatar);
    }

    public int hashCode() {
        Boolean bool = this.isUserSetDiscriminator;
        int i = 0;
        int hashCode = (bool != null ? bool.hashCode() : 0) * 31;
        Long l = this.oldDiscriminator;
        int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.newDiscriminator;
        int hashCode3 = (hashCode2 + (l2 != null ? l2.hashCode() : 0)) * 31;
        CharSequence charSequence = this.oldUsername;
        int hashCode4 = (hashCode3 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.newUsername;
        int hashCode5 = (hashCode4 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.oldEmail;
        int hashCode6 = (hashCode5 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.newEmail;
        int hashCode7 = (hashCode6 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        Boolean bool2 = this.hasBanner;
        int hashCode8 = (hashCode7 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Boolean bool3 = this.hasBannerColor;
        int hashCode9 = (hashCode8 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        Boolean bool4 = this.hasBio;
        int hashCode10 = (hashCode9 + (bool4 != null ? bool4.hashCode() : 0)) * 31;
        Boolean bool5 = this.hasPremium;
        int hashCode11 = (hashCode10 + (bool5 != null ? bool5.hashCode() : 0)) * 31;
        Boolean bool6 = this.hasAvatar;
        int hashCode12 = (hashCode11 + (bool6 != null ? bool6.hashCode() : 0)) * 31;
        Boolean bool7 = this.hasAnimatedAvatar;
        int hashCode13 = (hashCode12 + (bool7 != null ? bool7.hashCode() : 0)) * 31;
        Boolean bool8 = this.updatedBio;
        int hashCode14 = (hashCode13 + (bool8 != null ? bool8.hashCode() : 0)) * 31;
        Boolean bool9 = this.updatedBanner;
        int hashCode15 = (hashCode14 + (bool9 != null ? bool9.hashCode() : 0)) * 31;
        Boolean bool10 = this.updatedBannerColor;
        int hashCode16 = (hashCode15 + (bool10 != null ? bool10.hashCode() : 0)) * 31;
        Boolean bool11 = this.updatedAvatar;
        if (bool11 != null) {
            i = bool11.hashCode();
        }
        return hashCode16 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackUserAccountUpdated(isUserSetDiscriminator=");
        R.append(this.isUserSetDiscriminator);
        R.append(", oldDiscriminator=");
        R.append(this.oldDiscriminator);
        R.append(", newDiscriminator=");
        R.append(this.newDiscriminator);
        R.append(", oldUsername=");
        R.append(this.oldUsername);
        R.append(", newUsername=");
        R.append(this.newUsername);
        R.append(", oldEmail=");
        R.append(this.oldEmail);
        R.append(", newEmail=");
        R.append(this.newEmail);
        R.append(", hasBanner=");
        R.append(this.hasBanner);
        R.append(", hasBannerColor=");
        R.append(this.hasBannerColor);
        R.append(", hasBio=");
        R.append(this.hasBio);
        R.append(", hasPremium=");
        R.append(this.hasPremium);
        R.append(", hasAvatar=");
        R.append(this.hasAvatar);
        R.append(", hasAnimatedAvatar=");
        R.append(this.hasAnimatedAvatar);
        R.append(", updatedBio=");
        R.append(this.updatedBio);
        R.append(", updatedBanner=");
        R.append(this.updatedBanner);
        R.append(", updatedBannerColor=");
        R.append(this.updatedBannerColor);
        R.append(", updatedAvatar=");
        return a.C(R, this.updatedAvatar, ")");
    }
}
