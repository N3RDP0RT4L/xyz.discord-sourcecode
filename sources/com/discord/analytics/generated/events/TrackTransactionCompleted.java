package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackTransactionCompleted.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\r\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0014\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0015\u001a\u0004\b\u0019\u0010\u0017R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0015\u001a\u0004\b\u001b\u0010\u0017R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u0015\u001a\u0004\b\u001d\u0010\u0017R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u0015\u001a\u0004\b\u001f\u0010\u0017R\u001b\u0010 \u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0015\u001a\u0004\b!\u0010\u0017R\u001b\u0010\"\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u0015\u001a\u0004\b#\u0010\u0017R$\u0010%\u001a\u0004\u0018\u00010$8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b%\u0010&\u001a\u0004\b'\u0010(\"\u0004\b)\u0010*R\u001b\u0010+\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010\u0015\u001a\u0004\b,\u0010\u0017R\u001b\u0010-\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u0015\u001a\u0004\b.\u0010\u0017R\u001b\u00100\u001a\u0004\u0018\u00010/8\u0006@\u0006¢\u0006\f\n\u0004\b0\u00101\u001a\u0004\b2\u00103R\u001b\u00104\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010\u0015\u001a\u0004\b5\u0010\u0017R\u001b\u00106\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010\u0015\u001a\u0004\b7\u0010\u0017R\u001b\u00108\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010\u0015\u001a\u0004\b9\u0010\u0017R\u001b\u0010:\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010\u0015\u001a\u0004\b;\u0010\u0017R!\u0010=\u001a\n\u0018\u00010\u0013j\u0004\u0018\u0001`<8\u0006@\u0006¢\u0006\f\n\u0004\b=\u0010\u0015\u001a\u0004\b>\u0010\u0017R\u001c\u0010?\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b?\u0010@\u001a\u0004\bA\u0010\u0005R\u001b\u0010B\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010\u0015\u001a\u0004\bC\u0010\u0017R\u001b\u0010D\u001a\u0004\u0018\u00010/8\u0006@\u0006¢\u0006\f\n\u0004\bD\u00101\u001a\u0004\bE\u00103R\u001b\u0010F\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bF\u0010\u0015\u001a\u0004\bG\u0010\u0017R\u001b\u0010H\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bH\u0010\u0015\u001a\u0004\bI\u0010\u0017R\u001b\u0010J\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bJ\u0010\u0015\u001a\u0004\bK\u0010\u0017¨\u0006L"}, d2 = {"Lcom/discord/analytics/generated/events/TrackTransactionCompleted;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "exchangeRate", "Ljava/lang/Float;", "getExchangeRate", "()Ljava/lang/Float;", "", "transactionType", "Ljava/lang/Long;", "getTransactionType", "()Ljava/lang/Long;", "transactionId", "getTransactionId", "settlementAmount", "getSettlementAmount", "skuId", "getSkuId", "netPresentmentTax", "getNetPresentmentTax", "netSettlementTax", "getNetSettlementTax", "paymentGateway", "getPaymentGateway", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "settlementFees", "getSettlementFees", "netPresentmentFees", "getNetPresentmentFees", "", "settlementCurrency", "Ljava/lang/CharSequence;", "getSettlementCurrency", "()Ljava/lang/CharSequence;", "settlementTax", "getSettlementTax", "presentmentFees", "getPresentmentFees", "netPresentmentAmount", "getNetPresentmentAmount", "netSettlementAmount", "getNetSettlementAmount", "Lcom/discord/primitives/Timestamp;", "createdAt", "getCreatedAt", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "paymentId", "getPaymentId", "presentmentCurrency", "getPresentmentCurrency", "presentmentAmount", "getPresentmentAmount", "presentmentTax", "getPresentmentTax", "netSettlementFees", "getNetSettlementFees", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackTransactionCompleted implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Long paymentId = null;
    private final Long paymentGateway = null;
    private final Long createdAt = null;
    private final Long transactionId = null;
    private final Long transactionType = null;
    private final Long skuId = null;
    private final Float exchangeRate = null;
    private final CharSequence presentmentCurrency = null;
    private final CharSequence settlementCurrency = null;
    private final Long presentmentAmount = null;
    private final Long presentmentFees = null;
    private final Long presentmentTax = null;
    private final Long settlementAmount = null;
    private final Long settlementFees = null;
    private final Long settlementTax = null;
    private final Long netPresentmentAmount = null;
    private final Long netPresentmentFees = null;
    private final Long netPresentmentTax = null;
    private final Long netSettlementAmount = null;
    private final Long netSettlementFees = null;
    private final Long netSettlementTax = null;
    private final transient String analyticsSchemaTypeName = "transaction_completed";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackTransactionCompleted)) {
            return false;
        }
        TrackTransactionCompleted trackTransactionCompleted = (TrackTransactionCompleted) obj;
        return m.areEqual(this.paymentId, trackTransactionCompleted.paymentId) && m.areEqual(this.paymentGateway, trackTransactionCompleted.paymentGateway) && m.areEqual(this.createdAt, trackTransactionCompleted.createdAt) && m.areEqual(this.transactionId, trackTransactionCompleted.transactionId) && m.areEqual(this.transactionType, trackTransactionCompleted.transactionType) && m.areEqual(this.skuId, trackTransactionCompleted.skuId) && m.areEqual(this.exchangeRate, trackTransactionCompleted.exchangeRate) && m.areEqual(this.presentmentCurrency, trackTransactionCompleted.presentmentCurrency) && m.areEqual(this.settlementCurrency, trackTransactionCompleted.settlementCurrency) && m.areEqual(this.presentmentAmount, trackTransactionCompleted.presentmentAmount) && m.areEqual(this.presentmentFees, trackTransactionCompleted.presentmentFees) && m.areEqual(this.presentmentTax, trackTransactionCompleted.presentmentTax) && m.areEqual(this.settlementAmount, trackTransactionCompleted.settlementAmount) && m.areEqual(this.settlementFees, trackTransactionCompleted.settlementFees) && m.areEqual(this.settlementTax, trackTransactionCompleted.settlementTax) && m.areEqual(this.netPresentmentAmount, trackTransactionCompleted.netPresentmentAmount) && m.areEqual(this.netPresentmentFees, trackTransactionCompleted.netPresentmentFees) && m.areEqual(this.netPresentmentTax, trackTransactionCompleted.netPresentmentTax) && m.areEqual(this.netSettlementAmount, trackTransactionCompleted.netSettlementAmount) && m.areEqual(this.netSettlementFees, trackTransactionCompleted.netSettlementFees) && m.areEqual(this.netSettlementTax, trackTransactionCompleted.netSettlementTax);
    }

    public int hashCode() {
        Long l = this.paymentId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.paymentGateway;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.createdAt;
        int hashCode3 = (hashCode2 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.transactionId;
        int hashCode4 = (hashCode3 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.transactionType;
        int hashCode5 = (hashCode4 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.skuId;
        int hashCode6 = (hashCode5 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Float f = this.exchangeRate;
        int hashCode7 = (hashCode6 + (f != null ? f.hashCode() : 0)) * 31;
        CharSequence charSequence = this.presentmentCurrency;
        int hashCode8 = (hashCode7 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.settlementCurrency;
        int hashCode9 = (hashCode8 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l7 = this.presentmentAmount;
        int hashCode10 = (hashCode9 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Long l8 = this.presentmentFees;
        int hashCode11 = (hashCode10 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.presentmentTax;
        int hashCode12 = (hashCode11 + (l9 != null ? l9.hashCode() : 0)) * 31;
        Long l10 = this.settlementAmount;
        int hashCode13 = (hashCode12 + (l10 != null ? l10.hashCode() : 0)) * 31;
        Long l11 = this.settlementFees;
        int hashCode14 = (hashCode13 + (l11 != null ? l11.hashCode() : 0)) * 31;
        Long l12 = this.settlementTax;
        int hashCode15 = (hashCode14 + (l12 != null ? l12.hashCode() : 0)) * 31;
        Long l13 = this.netPresentmentAmount;
        int hashCode16 = (hashCode15 + (l13 != null ? l13.hashCode() : 0)) * 31;
        Long l14 = this.netPresentmentFees;
        int hashCode17 = (hashCode16 + (l14 != null ? l14.hashCode() : 0)) * 31;
        Long l15 = this.netPresentmentTax;
        int hashCode18 = (hashCode17 + (l15 != null ? l15.hashCode() : 0)) * 31;
        Long l16 = this.netSettlementAmount;
        int hashCode19 = (hashCode18 + (l16 != null ? l16.hashCode() : 0)) * 31;
        Long l17 = this.netSettlementFees;
        int hashCode20 = (hashCode19 + (l17 != null ? l17.hashCode() : 0)) * 31;
        Long l18 = this.netSettlementTax;
        if (l18 != null) {
            i = l18.hashCode();
        }
        return hashCode20 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackTransactionCompleted(paymentId=");
        R.append(this.paymentId);
        R.append(", paymentGateway=");
        R.append(this.paymentGateway);
        R.append(", createdAt=");
        R.append(this.createdAt);
        R.append(", transactionId=");
        R.append(this.transactionId);
        R.append(", transactionType=");
        R.append(this.transactionType);
        R.append(", skuId=");
        R.append(this.skuId);
        R.append(", exchangeRate=");
        R.append(this.exchangeRate);
        R.append(", presentmentCurrency=");
        R.append(this.presentmentCurrency);
        R.append(", settlementCurrency=");
        R.append(this.settlementCurrency);
        R.append(", presentmentAmount=");
        R.append(this.presentmentAmount);
        R.append(", presentmentFees=");
        R.append(this.presentmentFees);
        R.append(", presentmentTax=");
        R.append(this.presentmentTax);
        R.append(", settlementAmount=");
        R.append(this.settlementAmount);
        R.append(", settlementFees=");
        R.append(this.settlementFees);
        R.append(", settlementTax=");
        R.append(this.settlementTax);
        R.append(", netPresentmentAmount=");
        R.append(this.netPresentmentAmount);
        R.append(", netPresentmentFees=");
        R.append(this.netPresentmentFees);
        R.append(", netPresentmentTax=");
        R.append(this.netPresentmentTax);
        R.append(", netSettlementAmount=");
        R.append(this.netSettlementAmount);
        R.append(", netSettlementFees=");
        R.append(this.netSettlementFees);
        R.append(", netSettlementTax=");
        return a.F(R, this.netSettlementTax, ")");
    }
}
