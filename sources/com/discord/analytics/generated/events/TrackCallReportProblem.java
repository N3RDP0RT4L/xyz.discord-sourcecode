package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackCallReportProblem.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\r\n\u0002\b\n\n\u0002\u0010\u0007\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b.\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u000f\u001a\u0004\b\u0018\u0010\u0011R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u0014\u001a\u0004\b\u001a\u0010\u0016R\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u0014\u001a\u0004\b\u001c\u0010\u0016R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010!R\u001b\u0010\"\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u000f\u001a\u0004\b#\u0010\u0011R$\u0010%\u001a\u0004\u0018\u00010$8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b%\u0010&\u001a\u0004\b'\u0010(\"\u0004\b)\u0010*R\u001b\u0010,\u001a\u0004\u0018\u00010+8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010-\u001a\u0004\b.\u0010/R\u001b\u00100\u001a\u0004\u0018\u00010\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010\u001f\u001a\u0004\b1\u0010!R\u001b\u00102\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010\u000f\u001a\u0004\b3\u0010\u0011R\u001b\u00104\u001a\u0004\u0018\u00010+8\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010-\u001a\u0004\b5\u0010/R\u001b\u00106\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010\u0014\u001a\u0004\b7\u0010\u0016R\u001b\u00108\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010\u0014\u001a\u0004\b9\u0010\u0016R\u001b\u0010:\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010\u0014\u001a\u0004\b;\u0010\u0016R\u001b\u0010<\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b<\u0010\u000f\u001a\u0004\b=\u0010\u0011R\u001b\u0010>\u001a\u0004\u0018\u00010+8\u0006@\u0006¢\u0006\f\n\u0004\b>\u0010-\u001a\u0004\b?\u0010/R\u001b\u0010@\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b@\u0010\u0014\u001a\u0004\bA\u0010\u0016R\u001b\u0010B\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010\u0014\u001a\u0004\bC\u0010\u0016R\u001b\u0010D\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\bD\u0010\u0014\u001a\u0004\bE\u0010\u0016R\u001b\u0010F\u001a\u0004\u0018\u00010+8\u0006@\u0006¢\u0006\f\n\u0004\bF\u0010-\u001a\u0004\bG\u0010/R\u001c\u0010H\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\bH\u0010I\u001a\u0004\bJ\u0010\u0005R\u001b\u0010K\u001a\u0004\u0018\u00010+8\u0006@\u0006¢\u0006\f\n\u0004\bK\u0010-\u001a\u0004\bL\u0010/R\u001b\u0010M\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bM\u0010\u000f\u001a\u0004\bN\u0010\u0011R\u001b\u0010O\u001a\u0004\u0018\u00010+8\u0006@\u0006¢\u0006\f\n\u0004\bO\u0010-\u001a\u0004\bP\u0010/R\u001b\u0010Q\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\bQ\u0010\u0014\u001a\u0004\bR\u0010\u0016R\u001b\u0010S\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bS\u0010\u000f\u001a\u0004\bT\u0010\u0011R\u001b\u0010U\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\bU\u0010\u0014\u001a\u0004\bV\u0010\u0016R\u001b\u0010W\u001a\u0004\u0018\u00010+8\u0006@\u0006¢\u0006\f\n\u0004\bW\u0010-\u001a\u0004\bX\u0010/¨\u0006Y"}, d2 = {"Lcom/discord/analytics/generated/events/TrackCallReportProblem;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "videoHardwareScalingEnabled", "Ljava/lang/Boolean;", "getVideoHardwareScalingEnabled", "()Ljava/lang/Boolean;", "", "rtcConnectionId", "Ljava/lang/CharSequence;", "getRtcConnectionId", "()Ljava/lang/CharSequence;", "echoCancellationEnabled", "getEchoCancellationEnabled", "videoDeviceName", "getVideoDeviceName", "inputDeviceName", "getInputDeviceName", "", "voiceOutputVolume", "Ljava/lang/Float;", "getVoiceOutputVolume", "()Ljava/lang/Float;", "automaticAudioInputSensitivityEnabled", "getAutomaticAudioInputSensitivityEnabled", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "", "videoStreamCount", "Ljava/lang/Long;", "getVideoStreamCount", "()Ljava/lang/Long;", "audioInputSensitivity", "getAudioInputSensitivity", "automaticGainControlEnabled", "getAutomaticGainControlEnabled", "guildId", "getGuildId", "outputDeviceName", "getOutputDeviceName", "reasonDescription", "getReasonDescription", "mediaSessionId", "getMediaSessionId", "videoEnabled", "getVideoEnabled", "channelType", "getChannelType", "audioOutputMode", "getAudioOutputMode", "audioMode", "getAudioMode", "feedback", "getFeedback", "channelId", "getChannelId", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "voiceStateCount", "getVoiceStateCount", "noiseCancellationEnabled", "getNoiseCancellationEnabled", "duration", "getDuration", "rating", "getRating", "noiseSuppressionEnabled", "getNoiseSuppressionEnabled", "audioInputMode", "getAudioInputMode", "reasonCode", "getReasonCode", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackCallReportProblem implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Long channelId = null;
    private final Long channelType = null;
    private final Long guildId = null;
    private final CharSequence rtcConnectionId = null;
    private final Long duration = null;
    private final Long voiceStateCount = null;
    private final Long videoStreamCount = null;
    private final Boolean videoEnabled = null;
    private final CharSequence mediaSessionId = null;
    private final CharSequence rating = null;
    private final CharSequence feedback = null;
    private final Long reasonCode = null;
    private final CharSequence reasonDescription = null;
    private final CharSequence audioInputMode = null;
    private final Boolean automaticAudioInputSensitivityEnabled = null;
    private final Float audioInputSensitivity = null;
    private final Boolean echoCancellationEnabled = null;
    private final Boolean noiseSuppressionEnabled = null;
    private final Boolean noiseCancellationEnabled = null;
    private final Boolean automaticGainControlEnabled = null;
    private final CharSequence audioOutputMode = null;
    private final Float voiceOutputVolume = null;
    private final Boolean videoHardwareScalingEnabled = null;
    private final CharSequence audioMode = null;
    private final CharSequence inputDeviceName = null;
    private final CharSequence outputDeviceName = null;
    private final CharSequence videoDeviceName = null;
    private final transient String analyticsSchemaTypeName = "call_report_problem";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackCallReportProblem)) {
            return false;
        }
        TrackCallReportProblem trackCallReportProblem = (TrackCallReportProblem) obj;
        return m.areEqual(this.channelId, trackCallReportProblem.channelId) && m.areEqual(this.channelType, trackCallReportProblem.channelType) && m.areEqual(this.guildId, trackCallReportProblem.guildId) && m.areEqual(this.rtcConnectionId, trackCallReportProblem.rtcConnectionId) && m.areEqual(this.duration, trackCallReportProblem.duration) && m.areEqual(this.voiceStateCount, trackCallReportProblem.voiceStateCount) && m.areEqual(this.videoStreamCount, trackCallReportProblem.videoStreamCount) && m.areEqual(this.videoEnabled, trackCallReportProblem.videoEnabled) && m.areEqual(this.mediaSessionId, trackCallReportProblem.mediaSessionId) && m.areEqual(this.rating, trackCallReportProblem.rating) && m.areEqual(this.feedback, trackCallReportProblem.feedback) && m.areEqual(this.reasonCode, trackCallReportProblem.reasonCode) && m.areEqual(this.reasonDescription, trackCallReportProblem.reasonDescription) && m.areEqual(this.audioInputMode, trackCallReportProblem.audioInputMode) && m.areEqual(this.automaticAudioInputSensitivityEnabled, trackCallReportProblem.automaticAudioInputSensitivityEnabled) && m.areEqual(this.audioInputSensitivity, trackCallReportProblem.audioInputSensitivity) && m.areEqual(this.echoCancellationEnabled, trackCallReportProblem.echoCancellationEnabled) && m.areEqual(this.noiseSuppressionEnabled, trackCallReportProblem.noiseSuppressionEnabled) && m.areEqual(this.noiseCancellationEnabled, trackCallReportProblem.noiseCancellationEnabled) && m.areEqual(this.automaticGainControlEnabled, trackCallReportProblem.automaticGainControlEnabled) && m.areEqual(this.audioOutputMode, trackCallReportProblem.audioOutputMode) && m.areEqual(this.voiceOutputVolume, trackCallReportProblem.voiceOutputVolume) && m.areEqual(this.videoHardwareScalingEnabled, trackCallReportProblem.videoHardwareScalingEnabled) && m.areEqual(this.audioMode, trackCallReportProblem.audioMode) && m.areEqual(this.inputDeviceName, trackCallReportProblem.inputDeviceName) && m.areEqual(this.outputDeviceName, trackCallReportProblem.outputDeviceName) && m.areEqual(this.videoDeviceName, trackCallReportProblem.videoDeviceName);
    }

    public int hashCode() {
        Long l = this.channelId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.channelType;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.guildId;
        int hashCode3 = (hashCode2 + (l3 != null ? l3.hashCode() : 0)) * 31;
        CharSequence charSequence = this.rtcConnectionId;
        int hashCode4 = (hashCode3 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Long l4 = this.duration;
        int hashCode5 = (hashCode4 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.voiceStateCount;
        int hashCode6 = (hashCode5 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.videoStreamCount;
        int hashCode7 = (hashCode6 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Boolean bool = this.videoEnabled;
        int hashCode8 = (hashCode7 + (bool != null ? bool.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.mediaSessionId;
        int hashCode9 = (hashCode8 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.rating;
        int hashCode10 = (hashCode9 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.feedback;
        int hashCode11 = (hashCode10 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        Long l7 = this.reasonCode;
        int hashCode12 = (hashCode11 + (l7 != null ? l7.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.reasonDescription;
        int hashCode13 = (hashCode12 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        CharSequence charSequence6 = this.audioInputMode;
        int hashCode14 = (hashCode13 + (charSequence6 != null ? charSequence6.hashCode() : 0)) * 31;
        Boolean bool2 = this.automaticAudioInputSensitivityEnabled;
        int hashCode15 = (hashCode14 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Float f = this.audioInputSensitivity;
        int hashCode16 = (hashCode15 + (f != null ? f.hashCode() : 0)) * 31;
        Boolean bool3 = this.echoCancellationEnabled;
        int hashCode17 = (hashCode16 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        Boolean bool4 = this.noiseSuppressionEnabled;
        int hashCode18 = (hashCode17 + (bool4 != null ? bool4.hashCode() : 0)) * 31;
        Boolean bool5 = this.noiseCancellationEnabled;
        int hashCode19 = (hashCode18 + (bool5 != null ? bool5.hashCode() : 0)) * 31;
        Boolean bool6 = this.automaticGainControlEnabled;
        int hashCode20 = (hashCode19 + (bool6 != null ? bool6.hashCode() : 0)) * 31;
        CharSequence charSequence7 = this.audioOutputMode;
        int hashCode21 = (hashCode20 + (charSequence7 != null ? charSequence7.hashCode() : 0)) * 31;
        Float f2 = this.voiceOutputVolume;
        int hashCode22 = (hashCode21 + (f2 != null ? f2.hashCode() : 0)) * 31;
        Boolean bool7 = this.videoHardwareScalingEnabled;
        int hashCode23 = (hashCode22 + (bool7 != null ? bool7.hashCode() : 0)) * 31;
        CharSequence charSequence8 = this.audioMode;
        int hashCode24 = (hashCode23 + (charSequence8 != null ? charSequence8.hashCode() : 0)) * 31;
        CharSequence charSequence9 = this.inputDeviceName;
        int hashCode25 = (hashCode24 + (charSequence9 != null ? charSequence9.hashCode() : 0)) * 31;
        CharSequence charSequence10 = this.outputDeviceName;
        int hashCode26 = (hashCode25 + (charSequence10 != null ? charSequence10.hashCode() : 0)) * 31;
        CharSequence charSequence11 = this.videoDeviceName;
        if (charSequence11 != null) {
            i = charSequence11.hashCode();
        }
        return hashCode26 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackCallReportProblem(channelId=");
        R.append(this.channelId);
        R.append(", channelType=");
        R.append(this.channelType);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", rtcConnectionId=");
        R.append(this.rtcConnectionId);
        R.append(", duration=");
        R.append(this.duration);
        R.append(", voiceStateCount=");
        R.append(this.voiceStateCount);
        R.append(", videoStreamCount=");
        R.append(this.videoStreamCount);
        R.append(", videoEnabled=");
        R.append(this.videoEnabled);
        R.append(", mediaSessionId=");
        R.append(this.mediaSessionId);
        R.append(", rating=");
        R.append(this.rating);
        R.append(", feedback=");
        R.append(this.feedback);
        R.append(", reasonCode=");
        R.append(this.reasonCode);
        R.append(", reasonDescription=");
        R.append(this.reasonDescription);
        R.append(", audioInputMode=");
        R.append(this.audioInputMode);
        R.append(", automaticAudioInputSensitivityEnabled=");
        R.append(this.automaticAudioInputSensitivityEnabled);
        R.append(", audioInputSensitivity=");
        R.append(this.audioInputSensitivity);
        R.append(", echoCancellationEnabled=");
        R.append(this.echoCancellationEnabled);
        R.append(", noiseSuppressionEnabled=");
        R.append(this.noiseSuppressionEnabled);
        R.append(", noiseCancellationEnabled=");
        R.append(this.noiseCancellationEnabled);
        R.append(", automaticGainControlEnabled=");
        R.append(this.automaticGainControlEnabled);
        R.append(", audioOutputMode=");
        R.append(this.audioOutputMode);
        R.append(", voiceOutputVolume=");
        R.append(this.voiceOutputVolume);
        R.append(", videoHardwareScalingEnabled=");
        R.append(this.videoHardwareScalingEnabled);
        R.append(", audioMode=");
        R.append(this.audioMode);
        R.append(", inputDeviceName=");
        R.append(this.inputDeviceName);
        R.append(", outputDeviceName=");
        R.append(this.outputDeviceName);
        R.append(", videoDeviceName=");
        return a.D(R, this.videoDeviceName, ")");
    }
}
