package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackExternalPaymentSucceeded.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\r\n\u0002\b\u0018\n\u0002\u0018\u0002\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001c\u0010\u000e\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0005R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0013\u001a\u0004\b\u0017\u0010\u0015R!\u0010\u0019\u001a\n\u0018\u00010\u0011j\u0004\u0018\u0001`\u00188\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u0013\u001a\u0004\b\u001a\u0010\u0015R\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u0013\u001a\u0004\b\u001c\u0010\u0015R\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u0013\u001a\u0004\b\u001e\u0010\u0015R\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u0013\u001a\u0004\b \u0010\u0015R\u001b\u0010!\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u0013\u001a\u0004\b\"\u0010\u0015R\u001b\u0010$\u001a\u0004\u0018\u00010#8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'R\u001b\u0010(\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010\u0013\u001a\u0004\b)\u0010\u0015R\u001b\u0010*\u001a\u0004\u0018\u00010#8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010%\u001a\u0004\b+\u0010'R\u001b\u0010,\u001a\u0004\u0018\u00010#8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010%\u001a\u0004\b-\u0010'R\u001b\u0010.\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010\u0013\u001a\u0004\b/\u0010\u0015R!\u00100\u001a\n\u0018\u00010\u0011j\u0004\u0018\u0001`\u00188\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010\u0013\u001a\u0004\b1\u0010\u0015R\u001b\u00102\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010\u0013\u001a\u0004\b3\u0010\u0015R\u001b\u00104\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010\u0013\u001a\u0004\b5\u0010\u0015R!\u00106\u001a\n\u0018\u00010\u0011j\u0004\u0018\u0001`\u00188\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010\u0013\u001a\u0004\b7\u0010\u0015R\u001b\u00108\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010\u0013\u001a\u0004\b9\u0010\u0015R\u001b\u0010:\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010\u0013\u001a\u0004\b;\u0010\u0015R$\u0010=\u001a\u0004\u0018\u00010<8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b=\u0010>\u001a\u0004\b?\u0010@\"\u0004\bA\u0010BR\u001b\u0010C\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\bC\u0010\u0013\u001a\u0004\bD\u0010\u0015R\u001b\u0010E\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\bE\u0010\u0013\u001a\u0004\bF\u0010\u0015¨\u0006G"}, d2 = {"Lcom/discord/analytics/generated/events/TrackExternalPaymentSucceeded;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "", "amount", "Ljava/lang/Long;", "getAmount", "()Ljava/lang/Long;", "subscriptionPremiumGuildPlanId", "getSubscriptionPremiumGuildPlanId", "Lcom/discord/primitives/Timestamp;", "createdAt", "getCreatedAt", "skuId", "getSkuId", "skuSubscriptionPlanId", "getSkuSubscriptionPlanId", "subscriptionPremiumPlanId", "getSubscriptionPremiumPlanId", "subscriptionPremiumGuildQuantity", "getSubscriptionPremiumGuildQuantity", "", "currency", "Ljava/lang/CharSequence;", "getCurrency", "()Ljava/lang/CharSequence;", "skuType", "getSkuType", "paymentType", "getPaymentType", "subscriptionPaymentGatewayPlanId", "getSubscriptionPaymentGatewayPlanId", "paymentId", "getPaymentId", "subscriptionCurrentPeriodEnd", "getSubscriptionCurrentPeriodEnd", "subscriptionType", "getSubscriptionType", "paymentGateway", "getPaymentGateway", "subscriptionCurrentPeriodStart", "getSubscriptionCurrentPeriodStart", "subscriptionId", "getSubscriptionId", "subscriptionPlanId", "getSubscriptionPlanId", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "price", "getPrice", "amountRefunded", "getAmountRefunded", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackExternalPaymentSucceeded implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Long amount = null;
    private final Long createdAt = null;
    private final CharSequence currency = null;
    private final Long amountRefunded = null;
    private final Long paymentGateway = null;
    private final Long paymentId = null;
    private final CharSequence paymentType = null;
    private final Long price = null;
    private final Long skuId = null;
    private final Long skuSubscriptionPlanId = null;
    private final Long skuType = null;
    private final Long subscriptionCurrentPeriodEnd = null;
    private final Long subscriptionCurrentPeriodStart = null;
    private final Long subscriptionId = null;
    private final CharSequence subscriptionPaymentGatewayPlanId = null;
    private final Long subscriptionPlanId = null;
    private final Long subscriptionPremiumGuildPlanId = null;
    private final Long subscriptionPremiumGuildQuantity = null;
    private final Long subscriptionPremiumPlanId = null;
    private final Long subscriptionType = null;
    private final transient String analyticsSchemaTypeName = "external_payment_succeeded";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackExternalPaymentSucceeded)) {
            return false;
        }
        TrackExternalPaymentSucceeded trackExternalPaymentSucceeded = (TrackExternalPaymentSucceeded) obj;
        return m.areEqual(this.amount, trackExternalPaymentSucceeded.amount) && m.areEqual(this.createdAt, trackExternalPaymentSucceeded.createdAt) && m.areEqual(this.currency, trackExternalPaymentSucceeded.currency) && m.areEqual(this.amountRefunded, trackExternalPaymentSucceeded.amountRefunded) && m.areEqual(this.paymentGateway, trackExternalPaymentSucceeded.paymentGateway) && m.areEqual(this.paymentId, trackExternalPaymentSucceeded.paymentId) && m.areEqual(this.paymentType, trackExternalPaymentSucceeded.paymentType) && m.areEqual(this.price, trackExternalPaymentSucceeded.price) && m.areEqual(this.skuId, trackExternalPaymentSucceeded.skuId) && m.areEqual(this.skuSubscriptionPlanId, trackExternalPaymentSucceeded.skuSubscriptionPlanId) && m.areEqual(this.skuType, trackExternalPaymentSucceeded.skuType) && m.areEqual(this.subscriptionCurrentPeriodEnd, trackExternalPaymentSucceeded.subscriptionCurrentPeriodEnd) && m.areEqual(this.subscriptionCurrentPeriodStart, trackExternalPaymentSucceeded.subscriptionCurrentPeriodStart) && m.areEqual(this.subscriptionId, trackExternalPaymentSucceeded.subscriptionId) && m.areEqual(this.subscriptionPaymentGatewayPlanId, trackExternalPaymentSucceeded.subscriptionPaymentGatewayPlanId) && m.areEqual(this.subscriptionPlanId, trackExternalPaymentSucceeded.subscriptionPlanId) && m.areEqual(this.subscriptionPremiumGuildPlanId, trackExternalPaymentSucceeded.subscriptionPremiumGuildPlanId) && m.areEqual(this.subscriptionPremiumGuildQuantity, trackExternalPaymentSucceeded.subscriptionPremiumGuildQuantity) && m.areEqual(this.subscriptionPremiumPlanId, trackExternalPaymentSucceeded.subscriptionPremiumPlanId) && m.areEqual(this.subscriptionType, trackExternalPaymentSucceeded.subscriptionType);
    }

    public int hashCode() {
        Long l = this.amount;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.createdAt;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        CharSequence charSequence = this.currency;
        int hashCode3 = (hashCode2 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Long l3 = this.amountRefunded;
        int hashCode4 = (hashCode3 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.paymentGateway;
        int hashCode5 = (hashCode4 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.paymentId;
        int hashCode6 = (hashCode5 + (l5 != null ? l5.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.paymentType;
        int hashCode7 = (hashCode6 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l6 = this.price;
        int hashCode8 = (hashCode7 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.skuId;
        int hashCode9 = (hashCode8 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Long l8 = this.skuSubscriptionPlanId;
        int hashCode10 = (hashCode9 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.skuType;
        int hashCode11 = (hashCode10 + (l9 != null ? l9.hashCode() : 0)) * 31;
        Long l10 = this.subscriptionCurrentPeriodEnd;
        int hashCode12 = (hashCode11 + (l10 != null ? l10.hashCode() : 0)) * 31;
        Long l11 = this.subscriptionCurrentPeriodStart;
        int hashCode13 = (hashCode12 + (l11 != null ? l11.hashCode() : 0)) * 31;
        Long l12 = this.subscriptionId;
        int hashCode14 = (hashCode13 + (l12 != null ? l12.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.subscriptionPaymentGatewayPlanId;
        int hashCode15 = (hashCode14 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Long l13 = this.subscriptionPlanId;
        int hashCode16 = (hashCode15 + (l13 != null ? l13.hashCode() : 0)) * 31;
        Long l14 = this.subscriptionPremiumGuildPlanId;
        int hashCode17 = (hashCode16 + (l14 != null ? l14.hashCode() : 0)) * 31;
        Long l15 = this.subscriptionPremiumGuildQuantity;
        int hashCode18 = (hashCode17 + (l15 != null ? l15.hashCode() : 0)) * 31;
        Long l16 = this.subscriptionPremiumPlanId;
        int hashCode19 = (hashCode18 + (l16 != null ? l16.hashCode() : 0)) * 31;
        Long l17 = this.subscriptionType;
        if (l17 != null) {
            i = l17.hashCode();
        }
        return hashCode19 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackExternalPaymentSucceeded(amount=");
        R.append(this.amount);
        R.append(", createdAt=");
        R.append(this.createdAt);
        R.append(", currency=");
        R.append(this.currency);
        R.append(", amountRefunded=");
        R.append(this.amountRefunded);
        R.append(", paymentGateway=");
        R.append(this.paymentGateway);
        R.append(", paymentId=");
        R.append(this.paymentId);
        R.append(", paymentType=");
        R.append(this.paymentType);
        R.append(", price=");
        R.append(this.price);
        R.append(", skuId=");
        R.append(this.skuId);
        R.append(", skuSubscriptionPlanId=");
        R.append(this.skuSubscriptionPlanId);
        R.append(", skuType=");
        R.append(this.skuType);
        R.append(", subscriptionCurrentPeriodEnd=");
        R.append(this.subscriptionCurrentPeriodEnd);
        R.append(", subscriptionCurrentPeriodStart=");
        R.append(this.subscriptionCurrentPeriodStart);
        R.append(", subscriptionId=");
        R.append(this.subscriptionId);
        R.append(", subscriptionPaymentGatewayPlanId=");
        R.append(this.subscriptionPaymentGatewayPlanId);
        R.append(", subscriptionPlanId=");
        R.append(this.subscriptionPlanId);
        R.append(", subscriptionPremiumGuildPlanId=");
        R.append(this.subscriptionPremiumGuildPlanId);
        R.append(", subscriptionPremiumGuildQuantity=");
        R.append(this.subscriptionPremiumGuildQuantity);
        R.append(", subscriptionPremiumPlanId=");
        R.append(this.subscriptionPremiumPlanId);
        R.append(", subscriptionType=");
        return a.F(R, this.subscriptionType, ")");
    }
}
