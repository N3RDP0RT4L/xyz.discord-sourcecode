package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: TrackSmiteActionClassification.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0007\n\u0002\u0010 \n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u0011\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0013\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005R#\u0010\u0017\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u000e\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001fR#\u0010 \u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u000e\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0018\u001a\u0004\b!\u0010\u001aR\u001b\u0010\"\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u001d\u001a\u0004\b#\u0010\u001fR\u001b\u0010$\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u0010\u001a\u0004\b%\u0010\u0012R\u001b\u0010&\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010\u001d\u001a\u0004\b'\u0010\u001fR\u001b\u0010(\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010\u001d\u001a\u0004\b)\u0010\u001fR\u001b\u0010*\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010\u001d\u001a\u0004\b+\u0010\u001fR$\u0010-\u001a\u0004\u0018\u00010,8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b-\u0010.\u001a\u0004\b/\u00100\"\u0004\b1\u00102R#\u00103\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u000e\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010\u0018\u001a\u0004\b4\u0010\u001aR\u001b\u00105\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010\u0010\u001a\u0004\b6\u0010\u0012R\u001b\u00107\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010\u0010\u001a\u0004\b8\u0010\u0012R#\u00109\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u000e\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010\u0018\u001a\u0004\b:\u0010\u001aR\u001b\u0010;\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010\u001d\u001a\u0004\b<\u0010\u001f¨\u0006="}, d2 = {"Lcom/discord/analytics/generated/events/TrackSmiteActionClassification;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "actionId", "Ljava/lang/CharSequence;", "getActionId", "()Ljava/lang/CharSequence;", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "", "labelReasons", "Ljava/util/List;", "getLabelReasons", "()Ljava/util/List;", "", "locationChannelId", "Ljava/lang/Long;", "getLocationChannelId", "()Ljava/lang/Long;", "labelStatuses", "getLabelStatuses", "guildId", "getGuildId", "actionName", "getActionName", "channelId", "getChannelId", "locationGuildId", "getLocationGuildId", "locationChannelType", "getLocationChannelType", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "labelReasonsShas", "getLabelReasonsShas", "rulesGitSha", "getRulesGitSha", "rulesSourceSha", "getRulesSourceSha", "labels", "getLabels", "locationMessageId", "getLocationMessageId", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackSmiteActionClassification implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final CharSequence actionName = null;
    private final CharSequence actionId = null;
    private final CharSequence rulesGitSha = null;
    private final CharSequence rulesSourceSha = null;
    private final List<CharSequence> labels = null;
    private final List<CharSequence> labelStatuses = null;
    private final List<CharSequence> labelReasons = null;
    private final List<CharSequence> labelReasonsShas = null;
    private final Long guildId = null;
    private final Long channelId = null;
    private final Long locationGuildId = null;
    private final Long locationChannelId = null;
    private final Long locationChannelType = null;
    private final Long locationMessageId = null;
    private final transient String analyticsSchemaTypeName = "smite_action_classification";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackSmiteActionClassification)) {
            return false;
        }
        TrackSmiteActionClassification trackSmiteActionClassification = (TrackSmiteActionClassification) obj;
        return m.areEqual(this.actionName, trackSmiteActionClassification.actionName) && m.areEqual(this.actionId, trackSmiteActionClassification.actionId) && m.areEqual(this.rulesGitSha, trackSmiteActionClassification.rulesGitSha) && m.areEqual(this.rulesSourceSha, trackSmiteActionClassification.rulesSourceSha) && m.areEqual(this.labels, trackSmiteActionClassification.labels) && m.areEqual(this.labelStatuses, trackSmiteActionClassification.labelStatuses) && m.areEqual(this.labelReasons, trackSmiteActionClassification.labelReasons) && m.areEqual(this.labelReasonsShas, trackSmiteActionClassification.labelReasonsShas) && m.areEqual(this.guildId, trackSmiteActionClassification.guildId) && m.areEqual(this.channelId, trackSmiteActionClassification.channelId) && m.areEqual(this.locationGuildId, trackSmiteActionClassification.locationGuildId) && m.areEqual(this.locationChannelId, trackSmiteActionClassification.locationChannelId) && m.areEqual(this.locationChannelType, trackSmiteActionClassification.locationChannelType) && m.areEqual(this.locationMessageId, trackSmiteActionClassification.locationMessageId);
    }

    public int hashCode() {
        CharSequence charSequence = this.actionName;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        CharSequence charSequence2 = this.actionId;
        int hashCode2 = (hashCode + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.rulesGitSha;
        int hashCode3 = (hashCode2 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.rulesSourceSha;
        int hashCode4 = (hashCode3 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        List<CharSequence> list = this.labels;
        int hashCode5 = (hashCode4 + (list != null ? list.hashCode() : 0)) * 31;
        List<CharSequence> list2 = this.labelStatuses;
        int hashCode6 = (hashCode5 + (list2 != null ? list2.hashCode() : 0)) * 31;
        List<CharSequence> list3 = this.labelReasons;
        int hashCode7 = (hashCode6 + (list3 != null ? list3.hashCode() : 0)) * 31;
        List<CharSequence> list4 = this.labelReasonsShas;
        int hashCode8 = (hashCode7 + (list4 != null ? list4.hashCode() : 0)) * 31;
        Long l = this.guildId;
        int hashCode9 = (hashCode8 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.channelId;
        int hashCode10 = (hashCode9 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.locationGuildId;
        int hashCode11 = (hashCode10 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.locationChannelId;
        int hashCode12 = (hashCode11 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.locationChannelType;
        int hashCode13 = (hashCode12 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.locationMessageId;
        if (l6 != null) {
            i = l6.hashCode();
        }
        return hashCode13 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackSmiteActionClassification(actionName=");
        R.append(this.actionName);
        R.append(", actionId=");
        R.append(this.actionId);
        R.append(", rulesGitSha=");
        R.append(this.rulesGitSha);
        R.append(", rulesSourceSha=");
        R.append(this.rulesSourceSha);
        R.append(", labels=");
        R.append(this.labels);
        R.append(", labelStatuses=");
        R.append(this.labelStatuses);
        R.append(", labelReasons=");
        R.append(this.labelReasons);
        R.append(", labelReasonsShas=");
        R.append(this.labelReasonsShas);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", channelId=");
        R.append(this.channelId);
        R.append(", locationGuildId=");
        R.append(this.locationGuildId);
        R.append(", locationChannelId=");
        R.append(this.locationChannelId);
        R.append(", locationChannelType=");
        R.append(this.locationChannelType);
        R.append(", locationMessageId=");
        return a.F(R, this.locationMessageId, ")");
    }
}
