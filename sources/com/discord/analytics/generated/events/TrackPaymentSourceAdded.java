package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackPaymentSourceAdded.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\t\n\u0002\u0010\r\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0010\u001a\u0004\b\u0014\u0010\u0012R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0015\u0010\u0017R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u00188\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001cR\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u00188\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u001a\u001a\u0004\b\u001e\u0010\u001cR\u001c\u0010\u001f\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\u0005R!\u0010#\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`\"8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u0010\u001a\u0004\b$\u0010\u0012R$\u0010&\u001a\u0004\u0018\u00010%8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b&\u0010'\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R\u001b\u0010,\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010\u0010\u001a\u0004\b-\u0010\u0012¨\u0006."}, d2 = {"Lcom/discord/analytics/generated/events/TrackPaymentSourceAdded;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "paymentSourceId", "Ljava/lang/Long;", "getPaymentSourceId", "()Ljava/lang/Long;", "paymentGateway", "getPaymentGateway", "isDefault", "Ljava/lang/Boolean;", "()Ljava/lang/Boolean;", "", "paymentSourceCountry", "Ljava/lang/CharSequence;", "getPaymentSourceCountry", "()Ljava/lang/CharSequence;", "cardBrand", "getCardBrand", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "Lcom/discord/primitives/Timestamp;", "cardExpirationDate", "getCardExpirationDate", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "paymentSourceType", "getPaymentSourceType", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackPaymentSourceAdded implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Long paymentSourceId = null;
    private final Long paymentSourceType = null;
    private final Long paymentGateway = null;
    private final CharSequence cardBrand = null;
    private final Long cardExpirationDate = null;
    private final CharSequence paymentSourceCountry = null;
    private final Boolean isDefault = null;
    private final transient String analyticsSchemaTypeName = "payment_source_added";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackPaymentSourceAdded)) {
            return false;
        }
        TrackPaymentSourceAdded trackPaymentSourceAdded = (TrackPaymentSourceAdded) obj;
        return m.areEqual(this.paymentSourceId, trackPaymentSourceAdded.paymentSourceId) && m.areEqual(this.paymentSourceType, trackPaymentSourceAdded.paymentSourceType) && m.areEqual(this.paymentGateway, trackPaymentSourceAdded.paymentGateway) && m.areEqual(this.cardBrand, trackPaymentSourceAdded.cardBrand) && m.areEqual(this.cardExpirationDate, trackPaymentSourceAdded.cardExpirationDate) && m.areEqual(this.paymentSourceCountry, trackPaymentSourceAdded.paymentSourceCountry) && m.areEqual(this.isDefault, trackPaymentSourceAdded.isDefault);
    }

    public int hashCode() {
        Long l = this.paymentSourceId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.paymentSourceType;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.paymentGateway;
        int hashCode3 = (hashCode2 + (l3 != null ? l3.hashCode() : 0)) * 31;
        CharSequence charSequence = this.cardBrand;
        int hashCode4 = (hashCode3 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Long l4 = this.cardExpirationDate;
        int hashCode5 = (hashCode4 + (l4 != null ? l4.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.paymentSourceCountry;
        int hashCode6 = (hashCode5 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Boolean bool = this.isDefault;
        if (bool != null) {
            i = bool.hashCode();
        }
        return hashCode6 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackPaymentSourceAdded(paymentSourceId=");
        R.append(this.paymentSourceId);
        R.append(", paymentSourceType=");
        R.append(this.paymentSourceType);
        R.append(", paymentGateway=");
        R.append(this.paymentGateway);
        R.append(", cardBrand=");
        R.append(this.cardBrand);
        R.append(", cardExpirationDate=");
        R.append(this.cardExpirationDate);
        R.append(", paymentSourceCountry=");
        R.append(this.paymentSourceCountry);
        R.append(", isDefault=");
        return a.C(R, this.isDefault, ")");
    }
}
