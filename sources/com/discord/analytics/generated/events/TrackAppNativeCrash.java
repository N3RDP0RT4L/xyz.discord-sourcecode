package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackOverlayClientMetadata;
import com.discord.analytics.generated.traits.TrackOverlayClientMetadataReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackAppNativeCrash.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b \b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\u0010\u0010\u0005\u001a\u00020\u0004HÖ\u0001¢\u0006\u0004\b\u0005\u0010\u0006J\u0010\u0010\b\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\r\u001a\u00020\f2\b\u0010\u000b\u001a\u0004\u0018\u00010\nHÖ\u0003¢\u0006\u0004\b\r\u0010\u000eR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R$\u0010\u0015\u001a\u0004\u0018\u00010\u00148\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR$\u0010\u001c\u001a\u0004\u0018\u00010\u001b8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001f\"\u0004\b \u0010!R\u001b\u0010#\u001a\u0004\u0018\u00010\"8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010$\u001a\u0004\b%\u0010&R\u001b\u0010'\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010\u0011\u001a\u0004\b(\u0010\u0013R\u001b\u0010)\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010\u0011\u001a\u0004\b*\u0010\u0013R\u001b\u0010+\u001a\u0004\u0018\u00010\"8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010$\u001a\u0004\b,\u0010&R\u001b\u0010-\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u0011\u001a\u0004\b.\u0010\u0013R\u001b\u0010/\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u0011\u001a\u0004\b0\u0010\u0013R\u001b\u00101\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b1\u00102\u001a\u0004\b3\u00104R\u001b\u00105\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010\u0011\u001a\u0004\b6\u0010\u0013R\u001b\u00107\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010\u0011\u001a\u0004\b8\u0010\u0013R\u001b\u00109\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010\u0011\u001a\u0004\b:\u0010\u0013R\u001b\u0010;\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010\u0011\u001a\u0004\b<\u0010\u0013R\u001c\u0010=\u001a\u00020\u00048\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b=\u0010>\u001a\u0004\b?\u0010\u0006R\u001b\u0010@\u001a\u0004\u0018\u00010\"8\u0006@\u0006¢\u0006\f\n\u0004\b@\u0010$\u001a\u0004\bA\u0010&¨\u0006B"}, d2 = {"Lcom/discord/analytics/generated/events/TrackAppNativeCrash;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackOverlayClientMetadataReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "terminationReason", "Ljava/lang/CharSequence;", "getTerminationReason", "()Ljava/lang/CharSequence;", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "Lcom/discord/analytics/generated/traits/TrackOverlayClientMetadata;", "trackOverlayClientMetadata", "Lcom/discord/analytics/generated/traits/TrackOverlayClientMetadata;", "getTrackOverlayClientMetadata", "()Lcom/discord/analytics/generated/traits/TrackOverlayClientMetadata;", "setTrackOverlayClientMetadata", "(Lcom/discord/analytics/generated/traits/TrackOverlayClientMetadata;)V", "", "exceptionCode", "Ljava/lang/Long;", "getExceptionCode", "()Ljava/lang/Long;", "binaryName", "getBinaryName", "rendererCrashReason", "getRendererCrashReason", "exceptionType", "getExceptionType", "tombstoneCause", "getTombstoneCause", "tombstoneHash", "getTombstoneHash", "didCrash", "Ljava/lang/Boolean;", "getDidCrash", "()Ljava/lang/Boolean;", "exitReason", "getExitReason", "exitDescription", "getExitDescription", "minidumpExceptionType", "getMinidumpExceptionType", "callstackHash", "getCallstackHash", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "signal", "getSignal", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackAppNativeCrash implements AnalyticsSchema, TrackBaseReceiver, TrackOverlayClientMetadataReceiver {
    private TrackBase trackBase;
    private TrackOverlayClientMetadata trackOverlayClientMetadata;
    private final Boolean didCrash = null;
    private final CharSequence rendererCrashReason = null;
    private final CharSequence minidumpExceptionType = null;
    private final CharSequence exitReason = null;
    private final CharSequence exitDescription = null;
    private final CharSequence tombstoneHash = null;
    private final CharSequence tombstoneCause = null;
    private final Long signal = null;
    private final Long exceptionType = null;
    private final Long exceptionCode = null;
    private final CharSequence terminationReason = null;
    private final CharSequence binaryName = null;
    private final CharSequence callstackHash = null;
    private final transient String analyticsSchemaTypeName = "app_native_crash";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackAppNativeCrash)) {
            return false;
        }
        TrackAppNativeCrash trackAppNativeCrash = (TrackAppNativeCrash) obj;
        return m.areEqual(this.didCrash, trackAppNativeCrash.didCrash) && m.areEqual(this.rendererCrashReason, trackAppNativeCrash.rendererCrashReason) && m.areEqual(this.minidumpExceptionType, trackAppNativeCrash.minidumpExceptionType) && m.areEqual(this.exitReason, trackAppNativeCrash.exitReason) && m.areEqual(this.exitDescription, trackAppNativeCrash.exitDescription) && m.areEqual(this.tombstoneHash, trackAppNativeCrash.tombstoneHash) && m.areEqual(this.tombstoneCause, trackAppNativeCrash.tombstoneCause) && m.areEqual(this.signal, trackAppNativeCrash.signal) && m.areEqual(this.exceptionType, trackAppNativeCrash.exceptionType) && m.areEqual(this.exceptionCode, trackAppNativeCrash.exceptionCode) && m.areEqual(this.terminationReason, trackAppNativeCrash.terminationReason) && m.areEqual(this.binaryName, trackAppNativeCrash.binaryName) && m.areEqual(this.callstackHash, trackAppNativeCrash.callstackHash);
    }

    public int hashCode() {
        Boolean bool = this.didCrash;
        int i = 0;
        int hashCode = (bool != null ? bool.hashCode() : 0) * 31;
        CharSequence charSequence = this.rendererCrashReason;
        int hashCode2 = (hashCode + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.minidumpExceptionType;
        int hashCode3 = (hashCode2 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.exitReason;
        int hashCode4 = (hashCode3 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.exitDescription;
        int hashCode5 = (hashCode4 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.tombstoneHash;
        int hashCode6 = (hashCode5 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        CharSequence charSequence6 = this.tombstoneCause;
        int hashCode7 = (hashCode6 + (charSequence6 != null ? charSequence6.hashCode() : 0)) * 31;
        Long l = this.signal;
        int hashCode8 = (hashCode7 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.exceptionType;
        int hashCode9 = (hashCode8 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.exceptionCode;
        int hashCode10 = (hashCode9 + (l3 != null ? l3.hashCode() : 0)) * 31;
        CharSequence charSequence7 = this.terminationReason;
        int hashCode11 = (hashCode10 + (charSequence7 != null ? charSequence7.hashCode() : 0)) * 31;
        CharSequence charSequence8 = this.binaryName;
        int hashCode12 = (hashCode11 + (charSequence8 != null ? charSequence8.hashCode() : 0)) * 31;
        CharSequence charSequence9 = this.callstackHash;
        if (charSequence9 != null) {
            i = charSequence9.hashCode();
        }
        return hashCode12 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackAppNativeCrash(didCrash=");
        R.append(this.didCrash);
        R.append(", rendererCrashReason=");
        R.append(this.rendererCrashReason);
        R.append(", minidumpExceptionType=");
        R.append(this.minidumpExceptionType);
        R.append(", exitReason=");
        R.append(this.exitReason);
        R.append(", exitDescription=");
        R.append(this.exitDescription);
        R.append(", tombstoneHash=");
        R.append(this.tombstoneHash);
        R.append(", tombstoneCause=");
        R.append(this.tombstoneCause);
        R.append(", signal=");
        R.append(this.signal);
        R.append(", exceptionType=");
        R.append(this.exceptionType);
        R.append(", exceptionCode=");
        R.append(this.exceptionCode);
        R.append(", terminationReason=");
        R.append(this.terminationReason);
        R.append(", binaryName=");
        R.append(this.binaryName);
        R.append(", callstackHash=");
        return a.D(R, this.callstackHash, ")");
    }
}
