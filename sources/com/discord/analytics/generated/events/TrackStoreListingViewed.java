package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackLocationMetadata;
import com.discord.analytics.generated.traits.TrackLocationMetadataReceiver;
import com.discord.analytics.generated.traits.TrackPaymentMetadata;
import com.discord.analytics.generated.traits.TrackPaymentMetadataReceiver;
import com.discord.analytics.generated.traits.TrackSourceMetadata;
import com.discord.analytics.generated.traits.TrackSourceMetadataReceiver;
import com.discord.analytics.generated.traits.TrackStoreSkuMetadata;
import com.discord.analytics.generated.traits.TrackStoreSkuMetadataReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackStoreListingViewed.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0017\n\u0002\u0010\r\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u00052\u00020\u0006J\u0010\u0010\b\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u001a\u0010\u0010\u001a\u00020\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\rHÖ\u0003¢\u0006\u0004\b\u0010\u0010\u0011R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u0018\u001a\u0004\b\u001c\u0010\u001aR\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u0018\u001a\u0004\b\u001e\u0010\u001aR\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u0014\u001a\u0004\b \u0010\u0016R\u001b\u0010!\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u0018\u001a\u0004\b\"\u0010\u001aR\u001b\u0010#\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u0018\u001a\u0004\b$\u0010\u001aR\u001b\u0010%\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u0018\u001a\u0004\b&\u0010\u001aR\u001c\u0010'\u001a\u00020\u00078\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b)\u0010\tR\u001b\u0010+\u001a\u0004\u0018\u00010*8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010,\u001a\u0004\b-\u0010.R\u001b\u0010/\u001a\u0004\u0018\u00010*8\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010,\u001a\u0004\b0\u0010.R\u001b\u00101\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010\u0018\u001a\u0004\b2\u0010\u001aR\u001b\u00103\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010\u0018\u001a\u0004\b4\u0010\u001aR\u001b\u00105\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010\u0018\u001a\u0004\b6\u0010\u001aR$\u00108\u001a\u0004\u0018\u0001078\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b8\u00109\u001a\u0004\b:\u0010;\"\u0004\b<\u0010=R\u001b\u0010>\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b>\u0010\u0018\u001a\u0004\b?\u0010\u001aR\u001b\u0010@\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b@\u0010\u0018\u001a\u0004\bA\u0010\u001aR\u001b\u0010B\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010\u0018\u001a\u0004\bC\u0010\u001aR\u001b\u0010D\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bD\u0010\u0018\u001a\u0004\bE\u0010\u001aR\u001b\u0010F\u001a\u0004\u0018\u00010*8\u0006@\u0006¢\u0006\f\n\u0004\bF\u0010,\u001a\u0004\bG\u0010.R$\u0010I\u001a\u0004\u0018\u00010H8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\bI\u0010J\u001a\u0004\bK\u0010L\"\u0004\bM\u0010NR\u001b\u0010O\u001a\u0004\u0018\u00010*8\u0006@\u0006¢\u0006\f\n\u0004\bO\u0010,\u001a\u0004\bP\u0010.R\u001b\u0010Q\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\bQ\u0010\u0014\u001a\u0004\bR\u0010\u0016R$\u0010T\u001a\u0004\u0018\u00010S8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\bT\u0010U\u001a\u0004\bV\u0010W\"\u0004\bX\u0010YR$\u0010[\u001a\u0004\u0018\u00010Z8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b[\u0010\\\u001a\u0004\b]\u0010^\"\u0004\b_\u0010`R\u001b\u0010a\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\ba\u0010\u0018\u001a\u0004\bb\u0010\u001aR\u001b\u0010c\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bc\u0010\u0018\u001a\u0004\bd\u0010\u001aR\u001b\u0010e\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\be\u0010\u0018\u001a\u0004\bf\u0010\u001aR\u001b\u0010g\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bg\u0010\u0018\u001a\u0004\bh\u0010\u001aR$\u0010j\u001a\u0004\u0018\u00010i8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\bj\u0010k\u001a\u0004\bl\u0010m\"\u0004\bn\u0010oR\u001b\u0010p\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bp\u0010\u0018\u001a\u0004\bq\u0010\u001aR\u001b\u0010r\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\br\u0010\u0014\u001a\u0004\bs\u0010\u0016¨\u0006t"}, d2 = {"Lcom/discord/analytics/generated/events/TrackStoreListingViewed;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackLocationMetadataReceiver;", "Lcom/discord/analytics/generated/traits/TrackSourceMetadataReceiver;", "Lcom/discord/analytics/generated/traits/TrackStoreSkuMetadataReceiver;", "Lcom/discord/analytics/generated/traits/TrackPaymentMetadataReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "listIndex", "Ljava/lang/Long;", "getListIndex", "()Ljava/lang/Long;", "hasOnlineCoop", "Ljava/lang/Boolean;", "getHasOnlineCoop", "()Ljava/lang/Boolean;", "hasSpectatorMode", "getHasSpectatorMode", "hasSecureNetworking", "getHasSecureNetworking", "carouselImageCount", "getCarouselImageCount", "hasCrossPlatform", "getHasCrossPlatform", "hasGameInvites", "getHasGameInvites", "hasCoop", "getHasCoop", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "", "listSort", "Ljava/lang/CharSequence;", "getListSort", "()Ljava/lang/CharSequence;", "source", "getSource", "hasLocalMultiplayer", "getHasLocalMultiplayer", "hasNews", "getHasNews", "hasSinglePlayer", "getHasSinglePlayer", "Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "trackLocationMetadata", "Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "getTrackLocationMetadata", "()Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "setTrackLocationMetadata", "(Lcom/discord/analytics/generated/traits/TrackLocationMetadata;)V", "hasLocalCoop", "getHasLocalCoop", "hasOnlineMultiplayer", "getHasOnlineMultiplayer", "hasPvpFeatures", "getHasPvpFeatures", "hasCloudSaves", "getHasCloudSaves", "listFilterDistributionType", "getListFilterDistributionType", "Lcom/discord/analytics/generated/traits/TrackStoreSkuMetadata;", "trackStoreSkuMetadata", "Lcom/discord/analytics/generated/traits/TrackStoreSkuMetadata;", "getTrackStoreSkuMetadata", "()Lcom/discord/analytics/generated/traits/TrackStoreSkuMetadata;", "setTrackStoreSkuMetadata", "(Lcom/discord/analytics/generated/traits/TrackStoreSkuMetadata;)V", "loadId", "getLoadId", "carouselVideoCount", "getCarouselVideoCount", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "Lcom/discord/analytics/generated/traits/TrackSourceMetadata;", "trackSourceMetadata", "Lcom/discord/analytics/generated/traits/TrackSourceMetadata;", "getTrackSourceMetadata", "()Lcom/discord/analytics/generated/traits/TrackSourceMetadata;", "setTrackSourceMetadata", "(Lcom/discord/analytics/generated/traits/TrackSourceMetadata;)V", "hasStaffReview", "getHasStaffReview", "hasControllerSupport", "getHasControllerSupport", "hasRichPresence", "getHasRichPresence", "hasDescription", "getHasDescription", "Lcom/discord/analytics/generated/traits/TrackPaymentMetadata;", "trackPaymentMetadata", "Lcom/discord/analytics/generated/traits/TrackPaymentMetadata;", "getTrackPaymentMetadata", "()Lcom/discord/analytics/generated/traits/TrackPaymentMetadata;", "setTrackPaymentMetadata", "(Lcom/discord/analytics/generated/traits/TrackPaymentMetadata;)V", "listSearched", "getListSearched", "promotionId", "getPromotionId", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackStoreListingViewed implements AnalyticsSchema, TrackBaseReceiver, TrackLocationMetadataReceiver, TrackSourceMetadataReceiver, TrackStoreSkuMetadataReceiver, TrackPaymentMetadataReceiver {
    private TrackBase trackBase;
    private TrackLocationMetadata trackLocationMetadata;
    private TrackPaymentMetadata trackPaymentMetadata;
    private TrackSourceMetadata trackSourceMetadata;
    private TrackStoreSkuMetadata trackStoreSkuMetadata;
    private final CharSequence loadId = null;
    private final Boolean hasDescription = null;
    private final Boolean hasStaffReview = null;
    private final Long carouselImageCount = null;
    private final Long carouselVideoCount = null;
    private final Boolean hasNews = null;
    private final Boolean hasSinglePlayer = null;
    private final Boolean hasOnlineMultiplayer = null;
    private final Boolean hasLocalMultiplayer = null;
    private final Boolean hasPvpFeatures = null;
    private final Boolean hasCoop = null;
    private final Boolean hasLocalCoop = null;
    private final Boolean hasOnlineCoop = null;
    private final Boolean hasCrossPlatform = null;
    private final Boolean hasRichPresence = null;
    private final Boolean hasGameInvites = null;
    private final Boolean hasSpectatorMode = null;
    private final Boolean hasControllerSupport = null;
    private final Boolean hasCloudSaves = null;
    private final Boolean hasSecureNetworking = null;
    private final Long promotionId = null;
    private final Long listIndex = null;
    private final Boolean listSearched = null;
    private final CharSequence listSort = null;
    private final CharSequence listFilterDistributionType = null;
    private final CharSequence source = null;
    private final transient String analyticsSchemaTypeName = "store_listing_viewed";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackStoreListingViewed)) {
            return false;
        }
        TrackStoreListingViewed trackStoreListingViewed = (TrackStoreListingViewed) obj;
        return m.areEqual(this.loadId, trackStoreListingViewed.loadId) && m.areEqual(this.hasDescription, trackStoreListingViewed.hasDescription) && m.areEqual(this.hasStaffReview, trackStoreListingViewed.hasStaffReview) && m.areEqual(this.carouselImageCount, trackStoreListingViewed.carouselImageCount) && m.areEqual(this.carouselVideoCount, trackStoreListingViewed.carouselVideoCount) && m.areEqual(this.hasNews, trackStoreListingViewed.hasNews) && m.areEqual(this.hasSinglePlayer, trackStoreListingViewed.hasSinglePlayer) && m.areEqual(this.hasOnlineMultiplayer, trackStoreListingViewed.hasOnlineMultiplayer) && m.areEqual(this.hasLocalMultiplayer, trackStoreListingViewed.hasLocalMultiplayer) && m.areEqual(this.hasPvpFeatures, trackStoreListingViewed.hasPvpFeatures) && m.areEqual(this.hasCoop, trackStoreListingViewed.hasCoop) && m.areEqual(this.hasLocalCoop, trackStoreListingViewed.hasLocalCoop) && m.areEqual(this.hasOnlineCoop, trackStoreListingViewed.hasOnlineCoop) && m.areEqual(this.hasCrossPlatform, trackStoreListingViewed.hasCrossPlatform) && m.areEqual(this.hasRichPresence, trackStoreListingViewed.hasRichPresence) && m.areEqual(this.hasGameInvites, trackStoreListingViewed.hasGameInvites) && m.areEqual(this.hasSpectatorMode, trackStoreListingViewed.hasSpectatorMode) && m.areEqual(this.hasControllerSupport, trackStoreListingViewed.hasControllerSupport) && m.areEqual(this.hasCloudSaves, trackStoreListingViewed.hasCloudSaves) && m.areEqual(this.hasSecureNetworking, trackStoreListingViewed.hasSecureNetworking) && m.areEqual(this.promotionId, trackStoreListingViewed.promotionId) && m.areEqual(this.listIndex, trackStoreListingViewed.listIndex) && m.areEqual(this.listSearched, trackStoreListingViewed.listSearched) && m.areEqual(this.listSort, trackStoreListingViewed.listSort) && m.areEqual(this.listFilterDistributionType, trackStoreListingViewed.listFilterDistributionType) && m.areEqual(this.source, trackStoreListingViewed.source);
    }

    public int hashCode() {
        CharSequence charSequence = this.loadId;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        Boolean bool = this.hasDescription;
        int hashCode2 = (hashCode + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.hasStaffReview;
        int hashCode3 = (hashCode2 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Long l = this.carouselImageCount;
        int hashCode4 = (hashCode3 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.carouselVideoCount;
        int hashCode5 = (hashCode4 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Boolean bool3 = this.hasNews;
        int hashCode6 = (hashCode5 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        Boolean bool4 = this.hasSinglePlayer;
        int hashCode7 = (hashCode6 + (bool4 != null ? bool4.hashCode() : 0)) * 31;
        Boolean bool5 = this.hasOnlineMultiplayer;
        int hashCode8 = (hashCode7 + (bool5 != null ? bool5.hashCode() : 0)) * 31;
        Boolean bool6 = this.hasLocalMultiplayer;
        int hashCode9 = (hashCode8 + (bool6 != null ? bool6.hashCode() : 0)) * 31;
        Boolean bool7 = this.hasPvpFeatures;
        int hashCode10 = (hashCode9 + (bool7 != null ? bool7.hashCode() : 0)) * 31;
        Boolean bool8 = this.hasCoop;
        int hashCode11 = (hashCode10 + (bool8 != null ? bool8.hashCode() : 0)) * 31;
        Boolean bool9 = this.hasLocalCoop;
        int hashCode12 = (hashCode11 + (bool9 != null ? bool9.hashCode() : 0)) * 31;
        Boolean bool10 = this.hasOnlineCoop;
        int hashCode13 = (hashCode12 + (bool10 != null ? bool10.hashCode() : 0)) * 31;
        Boolean bool11 = this.hasCrossPlatform;
        int hashCode14 = (hashCode13 + (bool11 != null ? bool11.hashCode() : 0)) * 31;
        Boolean bool12 = this.hasRichPresence;
        int hashCode15 = (hashCode14 + (bool12 != null ? bool12.hashCode() : 0)) * 31;
        Boolean bool13 = this.hasGameInvites;
        int hashCode16 = (hashCode15 + (bool13 != null ? bool13.hashCode() : 0)) * 31;
        Boolean bool14 = this.hasSpectatorMode;
        int hashCode17 = (hashCode16 + (bool14 != null ? bool14.hashCode() : 0)) * 31;
        Boolean bool15 = this.hasControllerSupport;
        int hashCode18 = (hashCode17 + (bool15 != null ? bool15.hashCode() : 0)) * 31;
        Boolean bool16 = this.hasCloudSaves;
        int hashCode19 = (hashCode18 + (bool16 != null ? bool16.hashCode() : 0)) * 31;
        Boolean bool17 = this.hasSecureNetworking;
        int hashCode20 = (hashCode19 + (bool17 != null ? bool17.hashCode() : 0)) * 31;
        Long l3 = this.promotionId;
        int hashCode21 = (hashCode20 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.listIndex;
        int hashCode22 = (hashCode21 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Boolean bool18 = this.listSearched;
        int hashCode23 = (hashCode22 + (bool18 != null ? bool18.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.listSort;
        int hashCode24 = (hashCode23 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.listFilterDistributionType;
        int hashCode25 = (hashCode24 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.source;
        if (charSequence4 != null) {
            i = charSequence4.hashCode();
        }
        return hashCode25 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackStoreListingViewed(loadId=");
        R.append(this.loadId);
        R.append(", hasDescription=");
        R.append(this.hasDescription);
        R.append(", hasStaffReview=");
        R.append(this.hasStaffReview);
        R.append(", carouselImageCount=");
        R.append(this.carouselImageCount);
        R.append(", carouselVideoCount=");
        R.append(this.carouselVideoCount);
        R.append(", hasNews=");
        R.append(this.hasNews);
        R.append(", hasSinglePlayer=");
        R.append(this.hasSinglePlayer);
        R.append(", hasOnlineMultiplayer=");
        R.append(this.hasOnlineMultiplayer);
        R.append(", hasLocalMultiplayer=");
        R.append(this.hasLocalMultiplayer);
        R.append(", hasPvpFeatures=");
        R.append(this.hasPvpFeatures);
        R.append(", hasCoop=");
        R.append(this.hasCoop);
        R.append(", hasLocalCoop=");
        R.append(this.hasLocalCoop);
        R.append(", hasOnlineCoop=");
        R.append(this.hasOnlineCoop);
        R.append(", hasCrossPlatform=");
        R.append(this.hasCrossPlatform);
        R.append(", hasRichPresence=");
        R.append(this.hasRichPresence);
        R.append(", hasGameInvites=");
        R.append(this.hasGameInvites);
        R.append(", hasSpectatorMode=");
        R.append(this.hasSpectatorMode);
        R.append(", hasControllerSupport=");
        R.append(this.hasControllerSupport);
        R.append(", hasCloudSaves=");
        R.append(this.hasCloudSaves);
        R.append(", hasSecureNetworking=");
        R.append(this.hasSecureNetworking);
        R.append(", promotionId=");
        R.append(this.promotionId);
        R.append(", listIndex=");
        R.append(this.listIndex);
        R.append(", listSearched=");
        R.append(this.listSearched);
        R.append(", listSort=");
        R.append(this.listSort);
        R.append(", listFilterDistributionType=");
        R.append(this.listFilterDistributionType);
        R.append(", source=");
        return a.D(R, this.source, ")");
    }
}
