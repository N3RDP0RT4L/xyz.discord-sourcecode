package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackLeaveVoiceChannel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0014\n\u0002\u0018\u0002\n\u0002\b*\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0014\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0015\u001a\u0004\b\u0019\u0010\u0017R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0015\u001a\u0004\b\u001b\u0010\u0017R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u0015\u001a\u0004\b\u001d\u0010\u0017R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u0015\u001a\u0004\b\u001f\u0010\u0017R\u001b\u0010 \u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0010\u001a\u0004\b!\u0010\u0012R\u001b\u0010\"\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u0015\u001a\u0004\b#\u0010\u0017R\u001b\u0010$\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u0015\u001a\u0004\b%\u0010\u0017R\u001b\u0010&\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010\u0015\u001a\u0004\b'\u0010\u0017R$\u0010)\u001a\u0004\u0018\u00010(8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b)\u0010*\u001a\u0004\b+\u0010,\"\u0004\b-\u0010.R\u001b\u0010/\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b/\u00100\u001a\u0004\b1\u00102R\u001b\u00103\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010\u0010\u001a\u0004\b4\u0010\u0012R\u001b\u00105\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010\u0015\u001a\u0004\b6\u0010\u0017R\u001b\u00107\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010\u0015\u001a\u0004\b8\u0010\u0017R\u001b\u00109\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010\u0015\u001a\u0004\b:\u0010\u0017R\u001c\u0010;\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b;\u0010<\u001a\u0004\b=\u0010\u0005R\u001b\u0010>\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b>\u0010\u0010\u001a\u0004\b?\u0010\u0012R\u001b\u0010@\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b@\u0010\u0010\u001a\u0004\bA\u0010\u0012R\u001b\u0010B\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010\u0015\u001a\u0004\bC\u0010\u0017R\u001b\u0010D\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bD\u0010\u0015\u001a\u0004\bE\u0010\u0017R\u001b\u0010F\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bF\u0010\u0015\u001a\u0004\bG\u0010\u0017R\u001b\u0010H\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bH\u0010\u0015\u001a\u0004\bI\u0010\u0017R\u001b\u0010J\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bJ\u0010\u0015\u001a\u0004\bK\u0010\u0017R\u001b\u0010L\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bL\u0010\u0015\u001a\u0004\bM\u0010\u0017R\u001b\u0010N\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bN\u0010\u0015\u001a\u0004\bO\u0010\u0017R\u001b\u0010P\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\bP\u0010\u0015\u001a\u0004\bQ\u0010\u0017¨\u0006R"}, d2 = {"Lcom/discord/analytics/generated/events/TrackLeaveVoiceChannel;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "rtcConnectionId", "Ljava/lang/CharSequence;", "getRtcConnectionId", "()Ljava/lang/CharSequence;", "", "voiceStateCount", "Ljava/lang/Long;", "getVoiceStateCount", "()Ljava/lang/Long;", "viewModeToggleCount", "getViewModeToggleCount", "channelBitrate", "getChannelBitrate", "videoStreamCount", "getVideoStreamCount", "maxSpeakerCount", "getMaxSpeakerCount", "gamePlatform", "getGamePlatform", "totalSpeakerCount", "getTotalSpeakerCount", "gameId", "getGameId", "maxListenerCount", "getMaxListenerCount", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "videoEnabled", "Ljava/lang/Boolean;", "getVideoEnabled", "()Ljava/lang/Boolean;", "gameName", "getGameName", "maxVoiceStateCount", "getMaxVoiceStateCount", "totalListenerCount", "getTotalListenerCount", "guildId", "getGuildId", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "nonce", "getNonce", "mediaSessionId", "getMediaSessionId", "stageInstanceId", "getStageInstanceId", "viewModeFocusDurationMs", "getViewModeFocusDurationMs", "viewModeGridDurationMs", "getViewModeGridDurationMs", "channelId", "getChannelId", "channelType", "getChannelType", "duration", "getDuration", "guildScheduledEventId", "getGuildScheduledEventId", "totalVoiceStateCount", "getTotalVoiceStateCount", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackLeaveVoiceChannel implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Long channelId = null;
    private final Long channelType = null;
    private final Long channelBitrate = null;
    private final Long guildId = null;
    private final CharSequence nonce = null;
    private final CharSequence rtcConnectionId = null;
    private final CharSequence mediaSessionId = null;
    private final Long duration = null;
    private final Long voiceStateCount = null;
    private final Long videoStreamCount = null;
    private final Boolean videoEnabled = null;
    private final CharSequence gameName = null;
    private final CharSequence gamePlatform = null;
    private final Long gameId = null;
    private final Long maxVoiceStateCount = null;
    private final Long totalVoiceStateCount = null;
    private final Long maxSpeakerCount = null;
    private final Long totalSpeakerCount = null;
    private final Long maxListenerCount = null;
    private final Long totalListenerCount = null;
    private final Long stageInstanceId = null;
    private final Long guildScheduledEventId = null;
    private final Long viewModeGridDurationMs = null;
    private final Long viewModeFocusDurationMs = null;
    private final Long viewModeToggleCount = null;
    private final transient String analyticsSchemaTypeName = "leave_voice_channel";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackLeaveVoiceChannel)) {
            return false;
        }
        TrackLeaveVoiceChannel trackLeaveVoiceChannel = (TrackLeaveVoiceChannel) obj;
        return m.areEqual(this.channelId, trackLeaveVoiceChannel.channelId) && m.areEqual(this.channelType, trackLeaveVoiceChannel.channelType) && m.areEqual(this.channelBitrate, trackLeaveVoiceChannel.channelBitrate) && m.areEqual(this.guildId, trackLeaveVoiceChannel.guildId) && m.areEqual(this.nonce, trackLeaveVoiceChannel.nonce) && m.areEqual(this.rtcConnectionId, trackLeaveVoiceChannel.rtcConnectionId) && m.areEqual(this.mediaSessionId, trackLeaveVoiceChannel.mediaSessionId) && m.areEqual(this.duration, trackLeaveVoiceChannel.duration) && m.areEqual(this.voiceStateCount, trackLeaveVoiceChannel.voiceStateCount) && m.areEqual(this.videoStreamCount, trackLeaveVoiceChannel.videoStreamCount) && m.areEqual(this.videoEnabled, trackLeaveVoiceChannel.videoEnabled) && m.areEqual(this.gameName, trackLeaveVoiceChannel.gameName) && m.areEqual(this.gamePlatform, trackLeaveVoiceChannel.gamePlatform) && m.areEqual(this.gameId, trackLeaveVoiceChannel.gameId) && m.areEqual(this.maxVoiceStateCount, trackLeaveVoiceChannel.maxVoiceStateCount) && m.areEqual(this.totalVoiceStateCount, trackLeaveVoiceChannel.totalVoiceStateCount) && m.areEqual(this.maxSpeakerCount, trackLeaveVoiceChannel.maxSpeakerCount) && m.areEqual(this.totalSpeakerCount, trackLeaveVoiceChannel.totalSpeakerCount) && m.areEqual(this.maxListenerCount, trackLeaveVoiceChannel.maxListenerCount) && m.areEqual(this.totalListenerCount, trackLeaveVoiceChannel.totalListenerCount) && m.areEqual(this.stageInstanceId, trackLeaveVoiceChannel.stageInstanceId) && m.areEqual(this.guildScheduledEventId, trackLeaveVoiceChannel.guildScheduledEventId) && m.areEqual(this.viewModeGridDurationMs, trackLeaveVoiceChannel.viewModeGridDurationMs) && m.areEqual(this.viewModeFocusDurationMs, trackLeaveVoiceChannel.viewModeFocusDurationMs) && m.areEqual(this.viewModeToggleCount, trackLeaveVoiceChannel.viewModeToggleCount);
    }

    public int hashCode() {
        Long l = this.channelId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.channelType;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.channelBitrate;
        int hashCode3 = (hashCode2 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.guildId;
        int hashCode4 = (hashCode3 + (l4 != null ? l4.hashCode() : 0)) * 31;
        CharSequence charSequence = this.nonce;
        int hashCode5 = (hashCode4 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.rtcConnectionId;
        int hashCode6 = (hashCode5 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.mediaSessionId;
        int hashCode7 = (hashCode6 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Long l5 = this.duration;
        int hashCode8 = (hashCode7 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.voiceStateCount;
        int hashCode9 = (hashCode8 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.videoStreamCount;
        int hashCode10 = (hashCode9 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Boolean bool = this.videoEnabled;
        int hashCode11 = (hashCode10 + (bool != null ? bool.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.gameName;
        int hashCode12 = (hashCode11 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.gamePlatform;
        int hashCode13 = (hashCode12 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        Long l8 = this.gameId;
        int hashCode14 = (hashCode13 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.maxVoiceStateCount;
        int hashCode15 = (hashCode14 + (l9 != null ? l9.hashCode() : 0)) * 31;
        Long l10 = this.totalVoiceStateCount;
        int hashCode16 = (hashCode15 + (l10 != null ? l10.hashCode() : 0)) * 31;
        Long l11 = this.maxSpeakerCount;
        int hashCode17 = (hashCode16 + (l11 != null ? l11.hashCode() : 0)) * 31;
        Long l12 = this.totalSpeakerCount;
        int hashCode18 = (hashCode17 + (l12 != null ? l12.hashCode() : 0)) * 31;
        Long l13 = this.maxListenerCount;
        int hashCode19 = (hashCode18 + (l13 != null ? l13.hashCode() : 0)) * 31;
        Long l14 = this.totalListenerCount;
        int hashCode20 = (hashCode19 + (l14 != null ? l14.hashCode() : 0)) * 31;
        Long l15 = this.stageInstanceId;
        int hashCode21 = (hashCode20 + (l15 != null ? l15.hashCode() : 0)) * 31;
        Long l16 = this.guildScheduledEventId;
        int hashCode22 = (hashCode21 + (l16 != null ? l16.hashCode() : 0)) * 31;
        Long l17 = this.viewModeGridDurationMs;
        int hashCode23 = (hashCode22 + (l17 != null ? l17.hashCode() : 0)) * 31;
        Long l18 = this.viewModeFocusDurationMs;
        int hashCode24 = (hashCode23 + (l18 != null ? l18.hashCode() : 0)) * 31;
        Long l19 = this.viewModeToggleCount;
        if (l19 != null) {
            i = l19.hashCode();
        }
        return hashCode24 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackLeaveVoiceChannel(channelId=");
        R.append(this.channelId);
        R.append(", channelType=");
        R.append(this.channelType);
        R.append(", channelBitrate=");
        R.append(this.channelBitrate);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", nonce=");
        R.append(this.nonce);
        R.append(", rtcConnectionId=");
        R.append(this.rtcConnectionId);
        R.append(", mediaSessionId=");
        R.append(this.mediaSessionId);
        R.append(", duration=");
        R.append(this.duration);
        R.append(", voiceStateCount=");
        R.append(this.voiceStateCount);
        R.append(", videoStreamCount=");
        R.append(this.videoStreamCount);
        R.append(", videoEnabled=");
        R.append(this.videoEnabled);
        R.append(", gameName=");
        R.append(this.gameName);
        R.append(", gamePlatform=");
        R.append(this.gamePlatform);
        R.append(", gameId=");
        R.append(this.gameId);
        R.append(", maxVoiceStateCount=");
        R.append(this.maxVoiceStateCount);
        R.append(", totalVoiceStateCount=");
        R.append(this.totalVoiceStateCount);
        R.append(", maxSpeakerCount=");
        R.append(this.maxSpeakerCount);
        R.append(", totalSpeakerCount=");
        R.append(this.totalSpeakerCount);
        R.append(", maxListenerCount=");
        R.append(this.maxListenerCount);
        R.append(", totalListenerCount=");
        R.append(this.totalListenerCount);
        R.append(", stageInstanceId=");
        R.append(this.stageInstanceId);
        R.append(", guildScheduledEventId=");
        R.append(this.guildScheduledEventId);
        R.append(", viewModeGridDurationMs=");
        R.append(this.viewModeGridDurationMs);
        R.append(", viewModeFocusDurationMs=");
        R.append(this.viewModeFocusDurationMs);
        R.append(", viewModeToggleCount=");
        return a.F(R, this.viewModeToggleCount, ")");
    }
}
