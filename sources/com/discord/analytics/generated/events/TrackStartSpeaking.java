package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackChannel;
import com.discord.analytics.generated.traits.TrackChannelReceiver;
import com.discord.analytics.generated.traits.TrackGuild;
import com.discord.analytics.generated.traits.TrackGuildReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackStartSpeaking.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0016\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u001a\u0010\u000e\u001a\u00020\r2\b\u0010\f\u001a\u0004\u0018\u00010\u000bHÖ\u0003¢\u0006\u0004\b\u000e\u0010\u000fR\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0017\u001a\u0004\b\u001b\u0010\u0019R$\u0010\u001d\u001a\u0004\u0018\u00010\u001c8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R\u001b\u0010#\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u0017\u001a\u0004\b$\u0010\u0019R\u001b\u0010%\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u0017\u001a\u0004\b&\u0010\u0019R\u001b\u0010'\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010\u0012\u001a\u0004\b(\u0010\u0014R\u001b\u0010)\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010\u0017\u001a\u0004\b*\u0010\u0019R\u001b\u0010+\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010\u0012\u001a\u0004\b,\u0010\u0014R\u001b\u0010-\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u0012\u001a\u0004\b.\u0010\u0014R\u001b\u0010/\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u0017\u001a\u0004\b0\u0010\u0019R\u001b\u00101\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010\u0017\u001a\u0004\b2\u0010\u0019R$\u00104\u001a\u0004\u0018\u0001038\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b4\u00105\u001a\u0004\b6\u00107\"\u0004\b8\u00109R\u001b\u0010:\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010\u0017\u001a\u0004\b;\u0010\u0019R\u001b\u0010<\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b<\u0010\u0012\u001a\u0004\b=\u0010\u0014R$\u0010?\u001a\u0004\u0018\u00010>8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b?\u0010@\u001a\u0004\bA\u0010B\"\u0004\bC\u0010DR\u001b\u0010E\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\bE\u0010F\u001a\u0004\bG\u0010HR\u001b\u0010I\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\bI\u0010\u0012\u001a\u0004\bJ\u0010\u0014R\u001c\u0010K\u001a\u00020\u00058\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\bK\u0010L\u001a\u0004\bM\u0010\u0007¨\u0006N"}, d2 = {"Lcom/discord/analytics/generated/events/TrackStartSpeaking;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackGuildReceiver;", "Lcom/discord/analytics/generated/traits/TrackChannelReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "mode", "Ljava/lang/CharSequence;", "getMode", "()Ljava/lang/CharSequence;", "", "packetsSentLost", "Ljava/lang/Long;", "getPacketsSentLost", "()Ljava/lang/Long;", "packetsReceived", "getPacketsReceived", "Lcom/discord/analytics/generated/traits/TrackGuild;", "trackGuild", "Lcom/discord/analytics/generated/traits/TrackGuild;", "getTrackGuild", "()Lcom/discord/analytics/generated/traits/TrackGuild;", "setTrackGuild", "(Lcom/discord/analytics/generated/traits/TrackGuild;)V", "channel", "getChannel", "server", "getServer", "gameName", "getGameName", "gameId", "getGameId", "mediaSessionId", "getMediaSessionId", "rtcConnectionId", "getRtcConnectionId", "packetsReceivedLost", "getPacketsReceivedLost", "voiceStateCount", "getVoiceStateCount", "Lcom/discord/analytics/generated/traits/TrackChannel;", "trackChannel", "Lcom/discord/analytics/generated/traits/TrackChannel;", "getTrackChannel", "()Lcom/discord/analytics/generated/traits/TrackChannel;", "setTrackChannel", "(Lcom/discord/analytics/generated/traits/TrackChannel;)V", "packetsSent", "getPacketsSent", "gameExeName", "getGameExeName", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "priority", "Ljava/lang/Boolean;", "getPriority", "()Ljava/lang/Boolean;", "gamePlatform", "getGamePlatform", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackStartSpeaking implements AnalyticsSchema, TrackBaseReceiver, TrackGuildReceiver, TrackChannelReceiver {
    private TrackBase trackBase;
    private TrackChannel trackChannel;
    private TrackGuild trackGuild;
    private final Long channel = null;
    private final CharSequence mode = null;
    private final Boolean priority = null;
    private final Long server = null;
    private final CharSequence gamePlatform = null;
    private final CharSequence gameName = null;
    private final CharSequence gameExeName = null;
    private final Long gameId = null;
    private final CharSequence mediaSessionId = null;
    private final CharSequence rtcConnectionId = null;
    private final Long packetsSent = null;
    private final Long packetsSentLost = null;
    private final Long packetsReceived = null;
    private final Long packetsReceivedLost = null;
    private final Long voiceStateCount = null;
    private final transient String analyticsSchemaTypeName = "start_speaking";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackStartSpeaking)) {
            return false;
        }
        TrackStartSpeaking trackStartSpeaking = (TrackStartSpeaking) obj;
        return m.areEqual(this.channel, trackStartSpeaking.channel) && m.areEqual(this.mode, trackStartSpeaking.mode) && m.areEqual(this.priority, trackStartSpeaking.priority) && m.areEqual(this.server, trackStartSpeaking.server) && m.areEqual(this.gamePlatform, trackStartSpeaking.gamePlatform) && m.areEqual(this.gameName, trackStartSpeaking.gameName) && m.areEqual(this.gameExeName, trackStartSpeaking.gameExeName) && m.areEqual(this.gameId, trackStartSpeaking.gameId) && m.areEqual(this.mediaSessionId, trackStartSpeaking.mediaSessionId) && m.areEqual(this.rtcConnectionId, trackStartSpeaking.rtcConnectionId) && m.areEqual(this.packetsSent, trackStartSpeaking.packetsSent) && m.areEqual(this.packetsSentLost, trackStartSpeaking.packetsSentLost) && m.areEqual(this.packetsReceived, trackStartSpeaking.packetsReceived) && m.areEqual(this.packetsReceivedLost, trackStartSpeaking.packetsReceivedLost) && m.areEqual(this.voiceStateCount, trackStartSpeaking.voiceStateCount);
    }

    public int hashCode() {
        Long l = this.channel;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        CharSequence charSequence = this.mode;
        int hashCode2 = (hashCode + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Boolean bool = this.priority;
        int hashCode3 = (hashCode2 + (bool != null ? bool.hashCode() : 0)) * 31;
        Long l2 = this.server;
        int hashCode4 = (hashCode3 + (l2 != null ? l2.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.gamePlatform;
        int hashCode5 = (hashCode4 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.gameName;
        int hashCode6 = (hashCode5 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.gameExeName;
        int hashCode7 = (hashCode6 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        Long l3 = this.gameId;
        int hashCode8 = (hashCode7 + (l3 != null ? l3.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.mediaSessionId;
        int hashCode9 = (hashCode8 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        CharSequence charSequence6 = this.rtcConnectionId;
        int hashCode10 = (hashCode9 + (charSequence6 != null ? charSequence6.hashCode() : 0)) * 31;
        Long l4 = this.packetsSent;
        int hashCode11 = (hashCode10 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.packetsSentLost;
        int hashCode12 = (hashCode11 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.packetsReceived;
        int hashCode13 = (hashCode12 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.packetsReceivedLost;
        int hashCode14 = (hashCode13 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Long l8 = this.voiceStateCount;
        if (l8 != null) {
            i = l8.hashCode();
        }
        return hashCode14 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackStartSpeaking(channel=");
        R.append(this.channel);
        R.append(", mode=");
        R.append(this.mode);
        R.append(", priority=");
        R.append(this.priority);
        R.append(", server=");
        R.append(this.server);
        R.append(", gamePlatform=");
        R.append(this.gamePlatform);
        R.append(", gameName=");
        R.append(this.gameName);
        R.append(", gameExeName=");
        R.append(this.gameExeName);
        R.append(", gameId=");
        R.append(this.gameId);
        R.append(", mediaSessionId=");
        R.append(this.mediaSessionId);
        R.append(", rtcConnectionId=");
        R.append(this.rtcConnectionId);
        R.append(", packetsSent=");
        R.append(this.packetsSent);
        R.append(", packetsSentLost=");
        R.append(this.packetsSentLost);
        R.append(", packetsReceived=");
        R.append(this.packetsReceived);
        R.append(", packetsReceivedLost=");
        R.append(this.packetsReceivedLost);
        R.append(", voiceStateCount=");
        return a.F(R, this.voiceStateCount, ")");
    }
}
