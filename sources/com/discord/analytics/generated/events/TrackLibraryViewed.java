package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackLibraryViewed.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\r\n\u0002\b\u0012\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R$\u0010\u0014\u001a\u0004\u0018\u00010\u00138\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0010\u001a\u0004\b\u001b\u0010\u0012R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u0010\u001a\u0004\b\u001d\u0010\u0012R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u0010\u001a\u0004\b\u001f\u0010\u0012R\u001b\u0010!\u001a\u0004\u0018\u00010 8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010$R\u001b\u0010%\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u0010\u001a\u0004\b&\u0010\u0012R\u001b\u0010'\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010\u0010\u001a\u0004\b(\u0010\u0012R\u001b\u0010)\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010\u0010\u001a\u0004\b*\u0010\u0012R\u001b\u0010+\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010\u0010\u001a\u0004\b,\u0010\u0012R\u001b\u0010-\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u0010\u001a\u0004\b.\u0010\u0012R\u001c\u0010/\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b/\u00100\u001a\u0004\b1\u0010\u0005¨\u00062"}, d2 = {"Lcom/discord/analytics/generated/events/TrackLibraryViewed;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "numApplicationsUplay", "Ljava/lang/Long;", "getNumApplicationsUplay", "()Ljava/lang/Long;", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "numApplicationsBattlenet", "getNumApplicationsBattlenet", "numApplicationsSteam", "getNumApplicationsSteam", "numApplicationsTotal", "getNumApplicationsTotal", "", "loadId", "Ljava/lang/CharSequence;", "getLoadId", "()Ljava/lang/CharSequence;", "numApplicationsGog", "getNumApplicationsGog", "numApplicationsOrigin", "getNumApplicationsOrigin", "numApplicationsDiscord", "getNumApplicationsDiscord", "numApplicationsEpic", "getNumApplicationsEpic", "numApplicationsTwitch", "getNumApplicationsTwitch", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackLibraryViewed implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final CharSequence loadId = null;
    private final Long numApplicationsTotal = null;
    private final Long numApplicationsBattlenet = null;
    private final Long numApplicationsDiscord = null;
    private final Long numApplicationsSteam = null;
    private final Long numApplicationsTwitch = null;
    private final Long numApplicationsUplay = null;
    private final Long numApplicationsOrigin = null;
    private final Long numApplicationsGog = null;
    private final Long numApplicationsEpic = null;
    private final transient String analyticsSchemaTypeName = "library_viewed";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackLibraryViewed)) {
            return false;
        }
        TrackLibraryViewed trackLibraryViewed = (TrackLibraryViewed) obj;
        return m.areEqual(this.loadId, trackLibraryViewed.loadId) && m.areEqual(this.numApplicationsTotal, trackLibraryViewed.numApplicationsTotal) && m.areEqual(this.numApplicationsBattlenet, trackLibraryViewed.numApplicationsBattlenet) && m.areEqual(this.numApplicationsDiscord, trackLibraryViewed.numApplicationsDiscord) && m.areEqual(this.numApplicationsSteam, trackLibraryViewed.numApplicationsSteam) && m.areEqual(this.numApplicationsTwitch, trackLibraryViewed.numApplicationsTwitch) && m.areEqual(this.numApplicationsUplay, trackLibraryViewed.numApplicationsUplay) && m.areEqual(this.numApplicationsOrigin, trackLibraryViewed.numApplicationsOrigin) && m.areEqual(this.numApplicationsGog, trackLibraryViewed.numApplicationsGog) && m.areEqual(this.numApplicationsEpic, trackLibraryViewed.numApplicationsEpic);
    }

    public int hashCode() {
        CharSequence charSequence = this.loadId;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        Long l = this.numApplicationsTotal;
        int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.numApplicationsBattlenet;
        int hashCode3 = (hashCode2 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.numApplicationsDiscord;
        int hashCode4 = (hashCode3 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.numApplicationsSteam;
        int hashCode5 = (hashCode4 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.numApplicationsTwitch;
        int hashCode6 = (hashCode5 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.numApplicationsUplay;
        int hashCode7 = (hashCode6 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.numApplicationsOrigin;
        int hashCode8 = (hashCode7 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Long l8 = this.numApplicationsGog;
        int hashCode9 = (hashCode8 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.numApplicationsEpic;
        if (l9 != null) {
            i = l9.hashCode();
        }
        return hashCode9 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackLibraryViewed(loadId=");
        R.append(this.loadId);
        R.append(", numApplicationsTotal=");
        R.append(this.numApplicationsTotal);
        R.append(", numApplicationsBattlenet=");
        R.append(this.numApplicationsBattlenet);
        R.append(", numApplicationsDiscord=");
        R.append(this.numApplicationsDiscord);
        R.append(", numApplicationsSteam=");
        R.append(this.numApplicationsSteam);
        R.append(", numApplicationsTwitch=");
        R.append(this.numApplicationsTwitch);
        R.append(", numApplicationsUplay=");
        R.append(this.numApplicationsUplay);
        R.append(", numApplicationsOrigin=");
        R.append(this.numApplicationsOrigin);
        R.append(", numApplicationsGog=");
        R.append(this.numApplicationsGog);
        R.append(", numApplicationsEpic=");
        return a.F(R, this.numApplicationsEpic, ")");
    }
}
