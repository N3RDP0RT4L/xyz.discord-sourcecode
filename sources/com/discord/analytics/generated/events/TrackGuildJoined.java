package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackGuildJoined.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\n\n\u0002\u0010\r\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0018\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0010\u001a\u0004\b\u0014\u0010\u0012R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0010\u001a\u0004\b\u0016\u0010\u0012R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0010\u001a\u0004\b\u0018\u0010\u0012R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00198\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u001dR\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u00198\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u001b\u001a\u0004\b\u001f\u0010\u001dR\u001b\u0010 \u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0010\u001a\u0004\b!\u0010\u0012R\u001b\u0010\"\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u0010\u001a\u0004\b#\u0010\u0012R\u001b\u0010$\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u0010\u001a\u0004\b%\u0010\u0012R$\u0010'\u001a\u0004\u0018\u00010&8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*\"\u0004\b+\u0010,R\u001b\u0010-\u001a\u0004\u0018\u00010\u00198\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u001b\u001a\u0004\b.\u0010\u001dR\u001b\u0010/\u001a\u0004\u0018\u00010\u00198\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u001b\u001a\u0004\b0\u0010\u001dR\u001b\u00101\u001a\u0004\u0018\u00010\u00198\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010\u001b\u001a\u0004\b2\u0010\u001dR\u001b\u00103\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010\u0010\u001a\u0004\b4\u0010\u0012R\u001b\u00105\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010\u0010\u001a\u0004\b6\u0010\u0012R\u001b\u00107\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010\u0010\u001a\u0004\b8\u0010\u0012R\u001c\u00109\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b9\u0010:\u001a\u0004\b;\u0010\u0005R\u001b\u0010<\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b<\u0010\u0010\u001a\u0004\b=\u0010\u0012¨\u0006>"}, d2 = {"Lcom/discord/analytics/generated/events/TrackGuildJoined;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "botId", "Ljava/lang/Long;", "getBotId", "()Ljava/lang/Long;", "guildId", "getGuildId", "inviteGuildScheduledEventId", "getInviteGuildScheduledEventId", "postableChannels", "getPostableChannels", "", "recommendationLoadId", "Ljava/lang/CharSequence;", "getRecommendationLoadId", "()Ljava/lang/CharSequence;", "guildName", "getGuildName", "locationChannelType", "getLocationChannelType", "locationGuildId", "getLocationGuildId", "locationChannelId", "getLocationChannelId", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "joinMethod", "getJoinMethod", "joinType", "getJoinType", "source", "getSource", "guildOwnerId", "getGuildOwnerId", "locationMessageId", "getLocationMessageId", "userGuilds", "getUserGuilds", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "applicationId", "getApplicationId", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackGuildJoined implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final CharSequence joinMethod = null;
    private final Long applicationId = null;
    private final Long botId = null;
    private final Long guildId = null;
    private final CharSequence guildName = null;
    private final Long guildOwnerId = null;
    private final Long userGuilds = null;
    private final CharSequence joinType = null;
    private final CharSequence source = null;
    private final Long locationGuildId = null;
    private final Long locationChannelId = null;
    private final Long locationChannelType = null;
    private final Long locationMessageId = null;
    private final Long inviteGuildScheduledEventId = null;
    private final Long postableChannels = null;
    private final CharSequence recommendationLoadId = null;
    private final transient String analyticsSchemaTypeName = "guild_joined";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackGuildJoined)) {
            return false;
        }
        TrackGuildJoined trackGuildJoined = (TrackGuildJoined) obj;
        return m.areEqual(this.joinMethod, trackGuildJoined.joinMethod) && m.areEqual(this.applicationId, trackGuildJoined.applicationId) && m.areEqual(this.botId, trackGuildJoined.botId) && m.areEqual(this.guildId, trackGuildJoined.guildId) && m.areEqual(this.guildName, trackGuildJoined.guildName) && m.areEqual(this.guildOwnerId, trackGuildJoined.guildOwnerId) && m.areEqual(this.userGuilds, trackGuildJoined.userGuilds) && m.areEqual(this.joinType, trackGuildJoined.joinType) && m.areEqual(this.source, trackGuildJoined.source) && m.areEqual(this.locationGuildId, trackGuildJoined.locationGuildId) && m.areEqual(this.locationChannelId, trackGuildJoined.locationChannelId) && m.areEqual(this.locationChannelType, trackGuildJoined.locationChannelType) && m.areEqual(this.locationMessageId, trackGuildJoined.locationMessageId) && m.areEqual(this.inviteGuildScheduledEventId, trackGuildJoined.inviteGuildScheduledEventId) && m.areEqual(this.postableChannels, trackGuildJoined.postableChannels) && m.areEqual(this.recommendationLoadId, trackGuildJoined.recommendationLoadId);
    }

    public int hashCode() {
        CharSequence charSequence = this.joinMethod;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        Long l = this.applicationId;
        int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.botId;
        int hashCode3 = (hashCode2 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.guildId;
        int hashCode4 = (hashCode3 + (l3 != null ? l3.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.guildName;
        int hashCode5 = (hashCode4 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l4 = this.guildOwnerId;
        int hashCode6 = (hashCode5 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.userGuilds;
        int hashCode7 = (hashCode6 + (l5 != null ? l5.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.joinType;
        int hashCode8 = (hashCode7 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.source;
        int hashCode9 = (hashCode8 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        Long l6 = this.locationGuildId;
        int hashCode10 = (hashCode9 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.locationChannelId;
        int hashCode11 = (hashCode10 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Long l8 = this.locationChannelType;
        int hashCode12 = (hashCode11 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.locationMessageId;
        int hashCode13 = (hashCode12 + (l9 != null ? l9.hashCode() : 0)) * 31;
        Long l10 = this.inviteGuildScheduledEventId;
        int hashCode14 = (hashCode13 + (l10 != null ? l10.hashCode() : 0)) * 31;
        Long l11 = this.postableChannels;
        int hashCode15 = (hashCode14 + (l11 != null ? l11.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.recommendationLoadId;
        if (charSequence5 != null) {
            i = charSequence5.hashCode();
        }
        return hashCode15 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackGuildJoined(joinMethod=");
        R.append(this.joinMethod);
        R.append(", applicationId=");
        R.append(this.applicationId);
        R.append(", botId=");
        R.append(this.botId);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", guildName=");
        R.append(this.guildName);
        R.append(", guildOwnerId=");
        R.append(this.guildOwnerId);
        R.append(", userGuilds=");
        R.append(this.userGuilds);
        R.append(", joinType=");
        R.append(this.joinType);
        R.append(", source=");
        R.append(this.source);
        R.append(", locationGuildId=");
        R.append(this.locationGuildId);
        R.append(", locationChannelId=");
        R.append(this.locationChannelId);
        R.append(", locationChannelType=");
        R.append(this.locationChannelType);
        R.append(", locationMessageId=");
        R.append(this.locationMessageId);
        R.append(", inviteGuildScheduledEventId=");
        R.append(this.inviteGuildScheduledEventId);
        R.append(", postableChannels=");
        R.append(this.postableChannels);
        R.append(", recommendationLoadId=");
        return a.D(R, this.recommendationLoadId, ")");
    }
}
