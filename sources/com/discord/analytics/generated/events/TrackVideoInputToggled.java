package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: TrackVideoInputToggled.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u001b\n\u0002\u0010 \n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0011\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0010\u001a\u0004\b\u0014\u0010\u0012R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0017\u001a\u0004\b\u001b\u0010\u0019R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001fR\u001b\u0010 \u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0010\u001a\u0004\b!\u0010\u0012R\u001b\u0010\"\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u001d\u001a\u0004\b#\u0010\u001fR\u001b\u0010$\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u0010\u001a\u0004\b%\u0010\u0012R\u001b\u0010&\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010\u0017\u001a\u0004\b'\u0010\u0019R\u001c\u0010(\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b(\u0010)\u001a\u0004\b*\u0010\u0005R\u001b\u0010+\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010\u0010\u001a\u0004\b,\u0010\u0012R\u001b\u0010-\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u0017\u001a\u0004\b.\u0010\u0019R\u001b\u0010/\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u0010\u001a\u0004\b0\u0010\u0012R!\u00102\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u0001018\u0006@\u0006¢\u0006\f\n\u0004\b2\u00103\u001a\u0004\b4\u00105R\u001b\u00106\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010\u0017\u001a\u0004\b7\u0010\u0019R\u001b\u00108\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010\u0010\u001a\u0004\b9\u0010\u0012R\u001b\u0010:\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010\u0017\u001a\u0004\b;\u0010\u0019R\u001b\u0010<\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b<\u0010\u0017\u001a\u0004\b=\u0010\u0019R$\u0010?\u001a\u0004\u0018\u00010>8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b?\u0010@\u001a\u0004\bA\u0010B\"\u0004\bC\u0010DR\u001b\u0010E\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\bE\u0010\u0017\u001a\u0004\bF\u0010\u0019R\u001b\u0010G\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bG\u0010\u0010\u001a\u0004\bH\u0010\u0012R\u001b\u0010I\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\bI\u0010\u0017\u001a\u0004\bJ\u0010\u0019R\u001b\u0010K\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bK\u0010\u0010\u001a\u0004\bL\u0010\u0012R\u001b\u0010M\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bM\u0010\u0010\u001a\u0004\bN\u0010\u0012¨\u0006O"}, d2 = {"Lcom/discord/analytics/generated/events/TrackVideoInputToggled;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "videoToggleSource", "Ljava/lang/CharSequence;", "getVideoToggleSource", "()Ljava/lang/CharSequence;", "mediaSessionId", "getMediaSessionId", "", "guildId", "Ljava/lang/Long;", "getGuildId", "()Ljava/lang/Long;", "videoStreamCount", "getVideoStreamCount", "videoEnabled", "Ljava/lang/Boolean;", "getVideoEnabled", "()Ljava/lang/Boolean;", "gameName", "getGameName", "previewEnabled", "getPreviewEnabled", "source", "getSource", "shareGameId", "getShareGameId", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "shareGameName", "getShareGameName", "channelType", "getChannelType", "videoInputType", "getVideoInputType", "", "enabledInputs", "Ljava/util/List;", "getEnabledInputs", "()Ljava/util/List;", "videoInputFrameRate", "getVideoInputFrameRate", "soundshareSession", "getSoundshareSession", "voiceStateCount", "getVoiceStateCount", "channelId", "getChannelId", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "videoInputResolution", "getVideoInputResolution", "gamePlatform", "getGamePlatform", "gameId", "getGameId", "gameExeName", "getGameExeName", "nonce", "getNonce", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackVideoInputToggled implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Long channelId = null;
    private final Long channelType = null;
    private final Long guildId = null;
    private final CharSequence nonce = null;
    private final Long voiceStateCount = null;
    private final Long videoStreamCount = null;
    private final Boolean videoEnabled = null;
    private final CharSequence gameName = null;
    private final CharSequence gameExeName = null;
    private final Long gameId = null;
    private final CharSequence videoInputType = null;
    private final Long videoInputResolution = null;
    private final Long videoInputFrameRate = null;
    private final CharSequence soundshareSession = null;
    private final CharSequence shareGameName = null;
    private final Long shareGameId = null;
    private final CharSequence gamePlatform = null;
    private final CharSequence source = null;
    private final CharSequence videoToggleSource = null;
    private final CharSequence mediaSessionId = null;
    private final List<CharSequence> enabledInputs = null;
    private final Boolean previewEnabled = null;
    private final transient String analyticsSchemaTypeName = "video_input_toggled";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackVideoInputToggled)) {
            return false;
        }
        TrackVideoInputToggled trackVideoInputToggled = (TrackVideoInputToggled) obj;
        return m.areEqual(this.channelId, trackVideoInputToggled.channelId) && m.areEqual(this.channelType, trackVideoInputToggled.channelType) && m.areEqual(this.guildId, trackVideoInputToggled.guildId) && m.areEqual(this.nonce, trackVideoInputToggled.nonce) && m.areEqual(this.voiceStateCount, trackVideoInputToggled.voiceStateCount) && m.areEqual(this.videoStreamCount, trackVideoInputToggled.videoStreamCount) && m.areEqual(this.videoEnabled, trackVideoInputToggled.videoEnabled) && m.areEqual(this.gameName, trackVideoInputToggled.gameName) && m.areEqual(this.gameExeName, trackVideoInputToggled.gameExeName) && m.areEqual(this.gameId, trackVideoInputToggled.gameId) && m.areEqual(this.videoInputType, trackVideoInputToggled.videoInputType) && m.areEqual(this.videoInputResolution, trackVideoInputToggled.videoInputResolution) && m.areEqual(this.videoInputFrameRate, trackVideoInputToggled.videoInputFrameRate) && m.areEqual(this.soundshareSession, trackVideoInputToggled.soundshareSession) && m.areEqual(this.shareGameName, trackVideoInputToggled.shareGameName) && m.areEqual(this.shareGameId, trackVideoInputToggled.shareGameId) && m.areEqual(this.gamePlatform, trackVideoInputToggled.gamePlatform) && m.areEqual(this.source, trackVideoInputToggled.source) && m.areEqual(this.videoToggleSource, trackVideoInputToggled.videoToggleSource) && m.areEqual(this.mediaSessionId, trackVideoInputToggled.mediaSessionId) && m.areEqual(this.enabledInputs, trackVideoInputToggled.enabledInputs) && m.areEqual(this.previewEnabled, trackVideoInputToggled.previewEnabled);
    }

    public int hashCode() {
        Long l = this.channelId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.channelType;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.guildId;
        int hashCode3 = (hashCode2 + (l3 != null ? l3.hashCode() : 0)) * 31;
        CharSequence charSequence = this.nonce;
        int hashCode4 = (hashCode3 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Long l4 = this.voiceStateCount;
        int hashCode5 = (hashCode4 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.videoStreamCount;
        int hashCode6 = (hashCode5 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Boolean bool = this.videoEnabled;
        int hashCode7 = (hashCode6 + (bool != null ? bool.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.gameName;
        int hashCode8 = (hashCode7 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.gameExeName;
        int hashCode9 = (hashCode8 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Long l6 = this.gameId;
        int hashCode10 = (hashCode9 + (l6 != null ? l6.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.videoInputType;
        int hashCode11 = (hashCode10 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        Long l7 = this.videoInputResolution;
        int hashCode12 = (hashCode11 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Long l8 = this.videoInputFrameRate;
        int hashCode13 = (hashCode12 + (l8 != null ? l8.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.soundshareSession;
        int hashCode14 = (hashCode13 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        CharSequence charSequence6 = this.shareGameName;
        int hashCode15 = (hashCode14 + (charSequence6 != null ? charSequence6.hashCode() : 0)) * 31;
        Long l9 = this.shareGameId;
        int hashCode16 = (hashCode15 + (l9 != null ? l9.hashCode() : 0)) * 31;
        CharSequence charSequence7 = this.gamePlatform;
        int hashCode17 = (hashCode16 + (charSequence7 != null ? charSequence7.hashCode() : 0)) * 31;
        CharSequence charSequence8 = this.source;
        int hashCode18 = (hashCode17 + (charSequence8 != null ? charSequence8.hashCode() : 0)) * 31;
        CharSequence charSequence9 = this.videoToggleSource;
        int hashCode19 = (hashCode18 + (charSequence9 != null ? charSequence9.hashCode() : 0)) * 31;
        CharSequence charSequence10 = this.mediaSessionId;
        int hashCode20 = (hashCode19 + (charSequence10 != null ? charSequence10.hashCode() : 0)) * 31;
        List<CharSequence> list = this.enabledInputs;
        int hashCode21 = (hashCode20 + (list != null ? list.hashCode() : 0)) * 31;
        Boolean bool2 = this.previewEnabled;
        if (bool2 != null) {
            i = bool2.hashCode();
        }
        return hashCode21 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackVideoInputToggled(channelId=");
        R.append(this.channelId);
        R.append(", channelType=");
        R.append(this.channelType);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", nonce=");
        R.append(this.nonce);
        R.append(", voiceStateCount=");
        R.append(this.voiceStateCount);
        R.append(", videoStreamCount=");
        R.append(this.videoStreamCount);
        R.append(", videoEnabled=");
        R.append(this.videoEnabled);
        R.append(", gameName=");
        R.append(this.gameName);
        R.append(", gameExeName=");
        R.append(this.gameExeName);
        R.append(", gameId=");
        R.append(this.gameId);
        R.append(", videoInputType=");
        R.append(this.videoInputType);
        R.append(", videoInputResolution=");
        R.append(this.videoInputResolution);
        R.append(", videoInputFrameRate=");
        R.append(this.videoInputFrameRate);
        R.append(", soundshareSession=");
        R.append(this.soundshareSession);
        R.append(", shareGameName=");
        R.append(this.shareGameName);
        R.append(", shareGameId=");
        R.append(this.shareGameId);
        R.append(", gamePlatform=");
        R.append(this.gamePlatform);
        R.append(", source=");
        R.append(this.source);
        R.append(", videoToggleSource=");
        R.append(this.videoToggleSource);
        R.append(", mediaSessionId=");
        R.append(this.mediaSessionId);
        R.append(", enabledInputs=");
        R.append(this.enabledInputs);
        R.append(", previewEnabled=");
        return a.C(R, this.previewEnabled, ")");
    }
}
