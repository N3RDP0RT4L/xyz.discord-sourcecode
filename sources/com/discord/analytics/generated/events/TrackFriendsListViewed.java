package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackFriendsListViewed.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\t\n\u0002\b\n\n\u0002\u0010\r\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR$\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0015\u0010\u0017R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0016\u001a\u0004\b\u0018\u0010\u0017R\u001c\u0010\u0019\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u0005R\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 R\u001b\u0010!\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u0016\u001a\u0004\b\"\u0010\u0017R\u001b\u0010#\u001a\u0004\u0018\u00010\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u001e\u001a\u0004\b$\u0010 R\u001b\u0010%\u001a\u0004\u0018\u00010\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u001e\u001a\u0004\b&\u0010 R\u001b\u0010(\u001a\u0004\u0018\u00010'8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010)\u001a\u0004\b*\u0010+R\u001b\u0010,\u001a\u0004\u0018\u00010\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010\u001e\u001a\u0004\b-\u0010 R\u001b\u0010.\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010\u0016\u001a\u0004\b/\u0010\u0017¨\u00060"}, d2 = {"Lcom/discord/analytics/generated/events/TrackFriendsListViewed;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "isDiscoverableEmail", "Ljava/lang/Boolean;", "()Ljava/lang/Boolean;", "isDiscoverablePhone", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "", "numIncomingRequests", "Ljava/lang/Long;", "getNumIncomingRequests", "()Ljava/lang/Long;", "wasDismissed", "getWasDismissed", "numFriends", "getNumFriends", "numOutgoingRequests", "getNumOutgoingRequests", "", "tabOpened", "Ljava/lang/CharSequence;", "getTabOpened", "()Ljava/lang/CharSequence;", "numSuggestions", "getNumSuggestions", "contactSyncIsEnabled", "getContactSyncIsEnabled", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackFriendsListViewed implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final CharSequence tabOpened = null;
    private final Long numFriends = null;
    private final Long numOutgoingRequests = null;
    private final Long numIncomingRequests = null;
    private final Long numSuggestions = null;
    private final Boolean wasDismissed = null;
    private final Boolean contactSyncIsEnabled = null;
    private final Boolean isDiscoverableEmail = null;
    private final Boolean isDiscoverablePhone = null;
    private final transient String analyticsSchemaTypeName = "friends_list_viewed";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackFriendsListViewed)) {
            return false;
        }
        TrackFriendsListViewed trackFriendsListViewed = (TrackFriendsListViewed) obj;
        return m.areEqual(this.tabOpened, trackFriendsListViewed.tabOpened) && m.areEqual(this.numFriends, trackFriendsListViewed.numFriends) && m.areEqual(this.numOutgoingRequests, trackFriendsListViewed.numOutgoingRequests) && m.areEqual(this.numIncomingRequests, trackFriendsListViewed.numIncomingRequests) && m.areEqual(this.numSuggestions, trackFriendsListViewed.numSuggestions) && m.areEqual(this.wasDismissed, trackFriendsListViewed.wasDismissed) && m.areEqual(this.contactSyncIsEnabled, trackFriendsListViewed.contactSyncIsEnabled) && m.areEqual(this.isDiscoverableEmail, trackFriendsListViewed.isDiscoverableEmail) && m.areEqual(this.isDiscoverablePhone, trackFriendsListViewed.isDiscoverablePhone);
    }

    public int hashCode() {
        CharSequence charSequence = this.tabOpened;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        Long l = this.numFriends;
        int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.numOutgoingRequests;
        int hashCode3 = (hashCode2 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.numIncomingRequests;
        int hashCode4 = (hashCode3 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.numSuggestions;
        int hashCode5 = (hashCode4 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Boolean bool = this.wasDismissed;
        int hashCode6 = (hashCode5 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.contactSyncIsEnabled;
        int hashCode7 = (hashCode6 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Boolean bool3 = this.isDiscoverableEmail;
        int hashCode8 = (hashCode7 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        Boolean bool4 = this.isDiscoverablePhone;
        if (bool4 != null) {
            i = bool4.hashCode();
        }
        return hashCode8 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackFriendsListViewed(tabOpened=");
        R.append(this.tabOpened);
        R.append(", numFriends=");
        R.append(this.numFriends);
        R.append(", numOutgoingRequests=");
        R.append(this.numOutgoingRequests);
        R.append(", numIncomingRequests=");
        R.append(this.numIncomingRequests);
        R.append(", numSuggestions=");
        R.append(this.numSuggestions);
        R.append(", wasDismissed=");
        R.append(this.wasDismissed);
        R.append(", contactSyncIsEnabled=");
        R.append(this.contactSyncIsEnabled);
        R.append(", isDiscoverableEmail=");
        R.append(this.isDiscoverableEmail);
        R.append(", isDiscoverablePhone=");
        return a.C(R, this.isDiscoverablePhone, ")");
    }
}
