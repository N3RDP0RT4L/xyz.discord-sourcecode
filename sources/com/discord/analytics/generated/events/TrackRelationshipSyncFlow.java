package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackLocationMetadata;
import com.discord.analytics.generated.traits.TrackLocationMetadataReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackRelationshipSyncFlow.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\u0007\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\u0010\u0010\u0005\u001a\u00020\u0004HÖ\u0001¢\u0006\u0004\b\u0005\u0010\u0006J\u0010\u0010\b\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\r\u001a\u00020\f2\b\u0010\u000b\u001a\u0004\u0018\u00010\nHÖ\u0003¢\u0006\u0004\b\r\u0010\u000eR$\u0010\u0010\u001a\u0004\u0018\u00010\u000f8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u001b\u0010 \u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#R\u001b\u0010$\u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010!\u001a\u0004\b%\u0010#R\u001b\u0010'\u001a\u0004\u0018\u00010&8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*R\u001b\u0010+\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010\u0017\u001a\u0004\b,\u0010\u0019R\u001b\u0010-\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u0017\u001a\u0004\b.\u0010\u0019R\u001c\u0010/\u001a\u00020\u00048\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b/\u00100\u001a\u0004\b1\u0010\u0006R\u001b\u00102\u001a\u0004\u0018\u00010\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010\u001c\u001a\u0004\b3\u0010\u001eR$\u00105\u001a\u0004\u0018\u0001048\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b5\u00106\u001a\u0004\b7\u00108\"\u0004\b9\u0010:R\u001b\u0010;\u001a\u0004\u0018\u00010\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010\u001c\u001a\u0004\b<\u0010\u001eR\u001b\u0010=\u001a\u0004\u0018\u00010\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b=\u0010\u001c\u001a\u0004\b>\u0010\u001e¨\u0006?"}, d2 = {"Lcom/discord/analytics/generated/events/TrackRelationshipSyncFlow;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackLocationMetadataReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "trackLocationMetadata", "Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "getTrackLocationMetadata", "()Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "setTrackLocationMetadata", "(Lcom/discord/analytics/generated/traits/TrackLocationMetadata;)V", "back", "Ljava/lang/Boolean;", "getBack", "()Ljava/lang/Boolean;", "", "fromStep", "Ljava/lang/CharSequence;", "getFromStep", "()Ljava/lang/CharSequence;", "", "numContactsAdded", "Ljava/lang/Long;", "getNumContactsAdded", "()Ljava/lang/Long;", "numContactsFound", "getNumContactsFound", "", "secondsOnFromStep", "Ljava/lang/Float;", "getSecondsOnFromStep", "()Ljava/lang/Float;", "hasPhoneNumber", "getHasPhoneNumber", "skip", "getSkip", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "flowType", "getFlowType", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "toStep", "getToStep", "mobileContactsPermission", "getMobileContactsPermission", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackRelationshipSyncFlow implements AnalyticsSchema, TrackBaseReceiver, TrackLocationMetadataReceiver {
    private TrackBase trackBase;
    private TrackLocationMetadata trackLocationMetadata;
    private final CharSequence flowType = null;
    private final CharSequence fromStep = null;
    private final CharSequence toStep = null;
    private final Boolean skip = null;
    private final Boolean back = null;
    private final Float secondsOnFromStep = null;
    private final Long numContactsFound = null;
    private final Long numContactsAdded = null;
    private final CharSequence mobileContactsPermission = null;
    private final Boolean hasPhoneNumber = null;
    private final transient String analyticsSchemaTypeName = "relationship_sync_flow";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackRelationshipSyncFlow)) {
            return false;
        }
        TrackRelationshipSyncFlow trackRelationshipSyncFlow = (TrackRelationshipSyncFlow) obj;
        return m.areEqual(this.flowType, trackRelationshipSyncFlow.flowType) && m.areEqual(this.fromStep, trackRelationshipSyncFlow.fromStep) && m.areEqual(this.toStep, trackRelationshipSyncFlow.toStep) && m.areEqual(this.skip, trackRelationshipSyncFlow.skip) && m.areEqual(this.back, trackRelationshipSyncFlow.back) && m.areEqual(this.secondsOnFromStep, trackRelationshipSyncFlow.secondsOnFromStep) && m.areEqual(this.numContactsFound, trackRelationshipSyncFlow.numContactsFound) && m.areEqual(this.numContactsAdded, trackRelationshipSyncFlow.numContactsAdded) && m.areEqual(this.mobileContactsPermission, trackRelationshipSyncFlow.mobileContactsPermission) && m.areEqual(this.hasPhoneNumber, trackRelationshipSyncFlow.hasPhoneNumber);
    }

    public int hashCode() {
        CharSequence charSequence = this.flowType;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        CharSequence charSequence2 = this.fromStep;
        int hashCode2 = (hashCode + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.toStep;
        int hashCode3 = (hashCode2 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Boolean bool = this.skip;
        int hashCode4 = (hashCode3 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.back;
        int hashCode5 = (hashCode4 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Float f = this.secondsOnFromStep;
        int hashCode6 = (hashCode5 + (f != null ? f.hashCode() : 0)) * 31;
        Long l = this.numContactsFound;
        int hashCode7 = (hashCode6 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.numContactsAdded;
        int hashCode8 = (hashCode7 + (l2 != null ? l2.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.mobileContactsPermission;
        int hashCode9 = (hashCode8 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        Boolean bool3 = this.hasPhoneNumber;
        if (bool3 != null) {
            i = bool3.hashCode();
        }
        return hashCode9 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackRelationshipSyncFlow(flowType=");
        R.append(this.flowType);
        R.append(", fromStep=");
        R.append(this.fromStep);
        R.append(", toStep=");
        R.append(this.toStep);
        R.append(", skip=");
        R.append(this.skip);
        R.append(", back=");
        R.append(this.back);
        R.append(", secondsOnFromStep=");
        R.append(this.secondsOnFromStep);
        R.append(", numContactsFound=");
        R.append(this.numContactsFound);
        R.append(", numContactsAdded=");
        R.append(this.numContactsAdded);
        R.append(", mobileContactsPermission=");
        R.append(this.mobileContactsPermission);
        R.append(", hasPhoneNumber=");
        return a.C(R, this.hasPhoneNumber, ")");
    }
}
