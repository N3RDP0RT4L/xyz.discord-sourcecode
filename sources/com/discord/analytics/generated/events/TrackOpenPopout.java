package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackChannel;
import com.discord.analytics.generated.traits.TrackChannelReceiver;
import com.discord.analytics.generated.traits.TrackLocationMetadata;
import com.discord.analytics.generated.traits.TrackLocationMetadataReceiver;
import com.discord.analytics.generated.traits.TrackOverlayClientMetadata;
import com.discord.analytics.generated.traits.TrackOverlayClientMetadataReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackOpenPopout.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b)\n\u0002\u0018\u0002\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u0018\u001a\u0004\b\u001c\u0010\u001aR$\u0010\u001e\u001a\u0004\u0018\u00010\u001d8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010!\"\u0004\b\"\u0010#R\u001b\u0010$\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b$\u0010&R\u001b\u0010'\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010%\u001a\u0004\b(\u0010&R\u001b\u0010)\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010\u0013\u001a\u0004\b*\u0010\u0015R$\u0010,\u001a\u0004\u0018\u00010+8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b,\u0010-\u001a\u0004\b.\u0010/\"\u0004\b0\u00101R$\u00103\u001a\u0004\u0018\u0001028\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b3\u00104\u001a\u0004\b5\u00106\"\u0004\b7\u00108R\u001b\u00109\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010\u0018\u001a\u0004\b:\u0010\u001aR\u001b\u0010;\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010%\u001a\u0004\b<\u0010&R\u001b\u0010=\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b=\u0010\u0018\u001a\u0004\b>\u0010\u001aR\u001b\u0010?\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010%\u001a\u0004\b@\u0010&R\u001b\u0010A\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bA\u0010%\u001a\u0004\bB\u0010&R\u001b\u0010C\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\bC\u0010\u0013\u001a\u0004\bD\u0010\u0015R\u001b\u0010E\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bE\u0010%\u001a\u0004\bF\u0010&R\u001b\u0010G\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\bG\u0010\u0018\u001a\u0004\bH\u0010\u001aR\u001b\u0010I\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\bI\u0010\u0018\u001a\u0004\bJ\u0010\u001aR\u001c\u0010K\u001a\u00020\u00068\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\bK\u0010L\u001a\u0004\bM\u0010\bR\u001b\u0010N\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bN\u0010%\u001a\u0004\bO\u0010&R\u001b\u0010P\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\bP\u0010\u0013\u001a\u0004\bQ\u0010\u0015R\u001b\u0010R\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\bR\u0010\u0013\u001a\u0004\bS\u0010\u0015R\u001b\u0010T\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\bT\u0010\u0013\u001a\u0004\bU\u0010\u0015R\u001b\u0010V\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\bV\u0010\u0013\u001a\u0004\bW\u0010\u0015R\u001b\u0010X\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\bX\u0010\u0018\u001a\u0004\bY\u0010\u001aR\u001b\u0010Z\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\bZ\u0010\u0018\u001a\u0004\b[\u0010\u001aR$\u0010]\u001a\u0004\u0018\u00010\\8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b]\u0010^\u001a\u0004\b_\u0010`\"\u0004\ba\u0010bR\u001b\u0010c\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\bc\u0010%\u001a\u0004\bd\u0010&R\u001b\u0010e\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\be\u0010%\u001a\u0004\be\u0010&R\u001b\u0010f\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\bf\u0010\u0018\u001a\u0004\bg\u0010\u001aR\u001b\u0010h\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\bh\u0010\u0013\u001a\u0004\bi\u0010\u0015¨\u0006j"}, d2 = {"Lcom/discord/analytics/generated/events/TrackOpenPopout;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackChannelReceiver;", "Lcom/discord/analytics/generated/traits/TrackLocationMetadataReceiver;", "Lcom/discord/analytics/generated/traits/TrackOverlayClientMetadataReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "gameName", "Ljava/lang/CharSequence;", "getGameName", "()Ljava/lang/CharSequence;", "", "otherUserId", "Ljava/lang/Long;", "getOtherUserId", "()Ljava/lang/Long;", "stickerPackId", "getStickerPackId", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "isStreaming", "Ljava/lang/Boolean;", "()Ljava/lang/Boolean;", "hasGuildMemberBanner", "getHasGuildMemberBanner", "partyPlatform", "getPartyPlatform", "Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "trackLocationMetadata", "Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "getTrackLocationMetadata", "()Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "setTrackLocationMetadata", "(Lcom/discord/analytics/generated/traits/TrackLocationMetadata;)V", "Lcom/discord/analytics/generated/traits/TrackOverlayClientMetadata;", "trackOverlayClientMetadata", "Lcom/discord/analytics/generated/traits/TrackOverlayClientMetadata;", "getTrackOverlayClientMetadata", "()Lcom/discord/analytics/generated/traits/TrackOverlayClientMetadata;", "setTrackOverlayClientMetadata", "(Lcom/discord/analytics/generated/traits/TrackOverlayClientMetadata;)V", "guildId", "getGuildId", "hasCustomStatus", "getHasCustomStatus", "stickerId", "getStickerId", "hasGuildMemberBio", "getHasGuildMemberBio", "hasImages", "getHasImages", "gamePlatform", "getGamePlatform", "hasGuildMemberAvatar", "getHasGuildMemberAvatar", "emojiId", "getEmojiId", "applicationId", "getApplicationId", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "hasNickname", "getHasNickname", "type", "getType", "profileUserStatus", "getProfileUserStatus", "source", "getSource", "applicationName", "getApplicationName", "skuId", "getSkuId", "gameId", "getGameId", "Lcom/discord/analytics/generated/traits/TrackChannel;", "trackChannel", "Lcom/discord/analytics/generated/traits/TrackChannel;", "getTrackChannel", "()Lcom/discord/analytics/generated/traits/TrackChannel;", "setTrackChannel", "(Lcom/discord/analytics/generated/traits/TrackChannel;)V", "profileHasNitroCustomization", "getProfileHasNitroCustomization", "isFriend", "partyMax", "getPartyMax", "partyId", "getPartyId", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackOpenPopout implements AnalyticsSchema, TrackBaseReceiver, TrackChannelReceiver, TrackLocationMetadataReceiver, TrackOverlayClientMetadataReceiver {
    private TrackBase trackBase;
    private TrackChannel trackChannel;
    private TrackLocationMetadata trackLocationMetadata;
    private TrackOverlayClientMetadata trackOverlayClientMetadata;
    private final CharSequence source = null;
    private final CharSequence type = null;
    private final Long otherUserId = null;
    private final Long applicationId = null;
    private final CharSequence applicationName = null;
    private final Long gameId = null;
    private final Boolean isFriend = null;
    private final Boolean hasImages = null;
    private final Long partyMax = null;
    private final CharSequence partyId = null;
    private final CharSequence partyPlatform = null;
    private final CharSequence gameName = null;
    private final CharSequence gamePlatform = null;
    private final Long skuId = null;
    private final CharSequence profileUserStatus = null;
    private final Boolean isStreaming = null;
    private final Boolean hasCustomStatus = null;
    private final Long guildId = null;
    private final Long emojiId = null;
    private final Long stickerId = null;
    private final Long stickerPackId = null;
    private final Boolean profileHasNitroCustomization = null;
    private final Boolean hasNickname = null;
    private final Boolean hasGuildMemberAvatar = null;
    private final Boolean hasGuildMemberBanner = null;
    private final Boolean hasGuildMemberBio = null;
    private final transient String analyticsSchemaTypeName = "open_popout";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackOpenPopout)) {
            return false;
        }
        TrackOpenPopout trackOpenPopout = (TrackOpenPopout) obj;
        return m.areEqual(this.source, trackOpenPopout.source) && m.areEqual(this.type, trackOpenPopout.type) && m.areEqual(this.otherUserId, trackOpenPopout.otherUserId) && m.areEqual(this.applicationId, trackOpenPopout.applicationId) && m.areEqual(this.applicationName, trackOpenPopout.applicationName) && m.areEqual(this.gameId, trackOpenPopout.gameId) && m.areEqual(this.isFriend, trackOpenPopout.isFriend) && m.areEqual(this.hasImages, trackOpenPopout.hasImages) && m.areEqual(this.partyMax, trackOpenPopout.partyMax) && m.areEqual(this.partyId, trackOpenPopout.partyId) && m.areEqual(this.partyPlatform, trackOpenPopout.partyPlatform) && m.areEqual(this.gameName, trackOpenPopout.gameName) && m.areEqual(this.gamePlatform, trackOpenPopout.gamePlatform) && m.areEqual(this.skuId, trackOpenPopout.skuId) && m.areEqual(this.profileUserStatus, trackOpenPopout.profileUserStatus) && m.areEqual(this.isStreaming, trackOpenPopout.isStreaming) && m.areEqual(this.hasCustomStatus, trackOpenPopout.hasCustomStatus) && m.areEqual(this.guildId, trackOpenPopout.guildId) && m.areEqual(this.emojiId, trackOpenPopout.emojiId) && m.areEqual(this.stickerId, trackOpenPopout.stickerId) && m.areEqual(this.stickerPackId, trackOpenPopout.stickerPackId) && m.areEqual(this.profileHasNitroCustomization, trackOpenPopout.profileHasNitroCustomization) && m.areEqual(this.hasNickname, trackOpenPopout.hasNickname) && m.areEqual(this.hasGuildMemberAvatar, trackOpenPopout.hasGuildMemberAvatar) && m.areEqual(this.hasGuildMemberBanner, trackOpenPopout.hasGuildMemberBanner) && m.areEqual(this.hasGuildMemberBio, trackOpenPopout.hasGuildMemberBio);
    }

    public int hashCode() {
        CharSequence charSequence = this.source;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        CharSequence charSequence2 = this.type;
        int hashCode2 = (hashCode + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Long l = this.otherUserId;
        int hashCode3 = (hashCode2 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.applicationId;
        int hashCode4 = (hashCode3 + (l2 != null ? l2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.applicationName;
        int hashCode5 = (hashCode4 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Long l3 = this.gameId;
        int hashCode6 = (hashCode5 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Boolean bool = this.isFriend;
        int hashCode7 = (hashCode6 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.hasImages;
        int hashCode8 = (hashCode7 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Long l4 = this.partyMax;
        int hashCode9 = (hashCode8 + (l4 != null ? l4.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.partyId;
        int hashCode10 = (hashCode9 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.partyPlatform;
        int hashCode11 = (hashCode10 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        CharSequence charSequence6 = this.gameName;
        int hashCode12 = (hashCode11 + (charSequence6 != null ? charSequence6.hashCode() : 0)) * 31;
        CharSequence charSequence7 = this.gamePlatform;
        int hashCode13 = (hashCode12 + (charSequence7 != null ? charSequence7.hashCode() : 0)) * 31;
        Long l5 = this.skuId;
        int hashCode14 = (hashCode13 + (l5 != null ? l5.hashCode() : 0)) * 31;
        CharSequence charSequence8 = this.profileUserStatus;
        int hashCode15 = (hashCode14 + (charSequence8 != null ? charSequence8.hashCode() : 0)) * 31;
        Boolean bool3 = this.isStreaming;
        int hashCode16 = (hashCode15 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        Boolean bool4 = this.hasCustomStatus;
        int hashCode17 = (hashCode16 + (bool4 != null ? bool4.hashCode() : 0)) * 31;
        Long l6 = this.guildId;
        int hashCode18 = (hashCode17 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.emojiId;
        int hashCode19 = (hashCode18 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Long l8 = this.stickerId;
        int hashCode20 = (hashCode19 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.stickerPackId;
        int hashCode21 = (hashCode20 + (l9 != null ? l9.hashCode() : 0)) * 31;
        Boolean bool5 = this.profileHasNitroCustomization;
        int hashCode22 = (hashCode21 + (bool5 != null ? bool5.hashCode() : 0)) * 31;
        Boolean bool6 = this.hasNickname;
        int hashCode23 = (hashCode22 + (bool6 != null ? bool6.hashCode() : 0)) * 31;
        Boolean bool7 = this.hasGuildMemberAvatar;
        int hashCode24 = (hashCode23 + (bool7 != null ? bool7.hashCode() : 0)) * 31;
        Boolean bool8 = this.hasGuildMemberBanner;
        int hashCode25 = (hashCode24 + (bool8 != null ? bool8.hashCode() : 0)) * 31;
        Boolean bool9 = this.hasGuildMemberBio;
        if (bool9 != null) {
            i = bool9.hashCode();
        }
        return hashCode25 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackOpenPopout(source=");
        R.append(this.source);
        R.append(", type=");
        R.append(this.type);
        R.append(", otherUserId=");
        R.append(this.otherUserId);
        R.append(", applicationId=");
        R.append(this.applicationId);
        R.append(", applicationName=");
        R.append(this.applicationName);
        R.append(", gameId=");
        R.append(this.gameId);
        R.append(", isFriend=");
        R.append(this.isFriend);
        R.append(", hasImages=");
        R.append(this.hasImages);
        R.append(", partyMax=");
        R.append(this.partyMax);
        R.append(", partyId=");
        R.append(this.partyId);
        R.append(", partyPlatform=");
        R.append(this.partyPlatform);
        R.append(", gameName=");
        R.append(this.gameName);
        R.append(", gamePlatform=");
        R.append(this.gamePlatform);
        R.append(", skuId=");
        R.append(this.skuId);
        R.append(", profileUserStatus=");
        R.append(this.profileUserStatus);
        R.append(", isStreaming=");
        R.append(this.isStreaming);
        R.append(", hasCustomStatus=");
        R.append(this.hasCustomStatus);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", emojiId=");
        R.append(this.emojiId);
        R.append(", stickerId=");
        R.append(this.stickerId);
        R.append(", stickerPackId=");
        R.append(this.stickerPackId);
        R.append(", profileHasNitroCustomization=");
        R.append(this.profileHasNitroCustomization);
        R.append(", hasNickname=");
        R.append(this.hasNickname);
        R.append(", hasGuildMemberAvatar=");
        R.append(this.hasGuildMemberAvatar);
        R.append(", hasGuildMemberBanner=");
        R.append(this.hasGuildMemberBanner);
        R.append(", hasGuildMemberBio=");
        return a.C(R, this.hasGuildMemberBio, ")");
    }
}
