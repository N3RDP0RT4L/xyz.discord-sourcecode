package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackSubscriptionResubscribed.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\r\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0010\u001a\u0004\b\u0014\u0010\u0012R!\u0010\u0016\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0010\u001a\u0004\b\u0017\u0010\u0012R!\u0010\u0018\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0010\u001a\u0004\b\u0019\u0010\u0012R$\u0010\u001b\u001a\u0004\u0018\u00010\u001a8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R\u001b\u0010!\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u0010\u001a\u0004\b\"\u0010\u0012R\u001b\u0010#\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u0010\u001a\u0004\b$\u0010\u0012R\u001b\u0010&\u001a\u0004\u0018\u00010%8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010'\u001a\u0004\b(\u0010)R!\u0010*\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`\u00158\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010\u0010\u001a\u0004\b+\u0010\u0012R\u001c\u0010,\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b,\u0010-\u001a\u0004\b.\u0010\u0005R\u001b\u0010/\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u0010\u001a\u0004\b0\u0010\u0012R!\u00101\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`\u00158\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010\u0010\u001a\u0004\b2\u0010\u0012¨\u00063"}, d2 = {"Lcom/discord/analytics/generated/events/TrackSubscriptionResubscribed;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "planId", "Ljava/lang/Long;", "getPlanId", "()Ljava/lang/Long;", "paymentGateway", "getPaymentGateway", "Lcom/discord/primitives/Timestamp;", "createdAt", "getCreatedAt", "currentPeriodStart", "getCurrentPeriodStart", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "subscriptionType", "getSubscriptionType", "trialId", "getTrialId", "", "paymentGatewayPlanId", "Ljava/lang/CharSequence;", "getPaymentGatewayPlanId", "()Ljava/lang/CharSequence;", "trialEndsAt", "getTrialEndsAt", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "subscriptionId", "getSubscriptionId", "currentPeriodEnd", "getCurrentPeriodEnd", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackSubscriptionResubscribed implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Long subscriptionId = null;
    private final Long subscriptionType = null;
    private final CharSequence paymentGatewayPlanId = null;
    private final Long planId = null;
    private final Long paymentGateway = null;
    private final Long createdAt = null;
    private final Long currentPeriodStart = null;
    private final Long currentPeriodEnd = null;
    private final Long trialId = null;
    private final Long trialEndsAt = null;
    private final transient String analyticsSchemaTypeName = "subscription_resubscribed";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackSubscriptionResubscribed)) {
            return false;
        }
        TrackSubscriptionResubscribed trackSubscriptionResubscribed = (TrackSubscriptionResubscribed) obj;
        return m.areEqual(this.subscriptionId, trackSubscriptionResubscribed.subscriptionId) && m.areEqual(this.subscriptionType, trackSubscriptionResubscribed.subscriptionType) && m.areEqual(this.paymentGatewayPlanId, trackSubscriptionResubscribed.paymentGatewayPlanId) && m.areEqual(this.planId, trackSubscriptionResubscribed.planId) && m.areEqual(this.paymentGateway, trackSubscriptionResubscribed.paymentGateway) && m.areEqual(this.createdAt, trackSubscriptionResubscribed.createdAt) && m.areEqual(this.currentPeriodStart, trackSubscriptionResubscribed.currentPeriodStart) && m.areEqual(this.currentPeriodEnd, trackSubscriptionResubscribed.currentPeriodEnd) && m.areEqual(this.trialId, trackSubscriptionResubscribed.trialId) && m.areEqual(this.trialEndsAt, trackSubscriptionResubscribed.trialEndsAt);
    }

    public int hashCode() {
        Long l = this.subscriptionId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.subscriptionType;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        CharSequence charSequence = this.paymentGatewayPlanId;
        int hashCode3 = (hashCode2 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Long l3 = this.planId;
        int hashCode4 = (hashCode3 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.paymentGateway;
        int hashCode5 = (hashCode4 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.createdAt;
        int hashCode6 = (hashCode5 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.currentPeriodStart;
        int hashCode7 = (hashCode6 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.currentPeriodEnd;
        int hashCode8 = (hashCode7 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Long l8 = this.trialId;
        int hashCode9 = (hashCode8 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.trialEndsAt;
        if (l9 != null) {
            i = l9.hashCode();
        }
        return hashCode9 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackSubscriptionResubscribed(subscriptionId=");
        R.append(this.subscriptionId);
        R.append(", subscriptionType=");
        R.append(this.subscriptionType);
        R.append(", paymentGatewayPlanId=");
        R.append(this.paymentGatewayPlanId);
        R.append(", planId=");
        R.append(this.planId);
        R.append(", paymentGateway=");
        R.append(this.paymentGateway);
        R.append(", createdAt=");
        R.append(this.createdAt);
        R.append(", currentPeriodStart=");
        R.append(this.currentPeriodStart);
        R.append(", currentPeriodEnd=");
        R.append(this.currentPeriodEnd);
        R.append(", trialId=");
        R.append(this.trialId);
        R.append(", trialEndsAt=");
        return a.F(R, this.trialEndsAt, ")");
    }
}
