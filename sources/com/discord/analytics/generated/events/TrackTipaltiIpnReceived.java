package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackTipaltiIpnReceived.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\t\n\u0002\b\u0014\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R$\u0010\u0014\u001a\u0004\u0018\u00010\u00138\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R\u001c\u0010\u001a\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u0005R\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u0010\u001a\u0004\b\u001e\u0010\u0012R\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u0010\u001a\u0004\b \u0010\u0012R\u001b\u0010!\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u0010\u001a\u0004\b\"\u0010\u0012R\u001b\u0010$\u001a\u0004\u0018\u00010#8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'R\u001b\u0010(\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010\u0010\u001a\u0004\b)\u0010\u0012R\u001b\u0010*\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010\u0010\u001a\u0004\b+\u0010\u0012R\u001b\u0010,\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010\u0010\u001a\u0004\b-\u0010\u0012R\u001b\u0010.\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010\u0010\u001a\u0004\b/\u0010\u0012R\u001b\u00100\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010\u0010\u001a\u0004\b1\u0010\u0012R\u001b\u00102\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b2\u00103\u001a\u0004\b2\u00104R\u001b\u00105\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010\u0010\u001a\u0004\b6\u0010\u0012¨\u00067"}, d2 = {"Lcom/discord/analytics/generated/events/TrackTipaltiIpnReceived;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "groupPayments", "Ljava/lang/CharSequence;", "getGroupPayments", "()Ljava/lang/CharSequence;", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "type", "getType", "refCode", "getRefCode", "cDate", "getCDate", "", "payeeId", "Ljava/lang/Long;", "getPayeeId", "()Ljava/lang/Long;", "key", "getKey", "submittedDate", "getSubmittedDate", "declinedDate", "getDeclinedDate", "payeeStatus", "getPayeeStatus", "approvalDate", "getApprovalDate", "isPayable", "Ljava/lang/Boolean;", "()Ljava/lang/Boolean;", "complianceReviewStatus", "getComplianceReviewStatus", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackTipaltiIpnReceived implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final CharSequence key = null;
    private final CharSequence cDate = null;
    private final CharSequence type = null;
    private final Long payeeId = null;
    private final CharSequence refCode = null;
    private final Boolean isPayable = null;
    private final CharSequence complianceReviewStatus = null;
    private final CharSequence payeeStatus = null;
    private final CharSequence groupPayments = null;
    private final CharSequence submittedDate = null;
    private final CharSequence approvalDate = null;
    private final CharSequence declinedDate = null;
    private final transient String analyticsSchemaTypeName = "tipalti_ipn_received";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackTipaltiIpnReceived)) {
            return false;
        }
        TrackTipaltiIpnReceived trackTipaltiIpnReceived = (TrackTipaltiIpnReceived) obj;
        return m.areEqual(this.key, trackTipaltiIpnReceived.key) && m.areEqual(this.cDate, trackTipaltiIpnReceived.cDate) && m.areEqual(this.type, trackTipaltiIpnReceived.type) && m.areEqual(this.payeeId, trackTipaltiIpnReceived.payeeId) && m.areEqual(this.refCode, trackTipaltiIpnReceived.refCode) && m.areEqual(this.isPayable, trackTipaltiIpnReceived.isPayable) && m.areEqual(this.complianceReviewStatus, trackTipaltiIpnReceived.complianceReviewStatus) && m.areEqual(this.payeeStatus, trackTipaltiIpnReceived.payeeStatus) && m.areEqual(this.groupPayments, trackTipaltiIpnReceived.groupPayments) && m.areEqual(this.submittedDate, trackTipaltiIpnReceived.submittedDate) && m.areEqual(this.approvalDate, trackTipaltiIpnReceived.approvalDate) && m.areEqual(this.declinedDate, trackTipaltiIpnReceived.declinedDate);
    }

    public int hashCode() {
        CharSequence charSequence = this.key;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        CharSequence charSequence2 = this.cDate;
        int hashCode2 = (hashCode + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.type;
        int hashCode3 = (hashCode2 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Long l = this.payeeId;
        int hashCode4 = (hashCode3 + (l != null ? l.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.refCode;
        int hashCode5 = (hashCode4 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        Boolean bool = this.isPayable;
        int hashCode6 = (hashCode5 + (bool != null ? bool.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.complianceReviewStatus;
        int hashCode7 = (hashCode6 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        CharSequence charSequence6 = this.payeeStatus;
        int hashCode8 = (hashCode7 + (charSequence6 != null ? charSequence6.hashCode() : 0)) * 31;
        CharSequence charSequence7 = this.groupPayments;
        int hashCode9 = (hashCode8 + (charSequence7 != null ? charSequence7.hashCode() : 0)) * 31;
        CharSequence charSequence8 = this.submittedDate;
        int hashCode10 = (hashCode9 + (charSequence8 != null ? charSequence8.hashCode() : 0)) * 31;
        CharSequence charSequence9 = this.approvalDate;
        int hashCode11 = (hashCode10 + (charSequence9 != null ? charSequence9.hashCode() : 0)) * 31;
        CharSequence charSequence10 = this.declinedDate;
        if (charSequence10 != null) {
            i = charSequence10.hashCode();
        }
        return hashCode11 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackTipaltiIpnReceived(key=");
        R.append(this.key);
        R.append(", cDate=");
        R.append(this.cDate);
        R.append(", type=");
        R.append(this.type);
        R.append(", payeeId=");
        R.append(this.payeeId);
        R.append(", refCode=");
        R.append(this.refCode);
        R.append(", isPayable=");
        R.append(this.isPayable);
        R.append(", complianceReviewStatus=");
        R.append(this.complianceReviewStatus);
        R.append(", payeeStatus=");
        R.append(this.payeeStatus);
        R.append(", groupPayments=");
        R.append(this.groupPayments);
        R.append(", submittedDate=");
        R.append(this.submittedDate);
        R.append(", approvalDate=");
        R.append(this.approvalDate);
        R.append(", declinedDate=");
        return a.D(R, this.declinedDate, ")");
    }
}
