package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackGuildLfgGroup;
import com.discord.analytics.generated.traits.TrackGuildLfgGroupReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: TrackRemoveChannelRecipient.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\b\u000f\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\u0010\u0010\u0005\u001a\u00020\u0004HÖ\u0001¢\u0006\u0004\b\u0005\u0010\u0006J\u0010\u0010\b\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\r\u001a\u00020\f2\b\u0010\u000b\u001a\u0004\u0018\u00010\nHÖ\u0003¢\u0006\u0004\b\r\u0010\u000eR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R$\u0010\u0015\u001a\u0004\u0018\u00010\u00148\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR!\u0010\u001c\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001fR\u001b\u0010 \u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0011\u001a\u0004\b!\u0010\u0013R\u001b\u0010\"\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u0011\u001a\u0004\b#\u0010\u0013R\u001c\u0010$\u001a\u00020\u00048\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010\u0006R\u001b\u0010'\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*R\u001b\u0010,\u001a\u0004\u0018\u00010+8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010-\u001a\u0004\b.\u0010/R$\u00101\u001a\u0004\u0018\u0001008\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b1\u00102\u001a\u0004\b3\u00104\"\u0004\b5\u00106¨\u00067"}, d2 = {"Lcom/discord/analytics/generated/events/TrackRemoveChannelRecipient;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackGuildLfgGroupReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "recipientId", "Ljava/lang/Long;", "getRecipientId", "()Ljava/lang/Long;", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "", "recipientIds", "Ljava/util/List;", "getRecipientIds", "()Ljava/util/List;", "channelType", "getChannelType", "channelId", "getChannelId", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "owner", "Ljava/lang/Boolean;", "getOwner", "()Ljava/lang/Boolean;", "", "removeType", "Ljava/lang/CharSequence;", "getRemoveType", "()Ljava/lang/CharSequence;", "Lcom/discord/analytics/generated/traits/TrackGuildLfgGroup;", "trackGuildLfgGroup", "Lcom/discord/analytics/generated/traits/TrackGuildLfgGroup;", "getTrackGuildLfgGroup", "()Lcom/discord/analytics/generated/traits/TrackGuildLfgGroup;", "setTrackGuildLfgGroup", "(Lcom/discord/analytics/generated/traits/TrackGuildLfgGroup;)V", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackRemoveChannelRecipient implements AnalyticsSchema, TrackBaseReceiver, TrackGuildLfgGroupReceiver {
    private TrackBase trackBase;
    private TrackGuildLfgGroup trackGuildLfgGroup;
    private final Long channelId = null;
    private final Long channelType = null;
    private final Boolean owner = null;
    private final Long recipientId = null;
    private final List<Long> recipientIds = null;
    private final CharSequence removeType = null;
    private final transient String analyticsSchemaTypeName = "remove_channel_recipient";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackRemoveChannelRecipient)) {
            return false;
        }
        TrackRemoveChannelRecipient trackRemoveChannelRecipient = (TrackRemoveChannelRecipient) obj;
        return m.areEqual(this.channelId, trackRemoveChannelRecipient.channelId) && m.areEqual(this.channelType, trackRemoveChannelRecipient.channelType) && m.areEqual(this.owner, trackRemoveChannelRecipient.owner) && m.areEqual(this.recipientId, trackRemoveChannelRecipient.recipientId) && m.areEqual(this.recipientIds, trackRemoveChannelRecipient.recipientIds) && m.areEqual(this.removeType, trackRemoveChannelRecipient.removeType);
    }

    public int hashCode() {
        Long l = this.channelId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.channelType;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Boolean bool = this.owner;
        int hashCode3 = (hashCode2 + (bool != null ? bool.hashCode() : 0)) * 31;
        Long l3 = this.recipientId;
        int hashCode4 = (hashCode3 + (l3 != null ? l3.hashCode() : 0)) * 31;
        List<Long> list = this.recipientIds;
        int hashCode5 = (hashCode4 + (list != null ? list.hashCode() : 0)) * 31;
        CharSequence charSequence = this.removeType;
        if (charSequence != null) {
            i = charSequence.hashCode();
        }
        return hashCode5 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackRemoveChannelRecipient(channelId=");
        R.append(this.channelId);
        R.append(", channelType=");
        R.append(this.channelType);
        R.append(", owner=");
        R.append(this.owner);
        R.append(", recipientId=");
        R.append(this.recipientId);
        R.append(", recipientIds=");
        R.append(this.recipientIds);
        R.append(", removeType=");
        return a.D(R, this.removeType, ")");
    }
}
