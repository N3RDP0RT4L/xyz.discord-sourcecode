package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.analytics.generated.traits.TrackNetworkInformation;
import com.discord.analytics.generated.traits.TrackNetworkInformationReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackVideoStreamStarted.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\r\n\u0002\b%\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\u0010\u0010\u0005\u001a\u00020\u0004HÖ\u0001¢\u0006\u0004\b\u0005\u0010\u0006J\u0010\u0010\b\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\r\u001a\u00020\f2\b\u0010\u000b\u001a\u0004\u0018\u00010\nHÖ\u0003¢\u0006\u0004\b\r\u0010\u000eR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u0016\u001a\u0004\b\u001a\u0010\u0018R\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u0011\u001a\u0004\b\u001c\u0010\u0013R\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u0016\u001a\u0004\b\u001e\u0010\u0018R\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u0016\u001a\u0004\b \u0010\u0018R\u001b\u0010!\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u0016\u001a\u0004\b\"\u0010\u0018R\u001b\u0010#\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u0011\u001a\u0004\b$\u0010\u0013R\u001b\u0010%\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u0016\u001a\u0004\b&\u0010\u0018R\u001b\u0010'\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010\u0011\u001a\u0004\b(\u0010\u0013R\u001b\u0010)\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010\u0011\u001a\u0004\b*\u0010\u0013R\u001b\u0010+\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010\u0016\u001a\u0004\b,\u0010\u0018R\u001b\u0010-\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u0011\u001a\u0004\b.\u0010\u0013R\u001b\u0010/\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u0016\u001a\u0004\b0\u0010\u0018R\u001b\u00101\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010\u0016\u001a\u0004\b2\u0010\u0018R\u001b\u00103\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010\u0016\u001a\u0004\b4\u0010\u0018R\u001c\u00105\u001a\u00020\u00048\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b5\u00106\u001a\u0004\b7\u0010\u0006R\u001b\u00108\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010\u0011\u001a\u0004\b9\u0010\u0013R$\u0010;\u001a\u0004\u0018\u00010:8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b;\u0010<\u001a\u0004\b=\u0010>\"\u0004\b?\u0010@R\u001b\u0010A\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\bA\u0010\u0016\u001a\u0004\bB\u0010\u0018R$\u0010D\u001a\u0004\u0018\u00010C8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\bD\u0010E\u001a\u0004\bF\u0010G\"\u0004\bH\u0010IR\u001b\u0010J\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\bJ\u0010\u0016\u001a\u0004\bK\u0010\u0018¨\u0006L"}, d2 = {"Lcom/discord/analytics/generated/events/TrackVideoStreamStarted;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "Lcom/discord/analytics/generated/traits/TrackNetworkInformationReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "shareGameId", "Ljava/lang/Long;", "getShareGameId", "()Ljava/lang/Long;", "", "shareApplicationName", "Ljava/lang/CharSequence;", "getShareApplicationName", "()Ljava/lang/CharSequence;", "guildRegion", "getGuildRegion", "videoInputResolution", "getVideoInputResolution", "soundshareSession", "getSoundshareSession", "parentMediaSessionId", "getParentMediaSessionId", "context", "getContext", "channelId", "getChannelId", "streamRegion", "getStreamRegion", "shareApplicationId", "getShareApplicationId", "videoInputFrameRate", "getVideoInputFrameRate", "participantType", "getParticipantType", "guildId", "getGuildId", "shareApplicationExecutable", "getShareApplicationExecutable", "rtcConnectionId", "getRtcConnectionId", "shareGameName", "getShareGameName", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "senderUserId", "getSenderUserId", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "videoLayout", "getVideoLayout", "Lcom/discord/analytics/generated/traits/TrackNetworkInformation;", "trackNetworkInformation", "Lcom/discord/analytics/generated/traits/TrackNetworkInformation;", "getTrackNetworkInformation", "()Lcom/discord/analytics/generated/traits/TrackNetworkInformation;", "setTrackNetworkInformation", "(Lcom/discord/analytics/generated/traits/TrackNetworkInformation;)V", "mediaSessionId", "getMediaSessionId", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackVideoStreamStarted implements AnalyticsSchema, TrackBaseReceiver, TrackNetworkInformationReceiver {
    private TrackBase trackBase;
    private TrackNetworkInformation trackNetworkInformation;
    private final Long guildId = null;
    private final Long channelId = null;
    private final CharSequence rtcConnectionId = null;
    private final CharSequence mediaSessionId = null;
    private final CharSequence parentMediaSessionId = null;
    private final Long senderUserId = null;
    private final CharSequence context = null;
    private final CharSequence participantType = null;
    private final CharSequence shareApplicationName = null;
    private final Long shareApplicationId = null;
    private final CharSequence shareApplicationExecutable = null;
    private final CharSequence streamRegion = null;
    private final CharSequence guildRegion = null;
    private final CharSequence videoLayout = null;
    private final Long videoInputResolution = null;
    private final Long videoInputFrameRate = null;
    private final CharSequence soundshareSession = null;
    private final CharSequence shareGameName = null;
    private final Long shareGameId = null;
    private final transient String analyticsSchemaTypeName = "video_stream_started";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackVideoStreamStarted)) {
            return false;
        }
        TrackVideoStreamStarted trackVideoStreamStarted = (TrackVideoStreamStarted) obj;
        return m.areEqual(this.guildId, trackVideoStreamStarted.guildId) && m.areEqual(this.channelId, trackVideoStreamStarted.channelId) && m.areEqual(this.rtcConnectionId, trackVideoStreamStarted.rtcConnectionId) && m.areEqual(this.mediaSessionId, trackVideoStreamStarted.mediaSessionId) && m.areEqual(this.parentMediaSessionId, trackVideoStreamStarted.parentMediaSessionId) && m.areEqual(this.senderUserId, trackVideoStreamStarted.senderUserId) && m.areEqual(this.context, trackVideoStreamStarted.context) && m.areEqual(this.participantType, trackVideoStreamStarted.participantType) && m.areEqual(this.shareApplicationName, trackVideoStreamStarted.shareApplicationName) && m.areEqual(this.shareApplicationId, trackVideoStreamStarted.shareApplicationId) && m.areEqual(this.shareApplicationExecutable, trackVideoStreamStarted.shareApplicationExecutable) && m.areEqual(this.streamRegion, trackVideoStreamStarted.streamRegion) && m.areEqual(this.guildRegion, trackVideoStreamStarted.guildRegion) && m.areEqual(this.videoLayout, trackVideoStreamStarted.videoLayout) && m.areEqual(this.videoInputResolution, trackVideoStreamStarted.videoInputResolution) && m.areEqual(this.videoInputFrameRate, trackVideoStreamStarted.videoInputFrameRate) && m.areEqual(this.soundshareSession, trackVideoStreamStarted.soundshareSession) && m.areEqual(this.shareGameName, trackVideoStreamStarted.shareGameName) && m.areEqual(this.shareGameId, trackVideoStreamStarted.shareGameId);
    }

    public int hashCode() {
        Long l = this.guildId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.channelId;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        CharSequence charSequence = this.rtcConnectionId;
        int hashCode3 = (hashCode2 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.mediaSessionId;
        int hashCode4 = (hashCode3 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.parentMediaSessionId;
        int hashCode5 = (hashCode4 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Long l3 = this.senderUserId;
        int hashCode6 = (hashCode5 + (l3 != null ? l3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.context;
        int hashCode7 = (hashCode6 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.participantType;
        int hashCode8 = (hashCode7 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        CharSequence charSequence6 = this.shareApplicationName;
        int hashCode9 = (hashCode8 + (charSequence6 != null ? charSequence6.hashCode() : 0)) * 31;
        Long l4 = this.shareApplicationId;
        int hashCode10 = (hashCode9 + (l4 != null ? l4.hashCode() : 0)) * 31;
        CharSequence charSequence7 = this.shareApplicationExecutable;
        int hashCode11 = (hashCode10 + (charSequence7 != null ? charSequence7.hashCode() : 0)) * 31;
        CharSequence charSequence8 = this.streamRegion;
        int hashCode12 = (hashCode11 + (charSequence8 != null ? charSequence8.hashCode() : 0)) * 31;
        CharSequence charSequence9 = this.guildRegion;
        int hashCode13 = (hashCode12 + (charSequence9 != null ? charSequence9.hashCode() : 0)) * 31;
        CharSequence charSequence10 = this.videoLayout;
        int hashCode14 = (hashCode13 + (charSequence10 != null ? charSequence10.hashCode() : 0)) * 31;
        Long l5 = this.videoInputResolution;
        int hashCode15 = (hashCode14 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.videoInputFrameRate;
        int hashCode16 = (hashCode15 + (l6 != null ? l6.hashCode() : 0)) * 31;
        CharSequence charSequence11 = this.soundshareSession;
        int hashCode17 = (hashCode16 + (charSequence11 != null ? charSequence11.hashCode() : 0)) * 31;
        CharSequence charSequence12 = this.shareGameName;
        int hashCode18 = (hashCode17 + (charSequence12 != null ? charSequence12.hashCode() : 0)) * 31;
        Long l7 = this.shareGameId;
        if (l7 != null) {
            i = l7.hashCode();
        }
        return hashCode18 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackVideoStreamStarted(guildId=");
        R.append(this.guildId);
        R.append(", channelId=");
        R.append(this.channelId);
        R.append(", rtcConnectionId=");
        R.append(this.rtcConnectionId);
        R.append(", mediaSessionId=");
        R.append(this.mediaSessionId);
        R.append(", parentMediaSessionId=");
        R.append(this.parentMediaSessionId);
        R.append(", senderUserId=");
        R.append(this.senderUserId);
        R.append(", context=");
        R.append(this.context);
        R.append(", participantType=");
        R.append(this.participantType);
        R.append(", shareApplicationName=");
        R.append(this.shareApplicationName);
        R.append(", shareApplicationId=");
        R.append(this.shareApplicationId);
        R.append(", shareApplicationExecutable=");
        R.append(this.shareApplicationExecutable);
        R.append(", streamRegion=");
        R.append(this.streamRegion);
        R.append(", guildRegion=");
        R.append(this.guildRegion);
        R.append(", videoLayout=");
        R.append(this.videoLayout);
        R.append(", videoInputResolution=");
        R.append(this.videoInputResolution);
        R.append(", videoInputFrameRate=");
        R.append(this.videoInputFrameRate);
        R.append(", soundshareSession=");
        R.append(this.soundshareSession);
        R.append(", shareGameName=");
        R.append(this.shareGameName);
        R.append(", shareGameId=");
        return a.F(R, this.shareGameId, ")");
    }
}
