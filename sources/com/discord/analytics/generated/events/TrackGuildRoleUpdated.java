package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackGuildRoleUpdated.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\b\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0014\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR$\u0010\u001d\u001a\u0004\u0018\u00010\u001c8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R\u001b\u0010#\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u0019\u001a\u0004\b$\u0010\u001bR\u001c\u0010%\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b%\u0010&\u001a\u0004\b'\u0010\u0005R\u001b\u0010(\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010\u0014\u001a\u0004\b)\u0010\u0016R\u001b\u0010*\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010\u0010\u001a\u0004\b+\u0010\u0012R\u001b\u0010,\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010\u0010\u001a\u0004\b-\u0010\u0012R\u001b\u0010.\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010\u0019\u001a\u0004\b/\u0010\u001b¨\u00060"}, d2 = {"Lcom/discord/analytics/generated/events/TrackGuildRoleUpdated;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "unicodeEmoji", "Ljava/lang/CharSequence;", "getUnicodeEmoji", "()Ljava/lang/CharSequence;", "roleMentionable", "Ljava/lang/Boolean;", "getRoleMentionable", "()Ljava/lang/Boolean;", "", "roleId", "Ljava/lang/Long;", "getRoleId", "()Ljava/lang/Long;", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "guildId", "getGuildId", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "roleHoist", "getRoleHoist", "action", "getAction", "iconHash", "getIconHash", "rolePermissions", "getRolePermissions", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackGuildRoleUpdated implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final CharSequence action = null;
    private final Long guildId = null;
    private final Long roleId = null;
    private final Long rolePermissions = null;
    private final Boolean roleMentionable = null;
    private final Boolean roleHoist = null;
    private final CharSequence iconHash = null;
    private final CharSequence unicodeEmoji = null;
    private final transient String analyticsSchemaTypeName = "guild_role_updated";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackGuildRoleUpdated)) {
            return false;
        }
        TrackGuildRoleUpdated trackGuildRoleUpdated = (TrackGuildRoleUpdated) obj;
        return m.areEqual(this.action, trackGuildRoleUpdated.action) && m.areEqual(this.guildId, trackGuildRoleUpdated.guildId) && m.areEqual(this.roleId, trackGuildRoleUpdated.roleId) && m.areEqual(this.rolePermissions, trackGuildRoleUpdated.rolePermissions) && m.areEqual(this.roleMentionable, trackGuildRoleUpdated.roleMentionable) && m.areEqual(this.roleHoist, trackGuildRoleUpdated.roleHoist) && m.areEqual(this.iconHash, trackGuildRoleUpdated.iconHash) && m.areEqual(this.unicodeEmoji, trackGuildRoleUpdated.unicodeEmoji);
    }

    public int hashCode() {
        CharSequence charSequence = this.action;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        Long l = this.guildId;
        int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.roleId;
        int hashCode3 = (hashCode2 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.rolePermissions;
        int hashCode4 = (hashCode3 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Boolean bool = this.roleMentionable;
        int hashCode5 = (hashCode4 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.roleHoist;
        int hashCode6 = (hashCode5 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.iconHash;
        int hashCode7 = (hashCode6 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.unicodeEmoji;
        if (charSequence3 != null) {
            i = charSequence3.hashCode();
        }
        return hashCode7 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackGuildRoleUpdated(action=");
        R.append(this.action);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", roleId=");
        R.append(this.roleId);
        R.append(", rolePermissions=");
        R.append(this.rolePermissions);
        R.append(", roleMentionable=");
        R.append(this.roleMentionable);
        R.append(", roleHoist=");
        R.append(this.roleHoist);
        R.append(", iconHash=");
        R.append(this.iconHash);
        R.append(", unicodeEmoji=");
        return a.D(R, this.unicodeEmoji, ")");
    }
}
