package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackFriendSyncPerformed.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0017\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0013\u0010\u0015R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0010\u001a\u0004\b\u0017\u0010\u0012R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0010\u001a\u0004\b\u0019\u0010\u0012R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0010\u001a\u0004\b\u001b\u0010\u0012R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u0014\u001a\u0004\b\u001d\u0010\u0015R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u0010\u001a\u0004\b\u001f\u0010\u0012R\u001b\u0010 \u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0010\u001a\u0004\b!\u0010\u0012R\u001b\u0010\"\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u0010\u001a\u0004\b#\u0010\u0012R\u001b\u0010$\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u0010\u001a\u0004\b%\u0010\u0012R\u001b\u0010'\u001a\u0004\u0018\u00010&8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*R$\u0010,\u001a\u0004\u0018\u00010+8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b,\u0010-\u001a\u0004\b.\u0010/\"\u0004\b0\u00101R\u001c\u00102\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b2\u00103\u001a\u0004\b4\u0010\u0005¨\u00065"}, d2 = {"Lcom/discord/analytics/generated/events/TrackFriendSyncPerformed;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "numExternalFriendsAdded", "Ljava/lang/Long;", "getNumExternalFriendsAdded", "()Ljava/lang/Long;", "isBackgroundSync", "Ljava/lang/Boolean;", "()Ljava/lang/Boolean;", "numSuggestionsNonmutual", "getNumSuggestionsNonmutual", "numExternalFriendsRemoved", "getNumExternalFriendsRemoved", "numReverseSuggestionsNonmutual", "getNumReverseSuggestionsNonmutual", "friendSyncEnabled", "getFriendSyncEnabled", "numSuggestionsMutual", "getNumSuggestionsMutual", "numReverseSuggestionsMutual", "getNumReverseSuggestionsMutual", "uploadedContactsLength", "getUploadedContactsLength", "externalFriendListLength", "getExternalFriendListLength", "", "platformType", "Ljava/lang/CharSequence;", "getPlatformType", "()Ljava/lang/CharSequence;", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackFriendSyncPerformed implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final Boolean isBackgroundSync = null;
    private final CharSequence platformType = null;
    private final Long numExternalFriendsAdded = null;
    private final Long numExternalFriendsRemoved = null;
    private final Long numSuggestionsMutual = null;
    private final Long numReverseSuggestionsMutual = null;
    private final Long numSuggestionsNonmutual = null;
    private final Long numReverseSuggestionsNonmutual = null;
    private final Long uploadedContactsLength = null;
    private final Long externalFriendListLength = null;
    private final Boolean friendSyncEnabled = null;
    private final transient String analyticsSchemaTypeName = "friend_sync_performed";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackFriendSyncPerformed)) {
            return false;
        }
        TrackFriendSyncPerformed trackFriendSyncPerformed = (TrackFriendSyncPerformed) obj;
        return m.areEqual(this.isBackgroundSync, trackFriendSyncPerformed.isBackgroundSync) && m.areEqual(this.platformType, trackFriendSyncPerformed.platformType) && m.areEqual(this.numExternalFriendsAdded, trackFriendSyncPerformed.numExternalFriendsAdded) && m.areEqual(this.numExternalFriendsRemoved, trackFriendSyncPerformed.numExternalFriendsRemoved) && m.areEqual(this.numSuggestionsMutual, trackFriendSyncPerformed.numSuggestionsMutual) && m.areEqual(this.numReverseSuggestionsMutual, trackFriendSyncPerformed.numReverseSuggestionsMutual) && m.areEqual(this.numSuggestionsNonmutual, trackFriendSyncPerformed.numSuggestionsNonmutual) && m.areEqual(this.numReverseSuggestionsNonmutual, trackFriendSyncPerformed.numReverseSuggestionsNonmutual) && m.areEqual(this.uploadedContactsLength, trackFriendSyncPerformed.uploadedContactsLength) && m.areEqual(this.externalFriendListLength, trackFriendSyncPerformed.externalFriendListLength) && m.areEqual(this.friendSyncEnabled, trackFriendSyncPerformed.friendSyncEnabled);
    }

    public int hashCode() {
        Boolean bool = this.isBackgroundSync;
        int i = 0;
        int hashCode = (bool != null ? bool.hashCode() : 0) * 31;
        CharSequence charSequence = this.platformType;
        int hashCode2 = (hashCode + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Long l = this.numExternalFriendsAdded;
        int hashCode3 = (hashCode2 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.numExternalFriendsRemoved;
        int hashCode4 = (hashCode3 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.numSuggestionsMutual;
        int hashCode5 = (hashCode4 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.numReverseSuggestionsMutual;
        int hashCode6 = (hashCode5 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.numSuggestionsNonmutual;
        int hashCode7 = (hashCode6 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.numReverseSuggestionsNonmutual;
        int hashCode8 = (hashCode7 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.uploadedContactsLength;
        int hashCode9 = (hashCode8 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Long l8 = this.externalFriendListLength;
        int hashCode10 = (hashCode9 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Boolean bool2 = this.friendSyncEnabled;
        if (bool2 != null) {
            i = bool2.hashCode();
        }
        return hashCode10 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackFriendSyncPerformed(isBackgroundSync=");
        R.append(this.isBackgroundSync);
        R.append(", platformType=");
        R.append(this.platformType);
        R.append(", numExternalFriendsAdded=");
        R.append(this.numExternalFriendsAdded);
        R.append(", numExternalFriendsRemoved=");
        R.append(this.numExternalFriendsRemoved);
        R.append(", numSuggestionsMutual=");
        R.append(this.numSuggestionsMutual);
        R.append(", numReverseSuggestionsMutual=");
        R.append(this.numReverseSuggestionsMutual);
        R.append(", numSuggestionsNonmutual=");
        R.append(this.numSuggestionsNonmutual);
        R.append(", numReverseSuggestionsNonmutual=");
        R.append(this.numReverseSuggestionsNonmutual);
        R.append(", uploadedContactsLength=");
        R.append(this.uploadedContactsLength);
        R.append(", externalFriendListLength=");
        R.append(this.externalFriendListLength);
        R.append(", friendSyncEnabled=");
        return a.C(R, this.friendSyncEnabled, ")");
    }
}
