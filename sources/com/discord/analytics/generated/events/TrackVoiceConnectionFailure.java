package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackVoiceConnectionFailure.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u001d\n\u0002\u0018\u0002\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0014\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0010\u001a\u0004\b\u0019\u0010\u0012R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0010\u001a\u0004\b\u001b\u0010\u0012R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u0010\u001a\u0004\b\u001d\u0010\u0012R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u0010\u001a\u0004\b\u001f\u0010\u0012R\u001b\u0010 \u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0010\u001a\u0004\b!\u0010\u0012R\u001b\u0010\"\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u0015\u001a\u0004\b#\u0010\u0017R\u001b\u0010$\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u0015\u001a\u0004\b%\u0010\u0017R\u001c\u0010&\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b&\u0010'\u001a\u0004\b(\u0010\u0005R\u001b\u0010)\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010\u0015\u001a\u0004\b*\u0010\u0017R\u001b\u0010+\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010\u0010\u001a\u0004\b,\u0010\u0012R\u001b\u0010-\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u0010\u001a\u0004\b.\u0010\u0012R\u001b\u0010/\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u0010\u001a\u0004\b0\u0010\u0012R$\u00102\u001a\u0004\u0018\u0001018\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b2\u00103\u001a\u0004\b4\u00105\"\u0004\b6\u00107R\u001b\u00108\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010\u0010\u001a\u0004\b9\u0010\u0012¨\u0006:"}, d2 = {"Lcom/discord/analytics/generated/events/TrackVoiceConnectionFailure;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "rtcWorkerBackendVersion", "Ljava/lang/CharSequence;", "getRtcWorkerBackendVersion", "()Ljava/lang/CharSequence;", "", "port", "Ljava/lang/Long;", "getPort", "()Ljava/lang/Long;", "hostname", "getHostname", "voiceBackendVersion", "getVoiceBackendVersion", "sessionId", "getSessionId", "cloudflareBestRegion", "getCloudflareBestRegion", "mediaSessionId", "getMediaSessionId", "connectCount", "getConnectCount", "guildId", "getGuildId", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "channelId", "getChannelId", "protocol", "getProtocol", "rtcConnectionId", "getRtcConnectionId", "error", "getError", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "context", "getContext", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackVoiceConnectionFailure implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final CharSequence error = null;
    private final CharSequence hostname = null;
    private final CharSequence cloudflareBestRegion = null;
    private final Long port = null;
    private final CharSequence protocol = null;
    private final CharSequence sessionId = null;
    private final CharSequence mediaSessionId = null;
    private final CharSequence context = null;
    private final CharSequence rtcConnectionId = null;
    private final Long channelId = null;
    private final Long guildId = null;
    private final Long connectCount = null;
    private final CharSequence voiceBackendVersion = null;
    private final CharSequence rtcWorkerBackendVersion = null;
    private final transient String analyticsSchemaTypeName = "voice_connection_failure";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackVoiceConnectionFailure)) {
            return false;
        }
        TrackVoiceConnectionFailure trackVoiceConnectionFailure = (TrackVoiceConnectionFailure) obj;
        return m.areEqual(this.error, trackVoiceConnectionFailure.error) && m.areEqual(this.hostname, trackVoiceConnectionFailure.hostname) && m.areEqual(this.cloudflareBestRegion, trackVoiceConnectionFailure.cloudflareBestRegion) && m.areEqual(this.port, trackVoiceConnectionFailure.port) && m.areEqual(this.protocol, trackVoiceConnectionFailure.protocol) && m.areEqual(this.sessionId, trackVoiceConnectionFailure.sessionId) && m.areEqual(this.mediaSessionId, trackVoiceConnectionFailure.mediaSessionId) && m.areEqual(this.context, trackVoiceConnectionFailure.context) && m.areEqual(this.rtcConnectionId, trackVoiceConnectionFailure.rtcConnectionId) && m.areEqual(this.channelId, trackVoiceConnectionFailure.channelId) && m.areEqual(this.guildId, trackVoiceConnectionFailure.guildId) && m.areEqual(this.connectCount, trackVoiceConnectionFailure.connectCount) && m.areEqual(this.voiceBackendVersion, trackVoiceConnectionFailure.voiceBackendVersion) && m.areEqual(this.rtcWorkerBackendVersion, trackVoiceConnectionFailure.rtcWorkerBackendVersion);
    }

    public int hashCode() {
        CharSequence charSequence = this.error;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        CharSequence charSequence2 = this.hostname;
        int hashCode2 = (hashCode + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.cloudflareBestRegion;
        int hashCode3 = (hashCode2 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Long l = this.port;
        int hashCode4 = (hashCode3 + (l != null ? l.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.protocol;
        int hashCode5 = (hashCode4 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.sessionId;
        int hashCode6 = (hashCode5 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        CharSequence charSequence6 = this.mediaSessionId;
        int hashCode7 = (hashCode6 + (charSequence6 != null ? charSequence6.hashCode() : 0)) * 31;
        CharSequence charSequence7 = this.context;
        int hashCode8 = (hashCode7 + (charSequence7 != null ? charSequence7.hashCode() : 0)) * 31;
        CharSequence charSequence8 = this.rtcConnectionId;
        int hashCode9 = (hashCode8 + (charSequence8 != null ? charSequence8.hashCode() : 0)) * 31;
        Long l2 = this.channelId;
        int hashCode10 = (hashCode9 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.guildId;
        int hashCode11 = (hashCode10 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.connectCount;
        int hashCode12 = (hashCode11 + (l4 != null ? l4.hashCode() : 0)) * 31;
        CharSequence charSequence9 = this.voiceBackendVersion;
        int hashCode13 = (hashCode12 + (charSequence9 != null ? charSequence9.hashCode() : 0)) * 31;
        CharSequence charSequence10 = this.rtcWorkerBackendVersion;
        if (charSequence10 != null) {
            i = charSequence10.hashCode();
        }
        return hashCode13 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackVoiceConnectionFailure(error=");
        R.append(this.error);
        R.append(", hostname=");
        R.append(this.hostname);
        R.append(", cloudflareBestRegion=");
        R.append(this.cloudflareBestRegion);
        R.append(", port=");
        R.append(this.port);
        R.append(", protocol=");
        R.append(this.protocol);
        R.append(", sessionId=");
        R.append(this.sessionId);
        R.append(", mediaSessionId=");
        R.append(this.mediaSessionId);
        R.append(", context=");
        R.append(this.context);
        R.append(", rtcConnectionId=");
        R.append(this.rtcConnectionId);
        R.append(", channelId=");
        R.append(this.channelId);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", connectCount=");
        R.append(this.connectCount);
        R.append(", voiceBackendVersion=");
        R.append(this.voiceBackendVersion);
        R.append(", rtcWorkerBackendVersion=");
        return a.D(R, this.rtcWorkerBackendVersion, ")");
    }
}
