package com.discord.analytics.generated.events;

import b.d.b.a.a;
import com.discord.analytics.generated.traits.TrackBase;
import com.discord.analytics.generated.traits.TrackBaseReceiver;
import com.discord.api.science.AnalyticsSchema;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackNuoGuildInfo.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\r\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0004\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u001a\u0010\f\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tHÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0010\u001a\u0004\b\u0014\u0010\u0012R\u001c\u0010\u0015\u001a\u00020\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0005R$\u0010\u0019\u001a\u0004\u0018\u00010\u00188\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001c\"\u0004\b\u001d\u0010\u001eR\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"R\u001b\u0010$\u001a\u0004\u0018\u00010#8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'R\u001b\u0010(\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010\u0010\u001a\u0004\b)\u0010\u0012R\u001b\u0010*\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010 \u001a\u0004\b+\u0010\"R\u001b\u0010,\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010\u0010\u001a\u0004\b-\u0010\u0012¨\u0006."}, d2 = {"Lcom/discord/analytics/generated/events/TrackNuoGuildInfo;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackBaseReceiver;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "invitedGuildId", "Ljava/lang/Long;", "getInvitedGuildId", "()Ljava/lang/Long;", "channelType", "getChannelType", "analyticsSchemaTypeName", "Ljava/lang/String;", "b", "Lcom/discord/analytics/generated/traits/TrackBase;", "trackBase", "Lcom/discord/analytics/generated/traits/TrackBase;", "getTrackBase", "()Lcom/discord/analytics/generated/traits/TrackBase;", "setTrackBase", "(Lcom/discord/analytics/generated/traits/TrackBase;)V", "verificationLevelEnabled", "Ljava/lang/Boolean;", "getVerificationLevelEnabled", "()Ljava/lang/Boolean;", "", "evaluatedStep", "Ljava/lang/CharSequence;", "getEvaluatedStep", "()Ljava/lang/CharSequence;", "guildCount", "getGuildCount", "hasSplash", "getHasSplash", "memberCount", "getMemberCount", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackNuoGuildInfo implements AnalyticsSchema, TrackBaseReceiver {
    private TrackBase trackBase;
    private final CharSequence evaluatedStep = null;
    private final Long invitedGuildId = null;
    private final Long guildCount = null;
    private final Long memberCount = null;
    private final Boolean hasSplash = null;
    private final Long channelType = null;
    private final Boolean verificationLevelEnabled = null;
    private final transient String analyticsSchemaTypeName = "nuo_guild_info";

    @Override // com.discord.api.science.AnalyticsSchema
    public String b() {
        return this.analyticsSchemaTypeName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackNuoGuildInfo)) {
            return false;
        }
        TrackNuoGuildInfo trackNuoGuildInfo = (TrackNuoGuildInfo) obj;
        return m.areEqual(this.evaluatedStep, trackNuoGuildInfo.evaluatedStep) && m.areEqual(this.invitedGuildId, trackNuoGuildInfo.invitedGuildId) && m.areEqual(this.guildCount, trackNuoGuildInfo.guildCount) && m.areEqual(this.memberCount, trackNuoGuildInfo.memberCount) && m.areEqual(this.hasSplash, trackNuoGuildInfo.hasSplash) && m.areEqual(this.channelType, trackNuoGuildInfo.channelType) && m.areEqual(this.verificationLevelEnabled, trackNuoGuildInfo.verificationLevelEnabled);
    }

    public int hashCode() {
        CharSequence charSequence = this.evaluatedStep;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        Long l = this.invitedGuildId;
        int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.guildCount;
        int hashCode3 = (hashCode2 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.memberCount;
        int hashCode4 = (hashCode3 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Boolean bool = this.hasSplash;
        int hashCode5 = (hashCode4 + (bool != null ? bool.hashCode() : 0)) * 31;
        Long l4 = this.channelType;
        int hashCode6 = (hashCode5 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Boolean bool2 = this.verificationLevelEnabled;
        if (bool2 != null) {
            i = bool2.hashCode();
        }
        return hashCode6 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackNuoGuildInfo(evaluatedStep=");
        R.append(this.evaluatedStep);
        R.append(", invitedGuildId=");
        R.append(this.invitedGuildId);
        R.append(", guildCount=");
        R.append(this.guildCount);
        R.append(", memberCount=");
        R.append(this.memberCount);
        R.append(", hasSplash=");
        R.append(this.hasSplash);
        R.append(", channelType=");
        R.append(this.channelType);
        R.append(", verificationLevelEnabled=");
        return a.C(R, this.verificationLevelEnabled, ")");
    }
}
