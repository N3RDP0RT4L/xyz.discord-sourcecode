package com.discord.analytics.generated.traits;

import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackChannel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0011\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\r\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0011\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u000e\u001a\u0004\b\u0012\u0010\u0010R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u000e\u001a\u0004\b\u0014\u0010\u0010R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u000e\u001a\u0004\b\u0016\u0010\u0010R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u001b\u0010\u001b\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u000e\u001a\u0004\b\u001c\u0010\u0010¨\u0006\u001d"}, d2 = {"Lcom/discord/analytics/generated/traits/TrackChannel;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "channelSizeTotal", "Ljava/lang/Long;", "getChannelSizeTotal", "()Ljava/lang/Long;", "channelType", "getChannelType", "channelId", "getChannelId", "channelMemberPerms", "getChannelMemberPerms", "channelHidden", "Ljava/lang/Boolean;", "getChannelHidden", "()Ljava/lang/Boolean;", "channelSizeOnline", "getChannelSizeOnline", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackChannel {
    private final Long channelId = null;
    private final Long channelType = null;
    private final Long channelSizeTotal = null;
    private final Long channelSizeOnline = null;
    private final Long channelMemberPerms = null;
    private final Boolean channelHidden = null;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackChannel)) {
            return false;
        }
        TrackChannel trackChannel = (TrackChannel) obj;
        return m.areEqual(this.channelId, trackChannel.channelId) && m.areEqual(this.channelType, trackChannel.channelType) && m.areEqual(this.channelSizeTotal, trackChannel.channelSizeTotal) && m.areEqual(this.channelSizeOnline, trackChannel.channelSizeOnline) && m.areEqual(this.channelMemberPerms, trackChannel.channelMemberPerms) && m.areEqual(this.channelHidden, trackChannel.channelHidden);
    }

    public int hashCode() {
        Long l = this.channelId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.channelType;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.channelSizeTotal;
        int hashCode3 = (hashCode2 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.channelSizeOnline;
        int hashCode4 = (hashCode3 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.channelMemberPerms;
        int hashCode5 = (hashCode4 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Boolean bool = this.channelHidden;
        if (bool != null) {
            i = bool.hashCode();
        }
        return hashCode5 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackChannel(channelId=");
        R.append(this.channelId);
        R.append(", channelType=");
        R.append(this.channelType);
        R.append(", channelSizeTotal=");
        R.append(this.channelSizeTotal);
        R.append(", channelSizeOnline=");
        R.append(this.channelSizeOnline);
        R.append(", channelMemberPerms=");
        R.append(this.channelMemberPerms);
        R.append(", channelHidden=");
        return a.C(R, this.channelHidden, ")");
    }
}
