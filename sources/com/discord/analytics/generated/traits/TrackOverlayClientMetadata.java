package com.discord.analytics.generated.traits;

import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackOverlayClientMetadata.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\r\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0013\u001a\u0004\b\u0017\u0010\u0015¨\u0006\u0018"}, d2 = {"Lcom/discord/analytics/generated/traits/TrackOverlayClientMetadata;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "overlayGameName", "Ljava/lang/CharSequence;", "getOverlayGameName", "()Ljava/lang/CharSequence;", "", "overlayAppId", "Ljava/lang/Long;", "getOverlayAppId", "()Ljava/lang/Long;", "overlayGameId", "getOverlayGameId", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackOverlayClientMetadata {
    private final Long overlayGameId = null;
    private final CharSequence overlayGameName = null;
    private final Long overlayAppId = null;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackOverlayClientMetadata)) {
            return false;
        }
        TrackOverlayClientMetadata trackOverlayClientMetadata = (TrackOverlayClientMetadata) obj;
        return m.areEqual(this.overlayGameId, trackOverlayClientMetadata.overlayGameId) && m.areEqual(this.overlayGameName, trackOverlayClientMetadata.overlayGameName) && m.areEqual(this.overlayAppId, trackOverlayClientMetadata.overlayAppId);
    }

    public int hashCode() {
        Long l = this.overlayGameId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        CharSequence charSequence = this.overlayGameName;
        int hashCode2 = (hashCode + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Long l2 = this.overlayAppId;
        if (l2 != null) {
            i = l2.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackOverlayClientMetadata(overlayGameId=");
        R.append(this.overlayGameId);
        R.append(", overlayGameName=");
        R.append(this.overlayGameName);
        R.append(", overlayAppId=");
        return a.F(R, this.overlayAppId, ")");
    }
}
