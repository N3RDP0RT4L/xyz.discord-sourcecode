package com.discord.analytics.generated.traits;

import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackLfgGroup.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\r\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\r\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0013\u001a\u0004\b\u0017\u0010\u0015R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0013\u001a\u0004\b\u0019\u0010\u0015R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u000e\u001a\u0004\b\u001b\u0010\u0010R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u000e\u001a\u0004\b\u001d\u0010\u0010R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u000e\u001a\u0004\b\u001f\u0010\u0010¨\u0006 "}, d2 = {"Lcom/discord/analytics/generated/traits/TrackLfgGroup;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "freeSize", "Ljava/lang/Long;", "getFreeSize", "()Ljava/lang/Long;", "", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "Ljava/lang/CharSequence;", "getDescription", "()Ljava/lang/CharSequence;", "gameName", "getGameName", "title", "getTitle", "gameId", "getGameId", "ownerId", "getOwnerId", "totalSize", "getTotalSize", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackLfgGroup {
    private final CharSequence gameName = null;
    private final Long gameId = null;
    private final Long totalSize = null;
    private final Long freeSize = null;
    private final Long ownerId = null;
    private final CharSequence title = null;
    private final CharSequence description = null;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackLfgGroup)) {
            return false;
        }
        TrackLfgGroup trackLfgGroup = (TrackLfgGroup) obj;
        return m.areEqual(this.gameName, trackLfgGroup.gameName) && m.areEqual(this.gameId, trackLfgGroup.gameId) && m.areEqual(this.totalSize, trackLfgGroup.totalSize) && m.areEqual(this.freeSize, trackLfgGroup.freeSize) && m.areEqual(this.ownerId, trackLfgGroup.ownerId) && m.areEqual(this.title, trackLfgGroup.title) && m.areEqual(this.description, trackLfgGroup.description);
    }

    public int hashCode() {
        CharSequence charSequence = this.gameName;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        Long l = this.gameId;
        int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.totalSize;
        int hashCode3 = (hashCode2 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.freeSize;
        int hashCode4 = (hashCode3 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.ownerId;
        int hashCode5 = (hashCode4 + (l4 != null ? l4.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.title;
        int hashCode6 = (hashCode5 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.description;
        if (charSequence3 != null) {
            i = charSequence3.hashCode();
        }
        return hashCode6 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackLfgGroup(gameName=");
        R.append(this.gameName);
        R.append(", gameId=");
        R.append(this.gameId);
        R.append(", totalSize=");
        R.append(this.totalSize);
        R.append(", freeSize=");
        R.append(this.freeSize);
        R.append(", ownerId=");
        R.append(this.ownerId);
        R.append(", title=");
        R.append(this.title);
        R.append(", description=");
        return a.D(R, this.description, ")");
    }
}
