package com.discord.analytics.generated.traits;

import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackImpressionMetadata.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\r\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0011\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u000e\u001a\u0004\b\u0012\u0010\u0010R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u000e\u001a\u0004\b\u0014\u0010\u0010R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u000e\u001a\u0004\b\u0016\u0010\u0010¨\u0006\u0017"}, d2 = {"Lcom/discord/analytics/generated/traits/TrackImpressionMetadata;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "impressionType", "Ljava/lang/CharSequence;", "getImpressionType", "()Ljava/lang/CharSequence;", "impressionName", "getImpressionName", "impressionGroup", "getImpressionGroup", "sequenceId", "getSequenceId", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackImpressionMetadata {
    private final CharSequence impressionGroup;
    private final CharSequence impressionName;
    private final CharSequence impressionType;
    private final CharSequence sequenceId;

    public TrackImpressionMetadata() {
        this(null, null, null, null, 15);
    }

    public TrackImpressionMetadata(CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, CharSequence charSequence4, int i) {
        int i2 = i & 1;
        int i3 = i & 2;
        int i4 = i & 4;
        charSequence4 = (i & 8) != 0 ? null : charSequence4;
        this.sequenceId = null;
        this.impressionName = null;
        this.impressionType = null;
        this.impressionGroup = charSequence4;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackImpressionMetadata)) {
            return false;
        }
        TrackImpressionMetadata trackImpressionMetadata = (TrackImpressionMetadata) obj;
        return m.areEqual(this.sequenceId, trackImpressionMetadata.sequenceId) && m.areEqual(this.impressionName, trackImpressionMetadata.impressionName) && m.areEqual(this.impressionType, trackImpressionMetadata.impressionType) && m.areEqual(this.impressionGroup, trackImpressionMetadata.impressionGroup);
    }

    public int hashCode() {
        CharSequence charSequence = this.sequenceId;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        CharSequence charSequence2 = this.impressionName;
        int hashCode2 = (hashCode + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.impressionType;
        int hashCode3 = (hashCode2 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.impressionGroup;
        if (charSequence4 != null) {
            i = charSequence4.hashCode();
        }
        return hashCode3 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackImpressionMetadata(sequenceId=");
        R.append(this.sequenceId);
        R.append(", impressionName=");
        R.append(this.impressionName);
        R.append(", impressionType=");
        R.append(this.impressionType);
        R.append(", impressionGroup=");
        return a.D(R, this.impressionGroup, ")");
    }
}
