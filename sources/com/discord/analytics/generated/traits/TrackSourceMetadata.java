package com.discord.analytics.generated.traits;

import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackSourceMetadata.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\r\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\r\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0013\u001a\u0004\b\u0017\u0010\u0015R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0013\u001a\u0004\b\u0019\u0010\u0015R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0013\u001a\u0004\b\u001b\u0010\u0015¨\u0006\u001c"}, d2 = {"Lcom/discord/analytics/generated/traits/TrackSourceMetadata;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "sourcePromotionId", "Ljava/lang/Long;", "getSourcePromotionId", "()Ljava/lang/Long;", "", "sourceObjectType", "Ljava/lang/CharSequence;", "getSourceObjectType", "()Ljava/lang/CharSequence;", "sourcePage", "getSourcePage", "sourceObject", "getSourceObject", "sourceSection", "getSourceSection", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackSourceMetadata {
    private final CharSequence sourceObject;
    private final CharSequence sourceObjectType;
    private final CharSequence sourcePage;
    private final Long sourcePromotionId;
    private final CharSequence sourceSection;

    public TrackSourceMetadata() {
        this(null, null, null, null, null, 31);
    }

    public TrackSourceMetadata(CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, CharSequence charSequence4, Long l, int i) {
        int i2 = i & 2;
        int i3 = i & 4;
        int i4 = i & 8;
        int i5 = i & 16;
        this.sourcePage = (i & 1) != 0 ? null : charSequence;
        this.sourceSection = null;
        this.sourceObject = null;
        this.sourceObjectType = null;
        this.sourcePromotionId = null;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackSourceMetadata)) {
            return false;
        }
        TrackSourceMetadata trackSourceMetadata = (TrackSourceMetadata) obj;
        return m.areEqual(this.sourcePage, trackSourceMetadata.sourcePage) && m.areEqual(this.sourceSection, trackSourceMetadata.sourceSection) && m.areEqual(this.sourceObject, trackSourceMetadata.sourceObject) && m.areEqual(this.sourceObjectType, trackSourceMetadata.sourceObjectType) && m.areEqual(this.sourcePromotionId, trackSourceMetadata.sourcePromotionId);
    }

    public int hashCode() {
        CharSequence charSequence = this.sourcePage;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        CharSequence charSequence2 = this.sourceSection;
        int hashCode2 = (hashCode + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.sourceObject;
        int hashCode3 = (hashCode2 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.sourceObjectType;
        int hashCode4 = (hashCode3 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        Long l = this.sourcePromotionId;
        if (l != null) {
            i = l.hashCode();
        }
        return hashCode4 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackSourceMetadata(sourcePage=");
        R.append(this.sourcePage);
        R.append(", sourceSection=");
        R.append(this.sourceSection);
        R.append(", sourceObject=");
        R.append(this.sourceObject);
        R.append(", sourceObjectType=");
        R.append(this.sourceObjectType);
        R.append(", sourcePromotionId=");
        return a.F(R, this.sourcePromotionId, ")");
    }
}
