package com.discord.analytics.generated.traits;

import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackActivityInternalMetadata.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0011\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\r\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u000e\u001a\u0004\b\u0017\u0010\u0010R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0013\u001a\u0004\b\u0019\u0010\u0015R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u000e\u001a\u0004\b\u001b\u0010\u0010R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u000e\u001a\u0004\b\u001d\u0010\u0010R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u000e\u001a\u0004\b\u001f\u0010\u0010R\u001b\u0010 \u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0013\u001a\u0004\b!\u0010\u0015¨\u0006\""}, d2 = {"Lcom/discord/analytics/generated/traits/TrackActivityInternalMetadata;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "activityVersion", "Ljava/lang/CharSequence;", "getActivityVersion", "()Ljava/lang/CharSequence;", "", "numConcurrentUsers", "Ljava/lang/Long;", "getNumConcurrentUsers", "()Ljava/lang/Long;", "participationMode", "getParticipationMode", "activityApplicationId", "getActivityApplicationId", "groupSessionId", "getGroupSessionId", "activityInstanceId", "getActivityInstanceId", "userSessionId", "getUserSessionId", "activityGuildId", "getActivityGuildId", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackActivityInternalMetadata {
    private final Long activityApplicationId = null;
    private final CharSequence groupSessionId = null;
    private final CharSequence userSessionId = null;
    private final CharSequence activityInstanceId = null;
    private final CharSequence activityVersion = null;
    private final Long numConcurrentUsers = null;
    private final Long activityGuildId = null;
    private final CharSequence participationMode = null;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackActivityInternalMetadata)) {
            return false;
        }
        TrackActivityInternalMetadata trackActivityInternalMetadata = (TrackActivityInternalMetadata) obj;
        return m.areEqual(this.activityApplicationId, trackActivityInternalMetadata.activityApplicationId) && m.areEqual(this.groupSessionId, trackActivityInternalMetadata.groupSessionId) && m.areEqual(this.userSessionId, trackActivityInternalMetadata.userSessionId) && m.areEqual(this.activityInstanceId, trackActivityInternalMetadata.activityInstanceId) && m.areEqual(this.activityVersion, trackActivityInternalMetadata.activityVersion) && m.areEqual(this.numConcurrentUsers, trackActivityInternalMetadata.numConcurrentUsers) && m.areEqual(this.activityGuildId, trackActivityInternalMetadata.activityGuildId) && m.areEqual(this.participationMode, trackActivityInternalMetadata.participationMode);
    }

    public int hashCode() {
        Long l = this.activityApplicationId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        CharSequence charSequence = this.groupSessionId;
        int hashCode2 = (hashCode + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.userSessionId;
        int hashCode3 = (hashCode2 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.activityInstanceId;
        int hashCode4 = (hashCode3 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.activityVersion;
        int hashCode5 = (hashCode4 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        Long l2 = this.numConcurrentUsers;
        int hashCode6 = (hashCode5 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.activityGuildId;
        int hashCode7 = (hashCode6 + (l3 != null ? l3.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.participationMode;
        if (charSequence5 != null) {
            i = charSequence5.hashCode();
        }
        return hashCode7 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackActivityInternalMetadata(activityApplicationId=");
        R.append(this.activityApplicationId);
        R.append(", groupSessionId=");
        R.append(this.groupSessionId);
        R.append(", userSessionId=");
        R.append(this.userSessionId);
        R.append(", activityInstanceId=");
        R.append(this.activityInstanceId);
        R.append(", activityVersion=");
        R.append(this.activityVersion);
        R.append(", numConcurrentUsers=");
        R.append(this.numConcurrentUsers);
        R.append(", activityGuildId=");
        R.append(this.activityGuildId);
        R.append(", participationMode=");
        return a.D(R, this.participationMode, ")");
    }
}
