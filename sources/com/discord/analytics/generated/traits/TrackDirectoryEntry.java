package com.discord.analytics.generated.traits;

import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackDirectoryEntry.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\r\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\r\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0011\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u000e\u001a\u0004\b\u0012\u0010\u0010R!\u0010\u0014\u001a\n\u0018\u00010\fj\u0004\u0018\u0001`\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u000e\u001a\u0004\b\u0015\u0010\u0010R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u000e\u001a\u0004\b\u0017\u0010\u0010R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u000e\u001a\u0004\b\u0019\u0010\u0010R\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u001b\u0010\u001f\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u000e\u001a\u0004\b \u0010\u0010R\u001b\u0010!\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u000e\u001a\u0004\b\"\u0010\u0010¨\u0006#"}, d2 = {"Lcom/discord/analytics/generated/traits/TrackDirectoryEntry;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "entityId", "Ljava/lang/Long;", "getEntityId", "()Ljava/lang/Long;", "authorId", "getAuthorId", "Lcom/discord/primitives/Timestamp;", "createdAt", "getCreatedAt", "directoryChannelId", "getDirectoryChannelId", "directoryGuildId", "getDirectoryGuildId", "", "entryDescription", "Ljava/lang/CharSequence;", "getEntryDescription", "()Ljava/lang/CharSequence;", "entityType", "getEntityType", "primaryCategoryId", "getPrimaryCategoryId", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackDirectoryEntry {
    private final Long directoryChannelId = null;
    private final Long directoryGuildId = null;
    private final Long entityId = null;
    private final Long authorId = null;
    private final Long entityType = null;
    private final Long createdAt = null;
    private final Long primaryCategoryId = null;
    private final CharSequence entryDescription = null;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackDirectoryEntry)) {
            return false;
        }
        TrackDirectoryEntry trackDirectoryEntry = (TrackDirectoryEntry) obj;
        return m.areEqual(this.directoryChannelId, trackDirectoryEntry.directoryChannelId) && m.areEqual(this.directoryGuildId, trackDirectoryEntry.directoryGuildId) && m.areEqual(this.entityId, trackDirectoryEntry.entityId) && m.areEqual(this.authorId, trackDirectoryEntry.authorId) && m.areEqual(this.entityType, trackDirectoryEntry.entityType) && m.areEqual(this.createdAt, trackDirectoryEntry.createdAt) && m.areEqual(this.primaryCategoryId, trackDirectoryEntry.primaryCategoryId) && m.areEqual(this.entryDescription, trackDirectoryEntry.entryDescription);
    }

    public int hashCode() {
        Long l = this.directoryChannelId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.directoryGuildId;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.entityId;
        int hashCode3 = (hashCode2 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.authorId;
        int hashCode4 = (hashCode3 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.entityType;
        int hashCode5 = (hashCode4 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.createdAt;
        int hashCode6 = (hashCode5 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.primaryCategoryId;
        int hashCode7 = (hashCode6 + (l7 != null ? l7.hashCode() : 0)) * 31;
        CharSequence charSequence = this.entryDescription;
        if (charSequence != null) {
            i = charSequence.hashCode();
        }
        return hashCode7 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackDirectoryEntry(directoryChannelId=");
        R.append(this.directoryChannelId);
        R.append(", directoryGuildId=");
        R.append(this.directoryGuildId);
        R.append(", entityId=");
        R.append(this.entityId);
        R.append(", authorId=");
        R.append(this.authorId);
        R.append(", entityType=");
        R.append(this.entityType);
        R.append(", createdAt=");
        R.append(this.createdAt);
        R.append(", primaryCategoryId=");
        R.append(this.primaryCategoryId);
        R.append(", entryDescription=");
        return a.D(R, this.entryDescription, ")");
    }
}
