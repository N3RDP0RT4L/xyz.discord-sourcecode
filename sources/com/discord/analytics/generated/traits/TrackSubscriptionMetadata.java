package com.discord.analytics.generated.traits;

import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackSubscriptionMetadata.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\r\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\r\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u000e\u001a\u0004\b\u0017\u0010\u0010R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0013\u001a\u0004\b\u0019\u0010\u0015¨\u0006\u001a"}, d2 = {"Lcom/discord/analytics/generated/traits/TrackSubscriptionMetadata;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "subscriptionPlanId", "Ljava/lang/Long;", "getSubscriptionPlanId", "()Ljava/lang/Long;", "", "subscriptionPlanGatewayPlanId", "Ljava/lang/CharSequence;", "getSubscriptionPlanGatewayPlanId", "()Ljava/lang/CharSequence;", "subscriptionType", "getSubscriptionType", ModelAuditLogEntry.CHANGE_KEY_CODE, "getCode", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackSubscriptionMetadata {
    private final CharSequence subscriptionPlanGatewayPlanId = null;
    private final Long subscriptionType = null;
    private final Long subscriptionPlanId = null;
    private final CharSequence code = null;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackSubscriptionMetadata)) {
            return false;
        }
        TrackSubscriptionMetadata trackSubscriptionMetadata = (TrackSubscriptionMetadata) obj;
        return m.areEqual(this.subscriptionPlanGatewayPlanId, trackSubscriptionMetadata.subscriptionPlanGatewayPlanId) && m.areEqual(this.subscriptionType, trackSubscriptionMetadata.subscriptionType) && m.areEqual(this.subscriptionPlanId, trackSubscriptionMetadata.subscriptionPlanId) && m.areEqual(this.code, trackSubscriptionMetadata.code);
    }

    public int hashCode() {
        CharSequence charSequence = this.subscriptionPlanGatewayPlanId;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        Long l = this.subscriptionType;
        int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.subscriptionPlanId;
        int hashCode3 = (hashCode2 + (l2 != null ? l2.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.code;
        if (charSequence2 != null) {
            i = charSequence2.hashCode();
        }
        return hashCode3 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackSubscriptionMetadata(subscriptionPlanGatewayPlanId=");
        R.append(this.subscriptionPlanGatewayPlanId);
        R.append(", subscriptionType=");
        R.append(this.subscriptionType);
        R.append(", subscriptionPlanId=");
        R.append(this.subscriptionPlanId);
        R.append(", code=");
        return a.D(R, this.code, ")");
    }
}
