package com.discord.analytics.generated.traits;

import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackThread.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\r\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0011\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u000e\u001a\u0004\b\u0016\u0010\u0010R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u000e\u001a\u0004\b\u0018\u0010\u0010R!\u0010\u001a\u001a\n\u0018\u00010\fj\u0004\u0018\u0001`\u00198\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u000e\u001a\u0004\b\u001b\u0010\u0010R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u0012\u001a\u0004\b\u001d\u0010\u0014¨\u0006\u001e"}, d2 = {"Lcom/discord/analytics/generated/traits/TrackThread;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "threadApproximateMemberCount", "Ljava/lang/Long;", "getThreadApproximateMemberCount", "()Ljava/lang/Long;", "threadArchived", "Ljava/lang/Boolean;", "getThreadArchived", "()Ljava/lang/Boolean;", "threadApproximateMessageCount", "getThreadApproximateMessageCount", "threadAutoArchiveDurationMinutes", "getThreadAutoArchiveDurationMinutes", "Lcom/discord/primitives/Timestamp;", "threadApproximateCreationDate", "getThreadApproximateCreationDate", "threadLocked", "getThreadLocked", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackThread {
    private final Long threadApproximateMemberCount = null;
    private final Long threadApproximateMessageCount = null;
    private final Boolean threadArchived = null;
    private final Boolean threadLocked = null;
    private final Long threadAutoArchiveDurationMinutes = null;
    private final Long threadApproximateCreationDate = null;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackThread)) {
            return false;
        }
        TrackThread trackThread = (TrackThread) obj;
        return m.areEqual(this.threadApproximateMemberCount, trackThread.threadApproximateMemberCount) && m.areEqual(this.threadApproximateMessageCount, trackThread.threadApproximateMessageCount) && m.areEqual(this.threadArchived, trackThread.threadArchived) && m.areEqual(this.threadLocked, trackThread.threadLocked) && m.areEqual(this.threadAutoArchiveDurationMinutes, trackThread.threadAutoArchiveDurationMinutes) && m.areEqual(this.threadApproximateCreationDate, trackThread.threadApproximateCreationDate);
    }

    public int hashCode() {
        Long l = this.threadApproximateMemberCount;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.threadApproximateMessageCount;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Boolean bool = this.threadArchived;
        int hashCode3 = (hashCode2 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.threadLocked;
        int hashCode4 = (hashCode3 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        Long l3 = this.threadAutoArchiveDurationMinutes;
        int hashCode5 = (hashCode4 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.threadApproximateCreationDate;
        if (l4 != null) {
            i = l4.hashCode();
        }
        return hashCode5 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackThread(threadApproximateMemberCount=");
        R.append(this.threadApproximateMemberCount);
        R.append(", threadApproximateMessageCount=");
        R.append(this.threadApproximateMessageCount);
        R.append(", threadArchived=");
        R.append(this.threadArchived);
        R.append(", threadLocked=");
        R.append(this.threadLocked);
        R.append(", threadAutoArchiveDurationMinutes=");
        R.append(this.threadAutoArchiveDurationMinutes);
        R.append(", threadApproximateCreationDate=");
        return a.F(R, this.threadApproximateCreationDate, ")");
    }
}
