package com.discord.analytics.generated.traits;

import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackLocationMetadata.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\b\n\u0002\u0010\t\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\r\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0011\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u000e\u001a\u0004\b\u0012\u0010\u0010R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u000e\u001a\u0004\b\u0014\u0010\u0010R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u000e\u001a\u0004\b\u001b\u0010\u0010¨\u0006\u001c"}, d2 = {"Lcom/discord/analytics/generated/traits/TrackLocationMetadata;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "locationObject", "Ljava/lang/CharSequence;", "getLocationObject", "()Ljava/lang/CharSequence;", "locationObjectType", "getLocationObjectType", "locationSection", "getLocationSection", "", "locationPromotionId", "Ljava/lang/Long;", "getLocationPromotionId", "()Ljava/lang/Long;", "locationPage", "getLocationPage", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackLocationMetadata {
    private final CharSequence locationPage = null;
    private final CharSequence locationSection = null;
    private final CharSequence locationObject = null;
    private final CharSequence locationObjectType = null;
    private final Long locationPromotionId = null;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackLocationMetadata)) {
            return false;
        }
        TrackLocationMetadata trackLocationMetadata = (TrackLocationMetadata) obj;
        return m.areEqual(this.locationPage, trackLocationMetadata.locationPage) && m.areEqual(this.locationSection, trackLocationMetadata.locationSection) && m.areEqual(this.locationObject, trackLocationMetadata.locationObject) && m.areEqual(this.locationObjectType, trackLocationMetadata.locationObjectType) && m.areEqual(this.locationPromotionId, trackLocationMetadata.locationPromotionId);
    }

    public int hashCode() {
        CharSequence charSequence = this.locationPage;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        CharSequence charSequence2 = this.locationSection;
        int hashCode2 = (hashCode + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.locationObject;
        int hashCode3 = (hashCode2 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.locationObjectType;
        int hashCode4 = (hashCode3 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        Long l = this.locationPromotionId;
        if (l != null) {
            i = l.hashCode();
        }
        return hashCode4 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackLocationMetadata(locationPage=");
        R.append(this.locationPage);
        R.append(", locationSection=");
        R.append(this.locationSection);
        R.append(", locationObject=");
        R.append(this.locationObject);
        R.append(", locationObjectType=");
        R.append(this.locationObjectType);
        R.append(", locationPromotionId=");
        return a.F(R, this.locationPromotionId, ")");
    }
}
