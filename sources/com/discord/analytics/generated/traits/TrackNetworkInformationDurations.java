package com.discord.analytics.generated.traits;

import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackNetworkInformationDurations.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0019\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\r\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0011\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u000e\u001a\u0004\b\u0012\u0010\u0010R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u000e\u001a\u0004\b\u0014\u0010\u0010R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u000e\u001a\u0004\b\u0016\u0010\u0010R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u000e\u001a\u0004\b\u0018\u0010\u0010R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u000e\u001a\u0004\b\u001a\u0010\u0010R\u001b\u0010\u001b\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u000e\u001a\u0004\b\u001c\u0010\u0010R\u001b\u0010\u001d\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u000e\u001a\u0004\b\u001e\u0010\u0010R\u001b\u0010\u001f\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u000e\u001a\u0004\b \u0010\u0010R\u001b\u0010!\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u000e\u001a\u0004\b\"\u0010\u0010R\u001b\u0010#\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u000e\u001a\u0004\b$\u0010\u0010¨\u0006%"}, d2 = {"Lcom/discord/analytics/generated/traits/TrackNetworkInformationDurations;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "durationConnectionTypeWifi", "Ljava/lang/Long;", "getDurationConnectionTypeWifi", "()Ljava/lang/Long;", "durationConnectionTypeUnknown", "getDurationConnectionTypeUnknown", "durationConnectionTypeEthernet", "getDurationConnectionTypeEthernet", "durationConnectionTypeNone", "getDurationConnectionTypeNone", "durationConnectionTypeCellular", "getDurationConnectionTypeCellular", "durationEffectiveConnectionSpeed3g", "getDurationEffectiveConnectionSpeed3g", "durationConnectionTypeBluetooth", "getDurationConnectionTypeBluetooth", "durationEffectiveConnectionSpeed2g", "getDurationEffectiveConnectionSpeed2g", "durationEffectiveConnectionSpeed4g", "getDurationEffectiveConnectionSpeed4g", "durationEffectiveConnectionSpeedUnknown", "getDurationEffectiveConnectionSpeedUnknown", "durationConnectionTypeOther", "getDurationConnectionTypeOther", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackNetworkInformationDurations {
    private final Long durationConnectionTypeWifi = null;
    private final Long durationConnectionTypeCellular = null;
    private final Long durationConnectionTypeEthernet = null;
    private final Long durationConnectionTypeBluetooth = null;
    private final Long durationConnectionTypeOther = null;
    private final Long durationConnectionTypeUnknown = null;
    private final Long durationConnectionTypeNone = null;
    private final Long durationEffectiveConnectionSpeed2g = null;
    private final Long durationEffectiveConnectionSpeed3g = null;
    private final Long durationEffectiveConnectionSpeed4g = null;
    private final Long durationEffectiveConnectionSpeedUnknown = null;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackNetworkInformationDurations)) {
            return false;
        }
        TrackNetworkInformationDurations trackNetworkInformationDurations = (TrackNetworkInformationDurations) obj;
        return m.areEqual(this.durationConnectionTypeWifi, trackNetworkInformationDurations.durationConnectionTypeWifi) && m.areEqual(this.durationConnectionTypeCellular, trackNetworkInformationDurations.durationConnectionTypeCellular) && m.areEqual(this.durationConnectionTypeEthernet, trackNetworkInformationDurations.durationConnectionTypeEthernet) && m.areEqual(this.durationConnectionTypeBluetooth, trackNetworkInformationDurations.durationConnectionTypeBluetooth) && m.areEqual(this.durationConnectionTypeOther, trackNetworkInformationDurations.durationConnectionTypeOther) && m.areEqual(this.durationConnectionTypeUnknown, trackNetworkInformationDurations.durationConnectionTypeUnknown) && m.areEqual(this.durationConnectionTypeNone, trackNetworkInformationDurations.durationConnectionTypeNone) && m.areEqual(this.durationEffectiveConnectionSpeed2g, trackNetworkInformationDurations.durationEffectiveConnectionSpeed2g) && m.areEqual(this.durationEffectiveConnectionSpeed3g, trackNetworkInformationDurations.durationEffectiveConnectionSpeed3g) && m.areEqual(this.durationEffectiveConnectionSpeed4g, trackNetworkInformationDurations.durationEffectiveConnectionSpeed4g) && m.areEqual(this.durationEffectiveConnectionSpeedUnknown, trackNetworkInformationDurations.durationEffectiveConnectionSpeedUnknown);
    }

    public int hashCode() {
        Long l = this.durationConnectionTypeWifi;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.durationConnectionTypeCellular;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.durationConnectionTypeEthernet;
        int hashCode3 = (hashCode2 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.durationConnectionTypeBluetooth;
        int hashCode4 = (hashCode3 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.durationConnectionTypeOther;
        int hashCode5 = (hashCode4 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.durationConnectionTypeUnknown;
        int hashCode6 = (hashCode5 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.durationConnectionTypeNone;
        int hashCode7 = (hashCode6 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Long l8 = this.durationEffectiveConnectionSpeed2g;
        int hashCode8 = (hashCode7 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.durationEffectiveConnectionSpeed3g;
        int hashCode9 = (hashCode8 + (l9 != null ? l9.hashCode() : 0)) * 31;
        Long l10 = this.durationEffectiveConnectionSpeed4g;
        int hashCode10 = (hashCode9 + (l10 != null ? l10.hashCode() : 0)) * 31;
        Long l11 = this.durationEffectiveConnectionSpeedUnknown;
        if (l11 != null) {
            i = l11.hashCode();
        }
        return hashCode10 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackNetworkInformationDurations(durationConnectionTypeWifi=");
        R.append(this.durationConnectionTypeWifi);
        R.append(", durationConnectionTypeCellular=");
        R.append(this.durationConnectionTypeCellular);
        R.append(", durationConnectionTypeEthernet=");
        R.append(this.durationConnectionTypeEthernet);
        R.append(", durationConnectionTypeBluetooth=");
        R.append(this.durationConnectionTypeBluetooth);
        R.append(", durationConnectionTypeOther=");
        R.append(this.durationConnectionTypeOther);
        R.append(", durationConnectionTypeUnknown=");
        R.append(this.durationConnectionTypeUnknown);
        R.append(", durationConnectionTypeNone=");
        R.append(this.durationConnectionTypeNone);
        R.append(", durationEffectiveConnectionSpeed2g=");
        R.append(this.durationEffectiveConnectionSpeed2g);
        R.append(", durationEffectiveConnectionSpeed3g=");
        R.append(this.durationEffectiveConnectionSpeed3g);
        R.append(", durationEffectiveConnectionSpeed4g=");
        R.append(this.durationEffectiveConnectionSpeed4g);
        R.append(", durationEffectiveConnectionSpeedUnknown=");
        return a.F(R, this.durationEffectiveConnectionSpeedUnknown, ")");
    }
}
