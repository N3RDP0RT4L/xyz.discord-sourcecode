package com.discord.analytics.generated.traits;

import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackPriceByCurrency.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b/\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\r\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0011\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u000e\u001a\u0004\b\u0012\u0010\u0010R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u000e\u001a\u0004\b\u0014\u0010\u0010R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u000e\u001a\u0004\b\u0016\u0010\u0010R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u000e\u001a\u0004\b\u0018\u0010\u0010R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u000e\u001a\u0004\b\u001a\u0010\u0010R\u001b\u0010\u001b\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u000e\u001a\u0004\b\u001c\u0010\u0010R\u001b\u0010\u001d\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u000e\u001a\u0004\b\u001e\u0010\u0010R\u001b\u0010\u001f\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u000e\u001a\u0004\b \u0010\u0010R\u001b\u0010!\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u000e\u001a\u0004\b\"\u0010\u0010R\u001b\u0010#\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u000e\u001a\u0004\b$\u0010\u0010R\u001b\u0010%\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u000e\u001a\u0004\b&\u0010\u0010R\u001b\u0010'\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010\u000e\u001a\u0004\b(\u0010\u0010R\u001b\u0010)\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010\u000e\u001a\u0004\b*\u0010\u0010R\u001b\u0010+\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010\u000e\u001a\u0004\b,\u0010\u0010R\u001b\u0010-\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u000e\u001a\u0004\b.\u0010\u0010R\u001b\u0010/\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u000e\u001a\u0004\b0\u0010\u0010R\u001b\u00101\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010\u000e\u001a\u0004\b2\u0010\u0010R\u001b\u00103\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010\u000e\u001a\u0004\b4\u0010\u0010R\u001b\u00105\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010\u000e\u001a\u0004\b6\u0010\u0010R\u001b\u00107\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010\u000e\u001a\u0004\b8\u0010\u0010R\u001b\u00109\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010\u000e\u001a\u0004\b:\u0010\u0010¨\u0006;"}, d2 = {"Lcom/discord/analytics/generated/traits/TrackPriceByCurrency;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "priceNok", "Ljava/lang/Long;", "getPriceNok", "()Ljava/lang/Long;", "priceCad", "getPriceCad", "priceEur", "getPriceEur", "regularPriceRub", "getRegularPriceRub", "regularPriceTry", "getRegularPriceTry", "regularPriceJpy", "getRegularPriceJpy", "regularPriceCad", "getRegularPriceCad", "regularPriceEur", "getRegularPriceEur", "regularPriceGbp", "getRegularPriceGbp", "regularPriceAud", "getRegularPriceAud", "priceBrl", "getPriceBrl", "regularPricePln", "getRegularPricePln", "regularPriceNok", "getRegularPriceNok", "regularPriceBrl", "getRegularPriceBrl", "priceJpy", "getPriceJpy", "priceRub", "getPriceRub", "priceUsd", "getPriceUsd", "pricePln", "getPricePln", "regularPriceUsd", "getRegularPriceUsd", "priceTry", "getPriceTry", "priceAud", "getPriceAud", "priceGbp", "getPriceGbp", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackPriceByCurrency {
    private final Long priceUsd = null;
    private final Long priceCad = null;
    private final Long priceAud = null;
    private final Long priceEur = null;
    private final Long priceJpy = null;
    private final Long priceRub = null;
    private final Long priceGbp = null;
    private final Long priceNok = null;
    private final Long priceBrl = null;
    private final Long pricePln = null;
    private final Long priceTry = null;
    private final Long regularPriceUsd = null;
    private final Long regularPriceCad = null;
    private final Long regularPriceAud = null;
    private final Long regularPriceEur = null;
    private final Long regularPriceJpy = null;
    private final Long regularPriceRub = null;
    private final Long regularPriceGbp = null;
    private final Long regularPriceNok = null;
    private final Long regularPriceBrl = null;
    private final Long regularPricePln = null;
    private final Long regularPriceTry = null;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackPriceByCurrency)) {
            return false;
        }
        TrackPriceByCurrency trackPriceByCurrency = (TrackPriceByCurrency) obj;
        return m.areEqual(this.priceUsd, trackPriceByCurrency.priceUsd) && m.areEqual(this.priceCad, trackPriceByCurrency.priceCad) && m.areEqual(this.priceAud, trackPriceByCurrency.priceAud) && m.areEqual(this.priceEur, trackPriceByCurrency.priceEur) && m.areEqual(this.priceJpy, trackPriceByCurrency.priceJpy) && m.areEqual(this.priceRub, trackPriceByCurrency.priceRub) && m.areEqual(this.priceGbp, trackPriceByCurrency.priceGbp) && m.areEqual(this.priceNok, trackPriceByCurrency.priceNok) && m.areEqual(this.priceBrl, trackPriceByCurrency.priceBrl) && m.areEqual(this.pricePln, trackPriceByCurrency.pricePln) && m.areEqual(this.priceTry, trackPriceByCurrency.priceTry) && m.areEqual(this.regularPriceUsd, trackPriceByCurrency.regularPriceUsd) && m.areEqual(this.regularPriceCad, trackPriceByCurrency.regularPriceCad) && m.areEqual(this.regularPriceAud, trackPriceByCurrency.regularPriceAud) && m.areEqual(this.regularPriceEur, trackPriceByCurrency.regularPriceEur) && m.areEqual(this.regularPriceJpy, trackPriceByCurrency.regularPriceJpy) && m.areEqual(this.regularPriceRub, trackPriceByCurrency.regularPriceRub) && m.areEqual(this.regularPriceGbp, trackPriceByCurrency.regularPriceGbp) && m.areEqual(this.regularPriceNok, trackPriceByCurrency.regularPriceNok) && m.areEqual(this.regularPriceBrl, trackPriceByCurrency.regularPriceBrl) && m.areEqual(this.regularPricePln, trackPriceByCurrency.regularPricePln) && m.areEqual(this.regularPriceTry, trackPriceByCurrency.regularPriceTry);
    }

    public int hashCode() {
        Long l = this.priceUsd;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.priceCad;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.priceAud;
        int hashCode3 = (hashCode2 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.priceEur;
        int hashCode4 = (hashCode3 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.priceJpy;
        int hashCode5 = (hashCode4 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.priceRub;
        int hashCode6 = (hashCode5 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.priceGbp;
        int hashCode7 = (hashCode6 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Long l8 = this.priceNok;
        int hashCode8 = (hashCode7 + (l8 != null ? l8.hashCode() : 0)) * 31;
        Long l9 = this.priceBrl;
        int hashCode9 = (hashCode8 + (l9 != null ? l9.hashCode() : 0)) * 31;
        Long l10 = this.pricePln;
        int hashCode10 = (hashCode9 + (l10 != null ? l10.hashCode() : 0)) * 31;
        Long l11 = this.priceTry;
        int hashCode11 = (hashCode10 + (l11 != null ? l11.hashCode() : 0)) * 31;
        Long l12 = this.regularPriceUsd;
        int hashCode12 = (hashCode11 + (l12 != null ? l12.hashCode() : 0)) * 31;
        Long l13 = this.regularPriceCad;
        int hashCode13 = (hashCode12 + (l13 != null ? l13.hashCode() : 0)) * 31;
        Long l14 = this.regularPriceAud;
        int hashCode14 = (hashCode13 + (l14 != null ? l14.hashCode() : 0)) * 31;
        Long l15 = this.regularPriceEur;
        int hashCode15 = (hashCode14 + (l15 != null ? l15.hashCode() : 0)) * 31;
        Long l16 = this.regularPriceJpy;
        int hashCode16 = (hashCode15 + (l16 != null ? l16.hashCode() : 0)) * 31;
        Long l17 = this.regularPriceRub;
        int hashCode17 = (hashCode16 + (l17 != null ? l17.hashCode() : 0)) * 31;
        Long l18 = this.regularPriceGbp;
        int hashCode18 = (hashCode17 + (l18 != null ? l18.hashCode() : 0)) * 31;
        Long l19 = this.regularPriceNok;
        int hashCode19 = (hashCode18 + (l19 != null ? l19.hashCode() : 0)) * 31;
        Long l20 = this.regularPriceBrl;
        int hashCode20 = (hashCode19 + (l20 != null ? l20.hashCode() : 0)) * 31;
        Long l21 = this.regularPricePln;
        int hashCode21 = (hashCode20 + (l21 != null ? l21.hashCode() : 0)) * 31;
        Long l22 = this.regularPriceTry;
        if (l22 != null) {
            i = l22.hashCode();
        }
        return hashCode21 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackPriceByCurrency(priceUsd=");
        R.append(this.priceUsd);
        R.append(", priceCad=");
        R.append(this.priceCad);
        R.append(", priceAud=");
        R.append(this.priceAud);
        R.append(", priceEur=");
        R.append(this.priceEur);
        R.append(", priceJpy=");
        R.append(this.priceJpy);
        R.append(", priceRub=");
        R.append(this.priceRub);
        R.append(", priceGbp=");
        R.append(this.priceGbp);
        R.append(", priceNok=");
        R.append(this.priceNok);
        R.append(", priceBrl=");
        R.append(this.priceBrl);
        R.append(", pricePln=");
        R.append(this.pricePln);
        R.append(", priceTry=");
        R.append(this.priceTry);
        R.append(", regularPriceUsd=");
        R.append(this.regularPriceUsd);
        R.append(", regularPriceCad=");
        R.append(this.regularPriceCad);
        R.append(", regularPriceAud=");
        R.append(this.regularPriceAud);
        R.append(", regularPriceEur=");
        R.append(this.regularPriceEur);
        R.append(", regularPriceJpy=");
        R.append(this.regularPriceJpy);
        R.append(", regularPriceRub=");
        R.append(this.regularPriceRub);
        R.append(", regularPriceGbp=");
        R.append(this.regularPriceGbp);
        R.append(", regularPriceNok=");
        R.append(this.regularPriceNok);
        R.append(", regularPriceBrl=");
        R.append(this.regularPriceBrl);
        R.append(", regularPricePln=");
        R.append(this.regularPricePln);
        R.append(", regularPriceTry=");
        return a.F(R, this.regularPriceTry, ")");
    }
}
