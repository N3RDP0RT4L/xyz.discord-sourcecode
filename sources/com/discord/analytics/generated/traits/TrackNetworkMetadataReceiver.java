package com.discord.analytics.generated.traits;

import com.discord.api.science.AnalyticsSchema;
import kotlin.Metadata;
/* compiled from: TrackNetworkMetadata.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bf\u0018\u00002\u00020\u0001R\u001e\u0010\u0007\u001a\u0004\u0018\u00010\u00028&@&X¦\u000e¢\u0006\f\u001a\u0004\b\u0003\u0010\u0004\"\u0004\b\u0005\u0010\u0006¨\u0006\b"}, d2 = {"Lcom/discord/analytics/generated/traits/TrackNetworkMetadataReceiver;", "Lcom/discord/api/science/AnalyticsSchema;", "Lcom/discord/analytics/generated/traits/TrackNetworkMetadata;", "getTrackNetworkMetadata", "()Lcom/discord/analytics/generated/traits/TrackNetworkMetadata;", "a", "(Lcom/discord/analytics/generated/traits/TrackNetworkMetadata;)V", "trackNetworkMetadata", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public interface TrackNetworkMetadataReceiver extends AnalyticsSchema {
    void a(TrackNetworkMetadata trackNetworkMetadata);
}
