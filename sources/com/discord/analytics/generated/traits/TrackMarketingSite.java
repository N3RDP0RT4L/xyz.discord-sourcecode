package com.discord.analytics.generated.traits;

import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackMarketingSite.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0005\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\r\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u0011"}, d2 = {"Lcom/discord/analytics/generated/traits/TrackMarketingSite;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "marketingVariant", "Ljava/lang/CharSequence;", "getMarketingVariant", "()Ljava/lang/CharSequence;", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackMarketingSite {
    private final CharSequence marketingVariant = null;

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof TrackMarketingSite) && m.areEqual(this.marketingVariant, ((TrackMarketingSite) obj).marketingVariant);
        }
        return true;
    }

    public int hashCode() {
        CharSequence charSequence = this.marketingVariant;
        if (charSequence != null) {
            return charSequence.hashCode();
        }
        return 0;
    }

    public String toString() {
        return a.D(a.R("TrackMarketingSite(marketingVariant="), this.marketingVariant, ")");
    }
}
