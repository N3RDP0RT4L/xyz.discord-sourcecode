package com.discord.analytics.generated.traits;

import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackOverlay.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\n\n\u0002\u0010\t\n\u0002\b#\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\r\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0011\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u000e\u001a\u0004\b\u0016\u0010\u0010R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u001b\u0010\u001c\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u000e\u001a\u0004\b\u001d\u0010\u0010R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u0019\u001a\u0004\b\u001f\u0010\u001bR\u001b\u0010 \u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0019\u001a\u0004\b!\u0010\u001bR\u001b\u0010\"\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u0019\u001a\u0004\b#\u0010\u001bR\u001b\u0010$\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u0019\u001a\u0004\b%\u0010\u001bR\u001b\u0010&\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010\u000e\u001a\u0004\b'\u0010\u0010R\u001b\u0010(\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010\u0019\u001a\u0004\b)\u0010\u001bR\u001b\u0010*\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010\u0019\u001a\u0004\b+\u0010\u001bR\u001b\u0010,\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010\u0019\u001a\u0004\b-\u0010\u001bR\u001b\u0010.\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010\u0019\u001a\u0004\b/\u0010\u001bR\u001b\u00100\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010\u0019\u001a\u0004\b1\u0010\u001bR\u001b\u00102\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010\u000e\u001a\u0004\b3\u0010\u0010R\u001b\u00104\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010\u0019\u001a\u0004\b5\u0010\u001bR\u001b\u00106\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010\u000e\u001a\u0004\b7\u0010\u0010R\u001b\u00108\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010\u0019\u001a\u0004\b9\u0010\u001b¨\u0006:"}, d2 = {"Lcom/discord/analytics/generated/traits/TrackOverlay;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "error", "Ljava/lang/CharSequence;", "getError", "()Ljava/lang/CharSequence;", "rendererStarted", "Ljava/lang/Boolean;", "getRendererStarted", "()Ljava/lang/Boolean;", "cpu", "getCpu", "", "rendererLoadFailures", "Ljava/lang/Long;", "getRendererLoadFailures", "()Ljava/lang/Long;", "gpu", "getGpu", "gameId", "getGameId", "rendererReadyAfter", "getRendererReadyAfter", "rendererCrashCount", "getRendererCrashCount", "rendererIgnoredPaints", "getRendererIgnoredPaints", "framebufferSource", "getFramebufferSource", "rendererLoadSucceededAfter", "getRendererLoadSucceededAfter", "rendererStartedAfter", "getRendererStartedAfter", "hostCrashCount", "getHostCrashCount", "graphicsWidth", "getGraphicsWidth", "firstFramebufferAfter", "getFirstFramebufferAfter", "gameName", "getGameName", "graphicsHeight", "getGraphicsHeight", "graphicsApi", "getGraphicsApi", "graphicsInfoAfter", "getGraphicsInfoAfter", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackOverlay {
    private final CharSequence gameName = null;
    private final Long gameId = null;
    private final CharSequence error = null;
    private final Boolean rendererStarted = null;
    private final Long rendererStartedAfter = null;
    private final Long rendererReadyAfter = null;
    private final Long rendererLoadSucceededAfter = null;
    private final Long rendererCrashCount = null;
    private final Long rendererLoadFailures = null;
    private final Long rendererIgnoredPaints = null;
    private final Long hostCrashCount = null;
    private final CharSequence framebufferSource = null;
    private final Long firstFramebufferAfter = null;
    private final Long graphicsWidth = null;
    private final Long graphicsHeight = null;
    private final CharSequence graphicsApi = null;
    private final Long graphicsInfoAfter = null;
    private final CharSequence cpu = null;
    private final CharSequence gpu = null;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackOverlay)) {
            return false;
        }
        TrackOverlay trackOverlay = (TrackOverlay) obj;
        return m.areEqual(this.gameName, trackOverlay.gameName) && m.areEqual(this.gameId, trackOverlay.gameId) && m.areEqual(this.error, trackOverlay.error) && m.areEqual(this.rendererStarted, trackOverlay.rendererStarted) && m.areEqual(this.rendererStartedAfter, trackOverlay.rendererStartedAfter) && m.areEqual(this.rendererReadyAfter, trackOverlay.rendererReadyAfter) && m.areEqual(this.rendererLoadSucceededAfter, trackOverlay.rendererLoadSucceededAfter) && m.areEqual(this.rendererCrashCount, trackOverlay.rendererCrashCount) && m.areEqual(this.rendererLoadFailures, trackOverlay.rendererLoadFailures) && m.areEqual(this.rendererIgnoredPaints, trackOverlay.rendererIgnoredPaints) && m.areEqual(this.hostCrashCount, trackOverlay.hostCrashCount) && m.areEqual(this.framebufferSource, trackOverlay.framebufferSource) && m.areEqual(this.firstFramebufferAfter, trackOverlay.firstFramebufferAfter) && m.areEqual(this.graphicsWidth, trackOverlay.graphicsWidth) && m.areEqual(this.graphicsHeight, trackOverlay.graphicsHeight) && m.areEqual(this.graphicsApi, trackOverlay.graphicsApi) && m.areEqual(this.graphicsInfoAfter, trackOverlay.graphicsInfoAfter) && m.areEqual(this.cpu, trackOverlay.cpu) && m.areEqual(this.gpu, trackOverlay.gpu);
    }

    public int hashCode() {
        CharSequence charSequence = this.gameName;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        Long l = this.gameId;
        int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.error;
        int hashCode3 = (hashCode2 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        Boolean bool = this.rendererStarted;
        int hashCode4 = (hashCode3 + (bool != null ? bool.hashCode() : 0)) * 31;
        Long l2 = this.rendererStartedAfter;
        int hashCode5 = (hashCode4 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.rendererReadyAfter;
        int hashCode6 = (hashCode5 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.rendererLoadSucceededAfter;
        int hashCode7 = (hashCode6 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.rendererCrashCount;
        int hashCode8 = (hashCode7 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.rendererLoadFailures;
        int hashCode9 = (hashCode8 + (l6 != null ? l6.hashCode() : 0)) * 31;
        Long l7 = this.rendererIgnoredPaints;
        int hashCode10 = (hashCode9 + (l7 != null ? l7.hashCode() : 0)) * 31;
        Long l8 = this.hostCrashCount;
        int hashCode11 = (hashCode10 + (l8 != null ? l8.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.framebufferSource;
        int hashCode12 = (hashCode11 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Long l9 = this.firstFramebufferAfter;
        int hashCode13 = (hashCode12 + (l9 != null ? l9.hashCode() : 0)) * 31;
        Long l10 = this.graphicsWidth;
        int hashCode14 = (hashCode13 + (l10 != null ? l10.hashCode() : 0)) * 31;
        Long l11 = this.graphicsHeight;
        int hashCode15 = (hashCode14 + (l11 != null ? l11.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.graphicsApi;
        int hashCode16 = (hashCode15 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
        Long l12 = this.graphicsInfoAfter;
        int hashCode17 = (hashCode16 + (l12 != null ? l12.hashCode() : 0)) * 31;
        CharSequence charSequence5 = this.cpu;
        int hashCode18 = (hashCode17 + (charSequence5 != null ? charSequence5.hashCode() : 0)) * 31;
        CharSequence charSequence6 = this.gpu;
        if (charSequence6 != null) {
            i = charSequence6.hashCode();
        }
        return hashCode18 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackOverlay(gameName=");
        R.append(this.gameName);
        R.append(", gameId=");
        R.append(this.gameId);
        R.append(", error=");
        R.append(this.error);
        R.append(", rendererStarted=");
        R.append(this.rendererStarted);
        R.append(", rendererStartedAfter=");
        R.append(this.rendererStartedAfter);
        R.append(", rendererReadyAfter=");
        R.append(this.rendererReadyAfter);
        R.append(", rendererLoadSucceededAfter=");
        R.append(this.rendererLoadSucceededAfter);
        R.append(", rendererCrashCount=");
        R.append(this.rendererCrashCount);
        R.append(", rendererLoadFailures=");
        R.append(this.rendererLoadFailures);
        R.append(", rendererIgnoredPaints=");
        R.append(this.rendererIgnoredPaints);
        R.append(", hostCrashCount=");
        R.append(this.hostCrashCount);
        R.append(", framebufferSource=");
        R.append(this.framebufferSource);
        R.append(", firstFramebufferAfter=");
        R.append(this.firstFramebufferAfter);
        R.append(", graphicsWidth=");
        R.append(this.graphicsWidth);
        R.append(", graphicsHeight=");
        R.append(this.graphicsHeight);
        R.append(", graphicsApi=");
        R.append(this.graphicsApi);
        R.append(", graphicsInfoAfter=");
        R.append(this.graphicsInfoAfter);
        R.append(", cpu=");
        R.append(this.cpu);
        R.append(", gpu=");
        return a.D(R, this.gpu, ")");
    }
}
