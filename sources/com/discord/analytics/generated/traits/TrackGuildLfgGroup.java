package com.discord.analytics.generated.traits;

import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackGuildLfgGroup.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\r\n\u0002\b\u0011\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\r\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u000e\u001a\u0004\b\u0017\u0010\u0010R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u000e\u001a\u0004\b\u0019\u0010\u0010R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0013\u001a\u0004\b\u001b\u0010\u0015R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u000e\u001a\u0004\b\u001d\u0010\u0010R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u000e\u001a\u0004\b\u001f\u0010\u0010R\u001b\u0010 \u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u000e\u001a\u0004\b!\u0010\u0010¨\u0006\""}, d2 = {"Lcom/discord/analytics/generated/traits/TrackGuildLfgGroup;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "lfgGroupCurrentSize", "Ljava/lang/Long;", "getLfgGroupCurrentSize", "()Ljava/lang/Long;", "", "lfgGroupDescription", "Ljava/lang/CharSequence;", "getLfgGroupDescription", "()Ljava/lang/CharSequence;", "lfgGroupChannelId", "getLfgGroupChannelId", "lfgDirectoryChannelId", "getLfgDirectoryChannelId", "lfgDirectoryGuildName", "getLfgDirectoryGuildName", "lfgGroupOwnerId", "getLfgGroupOwnerId", "lfgGroupMaxSize", "getLfgGroupMaxSize", "lfgDirectoryGuildId", "getLfgDirectoryGuildId", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackGuildLfgGroup {
    private final Long lfgGroupChannelId = null;
    private final Long lfgGroupOwnerId = null;
    private final Long lfgDirectoryChannelId = null;
    private final Long lfgDirectoryGuildId = null;
    private final CharSequence lfgDirectoryGuildName = null;
    private final Long lfgGroupMaxSize = null;
    private final Long lfgGroupCurrentSize = null;
    private final CharSequence lfgGroupDescription = null;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackGuildLfgGroup)) {
            return false;
        }
        TrackGuildLfgGroup trackGuildLfgGroup = (TrackGuildLfgGroup) obj;
        return m.areEqual(this.lfgGroupChannelId, trackGuildLfgGroup.lfgGroupChannelId) && m.areEqual(this.lfgGroupOwnerId, trackGuildLfgGroup.lfgGroupOwnerId) && m.areEqual(this.lfgDirectoryChannelId, trackGuildLfgGroup.lfgDirectoryChannelId) && m.areEqual(this.lfgDirectoryGuildId, trackGuildLfgGroup.lfgDirectoryGuildId) && m.areEqual(this.lfgDirectoryGuildName, trackGuildLfgGroup.lfgDirectoryGuildName) && m.areEqual(this.lfgGroupMaxSize, trackGuildLfgGroup.lfgGroupMaxSize) && m.areEqual(this.lfgGroupCurrentSize, trackGuildLfgGroup.lfgGroupCurrentSize) && m.areEqual(this.lfgGroupDescription, trackGuildLfgGroup.lfgGroupDescription);
    }

    public int hashCode() {
        Long l = this.lfgGroupChannelId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.lfgGroupOwnerId;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.lfgDirectoryChannelId;
        int hashCode3 = (hashCode2 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.lfgDirectoryGuildId;
        int hashCode4 = (hashCode3 + (l4 != null ? l4.hashCode() : 0)) * 31;
        CharSequence charSequence = this.lfgDirectoryGuildName;
        int hashCode5 = (hashCode4 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
        Long l5 = this.lfgGroupMaxSize;
        int hashCode6 = (hashCode5 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.lfgGroupCurrentSize;
        int hashCode7 = (hashCode6 + (l6 != null ? l6.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.lfgGroupDescription;
        if (charSequence2 != null) {
            i = charSequence2.hashCode();
        }
        return hashCode7 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackGuildLfgGroup(lfgGroupChannelId=");
        R.append(this.lfgGroupChannelId);
        R.append(", lfgGroupOwnerId=");
        R.append(this.lfgGroupOwnerId);
        R.append(", lfgDirectoryChannelId=");
        R.append(this.lfgDirectoryChannelId);
        R.append(", lfgDirectoryGuildId=");
        R.append(this.lfgDirectoryGuildId);
        R.append(", lfgDirectoryGuildName=");
        R.append(this.lfgDirectoryGuildName);
        R.append(", lfgGroupMaxSize=");
        R.append(this.lfgGroupMaxSize);
        R.append(", lfgGroupCurrentSize=");
        R.append(this.lfgGroupCurrentSize);
        R.append(", lfgGroupDescription=");
        return a.D(R, this.lfgGroupDescription, ")");
    }
}
