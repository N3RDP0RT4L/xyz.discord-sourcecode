package com.discord.analytics.generated.traits;

import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrackGuildTemplate.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\r\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\r\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0013\u001a\u0004\b\u0017\u0010\u0015R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0013\u001a\u0004\b\u0019\u0010\u0015¨\u0006\u001a"}, d2 = {"Lcom/discord/analytics/generated/traits/TrackGuildTemplate;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "guildTemplateGuildId", "Ljava/lang/Long;", "getGuildTemplateGuildId", "()Ljava/lang/Long;", "", "guildTemplateCode", "Ljava/lang/CharSequence;", "getGuildTemplateCode", "()Ljava/lang/CharSequence;", "guildTemplateName", "getGuildTemplateName", "guildTemplateDescription", "getGuildTemplateDescription", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrackGuildTemplate {
    private final CharSequence guildTemplateCode = null;
    private final CharSequence guildTemplateName = null;
    private final CharSequence guildTemplateDescription = null;
    private final Long guildTemplateGuildId = null;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TrackGuildTemplate)) {
            return false;
        }
        TrackGuildTemplate trackGuildTemplate = (TrackGuildTemplate) obj;
        return m.areEqual(this.guildTemplateCode, trackGuildTemplate.guildTemplateCode) && m.areEqual(this.guildTemplateName, trackGuildTemplate.guildTemplateName) && m.areEqual(this.guildTemplateDescription, trackGuildTemplate.guildTemplateDescription) && m.areEqual(this.guildTemplateGuildId, trackGuildTemplate.guildTemplateGuildId);
    }

    public int hashCode() {
        CharSequence charSequence = this.guildTemplateCode;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        CharSequence charSequence2 = this.guildTemplateName;
        int hashCode2 = (hashCode + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.guildTemplateDescription;
        int hashCode3 = (hashCode2 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        Long l = this.guildTemplateGuildId;
        if (l != null) {
            i = l.hashCode();
        }
        return hashCode3 + i;
    }

    public String toString() {
        StringBuilder R = a.R("TrackGuildTemplate(guildTemplateCode=");
        R.append(this.guildTemplateCode);
        R.append(", guildTemplateName=");
        R.append(this.guildTemplateName);
        R.append(", guildTemplateDescription=");
        R.append(this.guildTemplateDescription);
        R.append(", guildTemplateGuildId=");
        return a.F(R, this.guildTemplateGuildId, ")");
    }
}
