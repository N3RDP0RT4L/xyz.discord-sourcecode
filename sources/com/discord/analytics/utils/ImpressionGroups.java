package com.discord.analytics.utils;

import andhook.lib.HookHelper;
import kotlin.Metadata;
/* compiled from: ImpressionGroups.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\n\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bR\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0007\u0010\u0004R\u0016\u0010\b\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\b\u0010\u0004R\u0016\u0010\t\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\t\u0010\u0004¨\u0006\f"}, d2 = {"Lcom/discord/analytics/utils/ImpressionGroups;", "", "", "GUILD_ADD_NUF", "Ljava/lang/String;", "USER_REGISTRATION_FLOW", "CHANNEL_ADD_FLOW", "GUILD_ADD_FLOW", "CONTACT_SYNC_FLOW", "USER_LOGIN_FLOW", HookHelper.constructorName, "()V", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ImpressionGroups {
    public static final String CHANNEL_ADD_FLOW = "channel_add_flow";
    public static final String CONTACT_SYNC_FLOW = "contact_sync_flow";
    public static final String GUILD_ADD_FLOW = "guild_add_flow";
    public static final String GUILD_ADD_NUF = "guild_add_nuf";
    public static final ImpressionGroups INSTANCE = new ImpressionGroups();
    public static final String USER_LOGIN_FLOW = "user_login_flow";
    public static final String USER_REGISTRATION_FLOW = "user_registration_flow";
}
