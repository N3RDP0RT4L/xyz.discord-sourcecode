package com.discord.analytics.utils;

import andhook.lib.HookHelper;
import kotlin.Metadata;
/* compiled from: RegistrationSteps.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/analytics/utils/RegistrationSteps;", "", "", "IDENTITY", "Ljava/lang/String;", "ACCOUNT_INFORMATION", HookHelper.constructorName, "()V", "analytics_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class RegistrationSteps {
    public static final String ACCOUNT_INFORMATION = "account_information";
    public static final String IDENTITY = "identity";
    public static final RegistrationSteps INSTANCE = new RegistrationSteps();
}
