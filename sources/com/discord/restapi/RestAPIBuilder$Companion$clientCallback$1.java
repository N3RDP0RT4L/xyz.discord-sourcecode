package com.discord.restapi;

import d0.z.d.m;
import d0.z.d.o;
import f0.x;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: RestAPIBuilder.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "<anonymous parameter 0>", "Lf0/x;", "<anonymous parameter 1>", "", "invoke", "(Ljava/lang/String;Lf0/x;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class RestAPIBuilder$Companion$clientCallback$1 extends o implements Function2<String, x, Unit> {
    public static final RestAPIBuilder$Companion$clientCallback$1 INSTANCE = new RestAPIBuilder$Companion$clientCallback$1();

    public RestAPIBuilder$Companion$clientCallback$1() {
        super(2);
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(String str, x xVar) {
        invoke2(str, xVar);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(String str, x xVar) {
        m.checkNotNullParameter(str, "<anonymous parameter 0>");
        m.checkNotNullParameter(xVar, "<anonymous parameter 1>");
    }
}
