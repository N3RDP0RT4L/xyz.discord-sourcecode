package com.discord.restapi;

import andhook.lib.HookHelper;
import androidx.annotation.VisibleForTesting;
import androidx.exifinterface.media.ExifInterface;
import b.a.b.a;
import b.i.d.c;
import b.i.d.e;
import com.discord.models.domain.Model;
import com.discord.models.experiments.dto.UserExperimentDto;
import com.discord.restapi.PayloadJSON;
import com.discord.restapi.RestAPIParams;
import com.google.gson.Gson;
import d0.z.d.m;
import f0.p;
import f0.w;
import f0.x;
import i0.d0.a.g;
import i0.e0.b.k;
import i0.i;
import i0.q;
import i0.u;
import i0.y;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import okhttp3.Interceptor;
/* compiled from: RestAPIBuilder.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000  2\u00020\u0001:\u0001 B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\u0006\u0010\u001c\u001a\u00020\u001b¢\u0006\u0004\b\u001e\u0010\u001fJM\u0010\r\u001a\u00028\u0000\"\u0004\b\u0000\u0010\u00022\u0006\u0010\u0004\u001a\u00020\u00032\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u00052\u0006\u0010\b\u001a\u00020\u00072\b\b\u0002\u0010\n\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\r\u0010\u000eJ-\u0010\u0014\u001a\u00020\u00032\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u000f2\u0010\b\u0002\u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\u0012\u0018\u00010\u0011H\u0002¢\u0006\u0004\b\u0014\u0010\u0015Jg\u0010\u0018\u001a\u00028\u0000\"\u0004\b\u0000\u0010\u00022\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00028\u00000\u00052\b\b\u0002\u0010\n\u001a\u00020\t2\b\b\u0002\u0010\u0010\u001a\u00020\u000f2\u0010\b\u0002\u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\u0012\u0018\u00010\u00112\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u00072\b\b\u0002\u0010\u000b\u001a\u00020\t2\b\b\u0002\u0010\f\u001a\u00020\u0007¢\u0006\u0004\b\u0018\u0010\u0019R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\u001aR\u0016\u0010\u001c\u001a\u00020\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001d¨\u0006!"}, d2 = {"Lcom/discord/restapi/RestAPIBuilder;", "", ExifInterface.GPS_DIRECTION_TRUE, "Lf0/x;", "client", "Ljava/lang/Class;", "api", "", "baseApiUrl", "", "serializeNulls", "addVersion", "contentType", "buildApi", "(Lf0/x;Ljava/lang/Class;Ljava/lang/String;ZZLjava/lang/String;)Ljava/lang/Object;", "", "timeoutMillis", "", "Lokhttp3/Interceptor;", "interceptors", "buildOkHttpClient", "(Ljava/lang/Long;Ljava/util/List;)Lf0/x;", "apiDefinition", "clientName", "build", "(Ljava/lang/Class;ZJLjava/util/List;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/Object;", "Ljava/lang/String;", "Lf0/p;", "cookieJar", "Lf0/p;", HookHelper.constructorName, "(Ljava/lang/String;Lf0/p;)V", "Companion", "restapi_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class RestAPIBuilder {
    private static final int API_VERSION = 9;
    public static final String CONTENT_TYPE_JSON = "application/json";
    public static final String CONTENT_TYPE_TEXT = "text/plain";
    private static final long DEFAULT_TIMEOUT_MILLIS = 10000;
    private final String baseApiUrl;
    private final p cookieJar;
    public static final Companion Companion = new Companion(null);
    private static Function2<? super String, ? super x, Unit> clientCallback = RestAPIBuilder$Companion$clientCallback$1.INSTANCE;

    /* compiled from: RestAPIBuilder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0004\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0017\u0010\rR:\u0010\u0006\u001a\u0014\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00028\u0006@\u0006X\u0087\u000e¢\u0006\u0018\n\u0004\b\u0006\u0010\u0007\u0012\u0004\b\f\u0010\r\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u0016\u0010\u000f\u001a\u00020\u000e8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0011\u001a\u00020\u00038\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0013\u001a\u00020\u00038\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0013\u0010\u0012R\u0016\u0010\u0015\u001a\u00020\u00148\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016¨\u0006\u0018"}, d2 = {"Lcom/discord/restapi/RestAPIBuilder$Companion;", "", "Lkotlin/Function2;", "", "Lf0/x;", "", "clientCallback", "Lkotlin/jvm/functions/Function2;", "getClientCallback", "()Lkotlin/jvm/functions/Function2;", "setClientCallback", "(Lkotlin/jvm/functions/Function2;)V", "getClientCallback$annotations", "()V", "", "API_VERSION", "I", "CONTENT_TYPE_JSON", "Ljava/lang/String;", "CONTENT_TYPE_TEXT", "", "DEFAULT_TIMEOUT_MILLIS", "J", HookHelper.constructorName, "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        @VisibleForTesting
        public static /* synthetic */ void getClientCallback$annotations() {
        }

        public final Function2<String, x, Unit> getClientCallback() {
            return RestAPIBuilder.clientCallback;
        }

        public final void setClientCallback(Function2<? super String, ? super x, Unit> function2) {
            m.checkNotNullParameter(function2, "<set-?>");
            RestAPIBuilder.clientCallback = function2;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public RestAPIBuilder(String str, p pVar) {
        m.checkNotNullParameter(str, "baseApiUrl");
        m.checkNotNullParameter(pVar, "cookieJar");
        this.baseApiUrl = str;
        this.cookieJar = pVar;
    }

    public static /* synthetic */ Object build$default(RestAPIBuilder restAPIBuilder, Class cls, boolean z2, long j, List list, String str, boolean z3, String str2, int i, Object obj) {
        boolean z4 = (i & 2) != 0 ? false : z2;
        long j2 = (i & 4) != 0 ? 10000L : j;
        String str3 = null;
        List list2 = (i & 8) != 0 ? null : list;
        if ((i & 16) == 0) {
            str3 = str;
        }
        return restAPIBuilder.build(cls, z4, j2, list2, str3, (i & 32) != 0 ? true : z3, (i & 64) != 0 ? CONTENT_TYPE_JSON : str2);
    }

    public final <T> T buildApi(x xVar, Class<T> cls, String str, boolean z2, boolean z3, String str2) {
        String str3;
        String str4;
        List<String> list;
        Method[] declaredMethods;
        e eVar = new e();
        eVar.c = c.m;
        m.checkNotNullExpressionValue(eVar, "GsonBuilder()\n        .s…ER_CASE_WITH_UNDERSCORES)");
        a.a(eVar);
        eVar.e.add(new Model.TypeAdapterFactory());
        eVar.e.add(UserExperimentDto.TypeAdapterFactory.INSTANCE);
        eVar.b(RestAPIParams.ChannelPosition.class, new RestAPIParams.ChannelPosition.Serializer());
        if (z2) {
            eVar.g = true;
        }
        Gson a = eVar.a();
        if (z3) {
            str4 = "v9/";
            str3 = str;
        } else {
            str3 = str;
            str4 = "";
        }
        String v = b.d.b.a.a.v(str3, str4);
        u uVar = u.a;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        Objects.requireNonNull(xVar, "client == null");
        arrayList2.add(new g(null, false));
        arrayList.add(new k());
        m.checkNotNullExpressionValue(a, "gson");
        arrayList.add(new PayloadJSON.ConverterFactory(a));
        if (m.areEqual(str2, CONTENT_TYPE_JSON)) {
            arrayList.add(new i0.e0.a.a(a));
        }
        Objects.requireNonNull(v, "baseUrl == null");
        m.checkParameterIsNotNull(v, "$this$toHttpUrl");
        w.a aVar = new w.a();
        aVar.e(null, v);
        w b2 = aVar.b();
        if ("".equals(b2.i.get(list.size() - 1))) {
            Executor a2 = uVar.a();
            ArrayList arrayList3 = new ArrayList(arrayList2);
            i iVar = new i(a2);
            arrayList3.addAll(uVar.f3730b ? Arrays.asList(i0.g.a, iVar) : Collections.singletonList(iVar));
            ArrayList arrayList4 = new ArrayList(arrayList.size() + 1 + (uVar.f3730b ? 1 : 0));
            arrayList4.add(new i0.c());
            arrayList4.addAll(arrayList);
            arrayList4.addAll(uVar.f3730b ? Collections.singletonList(q.a) : Collections.emptyList());
            y yVar = new y(xVar, b2, Collections.unmodifiableList(arrayList4), Collections.unmodifiableList(arrayList3), a2, false);
            if (cls.isInterface()) {
                ArrayDeque arrayDeque = new ArrayDeque(1);
                arrayDeque.add(cls);
                while (!arrayDeque.isEmpty()) {
                    Class<T> cls2 = (Class) arrayDeque.removeFirst();
                    if (cls2.getTypeParameters().length != 0) {
                        StringBuilder sb = new StringBuilder("Type parameters are unsupported on ");
                        sb.append(cls2.getName());
                        if (cls2 != cls) {
                            sb.append(" which is an interface of ");
                            sb.append(cls.getName());
                        }
                        throw new IllegalArgumentException(sb.toString());
                    }
                    Collections.addAll(arrayDeque, cls2.getInterfaces());
                }
                if (yVar.g) {
                    u uVar2 = u.a;
                    for (Method method : cls.getDeclaredMethods()) {
                        if (!(uVar2.f3730b && method.isDefault()) && !Modifier.isStatic(method.getModifiers())) {
                            yVar.b(method);
                        }
                    }
                }
                return (T) Proxy.newProxyInstance(cls.getClassLoader(), new Class[]{cls}, new i0.x(yVar, cls));
            }
            throw new IllegalArgumentException("API declarations must be interfaces.");
        }
        throw new IllegalArgumentException("baseUrl must end in /: " + b2);
    }

    private final x buildOkHttpClient(Long l, List<? extends Interceptor> list) {
        x.a aVar = new x.a();
        if (list != null) {
            for (Interceptor interceptor : list) {
                m.checkParameterIsNotNull(interceptor, "interceptor");
                aVar.c.add(interceptor);
            }
        }
        if (l != null) {
            long longValue = l.longValue();
            TimeUnit timeUnit = TimeUnit.MILLISECONDS;
            aVar.a(longValue, timeUnit);
            long longValue2 = l.longValue();
            m.checkParameterIsNotNull(timeUnit, "unit");
            aVar.f3663z = f0.e0.c.b("timeout", longValue2, timeUnit);
            long longValue3 = l.longValue();
            m.checkParameterIsNotNull(timeUnit, "unit");
            aVar.f3661x = f0.e0.c.b("timeout", longValue3, timeUnit);
        }
        p pVar = this.cookieJar;
        m.checkParameterIsNotNull(pVar, "cookieJar");
        aVar.j = pVar;
        return new x(aVar);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ x buildOkHttpClient$default(RestAPIBuilder restAPIBuilder, Long l, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            l = null;
        }
        if ((i & 2) != 0) {
            list = null;
        }
        return restAPIBuilder.buildOkHttpClient(l, list);
    }

    public final <T> T build(Class<T> cls, boolean z2, long j, List<? extends Interceptor> list, String str, boolean z3, String str2) {
        m.checkNotNullParameter(cls, "apiDefinition");
        m.checkNotNullParameter(str2, "contentType");
        x buildOkHttpClient = buildOkHttpClient(Long.valueOf(j), list);
        if (str != null) {
            clientCallback.invoke(str, buildOkHttpClient);
        }
        return (T) buildApi(buildOkHttpClient, cls, this.baseApiUrl, z2, z3, str2);
    }
}
