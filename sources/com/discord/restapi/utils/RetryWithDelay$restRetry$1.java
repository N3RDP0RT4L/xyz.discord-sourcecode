package com.discord.restapi.utils;

import androidx.browser.trusted.sharing.ShareTarget;
import androidx.exifinterface.media.ExifInterface;
import d0.z.d.m;
import d0.z.d.o;
import java.io.IOException;
import java.util.concurrent.TimeoutException;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.HttpException;
/* compiled from: RetryWithDelay.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u0003\"\u0004\b\u0000\u0010\u00002\u0006\u0010\u0002\u001a\u00020\u0001H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {ExifInterface.GPS_DIRECTION_TRUE, "", "throwable", "", "invoke", "(Ljava/lang/Throwable;)Z", "isNetworkError"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class RetryWithDelay$restRetry$1 extends o implements Function1<Throwable, Boolean> {
    public static final RetryWithDelay$restRetry$1 INSTANCE = new RetryWithDelay$restRetry$1();

    public RetryWithDelay$restRetry$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Boolean invoke(Throwable th) {
        return Boolean.valueOf(invoke2(th));
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final boolean invoke2(Throwable th) {
        Response response;
        Request request;
        String str;
        m.checkNotNullParameter(th, "throwable");
        if (th instanceof HttpException) {
            HttpException httpException = (HttpException) th;
            int a = httpException.a();
            retrofit2.Response<?> response2 = httpException.j;
            if (!(response2 == null || (response = response2.a) == null || (request = response.j) == null || (str = request.c) == null || !(!m.areEqual(str, ShareTarget.METHOD_GET))) || a == 401 || a == 429 || a == 503 || a == 403 || a == 404) {
                return false;
            }
        } else if (th instanceof TimeoutException) {
            return false;
        }
        return th instanceof IOException;
    }
}
