package com.discord.restapi.utils;

import andhook.lib.HookHelper;
import androidx.exifinterface.media.ExifInterface;
import d0.z.d.k;
import d0.z.d.m;
import j0.k.b;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Ref$IntRef;
import kotlin.jvm.internal.Ref$LongRef;
import rx.Observable;
/* compiled from: RetryWithDelay.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0003\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\n\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0012\u0010\u0013JQ\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00010\u0002*\n\u0012\u0006\b\u0001\u0012\u00020\u00030\u00022\u0006\u0010\u0005\u001a\u00020\u00042\b\u0010\u0007\u001a\u0004\u0018\u00010\u00062\b\u0010\b\u001a\u0004\u0018\u00010\u00062\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\n0\tH\u0002¢\u0006\u0004\b\f\u0010\rJE\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00000\u0002\"\u0004\b\u0000\u0010\u000e*\b\u0012\u0004\u0012\u00028\u00000\u00022\b\b\u0002\u0010\u0005\u001a\u00020\u00042\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\b\u000f\u0010\u0010JY\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00000\u0002\"\u0004\b\u0000\u0010\u000e*\b\u0012\u0004\u0012\u00028\u00000\u00022\b\b\u0002\u0010\u0005\u001a\u00020\u00042\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00062\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\n0\t¢\u0006\u0004\b\u000f\u0010\r¨\u0006\u0014"}, d2 = {"Lcom/discord/restapi/utils/RetryWithDelay;", "", "Lrx/Observable;", "", "", "delayMillis", "", "maxHalfLives", "maxRetries", "Lkotlin/Function1;", "", "retryPredicate", "retryWithDelay", "(Lrx/Observable;JLjava/lang/Integer;Ljava/lang/Integer;Lkotlin/jvm/functions/Function1;)Lrx/Observable;", ExifInterface.GPS_DIRECTION_TRUE, "restRetry", "(Lrx/Observable;JLjava/lang/Integer;Ljava/lang/Integer;)Lrx/Observable;", "predicate", HookHelper.constructorName, "()V", "restapi_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class RetryWithDelay {
    public static final RetryWithDelay INSTANCE = new RetryWithDelay();

    private RetryWithDelay() {
    }

    public static /* synthetic */ Observable restRetry$default(RetryWithDelay retryWithDelay, Observable observable, long j, Integer num, Integer num2, int i, Object obj) {
        if ((i & 1) != 0) {
            j = 1000;
        }
        long j2 = j;
        if ((i & 2) != 0) {
            num = 3;
        }
        Integer num3 = num;
        if ((i & 4) != 0) {
            num2 = 3;
        }
        return retryWithDelay.restRetry(observable, j2, num3, num2);
    }

    public final Observable<Object> retryWithDelay(Observable<? extends Throwable> observable, long j, Integer num, Integer num2, final Function1<? super Throwable, Boolean> function1) {
        final int intValue = num2 != null ? num2.intValue() : Integer.MAX_VALUE;
        final int intValue2 = num != null ? num.intValue() : Integer.MAX_VALUE;
        final Ref$IntRef ref$IntRef = new Ref$IntRef();
        ref$IntRef.element = 0;
        final Ref$IntRef ref$IntRef2 = new Ref$IntRef();
        ref$IntRef2.element = 0;
        final Ref$LongRef ref$LongRef = new Ref$LongRef();
        ref$LongRef.element = j;
        Observable<R> z2 = observable.z(new b<Throwable, Observable<? extends Object>>() { // from class: com.discord.restapi.utils.RetryWithDelay$retryWithDelay$1
            public final Observable<? extends Object> call(Throwable th) {
                Ref$IntRef ref$IntRef3 = ref$IntRef;
                int i = ref$IntRef3.element;
                ref$IntRef3.element = i + 1;
                if (i < intValue) {
                    Function1 function12 = function1;
                    m.checkNotNullExpressionValue(th, "it");
                    if (((Boolean) function12.invoke(th)).booleanValue()) {
                        Ref$IntRef ref$IntRef4 = ref$IntRef2;
                        int i2 = ref$IntRef4.element;
                        int i3 = i2 + 1;
                        ref$IntRef4.element = i3;
                        if (i2 < intValue2 && i3 > 1) {
                            ref$LongRef.element *= 2;
                        }
                        return Observable.d0(ref$LongRef.element, TimeUnit.MILLISECONDS);
                    }
                }
                return Observable.w(th);
            }
        });
        m.checkNotNullExpressionValue(z2, "flatMap {\n      if (curr…able.error<Any>(it)\n    }");
        return z2;
    }

    public final <T> Observable<T> restRetry(Observable<T> observable, final long j, final Integer num, final Integer num2) {
        m.checkNotNullParameter(observable, "$this$restRetry");
        RetryWithDelay$restRetry$1 retryWithDelay$restRetry$1 = RetryWithDelay$restRetry$1.INSTANCE;
        Observable<T> N = observable.N(new b<Observable<? extends Throwable>, Observable<?>>() { // from class: com.discord.restapi.utils.RetryWithDelay$restRetry$2

            /* compiled from: RetryWithDelay.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u0003\"\u0004\b\u0000\u0010\u00002\u0006\u0010\u0002\u001a\u00020\u0001¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {ExifInterface.GPS_DIRECTION_TRUE, "", "p1", "", "invoke", "(Ljava/lang/Throwable;)Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.restapi.utils.RetryWithDelay$restRetry$2$1  reason: invalid class name */
            /* loaded from: classes.dex */
            public static final /* synthetic */ class AnonymousClass1 extends k implements Function1<Throwable, Boolean> {
                public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                public AnonymousClass1() {
                    super(1, null, "isNetworkError", "invoke(Ljava/lang/Throwable;)Z", 0);
                }

                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Boolean invoke(Throwable th) {
                    return Boolean.valueOf(invoke2(th));
                }

                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final boolean invoke2(Throwable th) {
                    m.checkNotNullParameter(th, "p1");
                    return RetryWithDelay$restRetry$1.INSTANCE.invoke2(th);
                }
            }

            public final Observable<?> call(Observable<? extends Throwable> observable2) {
                Observable<?> retryWithDelay;
                RetryWithDelay retryWithDelay2 = RetryWithDelay.INSTANCE;
                m.checkNotNullExpressionValue(observable2, "it");
                retryWithDelay = retryWithDelay2.retryWithDelay(observable2, j, num, num2, AnonymousClass1.INSTANCE);
                return retryWithDelay;
            }
        });
        m.checkNotNullExpressionValue(N, "retryWhen { it.retryWith…ries, ::isNetworkError) }");
        return N;
    }

    public static /* synthetic */ Observable restRetry$default(RetryWithDelay retryWithDelay, Observable observable, long j, Integer num, Integer num2, Function1 function1, int i, Object obj) {
        if ((i & 1) != 0) {
            j = 1000;
        }
        long j2 = j;
        if ((i & 2) != 0) {
            num = 3;
        }
        Integer num3 = num;
        if ((i & 4) != 0) {
            num2 = 3;
        }
        return retryWithDelay.restRetry(observable, j2, num3, num2, function1);
    }

    public final <T> Observable<T> restRetry(Observable<T> observable, final long j, final Integer num, final Integer num2, final Function1<? super Throwable, Boolean> function1) {
        m.checkNotNullParameter(observable, "$this$restRetry");
        m.checkNotNullParameter(function1, "predicate");
        Observable<T> N = observable.N(new b<Observable<? extends Throwable>, Observable<?>>() { // from class: com.discord.restapi.utils.RetryWithDelay$restRetry$3
            public final Observable<?> call(Observable<? extends Throwable> observable2) {
                Observable<?> retryWithDelay;
                RetryWithDelay retryWithDelay2 = RetryWithDelay.INSTANCE;
                m.checkNotNullExpressionValue(observable2, "it");
                retryWithDelay = retryWithDelay2.retryWithDelay(observable2, j, num, num2, function1);
                return retryWithDelay;
            }
        });
        m.checkNotNullExpressionValue(N, "retryWhen { it.retryWith… maxRetries, predicate) }");
        return N;
    }
}
