package com.discord.restapi;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.restapi.RequiredHeadersInterceptor;
import d0.t.h0;
import d0.z.d.m;
import f0.e0.c;
import f0.w;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Metadata;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
/* compiled from: RestInterceptors.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/restapi/SpotifyTokenInterceptor;", "Lokhttp3/Interceptor;", "Lokhttp3/Interceptor$Chain;", "chain", "Lokhttp3/Response;", "intercept", "(Lokhttp3/Interceptor$Chain;)Lokhttp3/Response;", "Lcom/discord/restapi/RequiredHeadersInterceptor$HeadersProvider;", "headersProvider", "Lcom/discord/restapi/RequiredHeadersInterceptor$HeadersProvider;", HookHelper.constructorName, "(Lcom/discord/restapi/RequiredHeadersInterceptor$HeadersProvider;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class SpotifyTokenInterceptor implements Interceptor {
    private final RequiredHeadersInterceptor.HeadersProvider headersProvider;

    public SpotifyTokenInterceptor(RequiredHeadersInterceptor.HeadersProvider headersProvider) {
        m.checkNotNullParameter(headersProvider, "headersProvider");
        this.headersProvider = headersProvider;
    }

    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) {
        Map map;
        m.checkNotNullParameter(chain, "chain");
        Request c = chain.c();
        m.checkParameterIsNotNull(c, "request");
        new LinkedHashMap();
        w wVar = c.f3784b;
        String str = c.c;
        RequestBody requestBody = c.e;
        if (c.f.isEmpty()) {
            map = new LinkedHashMap();
        } else {
            map = h0.toMutableMap(c.f);
        }
        Headers.a e = c.d.e();
        StringBuilder R = a.R("Bearer ");
        R.append(this.headersProvider.getSpotifyToken());
        String sb = R.toString();
        m.checkParameterIsNotNull("Authorization", ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkParameterIsNotNull(sb, "value");
        e.a("Authorization", sb);
        if (wVar != null) {
            return chain.a(new Request(wVar, str, e.c(), requestBody, c.A(map)));
        }
        throw new IllegalStateException("url == null".toString());
    }
}
