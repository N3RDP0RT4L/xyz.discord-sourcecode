package com.discord.restapi;

import andhook.lib.HookHelper;
import androidx.appcompat.widget.ActivityChooserModel;
import androidx.core.app.NotificationCompat;
import androidx.media.AudioAttributesCompat;
import b.d.b.a.a;
import b.i.d.j;
import b.i.d.l;
import com.discord.api.application.Application;
import com.discord.api.botuikit.ComponentType;
import com.discord.api.commands.ApplicationCommandData;
import com.discord.api.friendsuggestions.AllowedInSuggestionsType;
import com.discord.api.guild.Guild;
import com.discord.api.guild.GuildExplicitContentFilter;
import com.discord.api.guild.GuildFeature;
import com.discord.api.guild.GuildVerificationLevel;
import com.discord.api.guildrolesubscription.GuildRoleSubscriptionBenefit;
import com.discord.api.guildscheduledevent.GuildScheduledEventEntityMetadata;
import com.discord.api.guildscheduledevent.GuildScheduledEventEntityType;
import com.discord.api.guildscheduledevent.GuildScheduledEventStatus;
import com.discord.api.message.activity.MessageActivityType;
import com.discord.api.message.allowedmentions.MessageAllowedMentions;
import com.discord.api.message.allowedmentions.MessageAllowedMentionsTypes;
import com.discord.api.permission.PermissionOverwrite;
import com.discord.api.presence.ClientStatus;
import com.discord.api.role.GuildRole;
import com.discord.api.stageinstance.StageInstancePrivacyLevel;
import com.discord.api.user.User;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelCustomStatusSetting;
import com.discord.models.domain.ModelGuildFolder;
import com.discord.models.domain.ModelMemberVerificationForm;
import com.discord.models.domain.ModelMuteConfig;
import com.discord.models.domain.ModelSubscription;
import com.discord.nullserializable.NullSerializable;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import d0.t.g0;
import d0.t.n;
import d0.t.o;
import d0.z.d.m;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.objectweb.asm.Opcodes;
/* compiled from: RestAPIParams.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\bi\u0018\u00002\u00020\u0001:f\u0004\u0005\u0006\u0007\b\t\n\u000b\f\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u001a\u001b\u001c\u001d\u001e\u001f !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghiB\u0007¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006j"}, d2 = {"Lcom/discord/restapi/RestAPIParams;", "", HookHelper.constructorName, "()V", "ActivateMfaSMS", "AddServerBody", "ApplicationCommand", "AuthLogin", "AuthRegister", "AuthRegisterPhone", "AuthorizeIP", "BanGuildMember", "CaptchaCode", "ChannelFollowerPost", "ChannelMessagesAck", "ChannelPermissionOverwrites", "ChannelPosition", "ChannelVoiceStateUpdate", "ComponentInteraction", "ComponentInteractionData", "ConnectedAccount", "ConnectedAccountContacts", "ConnectedAccountNameOnly", "ConnectionState", "Consents", "ContactEntry", "CreateChannel", "CreateGuild", "CreateGuildChannel", "CreateGuildFromTemplate", "CreateGuildRoleSubscriptionGroupListing", "CreateGuildRoleSubscriptionTierListing", "CreateGuildScheduledEventBody", "DeleteGuild", "DeletePhone", "DisableAccount", "DisableGuildCommunication", "DowngradeSubscriptionBody", "EmptyBody", "EnableIntegration", "EnableMFA", "ForgotPassword", "GenerateGiftCode", "GreetMessage", "GroupDM", "GuildBoosting", "GuildIntegration", "GuildMFA", "GuildMember", "GuildMemberDisconnect", "HubWaitlist", "Invite", "InviteCode", "InvoicePreviewBody", "LeaveGuildBody", "MFALogin", "MemberVerificationForm", "Message", "Nick", "OAuth2Authorize", "PatchGuildEmoji", "Phone", "PostGuildEmoji", "PruneGuild", "PurchaseMetadataBody", "RemoteAuthCancel", "RemoteAuthFinish", "RemoteAuthInitialize", "Report", "Ring", "Role", "StartStageInstanceBody", "TextChannel", "ThreadCreationSettings", "ThreadMemberSettings", "ThreadSettings", "Thumbnail", "TopicalChannel", "TransferGuildOwnership", "UpdateGuild", "UpdateGuildRoleSubscriptionGroupListing", "UpdateGuildRoleSubscriptionTierListing", "UpdateGuildScheduledEventBody", "UpdateStageInstanceBody", "UpdateSubscription", "UploadContacts", "UserAgreements", "UserBulkRelationship", "UserDevices", "UserEmailConfirmCode", "UserGuildSettings", "UserInfo", "UserNoteUpdate", "UserRelationship", "UserSettings", "UserSettingsCustomStatus", "VanityUrl", "VerificationCode", "VerificationCodeOnly", "VerificationCodeResend", "VerificationPhoneCode", "VerifyEmail", "VerifyEmailCode", "VerifyPurchaseResponse", "VerifyPurchaseTokenBody", "VoiceChannel", "restapi_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class RestAPIParams {

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/restapi/RestAPIParams$ActivateMfaSMS;", "", "", "password", "Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ActivateMfaSMS {
        private final String password;

        public ActivateMfaSMS(String str) {
            m.checkNotNullParameter(str, "password");
            this.password = str;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\f\u0010\u0004J\u0010\u0010\r\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\r\u0010\u0007J\u001a\u0010\u0010\u001a\u00020\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0010\u0010\u0011R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0012\u001a\u0004\b\u0013\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0014\u001a\u0004\b\u0015\u0010\u0007¨\u0006\u0018"}, d2 = {"Lcom/discord/restapi/RestAPIParams$AddServerBody;", "", "", "component1", "()Ljava/lang/String;", "", "component2", "()I", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "primaryCategoryId", "copy", "(Ljava/lang/String;I)Lcom/discord/restapi/RestAPIParams$AddServerBody;", "toString", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getDescription", "I", "getPrimaryCategoryId", HookHelper.constructorName, "(Ljava/lang/String;I)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class AddServerBody {
        private final String description;
        private final int primaryCategoryId;

        public AddServerBody(String str, int i) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION);
            this.description = str;
            this.primaryCategoryId = i;
        }

        public static /* synthetic */ AddServerBody copy$default(AddServerBody addServerBody, String str, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                str = addServerBody.description;
            }
            if ((i2 & 2) != 0) {
                i = addServerBody.primaryCategoryId;
            }
            return addServerBody.copy(str, i);
        }

        public final String component1() {
            return this.description;
        }

        public final int component2() {
            return this.primaryCategoryId;
        }

        public final AddServerBody copy(String str, int i) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION);
            return new AddServerBody(str, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof AddServerBody)) {
                return false;
            }
            AddServerBody addServerBody = (AddServerBody) obj;
            return m.areEqual(this.description, addServerBody.description) && this.primaryCategoryId == addServerBody.primaryCategoryId;
        }

        public final String getDescription() {
            return this.description;
        }

        public final int getPrimaryCategoryId() {
            return this.primaryCategoryId;
        }

        public int hashCode() {
            String str = this.description;
            return ((str != null ? str.hashCode() : 0) * 31) + this.primaryCategoryId;
        }

        public String toString() {
            StringBuilder R = a.R("AddServerBody(description=");
            R.append(this.description);
            R.append(", primaryCategoryId=");
            return a.A(R, this.primaryCategoryId, ")");
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001BC\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\u0006\u0010\u0010\u001a\u00020\u0005\u0012\u0006\u0010\u0011\u001a\u00020\u0005\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0013\u001a\u00020\n\u0012\b\u0010\u0014\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0015\u001a\u00020\u0005¢\u0006\u0004\b*\u0010+J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\t\u0010\u0007J\u0010\u0010\u000b\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0012\u0010\r\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\r\u0010\u0007J\u0010\u0010\u000e\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u000e\u0010\u0007JZ\u0010\u0016\u001a\u00020\u00002\b\b\u0002\u0010\u000f\u001a\u00020\u00022\b\b\u0002\u0010\u0010\u001a\u00020\u00052\b\b\u0002\u0010\u0011\u001a\u00020\u00052\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u0013\u001a\u00020\n2\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u0015\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0018\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0007J\u0010\u0010\u001a\u001a\u00020\u0019HÖ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u001a\u0010\u001e\u001a\u00020\u001d2\b\u0010\u001c\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001e\u0010\u001fR\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010 \u001a\u0004\b!\u0010\u0004R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\"\u001a\u0004\b#\u0010\u0007R\u0019\u0010\u0010\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\"\u001a\u0004\b$\u0010\u0007R\u001b\u0010\u0014\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\"\u001a\u0004\b%\u0010\u0007R\u0019\u0010\u0013\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010&\u001a\u0004\b'\u0010\fR\u0019\u0010\u0011\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\"\u001a\u0004\b(\u0010\u0007R\u0019\u0010\u0015\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\"\u001a\u0004\b)\u0010\u0007¨\u0006,"}, d2 = {"Lcom/discord/restapi/RestAPIParams$ApplicationCommand;", "", "", "component1", "()J", "", "component2", "()Ljava/lang/String;", "component3", "component4", "Lcom/discord/api/commands/ApplicationCommandData;", "component5", "()Lcom/discord/api/commands/ApplicationCommandData;", "component6", "component7", "type", "channelId", "applicationId", "guildId", "data", "sessionId", "nonce", "copy", "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/discord/api/commands/ApplicationCommandData;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/restapi/RestAPIParams$ApplicationCommand;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getType", "Ljava/lang/String;", "getGuildId", "getChannelId", "getSessionId", "Lcom/discord/api/commands/ApplicationCommandData;", "getData", "getApplicationId", "getNonce", HookHelper.constructorName, "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/discord/api/commands/ApplicationCommandData;Ljava/lang/String;Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ApplicationCommand {
        private final String applicationId;
        private final String channelId;
        private final ApplicationCommandData data;
        private final String guildId;
        private final String nonce;
        private final String sessionId;
        private final long type;

        public ApplicationCommand(long j, String str, String str2, String str3, ApplicationCommandData applicationCommandData, String str4, String str5) {
            m.checkNotNullParameter(str, "channelId");
            m.checkNotNullParameter(str2, "applicationId");
            m.checkNotNullParameter(applicationCommandData, "data");
            m.checkNotNullParameter(str5, "nonce");
            this.type = j;
            this.channelId = str;
            this.applicationId = str2;
            this.guildId = str3;
            this.data = applicationCommandData;
            this.sessionId = str4;
            this.nonce = str5;
        }

        public final long component1() {
            return this.type;
        }

        public final String component2() {
            return this.channelId;
        }

        public final String component3() {
            return this.applicationId;
        }

        public final String component4() {
            return this.guildId;
        }

        public final ApplicationCommandData component5() {
            return this.data;
        }

        public final String component6() {
            return this.sessionId;
        }

        public final String component7() {
            return this.nonce;
        }

        public final ApplicationCommand copy(long j, String str, String str2, String str3, ApplicationCommandData applicationCommandData, String str4, String str5) {
            m.checkNotNullParameter(str, "channelId");
            m.checkNotNullParameter(str2, "applicationId");
            m.checkNotNullParameter(applicationCommandData, "data");
            m.checkNotNullParameter(str5, "nonce");
            return new ApplicationCommand(j, str, str2, str3, applicationCommandData, str4, str5);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ApplicationCommand)) {
                return false;
            }
            ApplicationCommand applicationCommand = (ApplicationCommand) obj;
            return this.type == applicationCommand.type && m.areEqual(this.channelId, applicationCommand.channelId) && m.areEqual(this.applicationId, applicationCommand.applicationId) && m.areEqual(this.guildId, applicationCommand.guildId) && m.areEqual(this.data, applicationCommand.data) && m.areEqual(this.sessionId, applicationCommand.sessionId) && m.areEqual(this.nonce, applicationCommand.nonce);
        }

        public final String getApplicationId() {
            return this.applicationId;
        }

        public final String getChannelId() {
            return this.channelId;
        }

        public final ApplicationCommandData getData() {
            return this.data;
        }

        public final String getGuildId() {
            return this.guildId;
        }

        public final String getNonce() {
            return this.nonce;
        }

        public final String getSessionId() {
            return this.sessionId;
        }

        public final long getType() {
            return this.type;
        }

        public int hashCode() {
            long j = this.type;
            int i = ((int) (j ^ (j >>> 32))) * 31;
            String str = this.channelId;
            int i2 = 0;
            int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.applicationId;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.guildId;
            int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
            ApplicationCommandData applicationCommandData = this.data;
            int hashCode4 = (hashCode3 + (applicationCommandData != null ? applicationCommandData.hashCode() : 0)) * 31;
            String str4 = this.sessionId;
            int hashCode5 = (hashCode4 + (str4 != null ? str4.hashCode() : 0)) * 31;
            String str5 = this.nonce;
            if (str5 != null) {
                i2 = str5.hashCode();
            }
            return hashCode5 + i2;
        }

        public String toString() {
            StringBuilder R = a.R("ApplicationCommand(type=");
            R.append(this.type);
            R.append(", channelId=");
            R.append(this.channelId);
            R.append(", applicationId=");
            R.append(this.applicationId);
            R.append(", guildId=");
            R.append(this.guildId);
            R.append(", data=");
            R.append(this.data);
            R.append(", sessionId=");
            R.append(this.sessionId);
            R.append(", nonce=");
            return a.H(R, this.nonce, ")");
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001\rB9\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u000b\u0010\fR\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0018\u0010\u0005\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0018\u0010\u0006\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004R\u0018\u0010\b\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\tR\u0018\u0010\n\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u0004¨\u0006\u000e"}, d2 = {"Lcom/discord/restapi/RestAPIParams$AuthLogin;", "", "", "login", "Ljava/lang/String;", "password", "captchaKey", "", "undelete", "Ljava/lang/Boolean;", "loginSource", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V", "LoginSource", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class AuthLogin {
        private final String captchaKey;
        private final String login;
        private final String loginSource;
        private final String password;
        private final Boolean undelete;

        /* compiled from: RestAPIParams.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/restapi/RestAPIParams$AuthLogin$LoginSource;", "", "", "LOGIN_SOURCE_KEYCHAIN_AUTO", "Ljava/lang/String;", HookHelper.constructorName, "()V", "restapi_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class LoginSource {
            public static final LoginSource INSTANCE = new LoginSource();
            public static final String LOGIN_SOURCE_KEYCHAIN_AUTO = "keychain_auto_login";

            private LoginSource() {
            }
        }

        public AuthLogin(String str, String str2, String str3, Boolean bool, String str4) {
            this.login = str;
            this.password = str2;
            this.captchaKey = str3;
            this.undelete = bool;
            this.loginSource = str4;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001Bq\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0010\b\u0002\u0010\f\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u000b¢\u0006\u0004\b\u0011\u0010\u0012R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007R\u0018\u0010\b\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\u0004R\u0018\u0010\t\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\t\u0010\u0004R\u0018\u0010\n\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u0004R\u001e\u0010\f\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\u0004R\u0018\u0010\r\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u0004R\u0018\u0010\u000e\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u0004R\u0018\u0010\u000f\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0004R\u0018\u0010\u0010\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0004¨\u0006\u0013"}, d2 = {"Lcom/discord/restapi/RestAPIParams$AuthRegister;", "", "", "invite", "Ljava/lang/String;", "", "consent", "Z", "username", "guildTemplateCode", "password", "Lcom/discord/primitives/UtcTimestamp;", "dateOfBirth", NotificationCompat.CATEGORY_EMAIL, "captchaKey", "phoneToken", "fingerprint", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class AuthRegister {
        private final String captchaKey;
        private final boolean consent;
        private final String dateOfBirth;
        private final String email;
        private final String fingerprint;
        private final String guildTemplateCode;
        private final String invite;
        private final String password;
        private final String phoneToken;
        private final String username;

        public AuthRegister(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, boolean z2, String str9) {
            this.fingerprint = str;
            this.username = str2;
            this.email = str3;
            this.phoneToken = str4;
            this.password = str5;
            this.captchaKey = str6;
            this.invite = str7;
            this.guildTemplateCode = str8;
            this.consent = z2;
            this.dateOfBirth = str9;
        }

        public /* synthetic */ AuthRegister(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, boolean z2, String str9, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(str, str2, str3, str4, str5, str6, str7, str8, z2, (i & 512) != 0 ? null : str9);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\u0005\u001a\u00020\u0002\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/restapi/RestAPIParams$AuthRegisterPhone;", "", "", "captchaKey", "Ljava/lang/String;", "phone", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class AuthRegisterPhone {
        private final String captchaKey;
        private final String phone;

        public AuthRegisterPhone(String str, String str2) {
            m.checkNotNullParameter(str, "phone");
            this.phone = str;
            this.captchaKey = str2;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0011\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/restapi/RestAPIParams$AuthorizeIP;", "", "", "token", "Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class AuthorizeIP {
        private final String token;

        public AuthorizeIP(String str) {
            this.token = str;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0013\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/restapi/RestAPIParams$BanGuildMember;", "", "", "deleteMessageDays", "Ljava/lang/Integer;", HookHelper.constructorName, "(Ljava/lang/Integer;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class BanGuildMember {
        private final Integer deleteMessageDays;

        public BanGuildMember() {
            this(null, 1, null);
        }

        public BanGuildMember(Integer num) {
            this.deleteMessageDays = num;
        }

        public /* synthetic */ BanGuildMember(Integer num, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : num);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0011\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/restapi/RestAPIParams$CaptchaCode;", "", "", "captchaKey", "Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class CaptchaCode {
        private final String captchaKey;

        public CaptchaCode(String str) {
            this.captchaKey = str;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0013\u0012\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\b\u0010\tR\u001d\u0010\u0004\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/restapi/RestAPIParams$ChannelFollowerPost;", "", "", "Lcom/discord/primitives/ChannelId;", "webhookChannelId", "J", "getWebhookChannelId", "()J", HookHelper.constructorName, "(J)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ChannelFollowerPost {
        private final long webhookChannelId;

        public ChannelFollowerPost(long j) {
            this.webhookChannelId = j;
        }

        public final long getWebhookChannelId() {
            return this.webhookChannelId;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u001b\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\b\u0010\tR\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0018\u0010\u0006\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/restapi/RestAPIParams$ChannelMessagesAck;", "", "", "manual", "Ljava/lang/Boolean;", "", "mentionCount", "Ljava/lang/Integer;", HookHelper.constructorName, "(Ljava/lang/Boolean;Ljava/lang/Integer;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ChannelMessagesAck {
        private final Boolean manual;
        private final Integer mentionCount;

        public ChannelMessagesAck(Boolean bool, Integer num) {
            this.manual = bool;
            this.mentionCount = num;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0006\u0018\u0000 \u000f2\u00020\u0001:\u0001\u000fB=\b\u0002\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\n\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\u000e\u0010\t\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006\u0012\u000e\u0010\u0007\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006¢\u0006\u0004\b\r\u0010\u000eR\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u001e\u0010\u0007\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00068\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0007\u0010\bR\u001e\u0010\t\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00068\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\t\u0010\bR\u0018\u0010\u000b\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;", "", "", ModelAuditLogEntry.CHANGE_KEY_ID, "Ljava/lang/String;", "", "Lcom/discord/api/permission/PermissionBit;", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_DENIED, "Ljava/lang/Long;", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_GRANTED, "", "type", "Ljava/lang/Integer;", HookHelper.constructorName, "(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;)V", "Companion", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ChannelPermissionOverwrites {
        public static final Companion Companion = new Companion(null);
        private final Long allow;
        private final Long deny;

        /* renamed from: id  reason: collision with root package name */
        private final String f2732id;
        private final Integer type;

        /* compiled from: RestAPIParams.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000f\u0010\u0010J7\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\u000e\u0010\u0005\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00042\u000e\u0010\u0006\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0004H\u0007¢\u0006\u0004\b\b\u0010\tJ7\u0010\n\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\u000e\u0010\u0005\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00042\u000e\u0010\u0006\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0004H\u0007¢\u0006\u0004\b\n\u0010\tJ\u0015\u0010\r\u001a\u00020\u00072\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites$Companion;", "", "", ModelAuditLogEntry.CHANGE_KEY_ID, "Lcom/discord/api/permission/PermissionBit;", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_GRANTED, ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_DENIED, "Lcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;", "createForRole", "(JLjava/lang/Long;Ljava/lang/Long;)Lcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;", "createForMember", "Lcom/discord/api/permission/PermissionOverwrite;", "overwrite", "fromPermissionOverwrite", "(Lcom/discord/api/permission/PermissionOverwrite;)Lcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;", HookHelper.constructorName, "()V", "restapi_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Companion {

            @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
            /* loaded from: classes.dex */
            public final /* synthetic */ class WhenMappings {
                public static final /* synthetic */ int[] $EnumSwitchMapping$0;

                static {
                    PermissionOverwrite.Type.values();
                    int[] iArr = new int[2];
                    $EnumSwitchMapping$0 = iArr;
                    iArr[PermissionOverwrite.Type.ROLE.ordinal()] = 1;
                    iArr[PermissionOverwrite.Type.MEMBER.ordinal()] = 2;
                }
            }

            private Companion() {
            }

            public final ChannelPermissionOverwrites createForMember(long j, Long l, Long l2) {
                return new ChannelPermissionOverwrites(1, String.valueOf(j), l, l2, null);
            }

            public final ChannelPermissionOverwrites createForRole(long j, Long l, Long l2) {
                return new ChannelPermissionOverwrites(0, String.valueOf(j), l, l2, null);
            }

            public final ChannelPermissionOverwrites fromPermissionOverwrite(PermissionOverwrite permissionOverwrite) {
                m.checkNotNullParameter(permissionOverwrite, "overwrite");
                int ordinal = permissionOverwrite.f().ordinal();
                int i = 1;
                if (ordinal == 0) {
                    i = 0;
                } else if (ordinal != 1) {
                    throw new NoWhenBranchMatchedException();
                }
                return new ChannelPermissionOverwrites(Integer.valueOf(i), String.valueOf(permissionOverwrite.e()), Long.valueOf(permissionOverwrite.c()), Long.valueOf(permissionOverwrite.d()), null);
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        private ChannelPermissionOverwrites(Integer num, String str, Long l, Long l2) {
            this.type = num;
            this.f2732id = str;
            this.allow = l;
            this.deny = l2;
        }

        public static final ChannelPermissionOverwrites createForMember(long j, Long l, Long l2) {
            return Companion.createForMember(j, l, l2);
        }

        public static final ChannelPermissionOverwrites createForRole(long j, Long l, Long l2) {
            return Companion.createForRole(j, l, l2);
        }

        public /* synthetic */ ChannelPermissionOverwrites(Integer num, String str, Long l, Long l2, DefaultConstructorMarker defaultConstructorMarker) {
            this(num, str, l, l2);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\r\u0018\u0000 \u00122\u00020\u0001:\u0002\u0012\u0013B!\u0012\u0006\u0010\f\u001a\u00020\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0007¢\u0006\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u001b\u0010\b\u001a\u0004\u0018\u00010\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR\u0019\u0010\f\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000f¨\u0006\u0014"}, d2 = {"Lcom/discord/restapi/RestAPIParams$ChannelPosition;", "", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "I", "getPosition", "()I", "", "parentId", "Ljava/lang/Long;", "getParentId", "()Ljava/lang/Long;", ModelAuditLogEntry.CHANGE_KEY_ID, "J", "getId", "()J", HookHelper.constructorName, "(JILjava/lang/Long;)V", "Companion", "Serializer", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ChannelPosition {
        public static final Companion Companion = new Companion(null);
        public static final long PARENT_ID_NONE = -1;

        /* renamed from: id  reason: collision with root package name */
        private final long f2733id;
        private final Long parentId;
        private final int position;

        /* compiled from: RestAPIParams.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/restapi/RestAPIParams$ChannelPosition$Companion;", "", "", "PARENT_ID_NONE", "J", HookHelper.constructorName, "()V", "restapi_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* compiled from: RestAPIParams.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007¢\u0006\u0004\b\u000b\u0010\fJ/\u0010\t\u001a\u0004\u0018\u00010\b2\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\b\u0010\u0007\u001a\u0004\u0018\u00010\u0006H\u0016¢\u0006\u0004\b\t\u0010\n¨\u0006\r"}, d2 = {"Lcom/discord/restapi/RestAPIParams$ChannelPosition$Serializer;", "Lb/i/d/m;", "Lcom/discord/restapi/RestAPIParams$ChannelPosition;", "src", "Ljava/lang/reflect/Type;", "typeOfSrc", "Lb/i/d/l;", "context", "Lcom/google/gson/JsonElement;", "serialize", "(Lcom/discord/restapi/RestAPIParams$ChannelPosition;Ljava/lang/reflect/Type;Lb/i/d/l;)Lcom/google/gson/JsonElement;", HookHelper.constructorName, "()V", "restapi_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Serializer implements b.i.d.m<ChannelPosition> {
            public JsonElement serialize(ChannelPosition channelPosition, Type type, l lVar) {
                if (channelPosition == null || (!m.areEqual(type, ChannelPosition.class))) {
                    return null;
                }
                JsonObject jsonObject = new JsonObject();
                jsonObject.i(ModelAuditLogEntry.CHANGE_KEY_ID, Long.valueOf(channelPosition.getId()));
                jsonObject.i(ModelAuditLogEntry.CHANGE_KEY_POSITION, Integer.valueOf(channelPosition.getPosition()));
                Long parentId = channelPosition.getParentId();
                if (parentId != null) {
                    long longValue = parentId.longValue();
                    if (longValue == -1) {
                        jsonObject.a.put("parent_id", j.a);
                    } else {
                        jsonObject.i("parent_id", Long.valueOf(longValue));
                    }
                }
                return jsonObject;
            }
        }

        public ChannelPosition(long j, int i, Long l) {
            this.f2733id = j;
            this.position = i;
            this.parentId = l;
        }

        public final long getId() {
            return this.f2733id;
        }

        public final Long getParentId() {
            return this.parentId;
        }

        public final int getPosition() {
            return this.position;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\n\u0010\f\u001a\u00060\u0002j\u0002`\u0003\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\t¢\u0006\u0004\b \u0010!J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ6\u0010\u000f\u001a\u00020\u00002\f\b\u0002\u0010\f\u001a\u00060\u0002j\u0002`\u00032\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\tHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u001a\u0010\u0018\u001a\u00020\u00062\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u001b\u0010\u000e\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001a\u001a\u0004\b\u001b\u0010\u000bR\u001b\u0010\r\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001c\u001a\u0004\b\u001d\u0010\bR\u001d\u0010\f\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001e\u001a\u0004\b\u001f\u0010\u0005¨\u0006\""}, d2 = {"Lcom/discord/restapi/RestAPIParams$ChannelVoiceStateUpdate;", "", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "", "component2", "()Ljava/lang/Boolean;", "Lcom/discord/api/utcdatetime/UtcDateTime;", "component3", "()Lcom/discord/api/utcdatetime/UtcDateTime;", "channelId", "suppress", "requestToSpeakTimestamp", "copy", "(JLjava/lang/Boolean;Lcom/discord/api/utcdatetime/UtcDateTime;)Lcom/discord/restapi/RestAPIParams$ChannelVoiceStateUpdate;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/utcdatetime/UtcDateTime;", "getRequestToSpeakTimestamp", "Ljava/lang/Boolean;", "getSuppress", "J", "getChannelId", HookHelper.constructorName, "(JLjava/lang/Boolean;Lcom/discord/api/utcdatetime/UtcDateTime;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ChannelVoiceStateUpdate {
        private final long channelId;
        private final UtcDateTime requestToSpeakTimestamp;
        private final Boolean suppress;

        public ChannelVoiceStateUpdate(long j, Boolean bool, UtcDateTime utcDateTime) {
            this.channelId = j;
            this.suppress = bool;
            this.requestToSpeakTimestamp = utcDateTime;
        }

        public static /* synthetic */ ChannelVoiceStateUpdate copy$default(ChannelVoiceStateUpdate channelVoiceStateUpdate, long j, Boolean bool, UtcDateTime utcDateTime, int i, Object obj) {
            if ((i & 1) != 0) {
                j = channelVoiceStateUpdate.channelId;
            }
            if ((i & 2) != 0) {
                bool = channelVoiceStateUpdate.suppress;
            }
            if ((i & 4) != 0) {
                utcDateTime = channelVoiceStateUpdate.requestToSpeakTimestamp;
            }
            return channelVoiceStateUpdate.copy(j, bool, utcDateTime);
        }

        public final long component1() {
            return this.channelId;
        }

        public final Boolean component2() {
            return this.suppress;
        }

        public final UtcDateTime component3() {
            return this.requestToSpeakTimestamp;
        }

        public final ChannelVoiceStateUpdate copy(long j, Boolean bool, UtcDateTime utcDateTime) {
            return new ChannelVoiceStateUpdate(j, bool, utcDateTime);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ChannelVoiceStateUpdate)) {
                return false;
            }
            ChannelVoiceStateUpdate channelVoiceStateUpdate = (ChannelVoiceStateUpdate) obj;
            return this.channelId == channelVoiceStateUpdate.channelId && m.areEqual(this.suppress, channelVoiceStateUpdate.suppress) && m.areEqual(this.requestToSpeakTimestamp, channelVoiceStateUpdate.requestToSpeakTimestamp);
        }

        public final long getChannelId() {
            return this.channelId;
        }

        public final UtcDateTime getRequestToSpeakTimestamp() {
            return this.requestToSpeakTimestamp;
        }

        public final Boolean getSuppress() {
            return this.suppress;
        }

        public int hashCode() {
            long j = this.channelId;
            int i = ((int) (j ^ (j >>> 32))) * 31;
            Boolean bool = this.suppress;
            int i2 = 0;
            int hashCode = (i + (bool != null ? bool.hashCode() : 0)) * 31;
            UtcDateTime utcDateTime = this.requestToSpeakTimestamp;
            if (utcDateTime != null) {
                i2 = utcDateTime.hashCode();
            }
            return hashCode + i2;
        }

        public String toString() {
            StringBuilder R = a.R("ChannelVoiceStateUpdate(channelId=");
            R.append(this.channelId);
            R.append(", suppress=");
            R.append(this.suppress);
            R.append(", requestToSpeakTimestamp=");
            R.append(this.requestToSpeakTimestamp);
            R.append(")");
            return R.toString();
        }

        public /* synthetic */ ChannelVoiceStateUpdate(long j, Boolean bool, UtcDateTime utcDateTime, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(j, (i & 2) != 0 ? null : bool, (i & 4) != 0 ? null : utcDateTime);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000f\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0012\b\u0086\b\u0018\u00002\u00020\u0001Bg\u0012\u0006\u0010\u0016\u001a\u00020\u0002\u0012\n\u0010\u0017\u001a\u00060\u0002j\u0002`\u0005\u0012\n\u0010\u0018\u001a\u00060\u0002j\u0002`\u0007\u0012\u000e\u0010\u0019\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\t\u0012\n\u0010\u001a\u001a\u00060\u0002j\u0002`\f\u0012\b\u0010\u001b\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\u001c\u001a\u00020\u000f\u0012\b\u0010\u001d\u001a\u0004\u0018\u00010\u0012\u0012\u0006\u0010\u001e\u001a\u00020\u0012¢\u0006\u0004\b6\u00107J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0014\u0010\u0006\u001a\u00060\u0002j\u0002`\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J\u0014\u0010\b\u001a\u00060\u0002j\u0002`\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\u0004J\u0018\u0010\n\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0014\u0010\r\u001a\u00060\u0002j\u0002`\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u0004J\u0012\u0010\u000e\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000bJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\u0012HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0014J\u0082\u0001\u0010\u001f\u001a\u00020\u00002\b\b\u0002\u0010\u0016\u001a\u00020\u00022\f\b\u0002\u0010\u0017\u001a\u00060\u0002j\u0002`\u00052\f\b\u0002\u0010\u0018\u001a\u00060\u0002j\u0002`\u00072\u0010\b\u0002\u0010\u0019\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\t2\f\b\u0002\u0010\u001a\u001a\u00060\u0002j\u0002`\f2\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\u001c\u001a\u00020\u000f2\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u00122\b\b\u0002\u0010\u001e\u001a\u00020\u0012HÆ\u0001¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010!\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b!\u0010\u0014J\u0010\u0010#\u001a\u00020\"HÖ\u0001¢\u0006\u0004\b#\u0010$J\u001a\u0010'\u001a\u00020&2\b\u0010%\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b'\u0010(R\u001d\u0010\u0017\u001a\u00060\u0002j\u0002`\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010)\u001a\u0004\b*\u0010\u0004R\u0019\u0010\u001c\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010+\u001a\u0004\b,\u0010\u0011R\u001d\u0010\u001a\u001a\u00060\u0002j\u0002`\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010)\u001a\u0004\b-\u0010\u0004R\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010.\u001a\u0004\b/\u0010\u0014R!\u0010\u0019\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u00100\u001a\u0004\b1\u0010\u000bR\u0019\u0010\u001e\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010.\u001a\u0004\b2\u0010\u0014R\u001d\u0010\u0018\u001a\u00060\u0002j\u0002`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010)\u001a\u0004\b3\u0010\u0004R\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u00100\u001a\u0004\b4\u0010\u000bR\u0019\u0010\u0016\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010)\u001a\u0004\b5\u0010\u0004¨\u00068"}, d2 = {"Lcom/discord/restapi/RestAPIParams$ComponentInteraction;", "", "", "component1", "()J", "Lcom/discord/primitives/ChannelId;", "component2", "Lcom/discord/primitives/ApplicationId;", "component3", "Lcom/discord/primitives/GuildId;", "component4", "()Ljava/lang/Long;", "Lcom/discord/primitives/MessageId;", "component5", "component6", "Lcom/discord/restapi/RestAPIParams$ComponentInteractionData;", "component7", "()Lcom/discord/restapi/RestAPIParams$ComponentInteractionData;", "", "component8", "()Ljava/lang/String;", "component9", "type", "channelId", "applicationId", "guildId", "messageId", "messageFlags", "data", "sessionId", "nonce", "copy", "(JJJLjava/lang/Long;JLjava/lang/Long;Lcom/discord/restapi/RestAPIParams$ComponentInteractionData;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/restapi/RestAPIParams$ComponentInteraction;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getChannelId", "Lcom/discord/restapi/RestAPIParams$ComponentInteractionData;", "getData", "getMessageId", "Ljava/lang/String;", "getSessionId", "Ljava/lang/Long;", "getGuildId", "getNonce", "getApplicationId", "getMessageFlags", "getType", HookHelper.constructorName, "(JJJLjava/lang/Long;JLjava/lang/Long;Lcom/discord/restapi/RestAPIParams$ComponentInteractionData;Ljava/lang/String;Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ComponentInteraction {
        private final long applicationId;
        private final long channelId;
        private final ComponentInteractionData data;
        private final Long guildId;
        private final Long messageFlags;
        private final long messageId;
        private final String nonce;
        private final String sessionId;
        private final long type;

        public ComponentInteraction(long j, long j2, long j3, Long l, long j4, Long l2, ComponentInteractionData componentInteractionData, String str, String str2) {
            m.checkNotNullParameter(componentInteractionData, "data");
            m.checkNotNullParameter(str2, "nonce");
            this.type = j;
            this.channelId = j2;
            this.applicationId = j3;
            this.guildId = l;
            this.messageId = j4;
            this.messageFlags = l2;
            this.data = componentInteractionData;
            this.sessionId = str;
            this.nonce = str2;
        }

        public final long component1() {
            return this.type;
        }

        public final long component2() {
            return this.channelId;
        }

        public final long component3() {
            return this.applicationId;
        }

        public final Long component4() {
            return this.guildId;
        }

        public final long component5() {
            return this.messageId;
        }

        public final Long component6() {
            return this.messageFlags;
        }

        public final ComponentInteractionData component7() {
            return this.data;
        }

        public final String component8() {
            return this.sessionId;
        }

        public final String component9() {
            return this.nonce;
        }

        public final ComponentInteraction copy(long j, long j2, long j3, Long l, long j4, Long l2, ComponentInteractionData componentInteractionData, String str, String str2) {
            m.checkNotNullParameter(componentInteractionData, "data");
            m.checkNotNullParameter(str2, "nonce");
            return new ComponentInteraction(j, j2, j3, l, j4, l2, componentInteractionData, str, str2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ComponentInteraction)) {
                return false;
            }
            ComponentInteraction componentInteraction = (ComponentInteraction) obj;
            return this.type == componentInteraction.type && this.channelId == componentInteraction.channelId && this.applicationId == componentInteraction.applicationId && m.areEqual(this.guildId, componentInteraction.guildId) && this.messageId == componentInteraction.messageId && m.areEqual(this.messageFlags, componentInteraction.messageFlags) && m.areEqual(this.data, componentInteraction.data) && m.areEqual(this.sessionId, componentInteraction.sessionId) && m.areEqual(this.nonce, componentInteraction.nonce);
        }

        public final long getApplicationId() {
            return this.applicationId;
        }

        public final long getChannelId() {
            return this.channelId;
        }

        public final ComponentInteractionData getData() {
            return this.data;
        }

        public final Long getGuildId() {
            return this.guildId;
        }

        public final Long getMessageFlags() {
            return this.messageFlags;
        }

        public final long getMessageId() {
            return this.messageId;
        }

        public final String getNonce() {
            return this.nonce;
        }

        public final String getSessionId() {
            return this.sessionId;
        }

        public final long getType() {
            return this.type;
        }

        public int hashCode() {
            long j = this.type;
            long j2 = this.channelId;
            long j3 = this.applicationId;
            int i = ((((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31;
            Long l = this.guildId;
            int i2 = 0;
            int hashCode = l != null ? l.hashCode() : 0;
            long j4 = this.messageId;
            int i3 = (((i + hashCode) * 31) + ((int) (j4 ^ (j4 >>> 32)))) * 31;
            Long l2 = this.messageFlags;
            int hashCode2 = (i3 + (l2 != null ? l2.hashCode() : 0)) * 31;
            ComponentInteractionData componentInteractionData = this.data;
            int hashCode3 = (hashCode2 + (componentInteractionData != null ? componentInteractionData.hashCode() : 0)) * 31;
            String str = this.sessionId;
            int hashCode4 = (hashCode3 + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.nonce;
            if (str2 != null) {
                i2 = str2.hashCode();
            }
            return hashCode4 + i2;
        }

        public String toString() {
            StringBuilder R = a.R("ComponentInteraction(type=");
            R.append(this.type);
            R.append(", channelId=");
            R.append(this.channelId);
            R.append(", applicationId=");
            R.append(this.applicationId);
            R.append(", guildId=");
            R.append(this.guildId);
            R.append(", messageId=");
            R.append(this.messageId);
            R.append(", messageFlags=");
            R.append(this.messageFlags);
            R.append(", data=");
            R.append(this.data);
            R.append(", sessionId=");
            R.append(this.sessionId);
            R.append(", nonce=");
            return a.H(R, this.nonce, ")");
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/restapi/RestAPIParams$ComponentInteractionData;", "", HookHelper.constructorName, "()V", "ButtonComponentInteractionData", "SelectComponentInteractionData", "Lcom/discord/restapi/RestAPIParams$ComponentInteractionData$ButtonComponentInteractionData;", "Lcom/discord/restapi/RestAPIParams$ComponentInteractionData$SelectComponentInteractionData;", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class ComponentInteractionData {

        /* compiled from: RestAPIParams.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\b\b\u0002\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\f\u0010\u0007J\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0015\u001a\u0004\b\u0016\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0017\u001a\u0004\b\u0018\u0010\u0004¨\u0006\u001b"}, d2 = {"Lcom/discord/restapi/RestAPIParams$ComponentInteractionData$ButtonComponentInteractionData;", "Lcom/discord/restapi/RestAPIParams$ComponentInteractionData;", "Lcom/discord/api/botuikit/ComponentType;", "component1", "()Lcom/discord/api/botuikit/ComponentType;", "", "component2", "()Ljava/lang/String;", "componentType", "customId", "copy", "(Lcom/discord/api/botuikit/ComponentType;Ljava/lang/String;)Lcom/discord/restapi/RestAPIParams$ComponentInteractionData$ButtonComponentInteractionData;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getCustomId", "Lcom/discord/api/botuikit/ComponentType;", "getComponentType", HookHelper.constructorName, "(Lcom/discord/api/botuikit/ComponentType;Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class ButtonComponentInteractionData extends ComponentInteractionData {
            private final ComponentType componentType;
            private final String customId;

            public /* synthetic */ ButtonComponentInteractionData(ComponentType componentType, String str, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this((i & 1) != 0 ? ComponentType.BUTTON : componentType, str);
            }

            public static /* synthetic */ ButtonComponentInteractionData copy$default(ButtonComponentInteractionData buttonComponentInteractionData, ComponentType componentType, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    componentType = buttonComponentInteractionData.componentType;
                }
                if ((i & 2) != 0) {
                    str = buttonComponentInteractionData.customId;
                }
                return buttonComponentInteractionData.copy(componentType, str);
            }

            public final ComponentType component1() {
                return this.componentType;
            }

            public final String component2() {
                return this.customId;
            }

            public final ButtonComponentInteractionData copy(ComponentType componentType, String str) {
                m.checkNotNullParameter(componentType, "componentType");
                m.checkNotNullParameter(str, "customId");
                return new ButtonComponentInteractionData(componentType, str);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof ButtonComponentInteractionData)) {
                    return false;
                }
                ButtonComponentInteractionData buttonComponentInteractionData = (ButtonComponentInteractionData) obj;
                return m.areEqual(this.componentType, buttonComponentInteractionData.componentType) && m.areEqual(this.customId, buttonComponentInteractionData.customId);
            }

            public final ComponentType getComponentType() {
                return this.componentType;
            }

            public final String getCustomId() {
                return this.customId;
            }

            public int hashCode() {
                ComponentType componentType = this.componentType;
                int i = 0;
                int hashCode = (componentType != null ? componentType.hashCode() : 0) * 31;
                String str = this.customId;
                if (str != null) {
                    i = str.hashCode();
                }
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("ButtonComponentInteractionData(componentType=");
                R.append(this.componentType);
                R.append(", customId=");
                return a.H(R, this.customId, ")");
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public ButtonComponentInteractionData(ComponentType componentType, String str) {
                super(null);
                m.checkNotNullParameter(componentType, "componentType");
                m.checkNotNullParameter(str, "customId");
                this.componentType = componentType;
                this.customId = str;
            }
        }

        /* compiled from: RestAPIParams.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B'\u0012\b\b\u0002\u0010\u000b\u001a\u00020\u0002\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00050\b¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0016\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00050\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ4\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\u00052\u000e\b\u0002\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00050\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0007J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u001a\u0010\u0017\u001a\u00020\u00162\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u001f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00050\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u0019\u001a\u0004\b\u001a\u0010\nR\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001b\u001a\u0004\b\u001c\u0010\u0004R\u0019\u0010\f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001d\u001a\u0004\b\u001e\u0010\u0007¨\u0006!"}, d2 = {"Lcom/discord/restapi/RestAPIParams$ComponentInteractionData$SelectComponentInteractionData;", "Lcom/discord/restapi/RestAPIParams$ComponentInteractionData;", "Lcom/discord/api/botuikit/ComponentType;", "component1", "()Lcom/discord/api/botuikit/ComponentType;", "", "component2", "()Ljava/lang/String;", "", "component3", "()Ljava/util/List;", "componentType", "customId", "values", "copy", "(Lcom/discord/api/botuikit/ComponentType;Ljava/lang/String;Ljava/util/List;)Lcom/discord/restapi/RestAPIParams$ComponentInteractionData$SelectComponentInteractionData;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getValues", "Lcom/discord/api/botuikit/ComponentType;", "getComponentType", "Ljava/lang/String;", "getCustomId", HookHelper.constructorName, "(Lcom/discord/api/botuikit/ComponentType;Ljava/lang/String;Ljava/util/List;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class SelectComponentInteractionData extends ComponentInteractionData {
            private final ComponentType componentType;
            private final String customId;
            private final List<String> values;

            public /* synthetic */ SelectComponentInteractionData(ComponentType componentType, String str, List list, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this((i & 1) != 0 ? ComponentType.SELECT : componentType, str, list);
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ SelectComponentInteractionData copy$default(SelectComponentInteractionData selectComponentInteractionData, ComponentType componentType, String str, List list, int i, Object obj) {
                if ((i & 1) != 0) {
                    componentType = selectComponentInteractionData.componentType;
                }
                if ((i & 2) != 0) {
                    str = selectComponentInteractionData.customId;
                }
                if ((i & 4) != 0) {
                    list = selectComponentInteractionData.values;
                }
                return selectComponentInteractionData.copy(componentType, str, list);
            }

            public final ComponentType component1() {
                return this.componentType;
            }

            public final String component2() {
                return this.customId;
            }

            public final List<String> component3() {
                return this.values;
            }

            public final SelectComponentInteractionData copy(ComponentType componentType, String str, List<String> list) {
                m.checkNotNullParameter(componentType, "componentType");
                m.checkNotNullParameter(str, "customId");
                m.checkNotNullParameter(list, "values");
                return new SelectComponentInteractionData(componentType, str, list);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof SelectComponentInteractionData)) {
                    return false;
                }
                SelectComponentInteractionData selectComponentInteractionData = (SelectComponentInteractionData) obj;
                return m.areEqual(this.componentType, selectComponentInteractionData.componentType) && m.areEqual(this.customId, selectComponentInteractionData.customId) && m.areEqual(this.values, selectComponentInteractionData.values);
            }

            public final ComponentType getComponentType() {
                return this.componentType;
            }

            public final String getCustomId() {
                return this.customId;
            }

            public final List<String> getValues() {
                return this.values;
            }

            public int hashCode() {
                ComponentType componentType = this.componentType;
                int i = 0;
                int hashCode = (componentType != null ? componentType.hashCode() : 0) * 31;
                String str = this.customId;
                int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
                List<String> list = this.values;
                if (list != null) {
                    i = list.hashCode();
                }
                return hashCode2 + i;
            }

            public String toString() {
                StringBuilder R = a.R("SelectComponentInteractionData(componentType=");
                R.append(this.componentType);
                R.append(", customId=");
                R.append(this.customId);
                R.append(", values=");
                return a.K(R, this.values, ")");
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public SelectComponentInteractionData(ComponentType componentType, String str, List<String> list) {
                super(null);
                m.checkNotNullParameter(componentType, "componentType");
                m.checkNotNullParameter(str, "customId");
                m.checkNotNullParameter(list, "values");
                this.componentType = componentType;
                this.customId = str;
                this.values = list;
            }
        }

        private ComponentInteractionData() {
        }

        public /* synthetic */ ComponentInteractionData(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0007\u0018\u0000 \u00122\u00020\u0001:\u0001\u0012BI\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\n\u001a\u00020\u0005\u0012\u0006\u0010\u000b\u001a\u00020\u0005\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\u0006\u0010\b\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u0010\u0010\u0011R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007R\u0016\u0010\b\u001a\u00020\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\u0007R\u0016\u0010\t\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\t\u0010\u0004R\u0016\u0010\n\u001a\u00020\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u0007R\u0016\u0010\u000b\u001a\u00020\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\u0007R\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0016\u0010\u000f\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0004¨\u0006\u0013"}, d2 = {"Lcom/discord/restapi/RestAPIParams$ConnectedAccount;", "", "", ModelAuditLogEntry.CHANGE_KEY_NAME, "Ljava/lang/String;", "", "friend_sync", "Z", "verified", ModelAuditLogEntry.CHANGE_KEY_ID, "revoked", "show_activity", "", "visibility", "I", "type", HookHelper.constructorName, "(ZLjava/lang/String;Ljava/lang/String;ZZLjava/lang/String;ZI)V", "Companion", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ConnectedAccount {
        public static final Companion Companion = new Companion(null);
        private final boolean friend_sync;

        /* renamed from: id  reason: collision with root package name */
        private final String f2734id;
        private final String name;
        private final boolean revoked;
        private final boolean show_activity;
        private final String type;
        private final boolean verified;
        private final int visibility;

        /* compiled from: RestAPIParams.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rJ-\u0010\n\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/restapi/RestAPIParams$ConnectedAccount$Companion;", "", "Lcom/discord/api/connectedaccounts/ConnectedAccount;", "connectedAccount", "", "syncFriends", "showActivity", "", "visibility", "Lcom/discord/restapi/RestAPIParams$ConnectedAccount;", "create", "(Lcom/discord/api/connectedaccounts/ConnectedAccount;ZZI)Lcom/discord/restapi/RestAPIParams$ConnectedAccount;", HookHelper.constructorName, "()V", "restapi_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Companion {
            private Companion() {
            }

            public final ConnectedAccount create(com.discord.api.connectedaccounts.ConnectedAccount connectedAccount, boolean z2, boolean z3, int i) {
                m.checkNotNullParameter(connectedAccount, "connectedAccount");
                return new ConnectedAccount(z2, connectedAccount.b(), connectedAccount.d(), connectedAccount.e(), z3, connectedAccount.g(), connectedAccount.h(), i);
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public ConnectedAccount(boolean z2, String str, String str2, boolean z3, boolean z4, String str3, boolean z5, int i) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_ID);
            m.checkNotNullParameter(str3, "type");
            this.friend_sync = z2;
            this.f2734id = str;
            this.name = str2;
            this.revoked = z3;
            this.show_activity = z4;
            this.type = str3;
            this.verified = z5;
            this.visibility = i;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0015\u0010\u0016J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÂ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J&\u0010\n\u001a\u00020\u00002\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\f\u0010\u0004J\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0011\u001a\u00020\u00052\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0016\u0010\t\u001a\u00020\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\t\u0010\u0013R\u0018\u0010\b\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\u0014¨\u0006\u0017"}, d2 = {"Lcom/discord/restapi/RestAPIParams$ConnectedAccountContacts;", "", "", "component1", "()Ljava/lang/String;", "", "component2", "()Z", ModelAuditLogEntry.CHANGE_KEY_NAME, "friend_sync", "copy", "(Ljava/lang/String;Z)Lcom/discord/restapi/RestAPIParams$ConnectedAccountContacts;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;Z)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ConnectedAccountContacts {
        private final boolean friend_sync;
        private final String name;

        public ConnectedAccountContacts(String str, boolean z2) {
            this.name = str;
            this.friend_sync = z2;
        }

        private final String component1() {
            return this.name;
        }

        private final boolean component2() {
            return this.friend_sync;
        }

        public static /* synthetic */ ConnectedAccountContacts copy$default(ConnectedAccountContacts connectedAccountContacts, String str, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                str = connectedAccountContacts.name;
            }
            if ((i & 2) != 0) {
                z2 = connectedAccountContacts.friend_sync;
            }
            return connectedAccountContacts.copy(str, z2);
        }

        public final ConnectedAccountContacts copy(String str, boolean z2) {
            return new ConnectedAccountContacts(str, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ConnectedAccountContacts)) {
                return false;
            }
            ConnectedAccountContacts connectedAccountContacts = (ConnectedAccountContacts) obj;
            return m.areEqual(this.name, connectedAccountContacts.name) && this.friend_sync == connectedAccountContacts.friend_sync;
        }

        public int hashCode() {
            String str = this.name;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            boolean z2 = this.friend_sync;
            if (z2) {
                z2 = true;
            }
            int i = z2 ? 1 : 0;
            int i2 = z2 ? 1 : 0;
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("ConnectedAccountContacts(name=");
            R.append(this.name);
            R.append(", friend_sync=");
            return a.M(R, this.friend_sync, ")");
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001c\u0010\u0006\u001a\u00020\u00002\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000e\u001a\u00020\r2\b\u0010\f\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u000e\u0010\u000fR\u0018\u0010\u0005\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0010¨\u0006\u0013"}, d2 = {"Lcom/discord/restapi/RestAPIParams$ConnectedAccountNameOnly;", "", "", "component1", "()Ljava/lang/String;", ModelAuditLogEntry.CHANGE_KEY_NAME, "copy", "(Ljava/lang/String;)Lcom/discord/restapi/RestAPIParams$ConnectedAccountNameOnly;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ConnectedAccountNameOnly {
        private final String name;

        public ConnectedAccountNameOnly() {
            this(null, 1, null);
        }

        public ConnectedAccountNameOnly(String str) {
            this.name = str;
        }

        private final String component1() {
            return this.name;
        }

        public static /* synthetic */ ConnectedAccountNameOnly copy$default(ConnectedAccountNameOnly connectedAccountNameOnly, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                str = connectedAccountNameOnly.name;
            }
            return connectedAccountNameOnly.copy(str);
        }

        public final ConnectedAccountNameOnly copy(String str) {
            return new ConnectedAccountNameOnly(str);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof ConnectedAccountNameOnly) && m.areEqual(this.name, ((ConnectedAccountNameOnly) obj).name);
            }
            return true;
        }

        public int hashCode() {
            String str = this.name;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return a.H(a.R("ConnectedAccountNameOnly(name="), this.name, ")");
        }

        public /* synthetic */ ConnectedAccountNameOnly(String str, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : str);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\u0018\u00002\u00020\u0001B/\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\n\u0010\u000bR\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0018\u0010\u0006\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007R\u0018\u0010\b\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\u0007R\u0018\u0010\t\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\t\u0010\u0004¨\u0006\f"}, d2 = {"Lcom/discord/restapi/RestAPIParams$ConnectionState;", "", "", "fromContinuation", "Ljava/lang/Boolean;", "", "state", "Ljava/lang/String;", ModelAuditLogEntry.CHANGE_KEY_CODE, "insecure", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ConnectionState {
        private final String code;
        private final Boolean fromContinuation;
        private final Boolean insecure;
        private final String state;

        public ConnectionState(String str, String str2, Boolean bool, Boolean bool2) {
            this.code = str;
            this.state = str2;
            this.fromContinuation = bool;
            this.insecure = bool2;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001\tB'\u0012\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0007\u0010\bR\u001c\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0004\u0010\u0005R\u001c\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0005¨\u0006\n"}, d2 = {"Lcom/discord/restapi/RestAPIParams$Consents;", "", "", "", "grant", "Ljava/util/List;", "revoke", HookHelper.constructorName, "(Ljava/util/List;Ljava/util/List;)V", "Type", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Consents {
        private final List<String> grant;
        private final List<String> revoke;

        /* compiled from: RestAPIParams.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/restapi/RestAPIParams$Consents$Type;", "", "", "PERSONALIZATION", "Ljava/lang/String;", "USAGE_STATS", HookHelper.constructorName, "()V", "restapi_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Type {
            public static final Type INSTANCE = new Type();
            public static final String PERSONALIZATION = "personalization";
            public static final String USAGE_STATS = "usage_statistics";

            private Type() {
            }
        }

        public Consents() {
            this(null, null, 3, null);
        }

        public Consents(List<String> list, List<String> list2) {
            m.checkNotNullParameter(list, "grant");
            m.checkNotNullParameter(list2, "revoke");
            this.grant = list;
            this.revoke = list2;
        }

        public /* synthetic */ Consents(List list, List list2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? n.emptyList() : list, (i & 2) != 0 ? n.emptyList() : list2);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\b\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\b\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\t\u0010\nR\"\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00030\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0004\u0010\u0005R\u0016\u0010\u0006\u001a\u00020\u00038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007R\u0016\u0010\b\u001a\u00020\u00038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\u0007¨\u0006\u000b"}, d2 = {"Lcom/discord/restapi/RestAPIParams$ContactEntry;", "", "", "", "metadata", "Ljava/util/Map;", "friend_id", "Ljava/lang/String;", "mutual_id_hash", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ContactEntry {
        private final String friend_id;
        private final Map<String, String> metadata;
        private final String mutual_id_hash;

        public ContactEntry(String str, String str2, Map<String, String> map) {
            m.checkNotNullParameter(str, "mutual_id_hash");
            m.checkNotNullParameter(str2, "friend_id");
            m.checkNotNullParameter(map, "metadata");
            this.mutual_id_hash = str;
            this.friend_id = str2;
            this.metadata = map;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\u0005\u001a\u00020\u0002\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\u0010\b\u0002\u0010\b\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006\u0012\u0010\b\u0002\u0010\f\u001a\n\u0018\u00010\nj\u0004\u0018\u0001`\u000b¢\u0006\u0004\b\u000e\u0010\u000fR\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u001e\u0010\b\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u00068\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\tR\u001e\u0010\f\u001a\n\u0018\u00010\nj\u0004\u0018\u0001`\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\r¨\u0006\u0010"}, d2 = {"Lcom/discord/restapi/RestAPIParams$CreateGuild;", "", "", "icon", "Ljava/lang/String;", ModelAuditLogEntry.CHANGE_KEY_NAME, "", "Lcom/discord/restapi/RestAPIParams$CreateGuildChannel;", "channels", "Ljava/util/List;", "", "Lcom/discord/primitives/ChannelId;", "systemChannelId", "Ljava/lang/Long;", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Long;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class CreateGuild {
        private final List<CreateGuildChannel> channels;
        private final String icon;
        private final String name;
        private final Long systemChannelId;

        public CreateGuild(String str, String str2, List<CreateGuildChannel> list, Long l) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            this.name = str;
            this.icon = str2;
            this.channels = list;
            this.systemChannelId = l;
        }

        public /* synthetic */ CreateGuild(String str, String str2, List list, Long l, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(str, str2, (i & 4) != 0 ? null : list, (i & 8) != 0 ? null : l);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001BM\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\u000e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\n\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0002\u0012\u000e\u0010\u000f\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\r\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\n¢\u0006\u0004\b\u0012\u0010\u0013R\u001e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0004\u0010\u0005R\u0018\u0010\u0006\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0005R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\tR\u0018\u0010\u000b\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u001e\u0010\u000f\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010R\u0018\u0010\u0011\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\f¨\u0006\u0014"}, d2 = {"Lcom/discord/restapi/RestAPIParams$CreateGuildChannel;", "", "", "Lcom/discord/primitives/Snowflake;", ModelAuditLogEntry.CHANGE_KEY_ID, "Ljava/lang/Long;", "parentId", "", "type", "I", "", ModelAuditLogEntry.CHANGE_KEY_TOPIC, "Ljava/lang/String;", "", "Lcom/discord/restapi/RestAPIParams$ChannelPermissionOverwrites;", "permissionOverwrites", "Ljava/util/List;", ModelAuditLogEntry.CHANGE_KEY_NAME, HookHelper.constructorName, "(ILjava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class CreateGuildChannel {

        /* renamed from: id  reason: collision with root package name */
        private final Long f2735id;
        private final String name;
        private final Long parentId;
        private final List<ChannelPermissionOverwrites> permissionOverwrites;
        private final String topic;
        private final int type;

        public CreateGuildChannel(int i, Long l, String str, Long l2, List<ChannelPermissionOverwrites> list, String str2) {
            this.type = i;
            this.f2735id = l;
            this.name = str;
            this.parentId = l2;
            this.permissionOverwrites = list;
            this.topic = str2;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0018\u0010\u0005\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/restapi/RestAPIParams$CreateGuildFromTemplate;", "", "", ModelAuditLogEntry.CHANGE_KEY_NAME, "Ljava/lang/String;", "icon", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class CreateGuildFromTemplate {
        private final String icon;
        private final String name;

        public CreateGuildFromTemplate(String str, String str2) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            this.name = str;
            this.icon = str2;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\b\u001a\u0010\u001bJ\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ4\u0010\f\u001a\u00020\u00002\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0006HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000e\u0010\u0004J\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0013\u001a\u00020\u00062\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u001b\u0010\u000b\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0015\u001a\u0004\b\u0016\u0010\bR\u001b\u0010\t\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0004R\u001b\u0010\n\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0017\u001a\u0004\b\u0019\u0010\u0004¨\u0006\u001c"}, d2 = {"Lcom/discord/restapi/RestAPIParams$CreateGuildRoleSubscriptionGroupListing;", "", "", "component1", "()Ljava/lang/String;", "component2", "", "component3", "()Ljava/lang/Boolean;", "image", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "fullServerGate", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lcom/discord/restapi/RestAPIParams$CreateGuildRoleSubscriptionGroupListing;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Boolean;", "getFullServerGate", "Ljava/lang/String;", "getImage", "getDescription", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class CreateGuildRoleSubscriptionGroupListing {
        private final String description;
        private final Boolean fullServerGate;
        private final String image;

        public CreateGuildRoleSubscriptionGroupListing(String str, String str2, Boolean bool) {
            this.image = str;
            this.description = str2;
            this.fullServerGate = bool;
        }

        public static /* synthetic */ CreateGuildRoleSubscriptionGroupListing copy$default(CreateGuildRoleSubscriptionGroupListing createGuildRoleSubscriptionGroupListing, String str, String str2, Boolean bool, int i, Object obj) {
            if ((i & 1) != 0) {
                str = createGuildRoleSubscriptionGroupListing.image;
            }
            if ((i & 2) != 0) {
                str2 = createGuildRoleSubscriptionGroupListing.description;
            }
            if ((i & 4) != 0) {
                bool = createGuildRoleSubscriptionGroupListing.fullServerGate;
            }
            return createGuildRoleSubscriptionGroupListing.copy(str, str2, bool);
        }

        public final String component1() {
            return this.image;
        }

        public final String component2() {
            return this.description;
        }

        public final Boolean component3() {
            return this.fullServerGate;
        }

        public final CreateGuildRoleSubscriptionGroupListing copy(String str, String str2, Boolean bool) {
            return new CreateGuildRoleSubscriptionGroupListing(str, str2, bool);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof CreateGuildRoleSubscriptionGroupListing)) {
                return false;
            }
            CreateGuildRoleSubscriptionGroupListing createGuildRoleSubscriptionGroupListing = (CreateGuildRoleSubscriptionGroupListing) obj;
            return m.areEqual(this.image, createGuildRoleSubscriptionGroupListing.image) && m.areEqual(this.description, createGuildRoleSubscriptionGroupListing.description) && m.areEqual(this.fullServerGate, createGuildRoleSubscriptionGroupListing.fullServerGate);
        }

        public final String getDescription() {
            return this.description;
        }

        public final Boolean getFullServerGate() {
            return this.fullServerGate;
        }

        public final String getImage() {
            return this.image;
        }

        public int hashCode() {
            String str = this.image;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.description;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            Boolean bool = this.fullServerGate;
            if (bool != null) {
                i = bool.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("CreateGuildRoleSubscriptionGroupListing(image=");
            R.append(this.image);
            R.append(", description=");
            R.append(this.description);
            R.append(", fullServerGate=");
            return a.C(R, this.fullServerGate, ")");
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u001c\b\u0086\b\u0018\u00002\u00020\u0001BA\u0012\u0006\u0010\u0011\u001a\u00020\u0002\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0013\u001a\u0004\u0018\u00010\u0002\u0012\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\u0012\u0006\u0010\u0015\u001a\u00020\u000b\u0012\u0006\u0010\u0016\u001a\u00020\u000e¢\u0006\u0004\b(\u0010)J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J\u0016\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u0007HÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010JV\u0010\u0017\u001a\u00020\u00002\b\b\u0002\u0010\u0011\u001a\u00020\u00022\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00022\u000e\b\u0002\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\b\b\u0002\u0010\u0015\u001a\u00020\u000b2\b\b\u0002\u0010\u0016\u001a\u00020\u000eHÆ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0019\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u0004J\u0010\u0010\u001a\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\u001a\u0010\rJ\u001a\u0010\u001c\u001a\u00020\u000e2\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u0019\u0010\u0015\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u001e\u001a\u0004\b\u001f\u0010\rR\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010 \u001a\u0004\b!\u0010\u0004R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010 \u001a\u0004\b\"\u0010\u0004R\u0019\u0010\u0011\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010 \u001a\u0004\b#\u0010\u0004R\u001f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\b0\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010$\u001a\u0004\b%\u0010\nR\u0019\u0010\u0016\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010&\u001a\u0004\b'\u0010\u0010¨\u0006*"}, d2 = {"Lcom/discord/restapi/RestAPIParams$CreateGuildRoleSubscriptionTierListing;", "", "", "component1", "()Ljava/lang/String;", "component2", "component3", "", "Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefit;", "component4", "()Ljava/util/List;", "", "component5", "()I", "", "component6", "()Z", ModelAuditLogEntry.CHANGE_KEY_NAME, "image", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "benefits", "priceTier", "canAccessAllChannels", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZ)Lcom/discord/restapi/RestAPIParams$CreateGuildRoleSubscriptionTierListing;", "toString", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "I", "getPriceTier", "Ljava/lang/String;", "getImage", "getDescription", "getName", "Ljava/util/List;", "getBenefits", "Z", "getCanAccessAllChannels", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZ)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class CreateGuildRoleSubscriptionTierListing {
        private final List<GuildRoleSubscriptionBenefit> benefits;
        private final boolean canAccessAllChannels;
        private final String description;
        private final String image;
        private final String name;
        private final int priceTier;

        public CreateGuildRoleSubscriptionTierListing(String str, String str2, String str3, List<GuildRoleSubscriptionBenefit> list, int i, boolean z2) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkNotNullParameter(list, "benefits");
            this.name = str;
            this.image = str2;
            this.description = str3;
            this.benefits = list;
            this.priceTier = i;
            this.canAccessAllChannels = z2;
        }

        public static /* synthetic */ CreateGuildRoleSubscriptionTierListing copy$default(CreateGuildRoleSubscriptionTierListing createGuildRoleSubscriptionTierListing, String str, String str2, String str3, List list, int i, boolean z2, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                str = createGuildRoleSubscriptionTierListing.name;
            }
            if ((i2 & 2) != 0) {
                str2 = createGuildRoleSubscriptionTierListing.image;
            }
            String str4 = str2;
            if ((i2 & 4) != 0) {
                str3 = createGuildRoleSubscriptionTierListing.description;
            }
            String str5 = str3;
            List<GuildRoleSubscriptionBenefit> list2 = list;
            if ((i2 & 8) != 0) {
                list2 = createGuildRoleSubscriptionTierListing.benefits;
            }
            List list3 = list2;
            if ((i2 & 16) != 0) {
                i = createGuildRoleSubscriptionTierListing.priceTier;
            }
            int i3 = i;
            if ((i2 & 32) != 0) {
                z2 = createGuildRoleSubscriptionTierListing.canAccessAllChannels;
            }
            return createGuildRoleSubscriptionTierListing.copy(str, str4, str5, list3, i3, z2);
        }

        public final String component1() {
            return this.name;
        }

        public final String component2() {
            return this.image;
        }

        public final String component3() {
            return this.description;
        }

        public final List<GuildRoleSubscriptionBenefit> component4() {
            return this.benefits;
        }

        public final int component5() {
            return this.priceTier;
        }

        public final boolean component6() {
            return this.canAccessAllChannels;
        }

        public final CreateGuildRoleSubscriptionTierListing copy(String str, String str2, String str3, List<GuildRoleSubscriptionBenefit> list, int i, boolean z2) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkNotNullParameter(list, "benefits");
            return new CreateGuildRoleSubscriptionTierListing(str, str2, str3, list, i, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof CreateGuildRoleSubscriptionTierListing)) {
                return false;
            }
            CreateGuildRoleSubscriptionTierListing createGuildRoleSubscriptionTierListing = (CreateGuildRoleSubscriptionTierListing) obj;
            return m.areEqual(this.name, createGuildRoleSubscriptionTierListing.name) && m.areEqual(this.image, createGuildRoleSubscriptionTierListing.image) && m.areEqual(this.description, createGuildRoleSubscriptionTierListing.description) && m.areEqual(this.benefits, createGuildRoleSubscriptionTierListing.benefits) && this.priceTier == createGuildRoleSubscriptionTierListing.priceTier && this.canAccessAllChannels == createGuildRoleSubscriptionTierListing.canAccessAllChannels;
        }

        public final List<GuildRoleSubscriptionBenefit> getBenefits() {
            return this.benefits;
        }

        public final boolean getCanAccessAllChannels() {
            return this.canAccessAllChannels;
        }

        public final String getDescription() {
            return this.description;
        }

        public final String getImage() {
            return this.image;
        }

        public final String getName() {
            return this.name;
        }

        public final int getPriceTier() {
            return this.priceTier;
        }

        public int hashCode() {
            String str = this.name;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.image;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.description;
            int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
            List<GuildRoleSubscriptionBenefit> list = this.benefits;
            if (list != null) {
                i = list.hashCode();
            }
            int i2 = (((hashCode3 + i) * 31) + this.priceTier) * 31;
            boolean z2 = this.canAccessAllChannels;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            return i2 + i3;
        }

        public String toString() {
            StringBuilder R = a.R("CreateGuildRoleSubscriptionTierListing(name=");
            R.append(this.name);
            R.append(", image=");
            R.append(this.image);
            R.append(", description=");
            R.append(this.description);
            R.append(", benefits=");
            R.append(this.benefits);
            R.append(", priceTier=");
            R.append(this.priceTier);
            R.append(", canAccessAllChannels=");
            return a.M(R, this.canAccessAllChannels, ")");
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u000e\n\u0002\u0010\b\n\u0002\b\u0017\b\u0086\b\u0018\u00002\u00020\u0001B_\u0012\u0006\u0010\u0018\u001a\u00020\u0002\u0012\b\u0010\u0019\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\u001a\u001a\u00020\u0006\u0012\u0006\u0010\u001b\u001a\u00020\u0002\u0012\b\u0010\u001c\u001a\u0004\u0018\u00010\u0002\u0012\u000e\u0010\u001d\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\f\u0012\u0006\u0010\u001e\u001a\u00020\u000f\u0012\b\u0010\u001f\u001a\u0004\u0018\u00010\u0012\u0012\b\u0010 \u001a\u0004\u0018\u00010\u0015¢\u0006\u0004\b9\u0010:J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\t\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\u0004J\u0012\u0010\n\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\n\u0010\u0004J\u0018\u0010\r\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0012\u0010\u0016\u001a\u0004\u0018\u00010\u0015HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0017Jz\u0010!\u001a\u00020\u00002\b\b\u0002\u0010\u0018\u001a\u00020\u00022\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\u001a\u001a\u00020\u00062\b\b\u0002\u0010\u001b\u001a\u00020\u00022\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u00022\u0010\b\u0002\u0010\u001d\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\f2\b\b\u0002\u0010\u001e\u001a\u00020\u000f2\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u00122\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u0015HÆ\u0001¢\u0006\u0004\b!\u0010\"J\u0010\u0010#\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b#\u0010\u0004J\u0010\u0010%\u001a\u00020$HÖ\u0001¢\u0006\u0004\b%\u0010&J\u001a\u0010(\u001a\u00020\u00152\b\u0010'\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b(\u0010)R\u0019\u0010\u0018\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010*\u001a\u0004\b+\u0010\u0004R\u0019\u0010\u001e\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010,\u001a\u0004\b-\u0010\u0011R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010*\u001a\u0004\b.\u0010\u0004R\u0019\u0010\u001a\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010/\u001a\u0004\b0\u0010\bR\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00101\u001a\u0004\b2\u0010\u0014R!\u0010\u001d\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00103\u001a\u0004\b4\u0010\u000eR\u0019\u0010\u001b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010*\u001a\u0004\b5\u0010\u0004R\u001b\u0010 \u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b \u00106\u001a\u0004\b7\u0010\u0017R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010*\u001a\u0004\b8\u0010\u0004¨\u0006;"}, d2 = {"Lcom/discord/restapi/RestAPIParams$CreateGuildScheduledEventBody;", "", "", "component1", "()Ljava/lang/String;", "component2", "Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;", "component3", "()Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;", "component4", "component5", "", "Lcom/discord/primitives/ChannelId;", "component6", "()Ljava/lang/Long;", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;", "component7", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityMetadata;", "component8", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityMetadata;", "", "component9", "()Ljava/lang/Boolean;", ModelAuditLogEntry.CHANGE_KEY_NAME, ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "privacyLevel", "scheduledStartTime", "scheduledEndTime", "channelId", "entityType", "entityMetadata", "broadcastToDirectoryChannels", "copy", "(Ljava/lang/String;Ljava/lang/String;Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityMetadata;Ljava/lang/Boolean;)Lcom/discord/restapi/RestAPIParams$CreateGuildScheduledEventBody;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getName", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;", "getEntityType", "getDescription", "Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;", "getPrivacyLevel", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityMetadata;", "getEntityMetadata", "Ljava/lang/Long;", "getChannelId", "getScheduledStartTime", "Ljava/lang/Boolean;", "getBroadcastToDirectoryChannels", "getScheduledEndTime", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityMetadata;Ljava/lang/Boolean;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class CreateGuildScheduledEventBody {
        private final Boolean broadcastToDirectoryChannels;
        private final Long channelId;
        private final String description;
        private final GuildScheduledEventEntityMetadata entityMetadata;
        private final GuildScheduledEventEntityType entityType;
        private final String name;
        private final StageInstancePrivacyLevel privacyLevel;
        private final String scheduledEndTime;
        private final String scheduledStartTime;

        public CreateGuildScheduledEventBody(String str, String str2, StageInstancePrivacyLevel stageInstancePrivacyLevel, String str3, String str4, Long l, GuildScheduledEventEntityType guildScheduledEventEntityType, GuildScheduledEventEntityMetadata guildScheduledEventEntityMetadata, Boolean bool) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkNotNullParameter(stageInstancePrivacyLevel, "privacyLevel");
            m.checkNotNullParameter(str3, "scheduledStartTime");
            m.checkNotNullParameter(guildScheduledEventEntityType, "entityType");
            this.name = str;
            this.description = str2;
            this.privacyLevel = stageInstancePrivacyLevel;
            this.scheduledStartTime = str3;
            this.scheduledEndTime = str4;
            this.channelId = l;
            this.entityType = guildScheduledEventEntityType;
            this.entityMetadata = guildScheduledEventEntityMetadata;
            this.broadcastToDirectoryChannels = bool;
        }

        public final String component1() {
            return this.name;
        }

        public final String component2() {
            return this.description;
        }

        public final StageInstancePrivacyLevel component3() {
            return this.privacyLevel;
        }

        public final String component4() {
            return this.scheduledStartTime;
        }

        public final String component5() {
            return this.scheduledEndTime;
        }

        public final Long component6() {
            return this.channelId;
        }

        public final GuildScheduledEventEntityType component7() {
            return this.entityType;
        }

        public final GuildScheduledEventEntityMetadata component8() {
            return this.entityMetadata;
        }

        public final Boolean component9() {
            return this.broadcastToDirectoryChannels;
        }

        public final CreateGuildScheduledEventBody copy(String str, String str2, StageInstancePrivacyLevel stageInstancePrivacyLevel, String str3, String str4, Long l, GuildScheduledEventEntityType guildScheduledEventEntityType, GuildScheduledEventEntityMetadata guildScheduledEventEntityMetadata, Boolean bool) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkNotNullParameter(stageInstancePrivacyLevel, "privacyLevel");
            m.checkNotNullParameter(str3, "scheduledStartTime");
            m.checkNotNullParameter(guildScheduledEventEntityType, "entityType");
            return new CreateGuildScheduledEventBody(str, str2, stageInstancePrivacyLevel, str3, str4, l, guildScheduledEventEntityType, guildScheduledEventEntityMetadata, bool);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof CreateGuildScheduledEventBody)) {
                return false;
            }
            CreateGuildScheduledEventBody createGuildScheduledEventBody = (CreateGuildScheduledEventBody) obj;
            return m.areEqual(this.name, createGuildScheduledEventBody.name) && m.areEqual(this.description, createGuildScheduledEventBody.description) && m.areEqual(this.privacyLevel, createGuildScheduledEventBody.privacyLevel) && m.areEqual(this.scheduledStartTime, createGuildScheduledEventBody.scheduledStartTime) && m.areEqual(this.scheduledEndTime, createGuildScheduledEventBody.scheduledEndTime) && m.areEqual(this.channelId, createGuildScheduledEventBody.channelId) && m.areEqual(this.entityType, createGuildScheduledEventBody.entityType) && m.areEqual(this.entityMetadata, createGuildScheduledEventBody.entityMetadata) && m.areEqual(this.broadcastToDirectoryChannels, createGuildScheduledEventBody.broadcastToDirectoryChannels);
        }

        public final Boolean getBroadcastToDirectoryChannels() {
            return this.broadcastToDirectoryChannels;
        }

        public final Long getChannelId() {
            return this.channelId;
        }

        public final String getDescription() {
            return this.description;
        }

        public final GuildScheduledEventEntityMetadata getEntityMetadata() {
            return this.entityMetadata;
        }

        public final GuildScheduledEventEntityType getEntityType() {
            return this.entityType;
        }

        public final String getName() {
            return this.name;
        }

        public final StageInstancePrivacyLevel getPrivacyLevel() {
            return this.privacyLevel;
        }

        public final String getScheduledEndTime() {
            return this.scheduledEndTime;
        }

        public final String getScheduledStartTime() {
            return this.scheduledStartTime;
        }

        public int hashCode() {
            String str = this.name;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.description;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            StageInstancePrivacyLevel stageInstancePrivacyLevel = this.privacyLevel;
            int hashCode3 = (hashCode2 + (stageInstancePrivacyLevel != null ? stageInstancePrivacyLevel.hashCode() : 0)) * 31;
            String str3 = this.scheduledStartTime;
            int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
            String str4 = this.scheduledEndTime;
            int hashCode5 = (hashCode4 + (str4 != null ? str4.hashCode() : 0)) * 31;
            Long l = this.channelId;
            int hashCode6 = (hashCode5 + (l != null ? l.hashCode() : 0)) * 31;
            GuildScheduledEventEntityType guildScheduledEventEntityType = this.entityType;
            int hashCode7 = (hashCode6 + (guildScheduledEventEntityType != null ? guildScheduledEventEntityType.hashCode() : 0)) * 31;
            GuildScheduledEventEntityMetadata guildScheduledEventEntityMetadata = this.entityMetadata;
            int hashCode8 = (hashCode7 + (guildScheduledEventEntityMetadata != null ? guildScheduledEventEntityMetadata.hashCode() : 0)) * 31;
            Boolean bool = this.broadcastToDirectoryChannels;
            if (bool != null) {
                i = bool.hashCode();
            }
            return hashCode8 + i;
        }

        public String toString() {
            StringBuilder R = a.R("CreateGuildScheduledEventBody(name=");
            R.append(this.name);
            R.append(", description=");
            R.append(this.description);
            R.append(", privacyLevel=");
            R.append(this.privacyLevel);
            R.append(", scheduledStartTime=");
            R.append(this.scheduledStartTime);
            R.append(", scheduledEndTime=");
            R.append(this.scheduledEndTime);
            R.append(", channelId=");
            R.append(this.channelId);
            R.append(", entityType=");
            R.append(this.entityType);
            R.append(", entityMetadata=");
            R.append(this.entityMetadata);
            R.append(", broadcastToDirectoryChannels=");
            return a.C(R, this.broadcastToDirectoryChannels, ")");
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0011\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/restapi/RestAPIParams$DeleteGuild;", "", "", ModelAuditLogEntry.CHANGE_KEY_CODE, "Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class DeleteGuild {
        private final String code;

        public DeleteGuild(String str) {
            this.code = str;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/restapi/RestAPIParams$DeletePhone;", "", "", "password", "Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class DeletePhone {
        private final String password;

        public DeletePhone(String str) {
            m.checkNotNullParameter(str, "password");
            this.password = str;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0018\u0010\u0005\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/restapi/RestAPIParams$DisableAccount;", "", "", "password", "Ljava/lang/String;", ModelAuditLogEntry.CHANGE_KEY_CODE, HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class DisableAccount {
        private final String code;
        private final String password;

        public DisableAccount(String str, String str2) {
            m.checkNotNullParameter(str, "password");
            this.password = str;
            this.code = str2;
        }

        public /* synthetic */ DisableAccount(String str, String str2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(str, (i & 2) != 0 ? null : str2);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0013\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/restapi/RestAPIParams$DisableGuildCommunication;", "", "", "communicationDisabledUntil", "Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class DisableGuildCommunication {
        private final String communicationDisabledUntil;

        public DisableGuildCommunication() {
            this(null, 1, null);
        }

        public DisableGuildCommunication(String str) {
            this.communicationDisabledUntil = str;
        }

        public /* synthetic */ DisableGuildCommunication(String str, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : str);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0007\u001a\u00020\u0002\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J.\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\u0007\u001a\u00020\u00022\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\f\u0010\u0004J\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0014\u001a\u0004\b\u0015\u0010\u0004R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0014\u001a\u0004\b\u0016\u0010\u0004R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0014\u001a\u0004\b\u0017\u0010\u0004¨\u0006\u001a"}, d2 = {"Lcom/discord/restapi/RestAPIParams$DowngradeSubscriptionBody;", "", "", "component1", "()Ljava/lang/String;", "component2", "component3", "subscriptionId", "purchaseToken", "subscriptionSkuId", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/restapi/RestAPIParams$DowngradeSubscriptionBody;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getSubscriptionSkuId", "getSubscriptionId", "getPurchaseToken", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class DowngradeSubscriptionBody {
        private final String purchaseToken;
        private final String subscriptionId;
        private final String subscriptionSkuId;

        public DowngradeSubscriptionBody(String str, String str2, String str3) {
            a.n0(str, "subscriptionId", str2, "purchaseToken", str3, "subscriptionSkuId");
            this.subscriptionId = str;
            this.purchaseToken = str2;
            this.subscriptionSkuId = str3;
        }

        public static /* synthetic */ DowngradeSubscriptionBody copy$default(DowngradeSubscriptionBody downgradeSubscriptionBody, String str, String str2, String str3, int i, Object obj) {
            if ((i & 1) != 0) {
                str = downgradeSubscriptionBody.subscriptionId;
            }
            if ((i & 2) != 0) {
                str2 = downgradeSubscriptionBody.purchaseToken;
            }
            if ((i & 4) != 0) {
                str3 = downgradeSubscriptionBody.subscriptionSkuId;
            }
            return downgradeSubscriptionBody.copy(str, str2, str3);
        }

        public final String component1() {
            return this.subscriptionId;
        }

        public final String component2() {
            return this.purchaseToken;
        }

        public final String component3() {
            return this.subscriptionSkuId;
        }

        public final DowngradeSubscriptionBody copy(String str, String str2, String str3) {
            m.checkNotNullParameter(str, "subscriptionId");
            m.checkNotNullParameter(str2, "purchaseToken");
            m.checkNotNullParameter(str3, "subscriptionSkuId");
            return new DowngradeSubscriptionBody(str, str2, str3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof DowngradeSubscriptionBody)) {
                return false;
            }
            DowngradeSubscriptionBody downgradeSubscriptionBody = (DowngradeSubscriptionBody) obj;
            return m.areEqual(this.subscriptionId, downgradeSubscriptionBody.subscriptionId) && m.areEqual(this.purchaseToken, downgradeSubscriptionBody.purchaseToken) && m.areEqual(this.subscriptionSkuId, downgradeSubscriptionBody.subscriptionSkuId);
        }

        public final String getPurchaseToken() {
            return this.purchaseToken;
        }

        public final String getSubscriptionId() {
            return this.subscriptionId;
        }

        public final String getSubscriptionSkuId() {
            return this.subscriptionSkuId;
        }

        public int hashCode() {
            String str = this.subscriptionId;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.purchaseToken;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.subscriptionSkuId;
            if (str3 != null) {
                i = str3.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("DowngradeSubscriptionBody(subscriptionId=");
            R.append(this.subscriptionId);
            R.append(", purchaseToken=");
            R.append(this.purchaseToken);
            R.append(", subscriptionSkuId=");
            return a.H(R, this.subscriptionSkuId, ")");
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/restapi/RestAPIParams$EmptyBody;", "", HookHelper.constructorName, "()V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class EmptyBody {
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u001b\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0018\u0010\u0005\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/restapi/RestAPIParams$EnableIntegration;", "", "", "type", "Ljava/lang/String;", ModelAuditLogEntry.CHANGE_KEY_ID, HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class EnableIntegration {

        /* renamed from: id  reason: collision with root package name */
        private final String f2736id;
        private final String type;

        public EnableIntegration(String str, String str2) {
            this.type = str;
            this.f2736id = str2;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0005\u001a\u00020\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\u0006\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004¨\u0006\t"}, d2 = {"Lcom/discord/restapi/RestAPIParams$EnableMFA;", "", "", "secret", "Ljava/lang/String;", ModelAuditLogEntry.CHANGE_KEY_CODE, "password", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class EnableMFA {
        private final String code;
        private final String password;
        private final String secret;

        public EnableMFA(String str, String str2, String str3) {
            a.n0(str, ModelAuditLogEntry.CHANGE_KEY_CODE, str2, "secret", str3, "password");
            this.code = str;
            this.secret = str2;
            this.password = str3;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u001b\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0018\u0010\u0005\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/restapi/RestAPIParams$ForgotPassword;", "", "", "captchaKey", "Ljava/lang/String;", "login", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ForgotPassword {
        private final String captchaKey;
        private final String login;

        public ForgotPassword(String str, String str2) {
            this.login = str;
            this.captchaKey = str2;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B#\u0012\n\u0010\t\u001a\u00060\u0002j\u0002`\b\u0012\u000e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003¢\u0006\u0004\b\r\u0010\u000eR!\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007R\u001d\u0010\t\u001a\u00060\u0002j\u0002`\b8\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\f¨\u0006\u000f"}, d2 = {"Lcom/discord/restapi/RestAPIParams$GenerateGiftCode;", "", "", "Lcom/discord/primitives/PlanId;", "subscriptionPlanId", "Ljava/lang/Long;", "getSubscriptionPlanId", "()Ljava/lang/Long;", "Lcom/discord/primitives/SkuId;", "skuId", "J", "getSkuId", "()J", HookHelper.constructorName, "(JLjava/lang/Long;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class GenerateGiftCode {
        private final long skuId;
        private final Long subscriptionPlanId;

        public GenerateGiftCode(long j, Long l) {
            this.skuId = j;
            this.subscriptionPlanId = l;
        }

        public final long getSkuId() {
            return this.skuId;
        }

        public final Long getSubscriptionPlanId() {
            return this.subscriptionPlanId;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0015\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u001c\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0004\u0010\u0005¨\u0006\b"}, d2 = {"Lcom/discord/restapi/RestAPIParams$GreetMessage;", "", "", "", "sticker_ids", "Ljava/util/List;", HookHelper.constructorName, "(Ljava/util/List;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class GreetMessage {
        private final List<Long> sticker_ids;

        public GreetMessage(List<Long> list) {
            m.checkNotNullParameter(list, "sticker_ids");
            this.sticker_ids = list;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u001b\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0018\u0010\u0005\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/restapi/RestAPIParams$GroupDM;", "", "", "icon", "Ljava/lang/String;", ModelAuditLogEntry.CHANGE_KEY_NAME, HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class GroupDM {
        private final String icon;
        private final String name;

        public GroupDM(String str, String str2) {
            this.name = str;
            this.icon = str2;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0019\u0012\u0010\u0010\u0005\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002¢\u0006\u0004\b\t\u0010\nR#\u0010\u0005\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0006\u001a\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/restapi/RestAPIParams$GuildBoosting;", "", "", "", "Lcom/discord/primitives/GuildBoostSlotId;", "userBoostSlotIds", "Ljava/util/List;", "getUserBoostSlotIds", "()Ljava/util/List;", HookHelper.constructorName, "(Ljava/util/List;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class GuildBoosting {
        private final List<Long> userBoostSlotIds;

        public GuildBoosting(List<Long> list) {
            m.checkNotNullParameter(list, "userBoostSlotIds");
            this.userBoostSlotIds = list;
        }

        public final List<Long> getUserBoostSlotIds() {
            return this.userBoostSlotIds;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\b\u001a\u00020\u0005\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007R\u0016\u0010\b\u001a\u00020\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\u0007¨\u0006\u000b"}, d2 = {"Lcom/discord/restapi/RestAPIParams$GuildIntegration;", "", "", "enableEmoticons", "Z", "", "expireBehavior", "I", "expireGracePeriod", HookHelper.constructorName, "(IIZ)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class GuildIntegration {
        private final boolean enableEmoticons;
        private final int expireBehavior;
        private final int expireGracePeriod;

        public GuildIntegration(int i, int i2, boolean z2) {
            this.expireBehavior = i;
            this.expireGracePeriod = i2;
            this.enableEmoticons = z2;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\b\u0010\tR\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/restapi/RestAPIParams$GuildMFA;", "", "", ModelAuditLogEntry.CHANGE_KEY_CODE, "Ljava/lang/String;", "", "level", "I", HookHelper.constructorName, "(ILjava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class GuildMFA {
        private final String code;
        private final int level;

        public GuildMFA(int i, String str) {
            this.level = i;
            this.code = str;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0006\u0018\u0000 \u00122\u00020\u0001:\u0001\u0012BO\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\u0010\b\u0002\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\r\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0005\u0012\u0010\b\u0002\u0010\u000b\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\n¢\u0006\u0004\b\u0010\u0010\u0011R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0018\u0010\u0006\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007R\u0018\u0010\b\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\u0007R\u001e\u0010\u000b\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u001e\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000f¨\u0006\u0013"}, d2 = {"Lcom/discord/restapi/RestAPIParams$GuildMember;", "", "", ModelAuditLogEntry.CHANGE_KEY_NICK, "Ljava/lang/String;", "", ModelAuditLogEntry.CHANGE_KEY_MUTE, "Ljava/lang/Boolean;", ModelAuditLogEntry.CHANGE_KEY_DEAF, "", "Lcom/discord/primitives/ChannelId;", "channelId", "Ljava/lang/Long;", "", "roles", "Ljava/util/List;", HookHelper.constructorName, "(Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Long;)V", "Companion", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class GuildMember {
        public static final Companion Companion = new Companion(null);
        private final Long channelId;
        private final Boolean deaf;
        private final Boolean mute;
        private final String nick;
        private final List<Long> roles;

        /* compiled from: RestAPIParams.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0007¢\u0006\u0004\b\u0005\u0010\u0006J\u001f\u0010\n\u001a\u00020\u00042\u000e\u0010\t\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007H\u0007¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u000e\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\fH\u0007¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\fH\u0007¢\u0006\u0004\b\u0011\u0010\u000fJ\u001b\u0010\u0014\u001a\u00020\u00042\n\u0010\u0013\u001a\u00060\bj\u0002`\u0012H\u0007¢\u0006\u0004\b\u0014\u0010\u0015¨\u0006\u0018"}, d2 = {"Lcom/discord/restapi/RestAPIParams$GuildMember$Companion;", "", "", ModelAuditLogEntry.CHANGE_KEY_NICK, "Lcom/discord/restapi/RestAPIParams$GuildMember;", "createWithNick", "(Ljava/lang/String;)Lcom/discord/restapi/RestAPIParams$GuildMember;", "", "", "roles", "createWithRoles", "(Ljava/util/List;)Lcom/discord/restapi/RestAPIParams$GuildMember;", "", ModelAuditLogEntry.CHANGE_KEY_MUTE, "createWithMute", "(Z)Lcom/discord/restapi/RestAPIParams$GuildMember;", ModelAuditLogEntry.CHANGE_KEY_DEAF, "createWithDeaf", "Lcom/discord/primitives/ChannelId;", "channelId", "createWithChannelId", "(J)Lcom/discord/restapi/RestAPIParams$GuildMember;", HookHelper.constructorName, "()V", "restapi_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Companion {
            private Companion() {
            }

            public final GuildMember createWithChannelId(long j) {
                return new GuildMember(null, null, null, null, Long.valueOf(j), 15, null);
            }

            public final GuildMember createWithDeaf(boolean z2) {
                return new GuildMember(null, null, null, Boolean.valueOf(z2), null, 23, null);
            }

            public final GuildMember createWithMute(boolean z2) {
                return new GuildMember(null, null, Boolean.valueOf(z2), null, null, 27, null);
            }

            public final GuildMember createWithNick(String str) {
                return new GuildMember(str, null, null, null, null, 30, null);
            }

            public final GuildMember createWithRoles(List<Long> list) {
                return new GuildMember(null, list, null, null, null, 29, null);
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public GuildMember() {
            this(null, null, null, null, null, 31, null);
        }

        public GuildMember(String str, List<Long> list, Boolean bool, Boolean bool2, Long l) {
            this.nick = str;
            this.roles = list;
            this.mute = bool;
            this.deaf = bool2;
            this.channelId = l;
        }

        public static final GuildMember createWithChannelId(long j) {
            return Companion.createWithChannelId(j);
        }

        public static final GuildMember createWithDeaf(boolean z2) {
            return Companion.createWithDeaf(z2);
        }

        public static final GuildMember createWithMute(boolean z2) {
            return Companion.createWithMute(z2);
        }

        public static final GuildMember createWithNick(String str) {
            return Companion.createWithNick(str);
        }

        public static final GuildMember createWithRoles(List<Long> list) {
            return Companion.createWithRoles(list);
        }

        public /* synthetic */ GuildMember(String str, List list, Boolean bool, Boolean bool2, Long l, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : str, (i & 2) != 0 ? null : list, (i & 4) != 0 ? null : bool, (i & 8) != 0 ? null : bool2, (i & 16) != 0 ? null : l);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0019\u0012\u0010\b\u0002\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003¢\u0006\u0004\b\u0006\u0010\u0007R\u001e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0004\u0010\u0005¨\u0006\b"}, d2 = {"Lcom/discord/restapi/RestAPIParams$GuildMemberDisconnect;", "", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Ljava/lang/Long;", HookHelper.constructorName, "(Ljava/lang/Long;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class GuildMemberDisconnect {
        private final Long channelId;

        public GuildMemberDisconnect() {
            this(null, 1, null);
        }

        public GuildMemberDisconnect(Long l) {
            this.channelId = l;
        }

        public /* synthetic */ GuildMemberDisconnect(Long l, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : l);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J$\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u00022\b\b\u0002\u0010\u0007\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\n\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\n\u0010\u0004J\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0010\u001a\u00020\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0006\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0012\u001a\u0004\b\u0013\u0010\u0004R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0012\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/restapi/RestAPIParams$HubWaitlist;", "", "", "component1", "()Ljava/lang/String;", "component2", NotificationCompat.CATEGORY_EMAIL, "school", "copy", "(Ljava/lang/String;Ljava/lang/String;)Lcom/discord/restapi/RestAPIParams$HubWaitlist;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getEmail", "getSchool", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class HubWaitlist {
        private final String email;
        private final String school;

        public HubWaitlist(String str, String str2) {
            m.checkNotNullParameter(str, NotificationCompat.CATEGORY_EMAIL);
            m.checkNotNullParameter(str2, "school");
            this.email = str;
            this.school = str2;
        }

        public static /* synthetic */ HubWaitlist copy$default(HubWaitlist hubWaitlist, String str, String str2, int i, Object obj) {
            if ((i & 1) != 0) {
                str = hubWaitlist.email;
            }
            if ((i & 2) != 0) {
                str2 = hubWaitlist.school;
            }
            return hubWaitlist.copy(str, str2);
        }

        public final String component1() {
            return this.email;
        }

        public final String component2() {
            return this.school;
        }

        public final HubWaitlist copy(String str, String str2) {
            m.checkNotNullParameter(str, NotificationCompat.CATEGORY_EMAIL);
            m.checkNotNullParameter(str2, "school");
            return new HubWaitlist(str, str2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof HubWaitlist)) {
                return false;
            }
            HubWaitlist hubWaitlist = (HubWaitlist) obj;
            return m.areEqual(this.email, hubWaitlist.email) && m.areEqual(this.school, hubWaitlist.school);
        }

        public final String getEmail() {
            return this.email;
        }

        public final String getSchool() {
            return this.school;
        }

        public int hashCode() {
            String str = this.email;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.school;
            if (str2 != null) {
                i = str2.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("HubWaitlist(email=");
            R.append(this.email);
            R.append(", school=");
            return a.H(R, this.school, ")");
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\t\u001a\u00020\b\u0012\u0006\u0010\u000b\u001a\u00020\b\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\f\u0010\rR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0018\u0010\u0006\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007R\u0016\u0010\t\u001a\u00020\b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\t\u0010\nR\u0016\u0010\u000b\u001a\u00020\b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\n¨\u0006\u000e"}, d2 = {"Lcom/discord/restapi/RestAPIParams$Invite;", "", "", ModelAuditLogEntry.CHANGE_KEY_TEMPORARY, "Z", "", "regenerate", "Ljava/lang/String;", "", "maxAge", "I", "maxUses", HookHelper.constructorName, "(IIZLjava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Invite {
        private final int maxAge;
        private final int maxUses;
        private final String regenerate;
        private final boolean temporary;

        public Invite(int i, int i2, boolean z2, String str) {
            this.maxAge = i;
            this.maxUses = i2;
            this.temporary = z2;
            this.regenerate = str;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0011\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/restapi/RestAPIParams$InviteCode;", "", "", "captchaKey", "Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class InviteCode {
        private final String captchaKey;

        public InviteCode(String str) {
            this.captchaKey = str;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\u0006\u0010\n\u001a\u00020\u0005\u0012\u0006\u0010\u000b\u001a\u00020\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J.\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\t\u001a\u00020\u00022\b\b\u0002\u0010\n\u001a\u00020\u00052\b\b\u0002\u0010\u000b\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000e\u0010\u0004J\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0013\u001a\u00020\u00052\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u0019\u0010\u000b\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0015\u001a\u0004\b\u0016\u0010\u0007R\u0019\u0010\n\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0015\u001a\u0004\b\u0017\u0010\u0007R\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0018\u001a\u0004\b\u0019\u0010\u0004¨\u0006\u001c"}, d2 = {"Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;", "", "", "component1", "()Ljava/lang/String;", "", "component2", "()Z", "component3", "subscriptionId", "renewal", "applyEntitlements", "copy", "(Ljava/lang/String;ZZ)Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getApplyEntitlements", "getRenewal", "Ljava/lang/String;", "getSubscriptionId", HookHelper.constructorName, "(Ljava/lang/String;ZZ)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class InvoicePreviewBody {
        private final boolean applyEntitlements;
        private final boolean renewal;
        private final String subscriptionId;

        public InvoicePreviewBody(String str, boolean z2, boolean z3) {
            m.checkNotNullParameter(str, "subscriptionId");
            this.subscriptionId = str;
            this.renewal = z2;
            this.applyEntitlements = z3;
        }

        public static /* synthetic */ InvoicePreviewBody copy$default(InvoicePreviewBody invoicePreviewBody, String str, boolean z2, boolean z3, int i, Object obj) {
            if ((i & 1) != 0) {
                str = invoicePreviewBody.subscriptionId;
            }
            if ((i & 2) != 0) {
                z2 = invoicePreviewBody.renewal;
            }
            if ((i & 4) != 0) {
                z3 = invoicePreviewBody.applyEntitlements;
            }
            return invoicePreviewBody.copy(str, z2, z3);
        }

        public final String component1() {
            return this.subscriptionId;
        }

        public final boolean component2() {
            return this.renewal;
        }

        public final boolean component3() {
            return this.applyEntitlements;
        }

        public final InvoicePreviewBody copy(String str, boolean z2, boolean z3) {
            m.checkNotNullParameter(str, "subscriptionId");
            return new InvoicePreviewBody(str, z2, z3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof InvoicePreviewBody)) {
                return false;
            }
            InvoicePreviewBody invoicePreviewBody = (InvoicePreviewBody) obj;
            return m.areEqual(this.subscriptionId, invoicePreviewBody.subscriptionId) && this.renewal == invoicePreviewBody.renewal && this.applyEntitlements == invoicePreviewBody.applyEntitlements;
        }

        public final boolean getApplyEntitlements() {
            return this.applyEntitlements;
        }

        public final boolean getRenewal() {
            return this.renewal;
        }

        public final String getSubscriptionId() {
            return this.subscriptionId;
        }

        public int hashCode() {
            String str = this.subscriptionId;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            boolean z2 = this.renewal;
            int i = 1;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (hashCode + i2) * 31;
            boolean z3 = this.applyEntitlements;
            if (!z3) {
                i = z3 ? 1 : 0;
            }
            return i4 + i;
        }

        public String toString() {
            StringBuilder R = a.R("InvoicePreviewBody(subscriptionId=");
            R.append(this.subscriptionId);
            R.append(", renewal=");
            R.append(this.renewal);
            R.append(", applyEntitlements=");
            return a.M(R, this.applyEntitlements, ")");
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B\u0011\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u000f\u001a\u00020\u00022\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/restapi/RestAPIParams$LeaveGuildBody;", "", "", "component1", "()Z", "lurking", "copy", "(Z)Lcom/discord/restapi/RestAPIParams$LeaveGuildBody;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getLurking", HookHelper.constructorName, "(Z)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class LeaveGuildBody {
        private final boolean lurking;

        public LeaveGuildBody() {
            this(false, 1, null);
        }

        public LeaveGuildBody(boolean z2) {
            this.lurking = z2;
        }

        public static /* synthetic */ LeaveGuildBody copy$default(LeaveGuildBody leaveGuildBody, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                z2 = leaveGuildBody.lurking;
            }
            return leaveGuildBody.copy(z2);
        }

        public final boolean component1() {
            return this.lurking;
        }

        public final LeaveGuildBody copy(boolean z2) {
            return new LeaveGuildBody(z2);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof LeaveGuildBody) && this.lurking == ((LeaveGuildBody) obj).lurking;
            }
            return true;
        }

        public final boolean getLurking() {
            return this.lurking;
        }

        public int hashCode() {
            boolean z2 = this.lurking;
            if (z2) {
                return 1;
            }
            return z2 ? 1 : 0;
        }

        public String toString() {
            return a.M(a.R("LeaveGuildBody(lurking="), this.lurking, ")");
        }

        public /* synthetic */ LeaveGuildBody(boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? false : z2);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u001b\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0018\u0010\u0005\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/restapi/RestAPIParams$MFALogin;", "", "", "ticket", "Ljava/lang/String;", ModelAuditLogEntry.CHANGE_KEY_CODE, HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class MFALogin {
        private final String code;
        private final String ticket;

        public MFALogin(String str, String str2) {
            this.ticket = str;
            this.code = str2;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B%\u0012\u0010\b\u0002\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\t\u0010\nR\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u001e\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/restapi/RestAPIParams$MemberVerificationForm;", "", "", "version", "Ljava/lang/String;", "", "Lcom/discord/models/domain/ModelMemberVerificationForm$FormField;", "formFields", "Ljava/util/List;", HookHelper.constructorName, "(Ljava/util/List;Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class MemberVerificationForm {
        private final List<ModelMemberVerificationForm.FormField> formFields;
        private final String version;

        public MemberVerificationForm() {
            this(null, null, 3, null);
        }

        public MemberVerificationForm(List<ModelMemberVerificationForm.FormField> list, String str) {
            this.formFields = list;
            this.version = str;
        }

        public /* synthetic */ MemberVerificationForm(List list, String str, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : list, (i & 2) != 0 ? null : str);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0016\b\u0086\b\u0018\u00002\u00020\u0001:\u00039:;Bo\u0012\b\u0010\u0017\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0018\u001a\u0004\u0018\u00010\u0002\u0012\u0010\b\u0002\u0010\u0019\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007\u0012\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\n\u0012\u0010\b\u0002\u0010\u001b\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\r\u0012\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u0010\u0012\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u0013\u0012\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b7\u00108J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0018\u0010\b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0012\u0010\u000b\u001a\u0004\u0018\u00010\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0018\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0012\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0012\u0010\u0016\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0004J|\u0010\u001f\u001a\u00020\u00002\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u00022\u0010\b\u0002\u0010\u0019\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00072\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\n2\u0010\b\u0002\u0010\u001b\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\r2\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u00102\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u00132\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010!\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b!\u0010\u0004J\u0010\u0010#\u001a\u00020\"HÖ\u0001¢\u0006\u0004\b#\u0010$J\u001a\u0010'\u001a\u00020&2\b\u0010%\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b'\u0010(R!\u0010\u0019\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010)\u001a\u0004\b*\u0010\tR\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010+\u001a\u0004\b,\u0010\u0012R\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010-\u001a\u0004\b.\u0010\u0015R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010/\u001a\u0004\b0\u0010\u0004R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u00101\u001a\u0004\b2\u0010\fR!\u0010\u001b\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u00103\u001a\u0004\b4\u0010\u000fR\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010/\u001a\u0004\b5\u0010\u0004R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010/\u001a\u0004\b6\u0010\u0004¨\u0006<"}, d2 = {"Lcom/discord/restapi/RestAPIParams$Message;", "", "", "component1", "()Ljava/lang/String;", "component2", "", "Lcom/discord/primitives/ApplicationId;", "component3", "()Ljava/lang/Long;", "Lcom/discord/restapi/RestAPIParams$Message$Activity;", "component4", "()Lcom/discord/restapi/RestAPIParams$Message$Activity;", "", "component5", "()Ljava/util/List;", "Lcom/discord/restapi/RestAPIParams$Message$MessageReference;", "component6", "()Lcom/discord/restapi/RestAPIParams$Message$MessageReference;", "Lcom/discord/restapi/RestAPIParams$Message$AllowedMentions;", "component7", "()Lcom/discord/restapi/RestAPIParams$Message$AllowedMentions;", "component8", "content", "nonce", "applicationId", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "stickerIds", "messageReference", "allowedMentions", "captchaKey", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Lcom/discord/restapi/RestAPIParams$Message$Activity;Ljava/util/List;Lcom/discord/restapi/RestAPIParams$Message$MessageReference;Lcom/discord/restapi/RestAPIParams$Message$AllowedMentions;Ljava/lang/String;)Lcom/discord/restapi/RestAPIParams$Message;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Long;", "getApplicationId", "Lcom/discord/restapi/RestAPIParams$Message$MessageReference;", "getMessageReference", "Lcom/discord/restapi/RestAPIParams$Message$AllowedMentions;", "getAllowedMentions", "Ljava/lang/String;", "getCaptchaKey", "Lcom/discord/restapi/RestAPIParams$Message$Activity;", "getActivity", "Ljava/util/List;", "getStickerIds", "getContent", "getNonce", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Lcom/discord/restapi/RestAPIParams$Message$Activity;Ljava/util/List;Lcom/discord/restapi/RestAPIParams$Message$MessageReference;Lcom/discord/restapi/RestAPIParams$Message$AllowedMentions;Ljava/lang/String;)V", "Activity", "AllowedMentions", "MessageReference", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Message {
        private final Activity activity;
        private final AllowedMentions allowedMentions;
        private final Long applicationId;
        private final String captchaKey;
        private final String content;
        private final MessageReference messageReference;
        private final String nonce;
        private final List<Long> stickerIds;

        /* compiled from: RestAPIParams.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\f\u001a\u00020\u000b\u0012\n\u0010\t\u001a\u00060\u0002j\u0002`\b\u0012\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0010\u0010\u0011R\u001d\u0010\u0004\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007R\u001d\u0010\t\u001a\u00060\u0002j\u0002`\b8\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0005\u001a\u0004\b\n\u0010\u0007R\u0019\u0010\f\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000f¨\u0006\u0012"}, d2 = {"Lcom/discord/restapi/RestAPIParams$Message$Activity;", "", "", "Lcom/discord/primitives/SessionId;", "sessionId", "Ljava/lang/String;", "getSessionId", "()Ljava/lang/String;", "Lcom/discord/primitives/ActivityPartyId;", "partyId", "getPartyId", "Lcom/discord/api/message/activity/MessageActivityType;", "type", "Lcom/discord/api/message/activity/MessageActivityType;", "getType", "()Lcom/discord/api/message/activity/MessageActivityType;", HookHelper.constructorName, "(Lcom/discord/api/message/activity/MessageActivityType;Ljava/lang/String;Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Activity {
            private final String partyId;
            private final String sessionId;
            private final MessageActivityType type;

            public Activity(MessageActivityType messageActivityType, String str, String str2) {
                m.checkNotNullParameter(messageActivityType, "type");
                m.checkNotNullParameter(str, "partyId");
                m.checkNotNullParameter(str2, "sessionId");
                this.type = messageActivityType;
                this.partyId = str;
                this.sessionId = str2;
            }

            public final String getPartyId() {
                return this.partyId;
            }

            public final String getSessionId() {
                return this.sessionId;
            }

            public final MessageActivityType getType() {
                return this.type;
            }
        }

        /* compiled from: RestAPIParams.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\b\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014BA\u0012\u000e\u0010\t\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u0002\u0012\u000e\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0002\u0012\u000e\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0002\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\r¢\u0006\u0004\b\u0012\u0010\u0013R!\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007R!\u0010\t\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0005\u001a\u0004\b\n\u0010\u0007R!\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0005\u001a\u0004\b\f\u0010\u0007R\u001b\u0010\u000e\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011¨\u0006\u0015"}, d2 = {"Lcom/discord/restapi/RestAPIParams$Message$AllowedMentions;", "", "", "", "roles", "Ljava/util/List;", "getRoles", "()Ljava/util/List;", "", "parse", "getParse", "users", "getUsers", "", "repliedUser", "Ljava/lang/Boolean;", "getRepliedUser", "()Ljava/lang/Boolean;", HookHelper.constructorName, "(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/Boolean;)V", "Companion", "restapi_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class AllowedMentions {
            public static final Companion Companion = new Companion(null);
            private final List<String> parse;
            private final Boolean repliedUser;
            private final List<Long> roles;
            private final List<Long> users;

            /* compiled from: RestAPIParams.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/restapi/RestAPIParams$Message$AllowedMentions$Companion;", "", "Lcom/discord/api/message/allowedmentions/MessageAllowedMentions;", "model", "Lcom/discord/restapi/RestAPIParams$Message$AllowedMentions;", "create", "(Lcom/discord/api/message/allowedmentions/MessageAllowedMentions;)Lcom/discord/restapi/RestAPIParams$Message$AllowedMentions;", HookHelper.constructorName, "()V", "restapi_release"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes.dex */
            public static final class Companion {
                private Companion() {
                }

                public final AllowedMentions create(MessageAllowedMentions messageAllowedMentions) {
                    ArrayList arrayList;
                    m.checkNotNullParameter(messageAllowedMentions, "model");
                    List<MessageAllowedMentionsTypes> a = messageAllowedMentions.a();
                    if (a != null) {
                        arrayList = new ArrayList(o.collectionSizeOrDefault(a, 10));
                        for (MessageAllowedMentionsTypes messageAllowedMentionsTypes : a) {
                            arrayList.add(messageAllowedMentionsTypes.serialize());
                        }
                    } else {
                        arrayList = null;
                    }
                    return new AllowedMentions(arrayList, messageAllowedMentions.d(), messageAllowedMentions.c(), messageAllowedMentions.b());
                }

                public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                    this();
                }
            }

            public AllowedMentions(List<String> list, List<Long> list2, List<Long> list3, Boolean bool) {
                this.parse = list;
                this.users = list2;
                this.roles = list3;
                this.repliedUser = bool;
            }

            public final List<String> getParse() {
                return this.parse;
            }

            public final Boolean getRepliedUser() {
                return this.repliedUser;
            }

            public final List<Long> getRoles() {
                return this.roles;
            }

            public final List<Long> getUsers() {
                return this.users;
            }
        }

        /* compiled from: RestAPIParams.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B3\u0012\u000e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003\u0012\n\u0010\t\u001a\u00060\u0002j\u0002`\b\u0012\u000e\u0010\u000e\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\r¢\u0006\u0004\b\u0010\u0010\u0011R!\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007R\u001d\u0010\t\u001a\u00060\u0002j\u0002`\b8\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\fR!\u0010\u000e\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u0005\u001a\u0004\b\u000f\u0010\u0007¨\u0006\u0012"}, d2 = {"Lcom/discord/restapi/RestAPIParams$Message$MessageReference;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Ljava/lang/Long;", "getGuildId", "()Ljava/lang/Long;", "Lcom/discord/primitives/ChannelId;", "channelId", "J", "getChannelId", "()J", "Lcom/discord/primitives/MessageId;", "messageId", "getMessageId", HookHelper.constructorName, "(Ljava/lang/Long;JLjava/lang/Long;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class MessageReference {
            private final long channelId;
            private final Long guildId;
            private final Long messageId;

            public MessageReference(Long l, long j, Long l2) {
                this.guildId = l;
                this.channelId = j;
                this.messageId = l2;
            }

            public final long getChannelId() {
                return this.channelId;
            }

            public final Long getGuildId() {
                return this.guildId;
            }

            public final Long getMessageId() {
                return this.messageId;
            }
        }

        public Message(String str, String str2, Long l, Activity activity, List<Long> list, MessageReference messageReference, AllowedMentions allowedMentions, String str3) {
            this.content = str;
            this.nonce = str2;
            this.applicationId = l;
            this.activity = activity;
            this.stickerIds = list;
            this.messageReference = messageReference;
            this.allowedMentions = allowedMentions;
            this.captchaKey = str3;
        }

        public final String component1() {
            return this.content;
        }

        public final String component2() {
            return this.nonce;
        }

        public final Long component3() {
            return this.applicationId;
        }

        public final Activity component4() {
            return this.activity;
        }

        public final List<Long> component5() {
            return this.stickerIds;
        }

        public final MessageReference component6() {
            return this.messageReference;
        }

        public final AllowedMentions component7() {
            return this.allowedMentions;
        }

        public final String component8() {
            return this.captchaKey;
        }

        public final Message copy(String str, String str2, Long l, Activity activity, List<Long> list, MessageReference messageReference, AllowedMentions allowedMentions, String str3) {
            return new Message(str, str2, l, activity, list, messageReference, allowedMentions, str3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Message)) {
                return false;
            }
            Message message = (Message) obj;
            return m.areEqual(this.content, message.content) && m.areEqual(this.nonce, message.nonce) && m.areEqual(this.applicationId, message.applicationId) && m.areEqual(this.activity, message.activity) && m.areEqual(this.stickerIds, message.stickerIds) && m.areEqual(this.messageReference, message.messageReference) && m.areEqual(this.allowedMentions, message.allowedMentions) && m.areEqual(this.captchaKey, message.captchaKey);
        }

        public final Activity getActivity() {
            return this.activity;
        }

        public final AllowedMentions getAllowedMentions() {
            return this.allowedMentions;
        }

        public final Long getApplicationId() {
            return this.applicationId;
        }

        public final String getCaptchaKey() {
            return this.captchaKey;
        }

        public final String getContent() {
            return this.content;
        }

        public final MessageReference getMessageReference() {
            return this.messageReference;
        }

        public final String getNonce() {
            return this.nonce;
        }

        public final List<Long> getStickerIds() {
            return this.stickerIds;
        }

        public int hashCode() {
            String str = this.content;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.nonce;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            Long l = this.applicationId;
            int hashCode3 = (hashCode2 + (l != null ? l.hashCode() : 0)) * 31;
            Activity activity = this.activity;
            int hashCode4 = (hashCode3 + (activity != null ? activity.hashCode() : 0)) * 31;
            List<Long> list = this.stickerIds;
            int hashCode5 = (hashCode4 + (list != null ? list.hashCode() : 0)) * 31;
            MessageReference messageReference = this.messageReference;
            int hashCode6 = (hashCode5 + (messageReference != null ? messageReference.hashCode() : 0)) * 31;
            AllowedMentions allowedMentions = this.allowedMentions;
            int hashCode7 = (hashCode6 + (allowedMentions != null ? allowedMentions.hashCode() : 0)) * 31;
            String str3 = this.captchaKey;
            if (str3 != null) {
                i = str3.hashCode();
            }
            return hashCode7 + i;
        }

        public String toString() {
            StringBuilder R = a.R("Message(content=");
            R.append(this.content);
            R.append(", nonce=");
            R.append(this.nonce);
            R.append(", applicationId=");
            R.append(this.applicationId);
            R.append(", activity=");
            R.append(this.activity);
            R.append(", stickerIds=");
            R.append(this.stickerIds);
            R.append(", messageReference=");
            R.append(this.messageReference);
            R.append(", allowedMentions=");
            R.append(this.allowedMentions);
            R.append(", captchaKey=");
            return a.H(R, this.captchaKey, ")");
        }

        public /* synthetic */ Message(String str, String str2, Long l, Activity activity, List list, MessageReference messageReference, AllowedMentions allowedMentions, String str3, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(str, str2, (i & 4) != 0 ? null : l, (i & 8) != 0 ? null : activity, (i & 16) != 0 ? null : list, (i & 32) != 0 ? null : messageReference, (i & 64) != 0 ? null : allowedMentions, (i & 128) != 0 ? null : str3);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0011\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/restapi/RestAPIParams$Nick;", "", "", ModelAuditLogEntry.CHANGE_KEY_NICK, "Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Nick {
        private final String nick;

        public Nick(String str) {
            this.nick = str;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0006"}, d2 = {"Lcom/discord/restapi/RestAPIParams$OAuth2Authorize;", "", HookHelper.constructorName, "()V", "ResponseGet", "ResponsePost", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class OAuth2Authorize {
        public static final OAuth2Authorize INSTANCE = new OAuth2Authorize();

        /* compiled from: RestAPIParams.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B3\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\u0011\u001a\u00020\u0010\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\u0012\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\u0007¢\u0006\u0004\b\u0015\u0010\u0016R\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u001f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\fR\u001f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\n\u001a\u0004\b\u000f\u0010\fR\u0019\u0010\u0011\u001a\u00020\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014¨\u0006\u0017"}, d2 = {"Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponseGet;", "", "Lcom/discord/api/user/User;", "user", "Lcom/discord/api/user/User;", "getUser", "()Lcom/discord/api/user/User;", "", "Lcom/discord/restapi/RestAPIParams$ConnectedAccount;", "connections", "Ljava/util/List;", "getConnections", "()Ljava/util/List;", "Lcom/discord/api/guild/Guild;", "guilds", "getGuilds", "Lcom/discord/api/application/Application;", "application", "Lcom/discord/api/application/Application;", "getApplication", "()Lcom/discord/api/application/Application;", HookHelper.constructorName, "(Lcom/discord/api/user/User;Lcom/discord/api/application/Application;Ljava/util/List;Ljava/util/List;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class ResponseGet {
            private final Application application;
            private final List<ConnectedAccount> connections;
            private final List<Guild> guilds;
            private final User user;

            public ResponseGet(User user, Application application, List<ConnectedAccount> list, List<Guild> list2) {
                m.checkNotNullParameter(user, "user");
                m.checkNotNullParameter(application, "application");
                m.checkNotNullParameter(list, "connections");
                m.checkNotNullParameter(list2, "guilds");
                this.user = user;
                this.application = application;
                this.connections = list;
                this.guilds = list2;
            }

            public final Application getApplication() {
                return this.application;
            }

            public final List<ConnectedAccount> getConnections() {
                return this.connections;
            }

            public final List<Guild> getGuilds() {
                return this.guilds;
            }

            public final User getUser() {
                return this.user;
            }
        }

        /* compiled from: RestAPIParams.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponsePost;", "", "", ModelAuditLogEntry.CHANGE_KEY_LOCATION, "Ljava/lang/String;", "getLocation", "()Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class ResponsePost {
            private final String location;

            public ResponsePost(String str) {
                m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
                this.location = str;
            }

            public final String getLocation() {
                return this.location;
            }
        }

        private OAuth2Authorize() {
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/restapi/RestAPIParams$PatchGuildEmoji;", "", "", ModelAuditLogEntry.CHANGE_KEY_NAME, "Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class PatchGuildEmoji {
        private final String name;

        public PatchGuildEmoji(String str) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            this.name = str;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0007\u0018\u00002\u00020\u0001B%\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0018\u0010\u0005\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0018\u0010\u0006\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004¨\u0006\t"}, d2 = {"Lcom/discord/restapi/RestAPIParams$Phone;", "", "", "captchaKey", "Ljava/lang/String;", "phone", "source", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Phone {
        private final String captchaKey;
        private final String phone;
        private final String source;

        public Phone(String str, String str2, String str3) {
            this.phone = str;
            this.source = str2;
            this.captchaKey = str3;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0005\u001a\u00020\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/restapi/RestAPIParams$PostGuildEmoji;", "", "", "image", "Ljava/lang/String;", ModelAuditLogEntry.CHANGE_KEY_NAME, HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class PostGuildEmoji {
        private final String image;
        private final String name;

        public PostGuildEmoji(String str, String str2) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkNotNullParameter(str2, "image");
            this.name = str;
            this.image = str2;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u001f\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\b\u0010\tR\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0018\u0010\u0006\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/restapi/RestAPIParams$PruneGuild;", "", "", "computePruneCount", "Ljava/lang/Boolean;", "", "days", "Ljava/lang/Integer;", HookHelper.constructorName, "(Ljava/lang/Integer;Ljava/lang/Boolean;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class PruneGuild {
        private final Boolean computePruneCount;
        private final Integer days;

        public PruneGuild() {
            this(null, null, 3, null);
        }

        public PruneGuild(Integer num, Boolean bool) {
            this.days = num;
            this.computePruneCount = bool;
        }

        public /* synthetic */ PruneGuild(Integer num, Boolean bool, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : num, (i & 2) != 0 ? Boolean.FALSE : bool);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u001b\u0012\n\u0010\t\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\n\u001a\u00020\u0006¢\u0006\u0004\b\u0019\u0010\u001aJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ(\u0010\u000b\u001a\u00020\u00002\f\b\u0002\u0010\t\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\n\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\r\u0010\bJ\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u001d\u0010\t\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0015\u001a\u0004\b\u0016\u0010\u0005R\u0019\u0010\n\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0017\u001a\u0004\b\u0018\u0010\b¨\u0006\u001b"}, d2 = {"Lcom/discord/restapi/RestAPIParams$PurchaseMetadataBody;", "", "", "Lcom/discord/primitives/SkuId;", "component1", "()J", "", "component2", "()Ljava/lang/String;", "skuId", "paymentGatewaySkuId", "copy", "(JLjava/lang/String;)Lcom/discord/restapi/RestAPIParams$PurchaseMetadataBody;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getSkuId", "Ljava/lang/String;", "getPaymentGatewaySkuId", HookHelper.constructorName, "(JLjava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class PurchaseMetadataBody {
        private final String paymentGatewaySkuId;
        private final long skuId;

        public PurchaseMetadataBody(long j, String str) {
            m.checkNotNullParameter(str, "paymentGatewaySkuId");
            this.skuId = j;
            this.paymentGatewaySkuId = str;
        }

        public static /* synthetic */ PurchaseMetadataBody copy$default(PurchaseMetadataBody purchaseMetadataBody, long j, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                j = purchaseMetadataBody.skuId;
            }
            if ((i & 2) != 0) {
                str = purchaseMetadataBody.paymentGatewaySkuId;
            }
            return purchaseMetadataBody.copy(j, str);
        }

        public final long component1() {
            return this.skuId;
        }

        public final String component2() {
            return this.paymentGatewaySkuId;
        }

        public final PurchaseMetadataBody copy(long j, String str) {
            m.checkNotNullParameter(str, "paymentGatewaySkuId");
            return new PurchaseMetadataBody(j, str);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof PurchaseMetadataBody)) {
                return false;
            }
            PurchaseMetadataBody purchaseMetadataBody = (PurchaseMetadataBody) obj;
            return this.skuId == purchaseMetadataBody.skuId && m.areEqual(this.paymentGatewaySkuId, purchaseMetadataBody.paymentGatewaySkuId);
        }

        public final String getPaymentGatewaySkuId() {
            return this.paymentGatewaySkuId;
        }

        public final long getSkuId() {
            return this.skuId;
        }

        public int hashCode() {
            long j = this.skuId;
            int i = ((int) (j ^ (j >>> 32))) * 31;
            String str = this.paymentGatewaySkuId;
            return i + (str != null ? str.hashCode() : 0);
        }

        public String toString() {
            StringBuilder R = a.R("PurchaseMetadataBody(skuId=");
            R.append(this.skuId);
            R.append(", paymentGatewaySkuId=");
            return a.H(R, this.paymentGatewaySkuId, ")");
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/restapi/RestAPIParams$RemoteAuthCancel;", "", "", "handshakeToken", "Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class RemoteAuthCancel {
        private final String handshakeToken;

        public RemoteAuthCancel(String str) {
            m.checkNotNullParameter(str, "handshakeToken");
            this.handshakeToken = str;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/restapi/RestAPIParams$RemoteAuthFinish;", "", "", ModelAuditLogEntry.CHANGE_KEY_TEMPORARY, "Z", "", "handshakeToken", "Ljava/lang/String;", HookHelper.constructorName, "(ZLjava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class RemoteAuthFinish {
        private final String handshakeToken;
        private final boolean temporary;

        public RemoteAuthFinish(boolean z2, String str) {
            m.checkNotNullParameter(str, "handshakeToken");
            this.temporary = z2;
            this.handshakeToken = str;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/restapi/RestAPIParams$RemoteAuthInitialize;", "", "", "fingerprint", "Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class RemoteAuthInitialize {
        private final String fingerprint;

        public RemoteAuthInitialize(String str) {
            m.checkNotNullParameter(str, "fingerprint");
            this.fingerprint = str;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B3\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0010\b\u0002\u0010\u0007\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006\u0012\u0010\b\u0002\u0010\n\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\t¢\u0006\u0004\b\u000b\u0010\fR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u001e\u0010\u0007\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00068\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0007\u0010\bR\u001e\u0010\n\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\b¨\u0006\r"}, d2 = {"Lcom/discord/restapi/RestAPIParams$Report;", "", "", ModelAuditLogEntry.CHANGE_KEY_REASON, "I", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Ljava/lang/Long;", "Lcom/discord/primitives/MessageId;", "messageId", HookHelper.constructorName, "(ILjava/lang/Long;Ljava/lang/Long;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Report {
        private final Long channelId;
        private final Long messageId;
        private final int reason;

        public Report(int i, Long l, Long l2) {
            this.reason = i;
            this.channelId = l;
            this.messageId = l2;
        }

        public /* synthetic */ Report(int i, Long l, Long l2, int i2, DefaultConstructorMarker defaultConstructorMarker) {
            this(i, (i2 & 2) != 0 ? null : l, (i2 & 4) != 0 ? null : l2);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0017\u0012\u000e\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u001e\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0004\u0010\u0005¨\u0006\b"}, d2 = {"Lcom/discord/restapi/RestAPIParams$Ring;", "", "", "", "recipients", "Ljava/util/List;", HookHelper.constructorName, "(Ljava/util/List;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Ring {
        private final List<Long> recipients;

        public Ring(List<Long> list) {
            this.recipients = list;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0010\u0018\u0000 02\u00020\u0001:\u00010Bk\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u001a\u0012\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010(\u001a\u0004\u0018\u00010\u0010\u0012\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0010\u0012\u0010\b\u0002\u0010\"\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`!\u0012\b\b\u0002\u0010\u0003\u001a\u00020\u0002\u0012\n\b\u0002\u0010+\u001a\u0004\u0018\u00010\u001a¢\u0006\u0004\b.\u0010/R\"\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR$\u0010\n\u001a\u0004\u0018\u00010\t8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\n\u0010\u000b\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR$\u0010\u0011\u001a\u0004\u0018\u00010\u00108\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R$\u0010\u0017\u001a\u0004\u0018\u00010\t8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0017\u0010\u000b\u001a\u0004\b\u0018\u0010\r\"\u0004\b\u0019\u0010\u000fR$\u0010\u001b\u001a\u0004\u0018\u00010\u001a8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R*\u0010\"\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`!8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%\"\u0004\b&\u0010'R$\u0010(\u001a\u0004\u0018\u00010\u00108\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b(\u0010\u0012\u001a\u0004\b)\u0010\u0014\"\u0004\b*\u0010\u0016R$\u0010+\u001a\u0004\u0018\u00010\u001a8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b+\u0010\u001c\u001a\u0004\b,\u0010\u001e\"\u0004\b-\u0010 ¨\u00061"}, d2 = {"Lcom/discord/restapi/RestAPIParams$Role;", "", "", ModelAuditLogEntry.CHANGE_KEY_ID, "J", "getId", "()J", "setId", "(J)V", "", ModelAuditLogEntry.CHANGE_KEY_HOIST, "Ljava/lang/Boolean;", "getHoist", "()Ljava/lang/Boolean;", "setHoist", "(Ljava/lang/Boolean;)V", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Ljava/lang/Integer;", "getPosition", "()Ljava/lang/Integer;", "setPosition", "(Ljava/lang/Integer;)V", ModelAuditLogEntry.CHANGE_KEY_MENTIONABLE, "getMentionable", "setMentionable", "", ModelAuditLogEntry.CHANGE_KEY_NAME, "Ljava/lang/String;", "getName", "()Ljava/lang/String;", "setName", "(Ljava/lang/String;)V", "Lcom/discord/api/permission/PermissionBit;", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS, "Ljava/lang/Long;", "getPermissions", "()Ljava/lang/Long;", "setPermissions", "(Ljava/lang/Long;)V", ModelAuditLogEntry.CHANGE_KEY_COLOR, "getColor", "setColor", "icon", "getIcon", "setIcon", HookHelper.constructorName, "(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;JLjava/lang/String;)V", "Companion", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Role {
        public static final Companion Companion = new Companion(null);
        private Integer color;
        private Boolean hoist;
        private String icon;

        /* renamed from: id  reason: collision with root package name */
        private long f2737id;
        private Boolean mentionable;
        private String name;
        private Long permissions;
        private Integer position;

        /* compiled from: RestAPIParams.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0005\u0010\u0006J\u001f\u0010\u000b\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\tH\u0007¢\u0006\u0004\b\u000b\u0010\f¨\u0006\u000f"}, d2 = {"Lcom/discord/restapi/RestAPIParams$Role$Companion;", "", "Lcom/discord/api/role/GuildRole;", "role", "Lcom/discord/restapi/RestAPIParams$Role;", "createWithRole", "(Lcom/discord/api/role/GuildRole;)Lcom/discord/restapi/RestAPIParams$Role;", "", ModelAuditLogEntry.CHANGE_KEY_ID, "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "createForPosition", "(JI)Lcom/discord/restapi/RestAPIParams$Role;", HookHelper.constructorName, "()V", "restapi_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Role createForPosition(long j, int i) {
                return new Role(null, null, null, null, Integer.valueOf(i), null, j, null, 175, null);
            }

            public final Role createWithRole(GuildRole guildRole) {
                m.checkNotNullParameter(guildRole, "role");
                return new Role(Boolean.valueOf(guildRole.c()), guildRole.g(), Boolean.valueOf(guildRole.f()), Integer.valueOf(guildRole.b()), Integer.valueOf(guildRole.i()), Long.valueOf(guildRole.h()), guildRole.getId(), null, 128, null);
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Role() {
            this(null, null, null, null, null, null, 0L, null, 255, null);
        }

        public Role(Boolean bool, String str, Boolean bool2, Integer num, Integer num2, Long l, long j, String str2) {
            this.hoist = bool;
            this.name = str;
            this.mentionable = bool2;
            this.color = num;
            this.position = num2;
            this.permissions = l;
            this.f2737id = j;
            this.icon = str2;
        }

        public static final Role createForPosition(long j, int i) {
            return Companion.createForPosition(j, i);
        }

        public static final Role createWithRole(GuildRole guildRole) {
            return Companion.createWithRole(guildRole);
        }

        public final Integer getColor() {
            return this.color;
        }

        public final Boolean getHoist() {
            return this.hoist;
        }

        public final String getIcon() {
            return this.icon;
        }

        public final long getId() {
            return this.f2737id;
        }

        public final Boolean getMentionable() {
            return this.mentionable;
        }

        public final String getName() {
            return this.name;
        }

        public final Long getPermissions() {
            return this.permissions;
        }

        public final Integer getPosition() {
            return this.position;
        }

        public final void setColor(Integer num) {
            this.color = num;
        }

        public final void setHoist(Boolean bool) {
            this.hoist = bool;
        }

        public final void setIcon(String str) {
            this.icon = str;
        }

        public final void setId(long j) {
            this.f2737id = j;
        }

        public final void setMentionable(Boolean bool) {
            this.mentionable = bool;
        }

        public final void setName(String str) {
            this.name = str;
        }

        public final void setPermissions(Long l) {
            this.permissions = l;
        }

        public final void setPosition(Integer num) {
            this.position = num;
        }

        public /* synthetic */ Role(Boolean bool, String str, Boolean bool2, Integer num, Integer num2, Long l, long j, String str2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : bool, (i & 2) != 0 ? null : str, (i & 4) != 0 ? null : bool2, (i & 8) != 0 ? null : num, (i & 16) != 0 ? null : num2, (i & 32) != 0 ? null : l, (i & 64) != 0 ? 0L : j, (i & 128) == 0 ? str2 : null);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0010\b\n\u0002\b\u0011\b\u0086\b\u0018\u00002\u00020\u0001B7\u0012\n\u0010\u0010\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\u0011\u001a\u00020\u0006\u0012\u0006\u0010\u0012\u001a\u00020\t\u0012\u0006\u0010\u0013\u001a\u00020\f\u0012\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\b'\u0010(J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u000f\u0010\bJH\u0010\u0015\u001a\u00020\u00002\f\b\u0002\u0010\u0010\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u0011\u001a\u00020\u00062\b\b\u0002\u0010\u0012\u001a\u00020\t2\b\b\u0002\u0010\u0013\u001a\u00020\f2\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0006HÆ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0017\u0010\bJ\u0010\u0010\u0019\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u001a\u0010\u001c\u001a\u00020\f2\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u001b\u0010\u0014\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u001e\u001a\u0004\b\u001f\u0010\bR\u001d\u0010\u0010\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010 \u001a\u0004\b!\u0010\u0005R\u0019\u0010\u0011\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001e\u001a\u0004\b\"\u0010\bR\u0019\u0010\u0012\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010#\u001a\u0004\b$\u0010\u000bR\u0019\u0010\u0013\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010%\u001a\u0004\b&\u0010\u000e¨\u0006)"}, d2 = {"Lcom/discord/restapi/RestAPIParams$StartStageInstanceBody;", "", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "", "component2", "()Ljava/lang/String;", "Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;", "component3", "()Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;", "", "component4", "()Z", "component5", "channelId", ModelAuditLogEntry.CHANGE_KEY_TOPIC, "privacyLevel", "sendStartNotification", "guildScheduledEventId", "copy", "(JLjava/lang/String;Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;ZLjava/lang/String;)Lcom/discord/restapi/RestAPIParams$StartStageInstanceBody;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getGuildScheduledEventId", "J", "getChannelId", "getTopic", "Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;", "getPrivacyLevel", "Z", "getSendStartNotification", HookHelper.constructorName, "(JLjava/lang/String;Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;ZLjava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class StartStageInstanceBody {
        private final long channelId;
        private final String guildScheduledEventId;
        private final StageInstancePrivacyLevel privacyLevel;
        private final boolean sendStartNotification;
        private final String topic;

        public StartStageInstanceBody(long j, String str, StageInstancePrivacyLevel stageInstancePrivacyLevel, boolean z2, String str2) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_TOPIC);
            m.checkNotNullParameter(stageInstancePrivacyLevel, "privacyLevel");
            this.channelId = j;
            this.topic = str;
            this.privacyLevel = stageInstancePrivacyLevel;
            this.sendStartNotification = z2;
            this.guildScheduledEventId = str2;
        }

        public static /* synthetic */ StartStageInstanceBody copy$default(StartStageInstanceBody startStageInstanceBody, long j, String str, StageInstancePrivacyLevel stageInstancePrivacyLevel, boolean z2, String str2, int i, Object obj) {
            if ((i & 1) != 0) {
                j = startStageInstanceBody.channelId;
            }
            long j2 = j;
            if ((i & 2) != 0) {
                str = startStageInstanceBody.topic;
            }
            String str3 = str;
            if ((i & 4) != 0) {
                stageInstancePrivacyLevel = startStageInstanceBody.privacyLevel;
            }
            StageInstancePrivacyLevel stageInstancePrivacyLevel2 = stageInstancePrivacyLevel;
            if ((i & 8) != 0) {
                z2 = startStageInstanceBody.sendStartNotification;
            }
            boolean z3 = z2;
            if ((i & 16) != 0) {
                str2 = startStageInstanceBody.guildScheduledEventId;
            }
            return startStageInstanceBody.copy(j2, str3, stageInstancePrivacyLevel2, z3, str2);
        }

        public final long component1() {
            return this.channelId;
        }

        public final String component2() {
            return this.topic;
        }

        public final StageInstancePrivacyLevel component3() {
            return this.privacyLevel;
        }

        public final boolean component4() {
            return this.sendStartNotification;
        }

        public final String component5() {
            return this.guildScheduledEventId;
        }

        public final StartStageInstanceBody copy(long j, String str, StageInstancePrivacyLevel stageInstancePrivacyLevel, boolean z2, String str2) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_TOPIC);
            m.checkNotNullParameter(stageInstancePrivacyLevel, "privacyLevel");
            return new StartStageInstanceBody(j, str, stageInstancePrivacyLevel, z2, str2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StartStageInstanceBody)) {
                return false;
            }
            StartStageInstanceBody startStageInstanceBody = (StartStageInstanceBody) obj;
            return this.channelId == startStageInstanceBody.channelId && m.areEqual(this.topic, startStageInstanceBody.topic) && m.areEqual(this.privacyLevel, startStageInstanceBody.privacyLevel) && this.sendStartNotification == startStageInstanceBody.sendStartNotification && m.areEqual(this.guildScheduledEventId, startStageInstanceBody.guildScheduledEventId);
        }

        public final long getChannelId() {
            return this.channelId;
        }

        public final String getGuildScheduledEventId() {
            return this.guildScheduledEventId;
        }

        public final StageInstancePrivacyLevel getPrivacyLevel() {
            return this.privacyLevel;
        }

        public final boolean getSendStartNotification() {
            return this.sendStartNotification;
        }

        public final String getTopic() {
            return this.topic;
        }

        public int hashCode() {
            long j = this.channelId;
            int i = ((int) (j ^ (j >>> 32))) * 31;
            String str = this.topic;
            int i2 = 0;
            int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
            StageInstancePrivacyLevel stageInstancePrivacyLevel = this.privacyLevel;
            int hashCode2 = (hashCode + (stageInstancePrivacyLevel != null ? stageInstancePrivacyLevel.hashCode() : 0)) * 31;
            boolean z2 = this.sendStartNotification;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            int i5 = (hashCode2 + i3) * 31;
            String str2 = this.guildScheduledEventId;
            if (str2 != null) {
                i2 = str2.hashCode();
            }
            return i5 + i2;
        }

        public String toString() {
            StringBuilder R = a.R("StartStageInstanceBody(channelId=");
            R.append(this.channelId);
            R.append(", topic=");
            R.append(this.topic);
            R.append(", privacyLevel=");
            R.append(this.privacyLevel);
            R.append(", sendStartNotification=");
            R.append(this.sendStartNotification);
            R.append(", guildScheduledEventId=");
            return a.H(R, this.guildScheduledEventId, ")");
        }

        public /* synthetic */ StartStageInstanceBody(long j, String str, StageInstancePrivacyLevel stageInstancePrivacyLevel, boolean z2, String str2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(j, str, stageInstancePrivacyLevel, z2, (i & 16) != 0 ? null : str2);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0006\u0018\u00002\u00020\u0001BC\u0012\b\u0010\r\u001a\u0004\u0018\u00010\n\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\n\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u000e\u0010\u000fR\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0018\u0010\u0006\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007R\u0018\u0010\b\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\u0004R\u0018\u0010\t\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\t\u0010\u0004R\u0018\u0010\u000b\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u0018\u0010\r\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/restapi/RestAPIParams$TextChannel;", "", "", "defaultAutoArchiveDuration", "Ljava/lang/Integer;", "", ModelAuditLogEntry.CHANGE_KEY_NSFW, "Ljava/lang/Boolean;", "type", "rateLimitPerUser", "", ModelAuditLogEntry.CHANGE_KEY_TOPIC, "Ljava/lang/String;", ModelAuditLogEntry.CHANGE_KEY_NAME, HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class TextChannel {
        private final Integer defaultAutoArchiveDuration;
        private final String name;
        private final Boolean nsfw;
        private final Integer rateLimitPerUser;
        private final String topic;
        private final Integer type;

        public TextChannel(String str, Integer num, String str2, Boolean bool, Integer num2, Integer num3) {
            this.name = str;
            this.type = num;
            this.topic = str2;
            this.nsfw = bool;
            this.rateLimitPerUser = num2;
            this.defaultAutoArchiveDuration = num3;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\n\u001a\u00020\u0002\u0012\u0006\u0010\u000b\u001a\u00020\u0005\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u0003\u001a\u00020\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÂ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\b\u001a\u0004\u0018\u00010\u0005HÂ\u0003¢\u0006\u0004\b\b\u0010\tJ0\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u00022\b\b\u0002\u0010\u000b\u001a\u00020\u00052\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0004J\u0010\u0010\u0010\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0007J\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u0018\u0010\f\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\u0015R\u0016\u0010\u000b\u001a\u00020\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\u0016R\u0016\u0010\n\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u0017¨\u0006\u001a"}, d2 = {"Lcom/discord/restapi/RestAPIParams$ThreadCreationSettings;", "", "", "component1", "()Ljava/lang/String;", "", "component2", "()I", "component3", "()Ljava/lang/Integer;", ModelAuditLogEntry.CHANGE_KEY_NAME, "type", "autoArchiveDuration", "copy", "(Ljava/lang/String;ILjava/lang/Integer;)Lcom/discord/restapi/RestAPIParams$ThreadCreationSettings;", "toString", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Integer;", "I", "Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;ILjava/lang/Integer;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ThreadCreationSettings {
        private final Integer autoArchiveDuration;
        private final String name;
        private final int type;

        public ThreadCreationSettings(String str, int i, Integer num) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            this.name = str;
            this.type = i;
            this.autoArchiveDuration = num;
        }

        private final String component1() {
            return this.name;
        }

        private final int component2() {
            return this.type;
        }

        private final Integer component3() {
            return this.autoArchiveDuration;
        }

        public static /* synthetic */ ThreadCreationSettings copy$default(ThreadCreationSettings threadCreationSettings, String str, int i, Integer num, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                str = threadCreationSettings.name;
            }
            if ((i2 & 2) != 0) {
                i = threadCreationSettings.type;
            }
            if ((i2 & 4) != 0) {
                num = threadCreationSettings.autoArchiveDuration;
            }
            return threadCreationSettings.copy(str, i, num);
        }

        public final ThreadCreationSettings copy(String str, int i, Integer num) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            return new ThreadCreationSettings(str, i, num);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ThreadCreationSettings)) {
                return false;
            }
            ThreadCreationSettings threadCreationSettings = (ThreadCreationSettings) obj;
            return m.areEqual(this.name, threadCreationSettings.name) && this.type == threadCreationSettings.type && m.areEqual(this.autoArchiveDuration, threadCreationSettings.autoArchiveDuration);
        }

        public int hashCode() {
            String str = this.name;
            int i = 0;
            int hashCode = (((str != null ? str.hashCode() : 0) * 31) + this.type) * 31;
            Integer num = this.autoArchiveDuration;
            if (num != null) {
                i = num.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("ThreadCreationSettings(name=");
            R.append(this.name);
            R.append(", type=");
            R.append(this.type);
            R.append(", autoArchiveDuration=");
            return a.E(R, this.autoArchiveDuration, ")");
        }

        public /* synthetic */ ThreadCreationSettings(String str, int i, Integer num, int i2, DefaultConstructorMarker defaultConstructorMarker) {
            this(str, i, (i2 & 4) != 0 ? null : num);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\b¢\u0006\u0004\b\u001e\u0010\u001fJ\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ4\u0010\u000e\u001a\u00020\u00002\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0016\u001a\u00020\u00052\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u001b\u0010\f\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0018\u001a\u0004\b\u0019\u0010\u0007R\u001b\u0010\r\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001a\u001a\u0004\b\u001b\u0010\nR\u001b\u0010\u000b\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001c\u001a\u0004\b\u001d\u0010\u0004¨\u0006 "}, d2 = {"Lcom/discord/restapi/RestAPIParams$ThreadMemberSettings;", "", "", "component1", "()Ljava/lang/Integer;", "", "component2", "()Ljava/lang/Boolean;", "Lcom/discord/models/domain/ModelMuteConfig;", "component3", "()Lcom/discord/models/domain/ModelMuteConfig;", "flags", "muted", "muteConfig", "copy", "(Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/discord/models/domain/ModelMuteConfig;)Lcom/discord/restapi/RestAPIParams$ThreadMemberSettings;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Boolean;", "getMuted", "Lcom/discord/models/domain/ModelMuteConfig;", "getMuteConfig", "Ljava/lang/Integer;", "getFlags", HookHelper.constructorName, "(Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/discord/models/domain/ModelMuteConfig;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ThreadMemberSettings {
        private final Integer flags;
        private final ModelMuteConfig muteConfig;
        private final Boolean muted;

        public ThreadMemberSettings() {
            this(null, null, null, 7, null);
        }

        public ThreadMemberSettings(Integer num, Boolean bool, ModelMuteConfig modelMuteConfig) {
            this.flags = num;
            this.muted = bool;
            this.muteConfig = modelMuteConfig;
        }

        public static /* synthetic */ ThreadMemberSettings copy$default(ThreadMemberSettings threadMemberSettings, Integer num, Boolean bool, ModelMuteConfig modelMuteConfig, int i, Object obj) {
            if ((i & 1) != 0) {
                num = threadMemberSettings.flags;
            }
            if ((i & 2) != 0) {
                bool = threadMemberSettings.muted;
            }
            if ((i & 4) != 0) {
                modelMuteConfig = threadMemberSettings.muteConfig;
            }
            return threadMemberSettings.copy(num, bool, modelMuteConfig);
        }

        public final Integer component1() {
            return this.flags;
        }

        public final Boolean component2() {
            return this.muted;
        }

        public final ModelMuteConfig component3() {
            return this.muteConfig;
        }

        public final ThreadMemberSettings copy(Integer num, Boolean bool, ModelMuteConfig modelMuteConfig) {
            return new ThreadMemberSettings(num, bool, modelMuteConfig);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ThreadMemberSettings)) {
                return false;
            }
            ThreadMemberSettings threadMemberSettings = (ThreadMemberSettings) obj;
            return m.areEqual(this.flags, threadMemberSettings.flags) && m.areEqual(this.muted, threadMemberSettings.muted) && m.areEqual(this.muteConfig, threadMemberSettings.muteConfig);
        }

        public final Integer getFlags() {
            return this.flags;
        }

        public final ModelMuteConfig getMuteConfig() {
            return this.muteConfig;
        }

        public final Boolean getMuted() {
            return this.muted;
        }

        public int hashCode() {
            Integer num = this.flags;
            int i = 0;
            int hashCode = (num != null ? num.hashCode() : 0) * 31;
            Boolean bool = this.muted;
            int hashCode2 = (hashCode + (bool != null ? bool.hashCode() : 0)) * 31;
            ModelMuteConfig modelMuteConfig = this.muteConfig;
            if (modelMuteConfig != null) {
                i = modelMuteConfig.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("ThreadMemberSettings(flags=");
            R.append(this.flags);
            R.append(", muted=");
            R.append(this.muted);
            R.append(", muteConfig=");
            R.append(this.muteConfig);
            R.append(")");
            return R.toString();
        }

        public /* synthetic */ ThreadMemberSettings(Integer num, Boolean bool, ModelMuteConfig modelMuteConfig, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : num, (i & 2) != 0 ? null : bool, (i & 4) != 0 ? null : modelMuteConfig);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\b\u0018\u0010\u0019J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÂ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÂ\u0003¢\u0006\u0004\b\u0007\u0010\bJ4\u0010\f\u001a\u00020\u00002\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0006HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0014\u001a\u00020\u00022\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0018\u0010\n\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u0016R\u0018\u0010\u000b\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\u0017R\u0018\u0010\t\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\t\u0010\u0016¨\u0006\u001a"}, d2 = {"Lcom/discord/restapi/RestAPIParams$ThreadSettings;", "", "", "component1", "()Ljava/lang/Boolean;", "component2", "", "component3", "()Ljava/lang/Integer;", ModelAuditLogEntry.CHANGE_KEY_ARCHIVED, ModelAuditLogEntry.CHANGE_KEY_LOCKED, "autoArchiveDuration", "copy", "(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Integer;)Lcom/discord/restapi/RestAPIParams$ThreadSettings;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Boolean;", "Ljava/lang/Integer;", HookHelper.constructorName, "(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Integer;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ThreadSettings {
        private final Boolean archived;
        private final Integer autoArchiveDuration;
        private final Boolean locked;

        public ThreadSettings() {
            this(null, null, null, 7, null);
        }

        public ThreadSettings(Boolean bool, Boolean bool2, Integer num) {
            this.archived = bool;
            this.locked = bool2;
            this.autoArchiveDuration = num;
        }

        private final Boolean component1() {
            return this.archived;
        }

        private final Boolean component2() {
            return this.locked;
        }

        private final Integer component3() {
            return this.autoArchiveDuration;
        }

        public static /* synthetic */ ThreadSettings copy$default(ThreadSettings threadSettings, Boolean bool, Boolean bool2, Integer num, int i, Object obj) {
            if ((i & 1) != 0) {
                bool = threadSettings.archived;
            }
            if ((i & 2) != 0) {
                bool2 = threadSettings.locked;
            }
            if ((i & 4) != 0) {
                num = threadSettings.autoArchiveDuration;
            }
            return threadSettings.copy(bool, bool2, num);
        }

        public final ThreadSettings copy(Boolean bool, Boolean bool2, Integer num) {
            return new ThreadSettings(bool, bool2, num);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ThreadSettings)) {
                return false;
            }
            ThreadSettings threadSettings = (ThreadSettings) obj;
            return m.areEqual(this.archived, threadSettings.archived) && m.areEqual(this.locked, threadSettings.locked) && m.areEqual(this.autoArchiveDuration, threadSettings.autoArchiveDuration);
        }

        public int hashCode() {
            Boolean bool = this.archived;
            int i = 0;
            int hashCode = (bool != null ? bool.hashCode() : 0) * 31;
            Boolean bool2 = this.locked;
            int hashCode2 = (hashCode + (bool2 != null ? bool2.hashCode() : 0)) * 31;
            Integer num = this.autoArchiveDuration;
            if (num != null) {
                i = num.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("ThreadSettings(archived=");
            R.append(this.archived);
            R.append(", locked=");
            R.append(this.locked);
            R.append(", autoArchiveDuration=");
            return a.E(R, this.autoArchiveDuration, ")");
        }

        public /* synthetic */ ThreadSettings(Boolean bool, Boolean bool2, Integer num, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : bool, (i & 2) != 0 ? null : bool2, (i & 4) != 0 ? null : num);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/restapi/RestAPIParams$Thumbnail;", "", "", "thumbnail", "Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Thumbnail {
        private final String thumbnail;

        public Thumbnail(String str) {
            m.checkNotNullParameter(str, "thumbnail");
            this.thumbnail = str;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\u0018\u00002\u00020\u0001B%\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\t\u0010\nR\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0018\u0010\u0006\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007R\u0018\u0010\b\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\u0004¨\u0006\u000b"}, d2 = {"Lcom/discord/restapi/RestAPIParams$TopicalChannel;", "", "", ModelAuditLogEntry.CHANGE_KEY_NAME, "Ljava/lang/String;", "", "type", "Ljava/lang/Integer;", ModelAuditLogEntry.CHANGE_KEY_TOPIC, HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class TopicalChannel {
        private final String name;
        private final String topic;
        private final Integer type;

        public TopicalChannel(String str, Integer num, String str2) {
            this.name = str;
            this.type = num;
            this.topic = str2;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0018\u0010\u0006\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/restapi/RestAPIParams$TransferGuildOwnership;", "", "", "ownerId", "J", "", ModelAuditLogEntry.CHANGE_KEY_CODE, "Ljava/lang/String;", HookHelper.constructorName, "(JLjava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class TransferGuildOwnership {
        private final String code;
        private final long ownerId;

        public TransferGuildOwnership(long j, String str) {
            this.ownerId = j;
            this.code = str;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 '2\u00020\u0001:\u0001'BÁ\u0001\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\n\u0012\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\n\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\"\u0012\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u0016\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\n\u0012\u0010\b\u0002\u0010\u001b\u001a\n\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u0019\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b%\u0010&R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u001b\u0010\u0006\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0007\u001a\u0004\b\b\u0010\tR\u0018\u0010\u000b\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u0018\u0010\r\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u0004R\u0018\u0010\u000e\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u0004R\u0018\u0010\u000f\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0004R\u0018\u0010\u0010\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0007R\u0018\u0010\u0011\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\fR\u0018\u0010\u0012\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u0007R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0004\u001a\u0004\b\u0014\u0010\u0015R\u0018\u0010\u0017\u001a\u0004\u0018\u00010\u00168\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R!\u0010\u001b\u001a\n\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u00198\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u0007\u001a\u0004\b \u0010\tR\u0018\u0010!\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\fR\u0018\u0010#\u001a\u0004\u0018\u00010\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$¨\u0006("}, d2 = {"Lcom/discord/restapi/RestAPIParams$UpdateGuild;", "", "", "splash", "Ljava/lang/String;", "", "rulesChannelId", "Ljava/lang/Long;", "getRulesChannelId", "()Ljava/lang/Long;", "", "systemChannelFlags", "Ljava/lang/Integer;", "icon", "banner", ModelAuditLogEntry.CHANGE_KEY_NAME, "afkChannelId", "defaultMessageNotifications", "systemChannelId", "preferredLocale", "getPreferredLocale", "()Ljava/lang/String;", "Lcom/discord/api/guild/GuildExplicitContentFilter;", "explicitContentFilter", "Lcom/discord/api/guild/GuildExplicitContentFilter;", "", "Lcom/discord/api/guild/GuildFeature;", "features", "Ljava/util/List;", "getFeatures", "()Ljava/util/List;", "publicUpdatesChannelId", "getPublicUpdatesChannelId", "afkTimeout", "Lcom/discord/api/guild/GuildVerificationLevel;", "verificationLevel", "Lcom/discord/api/guild/GuildVerificationLevel;", HookHelper.constructorName, "(Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/discord/api/guild/GuildVerificationLevel;Lcom/discord/api/guild/GuildExplicitContentFilter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;)V", "Companion", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class UpdateGuild {
        public static final Companion Companion = new Companion(null);
        private final Long afkChannelId;
        private final Integer afkTimeout;
        private final String banner;
        private final Integer defaultMessageNotifications;
        private final GuildExplicitContentFilter explicitContentFilter;
        private final List<GuildFeature> features;
        private final String icon;
        private final String name;
        private final String preferredLocale;
        private final Long publicUpdatesChannelId;
        private final Long rulesChannelId;
        private final String splash;
        private final Integer systemChannelFlags;
        private final Long systemChannelId;
        private final GuildVerificationLevel verificationLevel;

        /* compiled from: RestAPIParams.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0007¢\u0006\u0004\b\t\u0010\n¨\u0006\r"}, d2 = {"Lcom/discord/restapi/RestAPIParams$UpdateGuild$Companion;", "", "Lcom/discord/api/guild/GuildVerificationLevel;", "verificationLevel", "Lcom/discord/restapi/RestAPIParams$UpdateGuild;", "createForVerificationLevel", "(Lcom/discord/api/guild/GuildVerificationLevel;)Lcom/discord/restapi/RestAPIParams$UpdateGuild;", "Lcom/discord/api/guild/GuildExplicitContentFilter;", "explicitContentFilter", "createForExplicitContentFilter", "(Lcom/discord/api/guild/GuildExplicitContentFilter;)Lcom/discord/restapi/RestAPIParams$UpdateGuild;", HookHelper.constructorName, "()V", "restapi_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Companion {
            private Companion() {
            }

            public final UpdateGuild createForExplicitContentFilter(GuildExplicitContentFilter guildExplicitContentFilter) {
                m.checkNotNullParameter(guildExplicitContentFilter, "explicitContentFilter");
                return new UpdateGuild(null, null, null, null, null, null, null, guildExplicitContentFilter, null, null, null, null, null, null, null, 32639, null);
            }

            public final UpdateGuild createForVerificationLevel(GuildVerificationLevel guildVerificationLevel) {
                m.checkNotNullParameter(guildVerificationLevel, "verificationLevel");
                return new UpdateGuild(null, null, null, null, null, null, guildVerificationLevel, null, null, null, null, null, null, null, null, 32703, null);
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public UpdateGuild() {
            this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 32767, null);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public UpdateGuild(Long l, Integer num, Long l2, Integer num2, String str, String str2, GuildVerificationLevel guildVerificationLevel, GuildExplicitContentFilter guildExplicitContentFilter, String str3, String str4, Integer num3, List<? extends GuildFeature> list, Long l3, Long l4, String str5) {
            this.afkChannelId = l;
            this.afkTimeout = num;
            this.systemChannelId = l2;
            this.defaultMessageNotifications = num2;
            this.icon = str;
            this.name = str2;
            this.verificationLevel = guildVerificationLevel;
            this.explicitContentFilter = guildExplicitContentFilter;
            this.splash = str3;
            this.banner = str4;
            this.systemChannelFlags = num3;
            this.features = list;
            this.rulesChannelId = l3;
            this.publicUpdatesChannelId = l4;
            this.preferredLocale = str5;
        }

        public static final UpdateGuild createForExplicitContentFilter(GuildExplicitContentFilter guildExplicitContentFilter) {
            return Companion.createForExplicitContentFilter(guildExplicitContentFilter);
        }

        public static final UpdateGuild createForVerificationLevel(GuildVerificationLevel guildVerificationLevel) {
            return Companion.createForVerificationLevel(guildVerificationLevel);
        }

        public final List<GuildFeature> getFeatures() {
            return this.features;
        }

        public final String getPreferredLocale() {
            return this.preferredLocale;
        }

        public final Long getPublicUpdatesChannelId() {
            return this.publicUpdatesChannelId;
        }

        public final Long getRulesChannelId() {
            return this.rulesChannelId;
        }

        public /* synthetic */ UpdateGuild(Long l, Integer num, Long l2, Integer num2, String str, String str2, GuildVerificationLevel guildVerificationLevel, GuildExplicitContentFilter guildExplicitContentFilter, String str3, String str4, Integer num3, List list, Long l3, Long l4, String str5, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : l, (i & 2) != 0 ? null : num, (i & 4) != 0 ? null : l2, (i & 8) != 0 ? null : num2, (i & 16) != 0 ? null : str, (i & 32) != 0 ? null : str2, (i & 64) != 0 ? null : guildVerificationLevel, (i & 128) != 0 ? null : guildExplicitContentFilter, (i & 256) != 0 ? null : str3, (i & 512) != 0 ? null : str4, (i & 1024) != 0 ? null : num3, (i & 2048) != 0 ? null : list, (i & 4096) != 0 ? null : l3, (i & 8192) != 0 ? null : l4, (i & 16384) == 0 ? str5 : null);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\b\u001a\u0010\u001bJ\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ4\u0010\f\u001a\u00020\u00002\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0006HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000e\u0010\u0004J\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0013\u001a\u00020\u00062\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u001b\u0010\t\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004R\u001b\u0010\n\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0015\u001a\u0004\b\u0017\u0010\u0004R\u001b\u0010\u000b\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0018\u001a\u0004\b\u0019\u0010\b¨\u0006\u001c"}, d2 = {"Lcom/discord/restapi/RestAPIParams$UpdateGuildRoleSubscriptionGroupListing;", "", "", "component1", "()Ljava/lang/String;", "component2", "", "component3", "()Ljava/lang/Boolean;", "image", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "fullServerGate", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lcom/discord/restapi/RestAPIParams$UpdateGuildRoleSubscriptionGroupListing;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getImage", "getDescription", "Ljava/lang/Boolean;", "getFullServerGate", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class UpdateGuildRoleSubscriptionGroupListing {
        private final String description;
        private final Boolean fullServerGate;
        private final String image;

        public UpdateGuildRoleSubscriptionGroupListing(String str, String str2, Boolean bool) {
            this.image = str;
            this.description = str2;
            this.fullServerGate = bool;
        }

        public static /* synthetic */ UpdateGuildRoleSubscriptionGroupListing copy$default(UpdateGuildRoleSubscriptionGroupListing updateGuildRoleSubscriptionGroupListing, String str, String str2, Boolean bool, int i, Object obj) {
            if ((i & 1) != 0) {
                str = updateGuildRoleSubscriptionGroupListing.image;
            }
            if ((i & 2) != 0) {
                str2 = updateGuildRoleSubscriptionGroupListing.description;
            }
            if ((i & 4) != 0) {
                bool = updateGuildRoleSubscriptionGroupListing.fullServerGate;
            }
            return updateGuildRoleSubscriptionGroupListing.copy(str, str2, bool);
        }

        public final String component1() {
            return this.image;
        }

        public final String component2() {
            return this.description;
        }

        public final Boolean component3() {
            return this.fullServerGate;
        }

        public final UpdateGuildRoleSubscriptionGroupListing copy(String str, String str2, Boolean bool) {
            return new UpdateGuildRoleSubscriptionGroupListing(str, str2, bool);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof UpdateGuildRoleSubscriptionGroupListing)) {
                return false;
            }
            UpdateGuildRoleSubscriptionGroupListing updateGuildRoleSubscriptionGroupListing = (UpdateGuildRoleSubscriptionGroupListing) obj;
            return m.areEqual(this.image, updateGuildRoleSubscriptionGroupListing.image) && m.areEqual(this.description, updateGuildRoleSubscriptionGroupListing.description) && m.areEqual(this.fullServerGate, updateGuildRoleSubscriptionGroupListing.fullServerGate);
        }

        public final String getDescription() {
            return this.description;
        }

        public final Boolean getFullServerGate() {
            return this.fullServerGate;
        }

        public final String getImage() {
            return this.image;
        }

        public int hashCode() {
            String str = this.image;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.description;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            Boolean bool = this.fullServerGate;
            if (bool != null) {
                i = bool.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("UpdateGuildRoleSubscriptionGroupListing(image=");
            R.append(this.image);
            R.append(", description=");
            R.append(this.description);
            R.append(", fullServerGate=");
            return a.C(R, this.fullServerGate, ")");
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b \b\u0086\b\u0018\u00002\u00020\u0001BS\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0013\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0014\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0015\u001a\u0004\u0018\u00010\u0007\u0012\u000e\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\n\u0012\b\u0010\u0017\u001a\u0004\u0018\u00010\u000e\u0012\b\u0010\u0018\u001a\u0004\u0018\u00010\u000e¢\u0006\u0004\b,\u0010-J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J\u0012\u0010\b\u001a\u0004\u0018\u00010\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0018\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\nHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u000eHÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0010Jj\u0010\u0019\u001a\u00020\u00002\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00072\u0010\b\u0002\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\n2\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u000e2\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u000eHÆ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u001b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u001b\u0010\u0004J\u0010\u0010\u001c\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u001a\u0010\u001f\u001a\u00020\u000e2\b\u0010\u001e\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001f\u0010 R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010!\u001a\u0004\b\"\u0010\u0010R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010#\u001a\u0004\b$\u0010\u0004R\u001b\u0010\u0014\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010#\u001a\u0004\b%\u0010\u0004R!\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010&\u001a\u0004\b'\u0010\rR\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010!\u001a\u0004\b(\u0010\u0010R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010)\u001a\u0004\b*\u0010\tR\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010#\u001a\u0004\b+\u0010\u0004¨\u0006."}, d2 = {"Lcom/discord/restapi/RestAPIParams$UpdateGuildRoleSubscriptionTierListing;", "", "", "component1", "()Ljava/lang/String;", "component2", "component3", "", "component4", "()Ljava/lang/Integer;", "", "Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefit;", "component5", "()Ljava/util/List;", "", "component6", "()Ljava/lang/Boolean;", "component7", ModelAuditLogEntry.CHANGE_KEY_NAME, "image", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "priceTier", "benefits", "published", "canAccessAllChannels", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/discord/restapi/RestAPIParams$UpdateGuildRoleSubscriptionTierListing;", "toString", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Boolean;", "getPublished", "Ljava/lang/String;", "getImage", "getDescription", "Ljava/util/List;", "getBenefits", "getCanAccessAllChannels", "Ljava/lang/Integer;", "getPriceTier", "getName", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class UpdateGuildRoleSubscriptionTierListing {
        private final List<GuildRoleSubscriptionBenefit> benefits;
        private final Boolean canAccessAllChannels;
        private final String description;
        private final String image;
        private final String name;
        private final Integer priceTier;
        private final Boolean published;

        public UpdateGuildRoleSubscriptionTierListing(String str, String str2, String str3, Integer num, List<GuildRoleSubscriptionBenefit> list, Boolean bool, Boolean bool2) {
            this.name = str;
            this.image = str2;
            this.description = str3;
            this.priceTier = num;
            this.benefits = list;
            this.published = bool;
            this.canAccessAllChannels = bool2;
        }

        public static /* synthetic */ UpdateGuildRoleSubscriptionTierListing copy$default(UpdateGuildRoleSubscriptionTierListing updateGuildRoleSubscriptionTierListing, String str, String str2, String str3, Integer num, List list, Boolean bool, Boolean bool2, int i, Object obj) {
            if ((i & 1) != 0) {
                str = updateGuildRoleSubscriptionTierListing.name;
            }
            if ((i & 2) != 0) {
                str2 = updateGuildRoleSubscriptionTierListing.image;
            }
            String str4 = str2;
            if ((i & 4) != 0) {
                str3 = updateGuildRoleSubscriptionTierListing.description;
            }
            String str5 = str3;
            if ((i & 8) != 0) {
                num = updateGuildRoleSubscriptionTierListing.priceTier;
            }
            Integer num2 = num;
            List<GuildRoleSubscriptionBenefit> list2 = list;
            if ((i & 16) != 0) {
                list2 = updateGuildRoleSubscriptionTierListing.benefits;
            }
            List list3 = list2;
            if ((i & 32) != 0) {
                bool = updateGuildRoleSubscriptionTierListing.published;
            }
            Boolean bool3 = bool;
            if ((i & 64) != 0) {
                bool2 = updateGuildRoleSubscriptionTierListing.canAccessAllChannels;
            }
            return updateGuildRoleSubscriptionTierListing.copy(str, str4, str5, num2, list3, bool3, bool2);
        }

        public final String component1() {
            return this.name;
        }

        public final String component2() {
            return this.image;
        }

        public final String component3() {
            return this.description;
        }

        public final Integer component4() {
            return this.priceTier;
        }

        public final List<GuildRoleSubscriptionBenefit> component5() {
            return this.benefits;
        }

        public final Boolean component6() {
            return this.published;
        }

        public final Boolean component7() {
            return this.canAccessAllChannels;
        }

        public final UpdateGuildRoleSubscriptionTierListing copy(String str, String str2, String str3, Integer num, List<GuildRoleSubscriptionBenefit> list, Boolean bool, Boolean bool2) {
            return new UpdateGuildRoleSubscriptionTierListing(str, str2, str3, num, list, bool, bool2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof UpdateGuildRoleSubscriptionTierListing)) {
                return false;
            }
            UpdateGuildRoleSubscriptionTierListing updateGuildRoleSubscriptionTierListing = (UpdateGuildRoleSubscriptionTierListing) obj;
            return m.areEqual(this.name, updateGuildRoleSubscriptionTierListing.name) && m.areEqual(this.image, updateGuildRoleSubscriptionTierListing.image) && m.areEqual(this.description, updateGuildRoleSubscriptionTierListing.description) && m.areEqual(this.priceTier, updateGuildRoleSubscriptionTierListing.priceTier) && m.areEqual(this.benefits, updateGuildRoleSubscriptionTierListing.benefits) && m.areEqual(this.published, updateGuildRoleSubscriptionTierListing.published) && m.areEqual(this.canAccessAllChannels, updateGuildRoleSubscriptionTierListing.canAccessAllChannels);
        }

        public final List<GuildRoleSubscriptionBenefit> getBenefits() {
            return this.benefits;
        }

        public final Boolean getCanAccessAllChannels() {
            return this.canAccessAllChannels;
        }

        public final String getDescription() {
            return this.description;
        }

        public final String getImage() {
            return this.image;
        }

        public final String getName() {
            return this.name;
        }

        public final Integer getPriceTier() {
            return this.priceTier;
        }

        public final Boolean getPublished() {
            return this.published;
        }

        public int hashCode() {
            String str = this.name;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.image;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.description;
            int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
            Integer num = this.priceTier;
            int hashCode4 = (hashCode3 + (num != null ? num.hashCode() : 0)) * 31;
            List<GuildRoleSubscriptionBenefit> list = this.benefits;
            int hashCode5 = (hashCode4 + (list != null ? list.hashCode() : 0)) * 31;
            Boolean bool = this.published;
            int hashCode6 = (hashCode5 + (bool != null ? bool.hashCode() : 0)) * 31;
            Boolean bool2 = this.canAccessAllChannels;
            if (bool2 != null) {
                i = bool2.hashCode();
            }
            return hashCode6 + i;
        }

        public String toString() {
            StringBuilder R = a.R("UpdateGuildRoleSubscriptionTierListing(name=");
            R.append(this.name);
            R.append(", image=");
            R.append(this.image);
            R.append(", description=");
            R.append(this.description);
            R.append(", priceTier=");
            R.append(this.priceTier);
            R.append(", benefits=");
            R.append(this.benefits);
            R.append(", published=");
            R.append(this.published);
            R.append(", canAccessAllChannels=");
            return a.C(R, this.canAccessAllChannels, ")");
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u000f\n\u0002\u0010\b\n\u0002\b\u0018\b\u0086\b\u0018\u00002\u00020\u0001B\u008f\u0001\u0012\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u0002\u0012\u0014\b\u0002\u0010 \u001a\u000e\u0012\b\u0012\u00060\fj\u0002`\r\u0018\u00010\u000b\u0012\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u0010\u0012\u0010\b\u0002\u0010\"\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u000b\u0012\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\u0015\u0012\n\b\u0002\u0010$\u001a\u0004\u0018\u00010\u0018¢\u0006\u0004\b>\u0010?J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\t\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\u0004J\u0012\u0010\n\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\n\u0010\u0004J\u001c\u0010\u000e\u001a\u000e\u0012\b\u0012\u00060\fj\u0002`\r\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0018\u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\u0014\u0010\u000fJ\u0012\u0010\u0016\u001a\u0004\u0018\u00010\u0015HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0012\u0010\u0019\u001a\u0004\u0018\u00010\u0018HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u001aJ\u0098\u0001\u0010%\u001a\u00020\u00002\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u00022\u0014\b\u0002\u0010 \u001a\u000e\u0012\b\u0012\u00060\fj\u0002`\r\u0018\u00010\u000b2\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u00102\u0010\b\u0002\u0010\"\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u000b2\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\u00152\n\b\u0002\u0010$\u001a\u0004\u0018\u00010\u0018HÆ\u0001¢\u0006\u0004\b%\u0010&J\u0010\u0010'\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b'\u0010\u0004J\u0010\u0010)\u001a\u00020(HÖ\u0001¢\u0006\u0004\b)\u0010*J\u001a\u0010,\u001a\u00020\u00182\b\u0010+\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b,\u0010-R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010.\u001a\u0004\b/\u0010\u0004R!\u0010\"\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00100\u001a\u0004\b1\u0010\u000fR\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010.\u001a\u0004\b2\u0010\u0004R\u001b\u0010$\u001a\u0004\u0018\u00010\u00188\u0006@\u0006¢\u0006\f\n\u0004\b$\u00103\u001a\u0004\b4\u0010\u001aR\u001b\u0010#\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b#\u00105\u001a\u0004\b6\u0010\u0017R\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00107\u001a\u0004\b8\u0010\bR\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010.\u001a\u0004\b9\u0010\u0004R%\u0010 \u001a\u000e\u0012\b\u0012\u00060\fj\u0002`\r\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b \u00100\u001a\u0004\b:\u0010\u000fR\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010.\u001a\u0004\b;\u0010\u0004R\u001b\u0010!\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010<\u001a\u0004\b=\u0010\u0012¨\u0006@"}, d2 = {"Lcom/discord/restapi/RestAPIParams$UpdateGuildScheduledEventBody;", "", "", "component1", "()Ljava/lang/String;", "component2", "Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;", "component3", "()Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;", "component4", "component5", "Lcom/discord/nullserializable/NullSerializable;", "", "Lcom/discord/primitives/ChannelId;", "component6", "()Lcom/discord/nullserializable/NullSerializable;", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;", "component7", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityMetadata;", "component8", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventStatus;", "component9", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEventStatus;", "", "component10", "()Ljava/lang/Boolean;", ModelAuditLogEntry.CHANGE_KEY_NAME, ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "privacyLevel", "scheduledStartTime", "scheduledEndTime", "channelId", "entityType", "entityMetadata", "status", "broadcastToDirectoryChannels", "copy", "(Ljava/lang/String;Ljava/lang/String;Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;Ljava/lang/String;Ljava/lang/String;Lcom/discord/nullserializable/NullSerializable;Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;Lcom/discord/nullserializable/NullSerializable;Lcom/discord/api/guildscheduledevent/GuildScheduledEventStatus;Ljava/lang/Boolean;)Lcom/discord/restapi/RestAPIParams$UpdateGuildScheduledEventBody;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getDescription", "Lcom/discord/nullserializable/NullSerializable;", "getEntityMetadata", "getScheduledStartTime", "Ljava/lang/Boolean;", "getBroadcastToDirectoryChannels", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventStatus;", "getStatus", "Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;", "getPrivacyLevel", "getScheduledEndTime", "getChannelId", "getName", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;", "getEntityType", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;Ljava/lang/String;Ljava/lang/String;Lcom/discord/nullserializable/NullSerializable;Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;Lcom/discord/nullserializable/NullSerializable;Lcom/discord/api/guildscheduledevent/GuildScheduledEventStatus;Ljava/lang/Boolean;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class UpdateGuildScheduledEventBody {
        private final Boolean broadcastToDirectoryChannels;
        private final NullSerializable<Long> channelId;
        private final String description;
        private final NullSerializable<GuildScheduledEventEntityMetadata> entityMetadata;
        private final GuildScheduledEventEntityType entityType;
        private final String name;
        private final StageInstancePrivacyLevel privacyLevel;
        private final String scheduledEndTime;
        private final String scheduledStartTime;
        private final GuildScheduledEventStatus status;

        public UpdateGuildScheduledEventBody() {
            this(null, null, null, null, null, null, null, null, null, null, AudioAttributesCompat.FLAG_ALL, null);
        }

        public UpdateGuildScheduledEventBody(String str, String str2, StageInstancePrivacyLevel stageInstancePrivacyLevel, String str3, String str4, NullSerializable<Long> nullSerializable, GuildScheduledEventEntityType guildScheduledEventEntityType, NullSerializable<GuildScheduledEventEntityMetadata> nullSerializable2, GuildScheduledEventStatus guildScheduledEventStatus, Boolean bool) {
            this.name = str;
            this.description = str2;
            this.privacyLevel = stageInstancePrivacyLevel;
            this.scheduledStartTime = str3;
            this.scheduledEndTime = str4;
            this.channelId = nullSerializable;
            this.entityType = guildScheduledEventEntityType;
            this.entityMetadata = nullSerializable2;
            this.status = guildScheduledEventStatus;
            this.broadcastToDirectoryChannels = bool;
        }

        public final String component1() {
            return this.name;
        }

        public final Boolean component10() {
            return this.broadcastToDirectoryChannels;
        }

        public final String component2() {
            return this.description;
        }

        public final StageInstancePrivacyLevel component3() {
            return this.privacyLevel;
        }

        public final String component4() {
            return this.scheduledStartTime;
        }

        public final String component5() {
            return this.scheduledEndTime;
        }

        public final NullSerializable<Long> component6() {
            return this.channelId;
        }

        public final GuildScheduledEventEntityType component7() {
            return this.entityType;
        }

        public final NullSerializable<GuildScheduledEventEntityMetadata> component8() {
            return this.entityMetadata;
        }

        public final GuildScheduledEventStatus component9() {
            return this.status;
        }

        public final UpdateGuildScheduledEventBody copy(String str, String str2, StageInstancePrivacyLevel stageInstancePrivacyLevel, String str3, String str4, NullSerializable<Long> nullSerializable, GuildScheduledEventEntityType guildScheduledEventEntityType, NullSerializable<GuildScheduledEventEntityMetadata> nullSerializable2, GuildScheduledEventStatus guildScheduledEventStatus, Boolean bool) {
            return new UpdateGuildScheduledEventBody(str, str2, stageInstancePrivacyLevel, str3, str4, nullSerializable, guildScheduledEventEntityType, nullSerializable2, guildScheduledEventStatus, bool);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof UpdateGuildScheduledEventBody)) {
                return false;
            }
            UpdateGuildScheduledEventBody updateGuildScheduledEventBody = (UpdateGuildScheduledEventBody) obj;
            return m.areEqual(this.name, updateGuildScheduledEventBody.name) && m.areEqual(this.description, updateGuildScheduledEventBody.description) && m.areEqual(this.privacyLevel, updateGuildScheduledEventBody.privacyLevel) && m.areEqual(this.scheduledStartTime, updateGuildScheduledEventBody.scheduledStartTime) && m.areEqual(this.scheduledEndTime, updateGuildScheduledEventBody.scheduledEndTime) && m.areEqual(this.channelId, updateGuildScheduledEventBody.channelId) && m.areEqual(this.entityType, updateGuildScheduledEventBody.entityType) && m.areEqual(this.entityMetadata, updateGuildScheduledEventBody.entityMetadata) && m.areEqual(this.status, updateGuildScheduledEventBody.status) && m.areEqual(this.broadcastToDirectoryChannels, updateGuildScheduledEventBody.broadcastToDirectoryChannels);
        }

        public final Boolean getBroadcastToDirectoryChannels() {
            return this.broadcastToDirectoryChannels;
        }

        public final NullSerializable<Long> getChannelId() {
            return this.channelId;
        }

        public final String getDescription() {
            return this.description;
        }

        public final NullSerializable<GuildScheduledEventEntityMetadata> getEntityMetadata() {
            return this.entityMetadata;
        }

        public final GuildScheduledEventEntityType getEntityType() {
            return this.entityType;
        }

        public final String getName() {
            return this.name;
        }

        public final StageInstancePrivacyLevel getPrivacyLevel() {
            return this.privacyLevel;
        }

        public final String getScheduledEndTime() {
            return this.scheduledEndTime;
        }

        public final String getScheduledStartTime() {
            return this.scheduledStartTime;
        }

        public final GuildScheduledEventStatus getStatus() {
            return this.status;
        }

        public int hashCode() {
            String str = this.name;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.description;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            StageInstancePrivacyLevel stageInstancePrivacyLevel = this.privacyLevel;
            int hashCode3 = (hashCode2 + (stageInstancePrivacyLevel != null ? stageInstancePrivacyLevel.hashCode() : 0)) * 31;
            String str3 = this.scheduledStartTime;
            int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
            String str4 = this.scheduledEndTime;
            int hashCode5 = (hashCode4 + (str4 != null ? str4.hashCode() : 0)) * 31;
            NullSerializable<Long> nullSerializable = this.channelId;
            int hashCode6 = (hashCode5 + (nullSerializable != null ? nullSerializable.hashCode() : 0)) * 31;
            GuildScheduledEventEntityType guildScheduledEventEntityType = this.entityType;
            int hashCode7 = (hashCode6 + (guildScheduledEventEntityType != null ? guildScheduledEventEntityType.hashCode() : 0)) * 31;
            NullSerializable<GuildScheduledEventEntityMetadata> nullSerializable2 = this.entityMetadata;
            int hashCode8 = (hashCode7 + (nullSerializable2 != null ? nullSerializable2.hashCode() : 0)) * 31;
            GuildScheduledEventStatus guildScheduledEventStatus = this.status;
            int hashCode9 = (hashCode8 + (guildScheduledEventStatus != null ? guildScheduledEventStatus.hashCode() : 0)) * 31;
            Boolean bool = this.broadcastToDirectoryChannels;
            if (bool != null) {
                i = bool.hashCode();
            }
            return hashCode9 + i;
        }

        public String toString() {
            StringBuilder R = a.R("UpdateGuildScheduledEventBody(name=");
            R.append(this.name);
            R.append(", description=");
            R.append(this.description);
            R.append(", privacyLevel=");
            R.append(this.privacyLevel);
            R.append(", scheduledStartTime=");
            R.append(this.scheduledStartTime);
            R.append(", scheduledEndTime=");
            R.append(this.scheduledEndTime);
            R.append(", channelId=");
            R.append(this.channelId);
            R.append(", entityType=");
            R.append(this.entityType);
            R.append(", entityMetadata=");
            R.append(this.entityMetadata);
            R.append(", status=");
            R.append(this.status);
            R.append(", broadcastToDirectoryChannels=");
            return a.C(R, this.broadcastToDirectoryChannels, ")");
        }

        public /* synthetic */ UpdateGuildScheduledEventBody(String str, String str2, StageInstancePrivacyLevel stageInstancePrivacyLevel, String str3, String str4, NullSerializable nullSerializable, GuildScheduledEventEntityType guildScheduledEventEntityType, NullSerializable nullSerializable2, GuildScheduledEventStatus guildScheduledEventStatus, Boolean bool, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : str, (i & 2) != 0 ? null : str2, (i & 4) != 0 ? null : stageInstancePrivacyLevel, (i & 8) != 0 ? null : str3, (i & 16) != 0 ? null : str4, (i & 32) != 0 ? null : nullSerializable, (i & 64) != 0 ? null : guildScheduledEventEntityType, (i & 128) != 0 ? null : nullSerializable2, (i & 256) != 0 ? null : guildScheduledEventStatus, (i & 512) == 0 ? bool : null);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u0018\u0010\u0019J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J(\u0010\n\u001a\u00020\u00002\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\f\u0010\u0004J\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001b\u0010\b\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0014\u001a\u0004\b\u0015\u0010\u0004R\u001b\u0010\t\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0016\u001a\u0004\b\u0017\u0010\u0007¨\u0006\u001a"}, d2 = {"Lcom/discord/restapi/RestAPIParams$UpdateStageInstanceBody;", "", "", "component1", "()Ljava/lang/String;", "Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;", "component2", "()Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;", ModelAuditLogEntry.CHANGE_KEY_TOPIC, "privacyLevel", "copy", "(Ljava/lang/String;Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;)Lcom/discord/restapi/RestAPIParams$UpdateStageInstanceBody;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getTopic", "Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;", "getPrivacyLevel", HookHelper.constructorName, "(Ljava/lang/String;Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class UpdateStageInstanceBody {
        private final StageInstancePrivacyLevel privacyLevel;
        private final String topic;

        public UpdateStageInstanceBody() {
            this(null, null, 3, null);
        }

        public UpdateStageInstanceBody(String str, StageInstancePrivacyLevel stageInstancePrivacyLevel) {
            this.topic = str;
            this.privacyLevel = stageInstancePrivacyLevel;
        }

        public static /* synthetic */ UpdateStageInstanceBody copy$default(UpdateStageInstanceBody updateStageInstanceBody, String str, StageInstancePrivacyLevel stageInstancePrivacyLevel, int i, Object obj) {
            if ((i & 1) != 0) {
                str = updateStageInstanceBody.topic;
            }
            if ((i & 2) != 0) {
                stageInstancePrivacyLevel = updateStageInstanceBody.privacyLevel;
            }
            return updateStageInstanceBody.copy(str, stageInstancePrivacyLevel);
        }

        public final String component1() {
            return this.topic;
        }

        public final StageInstancePrivacyLevel component2() {
            return this.privacyLevel;
        }

        public final UpdateStageInstanceBody copy(String str, StageInstancePrivacyLevel stageInstancePrivacyLevel) {
            return new UpdateStageInstanceBody(str, stageInstancePrivacyLevel);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof UpdateStageInstanceBody)) {
                return false;
            }
            UpdateStageInstanceBody updateStageInstanceBody = (UpdateStageInstanceBody) obj;
            return m.areEqual(this.topic, updateStageInstanceBody.topic) && m.areEqual(this.privacyLevel, updateStageInstanceBody.privacyLevel);
        }

        public final StageInstancePrivacyLevel getPrivacyLevel() {
            return this.privacyLevel;
        }

        public final String getTopic() {
            return this.topic;
        }

        public int hashCode() {
            String str = this.topic;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            StageInstancePrivacyLevel stageInstancePrivacyLevel = this.privacyLevel;
            if (stageInstancePrivacyLevel != null) {
                i = stageInstancePrivacyLevel.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("UpdateStageInstanceBody(topic=");
            R.append(this.topic);
            R.append(", privacyLevel=");
            R.append(this.privacyLevel);
            R.append(")");
            return R.toString();
        }

        public /* synthetic */ UpdateStageInstanceBody(String str, StageInstancePrivacyLevel stageInstancePrivacyLevel, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : str, (i & 2) != 0 ? null : stageInstancePrivacyLevel);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\u0018\u00002\u00020\u0001BC\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\r\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\u0010\b\u0002\u0010\u0007\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006\u0012\u0010\b\u0002\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010\t¢\u0006\u0004\b\u0010\u0010\u0011R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u001e\u0010\u0007\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00068\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0007\u0010\bR\u001e\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u0018\u0010\u000e\u001a\u0004\u0018\u00010\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000f¨\u0006\u0012"}, d2 = {"Lcom/discord/restapi/RestAPIParams$UpdateSubscription;", "", "", "paymentSourceId", "Ljava/lang/String;", "", "Lcom/discord/primitives/PlanId;", "planId", "Ljava/lang/Long;", "", "Lcom/discord/models/domain/ModelSubscription$SubscriptionAdditionalPlan;", "additionalPlans", "Ljava/util/List;", "", "status", "Ljava/lang/Integer;", HookHelper.constructorName, "(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class UpdateSubscription {
        private final List<ModelSubscription.SubscriptionAdditionalPlan> additionalPlans;
        private final String paymentSourceId;
        private final Long planId;
        private final Integer status;

        public UpdateSubscription() {
            this(null, null, null, null, 15, null);
        }

        public UpdateSubscription(Integer num, String str, Long l, List<ModelSubscription.SubscriptionAdditionalPlan> list) {
            this.status = num;
            this.paymentSourceId = str;
            this.planId = l;
            this.additionalPlans = list;
        }

        public /* synthetic */ UpdateSubscription(Integer num, String str, Long l, List list, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : num, (i & 2) != 0 ? null : str, (i & 4) != 0 ? null : l, (i & 8) != 0 ? null : list);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B)\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\t¢\u0006\u0004\b\f\u0010\rR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u001c\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0007\u0010\bR\u0018\u0010\n\u001a\u0004\u0018\u00010\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/restapi/RestAPIParams$UploadContacts;", "", "", "background", "Z", "", "Lcom/discord/restapi/RestAPIParams$ContactEntry;", "friend_list_entries", "Ljava/util/List;", "Lcom/discord/api/friendsuggestions/AllowedInSuggestionsType;", "set_use_for_suggestions", "Lcom/discord/api/friendsuggestions/AllowedInSuggestionsType;", HookHelper.constructorName, "(Ljava/util/List;ZLcom/discord/api/friendsuggestions/AllowedInSuggestionsType;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class UploadContacts {
        private final boolean background;
        private final List<ContactEntry> friend_list_entries;
        private final AllowedInSuggestionsType set_use_for_suggestions;

        public UploadContacts(List<ContactEntry> list, boolean z2, AllowedInSuggestionsType allowedInSuggestionsType) {
            m.checkNotNullParameter(list, "friend_list_entries");
            this.friend_list_entries = list;
            this.background = z2;
            this.set_use_for_suggestions = allowedInSuggestionsType;
        }

        public /* synthetic */ UploadContacts(List list, boolean z2, AllowedInSuggestionsType allowedInSuggestionsType, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(list, z2, (i & 4) != 0 ? AllowedInSuggestionsType.DISABLED : allowedInSuggestionsType);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u001b\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0006\u0010\u0007B\t\b\u0016¢\u0006\u0004\b\u0006\u0010\bR\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0018\u0010\u0005\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\t"}, d2 = {"Lcom/discord/restapi/RestAPIParams$UserAgreements;", "", "", "terms", "Ljava/lang/Boolean;", "privacy", HookHelper.constructorName, "(Ljava/lang/Boolean;Ljava/lang/Boolean;)V", "()V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class UserAgreements {
        private final Boolean privacy;
        private final Boolean terms;

        public UserAgreements(Boolean bool, Boolean bool2) {
            this.terms = bool;
            this.privacy = bool2;
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public UserAgreements() {
            /*
                r1 = this;
                java.lang.Boolean r0 = java.lang.Boolean.TRUE
                r1.<init>(r0, r0)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.restapi.RestAPIParams.UserAgreements.<init>():void");
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B!\u0012\u0010\u0010\n\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002\u0012\u0006\u0010\u000b\u001a\u00020\u0007¢\u0006\u0004\b\u0018\u0010\u0019J\u001a\u0010\u0005\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002HÂ\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u0010\u0010\b\u001a\u00020\u0007HÂ\u0003¢\u0006\u0004\b\b\u0010\tJ.\u0010\f\u001a\u00020\u00002\u0012\b\u0002\u0010\n\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00022\b\b\u0002\u0010\u000b\u001a\u00020\u0007HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\u000e\u0010\tJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0016\u0010\u000b\u001a\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\u0016R \u0010\n\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u0017¨\u0006\u001a"}, d2 = {"Lcom/discord/restapi/RestAPIParams$UserBulkRelationship;", "", "", "", "Lcom/discord/primitives/UserId;", "component1", "()Ljava/util/List;", "", "component2", "()Ljava/lang/String;", "user_ids", "token", "copy", "(Ljava/util/List;Ljava/lang/String;)Lcom/discord/restapi/RestAPIParams$UserBulkRelationship;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "Ljava/util/List;", HookHelper.constructorName, "(Ljava/util/List;Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class UserBulkRelationship {
        private final String token;
        private final List<Long> user_ids;

        public UserBulkRelationship(List<Long> list, String str) {
            m.checkNotNullParameter(list, "user_ids");
            m.checkNotNullParameter(str, "token");
            this.user_ids = list;
            this.token = str;
        }

        private final List<Long> component1() {
            return this.user_ids;
        }

        private final String component2() {
            return this.token;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ UserBulkRelationship copy$default(UserBulkRelationship userBulkRelationship, List list, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                list = userBulkRelationship.user_ids;
            }
            if ((i & 2) != 0) {
                str = userBulkRelationship.token;
            }
            return userBulkRelationship.copy(list, str);
        }

        public final UserBulkRelationship copy(List<Long> list, String str) {
            m.checkNotNullParameter(list, "user_ids");
            m.checkNotNullParameter(str, "token");
            return new UserBulkRelationship(list, str);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof UserBulkRelationship)) {
                return false;
            }
            UserBulkRelationship userBulkRelationship = (UserBulkRelationship) obj;
            return m.areEqual(this.user_ids, userBulkRelationship.user_ids) && m.areEqual(this.token, userBulkRelationship.token);
        }

        public int hashCode() {
            List<Long> list = this.user_ids;
            int i = 0;
            int hashCode = (list != null ? list.hashCode() : 0) * 31;
            String str = this.token;
            if (str != null) {
                i = str.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("UserBulkRelationship(user_ids=");
            R.append(this.user_ids);
            R.append(", token=");
            return a.H(R, this.token, ")");
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0011\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082D¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/restapi/RestAPIParams$UserDevices;", "", "", "token", "Ljava/lang/String;", "provider", HookHelper.constructorName, "(Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class UserDevices {
        private final String provider = "gcm";
        private final String token;

        public UserDevices(String str) {
            this.token = str;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000e\u001a\u00020\r2\b\u0010\f\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u000e\u0010\u000fR\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0010\u001a\u0004\b\u0011\u0010\u0004¨\u0006\u0014"}, d2 = {"Lcom/discord/restapi/RestAPIParams$UserEmailConfirmCode;", "", "", "component1", "()Ljava/lang/String;", ModelAuditLogEntry.CHANGE_KEY_CODE, "copy", "(Ljava/lang/String;)Lcom/discord/restapi/RestAPIParams$UserEmailConfirmCode;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getCode", HookHelper.constructorName, "(Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class UserEmailConfirmCode {
        private final String code;

        public UserEmailConfirmCode(String str) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_CODE);
            this.code = str;
        }

        public static /* synthetic */ UserEmailConfirmCode copy$default(UserEmailConfirmCode userEmailConfirmCode, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                str = userEmailConfirmCode.code;
            }
            return userEmailConfirmCode.copy(str);
        }

        public final String component1() {
            return this.code;
        }

        public final UserEmailConfirmCode copy(String str) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_CODE);
            return new UserEmailConfirmCode(str);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof UserEmailConfirmCode) && m.areEqual(this.code, ((UserEmailConfirmCode) obj).code);
            }
            return true;
        }

        public final String getCode() {
            return this.code;
        }

        public int hashCode() {
            String str = this.code;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return a.H(a.R("UserEmailConfirmCode(code="), this.code, ")");
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0001$Bg\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0012\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u0019\u0012\u0016\b\u0002\u0010\u0005\u001a\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u001e\u0010\u001fB\u001d\b\u0016\u0012\n\u0010!\u001a\u00060\u0003j\u0002` \u0012\u0006\u0010\"\u001a\u00020\u0004¢\u0006\u0004\b\u001e\u0010#R'\u0010\u0005\u001a\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0006\u001a\u0004\b\u0007\u0010\bR\u001b\u0010\n\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\f\u0010\rR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000b\u001a\u0004\b\u000f\u0010\rR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u000b\u001a\u0004\b\u0011\u0010\rR\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u000b\u001a\u0004\b\u0018\u0010\rR\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00198\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u001d¨\u0006%"}, d2 = {"Lcom/discord/restapi/RestAPIParams$UserGuildSettings;", "", "", "", "Lcom/discord/restapi/RestAPIParams$UserGuildSettings$ChannelOverride;", "channelOverrides", "Ljava/util/Map;", "getChannelOverrides", "()Ljava/util/Map;", "", "suppressEveryone", "Ljava/lang/Boolean;", "getSuppressEveryone", "()Ljava/lang/Boolean;", "mobilePush", "getMobilePush", "suppressRoles", "getSuppressRoles", "Lcom/discord/models/domain/ModelMuteConfig;", "muteConfig", "Lcom/discord/models/domain/ModelMuteConfig;", "getMuteConfig", "()Lcom/discord/models/domain/ModelMuteConfig;", "muted", "getMuted", "", "messageNotifications", "Ljava/lang/Integer;", "getMessageNotifications", "()Ljava/lang/Integer;", HookHelper.constructorName, "(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/discord/models/domain/ModelMuteConfig;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/util/Map;)V", "Lcom/discord/primitives/ChannelId;", "channelId", "channelOverride", "(JLcom/discord/restapi/RestAPIParams$UserGuildSettings$ChannelOverride;)V", "ChannelOverride", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class UserGuildSettings {
        private final Map<Long, ChannelOverride> channelOverrides;
        private final Integer messageNotifications;
        private final Boolean mobilePush;
        private final ModelMuteConfig muteConfig;
        private final Boolean muted;
        private final Boolean suppressEveryone;
        private final Boolean suppressRoles;

        /* compiled from: RestAPIParams.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001B'\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\r\u001a\u0004\u0018\u00010\f\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0011\u0010\u0012B\u0013\b\u0016\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0011\u0010\u0013R\u001b\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u001b\u0010\b\u001a\u0004\u0018\u00010\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR\u001b\u0010\r\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u0014"}, d2 = {"Lcom/discord/restapi/RestAPIParams$UserGuildSettings$ChannelOverride;", "", "", "messageNotifications", "Ljava/lang/Integer;", "getMessageNotifications", "()Ljava/lang/Integer;", "", "muted", "Ljava/lang/Boolean;", "getMuted", "()Ljava/lang/Boolean;", "Lcom/discord/models/domain/ModelMuteConfig;", "muteConfig", "Lcom/discord/models/domain/ModelMuteConfig;", "getMuteConfig", "()Lcom/discord/models/domain/ModelMuteConfig;", HookHelper.constructorName, "(Ljava/lang/Boolean;Lcom/discord/models/domain/ModelMuteConfig;Ljava/lang/Integer;)V", "(Ljava/lang/Integer;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class ChannelOverride {
            private final Integer messageNotifications;
            private final ModelMuteConfig muteConfig;
            private final Boolean muted;

            public ChannelOverride(Boolean bool, ModelMuteConfig modelMuteConfig, Integer num) {
                this.muted = bool;
                this.muteConfig = modelMuteConfig;
                this.messageNotifications = num;
            }

            public final Integer getMessageNotifications() {
                return this.messageNotifications;
            }

            public final ModelMuteConfig getMuteConfig() {
                return this.muteConfig;
            }

            public final Boolean getMuted() {
                return this.muted;
            }

            public /* synthetic */ ChannelOverride(Boolean bool, ModelMuteConfig modelMuteConfig, Integer num, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this(bool, modelMuteConfig, (i & 4) != 0 ? null : num);
            }

            public ChannelOverride(Integer num) {
                this(null, null, num);
            }
        }

        public UserGuildSettings() {
            this(null, null, null, null, null, null, null, Opcodes.LAND, null);
        }

        public UserGuildSettings(Boolean bool, Boolean bool2, Boolean bool3, ModelMuteConfig modelMuteConfig, Boolean bool4, Integer num, Map<Long, ChannelOverride> map) {
            this.suppressEveryone = bool;
            this.suppressRoles = bool2;
            this.muted = bool3;
            this.muteConfig = modelMuteConfig;
            this.mobilePush = bool4;
            this.messageNotifications = num;
            this.channelOverrides = map;
        }

        public final Map<Long, ChannelOverride> getChannelOverrides() {
            return this.channelOverrides;
        }

        public final Integer getMessageNotifications() {
            return this.messageNotifications;
        }

        public final Boolean getMobilePush() {
            return this.mobilePush;
        }

        public final ModelMuteConfig getMuteConfig() {
            return this.muteConfig;
        }

        public final Boolean getMuted() {
            return this.muted;
        }

        public final Boolean getSuppressEveryone() {
            return this.suppressEveryone;
        }

        public final Boolean getSuppressRoles() {
            return this.suppressRoles;
        }

        public /* synthetic */ UserGuildSettings(Boolean bool, Boolean bool2, Boolean bool3, ModelMuteConfig modelMuteConfig, Boolean bool4, Integer num, Map map, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : bool, (i & 2) != 0 ? null : bool2, (i & 4) != 0 ? null : bool3, (i & 8) != 0 ? null : modelMuteConfig, (i & 16) != 0 ? null : bool4, (i & 32) != 0 ? null : num, (i & 64) != 0 ? null : map);
        }

        /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
        public UserGuildSettings(long j, ChannelOverride channelOverride) {
            this(null, null, null, null, null, null, g0.mapOf(d0.o.to(Long.valueOf(j), channelOverride)), 63, null);
            m.checkNotNullParameter(channelOverride, "channelOverride");
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u0086\b\u0018\u00002\u00020\u0001B\u0011\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001c\u0010\u0006\u001a\u00020\u00002\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000e\u001a\u00020\r2\b\u0010\f\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u000e\u0010\u000fR\u0018\u0010\u0005\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0010¨\u0006\u0013"}, d2 = {"Lcom/discord/restapi/RestAPIParams$UserNoteUpdate;", "", "", "component1", "()Ljava/lang/String;", "note", "copy", "(Ljava/lang/String;)Lcom/discord/restapi/RestAPIParams$UserNoteUpdate;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class UserNoteUpdate {
        private final String note;

        public UserNoteUpdate(String str) {
            this.note = str;
        }

        private final String component1() {
            return this.note;
        }

        public static /* synthetic */ UserNoteUpdate copy$default(UserNoteUpdate userNoteUpdate, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                str = userNoteUpdate.note;
            }
            return userNoteUpdate.copy(str);
        }

        public final UserNoteUpdate copy(String str) {
            return new UserNoteUpdate(str);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof UserNoteUpdate) && m.areEqual(this.note, ((UserNoteUpdate) obj).note);
            }
            return true;
        }

        public int hashCode() {
            String str = this.note;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return a.H(a.R("UserNoteUpdate(note="), this.note, ")");
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001\u000bB%\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\t\u0010\nR\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0018\u0010\u0006\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007R\u0018\u0010\b\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\u0004¨\u0006\f"}, d2 = {"Lcom/discord/restapi/RestAPIParams$UserRelationship;", "", "", "captchaKey", "Ljava/lang/String;", "", "type", "Ljava/lang/Integer;", "friendToken", HookHelper.constructorName, "(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)V", "Add", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class UserRelationship {
        private final String captchaKey;
        private final String friendToken;
        private final Integer type;

        /* compiled from: RestAPIParams.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\u0005\u001a\u00020\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\t\u0010\nR\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00068\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/restapi/RestAPIParams$UserRelationship$Add;", "", "", "captchaKey", "Ljava/lang/String;", "username", "", "discriminator", "I", HookHelper.constructorName, "(Ljava/lang/String;ILjava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Add {
            private final String captchaKey;
            private final int discriminator;
            private final String username;

            public Add(String str, int i, String str2) {
                m.checkNotNullParameter(str, "username");
                this.username = str;
                this.discriminator = i;
                this.captchaKey = str2;
            }
        }

        public UserRelationship(Integer num, String str, String str2) {
            this.type = num;
            this.friendToken = str;
            this.captchaKey = str2;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0002\b\r\u0018\u0000 #2\u00020\u0001:\u0002#$Bù\u0001\b\u0002\u0012\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u0010\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0010\u0012\u0010\b\u0002\u0010\u0017\u001a\n\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0010\u0012\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u0002\u0012\u0010\b\u0002\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u0018\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u0018\u0012\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u0018¢\u0006\u0004\b!\u0010\"R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u001e\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0007\u0010\bR\u0018\u0010\n\u001a\u0004\u0018\u00010\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000bR\u0018\u0010\f\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\u0004R\u0018\u0010\r\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u0004R\u0018\u0010\u000e\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u0004R\u0018\u0010\u000f\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0004R\u0018\u0010\u0011\u001a\u0004\u0018\u00010\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u0018\u0010\u0013\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0004R\u0018\u0010\u0014\u001a\u0004\u0018\u00010\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0012R\u0018\u0010\u0015\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0015\u0010\u0004R\u001e\u0010\u0017\u001a\n\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\bR\u0018\u0010\u0019\u001a\u0004\u0018\u00010\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u0018\u0010\u001b\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u0004R\u0018\u0010\u001c\u001a\u0004\u0018\u00010\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001aR\u0018\u0010\u001d\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u0004R\u0018\u0010\u001e\u001a\u0004\u0018\u00010\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001e\u0010\u001aR\u0018\u0010\u001f\u001a\u0004\u0018\u00010\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010\u0012R\u0018\u0010 \u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b \u0010\u0004¨\u0006%"}, d2 = {"Lcom/discord/restapi/RestAPIParams$UserSettings;", "", "", "developerMode", "Ljava/lang/Boolean;", "", "Lcom/discord/models/domain/ModelGuildFolder;", "guildFolders", "Ljava/util/Collection;", "Lcom/discord/restapi/RestAPIParams$UserSettings$FriendSourceFlags;", "friendSourceFlags", "Lcom/discord/restapi/RestAPIParams$UserSettings$FriendSourceFlags;", "inlineAttachmentMedia", "allowAccessibilityDetection", "animateEmoji", "inlineEmbedMedia", "", "status", "Ljava/lang/String;", "renderEmbeds", "locale", "defaultGuildsRestricted", "", "restrictedGuilds", "", "animateStickers", "Ljava/lang/Integer;", "blockedMessageBar", "explicitContentFilter", "contactSyncEnabled", "friendDiscoveryFlags", "theme", "showCurrentGame", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/Collection;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/Collection;Ljava/lang/Boolean;Lcom/discord/restapi/RestAPIParams$UserSettings$FriendSourceFlags;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;)V", "Companion", "FriendSourceFlags", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class UserSettings {
        public static final Companion Companion = new Companion(null);
        private final Boolean allowAccessibilityDetection;
        private final Boolean animateEmoji;
        private final Integer animateStickers;
        private final Boolean blockedMessageBar;
        private final Boolean contactSyncEnabled;
        private final Boolean defaultGuildsRestricted;
        private final Boolean developerMode;
        private final Integer explicitContentFilter;
        private final Integer friendDiscoveryFlags;
        private final FriendSourceFlags friendSourceFlags;
        private final Collection<ModelGuildFolder> guildFolders;
        private final Boolean inlineAttachmentMedia;
        private final Boolean inlineEmbedMedia;
        private final String locale;
        private final Boolean renderEmbeds;
        private final Collection<Long> restrictedGuilds;
        private final Boolean showCurrentGame;
        private final String status;
        private final String theme;

        /* compiled from: RestAPIParams.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u001e\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0014\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b;\u0010<J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0007¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\f\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\u0007H\u0007¢\u0006\u0004\b\f\u0010\nJ\u0017\u0010\u000e\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u0007H\u0007¢\u0006\u0004\b\u000e\u0010\nJ\u0017\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u0007H\u0007¢\u0006\u0004\b\u0010\u0010\nJ\u0017\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u0007H\u0007¢\u0006\u0004\b\u0012\u0010\nJ\u0017\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0014\u0010\u0006J\u0017\u0010\u0017\u001a\u00020\u00042\u0006\u0010\u0016\u001a\u00020\u0015H\u0007¢\u0006\u0004\b\u0017\u0010\u0018J\u001d\u0010\u001c\u001a\u00020\u00042\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001a0\u0019H\u0007¢\u0006\u0004\b\u001c\u0010\u001dJ)\u0010\"\u001a\u00020\u00042\b\u0010\u001e\u001a\u0004\u0018\u00010\u00072\u000e\u0010!\u001a\n\u0012\u0004\u0012\u00020 \u0018\u00010\u001fH\u0007¢\u0006\u0004\b\"\u0010#J-\u0010'\u001a\u00020\u00042\b\u0010$\u001a\u0004\u0018\u00010\u00072\b\u0010%\u001a\u0004\u0018\u00010\u00072\b\u0010&\u001a\u0004\u0018\u00010\u0007H\u0007¢\u0006\u0004\b'\u0010(J\u0017\u0010+\u001a\u00020\u00042\u0006\u0010*\u001a\u00020)H\u0007¢\u0006\u0004\b+\u0010,J\u0017\u0010.\u001a\u00020\u00042\u0006\u0010-\u001a\u00020\u0007H\u0007¢\u0006\u0004\b.\u0010\nJ\u0019\u00100\u001a\u00020\u00042\b\u0010/\u001a\u0004\u0018\u00010\u0007H\u0007¢\u0006\u0004\b0\u00101J\u0019\u00103\u001a\u00020\u00042\b\u00102\u001a\u0004\u0018\u00010\u0007H\u0007¢\u0006\u0004\b3\u00101J\u0019\u00105\u001a\u00020\u00042\b\u00104\u001a\u0004\u0018\u00010)H\u0007¢\u0006\u0004\b5\u00106J\u0019\u00108\u001a\u00020\u00042\b\u00107\u001a\u0004\u0018\u00010)H\u0007¢\u0006\u0004\b8\u00106J\u000f\u00109\u001a\u00020\u0004H\u0007¢\u0006\u0004\b9\u0010:¨\u0006="}, d2 = {"Lcom/discord/restapi/RestAPIParams$UserSettings$Companion;", "", "", "theme", "Lcom/discord/restapi/RestAPIParams$UserSettings;", "createWithTheme", "(Ljava/lang/String;)Lcom/discord/restapi/RestAPIParams$UserSettings;", "", "developerMode", "createWithDeveloperMode", "(Z)Lcom/discord/restapi/RestAPIParams$UserSettings;", "renderEmbeds", "createWithRenderEmbeds", "inlineEmbedMedia", "createWithInlineEmbedMedia", "inlineAttachmentMedia", "createWithInlineAttachmentMedia", "blockedMessageBar", "createWithBlockedMessageBar", "locale", "createWithLocale", "Lcom/discord/api/presence/ClientStatus;", "status", "createWithStatus", "(Lcom/discord/api/presence/ClientStatus;)Lcom/discord/restapi/RestAPIParams$UserSettings;", "", "Lcom/discord/models/domain/ModelGuildFolder;", "guildFolders", "createWithGuildFolders", "(Ljava/util/List;)Lcom/discord/restapi/RestAPIParams$UserSettings;", "defaultGuildsRestricted", "", "", "restrictedGuilds", "createWithRestrictedGuilds", "(Ljava/lang/Boolean;Ljava/util/Collection;)Lcom/discord/restapi/RestAPIParams$UserSettings;", "all", "mutualGuilds", "mutualFriends", "createWithFriendSourceFlags", "(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/discord/restapi/RestAPIParams$UserSettings;", "", "explicitContentFilter", "createWithExplicitContentFilter", "(I)Lcom/discord/restapi/RestAPIParams$UserSettings;", "showCurrentGame", "createWithShowCurrentGame", "allowAnimatedEmojis", "createWithAllowAnimatedEmojis", "(Ljava/lang/Boolean;)Lcom/discord/restapi/RestAPIParams$UserSettings;", "allowAccessibilityDetection", "createWithAllowAccessibilityDetection", "stickerAnimationSettings", "createWithStickerAnimationSettings", "(Ljava/lang/Integer;)Lcom/discord/restapi/RestAPIParams$UserSettings;", "friendDiscoveryFlags", "createWithFriendDiscoveryFlags", "createWithContactSyncUpsellShown", "()Lcom/discord/restapi/RestAPIParams$UserSettings;", HookHelper.constructorName, "()V", "restapi_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Companion {
            private Companion() {
            }

            public final UserSettings createWithAllowAccessibilityDetection(Boolean bool) {
                return new UserSettings(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, bool, null, null, null, 491519, null);
            }

            public final UserSettings createWithAllowAnimatedEmojis(Boolean bool) {
                return new UserSettings(null, null, null, null, null, null, null, null, null, null, null, null, null, null, bool, null, null, null, null, 507903, null);
            }

            public final UserSettings createWithBlockedMessageBar(boolean z2) {
                return new UserSettings(null, null, null, null, null, Boolean.valueOf(z2), null, null, null, null, null, null, null, null, null, null, null, null, null, 524255, null);
            }

            public final UserSettings createWithContactSyncUpsellShown() {
                return new UserSettings(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, Boolean.TRUE, null, 393215, null);
            }

            public final UserSettings createWithDeveloperMode(boolean z2) {
                return new UserSettings(null, Boolean.valueOf(z2), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 524285, null);
            }

            public final UserSettings createWithExplicitContentFilter(int i) {
                return new UserSettings(null, null, null, null, null, null, null, null, null, null, null, null, null, Integer.valueOf(i), null, null, null, null, null, 516095, null);
            }

            public final UserSettings createWithFriendDiscoveryFlags(Integer num) {
                return new UserSettings(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, num, 262143, null);
            }

            public final UserSettings createWithFriendSourceFlags(Boolean bool, Boolean bool2, Boolean bool3) {
                return new UserSettings(null, null, null, null, null, null, null, null, null, null, null, null, new FriendSourceFlags(bool, bool2, bool3), null, null, null, null, null, null, 520191, null);
            }

            public final UserSettings createWithGuildFolders(List<ModelGuildFolder> list) {
                m.checkNotNullParameter(list, "guildFolders");
                return new UserSettings(null, null, null, null, null, null, null, null, null, null, list, null, null, null, null, null, null, null, null, 523263, null);
            }

            public final UserSettings createWithInlineAttachmentMedia(boolean z2) {
                return new UserSettings(null, null, null, null, Boolean.valueOf(z2), null, null, null, null, null, null, null, null, null, null, null, null, null, null, 524271, null);
            }

            public final UserSettings createWithInlineEmbedMedia(boolean z2) {
                return new UserSettings(null, null, null, Boolean.valueOf(z2), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 524279, null);
            }

            public final UserSettings createWithLocale(String str) {
                m.checkNotNullParameter(str, "locale");
                return new UserSettings(null, null, null, null, null, null, str, null, null, null, null, null, null, null, null, null, null, null, null, 524223, null);
            }

            public final UserSettings createWithRenderEmbeds(boolean z2) {
                return new UserSettings(null, null, Boolean.valueOf(z2), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 524283, null);
            }

            public final UserSettings createWithRestrictedGuilds(Boolean bool, Collection<Long> collection) {
                return new UserSettings(null, null, null, null, null, null, null, collection, null, null, null, bool, null, null, null, null, null, null, null, 522111, null);
            }

            public final UserSettings createWithShowCurrentGame(boolean z2) {
                return new UserSettings(null, null, null, null, null, null, null, null, null, Boolean.valueOf(z2), null, null, null, null, null, null, null, null, null, 523775, null);
            }

            public final UserSettings createWithStatus(ClientStatus clientStatus) {
                m.checkNotNullParameter(clientStatus, "status");
                String name = clientStatus.name();
                Locale locale = Locale.ROOT;
                m.checkNotNullExpressionValue(locale, "Locale.ROOT");
                Objects.requireNonNull(name, "null cannot be cast to non-null type java.lang.String");
                String lowerCase = name.toLowerCase(locale);
                m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                return new UserSettings(null, null, null, null, null, null, null, null, lowerCase, null, null, null, null, null, null, null, null, null, null, 524031, null);
            }

            public final UserSettings createWithStickerAnimationSettings(Integer num) {
                return new UserSettings(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, num, null, null, 458751, null);
            }

            public final UserSettings createWithTheme(String str) {
                m.checkNotNullParameter(str, "theme");
                return new UserSettings(str, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 524286, null);
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* compiled from: RestAPIParams.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0002\u0018\u00002\u00020\u0001B%\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0018\u0010\u0005\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0018\u0010\u0006\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004¨\u0006\t"}, d2 = {"Lcom/discord/restapi/RestAPIParams$UserSettings$FriendSourceFlags;", "", "", "mutualFriends", "Ljava/lang/Boolean;", "mutualGuilds", "all", HookHelper.constructorName, "(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class FriendSourceFlags {
            private final Boolean all;
            private final Boolean mutualFriends;
            private final Boolean mutualGuilds;

            public FriendSourceFlags(Boolean bool, Boolean bool2, Boolean bool3) {
                this.all = bool;
                this.mutualGuilds = bool2;
                this.mutualFriends = bool3;
            }
        }

        private UserSettings(String str, Boolean bool, Boolean bool2, Boolean bool3, Boolean bool4, Boolean bool5, String str2, Collection<Long> collection, String str3, Boolean bool6, Collection<ModelGuildFolder> collection2, Boolean bool7, FriendSourceFlags friendSourceFlags, Integer num, Boolean bool8, Boolean bool9, Integer num2, Boolean bool10, Integer num3) {
            this.theme = str;
            this.developerMode = bool;
            this.renderEmbeds = bool2;
            this.inlineEmbedMedia = bool3;
            this.inlineAttachmentMedia = bool4;
            this.blockedMessageBar = bool5;
            this.locale = str2;
            this.restrictedGuilds = collection;
            this.status = str3;
            this.showCurrentGame = bool6;
            this.guildFolders = collection2;
            this.defaultGuildsRestricted = bool7;
            this.friendSourceFlags = friendSourceFlags;
            this.explicitContentFilter = num;
            this.animateEmoji = bool8;
            this.allowAccessibilityDetection = bool9;
            this.animateStickers = num2;
            this.contactSyncEnabled = bool10;
            this.friendDiscoveryFlags = num3;
        }

        public static final UserSettings createWithAllowAccessibilityDetection(Boolean bool) {
            return Companion.createWithAllowAccessibilityDetection(bool);
        }

        public static final UserSettings createWithAllowAnimatedEmojis(Boolean bool) {
            return Companion.createWithAllowAnimatedEmojis(bool);
        }

        public static final UserSettings createWithBlockedMessageBar(boolean z2) {
            return Companion.createWithBlockedMessageBar(z2);
        }

        public static final UserSettings createWithContactSyncUpsellShown() {
            return Companion.createWithContactSyncUpsellShown();
        }

        public static final UserSettings createWithDeveloperMode(boolean z2) {
            return Companion.createWithDeveloperMode(z2);
        }

        public static final UserSettings createWithExplicitContentFilter(int i) {
            return Companion.createWithExplicitContentFilter(i);
        }

        public static final UserSettings createWithFriendDiscoveryFlags(Integer num) {
            return Companion.createWithFriendDiscoveryFlags(num);
        }

        public static final UserSettings createWithFriendSourceFlags(Boolean bool, Boolean bool2, Boolean bool3) {
            return Companion.createWithFriendSourceFlags(bool, bool2, bool3);
        }

        public static final UserSettings createWithGuildFolders(List<ModelGuildFolder> list) {
            return Companion.createWithGuildFolders(list);
        }

        public static final UserSettings createWithInlineAttachmentMedia(boolean z2) {
            return Companion.createWithInlineAttachmentMedia(z2);
        }

        public static final UserSettings createWithInlineEmbedMedia(boolean z2) {
            return Companion.createWithInlineEmbedMedia(z2);
        }

        public static final UserSettings createWithLocale(String str) {
            return Companion.createWithLocale(str);
        }

        public static final UserSettings createWithRenderEmbeds(boolean z2) {
            return Companion.createWithRenderEmbeds(z2);
        }

        public static final UserSettings createWithRestrictedGuilds(Boolean bool, Collection<Long> collection) {
            return Companion.createWithRestrictedGuilds(bool, collection);
        }

        public static final UserSettings createWithShowCurrentGame(boolean z2) {
            return Companion.createWithShowCurrentGame(z2);
        }

        public static final UserSettings createWithStatus(ClientStatus clientStatus) {
            return Companion.createWithStatus(clientStatus);
        }

        public static final UserSettings createWithStickerAnimationSettings(Integer num) {
            return Companion.createWithStickerAnimationSettings(num);
        }

        public static final UserSettings createWithTheme(String str) {
            return Companion.createWithTheme(str);
        }

        public /* synthetic */ UserSettings(String str, Boolean bool, Boolean bool2, Boolean bool3, Boolean bool4, Boolean bool5, String str2, Collection collection, String str3, Boolean bool6, Collection collection2, Boolean bool7, FriendSourceFlags friendSourceFlags, Integer num, Boolean bool8, Boolean bool9, Integer num2, Boolean bool10, Integer num3, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : str, (i & 2) != 0 ? null : bool, (i & 4) != 0 ? null : bool2, (i & 8) != 0 ? null : bool3, (i & 16) != 0 ? null : bool4, (i & 32) != 0 ? null : bool5, (i & 64) != 0 ? null : str2, (i & 128) != 0 ? null : collection, (i & 256) != 0 ? null : str3, (i & 512) != 0 ? null : bool6, (i & 1024) != 0 ? null : collection2, (i & 2048) != 0 ? null : bool7, (i & 4096) != 0 ? null : friendSourceFlags, (i & 8192) != 0 ? null : num, (i & 16384) != 0 ? null : bool8, (i & 32768) != 0 ? null : bool9, (i & 65536) != 0 ? null : num2, (i & 131072) != 0 ? null : bool10, (i & 262144) != 0 ? null : num3);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0011\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/restapi/RestAPIParams$UserSettingsCustomStatus;", "", "Lcom/discord/models/domain/ModelCustomStatusSetting;", "customStatus", "Lcom/discord/models/domain/ModelCustomStatusSetting;", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelCustomStatusSetting;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class UserSettingsCustomStatus {
        private final ModelCustomStatusSetting customStatus;

        public UserSettingsCustomStatus(ModelCustomStatusSetting modelCustomStatusSetting) {
            this.customStatus = modelCustomStatusSetting;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0013\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/restapi/RestAPIParams$VanityUrl;", "", "", ModelAuditLogEntry.CHANGE_KEY_CODE, "Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class VanityUrl {
        private final String code;

        public VanityUrl() {
            this(null, 1, null);
        }

        public VanityUrl(String str) {
            this.code = str;
        }

        public /* synthetic */ VanityUrl(String str, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : str);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0005\u001a\u00020\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/restapi/RestAPIParams$VerificationCode;", "", "", ModelAuditLogEntry.CHANGE_KEY_CODE, "Ljava/lang/String;", "phone", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class VerificationCode {
        private final String code;
        private final String phone;

        public VerificationCode(String str, String str2) {
            m.checkNotNullParameter(str, "phone");
            m.checkNotNullParameter(str2, ModelAuditLogEntry.CHANGE_KEY_CODE);
            this.phone = str;
            this.code = str2;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/restapi/RestAPIParams$VerificationCodeOnly;", "", "", ModelAuditLogEntry.CHANGE_KEY_CODE, "Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class VerificationCodeOnly {
        private final String code;

        public VerificationCodeOnly(String str) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_CODE);
            this.code = str;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/restapi/RestAPIParams$VerificationCodeResend;", "", "", "phone", "Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class VerificationCodeResend {
        private final String phone;

        public VerificationCodeResend(String str) {
            m.checkNotNullParameter(str, "phone");
            this.phone = str;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004¨\u0006\t"}, d2 = {"Lcom/discord/restapi/RestAPIParams$VerificationPhoneCode;", "", "", "password", "Ljava/lang/String;", "source", "phoneToken", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class VerificationPhoneCode {
        private final String password;
        private final String phoneToken;
        private final String source;

        public VerificationPhoneCode(String str, String str2, String str3) {
            a.n0(str, "phoneToken", str2, "password", str3, "source");
            this.phoneToken = str;
            this.password = str2;
            this.source = str3;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\r\u001a\u00020\u0002\u0012\u0010\b\u0002\u0010\u000e\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006\u0012\b\b\u0002\u0010\u000f\u001a\u00020\t\u0012\b\b\u0002\u0010\u0010\u001a\u00020\t¢\u0006\u0004\b!\u0010\"J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0018\u0010\u0007\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\f\u0010\u000bJ@\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\r\u001a\u00020\u00022\u0010\b\u0002\u0010\u000e\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00062\b\b\u0002\u0010\u000f\u001a\u00020\t2\b\b\u0002\u0010\u0010\u001a\u00020\tHÆ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0004J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u001a\u0010\u0018\u001a\u00020\t2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u0019\u0010\u0010\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001a\u001a\u0004\b\u001b\u0010\u000bR\u0019\u0010\u000f\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001a\u001a\u0004\b\u001c\u0010\u000bR!\u0010\u000e\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001d\u001a\u0004\b\u001e\u0010\bR\u0019\u0010\r\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001f\u001a\u0004\b \u0010\u0004¨\u0006#"}, d2 = {"Lcom/discord/restapi/RestAPIParams$VerifyEmail;", "", "", "component1", "()Ljava/lang/String;", "", "Lcom/discord/primitives/GuildId;", "component2", "()Ljava/lang/Long;", "", "component3", "()Z", "component4", NotificationCompat.CATEGORY_EMAIL, "guildId", "allowMultipleGuilds", "useVerificationCode", "copy", "(Ljava/lang/String;Ljava/lang/Long;ZZ)Lcom/discord/restapi/RestAPIParams$VerifyEmail;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getUseVerificationCode", "getAllowMultipleGuilds", "Ljava/lang/Long;", "getGuildId", "Ljava/lang/String;", "getEmail", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/Long;ZZ)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class VerifyEmail {
        private final boolean allowMultipleGuilds;
        private final String email;
        private final Long guildId;
        private final boolean useVerificationCode;

        public VerifyEmail(String str, Long l, boolean z2, boolean z3) {
            m.checkNotNullParameter(str, NotificationCompat.CATEGORY_EMAIL);
            this.email = str;
            this.guildId = l;
            this.allowMultipleGuilds = z2;
            this.useVerificationCode = z3;
        }

        public static /* synthetic */ VerifyEmail copy$default(VerifyEmail verifyEmail, String str, Long l, boolean z2, boolean z3, int i, Object obj) {
            if ((i & 1) != 0) {
                str = verifyEmail.email;
            }
            if ((i & 2) != 0) {
                l = verifyEmail.guildId;
            }
            if ((i & 4) != 0) {
                z2 = verifyEmail.allowMultipleGuilds;
            }
            if ((i & 8) != 0) {
                z3 = verifyEmail.useVerificationCode;
            }
            return verifyEmail.copy(str, l, z2, z3);
        }

        public final String component1() {
            return this.email;
        }

        public final Long component2() {
            return this.guildId;
        }

        public final boolean component3() {
            return this.allowMultipleGuilds;
        }

        public final boolean component4() {
            return this.useVerificationCode;
        }

        public final VerifyEmail copy(String str, Long l, boolean z2, boolean z3) {
            m.checkNotNullParameter(str, NotificationCompat.CATEGORY_EMAIL);
            return new VerifyEmail(str, l, z2, z3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof VerifyEmail)) {
                return false;
            }
            VerifyEmail verifyEmail = (VerifyEmail) obj;
            return m.areEqual(this.email, verifyEmail.email) && m.areEqual(this.guildId, verifyEmail.guildId) && this.allowMultipleGuilds == verifyEmail.allowMultipleGuilds && this.useVerificationCode == verifyEmail.useVerificationCode;
        }

        public final boolean getAllowMultipleGuilds() {
            return this.allowMultipleGuilds;
        }

        public final String getEmail() {
            return this.email;
        }

        public final Long getGuildId() {
            return this.guildId;
        }

        public final boolean getUseVerificationCode() {
            return this.useVerificationCode;
        }

        public int hashCode() {
            String str = this.email;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            Long l = this.guildId;
            if (l != null) {
                i = l.hashCode();
            }
            int i2 = (hashCode + i) * 31;
            boolean z2 = this.allowMultipleGuilds;
            int i3 = 1;
            if (z2) {
                z2 = true;
            }
            int i4 = z2 ? 1 : 0;
            int i5 = z2 ? 1 : 0;
            int i6 = (i2 + i4) * 31;
            boolean z3 = this.useVerificationCode;
            if (!z3) {
                i3 = z3 ? 1 : 0;
            }
            return i6 + i3;
        }

        public String toString() {
            StringBuilder R = a.R("VerifyEmail(email=");
            R.append(this.email);
            R.append(", guildId=");
            R.append(this.guildId);
            R.append(", allowMultipleGuilds=");
            R.append(this.allowMultipleGuilds);
            R.append(", useVerificationCode=");
            return a.M(R, this.useVerificationCode, ")");
        }

        public /* synthetic */ VerifyEmail(String str, Long l, boolean z2, boolean z3, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(str, (i & 2) != 0 ? null : l, (i & 4) != 0 ? false : z2, (i & 8) != 0 ? true : z3);
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\n\u001a\u00020\u0002\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\u000e\u0010\f\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0018\u0010\b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ6\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u00022\b\b\u0002\u0010\u000b\u001a\u00020\u00022\u0010\b\u0002\u0010\f\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007HÆ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0004J\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u0019\u0010\n\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0017\u001a\u0004\b\u0018\u0010\u0004R\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0017\u001a\u0004\b\u0019\u0010\u0004R!\u0010\f\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001a\u001a\u0004\b\u001b\u0010\t¨\u0006\u001e"}, d2 = {"Lcom/discord/restapi/RestAPIParams$VerifyEmailCode;", "", "", "component1", "()Ljava/lang/String;", "component2", "", "Lcom/discord/primitives/GuildId;", "component3", "()Ljava/lang/Long;", NotificationCompat.CATEGORY_EMAIL, ModelAuditLogEntry.CHANGE_KEY_CODE, "guildId", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Lcom/discord/restapi/RestAPIParams$VerifyEmailCode;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getEmail", "getCode", "Ljava/lang/Long;", "getGuildId", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class VerifyEmailCode {
        private final String code;
        private final String email;
        private final Long guildId;

        public VerifyEmailCode(String str, String str2, Long l) {
            m.checkNotNullParameter(str, NotificationCompat.CATEGORY_EMAIL);
            m.checkNotNullParameter(str2, ModelAuditLogEntry.CHANGE_KEY_CODE);
            this.email = str;
            this.code = str2;
            this.guildId = l;
        }

        public static /* synthetic */ VerifyEmailCode copy$default(VerifyEmailCode verifyEmailCode, String str, String str2, Long l, int i, Object obj) {
            if ((i & 1) != 0) {
                str = verifyEmailCode.email;
            }
            if ((i & 2) != 0) {
                str2 = verifyEmailCode.code;
            }
            if ((i & 4) != 0) {
                l = verifyEmailCode.guildId;
            }
            return verifyEmailCode.copy(str, str2, l);
        }

        public final String component1() {
            return this.email;
        }

        public final String component2() {
            return this.code;
        }

        public final Long component3() {
            return this.guildId;
        }

        public final VerifyEmailCode copy(String str, String str2, Long l) {
            m.checkNotNullParameter(str, NotificationCompat.CATEGORY_EMAIL);
            m.checkNotNullParameter(str2, ModelAuditLogEntry.CHANGE_KEY_CODE);
            return new VerifyEmailCode(str, str2, l);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof VerifyEmailCode)) {
                return false;
            }
            VerifyEmailCode verifyEmailCode = (VerifyEmailCode) obj;
            return m.areEqual(this.email, verifyEmailCode.email) && m.areEqual(this.code, verifyEmailCode.code) && m.areEqual(this.guildId, verifyEmailCode.guildId);
        }

        public final String getCode() {
            return this.code;
        }

        public final String getEmail() {
            return this.email;
        }

        public final Long getGuildId() {
            return this.guildId;
        }

        public int hashCode() {
            String str = this.email;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.code;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            Long l = this.guildId;
            if (l != null) {
                i = l.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("VerifyEmailCode(email=");
            R.append(this.email);
            R.append(", code=");
            R.append(this.code);
            R.append(", guildId=");
            return a.F(R, this.guildId, ")");
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B-\u0012\n\u0010\f\u001a\u00060\u0002j\u0002`\u0003\u0012\u000e\u0010\r\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0006\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\t¢\u0006\u0004\b\u001f\u0010 J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0018\u0010\u0007\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ<\u0010\u000f\u001a\u00020\u00002\f\b\u0002\u0010\f\u001a\u00060\u0002j\u0002`\u00032\u0010\b\u0002\u0010\r\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00062\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\tHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\u0011\u0010\u000bJ\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0017\u001a\u00020\u00162\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R!\u0010\r\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u0019\u001a\u0004\b\u001a\u0010\bR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001b\u001a\u0004\b\u001c\u0010\u000bR\u001d\u0010\f\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001d\u001a\u0004\b\u001e\u0010\u0005¨\u0006!"}, d2 = {"Lcom/discord/restapi/RestAPIParams$VerifyPurchaseResponse;", "", "", "Lcom/discord/primitives/SkuId;", "component1", "()J", "Lcom/discord/primitives/PlanId;", "component2", "()Ljava/lang/Long;", "", "component3", "()Ljava/lang/String;", "verifiedSkuId", "subscriptionPlanId", "giftCode", "copy", "(JLjava/lang/Long;Ljava/lang/String;)Lcom/discord/restapi/RestAPIParams$VerifyPurchaseResponse;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Long;", "getSubscriptionPlanId", "Ljava/lang/String;", "getGiftCode", "J", "getVerifiedSkuId", HookHelper.constructorName, "(JLjava/lang/Long;Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class VerifyPurchaseResponse {
        private final String giftCode;
        private final Long subscriptionPlanId;
        private final long verifiedSkuId;

        public VerifyPurchaseResponse(long j, Long l, String str) {
            this.verifiedSkuId = j;
            this.subscriptionPlanId = l;
            this.giftCode = str;
        }

        public static /* synthetic */ VerifyPurchaseResponse copy$default(VerifyPurchaseResponse verifyPurchaseResponse, long j, Long l, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                j = verifyPurchaseResponse.verifiedSkuId;
            }
            if ((i & 2) != 0) {
                l = verifyPurchaseResponse.subscriptionPlanId;
            }
            if ((i & 4) != 0) {
                str = verifyPurchaseResponse.giftCode;
            }
            return verifyPurchaseResponse.copy(j, l, str);
        }

        public final long component1() {
            return this.verifiedSkuId;
        }

        public final Long component2() {
            return this.subscriptionPlanId;
        }

        public final String component3() {
            return this.giftCode;
        }

        public final VerifyPurchaseResponse copy(long j, Long l, String str) {
            return new VerifyPurchaseResponse(j, l, str);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof VerifyPurchaseResponse)) {
                return false;
            }
            VerifyPurchaseResponse verifyPurchaseResponse = (VerifyPurchaseResponse) obj;
            return this.verifiedSkuId == verifyPurchaseResponse.verifiedSkuId && m.areEqual(this.subscriptionPlanId, verifyPurchaseResponse.subscriptionPlanId) && m.areEqual(this.giftCode, verifyPurchaseResponse.giftCode);
        }

        public final String getGiftCode() {
            return this.giftCode;
        }

        public final Long getSubscriptionPlanId() {
            return this.subscriptionPlanId;
        }

        public final long getVerifiedSkuId() {
            return this.verifiedSkuId;
        }

        public int hashCode() {
            long j = this.verifiedSkuId;
            int i = ((int) (j ^ (j >>> 32))) * 31;
            Long l = this.subscriptionPlanId;
            int i2 = 0;
            int hashCode = (i + (l != null ? l.hashCode() : 0)) * 31;
            String str = this.giftCode;
            if (str != null) {
                i2 = str.hashCode();
            }
            return hashCode + i2;
        }

        public String toString() {
            StringBuilder R = a.R("VerifyPurchaseResponse(verifiedSkuId=");
            R.append(this.verifiedSkuId);
            R.append(", subscriptionPlanId=");
            R.append(this.subscriptionPlanId);
            R.append(", giftCode=");
            return a.H(R, this.giftCode, ")");
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001BC\u0012\u0006\u0010\r\u001a\u00020\u0002\u0012\n\u0010\u000e\u001a\u00060\u0005j\u0002`\u0006\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\u000e\u0010\u0010\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\n\u0012\u000e\u0010\u0011\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\n¢\u0006\u0004\b#\u0010$J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0014\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\t\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\u0004J\u0018\u0010\u000b\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\u0004J\u0018\u0010\f\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\nHÆ\u0003¢\u0006\u0004\b\f\u0010\u0004JV\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\r\u001a\u00020\u00022\f\b\u0002\u0010\u000e\u001a\u00060\u0005j\u0002`\u00062\b\b\u0002\u0010\u000f\u001a\u00020\u00022\u0010\b\u0002\u0010\u0010\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\n2\u0010\b\u0002\u0010\u0011\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\nHÆ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0004J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u001a\u0010\u001a\u001a\u00020\u00192\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001a\u0010\u001bR\u0019\u0010\r\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001c\u001a\u0004\b\u001d\u0010\u0004R\u001d\u0010\u000e\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001e\u001a\u0004\b\u001f\u0010\bR\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001c\u001a\u0004\b \u0010\u0004R!\u0010\u0010\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001c\u001a\u0004\b!\u0010\u0004R!\u0010\u0011\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001c\u001a\u0004\b\"\u0010\u0004¨\u0006%"}, d2 = {"Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;", "", "", "component1", "()Ljava/lang/String;", "", "Lcom/discord/primitives/UserId;", "component2", "()J", "component3", "Lcom/discord/primitives/PaymentGatewaySkuId;", "component4", "component5", "purchaseToken", "userId", "packageName", "subscriptionSkuId", "oneTimePurchaseSkuId", "copy", "(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/restapi/RestAPIParams$VerifyPurchaseTokenBody;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getPurchaseToken", "J", "getUserId", "getPackageName", "getSubscriptionSkuId", "getOneTimePurchaseSkuId", HookHelper.constructorName, "(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class VerifyPurchaseTokenBody {
        private final String oneTimePurchaseSkuId;
        private final String packageName;
        private final String purchaseToken;
        private final String subscriptionSkuId;
        private final long userId;

        public VerifyPurchaseTokenBody(String str, long j, String str2, String str3, String str4) {
            m.checkNotNullParameter(str, "purchaseToken");
            m.checkNotNullParameter(str2, "packageName");
            this.purchaseToken = str;
            this.userId = j;
            this.packageName = str2;
            this.subscriptionSkuId = str3;
            this.oneTimePurchaseSkuId = str4;
        }

        public static /* synthetic */ VerifyPurchaseTokenBody copy$default(VerifyPurchaseTokenBody verifyPurchaseTokenBody, String str, long j, String str2, String str3, String str4, int i, Object obj) {
            if ((i & 1) != 0) {
                str = verifyPurchaseTokenBody.purchaseToken;
            }
            if ((i & 2) != 0) {
                j = verifyPurchaseTokenBody.userId;
            }
            long j2 = j;
            if ((i & 4) != 0) {
                str2 = verifyPurchaseTokenBody.packageName;
            }
            String str5 = str2;
            if ((i & 8) != 0) {
                str3 = verifyPurchaseTokenBody.subscriptionSkuId;
            }
            String str6 = str3;
            if ((i & 16) != 0) {
                str4 = verifyPurchaseTokenBody.oneTimePurchaseSkuId;
            }
            return verifyPurchaseTokenBody.copy(str, j2, str5, str6, str4);
        }

        public final String component1() {
            return this.purchaseToken;
        }

        public final long component2() {
            return this.userId;
        }

        public final String component3() {
            return this.packageName;
        }

        public final String component4() {
            return this.subscriptionSkuId;
        }

        public final String component5() {
            return this.oneTimePurchaseSkuId;
        }

        public final VerifyPurchaseTokenBody copy(String str, long j, String str2, String str3, String str4) {
            m.checkNotNullParameter(str, "purchaseToken");
            m.checkNotNullParameter(str2, "packageName");
            return new VerifyPurchaseTokenBody(str, j, str2, str3, str4);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof VerifyPurchaseTokenBody)) {
                return false;
            }
            VerifyPurchaseTokenBody verifyPurchaseTokenBody = (VerifyPurchaseTokenBody) obj;
            return m.areEqual(this.purchaseToken, verifyPurchaseTokenBody.purchaseToken) && this.userId == verifyPurchaseTokenBody.userId && m.areEqual(this.packageName, verifyPurchaseTokenBody.packageName) && m.areEqual(this.subscriptionSkuId, verifyPurchaseTokenBody.subscriptionSkuId) && m.areEqual(this.oneTimePurchaseSkuId, verifyPurchaseTokenBody.oneTimePurchaseSkuId);
        }

        public final String getOneTimePurchaseSkuId() {
            return this.oneTimePurchaseSkuId;
        }

        public final String getPackageName() {
            return this.packageName;
        }

        public final String getPurchaseToken() {
            return this.purchaseToken;
        }

        public final String getSubscriptionSkuId() {
            return this.subscriptionSkuId;
        }

        public final long getUserId() {
            return this.userId;
        }

        public int hashCode() {
            String str = this.purchaseToken;
            int i = 0;
            int hashCode = str != null ? str.hashCode() : 0;
            long j = this.userId;
            int i2 = ((hashCode * 31) + ((int) (j ^ (j >>> 32)))) * 31;
            String str2 = this.packageName;
            int hashCode2 = (i2 + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.subscriptionSkuId;
            int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
            String str4 = this.oneTimePurchaseSkuId;
            if (str4 != null) {
                i = str4.hashCode();
            }
            return hashCode3 + i;
        }

        public String toString() {
            StringBuilder R = a.R("VerifyPurchaseTokenBody(purchaseToken=");
            R.append(this.purchaseToken);
            R.append(", userId=");
            R.append(this.userId);
            R.append(", packageName=");
            R.append(this.packageName);
            R.append(", subscriptionSkuId=");
            R.append(this.subscriptionSkuId);
            R.append(", oneTimePurchaseSkuId=");
            return a.H(R, this.oneTimePurchaseSkuId, ")");
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\t\u0018\u00002\u00020\u0001BM\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\b\u0012\b\u0010\t\u001a\u0004\u0018\u00010\b\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\f\u001a\u0004\u0018\u00010\b¢\u0006\u0004\b\u000f\u0010\u0010R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0018\u0010\u0006\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007R\u0018\u0010\t\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\t\u0010\nR\u0018\u0010\u000b\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\nR\u0018\u0010\f\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\nR\u0018\u0010\r\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u0007R\u0018\u0010\u000e\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u0007¨\u0006\u0011"}, d2 = {"Lcom/discord/restapi/RestAPIParams$VoiceChannel;", "", "", ModelAuditLogEntry.CHANGE_KEY_NSFW, "Ljava/lang/Boolean;", "", "userLimit", "Ljava/lang/Integer;", "", ModelAuditLogEntry.CHANGE_KEY_TOPIC, "Ljava/lang/String;", ModelAuditLogEntry.CHANGE_KEY_NAME, "rtcRegion", "type", ModelAuditLogEntry.CHANGE_KEY_BITRATE, HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class VoiceChannel {
        private final Integer bitrate;
        private final String name;
        private final Boolean nsfw;
        private final String rtcRegion;
        private final String topic;
        private final Integer type;
        private final Integer userLimit;

        public VoiceChannel(String str, String str2, Integer num, Boolean bool, Integer num2, Integer num3, String str3) {
            this.name = str;
            this.topic = str2;
            this.type = num;
            this.nsfw = bool;
            this.userLimit = num2;
            this.bitrate = num3;
            this.rtcRegion = str3;
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0017\u0012\u000e\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0002¢\u0006\u0004\b\u0006\u0010\u0007B\u0011\b\u0016\u0012\u0006\u0010\b\u001a\u00020\u0003¢\u0006\u0004\b\u0006\u0010\tR\u001c\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0004\u0010\u0005¨\u0006\n"}, d2 = {"Lcom/discord/restapi/RestAPIParams$CreateChannel;", "", "", "", "recipients", "Ljava/util/List;", HookHelper.constructorName, "(Ljava/util/List;)V", "recipientId", "(J)V", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class CreateChannel {
        private final List<Long> recipients;

        public CreateChannel(List<Long> list) {
            this.recipients = list == null ? n.emptyList() : list;
        }

        public CreateChannel(long j) {
            this(d0.t.m.listOf(Long.valueOf(j)));
        }
    }

    /* compiled from: RestAPIParams.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \u00152\u00020\u0001:\u0001\u0015B\u0091\u0001\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u0012\u0010\b\u0002\u0010\u0012\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0011¢\u0006\u0004\b\u0013\u0010\u0014R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0018\u0010\u0005\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0018\u0010\u0007\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0007\u0010\bR\u0018\u0010\t\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\t\u0010\u0004R\u0018\u0010\n\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u0004R\u0016\u0010\u000b\u001a\u00020\u00028\u0002@\u0002X\u0082D¢\u0006\u0006\n\u0004\b\u000b\u0010\u0004R\u0018\u0010\f\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\u0004R\u0018\u0010\r\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u0004R\u0018\u0010\u000e\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u0004R\u0018\u0010\u000f\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0004R\u0018\u0010\u0010\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0004R\u001e\u0010\u0012\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00118\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u0004¨\u0006\u0016"}, d2 = {"Lcom/discord/restapi/RestAPIParams$UserInfo;", "", "", "username", "Ljava/lang/String;", "emailToken", "", "flags", "Ljava/lang/Integer;", NotificationCompat.CATEGORY_EMAIL, "avatar", "pushProvider", "discriminator", "newPassword", "pushToken", "password", ModelAuditLogEntry.CHANGE_KEY_CODE, "Lcom/discord/primitives/UtcTimestamp;", "dateOfBirth", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V", "Companion", "restapi_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class UserInfo {
        public static final Companion Companion = new Companion(null);
        private final String avatar;
        private final String code;
        private final String dateOfBirth;
        private final String discriminator;
        private final String email;
        private final String emailToken;
        private final Integer flags;
        private final String newPassword;
        private final String password;
        private final String pushProvider;
        private final String pushToken;
        private final String username;

        /* compiled from: RestAPIParams.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tJ\u001f\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/restapi/RestAPIParams$UserInfo$Companion;", "", "", NotificationCompat.CATEGORY_EMAIL, "password", "Lcom/discord/restapi/RestAPIParams$UserInfo;", "createForEmail", "(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/discord/restapi/RestAPIParams$UserInfo;", HookHelper.constructorName, "()V", "restapi_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Companion {
            private Companion() {
            }

            public final UserInfo createForEmail(CharSequence charSequence, CharSequence charSequence2) {
                m.checkNotNullParameter(charSequence, NotificationCompat.CATEGORY_EMAIL);
                m.checkNotNullParameter(charSequence2, "password");
                return new UserInfo(null, charSequence.toString(), null, charSequence2.toString(), null, null, null, null, null, null, null, 2037, null);
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public UserInfo() {
            this(null, null, null, null, null, null, null, null, null, null, null, 2047, null);
        }

        public UserInfo(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, Integer num, String str10) {
            this.avatar = str;
            this.email = str2;
            this.emailToken = str3;
            this.password = str4;
            this.newPassword = str5;
            this.username = str6;
            this.pushToken = str7;
            this.code = str8;
            this.discriminator = str9;
            this.flags = num;
            this.dateOfBirth = str10;
            this.pushProvider = "gcm";
        }

        public static final UserInfo createForEmail(CharSequence charSequence, CharSequence charSequence2) {
            return Companion.createForEmail(charSequence, charSequence2);
        }

        public /* synthetic */ UserInfo(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, Integer num, String str10, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : str, (i & 2) != 0 ? null : str2, (i & 4) != 0 ? null : str3, (i & 8) != 0 ? null : str4, (i & 16) != 0 ? null : str5, (i & 32) != 0 ? null : str6, (i & 64) != 0 ? null : str7, (i & 128) != 0 ? null : str8, (i & 256) != 0 ? null : str9, (i & 512) != 0 ? null : num, (i & 1024) == 0 ? str10 : null);
        }
    }
}
