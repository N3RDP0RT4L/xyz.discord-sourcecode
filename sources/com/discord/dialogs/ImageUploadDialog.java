package com.discord.dialogs;

import andhook.lib.HookHelper;
import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.DimenRes;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import b.a.i.z;
import com.discord.app.AppDialog;
import com.discord.media_picker.MediaPicker;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.chat.AutocompleteSelectionTypes;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.functions.Action1;
import xyz.discord.R;
/* compiled from: ImageUploadDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u0000 72\u00020\u0001:\u000289B\u0007¢\u0006\u0004\b5\u00106J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006R\u001d\u0010\f\u001a\u00020\u00078B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR$\u0010\u0014\u001a\u0004\u0018\u00010\r8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R$\u0010\u001c\u001a\u0004\u0018\u00010\u00158\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR\"\u0010$\u001a\u00020\u001d8\u0006@\u0006X\u0086.¢\u0006\u0012\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010!\"\u0004\b\"\u0010#R*\u0010,\u001a\n\u0012\u0004\u0012\u00020\u0015\u0018\u00010%8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b&\u0010'\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R\"\u00104\u001a\u00020-8\u0006@\u0006X\u0086.¢\u0006\u0012\n\u0004\b.\u0010/\u001a\u0004\b0\u00101\"\u0004\b2\u00103¨\u0006:"}, d2 = {"Lcom/discord/dialogs/ImageUploadDialog;", "Lcom/discord/app/AppDialog;", "Landroid/view/View;", "view", "", "onViewBound", "(Landroid/view/View;)V", "Lb/a/i/z;", "l", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "g", "()Lb/a/i/z;", "binding", "Lcom/discord/dialogs/ImageUploadDialog$PreviewType;", "q", "Lcom/discord/dialogs/ImageUploadDialog$PreviewType;", "getPreviewType", "()Lcom/discord/dialogs/ImageUploadDialog$PreviewType;", "setPreviewType", "(Lcom/discord/dialogs/ImageUploadDialog$PreviewType;)V", "previewType", "", "o", "Ljava/lang/String;", "getMimeType", "()Ljava/lang/String;", "setMimeType", "(Ljava/lang/String;)V", "mimeType", "Landroid/net/Uri;", "m", "Landroid/net/Uri;", "getUri", "()Landroid/net/Uri;", "setUri", "(Landroid/net/Uri;)V", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "Lrx/functions/Action1;", "p", "Lrx/functions/Action1;", "getCropResultCallBack", "()Lrx/functions/Action1;", "setCropResultCallBack", "(Lrx/functions/Action1;)V", "cropResultCallBack", "Lcom/discord/media_picker/MediaPicker$Provider;", "n", "Lcom/discord/media_picker/MediaPicker$Provider;", "getProvider", "()Lcom/discord/media_picker/MediaPicker$Provider;", "setProvider", "(Lcom/discord/media_picker/MediaPicker$Provider;)V", "provider", HookHelper.constructorName, "()V", "k", "b", "PreviewType", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ImageUploadDialog extends AppDialog {
    public static final /* synthetic */ KProperty[] j = {b.d.b.a.a.b0(ImageUploadDialog.class, "binding", "getBinding()Lcom/discord/databinding/ImageUploadDialogBinding;", 0)};
    public static final b k = new b(null);
    public final FragmentViewBindingDelegate l = FragmentViewBindingDelegateKt.viewBinding$default(this, c.j, null, 2, null);
    public Uri m;
    public MediaPicker.Provider n;
    public String o;
    public Action1<String> p;
    public PreviewType q;

    /* compiled from: ImageUploadDialog.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\b\n\u0002\b\u000b\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0013\b\u0002\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\f¨\u0006\r"}, d2 = {"Lcom/discord/dialogs/ImageUploadDialog$PreviewType;", "", "", "previewSizeDimenId", "I", "getPreviewSizeDimenId", "()I", HookHelper.constructorName, "(Ljava/lang/String;II)V", AutocompleteSelectionTypes.EMOJI, "USER_AVATAR", "GUILD_AVATAR", "GUILD_SUBSCRIPTION_ROLE_AVATAR", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum PreviewType {
        EMOJI(R.dimen.emoji_size),
        USER_AVATAR(R.dimen.avatar_size_extra_large_account),
        GUILD_AVATAR(R.dimen.avatar_size_xxlarge),
        GUILD_SUBSCRIPTION_ROLE_AVATAR(R.dimen.avatar_size_huge);
        
        private final int previewSizeDimenId;

        PreviewType(@DimenRes int i) {
            this.previewSizeDimenId = i;
        }

        public final int getPreviewSizeDimenId() {
            return this.previewSizeDimenId;
        }
    }

    /* compiled from: java-style lambda group */
    /* loaded from: classes2.dex */
    public static final class a implements View.OnClickListener {
        public final /* synthetic */ int j;
        public final /* synthetic */ Object k;

        public a(int i, Object obj) {
            this.j = i;
            this.k = obj;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            int i = this.j;
            if (i == 0) {
                Context context = ((ImageUploadDialog) this.k).getContext();
                MediaPicker.Provider provider = ((ImageUploadDialog) this.k).n;
                if (provider == null) {
                    m.throwUninitializedPropertyAccessException("provider");
                }
                Uri uri = ((ImageUploadDialog) this.k).m;
                if (uri == null) {
                    m.throwUninitializedPropertyAccessException(NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
                }
                MGImages.requestImageCrop(context, provider, uri, (r13 & 8) != 0 ? 1.0f : 0.0f, (r13 & 16) != 0 ? 1.0f : 0.0f, (r13 & 32) != 0 ? 1024 : 0);
                ((ImageUploadDialog) this.k).dismiss();
            } else if (i == 1) {
                ((ImageUploadDialog) this.k).dismiss();
            } else {
                throw null;
            }
        }
    }

    /* compiled from: ImageUploadDialog.kt */
    /* loaded from: classes.dex */
    public static final class b {
        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: ImageUploadDialog.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class c extends k implements Function1<View, z> {
        public static final c j = new c();

        public c() {
            super(1, z.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/ImageUploadDialogBinding;", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public z invoke(View view) {
            View view2 = view;
            m.checkNotNullParameter(view2, "p1");
            int i = R.id.CANCEL_BUTTON;
            MaterialButton materialButton = (MaterialButton) view2.findViewById(R.id.CANCEL_BUTTON);
            if (materialButton != null) {
                i = R.id.notice_crop;
                TextView textView = (TextView) view2.findViewById(R.id.notice_crop);
                if (textView != null) {
                    i = R.id.notice_image;
                    SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view2.findViewById(R.id.notice_image);
                    if (simpleDraweeView != null) {
                        i = R.id.notice_upload;
                        MaterialButton materialButton2 = (MaterialButton) view2.findViewById(R.id.notice_upload);
                        if (materialButton2 != null) {
                            return new z((LinearLayout) view2, materialButton, textView, simpleDraweeView, materialButton2);
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view2.getResources().getResourceName(i)));
        }
    }

    /* compiled from: ImageUploadDialog.kt */
    /* loaded from: classes.dex */
    public static final class d implements View.OnClickListener {
        public final /* synthetic */ String k;

        public d(String str) {
            this.k = str;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            Context context = ImageUploadDialog.this.getContext();
            Uri uri = ImageUploadDialog.this.m;
            if (uri == null) {
                m.throwUninitializedPropertyAccessException(NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
            }
            MGImages.requestDataUrl(context, uri, this.k, ImageUploadDialog.this.p);
            ImageUploadDialog.this.dismiss();
        }
    }

    public ImageUploadDialog() {
        super(R.layout.image_upload_dialog);
    }

    public final z g() {
        return (z) this.l.getValue((Fragment) this, j[0]);
    }

    @Override // com.discord.app.AppDialog
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        PreviewType previewType = this.q;
        String str = this.o;
        if (previewType == null || str == null) {
            dismiss();
            return;
        }
        boolean areEqual = m.areEqual(str, "image/gif");
        g().e.setOnClickListener(new d(str));
        TextView textView = g().c;
        m.checkNotNullExpressionValue(textView, "binding.noticeCrop");
        textView.setVisibility(areEqual ? 4 : 0);
        if (!areEqual) {
            g().c.setOnClickListener(new a(0, this));
        }
        g().f233b.setOnClickListener(new a(1, this));
        int dimensionPixelSize = getResources().getDimensionPixelSize(previewType.getPreviewSizeDimenId());
        SimpleDraweeView simpleDraweeView = g().d;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.noticeImage");
        ViewGroup.LayoutParams layoutParams = simpleDraweeView.getLayoutParams();
        layoutParams.height = dimensionPixelSize;
        layoutParams.width = dimensionPixelSize;
        SimpleDraweeView simpleDraweeView2 = g().d;
        m.checkNotNullExpressionValue(simpleDraweeView2, "binding.noticeImage");
        simpleDraweeView2.setLayoutParams(layoutParams);
        int ordinal = previewType.ordinal();
        if (ordinal == 1 || ordinal == 2) {
            SimpleDraweeView simpleDraweeView3 = g().d;
            m.checkNotNullExpressionValue(simpleDraweeView3, "binding.noticeImage");
            MGImages.setRoundingParams(simpleDraweeView3, dimensionPixelSize, true, (r13 & 8) != 0 ? null : Integer.valueOf(ColorCompat.getThemedColor(view, (int) R.attr.primary_600)), (r13 & 16) != 0 ? null : null, (r13 & 32) != 0 ? null : null);
        }
        SimpleDraweeView simpleDraweeView4 = g().d;
        m.checkNotNullExpressionValue(simpleDraweeView4, "binding.noticeImage");
        Uri uri = this.m;
        if (uri == null) {
            m.throwUninitializedPropertyAccessException(NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        }
        MGImages.setImage$default(simpleDraweeView4, uri.toString(), dimensionPixelSize, dimensionPixelSize, false, null, null, 112, null);
    }
}
