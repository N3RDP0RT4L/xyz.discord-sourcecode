package com.discord.overlay;

import andhook.lib.HookHelper;
import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.view.View;
import b.a.n.f;
import com.discord.overlay.views.OverlayBubbleWrap;
import d0.j;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: OverlayService.kt */
/* loaded from: classes.dex */
public abstract class OverlayService extends Service {
    public static final Companion Companion = new Companion(null);
    public static final int NOTIFICATION_ID = 5858;
    public OverlayManager overlayManager;

    /* compiled from: OverlayService.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/overlay/OverlayService$Companion;", "", "", "NOTIFICATION_ID", "I", HookHelper.constructorName, "()V", "overlay_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        public Companion() {
        }

        public Companion(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: OverlayService.kt */
    /* loaded from: classes.dex */
    public static final class a implements Runnable {
        public final /* synthetic */ OverlayBubbleWrap k;

        public a(OverlayBubbleWrap overlayBubbleWrap) {
            this.k = overlayBubbleWrap;
        }

        @Override // java.lang.Runnable
        public final void run() {
            OverlayService.this.getOverlayManager().b(this.k);
        }
    }

    /* compiled from: OverlayService.kt */
    /* loaded from: classes.dex */
    public static final class b implements f {
        public b() {
        }

        @Override // b.a.n.f
        public void a(OverlayBubbleWrap overlayBubbleWrap) {
        }

        @Override // b.a.n.f
        public void b(OverlayBubbleWrap overlayBubbleWrap) {
            m.checkNotNullParameter(overlayBubbleWrap, "bubble");
            OverlayService.this.getOverlayManager().d(overlayBubbleWrap);
        }
    }

    /* compiled from: OverlayService.kt */
    /* loaded from: classes.dex */
    public static final class c extends o implements Function1<View, Unit> {
        public c() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(View view) {
            m.checkNotNullParameter(view, "it");
            if (!OverlayService.this.getOverlayManager().k.isEmpty()) {
                return Unit.a;
            }
            throw new j(b.d.b.a.a.v("An operation is not implemented: ", "handle stop service"));
        }
    }

    private final boolean attachBubbleToWindow(Intent intent) {
        OverlayBubbleWrap createOverlayBubble = createOverlayBubble(intent);
        if (createOverlayBubble == null) {
            return false;
        }
        OverlayManager overlayManager = this.overlayManager;
        if (overlayManager == null) {
            m.throwUninitializedPropertyAccessException("overlayManager");
        }
        overlayManager.a(createOverlayBubble);
        createOverlayBubble.post(new a(createOverlayBubble));
        OverlayManager overlayManager2 = this.overlayManager;
        if (overlayManager2 == null) {
            m.throwUninitializedPropertyAccessException("overlayManager");
        }
        if (overlayManager2.n != null) {
            return true;
        }
        Context applicationContext = getApplicationContext();
        m.checkNotNullExpressionValue(applicationContext, "applicationContext");
        b.a.n.h.a aVar = new b.a.n.h.a(applicationContext);
        OverlayManager overlayManager3 = this.overlayManager;
        if (overlayManager3 == null) {
            m.throwUninitializedPropertyAccessException("overlayManager");
        }
        Objects.requireNonNull(overlayManager3);
        m.checkNotNullParameter(aVar, "trashWrap");
        overlayManager3.n = aVar;
        overlayManager3.f2728s.addView(aVar, aVar.getWindowLayoutParams());
        return true;
    }

    public abstract Notification createNotification(Intent intent);

    public abstract OverlayBubbleWrap createOverlayBubble(Intent intent);

    public final OverlayManager getOverlayManager() {
        OverlayManager overlayManager = this.overlayManager;
        if (overlayManager == null) {
            m.throwUninitializedPropertyAccessException("overlayManager");
        }
        return overlayManager;
    }

    public final void handleStart(Intent intent) {
        if (intent == null) {
            intent = new Intent();
        }
        if (attachBubbleToWindow(intent)) {
            startForeground(NOTIFICATION_ID, createNotification(intent));
        }
    }

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override // android.app.Service
    public void onCreate() {
        super.onCreate();
        Context applicationContext = getApplicationContext();
        m.checkNotNullExpressionValue(applicationContext, "applicationContext");
        OverlayManager overlayManager = new OverlayManager(applicationContext, null, 2);
        this.overlayManager = overlayManager;
        overlayManager.o = new b();
        if (overlayManager == null) {
            m.throwUninitializedPropertyAccessException("overlayManager");
        }
        c cVar = new c();
        Objects.requireNonNull(overlayManager);
        m.checkNotNullParameter(cVar, "<set-?>");
        overlayManager.m = cVar;
    }

    @Override // android.app.Service
    public void onDestroy() {
        OverlayManager overlayManager = this.overlayManager;
        if (overlayManager == null) {
            m.throwUninitializedPropertyAccessException("overlayManager");
        }
        overlayManager.close();
        super.onDestroy();
    }

    public final void setOverlayManager(OverlayManager overlayManager) {
        m.checkNotNullParameter(overlayManager, "<set-?>");
        this.overlayManager = overlayManager;
    }
}
