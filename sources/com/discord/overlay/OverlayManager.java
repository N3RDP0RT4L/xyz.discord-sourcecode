package com.discord.overlay;

import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import b.a.n.b;
import b.a.n.f;
import b.a.n.h.a;
import com.discord.overlay.views.OverlayBubbleWrap;
import com.discord.utilities.display.DisplayUtils;
import d0.t.r;
import d0.z.d.m;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: OverlayManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0010 \n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0016\u0018\u00002\u00020\u0001J\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0015\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\u0006J\u0015\u0010\b\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\b\u0010\u0006J\u000f\u0010\t\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\t\u0010\nJ\u0019\u0010\u000b\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u000b\u0010\u0006R\u001f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R.\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00040\u00128\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R$\u0010\"\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001f\"\u0004\b \u0010!R\u001c\u0010(\u001a\u00020#8\u0004@\u0004X\u0084\u0004¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'R(\u0010/\u001a\u0004\u0018\u00010)2\b\u0010*\u001a\u0004\u0018\u00010)8\u0006@BX\u0086\u000e¢\u0006\f\n\u0004\b+\u0010,\u001a\u0004\b-\u0010.R\u0018\u00102\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b0\u00101R&\u00107\u001a\u0012\u0012\u0004\u0012\u00020\u000203j\b\u0012\u0004\u0012\u00020\u0002`48\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b5\u00106R.\u0010;\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00040\u00128\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b8\u0010\u0015\u001a\u0004\b9\u0010\u0017\"\u0004\b:\u0010\u0019R\u001c\u0010A\u001a\u00020<8\u0004@\u0004X\u0084\u0004¢\u0006\f\n\u0004\b=\u0010>\u001a\u0004\b?\u0010@R\u0016\u0010E\u001a\u00020B8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bC\u0010D¨\u0006F"}, d2 = {"Lcom/discord/overlay/OverlayManager;", "Ljava/io/Closeable;", "Lcom/discord/overlay/views/OverlayBubbleWrap;", "bubble", "", "d", "(Lcom/discord/overlay/views/OverlayBubbleWrap;)V", "a", "b", "close", "()V", "c", "", "k", "Ljava/util/List;", "getActiveBubbles", "()Ljava/util/List;", "activeBubbles", "Lkotlin/Function1;", "Landroid/view/View;", "m", "Lkotlin/jvm/functions/Function1;", "getOnOverlayBubbleRemoved", "()Lkotlin/jvm/functions/Function1;", "setOnOverlayBubbleRemoved", "(Lkotlin/jvm/functions/Function1;)V", "onOverlayBubbleRemoved", "Lb/a/n/f;", "o", "Lb/a/n/f;", "getTrashEventListener", "()Lb/a/n/f;", "setTrashEventListener", "(Lb/a/n/f;)V", "trashEventListener", "Landroid/content/Context;", "r", "Landroid/content/Context;", "getContext", "()Landroid/content/Context;", "context", "Lb/a/n/h/a;", "<set-?>", "n", "Lb/a/n/h/a;", "getTrashWrap", "()Lb/a/n/h/a;", "trashWrap", "q", "Lcom/discord/overlay/views/OverlayBubbleWrap;", "bubbleInTrashZone", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "j", "Ljava/util/ArrayList;", "overlaysOnDisplay", "l", "getOnOverlayBubbleAdded", "setOnOverlayBubbleAdded", "onOverlayBubbleAdded", "Landroid/view/WindowManager;", "s", "Landroid/view/WindowManager;", "getWindowManager", "()Landroid/view/WindowManager;", "windowManager", "Landroid/view/View$OnTouchListener;", "p", "Landroid/view/View$OnTouchListener;", "bubbleOnTouchListener", "overlay_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public class OverlayManager implements Closeable {
    public final ArrayList<OverlayBubbleWrap> j;
    public final List<OverlayBubbleWrap> k;
    public Function1<? super View, Unit> l;
    public Function1<? super View, Unit> m;
    public a n;
    public f o;
    public View.OnTouchListener p;
    public OverlayBubbleWrap q;
    public final Context r;

    /* renamed from: s  reason: collision with root package name */
    public final WindowManager f2728s;

    public OverlayManager(Context context, WindowManager windowManager, int i) {
        WindowManager windowManager2;
        if ((i & 2) != 0) {
            Object systemService = context.getSystemService("window");
            Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.view.WindowManager");
            windowManager2 = (WindowManager) systemService;
        } else {
            windowManager2 = null;
        }
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(windowManager2, "windowManager");
        this.r = context;
        this.f2728s = windowManager2;
        ArrayList<OverlayBubbleWrap> arrayList = new ArrayList<>(5);
        this.j = arrayList;
        this.k = arrayList;
        this.l = n.j;
        this.m = n.k;
        this.p = new b.a.n.a(this);
    }

    public final void a(OverlayBubbleWrap overlayBubbleWrap) {
        m.checkNotNullParameter(overlayBubbleWrap, "bubble");
        this.f2728s.addView(overlayBubbleWrap, overlayBubbleWrap.getWindowLayoutParams());
        this.j.add(overlayBubbleWrap);
        this.l.invoke(overlayBubbleWrap);
    }

    public final void b(OverlayBubbleWrap overlayBubbleWrap) {
        m.checkNotNullParameter(overlayBubbleWrap, "bubble");
        if (overlayBubbleWrap.getCenterX() > DisplayUtils.getScreenSize(this.r).centerX()) {
            OverlayBubbleWrap.c(overlayBubbleWrap, Integer.MAX_VALUE, (int) overlayBubbleWrap.getY(), null, 4, null);
        } else {
            OverlayBubbleWrap.c(overlayBubbleWrap, Integer.MIN_VALUE, (int) overlayBubbleWrap.getY(), null, 4, null);
        }
    }

    public final void c(OverlayBubbleWrap overlayBubbleWrap) {
        if (!m.areEqual(this.q, overlayBubbleWrap)) {
            this.q = overlayBubbleWrap;
            a aVar = this.n;
            if (aVar != null) {
                aVar.a(overlayBubbleWrap);
            }
            f fVar = this.o;
            if (fVar != null) {
                fVar.a(overlayBubbleWrap);
            }
        }
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        r.removeAll((List) this.j, (Function1) new b(this));
        a aVar = this.n;
        if (aVar != null) {
            this.f2728s.removeView(aVar);
        }
        this.n = null;
    }

    public final void d(OverlayBubbleWrap overlayBubbleWrap) {
        m.checkNotNullParameter(overlayBubbleWrap, "bubble");
        if (this.j.remove(overlayBubbleWrap)) {
            this.f2728s.removeViewImmediate(overlayBubbleWrap);
            this.m.invoke(overlayBubbleWrap);
        }
    }
}
