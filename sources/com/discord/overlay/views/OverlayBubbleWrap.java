package com.discord.overlay.views;

import andhook.lib.HookHelper;
import android.animation.AnimatorInflater;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import androidx.annotation.CallSuper;
import androidx.core.view.ViewCompat;
import androidx.dynamicanimation.animation.DynamicAnimation;
import androidx.dynamicanimation.animation.SpringAnimation;
import androidx.dynamicanimation.animation.SpringForce;
import com.discord.overlay.R;
import com.discord.utilities.display.DisplayUtils;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.s;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.properties.ReadWriteProperty;
import kotlin.reflect.KProperty;
/* compiled from: OverlayBubbleWrap.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0007\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u0015\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b \n\u0002\u0018\u0002\n\u0002\b\b\b\u0016\u0018\u00002\u00020\u0001B\u0011\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\bw\u0010xJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0004¢\u0006\u0004\b\u0005\u0010\u0006J\u0015\u0010\n\u001a\u00020\t2\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\n\u0010\u000bJ7\u0010\u0012\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u00072\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\r2\u0006\u0010\u0011\u001a\u00020\rH\u0014¢\u0006\u0004\b\u0012\u0010\u0013J\u0019\u0010\u0016\u001a\u00020\t2\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014H\u0015¢\u0006\u0004\b\u0016\u0010\u0017J\u0017\u0010\u001a\u001a\u00020\u00072\u0006\u0010\u0019\u001a\u00020\u0018H\u0016¢\u0006\u0004\b\u001a\u0010\u001bJ\u0017\u0010\u001c\u001a\u00020\u00072\u0006\u0010\u0019\u001a\u00020\u0018H\u0016¢\u0006\u0004\b\u001c\u0010\u001bJ\u000f\u0010\u001e\u001a\u00020\u001dH\u0016¢\u0006\u0004\b\u001e\u0010\u001fJ\u0017\u0010!\u001a\u00020\t2\u0006\u0010 \u001a\u00020\u001dH\u0016¢\u0006\u0004\b!\u0010\"J\u000f\u0010#\u001a\u00020\u001dH\u0016¢\u0006\u0004\b#\u0010\u001fJ\u0017\u0010%\u001a\u00020\t2\u0006\u0010$\u001a\u00020\u001dH\u0016¢\u0006\u0004\b%\u0010\"J\u0017\u0010(\u001a\u00020\t2\u0006\u0010'\u001a\u00020&H\u0004¢\u0006\u0004\b(\u0010)J\u0017\u0010,\u001a\u00020\t2\u0006\u0010+\u001a\u00020*H\u0016¢\u0006\u0004\b,\u0010-J\u0019\u0010/\u001a\u00020\t2\b\b\u0002\u0010.\u001a\u00020\u0007H\u0016¢\u0006\u0004\b/\u0010\u000bJ\u000f\u00100\u001a\u00020\tH\u0002¢\u0006\u0004\b0\u00101R\u0013\u00104\u001a\u00020\r8F@\u0006¢\u0006\u0006\u001a\u0004\b2\u00103R\u0019\u0010:\u001a\u0002058\u0006@\u0006¢\u0006\f\n\u0004\b6\u00107\u001a\u0004\b8\u00109R0\u0010B\u001a\u0010\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\t\u0018\u00010;8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b<\u0010=\u001a\u0004\b>\u0010?\"\u0004\b@\u0010AR\u0019\u0010F\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\bC\u0010D\u001a\u0004\bE\u00103R\u0016\u0010H\u001a\u00020\r8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bG\u0010DR\"\u0010O\u001a\u00020I8\u0004@\u0004X\u0084\u0004¢\u0006\u0012\n\u0004\bJ\u0010K\u0012\u0004\bN\u00101\u001a\u0004\bL\u0010MR\"\u0010W\u001a\u00020P8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\bQ\u0010R\u001a\u0004\bS\u0010T\"\u0004\bU\u0010VR\u0013\u0010Y\u001a\u00020\r8F@\u0006¢\u0006\u0006\u001a\u0004\bX\u00103R+\u0010`\u001a\u00020\u00072\u0006\u0010Z\u001a\u00020\u00078V@VX\u0096\u008e\u0002¢\u0006\u0012\n\u0004\b[\u0010\\\u001a\u0004\b]\u0010^\"\u0004\b_\u0010\u000bR\u0016\u0010c\u001a\u00020&8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\ba\u0010bR\u0016\u0010e\u001a\u00020\r8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bd\u0010DR\u0018\u0010g\u001a\u0004\u0018\u00010&8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bf\u0010bR\"\u0010k\u001a\u00020I8\u0004@\u0004X\u0084\u0004¢\u0006\u0012\n\u0004\bh\u0010K\u0012\u0004\bj\u00101\u001a\u0004\bi\u0010MR\u0019\u0010p\u001a\u00020\u00048\u0006@\u0006¢\u0006\f\n\u0004\bl\u0010m\u001a\u0004\bn\u0010oR\u001c\u0010v\u001a\u00020q8\u0004@\u0004X\u0084\u0004¢\u0006\f\n\u0004\br\u0010s\u001a\u0004\bt\u0010u¨\u0006y"}, d2 = {"Lcom/discord/overlay/views/OverlayBubbleWrap;", "Landroid/widget/FrameLayout;", "Landroid/content/Context;", "context", "Landroid/graphics/Rect;", "e", "(Landroid/content/Context;)Landroid/graphics/Rect;", "", "isTouchable", "", "setBubbleTouchable", "(Z)V", "changed", "", "left", "top", "right", "bottom", "onLayout", "(ZIIII)V", "Landroid/content/res/Configuration;", "newConfig", "onConfigurationChanged", "(Landroid/content/res/Configuration;)V", "Landroid/view/MotionEvent;", "motionEvent", "dispatchTouchEvent", "(Landroid/view/MotionEvent;)Z", "onInterceptTouchEvent", "", "getX", "()F", "x", "setX", "(F)V", "getY", "y", "setY", "Landroid/graphics/Point;", "newAnchorPoint", "setAnchorAt", "(Landroid/graphics/Point;)V", "Landroid/view/View;", "targetView", "b", "(Landroid/view/View;)V", "animate", "a", "d", "()V", "getCenterX", "()I", "centerX", "", "q", "[I", "getScreenOffset", "()[I", "screenOffset", "Lkotlin/Function1;", "p", "Lkotlin/jvm/functions/Function1;", "getOnMovingStateChanged", "()Lkotlin/jvm/functions/Function1;", "setOnMovingStateChanged", "(Lkotlin/jvm/functions/Function1;)V", "onMovingStateChanged", "n", "I", "getMoveThresholdPx", "moveThresholdPx", "r", "deltaX", "Landroidx/dynamicanimation/animation/SpringAnimation;", "u", "Landroidx/dynamicanimation/animation/SpringAnimation;", "getSpringAnimationY", "()Landroidx/dynamicanimation/animation/SpringAnimation;", "getSpringAnimationY$annotations", "springAnimationY", "Landroid/view/WindowManager$LayoutParams;", "l", "Landroid/view/WindowManager$LayoutParams;", "getWindowLayoutParams", "()Landroid/view/WindowManager$LayoutParams;", "setWindowLayoutParams", "(Landroid/view/WindowManager$LayoutParams;)V", "windowLayoutParams", "getCenterY", "centerY", "<set-?>", "o", "Lkotlin/properties/ReadWriteProperty;", "f", "()Z", "setMoving", "isMoving", "v", "Landroid/graphics/Point;", "actualPosition", "s", "deltaY", "w", "anchorPosition", "t", "getSpringAnimationX", "getSpringAnimationX$annotations", "springAnimationX", "m", "Landroid/graphics/Rect;", "getInsetMargins", "()Landroid/graphics/Rect;", "insetMargins", "Landroid/view/WindowManager;", "k", "Landroid/view/WindowManager;", "getWindowManager", "()Landroid/view/WindowManager;", "windowManager", HookHelper.constructorName, "(Landroid/content/Context;)V", "overlay_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public class OverlayBubbleWrap extends FrameLayout {
    public static final /* synthetic */ KProperty[] j = {a0.mutableProperty1(new s(OverlayBubbleWrap.class, "isMoving", "isMoving()Z", 0))};
    public final WindowManager k;
    public WindowManager.LayoutParams l;
    public final Rect m;
    public final int n;
    public final ReadWriteProperty o;
    public Function1<? super Boolean, Unit> p;
    public final int[] q;
    public int r;

    /* renamed from: s  reason: collision with root package name */
    public int f2729s;
    public final SpringAnimation t;
    public final SpringAnimation u;
    public Point v;
    public Point w;

    /* compiled from: Delegates.kt */
    /* loaded from: classes.dex */
    public static final class a extends d0.b0.a<Boolean> {
        public final /* synthetic */ OverlayBubbleWrap a;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(Object obj, Object obj2, OverlayBubbleWrap overlayBubbleWrap) {
            super(obj2);
            this.a = overlayBubbleWrap;
        }

        @Override // d0.b0.a
        public void afterChange(KProperty<?> kProperty, Boolean bool, Boolean bool2) {
            m.checkNotNullParameter(kProperty, "property");
            boolean booleanValue = bool2.booleanValue();
            if (bool.booleanValue() != booleanValue) {
                Function1<Boolean, Unit> onMovingStateChanged = this.a.getOnMovingStateChanged();
                if (onMovingStateChanged != null) {
                    onMovingStateChanged.invoke(Boolean.valueOf(booleanValue));
                }
                if (booleanValue) {
                    this.a.performHapticFeedback(1);
                    this.a.getSpringAnimationX().cancel();
                    this.a.getSpringAnimationY().cancel();
                }
                this.a.setPressed(booleanValue);
            }
        }
    }

    /* compiled from: View.kt */
    /* loaded from: classes.dex */
    public static final class b implements View.OnLayoutChangeListener {
        public b() {
        }

        @Override // android.view.View.OnLayoutChangeListener
        public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            m.checkNotNullParameter(view, "view");
            view.removeOnLayoutChangeListener(this);
            OverlayBubbleWrap overlayBubbleWrap = OverlayBubbleWrap.this;
            KProperty[] kPropertyArr = OverlayBubbleWrap.j;
            overlayBubbleWrap.d();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public OverlayBubbleWrap(Context context) {
        super(context);
        m.checkNotNullParameter(context, "context");
        Object systemService = getContext().getSystemService("window");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.view.WindowManager");
        this.k = (WindowManager) systemService;
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(-2, -2, Build.VERSION.SDK_INT <= 25 ? 2007 : 2038, 16777768, -3);
        layoutParams.gravity = 51;
        this.l = layoutParams;
        this.m = new Rect();
        this.n = getResources().getDimensionPixelOffset(R.b.movement_threshold_dp);
        setStateListAnimator(AnimatorInflater.loadStateListAnimator(getContext(), R.c.bubble_state_selector));
        Boolean bool = Boolean.FALSE;
        this.o = new a(bool, bool, this);
        this.q = new int[2];
        SpringAnimation springAnimation = new SpringAnimation(this, DynamicAnimation.X, 0.0f);
        springAnimation.setStartVelocity(50.0f);
        SpringForce spring = springAnimation.getSpring();
        m.checkNotNullExpressionValue(spring, "spring");
        spring.setStiffness(200.0f);
        SpringForce spring2 = springAnimation.getSpring();
        m.checkNotNullExpressionValue(spring2, "spring");
        spring2.setDampingRatio(0.75f);
        this.t = springAnimation;
        SpringAnimation springAnimation2 = new SpringAnimation(this, DynamicAnimation.Y, 0.0f);
        springAnimation2.setStartVelocity(50.0f);
        SpringForce spring3 = springAnimation2.getSpring();
        m.checkNotNullExpressionValue(spring3, "spring");
        spring3.setDampingRatio(0.75f);
        SpringForce spring4 = springAnimation2.getSpring();
        m.checkNotNullExpressionValue(spring4, "spring");
        spring4.setStiffness(200.0f);
        this.u = springAnimation2;
        WindowManager.LayoutParams layoutParams2 = this.l;
        this.v = new Point(layoutParams2.x, layoutParams2.y);
    }

    public static void c(OverlayBubbleWrap overlayBubbleWrap, int i, int i2, Rect rect, int i3, Object obj) {
        Rect rect2;
        if ((i3 & 4) != 0) {
            Context context = overlayBubbleWrap.getContext();
            m.checkNotNullExpressionValue(context, "context");
            rect2 = overlayBubbleWrap.e(context);
        } else {
            rect2 = null;
        }
        m.checkNotNullParameter(rect2, "screenBounds");
        int min = Math.min(Math.max(rect2.left, i), rect2.right - overlayBubbleWrap.getWidth());
        SpringAnimation springAnimation = overlayBubbleWrap.t;
        m.checkNotNullParameter(springAnimation, "$this$animateTo");
        springAnimation.cancel();
        springAnimation.setStartValue(overlayBubbleWrap.l.x);
        springAnimation.animateToFinalPosition(min);
        int min2 = Math.min(Math.max(rect2.top - overlayBubbleWrap.q[1], i2), rect2.bottom - overlayBubbleWrap.getHeight());
        SpringAnimation springAnimation2 = overlayBubbleWrap.u;
        m.checkNotNullParameter(springAnimation2, "$this$animateTo");
        springAnimation2.cancel();
        springAnimation2.setStartValue(overlayBubbleWrap.l.y);
        springAnimation2.animateToFinalPosition(min2);
    }

    public static /* synthetic */ void getSpringAnimationX$annotations() {
    }

    public static /* synthetic */ void getSpringAnimationY$annotations() {
    }

    public void a(boolean z2) {
        String simpleName = getClass().getSimpleName();
        StringBuilder R = b.d.b.a.a.R("Unanchoring[");
        R.append(this.w);
        R.append("] -> ");
        R.append(this.v);
        Log.d(simpleName, R.toString());
        this.w = null;
        this.t.cancel();
        this.u.cancel();
        if (z2) {
            Point point = this.v;
            c(this, point.x, point.y, null, 4, null);
            return;
        }
        WindowManager.LayoutParams layoutParams = this.l;
        Point point2 = this.v;
        layoutParams.x = point2.x;
        layoutParams.y = point2.y;
        this.k.updateViewLayout(this, layoutParams);
    }

    public void b(View view) {
        m.checkNotNullParameter(view, "targetView");
        m.checkNotNullParameter(view, "view");
        m.checkNotNullParameter(r1, "outLocation");
        view.getLocationOnScreen(r1);
        int[] iArr = {(view.getWidth() / 2) + iArr[0], (view.getHeight() / 2) + iArr[1]};
        int i = iArr[0];
        int[] iArr2 = this.q;
        Point point = new Point((i - iArr2[0]) - (getWidth() / 2), (iArr[1] - iArr2[1]) - (getHeight() / 2));
        setAnchorAt(point);
        c(this, point.x, point.y, null, 4, null);
        String simpleName = getClass().getSimpleName();
        StringBuilder R = b.d.b.a.a.R("Anchored[");
        R.append(this.v);
        R.append("] -> ");
        R.append(view);
        Log.d(simpleName, R.toString());
    }

    public final void d() {
        getLocationOnScreen(this.q);
        int[] iArr = this.q;
        int i = iArr[0];
        WindowManager.LayoutParams layoutParams = this.l;
        iArr[0] = i - layoutParams.x;
        iArr[1] = iArr[1] - layoutParams.y;
    }

    @Override // android.view.ViewGroup, android.view.View
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        m.checkNotNullParameter(motionEvent, "motionEvent");
        int action = motionEvent.getAction();
        if (action != 0) {
            boolean z2 = false;
            if (action != 1) {
                if (action == 2) {
                    if (!f()) {
                        if (Math.abs((this.r + this.l.x) - ((int) motionEvent.getRawX())) > this.n) {
                            z2 = true;
                        }
                        if (z2) {
                            setMoving(true);
                        }
                    } else {
                        this.v.x = ((int) motionEvent.getRawX()) - this.r;
                        this.v.y = ((int) motionEvent.getRawY()) - this.f2729s;
                        if (ViewCompat.isAttachedToWindow(this) && this.w == null) {
                            WindowManager.LayoutParams layoutParams = this.l;
                            Point point = this.v;
                            layoutParams.x = point.x;
                            layoutParams.y = point.y;
                            this.k.updateViewLayout(this, layoutParams);
                        }
                    }
                }
            } else if (f()) {
                setMoving(false);
            }
        } else {
            this.r = ((int) motionEvent.getRawX()) - this.l.x;
            this.f2729s = ((int) motionEvent.getRawY()) - this.l.y;
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public final Rect e(Context context) {
        m.checkNotNullParameter(context, "context");
        Rect screenSize = DisplayUtils.getScreenSize(context);
        int i = screenSize.left;
        Rect rect = this.m;
        screenSize.left = i + rect.left;
        screenSize.right -= rect.right;
        screenSize.top += rect.top;
        screenSize.bottom -= rect.bottom;
        return screenSize;
    }

    public boolean f() {
        return ((Boolean) this.o.getValue(this, j[0])).booleanValue();
    }

    public final int getCenterX() {
        return (getWidth() / 2) + this.l.x;
    }

    public final int getCenterY() {
        return (getHeight() / 2) + this.l.y;
    }

    public final Rect getInsetMargins() {
        return this.m;
    }

    public final int getMoveThresholdPx() {
        return this.n;
    }

    public final Function1<Boolean, Unit> getOnMovingStateChanged() {
        return this.p;
    }

    public final int[] getScreenOffset() {
        return this.q;
    }

    public final SpringAnimation getSpringAnimationX() {
        return this.t;
    }

    public final SpringAnimation getSpringAnimationY() {
        return this.u;
    }

    public final WindowManager.LayoutParams getWindowLayoutParams() {
        return this.l;
    }

    public final WindowManager getWindowManager() {
        return this.k;
    }

    @Override // android.view.View
    public float getX() {
        return this.l.x;
    }

    @Override // android.view.View
    public float getY() {
        return this.l.y;
    }

    @Override // android.view.View
    @CallSuper
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.k.updateViewLayout(this, this.l);
        if (!ViewCompat.isLaidOut(this) || isLayoutRequested()) {
            addOnLayoutChangeListener(new b());
        } else {
            d();
        }
    }

    @Override // android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        m.checkNotNullParameter(motionEvent, "motionEvent");
        if (motionEvent.getAction() != 2) {
            return super.onInterceptTouchEvent(motionEvent);
        }
        return f();
    }

    @Override // android.widget.FrameLayout, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
        super.onLayout(z2, i, i2, i3, i4);
        if (z2) {
            d();
        }
    }

    public final void setAnchorAt(Point point) {
        m.checkNotNullParameter(point, "newAnchorPoint");
        this.v.x = (int) getX();
        this.v.y = (int) getY();
        this.w = point;
        String simpleName = getClass().getSimpleName();
        StringBuilder R = b.d.b.a.a.R("Anchoring[");
        R.append(this.v);
        R.append("] -> ");
        R.append(this.w);
        Log.d(simpleName, R.toString());
    }

    public final void setBubbleTouchable(boolean z2) {
        WindowManager.LayoutParams layoutParams = this.l;
        m.checkNotNullParameter(layoutParams, "$this$setFlagTouchable");
        if (z2) {
            m.checkNotNullParameter(layoutParams, "$this$removeFlag");
            layoutParams.flags &= -17;
        } else {
            m.checkNotNullParameter(layoutParams, "$this$addFlag");
            layoutParams.flags = 16 | layoutParams.flags;
        }
        this.k.updateViewLayout(this, this.l);
    }

    public void setMoving(boolean z2) {
        this.o.setValue(this, j[0], Boolean.valueOf(z2));
    }

    public final void setOnMovingStateChanged(Function1<? super Boolean, Unit> function1) {
        this.p = function1;
    }

    public final void setWindowLayoutParams(WindowManager.LayoutParams layoutParams) {
        m.checkNotNullParameter(layoutParams, "<set-?>");
        this.l = layoutParams;
    }

    @Override // android.view.View
    public void setX(float f) {
        this.l.x = (int) f;
        if (ViewCompat.isAttachedToWindow(this)) {
            this.k.updateViewLayout(this, this.l);
        }
    }

    @Override // android.view.View
    public void setY(float f) {
        this.l.y = (int) f;
        if (ViewCompat.isAttachedToWindow(this)) {
            this.k.updateViewLayout(this, this.l);
        }
    }
}
