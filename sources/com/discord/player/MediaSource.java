package com.discord.player;

import andhook.lib.HookHelper;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: MediaSource.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\n\b\u0086\b\u0018\u0000 &2\u00020\u0001:\u0001'B\u001f\u0012\u0006\u0010\u001d\u001a\u00020\u0018\u0012\u0006\u0010\u0017\u001a\u00020\u000b\u0012\u0006\u0010#\u001a\u00020\u001e¢\u0006\u0004\b$\u0010%J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\t\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u0004HÖ\u0001¢\u0006\u0004\b\u000e\u0010\nJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u0019\u0010\u0017\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\rR\u0019\u0010\u001d\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001cR\u0019\u0010#\u001a\u00020\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"¨\u0006("}, d2 = {"Lcom/discord/player/MediaSource;", "Landroid/os/Parcelable;", "Landroid/os/Parcel;", "parcel", "", "flags", "", "writeToParcel", "(Landroid/os/Parcel;I)V", "describeContents", "()I", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "k", "Ljava/lang/String;", "getFeatureTag", "featureTag", "Landroid/net/Uri;", "j", "Landroid/net/Uri;", "getProgressiveMediaUri", "()Landroid/net/Uri;", "progressiveMediaUri", "Lcom/discord/player/MediaType;", "l", "Lcom/discord/player/MediaType;", "getMediaType", "()Lcom/discord/player/MediaType;", "mediaType", HookHelper.constructorName, "(Landroid/net/Uri;Ljava/lang/String;Lcom/discord/player/MediaType;)V", "CREATOR", "a", "app_media_player_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class MediaSource implements Parcelable {
    public static final a CREATOR = new a(null);
    public final Uri j;
    public final String k;
    public final MediaType l;

    /* compiled from: MediaSource.kt */
    /* loaded from: classes.dex */
    public static final class a implements Parcelable.Creator<MediaSource> {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        @Override // android.os.Parcelable.Creator
        public MediaSource createFromParcel(Parcel parcel) {
            m.checkNotNullParameter(parcel, "parcel");
            m.checkNotNullParameter(parcel, "parcel");
            Parcelable readParcelable = parcel.readParcelable(Uri.class.getClassLoader());
            m.checkNotNull(readParcelable);
            String readString = parcel.readString();
            m.checkNotNull(readString);
            m.checkNotNullExpressionValue(readString, "parcel.readString()!!");
            Parcelable readParcelable2 = parcel.readParcelable(MediaType.class.getClassLoader());
            m.checkNotNull(readParcelable2);
            return new MediaSource((Uri) readParcelable, readString, (MediaType) readParcelable2);
        }

        @Override // android.os.Parcelable.Creator
        public MediaSource[] newArray(int i) {
            return new MediaSource[i];
        }
    }

    public MediaSource(Uri uri, String str, MediaType mediaType) {
        m.checkNotNullParameter(uri, "progressiveMediaUri");
        m.checkNotNullParameter(str, "featureTag");
        m.checkNotNullParameter(mediaType, "mediaType");
        this.j = uri;
        this.k = str;
        this.l = mediaType;
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MediaSource)) {
            return false;
        }
        MediaSource mediaSource = (MediaSource) obj;
        return m.areEqual(this.j, mediaSource.j) && m.areEqual(this.k, mediaSource.k) && m.areEqual(this.l, mediaSource.l);
    }

    public int hashCode() {
        Uri uri = this.j;
        int i = 0;
        int hashCode = (uri != null ? uri.hashCode() : 0) * 31;
        String str = this.k;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        MediaType mediaType = this.l;
        if (mediaType != null) {
            i = mediaType.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("MediaSource(progressiveMediaUri=");
        R.append(this.j);
        R.append(", featureTag=");
        R.append(this.k);
        R.append(", mediaType=");
        R.append(this.l);
        R.append(")");
        return R.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        m.checkNotNullParameter(parcel, "parcel");
        parcel.writeParcelable(this.j, i);
        parcel.writeString(this.k);
        parcel.writeParcelable(this.l, i);
    }
}
