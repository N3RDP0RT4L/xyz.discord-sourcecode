package com.discord.player;

import andhook.lib.HookHelper;
import android.media.AudioTrack;
import android.net.Uri;
import android.view.Surface;
import androidx.annotation.MainThread;
import b.a.p.a;
import b.a.p.b;
import b.a.p.c;
import b.a.p.d;
import b.a.p.e;
import b.a.p.f;
import b.a.p.k;
import b.i.a.c.a3.a0;
import b.i.a.c.a3.f0;
import b.i.a.c.a3.j;
import b.i.a.c.a3.k0;
import b.i.a.c.c2;
import b.i.a.c.e1;
import b.i.a.c.e3.l;
import b.i.a.c.e3.s;
import b.i.a.c.f1;
import b.i.a.c.f3.b0;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.o;
import b.i.a.c.f3.p;
import b.i.a.c.f3.q;
import b.i.a.c.h1;
import b.i.a.c.k2;
import b.i.a.c.m2;
import b.i.a.c.o1;
import b.i.a.c.p1;
import b.i.a.c.q2;
import b.i.a.c.r2;
import b.i.a.c.s2.g1;
import b.i.a.c.s2.h1;
import b.i.a.c.t0;
import b.i.a.c.u0;
import b.i.a.c.u1;
import b.i.a.c.w0;
import b.i.a.c.w1;
import b.i.a.c.w2.r;
import b.i.a.c.w2.u;
import b.i.a.c.y1;
import b.i.b.b.h0;
import b.i.b.b.p;
import com.discord.utilities.logging.Logger;
import com.google.android.exoplayer2.IllegalSeekPositionException;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Scheduler;
import rx.Subscription;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
import rx.subscriptions.CompositeSubscription;
/* compiled from: AppMediaPlayer.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0084\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001<B/\u0012\u0006\u00109\u001a\u000206\u0012\u0006\u0010.\u001a\u00020+\u0012\u0006\u00105\u001a\u000202\u0012\u0006\u0010#\u001a\u00020 \u0012\u0006\u0010*\u001a\u00020'¢\u0006\u0004\b:\u0010;JI\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0003\u001a\u00020\u00022\b\b\u0002\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0006\u001a\u00020\u00042\b\b\u0002\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u000bH\u0007¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0012\u001a\u00020\r2\u0006\u0010\u0011\u001a\u00020\u0010H\u0007¢\u0006\u0004\b\u0012\u0010\u0013J\u000f\u0010\u0014\u001a\u00020\rH\u0007¢\u0006\u0004\b\u0014\u0010\u0015R\u0018\u0010\u0019\u001a\u0004\u0018\u00010\u00168\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001a\u0010\u001bR\u001c\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u001d0\u001c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u001eR\u0016\u0010#\u001a\u00020 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"R\u0016\u0010&\u001a\u00020$8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010%R\u0016\u0010*\u001a\u00020'8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u0010)R\u0016\u0010.\u001a\u00020+8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b,\u0010-R\u001c\u00101\u001a\b\u0012\u0004\u0012\u00020\u00100/8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0012\u00100R\u0016\u00105\u001a\u0002028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b3\u00104R\u0016\u00109\u001a\u0002068\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b7\u00108¨\u0006="}, d2 = {"Lcom/discord/player/AppMediaPlayer;", "", "Lcom/discord/player/MediaSource;", "mediaSource", "", "autoPlayWhenReady", "loopMedia", "", "initialSeekPositionMs", "Lcom/google/android/exoplayer2/ui/PlayerView;", "playerView", "Lcom/google/android/exoplayer2/ui/PlayerControlView;", "playerControlView", "", "a", "(Lcom/discord/player/MediaSource;ZZJLcom/google/android/exoplayer2/ui/PlayerView;Lcom/google/android/exoplayer2/ui/PlayerControlView;)V", "", "volume", "d", "(F)V", "c", "()V", "Lrx/Subscription;", "b", "Lrx/Subscription;", "positionPollingSubscription", "e", "Lcom/discord/player/MediaSource;", "Lrx/subjects/PublishSubject;", "Lcom/discord/player/AppMediaPlayer$Event;", "Lrx/subjects/PublishSubject;", "eventSubject", "Lrx/Scheduler;", "i", "Lrx/Scheduler;", "timerScheduler", "Lrx/subscriptions/CompositeSubscription;", "Lrx/subscriptions/CompositeSubscription;", "compositeSubscription", "Lcom/discord/utilities/logging/Logger;", "j", "Lcom/discord/utilities/logging/Logger;", "logger", "Lb/a/p/k;", "g", "Lb/a/p/k;", "rxPlayerEventListener", "Lrx/subjects/BehaviorSubject;", "Lrx/subjects/BehaviorSubject;", "volumeSubject", "Lb/i/a/c/e3/l$a;", "h", "Lb/i/a/c/e3/l$a;", "dataSourceFactory", "Lb/i/a/c/e1;", "f", "Lb/i/a/c/e1;", "exoPlayer", HookHelper.constructorName, "(Lb/i/a/c/e1;Lb/a/p/k;Lb/i/a/c/e3/l$a;Lrx/Scheduler;Lcom/discord/utilities/logging/Logger;)V", "Event", "app_media_player_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AppMediaPlayer {
    public final PublishSubject<Event> a;

    /* renamed from: b  reason: collision with root package name */
    public Subscription f2731b;
    public final CompositeSubscription c;
    public final BehaviorSubject<Float> d;
    public MediaSource e;
    public final e1 f;
    public final k g;
    public final l.a h;
    public final Scheduler i;
    public final Logger j;

    /* compiled from: AppMediaPlayer.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0006\u0004\u0005\u0006\u0007\b\tB\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0006\n\u000b\f\r\u000e\u000f¨\u0006\u0010"}, d2 = {"Lcom/discord/player/AppMediaPlayer$Event;", "", HookHelper.constructorName, "()V", "a", "b", "c", "d", "e", "f", "Lcom/discord/player/AppMediaPlayer$Event$b;", "Lcom/discord/player/AppMediaPlayer$Event$a;", "Lcom/discord/player/AppMediaPlayer$Event$c;", "Lcom/discord/player/AppMediaPlayer$Event$d;", "Lcom/discord/player/AppMediaPlayer$Event$f;", "Lcom/discord/player/AppMediaPlayer$Event$e;", "app_media_player_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class Event {

        /* compiled from: AppMediaPlayer.kt */
        /* loaded from: classes.dex */
        public static final class a extends Event {
            public static final a a = new a();

            public a() {
                super(null);
            }
        }

        /* compiled from: AppMediaPlayer.kt */
        /* loaded from: classes.dex */
        public static final class b extends Event {
            public static final b a = new b();

            public b() {
                super(null);
            }
        }

        /* compiled from: AppMediaPlayer.kt */
        /* loaded from: classes.dex */
        public static final class c extends Event {
            public final long a;

            public c(long j) {
                super(null);
                this.a = j;
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof c) && this.a == ((c) obj).a;
                }
                return true;
            }

            public int hashCode() {
                return a0.a.a.b.a(this.a);
            }

            public String toString() {
                return b.d.b.a.a.B(b.d.b.a.a.R("CurrentPositionUpdate(positionMs="), this.a, ")");
            }
        }

        /* compiled from: AppMediaPlayer.kt */
        /* loaded from: classes.dex */
        public static final class d extends Event {
            public static final d a = new d();

            public d() {
                super(null);
            }
        }

        /* compiled from: AppMediaPlayer.kt */
        /* loaded from: classes.dex */
        public static final class e extends Event {
            public static final e a = new e();

            public e() {
                super(null);
            }
        }

        /* compiled from: AppMediaPlayer.kt */
        /* loaded from: classes.dex */
        public static final class f extends Event {
            public static final f a = new f();

            public f() {
                super(null);
            }
        }

        public Event() {
        }

        public Event(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    public AppMediaPlayer(e1 e1Var, k kVar, l.a aVar, Scheduler scheduler, Logger logger) {
        m.checkNotNullParameter(e1Var, "exoPlayer");
        m.checkNotNullParameter(kVar, "rxPlayerEventListener");
        m.checkNotNullParameter(aVar, "dataSourceFactory");
        m.checkNotNullParameter(scheduler, "timerScheduler");
        m.checkNotNullParameter(logger, "logger");
        this.f = e1Var;
        this.g = kVar;
        this.h = aVar;
        this.i = scheduler;
        this.j = logger;
        PublishSubject<Event> k0 = PublishSubject.k0();
        m.checkNotNullExpressionValue(k0, "PublishSubject.create()");
        this.a = k0;
        CompositeSubscription compositeSubscription = new CompositeSubscription();
        this.c = compositeSubscription;
        BehaviorSubject<Float> l0 = BehaviorSubject.l0(Float.valueOf(((k2) e1Var).B));
        m.checkNotNullExpressionValue(l0, "BehaviorSubject.create(exoPlayer.volume)");
        this.d = l0;
        PublishSubject<k.c> publishSubject = kVar.j;
        m.checkNotNullExpressionValue(publishSubject, "playerStateChangeSubject");
        compositeSubscription.a(publishSubject.J().W(new e(this), new f(this)));
        PublishSubject<k.a> publishSubject2 = kVar.k;
        m.checkNotNullExpressionValue(publishSubject2, "isPlayingChangeSubject");
        compositeSubscription.a(publishSubject2.K().W(new a(this), new b(this)));
        MediaSource mediaSource = this.e;
        String str = (mediaSource == null || (str = mediaSource.k) == null) ? "" : str;
        PublishSubject<k.b> publishSubject3 = kVar.l;
        m.checkNotNullExpressionValue(publishSubject3, "playerErrorSubject");
        compositeSubscription.a(publishSubject3.J().W(new c(this, str), new d(this)));
    }

    public static /* synthetic */ void b(AppMediaPlayer appMediaPlayer, MediaSource mediaSource, boolean z2, boolean z3, long j, PlayerView playerView, PlayerControlView playerControlView, int i) {
        int i2 = i & 32;
        appMediaPlayer.a(mediaSource, (i & 2) != 0 ? false : z2, (i & 4) != 0 ? false : z3, (i & 8) != 0 ? 0L : j, playerView, null);
    }

    @MainThread
    public final void a(MediaSource mediaSource, boolean z2, boolean z3, long j, PlayerView playerView, PlayerControlView playerControlView) {
        o1.i iVar;
        u uVar;
        m.checkNotNullParameter(mediaSource, "mediaSource");
        m.checkNotNullParameter(playerView, "playerView");
        this.e = mediaSource;
        k2 k2Var = (k2) this.f;
        k2Var.n0();
        k2Var.w = 1;
        int i = 4;
        k2Var.j0(2, 4, 1);
        playerView.setPlayer(this.f);
        if (playerControlView != null) {
            playerControlView.setPlayer(this.f);
        }
        l.a aVar = this.h;
        j jVar = new j(new b.i.a.c.x2.f());
        r rVar = new r();
        s sVar = new s();
        Uri uri = mediaSource.j;
        w0.a<o1> aVar2 = o1.j;
        o1.d.a aVar3 = new o1.d.a();
        o1.f.a aVar4 = new o1.f.a(null);
        List emptyList = Collections.emptyList();
        p<Object> pVar = h0.l;
        o1.g.a aVar5 = new o1.g.a();
        b.c.a.a0.d.D(aVar4.f1033b == null || aVar4.a != null);
        if (uri != null) {
            iVar = new o1.i(uri, null, aVar4.a != null ? new o1.f(aVar4, null) : null, null, emptyList, null, pVar, null, null);
        } else {
            iVar = null;
        }
        o1 o1Var = new o1("", aVar3.a(), iVar, new o1.g(aVar5, null), p1.j, null);
        Objects.requireNonNull(iVar);
        o1.f fVar = iVar.c;
        if (fVar == null || e0.a < 18) {
            uVar = u.a;
        } else {
            synchronized (rVar.a) {
                if (!e0.a(fVar, rVar.f1151b)) {
                    rVar.f1151b = fVar;
                    rVar.c = rVar.a(fVar);
                }
                uVar = rVar.c;
                Objects.requireNonNull(uVar);
            }
        }
        f0 f0Var = new f0(o1Var, aVar, jVar, uVar, sVar, 1048576, null);
        m.checkNotNullExpressionValue(f0Var, "ProgressiveMediaSource.F…rce.progressiveMediaUri))");
        k2 k2Var2 = (k2) this.f;
        k2Var2.n0();
        f1 f1Var = k2Var2.e;
        Objects.requireNonNull(f1Var);
        List singletonList = Collections.singletonList(f0Var);
        f1Var.h0();
        f1Var.T();
        f1Var.f956x++;
        if (!f1Var.m.isEmpty()) {
            f1Var.p0(0, f1Var.m.size());
        }
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < singletonList.size(); i2++) {
            u1.c cVar = new u1.c((a0) singletonList.get(i2), f1Var.n);
            arrayList.add(cVar);
            f1Var.m.add(i2 + 0, new f1.a(cVar.f1135b, cVar.a.n));
        }
        k0 f = f1Var.B.f(0, arrayList.size());
        f1Var.B = f;
        c2 c2Var = new c2(f1Var.m, f);
        if (c2Var.q() || -1 < c2Var.n) {
            int a = c2Var.a(f1Var.w);
            w1 l0 = f1Var.l0(f1Var.F, c2Var, f1Var.i0(c2Var, a, -9223372036854775807L));
            int i3 = l0.f;
            if (a == -1 || i3 == 1) {
                i = i3;
            } else if (!c2Var.q() && a < c2Var.n) {
                i = 2;
            }
            w1 f2 = l0.f(i);
            ((b0.b) f1Var.i.q.i(17, new h1.a(arrayList, f1Var.B, a, e0.B(-9223372036854775807L), null))).b();
            f1Var.s0(f2, 0, 1, false, !f1Var.F.c.a.equals(f2.c.a) && !f1Var.F.f1142b.q(), 4, f1Var.g0(f2), -1);
            ((k2) this.f).a();
            if (z2) {
                ((k2) this.f).u(true);
            }
            if (j > 0) {
                u0 u0Var = (u0) this.f;
                u0Var.h(u0Var.C(), j);
            }
            k2 k2Var3 = (k2) this.f;
            k2Var3.n0();
            k2Var3.e.E(z3 ? 1 : 0);
            return;
        }
        throw new IllegalSeekPositionException(c2Var, -1, -9223372036854775807L);
    }

    @MainThread
    public final void c() {
        AudioTrack audioTrack;
        ((k2) this.f).u(false);
        Subscription subscription = this.f2731b;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        this.c.unsubscribe();
        k2 k2Var = (k2) this.f;
        k2Var.n0();
        if (e0.a < 21 && (audioTrack = k2Var.p) != null) {
            audioTrack.release();
            k2Var.p = null;
        }
        k2Var.j.a(false);
        m2 m2Var = k2Var.l;
        m2.c cVar = m2Var.e;
        if (cVar != null) {
            try {
                m2Var.a.unregisterReceiver(cVar);
            } catch (RuntimeException e) {
                q.c("StreamVolumeManager", "Error unregistering stream volume receiver", e);
            }
            m2Var.e = null;
        }
        q2 q2Var = k2Var.m;
        q2Var.d = false;
        q2Var.a();
        r2 r2Var = k2Var.n;
        r2Var.d = false;
        r2Var.a();
        t0 t0Var = k2Var.k;
        t0Var.c = null;
        t0Var.a();
        k2Var.e.n0();
        final g1 g1Var = k2Var.i;
        o oVar = g1Var.q;
        b.c.a.a0.d.H(oVar);
        oVar.b(new Runnable() { // from class: b.i.a.c.s2.p
            @Override // java.lang.Runnable
            public final void run() {
                g1 g1Var2 = g1.this;
                final h1.a k0 = g1Var2.k0();
                p.a<h1> x0Var = new p.a() { // from class: b.i.a.c.s2.x0
                    @Override // b.i.a.c.f3.p.a
                    public final void invoke(Object obj) {
                        ((h1) obj).E();
                    }
                };
                g1Var2.n.put(1036, k0);
                b.i.a.c.f3.p<h1> pVar = g1Var2.o;
                pVar.b(1036, x0Var);
                pVar.a();
                g1Var2.o.c();
            }
        });
        k2Var.i0();
        Surface surface = k2Var.r;
        if (surface != null) {
            surface.release();
            k2Var.r = null;
        }
        if (!k2Var.G) {
            k2Var.D = Collections.emptyList();
        } else {
            Objects.requireNonNull(null);
            throw null;
        }
    }

    @MainThread
    public final void d(float f) {
        k2 k2Var = (k2) this.f;
        k2Var.n0();
        float g = e0.g(f, 0.0f, 1.0f);
        if (k2Var.B != g) {
            k2Var.B = g;
            k2Var.j0(1, 2, Float.valueOf(k2Var.k.g * g));
            k2Var.i.w(g);
            Iterator<y1.e> it = k2Var.h.iterator();
            while (it.hasNext()) {
                it.next().w(g);
            }
        }
        this.d.onNext(Float.valueOf(f));
    }
}
