package com.discord.p000native.engine;

import andhook.lib.HookHelper;
import kotlin.Metadata;
/* compiled from: VideoInputDeviceFacing.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/native/engine/VideoInputDeviceFacing;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "Unknown", "Front", "Back", "android_release"}, k = 1, mv = {1, 4, 2})
/* renamed from: com.discord.native.engine.VideoInputDeviceFacing  reason: invalid package */
/* loaded from: classes.dex */
public enum VideoInputDeviceFacing {
    Unknown,
    Front,
    Back
}
