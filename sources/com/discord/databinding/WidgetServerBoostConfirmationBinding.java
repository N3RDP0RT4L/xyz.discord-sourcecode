package com.discord.databinding;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewbinding.ViewBinding;
import com.discord.utilities.dimmer.DimmerView;
import com.discord.views.guildboost.GuildBoostConfirmationView;
import com.google.android.material.button.MaterialButton;
/* loaded from: classes.dex */
public final class WidgetServerBoostConfirmationBinding implements ViewBinding {
    @NonNull
    public final CoordinatorLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final GuildBoostConfirmationView f2510b;
    @NonNull
    public final TextView c;
    @NonNull
    public final TextView d;
    @NonNull
    public final MaterialButton e;
    @NonNull
    public final DimmerView f;

    public WidgetServerBoostConfirmationBinding(@NonNull CoordinatorLayout coordinatorLayout, @NonNull LinearLayout linearLayout, @NonNull GuildBoostConfirmationView guildBoostConfirmationView, @NonNull TextView textView, @NonNull TextView textView2, @NonNull MaterialButton materialButton, @NonNull DimmerView dimmerView) {
        this.a = coordinatorLayout;
        this.f2510b = guildBoostConfirmationView;
        this.c = textView;
        this.d = textView2;
        this.e = materialButton;
        this.f = dimmerView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
