package com.discord.databinding;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.viewbinding.ViewBinding;
import com.discord.views.calls.VideoCallParticipantView;
/* loaded from: classes.dex */
public final class WidgetStageChannelSpeakerMediaBinding implements ViewBinding {
    @NonNull
    public final CardView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final VideoCallParticipantView f2629b;

    public WidgetStageChannelSpeakerMediaBinding(@NonNull CardView cardView, @NonNull VideoCallParticipantView videoCallParticipantView) {
        this.a = cardView;
        this.f2629b = videoCallParticipantView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
