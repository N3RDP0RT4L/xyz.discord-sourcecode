package com.discord.databinding;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewbinding.ViewBinding;
import b.a.i.e0;
import b.a.i.f0;
import b.a.i.g0;
import com.discord.app.AppViewFlipper;
/* loaded from: classes.dex */
public final class WidgetAgeVerifyBinding implements ViewBinding {
    @NonNull
    public final CoordinatorLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final AppViewFlipper f2207b;
    @NonNull
    public final f0 c;
    @NonNull
    public final g0 d;
    @NonNull
    public final e0 e;

    public WidgetAgeVerifyBinding(@NonNull CoordinatorLayout coordinatorLayout, @NonNull AppViewFlipper appViewFlipper, @NonNull f0 f0Var, @NonNull g0 g0Var, @NonNull e0 e0Var) {
        this.a = coordinatorLayout;
        this.f2207b = appViewFlipper;
        this.c = f0Var;
        this.d = g0Var;
        this.e = e0Var;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
