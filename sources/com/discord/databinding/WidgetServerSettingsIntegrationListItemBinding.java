package com.discord.databinding;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.views.CheckedSetting;
/* loaded from: classes.dex */
public final class WidgetServerSettingsIntegrationListItemBinding implements ViewBinding {
    @NonNull
    public final FrameLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final RelativeLayout f2550b;
    @NonNull
    public final View c;
    @NonNull
    public final ImageView d;
    @NonNull
    public final TextView e;
    @NonNull
    public final TextView f;
    @NonNull
    public final ImageView g;
    @NonNull
    public final CheckedSetting h;
    @NonNull
    public final ProgressBar i;

    public WidgetServerSettingsIntegrationListItemBinding(@NonNull FrameLayout frameLayout, @NonNull RelativeLayout relativeLayout, @NonNull View view, @NonNull ImageView imageView, @NonNull TextView textView, @NonNull LinearLayout linearLayout, @NonNull TextView textView2, @NonNull ImageView imageView2, @NonNull CheckedSetting checkedSetting, @NonNull ProgressBar progressBar) {
        this.a = frameLayout;
        this.f2550b = relativeLayout;
        this.c = view;
        this.d = imageView;
        this.e = textView;
        this.f = textView2;
        this.g = imageView2;
        this.h = checkedSetting;
        this.i = progressBar;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
