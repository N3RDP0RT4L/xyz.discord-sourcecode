package com.discord.databinding;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.facebook.samples.zoomable.ZoomableDraweeView;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.material.appbar.AppBarLayout;
/* loaded from: classes.dex */
public final class WidgetMediaBinding implements ViewBinding {
    @NonNull
    public final FrameLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final AppBarLayout f2463b;
    @NonNull
    public final FrameLayout c;
    @NonNull
    public final ZoomableDraweeView d;
    @NonNull
    public final ProgressBar e;
    @NonNull
    public final PlayerControlView f;
    @NonNull
    public final PlayerView g;

    public WidgetMediaBinding(@NonNull FrameLayout frameLayout, @NonNull AppBarLayout appBarLayout, @NonNull FrameLayout frameLayout2, @NonNull ZoomableDraweeView zoomableDraweeView, @NonNull ProgressBar progressBar, @NonNull PlayerControlView playerControlView, @NonNull PlayerView playerView) {
        this.a = frameLayout;
        this.f2463b = appBarLayout;
        this.c = frameLayout2;
        this.d = zoomableDraweeView;
        this.e = progressBar;
        this.f = playerControlView;
        this.g = playerView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
