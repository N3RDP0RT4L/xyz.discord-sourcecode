package com.discord.databinding;

import android.view.View;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.tooltips.SparkleView;
/* loaded from: classes.dex */
public final class WidgetGuildsListItemHubVerificationBinding implements ViewBinding {
    @NonNull
    public final FrameLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final SparkleView f2440b;

    public WidgetGuildsListItemHubVerificationBinding(@NonNull FrameLayout frameLayout, @NonNull SparkleView sparkleView) {
        this.a = frameLayout;
        this.f2440b = sparkleView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
