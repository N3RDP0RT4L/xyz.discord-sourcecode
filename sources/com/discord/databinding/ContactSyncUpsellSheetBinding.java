package com.discord.databinding;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import b.a.i.m0;
import com.google.android.material.button.MaterialButton;
/* loaded from: classes.dex */
public final class ContactSyncUpsellSheetBinding implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final MaterialButton f2081b;
    @NonNull
    public final MaterialButton c;
    @NonNull
    public final m0 d;

    public ContactSyncUpsellSheetBinding(@NonNull ConstraintLayout constraintLayout, @NonNull LinearLayout linearLayout, @NonNull MaterialButton materialButton, @NonNull MaterialButton materialButton2, @NonNull TextView textView, @NonNull TextView textView2, @NonNull m0 m0Var) {
        this.a = constraintLayout;
        this.f2081b = materialButton;
        this.c = materialButton2;
        this.d = m0Var;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
