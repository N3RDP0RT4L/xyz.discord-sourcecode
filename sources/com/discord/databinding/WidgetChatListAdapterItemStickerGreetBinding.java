package com.discord.databinding;

import android.view.View;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.views.sticker.StickerView;
import com.google.android.material.button.MaterialButton;
/* loaded from: classes.dex */
public final class WidgetChatListAdapterItemStickerGreetBinding implements ViewBinding {
    @NonNull
    public final LinearLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final StickerView f2316b;
    @NonNull
    public final MaterialButton c;

    public WidgetChatListAdapterItemStickerGreetBinding(@NonNull LinearLayout linearLayout, @NonNull StickerView stickerView, @NonNull MaterialButton materialButton) {
        this.a = linearLayout;
        this.f2316b = stickerView;
        this.c = materialButton;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
