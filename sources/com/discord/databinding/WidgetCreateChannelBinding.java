package com.discord.databinding;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.textfield.TextInputLayout;
/* loaded from: classes.dex */
public final class WidgetCreateChannelBinding implements ViewBinding {
    @NonNull
    public final CoordinatorLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ConstraintLayout f2338b;
    @NonNull
    public final RadioButton c;
    @NonNull
    public final TextInputLayout d;
    @NonNull
    public final RelativeLayout e;
    @NonNull
    public final TextView f;
    @NonNull
    public final SwitchCompat g;
    @NonNull
    public final TextView h;
    @NonNull
    public final RecyclerView i;
    @NonNull
    public final View j;
    @NonNull
    public final TextView k;
    @NonNull
    public final ConstraintLayout l;
    @NonNull
    public final RadioButton m;
    @NonNull
    public final ConstraintLayout n;
    @NonNull
    public final RadioButton o;
    @NonNull
    public final LinearLayout p;
    @NonNull
    public final ConstraintLayout q;
    @NonNull
    public final RadioButton r;

    public WidgetCreateChannelBinding(@NonNull CoordinatorLayout coordinatorLayout, @NonNull ConstraintLayout constraintLayout, @NonNull ImageView imageView, @NonNull RadioButton radioButton, @NonNull TextView textView, @NonNull TextInputLayout textInputLayout, @NonNull RelativeLayout relativeLayout, @NonNull TextView textView2, @NonNull SwitchCompat switchCompat, @NonNull TextView textView3, @NonNull RecyclerView recyclerView, @NonNull View view, @NonNull TextView textView4, @NonNull ConstraintLayout constraintLayout2, @NonNull ImageView imageView2, @NonNull RadioButton radioButton2, @NonNull TextView textView5, @NonNull ConstraintLayout constraintLayout3, @NonNull ImageView imageView3, @NonNull RadioButton radioButton3, @NonNull TextView textView6, @NonNull LinearLayout linearLayout, @NonNull ConstraintLayout constraintLayout4, @NonNull ImageView imageView4, @NonNull RadioButton radioButton4, @NonNull TextView textView7) {
        this.a = coordinatorLayout;
        this.f2338b = constraintLayout;
        this.c = radioButton;
        this.d = textInputLayout;
        this.e = relativeLayout;
        this.f = textView2;
        this.g = switchCompat;
        this.h = textView3;
        this.i = recyclerView;
        this.j = view;
        this.k = textView4;
        this.l = constraintLayout2;
        this.m = radioButton2;
        this.n = constraintLayout3;
        this.o = radioButton3;
        this.p = linearLayout;
        this.q = constraintLayout4;
        this.r = radioButton4;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
