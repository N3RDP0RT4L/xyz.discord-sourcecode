package com.discord.databinding;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
import com.discord.app.AppViewFlipper;
import com.discord.views.SearchInputView;
import com.google.android.material.appbar.AppBarLayout;
/* loaded from: classes.dex */
public final class WidgetChannelPickerSheetBinding implements ViewBinding {
    @NonNull
    public final CoordinatorLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final AppViewFlipper f2247b;
    @NonNull
    public final RecyclerView c;
    @NonNull
    public final SearchInputView d;

    public WidgetChannelPickerSheetBinding(@NonNull CoordinatorLayout coordinatorLayout, @NonNull AppBarLayout appBarLayout, @NonNull AppViewFlipper appViewFlipper, @NonNull RecyclerView recyclerView, @NonNull SearchInputView searchInputView, @NonNull Toolbar toolbar) {
        this.a = coordinatorLayout;
        this.f2247b = appViewFlipper;
        this.c = recyclerView;
        this.d = searchInputView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
