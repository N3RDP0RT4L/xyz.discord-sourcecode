package com.discord.databinding;

import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.google.android.material.button.MaterialButton;
/* loaded from: classes.dex */
public final class WidgetTosAcceptBinding implements ViewBinding {
    @NonNull
    public final RelativeLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final CheckBox f2647b;
    @NonNull
    public final MaterialButton c;
    @NonNull
    public final LinkifiedTextView d;
    @NonNull
    public final LinkifiedTextView e;

    public WidgetTosAcceptBinding(@NonNull RelativeLayout relativeLayout, @NonNull CheckBox checkBox, @NonNull MaterialButton materialButton, @NonNull LinearLayout linearLayout, @NonNull LinkifiedTextView linkifiedTextView, @NonNull LinkifiedTextView linkifiedTextView2, @NonNull TextView textView, @NonNull TextView textView2) {
        this.a = relativeLayout;
        this.f2647b = checkBox;
        this.c = materialButton;
        this.d = linkifiedTextView;
        this.e = linkifiedTextView2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
