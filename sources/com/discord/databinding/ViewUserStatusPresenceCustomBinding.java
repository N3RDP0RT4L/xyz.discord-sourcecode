package com.discord.databinding;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.viewbinding.ViewBinding;
import com.facebook.drawee.view.SimpleDraweeView;
/* loaded from: classes.dex */
public final class ViewUserStatusPresenceCustomBinding implements ViewBinding {
    @NonNull
    public final LinearLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final AppCompatImageView f2199b;
    @NonNull
    public final SimpleDraweeView c;
    @NonNull
    public final TextView d;

    public ViewUserStatusPresenceCustomBinding(@NonNull LinearLayout linearLayout, @NonNull AppCompatImageView appCompatImageView, @NonNull SimpleDraweeView simpleDraweeView, @NonNull TextView textView) {
        this.a = linearLayout;
        this.f2199b = appCompatImageView;
        this.c = simpleDraweeView;
        this.d = textView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
