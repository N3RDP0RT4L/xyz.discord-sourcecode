package com.discord.databinding;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.radiobutton.MaterialRadioButton;
import xyz.discord.R;
/* loaded from: classes.dex */
public final class WidgetThreadArchiveActionsSheetBinding implements ViewBinding {
    @NonNull
    public final NestedScrollView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f2640b;
    @NonNull
    public final LinearLayout c;
    @NonNull
    public final ConstraintLayout d;
    @NonNull
    public final MaterialRadioButton e;
    @NonNull
    public final ConstraintLayout f;
    @NonNull
    public final TextView g;
    @NonNull
    public final ImageView h;
    @NonNull
    public final TextView i;
    @NonNull
    public final MaterialRadioButton j;
    @NonNull
    public final ConstraintLayout k;
    @NonNull
    public final TextView l;
    @NonNull
    public final ImageView m;
    @NonNull
    public final TextView n;
    @NonNull
    public final MaterialRadioButton o;
    @NonNull
    public final ConstraintLayout p;
    @NonNull
    public final MaterialRadioButton q;
    @NonNull
    public final TextView r;

    public WidgetThreadArchiveActionsSheetBinding(@NonNull NestedScrollView nestedScrollView, @NonNull TextView textView, @NonNull LinearLayout linearLayout, @NonNull LinearLayout linearLayout2, @NonNull ConstraintLayout constraintLayout, @NonNull MaterialRadioButton materialRadioButton, @NonNull ConstraintLayout constraintLayout2, @NonNull TextView textView2, @NonNull ImageView imageView, @NonNull TextView textView3, @NonNull MaterialRadioButton materialRadioButton2, @NonNull ConstraintLayout constraintLayout3, @NonNull TextView textView4, @NonNull ImageView imageView2, @NonNull TextView textView5, @NonNull MaterialRadioButton materialRadioButton3, @NonNull ConstraintLayout constraintLayout4, @NonNull MaterialRadioButton materialRadioButton4, @NonNull TextView textView6, @NonNull TextView textView7) {
        this.a = nestedScrollView;
        this.f2640b = textView;
        this.c = linearLayout;
        this.d = constraintLayout;
        this.e = materialRadioButton;
        this.f = constraintLayout2;
        this.g = textView2;
        this.h = imageView;
        this.i = textView3;
        this.j = materialRadioButton2;
        this.k = constraintLayout3;
        this.l = textView4;
        this.m = imageView2;
        this.n = textView5;
        this.o = materialRadioButton3;
        this.p = constraintLayout4;
        this.q = materialRadioButton4;
        this.r = textView6;
    }

    @NonNull
    public static WidgetThreadArchiveActionsSheetBinding a(@NonNull View view) {
        int i = R.id.archive_now;
        TextView textView = (TextView) view.findViewById(R.id.archive_now);
        if (textView != null) {
            i = R.id.auto_archive_header;
            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.auto_archive_header);
            if (linearLayout != null) {
                i = R.id.auto_archive_sheet_duration_options;
                LinearLayout linearLayout2 = (LinearLayout) view.findViewById(R.id.auto_archive_sheet_duration_options);
                if (linearLayout2 != null) {
                    i = R.id.option_one_hour;
                    ConstraintLayout constraintLayout = (ConstraintLayout) view.findViewById(R.id.option_one_hour);
                    if (constraintLayout != null) {
                        i = R.id.option_one_hour_radio;
                        MaterialRadioButton materialRadioButton = (MaterialRadioButton) view.findViewById(R.id.option_one_hour_radio);
                        if (materialRadioButton != null) {
                            i = R.id.option_seven_days;
                            ConstraintLayout constraintLayout2 = (ConstraintLayout) view.findViewById(R.id.option_seven_days);
                            if (constraintLayout2 != null) {
                                i = R.id.option_seven_days_duration;
                                TextView textView2 = (TextView) view.findViewById(R.id.option_seven_days_duration);
                                if (textView2 != null) {
                                    i = R.id.option_seven_days_icon;
                                    ImageView imageView = (ImageView) view.findViewById(R.id.option_seven_days_icon);
                                    if (imageView != null) {
                                        i = R.id.option_seven_days_label;
                                        TextView textView3 = (TextView) view.findViewById(R.id.option_seven_days_label);
                                        if (textView3 != null) {
                                            i = R.id.option_seven_days_radio;
                                            MaterialRadioButton materialRadioButton2 = (MaterialRadioButton) view.findViewById(R.id.option_seven_days_radio);
                                            if (materialRadioButton2 != null) {
                                                i = R.id.option_three_days;
                                                ConstraintLayout constraintLayout3 = (ConstraintLayout) view.findViewById(R.id.option_three_days);
                                                if (constraintLayout3 != null) {
                                                    i = R.id.option_three_days_duration;
                                                    TextView textView4 = (TextView) view.findViewById(R.id.option_three_days_duration);
                                                    if (textView4 != null) {
                                                        i = R.id.option_three_days_icon;
                                                        ImageView imageView2 = (ImageView) view.findViewById(R.id.option_three_days_icon);
                                                        if (imageView2 != null) {
                                                            i = R.id.option_three_days_label;
                                                            TextView textView5 = (TextView) view.findViewById(R.id.option_three_days_label);
                                                            if (textView5 != null) {
                                                                i = R.id.option_three_days_radio;
                                                                MaterialRadioButton materialRadioButton3 = (MaterialRadioButton) view.findViewById(R.id.option_three_days_radio);
                                                                if (materialRadioButton3 != null) {
                                                                    i = R.id.option_twenty_four_hours;
                                                                    ConstraintLayout constraintLayout4 = (ConstraintLayout) view.findViewById(R.id.option_twenty_four_hours);
                                                                    if (constraintLayout4 != null) {
                                                                        i = R.id.option_twenty_four_hours_radio;
                                                                        MaterialRadioButton materialRadioButton4 = (MaterialRadioButton) view.findViewById(R.id.option_twenty_four_hours_radio);
                                                                        if (materialRadioButton4 != null) {
                                                                            i = R.id.subtitle;
                                                                            TextView textView6 = (TextView) view.findViewById(R.id.subtitle);
                                                                            if (textView6 != null) {
                                                                                i = R.id.title;
                                                                                TextView textView7 = (TextView) view.findViewById(R.id.title);
                                                                                if (textView7 != null) {
                                                                                    return new WidgetThreadArchiveActionsSheetBinding((NestedScrollView) view, textView, linearLayout, linearLayout2, constraintLayout, materialRadioButton, constraintLayout2, textView2, imageView, textView3, materialRadioButton2, constraintLayout3, textView4, imageView2, textView5, materialRadioButton3, constraintLayout4, materialRadioButton4, textView6, textView7);
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
