package com.discord.databinding;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import androidx.viewbinding.ViewBinding;
import com.discord.widgets.servers.NotificationMuteSettingsView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
/* loaded from: classes.dex */
public final class WidgetChannelGroupDmSettingsBinding implements ViewBinding {
    @NonNull
    public final CoordinatorLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextInputLayout f2240b;
    @NonNull
    public final FloatingActionButton c;
    @NonNull
    public final NotificationMuteSettingsView d;
    @NonNull
    public final NestedScrollView e;
    @NonNull
    public final SimpleDraweeView f;
    @NonNull
    public final TextView g;
    @NonNull
    public final TextView h;

    public WidgetChannelGroupDmSettingsBinding(@NonNull CoordinatorLayout coordinatorLayout, @NonNull TextInputLayout textInputLayout, @NonNull FloatingActionButton floatingActionButton, @NonNull NotificationMuteSettingsView notificationMuteSettingsView, @NonNull NestedScrollView nestedScrollView, @NonNull SimpleDraweeView simpleDraweeView, @NonNull TextView textView, @NonNull TextView textView2) {
        this.a = coordinatorLayout;
        this.f2240b = textInputLayout;
        this.c = floatingActionButton;
        this.d = notificationMuteSettingsView;
        this.e = nestedScrollView;
        this.f = simpleDraweeView;
        this.g = textView;
        this.h = textView2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
