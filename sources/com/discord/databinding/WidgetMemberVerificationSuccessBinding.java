package com.discord.databinding;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.button.MaterialButton;
/* loaded from: classes.dex */
public final class WidgetMemberVerificationSuccessBinding implements ViewBinding {
    @NonNull
    public final RelativeLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final MaterialButton f2471b;
    @NonNull
    public final TextView c;

    public WidgetMemberVerificationSuccessBinding(@NonNull RelativeLayout relativeLayout, @NonNull MaterialButton materialButton, @NonNull TextView textView, @NonNull CardView cardView) {
        this.a = relativeLayout;
        this.f2471b = materialButton;
        this.c = textView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
