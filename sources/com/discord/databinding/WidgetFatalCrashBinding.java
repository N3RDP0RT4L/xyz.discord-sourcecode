package com.discord.databinding;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.utilities.view.text.LinkifiedTextView;
/* loaded from: classes.dex */
public final class WidgetFatalCrashBinding implements ViewBinding {
    @NonNull
    public final ScrollView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f2365b;
    @NonNull
    public final TextView c;
    @NonNull
    public final TextView d;
    @NonNull
    public final TextView e;
    @NonNull
    public final LinkifiedTextView f;
    @NonNull
    public final TextView g;

    public WidgetFatalCrashBinding(@NonNull ScrollView scrollView, @NonNull TextView textView, @NonNull LinearLayout linearLayout, @NonNull TextView textView2, @NonNull TextView textView3, @NonNull TextView textView4, @NonNull LinkifiedTextView linkifiedTextView, @NonNull TextView textView5) {
        this.a = scrollView;
        this.f2365b = textView;
        this.c = textView2;
        this.d = textView3;
        this.e = textView4;
        this.f = linkifiedTextView;
        this.g = textView5;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
