package com.discord.databinding;

import android.view.View;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.views.JoinVoiceChannelButton;
/* loaded from: classes.dex */
public final class WidgetGuildCallOnboardingBinding implements ViewBinding {
    @NonNull
    public final LinearLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final JoinVoiceChannelButton f2387b;

    public WidgetGuildCallOnboardingBinding(@NonNull LinearLayout linearLayout, @NonNull JoinVoiceChannelButton joinVoiceChannelButton) {
        this.a = linearLayout;
        this.f2387b = joinVoiceChannelButton;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
