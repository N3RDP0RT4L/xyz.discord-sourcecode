package com.discord.databinding;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.viewbinding.ViewBinding;
import com.discord.views.CheckedSetting;
/* loaded from: classes.dex */
public final class WidgetThreadBrowserFilterSheetBinding implements ViewBinding {
    @NonNull
    public final NestedScrollView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final CheckedSetting f2644b;
    @NonNull
    public final CardView c;
    @NonNull
    public final CheckedSetting d;
    @NonNull
    public final CheckedSetting e;

    public WidgetThreadBrowserFilterSheetBinding(@NonNull NestedScrollView nestedScrollView, @NonNull TextView textView, @NonNull CheckedSetting checkedSetting, @NonNull CardView cardView, @NonNull CheckedSetting checkedSetting2, @NonNull CheckedSetting checkedSetting3) {
        this.a = nestedScrollView;
        this.f2644b = checkedSetting;
        this.c = cardView;
        this.d = checkedSetting2;
        this.e = checkedSetting3;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
