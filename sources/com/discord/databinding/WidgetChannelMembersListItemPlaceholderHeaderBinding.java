package com.discord.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewbinding.ViewBinding;
import xyz.discord.R;
/* loaded from: classes.dex */
public final class WidgetChannelMembersListItemPlaceholderHeaderBinding implements ViewBinding {
    @NonNull
    public final FrameLayout a;

    public WidgetChannelMembersListItemPlaceholderHeaderBinding(@NonNull FrameLayout frameLayout, @NonNull View view) {
        this.a = frameLayout;
    }

    @NonNull
    public static WidgetChannelMembersListItemPlaceholderHeaderBinding a(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, boolean z2) {
        View inflate = layoutInflater.inflate(R.layout.widget_channel_members_list_item_placeholder_header, viewGroup, false);
        if (z2) {
            viewGroup.addView(inflate);
        }
        View findViewById = inflate.findViewById(R.id.channel_members_list_item_header_placeholder);
        if (findViewById != null) {
            return new WidgetChannelMembersListItemPlaceholderHeaderBinding((FrameLayout) inflate, findViewById);
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(R.id.channel_members_list_item_header_placeholder)));
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
