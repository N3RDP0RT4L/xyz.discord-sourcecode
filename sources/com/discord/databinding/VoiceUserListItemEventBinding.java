package com.discord.databinding;

import android.view.View;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventItemView;
/* loaded from: classes.dex */
public final class VoiceUserListItemEventBinding implements ViewBinding {
    @NonNull
    public final FrameLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final GuildScheduledEventItemView f2203b;

    public VoiceUserListItemEventBinding(@NonNull FrameLayout frameLayout, @NonNull GuildScheduledEventItemView guildScheduledEventItemView) {
        this.a = frameLayout;
        this.f2203b = guildScheduledEventItemView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
