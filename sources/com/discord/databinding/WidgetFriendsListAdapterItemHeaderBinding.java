package com.discord.databinding;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
/* loaded from: classes.dex */
public final class WidgetFriendsListAdapterItemHeaderBinding implements ViewBinding {
    @NonNull
    public final FrameLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f2371b;

    public WidgetFriendsListAdapterItemHeaderBinding(@NonNull FrameLayout frameLayout, @NonNull TextView textView) {
        this.a = frameLayout;
        this.f2371b = textView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
