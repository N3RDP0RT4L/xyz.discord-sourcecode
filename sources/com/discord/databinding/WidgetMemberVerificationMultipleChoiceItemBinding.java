package com.discord.databinding;

import android.view.View;
import android.widget.RadioGroup;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
/* loaded from: classes.dex */
public final class WidgetMemberVerificationMultipleChoiceItemBinding implements ViewBinding {
    @NonNull
    public final RadioGroup a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final RadioGroup f2467b;

    public WidgetMemberVerificationMultipleChoiceItemBinding(@NonNull RadioGroup radioGroup, @NonNull RadioGroup radioGroup2) {
        this.a = radioGroup;
        this.f2467b = radioGroup2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
