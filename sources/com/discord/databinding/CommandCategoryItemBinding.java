package com.discord.databinding;

import android.view.View;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import b.a.i.n;
import com.facebook.drawee.view.SimpleDraweeView;
/* loaded from: classes.dex */
public final class CommandCategoryItemBinding implements ViewBinding {
    @NonNull
    public final FrameLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final SimpleDraweeView f2078b;
    @NonNull
    public final n c;

    public CommandCategoryItemBinding(@NonNull FrameLayout frameLayout, @NonNull SimpleDraweeView simpleDraweeView, @NonNull n nVar) {
        this.a = frameLayout;
        this.f2078b = simpleDraweeView;
        this.c = nVar;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
