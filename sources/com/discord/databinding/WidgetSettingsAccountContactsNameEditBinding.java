package com.discord.databinding;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
/* loaded from: classes.dex */
public final class WidgetSettingsAccountContactsNameEditBinding implements ViewBinding {
    @NonNull
    public final CoordinatorLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextInputLayout f2571b;
    @NonNull
    public final MaterialButton c;
    @NonNull
    public final FloatingActionButton d;

    public WidgetSettingsAccountContactsNameEditBinding(@NonNull CoordinatorLayout coordinatorLayout, @NonNull TextInputLayout textInputLayout, @NonNull MaterialButton materialButton, @NonNull FloatingActionButton floatingActionButton) {
        this.a = coordinatorLayout;
        this.f2571b = textInputLayout;
        this.c = materialButton;
        this.d = floatingActionButton;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
