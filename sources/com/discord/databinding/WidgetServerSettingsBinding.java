package com.discord.databinding;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewbinding.ViewBinding;
import com.facebook.drawee.view.SimpleDraweeView;
/* loaded from: classes.dex */
public final class WidgetServerSettingsBinding implements ViewBinding {
    @NonNull
    public final TextView A;
    @NonNull
    public final CoordinatorLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f2521b;
    @NonNull
    public final TextView c;
    @NonNull
    public final TextView d;
    @NonNull
    public final TextView e;
    @NonNull
    public final TextView f;
    @NonNull
    public final TextView g;
    @NonNull
    public final TextView h;
    @NonNull
    public final TextView i;
    @NonNull
    public final TextView j;
    @NonNull
    public final TextView k;
    @NonNull
    public final TextView l;
    @NonNull
    public final TextView m;
    @NonNull
    public final TextView n;
    @NonNull
    public final TextView o;
    @NonNull
    public final TextView p;
    @NonNull
    public final TextView q;
    @NonNull
    public final TextView r;
    @NonNull

    /* renamed from: s  reason: collision with root package name */
    public final TextView f2522s;
    @NonNull
    public final LinearLayout t;
    @NonNull
    public final LinearLayout u;
    @NonNull
    public final LinearLayout v;
    @NonNull
    public final LinearLayout w;
    @NonNull

    /* renamed from: x  reason: collision with root package name */
    public final View f2523x;
    @NonNull

    /* renamed from: y  reason: collision with root package name */
    public final SimpleDraweeView f2524y;
    @NonNull

    /* renamed from: z  reason: collision with root package name */
    public final TextView f2525z;

    public WidgetServerSettingsBinding(@NonNull CoordinatorLayout coordinatorLayout, @NonNull TextView textView, @NonNull TextView textView2, @NonNull TextView textView3, @NonNull TextView textView4, @NonNull TextView textView5, @NonNull TextView textView6, @NonNull TextView textView7, @NonNull TextView textView8, @NonNull TextView textView9, @NonNull TextView textView10, @NonNull TextView textView11, @NonNull TextView textView12, @NonNull TextView textView13, @NonNull TextView textView14, @NonNull TextView textView15, @NonNull TextView textView16, @NonNull TextView textView17, @NonNull TextView textView18, @NonNull LinearLayout linearLayout, @NonNull View view, @NonNull LinearLayout linearLayout2, @NonNull LinearLayout linearLayout3, @NonNull View view2, @NonNull LinearLayout linearLayout4, @NonNull View view3, @NonNull SimpleDraweeView simpleDraweeView, @NonNull TextView textView19, @NonNull TextView textView20) {
        this.a = coordinatorLayout;
        this.f2521b = textView;
        this.c = textView2;
        this.d = textView3;
        this.e = textView4;
        this.f = textView5;
        this.g = textView6;
        this.h = textView7;
        this.i = textView8;
        this.j = textView9;
        this.k = textView10;
        this.l = textView11;
        this.m = textView12;
        this.n = textView13;
        this.o = textView14;
        this.p = textView15;
        this.q = textView16;
        this.r = textView17;
        this.f2522s = textView18;
        this.t = linearLayout;
        this.u = linearLayout2;
        this.v = linearLayout3;
        this.w = linearLayout4;
        this.f2523x = view3;
        this.f2524y = simpleDraweeView;
        this.f2525z = textView19;
        this.A = textView20;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
