package com.discord.databinding;

import android.view.View;
import android.widget.RelativeLayout;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
import b.a.i.r0;
import b.a.i.s0;
import com.discord.widgets.voice.controls.AnchoredVoiceControlsView;
import com.google.android.material.button.MaterialButton;
/* loaded from: classes.dex */
public final class WidgetVoiceBottomSheetBinding implements ViewBinding {
    @NonNull
    public final CoordinatorLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final r0 f2672b;
    @NonNull
    public final s0 c;
    @NonNull
    public final MaterialButton d;
    @NonNull
    public final RelativeLayout e;
    @NonNull
    public final AnchoredVoiceControlsView f;
    @NonNull
    public final MaterialButton g;
    @NonNull
    public final RecyclerView h;
    @NonNull
    public final CoordinatorLayout i;

    public WidgetVoiceBottomSheetBinding(@NonNull CoordinatorLayout coordinatorLayout, @NonNull r0 r0Var, @NonNull s0 s0Var, @NonNull ConstraintLayout constraintLayout, @NonNull MaterialButton materialButton, @NonNull RelativeLayout relativeLayout, @NonNull AnchoredVoiceControlsView anchoredVoiceControlsView, @NonNull MaterialButton materialButton2, @NonNull RecyclerView recyclerView, @NonNull CoordinatorLayout coordinatorLayout2) {
        this.a = coordinatorLayout;
        this.f2672b = r0Var;
        this.c = s0Var;
        this.d = materialButton;
        this.e = relativeLayout;
        this.f = anchoredVoiceControlsView;
        this.g = materialButton2;
        this.h = recyclerView;
        this.i = coordinatorLayout2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
