package com.discord.databinding;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.views.directories.ServerDiscoveryItem;
/* loaded from: classes.dex */
public final class DirectoryEntryListItemBinding implements ViewBinding {
    @NonNull
    public final ServerDiscoveryItem a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ServerDiscoveryItem f2084b;

    public DirectoryEntryListItemBinding(@NonNull ServerDiscoveryItem serverDiscoveryItem, @NonNull ServerDiscoveryItem serverDiscoveryItem2) {
        this.a = serverDiscoveryItem;
        this.f2084b = serverDiscoveryItem2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
