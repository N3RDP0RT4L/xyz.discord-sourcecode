package com.discord.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.utilities.view.text.LinkifiedTextView;
import xyz.discord.R;
/* loaded from: classes.dex */
public final class ViewGuildRoleSubscriptionRevenueMetricBinding implements ViewBinding {
    @NonNull
    public final View a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f2181b;
    @NonNull
    public final TextView c;
    @NonNull
    public final LinkifiedTextView d;
    @NonNull
    public final ImageView e;
    @NonNull
    public final TextView f;

    public ViewGuildRoleSubscriptionRevenueMetricBinding(@NonNull View view, @NonNull TextView textView, @NonNull TextView textView2, @NonNull LinkifiedTextView linkifiedTextView, @NonNull ImageView imageView, @NonNull TextView textView3) {
        this.a = view;
        this.f2181b = textView;
        this.c = textView2;
        this.d = linkifiedTextView;
        this.e = imageView;
        this.f = textView3;
    }

    @NonNull
    public static ViewGuildRoleSubscriptionRevenueMetricBinding a(@NonNull LayoutInflater layoutInflater, @NonNull ViewGroup viewGroup) {
        layoutInflater.inflate(R.layout.view_guild_role_subscription_revenue_metric, viewGroup);
        int i = R.id.metrics_date;
        TextView textView = (TextView) viewGroup.findViewById(R.id.metrics_date);
        if (textView != null) {
            i = R.id.metrics_description;
            TextView textView2 = (TextView) viewGroup.findViewById(R.id.metrics_description);
            if (textView2 != null) {
                i = R.id.metrics_footer;
                LinkifiedTextView linkifiedTextView = (LinkifiedTextView) viewGroup.findViewById(R.id.metrics_footer);
                if (linkifiedTextView != null) {
                    i = R.id.metrics_footer_leading_icon;
                    ImageView imageView = (ImageView) viewGroup.findViewById(R.id.metrics_footer_leading_icon);
                    if (imageView != null) {
                        i = R.id.metrics_title;
                        TextView textView3 = (TextView) viewGroup.findViewById(R.id.metrics_title);
                        if (textView3 != null) {
                            return new ViewGuildRoleSubscriptionRevenueMetricBinding(viewGroup, textView, textView2, linkifiedTextView, imageView, textView3);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(viewGroup.getResources().getResourceName(i)));
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
