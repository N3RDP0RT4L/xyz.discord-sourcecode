package com.discord.databinding;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
/* loaded from: classes.dex */
public final class WidgetFriendsAddByIdBinding implements ViewBinding {
    @NonNull
    public final NestedScrollView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f2369b;
    @NonNull
    public final LinearLayout c;
    @NonNull
    public final MaterialButton d;
    @NonNull
    public final TextInputLayout e;

    public WidgetFriendsAddByIdBinding(@NonNull NestedScrollView nestedScrollView, @NonNull TextView textView, @NonNull LinearLayout linearLayout, @NonNull MaterialButton materialButton, @NonNull TextInputLayout textInputLayout) {
        this.a = nestedScrollView;
        this.f2369b = textView;
        this.c = linearLayout;
        this.d = materialButton;
        this.e = textInputLayout;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
