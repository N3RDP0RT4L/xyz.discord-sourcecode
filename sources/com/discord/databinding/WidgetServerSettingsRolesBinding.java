package com.discord.databinding;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
import com.discord.utilities.dimmer.DimmerView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
/* loaded from: classes.dex */
public final class WidgetServerSettingsRolesBinding implements ViewBinding {
    @NonNull
    public final CoordinatorLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final DimmerView f2559b;
    @NonNull
    public final FloatingActionButton c;
    @NonNull
    public final RecyclerView d;

    public WidgetServerSettingsRolesBinding(@NonNull CoordinatorLayout coordinatorLayout, @NonNull DimmerView dimmerView, @NonNull FloatingActionButton floatingActionButton, @NonNull RecyclerView recyclerView) {
        this.a = coordinatorLayout;
        this.f2559b = dimmerView;
        this.c = floatingActionButton;
        this.d = recyclerView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
