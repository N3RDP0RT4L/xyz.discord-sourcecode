package com.discord.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.discord.views.user.UserAvatarPresenceView;
import xyz.discord.R;
/* loaded from: classes.dex */
public final class TabsHostBottomNavigationViewBinding implements ViewBinding {
    @NonNull
    public final LinearLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ImageView f2140b;
    @NonNull
    public final ConstraintLayout c;
    @NonNull
    public final TextView d;
    @NonNull
    public final ImageView e;
    @NonNull
    public final ConstraintLayout f;
    @NonNull
    public final TextView g;
    @NonNull
    public final ImageView h;
    @NonNull
    public final FrameLayout i;
    @NonNull
    public final ImageView j;
    @NonNull
    public final FrameLayout k;
    @NonNull
    public final LinearLayout l;
    @NonNull
    public final UserAvatarPresenceView m;
    @NonNull
    public final FrameLayout n;

    public TabsHostBottomNavigationViewBinding(@NonNull LinearLayout linearLayout, @NonNull ImageView imageView, @NonNull ConstraintLayout constraintLayout, @NonNull TextView textView, @NonNull ImageView imageView2, @NonNull ConstraintLayout constraintLayout2, @NonNull TextView textView2, @NonNull ImageView imageView3, @NonNull FrameLayout frameLayout, @NonNull ImageView imageView4, @NonNull FrameLayout frameLayout2, @NonNull ImageView imageView5, @NonNull FrameLayout frameLayout3, @NonNull LinearLayout linearLayout2, @NonNull UserAvatarPresenceView userAvatarPresenceView, @NonNull FrameLayout frameLayout4) {
        this.a = linearLayout;
        this.f2140b = imageView;
        this.c = constraintLayout;
        this.d = textView;
        this.e = imageView2;
        this.f = constraintLayout2;
        this.g = textView2;
        this.h = imageView3;
        this.i = frameLayout;
        this.j = imageView4;
        this.k = frameLayout2;
        this.l = linearLayout2;
        this.m = userAvatarPresenceView;
        this.n = frameLayout4;
    }

    @NonNull
    public static TabsHostBottomNavigationViewBinding a(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, boolean z2) {
        View inflate = layoutInflater.inflate(R.layout.tabs_host_bottom_navigation_view, viewGroup, false);
        if (z2) {
            viewGroup.addView(inflate);
        }
        int i = R.id.tabs_host_bottom_nav_friends_icon;
        ImageView imageView = (ImageView) inflate.findViewById(R.id.tabs_host_bottom_nav_friends_icon);
        if (imageView != null) {
            i = R.id.tabs_host_bottom_nav_friends_item;
            ConstraintLayout constraintLayout = (ConstraintLayout) inflate.findViewById(R.id.tabs_host_bottom_nav_friends_item);
            if (constraintLayout != null) {
                i = R.id.tabs_host_bottom_nav_friends_notifications_badge;
                TextView textView = (TextView) inflate.findViewById(R.id.tabs_host_bottom_nav_friends_notifications_badge);
                if (textView != null) {
                    i = R.id.tabs_host_bottom_nav_home_icon;
                    ImageView imageView2 = (ImageView) inflate.findViewById(R.id.tabs_host_bottom_nav_home_icon);
                    if (imageView2 != null) {
                        i = R.id.tabs_host_bottom_nav_home_item;
                        ConstraintLayout constraintLayout2 = (ConstraintLayout) inflate.findViewById(R.id.tabs_host_bottom_nav_home_item);
                        if (constraintLayout2 != null) {
                            i = R.id.tabs_host_bottom_nav_home_notifications_badge;
                            TextView textView2 = (TextView) inflate.findViewById(R.id.tabs_host_bottom_nav_home_notifications_badge);
                            if (textView2 != null) {
                                i = R.id.tabs_host_bottom_nav_mentions_icon;
                                ImageView imageView3 = (ImageView) inflate.findViewById(R.id.tabs_host_bottom_nav_mentions_icon);
                                if (imageView3 != null) {
                                    i = R.id.tabs_host_bottom_nav_mentions_item;
                                    FrameLayout frameLayout = (FrameLayout) inflate.findViewById(R.id.tabs_host_bottom_nav_mentions_item);
                                    if (frameLayout != null) {
                                        i = R.id.tabs_host_bottom_nav_search_icon;
                                        ImageView imageView4 = (ImageView) inflate.findViewById(R.id.tabs_host_bottom_nav_search_icon);
                                        if (imageView4 != null) {
                                            i = R.id.tabs_host_bottom_nav_search_item;
                                            FrameLayout frameLayout2 = (FrameLayout) inflate.findViewById(R.id.tabs_host_bottom_nav_search_item);
                                            if (frameLayout2 != null) {
                                                i = R.id.tabs_host_bottom_nav_stage_discovery_icon;
                                                ImageView imageView5 = (ImageView) inflate.findViewById(R.id.tabs_host_bottom_nav_stage_discovery_icon);
                                                if (imageView5 != null) {
                                                    i = R.id.tabs_host_bottom_nav_stage_discovery_item;
                                                    FrameLayout frameLayout3 = (FrameLayout) inflate.findViewById(R.id.tabs_host_bottom_nav_stage_discovery_item);
                                                    if (frameLayout3 != null) {
                                                        i = R.id.tabs_host_bottom_nav_tabs_container;
                                                        LinearLayout linearLayout = (LinearLayout) inflate.findViewById(R.id.tabs_host_bottom_nav_tabs_container);
                                                        if (linearLayout != null) {
                                                            i = R.id.tabs_host_bottom_nav_user_avatar_presence_view;
                                                            UserAvatarPresenceView userAvatarPresenceView = (UserAvatarPresenceView) inflate.findViewById(R.id.tabs_host_bottom_nav_user_avatar_presence_view);
                                                            if (userAvatarPresenceView != null) {
                                                                i = R.id.tabs_host_bottom_nav_user_settings_item;
                                                                FrameLayout frameLayout4 = (FrameLayout) inflate.findViewById(R.id.tabs_host_bottom_nav_user_settings_item);
                                                                if (frameLayout4 != null) {
                                                                    return new TabsHostBottomNavigationViewBinding((LinearLayout) inflate, imageView, constraintLayout, textView, imageView2, constraintLayout2, textView2, imageView3, frameLayout, imageView4, frameLayout2, imageView5, frameLayout3, linearLayout, userAvatarPresenceView, frameLayout4);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
