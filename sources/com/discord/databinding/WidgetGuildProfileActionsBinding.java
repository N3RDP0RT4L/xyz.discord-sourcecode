package com.discord.databinding;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
import com.discord.views.CheckedSetting;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.button.MaterialButton;
/* loaded from: classes.dex */
public final class WidgetGuildProfileActionsBinding implements ViewBinding {
    @NonNull
    public final MaterialButton A;
    @NonNull
    public final LinearLayout B;
    @NonNull
    public final LinearLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final LinearLayout f2403b;
    @NonNull
    public final LinearLayout c;
    @NonNull
    public final CheckedSetting d;
    @NonNull
    public final LinearLayout e;
    @NonNull
    public final LinearLayout f;
    @NonNull
    public final TextView g;
    @NonNull
    public final TextView h;
    @NonNull
    public final TextView i;
    @NonNull
    public final ConstraintLayout j;
    @NonNull
    public final TextView k;
    @NonNull
    public final CardView l;
    @NonNull
    public final RecyclerView m;
    @NonNull
    public final CardView n;
    @NonNull
    public final TextView o;
    @NonNull
    public final SimpleDraweeView p;
    @NonNull
    public final CheckedSetting q;
    @NonNull
    public final MaterialButton r;
    @NonNull

    /* renamed from: s  reason: collision with root package name */
    public final TextView f2404s;
    @NonNull
    public final TextView t;
    @NonNull
    public final CardView u;
    @NonNull
    public final TextView v;
    @NonNull
    public final ImageView w;
    @NonNull

    /* renamed from: x  reason: collision with root package name */
    public final TextView f2405x;
    @NonNull

    /* renamed from: y  reason: collision with root package name */
    public final CardView f2406y;
    @NonNull

    /* renamed from: z  reason: collision with root package name */
    public final MaterialButton f2407z;

    public WidgetGuildProfileActionsBinding(@NonNull LinearLayout linearLayout, @NonNull LinearLayout linearLayout2, @NonNull TextView textView, @NonNull LinearLayout linearLayout3, @NonNull CheckedSetting checkedSetting, @NonNull LinearLayout linearLayout4, @NonNull LinearLayout linearLayout5, @NonNull TextView textView2, @NonNull TextView textView3, @NonNull TextView textView4, @NonNull ConstraintLayout constraintLayout, @NonNull TextView textView5, @NonNull TextView textView6, @NonNull CardView cardView, @NonNull RecyclerView recyclerView, @NonNull CardView cardView2, @NonNull TextView textView7, @NonNull SimpleDraweeView simpleDraweeView, @NonNull CheckedSetting checkedSetting2, @NonNull MaterialButton materialButton, @NonNull TextView textView8, @NonNull TextView textView9, @NonNull CardView cardView3, @NonNull TextView textView10, @NonNull ImageView imageView, @NonNull TextView textView11, @NonNull CardView cardView4, @NonNull CardView cardView5, @NonNull MaterialButton materialButton2, @NonNull MaterialButton materialButton3, @NonNull LinearLayout linearLayout6) {
        this.a = linearLayout;
        this.f2403b = linearLayout2;
        this.c = linearLayout3;
        this.d = checkedSetting;
        this.e = linearLayout4;
        this.f = linearLayout5;
        this.g = textView2;
        this.h = textView3;
        this.i = textView4;
        this.j = constraintLayout;
        this.k = textView6;
        this.l = cardView;
        this.m = recyclerView;
        this.n = cardView2;
        this.o = textView7;
        this.p = simpleDraweeView;
        this.q = checkedSetting2;
        this.r = materialButton;
        this.f2404s = textView8;
        this.t = textView9;
        this.u = cardView3;
        this.v = textView10;
        this.w = imageView;
        this.f2405x = textView11;
        this.f2406y = cardView4;
        this.f2407z = materialButton2;
        this.A = materialButton3;
        this.B = linearLayout6;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
