package com.discord.databinding;

import android.view.View;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.viewbinding.ViewBinding;
import com.discord.widgets.guilds.invite.WidgetInviteInfo;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventItemView;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.button.MaterialButton;
/* loaded from: classes.dex */
public final class WidgetGuildInvitePageBinding implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final MaterialButton f2394b;
    @NonNull
    public final MaterialButton c;
    @NonNull
    public final GuildScheduledEventItemView d;
    @NonNull
    public final WidgetInviteInfo e;

    public WidgetGuildInvitePageBinding(@NonNull ConstraintLayout constraintLayout, @NonNull MaterialButton materialButton, @NonNull LinearLayout linearLayout, @NonNull MaterialButton materialButton2, @NonNull GuildScheduledEventItemView guildScheduledEventItemView, @NonNull WidgetInviteInfo widgetInviteInfo, @NonNull NestedScrollView nestedScrollView, @NonNull AppBarLayout appBarLayout) {
        this.a = constraintLayout;
        this.f2394b = materialButton;
        this.c = materialButton2;
        this.d = guildScheduledEventItemView;
        this.e = widgetInviteInfo;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
