package com.discord.databinding;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.widgets.channels.threads.browser.ThreadBrowserThreadView;
/* loaded from: classes.dex */
public final class ThreadBrowserItemThreadBinding implements ViewBinding {
    @NonNull
    public final ThreadBrowserThreadView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ThreadBrowserThreadView f2142b;

    public ThreadBrowserItemThreadBinding(@NonNull ThreadBrowserThreadView threadBrowserThreadView, @NonNull ThreadBrowserThreadView threadBrowserThreadView2) {
        this.a = threadBrowserThreadView;
        this.f2142b = threadBrowserThreadView2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
