package com.discord.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.viewbinding.ViewBinding;
import b.a.i.k2;
import b.a.i.l2;
import b.a.i.u4;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.widgets.mobile_reports.MobileReportsBottomButton;
import com.discord.widgets.mobile_reports.MobileReportsBreadcrumbs;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import xyz.discord.R;
/* loaded from: classes.dex */
public final class ViewReportsMenuNodeBinding implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final l2 f2190b;
    @NonNull
    public final l2 c;
    @NonNull
    public final u4 d;
    @NonNull
    public final LinearLayout e;
    @NonNull
    public final k2 f;
    @NonNull
    public final MobileReportsBottomButton g;
    @NonNull
    public final MobileReportsBreadcrumbs h;
    @NonNull
    public final MaterialCardView i;
    @NonNull
    public final LinearLayout j;
    @NonNull
    public final MaterialCardView k;
    @NonNull
    public final TextView l;
    @NonNull
    public final TextView m;
    @NonNull
    public final MaterialCardView n;
    @NonNull
    public final TextView o;
    @NonNull
    public final LinkifiedTextView p;
    @NonNull
    public final ImageView q;

    public ViewReportsMenuNodeBinding(@NonNull ConstraintLayout constraintLayout, @NonNull l2 l2Var, @NonNull l2 l2Var2, @NonNull u4 u4Var, @NonNull LinearLayout linearLayout, @NonNull k2 k2Var, @NonNull MobileReportsBottomButton mobileReportsBottomButton, @NonNull MobileReportsBreadcrumbs mobileReportsBreadcrumbs, @NonNull MaterialCardView materialCardView, @NonNull LinearLayout linearLayout2, @NonNull MaterialCardView materialCardView2, @NonNull TextView textView, @NonNull TextView textView2, @NonNull MaterialCardView materialCardView3, @NonNull TextView textView3, @NonNull MaterialCardView materialCardView4, @NonNull LinkifiedTextView linkifiedTextView, @NonNull ImageView imageView) {
        this.a = constraintLayout;
        this.f2190b = l2Var;
        this.c = l2Var2;
        this.d = u4Var;
        this.e = linearLayout;
        this.f = k2Var;
        this.g = mobileReportsBottomButton;
        this.h = mobileReportsBreadcrumbs;
        this.i = materialCardView;
        this.j = linearLayout2;
        this.k = materialCardView2;
        this.l = textView;
        this.m = textView2;
        this.n = materialCardView3;
        this.o = textView3;
        this.p = linkifiedTextView;
        this.q = imageView;
    }

    @NonNull
    public static ViewReportsMenuNodeBinding a(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, boolean z2) {
        View inflate = layoutInflater.inflate(R.layout.view_reports_menu_node, viewGroup, false);
        if (z2) {
            viewGroup.addView(inflate);
        }
        View findViewById = inflate.findViewById(R.id.mobile_reports_channel_preview);
        int i = R.id.mobile_reports_node_header;
        if (findViewById != null) {
            l2 a = l2.a(findViewById);
            View findViewById2 = inflate.findViewById(R.id.mobile_reports_directory_channel_preview);
            if (findViewById2 != null) {
                l2 a2 = l2.a(findViewById2);
                View findViewById3 = inflate.findViewById(R.id.mobile_reports_message_preview);
                if (findViewById3 != null) {
                    int i2 = R.id.chat_list_adapter_item_chat_attachment_icon;
                    ImageView imageView = (ImageView) findViewById3.findViewById(R.id.chat_list_adapter_item_chat_attachment_icon);
                    if (imageView != null) {
                        i2 = R.id.chat_list_adapter_item_text;
                        LinkifiedTextView linkifiedTextView = (LinkifiedTextView) findViewById3.findViewById(R.id.chat_list_adapter_item_text);
                        if (linkifiedTextView != null) {
                            i2 = R.id.chat_list_adapter_item_text_avatar;
                            SimpleDraweeView simpleDraweeView = (SimpleDraweeView) findViewById3.findViewById(R.id.chat_list_adapter_item_text_avatar);
                            if (simpleDraweeView != null) {
                                i2 = R.id.chat_list_adapter_item_text_name;
                                TextView textView = (TextView) findViewById3.findViewById(R.id.chat_list_adapter_item_text_name);
                                if (textView != null) {
                                    i2 = R.id.report_message_end_guideline;
                                    Guideline guideline = (Guideline) findViewById3.findViewById(R.id.report_message_end_guideline);
                                    if (guideline != null) {
                                        i2 = R.id.report_message_start_guideline;
                                        Guideline guideline2 = (Guideline) findViewById3.findViewById(R.id.report_message_start_guideline);
                                        if (guideline2 != null) {
                                            i2 = R.id.report_message_top_guideline;
                                            Guideline guideline3 = (Guideline) findViewById3.findViewById(R.id.report_message_top_guideline);
                                            if (guideline3 != null) {
                                                u4 u4Var = new u4((ConstraintLayout) findViewById3, imageView, linkifiedTextView, simpleDraweeView, textView, guideline, guideline2, guideline3);
                                                LinearLayout linearLayout = (LinearLayout) inflate.findViewById(R.id.mobile_reports_multiselect);
                                                if (linearLayout != null) {
                                                    View findViewById4 = inflate.findViewById(R.id.mobile_reports_node_block_user);
                                                    if (findViewById4 != null) {
                                                        int i3 = R.id.mobile_reports_block_user_avatar;
                                                        SimpleDraweeView simpleDraweeView2 = (SimpleDraweeView) findViewById4.findViewById(R.id.mobile_reports_block_user_avatar);
                                                        if (simpleDraweeView2 != null) {
                                                            i3 = R.id.mobile_reports_block_user_button;
                                                            MaterialButton materialButton = (MaterialButton) findViewById4.findViewById(R.id.mobile_reports_block_user_button);
                                                            if (materialButton != null) {
                                                                i3 = R.id.mobile_reports_block_user_description;
                                                                TextView textView2 = (TextView) findViewById4.findViewById(R.id.mobile_reports_block_user_description);
                                                                if (textView2 != null) {
                                                                    i3 = R.id.mobile_reports_block_user_header;
                                                                    TextView textView3 = (TextView) findViewById4.findViewById(R.id.mobile_reports_block_user_header);
                                                                    if (textView3 != null) {
                                                                        i3 = R.id.mobile_reports_block_user_name;
                                                                        TextView textView4 = (TextView) findViewById4.findViewById(R.id.mobile_reports_block_user_name);
                                                                        if (textView4 != null) {
                                                                            k2 k2Var = new k2((LinearLayout) findViewById4, simpleDraweeView2, materialButton, textView2, textView3, textView4);
                                                                            MobileReportsBottomButton mobileReportsBottomButton = (MobileReportsBottomButton) inflate.findViewById(R.id.mobile_reports_node_bottom_button);
                                                                            if (mobileReportsBottomButton != null) {
                                                                                MobileReportsBreadcrumbs mobileReportsBreadcrumbs = (MobileReportsBreadcrumbs) inflate.findViewById(R.id.mobile_reports_node_breadcrumbs);
                                                                                if (mobileReportsBreadcrumbs != null) {
                                                                                    MaterialCardView materialCardView = (MaterialCardView) inflate.findViewById(R.id.mobile_reports_node_channel_preview);
                                                                                    if (materialCardView != null) {
                                                                                        LinearLayout linearLayout2 = (LinearLayout) inflate.findViewById(R.id.mobile_reports_node_child_list);
                                                                                        if (linearLayout2 != null) {
                                                                                            MaterialCardView materialCardView2 = (MaterialCardView) inflate.findViewById(R.id.mobile_reports_node_directory_channel_preview);
                                                                                            if (materialCardView2 != null) {
                                                                                                TextView textView5 = (TextView) inflate.findViewById(R.id.mobile_reports_node_directory_channel_preview_title);
                                                                                                if (textView5 != null) {
                                                                                                    TextView textView6 = (TextView) inflate.findViewById(R.id.mobile_reports_node_header);
                                                                                                    if (textView6 != null) {
                                                                                                        MaterialCardView materialCardView3 = (MaterialCardView) inflate.findViewById(R.id.mobile_reports_node_info_box);
                                                                                                        if (materialCardView3 != null) {
                                                                                                            i = R.id.mobile_reports_node_info_text;
                                                                                                            TextView textView7 = (TextView) inflate.findViewById(R.id.mobile_reports_node_info_text);
                                                                                                            if (textView7 != null) {
                                                                                                                MaterialCardView materialCardView4 = (MaterialCardView) inflate.findViewById(R.id.mobile_reports_node_message_preview);
                                                                                                                if (materialCardView4 != null) {
                                                                                                                    i = R.id.mobile_reports_node_subheader;
                                                                                                                    LinkifiedTextView linkifiedTextView2 = (LinkifiedTextView) inflate.findViewById(R.id.mobile_reports_node_subheader);
                                                                                                                    if (linkifiedTextView2 != null) {
                                                                                                                        ImageView imageView2 = (ImageView) inflate.findViewById(R.id.mobile_reports_node_success_shield);
                                                                                                                        if (imageView2 != null) {
                                                                                                                            return new ViewReportsMenuNodeBinding((ConstraintLayout) inflate, a, a2, u4Var, linearLayout, k2Var, mobileReportsBottomButton, mobileReportsBreadcrumbs, materialCardView, linearLayout2, materialCardView2, textView5, textView6, materialCardView3, textView7, materialCardView4, linkifiedTextView2, imageView2);
                                                                                                                        }
                                                                                                                        i = R.id.mobile_reports_node_success_shield;
                                                                                                                    }
                                                                                                                } else {
                                                                                                                    i = R.id.mobile_reports_node_message_preview;
                                                                                                                }
                                                                                                            }
                                                                                                        } else {
                                                                                                            i = R.id.mobile_reports_node_info_box;
                                                                                                        }
                                                                                                    }
                                                                                                } else {
                                                                                                    i = R.id.mobile_reports_node_directory_channel_preview_title;
                                                                                                }
                                                                                            } else {
                                                                                                i = R.id.mobile_reports_node_directory_channel_preview;
                                                                                            }
                                                                                        } else {
                                                                                            i = R.id.mobile_reports_node_child_list;
                                                                                        }
                                                                                    } else {
                                                                                        i = R.id.mobile_reports_node_channel_preview;
                                                                                    }
                                                                                } else {
                                                                                    i = R.id.mobile_reports_node_breadcrumbs;
                                                                                }
                                                                            } else {
                                                                                i = R.id.mobile_reports_node_bottom_button;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        throw new NullPointerException("Missing required view with ID: ".concat(findViewById4.getResources().getResourceName(i3)));
                                                    }
                                                    i = R.id.mobile_reports_node_block_user;
                                                } else {
                                                    i = R.id.mobile_reports_multiselect;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    throw new NullPointerException("Missing required view with ID: ".concat(findViewById3.getResources().getResourceName(i2)));
                }
                i = R.id.mobile_reports_message_preview;
            } else {
                i = R.id.mobile_reports_directory_channel_preview;
            }
        } else {
            i = R.id.mobile_reports_channel_preview;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
