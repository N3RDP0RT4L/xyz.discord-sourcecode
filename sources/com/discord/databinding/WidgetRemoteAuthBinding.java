package com.discord.databinding;

import android.view.View;
import android.widget.RelativeLayout;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import b.a.i.q5;
import b.a.i.r5;
import b.a.i.s5;
import com.discord.app.AppViewFlipper;
/* loaded from: classes.dex */
public final class WidgetRemoteAuthBinding implements ViewBinding {
    @NonNull
    public final RelativeLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final s5 f2496b;
    @NonNull
    public final q5 c;
    @NonNull
    public final r5 d;
    @NonNull
    public final AppViewFlipper e;

    public WidgetRemoteAuthBinding(@NonNull RelativeLayout relativeLayout, @NonNull s5 s5Var, @NonNull q5 q5Var, @NonNull r5 r5Var, @NonNull AppViewFlipper appViewFlipper) {
        this.a = relativeLayout;
        this.f2496b = s5Var;
        this.c = q5Var;
        this.d = r5Var;
        this.e = appViewFlipper;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
