package com.discord.databinding;

import android.view.View;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import androidx.viewbinding.ViewBinding;
import com.discord.views.CheckedSetting;
import com.discord.widgets.servers.NotificationMuteSettingsView;
/* loaded from: classes.dex */
public final class WidgetChannelNotificationSettingsBinding implements ViewBinding {
    @NonNull
    public final CoordinatorLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final LinearLayout f2245b;
    @NonNull
    public final CheckedSetting c;
    @NonNull
    public final CheckedSetting d;
    @NonNull
    public final CheckedSetting e;
    @NonNull
    public final View f;
    @NonNull
    public final NotificationMuteSettingsView g;

    public WidgetChannelNotificationSettingsBinding(@NonNull CoordinatorLayout coordinatorLayout, @NonNull LinearLayout linearLayout, @NonNull LinearLayout linearLayout2, @NonNull CheckedSetting checkedSetting, @NonNull CheckedSetting checkedSetting2, @NonNull CheckedSetting checkedSetting3, @NonNull View view, @NonNull NotificationMuteSettingsView notificationMuteSettingsView, @NonNull NestedScrollView nestedScrollView) {
        this.a = coordinatorLayout;
        this.f2245b = linearLayout;
        this.c = checkedSetting;
        this.d = checkedSetting2;
        this.e = checkedSetting3;
        this.f = view;
        this.g = notificationMuteSettingsView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
