package com.discord.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewbinding.ViewBinding;
import xyz.discord.R;
/* loaded from: classes.dex */
public final class WidgetChannelMembersListItemAddOrLeaveBinding implements ViewBinding {
    @NonNull
    public final LinearLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ImageView f2241b;
    @NonNull
    public final TextView c;

    public WidgetChannelMembersListItemAddOrLeaveBinding(@NonNull LinearLayout linearLayout, @NonNull ImageView imageView, @NonNull TextView textView) {
        this.a = linearLayout;
        this.f2241b = imageView;
        this.c = textView;
    }

    @NonNull
    public static WidgetChannelMembersListItemAddOrLeaveBinding a(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, boolean z2) {
        View inflate = layoutInflater.inflate(R.layout.widget_channel_members_list_item_add_or_leave, viewGroup, false);
        if (z2) {
            viewGroup.addView(inflate);
        }
        int i = R.id.channel_members_list_item_add_or_leave;
        ImageView imageView = (ImageView) inflate.findViewById(R.id.channel_members_list_item_add_or_leave);
        if (imageView != null) {
            i = R.id.channel_members_list_item_add_or_leave_title;
            TextView textView = (TextView) inflate.findViewById(R.id.channel_members_list_item_add_or_leave_title);
            if (textView != null) {
                return new WidgetChannelMembersListItemAddOrLeaveBinding((LinearLayout) inflate, imageView, textView);
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
