package com.discord.databinding;

import android.view.View;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import b.a.i.n;
/* loaded from: classes.dex */
public final class StickerCategoryItemRecentBinding implements ViewBinding {
    @NonNull
    public final FrameLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final n f2133b;

    public StickerCategoryItemRecentBinding(@NonNull FrameLayout frameLayout, @NonNull n nVar) {
        this.a = frameLayout;
        this.f2133b = nVar;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
