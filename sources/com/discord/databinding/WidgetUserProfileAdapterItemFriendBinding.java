package com.discord.databinding;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import b.a.i.a6;
import com.discord.utilities.view.text.SimpleDraweeSpanTextView;
import com.discord.views.StatusView;
import com.facebook.drawee.view.SimpleDraweeView;
/* loaded from: classes.dex */
public final class WidgetUserProfileAdapterItemFriendBinding implements ViewBinding {
    @NonNull
    public final RelativeLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final a6 f2663b;
    @NonNull
    public final a6 c;
    @NonNull
    public final a6 d;
    @NonNull
    public final a6 e;
    @NonNull
    public final SimpleDraweeView f;
    @NonNull
    public final StatusView g;
    @NonNull
    public final SimpleDraweeSpanTextView h;
    @NonNull
    public final TextView i;

    public WidgetUserProfileAdapterItemFriendBinding(@NonNull RelativeLayout relativeLayout, @NonNull a6 a6Var, @NonNull a6 a6Var2, @NonNull a6 a6Var3, @NonNull a6 a6Var4, @NonNull SimpleDraweeView simpleDraweeView, @NonNull LinearLayout linearLayout, @NonNull StatusView statusView, @NonNull SimpleDraweeSpanTextView simpleDraweeSpanTextView, @NonNull TextView textView) {
        this.a = relativeLayout;
        this.f2663b = a6Var;
        this.c = a6Var2;
        this.d = a6Var3;
        this.e = a6Var4;
        this.f = simpleDraweeView;
        this.g = statusView;
        this.h = simpleDraweeSpanTextView;
        this.i = textView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
