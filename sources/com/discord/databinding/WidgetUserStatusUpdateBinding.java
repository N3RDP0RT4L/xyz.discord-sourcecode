package com.discord.databinding;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.viewbinding.ViewBinding;
import b.a.i.a4;
import com.discord.widgets.user.profile.UserStatusPresenceCustomView;
/* loaded from: classes.dex */
public final class WidgetUserStatusUpdateBinding implements ViewBinding {
    @NonNull
    public final NestedScrollView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final UserStatusPresenceCustomView f2671b;
    @NonNull
    public final a4 c;
    @NonNull
    public final a4 d;
    @NonNull
    public final a4 e;
    @NonNull
    public final a4 f;

    public WidgetUserStatusUpdateBinding(@NonNull NestedScrollView nestedScrollView, @NonNull TextView textView, @NonNull UserStatusPresenceCustomView userStatusPresenceCustomView, @NonNull a4 a4Var, @NonNull a4 a4Var2, @NonNull a4 a4Var3, @NonNull a4 a4Var4) {
        this.a = nestedScrollView;
        this.f2671b = userStatusPresenceCustomView;
        this.c = a4Var;
        this.d = a4Var2;
        this.e = a4Var3;
        this.f = a4Var4;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
