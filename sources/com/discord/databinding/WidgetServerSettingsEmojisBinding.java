package com.discord.databinding;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
import com.discord.app.AppViewFlipper;
/* loaded from: classes.dex */
public final class WidgetServerSettingsEmojisBinding implements ViewBinding {
    @NonNull
    public final CoordinatorLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final RecyclerView f2535b;
    @NonNull
    public final AppViewFlipper c;

    public WidgetServerSettingsEmojisBinding(@NonNull CoordinatorLayout coordinatorLayout, @NonNull RecyclerView recyclerView, @NonNull AppViewFlipper appViewFlipper) {
        this.a = coordinatorLayout;
        this.f2535b = recyclerView;
        this.c = appViewFlipper;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
