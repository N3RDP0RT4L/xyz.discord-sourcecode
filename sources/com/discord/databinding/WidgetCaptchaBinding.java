package com.discord.databinding;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewbinding.ViewBinding;
import com.discord.views.ScreenTitleView;
import com.google.android.material.button.MaterialButton;
/* loaded from: classes.dex */
public final class WidgetCaptchaBinding implements ViewBinding {
    @NonNull
    public final CoordinatorLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f2233b;
    @NonNull
    public final MaterialButton c;
    @NonNull
    public final ScreenTitleView d;

    public WidgetCaptchaBinding(@NonNull CoordinatorLayout coordinatorLayout, @NonNull TextView textView, @NonNull MaterialButton materialButton, @NonNull ScreenTitleView screenTitleView) {
        this.a = coordinatorLayout;
        this.f2233b = textView;
        this.c = materialButton;
        this.d = screenTitleView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
