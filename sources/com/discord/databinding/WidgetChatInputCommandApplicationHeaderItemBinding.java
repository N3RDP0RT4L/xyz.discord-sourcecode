package com.discord.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import xyz.discord.R;
/* loaded from: classes.dex */
public final class WidgetChatInputCommandApplicationHeaderItemBinding implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ImageView f2288b;
    @NonNull
    public final TextView c;

    public WidgetChatInputCommandApplicationHeaderItemBinding(@NonNull ConstraintLayout constraintLayout, @NonNull ImageView imageView, @NonNull TextView textView) {
        this.a = constraintLayout;
        this.f2288b = imageView;
        this.c = textView;
    }

    @NonNull
    public static WidgetChatInputCommandApplicationHeaderItemBinding a(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, boolean z2) {
        View inflate = layoutInflater.inflate(R.layout.widget_chat_input_command_application_header_item, viewGroup, false);
        if (z2) {
            viewGroup.addView(inflate);
        }
        int i = R.id.chat_input_application_avatar;
        ImageView imageView = (ImageView) inflate.findViewById(R.id.chat_input_application_avatar);
        if (imageView != null) {
            i = R.id.chat_input_application_name;
            TextView textView = (TextView) inflate.findViewById(R.id.chat_input_application_name);
            if (textView != null) {
                return new WidgetChatInputCommandApplicationHeaderItemBinding((ConstraintLayout) inflate, imageView, textView);
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
