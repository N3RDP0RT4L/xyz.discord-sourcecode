package com.discord.databinding;

import android.view.View;
import android.widget.ScrollView;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewbinding.ViewBinding;
import b.a.i.f3;
import b.a.i.g3;
import b.a.i.h3;
import b.a.i.j3;
import b.a.i.k3;
import b.a.i.u5;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
/* loaded from: classes.dex */
public final class WidgetServerSettingsOverviewBinding implements ViewBinding {
    @NonNull
    public final CoordinatorLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final u5 f2556b;
    @NonNull
    public final f3 c;
    @NonNull
    public final g3 d;
    @NonNull
    public final FloatingActionButton e;
    @NonNull
    public final ScrollView f;
    @NonNull
    public final h3 g;
    @NonNull
    public final j3 h;
    @NonNull
    public final k3 i;

    public WidgetServerSettingsOverviewBinding(@NonNull CoordinatorLayout coordinatorLayout, @NonNull u5 u5Var, @NonNull f3 f3Var, @NonNull g3 g3Var, @NonNull FloatingActionButton floatingActionButton, @NonNull ScrollView scrollView, @NonNull h3 h3Var, @NonNull j3 j3Var, @NonNull k3 k3Var) {
        this.a = coordinatorLayout;
        this.f2556b = u5Var;
        this.c = f3Var;
        this.d = g3Var;
        this.e = floatingActionButton;
        this.f = scrollView;
        this.g = h3Var;
        this.h = j3Var;
        this.i = k3Var;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
