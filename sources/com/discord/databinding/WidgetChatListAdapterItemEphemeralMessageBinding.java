package com.discord.databinding;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.viewbinding.ViewBinding;
/* loaded from: classes.dex */
public final class WidgetChatListAdapterItemEphemeralMessageBinding implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final View f2299b;
    @NonNull
    public final View c;
    @NonNull
    public final ImageView d;
    @NonNull
    public final TextView e;

    public WidgetChatListAdapterItemEphemeralMessageBinding(@NonNull ConstraintLayout constraintLayout, @NonNull View view, @NonNull View view2, @NonNull ImageView imageView, @NonNull TextView textView, @NonNull ImageView imageView2, @NonNull Guideline guideline) {
        this.a = constraintLayout;
        this.f2299b = view;
        this.c = view2;
        this.d = imageView;
        this.e = textView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
