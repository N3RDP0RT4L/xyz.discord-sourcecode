package com.discord.databinding;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
/* loaded from: classes.dex */
public final class ViewMobileReportsBreadcrumbsBinding implements ViewBinding {
    @NonNull
    public final View a;

    public ViewMobileReportsBreadcrumbsBinding(@NonNull View view) {
        this.a = view;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
