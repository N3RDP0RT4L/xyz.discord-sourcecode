package com.discord.databinding;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
/* loaded from: classes.dex */
public final class WidgetGuildScheduledEventListBottomSheetBinding implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f2423b;
    @NonNull
    public final ConstraintLayout c;
    @NonNull
    public final RecyclerView d;
    @NonNull
    public final TextView e;

    public WidgetGuildScheduledEventListBottomSheetBinding(@NonNull ConstraintLayout constraintLayout, @NonNull TextView textView, @NonNull ConstraintLayout constraintLayout2, @NonNull ImageView imageView, @NonNull TextView textView2, @NonNull TextView textView3, @NonNull RecyclerView recyclerView, @NonNull ConstraintLayout constraintLayout3, @NonNull TextView textView4) {
        this.a = constraintLayout;
        this.f2423b = textView;
        this.c = constraintLayout2;
        this.d = recyclerView;
        this.e = textView4;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
