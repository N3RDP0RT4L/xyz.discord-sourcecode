package com.discord.databinding;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.viewbinding.ViewBinding;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.views.sticker.StickerView;
/* loaded from: classes.dex */
public final class WidgetChatListAdapterItemSystemBinding implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final AppCompatImageView f2318b;
    @NonNull
    public final ImageView c;
    @NonNull
    public final LinkifiedTextView d;
    @NonNull
    public final TextView e;
    @NonNull
    public final LinearLayout f;
    @NonNull
    public final StickerView g;

    public WidgetChatListAdapterItemSystemBinding(@NonNull ConstraintLayout constraintLayout, @NonNull AppCompatImageView appCompatImageView, @NonNull ImageView imageView, @NonNull LinkifiedTextView linkifiedTextView, @NonNull TextView textView, @NonNull LinearLayout linearLayout, @NonNull StickerView stickerView, @NonNull Guideline guideline) {
        this.a = constraintLayout;
        this.f2318b = appCompatImageView;
        this.c = imageView;
        this.d = linkifiedTextView;
        this.e = textView;
        this.f = linearLayout;
        this.g = stickerView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
