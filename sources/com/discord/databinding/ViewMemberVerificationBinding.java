package com.discord.databinding;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.button.MaterialButton;
/* loaded from: classes.dex */
public final class ViewMemberVerificationBinding implements ViewBinding {
    @NonNull
    public final View a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f2185b;
    @NonNull
    public final MaterialButton c;
    @NonNull
    public final ImageView d;
    @NonNull
    public final TextView e;

    public ViewMemberVerificationBinding(@NonNull View view, @NonNull TextView textView, @NonNull MaterialButton materialButton, @NonNull ImageView imageView, @NonNull TextView textView2) {
        this.a = view;
        this.f2185b = textView;
        this.c = materialButton;
        this.d = imageView;
        this.e = textView2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
