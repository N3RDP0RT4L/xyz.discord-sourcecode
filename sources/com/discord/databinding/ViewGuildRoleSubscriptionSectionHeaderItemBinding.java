package com.discord.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewbinding.ViewBinding;
import xyz.discord.R;
/* loaded from: classes.dex */
public final class ViewGuildRoleSubscriptionSectionHeaderItemBinding implements ViewBinding {
    @NonNull
    public final LinearLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f2183b;
    @NonNull
    public final TextView c;

    public ViewGuildRoleSubscriptionSectionHeaderItemBinding(@NonNull LinearLayout linearLayout, @NonNull TextView textView, @NonNull TextView textView2) {
        this.a = linearLayout;
        this.f2183b = textView;
        this.c = textView2;
    }

    @NonNull
    public static ViewGuildRoleSubscriptionSectionHeaderItemBinding a(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, boolean z2) {
        View inflate = layoutInflater.inflate(R.layout.view_guild_role_subscription_section_header_item, viewGroup, false);
        if (z2) {
            viewGroup.addView(inflate);
        }
        int i = R.id.guild_role_subscription_section_description;
        TextView textView = (TextView) inflate.findViewById(R.id.guild_role_subscription_section_description);
        if (textView != null) {
            i = R.id.guild_role_subscription_section_header;
            TextView textView2 = (TextView) inflate.findViewById(R.id.guild_role_subscription_section_header);
            if (textView2 != null) {
                return new ViewGuildRoleSubscriptionSectionHeaderItemBinding((LinearLayout) inflate, textView, textView2);
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
