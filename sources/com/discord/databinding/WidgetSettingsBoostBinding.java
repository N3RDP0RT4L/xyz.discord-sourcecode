package com.discord.databinding;

import android.view.View;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
import b.a.i.p3;
import com.discord.app.AppViewFlipper;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.views.guildboost.GuildBoostMarketingView;
import com.discord.views.guildboost.GuildBoostSubscriptionUpsellView;
import com.google.android.material.button.MaterialButton;
/* loaded from: classes.dex */
public final class WidgetSettingsBoostBinding implements ViewBinding {
    @NonNull
    public final CoordinatorLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final p3 f2586b;
    @NonNull
    public final AppViewFlipper c;
    @NonNull
    public final GuildBoostMarketingView d;
    @NonNull
    public final RecyclerView e;
    @NonNull
    public final MaterialButton f;
    @NonNull
    public final RecyclerView g;
    @NonNull
    public final LinkifiedTextView h;
    @NonNull
    public final LinearLayout i;
    @NonNull
    public final GuildBoostSubscriptionUpsellView j;

    public WidgetSettingsBoostBinding(@NonNull CoordinatorLayout coordinatorLayout, @NonNull p3 p3Var, @NonNull AppViewFlipper appViewFlipper, @NonNull GuildBoostMarketingView guildBoostMarketingView, @NonNull RecyclerView recyclerView, @NonNull MaterialButton materialButton, @NonNull RecyclerView recyclerView2, @NonNull LinkifiedTextView linkifiedTextView, @NonNull LinearLayout linearLayout, @NonNull GuildBoostSubscriptionUpsellView guildBoostSubscriptionUpsellView) {
        this.a = coordinatorLayout;
        this.f2586b = p3Var;
        this.c = appViewFlipper;
        this.d = guildBoostMarketingView;
        this.e = recyclerView;
        this.f = materialButton;
        this.g = recyclerView2;
        this.h = linkifiedTextView;
        this.i = linearLayout;
        this.j = guildBoostSubscriptionUpsellView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
