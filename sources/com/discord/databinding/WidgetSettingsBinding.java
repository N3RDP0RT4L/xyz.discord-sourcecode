package com.discord.databinding;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewbinding.ViewBinding;
import com.discord.views.StatusView;
import com.discord.widgets.user.profile.UserProfileHeaderView;
import com.google.android.material.button.MaterialButton;
/* loaded from: classes.dex */
public final class WidgetSettingsBinding implements ViewBinding {
    @NonNull
    public final TextView A;
    @NonNull
    public final TextView B;
    @NonNull
    public final LinearLayout C;
    @NonNull
    public final TextView D;
    @NonNull
    public final TextView E;
    @NonNull
    public final StatusView F;
    @NonNull
    public final TextView G;
    @NonNull
    public final TextView H;
    @NonNull
    public final TextView I;
    @NonNull
    public final TextView J;
    @NonNull
    public final UserProfileHeaderView K;
    @NonNull
    public final TextView L;
    @NonNull
    public final CoordinatorLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f2580b;
    @NonNull
    public final TextView c;
    @NonNull
    public final TextView d;
    @NonNull
    public final TextView e;
    @NonNull
    public final TextView f;
    @NonNull
    public final TextView g;
    @NonNull
    public final TextView h;
    @NonNull
    public final TextView i;
    @NonNull
    public final TextView j;
    @NonNull
    public final TextView k;
    @NonNull
    public final TextView l;
    @NonNull
    public final TextView m;
    @NonNull
    public final View n;
    @NonNull
    public final TextView o;
    @NonNull
    public final TextView p;
    @NonNull
    public final TextView q;
    @NonNull
    public final TextView r;
    @NonNull

    /* renamed from: s  reason: collision with root package name */
    public final LinearLayout f2581s;
    @NonNull
    public final TextView t;
    @NonNull
    public final LinearLayout u;
    @NonNull
    public final LinearLayout v;
    @NonNull
    public final AppCompatImageView w;
    @NonNull

    /* renamed from: x  reason: collision with root package name */
    public final MaterialButton f2582x;
    @NonNull

    /* renamed from: y  reason: collision with root package name */
    public final TextView f2583y;
    @NonNull

    /* renamed from: z  reason: collision with root package name */
    public final TextView f2584z;

    public WidgetSettingsBinding(@NonNull CoordinatorLayout coordinatorLayout, @NonNull TextView textView, @NonNull TextView textView2, @NonNull TextView textView3, @NonNull TextView textView4, @NonNull TextView textView5, @NonNull TextView textView6, @NonNull TextView textView7, @NonNull TextView textView8, @NonNull TextView textView9, @NonNull TextView textView10, @NonNull TextView textView11, @NonNull TextView textView12, @NonNull View view, @NonNull TextView textView13, @NonNull TextView textView14, @NonNull TextView textView15, @NonNull TextView textView16, @NonNull TextView textView17, @NonNull LinearLayout linearLayout, @NonNull TextView textView18, @NonNull LinearLayout linearLayout2, @NonNull LinearLayout linearLayout3, @NonNull AppCompatImageView appCompatImageView, @NonNull MaterialButton materialButton, @NonNull TextView textView19, @NonNull TextView textView20, @NonNull TextView textView21, @NonNull TextView textView22, @NonNull LinearLayout linearLayout4, @NonNull TextView textView23, @NonNull TextView textView24, @NonNull TextView textView25, @NonNull StatusView statusView, @NonNull TextView textView26, @NonNull TextView textView27, @NonNull TextView textView28, @NonNull TextView textView29, @NonNull UserProfileHeaderView userProfileHeaderView, @NonNull TextView textView30) {
        this.a = coordinatorLayout;
        this.f2580b = textView;
        this.c = textView2;
        this.d = textView3;
        this.e = textView4;
        this.f = textView5;
        this.g = textView6;
        this.h = textView7;
        this.i = textView8;
        this.j = textView9;
        this.k = textView10;
        this.l = textView11;
        this.m = textView12;
        this.n = view;
        this.o = textView13;
        this.p = textView14;
        this.q = textView15;
        this.r = textView17;
        this.f2581s = linearLayout;
        this.t = textView18;
        this.u = linearLayout2;
        this.v = linearLayout3;
        this.w = appCompatImageView;
        this.f2582x = materialButton;
        this.f2583y = textView19;
        this.f2584z = textView20;
        this.A = textView21;
        this.B = textView22;
        this.C = linearLayout4;
        this.D = textView24;
        this.E = textView25;
        this.F = statusView;
        this.G = textView26;
        this.H = textView27;
        this.I = textView28;
        this.J = textView29;
        this.K = userProfileHeaderView;
        this.L = textView30;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
