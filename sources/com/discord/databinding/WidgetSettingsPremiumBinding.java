package com.discord.databinding;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewbinding.ViewBinding;
import b.a.i.d0;
import b.a.i.q0;
import b.a.i.v2;
import b.a.i.v5;
import b.a.i.w2;
import b.a.i.x2;
import com.discord.app.AppViewFlipper;
import com.discord.views.ActiveSubscriptionView;
import com.google.android.material.button.MaterialButton;
/* loaded from: classes.dex */
public final class WidgetSettingsPremiumBinding implements ViewBinding {
    @NonNull
    public final CoordinatorLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final d0 f2607b;
    @NonNull
    public final q0 c;
    @NonNull
    public final v2 d;
    @NonNull
    public final ActiveSubscriptionView e;
    @NonNull
    public final ActiveSubscriptionView f;
    @NonNull
    public final LinearLayout g;
    @NonNull
    public final TextView h;
    @NonNull
    public final TextView i;
    @NonNull
    public final TextView j;
    @NonNull
    public final v5 k;
    @NonNull
    public final MaterialButton l;
    @NonNull
    public final ScrollView m;
    @NonNull
    public final Button n;
    @NonNull
    public final CardView o;
    @NonNull
    public final TextView p;
    @NonNull
    public final LinearLayout q;
    @NonNull
    public final TextView r;
    @NonNull

    /* renamed from: s  reason: collision with root package name */
    public final AppViewFlipper f2608s;
    @NonNull
    public final w2 t;
    @NonNull
    public final x2 u;

    public WidgetSettingsPremiumBinding(@NonNull CoordinatorLayout coordinatorLayout, @NonNull d0 d0Var, @NonNull q0 q0Var, @NonNull v2 v2Var, @NonNull ActiveSubscriptionView activeSubscriptionView, @NonNull ActiveSubscriptionView activeSubscriptionView2, @NonNull LinearLayout linearLayout, @NonNull TextView textView, @NonNull TextView textView2, @NonNull TextView textView3, @NonNull v5 v5Var, @NonNull MaterialButton materialButton, @NonNull ScrollView scrollView, @NonNull Button button, @NonNull CardView cardView, @NonNull TextView textView4, @NonNull LinearLayout linearLayout2, @NonNull TextView textView5, @NonNull AppViewFlipper appViewFlipper, @NonNull w2 w2Var, @NonNull x2 x2Var) {
        this.a = coordinatorLayout;
        this.f2607b = d0Var;
        this.c = q0Var;
        this.d = v2Var;
        this.e = activeSubscriptionView;
        this.f = activeSubscriptionView2;
        this.g = linearLayout;
        this.h = textView;
        this.i = textView2;
        this.j = textView3;
        this.k = v5Var;
        this.l = materialButton;
        this.m = scrollView;
        this.n = button;
        this.o = cardView;
        this.p = textView4;
        this.q = linearLayout2;
        this.r = textView5;
        this.f2608s = appViewFlipper;
        this.t = w2Var;
        this.u = x2Var;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
