package com.discord.databinding;

import android.view.View;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.chip.Chip;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
/* loaded from: classes.dex */
public final class WidgetQrScannerBinding implements ViewBinding {
    @NonNull
    public final FrameLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ZXingScannerView f2492b;
    @NonNull
    public final Chip c;

    public WidgetQrScannerBinding(@NonNull FrameLayout frameLayout, @NonNull ZXingScannerView zXingScannerView, @NonNull Chip chip) {
        this.a = frameLayout;
        this.f2492b = zXingScannerView;
        this.c = chip;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
