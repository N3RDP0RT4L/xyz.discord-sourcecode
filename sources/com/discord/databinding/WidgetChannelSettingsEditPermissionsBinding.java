package com.discord.databinding;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewbinding.ViewBinding;
import com.discord.views.TernaryCheckBox;
import com.discord.views.user.SettingsMemberView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
/* loaded from: classes.dex */
public final class WidgetChannelSettingsEditPermissionsBinding implements ViewBinding {
    @NonNull
    public final TernaryCheckBox A;
    @NonNull
    public final TernaryCheckBox B;
    @NonNull
    public final TernaryCheckBox C;
    @NonNull
    public final TernaryCheckBox D;
    @NonNull
    public final TernaryCheckBox E;
    @NonNull
    public final TextView F;
    @NonNull
    public final LinearLayout G;
    @NonNull
    public final FloatingActionButton H;
    @NonNull
    public final LinearLayout I;
    @NonNull
    public final TextView J;
    @NonNull
    public final LinearLayout K;
    @NonNull
    public final LinearLayout L;
    @NonNull
    public final SettingsMemberView M;
    @NonNull
    public final TextView N;
    @NonNull
    public final CoordinatorLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TernaryCheckBox f2256b;
    @NonNull
    public final TernaryCheckBox c;
    @NonNull
    public final TernaryCheckBox d;
    @NonNull
    public final TernaryCheckBox e;
    @NonNull
    public final TernaryCheckBox f;
    @NonNull
    public final TernaryCheckBox g;
    @NonNull
    public final TernaryCheckBox h;
    @NonNull
    public final TernaryCheckBox i;
    @NonNull
    public final TernaryCheckBox j;
    @NonNull
    public final TernaryCheckBox k;
    @NonNull
    public final TernaryCheckBox l;
    @NonNull
    public final TernaryCheckBox m;
    @NonNull
    public final TernaryCheckBox n;
    @NonNull
    public final TernaryCheckBox o;
    @NonNull
    public final TernaryCheckBox p;
    @NonNull
    public final TernaryCheckBox q;
    @NonNull
    public final TernaryCheckBox r;
    @NonNull

    /* renamed from: s  reason: collision with root package name */
    public final TernaryCheckBox f2257s;
    @NonNull
    public final TernaryCheckBox t;
    @NonNull
    public final TernaryCheckBox u;
    @NonNull
    public final TernaryCheckBox v;
    @NonNull
    public final TernaryCheckBox w;
    @NonNull

    /* renamed from: x  reason: collision with root package name */
    public final TernaryCheckBox f2258x;
    @NonNull

    /* renamed from: y  reason: collision with root package name */
    public final TernaryCheckBox f2259y;
    @NonNull

    /* renamed from: z  reason: collision with root package name */
    public final TernaryCheckBox f2260z;

    public WidgetChannelSettingsEditPermissionsBinding(@NonNull CoordinatorLayout coordinatorLayout, @NonNull TernaryCheckBox ternaryCheckBox, @NonNull TernaryCheckBox ternaryCheckBox2, @NonNull TernaryCheckBox ternaryCheckBox3, @NonNull TernaryCheckBox ternaryCheckBox4, @NonNull TernaryCheckBox ternaryCheckBox5, @NonNull TernaryCheckBox ternaryCheckBox6, @NonNull TernaryCheckBox ternaryCheckBox7, @NonNull TernaryCheckBox ternaryCheckBox8, @NonNull TernaryCheckBox ternaryCheckBox9, @NonNull TernaryCheckBox ternaryCheckBox10, @NonNull TernaryCheckBox ternaryCheckBox11, @NonNull TernaryCheckBox ternaryCheckBox12, @NonNull TernaryCheckBox ternaryCheckBox13, @NonNull TernaryCheckBox ternaryCheckBox14, @NonNull TernaryCheckBox ternaryCheckBox15, @NonNull TernaryCheckBox ternaryCheckBox16, @NonNull TernaryCheckBox ternaryCheckBox17, @NonNull TernaryCheckBox ternaryCheckBox18, @NonNull TernaryCheckBox ternaryCheckBox19, @NonNull TernaryCheckBox ternaryCheckBox20, @NonNull TernaryCheckBox ternaryCheckBox21, @NonNull TernaryCheckBox ternaryCheckBox22, @NonNull TernaryCheckBox ternaryCheckBox23, @NonNull TernaryCheckBox ternaryCheckBox24, @NonNull TernaryCheckBox ternaryCheckBox25, @NonNull TernaryCheckBox ternaryCheckBox26, @NonNull TernaryCheckBox ternaryCheckBox27, @NonNull TernaryCheckBox ternaryCheckBox28, @NonNull TernaryCheckBox ternaryCheckBox29, @NonNull TernaryCheckBox ternaryCheckBox30, @NonNull TextView textView, @NonNull LinearLayout linearLayout, @NonNull FloatingActionButton floatingActionButton, @NonNull LinearLayout linearLayout2, @NonNull TextView textView2, @NonNull LinearLayout linearLayout3, @NonNull LinearLayout linearLayout4, @NonNull SettingsMemberView settingsMemberView, @NonNull TextView textView3) {
        this.a = coordinatorLayout;
        this.f2256b = ternaryCheckBox;
        this.c = ternaryCheckBox2;
        this.d = ternaryCheckBox3;
        this.e = ternaryCheckBox4;
        this.f = ternaryCheckBox5;
        this.g = ternaryCheckBox6;
        this.h = ternaryCheckBox7;
        this.i = ternaryCheckBox8;
        this.j = ternaryCheckBox9;
        this.k = ternaryCheckBox10;
        this.l = ternaryCheckBox11;
        this.m = ternaryCheckBox12;
        this.n = ternaryCheckBox13;
        this.o = ternaryCheckBox14;
        this.p = ternaryCheckBox15;
        this.q = ternaryCheckBox16;
        this.r = ternaryCheckBox17;
        this.f2257s = ternaryCheckBox18;
        this.t = ternaryCheckBox19;
        this.u = ternaryCheckBox20;
        this.v = ternaryCheckBox21;
        this.w = ternaryCheckBox22;
        this.f2258x = ternaryCheckBox23;
        this.f2259y = ternaryCheckBox24;
        this.f2260z = ternaryCheckBox25;
        this.A = ternaryCheckBox26;
        this.B = ternaryCheckBox27;
        this.C = ternaryCheckBox28;
        this.D = ternaryCheckBox29;
        this.E = ternaryCheckBox30;
        this.F = textView;
        this.G = linearLayout;
        this.H = floatingActionButton;
        this.I = linearLayout2;
        this.J = textView2;
        this.K = linearLayout3;
        this.L = linearLayout4;
        this.M = settingsMemberView;
        this.N = textView3;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
