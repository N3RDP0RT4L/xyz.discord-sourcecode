package com.discord.databinding;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.viewbinding.ViewBinding;
import com.discord.views.guildboost.GuildBoostProgressView;
import com.facebook.drawee.view.SimpleDraweeView;
/* loaded from: classes.dex */
public final class ViewSettingsBoostedSampleGuildBinding implements ViewBinding {
    @NonNull
    public final CardView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f2198b;
    @NonNull
    public final SimpleDraweeView c;
    @NonNull
    public final TextView d;
    @NonNull
    public final GuildBoostProgressView e;

    public ViewSettingsBoostedSampleGuildBinding(@NonNull CardView cardView, @NonNull TextView textView, @NonNull SimpleDraweeView simpleDraweeView, @NonNull TextView textView2, @NonNull GuildBoostProgressView guildBoostProgressView) {
        this.a = cardView;
        this.f2198b = textView;
        this.c = simpleDraweeView;
        this.d = textView2;
        this.e = guildBoostProgressView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
