package com.discord.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewbinding.ViewBinding;
import com.discord.widgets.guildscheduledevent.GuildEventPromptView;
import xyz.discord.R;
/* loaded from: classes.dex */
public final class GuildEventPromptListViewBinding implements ViewBinding {
    @NonNull
    public final LinearLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final GuildEventPromptView f2100b;
    @NonNull
    public final GuildEventPromptView c;
    @NonNull
    public final GuildEventPromptView d;

    public GuildEventPromptListViewBinding(@NonNull LinearLayout linearLayout, @NonNull GuildEventPromptView guildEventPromptView, @NonNull GuildEventPromptView guildEventPromptView2, @NonNull GuildEventPromptView guildEventPromptView3) {
        this.a = linearLayout;
        this.f2100b = guildEventPromptView;
        this.c = guildEventPromptView2;
        this.d = guildEventPromptView3;
    }

    @NonNull
    public static GuildEventPromptListViewBinding a(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, boolean z2) {
        View inflate = layoutInflater.inflate(R.layout.guild_event_prompt_list_view, viewGroup, false);
        if (z2) {
            viewGroup.addView(inflate);
        }
        int i = R.id.create_event_button;
        GuildEventPromptView guildEventPromptView = (GuildEventPromptView) inflate.findViewById(R.id.create_event_button);
        if (guildEventPromptView != null) {
            i = R.id.start_event_button;
            GuildEventPromptView guildEventPromptView2 = (GuildEventPromptView) inflate.findViewById(R.id.start_event_button);
            if (guildEventPromptView2 != null) {
                i = R.id.start_stage_button;
                GuildEventPromptView guildEventPromptView3 = (GuildEventPromptView) inflate.findViewById(R.id.start_stage_button);
                if (guildEventPromptView3 != null) {
                    return new GuildEventPromptListViewBinding((LinearLayout) inflate, guildEventPromptView, guildEventPromptView2, guildEventPromptView3);
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
