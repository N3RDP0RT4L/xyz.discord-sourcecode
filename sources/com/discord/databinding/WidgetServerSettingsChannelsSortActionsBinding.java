package com.discord.databinding;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.viewbinding.ViewBinding;
/* loaded from: classes.dex */
public final class WidgetServerSettingsChannelsSortActionsBinding implements ViewBinding {
    @NonNull
    public final NestedScrollView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f2527b;
    @NonNull
    public final TextView c;
    @NonNull
    public final TextView d;

    public WidgetServerSettingsChannelsSortActionsBinding(@NonNull NestedScrollView nestedScrollView, @NonNull TextView textView, @NonNull TextView textView2, @NonNull TextView textView3, @NonNull TextView textView4) {
        this.a = nestedScrollView;
        this.f2527b = textView2;
        this.c = textView3;
        this.d = textView4;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
