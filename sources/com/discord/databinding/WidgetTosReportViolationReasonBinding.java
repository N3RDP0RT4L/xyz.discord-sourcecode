package com.discord.databinding;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.radiobutton.MaterialRadioButton;
/* loaded from: classes.dex */
public final class WidgetTosReportViolationReasonBinding implements ViewBinding {
    @NonNull
    public final RelativeLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f2649b;
    @NonNull
    public final TextView c;
    @NonNull
    public final MaterialRadioButton d;

    public WidgetTosReportViolationReasonBinding(@NonNull RelativeLayout relativeLayout, @NonNull TextView textView, @NonNull TextView textView2, @NonNull MaterialRadioButton materialRadioButton) {
        this.a = relativeLayout;
        this.f2649b = textView;
        this.c = textView2;
        this.d = materialRadioButton;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
