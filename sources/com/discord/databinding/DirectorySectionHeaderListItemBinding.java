package com.discord.databinding;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
/* loaded from: classes.dex */
public final class DirectorySectionHeaderListItemBinding implements ViewBinding {
    @NonNull
    public final TextView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f2085b;

    public DirectorySectionHeaderListItemBinding(@NonNull TextView textView, @NonNull TextView textView2) {
        this.a = textView;
        this.f2085b = textView2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
