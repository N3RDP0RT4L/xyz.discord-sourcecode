package com.discord.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
import xyz.discord.R;
/* loaded from: classes.dex */
public final class ViewNitroBoostPerksBinding implements ViewBinding {
    @NonNull
    public final CardView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f2188b;
    @NonNull
    public final RecyclerView c;
    @NonNull
    public final RelativeLayout d;
    @NonNull
    public final TextView e;
    @NonNull
    public final TextView f;
    @NonNull
    public final CardView g;

    public ViewNitroBoostPerksBinding(@NonNull CardView cardView, @NonNull LinearLayout linearLayout, @NonNull TextView textView, @NonNull RecyclerView recyclerView, @NonNull RelativeLayout relativeLayout, @NonNull TextView textView2, @NonNull FrameLayout frameLayout, @NonNull TextView textView3, @NonNull CardView cardView2) {
        this.a = cardView;
        this.f2188b = textView;
        this.c = recyclerView;
        this.d = relativeLayout;
        this.e = textView2;
        this.f = textView3;
        this.g = cardView2;
    }

    @NonNull
    public static ViewNitroBoostPerksBinding a(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, boolean z2) {
        View inflate = layoutInflater.inflate(R.layout.view_nitro_boost_perks, viewGroup, false);
        if (z2) {
            viewGroup.addView(inflate);
        }
        int i = R.id.container;
        LinearLayout linearLayout = (LinearLayout) inflate.findViewById(R.id.container);
        if (linearLayout != null) {
            i = R.id.perks_level_contents_header;
            TextView textView = (TextView) inflate.findViewById(R.id.perks_level_contents_header);
            if (textView != null) {
                i = R.id.perks_level_contents_recycler;
                RecyclerView recyclerView = (RecyclerView) inflate.findViewById(R.id.perks_level_contents_recycler);
                if (recyclerView != null) {
                    i = R.id.perks_level_header;
                    RelativeLayout relativeLayout = (RelativeLayout) inflate.findViewById(R.id.perks_level_header);
                    if (relativeLayout != null) {
                        i = R.id.perks_level_header_boosts;
                        TextView textView2 = (TextView) inflate.findViewById(R.id.perks_level_header_boosts);
                        if (textView2 != null) {
                            i = R.id.perks_level_header_boosts_container;
                            FrameLayout frameLayout = (FrameLayout) inflate.findViewById(R.id.perks_level_header_boosts_container);
                            if (frameLayout != null) {
                                i = R.id.perks_level_header_text;
                                TextView textView3 = (TextView) inflate.findViewById(R.id.perks_level_header_text);
                                if (textView3 != null) {
                                    i = R.id.perks_level_header_unlocked;
                                    CardView cardView = (CardView) inflate.findViewById(R.id.perks_level_header_unlocked);
                                    if (cardView != null) {
                                        return new ViewNitroBoostPerksBinding((CardView) inflate, linearLayout, textView, recyclerView, relativeLayout, textView2, frameLayout, textView3, cardView);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
