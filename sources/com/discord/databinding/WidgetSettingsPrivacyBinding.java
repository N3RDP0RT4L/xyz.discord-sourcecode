package com.discord.databinding;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewbinding.ViewBinding;
import com.discord.utilities.dimmer.DimmerView;
import com.discord.views.CheckedSetting;
import com.google.android.material.button.MaterialButton;
/* loaded from: classes.dex */
public final class WidgetSettingsPrivacyBinding implements ViewBinding {
    @NonNull
    public final CoordinatorLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final CheckedSetting f2609b;
    @NonNull
    public final CheckedSetting c;
    @NonNull
    public final TextView d;
    @NonNull
    public final CheckedSetting e;
    @NonNull
    public final CheckedSetting f;
    @NonNull
    public final DimmerView g;
    @NonNull
    public final TextView h;
    @NonNull
    public final CheckedSetting i;
    @NonNull
    public final LinearLayout j;
    @NonNull
    public final LinearLayout k;
    @NonNull
    public final TextView l;
    @NonNull
    public final CheckedSetting m;
    @NonNull
    public final TextView n;
    @NonNull
    public final CheckedSetting o;
    @NonNull
    public final CheckedSetting p;
    @NonNull
    public final CheckedSetting q;
    @NonNull
    public final CheckedSetting r;
    @NonNull

    /* renamed from: s  reason: collision with root package name */
    public final CheckedSetting f2610s;
    @NonNull
    public final CheckedSetting t;
    @NonNull
    public final TextView u;
    @NonNull
    public final CheckedSetting v;
    @NonNull
    public final MaterialButton w;
    @NonNull

    /* renamed from: x  reason: collision with root package name */
    public final TextView f2611x;
    @NonNull

    /* renamed from: y  reason: collision with root package name */
    public final CheckedSetting f2612y;
    @NonNull

    /* renamed from: z  reason: collision with root package name */
    public final CheckedSetting f2613z;

    public WidgetSettingsPrivacyBinding(@NonNull CoordinatorLayout coordinatorLayout, @NonNull CheckedSetting checkedSetting, @NonNull CheckedSetting checkedSetting2, @NonNull TextView textView, @NonNull CheckedSetting checkedSetting3, @NonNull CheckedSetting checkedSetting4, @NonNull DimmerView dimmerView, @NonNull TextView textView2, @NonNull CheckedSetting checkedSetting5, @NonNull LinearLayout linearLayout, @NonNull TextView textView3, @NonNull LinearLayout linearLayout2, @NonNull TextView textView4, @NonNull CheckedSetting checkedSetting6, @NonNull TextView textView5, @NonNull CheckedSetting checkedSetting7, @NonNull CheckedSetting checkedSetting8, @NonNull CheckedSetting checkedSetting9, @NonNull CheckedSetting checkedSetting10, @NonNull CheckedSetting checkedSetting11, @NonNull CheckedSetting checkedSetting12, @NonNull TextView textView6, @NonNull CheckedSetting checkedSetting13, @NonNull MaterialButton materialButton, @NonNull TextView textView7, @NonNull CheckedSetting checkedSetting14, @NonNull CheckedSetting checkedSetting15) {
        this.a = coordinatorLayout;
        this.f2609b = checkedSetting;
        this.c = checkedSetting2;
        this.d = textView;
        this.e = checkedSetting3;
        this.f = checkedSetting4;
        this.g = dimmerView;
        this.h = textView2;
        this.i = checkedSetting5;
        this.j = linearLayout;
        this.k = linearLayout2;
        this.l = textView4;
        this.m = checkedSetting6;
        this.n = textView5;
        this.o = checkedSetting7;
        this.p = checkedSetting8;
        this.q = checkedSetting9;
        this.r = checkedSetting10;
        this.f2610s = checkedSetting11;
        this.t = checkedSetting12;
        this.u = textView6;
        this.v = checkedSetting13;
        this.w = materialButton;
        this.f2611x = textView7;
        this.f2612y = checkedSetting14;
        this.f2613z = checkedSetting15;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
