package com.discord.databinding;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.views.GuildView;
import com.discord.views.guilds.ServerMemberCount;
import com.facebook.drawee.view.SimpleDraweeView;
/* loaded from: classes.dex */
public final class WidgetGuildInviteInfoViewBinding implements ViewBinding {
    @NonNull
    public final View a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final GuildView f2393b;
    @NonNull
    public final SimpleDraweeView c;
    @NonNull
    public final ServerMemberCount d;
    @NonNull
    public final TextView e;
    @NonNull
    public final TextView f;

    public WidgetGuildInviteInfoViewBinding(@NonNull View view, @NonNull GuildView guildView, @NonNull SimpleDraweeView simpleDraweeView, @NonNull ServerMemberCount serverMemberCount, @NonNull TextView textView, @NonNull TextView textView2, @NonNull LinearLayout linearLayout) {
        this.a = view;
        this.f2393b = guildView;
        this.c = simpleDraweeView;
        this.d = serverMemberCount;
        this.e = textView;
        this.f = textView2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
