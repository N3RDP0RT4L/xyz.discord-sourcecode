package com.discord.databinding;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.discord.widgets.servers.guild_role_subscription.payments.GuildRoleSubscriptionTierRevenueListView;
/* loaded from: classes.dex */
public final class ViewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f2177b;
    @NonNull
    public final TextView c;
    @NonNull
    public final TextView d;
    @NonNull
    public final TextView e;
    @NonNull
    public final GuildRoleSubscriptionTierRevenueListView f;

    public ViewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding(@NonNull ConstraintLayout constraintLayout, @NonNull TextView textView, @NonNull TextView textView2, @NonNull TextView textView3, @NonNull TextView textView4, @NonNull GuildRoleSubscriptionTierRevenueListView guildRoleSubscriptionTierRevenueListView) {
        this.a = constraintLayout;
        this.f2177b = textView;
        this.c = textView2;
        this.d = textView3;
        this.e = textView4;
        this.f = guildRoleSubscriptionTierRevenueListView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
