package com.discord.databinding;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
import b.a.i.q1;
import com.google.android.material.button.MaterialButton;
/* loaded from: classes.dex */
public final class WidgetThreadBrowserArchivedBinding implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final q1 f2642b;
    @NonNull
    public final RecyclerView c;
    @NonNull
    public final ConstraintLayout d;
    @NonNull
    public final MaterialButton e;

    public WidgetThreadBrowserArchivedBinding(@NonNull ConstraintLayout constraintLayout, @NonNull q1 q1Var, @NonNull RecyclerView recyclerView, @NonNull TextView textView, @NonNull ConstraintLayout constraintLayout2, @NonNull MaterialButton materialButton) {
        this.a = constraintLayout;
        this.f2642b = q1Var;
        this.c = recyclerView;
        this.d = constraintLayout2;
        this.e = materialButton;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
