package com.discord.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewbinding.ViewBinding;
import com.discord.views.GuildView;
import com.google.android.material.card.MaterialCardView;
import xyz.discord.R;
/* loaded from: classes.dex */
public final class DiscordHubAddServerListItemBinding implements ViewBinding {
    @NonNull
    public final FrameLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final GuildView f2086b;
    @NonNull
    public final MaterialCardView c;
    @NonNull
    public final TextView d;

    public DiscordHubAddServerListItemBinding(@NonNull FrameLayout frameLayout, @NonNull GuildView guildView, @NonNull MaterialCardView materialCardView, @NonNull TextView textView) {
        this.a = frameLayout;
        this.f2086b = guildView;
        this.c = materialCardView;
        this.d = textView;
    }

    @NonNull
    public static DiscordHubAddServerListItemBinding a(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, boolean z2) {
        View inflate = layoutInflater.inflate(R.layout.discord_hub_add_server_list_item, viewGroup, false);
        if (z2) {
            viewGroup.addView(inflate);
        }
        int i = R.id.discord_u_add_server_list_item_image;
        GuildView guildView = (GuildView) inflate.findViewById(R.id.discord_u_add_server_list_item_image);
        if (guildView != null) {
            i = R.id.discord_u_add_server_list_item_layout;
            MaterialCardView materialCardView = (MaterialCardView) inflate.findViewById(R.id.discord_u_add_server_list_item_layout);
            if (materialCardView != null) {
                i = R.id.discord_u_add_server_list_item_text;
                TextView textView = (TextView) inflate.findViewById(R.id.discord_u_add_server_list_item_text);
                if (textView != null) {
                    return new DiscordHubAddServerListItemBinding((FrameLayout) inflate, guildView, materialCardView, textView);
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
