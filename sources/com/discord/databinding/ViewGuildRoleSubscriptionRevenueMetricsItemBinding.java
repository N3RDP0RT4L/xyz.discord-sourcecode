package com.discord.databinding;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.discord.widgets.servers.guild_role_subscription.payments.GuildRoleSubscriptionRevenueMetrics;
/* loaded from: classes.dex */
public final class ViewGuildRoleSubscriptionRevenueMetricsItemBinding implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final GuildRoleSubscriptionRevenueMetrics f2182b;
    @NonNull
    public final GuildRoleSubscriptionRevenueMetrics c;
    @NonNull
    public final GuildRoleSubscriptionRevenueMetrics d;

    public ViewGuildRoleSubscriptionRevenueMetricsItemBinding(@NonNull ConstraintLayout constraintLayout, @NonNull GuildRoleSubscriptionRevenueMetrics guildRoleSubscriptionRevenueMetrics, @NonNull GuildRoleSubscriptionRevenueMetrics guildRoleSubscriptionRevenueMetrics2, @NonNull GuildRoleSubscriptionRevenueMetrics guildRoleSubscriptionRevenueMetrics3) {
        this.a = constraintLayout;
        this.f2182b = guildRoleSubscriptionRevenueMetrics;
        this.c = guildRoleSubscriptionRevenueMetrics2;
        this.d = guildRoleSubscriptionRevenueMetrics3;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
