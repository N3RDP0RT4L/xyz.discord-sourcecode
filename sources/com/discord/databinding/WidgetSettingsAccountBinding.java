package com.discord.databinding;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import androidx.viewbinding.ViewBinding;
import com.airbnb.lottie.LottieAnimationView;
import com.discord.app.AppViewFlipper;
import com.discord.utilities.dimmer.DimmerView;
import com.discord.views.CheckedSetting;
import com.google.android.material.button.MaterialButton;
/* loaded from: classes.dex */
public final class WidgetSettingsAccountBinding implements ViewBinding {
    @NonNull
    public final LinearLayout A;
    @NonNull
    public final TextView B;
    @NonNull
    public final RelativeLayout C;
    @NonNull
    public final MaterialButton D;
    @NonNull
    public final TextView E;
    @NonNull
    public final TextView F;
    @NonNull
    public final TextView G;
    @NonNull
    public final CoordinatorLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final DimmerView f2565b;
    @NonNull
    public final TextView c;
    @NonNull
    public final TextView d;
    @NonNull
    public final LinearLayout e;
    @NonNull
    public final TextView f;
    @NonNull
    public final TextView g;
    @NonNull
    public final TextView h;
    @NonNull
    public final LinearLayout i;
    @NonNull
    public final MaterialButton j;
    @NonNull
    public final LinearLayout k;
    @NonNull
    public final TextView l;
    @NonNull
    public final AppViewFlipper m;
    @NonNull
    public final LottieAnimationView n;
    @NonNull
    public final TextView o;
    @NonNull
    public final LinearLayout p;
    @NonNull
    public final TextView q;
    @NonNull
    public final LinearLayout r;
    @NonNull

    /* renamed from: s  reason: collision with root package name */
    public final TextView f2566s;
    @NonNull
    public final TextView t;
    @NonNull
    public final TextView u;
    @NonNull
    public final LinearLayout v;
    @NonNull
    public final TextView w;
    @NonNull

    /* renamed from: x  reason: collision with root package name */
    public final NestedScrollView f2567x;
    @NonNull

    /* renamed from: y  reason: collision with root package name */
    public final CheckedSetting f2568y;
    @NonNull

    /* renamed from: z  reason: collision with root package name */
    public final TextView f2569z;

    public WidgetSettingsAccountBinding(@NonNull CoordinatorLayout coordinatorLayout, @NonNull DimmerView dimmerView, @NonNull TextView textView, @NonNull TextView textView2, @NonNull LinearLayout linearLayout, @NonNull TextView textView3, @NonNull TextView textView4, @NonNull LinearLayout linearLayout2, @NonNull TextView textView5, @NonNull TextView textView6, @NonNull LinearLayout linearLayout3, @NonNull LinearLayout linearLayout4, @NonNull MaterialButton materialButton, @NonNull LinearLayout linearLayout5, @NonNull TextView textView7, @NonNull AppViewFlipper appViewFlipper, @NonNull LottieAnimationView lottieAnimationView, @NonNull TextView textView8, @NonNull LinearLayout linearLayout6, @NonNull TextView textView9, @NonNull TextView textView10, @NonNull LinearLayout linearLayout7, @NonNull TextView textView11, @NonNull TextView textView12, @NonNull TextView textView13, @NonNull TextView textView14, @NonNull LinearLayout linearLayout8, @NonNull TextView textView15, @NonNull NestedScrollView nestedScrollView, @NonNull CheckedSetting checkedSetting, @NonNull TextView textView16, @NonNull LinearLayout linearLayout9, @NonNull TextView textView17, @NonNull TextView textView18, @NonNull RelativeLayout relativeLayout, @NonNull MaterialButton materialButton2, @NonNull TextView textView19, @NonNull TextView textView20, @NonNull TextView textView21) {
        this.a = coordinatorLayout;
        this.f2565b = dimmerView;
        this.c = textView;
        this.d = textView2;
        this.e = linearLayout;
        this.f = textView4;
        this.g = textView5;
        this.h = textView6;
        this.i = linearLayout4;
        this.j = materialButton;
        this.k = linearLayout5;
        this.l = textView7;
        this.m = appViewFlipper;
        this.n = lottieAnimationView;
        this.o = textView8;
        this.p = linearLayout6;
        this.q = textView10;
        this.r = linearLayout7;
        this.f2566s = textView12;
        this.t = textView13;
        this.u = textView14;
        this.v = linearLayout8;
        this.w = textView15;
        this.f2567x = nestedScrollView;
        this.f2568y = checkedSetting;
        this.f2569z = textView16;
        this.A = linearLayout9;
        this.B = textView18;
        this.C = relativeLayout;
        this.D = materialButton2;
        this.E = textView19;
        this.F = textView20;
        this.G = textView21;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
