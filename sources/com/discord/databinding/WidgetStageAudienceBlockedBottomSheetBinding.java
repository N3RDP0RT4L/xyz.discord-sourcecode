package com.discord.databinding;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.discord.utilities.view.recycler.MaxHeightRecyclerView;
import com.google.android.material.button.MaterialButton;
/* loaded from: classes.dex */
public final class WidgetStageAudienceBlockedBottomSheetBinding implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final MaterialButton f2621b;
    @NonNull
    public final MaxHeightRecyclerView c;
    @NonNull
    public final MaterialButton d;

    public WidgetStageAudienceBlockedBottomSheetBinding(@NonNull ConstraintLayout constraintLayout, @NonNull MaterialButton materialButton, @NonNull MaxHeightRecyclerView maxHeightRecyclerView, @NonNull MaterialButton materialButton2) {
        this.a = constraintLayout;
        this.f2621b = materialButton;
        this.c = maxHeightRecyclerView;
        this.d = materialButton2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
