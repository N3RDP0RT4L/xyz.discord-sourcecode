package com.discord.databinding;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import androidx.viewbinding.ViewBinding;
import com.discord.views.CheckedSetting;
import com.discord.widgets.roles.RoleIconView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
/* loaded from: classes.dex */
public final class WidgetEditRoleBinding implements ViewBinding {
    @NonNull
    public final CheckedSetting A;
    @NonNull
    public final CheckedSetting B;
    @NonNull
    public final CheckedSetting C;
    @NonNull
    public final CheckedSetting D;
    @NonNull
    public final CheckedSetting E;
    @NonNull
    public final CheckedSetting F;
    @NonNull
    public final CheckedSetting G;
    @NonNull
    public final CheckedSetting H;
    @NonNull
    public final CheckedSetting I;
    @NonNull
    public final CheckedSetting J;
    @NonNull
    public final CheckedSetting K;
    @NonNull
    public final RoleIconView L;
    @NonNull
    public final View M;
    @NonNull
    public final TextView N;
    @NonNull
    public final CheckedSetting O;
    @NonNull
    public final CheckedSetting P;
    @NonNull
    public final CheckedSetting Q;
    @NonNull
    public final CheckedSetting R;
    @NonNull
    public final CheckedSetting S;
    @NonNull
    public final CheckedSetting T;
    @NonNull
    public final CheckedSetting U;
    @NonNull
    public final CheckedSetting V;
    @NonNull
    public final CheckedSetting W;
    @NonNull
    public final CheckedSetting X;
    @NonNull
    public final CheckedSetting Y;
    @NonNull
    public final CheckedSetting Z;
    @NonNull
    public final CoordinatorLayout a;
    @NonNull

    /* renamed from: a0  reason: collision with root package name */
    public final CheckedSetting f2349a0;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextInputLayout f2350b;
    @NonNull

    /* renamed from: b0  reason: collision with root package name */
    public final CheckedSetting f2351b0;
    @NonNull
    public final FloatingActionButton c;
    @NonNull

    /* renamed from: c0  reason: collision with root package name */
    public final CheckedSetting f2352c0;
    @NonNull
    public final View d;
    @NonNull
    public final TextView e;
    @NonNull
    public final CheckedSetting f;
    @NonNull
    public final CheckedSetting g;
    @NonNull
    public final CheckedSetting h;
    @NonNull
    public final CheckedSetting i;
    @NonNull
    public final CheckedSetting j;
    @NonNull
    public final View k;
    @NonNull
    public final RelativeLayout l;
    @NonNull
    public final CheckedSetting m;
    @NonNull
    public final CheckedSetting n;
    @NonNull
    public final CheckedSetting o;
    @NonNull
    public final View p;
    @NonNull
    public final View q;
    @NonNull
    public final CheckedSetting r;
    @NonNull

    /* renamed from: s  reason: collision with root package name */
    public final CheckedSetting f2353s;
    @NonNull
    public final RelativeLayout t;
    @NonNull
    public final CheckedSetting u;
    @NonNull
    public final CheckedSetting v;
    @NonNull
    public final CheckedSetting w;
    @NonNull

    /* renamed from: x  reason: collision with root package name */
    public final CheckedSetting f2354x;
    @NonNull

    /* renamed from: y  reason: collision with root package name */
    public final CheckedSetting f2355y;
    @NonNull

    /* renamed from: z  reason: collision with root package name */
    public final CheckedSetting f2356z;

    public WidgetEditRoleBinding(@NonNull CoordinatorLayout coordinatorLayout, @NonNull TextInputLayout textInputLayout, @NonNull FloatingActionButton floatingActionButton, @NonNull View view, @NonNull TextView textView, @NonNull CheckedSetting checkedSetting, @NonNull CheckedSetting checkedSetting2, @NonNull CheckedSetting checkedSetting3, @NonNull CheckedSetting checkedSetting4, @NonNull CheckedSetting checkedSetting5, @NonNull View view2, @NonNull RelativeLayout relativeLayout, @NonNull CheckedSetting checkedSetting6, @NonNull CheckedSetting checkedSetting7, @NonNull CheckedSetting checkedSetting8, @NonNull View view3, @NonNull View view4, @NonNull CheckedSetting checkedSetting9, @NonNull CheckedSetting checkedSetting10, @NonNull RelativeLayout relativeLayout2, @NonNull CheckedSetting checkedSetting11, @NonNull CheckedSetting checkedSetting12, @NonNull CheckedSetting checkedSetting13, @NonNull CheckedSetting checkedSetting14, @NonNull CheckedSetting checkedSetting15, @NonNull CheckedSetting checkedSetting16, @NonNull CheckedSetting checkedSetting17, @NonNull CheckedSetting checkedSetting18, @NonNull CheckedSetting checkedSetting19, @NonNull CheckedSetting checkedSetting20, @NonNull CheckedSetting checkedSetting21, @NonNull CheckedSetting checkedSetting22, @NonNull CheckedSetting checkedSetting23, @NonNull NestedScrollView nestedScrollView, @NonNull CheckedSetting checkedSetting24, @NonNull CheckedSetting checkedSetting25, @NonNull CheckedSetting checkedSetting26, @NonNull CheckedSetting checkedSetting27, @NonNull RoleIconView roleIconView, @NonNull View view5, @NonNull TextView textView2, @NonNull CheckedSetting checkedSetting28, @NonNull CheckedSetting checkedSetting29, @NonNull CheckedSetting checkedSetting30, @NonNull CheckedSetting checkedSetting31, @NonNull CheckedSetting checkedSetting32, @NonNull CheckedSetting checkedSetting33, @NonNull CheckedSetting checkedSetting34, @NonNull CheckedSetting checkedSetting35, @NonNull CheckedSetting checkedSetting36, @NonNull CheckedSetting checkedSetting37, @NonNull CheckedSetting checkedSetting38, @NonNull CheckedSetting checkedSetting39, @NonNull CheckedSetting checkedSetting40, @NonNull CheckedSetting checkedSetting41, @NonNull CheckedSetting checkedSetting42) {
        this.a = coordinatorLayout;
        this.f2350b = textInputLayout;
        this.c = floatingActionButton;
        this.d = view;
        this.e = textView;
        this.f = checkedSetting;
        this.g = checkedSetting2;
        this.h = checkedSetting3;
        this.i = checkedSetting4;
        this.j = checkedSetting5;
        this.k = view2;
        this.l = relativeLayout;
        this.m = checkedSetting6;
        this.n = checkedSetting7;
        this.o = checkedSetting8;
        this.p = view3;
        this.q = view4;
        this.r = checkedSetting9;
        this.f2353s = checkedSetting10;
        this.t = relativeLayout2;
        this.u = checkedSetting11;
        this.v = checkedSetting12;
        this.w = checkedSetting13;
        this.f2354x = checkedSetting14;
        this.f2355y = checkedSetting15;
        this.f2356z = checkedSetting16;
        this.A = checkedSetting17;
        this.B = checkedSetting18;
        this.C = checkedSetting19;
        this.D = checkedSetting20;
        this.E = checkedSetting21;
        this.F = checkedSetting22;
        this.G = checkedSetting23;
        this.H = checkedSetting24;
        this.I = checkedSetting25;
        this.J = checkedSetting26;
        this.K = checkedSetting27;
        this.L = roleIconView;
        this.M = view5;
        this.N = textView2;
        this.O = checkedSetting28;
        this.P = checkedSetting29;
        this.Q = checkedSetting30;
        this.R = checkedSetting31;
        this.S = checkedSetting32;
        this.T = checkedSetting33;
        this.U = checkedSetting34;
        this.V = checkedSetting35;
        this.W = checkedSetting36;
        this.X = checkedSetting37;
        this.Y = checkedSetting38;
        this.Z = checkedSetting39;
        this.f2349a0 = checkedSetting40;
        this.f2351b0 = checkedSetting41;
        this.f2352c0 = checkedSetting42;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
