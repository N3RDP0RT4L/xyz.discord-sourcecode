package com.discord.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import xyz.discord.R;
/* loaded from: classes.dex */
public final class GuildScheduledEventDateViewBinding implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ImageView f2103b;
    @NonNull
    public final TextView c;

    public GuildScheduledEventDateViewBinding(@NonNull ConstraintLayout constraintLayout, @NonNull ImageView imageView, @NonNull TextView textView) {
        this.a = constraintLayout;
        this.f2103b = imageView;
        this.c = textView;
    }

    @NonNull
    public static GuildScheduledEventDateViewBinding a(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, boolean z2) {
        View inflate = layoutInflater.inflate(R.layout.guild_scheduled_event_date_view, viewGroup, false);
        if (z2) {
            viewGroup.addView(inflate);
        }
        int i = R.id.guild_scheduled_event_date_view_icon;
        ImageView imageView = (ImageView) inflate.findViewById(R.id.guild_scheduled_event_date_view_icon);
        if (imageView != null) {
            i = R.id.guild_scheduled_event_date_view_text;
            TextView textView = (TextView) inflate.findViewById(R.id.guild_scheduled_event_date_view_text);
            if (textView != null) {
                return new GuildScheduledEventDateViewBinding((ConstraintLayout) inflate, imageView, textView);
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
