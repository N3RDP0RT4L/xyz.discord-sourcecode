package com.discord.databinding;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.views.LoadingButton;
/* loaded from: classes.dex */
public final class WidgetThreadStatusBinding implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ConstraintLayout f2646b;
    @NonNull
    public final LoadingButton c;
    @NonNull
    public final LinkifiedTextView d;

    public WidgetThreadStatusBinding(@NonNull ConstraintLayout constraintLayout, @NonNull ConstraintLayout constraintLayout2, @NonNull LoadingButton loadingButton, @NonNull LinkifiedTextView linkifiedTextView) {
        this.a = constraintLayout;
        this.f2646b = constraintLayout2;
        this.c = loadingButton;
        this.d = linkifiedTextView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
