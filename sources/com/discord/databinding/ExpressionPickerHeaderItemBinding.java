package com.discord.databinding;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
/* loaded from: classes.dex */
public final class ExpressionPickerHeaderItemBinding implements ViewBinding {
    @NonNull
    public final TextView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f2093b;

    public ExpressionPickerHeaderItemBinding(@NonNull TextView textView, @NonNull TextView textView2) {
        this.a = textView;
        this.f2093b = textView2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
