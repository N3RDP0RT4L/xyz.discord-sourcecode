package com.discord.databinding;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.viewbinding.ViewBinding;
import com.discord.widgets.chat.list.ViewEmbedGameInvite;
/* loaded from: classes.dex */
public final class WidgetChatListAdapterItemGameInviteBinding implements ViewBinding {
    @NonNull
    public final CardView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ViewEmbedGameInvite f2300b;

    public WidgetChatListAdapterItemGameInviteBinding(@NonNull CardView cardView, @NonNull ViewEmbedGameInvite viewEmbedGameInvite) {
        this.a = cardView;
        this.f2300b = viewEmbedGameInvite;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
