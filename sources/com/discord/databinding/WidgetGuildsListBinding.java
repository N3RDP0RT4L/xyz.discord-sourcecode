package com.discord.databinding;

import android.view.View;
import android.view.ViewStub;
import android.widget.RelativeLayout;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
/* loaded from: classes.dex */
public final class WidgetGuildsListBinding implements ViewBinding {
    @NonNull
    public final RelativeLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final RecyclerView f2435b;
    @NonNull
    public final ViewStub c;

    public WidgetGuildsListBinding(@NonNull RelativeLayout relativeLayout, @NonNull RecyclerView recyclerView, @NonNull ViewStub viewStub) {
        this.a = relativeLayout;
        this.f2435b = recyclerView;
        this.c = viewStub;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
