package com.discord.databinding;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.card.MaterialCardView;
/* loaded from: classes.dex */
public final class WidgetFriendsListContactSyncUpsellBinding implements ViewBinding {
    @NonNull
    public final MaterialCardView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final MaterialCardView f2375b;

    public WidgetFriendsListContactSyncUpsellBinding(@NonNull MaterialCardView materialCardView, @NonNull ImageView imageView, @NonNull MaterialCardView materialCardView2, @NonNull ImageView imageView2, @NonNull TextView textView, @NonNull TextView textView2) {
        this.a = materialCardView;
        this.f2375b = materialCardView2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
