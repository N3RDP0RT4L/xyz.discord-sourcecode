package com.discord.databinding;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.button.MaterialButton;
/* loaded from: classes.dex */
public final class PublishMessageDialogBinding implements ViewBinding {
    @NonNull
    public final LinearLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f2120b;
    @NonNull
    public final MaterialButton c;
    @NonNull
    public final TextView d;
    @NonNull
    public final LinearLayout e;
    @NonNull
    public final MaterialButton f;

    public PublishMessageDialogBinding(@NonNull LinearLayout linearLayout, @NonNull LinearLayout linearLayout2, @NonNull TextView textView, @NonNull MaterialButton materialButton, @NonNull TextView textView2, @NonNull LinearLayout linearLayout3, @NonNull MaterialButton materialButton2) {
        this.a = linearLayout;
        this.f2120b = textView;
        this.c = materialButton;
        this.d = textView2;
        this.e = linearLayout3;
        this.f = materialButton2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
