package com.discord.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.textview.MaterialTextView;
import xyz.discord.R;
/* loaded from: classes.dex */
public final class GuildEventPromptViewBinding implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ImageView f2101b;
    @NonNull
    public final ImageView c;
    @NonNull
    public final MaterialTextView d;
    @NonNull
    public final MaterialTextView e;

    public GuildEventPromptViewBinding(@NonNull ConstraintLayout constraintLayout, @NonNull ImageView imageView, @NonNull ImageView imageView2, @NonNull ImageView imageView3, @NonNull MaterialTextView materialTextView, @NonNull MaterialTextView materialTextView2) {
        this.a = constraintLayout;
        this.f2101b = imageView2;
        this.c = imageView3;
        this.d = materialTextView;
        this.e = materialTextView2;
    }

    @NonNull
    public static GuildEventPromptViewBinding a(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, boolean z2) {
        View inflate = layoutInflater.inflate(R.layout.guild_event_prompt_view, viewGroup, false);
        if (z2) {
            viewGroup.addView(inflate);
        }
        int i = R.id.arrow;
        ImageView imageView = (ImageView) inflate.findViewById(R.id.arrow);
        if (imageView != null) {
            i = R.id.icon;
            ImageView imageView2 = (ImageView) inflate.findViewById(R.id.icon);
            if (imageView2 != null) {
                i = R.id.icon_bg;
                ImageView imageView3 = (ImageView) inflate.findViewById(R.id.icon_bg);
                if (imageView3 != null) {
                    i = R.id.subtitle;
                    MaterialTextView materialTextView = (MaterialTextView) inflate.findViewById(R.id.subtitle);
                    if (materialTextView != null) {
                        i = R.id.title;
                        MaterialTextView materialTextView2 = (MaterialTextView) inflate.findViewById(R.id.title);
                        if (materialTextView2 != null) {
                            return new GuildEventPromptViewBinding((ConstraintLayout) inflate, imageView, imageView2, imageView3, materialTextView, materialTextView2);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
