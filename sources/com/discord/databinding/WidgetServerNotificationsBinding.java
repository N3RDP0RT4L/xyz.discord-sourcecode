package com.discord.databinding;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
import b.a.i.x1;
import com.discord.views.CheckedSetting;
import com.discord.widgets.servers.NotificationMuteSettingsView;
/* loaded from: classes.dex */
public final class WidgetServerNotificationsBinding implements ViewBinding {
    @NonNull
    public final CoordinatorLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final x1 f2514b;
    @NonNull
    public final RecyclerView c;
    @NonNull
    public final CheckedSetting d;
    @NonNull
    public final CheckedSetting e;
    @NonNull
    public final CheckedSetting f;
    @NonNull
    public final CheckedSetting g;
    @NonNull
    public final View h;
    @NonNull
    public final LinearLayout i;
    @NonNull
    public final NotificationMuteSettingsView j;
    @NonNull
    public final View k;
    @NonNull
    public final TextView l;
    @NonNull
    public final CheckedSetting m;
    @NonNull
    public final View n;
    @NonNull
    public final CheckedSetting o;

    public WidgetServerNotificationsBinding(@NonNull CoordinatorLayout coordinatorLayout, @NonNull x1 x1Var, @NonNull RecyclerView recyclerView, @NonNull CheckedSetting checkedSetting, @NonNull CheckedSetting checkedSetting2, @NonNull CheckedSetting checkedSetting3, @NonNull CheckedSetting checkedSetting4, @NonNull View view, @NonNull LinearLayout linearLayout, @NonNull NotificationMuteSettingsView notificationMuteSettingsView, @NonNull View view2, @NonNull TextView textView, @NonNull CheckedSetting checkedSetting5, @NonNull View view3, @NonNull CheckedSetting checkedSetting6) {
        this.a = coordinatorLayout;
        this.f2514b = x1Var;
        this.c = recyclerView;
        this.d = checkedSetting;
        this.e = checkedSetting2;
        this.f = checkedSetting3;
        this.g = checkedSetting4;
        this.h = view;
        this.i = linearLayout;
        this.j = notificationMuteSettingsView;
        this.k = view2;
        this.l = textView;
        this.m = checkedSetting5;
        this.n = view3;
        this.o = checkedSetting6;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
