package com.discord.databinding;

import android.view.View;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import b.a.i.j5;
import b.a.i.k5;
import b.a.i.z4;
import com.discord.views.LoadingButton;
import com.google.android.material.button.MaterialButton;
/* loaded from: classes.dex */
public final class WidgetHubEmailFlowBinding implements ViewBinding {
    @NonNull
    public final LinearLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final j5 f2450b;
    @NonNull
    public final z4 c;
    @NonNull
    public final MaterialButton d;
    @NonNull
    public final LoadingButton e;
    @NonNull
    public final k5 f;

    public WidgetHubEmailFlowBinding(@NonNull LinearLayout linearLayout, @NonNull LinearLayout linearLayout2, @NonNull j5 j5Var, @NonNull z4 z4Var, @NonNull MaterialButton materialButton, @NonNull LoadingButton loadingButton, @NonNull k5 k5Var) {
        this.a = linearLayout;
        this.f2450b = j5Var;
        this.c = z4Var;
        this.d = materialButton;
        this.e = loadingButton;
        this.f = k5Var;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
