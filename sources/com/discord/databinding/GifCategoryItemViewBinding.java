package com.discord.databinding;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.viewbinding.ViewBinding;
import com.facebook.drawee.view.SimpleDraweeView;
/* loaded from: classes.dex */
public final class GifCategoryItemViewBinding implements ViewBinding {
    @NonNull
    public final CardView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ImageView f2095b;
    @NonNull
    public final SimpleDraweeView c;
    @NonNull
    public final TextView d;

    public GifCategoryItemViewBinding(@NonNull CardView cardView, @NonNull ImageView imageView, @NonNull SimpleDraweeView simpleDraweeView, @NonNull TextView textView) {
        this.a = cardView;
        this.f2095b = imageView;
        this.c = simpleDraweeView;
        this.d = textView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
