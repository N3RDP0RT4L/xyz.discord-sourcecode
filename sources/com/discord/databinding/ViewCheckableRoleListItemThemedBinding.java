package com.discord.databinding;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.views.CheckedSetting;
/* loaded from: classes.dex */
public final class ViewCheckableRoleListItemThemedBinding implements ViewBinding {
    @NonNull
    public final CheckedSetting a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final CheckedSetting f2163b;

    public ViewCheckableRoleListItemThemedBinding(@NonNull CheckedSetting checkedSetting, @NonNull CheckedSetting checkedSetting2) {
        this.a = checkedSetting;
        this.f2163b = checkedSetting2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
