package com.discord.databinding;

import android.view.View;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.views.CheckedSetting;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
/* loaded from: classes.dex */
public final class WidgetGuildTransferOwnershipBinding implements ViewBinding {
    @NonNull
    public final LinearLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final CheckedSetting f2432b;
    @NonNull
    public final MaterialButton c;
    @NonNull
    public final MaterialButton d;
    @NonNull
    public final TextInputLayout e;
    @NonNull
    public final LinearLayout f;

    public WidgetGuildTransferOwnershipBinding(@NonNull LinearLayout linearLayout, @NonNull CheckedSetting checkedSetting, @NonNull MaterialButton materialButton, @NonNull MaterialButton materialButton2, @NonNull TextInputLayout textInputLayout, @NonNull LinearLayout linearLayout2) {
        this.a = linearLayout;
        this.f2432b = checkedSetting;
        this.c = materialButton;
        this.d = materialButton2;
        this.e = textInputLayout;
        this.f = linearLayout2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
