package com.discord.databinding;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.discord.views.StatusView;
import com.facebook.drawee.view.SimpleDraweeView;
/* loaded from: classes.dex */
public final class WidgetAddFriendUserListItemBinding implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final AppCompatImageView f2206b;
    @NonNull
    public final AppCompatImageView c;
    @NonNull
    public final AppCompatImageView d;
    @NonNull
    public final SimpleDraweeView e;
    @NonNull
    public final TextView f;
    @NonNull
    public final TextView g;
    @NonNull
    public final StatusView h;
    @NonNull
    public final AppCompatImageView i;

    public WidgetAddFriendUserListItemBinding(@NonNull ConstraintLayout constraintLayout, @NonNull AppCompatImageView appCompatImageView, @NonNull AppCompatImageView appCompatImageView2, @NonNull AppCompatImageView appCompatImageView3, @NonNull SimpleDraweeView simpleDraweeView, @NonNull LinearLayout linearLayout, @NonNull TextView textView, @NonNull TextView textView2, @NonNull StatusView statusView, @NonNull LinearLayout linearLayout2, @NonNull AppCompatImageView appCompatImageView4) {
        this.a = constraintLayout;
        this.f2206b = appCompatImageView;
        this.c = appCompatImageView2;
        this.d = appCompatImageView3;
        this.e = simpleDraweeView;
        this.f = textView;
        this.g = textView2;
        this.h = statusView;
        this.i = appCompatImageView4;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
