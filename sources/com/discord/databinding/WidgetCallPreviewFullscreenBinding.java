package com.discord.databinding;

import android.view.View;
import android.view.ViewStub;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
import b.a.i.o0;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.button.MaterialButton;
/* loaded from: classes.dex */
public final class WidgetCallPreviewFullscreenBinding implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final Toolbar f2232b;
    @NonNull
    public final AppBarLayout c;
    @NonNull
    public final ConstraintLayout d;
    @NonNull
    public final TextView e;
    @NonNull
    public final ConstraintLayout f;
    @NonNull
    public final MaterialButton g;
    @NonNull
    public final MaterialButton h;
    @NonNull
    public final ViewStub i;
    @NonNull
    public final RecyclerView j;
    @NonNull
    public final o0 k;

    public WidgetCallPreviewFullscreenBinding(@NonNull ConstraintLayout constraintLayout, @NonNull Toolbar toolbar, @NonNull AppBarLayout appBarLayout, @NonNull ConstraintLayout constraintLayout2, @NonNull TextView textView, @NonNull ConstraintLayout constraintLayout3, @NonNull MaterialButton materialButton, @NonNull MaterialButton materialButton2, @NonNull ViewStub viewStub, @NonNull RecyclerView recyclerView, @NonNull o0 o0Var) {
        this.a = constraintLayout;
        this.f2232b = toolbar;
        this.c = appBarLayout;
        this.d = constraintLayout2;
        this.e = textView;
        this.f = constraintLayout3;
        this.g = materialButton;
        this.h = materialButton2;
        this.i = viewStub;
        this.j = recyclerView;
        this.k = o0Var;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
