package com.discord.databinding;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import b.a.i.s4;
import com.discord.views.typing.TypingDots;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
/* loaded from: classes.dex */
public final class WidgetChatOverlayBinding implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final FloatingActionButton f2326b;
    @NonNull
    public final RelativeLayout c;
    @NonNull
    public final TypingDots d;
    @NonNull
    public final TextView e;
    @NonNull
    public final ImageView f;
    @NonNull
    public final TextView g;
    @NonNull
    public final s4 h;

    public WidgetChatOverlayBinding(@NonNull ConstraintLayout constraintLayout, @NonNull FloatingActionButton floatingActionButton, @NonNull RelativeLayout relativeLayout, @NonNull TypingDots typingDots, @NonNull TextView textView, @NonNull ImageView imageView, @NonNull TextView textView2, @NonNull s4 s4Var) {
        this.a = constraintLayout;
        this.f2326b = floatingActionButton;
        this.c = relativeLayout;
        this.d = typingDots;
        this.e = textView;
        this.f = imageView;
        this.g = textView2;
        this.h = s4Var;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
