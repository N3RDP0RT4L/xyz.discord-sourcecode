package com.discord.databinding;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
/* loaded from: classes.dex */
public final class WidgetSettingsConnectionsBinding implements ViewBinding {
    @NonNull
    public final CoordinatorLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextView f2588b;
    @NonNull
    public final RecyclerView c;

    public WidgetSettingsConnectionsBinding(@NonNull CoordinatorLayout coordinatorLayout, @NonNull TextView textView, @NonNull RecyclerView recyclerView) {
        this.a = coordinatorLayout;
        this.f2588b = textView;
        this.c = recyclerView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
