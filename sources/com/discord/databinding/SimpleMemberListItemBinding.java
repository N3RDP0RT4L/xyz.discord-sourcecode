package com.discord.databinding;

import android.view.View;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import com.discord.views.user.SettingsMemberView;
/* loaded from: classes.dex */
public final class SimpleMemberListItemBinding implements ViewBinding {
    @NonNull
    public final FrameLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final SettingsMemberView f2125b;

    public SimpleMemberListItemBinding(@NonNull FrameLayout frameLayout, @NonNull SettingsMemberView settingsMemberView) {
        this.a = frameLayout;
        this.f2125b = settingsMemberView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
