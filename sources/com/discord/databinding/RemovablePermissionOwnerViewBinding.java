package com.discord.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.discord.views.permissions.ChannelPermissionOwnerView;
import xyz.discord.R;
/* loaded from: classes.dex */
public final class RemovablePermissionOwnerViewBinding implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ChannelPermissionOwnerView f2121b;
    @NonNull
    public final ImageView c;

    public RemovablePermissionOwnerViewBinding(@NonNull ConstraintLayout constraintLayout, @NonNull ChannelPermissionOwnerView channelPermissionOwnerView, @NonNull ImageView imageView) {
        this.a = constraintLayout;
        this.f2121b = channelPermissionOwnerView;
        this.c = imageView;
    }

    @NonNull
    public static RemovablePermissionOwnerViewBinding a(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, boolean z2) {
        View inflate = layoutInflater.inflate(R.layout.removable_permission_owner_view, viewGroup, false);
        if (z2) {
            viewGroup.addView(inflate);
        }
        int i = R.id.permission_owner_view;
        ChannelPermissionOwnerView channelPermissionOwnerView = (ChannelPermissionOwnerView) inflate.findViewById(R.id.permission_owner_view);
        if (channelPermissionOwnerView != null) {
            i = R.id.remove;
            ImageView imageView = (ImageView) inflate.findViewById(R.id.remove);
            if (imageView != null) {
                return new RemovablePermissionOwnerViewBinding((ConstraintLayout) inflate, channelPermissionOwnerView, imageView);
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
