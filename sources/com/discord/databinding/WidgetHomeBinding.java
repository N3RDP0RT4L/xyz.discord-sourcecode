package com.discord.databinding;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
import b.a.i.d5;
import b.a.i.g5;
import b.a.i.h5;
import b.a.i.i5;
import com.discord.widgets.home.HomePanelsLayout;
/* loaded from: classes.dex */
public final class WidgetHomeBinding implements ViewBinding {
    @NonNull
    public final FrameLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final h5 f2442b;
    @NonNull
    public final HomePanelsLayout c;
    @NonNull
    public final d5 d;
    @NonNull
    public final g5 e;
    @NonNull
    public final i5 f;
    @NonNull
    public final ImageView g;
    @NonNull
    public final FrameLayout h;

    public WidgetHomeBinding(@NonNull FrameLayout frameLayout, @NonNull h5 h5Var, @NonNull HomePanelsLayout homePanelsLayout, @NonNull d5 d5Var, @NonNull g5 g5Var, @NonNull i5 i5Var, @NonNull ImageView imageView, @NonNull FrameLayout frameLayout2) {
        this.a = frameLayout;
        this.f2442b = h5Var;
        this.c = homePanelsLayout;
        this.d = d5Var;
        this.e = g5Var;
        this.f = i5Var;
        this.g = imageView;
        this.h = frameLayout2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
