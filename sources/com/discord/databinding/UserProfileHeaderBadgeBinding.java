package com.discord.databinding;

import android.view.View;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;
/* loaded from: classes.dex */
public final class UserProfileHeaderBadgeBinding implements ViewBinding {
    @NonNull
    public final ImageView a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ImageView f2148b;

    public UserProfileHeaderBadgeBinding(@NonNull ImageView imageView, @NonNull ImageView imageView2) {
        this.a = imageView;
        this.f2148b = imageView2;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
