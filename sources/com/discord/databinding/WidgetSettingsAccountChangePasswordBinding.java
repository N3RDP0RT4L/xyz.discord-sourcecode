package com.discord.databinding;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewbinding.ViewBinding;
import com.discord.utilities.dimmer.DimmerView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
/* loaded from: classes.dex */
public final class WidgetSettingsAccountChangePasswordBinding implements ViewBinding {
    @NonNull
    public final CoordinatorLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final TextInputLayout f2570b;
    @NonNull
    public final TextInputLayout c;
    @NonNull
    public final FloatingActionButton d;
    @NonNull
    public final TextInputLayout e;
    @NonNull
    public final DimmerView f;

    public WidgetSettingsAccountChangePasswordBinding(@NonNull CoordinatorLayout coordinatorLayout, @NonNull TextInputLayout textInputLayout, @NonNull TextInputLayout textInputLayout2, @NonNull FloatingActionButton floatingActionButton, @NonNull TextInputLayout textInputLayout3, @NonNull DimmerView dimmerView) {
        this.a = coordinatorLayout;
        this.f2570b = textInputLayout;
        this.c = textInputLayout2;
        this.d = floatingActionButton;
        this.e = textInputLayout3;
        this.f = dimmerView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
