package com.discord.databinding;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.viewbinding.ViewBinding;
import com.discord.views.ScreenTitleView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
/* loaded from: classes.dex */
public final class WidgetGuildScheduledEventSettingsBinding implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ImageView f2425b;
    @NonNull
    public final ImageView c;
    @NonNull
    public final TextView d;
    @NonNull
    public final TextInputEditText e;
    @NonNull
    public final TextInputLayout f;
    @NonNull
    public final TextView g;
    @NonNull
    public final TextInputEditText h;
    @NonNull
    public final TextInputLayout i;
    @NonNull
    public final TextInputEditText j;
    @NonNull
    public final MaterialButton k;
    @NonNull
    public final TextInputEditText l;
    @NonNull
    public final TextInputEditText m;
    @NonNull
    public final TextView n;
    @NonNull
    public final TextInputEditText o;
    @NonNull
    public final MaterialCheckBox p;
    @NonNull
    public final LinearLayout q;
    @NonNull
    public final View r;
    @NonNull

    /* renamed from: s  reason: collision with root package name */
    public final TextView f2426s;
    @NonNull
    public final TextView t;

    public WidgetGuildScheduledEventSettingsBinding(@NonNull ConstraintLayout constraintLayout, @NonNull ImageView imageView, @NonNull ImageView imageView2, @NonNull TextView textView, @NonNull TextInputEditText textInputEditText, @NonNull TextInputLayout textInputLayout, @NonNull TextView textView2, @NonNull TextInputEditText textInputEditText2, @NonNull TextInputLayout textInputLayout2, @NonNull TextView textView3, @NonNull TextInputEditText textInputEditText3, @NonNull TextInputLayout textInputLayout3, @NonNull MaterialButton materialButton, @NonNull NestedScrollView nestedScrollView, @NonNull TextView textView4, @NonNull TextInputEditText textInputEditText4, @NonNull TextInputLayout textInputLayout4, @NonNull TextView textView5, @NonNull TextInputEditText textInputEditText5, @NonNull TextInputLayout textInputLayout5, @NonNull TextView textView6, @NonNull ScreenTitleView screenTitleView, @NonNull ConstraintLayout constraintLayout2, @NonNull TextView textView7, @NonNull TextInputEditText textInputEditText6, @NonNull TextInputLayout textInputLayout6, @NonNull MaterialCheckBox materialCheckBox, @NonNull LinearLayout linearLayout, @NonNull View view, @NonNull TextView textView8, @NonNull TextView textView9) {
        this.a = constraintLayout;
        this.f2425b = imageView;
        this.c = imageView2;
        this.d = textView;
        this.e = textInputEditText;
        this.f = textInputLayout;
        this.g = textView2;
        this.h = textInputEditText2;
        this.i = textInputLayout2;
        this.j = textInputEditText3;
        this.k = materialButton;
        this.l = textInputEditText4;
        this.m = textInputEditText5;
        this.n = textView6;
        this.o = textInputEditText6;
        this.p = materialCheckBox;
        this.q = linearLayout;
        this.r = view;
        this.f2426s = textView8;
        this.t = textView9;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
