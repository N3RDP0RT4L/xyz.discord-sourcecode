package com.discord.databinding;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
import b.a.i.q1;
/* loaded from: classes.dex */
public final class WidgetThreadBrowserActiveBinding implements ViewBinding {
    @NonNull
    public final ConstraintLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final q1 f2641b;
    @NonNull
    public final RecyclerView c;

    public WidgetThreadBrowserActiveBinding(@NonNull ConstraintLayout constraintLayout, @NonNull q1 q1Var, @NonNull RecyclerView recyclerView) {
        this.a = constraintLayout;
        this.f2641b = q1Var;
        this.c = recyclerView;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
