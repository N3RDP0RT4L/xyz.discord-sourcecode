package com.discord.databinding;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewbinding.ViewBinding;
import com.discord.widgets.guilds.invite.ViewInviteSettingsSheet;
import com.google.android.material.button.MaterialButton;
/* loaded from: classes.dex */
public final class WidgetGuildInviteShareEmptySuggestionsBinding implements ViewBinding {
    @NonNull
    public final CoordinatorLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final View f2398b;
    @NonNull
    public final TextView c;
    @NonNull
    public final MaterialButton d;
    @NonNull
    public final ImageButton e;
    @NonNull
    public final TextView f;
    @NonNull
    public final ViewInviteSettingsSheet g;

    public WidgetGuildInviteShareEmptySuggestionsBinding(@NonNull CoordinatorLayout coordinatorLayout, @NonNull View view, @NonNull TextView textView, @NonNull MaterialButton materialButton, @NonNull ImageButton imageButton, @NonNull TextView textView2, @NonNull ViewInviteSettingsSheet viewInviteSettingsSheet) {
        this.a = coordinatorLayout;
        this.f2398b = view;
        this.c = textView;
        this.d = materialButton;
        this.e = imageButton;
        this.f = textView2;
        this.g = viewInviteSettingsSheet;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
