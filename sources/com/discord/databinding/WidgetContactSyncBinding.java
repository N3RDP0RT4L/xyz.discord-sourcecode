package com.discord.databinding;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewbinding.ViewBinding;
import b.a.i.h0;
import b.a.i.i0;
import b.a.i.j0;
import b.a.i.k0;
import b.a.i.l0;
import b.a.i.n0;
import com.discord.app.AppViewFlipper;
/* loaded from: classes.dex */
public final class WidgetContactSyncBinding implements ViewBinding {
    @NonNull
    public final CoordinatorLayout a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final h0 f2336b;
    @NonNull
    public final k0 c;
    @NonNull
    public final l0 d;
    @NonNull
    public final i0 e;
    @NonNull
    public final j0 f;
    @NonNull
    public final n0 g;
    @NonNull
    public final AppViewFlipper h;

    public WidgetContactSyncBinding(@NonNull CoordinatorLayout coordinatorLayout, @NonNull h0 h0Var, @NonNull k0 k0Var, @NonNull l0 l0Var, @NonNull i0 i0Var, @NonNull j0 j0Var, @NonNull n0 n0Var, @NonNull AppViewFlipper appViewFlipper) {
        this.a = coordinatorLayout;
        this.f2336b = h0Var;
        this.c = k0Var;
        this.d = l0Var;
        this.e = i0Var;
        this.f = j0Var;
        this.g = n0Var;
        this.h = appViewFlipper;
    }

    @Override // androidx.viewbinding.ViewBinding
    @NonNull
    public View getRoot() {
        return this.a;
    }
}
