package com.discord.i18n;

import andhook.lib.HookHelper;
import android.view.View;
import androidx.annotation.ColorInt;
import b.a.k.d;
import com.discord.i18n.Hook;
import d0.z.d.m;
import d0.z.d.o;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: RenderContext.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0010 \n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0010\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b4\u00105J)\u0010\b\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004¢\u0006\u0004\b\b\u0010\tJ)\u0010\f\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u00060\u0004¢\u0006\u0004\b\f\u0010\tR%\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R$\u0010\u0019\u001a\u0004\u0018\u00010\u00128\u0006@\u0006X\u0087\u000e¢\u0006\u0012\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R$\u0010\u001d\u001a\u0004\u0018\u00010\u00128\u0006@\u0006X\u0087\u000e¢\u0006\u0012\n\u0004\b\u001a\u0010\u0014\u001a\u0004\b\u001b\u0010\u0016\"\u0004\b\u001c\u0010\u0018R,\u0010%\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0001\u0018\u00010\u001e8\u0000@\u0000X\u0080\u000e¢\u0006\u0012\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R\"\u0010-\u001a\u00020&8\u0000@\u0000X\u0080\u000e¢\u0006\u0012\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*\"\u0004\b+\u0010,R\"\u00101\u001a\u00020&8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b.\u0010(\u001a\u0004\b/\u0010*\"\u0004\b0\u0010,R(\u00103\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\n0\r8\u0000@\u0000X\u0080\u0004¢\u0006\f\n\u0004\b\b\u0010\u000e\u001a\u0004\b2\u0010\u0010¨\u00066"}, d2 = {"Lcom/discord/i18n/RenderContext;", "", "", "key", "Lkotlin/Function1;", "Landroid/view/View;", "", "onClick", "b", "(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V", "Lcom/discord/i18n/Hook;", "hookInitializer", "a", "", "Ljava/util/Map;", "getArgs", "()Ljava/util/Map;", "args", "", "d", "Ljava/lang/Integer;", "getBoldColor", "()Ljava/lang/Integer;", "setBoldColor", "(Ljava/lang/Integer;)V", "boldColor", "e", "getStrikethroughColor", "setStrikethroughColor", "strikethroughColor", "", "c", "Ljava/util/List;", "getOrderedArguments$i18n_release", "()Ljava/util/List;", "setOrderedArguments$i18n_release", "(Ljava/util/List;)V", "orderedArguments", "", "g", "Z", "getHasClickables$i18n_release", "()Z", "setHasClickables$i18n_release", "(Z)V", "hasClickables", "f", "getUppercase", "setUppercase", "uppercase", "getHooks$i18n_release", "hooks", HookHelper.constructorName, "()V", "i18n_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class RenderContext {
    public final Map<String, String> a = new HashMap();

    /* renamed from: b  reason: collision with root package name */
    public final Map<String, Hook> f2683b = new HashMap();
    public List<? extends Object> c;
    @ColorInt
    public Integer d;
    @ColorInt
    public Integer e;
    public boolean f;
    public boolean g;

    /* compiled from: RenderContext.kt */
    /* loaded from: classes.dex */
    public static final class a extends o implements Function1<Hook, Unit> {
        public final /* synthetic */ Function1 $onClick;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(Function1 function1) {
            super(1);
            this.$onClick = function1;
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(Hook hook) {
            Hook hook2 = hook;
            m.checkNotNullParameter(hook2, "$receiver");
            d dVar = new d(this);
            Objects.requireNonNull(hook2);
            m.checkNotNullParameter(dVar, "onClick");
            hook2.c = new Hook.a(null, dVar);
            return Unit.a;
        }
    }

    public final void a(String str, Function1<? super Hook, Unit> function1) {
        m.checkNotNullParameter(str, "key");
        m.checkNotNullParameter(function1, "hookInitializer");
        Map<String, Hook> map = this.f2683b;
        Hook hook = new Hook();
        function1.invoke(hook);
        map.put(str, hook);
    }

    public final void b(String str, Function1<? super View, Unit> function1) {
        m.checkNotNullParameter(str, "key");
        m.checkNotNullParameter(function1, "onClick");
        a(str, new a(function1));
    }
}
