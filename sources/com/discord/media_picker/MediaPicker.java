package com.discord.media_picker;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import androidx.core.content.FileProvider;
import d0.z.d.m;
import java.io.File;
import java.io.IOException;
import java.util.List;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
/* compiled from: MediaPicker.kt */
/* loaded from: classes.dex */
public final class MediaPicker {
    public static final MediaPicker a = new MediaPicker();

    /* compiled from: MediaPicker.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\u000f\u0010\u0003\u001a\u00020\u0002H&¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0006\u001a\u00020\u0005H&¢\u0006\u0004\b\u0006\u0010\u0007J!\u0010\r\u001a\u00020\f2\b\u0010\t\u001a\u0004\u0018\u00010\b2\u0006\u0010\u000b\u001a\u00020\nH&¢\u0006\u0004\b\r\u0010\u000e¨\u0006\u000f"}, d2 = {"Lcom/discord/media_picker/MediaPicker$Provider;", "", "Landroid/content/Context;", "requireContext", "()Landroid/content/Context;", "Ljava/io/File;", "getImageFile", "()Ljava/io/File;", "Landroid/content/Intent;", "intent", "", "requestCode", "", "startActivityForResult", "(Landroid/content/Intent;I)V", "media_picker_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public interface Provider {
        File getImageFile();

        Context requireContext();

        void startActivityForResult(Intent intent, int i);
    }

    /* compiled from: MediaPicker.kt */
    /* loaded from: classes.dex */
    public interface a {
        void a(Exception exc);

        void b(Uri uri, RequestType requestType);
    }

    public final Uri a(Provider provider) throws IOException {
        File imageFile = provider.getImageFile();
        Context requireContext = provider.requireContext();
        Uri uriForFile = FileProvider.getUriForFile(requireContext, requireContext.getPackageName().toString() + ".file-provider", imageFile);
        m.checkNotNullExpressionValue(uriForFile, "FileProvider.getUriForFi…context, authority, file)");
        e(requireContext, imageFile.toURI().toString());
        return uriForFile;
    }

    public final Uri b(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("picker", 0);
        m.checkNotNullExpressionValue(sharedPreferences, "context.getSharedPrefere…r\", Context.MODE_PRIVATE)");
        String string = sharedPreferences.getString("picker_uri", null);
        if (string != null) {
            e(context, null);
            Uri parse = Uri.parse(string);
            m.checkNotNullExpressionValue(parse, "Uri.parse(uriString)");
            return parse;
        }
        Uri uri = Uri.EMPTY;
        m.checkNotNullExpressionValue(uri, "Uri.EMPTY");
        return uri;
    }

    @SuppressLint({"QueryPermissionsNeeded"})
    public final void c(Context context, Intent intent, Uri uri) {
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 65536);
        m.checkNotNullExpressionValue(queryIntentActivities, "context\n        .package…nager.MATCH_DEFAULT_ONLY)");
        for (ResolveInfo resolveInfo : queryIntentActivities) {
            context.grantUriPermission(resolveInfo.activityInfo.packageName, uri, 3);
        }
    }

    public final Uri d(Context context, RequestType requestType, Intent intent) throws IOException {
        Uri uri;
        Uri uri2;
        int ordinal = requestType.ordinal();
        if (ordinal == 0) {
            return b(context);
        }
        if (ordinal == 1 || ordinal == 2) {
            if (intent == null || intent.getData() == null) {
                throw new IOException("Picker returned no data result.");
            }
            Uri data = intent.getData();
            if (data == null) {
                data = Uri.EMPTY;
            }
            m.checkNotNullExpressionValue(data, "if (data == null || data…ta ?: Uri.EMPTY\n        }");
            return data;
        } else if (ordinal == 3) {
            if (intent != null) {
                uri = (Uri) intent.getParcelableExtra("com.yalantis.ucrop.OutputUri");
                if (uri == null) {
                    uri = Uri.EMPTY;
                }
            } else {
                uri = Uri.EMPTY;
            }
            m.checkNotNullExpressionValue(uri, "if (data != null) {\n    …      Uri.EMPTY\n        }");
            return uri;
        } else if (ordinal == 4) {
            if (intent == null || intent.getData() == null) {
                uri2 = b(context);
            } else {
                uri2 = intent.getData();
                if (uri2 == null) {
                    uri2 = Uri.EMPTY;
                }
            }
            m.checkNotNullExpressionValue(uri2, "if (data != null && data…dClear(context)\n        }");
            return uri2;
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    public final void e(Context context, String str) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("picker", 0);
        m.checkNotNullExpressionValue(sharedPreferences, "context.getSharedPrefere…r\", Context.MODE_PRIVATE)");
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("picker_uri", str);
        edit.apply();
    }
}
