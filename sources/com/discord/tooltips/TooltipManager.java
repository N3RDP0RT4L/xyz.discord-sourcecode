package com.discord.tooltips;

import android.content.SharedPreferences;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.MainThread;
import androidx.core.view.ViewCompat;
import b.a.j.a;
import b.a.j.c;
import b.a.j.d;
import b.a.j.f;
import b.a.v.e;
import com.discord.floating_view_manager.FloatingViewGravity;
import d0.g;
import d0.z.d.m;
import d0.z.d.o;
import java.lang.ref.WeakReference;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: TooltipManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010#\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0016\u0018\u00002\u00020\u0001:\u0002\u0015\u0016J]\u0010\u0011\u001a\u00020\u000f2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\b\u001a\u00020\u00072\b\b\u0002\u0010\n\u001a\u00020\t2\b\b\u0002\u0010\u000b\u001a\u00020\t2\b\b\u0002\u0010\r\u001a\u00020\f2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000eH\u0007¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0013\u001a\u00020\u000f2\u0006\u0010\u0006\u001a\u00020\u0005H\u0007¢\u0006\u0004\b\u0013\u0010\u0014J\u0017\u0010\u0015\u001a\u00020\u000f2\u0006\u0010\u0006\u001a\u00020\u0005H\u0007¢\u0006\u0004\b\u0015\u0010\u0014J!\u0010\u0016\u001a\u00020\f2\u0006\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\r\u001a\u00020\fH\u0007¢\u0006\u0004\b\u0016\u0010\u0017R\u0016\u0010\u001a\u001a\u00020\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0019R\"\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\t0\u001b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0015\u0010\u001dR\u0016\u0010 \u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u001fR\u001c\u0010#\u001a\b\u0012\u0004\u0012\u00020\u001c0!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\"R\u0016\u0010'\u001a\u00020$8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b%\u0010&¨\u0006("}, d2 = {"Lcom/discord/tooltips/TooltipManager;", "", "Landroid/view/View;", "anchorView", "tooltipView", "Lcom/discord/tooltips/TooltipManager$b;", "tooltip", "Lcom/discord/floating_view_manager/FloatingViewGravity;", "tooltipGravity", "", "xOffset", "yOffset", "", "ignoreMaxTooltips", "Lrx/Observable;", "", "componentPausedObservable", "d", "(Landroid/view/View;Landroid/view/View;Lcom/discord/tooltips/TooltipManager$b;Lcom/discord/floating_view_manager/FloatingViewGravity;IIZLrx/Observable;)V", "c", "(Lcom/discord/tooltips/TooltipManager$b;)V", "a", "b", "(Lcom/discord/tooltips/TooltipManager$b;Z)Z", "Lb/a/v/a;", "Lb/a/v/a;", "acknowledgedTooltipsCache", "", "", "Ljava/util/Map;", "tooltipNameToTooltipViewIdMap", "I", "maxTooltipsPerColdStart", "", "Ljava/util/Set;", "shownTooltipNames", "Lb/a/j/a;", "e", "Lb/a/j/a;", "floatingViewManager", "tooltips_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public class TooltipManager {
    public Map<String, Integer> a;

    /* renamed from: b  reason: collision with root package name */
    public final b.a.v.a f2786b;
    public final Set<String> c;
    public final int d;
    public final b.a.j.a e;

    /* compiled from: TooltipManager.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public static WeakReference<TooltipManager> a;

        /* renamed from: b  reason: collision with root package name */
        public static final Lazy f2787b = g.lazy(C0221a.j);
        public static final Lazy c = g.lazy(b.j);
        public static final a d = null;

        /* compiled from: TooltipManager.kt */
        /* renamed from: com.discord.tooltips.TooltipManager$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C0221a extends o implements Function0<b.a.v.a> {
            public static final C0221a j = new C0221a();

            public C0221a() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            public b.a.v.a invoke() {
                return new b.a.v.a(null, 1);
            }
        }

        /* compiled from: TooltipManager.kt */
        /* loaded from: classes.dex */
        public static final class b extends o implements Function0<Set<String>> {
            public static final b j = new b();

            public b() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            public Set<String> invoke() {
                return new LinkedHashSet();
            }
        }
    }

    /* compiled from: TooltipManager.kt */
    /* loaded from: classes.dex */
    public static class b {
        private final String cacheKey;
        private final String tooltipName;

        public b(String str, String str2) {
            m.checkNotNullParameter(str2, "tooltipName");
            this.cacheKey = str;
            this.tooltipName = str2;
        }

        public final String getCacheKey() {
            return this.cacheKey;
        }

        public final String getTooltipName() {
            return this.tooltipName;
        }

        public /* synthetic */ b(String str, String str2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : str, str2);
        }
    }

    public TooltipManager(b.a.v.a aVar, Set set, int i, b.a.j.a aVar2, int i2) {
        i = (i2 & 4) != 0 ? 1 : i;
        m.checkNotNullParameter(aVar, "acknowledgedTooltipsCache");
        m.checkNotNullParameter(set, "shownTooltipNames");
        m.checkNotNullParameter(aVar2, "floatingViewManager");
        this.f2786b = aVar;
        this.c = set;
        this.d = i;
        this.e = aVar2;
        aVar2.a = new e(this);
        this.a = new LinkedHashMap();
    }

    @MainThread
    public final void a(b bVar) {
        m.checkNotNullParameter(bVar, "tooltip");
        c(bVar);
        String cacheKey = bVar.getCacheKey();
        if (cacheKey != null) {
            b.a.v.a aVar = this.f2786b;
            Objects.requireNonNull(aVar);
            m.checkNotNullParameter(cacheKey, "tooltipCacheKey");
            if (!aVar.a.getBoolean(cacheKey, false)) {
                b.a.v.a aVar2 = this.f2786b;
                Objects.requireNonNull(aVar2);
                m.checkNotNullParameter(cacheKey, "tooltipCacheKey");
                SharedPreferences.Editor edit = aVar2.a.edit();
                m.checkNotNullExpressionValue(edit, "editor");
                edit.putBoolean(cacheKey, true);
                edit.apply();
            }
        }
    }

    @MainThread
    public final boolean b(b bVar, boolean z2) {
        m.checkNotNullParameter(bVar, "tooltip");
        String cacheKey = bVar.getCacheKey();
        if (cacheKey != null) {
            b.a.v.a aVar = this.f2786b;
            Objects.requireNonNull(aVar);
            m.checkNotNullParameter(cacheKey, "tooltipCacheKey");
            boolean z3 = aVar.a.getBoolean(cacheKey, false);
            boolean contains = this.c.contains(bVar.getTooltipName());
            int size = this.c.size();
            if (z3) {
                return false;
            }
            if (!contains && !z2 && size >= this.d) {
                return false;
            }
        }
        return true;
    }

    @MainThread
    public final void c(b bVar) {
        m.checkNotNullParameter(bVar, "tooltip");
        Integer num = this.a.get(bVar.getTooltipName());
        if (num != null) {
            this.e.b(num.intValue());
        }
    }

    @MainThread
    public final void d(View view, View view2, b bVar, FloatingViewGravity floatingViewGravity, int i, int i2, boolean z2, Observable<Unit> observable) {
        m.checkNotNullParameter(view, "anchorView");
        m.checkNotNullParameter(view2, "tooltipView");
        m.checkNotNullParameter(bVar, "tooltip");
        m.checkNotNullParameter(floatingViewGravity, "tooltipGravity");
        m.checkNotNullParameter(observable, "componentPausedObservable");
        if (b(bVar, z2)) {
            c(bVar);
            this.c.add(bVar.getTooltipName());
            this.a.put(bVar.getTooltipName(), Integer.valueOf(view2.getId()));
            b.a.j.a aVar = this.e;
            Objects.requireNonNull(aVar);
            m.checkNotNullParameter(view, "anchorView");
            m.checkNotNullParameter(view2, "floatingView");
            m.checkNotNullParameter(floatingViewGravity, "floatingViewGravity");
            m.checkNotNullParameter(observable, "componentPausedObservable");
            View rootView = view.getRootView();
            Objects.requireNonNull(rootView, "null cannot be cast to non-null type android.view.ViewGroup");
            ViewGroup viewGroup = (ViewGroup) rootView;
            view2.setVisibility(4);
            if (!aVar.f239b.containsKey(Integer.valueOf(view2.getId()))) {
                viewGroup.addView(view2);
            }
            if (!ViewCompat.isLaidOut(view2) || view2.isLayoutRequested()) {
                view2.addOnLayoutChangeListener(new b.a.j.b(aVar, view, view2, floatingViewGravity, i, i2));
            } else if (!ViewCompat.isLaidOut(view) || view.isLayoutRequested()) {
                view.addOnLayoutChangeListener(new c(aVar, view, view2, floatingViewGravity, i, i2));
            } else {
                b.a.j.a.a(aVar, view2, view, floatingViewGravity, i, i2);
                view2.setVisibility(0);
            }
            f fVar = new f(aVar, view2, view, floatingViewGravity, i, i2);
            viewGroup.getViewTreeObserver().addOnPreDrawListener(fVar);
            aVar.f239b.put(Integer.valueOf(view2.getId()), new a.C0032a(view2, viewGroup, fVar));
            observable.Z(1).W(new d(aVar, view2), new b.a.j.e(aVar));
        }
    }
}
