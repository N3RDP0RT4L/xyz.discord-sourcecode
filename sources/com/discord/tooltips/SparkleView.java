package com.discord.tooltips;

import andhook.lib.HookHelper;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat;
import b.a.i.o1;
import b.a.v.c;
import b.a.v.d;
import d0.g;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: SparkleView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u001d\b\u0016\u0012\u0006\u0010\u0014\u001a\u00020\u0013\u0012\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u0015¢\u0006\u0004\b\u0017\u0010\u0018J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004R\u001f\u0010\n\u001a\u0004\u0018\u00010\u00058B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0006\u0010\u0007\u001a\u0004\b\b\u0010\tR\u0016\u0010\u000e\u001a\u00020\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\rR\u0016\u0010\u0012\u001a\u00020\u000f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011¨\u0006\u0019"}, d2 = {"Lcom/discord/tooltips/SparkleView;", "Landroid/widget/FrameLayout;", "", "b", "()V", "Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;", "l", "Lkotlin/Lazy;", "getSparkleDrawable", "()Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;", "sparkleDrawable", "Lb/a/i/o1;", "j", "Lb/a/i/o1;", "binding", "", "k", "I", "sparkleAnimationResId", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class SparkleView extends FrameLayout {
    public final o1 j;
    public int k;
    public final Lazy l;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SparkleView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        LayoutInflater.from(getContext()).inflate(R.layout.sparkle_view, this);
        ImageView imageView = (ImageView) findViewById(R.id.sparkle_view_image);
        if (imageView != null) {
            o1 o1Var = new o1(this, imageView);
            m.checkNotNullExpressionValue(o1Var, "SparkleViewBinding.infla…ater.from(context), this)");
            this.j = o1Var;
            this.k = R.drawable.sparkle_animated_vector;
            this.l = g.lazy(new c(this));
            setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
            imageView.setImageDrawable(getSparkleDrawable());
            AnimatedVectorDrawableCompat sparkleDrawable = getSparkleDrawable();
            if (sparkleDrawable != null) {
                sparkleDrawable.registerAnimationCallback(new d(this));
            }
            AnimatedVectorDrawableCompat sparkleDrawable2 = getSparkleDrawable();
            if (sparkleDrawable2 != null) {
                sparkleDrawable2.start();
                return;
            }
            return;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(getResources().getResourceName(R.id.sparkle_view_image)));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final AnimatedVectorDrawableCompat getSparkleDrawable() {
        return (AnimatedVectorDrawableCompat) this.l.getValue();
    }

    public final void b() {
        AnimatedVectorDrawableCompat sparkleDrawable = getSparkleDrawable();
        if (sparkleDrawable != null) {
            sparkleDrawable.stop();
        }
    }
}
