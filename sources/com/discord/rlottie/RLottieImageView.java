package com.discord.rlottie;

import andhook.lib.HookHelper;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Display;
import android.view.WindowManager;
import androidx.appcompat.widget.AppCompatImageView;
import com.discord.rlottie.RLottieDrawable;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.TypeCastException;
/* compiled from: RLottieImageView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0016\u0018\u00002\u00020\u0001B\u001b\b\u0016\u0012\u0006\u0010#\u001a\u00020\u0015\u0012\b\u0010%\u001a\u0004\u0018\u00010$¢\u0006\u0004\b&\u0010'J%\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\t\u001a\u00020\u0006H\u0014¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\u0006H\u0014¢\u0006\u0004\b\u000b\u0010\nJ\u0015\u0010\u000e\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u000e\u0010\u000fJ\u0015\u0010\u0012\u001a\u00020\u00062\u0006\u0010\u0011\u001a\u00020\u0010¢\u0006\u0004\b\u0012\u0010\u0013J\r\u0010\u0014\u001a\u00020\u0006¢\u0006\u0004\b\u0014\u0010\nJ\u0013\u0010\u0017\u001a\u00020\u0016*\u00020\u0015H\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u0016\u0010\u001c\u001a\u00020\u00198\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001a\u0010\u001bR\u0016\u0010\u001e\u001a\u00020\u00198\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001d\u0010\u001bR\u0018\u0010\"\u001a\u0004\u0018\u00010\u001f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b \u0010!¨\u0006("}, d2 = {"Lcom/discord/rlottie/RLottieImageView;", "Landroidx/appcompat/widget/AppCompatImageView;", "", "resId", "w", "h", "", "c", "(III)V", "onAttachedToWindow", "()V", "onDetachedFromWindow", "Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;", "playbackMode", "setPlaybackMode", "(Lcom/discord/rlottie/RLottieDrawable$PlaybackMode;)V", "", "progress", "setProgress", "(F)V", "b", "Landroid/content/Context;", "Landroid/view/Display;", "a", "(Landroid/content/Context;)Landroid/view/Display;", "", "k", "Z", "attachedToWindow", "l", "playing", "Lcom/discord/rlottie/RLottieDrawable;", "j", "Lcom/discord/rlottie/RLottieDrawable;", "drawable", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "rlottie_release"}, k = 1, mv = {1, 4, 0})
/* loaded from: classes.dex */
public class RLottieImageView extends AppCompatImageView {
    public RLottieDrawable j;
    public boolean k;
    public boolean l;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public RLottieImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkParameterIsNotNull(context, "context");
        RLottieDrawable.PlaybackMode playbackMode = RLottieDrawable.PlaybackMode.FREEZE;
    }

    @SuppressLint({"AnnotateVersionCheck"})
    public final Display a(Context context) {
        if (Build.VERSION.SDK_INT >= 30) {
            Display display = context.getDisplay();
            if (display == null) {
                m.throwNpe();
            }
            m.checkExpressionValueIsNotNull(display, "display!!");
            return display;
        }
        Object systemService = context.getSystemService("window");
        if (systemService != null) {
            Display defaultDisplay = ((WindowManager) systemService).getDefaultDisplay();
            m.checkExpressionValueIsNotNull(defaultDisplay, "(getSystemService(Contex…owManager).defaultDisplay");
            return defaultDisplay;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.view.WindowManager");
    }

    public final void b() {
        RLottieDrawable rLottieDrawable = this.j;
        if (rLottieDrawable != null) {
            this.l = true;
            if (this.k && rLottieDrawable != null) {
                rLottieDrawable.start();
            }
        }
    }

    public final void c(int i, int i2, int i3) {
        RLottieDrawable.PlaybackMode playbackMode = RLottieDrawable.PlaybackMode.LOOP;
        m.checkParameterIsNotNull(playbackMode, "playbackMode");
        Context context = getContext();
        m.checkExpressionValueIsNotNull(context, "context");
        String valueOf = String.valueOf(i);
        Context context2 = getContext();
        m.checkExpressionValueIsNotNull(context2, "context");
        RLottieDrawable rLottieDrawable = new RLottieDrawable(context, i, valueOf, i2, i3, a(context2).getRefreshRate(), false, (int[]) null);
        this.j = rLottieDrawable;
        rLottieDrawable.f(playbackMode);
        RLottieDrawable rLottieDrawable2 = this.j;
        if (rLottieDrawable2 != null) {
            rLottieDrawable2.e(true);
        }
        setImageDrawable(this.j);
    }

    @Override // android.widget.ImageView, android.view.View
    public void onAttachedToWindow() {
        RLottieDrawable rLottieDrawable;
        super.onAttachedToWindow();
        this.k = true;
        if (this.l && (rLottieDrawable = this.j) != null) {
            rLottieDrawable.start();
        }
    }

    @Override // android.widget.ImageView, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.k = false;
        RLottieDrawable rLottieDrawable = this.j;
        if (rLottieDrawable != null) {
            rLottieDrawable.P = false;
        }
    }

    public final void setPlaybackMode(RLottieDrawable.PlaybackMode playbackMode) {
        m.checkParameterIsNotNull(playbackMode, "playbackMode");
        RLottieDrawable rLottieDrawable = this.j;
        if (rLottieDrawable != null) {
            rLottieDrawable.f(playbackMode);
        }
    }

    public final void setProgress(float f) {
        RLottieDrawable rLottieDrawable = this.j;
        if (rLottieDrawable != null) {
            if (f < 0.0f) {
                f = 0.0f;
            } else if (f > 1.0f) {
                f = 1.0f;
            }
            rLottieDrawable.I = (int) (rLottieDrawable.r[0] * f);
            rLottieDrawable.f2741z = false;
            rLottieDrawable.G = false;
            if (!rLottieDrawable.d()) {
                rLottieDrawable.H = true;
            }
            rLottieDrawable.invalidateSelf();
        }
    }
}
