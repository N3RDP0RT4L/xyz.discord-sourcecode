package com.discord.models.ktx;

import androidx.core.app.Person;
import com.discord.models.user.User;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ModelUserExtensions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a\u0011\u0010\u0002\u001a\u00020\u0001*\u00020\u0000¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/models/user/User;", "Landroidx/core/app/Person;", "toPerson", "(Lcom/discord/models/user/User;)Landroidx/core/app/Person;", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelUserExtensionsKt {
    public static final Person toPerson(User user) {
        m.checkNotNullParameter(user, "$this$toPerson");
        Person build = new Person.Builder().setName(user.getUsername()).setKey(String.valueOf(user.getId())).setBot(user.isBot()).build();
        m.checkNotNullExpressionValue(build, "Person.Builder()\n       …t(isBot)\n        .build()");
        return build;
    }
}
