package com.discord.models.gifpicker.dto;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TrendingGifPreviewDto.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000e\u001a\u00020\r2\b\u0010\f\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u000e\u0010\u000fR\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0010\u001a\u0004\b\u0011\u0010\u0004¨\u0006\u0014"}, d2 = {"Lcom/discord/models/gifpicker/dto/TrendingGifPreviewDto;", "", "", "component1", "()Ljava/lang/String;", "src", "copy", "(Ljava/lang/String;)Lcom/discord/models/gifpicker/dto/TrendingGifPreviewDto;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getSrc", HookHelper.constructorName, "(Ljava/lang/String;)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class TrendingGifPreviewDto {
    private final String src;

    public TrendingGifPreviewDto(String str) {
        m.checkNotNullParameter(str, "src");
        this.src = str;
    }

    public static /* synthetic */ TrendingGifPreviewDto copy$default(TrendingGifPreviewDto trendingGifPreviewDto, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            str = trendingGifPreviewDto.src;
        }
        return trendingGifPreviewDto.copy(str);
    }

    public final String component1() {
        return this.src;
    }

    public final TrendingGifPreviewDto copy(String str) {
        m.checkNotNullParameter(str, "src");
        return new TrendingGifPreviewDto(str);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof TrendingGifPreviewDto) && m.areEqual(this.src, ((TrendingGifPreviewDto) obj).src);
        }
        return true;
    }

    public final String getSrc() {
        return this.src;
    }

    public int hashCode() {
        String str = this.src;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    public String toString() {
        return a.H(a.R("TrendingGifPreviewDto(src="), this.src, ")");
    }
}
