package com.discord.models.gifpicker.dto;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ModelGif.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u0000 \u001e2\u00020\u0001:\u0001\u001eB'\u0012\u0006\u0010\n\u001a\u00020\u0002\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\u0006\u0010\f\u001a\u00020\u0006\u0012\u0006\u0010\r\u001a\u00020\u0006¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\t\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\t\u0010\bJ8\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u00022\b\b\u0002\u0010\u000b\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\u00062\b\b\u0002\u0010\r\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0004J\u0010\u0010\u0011\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0011\u0010\bJ\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\f\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0016\u001a\u0004\b\u0017\u0010\bR\u0019\u0010\r\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u0016\u001a\u0004\b\u0018\u0010\bR\u0019\u0010\n\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004R\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0019\u001a\u0004\b\u001b\u0010\u0004¨\u0006\u001f"}, d2 = {"Lcom/discord/models/gifpicker/dto/ModelGif;", "", "", "component1", "()Ljava/lang/String;", "component2", "", "component3", "()I", "component4", "gifImageUrl", "tenorGifUrl", "width", "height", "copy", "(Ljava/lang/String;Ljava/lang/String;II)Lcom/discord/models/gifpicker/dto/ModelGif;", "toString", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getWidth", "getHeight", "Ljava/lang/String;", "getGifImageUrl", "getTenorGifUrl", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;II)V", "Companion", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelGif {
    public static final Companion Companion = new Companion(null);
    private final String gifImageUrl;
    private final int height;
    private final String tenorGifUrl;
    private final int width;

    /* compiled from: ModelGif.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/models/gifpicker/dto/ModelGif$Companion;", "", "Lcom/discord/models/gifpicker/dto/GifDto;", "gifDto", "Lcom/discord/models/gifpicker/dto/ModelGif;", "createFromGifDto", "(Lcom/discord/models/gifpicker/dto/GifDto;)Lcom/discord/models/gifpicker/dto/ModelGif;", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public final ModelGif createFromGifDto(GifDto gifDto) {
            m.checkNotNullParameter(gifDto, "gifDto");
            return new ModelGif(gifDto.getSrc(), gifDto.getUrl(), gifDto.getWidth(), gifDto.getHeight());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public ModelGif(String str, String str2, int i, int i2) {
        m.checkNotNullParameter(str, "gifImageUrl");
        m.checkNotNullParameter(str2, "tenorGifUrl");
        this.gifImageUrl = str;
        this.tenorGifUrl = str2;
        this.width = i;
        this.height = i2;
    }

    public static /* synthetic */ ModelGif copy$default(ModelGif modelGif, String str, String str2, int i, int i2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            str = modelGif.gifImageUrl;
        }
        if ((i3 & 2) != 0) {
            str2 = modelGif.tenorGifUrl;
        }
        if ((i3 & 4) != 0) {
            i = modelGif.width;
        }
        if ((i3 & 8) != 0) {
            i2 = modelGif.height;
        }
        return modelGif.copy(str, str2, i, i2);
    }

    public final String component1() {
        return this.gifImageUrl;
    }

    public final String component2() {
        return this.tenorGifUrl;
    }

    public final int component3() {
        return this.width;
    }

    public final int component4() {
        return this.height;
    }

    public final ModelGif copy(String str, String str2, int i, int i2) {
        m.checkNotNullParameter(str, "gifImageUrl");
        m.checkNotNullParameter(str2, "tenorGifUrl");
        return new ModelGif(str, str2, i, i2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModelGif)) {
            return false;
        }
        ModelGif modelGif = (ModelGif) obj;
        return m.areEqual(this.gifImageUrl, modelGif.gifImageUrl) && m.areEqual(this.tenorGifUrl, modelGif.tenorGifUrl) && this.width == modelGif.width && this.height == modelGif.height;
    }

    public final String getGifImageUrl() {
        return this.gifImageUrl;
    }

    public final int getHeight() {
        return this.height;
    }

    public final String getTenorGifUrl() {
        return this.tenorGifUrl;
    }

    public final int getWidth() {
        return this.width;
    }

    public int hashCode() {
        String str = this.gifImageUrl;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.tenorGifUrl;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return ((((hashCode + i) * 31) + this.width) * 31) + this.height;
    }

    public String toString() {
        StringBuilder R = a.R("ModelGif(gifImageUrl=");
        R.append(this.gifImageUrl);
        R.append(", tenorGifUrl=");
        R.append(this.tenorGifUrl);
        R.append(", width=");
        R.append(this.width);
        R.append(", height=");
        return a.A(R, this.height, ")");
    }
}
