package com.discord.models.domain;

import andhook.lib.HookHelper;
import com.discord.api.premium.SubscriptionInterval;
import com.discord.models.domain.ModelSubscription;
import com.discord.models.domain.premium.SubscriptionPlanType;
import d0.t.o;
import d0.t.u;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
/* compiled from: ModelSubscription.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\b&\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u000f\u0010\u0010J\u0011\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0013\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\u0004\b\b\u0010\tJ\u0015\u0010\n\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001c\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\f0\u00068&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\t¨\u0006\u0011"}, d2 = {"Lcom/discord/models/domain/HasSubscriptionItems;", "", "", "Lcom/discord/primitives/PlanId;", "getPremiumBasePlanId", "()J", "", "Lcom/discord/models/domain/ModelSubscription$SubscriptionAdditionalPlan;", "getPremiumAdditionalPlans", "()Ljava/util/List;", "getGuildBoostPlanId", "()Ljava/lang/Long;", "Lcom/discord/models/domain/ModelSubscription$SubscriptionItem;", "getItems", "items", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public abstract class HasSubscriptionItems {
    public final Long getGuildBoostPlanId() {
        ModelSubscription.SubscriptionAdditionalPlan subscriptionAdditionalPlan = (ModelSubscription.SubscriptionAdditionalPlan) u.firstOrNull((List<? extends Object>) getPremiumAdditionalPlans());
        if (subscriptionAdditionalPlan != null) {
            return Long.valueOf(subscriptionAdditionalPlan.getPlanId());
        }
        return null;
    }

    public abstract List<ModelSubscription.SubscriptionItem> getItems();

    public final List<ModelSubscription.SubscriptionAdditionalPlan> getPremiumAdditionalPlans() {
        List<ModelSubscription.SubscriptionItem> items = getItems();
        ArrayList<ModelSubscription.SubscriptionItem> arrayList = new ArrayList();
        for (Object obj : items) {
            if (((ModelSubscription.SubscriptionItem) obj).getPlanId() != getPremiumBasePlanId()) {
                arrayList.add(obj);
            }
        }
        ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(arrayList, 10));
        for (ModelSubscription.SubscriptionItem subscriptionItem : arrayList) {
            arrayList2.add(new ModelSubscription.SubscriptionAdditionalPlan(subscriptionItem.getPlanId(), subscriptionItem.getQuantity()));
        }
        return arrayList2;
    }

    public final long getPremiumBasePlanId() {
        ModelSubscription.SubscriptionItem subscriptionItem = (ModelSubscription.SubscriptionItem) u.firstOrNull((List<? extends Object>) getItems());
        Long valueOf = subscriptionItem != null ? Long.valueOf(subscriptionItem.getPlanId()) : null;
        return SubscriptionPlanType.Companion.getBasePlanFromSubscriptionItems(getItems(), valueOf != null ? SubscriptionPlanType.Companion.from(valueOf.longValue()).getInterval() : SubscriptionInterval.MONTHLY).getPlanId();
    }
}
