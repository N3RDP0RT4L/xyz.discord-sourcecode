package com.discord.models.domain;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.guildjoinrequest.ApplicationStatus;
import com.discord.models.domain.ModelMemberVerificationForm;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: ModelMemberVerificationFormResponse.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B9\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\u0011\u001a\u00020\u0005\u0012\u000e\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\b\u0012\u000e\u0010\u0013\u001a\n\u0018\u00010\fj\u0004\u0018\u0001`\r¢\u0006\u0004\b&\u0010'J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0018\u0010\n\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0018\u0010\u000e\u001a\n\u0018\u00010\fj\u0004\u0018\u0001`\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJJ\u0010\u0014\u001a\u00020\u00002\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\u0011\u001a\u00020\u00052\u0010\b\u0002\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\b2\u0010\b\u0002\u0010\u0013\u001a\n\u0018\u00010\fj\u0004\u0018\u0001`\rHÆ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0004J\u0010\u0010\u0018\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u001a\u0010\u001c\u001a\u00020\u001b2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR!\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u001e\u001a\u0004\b\u001f\u0010\u000bR\u0019\u0010\u0011\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010 \u001a\u0004\b!\u0010\u0007R\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\"\u001a\u0004\b#\u0010\u0004R!\u0010\u0013\u001a\n\u0018\u00010\fj\u0004\u0018\u0001`\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010$\u001a\u0004\b%\u0010\u000f¨\u0006("}, d2 = {"Lcom/discord/models/domain/ModelMemberVerificationFormResponse;", "", "", "component1", "()Ljava/lang/String;", "Lcom/discord/api/guildjoinrequest/ApplicationStatus;", "component2", "()Lcom/discord/api/guildjoinrequest/ApplicationStatus;", "", "Lcom/discord/models/domain/ModelMemberVerificationForm$FormField;", "component3", "()Ljava/util/List;", "", "Lcom/discord/primitives/GuildId;", "component4", "()Ljava/lang/Long;", "lastSeen", "applicationStatus", "formResponses", "guildId", "copy", "(Ljava/lang/String;Lcom/discord/api/guildjoinrequest/ApplicationStatus;Ljava/util/List;Ljava/lang/Long;)Lcom/discord/models/domain/ModelMemberVerificationFormResponse;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getFormResponses", "Lcom/discord/api/guildjoinrequest/ApplicationStatus;", "getApplicationStatus", "Ljava/lang/String;", "getLastSeen", "Ljava/lang/Long;", "getGuildId", HookHelper.constructorName, "(Ljava/lang/String;Lcom/discord/api/guildjoinrequest/ApplicationStatus;Ljava/util/List;Ljava/lang/Long;)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelMemberVerificationFormResponse {
    private final ApplicationStatus applicationStatus;
    private final List<ModelMemberVerificationForm.FormField> formResponses;
    private final Long guildId;
    private final String lastSeen;

    public ModelMemberVerificationFormResponse(String str, ApplicationStatus applicationStatus, List<ModelMemberVerificationForm.FormField> list, Long l) {
        m.checkNotNullParameter(applicationStatus, "applicationStatus");
        this.lastSeen = str;
        this.applicationStatus = applicationStatus;
        this.formResponses = list;
        this.guildId = l;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ ModelMemberVerificationFormResponse copy$default(ModelMemberVerificationFormResponse modelMemberVerificationFormResponse, String str, ApplicationStatus applicationStatus, List list, Long l, int i, Object obj) {
        if ((i & 1) != 0) {
            str = modelMemberVerificationFormResponse.lastSeen;
        }
        if ((i & 2) != 0) {
            applicationStatus = modelMemberVerificationFormResponse.applicationStatus;
        }
        if ((i & 4) != 0) {
            list = modelMemberVerificationFormResponse.formResponses;
        }
        if ((i & 8) != 0) {
            l = modelMemberVerificationFormResponse.guildId;
        }
        return modelMemberVerificationFormResponse.copy(str, applicationStatus, list, l);
    }

    public final String component1() {
        return this.lastSeen;
    }

    public final ApplicationStatus component2() {
        return this.applicationStatus;
    }

    public final List<ModelMemberVerificationForm.FormField> component3() {
        return this.formResponses;
    }

    public final Long component4() {
        return this.guildId;
    }

    public final ModelMemberVerificationFormResponse copy(String str, ApplicationStatus applicationStatus, List<ModelMemberVerificationForm.FormField> list, Long l) {
        m.checkNotNullParameter(applicationStatus, "applicationStatus");
        return new ModelMemberVerificationFormResponse(str, applicationStatus, list, l);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModelMemberVerificationFormResponse)) {
            return false;
        }
        ModelMemberVerificationFormResponse modelMemberVerificationFormResponse = (ModelMemberVerificationFormResponse) obj;
        return m.areEqual(this.lastSeen, modelMemberVerificationFormResponse.lastSeen) && m.areEqual(this.applicationStatus, modelMemberVerificationFormResponse.applicationStatus) && m.areEqual(this.formResponses, modelMemberVerificationFormResponse.formResponses) && m.areEqual(this.guildId, modelMemberVerificationFormResponse.guildId);
    }

    public final ApplicationStatus getApplicationStatus() {
        return this.applicationStatus;
    }

    public final List<ModelMemberVerificationForm.FormField> getFormResponses() {
        return this.formResponses;
    }

    public final Long getGuildId() {
        return this.guildId;
    }

    public final String getLastSeen() {
        return this.lastSeen;
    }

    public int hashCode() {
        String str = this.lastSeen;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        ApplicationStatus applicationStatus = this.applicationStatus;
        int hashCode2 = (hashCode + (applicationStatus != null ? applicationStatus.hashCode() : 0)) * 31;
        List<ModelMemberVerificationForm.FormField> list = this.formResponses;
        int hashCode3 = (hashCode2 + (list != null ? list.hashCode() : 0)) * 31;
        Long l = this.guildId;
        if (l != null) {
            i = l.hashCode();
        }
        return hashCode3 + i;
    }

    public String toString() {
        StringBuilder R = a.R("ModelMemberVerificationFormResponse(lastSeen=");
        R.append(this.lastSeen);
        R.append(", applicationStatus=");
        R.append(this.applicationStatus);
        R.append(", formResponses=");
        R.append(this.formResponses);
        R.append(", guildId=");
        return a.F(R, this.guildId, ")");
    }
}
