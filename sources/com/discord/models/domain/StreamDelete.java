package com.discord.models.domain;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.Model;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ModelApplicationStream.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001:\u0002 !B#\u0012\n\u0010\f\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\r\u001a\u00020\u0006\u0012\u0006\u0010\u000e\u001a\u00020\t¢\u0006\u0004\b\u001e\u0010\u001fJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ2\u0010\u000f\u001a\u00020\u00002\f\b\u0002\u0010\f\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\r\u001a\u00020\u00062\b\b\u0002\u0010\u000e\u001a\u00020\tHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0005J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0016\u001a\u00020\t2\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u0019\u0010\u000e\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u0018\u001a\u0004\b\u0019\u0010\u000bR\u001d\u0010\f\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001a\u001a\u0004\b\u001b\u0010\u0005R\u0019\u0010\r\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001c\u001a\u0004\b\u001d\u0010\b¨\u0006\""}, d2 = {"Lcom/discord/models/domain/StreamDelete;", "", "", "Lcom/discord/primitives/StreamKey;", "component1", "()Ljava/lang/String;", "Lcom/discord/models/domain/StreamDelete$Reason;", "component2", "()Lcom/discord/models/domain/StreamDelete$Reason;", "", "component3", "()Z", "streamKey", ModelAuditLogEntry.CHANGE_KEY_REASON, "unavailable", "copy", "(Ljava/lang/String;Lcom/discord/models/domain/StreamDelete$Reason;Z)Lcom/discord/models/domain/StreamDelete;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getUnavailable", "Ljava/lang/String;", "getStreamKey", "Lcom/discord/models/domain/StreamDelete$Reason;", "getReason", HookHelper.constructorName, "(Ljava/lang/String;Lcom/discord/models/domain/StreamDelete$Reason;Z)V", "Parser", "Reason", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StreamDelete {
    private final Reason reason;
    private final String streamKey;
    private final boolean unavailable;

    /* compiled from: ModelApplicationStream.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/models/domain/StreamDelete$Parser;", "Lcom/discord/models/domain/Model$Parser;", "Lcom/discord/models/domain/StreamDelete;", "Lcom/discord/models/domain/Model$JsonReader;", "reader", "parse", "(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/StreamDelete;", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Parser implements Model.Parser<StreamDelete> {
        public static final Parser INSTANCE = new Parser();

        private Parser() {
        }

        /* JADX WARN: Can't rename method to resolve collision */
        /* JADX WARN: Removed duplicated region for block: B:22:0x0067  */
        /* JADX WARN: Removed duplicated region for block: B:25:0x0075  */
        /* JADX WARN: Removed duplicated region for block: B:26:0x007a  */
        @Override // com.discord.models.domain.Model.Parser
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public com.discord.models.domain.StreamDelete parse(final com.discord.models.domain.Model.JsonReader r5) {
            /*
                r4 = this;
                java.lang.String r0 = "reader"
                kotlin.jvm.internal.Ref$ObjectRef r0 = b.d.b.a.a.a0(r5, r0)
                r1 = 0
                r0.element = r1
                kotlin.jvm.internal.Ref$ObjectRef r2 = new kotlin.jvm.internal.Ref$ObjectRef
                r2.<init>()
                r2.element = r1
                kotlin.jvm.internal.Ref$ObjectRef r3 = new kotlin.jvm.internal.Ref$ObjectRef
                r3.<init>()
                r3.element = r1
                com.discord.models.domain.StreamDelete$Parser$parse$1 r1 = new com.discord.models.domain.StreamDelete$Parser$parse$1
                r1.<init>()
                r5.nextObject(r1)
                T r5 = r2.element
                java.lang.String r5 = (java.lang.String) r5
                if (r5 != 0) goto L27
                goto L5f
            L27:
                int r1 = r5.hashCode()
                r2 = -2033943558(0xffffffff86c47bfa, float:-7.390917E-35)
                if (r1 == r2) goto L53
                r2 = 620910836(0x250258f4, float:1.1305841E-16)
                if (r1 == r2) goto L47
                r2 = 1627077614(0x60fb3bee, float:1.448266E20)
                if (r1 == r2) goto L3b
                goto L5f
            L3b:
                java.lang.String r1 = "stream_full"
                boolean r5 = r5.equals(r1)
                if (r5 == 0) goto L5f
                com.discord.models.domain.StreamDelete$Reason r5 = com.discord.models.domain.StreamDelete.Reason.STREAM_FULL
                goto L61
            L47:
                java.lang.String r1 = "unauthorized"
                boolean r5 = r5.equals(r1)
                if (r5 == 0) goto L5f
                com.discord.models.domain.StreamDelete$Reason r5 = com.discord.models.domain.StreamDelete.Reason.UNAUTHORIZED
                goto L61
            L53:
                java.lang.String r1 = "user_requested"
                boolean r5 = r5.equals(r1)
                if (r5 == 0) goto L5f
                com.discord.models.domain.StreamDelete$Reason r5 = com.discord.models.domain.StreamDelete.Reason.USER_REQUESTED
                goto L61
            L5f:
                com.discord.models.domain.StreamDelete$Reason r5 = com.discord.models.domain.StreamDelete.Reason.UNKNOWN
            L61:
                com.discord.models.domain.StreamDelete r1 = new com.discord.models.domain.StreamDelete
                T r0 = r0.element
                if (r0 != 0) goto L6d
                java.lang.String r2 = "streamKey"
                d0.z.d.m.throwUninitializedPropertyAccessException(r2)
            L6d:
                java.lang.String r0 = (java.lang.String) r0
                T r2 = r3.element
                java.lang.Boolean r2 = (java.lang.Boolean) r2
                if (r2 == 0) goto L7a
                boolean r2 = r2.booleanValue()
                goto L7b
            L7a:
                r2 = 0
            L7b:
                r1.<init>(r0, r5, r2)
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.models.domain.StreamDelete.Parser.parse(com.discord.models.domain.Model$JsonReader):com.discord.models.domain.StreamDelete");
        }
    }

    /* compiled from: ModelApplicationStream.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/discord/models/domain/StreamDelete$Reason;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "USER_REQUESTED", "STREAM_FULL", "UNAUTHORIZED", "UNKNOWN", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum Reason {
        USER_REQUESTED,
        STREAM_FULL,
        UNAUTHORIZED,
        UNKNOWN
    }

    public StreamDelete(String str, Reason reason, boolean z2) {
        m.checkNotNullParameter(str, "streamKey");
        m.checkNotNullParameter(reason, ModelAuditLogEntry.CHANGE_KEY_REASON);
        this.streamKey = str;
        this.reason = reason;
        this.unavailable = z2;
    }

    public static /* synthetic */ StreamDelete copy$default(StreamDelete streamDelete, String str, Reason reason, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = streamDelete.streamKey;
        }
        if ((i & 2) != 0) {
            reason = streamDelete.reason;
        }
        if ((i & 4) != 0) {
            z2 = streamDelete.unavailable;
        }
        return streamDelete.copy(str, reason, z2);
    }

    public final String component1() {
        return this.streamKey;
    }

    public final Reason component2() {
        return this.reason;
    }

    public final boolean component3() {
        return this.unavailable;
    }

    public final StreamDelete copy(String str, Reason reason, boolean z2) {
        m.checkNotNullParameter(str, "streamKey");
        m.checkNotNullParameter(reason, ModelAuditLogEntry.CHANGE_KEY_REASON);
        return new StreamDelete(str, reason, z2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StreamDelete)) {
            return false;
        }
        StreamDelete streamDelete = (StreamDelete) obj;
        return m.areEqual(this.streamKey, streamDelete.streamKey) && m.areEqual(this.reason, streamDelete.reason) && this.unavailable == streamDelete.unavailable;
    }

    public final Reason getReason() {
        return this.reason;
    }

    public final String getStreamKey() {
        return this.streamKey;
    }

    public final boolean getUnavailable() {
        return this.unavailable;
    }

    public int hashCode() {
        String str = this.streamKey;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Reason reason = this.reason;
        if (reason != null) {
            i = reason.hashCode();
        }
        int i2 = (hashCode + i) * 31;
        boolean z2 = this.unavailable;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        return i2 + i3;
    }

    public String toString() {
        StringBuilder R = a.R("StreamDelete(streamKey=");
        R.append(this.streamKey);
        R.append(", reason=");
        R.append(this.reason);
        R.append(", unavailable=");
        return a.M(R, this.unavailable, ")");
    }
}
