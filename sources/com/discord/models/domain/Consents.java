package com.discord.models.domain;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.restapi.RestAPIParams;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ModelUserConsents.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u0000 \u00192\u00020\u0001:\u0001\u0019B\u0017\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J$\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u00022\b\b\u0002\u0010\u0007\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u0019\u0010\u0006\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0004R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0014\u001a\u0004\b\u0016\u0010\u0004¨\u0006\u001a"}, d2 = {"Lcom/discord/models/domain/Consents;", "", "Lcom/discord/models/domain/Consent;", "component1", "()Lcom/discord/models/domain/Consent;", "component2", "usageStatistics", RestAPIParams.Consents.Type.PERSONALIZATION, "copy", "(Lcom/discord/models/domain/Consent;Lcom/discord/models/domain/Consent;)Lcom/discord/models/domain/Consents;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/Consent;", "getUsageStatistics", "getPersonalization", HookHelper.constructorName, "(Lcom/discord/models/domain/Consent;Lcom/discord/models/domain/Consent;)V", "Companion", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class Consents {
    public static final Companion Companion = new Companion(null);
    private static final Consents DEFAULT = new Consents(new Consent(false, null, 3, null), new Consent(false, null, 3, null));
    private final Consent personalization;
    private final Consent usageStatistics;

    /* compiled from: ModelUserConsents.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/models/domain/Consents$Companion;", "", "Lcom/discord/models/domain/Consents;", "DEFAULT", "Lcom/discord/models/domain/Consents;", "getDEFAULT", "()Lcom/discord/models/domain/Consents;", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public final Consents getDEFAULT() {
            return Consents.DEFAULT;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public Consents(Consent consent, Consent consent2) {
        m.checkNotNullParameter(consent, "usageStatistics");
        m.checkNotNullParameter(consent2, RestAPIParams.Consents.Type.PERSONALIZATION);
        this.usageStatistics = consent;
        this.personalization = consent2;
    }

    public static /* synthetic */ Consents copy$default(Consents consents, Consent consent, Consent consent2, int i, Object obj) {
        if ((i & 1) != 0) {
            consent = consents.usageStatistics;
        }
        if ((i & 2) != 0) {
            consent2 = consents.personalization;
        }
        return consents.copy(consent, consent2);
    }

    public final Consent component1() {
        return this.usageStatistics;
    }

    public final Consent component2() {
        return this.personalization;
    }

    public final Consents copy(Consent consent, Consent consent2) {
        m.checkNotNullParameter(consent, "usageStatistics");
        m.checkNotNullParameter(consent2, RestAPIParams.Consents.Type.PERSONALIZATION);
        return new Consents(consent, consent2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Consents)) {
            return false;
        }
        Consents consents = (Consents) obj;
        return m.areEqual(this.usageStatistics, consents.usageStatistics) && m.areEqual(this.personalization, consents.personalization);
    }

    public final Consent getPersonalization() {
        return this.personalization;
    }

    public final Consent getUsageStatistics() {
        return this.usageStatistics;
    }

    public int hashCode() {
        Consent consent = this.usageStatistics;
        int i = 0;
        int hashCode = (consent != null ? consent.hashCode() : 0) * 31;
        Consent consent2 = this.personalization;
        if (consent2 != null) {
            i = consent2.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder R = a.R("Consents(usageStatistics=");
        R.append(this.usageStatistics);
        R.append(", personalization=");
        R.append(this.personalization);
        R.append(")");
        return R.toString();
    }
}
