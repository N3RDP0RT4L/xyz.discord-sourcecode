package com.discord.models.domain.premium;

import andhook.lib.HookHelper;
import androidx.room.RoomDatabase;
import b.d.b.a.a;
import com.discord.api.premium.PremiumTier;
import com.discord.api.premium.SubscriptionInterval;
import com.discord.models.domain.ModelSubscription;
import d0.t.n0;
import d0.t.o;
import d0.t.o0;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* JADX WARN: Init of enum NONE_MONTH can be incorrect */
/* JADX WARN: Init of enum NONE_YEAR can be incorrect */
/* JADX WARN: Init of enum PREMIUM_GUILD_MONTH can be incorrect */
/* JADX WARN: Init of enum PREMIUM_GUILD_YEAR can be incorrect */
/* JADX WARN: Init of enum PREMIUM_MONTH_LEGACY can be incorrect */
/* JADX WARN: Init of enum PREMIUM_MONTH_TIER_1 can be incorrect */
/* JADX WARN: Init of enum PREMIUM_MONTH_TIER_2 can be incorrect */
/* JADX WARN: Init of enum PREMIUM_YEAR_LEGACY can be incorrect */
/* JADX WARN: Init of enum PREMIUM_YEAR_TIER_1 can be incorrect */
/* JADX WARN: Init of enum PREMIUM_YEAR_TIER_2 can be incorrect */
/* compiled from: SubscriptionPlanType.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0012\b\u0086\u0001\u0018\u0000 \"2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\"B5\b\u0002\u0012\u0006\u0010\u0017\u001a\u00020\u0016\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0011\u0012\u0006\u0010\r\u001a\u00020\f\u0012\b\u0010\u001c\u001a\u0004\u0018\u00010\u001b¢\u0006\u0004\b \u0010!J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\r\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0004J\r\u0010\u0006\u001a\u00020\u0002¢\u0006\u0004\b\u0006\u0010\u0004R\u0019\u0010\b\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR\u0019\u0010\r\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u0019\u0010\u0017\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001fj\u0002\b#j\u0002\b$j\u0002\b%j\u0002\b&j\u0002\b'j\u0002\b(j\u0002\b)j\u0002\b*j\u0002\b+j\u0002\b,¨\u0006-"}, d2 = {"Lcom/discord/models/domain/premium/SubscriptionPlanType;", "", "", "isGrandfathered", "()Z", "isPremiumSubscription", "isMonthlyInterval", "Lcom/discord/api/premium/SubscriptionInterval;", "interval", "Lcom/discord/api/premium/SubscriptionInterval;", "getInterval", "()Lcom/discord/api/premium/SubscriptionInterval;", "", "planId", "J", "getPlanId", "()J", "", "planTypeString", "Ljava/lang/String;", "getPlanTypeString", "()Ljava/lang/String;", "", "price", "I", "getPrice", "()I", "Lcom/discord/api/premium/PremiumTier;", "premiumTier", "Lcom/discord/api/premium/PremiumTier;", "getPremiumTier", "()Lcom/discord/api/premium/PremiumTier;", HookHelper.constructorName, "(Ljava/lang/String;IILcom/discord/api/premium/SubscriptionInterval;Ljava/lang/String;JLcom/discord/api/premium/PremiumTier;)V", "Companion", "NONE_MONTH", "NONE_YEAR", "PREMIUM_MONTH_LEGACY", "PREMIUM_YEAR_LEGACY", "PREMIUM_MONTH_TIER_1", "PREMIUM_YEAR_TIER_1", "PREMIUM_MONTH_TIER_2", "PREMIUM_YEAR_TIER_2", "PREMIUM_GUILD_MONTH", "PREMIUM_GUILD_YEAR", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public enum SubscriptionPlanType {
    NONE_MONTH(0, r11, "none_month", 628379151761408000L, r12),
    NONE_YEAR(0, r14, "none_year", 628381571568631808L, r12),
    PREMIUM_MONTH_LEGACY(499, r11, "premium_month", 511651856145973248L, r15),
    PREMIUM_YEAR_LEGACY(4999, r14, "premium_year", 511651860671627264L, r15),
    PREMIUM_MONTH_TIER_1(499, r11, "premium_month_tier_1", 511651871736201216L, r18),
    PREMIUM_YEAR_TIER_1(4999, r14, "premium_year_tier_1", 511651876987469824L, r18),
    PREMIUM_MONTH_TIER_2(RoomDatabase.MAX_BIND_PARAMETER_CNT, r11, "premium_month_tier_2", 511651880837840896L, r15),
    PREMIUM_YEAR_TIER_2(9999, r14, "premium_year_tier_2", 511651885459963904L, r15),
    PREMIUM_GUILD_MONTH(499, r11, null, 590665532894740483L, null),
    PREMIUM_GUILD_YEAR(4999, r14, null, 590665538238152709L, null);
    
    public static final Companion Companion = new Companion(null);
    private static final Set<SubscriptionPlanType> LEGACY_PLANS;
    private static final Set<SubscriptionPlanType> PREMIUM_GUILD_PLANS;
    private static final Set<SubscriptionPlanType> PREMIUM_PLANS;
    private static final Set<SubscriptionPlanType> TIER_1_PLANS;
    private static final Set<SubscriptionPlanType> TIER_2_PLANS;
    private final SubscriptionInterval interval;
    private final long planId;
    private final String planTypeString;
    private final PremiumTier premiumTier;
    private final int price;

    /* compiled from: SubscriptionPlanType.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\"\n\u0002\b\r\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001f\u0010 J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0019\u0010\n\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\b\n\u0010\u000bJ\u0015\u0010\n\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\n\u0010\u000eJ#\u0010\u0012\u001a\u00020\u00042\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f2\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0012\u0010\u0013R\u001f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00040\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018R\u001f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00040\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u0016\u001a\u0004\b\u001a\u0010\u0018R\u001f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00040\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u0016\u001a\u0004\b\u001c\u0010\u0018R\u001c\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00040\u00148\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u0016R\u001c\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00040\u00148\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001e\u0010\u0016¨\u0006!"}, d2 = {"Lcom/discord/models/domain/premium/SubscriptionPlanType$Companion;", "", "Lcom/discord/api/premium/SubscriptionInterval;", "interval", "Lcom/discord/models/domain/premium/SubscriptionPlanType;", "getNonePlanForIntervalType", "(Lcom/discord/api/premium/SubscriptionInterval;)Lcom/discord/models/domain/premium/SubscriptionPlanType;", "", "Lcom/discord/primitives/PlanId;", "planId", "from", "(J)Lcom/discord/models/domain/premium/SubscriptionPlanType;", "", "planTypeString", "(Ljava/lang/String;)Lcom/discord/models/domain/premium/SubscriptionPlanType;", "", "Lcom/discord/models/domain/ModelSubscription$SubscriptionItem;", "items", "getBasePlanFromSubscriptionItems", "(Ljava/util/List;Lcom/discord/api/premium/SubscriptionInterval;)Lcom/discord/models/domain/premium/SubscriptionPlanType;", "", "LEGACY_PLANS", "Ljava/util/Set;", "getLEGACY_PLANS", "()Ljava/util/Set;", "PREMIUM_PLANS", "getPREMIUM_PLANS", "PREMIUM_GUILD_PLANS", "getPREMIUM_GUILD_PLANS", "TIER_1_PLANS", "TIER_2_PLANS", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {

        @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public final /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                SubscriptionInterval.values();
                int[] iArr = new int[2];
                $EnumSwitchMapping$0 = iArr;
                iArr[SubscriptionInterval.MONTHLY.ordinal()] = 1;
                iArr[SubscriptionInterval.YEARLY.ordinal()] = 2;
            }
        }

        private Companion() {
        }

        private final SubscriptionPlanType getNonePlanForIntervalType(SubscriptionInterval subscriptionInterval) {
            int ordinal = subscriptionInterval.ordinal();
            if (ordinal == 0) {
                return SubscriptionPlanType.NONE_MONTH;
            }
            if (ordinal == 1) {
                return SubscriptionPlanType.NONE_YEAR;
            }
            throw new IllegalArgumentException("unsupported plan interval: " + subscriptionInterval);
        }

        public final SubscriptionPlanType from(long j) {
            SubscriptionPlanType subscriptionPlanType;
            SubscriptionPlanType[] values = SubscriptionPlanType.values();
            int i = 0;
            while (true) {
                if (i >= 10) {
                    subscriptionPlanType = null;
                    break;
                }
                subscriptionPlanType = values[i];
                if (subscriptionPlanType.getPlanId() == j) {
                    break;
                }
                i++;
            }
            if (subscriptionPlanType != null) {
                return subscriptionPlanType;
            }
            throw new IllegalArgumentException(a.s("unsupported type plan id: ", j));
        }

        public final SubscriptionPlanType getBasePlanFromSubscriptionItems(List<ModelSubscription.SubscriptionItem> list, SubscriptionInterval subscriptionInterval) {
            Object obj;
            boolean z2;
            SubscriptionInterval interval;
            m.checkNotNullParameter(list, "items");
            m.checkNotNullParameter(subscriptionInterval, "interval");
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
            for (ModelSubscription.SubscriptionItem subscriptionItem : list) {
                arrayList.add(SubscriptionPlanType.Companion.from(subscriptionItem.getPlanId()));
            }
            SubscriptionPlanType subscriptionPlanType = (SubscriptionPlanType) u.firstOrNull((List<? extends Object>) arrayList);
            if (!(subscriptionPlanType == null || (interval = subscriptionPlanType.getInterval()) == null)) {
                subscriptionInterval = interval;
            }
            Iterator it = arrayList.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (((SubscriptionPlanType) obj).getPremiumTier() != null) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    break;
                }
            }
            SubscriptionPlanType subscriptionPlanType2 = (SubscriptionPlanType) obj;
            return subscriptionPlanType2 != null ? subscriptionPlanType2 : getNonePlanForIntervalType(subscriptionInterval);
        }

        public final Set<SubscriptionPlanType> getLEGACY_PLANS() {
            return SubscriptionPlanType.LEGACY_PLANS;
        }

        public final Set<SubscriptionPlanType> getPREMIUM_GUILD_PLANS() {
            return SubscriptionPlanType.PREMIUM_GUILD_PLANS;
        }

        public final Set<SubscriptionPlanType> getPREMIUM_PLANS() {
            return SubscriptionPlanType.PREMIUM_PLANS;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final SubscriptionPlanType from(String str) {
            SubscriptionPlanType subscriptionPlanType;
            m.checkNotNullParameter(str, "planTypeString");
            SubscriptionPlanType[] values = SubscriptionPlanType.values();
            int i = 0;
            while (true) {
                if (i >= 10) {
                    subscriptionPlanType = null;
                    break;
                }
                subscriptionPlanType = values[i];
                if (m.areEqual(subscriptionPlanType.getPlanTypeString(), str)) {
                    break;
                }
                i++;
            }
            if (subscriptionPlanType != null) {
                return subscriptionPlanType;
            }
            throw new IllegalArgumentException(a.v("unsupported planTypeString: ", str));
        }
    }

    static {
        SubscriptionInterval subscriptionInterval = SubscriptionInterval.MONTHLY;
        PremiumTier premiumTier = PremiumTier.PREMIUM_GUILD_SUBSCRIPTION_ONLY;
        SubscriptionInterval subscriptionInterval2 = SubscriptionInterval.YEARLY;
        PremiumTier premiumTier2 = PremiumTier.TIER_2;
        SubscriptionPlanType subscriptionPlanType = PREMIUM_MONTH_LEGACY;
        SubscriptionPlanType subscriptionPlanType2 = PREMIUM_YEAR_LEGACY;
        PremiumTier premiumTier3 = PremiumTier.TIER_1;
        SubscriptionPlanType subscriptionPlanType3 = PREMIUM_MONTH_TIER_1;
        SubscriptionPlanType subscriptionPlanType4 = PREMIUM_YEAR_TIER_1;
        SubscriptionPlanType subscriptionPlanType5 = PREMIUM_MONTH_TIER_2;
        SubscriptionPlanType subscriptionPlanType6 = PREMIUM_YEAR_TIER_2;
        SubscriptionPlanType subscriptionPlanType7 = PREMIUM_GUILD_MONTH;
        SubscriptionPlanType subscriptionPlanType8 = PREMIUM_GUILD_YEAR;
        Set<SubscriptionPlanType> of = n0.setOf((Object[]) new SubscriptionPlanType[]{subscriptionPlanType4, subscriptionPlanType3});
        TIER_1_PLANS = of;
        Set<SubscriptionPlanType> of2 = n0.setOf((Object[]) new SubscriptionPlanType[]{subscriptionPlanType2, subscriptionPlanType});
        LEGACY_PLANS = of2;
        Set<SubscriptionPlanType> plus = o0.plus(n0.setOf((Object[]) new SubscriptionPlanType[]{subscriptionPlanType6, subscriptionPlanType5}), (Iterable) of2);
        TIER_2_PLANS = plus;
        PREMIUM_PLANS = o0.plus((Set) of, (Iterable) plus);
        PREMIUM_GUILD_PLANS = n0.setOf((Object[]) new SubscriptionPlanType[]{subscriptionPlanType7, subscriptionPlanType8});
    }

    SubscriptionPlanType(int i, SubscriptionInterval subscriptionInterval, String str, long j, PremiumTier premiumTier) {
        this.price = i;
        this.interval = subscriptionInterval;
        this.planTypeString = str;
        this.planId = j;
        this.premiumTier = premiumTier;
    }

    public final SubscriptionInterval getInterval() {
        return this.interval;
    }

    public final long getPlanId() {
        return this.planId;
    }

    public final String getPlanTypeString() {
        return this.planTypeString;
    }

    public final PremiumTier getPremiumTier() {
        return this.premiumTier;
    }

    public final int getPrice() {
        return this.price;
    }

    public final boolean isGrandfathered() {
        return LEGACY_PLANS.contains(this);
    }

    public final boolean isMonthlyInterval() {
        return this.interval == SubscriptionInterval.MONTHLY;
    }

    public final boolean isPremiumSubscription() {
        return PREMIUM_PLANS.contains(this);
    }
}
