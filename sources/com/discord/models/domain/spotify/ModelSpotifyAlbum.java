package com.discord.models.domain.spotify;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: ModelSpotifyAlbum.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001:\u0001!B-\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0006\u0010\u000e\u001a\u00020\u0002¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0016\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\n\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\n\u0010\u0004J>\u0010\u000f\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\u00022\u000e\b\u0002\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\b\b\u0002\u0010\u000e\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0004J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0017\u001a\u00020\u00162\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004R\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0019\u001a\u0004\b\u001b\u0010\u0004R\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u0019\u001a\u0004\b\u001c\u0010\u0004R\u001f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00070\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001d\u001a\u0004\b\u001e\u0010\t¨\u0006\""}, d2 = {"Lcom/discord/models/domain/spotify/ModelSpotifyAlbum;", "", "", "component1", "()Ljava/lang/String;", "component2", "", "Lcom/discord/models/domain/spotify/ModelSpotifyAlbum$AlbumImage;", "component3", "()Ljava/util/List;", "component4", "albumType", ModelAuditLogEntry.CHANGE_KEY_ID, "images", ModelAuditLogEntry.CHANGE_KEY_NAME, "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/discord/models/domain/spotify/ModelSpotifyAlbum;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getId", "getAlbumType", "getName", "Ljava/util/List;", "getImages", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V", "AlbumImage", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelSpotifyAlbum {
    private final String albumType;

    /* renamed from: id  reason: collision with root package name */
    private final String f2716id;
    private final List<AlbumImage> images;
    private final String name;

    /* compiled from: ModelSpotifyAlbum.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0011\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001c\u0010\u0006\u001a\u00020\u00002\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000e\u001a\u00020\r2\b\u0010\f\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u000e\u0010\u000fR\u001b\u0010\u0005\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0010\u001a\u0004\b\u0011\u0010\u0004¨\u0006\u0014"}, d2 = {"Lcom/discord/models/domain/spotify/ModelSpotifyAlbum$AlbumImage;", "", "", "component1", "()Ljava/lang/String;", "url", "copy", "(Ljava/lang/String;)Lcom/discord/models/domain/spotify/ModelSpotifyAlbum$AlbumImage;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getUrl", HookHelper.constructorName, "(Ljava/lang/String;)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class AlbumImage {
        private final String url;

        public AlbumImage(String str) {
            this.url = str;
        }

        public static /* synthetic */ AlbumImage copy$default(AlbumImage albumImage, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                str = albumImage.url;
            }
            return albumImage.copy(str);
        }

        public final String component1() {
            return this.url;
        }

        public final AlbumImage copy(String str) {
            return new AlbumImage(str);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof AlbumImage) && m.areEqual(this.url, ((AlbumImage) obj).url);
            }
            return true;
        }

        public final String getUrl() {
            return this.url;
        }

        public int hashCode() {
            String str = this.url;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return a.H(a.R("AlbumImage(url="), this.url, ")");
        }
    }

    public ModelSpotifyAlbum(String str, String str2, List<AlbumImage> list, String str3) {
        m.checkNotNullParameter(str, "albumType");
        m.checkNotNullParameter(str2, ModelAuditLogEntry.CHANGE_KEY_ID);
        m.checkNotNullParameter(list, "images");
        m.checkNotNullParameter(str3, ModelAuditLogEntry.CHANGE_KEY_NAME);
        this.albumType = str;
        this.f2716id = str2;
        this.images = list;
        this.name = str3;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ ModelSpotifyAlbum copy$default(ModelSpotifyAlbum modelSpotifyAlbum, String str, String str2, List list, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = modelSpotifyAlbum.albumType;
        }
        if ((i & 2) != 0) {
            str2 = modelSpotifyAlbum.f2716id;
        }
        if ((i & 4) != 0) {
            list = modelSpotifyAlbum.images;
        }
        if ((i & 8) != 0) {
            str3 = modelSpotifyAlbum.name;
        }
        return modelSpotifyAlbum.copy(str, str2, list, str3);
    }

    public final String component1() {
        return this.albumType;
    }

    public final String component2() {
        return this.f2716id;
    }

    public final List<AlbumImage> component3() {
        return this.images;
    }

    public final String component4() {
        return this.name;
    }

    public final ModelSpotifyAlbum copy(String str, String str2, List<AlbumImage> list, String str3) {
        m.checkNotNullParameter(str, "albumType");
        m.checkNotNullParameter(str2, ModelAuditLogEntry.CHANGE_KEY_ID);
        m.checkNotNullParameter(list, "images");
        m.checkNotNullParameter(str3, ModelAuditLogEntry.CHANGE_KEY_NAME);
        return new ModelSpotifyAlbum(str, str2, list, str3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModelSpotifyAlbum)) {
            return false;
        }
        ModelSpotifyAlbum modelSpotifyAlbum = (ModelSpotifyAlbum) obj;
        return m.areEqual(this.albumType, modelSpotifyAlbum.albumType) && m.areEqual(this.f2716id, modelSpotifyAlbum.f2716id) && m.areEqual(this.images, modelSpotifyAlbum.images) && m.areEqual(this.name, modelSpotifyAlbum.name);
    }

    public final String getAlbumType() {
        return this.albumType;
    }

    public final String getId() {
        return this.f2716id;
    }

    public final List<AlbumImage> getImages() {
        return this.images;
    }

    public final String getName() {
        return this.name;
    }

    public int hashCode() {
        String str = this.albumType;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.f2716id;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        List<AlbumImage> list = this.images;
        int hashCode3 = (hashCode2 + (list != null ? list.hashCode() : 0)) * 31;
        String str3 = this.name;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode3 + i;
    }

    public String toString() {
        StringBuilder R = a.R("ModelSpotifyAlbum(albumType=");
        R.append(this.albumType);
        R.append(", id=");
        R.append(this.f2716id);
        R.append(", images=");
        R.append(this.images);
        R.append(", name=");
        return a.H(R, this.name, ")");
    }
}
