package com.discord.models.domain;

import b.a.m.a.a;
import com.discord.models.domain.Model;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
/* loaded from: classes.dex */
public class ModelMessageDelete implements Model {
    private long channelId;
    private List<Long> messageIds;

    public ModelMessageDelete() {
    }

    @Override // com.discord.models.domain.Model
    public void assignField(Model.JsonReader jsonReader) throws IOException {
        String nextName = jsonReader.nextName();
        nextName.hashCode();
        char c = 65535;
        switch (nextName.hashCode()) {
            case -1930808873:
                if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_CHANNEL_ID)) {
                    c = 0;
                    break;
                }
                break;
            case 3355:
                if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_ID)) {
                    c = 1;
                    break;
                }
                break;
            case 104120:
                if (nextName.equals("ids")) {
                    c = 2;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                this.channelId = jsonReader.nextLong(this.channelId);
                return;
            case 1:
                this.messageIds = Collections.singletonList(Long.valueOf(jsonReader.nextLong(0L)));
                return;
            case 2:
                this.messageIds = jsonReader.nextList(new a(jsonReader));
                return;
            default:
                jsonReader.skipValue();
                return;
        }
    }

    public boolean canEqual(Object obj) {
        return obj instanceof ModelMessageDelete;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ModelMessageDelete)) {
            return false;
        }
        ModelMessageDelete modelMessageDelete = (ModelMessageDelete) obj;
        if (!modelMessageDelete.canEqual(this) || getChannelId() != modelMessageDelete.getChannelId()) {
            return false;
        }
        List<Long> messageIds = getMessageIds();
        List<Long> messageIds2 = modelMessageDelete.getMessageIds();
        return messageIds != null ? messageIds.equals(messageIds2) : messageIds2 == null;
    }

    public long getChannelId() {
        return this.channelId;
    }

    public List<Long> getMessageIds() {
        return this.messageIds;
    }

    public int hashCode() {
        long channelId = getChannelId();
        List<Long> messageIds = getMessageIds();
        return ((((int) (channelId ^ (channelId >>> 32))) + 59) * 59) + (messageIds == null ? 43 : messageIds.hashCode());
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("ModelMessageDelete(channelId=");
        R.append(getChannelId());
        R.append(", messageIds=");
        R.append(getMessageIds());
        R.append(")");
        return R.toString();
    }

    public ModelMessageDelete(long j, long j2) {
        this.channelId = j;
        this.messageIds = Collections.singletonList(Long.valueOf(j2));
    }
}
