package com.discord.models.domain;

import b.d.b.a.a;
import com.discord.api.user.User;
import com.discord.models.deserialization.gson.InboundGatewayGsonParser;
import com.discord.models.domain.Model;
import java.io.IOException;
import java.util.Map;
/* loaded from: classes.dex */
public class ModelUserRelationship implements Model {
    public static final int TYPE_BLOCKED = 2;
    public static final int TYPE_FRIEND = 1;
    public static final int TYPE_INVITE_INCOMING = 3;
    public static final int TYPE_INVITE_OUTGOING = 4;
    public static final int TYPE_NONE = 0;

    /* renamed from: id  reason: collision with root package name */
    private long f2708id;
    private int type;
    private User user;
    private Long userId;

    public ModelUserRelationship() {
    }

    public static boolean isType(Integer num, int i) {
        return num != null && num.intValue() == i;
    }

    @Override // com.discord.models.domain.Model
    public void assignField(Model.JsonReader jsonReader) throws IOException {
        String nextName = jsonReader.nextName();
        nextName.hashCode();
        char c = 65535;
        switch (nextName.hashCode()) {
            case -147132913:
                if (nextName.equals("user_id")) {
                    c = 0;
                    break;
                }
                break;
            case 3355:
                if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_ID)) {
                    c = 1;
                    break;
                }
                break;
            case 3575610:
                if (nextName.equals("type")) {
                    c = 2;
                    break;
                }
                break;
            case 3599307:
                if (nextName.equals("user")) {
                    c = 3;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                this.userId = jsonReader.nextLongOrNull();
                return;
            case 1:
                this.f2708id = jsonReader.nextLong(this.f2708id);
                return;
            case 2:
                this.type = jsonReader.nextInt(this.type);
                return;
            case 3:
                this.user = (User) InboundGatewayGsonParser.fromJson(jsonReader, User.class);
                return;
            default:
                jsonReader.skipValue();
                return;
        }
    }

    public boolean canEqual(Object obj) {
        return obj instanceof ModelUserRelationship;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ModelUserRelationship)) {
            return false;
        }
        ModelUserRelationship modelUserRelationship = (ModelUserRelationship) obj;
        if (!modelUserRelationship.canEqual(this) || getId() != modelUserRelationship.getId() || getType() != modelUserRelationship.getType()) {
            return false;
        }
        Long userId = getUserId();
        Long userId2 = modelUserRelationship.getUserId();
        if (userId != null ? !userId.equals(userId2) : userId2 != null) {
            return false;
        }
        User user = getUser();
        User user2 = modelUserRelationship.getUser();
        return user != null ? user.equals(user2) : user2 == null;
    }

    public long getId() {
        return this.f2708id;
    }

    public int getType() {
        return this.type;
    }

    public User getUser() {
        return this.user;
    }

    public Long getUserId() {
        return this.userId;
    }

    public int hashCode() {
        long id2 = getId();
        Long userId = getUserId();
        int i = 43;
        int type = ((getType() + ((((int) (id2 ^ (id2 >>> 32))) + 59) * 59)) * 59) + (userId == null ? 43 : userId.hashCode());
        User user = getUser();
        int i2 = type * 59;
        if (user != null) {
            i = user.hashCode();
        }
        return i2 + i;
    }

    public ModelUserRelationship hydrate(Map<Long, User> map) {
        if (this.userId == null) {
            return this;
        }
        ModelUserRelationship modelUserRelationship = new ModelUserRelationship(this);
        modelUserRelationship.user = map.get(modelUserRelationship.userId);
        modelUserRelationship.userId = null;
        return modelUserRelationship;
    }

    public String toString() {
        StringBuilder R = a.R("ModelUserRelationship(id=");
        R.append(getId());
        R.append(", type=");
        R.append(getType());
        R.append(", user=");
        R.append(getUser());
        R.append(", userId=");
        R.append(getUserId());
        R.append(")");
        return R.toString();
    }

    public ModelUserRelationship(long j, int i, User user, Long l) {
        this.f2708id = j;
        this.type = i;
        this.user = user;
        this.userId = l;
    }

    public static int getType(Integer num) {
        if (num == null) {
            return 0;
        }
        return num.intValue();
    }

    public ModelUserRelationship(ModelUserRelationship modelUserRelationship) {
        this(modelUserRelationship.f2708id, modelUserRelationship.type, modelUserRelationship.user, modelUserRelationship.userId);
    }
}
