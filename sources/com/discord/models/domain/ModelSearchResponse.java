package com.discord.models.domain;

import andhook.lib.HookHelper;
import androidx.core.app.FrameMetricsAggregator;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.message.Message;
import com.discord.api.thread.ThreadMember;
import d0.g;
import d0.z.d.m;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ModelSearchResponse.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0013\n\u0002\u0010\u000b\n\u0002\b\u0010\n\u0002\u0010\t\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0089\u0001\u0012\b\b\u0002\u0010\u0015\u001a\u00020\u0002\u0012\u0016\b\u0002\u0010\u0016\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u0007\u0018\u00010\u0007\u0012\u0010\b\u0002\u0010\u0017\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u0007\u0012\u0010\b\u0002\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\u0007\u0012\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b;\u0010<J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u001e\u0010\t\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u0007\u0018\u00010\u0007HÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0018\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u0007HÆ\u0003¢\u0006\u0004\b\f\u0010\nJ\u0018\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\u0007HÆ\u0003¢\u0006\u0004\b\u000e\u0010\nJ\u0012\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0012\u0010\u0012\u001a\u0004\u0018\u00010\u000fHÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0011J\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0004J\u0012\u0010\u0014\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0004J\u0092\u0001\u0010\u001e\u001a\u00020\u00002\b\b\u0002\u0010\u0015\u001a\u00020\u00022\u0016\b\u0002\u0010\u0016\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u0007\u0018\u00010\u00072\u0010\b\u0002\u0010\u0017\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u00072\u0010\b\u0002\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\u00072\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010 \u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b \u0010\u0011J\u0010\u0010!\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b!\u0010\u0006J\u001a\u0010$\u001a\u00020#2\b\u0010\"\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b$\u0010%R\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010&\u001a\u0004\b'\u0010\u0004R#\u0010+\u001a\b\u0012\u0004\u0012\u00020\b0\u00078F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b(\u0010)\u001a\u0004\b*\u0010\nR\u0018\u0010\u001d\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010&R'\u0010\u0016\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u0007\u0018\u00010\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010,\u001a\u0004\b-\u0010\nR\u0019\u0010\u0015\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010.\u001a\u0004\b/\u0010\u0006R!\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010,\u001a\u0004\b0\u0010\nR\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u00101\u001a\u0004\b2\u0010\u0011R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010&\u001a\u0004\b3\u0010\u0004R\u0019\u00105\u001a\u0002048\u0006@\u0006¢\u0006\f\n\u0004\b5\u00106\u001a\u0004\b7\u00108R!\u0010\u0017\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010,\u001a\u0004\b9\u0010\nR\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u00101\u001a\u0004\b:\u0010\u0011¨\u0006="}, d2 = {"Lcom/discord/models/domain/ModelSearchResponse;", "", "", "component9", "()Ljava/lang/Integer;", "component1", "()I", "", "Lcom/discord/api/message/Message;", "component2", "()Ljava/util/List;", "Lcom/discord/api/channel/Channel;", "component3", "Lcom/discord/api/thread/ThreadMember;", "component4", "", "component5", "()Ljava/lang/String;", "component6", "component7", "component8", "totalResults", "messages", "threads", "members", "analyticsId", "message", "errorCode", "documentIndexed", "retryAfter", "copy", "(ILjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)Lcom/discord/models/domain/ModelSearchResponse;", "toString", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Integer;", "getErrorCode", "hits$delegate", "Lkotlin/Lazy;", "getHits", "hits", "Ljava/util/List;", "getMessages", "I", "getTotalResults", "getMembers", "Ljava/lang/String;", "getAnalyticsId", "getDocumentIndexed", "", "retryMillis", "J", "getRetryMillis", "()J", "getThreads", "getMessage", HookHelper.constructorName, "(ILjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelSearchResponse {
    private final String analyticsId;
    private final Integer documentIndexed;
    private final Integer errorCode;
    private final Lazy hits$delegate;
    private final List<ThreadMember> members;
    private final String message;
    private final List<List<Message>> messages;
    private final Integer retryAfter;
    private final long retryMillis;
    private final List<Channel> threads;
    private final int totalResults;

    public ModelSearchResponse() {
        this(0, null, null, null, null, null, null, null, null, FrameMetricsAggregator.EVERY_DURATION, null);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public ModelSearchResponse(int i, List<? extends List<Message>> list, List<Channel> list2, List<? extends ThreadMember> list3, String str, String str2, Integer num, Integer num2, Integer num3) {
        this.totalResults = i;
        this.messages = list;
        this.threads = list2;
        this.members = list3;
        this.analyticsId = str;
        this.message = str2;
        this.errorCode = num;
        this.documentIndexed = num2;
        this.retryAfter = num3;
        this.retryMillis = (num3 != null ? num3.intValue() : 0L) * 1000;
        this.hits$delegate = g.lazy(new ModelSearchResponse$hits$2(this));
    }

    private final Integer component9() {
        return this.retryAfter;
    }

    public final int component1() {
        return this.totalResults;
    }

    public final List<List<Message>> component2() {
        return this.messages;
    }

    public final List<Channel> component3() {
        return this.threads;
    }

    public final List<ThreadMember> component4() {
        return this.members;
    }

    public final String component5() {
        return this.analyticsId;
    }

    public final String component6() {
        return this.message;
    }

    public final Integer component7() {
        return this.errorCode;
    }

    public final Integer component8() {
        return this.documentIndexed;
    }

    public final ModelSearchResponse copy(int i, List<? extends List<Message>> list, List<Channel> list2, List<? extends ThreadMember> list3, String str, String str2, Integer num, Integer num2, Integer num3) {
        return new ModelSearchResponse(i, list, list2, list3, str, str2, num, num2, num3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModelSearchResponse)) {
            return false;
        }
        ModelSearchResponse modelSearchResponse = (ModelSearchResponse) obj;
        return this.totalResults == modelSearchResponse.totalResults && m.areEqual(this.messages, modelSearchResponse.messages) && m.areEqual(this.threads, modelSearchResponse.threads) && m.areEqual(this.members, modelSearchResponse.members) && m.areEqual(this.analyticsId, modelSearchResponse.analyticsId) && m.areEqual(this.message, modelSearchResponse.message) && m.areEqual(this.errorCode, modelSearchResponse.errorCode) && m.areEqual(this.documentIndexed, modelSearchResponse.documentIndexed) && m.areEqual(this.retryAfter, modelSearchResponse.retryAfter);
    }

    public final String getAnalyticsId() {
        return this.analyticsId;
    }

    public final Integer getDocumentIndexed() {
        return this.documentIndexed;
    }

    public final Integer getErrorCode() {
        return this.errorCode;
    }

    public final List<Message> getHits() {
        return (List) this.hits$delegate.getValue();
    }

    public final List<ThreadMember> getMembers() {
        return this.members;
    }

    public final String getMessage() {
        return this.message;
    }

    public final List<List<Message>> getMessages() {
        return this.messages;
    }

    public final long getRetryMillis() {
        return this.retryMillis;
    }

    public final List<Channel> getThreads() {
        return this.threads;
    }

    public final int getTotalResults() {
        return this.totalResults;
    }

    public int hashCode() {
        int i = this.totalResults * 31;
        List<List<Message>> list = this.messages;
        int i2 = 0;
        int hashCode = (i + (list != null ? list.hashCode() : 0)) * 31;
        List<Channel> list2 = this.threads;
        int hashCode2 = (hashCode + (list2 != null ? list2.hashCode() : 0)) * 31;
        List<ThreadMember> list3 = this.members;
        int hashCode3 = (hashCode2 + (list3 != null ? list3.hashCode() : 0)) * 31;
        String str = this.analyticsId;
        int hashCode4 = (hashCode3 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.message;
        int hashCode5 = (hashCode4 + (str2 != null ? str2.hashCode() : 0)) * 31;
        Integer num = this.errorCode;
        int hashCode6 = (hashCode5 + (num != null ? num.hashCode() : 0)) * 31;
        Integer num2 = this.documentIndexed;
        int hashCode7 = (hashCode6 + (num2 != null ? num2.hashCode() : 0)) * 31;
        Integer num3 = this.retryAfter;
        if (num3 != null) {
            i2 = num3.hashCode();
        }
        return hashCode7 + i2;
    }

    public String toString() {
        StringBuilder R = a.R("ModelSearchResponse(totalResults=");
        R.append(this.totalResults);
        R.append(", messages=");
        R.append(this.messages);
        R.append(", threads=");
        R.append(this.threads);
        R.append(", members=");
        R.append(this.members);
        R.append(", analyticsId=");
        R.append(this.analyticsId);
        R.append(", message=");
        R.append(this.message);
        R.append(", errorCode=");
        R.append(this.errorCode);
        R.append(", documentIndexed=");
        R.append(this.documentIndexed);
        R.append(", retryAfter=");
        return a.E(R, this.retryAfter, ")");
    }

    public /* synthetic */ ModelSearchResponse(int i, List list, List list2, List list3, String str, String str2, Integer num, Integer num2, Integer num3, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this((i2 & 1) != 0 ? 0 : i, (i2 & 2) != 0 ? null : list, (i2 & 4) != 0 ? null : list2, (i2 & 8) != 0 ? null : list3, (i2 & 16) != 0 ? null : str, (i2 & 32) != 0 ? null : str2, (i2 & 64) != 0 ? null : num, (i2 & 128) != 0 ? null : num2, (i2 & 256) == 0 ? num3 : null);
    }
}
