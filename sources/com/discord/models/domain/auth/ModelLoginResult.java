package com.discord.models.domain.auth;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ModelLoginResult.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001B/\u0012\b\b\u0002\u0010\f\u001a\u00020\u0002\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\t¢\u0006\u0004\b \u0010!J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\b\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ>\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\tHÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0007J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0017\u001a\u00020\u00022\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004R\u001b\u0010\u000f\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001b\u001a\u0004\b\u001c\u0010\u000bR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001d\u001a\u0004\b\u001e\u0010\u0007R\u001b\u0010\r\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001d\u001a\u0004\b\u001f\u0010\u0007¨\u0006\""}, d2 = {"Lcom/discord/models/domain/auth/ModelLoginResult;", "", "", "component1", "()Z", "", "component2", "()Ljava/lang/String;", "component3", "Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;", "component4", "()Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;", "mfa", "ticket", "token", "userSettings", "copy", "(ZLjava/lang/String;Ljava/lang/String;Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;)Lcom/discord/models/domain/auth/ModelLoginResult;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getMfa", "Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;", "getUserSettings", "Ljava/lang/String;", "getToken", "getTicket", HookHelper.constructorName, "(ZLjava/lang/String;Ljava/lang/String;Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelLoginResult {
    private final boolean mfa;
    private final String ticket;
    private final String token;
    private final ModelUserSettingsBootstrap userSettings;

    public ModelLoginResult(boolean z2, String str, String str2, ModelUserSettingsBootstrap modelUserSettingsBootstrap) {
        this.mfa = z2;
        this.ticket = str;
        this.token = str2;
        this.userSettings = modelUserSettingsBootstrap;
    }

    public static /* synthetic */ ModelLoginResult copy$default(ModelLoginResult modelLoginResult, boolean z2, String str, String str2, ModelUserSettingsBootstrap modelUserSettingsBootstrap, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = modelLoginResult.mfa;
        }
        if ((i & 2) != 0) {
            str = modelLoginResult.ticket;
        }
        if ((i & 4) != 0) {
            str2 = modelLoginResult.token;
        }
        if ((i & 8) != 0) {
            modelUserSettingsBootstrap = modelLoginResult.userSettings;
        }
        return modelLoginResult.copy(z2, str, str2, modelUserSettingsBootstrap);
    }

    public final boolean component1() {
        return this.mfa;
    }

    public final String component2() {
        return this.ticket;
    }

    public final String component3() {
        return this.token;
    }

    public final ModelUserSettingsBootstrap component4() {
        return this.userSettings;
    }

    public final ModelLoginResult copy(boolean z2, String str, String str2, ModelUserSettingsBootstrap modelUserSettingsBootstrap) {
        return new ModelLoginResult(z2, str, str2, modelUserSettingsBootstrap);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModelLoginResult)) {
            return false;
        }
        ModelLoginResult modelLoginResult = (ModelLoginResult) obj;
        return this.mfa == modelLoginResult.mfa && m.areEqual(this.ticket, modelLoginResult.ticket) && m.areEqual(this.token, modelLoginResult.token) && m.areEqual(this.userSettings, modelLoginResult.userSettings);
    }

    public final boolean getMfa() {
        return this.mfa;
    }

    public final String getTicket() {
        return this.ticket;
    }

    public final String getToken() {
        return this.token;
    }

    public final ModelUserSettingsBootstrap getUserSettings() {
        return this.userSettings;
    }

    public int hashCode() {
        boolean z2 = this.mfa;
        if (z2) {
            z2 = true;
        }
        int i = z2 ? 1 : 0;
        int i2 = z2 ? 1 : 0;
        int i3 = i * 31;
        String str = this.ticket;
        int i4 = 0;
        int hashCode = (i3 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.token;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        ModelUserSettingsBootstrap modelUserSettingsBootstrap = this.userSettings;
        if (modelUserSettingsBootstrap != null) {
            i4 = modelUserSettingsBootstrap.hashCode();
        }
        return hashCode2 + i4;
    }

    public String toString() {
        StringBuilder R = a.R("ModelLoginResult(mfa=");
        R.append(this.mfa);
        R.append(", ticket=");
        R.append(this.ticket);
        R.append(", token=");
        R.append(this.token);
        R.append(", userSettings=");
        R.append(this.userSettings);
        R.append(")");
        return R.toString();
    }

    public /* synthetic */ ModelLoginResult(boolean z2, String str, String str2, ModelUserSettingsBootstrap modelUserSettingsBootstrap, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? false : z2, str, str2, modelUserSettingsBootstrap);
    }
}
