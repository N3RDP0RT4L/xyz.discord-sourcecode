package com.discord.models.domain.auth;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ModelLoginResult.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001B\u001b\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J(\u0010\b\u001a\u00020\u00002\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\n\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\n\u0010\u0004J\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0010\u001a\u00020\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0010\u0010\u0011R\u001b\u0010\u0007\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0012\u001a\u0004\b\u0013\u0010\u0004R\u001b\u0010\u0006\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0012\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;", "", "", "component1", "()Ljava/lang/String;", "component2", "theme", "locale", "copy", "(Ljava/lang/String;Ljava/lang/String;)Lcom/discord/models/domain/auth/ModelUserSettingsBootstrap;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getLocale", "getTheme", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelUserSettingsBootstrap {
    private final String locale;
    private final String theme;

    public ModelUserSettingsBootstrap(String str, String str2) {
        this.theme = str;
        this.locale = str2;
    }

    public static /* synthetic */ ModelUserSettingsBootstrap copy$default(ModelUserSettingsBootstrap modelUserSettingsBootstrap, String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = modelUserSettingsBootstrap.theme;
        }
        if ((i & 2) != 0) {
            str2 = modelUserSettingsBootstrap.locale;
        }
        return modelUserSettingsBootstrap.copy(str, str2);
    }

    public final String component1() {
        return this.theme;
    }

    public final String component2() {
        return this.locale;
    }

    public final ModelUserSettingsBootstrap copy(String str, String str2) {
        return new ModelUserSettingsBootstrap(str, str2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModelUserSettingsBootstrap)) {
            return false;
        }
        ModelUserSettingsBootstrap modelUserSettingsBootstrap = (ModelUserSettingsBootstrap) obj;
        return m.areEqual(this.theme, modelUserSettingsBootstrap.theme) && m.areEqual(this.locale, modelUserSettingsBootstrap.locale);
    }

    public final String getLocale() {
        return this.locale;
    }

    public final String getTheme() {
        return this.theme;
    }

    public int hashCode() {
        String str = this.theme;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.locale;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder R = a.R("ModelUserSettingsBootstrap(theme=");
        R.append(this.theme);
        R.append(", locale=");
        return a.H(R, this.locale, ")");
    }
}
