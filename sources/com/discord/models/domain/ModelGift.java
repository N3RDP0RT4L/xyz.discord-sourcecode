package com.discord.models.domain;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.premium.SubscriptionPlan;
import com.discord.api.user.User;
import com.discord.models.domain.ModelSku;
import com.discord.utilities.time.TimeUtils;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ModelGift.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b2\b\u0086\b\u0018\u00002\u00020\u0001Bq\u0012\u0006\u0010$\u001a\u00020\u0002\u0012\u0006\u0010%\u001a\u00020\u0007\u0012\b\u0010&\u001a\u0004\u0018\u00010\u000e\u0012\u0006\u0010'\u001a\u00020\u000e\u0012\u0006\u0010(\u001a\u00020\u0012\u0012\b\u0010)\u001a\u0004\u0018\u00010\u0015\u0012\u0006\u0010*\u001a\u00020\u0012\u0012\b\u0010+\u001a\u0004\u0018\u00010\u0019\u0012\u000e\u0010,\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u001c\u0012\b\u0010-\u001a\u0004\u0018\u00010\u001f\u0012\b\u0010.\u001a\u0004\u0018\u00010\u0012¢\u0006\u0004\bO\u0010PJ\u0019\u0010\u0005\u001a\u00020\u00022\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u0019\u0010\b\u001a\u00020\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\n\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0010J\u0010\u0010\u0013\u001a\u00020\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0012\u0010\u0016\u001a\u0004\u0018\u00010\u0015HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0018\u001a\u00020\u0012HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0014J\u0012\u0010\u001a\u001a\u0004\u0018\u00010\u0019HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u001bJ\u0018\u0010\u001d\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u001cHÆ\u0003¢\u0006\u0004\b\u001d\u0010\u001eJ\u0012\u0010 \u001a\u0004\u0018\u00010\u001fHÆ\u0003¢\u0006\u0004\b \u0010!J\u0012\u0010\"\u001a\u0004\u0018\u00010\u0012HÆ\u0003¢\u0006\u0004\b\"\u0010#J\u0090\u0001\u0010/\u001a\u00020\u00002\b\b\u0002\u0010$\u001a\u00020\u00022\b\b\u0002\u0010%\u001a\u00020\u00072\n\b\u0002\u0010&\u001a\u0004\u0018\u00010\u000e2\b\b\u0002\u0010'\u001a\u00020\u000e2\b\b\u0002\u0010(\u001a\u00020\u00122\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u00152\b\b\u0002\u0010*\u001a\u00020\u00122\n\b\u0002\u0010+\u001a\u0004\u0018\u00010\u00192\u0010\b\u0002\u0010,\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u001c2\n\b\u0002\u0010-\u001a\u0004\u0018\u00010\u001f2\n\b\u0002\u0010.\u001a\u0004\u0018\u00010\u0012HÆ\u0001¢\u0006\u0004\b/\u00100J\u0010\u00101\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b1\u0010\u0010J\u0010\u00102\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b2\u0010\u0014J\u001a\u00104\u001a\u00020\u00072\b\u00103\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b4\u00105R\u001b\u0010&\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b&\u00106\u001a\u0004\b7\u0010\u0010R\u0019\u0010(\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b(\u00108\u001a\u0004\b9\u0010\u0014R\u0019\u0010$\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010:\u001a\u0004\b;\u0010\u000bR\u0019\u0010%\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010<\u001a\u0004\b=\u0010\rR\u001b\u0010)\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010>\u001a\u0004\b?\u0010\u0017R\u0013\u0010@\u001a\u00020\u00078F@\u0006¢\u0006\u0006\u001a\u0004\b@\u0010\rR\u0013\u0010A\u001a\u00020\u00078F@\u0006¢\u0006\u0006\u001a\u0004\bA\u0010\rR!\u0010,\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010B\u001a\u0004\bC\u0010\u001eR\u0013\u0010D\u001a\u00020\u00078F@\u0006¢\u0006\u0006\u001a\u0004\bD\u0010\rR\u001b\u0010-\u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010E\u001a\u0004\bF\u0010!R\u0013\u0010G\u001a\u00020\u00078F@\u0006¢\u0006\u0006\u001a\u0004\bG\u0010\rR\u001b\u0010.\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010H\u001a\u0004\bI\u0010#R\u001b\u0010+\u001a\u0004\u0018\u00010\u00198\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010J\u001a\u0004\bK\u0010\u001bR\u0019\u0010'\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b'\u00106\u001a\u0004\bL\u0010\u0010R\u0019\u0010*\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b*\u00108\u001a\u0004\bM\u0010\u0014R\u0013\u0010N\u001a\u00020\u00078F@\u0006¢\u0006\u0006\u001a\u0004\bN\u0010\r¨\u0006Q"}, d2 = {"Lcom/discord/models/domain/ModelGift;", "", "", "Lcom/discord/primitives/Timestamp;", "currentTime", "getExpiresDiff", "(J)J", "", "isExpired", "(J)Z", "component1", "()J", "component2", "()Z", "", "component3", "()Ljava/lang/String;", "component4", "", "component5", "()I", "Lcom/discord/models/domain/ModelStoreListing;", "component6", "()Lcom/discord/models/domain/ModelStoreListing;", "component7", "Lcom/discord/api/user/User;", "component8", "()Lcom/discord/api/user/User;", "Lcom/discord/primitives/PlanId;", "component9", "()Ljava/lang/Long;", "Lcom/discord/api/premium/SubscriptionPlan;", "component10", "()Lcom/discord/api/premium/SubscriptionPlan;", "component11", "()Ljava/lang/Integer;", "skuId", "redeemed", "expiresAt", ModelAuditLogEntry.CHANGE_KEY_CODE, ModelAuditLogEntry.CHANGE_KEY_USES, "storeListing", "maxUses", "user", "subscriptionPlanId", "subscriptionPlan", "giftStyle", "copy", "(JZLjava/lang/String;Ljava/lang/String;ILcom/discord/models/domain/ModelStoreListing;ILcom/discord/api/user/User;Ljava/lang/Long;Lcom/discord/api/premium/SubscriptionPlan;Ljava/lang/Integer;)Lcom/discord/models/domain/ModelGift;", "toString", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getExpiresAt", "I", "getUses", "J", "getSkuId", "Z", "getRedeemed", "Lcom/discord/models/domain/ModelStoreListing;", "getStoreListing", "isAnyNitroGift", "isClaimedByMe", "Ljava/lang/Long;", "getSubscriptionPlanId", "isNitroClassicGift", "Lcom/discord/api/premium/SubscriptionPlan;", "getSubscriptionPlan", "isComplete", "Ljava/lang/Integer;", "getGiftStyle", "Lcom/discord/api/user/User;", "getUser", "getCode", "getMaxUses", "isNitroGift", HookHelper.constructorName, "(JZLjava/lang/String;Ljava/lang/String;ILcom/discord/models/domain/ModelStoreListing;ILcom/discord/api/user/User;Ljava/lang/Long;Lcom/discord/api/premium/SubscriptionPlan;Ljava/lang/Integer;)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelGift {
    private final String code;
    private final String expiresAt;
    private final Integer giftStyle;
    private final int maxUses;
    private final boolean redeemed;
    private final long skuId;
    private final ModelStoreListing storeListing;
    private final SubscriptionPlan subscriptionPlan;
    private final Long subscriptionPlanId;
    private final User user;
    private final int uses;

    public ModelGift(long j, boolean z2, String str, String str2, int i, ModelStoreListing modelStoreListing, int i2, User user, Long l, SubscriptionPlan subscriptionPlan, Integer num) {
        m.checkNotNullParameter(str2, ModelAuditLogEntry.CHANGE_KEY_CODE);
        this.skuId = j;
        this.redeemed = z2;
        this.expiresAt = str;
        this.code = str2;
        this.uses = i;
        this.storeListing = modelStoreListing;
        this.maxUses = i2;
        this.user = user;
        this.subscriptionPlanId = l;
        this.subscriptionPlan = subscriptionPlan;
        this.giftStyle = num;
    }

    public final long component1() {
        return this.skuId;
    }

    public final SubscriptionPlan component10() {
        return this.subscriptionPlan;
    }

    public final Integer component11() {
        return this.giftStyle;
    }

    public final boolean component2() {
        return this.redeemed;
    }

    public final String component3() {
        return this.expiresAt;
    }

    public final String component4() {
        return this.code;
    }

    public final int component5() {
        return this.uses;
    }

    public final ModelStoreListing component6() {
        return this.storeListing;
    }

    public final int component7() {
        return this.maxUses;
    }

    public final User component8() {
        return this.user;
    }

    public final Long component9() {
        return this.subscriptionPlanId;
    }

    public final ModelGift copy(long j, boolean z2, String str, String str2, int i, ModelStoreListing modelStoreListing, int i2, User user, Long l, SubscriptionPlan subscriptionPlan, Integer num) {
        m.checkNotNullParameter(str2, ModelAuditLogEntry.CHANGE_KEY_CODE);
        return new ModelGift(j, z2, str, str2, i, modelStoreListing, i2, user, l, subscriptionPlan, num);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModelGift)) {
            return false;
        }
        ModelGift modelGift = (ModelGift) obj;
        return this.skuId == modelGift.skuId && this.redeemed == modelGift.redeemed && m.areEqual(this.expiresAt, modelGift.expiresAt) && m.areEqual(this.code, modelGift.code) && this.uses == modelGift.uses && m.areEqual(this.storeListing, modelGift.storeListing) && this.maxUses == modelGift.maxUses && m.areEqual(this.user, modelGift.user) && m.areEqual(this.subscriptionPlanId, modelGift.subscriptionPlanId) && m.areEqual(this.subscriptionPlan, modelGift.subscriptionPlan) && m.areEqual(this.giftStyle, modelGift.giftStyle);
    }

    public final String getCode() {
        return this.code;
    }

    public final String getExpiresAt() {
        return this.expiresAt;
    }

    public final long getExpiresDiff(long j) {
        String str = this.expiresAt;
        if (str != null) {
            return TimeUtils.parseUTCDate(str) - j;
        }
        return 0L;
    }

    public final Integer getGiftStyle() {
        return this.giftStyle;
    }

    public final int getMaxUses() {
        return this.maxUses;
    }

    public final boolean getRedeemed() {
        return this.redeemed;
    }

    public final long getSkuId() {
        return this.skuId;
    }

    public final ModelStoreListing getStoreListing() {
        return this.storeListing;
    }

    public final SubscriptionPlan getSubscriptionPlan() {
        return this.subscriptionPlan;
    }

    public final Long getSubscriptionPlanId() {
        return this.subscriptionPlanId;
    }

    public final User getUser() {
        return this.user;
    }

    public final int getUses() {
        return this.uses;
    }

    public int hashCode() {
        long j = this.skuId;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        boolean z2 = this.redeemed;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        int i4 = (i + i2) * 31;
        String str = this.expiresAt;
        int i5 = 0;
        int hashCode = (i4 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.code;
        int hashCode2 = (((hashCode + (str2 != null ? str2.hashCode() : 0)) * 31) + this.uses) * 31;
        ModelStoreListing modelStoreListing = this.storeListing;
        int hashCode3 = (((hashCode2 + (modelStoreListing != null ? modelStoreListing.hashCode() : 0)) * 31) + this.maxUses) * 31;
        User user = this.user;
        int hashCode4 = (hashCode3 + (user != null ? user.hashCode() : 0)) * 31;
        Long l = this.subscriptionPlanId;
        int hashCode5 = (hashCode4 + (l != null ? l.hashCode() : 0)) * 31;
        SubscriptionPlan subscriptionPlan = this.subscriptionPlan;
        int hashCode6 = (hashCode5 + (subscriptionPlan != null ? subscriptionPlan.hashCode() : 0)) * 31;
        Integer num = this.giftStyle;
        if (num != null) {
            i5 = num.hashCode();
        }
        return hashCode6 + i5;
    }

    public final boolean isAnyNitroGift() {
        return isNitroGift() || isNitroClassicGift();
    }

    public final boolean isClaimedByMe() {
        return this.redeemed;
    }

    public final boolean isComplete() {
        return this.storeListing != null;
    }

    public final boolean isExpired(long j) {
        String str = this.expiresAt;
        return str != null && j > TimeUtils.parseUTCDate(str);
    }

    public final boolean isNitroClassicGift() {
        ModelSku sku;
        ModelStoreListing modelStoreListing = this.storeListing;
        return ((modelStoreListing == null || (sku = modelStoreListing.getSku()) == null) ? null : sku.getSkuCategory()) == ModelSku.SkuCategory.NITRO_CLASSIC;
    }

    public final boolean isNitroGift() {
        ModelSku sku;
        ModelStoreListing modelStoreListing = this.storeListing;
        return ((modelStoreListing == null || (sku = modelStoreListing.getSku()) == null) ? null : sku.getSkuCategory()) == ModelSku.SkuCategory.NITRO;
    }

    public String toString() {
        StringBuilder R = a.R("ModelGift(skuId=");
        R.append(this.skuId);
        R.append(", redeemed=");
        R.append(this.redeemed);
        R.append(", expiresAt=");
        R.append(this.expiresAt);
        R.append(", code=");
        R.append(this.code);
        R.append(", uses=");
        R.append(this.uses);
        R.append(", storeListing=");
        R.append(this.storeListing);
        R.append(", maxUses=");
        R.append(this.maxUses);
        R.append(", user=");
        R.append(this.user);
        R.append(", subscriptionPlanId=");
        R.append(this.subscriptionPlanId);
        R.append(", subscriptionPlan=");
        R.append(this.subscriptionPlan);
        R.append(", giftStyle=");
        return a.E(R, this.giftStyle, ")");
    }
}
