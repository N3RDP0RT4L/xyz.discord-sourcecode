package com.discord.models.domain;

import b.d.b.a.a;
import com.discord.models.domain.Model;
import java.io.IOException;
/* loaded from: classes.dex */
public class ModelWebhook implements Model {
    private String avatar;
    private long channelId;
    private long guildId;

    /* renamed from: id  reason: collision with root package name */
    private long f2710id;
    private String name;

    @Override // com.discord.models.domain.Model
    public void assignField(Model.JsonReader jsonReader) throws IOException {
        String nextName = jsonReader.nextName();
        nextName.hashCode();
        char c = 65535;
        switch (nextName.hashCode()) {
            case -1930808873:
                if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_CHANNEL_ID)) {
                    c = 0;
                    break;
                }
                break;
            case -1405959847:
                if (nextName.equals("avatar")) {
                    c = 1;
                    break;
                }
                break;
            case -1306538777:
                if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_GUILD_ID)) {
                    c = 2;
                    break;
                }
                break;
            case 3355:
                if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_ID)) {
                    c = 3;
                    break;
                }
                break;
            case 3373707:
                if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_NAME)) {
                    c = 4;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                this.channelId = jsonReader.nextLong(this.channelId);
                return;
            case 1:
                this.avatar = jsonReader.nextString(this.avatar);
                return;
            case 2:
                this.guildId = jsonReader.nextLong(this.guildId);
                return;
            case 3:
                this.f2710id = jsonReader.nextLong(this.f2710id);
                return;
            case 4:
                this.name = jsonReader.nextString(this.name);
                return;
            default:
                jsonReader.skipValue();
                return;
        }
    }

    public boolean canEqual(Object obj) {
        return obj instanceof ModelWebhook;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ModelWebhook)) {
            return false;
        }
        ModelWebhook modelWebhook = (ModelWebhook) obj;
        if (!modelWebhook.canEqual(this) || getChannelId() != modelWebhook.getChannelId() || getGuildId() != modelWebhook.getGuildId() || getId() != modelWebhook.getId()) {
            return false;
        }
        String avatar = getAvatar();
        String avatar2 = modelWebhook.getAvatar();
        if (avatar != null ? !avatar.equals(avatar2) : avatar2 != null) {
            return false;
        }
        String name = getName();
        String name2 = modelWebhook.getName();
        return name != null ? name.equals(name2) : name2 == null;
    }

    public String getAvatar() {
        return this.avatar;
    }

    public long getChannelId() {
        return this.channelId;
    }

    public long getGuildId() {
        return this.guildId;
    }

    public long getId() {
        return this.f2710id;
    }

    public String getName() {
        return this.name;
    }

    public int hashCode() {
        long channelId = getChannelId();
        long guildId = getGuildId();
        long id2 = getId();
        String avatar = getAvatar();
        int i = (((((((int) (channelId ^ (channelId >>> 32))) + 59) * 59) + ((int) (guildId ^ (guildId >>> 32)))) * 59) + ((int) ((id2 >>> 32) ^ id2))) * 59;
        int i2 = 43;
        int hashCode = i + (avatar == null ? 43 : avatar.hashCode());
        String name = getName();
        int i3 = hashCode * 59;
        if (name != null) {
            i2 = name.hashCode();
        }
        return i3 + i2;
    }

    public String toString() {
        StringBuilder R = a.R("ModelWebhook(avatar=");
        R.append(getAvatar());
        R.append(", name=");
        R.append(getName());
        R.append(", channelId=");
        R.append(getChannelId());
        R.append(", guildId=");
        R.append(getGuildId());
        R.append(", id=");
        R.append(getId());
        R.append(")");
        return R.toString();
    }
}
