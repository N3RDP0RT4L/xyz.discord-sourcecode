package com.discord.models.domain;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.models.domain.billing.ModelBillingAddress;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ModelPaymentSource.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b*\b\u0086\b\u0018\u00002\u00020\u0001B]\u0012\u0006\u0010\u0014\u001a\u00020\u0002\u0012\u0006\u0010\u0015\u001a\u00020\u0005\u0012\u0006\u0010\u0016\u001a\u00020\b\u0012\u0006\u0010\u0017\u001a\u00020\u000b\u0012\u0006\u0010\u0018\u001a\u00020\b\u0012\b\u0010\u0019\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u001a\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u001b\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u001c\u001a\u00020\u0002\u0012\u0006\u0010\u001d\u001a\u00020\u0002¢\u0006\u0004\b3\u00104J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u000e\u0010\nJ\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0007J\u0012\u0010\u0010\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0007J\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0007J\u0010\u0010\u0012\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0004J\u0010\u0010\u0013\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0004Jz\u0010\u001e\u001a\u00020\u00002\b\b\u0002\u0010\u0014\u001a\u00020\u00022\b\b\u0002\u0010\u0015\u001a\u00020\u00052\b\b\u0002\u0010\u0016\u001a\u00020\b2\b\b\u0002\u0010\u0017\u001a\u00020\u000b2\b\b\u0002\u0010\u0018\u001a\u00020\b2\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u001c\u001a\u00020\u00022\b\b\u0002\u0010\u001d\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010 \u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b \u0010\u0007J\u0010\u0010!\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b!\u0010\u0004J\u001a\u0010#\u001a\u00020\b2\b\u0010\"\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b#\u0010$R\u0019\u0010\u0017\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010%\u001a\u0004\b&\u0010\rR\u0019\u0010\u001c\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010'\u001a\u0004\b(\u0010\u0004R\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010)\u001a\u0004\b*\u0010\u0007R\u0019\u0010\u0015\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010)\u001a\u0004\b+\u0010\u0007R\u0019\u0010\u0018\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010,\u001a\u0004\b-\u0010\nR\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010)\u001a\u0004\b.\u0010\u0007R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010)\u001a\u0004\b/\u0010\u0007R\u0019\u0010\u001d\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010'\u001a\u0004\b0\u0010\u0004R\u0019\u0010\u0014\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010'\u001a\u0004\b1\u0010\u0004R\u0019\u0010\u0016\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010,\u001a\u0004\b2\u0010\n¨\u00065"}, d2 = {"Lcom/discord/models/domain/PaymentSourceRaw;", "", "", "component1", "()I", "", "component2", "()Ljava/lang/String;", "", "component3", "()Z", "Lcom/discord/models/domain/billing/ModelBillingAddress;", "component4", "()Lcom/discord/models/domain/billing/ModelBillingAddress;", "component5", "component6", "component7", "component8", "component9", "component10", "type", ModelAuditLogEntry.CHANGE_KEY_ID, "invalid", "billingAddress", "default", NotificationCompat.CATEGORY_EMAIL, "brand", "last_4", "expiresMonth", "expiresYear", "copy", "(ILjava/lang/String;ZLcom/discord/models/domain/billing/ModelBillingAddress;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Lcom/discord/models/domain/PaymentSourceRaw;", "toString", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/billing/ModelBillingAddress;", "getBillingAddress", "I", "getExpiresMonth", "Ljava/lang/String;", "getLast_4", "getId", "Z", "getDefault", "getEmail", "getBrand", "getExpiresYear", "getType", "getInvalid", HookHelper.constructorName, "(ILjava/lang/String;ZLcom/discord/models/domain/billing/ModelBillingAddress;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class PaymentSourceRaw {
    private final ModelBillingAddress billingAddress;
    private final String brand;

    /* renamed from: default */
    private final boolean f10default;
    private final String email;
    private final int expiresMonth;
    private final int expiresYear;

    /* renamed from: id */
    private final String f2711id;
    private final boolean invalid;
    private final String last_4;
    private final int type;

    public PaymentSourceRaw(int i, String str, boolean z2, ModelBillingAddress modelBillingAddress, boolean z3, String str2, String str3, String str4, int i2, int i3) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_ID);
        m.checkNotNullParameter(modelBillingAddress, "billingAddress");
        this.type = i;
        this.f2711id = str;
        this.invalid = z2;
        this.billingAddress = modelBillingAddress;
        this.f10default = z3;
        this.email = str2;
        this.brand = str3;
        this.last_4 = str4;
        this.expiresMonth = i2;
        this.expiresYear = i3;
    }

    public final int component1() {
        return this.type;
    }

    public final int component10() {
        return this.expiresYear;
    }

    public final String component2() {
        return this.f2711id;
    }

    public final boolean component3() {
        return this.invalid;
    }

    public final ModelBillingAddress component4() {
        return this.billingAddress;
    }

    public final boolean component5() {
        return this.f10default;
    }

    public final String component6() {
        return this.email;
    }

    public final String component7() {
        return this.brand;
    }

    public final String component8() {
        return this.last_4;
    }

    public final int component9() {
        return this.expiresMonth;
    }

    public final PaymentSourceRaw copy(int i, String str, boolean z2, ModelBillingAddress modelBillingAddress, boolean z3, String str2, String str3, String str4, int i2, int i3) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_ID);
        m.checkNotNullParameter(modelBillingAddress, "billingAddress");
        return new PaymentSourceRaw(i, str, z2, modelBillingAddress, z3, str2, str3, str4, i2, i3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PaymentSourceRaw)) {
            return false;
        }
        PaymentSourceRaw paymentSourceRaw = (PaymentSourceRaw) obj;
        return this.type == paymentSourceRaw.type && m.areEqual(this.f2711id, paymentSourceRaw.f2711id) && this.invalid == paymentSourceRaw.invalid && m.areEqual(this.billingAddress, paymentSourceRaw.billingAddress) && this.f10default == paymentSourceRaw.f10default && m.areEqual(this.email, paymentSourceRaw.email) && m.areEqual(this.brand, paymentSourceRaw.brand) && m.areEqual(this.last_4, paymentSourceRaw.last_4) && this.expiresMonth == paymentSourceRaw.expiresMonth && this.expiresYear == paymentSourceRaw.expiresYear;
    }

    public final ModelBillingAddress getBillingAddress() {
        return this.billingAddress;
    }

    public final String getBrand() {
        return this.brand;
    }

    public final boolean getDefault() {
        return this.f10default;
    }

    public final String getEmail() {
        return this.email;
    }

    public final int getExpiresMonth() {
        return this.expiresMonth;
    }

    public final int getExpiresYear() {
        return this.expiresYear;
    }

    public final String getId() {
        return this.f2711id;
    }

    public final boolean getInvalid() {
        return this.invalid;
    }

    public final String getLast_4() {
        return this.last_4;
    }

    public final int getType() {
        return this.type;
    }

    public int hashCode() {
        int i = this.type * 31;
        String str = this.f2711id;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        boolean z2 = this.invalid;
        int i3 = 1;
        if (z2) {
            z2 = true;
        }
        int i4 = z2 ? 1 : 0;
        int i5 = z2 ? 1 : 0;
        int i6 = (hashCode + i4) * 31;
        ModelBillingAddress modelBillingAddress = this.billingAddress;
        int hashCode2 = (i6 + (modelBillingAddress != null ? modelBillingAddress.hashCode() : 0)) * 31;
        boolean z3 = this.f10default;
        if (!z3) {
            i3 = z3 ? 1 : 0;
        }
        int i7 = (hashCode2 + i3) * 31;
        String str2 = this.email;
        int hashCode3 = (i7 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.brand;
        int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.last_4;
        if (str4 != null) {
            i2 = str4.hashCode();
        }
        return ((((hashCode4 + i2) * 31) + this.expiresMonth) * 31) + this.expiresYear;
    }

    public String toString() {
        StringBuilder R = a.R("PaymentSourceRaw(type=");
        R.append(this.type);
        R.append(", id=");
        R.append(this.f2711id);
        R.append(", invalid=");
        R.append(this.invalid);
        R.append(", billingAddress=");
        R.append(this.billingAddress);
        R.append(", default=");
        R.append(this.f10default);
        R.append(", email=");
        R.append(this.email);
        R.append(", brand=");
        R.append(this.brand);
        R.append(", last_4=");
        R.append(this.last_4);
        R.append(", expiresMonth=");
        R.append(this.expiresMonth);
        R.append(", expiresYear=");
        return a.A(R, this.expiresYear, ")");
    }
}
