package com.discord.models.domain;

import android.text.TextUtils;
import b.d.b.a.a;
import com.discord.api.presence.ClientStatus;
import com.discord.models.deserialization.gson.InboundGatewayGsonParser;
import com.discord.models.domain.Model;
import com.discord.models.domain.ModelCustomStatusSetting;
import com.discord.models.domain.ModelGuildFolder;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
/* loaded from: classes.dex */
public class ModelUserSettings implements Model {
    public static final int EXPLICIT_CONTENT_FILTER_DISABLED = 0;
    public static final int EXPLICIT_CONTENT_FILTER_FRIENDS_AND_NON_FRIENDS = 2;
    public static final int EXPLICIT_CONTENT_FILTER_NON_FRIENDS = 1;
    public static final int STICKER_ANIMATION_ALWAYS_ANIMATE = 0;
    public static final int STICKER_ANIMATION_ANIMATE_ON_INTERACTION = 1;
    public static final int STICKER_ANIMATION_NEVER_ANIMATE = 2;
    public static final String THEME_DARK = "dark";
    public static final String THEME_LIGHT = "light";
    public static final String THEME_PURE_EVIL = "pureEvil";
    private Boolean allowAccessibilityDetection;
    private Boolean animateEmoji;
    private Integer animateStickers;
    private Boolean contactSyncUpsellShown;
    private ModelCustomStatusSetting customStatus;
    private Boolean defaultGuildsRestricted;
    private Boolean developerMode;
    private Integer explicitContentFilter;
    private Integer friendDiscoveryFlags;
    private FriendSourceFlags friendSourceFlags;
    private List<ModelGuildFolder> guildFolders;
    private Boolean inlineAttachmentMedia;
    private Boolean inlineEmbedMedia;
    private String locale;
    private Boolean renderEmbeds;
    private List<Long> restrictedGuilds;
    private Boolean showCurrentGame;
    private ClientStatus status;
    private String theme;

    /* loaded from: classes.dex */
    public static class FriendSourceFlags implements Model {
        private boolean all;
        private boolean mutualFriends;
        private boolean mutualGuilds;

        @Override // com.discord.models.domain.Model
        public void assignField(Model.JsonReader jsonReader) throws IOException {
            String nextName = jsonReader.nextName();
            nextName.hashCode();
            char c = 65535;
            switch (nextName.hashCode()) {
                case -2014426773:
                    if (nextName.equals("mutual_guilds")) {
                        c = 0;
                        break;
                    }
                    break;
                case 96673:
                    if (nextName.equals("all")) {
                        c = 1;
                        break;
                    }
                    break;
                case 1003689066:
                    if (nextName.equals("mutual_friends")) {
                        c = 2;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    this.mutualGuilds = jsonReader.nextBoolean(this.mutualGuilds);
                    return;
                case 1:
                    this.all = jsonReader.nextBoolean(this.all);
                    return;
                case 2:
                    this.mutualFriends = jsonReader.nextBoolean(this.mutualFriends);
                    return;
                default:
                    return;
            }
        }

        public boolean canEqual(Object obj) {
            return obj instanceof FriendSourceFlags;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof FriendSourceFlags)) {
                return false;
            }
            FriendSourceFlags friendSourceFlags = (FriendSourceFlags) obj;
            return friendSourceFlags.canEqual(this) && isAll() == friendSourceFlags.isAll() && isMutualGuilds() == friendSourceFlags.isMutualGuilds() && isMutualFriends() == friendSourceFlags.isMutualFriends();
        }

        public int hashCode() {
            int i = 79;
            int i2 = ((((isAll() ? 79 : 97) + 59) * 59) + (isMutualGuilds() ? 79 : 97)) * 59;
            if (!isMutualFriends()) {
                i = 97;
            }
            return i2 + i;
        }

        public boolean isAll() {
            return this.all;
        }

        public boolean isMutualFriends() {
            return this.mutualFriends;
        }

        public boolean isMutualGuilds() {
            return this.mutualGuilds;
        }

        public String toString() {
            StringBuilder R = a.R("ModelUserSettings.FriendSourceFlags(all=");
            R.append(isAll());
            R.append(", mutualGuilds=");
            R.append(isMutualGuilds());
            R.append(", mutualFriends=");
            R.append(isMutualFriends());
            R.append(")");
            return R.toString();
        }
    }

    private static List<ModelGuildFolder> convertFromPositions(List<Long> list) {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < list.size(); i++) {
            arrayList.add(new ModelGuildFolder(null, Collections.singletonList(list.get(i)), null, null));
        }
        return arrayList;
    }

    public static String getDefaultLocale() {
        return getLocaleString(Locale.getDefault());
    }

    public static Locale getLocaleObject(String str) {
        if (str.contains("-")) {
            return new Locale(str.split("-")[0], str.split("-")[1]);
        }
        return new Locale(str);
    }

    private static String getLocaleString(Locale locale) {
        String str;
        if (TextUtils.isEmpty(locale.getCountry())) {
            str = "";
        } else {
            StringBuilder O = a.O('-');
            O.append(locale.getCountry().toUpperCase(Locale.getDefault()));
            str = O.toString();
        }
        return locale.getLanguage() + str;
    }

    @Override // com.discord.models.domain.Model
    public void assignField(final Model.JsonReader jsonReader) throws IOException {
        String nextName = jsonReader.nextName();
        nextName.hashCode();
        char c = 65535;
        switch (nextName.hashCode()) {
            case -1984452248:
                if (nextName.equals("animate_emoji")) {
                    c = 0;
                    break;
                }
                break;
            case -1919243602:
                if (nextName.equals("inline_attachment_media")) {
                    c = 1;
                    break;
                }
                break;
            case -1705139351:
                if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_EXPLICIT_CONTENT_FILTER)) {
                    c = 2;
                    break;
                }
                break;
            case -1431589090:
                if (nextName.equals("guild_positions")) {
                    c = 3;
                    break;
                }
                break;
            case -1376527110:
                if (nextName.equals("show_current_game")) {
                    c = 4;
                    break;
                }
                break;
            case -1359336636:
                if (nextName.equals("restricted_guilds")) {
                    c = 5;
                    break;
                }
                break;
            case -1147047037:
                if (nextName.equals("render_embeds")) {
                    c = 6;
                    break;
                }
                break;
            case -1097462182:
                if (nextName.equals("locale")) {
                    c = 7;
                    break;
                }
                break;
            case -1005561378:
                if (nextName.equals("allow_accessibility_detection")) {
                    c = '\b';
                    break;
                }
                break;
            case -892481550:
                if (nextName.equals("status")) {
                    c = '\t';
                    break;
                }
                break;
            case -364727943:
                if (nextName.equals("guild_folders")) {
                    c = '\n';
                    break;
                }
                break;
            case 110327241:
                if (nextName.equals("theme")) {
                    c = 11;
                    break;
                }
                break;
            case 275273976:
                if (nextName.equals("developer_mode")) {
                    c = '\f';
                    break;
                }
                break;
            case 554286680:
                if (nextName.equals("inline_embed_media")) {
                    c = '\r';
                    break;
                }
                break;
            case 652639068:
                if (nextName.equals("contact_sync_enabled")) {
                    c = 14;
                    break;
                }
                break;
            case 1168748672:
                if (nextName.equals("custom_status")) {
                    c = 15;
                    break;
                }
                break;
            case 1265830244:
                if (nextName.equals("friend_source_flags")) {
                    c = 16;
                    break;
                }
                break;
            case 1776929396:
                if (nextName.equals("animate_stickers")) {
                    c = 17;
                    break;
                }
                break;
            case 2020489111:
                if (nextName.equals("friend_discovery_flags")) {
                    c = 18;
                    break;
                }
                break;
            case 2039062684:
                if (nextName.equals("default_guilds_restricted")) {
                    c = 19;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                this.animateEmoji = jsonReader.nextBooleanOrNull();
                return;
            case 1:
                this.inlineAttachmentMedia = jsonReader.nextBooleanOrNull();
                return;
            case 2:
                this.explicitContentFilter = jsonReader.nextIntOrNull();
                return;
            case 3:
                List<ModelGuildFolder> list = this.guildFolders;
                if (list == null || list.isEmpty()) {
                    this.guildFolders = convertFromPositions(jsonReader.nextList(new b.a.m.a.a(jsonReader)));
                    return;
                }
                return;
            case 4:
                this.showCurrentGame = Boolean.valueOf(jsonReader.nextBoolean(false));
                return;
            case 5:
                this.restrictedGuilds = jsonReader.nextList(new b.a.m.a.a(jsonReader));
                return;
            case 6:
                this.renderEmbeds = jsonReader.nextBooleanOrNull();
                return;
            case 7:
                this.locale = jsonReader.nextString(this.locale);
                return;
            case '\b':
                this.allowAccessibilityDetection = jsonReader.nextBooleanOrNull();
                return;
            case '\t':
                this.status = (ClientStatus) InboundGatewayGsonParser.fromJson(jsonReader, ClientStatus.class);
                return;
            case '\n':
                List<ModelGuildFolder> nextList = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.d0
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        return ModelGuildFolder.Parser.INSTANCE.parse(Model.JsonReader.this);
                    }
                });
                if (!nextList.isEmpty()) {
                    this.guildFolders = nextList;
                    return;
                }
                return;
            case 11:
                this.theme = jsonReader.nextString(this.theme);
                return;
            case '\f':
                this.developerMode = Boolean.valueOf(jsonReader.nextBoolean(false));
                return;
            case '\r':
                this.inlineEmbedMedia = jsonReader.nextBooleanOrNull();
                return;
            case 14:
                this.contactSyncUpsellShown = jsonReader.nextBooleanOrNull();
                return;
            case 15:
                this.customStatus = ModelCustomStatusSetting.Parser.INSTANCE.parse(jsonReader);
                return;
            case 16:
                this.friendSourceFlags = (FriendSourceFlags) jsonReader.parse(new FriendSourceFlags());
                return;
            case 17:
                this.animateStickers = jsonReader.nextIntOrNull();
                return;
            case 18:
                this.friendDiscoveryFlags = Integer.valueOf(jsonReader.nextInt(0));
                return;
            case 19:
                this.defaultGuildsRestricted = jsonReader.nextBooleanOrNull();
                return;
            default:
                jsonReader.skipValue();
                return;
        }
    }

    public boolean canEqual(Object obj) {
        return obj instanceof ModelUserSettings;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ModelUserSettings)) {
            return false;
        }
        ModelUserSettings modelUserSettings = (ModelUserSettings) obj;
        if (!modelUserSettings.canEqual(this)) {
            return false;
        }
        Boolean renderEmbeds = getRenderEmbeds();
        Boolean renderEmbeds2 = modelUserSettings.getRenderEmbeds();
        if (renderEmbeds != null ? !renderEmbeds.equals(renderEmbeds2) : renderEmbeds2 != null) {
            return false;
        }
        Boolean inlineEmbedMedia = getInlineEmbedMedia();
        Boolean inlineEmbedMedia2 = modelUserSettings.getInlineEmbedMedia();
        if (inlineEmbedMedia != null ? !inlineEmbedMedia.equals(inlineEmbedMedia2) : inlineEmbedMedia2 != null) {
            return false;
        }
        Boolean inlineAttachmentMedia = getInlineAttachmentMedia();
        Boolean inlineAttachmentMedia2 = modelUserSettings.getInlineAttachmentMedia();
        if (inlineAttachmentMedia != null ? !inlineAttachmentMedia.equals(inlineAttachmentMedia2) : inlineAttachmentMedia2 != null) {
            return false;
        }
        Boolean developerMode = getDeveloperMode();
        Boolean developerMode2 = modelUserSettings.getDeveloperMode();
        if (developerMode != null ? !developerMode.equals(developerMode2) : developerMode2 != null) {
            return false;
        }
        Boolean defaultGuildsRestricted = getDefaultGuildsRestricted();
        Boolean defaultGuildsRestricted2 = modelUserSettings.getDefaultGuildsRestricted();
        if (defaultGuildsRestricted != null ? !defaultGuildsRestricted.equals(defaultGuildsRestricted2) : defaultGuildsRestricted2 != null) {
            return false;
        }
        Integer explicitContentFilter = getExplicitContentFilter();
        Integer explicitContentFilter2 = modelUserSettings.getExplicitContentFilter();
        if (explicitContentFilter != null ? !explicitContentFilter.equals(explicitContentFilter2) : explicitContentFilter2 != null) {
            return false;
        }
        Integer friendDiscoveryFlags = getFriendDiscoveryFlags();
        Integer friendDiscoveryFlags2 = modelUserSettings.getFriendDiscoveryFlags();
        if (friendDiscoveryFlags != null ? !friendDiscoveryFlags.equals(friendDiscoveryFlags2) : friendDiscoveryFlags2 != null) {
            return false;
        }
        Boolean contactSyncUpsellShown = getContactSyncUpsellShown();
        Boolean contactSyncUpsellShown2 = modelUserSettings.getContactSyncUpsellShown();
        if (contactSyncUpsellShown != null ? !contactSyncUpsellShown.equals(contactSyncUpsellShown2) : contactSyncUpsellShown2 != null) {
            return false;
        }
        Boolean showCurrentGame = getShowCurrentGame();
        Boolean showCurrentGame2 = modelUserSettings.getShowCurrentGame();
        if (showCurrentGame != null ? !showCurrentGame.equals(showCurrentGame2) : showCurrentGame2 != null) {
            return false;
        }
        Boolean animateEmoji = getAnimateEmoji();
        Boolean animateEmoji2 = modelUserSettings.getAnimateEmoji();
        if (animateEmoji != null ? !animateEmoji.equals(animateEmoji2) : animateEmoji2 != null) {
            return false;
        }
        Boolean allowAccessibilityDetection = getAllowAccessibilityDetection();
        Boolean allowAccessibilityDetection2 = modelUserSettings.getAllowAccessibilityDetection();
        if (allowAccessibilityDetection != null ? !allowAccessibilityDetection.equals(allowAccessibilityDetection2) : allowAccessibilityDetection2 != null) {
            return false;
        }
        Integer animateStickers = getAnimateStickers();
        Integer animateStickers2 = modelUserSettings.getAnimateStickers();
        if (animateStickers != null ? !animateStickers.equals(animateStickers2) : animateStickers2 != null) {
            return false;
        }
        String theme = getTheme();
        String theme2 = modelUserSettings.getTheme();
        if (theme != null ? !theme.equals(theme2) : theme2 != null) {
            return false;
        }
        List<ModelGuildFolder> guildFolders = getGuildFolders();
        List<ModelGuildFolder> guildFolders2 = modelUserSettings.getGuildFolders();
        if (guildFolders != null ? !guildFolders.equals(guildFolders2) : guildFolders2 != null) {
            return false;
        }
        String locale = getLocale();
        String locale2 = modelUserSettings.getLocale();
        if (locale != null ? !locale.equals(locale2) : locale2 != null) {
            return false;
        }
        List<Long> restrictedGuilds = getRestrictedGuilds();
        List<Long> restrictedGuilds2 = modelUserSettings.getRestrictedGuilds();
        if (restrictedGuilds != null ? !restrictedGuilds.equals(restrictedGuilds2) : restrictedGuilds2 != null) {
            return false;
        }
        FriendSourceFlags friendSourceFlags = getFriendSourceFlags();
        FriendSourceFlags friendSourceFlags2 = modelUserSettings.getFriendSourceFlags();
        if (friendSourceFlags != null ? !friendSourceFlags.equals(friendSourceFlags2) : friendSourceFlags2 != null) {
            return false;
        }
        ClientStatus status = getStatus();
        ClientStatus status2 = modelUserSettings.getStatus();
        if (status != null ? !status.equals(status2) : status2 != null) {
            return false;
        }
        ModelCustomStatusSetting customStatus = getCustomStatus();
        ModelCustomStatusSetting customStatus2 = modelUserSettings.getCustomStatus();
        return customStatus != null ? customStatus.equals(customStatus2) : customStatus2 == null;
    }

    public Boolean getAllowAccessibilityDetection() {
        return this.allowAccessibilityDetection;
    }

    public Boolean getAnimateEmoji() {
        return this.animateEmoji;
    }

    public Integer getAnimateStickers() {
        return this.animateStickers;
    }

    public Boolean getContactSyncUpsellShown() {
        return this.contactSyncUpsellShown;
    }

    public ModelCustomStatusSetting getCustomStatus() {
        return this.customStatus;
    }

    public Boolean getDefaultGuildsRestricted() {
        return this.defaultGuildsRestricted;
    }

    public Boolean getDeveloperMode() {
        return this.developerMode;
    }

    public Integer getExplicitContentFilter() {
        return this.explicitContentFilter;
    }

    public Integer getFriendDiscoveryFlags() {
        return this.friendDiscoveryFlags;
    }

    public FriendSourceFlags getFriendSourceFlags() {
        return this.friendSourceFlags;
    }

    public List<ModelGuildFolder> getGuildFolders() {
        return this.guildFolders;
    }

    public Boolean getInlineAttachmentMedia() {
        return this.inlineAttachmentMedia;
    }

    public Boolean getInlineEmbedMedia() {
        return this.inlineEmbedMedia;
    }

    public String getLocale() {
        return this.locale;
    }

    public Boolean getRenderEmbeds() {
        return this.renderEmbeds;
    }

    public List<Long> getRestrictedGuilds() {
        return this.restrictedGuilds;
    }

    public Boolean getShowCurrentGame() {
        return this.showCurrentGame;
    }

    public ClientStatus getStatus() {
        return this.status;
    }

    public String getTheme() {
        return this.theme;
    }

    public int hashCode() {
        Boolean renderEmbeds = getRenderEmbeds();
        int i = 43;
        int hashCode = renderEmbeds == null ? 43 : renderEmbeds.hashCode();
        Boolean inlineEmbedMedia = getInlineEmbedMedia();
        int hashCode2 = ((hashCode + 59) * 59) + (inlineEmbedMedia == null ? 43 : inlineEmbedMedia.hashCode());
        Boolean inlineAttachmentMedia = getInlineAttachmentMedia();
        int hashCode3 = (hashCode2 * 59) + (inlineAttachmentMedia == null ? 43 : inlineAttachmentMedia.hashCode());
        Boolean developerMode = getDeveloperMode();
        int hashCode4 = (hashCode3 * 59) + (developerMode == null ? 43 : developerMode.hashCode());
        Boolean defaultGuildsRestricted = getDefaultGuildsRestricted();
        int hashCode5 = (hashCode4 * 59) + (defaultGuildsRestricted == null ? 43 : defaultGuildsRestricted.hashCode());
        Integer explicitContentFilter = getExplicitContentFilter();
        int hashCode6 = (hashCode5 * 59) + (explicitContentFilter == null ? 43 : explicitContentFilter.hashCode());
        Integer friendDiscoveryFlags = getFriendDiscoveryFlags();
        int hashCode7 = (hashCode6 * 59) + (friendDiscoveryFlags == null ? 43 : friendDiscoveryFlags.hashCode());
        Boolean contactSyncUpsellShown = getContactSyncUpsellShown();
        int hashCode8 = (hashCode7 * 59) + (contactSyncUpsellShown == null ? 43 : contactSyncUpsellShown.hashCode());
        Boolean showCurrentGame = getShowCurrentGame();
        int hashCode9 = (hashCode8 * 59) + (showCurrentGame == null ? 43 : showCurrentGame.hashCode());
        Boolean animateEmoji = getAnimateEmoji();
        int hashCode10 = (hashCode9 * 59) + (animateEmoji == null ? 43 : animateEmoji.hashCode());
        Boolean allowAccessibilityDetection = getAllowAccessibilityDetection();
        int hashCode11 = (hashCode10 * 59) + (allowAccessibilityDetection == null ? 43 : allowAccessibilityDetection.hashCode());
        Integer animateStickers = getAnimateStickers();
        int hashCode12 = (hashCode11 * 59) + (animateStickers == null ? 43 : animateStickers.hashCode());
        String theme = getTheme();
        int hashCode13 = (hashCode12 * 59) + (theme == null ? 43 : theme.hashCode());
        List<ModelGuildFolder> guildFolders = getGuildFolders();
        int hashCode14 = (hashCode13 * 59) + (guildFolders == null ? 43 : guildFolders.hashCode());
        String locale = getLocale();
        int hashCode15 = (hashCode14 * 59) + (locale == null ? 43 : locale.hashCode());
        List<Long> restrictedGuilds = getRestrictedGuilds();
        int hashCode16 = (hashCode15 * 59) + (restrictedGuilds == null ? 43 : restrictedGuilds.hashCode());
        FriendSourceFlags friendSourceFlags = getFriendSourceFlags();
        int hashCode17 = (hashCode16 * 59) + (friendSourceFlags == null ? 43 : friendSourceFlags.hashCode());
        ClientStatus status = getStatus();
        int hashCode18 = (hashCode17 * 59) + (status == null ? 43 : status.hashCode());
        ModelCustomStatusSetting customStatus = getCustomStatus();
        int i2 = hashCode18 * 59;
        if (customStatus != null) {
            i = customStatus.hashCode();
        }
        return i2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("ModelUserSettings(theme=");
        R.append(getTheme());
        R.append(", guildFolders=");
        R.append(getGuildFolders());
        R.append(", renderEmbeds=");
        R.append(getRenderEmbeds());
        R.append(", inlineEmbedMedia=");
        R.append(getInlineEmbedMedia());
        R.append(", inlineAttachmentMedia=");
        R.append(getInlineAttachmentMedia());
        R.append(", developerMode=");
        R.append(getDeveloperMode());
        R.append(", locale=");
        R.append(getLocale());
        R.append(", restrictedGuilds=");
        R.append(getRestrictedGuilds());
        R.append(", defaultGuildsRestricted=");
        R.append(getDefaultGuildsRestricted());
        R.append(", friendSourceFlags=");
        R.append(getFriendSourceFlags());
        R.append(", explicitContentFilter=");
        R.append(getExplicitContentFilter());
        R.append(", friendDiscoveryFlags=");
        R.append(getFriendDiscoveryFlags());
        R.append(", contactSyncUpsellShown=");
        R.append(getContactSyncUpsellShown());
        R.append(", status=");
        R.append(getStatus());
        R.append(", customStatus=");
        R.append(getCustomStatus());
        R.append(", showCurrentGame=");
        R.append(getShowCurrentGame());
        R.append(", animateEmoji=");
        R.append(getAnimateEmoji());
        R.append(", allowAccessibilityDetection=");
        R.append(getAllowAccessibilityDetection());
        R.append(", animateStickers=");
        R.append(getAnimateStickers());
        R.append(")");
        return R.toString();
    }
}
