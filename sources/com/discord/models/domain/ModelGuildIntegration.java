package com.discord.models.domain;

import b.d.b.a.a;
import com.discord.api.user.User;
import com.discord.models.deserialization.gson.InboundGatewayGsonParser;
import com.discord.models.domain.Model;
import java.io.IOException;
/* loaded from: classes.dex */
public class ModelGuildIntegration implements Model {
    public static final String[] SUPPORTED_TYPES = {"twitch", "youtube"};
    private static final String TWITCH_URL_PREFIX = "twitch.tv/";
    public static final String TYPE_TWITCH = "twitch";
    public static final String TYPE_YOUTUBE = "youtube";
    private IntegrationAccount account;
    private boolean enableEmoticons;
    private boolean enabled;
    private int expireBehavior;
    private int expireGracePeriod;

    /* renamed from: id  reason: collision with root package name */
    private long f2694id;
    private String name;
    private long roleId;
    private int subscriberCount;
    private String syncedAt;
    private boolean syncing;
    private String type;
    private User user;

    /* loaded from: classes.dex */
    public static class IntegrationAccount implements Model {

        /* renamed from: id  reason: collision with root package name */
        private String f2695id;
        private String name;

        @Override // com.discord.models.domain.Model
        public void assignField(Model.JsonReader jsonReader) throws IOException {
            String nextName = jsonReader.nextName();
            nextName.hashCode();
            if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_ID)) {
                this.f2695id = jsonReader.nextString(this.f2695id);
            } else if (!nextName.equals(ModelAuditLogEntry.CHANGE_KEY_NAME)) {
                jsonReader.skipValue();
            } else {
                this.name = jsonReader.nextString(this.name);
            }
        }

        public boolean canEqual(Object obj) {
            return obj instanceof IntegrationAccount;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof IntegrationAccount)) {
                return false;
            }
            IntegrationAccount integrationAccount = (IntegrationAccount) obj;
            if (!integrationAccount.canEqual(this)) {
                return false;
            }
            String id2 = getId();
            String id3 = integrationAccount.getId();
            if (id2 != null ? !id2.equals(id3) : id3 != null) {
                return false;
            }
            String name = getName();
            String name2 = integrationAccount.getName();
            return name != null ? name.equals(name2) : name2 == null;
        }

        public String getId() {
            return this.f2695id;
        }

        public String getName() {
            return this.name;
        }

        public int hashCode() {
            String id2 = getId();
            int i = 43;
            int hashCode = id2 == null ? 43 : id2.hashCode();
            String name = getName();
            int i2 = (hashCode + 59) * 59;
            if (name != null) {
                i = name.hashCode();
            }
            return i2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("ModelGuildIntegration.IntegrationAccount(id=");
            R.append(getId());
            R.append(", name=");
            R.append(getName());
            R.append(")");
            return R.toString();
        }
    }

    /* loaded from: classes.dex */
    public static class Update implements Model {
        private long guildId;

        @Override // com.discord.models.domain.Model
        public void assignField(Model.JsonReader jsonReader) throws IOException {
            String nextName = jsonReader.nextName();
            nextName.hashCode();
            if (!nextName.equals(ModelAuditLogEntry.CHANGE_KEY_GUILD_ID)) {
                jsonReader.skipValue();
            } else {
                this.guildId = jsonReader.nextLong(this.guildId);
            }
        }

        public boolean canEqual(Object obj) {
            return obj instanceof Update;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Update)) {
                return false;
            }
            Update update = (Update) obj;
            return update.canEqual(this) && getGuildId() == update.getGuildId();
        }

        public long getGuildId() {
            return this.guildId;
        }

        public int hashCode() {
            long guildId = getGuildId();
            return 59 + ((int) (guildId ^ (guildId >>> 32)));
        }

        public String toString() {
            StringBuilder R = a.R("ModelGuildIntegration.Update(guildId=");
            R.append(getGuildId());
            R.append(")");
            return R.toString();
        }
    }

    @Override // com.discord.models.domain.Model
    public void assignField(Model.JsonReader jsonReader) throws IOException {
        String nextName = jsonReader.nextName();
        nextName.hashCode();
        char c = 65535;
        switch (nextName.hashCode()) {
            case -1743820047:
                if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_ENABLE_EMOTICONS)) {
                    c = 0;
                    break;
                }
                break;
            case -1742490777:
                if (nextName.equals("syncing")) {
                    c = 1;
                    break;
                }
                break;
            case -1609594047:
                if (nextName.equals("enabled")) {
                    c = 2;
                    break;
                }
                break;
            case -1177318867:
                if (nextName.equals("account")) {
                    c = 3;
                    break;
                }
                break;
            case -486786702:
                if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_EXPIRE_BEHAVIOR)) {
                    c = 4;
                    break;
                }
                break;
            case 3355:
                if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_ID)) {
                    c = 5;
                    break;
                }
                break;
            case 3373707:
                if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_NAME)) {
                    c = 6;
                    break;
                }
                break;
            case 3575610:
                if (nextName.equals("type")) {
                    c = 7;
                    break;
                }
                break;
            case 3599307:
                if (nextName.equals("user")) {
                    c = '\b';
                    break;
                }
                break;
            case 499612184:
                if (nextName.equals("synced_at")) {
                    c = '\t';
                    break;
                }
                break;
            case 1376884100:
                if (nextName.equals("role_id")) {
                    c = '\n';
                    break;
                }
                break;
            case 1767574344:
                if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_EXPIRE_GRACE_PERIOD)) {
                    c = 11;
                    break;
                }
                break;
            case 1871614584:
                if (nextName.equals("subscriber_count")) {
                    c = '\f';
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                this.enableEmoticons = jsonReader.nextBoolean(this.enableEmoticons);
                return;
            case 1:
                this.syncing = jsonReader.nextBoolean(this.syncing);
                return;
            case 2:
                this.enabled = jsonReader.nextBoolean(this.enabled);
                return;
            case 3:
                this.account = (IntegrationAccount) jsonReader.parse(new IntegrationAccount());
                return;
            case 4:
                this.expireBehavior = jsonReader.nextInt(this.expireBehavior);
                return;
            case 5:
                this.f2694id = jsonReader.nextLong(this.f2694id);
                return;
            case 6:
                this.name = jsonReader.nextString(this.name);
                return;
            case 7:
                this.type = jsonReader.nextString(this.type);
                return;
            case '\b':
                this.user = (User) InboundGatewayGsonParser.fromJson(jsonReader, User.class);
                return;
            case '\t':
                this.syncedAt = jsonReader.nextString(this.syncedAt);
                return;
            case '\n':
                this.roleId = jsonReader.nextLong(this.roleId);
                return;
            case 11:
                this.expireGracePeriod = jsonReader.nextInt(this.expireGracePeriod);
                return;
            case '\f':
                this.subscriberCount = jsonReader.nextInt(this.subscriberCount);
                return;
            default:
                jsonReader.skipValue();
                return;
        }
    }

    public boolean canEqual(Object obj) {
        return obj instanceof ModelGuildIntegration;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ModelGuildIntegration)) {
            return false;
        }
        ModelGuildIntegration modelGuildIntegration = (ModelGuildIntegration) obj;
        if (!modelGuildIntegration.canEqual(this) || isEnableEmoticons() != modelGuildIntegration.isEnableEmoticons() || isEnabled() != modelGuildIntegration.isEnabled() || getExpireBehavior() != modelGuildIntegration.getExpireBehavior() || getExpireGracePeriod() != modelGuildIntegration.getExpireGracePeriod() || getId() != modelGuildIntegration.getId() || getRoleId() != modelGuildIntegration.getRoleId() || getSubscriberCount() != modelGuildIntegration.getSubscriberCount() || isSyncing() != modelGuildIntegration.isSyncing()) {
            return false;
        }
        String name = getName();
        String name2 = modelGuildIntegration.getName();
        if (name != null ? !name.equals(name2) : name2 != null) {
            return false;
        }
        String syncedAt = getSyncedAt();
        String syncedAt2 = modelGuildIntegration.getSyncedAt();
        if (syncedAt != null ? !syncedAt.equals(syncedAt2) : syncedAt2 != null) {
            return false;
        }
        String type = getType();
        String type2 = modelGuildIntegration.getType();
        if (type != null ? !type.equals(type2) : type2 != null) {
            return false;
        }
        User user = getUser();
        User user2 = modelGuildIntegration.getUser();
        if (user != null ? !user.equals(user2) : user2 != null) {
            return false;
        }
        IntegrationAccount account = getAccount();
        IntegrationAccount account2 = modelGuildIntegration.getAccount();
        return account != null ? account.equals(account2) : account2 == null;
    }

    public IntegrationAccount getAccount() {
        return this.account;
    }

    public String getDisplayName() {
        if (!"twitch".equals(this.type)) {
            return this.name;
        }
        StringBuilder R = a.R("twitch.tv/");
        R.append(this.name);
        return R.toString();
    }

    public int getExpireBehavior() {
        return this.expireBehavior;
    }

    public int getExpireGracePeriod() {
        return this.expireGracePeriod;
    }

    public long getId() {
        return this.f2694id;
    }

    public String getName() {
        return this.name;
    }

    public long getRoleId() {
        return this.roleId;
    }

    public int getSubscriberCount() {
        return this.subscriberCount;
    }

    public String getSyncedAt() {
        return this.syncedAt;
    }

    public String getType() {
        return this.type;
    }

    public User getUser() {
        return this.user;
    }

    public int hashCode() {
        int i = 79;
        int i2 = ((isEnableEmoticons() ? 79 : 97) + 59) * 59;
        int i3 = isEnabled() ? 79 : 97;
        int expireGracePeriod = getExpireGracePeriod() + ((getExpireBehavior() + ((i2 + i3) * 59)) * 59);
        long id2 = getId();
        long roleId = getRoleId();
        int subscriberCount = (getSubscriberCount() + (((((expireGracePeriod * 59) + ((int) (id2 ^ (id2 >>> 32)))) * 59) + ((int) (roleId ^ (roleId >>> 32)))) * 59)) * 59;
        if (!isSyncing()) {
            i = 97;
        }
        String name = getName();
        int i4 = (subscriberCount + i) * 59;
        int i5 = 43;
        int hashCode = i4 + (name == null ? 43 : name.hashCode());
        String syncedAt = getSyncedAt();
        int hashCode2 = (hashCode * 59) + (syncedAt == null ? 43 : syncedAt.hashCode());
        String type = getType();
        int hashCode3 = (hashCode2 * 59) + (type == null ? 43 : type.hashCode());
        User user = getUser();
        int hashCode4 = (hashCode3 * 59) + (user == null ? 43 : user.hashCode());
        IntegrationAccount account = getAccount();
        int i6 = hashCode4 * 59;
        if (account != null) {
            i5 = account.hashCode();
        }
        return i6 + i5;
    }

    public boolean isEnableEmoticons() {
        return this.enableEmoticons;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public boolean isSyncing() {
        return this.syncing;
    }

    public String toString() {
        StringBuilder R = a.R("ModelGuildIntegration(enableEmoticons=");
        R.append(isEnableEmoticons());
        R.append(", enabled=");
        R.append(isEnabled());
        R.append(", expireBehavior=");
        R.append(getExpireBehavior());
        R.append(", expireGracePeriod=");
        R.append(getExpireGracePeriod());
        R.append(", id=");
        R.append(getId());
        R.append(", name=");
        R.append(getName());
        R.append(", roleId=");
        R.append(getRoleId());
        R.append(", subscriberCount=");
        R.append(getSubscriberCount());
        R.append(", syncedAt=");
        R.append(getSyncedAt());
        R.append(", syncing=");
        R.append(isSyncing());
        R.append(", type=");
        R.append(getType());
        R.append(", user=");
        R.append(getUser());
        R.append(", account=");
        R.append(getAccount());
        R.append(")");
        return R.toString();
    }
}
