package com.discord.models.domain;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.premium.SubscriptionPlan;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ModelEntitlement.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\u000e\n\u0002\b\u0019\b\u0086\b\u0018\u00002\u00020\u0001Bm\u0012\u0006\u0010\u001a\u001a\u00020\u0002\u0012\u0006\u0010\u001b\u001a\u00020\u0002\u0012\u0006\u0010\u001c\u001a\u00020\u0006\u0012\n\u0010\u001d\u001a\u00060\u0002j\u0002`\t\u0012\n\u0010\u001e\u001a\u00060\u0002j\u0002`\u000b\u0012\u0006\u0010\u001f\u001a\u00020\r\u0012\b\u0010 \u001a\u0004\u0018\u00010\u0010\u0012\b\u0010!\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\"\u001a\u0004\u0018\u00010\u0015\u0012\u000e\u0010#\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0018¢\u0006\u0004\b=\u0010>J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0014\u0010\n\u001a\u00060\u0002j\u0002`\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u0004J\u0014\u0010\f\u001a\u00060\u0002j\u0002`\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\u0004J\u0010\u0010\u000e\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0012\u0010\u0016\u001a\u0004\u0018\u00010\u0015HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0018\u0010\u0019\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0018HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u0014J\u008a\u0001\u0010$\u001a\u00020\u00002\b\b\u0002\u0010\u001a\u001a\u00020\u00022\b\b\u0002\u0010\u001b\u001a\u00020\u00022\b\b\u0002\u0010\u001c\u001a\u00020\u00062\f\b\u0002\u0010\u001d\u001a\u00060\u0002j\u0002`\t2\f\b\u0002\u0010\u001e\u001a\u00060\u0002j\u0002`\u000b2\b\b\u0002\u0010\u001f\u001a\u00020\r2\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u00102\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\u00152\u0010\b\u0002\u0010#\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0018HÆ\u0001¢\u0006\u0004\b$\u0010%J\u0010\u0010'\u001a\u00020&HÖ\u0001¢\u0006\u0004\b'\u0010(J\u0010\u0010)\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b)\u0010\bJ\u001a\u0010+\u001a\u00020\u00152\b\u0010*\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b+\u0010,R\u0019\u0010\u001c\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010-\u001a\u0004\b.\u0010\bR\u001b\u0010 \u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b \u0010/\u001a\u0004\b0\u0010\u0012R!\u0010#\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00188\u0006@\u0006¢\u0006\f\n\u0004\b#\u00101\u001a\u0004\b2\u0010\u0014R\u001b\u0010\"\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00103\u001a\u0004\b4\u0010\u0017R\u0019\u0010\u001a\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u00105\u001a\u0004\b6\u0010\u0004R\u001d\u0010\u001e\u001a\u00060\u0002j\u0002`\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00105\u001a\u0004\b7\u0010\u0004R\u0019\u0010\u001f\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00108\u001a\u0004\b9\u0010\u000fR\u001d\u0010\u001d\u001a\u00060\u0002j\u0002`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00105\u001a\u0004\b:\u0010\u0004R\u001b\u0010!\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b!\u00101\u001a\u0004\b;\u0010\u0014R\u0019\u0010\u001b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u00105\u001a\u0004\b<\u0010\u0004¨\u0006?"}, d2 = {"Lcom/discord/models/domain/ModelEntitlement;", "", "", "component1", "()J", "component2", "", "component3", "()I", "Lcom/discord/primitives/UserId;", "component4", "Lcom/discord/primitives/SkuId;", "component5", "Lcom/discord/models/domain/ModelSku;", "component6", "()Lcom/discord/models/domain/ModelSku;", "Lcom/discord/api/premium/SubscriptionPlan;", "component7", "()Lcom/discord/api/premium/SubscriptionPlan;", "component8", "()Ljava/lang/Long;", "", "component9", "()Ljava/lang/Boolean;", "Lcom/discord/primitives/PromoId;", "component10", ModelAuditLogEntry.CHANGE_KEY_ID, "applicationId", "type", "userId", "skuId", "sku", "subscriptionPlan", "parentId", "consumed", "promotionId", "copy", "(JJIJJLcom/discord/models/domain/ModelSku;Lcom/discord/api/premium/SubscriptionPlan;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Long;)Lcom/discord/models/domain/ModelEntitlement;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "I", "getType", "Lcom/discord/api/premium/SubscriptionPlan;", "getSubscriptionPlan", "Ljava/lang/Long;", "getPromotionId", "Ljava/lang/Boolean;", "getConsumed", "J", "getId", "getSkuId", "Lcom/discord/models/domain/ModelSku;", "getSku", "getUserId", "getParentId", "getApplicationId", HookHelper.constructorName, "(JJIJJLcom/discord/models/domain/ModelSku;Lcom/discord/api/premium/SubscriptionPlan;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Long;)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelEntitlement {
    private final long applicationId;
    private final Boolean consumed;

    /* renamed from: id  reason: collision with root package name */
    private final long f2691id;
    private final Long parentId;
    private final Long promotionId;
    private final ModelSku sku;
    private final long skuId;
    private final SubscriptionPlan subscriptionPlan;
    private final int type;
    private final long userId;

    public ModelEntitlement(long j, long j2, int i, long j3, long j4, ModelSku modelSku, SubscriptionPlan subscriptionPlan, Long l, Boolean bool, Long l2) {
        m.checkNotNullParameter(modelSku, "sku");
        this.f2691id = j;
        this.applicationId = j2;
        this.type = i;
        this.userId = j3;
        this.skuId = j4;
        this.sku = modelSku;
        this.subscriptionPlan = subscriptionPlan;
        this.parentId = l;
        this.consumed = bool;
        this.promotionId = l2;
    }

    public final long component1() {
        return this.f2691id;
    }

    public final Long component10() {
        return this.promotionId;
    }

    public final long component2() {
        return this.applicationId;
    }

    public final int component3() {
        return this.type;
    }

    public final long component4() {
        return this.userId;
    }

    public final long component5() {
        return this.skuId;
    }

    public final ModelSku component6() {
        return this.sku;
    }

    public final SubscriptionPlan component7() {
        return this.subscriptionPlan;
    }

    public final Long component8() {
        return this.parentId;
    }

    public final Boolean component9() {
        return this.consumed;
    }

    public final ModelEntitlement copy(long j, long j2, int i, long j3, long j4, ModelSku modelSku, SubscriptionPlan subscriptionPlan, Long l, Boolean bool, Long l2) {
        m.checkNotNullParameter(modelSku, "sku");
        return new ModelEntitlement(j, j2, i, j3, j4, modelSku, subscriptionPlan, l, bool, l2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModelEntitlement)) {
            return false;
        }
        ModelEntitlement modelEntitlement = (ModelEntitlement) obj;
        return this.f2691id == modelEntitlement.f2691id && this.applicationId == modelEntitlement.applicationId && this.type == modelEntitlement.type && this.userId == modelEntitlement.userId && this.skuId == modelEntitlement.skuId && m.areEqual(this.sku, modelEntitlement.sku) && m.areEqual(this.subscriptionPlan, modelEntitlement.subscriptionPlan) && m.areEqual(this.parentId, modelEntitlement.parentId) && m.areEqual(this.consumed, modelEntitlement.consumed) && m.areEqual(this.promotionId, modelEntitlement.promotionId);
    }

    public final long getApplicationId() {
        return this.applicationId;
    }

    public final Boolean getConsumed() {
        return this.consumed;
    }

    public final long getId() {
        return this.f2691id;
    }

    public final Long getParentId() {
        return this.parentId;
    }

    public final Long getPromotionId() {
        return this.promotionId;
    }

    public final ModelSku getSku() {
        return this.sku;
    }

    public final long getSkuId() {
        return this.skuId;
    }

    public final SubscriptionPlan getSubscriptionPlan() {
        return this.subscriptionPlan;
    }

    public final int getType() {
        return this.type;
    }

    public final long getUserId() {
        return this.userId;
    }

    public int hashCode() {
        long j = this.f2691id;
        long j2 = this.applicationId;
        long j3 = this.userId;
        long j4 = this.skuId;
        int i = ((((((((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + this.type) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + ((int) (j4 ^ (j4 >>> 32)))) * 31;
        ModelSku modelSku = this.sku;
        int i2 = 0;
        int hashCode = (i + (modelSku != null ? modelSku.hashCode() : 0)) * 31;
        SubscriptionPlan subscriptionPlan = this.subscriptionPlan;
        int hashCode2 = (hashCode + (subscriptionPlan != null ? subscriptionPlan.hashCode() : 0)) * 31;
        Long l = this.parentId;
        int hashCode3 = (hashCode2 + (l != null ? l.hashCode() : 0)) * 31;
        Boolean bool = this.consumed;
        int hashCode4 = (hashCode3 + (bool != null ? bool.hashCode() : 0)) * 31;
        Long l2 = this.promotionId;
        if (l2 != null) {
            i2 = l2.hashCode();
        }
        return hashCode4 + i2;
    }

    public String toString() {
        StringBuilder R = a.R("ModelEntitlement(id=");
        R.append(this.f2691id);
        R.append(", applicationId=");
        R.append(this.applicationId);
        R.append(", type=");
        R.append(this.type);
        R.append(", userId=");
        R.append(this.userId);
        R.append(", skuId=");
        R.append(this.skuId);
        R.append(", sku=");
        R.append(this.sku);
        R.append(", subscriptionPlan=");
        R.append(this.subscriptionPlan);
        R.append(", parentId=");
        R.append(this.parentId);
        R.append(", consumed=");
        R.append(this.consumed);
        R.append(", promotionId=");
        return a.F(R, this.promotionId, ")");
    }
}
