package com.discord.models.domain;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.application.Application;
import com.discord.api.premium.PremiumTier;
import com.discord.models.deserialization.gson.InboundGatewayGsonParser;
import com.discord.models.domain.Model;
import com.discord.models.domain.ModelSku;
import d0.z.d.m;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Ref$ObjectRef;
import rx.functions.Action1;
/* compiled from: ModelSku.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b$\n\u0002\u0018\u0002\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001:\u0006FGHIJKBs\u0012\n\u0010\u001e\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\u001f\u001a\u00020\u0006\u0012\n\u0010 \u001a\u00060\u0002j\u0002`\t\u0012\b\u0010!\u001a\u0004\u0018\u00010\u000b\u0012\u0006\u0010\"\u001a\u00020\u000e\u0012\u0006\u0010#\u001a\u00020\u0011\u0012\b\u0010$\u001a\u0004\u0018\u00010\u0014\u0012\u0006\u0010%\u001a\u00020\u0011\u0012\u001e\u0010&\u001a\u001a\u0012\b\u0012\u00060\u0011j\u0002`\u0019\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u0018j\u0004\u0018\u0001`\u001b¢\u0006\u0004\bD\u0010EJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0014\u0010\n\u001a\u00060\u0002j\u0002`\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u0005J\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u0012\u0010\u0015\u001a\u0004\u0018\u00010\u0014HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0013J(\u0010\u001c\u001a\u001a\u0012\b\u0012\u00060\u0011j\u0002`\u0019\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u0018j\u0004\u0018\u0001`\u001bHÆ\u0003¢\u0006\u0004\b\u001c\u0010\u001dJ\u008e\u0001\u0010'\u001a\u00020\u00002\f\b\u0002\u0010\u001e\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u001f\u001a\u00020\u00062\f\b\u0002\u0010 \u001a\u00060\u0002j\u0002`\t2\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u000b2\b\b\u0002\u0010\"\u001a\u00020\u000e2\b\b\u0002\u0010#\u001a\u00020\u00112\n\b\u0002\u0010$\u001a\u0004\u0018\u00010\u00142\b\b\u0002\u0010%\u001a\u00020\u00112 \b\u0002\u0010&\u001a\u001a\u0012\b\u0012\u00060\u0011j\u0002`\u0019\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u0018j\u0004\u0018\u0001`\u001bHÆ\u0001¢\u0006\u0004\b'\u0010(J\u0010\u0010)\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b)\u0010\bJ\u0010\u0010*\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b*\u0010\u0013J\u001a\u0010,\u001a\u00020\u000e2\b\u0010+\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b,\u0010-R\u0019\u0010\"\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010.\u001a\u0004\b/\u0010\u0010R\u0013\u00100\u001a\u00020\u000e8F@\u0006¢\u0006\u0006\u001a\u0004\b0\u0010\u0010R\u0019\u0010\u001f\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00101\u001a\u0004\b2\u0010\bR\u0013\u00103\u001a\u00020\u000e8F@\u0006¢\u0006\u0006\u001a\u0004\b3\u0010\u0010R1\u0010&\u001a\u001a\u0012\b\u0012\u00060\u0011j\u0002`\u0019\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u0018j\u0004\u0018\u0001`\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b&\u00104\u001a\u0004\b5\u0010\u001dR\u001d\u0010 \u001a\u00060\u0002j\u0002`\t8\u0006@\u0006¢\u0006\f\n\u0004\b \u00106\u001a\u0004\b7\u0010\u0005R\u0019\u0010#\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b#\u00108\u001a\u0004\b9\u0010\u0013R\u001d\u0010\u001e\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00106\u001a\u0004\b:\u0010\u0005R\u0019\u0010%\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b%\u00108\u001a\u0004\b;\u0010\u0013R\u001b\u0010!\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010<\u001a\u0004\b=\u0010\rR\u001b\u0010$\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010>\u001a\u0004\b?\u0010\u0016R\u0013\u0010C\u001a\u00020@8F@\u0006¢\u0006\u0006\u001a\u0004\bA\u0010B¨\u0006L"}, d2 = {"Lcom/discord/models/domain/ModelSku;", "", "", "Lcom/discord/primitives/ApplicationId;", "component1", "()J", "", "component2", "()Ljava/lang/String;", "Lcom/discord/primitives/SkuId;", "component3", "Lcom/discord/api/application/Application;", "component4", "()Lcom/discord/api/application/Application;", "", "component5", "()Z", "", "component6", "()I", "Lcom/discord/models/domain/ModelSku$Price;", "component7", "()Lcom/discord/models/domain/ModelSku$Price;", "component8", "", "Lcom/discord/primitives/PaymentGatewayId;", "Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;", "Lcom/discord/models/domain/ExternalSkuStrategies;", "component9", "()Ljava/util/Map;", "applicationId", ModelAuditLogEntry.CHANGE_KEY_NAME, ModelAuditLogEntry.CHANGE_KEY_ID, "application", "premium", "type", "price", "flags", "externalSkuStrategies", "copy", "(JLjava/lang/String;JLcom/discord/api/application/Application;ZILcom/discord/models/domain/ModelSku$Price;ILjava/util/Map;)Lcom/discord/models/domain/ModelSku;", "toString", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getPremium", "isAvailable", "Ljava/lang/String;", "getName", "isStickerPack", "Ljava/util/Map;", "getExternalSkuStrategies", "J", "getId", "I", "getType", "getApplicationId", "getFlags", "Lcom/discord/api/application/Application;", "getApplication", "Lcom/discord/models/domain/ModelSku$Price;", "getPrice", "Lcom/discord/models/domain/ModelSku$SkuCategory;", "getSkuCategory", "()Lcom/discord/models/domain/ModelSku$SkuCategory;", "skuCategory", HookHelper.constructorName, "(JLjava/lang/String;JLcom/discord/api/application/Application;ZILcom/discord/models/domain/ModelSku$Price;ILjava/util/Map;)V", "ExternalSkuStrategy", "ExternalStrategyTypes", "ModelPremiumSkuPrice", "Parser", "Price", "SkuCategory", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelSku {
    private final Application application;
    private final long applicationId;
    private final Map<Integer, ExternalSkuStrategy> externalSkuStrategies;
    private final int flags;

    /* renamed from: id  reason: collision with root package name */
    private final long f2703id;
    private final String name;
    private final boolean premium;
    private final Price price;
    private final int type;

    /* compiled from: ModelSku.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\b\u001a\u00020\u0005¢\u0006\u0004\b\u0014\u0010\u0015J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\t\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u000e\u0010\u0007J\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\b\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0013\u001a\u0004\b\u0003\u0010\u0007¨\u0006\u0016"}, d2 = {"Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;", "", "Lcom/discord/models/domain/ModelSku$ExternalStrategyTypes;", "getType", "()Lcom/discord/models/domain/ModelSku$ExternalStrategyTypes;", "", "component1", "()I", "type", "copy", "(I)Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", HookHelper.constructorName, "(I)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ExternalSkuStrategy {
        private final int type;

        public ExternalSkuStrategy(int i) {
            this.type = i;
        }

        public static /* synthetic */ ExternalSkuStrategy copy$default(ExternalSkuStrategy externalSkuStrategy, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                i = externalSkuStrategy.type;
            }
            return externalSkuStrategy.copy(i);
        }

        public final int component1() {
            return this.type;
        }

        public final ExternalSkuStrategy copy(int i) {
            return new ExternalSkuStrategy(i);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof ExternalSkuStrategy) && this.type == ((ExternalSkuStrategy) obj).type;
            }
            return true;
        }

        public final int getType() {
            return this.type;
        }

        public int hashCode() {
            return this.type;
        }

        public String toString() {
            return a.A(a.R("ExternalSkuStrategy(type="), this.type, ")");
        }

        /* renamed from: getType  reason: collision with other method in class */
        public final ExternalStrategyTypes m2getType() {
            return ExternalStrategyTypes.Companion.from(this.type);
        }
    }

    /* compiled from: ModelSku.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\b\n\u0002\b\f\b\u0086\u0001\u0018\u0000 \t2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\tB\u0013\b\u0002\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0007\u0010\bR\u001b\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\r¨\u0006\u000e"}, d2 = {"Lcom/discord/models/domain/ModelSku$ExternalStrategyTypes;", "", "", "intRepresentation", "Ljava/lang/Integer;", "getIntRepresentation", "()Ljava/lang/Integer;", HookHelper.constructorName, "(Ljava/lang/String;ILjava/lang/Integer;)V", "Companion", "CONSTANT", "APPLE", "GOOGLE", "UNKNOWN", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum ExternalStrategyTypes {
        CONSTANT(1),
        APPLE(2),
        GOOGLE(3),
        UNKNOWN(null);
        
        public static final Companion Companion = new Companion(null);
        private final Integer intRepresentation;

        /* compiled from: ModelSku.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/models/domain/ModelSku$ExternalStrategyTypes$Companion;", "", "", "externalStrategyInt", "Lcom/discord/models/domain/ModelSku$ExternalStrategyTypes;", "from", "(I)Lcom/discord/models/domain/ModelSku$ExternalStrategyTypes;", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Companion {
            private Companion() {
            }

            public final ExternalStrategyTypes from(int i) {
                ExternalStrategyTypes externalStrategyTypes;
                ExternalStrategyTypes[] values = ExternalStrategyTypes.values();
                int i2 = 0;
                while (true) {
                    if (i2 >= 4) {
                        externalStrategyTypes = null;
                        break;
                    }
                    externalStrategyTypes = values[i2];
                    Integer intRepresentation = externalStrategyTypes.getIntRepresentation();
                    if (intRepresentation != null && intRepresentation.intValue() == i) {
                        break;
                    }
                    i2++;
                }
                return externalStrategyTypes != null ? externalStrategyTypes : ExternalStrategyTypes.UNKNOWN;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        ExternalStrategyTypes(Integer num) {
            this.intRepresentation = num;
        }

        public final Integer getIntRepresentation() {
            return this.intRepresentation;
        }
    }

    /* compiled from: ModelSku.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J$\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u00022\b\b\u0002\u0010\u0007\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\r\u0010\u0004J\u001a\u0010\u0010\u001a\u00020\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0006\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0012\u001a\u0004\b\u0013\u0010\u0004R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0012\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;", "", "", "component1", "()I", "component2", "amount", "percentage", "copy", "(II)Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getAmount", "getPercentage", HookHelper.constructorName, "(II)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ModelPremiumSkuPrice {
        private final int amount;
        private final int percentage;

        public ModelPremiumSkuPrice(int i, int i2) {
            this.amount = i;
            this.percentage = i2;
        }

        public static /* synthetic */ ModelPremiumSkuPrice copy$default(ModelPremiumSkuPrice modelPremiumSkuPrice, int i, int i2, int i3, Object obj) {
            if ((i3 & 1) != 0) {
                i = modelPremiumSkuPrice.amount;
            }
            if ((i3 & 2) != 0) {
                i2 = modelPremiumSkuPrice.percentage;
            }
            return modelPremiumSkuPrice.copy(i, i2);
        }

        public final int component1() {
            return this.amount;
        }

        public final int component2() {
            return this.percentage;
        }

        public final ModelPremiumSkuPrice copy(int i, int i2) {
            return new ModelPremiumSkuPrice(i, i2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ModelPremiumSkuPrice)) {
                return false;
            }
            ModelPremiumSkuPrice modelPremiumSkuPrice = (ModelPremiumSkuPrice) obj;
            return this.amount == modelPremiumSkuPrice.amount && this.percentage == modelPremiumSkuPrice.percentage;
        }

        public final int getAmount() {
            return this.amount;
        }

        public final int getPercentage() {
            return this.percentage;
        }

        public int hashCode() {
            return (this.amount * 31) + this.percentage;
        }

        public String toString() {
            StringBuilder R = a.R("ModelPremiumSkuPrice(amount=");
            R.append(this.amount);
            R.append(", percentage=");
            return a.A(R, this.percentage, ")");
        }
    }

    /* compiled from: ModelSku.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/models/domain/ModelSku$Parser;", "Lcom/discord/models/domain/Model$Parser;", "Lcom/discord/models/domain/ModelSku;", "Lcom/discord/models/domain/Model$JsonReader;", "reader", "parse", "(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelSku;", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Parser implements Model.Parser<ModelSku> {
        public static final Parser INSTANCE = new Parser();

        private Parser() {
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // com.discord.models.domain.Model.Parser
        public ModelSku parse(final Model.JsonReader jsonReader) {
            final Ref$ObjectRef a02 = a.a0(jsonReader, "reader");
            a02.element = null;
            final Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
            ref$ObjectRef.element = null;
            final Ref$ObjectRef ref$ObjectRef2 = new Ref$ObjectRef();
            ref$ObjectRef2.element = null;
            final Ref$ObjectRef ref$ObjectRef3 = new Ref$ObjectRef();
            ref$ObjectRef3.element = null;
            final Ref$ObjectRef ref$ObjectRef4 = new Ref$ObjectRef();
            ref$ObjectRef4.element = null;
            final Ref$ObjectRef ref$ObjectRef5 = new Ref$ObjectRef();
            ref$ObjectRef5.element = null;
            final Ref$ObjectRef ref$ObjectRef6 = new Ref$ObjectRef();
            ref$ObjectRef6.element = null;
            final Ref$ObjectRef ref$ObjectRef7 = new Ref$ObjectRef();
            ref$ObjectRef7.element = null;
            jsonReader.nextObject(new Action1<String>() { // from class: com.discord.models.domain.ModelSku$Parser$parse$1
                public final void call(String str) {
                    if (str != null) {
                        switch (str.hashCode()) {
                            case -1287148950:
                                if (str.equals(ModelAuditLogEntry.CHANGE_KEY_APPLICATION_ID)) {
                                    Ref$ObjectRef.this.element = (T) jsonReader.nextLongOrNull();
                                    return;
                                }
                                break;
                            case -318452137:
                                if (str.equals("premium")) {
                                    ref$ObjectRef4.element = (T) jsonReader.nextBooleanOrNull();
                                    return;
                                }
                                break;
                            case 3355:
                                if (str.equals(ModelAuditLogEntry.CHANGE_KEY_ID)) {
                                    ref$ObjectRef2.element = (T) jsonReader.nextLongOrNull();
                                    return;
                                }
                                break;
                            case 3373707:
                                if (str.equals(ModelAuditLogEntry.CHANGE_KEY_NAME)) {
                                    ref$ObjectRef.element = (T) jsonReader.nextStringOrNull();
                                    return;
                                }
                                break;
                            case 3575610:
                                if (str.equals("type")) {
                                    ref$ObjectRef5.element = (T) jsonReader.nextIntOrNull();
                                    return;
                                }
                                break;
                            case 97513095:
                                if (str.equals("flags")) {
                                    ref$ObjectRef7.element = (T) jsonReader.nextIntOrNull();
                                    return;
                                }
                                break;
                            case 106934601:
                                if (str.equals("price")) {
                                    ref$ObjectRef6.element = (T) ModelSku.Price.Parser.INSTANCE.parse(jsonReader);
                                    return;
                                }
                                break;
                            case 1554253136:
                                if (str.equals("application")) {
                                    ref$ObjectRef3.element = (T) ((Application) InboundGatewayGsonParser.fromJson(jsonReader, Application.class));
                                    return;
                                }
                                break;
                        }
                    }
                    jsonReader.skipValue();
                }
            });
            Long l = (Long) a02.element;
            long longValue = l != null ? l.longValue() : 0L;
            String str = (String) ref$ObjectRef.element;
            if (str == null) {
                str = "";
            }
            String str2 = str;
            Long l2 = (Long) ref$ObjectRef2.element;
            long longValue2 = l2 != null ? l2.longValue() : 0L;
            Application application = (Application) ref$ObjectRef3.element;
            Boolean bool = (Boolean) ref$ObjectRef4.element;
            m.checkNotNull(bool);
            boolean booleanValue = bool.booleanValue();
            Integer num = (Integer) ref$ObjectRef5.element;
            m.checkNotNull(num);
            int intValue = num.intValue();
            Price price = (Price) ref$ObjectRef6.element;
            Integer num2 = (Integer) ref$ObjectRef7.element;
            m.checkNotNull(num2);
            return new ModelSku(longValue, str2, longValue2, application, booleanValue, intValue, price, num2.intValue(), null);
        }
    }

    /* compiled from: ModelSku.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001:\u0001 B/\u0012\u0006\u0010\r\u001a\u00020\u0002\u0012\u0006\u0010\u000e\u001a\u00020\u0005\u0012\u0016\b\u0002\u0010\u000f\u001a\u0010\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n\u0018\u00010\b¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u001e\u0010\u000b\u001a\u0010\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ<\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\r\u001a\u00020\u00022\b\b\u0002\u0010\u000e\u001a\u00020\u00052\u0016\b\u0002\u0010\u000f\u001a\u0010\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n\u0018\u00010\bHÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0007J\u0010\u0010\u0013\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0004J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R'\u0010\u000f\u001a\u0010\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0018\u001a\u0004\b\u0019\u0010\fR\u0019\u0010\r\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001a\u001a\u0004\b\u001b\u0010\u0004R\u0019\u0010\u000e\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001c\u001a\u0004\b\u001d\u0010\u0007¨\u0006!"}, d2 = {"Lcom/discord/models/domain/ModelSku$Price;", "", "", "component1", "()I", "", "component2", "()Ljava/lang/String;", "", "Lcom/discord/api/premium/PremiumTier;", "Lcom/discord/models/domain/ModelSku$ModelPremiumSkuPrice;", "component3", "()Ljava/util/Map;", "amount", "currency", "premium", "copy", "(ILjava/lang/String;Ljava/util/Map;)Lcom/discord/models/domain/ModelSku$Price;", "toString", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getPremium", "I", "getAmount", "Ljava/lang/String;", "getCurrency", HookHelper.constructorName, "(ILjava/lang/String;Ljava/util/Map;)V", "Parser", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Price {
        private final int amount;
        private final String currency;
        private final Map<PremiumTier, ModelPremiumSkuPrice> premium;

        /* compiled from: ModelSku.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/models/domain/ModelSku$Price$Parser;", "Lcom/discord/models/domain/Model$Parser;", "Lcom/discord/models/domain/ModelSku$Price;", "Lcom/discord/models/domain/Model$JsonReader;", "reader", "parse", "(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelSku$Price;", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Parser implements Model.Parser<Price> {
            public static final Parser INSTANCE = new Parser();

            private Parser() {
            }

            /* JADX WARN: Can't rename method to resolve collision */
            @Override // com.discord.models.domain.Model.Parser
            public Price parse(final Model.JsonReader jsonReader) {
                final Ref$ObjectRef a02 = a.a0(jsonReader, "reader");
                a02.element = null;
                final Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
                ref$ObjectRef.element = null;
                jsonReader.nextObject(new Action1<String>() { // from class: com.discord.models.domain.ModelSku$Price$Parser$parse$1
                    public final void call(String str) {
                        if (str != null) {
                            int hashCode = str.hashCode();
                            if (hashCode != -1413853096) {
                                if (hashCode == 575402001 && str.equals("currency")) {
                                    ref$ObjectRef.element = (T) jsonReader.nextStringOrNull();
                                    return;
                                }
                            } else if (str.equals("amount")) {
                                Ref$ObjectRef.this.element = (T) jsonReader.nextIntOrNull();
                                return;
                            }
                        }
                        jsonReader.skipValue();
                    }
                });
                Integer num = (Integer) a02.element;
                int intValue = num != null ? num.intValue() : -1;
                String str = (String) ref$ObjectRef.element;
                if (str == null) {
                    str = "";
                }
                return new Price(intValue, str, null, 4, null);
            }
        }

        public Price(int i, String str, Map<PremiumTier, ModelPremiumSkuPrice> map) {
            m.checkNotNullParameter(str, "currency");
            this.amount = i;
            this.currency = str;
            this.premium = map;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ Price copy$default(Price price, int i, String str, Map map, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                i = price.amount;
            }
            if ((i2 & 2) != 0) {
                str = price.currency;
            }
            if ((i2 & 4) != 0) {
                map = price.premium;
            }
            return price.copy(i, str, map);
        }

        public final int component1() {
            return this.amount;
        }

        public final String component2() {
            return this.currency;
        }

        public final Map<PremiumTier, ModelPremiumSkuPrice> component3() {
            return this.premium;
        }

        public final Price copy(int i, String str, Map<PremiumTier, ModelPremiumSkuPrice> map) {
            m.checkNotNullParameter(str, "currency");
            return new Price(i, str, map);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Price)) {
                return false;
            }
            Price price = (Price) obj;
            return this.amount == price.amount && m.areEqual(this.currency, price.currency) && m.areEqual(this.premium, price.premium);
        }

        public final int getAmount() {
            return this.amount;
        }

        public final String getCurrency() {
            return this.currency;
        }

        public final Map<PremiumTier, ModelPremiumSkuPrice> getPremium() {
            return this.premium;
        }

        public int hashCode() {
            int i = this.amount * 31;
            String str = this.currency;
            int i2 = 0;
            int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
            Map<PremiumTier, ModelPremiumSkuPrice> map = this.premium;
            if (map != null) {
                i2 = map.hashCode();
            }
            return hashCode + i2;
        }

        public String toString() {
            StringBuilder R = a.R("Price(amount=");
            R.append(this.amount);
            R.append(", currency=");
            R.append(this.currency);
            R.append(", premium=");
            return a.L(R, this.premium, ")");
        }

        public /* synthetic */ Price(int i, String str, Map map, int i2, DefaultConstructorMarker defaultConstructorMarker) {
            this(i, str, (i2 & 4) != 0 ? null : map);
        }
    }

    /* compiled from: ModelSku.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/models/domain/ModelSku$SkuCategory;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "GAME", "NITRO", "NITRO_CLASSIC", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum SkuCategory {
        GAME,
        NITRO,
        NITRO_CLASSIC
    }

    public ModelSku(long j, String str, long j2, Application application, boolean z2, int i, Price price, int i2, Map<Integer, ExternalSkuStrategy> map) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        this.applicationId = j;
        this.name = str;
        this.f2703id = j2;
        this.application = application;
        this.premium = z2;
        this.type = i;
        this.price = price;
        this.flags = i2;
        this.externalSkuStrategies = map;
    }

    public final long component1() {
        return this.applicationId;
    }

    public final String component2() {
        return this.name;
    }

    public final long component3() {
        return this.f2703id;
    }

    public final Application component4() {
        return this.application;
    }

    public final boolean component5() {
        return this.premium;
    }

    public final int component6() {
        return this.type;
    }

    public final Price component7() {
        return this.price;
    }

    public final int component8() {
        return this.flags;
    }

    public final Map<Integer, ExternalSkuStrategy> component9() {
        return this.externalSkuStrategies;
    }

    public final ModelSku copy(long j, String str, long j2, Application application, boolean z2, int i, Price price, int i2, Map<Integer, ExternalSkuStrategy> map) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        return new ModelSku(j, str, j2, application, z2, i, price, i2, map);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModelSku)) {
            return false;
        }
        ModelSku modelSku = (ModelSku) obj;
        return this.applicationId == modelSku.applicationId && m.areEqual(this.name, modelSku.name) && this.f2703id == modelSku.f2703id && m.areEqual(this.application, modelSku.application) && this.premium == modelSku.premium && this.type == modelSku.type && m.areEqual(this.price, modelSku.price) && this.flags == modelSku.flags && m.areEqual(this.externalSkuStrategies, modelSku.externalSkuStrategies);
    }

    public final Application getApplication() {
        return this.application;
    }

    public final long getApplicationId() {
        return this.applicationId;
    }

    public final Map<Integer, ExternalSkuStrategy> getExternalSkuStrategies() {
        return this.externalSkuStrategies;
    }

    public final int getFlags() {
        return this.flags;
    }

    public final long getId() {
        return this.f2703id;
    }

    public final String getName() {
        return this.name;
    }

    public final boolean getPremium() {
        return this.premium;
    }

    public final Price getPrice() {
        return this.price;
    }

    public final SkuCategory getSkuCategory() {
        long j = this.f2703id;
        if (j == ModelSkuKt.PREMIUM_TIER_1_SKU_ID) {
            return SkuCategory.NITRO_CLASSIC;
        }
        if (j == ModelSkuKt.PREMIUM_TIER_2_SKU_ID) {
            return SkuCategory.NITRO;
        }
        return SkuCategory.GAME;
    }

    public final int getType() {
        return this.type;
    }

    public int hashCode() {
        long j = this.applicationId;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        String str = this.name;
        int i2 = 0;
        int hashCode = str != null ? str.hashCode() : 0;
        long j2 = this.f2703id;
        int i3 = (((i + hashCode) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31;
        Application application = this.application;
        int hashCode2 = (i3 + (application != null ? application.hashCode() : 0)) * 31;
        boolean z2 = this.premium;
        if (z2) {
            z2 = true;
        }
        int i4 = z2 ? 1 : 0;
        int i5 = z2 ? 1 : 0;
        int i6 = (((hashCode2 + i4) * 31) + this.type) * 31;
        Price price = this.price;
        int hashCode3 = (((i6 + (price != null ? price.hashCode() : 0)) * 31) + this.flags) * 31;
        Map<Integer, ExternalSkuStrategy> map = this.externalSkuStrategies;
        if (map != null) {
            i2 = map.hashCode();
        }
        return hashCode3 + i2;
    }

    public final boolean isAvailable() {
        return (this.flags & 4) > 0;
    }

    public final boolean isStickerPack() {
        return (this.flags & 16) > 0;
    }

    public String toString() {
        StringBuilder R = a.R("ModelSku(applicationId=");
        R.append(this.applicationId);
        R.append(", name=");
        R.append(this.name);
        R.append(", id=");
        R.append(this.f2703id);
        R.append(", application=");
        R.append(this.application);
        R.append(", premium=");
        R.append(this.premium);
        R.append(", type=");
        R.append(this.type);
        R.append(", price=");
        R.append(this.price);
        R.append(", flags=");
        R.append(this.flags);
        R.append(", externalSkuStrategies=");
        return a.L(R, this.externalSkuStrategies, ")");
    }
}
