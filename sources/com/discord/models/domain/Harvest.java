package com.discord.models.domain;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.time.TimeUtils;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemGuildWelcomeKt;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ModelUserConsents.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0011\u001a\u00020\n\u0012\n\u0010\u0012\u001a\u00060\u0002j\u0002`\r\u0012\u0006\u0010\u0013\u001a\u00020\n\u0012\b\u0010\u0014\u001a\u0004\u0018\u00010\n¢\u0006\u0004\b$\u0010%J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\r\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0004J\u0017\u0010\b\u001a\u00020\u00072\b\b\u0002\u0010\u0006\u001a\u00020\u0002¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0014\u0010\u000e\u001a\u00060\u0002j\u0002`\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u0004J\u0010\u0010\u000f\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000f\u0010\fJ\u0012\u0010\u0010\u001a\u0004\u0018\u00010\nHÆ\u0003¢\u0006\u0004\b\u0010\u0010\fJ>\u0010\u0015\u001a\u00020\u00002\b\b\u0002\u0010\u0011\u001a\u00020\n2\f\b\u0002\u0010\u0012\u001a\u00060\u0002j\u0002`\r2\b\b\u0002\u0010\u0013\u001a\u00020\n2\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\nHÆ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u0017\u0010\fJ\u0010\u0010\u0019\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u001a\u0010\u001c\u001a\u00020\u00072\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u0019\u0010\u0013\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u001e\u001a\u0004\b\u001f\u0010\fR\u001d\u0010\u0012\u001a\u00060\u0002j\u0002`\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010 \u001a\u0004\b!\u0010\u0004R\u001b\u0010\u0014\u001a\u0004\u0018\u00010\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u001e\u001a\u0004\b\"\u0010\fR\u0019\u0010\u0011\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001e\u001a\u0004\b#\u0010\f¨\u0006&"}, d2 = {"Lcom/discord/models/domain/Harvest;", "", "", "createdAtInMillis", "()J", "nextAvailableRequestInMillis", "now", "", "canRequest", "(J)Z", "", "component1", "()Ljava/lang/String;", "Lcom/discord/primitives/UserId;", "component2", "component3", "component4", ModelAuditLogEntry.CHANGE_KEY_ID, "userId", "createdAt", "completedAt", "copy", "(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)Lcom/discord/models/domain/Harvest;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getCreatedAt", "J", "getUserId", "getCompletedAt", "getId", HookHelper.constructorName, "(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class Harvest {
    private final String completedAt;
    private final String createdAt;

    /* renamed from: id  reason: collision with root package name */
    private final String f2686id;
    private final long userId;

    public Harvest(String str, long j, String str2, String str3) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_ID);
        m.checkNotNullParameter(str2, "createdAt");
        this.f2686id = str;
        this.userId = j;
        this.createdAt = str2;
        this.completedAt = str3;
    }

    public static /* synthetic */ boolean canRequest$default(Harvest harvest, long j, int i, Object obj) {
        if ((i & 1) != 0) {
            j = ClockFactory.get().currentTimeMillis();
        }
        return harvest.canRequest(j);
    }

    public static /* synthetic */ Harvest copy$default(Harvest harvest, String str, long j, String str2, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = harvest.f2686id;
        }
        if ((i & 2) != 0) {
            j = harvest.userId;
        }
        long j2 = j;
        if ((i & 4) != 0) {
            str2 = harvest.createdAt;
        }
        String str4 = str2;
        if ((i & 8) != 0) {
            str3 = harvest.completedAt;
        }
        return harvest.copy(str, j2, str4, str3);
    }

    private final long createdAtInMillis() {
        return TimeUtils.parseUTCDate(this.createdAt);
    }

    public final boolean canRequest(long j) {
        return j > nextAvailableRequestInMillis();
    }

    public final String component1() {
        return this.f2686id;
    }

    public final long component2() {
        return this.userId;
    }

    public final String component3() {
        return this.createdAt;
    }

    public final String component4() {
        return this.completedAt;
    }

    public final Harvest copy(String str, long j, String str2, String str3) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_ID);
        m.checkNotNullParameter(str2, "createdAt");
        return new Harvest(str, j, str2, str3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Harvest)) {
            return false;
        }
        Harvest harvest = (Harvest) obj;
        return m.areEqual(this.f2686id, harvest.f2686id) && this.userId == harvest.userId && m.areEqual(this.createdAt, harvest.createdAt) && m.areEqual(this.completedAt, harvest.completedAt);
    }

    public final String getCompletedAt() {
        return this.completedAt;
    }

    public final String getCreatedAt() {
        return this.createdAt;
    }

    public final String getId() {
        return this.f2686id;
    }

    public final long getUserId() {
        return this.userId;
    }

    public int hashCode() {
        String str = this.f2686id;
        int i = 0;
        int hashCode = str != null ? str.hashCode() : 0;
        long j = this.userId;
        int i2 = ((hashCode * 31) + ((int) (j ^ (j >>> 32)))) * 31;
        String str2 = this.createdAt;
        int hashCode2 = (i2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.completedAt;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode2 + i;
    }

    public final long nextAvailableRequestInMillis() {
        return createdAtInMillis() + WidgetChatListAdapterItemGuildWelcomeKt.OLD_GUILD_AGE_THRESHOLD;
    }

    public String toString() {
        StringBuilder R = a.R("Harvest(id=");
        R.append(this.f2686id);
        R.append(", userId=");
        R.append(this.userId);
        R.append(", createdAt=");
        R.append(this.createdAt);
        R.append(", completedAt=");
        return a.H(R, this.completedAt, ")");
    }
}
