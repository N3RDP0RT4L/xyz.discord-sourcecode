package com.discord.models.domain;

import andhook.lib.HookHelper;
import androidx.browser.customtabs.CustomTabsCallback;
import b.d.b.a.a;
import com.discord.api.guildmember.GuildMember;
import com.discord.models.deserialization.gson.InboundGatewayGsonParser;
import com.discord.models.domain.Model;
import com.discord.models.domain.ModelGuildMemberListUpdate;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Ref$IntRef;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlin.ranges.IntRange;
import rx.functions.Action1;
/* compiled from: ModelGuildMemberListUpdate.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0010\b\u0086\b\u0018\u0000 '2\u00020\u0001:\u0004'()*B;\u0012\n\u0010\u0010\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010\u0011\u001a\u00060\u0006j\u0002`\u0007\u0012\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u000b0\n\u0012\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u000e0\n¢\u0006\u0004\b%\u0010&J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\b\u001a\u00060\u0006j\u0002`\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0016\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\nHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0016\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\nHÆ\u0003¢\u0006\u0004\b\u000f\u0010\rJL\u0010\u0014\u001a\u00020\u00002\f\b\u0002\u0010\u0010\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010\u0011\u001a\u00060\u0006j\u0002`\u00072\u000e\b\u0002\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u000b0\n2\u000e\b\u0002\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u000e0\nHÆ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0016\u0010\tJ\u0010\u0010\u0018\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u001a\u0010\u001c\u001a\u00020\u001b2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u001d\u0010\u0010\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001e\u001a\u0004\b\u001f\u0010\u0005R\u001f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u000b0\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010 \u001a\u0004\b!\u0010\rR\u001d\u0010\u0011\u001a\u00060\u0006j\u0002`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\"\u001a\u0004\b#\u0010\tR\u001f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u000e0\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010 \u001a\u0004\b$\u0010\r¨\u0006+"}, d2 = {"Lcom/discord/models/domain/ModelGuildMemberListUpdate;", "", "", "Lcom/discord/primitives/GuildId;", "component1", "()J", "", "Lcom/discord/primitives/MemberListId;", "component2", "()Ljava/lang/String;", "", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation;", "component3", "()Ljava/util/List;", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;", "component4", "guildId", ModelAuditLogEntry.CHANGE_KEY_ID, "operations", "groups", "copy", "(JLjava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/discord/models/domain/ModelGuildMemberListUpdate;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getGuildId", "Ljava/util/List;", "getOperations", "Ljava/lang/String;", "getId", "getGroups", HookHelper.constructorName, "(JLjava/lang/String;Ljava/util/List;Ljava/util/List;)V", "Companion", "Group", "Operation", "Parser", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelGuildMemberListUpdate {
    public static final Companion Companion = new Companion(null);
    public static final String EVERYONE_ID = "everyone";
    private final List<Group> groups;
    private final long guildId;

    /* renamed from: id  reason: collision with root package name */
    private final String f2696id;
    private final List<Operation> operations;

    /* compiled from: ModelGuildMemberListUpdate.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/models/domain/ModelGuildMemberListUpdate$Companion;", "", "", "EVERYONE_ID", "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: ModelGuildMemberListUpdate.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001:\u0002\u001d\u001eB\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\f\u0010\u0004J\u0010\u0010\r\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\r\u0010\u0007J\u001a\u0010\u0010\u001a\u00020\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0013\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004¨\u0006\u001f"}, d2 = {"Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;", "", "", "component1", "()Ljava/lang/String;", "", "component2", "()I", ModelAuditLogEntry.CHANGE_KEY_ID, "count", "copy", "(Ljava/lang/String;I)Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;", "toString", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group$Type;", "type", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group$Type;", "getType", "()Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group$Type;", "I", "getCount", "Ljava/lang/String;", "getId", HookHelper.constructorName, "(Ljava/lang/String;I)V", "Parser", "Type", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Group {
        private final int count;

        /* renamed from: id  reason: collision with root package name */
        private final String f2697id;
        private final Type type;

        /* compiled from: ModelGuildMemberListUpdate.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group$Parser;", "Lcom/discord/models/domain/Model$Parser;", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;", "Lcom/discord/models/domain/Model$JsonReader;", "reader", "parse", "(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Parser implements Model.Parser<Group> {
            public static final Parser INSTANCE = new Parser();

            private Parser() {
            }

            /* JADX WARN: Can't rename method to resolve collision */
            @Override // com.discord.models.domain.Model.Parser
            public Group parse(final Model.JsonReader jsonReader) {
                m.checkNotNullParameter(jsonReader, "reader");
                final Ref$IntRef ref$IntRef = new Ref$IntRef();
                ref$IntRef.element = 0;
                final Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
                ref$ObjectRef.element = "";
                jsonReader.nextObject(new Action1<String>() { // from class: com.discord.models.domain.ModelGuildMemberListUpdate$Group$Parser$parse$1
                    public final void call(String str) {
                        if (str != null) {
                            int hashCode = str.hashCode();
                            if (hashCode != 3355) {
                                if (hashCode == 94851343 && str.equals("count")) {
                                    Ref$IntRef ref$IntRef2 = ref$IntRef;
                                    Integer nextIntOrNull = jsonReader.nextIntOrNull();
                                    m.checkNotNullExpressionValue(nextIntOrNull, "reader.nextIntOrNull()");
                                    ref$IntRef2.element = nextIntOrNull.intValue();
                                    return;
                                }
                            } else if (str.equals(ModelAuditLogEntry.CHANGE_KEY_ID)) {
                                Ref$ObjectRef ref$ObjectRef2 = Ref$ObjectRef.this;
                                T t = (T) jsonReader.nextString("");
                                m.checkNotNullExpressionValue(t, "reader.nextString(\"\")");
                                ref$ObjectRef2.element = t;
                                return;
                            }
                        }
                        jsonReader.skipValue();
                    }
                });
                return new Group((String) ref$ObjectRef.element, ref$IntRef.element);
            }
        }

        /* compiled from: ModelGuildMemberListUpdate.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group$Type;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "ROLE", "OFFLINE", "ONLINE", "app_models_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public enum Type {
            ROLE,
            OFFLINE,
            ONLINE
        }

        public Group(String str, int i) {
            Type type;
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_ID);
            this.f2697id = str;
            this.count = i;
            int hashCode = str.hashCode();
            if (hashCode != -1548612125) {
                if (hashCode == -1012222381 && str.equals(CustomTabsCallback.ONLINE_EXTRAS_KEY)) {
                    type = Type.ONLINE;
                }
                type = Type.ROLE;
            } else {
                if (str.equals("offline")) {
                    type = Type.OFFLINE;
                }
                type = Type.ROLE;
            }
            this.type = type;
        }

        public static /* synthetic */ Group copy$default(Group group, String str, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                str = group.f2697id;
            }
            if ((i2 & 2) != 0) {
                i = group.count;
            }
            return group.copy(str, i);
        }

        public final String component1() {
            return this.f2697id;
        }

        public final int component2() {
            return this.count;
        }

        public final Group copy(String str, int i) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_ID);
            return new Group(str, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Group)) {
                return false;
            }
            Group group = (Group) obj;
            return m.areEqual(this.f2697id, group.f2697id) && this.count == group.count;
        }

        public final int getCount() {
            return this.count;
        }

        public final String getId() {
            return this.f2697id;
        }

        public final Type getType() {
            return this.type;
        }

        public int hashCode() {
            String str = this.f2697id;
            return ((str != null ? str.hashCode() : 0) * 31) + this.count;
        }

        public String toString() {
            StringBuilder R = a.R("Group(id=");
            R.append(this.f2697id);
            R.append(", count=");
            return a.A(R, this.count, ")");
        }
    }

    /* compiled from: ModelGuildMemberListUpdate.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0007\u000e\u000f\u0010\u0011\u0012\u0013\u0014B'\b\u0002\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\u0010\b\u0002\u0010\b\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u0007¢\u0006\u0004\b\f\u0010\rR\u001e\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R$\u0010\b\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u00078\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000b\u0082\u0001\u0005\u0015\u0016\u0017\u0018\u0019¨\u0006\u001a"}, d2 = {"Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation;", "", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;", "item", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;", "getItem", "()Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;", "", "items", "Ljava/util/List;", "getItems", "()Ljava/util/List;", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;Ljava/util/List;)V", "Delete", "Insert", "Invalidate", "Item", "Parser", "Sync", "Update", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Sync;", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Update;", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Insert;", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Delete;", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Invalidate;", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class Operation {
        private final Item item;
        private final List<Item> items;

        /* compiled from: ModelGuildMemberListUpdate.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Delete;", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation;", "", "component1", "()I", "index", "copy", "(I)Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Delete;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getIndex", HookHelper.constructorName, "(I)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Delete extends Operation {
            private final int index;

            public Delete(int i) {
                super(null, null, 3, null);
                this.index = i;
            }

            public static /* synthetic */ Delete copy$default(Delete delete, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = delete.index;
                }
                return delete.copy(i);
            }

            public final int component1() {
                return this.index;
            }

            public final Delete copy(int i) {
                return new Delete(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Delete) && this.index == ((Delete) obj).index;
                }
                return true;
            }

            public final int getIndex() {
                return this.index;
            }

            public int hashCode() {
                return this.index;
            }

            public String toString() {
                return a.A(a.R("Delete(index="), this.index, ")");
            }
        }

        /* compiled from: ModelGuildMemberListUpdate.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0004J\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004R\u001c\u0010\t\u001a\u00020\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0007¨\u0006\u001b"}, d2 = {"Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Insert;", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation;", "", "component1", "()I", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;", "component2", "()Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;", "index", "item", "copy", "(ILcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;)Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Insert;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getIndex", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;", "getItem", HookHelper.constructorName, "(ILcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Insert extends Operation {
            private final int index;
            private final Item item;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Insert(int i, Item item) {
                super(item, null, 2, null);
                m.checkNotNullParameter(item, "item");
                this.index = i;
                this.item = item;
            }

            public static /* synthetic */ Insert copy$default(Insert insert, int i, Item item, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = insert.index;
                }
                if ((i2 & 2) != 0) {
                    item = insert.getItem();
                }
                return insert.copy(i, item);
            }

            public final int component1() {
                return this.index;
            }

            public final Item component2() {
                return getItem();
            }

            public final Insert copy(int i, Item item) {
                m.checkNotNullParameter(item, "item");
                return new Insert(i, item);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Insert)) {
                    return false;
                }
                Insert insert = (Insert) obj;
                return this.index == insert.index && m.areEqual(getItem(), insert.getItem());
            }

            public final int getIndex() {
                return this.index;
            }

            @Override // com.discord.models.domain.ModelGuildMemberListUpdate.Operation
            public Item getItem() {
                return this.item;
            }

            public int hashCode() {
                int i = this.index * 31;
                Item item = getItem();
                return i + (item != null ? item.hashCode() : 0);
            }

            public String toString() {
                StringBuilder R = a.R("Insert(index=");
                R.append(this.index);
                R.append(", item=");
                R.append(getItem());
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: ModelGuildMemberListUpdate.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Invalidate;", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation;", "Lkotlin/ranges/IntRange;", "component1", "()Lkotlin/ranges/IntRange;", "range", "copy", "(Lkotlin/ranges/IntRange;)Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Invalidate;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lkotlin/ranges/IntRange;", "getRange", HookHelper.constructorName, "(Lkotlin/ranges/IntRange;)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Invalidate extends Operation {
            private final IntRange range;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Invalidate(IntRange intRange) {
                super(null, null, 3, null);
                m.checkNotNullParameter(intRange, "range");
                this.range = intRange;
            }

            public static /* synthetic */ Invalidate copy$default(Invalidate invalidate, IntRange intRange, int i, Object obj) {
                if ((i & 1) != 0) {
                    intRange = invalidate.range;
                }
                return invalidate.copy(intRange);
            }

            public final IntRange component1() {
                return this.range;
            }

            public final Invalidate copy(IntRange intRange) {
                m.checkNotNullParameter(intRange, "range");
                return new Invalidate(intRange);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Invalidate) && m.areEqual(this.range, ((Invalidate) obj).range);
                }
                return true;
            }

            public final IntRange getRange() {
                return this.range;
            }

            public int hashCode() {
                IntRange intRange = this.range;
                if (intRange != null) {
                    return intRange.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Invalidate(range=");
                R.append(this.range);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: ModelGuildMemberListUpdate.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0007\b¨\u0006\t"}, d2 = {"Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;", "", HookHelper.constructorName, "()V", "GroupItem", "MemberItem", "Parser", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item$GroupItem;", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item$MemberItem;", "app_models_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static abstract class Item {

            /* compiled from: ModelGuildMemberListUpdate.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item$GroupItem;", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;", "component1", "()Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;", "group", "copy", "(Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;)Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item$GroupItem;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;", "getGroup", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes.dex */
            public static final class GroupItem extends Item {
                private final Group group;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public GroupItem(Group group) {
                    super(null);
                    m.checkNotNullParameter(group, "group");
                    this.group = group;
                }

                public static /* synthetic */ GroupItem copy$default(GroupItem groupItem, Group group, int i, Object obj) {
                    if ((i & 1) != 0) {
                        group = groupItem.group;
                    }
                    return groupItem.copy(group);
                }

                public final Group component1() {
                    return this.group;
                }

                public final GroupItem copy(Group group) {
                    m.checkNotNullParameter(group, "group");
                    return new GroupItem(group);
                }

                public boolean equals(Object obj) {
                    if (this != obj) {
                        return (obj instanceof GroupItem) && m.areEqual(this.group, ((GroupItem) obj).group);
                    }
                    return true;
                }

                public final Group getGroup() {
                    return this.group;
                }

                public int hashCode() {
                    Group group = this.group;
                    if (group != null) {
                        return group.hashCode();
                    }
                    return 0;
                }

                public String toString() {
                    StringBuilder R = a.R("GroupItem(group=");
                    R.append(this.group);
                    R.append(")");
                    return R.toString();
                }
            }

            /* compiled from: ModelGuildMemberListUpdate.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item$MemberItem;", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;", "Lcom/discord/api/guildmember/GuildMember;", "component1", "()Lcom/discord/api/guildmember/GuildMember;", "member", "copy", "(Lcom/discord/api/guildmember/GuildMember;)Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item$MemberItem;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/guildmember/GuildMember;", "getMember", HookHelper.constructorName, "(Lcom/discord/api/guildmember/GuildMember;)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes.dex */
            public static final class MemberItem extends Item {
                private final GuildMember member;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public MemberItem(GuildMember guildMember) {
                    super(null);
                    m.checkNotNullParameter(guildMember, "member");
                    this.member = guildMember;
                }

                public static /* synthetic */ MemberItem copy$default(MemberItem memberItem, GuildMember guildMember, int i, Object obj) {
                    if ((i & 1) != 0) {
                        guildMember = memberItem.member;
                    }
                    return memberItem.copy(guildMember);
                }

                public final GuildMember component1() {
                    return this.member;
                }

                public final MemberItem copy(GuildMember guildMember) {
                    m.checkNotNullParameter(guildMember, "member");
                    return new MemberItem(guildMember);
                }

                public boolean equals(Object obj) {
                    if (this != obj) {
                        return (obj instanceof MemberItem) && m.areEqual(this.member, ((MemberItem) obj).member);
                    }
                    return true;
                }

                public final GuildMember getMember() {
                    return this.member;
                }

                public int hashCode() {
                    GuildMember guildMember = this.member;
                    if (guildMember != null) {
                        return guildMember.hashCode();
                    }
                    return 0;
                }

                public String toString() {
                    StringBuilder R = a.R("MemberItem(member=");
                    R.append(this.member);
                    R.append(")");
                    return R.toString();
                }
            }

            /* compiled from: ModelGuildMemberListUpdate.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item$Parser;", "Lcom/discord/models/domain/Model$Parser;", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;", "Lcom/discord/models/domain/Model$JsonReader;", "reader", "parse", "(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes.dex */
            public static final class Parser implements Model.Parser<Item> {
                public static final Parser INSTANCE = new Parser();

                private Parser() {
                }

                /* JADX WARN: Can't rename method to resolve collision */
                @Override // com.discord.models.domain.Model.Parser
                public Item parse(final Model.JsonReader jsonReader) {
                    final Ref$ObjectRef a02 = a.a0(jsonReader, "reader");
                    a02.element = null;
                    final Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
                    ref$ObjectRef.element = null;
                    jsonReader.nextObject(new Action1<String>() { // from class: com.discord.models.domain.ModelGuildMemberListUpdate$Operation$Item$Parser$parse$1
                        public final void call(String str) {
                            if (str != null) {
                                int hashCode = str.hashCode();
                                if (hashCode != -1077769574) {
                                    if (hashCode == 98629247 && str.equals("group")) {
                                        Ref$ObjectRef.this.element = (T) ModelGuildMemberListUpdate.Group.Parser.INSTANCE.parse(jsonReader);
                                        return;
                                    }
                                } else if (str.equals("member")) {
                                    ref$ObjectRef.element = (T) ((GuildMember) InboundGatewayGsonParser.fromJson(jsonReader, GuildMember.class));
                                    return;
                                }
                            }
                            jsonReader.skipValue();
                        }
                    });
                    T t = ref$ObjectRef.element;
                    if (((GuildMember) t) != null) {
                        GuildMember guildMember = (GuildMember) t;
                        m.checkNotNull(guildMember);
                        return new MemberItem(guildMember);
                    }
                    T t2 = a02.element;
                    if (((Group) t2) != null) {
                        Group group = (Group) t2;
                        m.checkNotNull(group);
                        return new GroupItem(group);
                    }
                    throw new IllegalArgumentException("either member or group must be present.");
                }
            }

            private Item() {
            }

            public /* synthetic */ Item(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* compiled from: ModelGuildMemberListUpdate.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Parser;", "Lcom/discord/models/domain/Model$Parser;", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation;", "Lcom/discord/models/domain/Model$JsonReader;", "reader", "parse", "(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation;", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Parser implements Model.Parser<Operation> {
            public static final Parser INSTANCE = new Parser();

            private Parser() {
            }

            /* JADX WARN: Can't rename method to resolve collision */
            @Override // com.discord.models.domain.Model.Parser
            public Operation parse(final Model.JsonReader jsonReader) {
                final Ref$ObjectRef a02 = a.a0(jsonReader, "reader");
                a02.element = null;
                final Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
                ref$ObjectRef.element = null;
                final Ref$ObjectRef ref$ObjectRef2 = new Ref$ObjectRef();
                ref$ObjectRef2.element = null;
                final Ref$ObjectRef ref$ObjectRef3 = new Ref$ObjectRef();
                ref$ObjectRef3.element = null;
                final Ref$ObjectRef ref$ObjectRef4 = new Ref$ObjectRef();
                ref$ObjectRef4.element = null;
                jsonReader.nextObject(new Action1<String>() { // from class: com.discord.models.domain.ModelGuildMemberListUpdate$Operation$Parser$parse$1
                    public final void call(String str) {
                        if (str != null) {
                            switch (str.hashCode()) {
                                case 3553:
                                    if (str.equals("op")) {
                                        Ref$ObjectRef.this.element = (T) jsonReader.nextStringOrNull();
                                        return;
                                    }
                                    break;
                                case 3242771:
                                    if (str.equals("item")) {
                                        ref$ObjectRef2.element = (T) ModelGuildMemberListUpdate.Operation.Item.Parser.INSTANCE.parse(jsonReader);
                                        return;
                                    }
                                    break;
                                case 100346066:
                                    if (str.equals("index")) {
                                        ref$ObjectRef.element = (T) jsonReader.nextIntOrNull();
                                        return;
                                    }
                                    break;
                                case 100526016:
                                    if (str.equals("items")) {
                                        ref$ObjectRef4.element = (T) jsonReader.nextList(new Model.JsonReader.ItemFactory<ModelGuildMemberListUpdate.Operation.Item>() { // from class: com.discord.models.domain.ModelGuildMemberListUpdate$Operation$Parser$parse$1.3
                                            /* JADX WARN: Can't rename method to resolve collision */
                                            @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                                            public final ModelGuildMemberListUpdate.Operation.Item get() {
                                                return ModelGuildMemberListUpdate.Operation.Item.Parser.INSTANCE.parse(jsonReader);
                                            }
                                        });
                                        return;
                                    }
                                    break;
                                case 108280125:
                                    if (str.equals("range")) {
                                        Ref$ObjectRef ref$ObjectRef5 = ref$ObjectRef3;
                                        List<T> nextList = jsonReader.nextList(new Model.JsonReader.ItemFactory<Integer>() { // from class: com.discord.models.domain.ModelGuildMemberListUpdate$Operation$Parser$parse$1.1
                                            /* JADX WARN: Can't rename method to resolve collision */
                                            @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                                            public final Integer get() {
                                                return jsonReader.nextIntOrNull();
                                            }
                                        });
                                        T t = nextList.get(0);
                                        m.checkNotNullExpressionValue(t, "it[0]");
                                        int intValue = ((Number) t).intValue();
                                        T t2 = nextList.get(1);
                                        m.checkNotNullExpressionValue(t2, "it[1]");
                                        ref$ObjectRef5.element = (T) new IntRange(intValue, ((Number) t2).intValue());
                                        return;
                                    }
                                    break;
                            }
                        }
                        jsonReader.skipValue();
                    }
                });
                String str = (String) a02.element;
                if (str != null) {
                    switch (str.hashCode()) {
                        case -2130463047:
                            if (str.equals("INSERT")) {
                                Integer num = (Integer) ref$ObjectRef.element;
                                m.checkNotNull(num);
                                int intValue = num.intValue();
                                Item item = (Item) ref$ObjectRef2.element;
                                m.checkNotNull(item);
                                return new Insert(intValue, item);
                            }
                            break;
                        case -1785516855:
                            if (str.equals("UPDATE")) {
                                Integer num2 = (Integer) ref$ObjectRef.element;
                                m.checkNotNull(num2);
                                int intValue2 = num2.intValue();
                                Item item2 = (Item) ref$ObjectRef2.element;
                                m.checkNotNull(item2);
                                return new Update(intValue2, item2);
                            }
                            break;
                        case -1346757317:
                            if (str.equals("INVALIDATE")) {
                                IntRange intRange = (IntRange) ref$ObjectRef3.element;
                                m.checkNotNull(intRange);
                                return new Invalidate(intRange);
                            }
                            break;
                        case 2560667:
                            if (str.equals("SYNC")) {
                                IntRange intRange2 = (IntRange) ref$ObjectRef3.element;
                                m.checkNotNull(intRange2);
                                List list = (List) ref$ObjectRef4.element;
                                m.checkNotNull(list);
                                return new Sync(intRange2, list);
                            }
                            break;
                        case 2012838315:
                            if (str.equals("DELETE")) {
                                Integer num3 = (Integer) ref$ObjectRef.element;
                                m.checkNotNull(num3);
                                return new Delete(num3.intValue());
                            }
                            break;
                    }
                }
                StringBuilder R = a.R("Invalid operation type: ");
                R.append((String) a02.element);
                throw new IllegalArgumentException(R.toString());
            }
        }

        /* compiled from: ModelGuildMemberListUpdate.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ*\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\t\u001a\u00020\u00022\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\"\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\n\u0010\u0018\u001a\u0004\b\u0019\u0010\bR\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u001a\u001a\u0004\b\u001b\u0010\u0004¨\u0006\u001e"}, d2 = {"Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Sync;", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation;", "Lkotlin/ranges/IntRange;", "component1", "()Lkotlin/ranges/IntRange;", "", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;", "component2", "()Ljava/util/List;", "range", "items", "copy", "(Lkotlin/ranges/IntRange;Ljava/util/List;)Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Sync;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getItems", "Lkotlin/ranges/IntRange;", "getRange", HookHelper.constructorName, "(Lkotlin/ranges/IntRange;Ljava/util/List;)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Sync extends Operation {
            private final List<Item> items;
            private final IntRange range;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Sync(IntRange intRange, List<? extends Item> list) {
                super(null, list, 1, null);
                m.checkNotNullParameter(intRange, "range");
                m.checkNotNullParameter(list, "items");
                this.range = intRange;
                this.items = list;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Sync copy$default(Sync sync, IntRange intRange, List list, int i, Object obj) {
                if ((i & 1) != 0) {
                    intRange = sync.range;
                }
                if ((i & 2) != 0) {
                    list = sync.getItems();
                }
                return sync.copy(intRange, list);
            }

            public final IntRange component1() {
                return this.range;
            }

            public final List<Item> component2() {
                return getItems();
            }

            public final Sync copy(IntRange intRange, List<? extends Item> list) {
                m.checkNotNullParameter(intRange, "range");
                m.checkNotNullParameter(list, "items");
                return new Sync(intRange, list);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Sync)) {
                    return false;
                }
                Sync sync = (Sync) obj;
                return m.areEqual(this.range, sync.range) && m.areEqual(getItems(), sync.getItems());
            }

            @Override // com.discord.models.domain.ModelGuildMemberListUpdate.Operation
            public List<Item> getItems() {
                return this.items;
            }

            public final IntRange getRange() {
                return this.range;
            }

            public int hashCode() {
                IntRange intRange = this.range;
                int i = 0;
                int hashCode = (intRange != null ? intRange.hashCode() : 0) * 31;
                List<Item> items = getItems();
                if (items != null) {
                    i = items.hashCode();
                }
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("Sync(range=");
                R.append(this.range);
                R.append(", items=");
                R.append(getItems());
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: ModelGuildMemberListUpdate.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0004J\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u001c\u0010\t\u001a\u00020\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\t\u0010\u0015\u001a\u0004\b\u0016\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0017\u001a\u0004\b\u0018\u0010\u0004¨\u0006\u001b"}, d2 = {"Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Update;", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation;", "", "component1", "()I", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;", "component2", "()Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;", "index", "item", "copy", "(ILcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;)Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Update;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;", "getItem", "I", "getIndex", HookHelper.constructorName, "(ILcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Update extends Operation {
            private final int index;
            private final Item item;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Update(int i, Item item) {
                super(item, null, 2, null);
                m.checkNotNullParameter(item, "item");
                this.index = i;
                this.item = item;
            }

            public static /* synthetic */ Update copy$default(Update update, int i, Item item, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = update.index;
                }
                if ((i2 & 2) != 0) {
                    item = update.getItem();
                }
                return update.copy(i, item);
            }

            public final int component1() {
                return this.index;
            }

            public final Item component2() {
                return getItem();
            }

            public final Update copy(int i, Item item) {
                m.checkNotNullParameter(item, "item");
                return new Update(i, item);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Update)) {
                    return false;
                }
                Update update = (Update) obj;
                return this.index == update.index && m.areEqual(getItem(), update.getItem());
            }

            public final int getIndex() {
                return this.index;
            }

            @Override // com.discord.models.domain.ModelGuildMemberListUpdate.Operation
            public Item getItem() {
                return this.item;
            }

            public int hashCode() {
                int i = this.index * 31;
                Item item = getItem();
                return i + (item != null ? item.hashCode() : 0);
            }

            public String toString() {
                StringBuilder R = a.R("Update(index=");
                R.append(this.index);
                R.append(", item=");
                R.append(getItem());
                R.append(")");
                return R.toString();
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        private Operation(Item item, List<? extends Item> list) {
            this.item = item;
            this.items = list;
        }

        public Item getItem() {
            return this.item;
        }

        public List<Item> getItems() {
            return this.items;
        }

        public /* synthetic */ Operation(Item item, List list, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : item, (i & 2) != 0 ? null : list);
        }
    }

    /* compiled from: ModelGuildMemberListUpdate.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/models/domain/ModelGuildMemberListUpdate$Parser;", "Lcom/discord/models/domain/Model$Parser;", "Lcom/discord/models/domain/ModelGuildMemberListUpdate;", "Lcom/discord/models/domain/Model$JsonReader;", "reader", "parse", "(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelGuildMemberListUpdate;", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Parser implements Model.Parser<ModelGuildMemberListUpdate> {
        public static final Parser INSTANCE = new Parser();

        private Parser() {
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // com.discord.models.domain.Model.Parser
        public ModelGuildMemberListUpdate parse(final Model.JsonReader jsonReader) {
            final Ref$ObjectRef a02 = a.a0(jsonReader, "reader");
            a02.element = null;
            final Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
            ref$ObjectRef.element = null;
            final Ref$ObjectRef ref$ObjectRef2 = new Ref$ObjectRef();
            ref$ObjectRef2.element = null;
            final Ref$ObjectRef ref$ObjectRef3 = new Ref$ObjectRef();
            ref$ObjectRef3.element = null;
            jsonReader.nextObject(new Action1<String>() { // from class: com.discord.models.domain.ModelGuildMemberListUpdate$Parser$parse$1
                public final void call(String str) {
                    if (str != null) {
                        int hashCode = str.hashCode();
                        if (hashCode != -1306538777) {
                            if (hashCode != -1237460524) {
                                if (hashCode != 3355) {
                                    if (hashCode == 110258 && str.equals("ops")) {
                                        ref$ObjectRef2.element = (T) jsonReader.nextList(new Model.JsonReader.ItemFactory<ModelGuildMemberListUpdate.Operation>() { // from class: com.discord.models.domain.ModelGuildMemberListUpdate$Parser$parse$1.1
                                            /* JADX WARN: Can't rename method to resolve collision */
                                            @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                                            public final ModelGuildMemberListUpdate.Operation get() {
                                                return ModelGuildMemberListUpdate.Operation.Parser.INSTANCE.parse(jsonReader);
                                            }
                                        });
                                        return;
                                    }
                                } else if (str.equals(ModelAuditLogEntry.CHANGE_KEY_ID)) {
                                    ref$ObjectRef.element = (T) jsonReader.nextStringOrNull();
                                    return;
                                }
                            } else if (str.equals("groups")) {
                                ref$ObjectRef3.element = (T) jsonReader.nextList(new Model.JsonReader.ItemFactory<ModelGuildMemberListUpdate.Group>() { // from class: com.discord.models.domain.ModelGuildMemberListUpdate$Parser$parse$1.2
                                    /* JADX WARN: Can't rename method to resolve collision */
                                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                                    public final ModelGuildMemberListUpdate.Group get() {
                                        return ModelGuildMemberListUpdate.Group.Parser.INSTANCE.parse(jsonReader);
                                    }
                                });
                                return;
                            }
                        } else if (str.equals(ModelAuditLogEntry.CHANGE_KEY_GUILD_ID)) {
                            Ref$ObjectRef.this.element = (T) jsonReader.nextLongOrNull();
                            return;
                        }
                    }
                    jsonReader.skipValue();
                }
            });
            Long l = (Long) a02.element;
            m.checkNotNull(l);
            long longValue = l.longValue();
            String str = (String) ref$ObjectRef.element;
            if (str == null) {
                str = ModelGuildMemberListUpdate.EVERYONE_ID;
            }
            String str2 = str;
            List list = (List) ref$ObjectRef2.element;
            m.checkNotNull(list);
            List list2 = (List) ref$ObjectRef3.element;
            m.checkNotNull(list2);
            return new ModelGuildMemberListUpdate(longValue, str2, list, list2);
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public ModelGuildMemberListUpdate(long j, String str, List<? extends Operation> list, List<Group> list2) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_ID);
        m.checkNotNullParameter(list, "operations");
        m.checkNotNullParameter(list2, "groups");
        this.guildId = j;
        this.f2696id = str;
        this.operations = list;
        this.groups = list2;
    }

    public static /* synthetic */ ModelGuildMemberListUpdate copy$default(ModelGuildMemberListUpdate modelGuildMemberListUpdate, long j, String str, List list, List list2, int i, Object obj) {
        if ((i & 1) != 0) {
            j = modelGuildMemberListUpdate.guildId;
        }
        long j2 = j;
        if ((i & 2) != 0) {
            str = modelGuildMemberListUpdate.f2696id;
        }
        String str2 = str;
        List<Operation> list3 = list;
        if ((i & 4) != 0) {
            list3 = modelGuildMemberListUpdate.operations;
        }
        List list4 = list3;
        List<Group> list5 = list2;
        if ((i & 8) != 0) {
            list5 = modelGuildMemberListUpdate.groups;
        }
        return modelGuildMemberListUpdate.copy(j2, str2, list4, list5);
    }

    public final long component1() {
        return this.guildId;
    }

    public final String component2() {
        return this.f2696id;
    }

    public final List<Operation> component3() {
        return this.operations;
    }

    public final List<Group> component4() {
        return this.groups;
    }

    public final ModelGuildMemberListUpdate copy(long j, String str, List<? extends Operation> list, List<Group> list2) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_ID);
        m.checkNotNullParameter(list, "operations");
        m.checkNotNullParameter(list2, "groups");
        return new ModelGuildMemberListUpdate(j, str, list, list2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModelGuildMemberListUpdate)) {
            return false;
        }
        ModelGuildMemberListUpdate modelGuildMemberListUpdate = (ModelGuildMemberListUpdate) obj;
        return this.guildId == modelGuildMemberListUpdate.guildId && m.areEqual(this.f2696id, modelGuildMemberListUpdate.f2696id) && m.areEqual(this.operations, modelGuildMemberListUpdate.operations) && m.areEqual(this.groups, modelGuildMemberListUpdate.groups);
    }

    public final List<Group> getGroups() {
        return this.groups;
    }

    public final long getGuildId() {
        return this.guildId;
    }

    public final String getId() {
        return this.f2696id;
    }

    public final List<Operation> getOperations() {
        return this.operations;
    }

    public int hashCode() {
        long j = this.guildId;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        String str = this.f2696id;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        List<Operation> list = this.operations;
        int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
        List<Group> list2 = this.groups;
        if (list2 != null) {
            i2 = list2.hashCode();
        }
        return hashCode2 + i2;
    }

    public String toString() {
        StringBuilder R = a.R("ModelGuildMemberListUpdate(guildId=");
        R.append(this.guildId);
        R.append(", id=");
        R.append(this.f2696id);
        R.append(", operations=");
        R.append(this.operations);
        R.append(", groups=");
        return a.K(R, this.groups, ")");
    }
}
