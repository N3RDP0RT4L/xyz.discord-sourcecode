package com.discord.models.domain;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.models.domain.Model;
import com.google.gson.stream.JsonToken;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Ref$ObjectRef;
import rx.functions.Action1;
/* compiled from: ModelCustomStatusSetting.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\r\b\u0086\b\u0018\u0000 !2\u00020\u0001:\u0002!\"B5\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0002\u0012\u000e\u0010\u000e\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\t¢\u0006\u0004\b\u001f\u0010 J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\b\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\b\u0010\u0004J\u0018\u0010\n\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u0004JF\u0010\u000f\u001a\u00020\u00002\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00022\u0010\b\u0002\u0010\u000e\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\tHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0004J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0017\u001a\u00020\u00162\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u001b\u0010\u000b\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004R\u001b\u0010\r\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u0019\u001a\u0004\b\u001b\u0010\u0004R!\u0010\u000e\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u0019\u001a\u0004\b\u001c\u0010\u0004R\u001b\u0010\f\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001d\u001a\u0004\b\u001e\u0010\u0007¨\u0006#"}, d2 = {"Lcom/discord/models/domain/ModelCustomStatusSetting;", "", "", "component1", "()Ljava/lang/String;", "", "component2", "()Ljava/lang/Long;", "component3", "Lcom/discord/primitives/UtcTimestamp;", "component4", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "emojiId", "emojiName", "expiresAt", "copy", "(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/models/domain/ModelCustomStatusSetting;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getText", "getEmojiName", "getExpiresAt", "Ljava/lang/Long;", "getEmojiId", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V", "Companion", "Parser", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelCustomStatusSetting {
    private final Long emojiId;
    private final String emojiName;
    private final String expiresAt;
    private final String text;
    public static final Companion Companion = new Companion(null);
    private static final ModelCustomStatusSetting CLEAR = new ModelCustomStatusSetting(null, null, null, null);

    /* compiled from: ModelCustomStatusSetting.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/models/domain/ModelCustomStatusSetting$Companion;", "", "Lcom/discord/models/domain/ModelCustomStatusSetting;", "CLEAR", "Lcom/discord/models/domain/ModelCustomStatusSetting;", "getCLEAR", "()Lcom/discord/models/domain/ModelCustomStatusSetting;", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public final ModelCustomStatusSetting getCLEAR() {
            return ModelCustomStatusSetting.CLEAR;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: ModelCustomStatusSetting.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/models/domain/ModelCustomStatusSetting$Parser;", "Lcom/discord/models/domain/Model$Parser;", "Lcom/discord/models/domain/ModelCustomStatusSetting;", "Lcom/discord/models/domain/Model$JsonReader;", "reader", "parse", "(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelCustomStatusSetting;", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Parser implements Model.Parser<ModelCustomStatusSetting> {
        public static final Parser INSTANCE = new Parser();

        private Parser() {
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // com.discord.models.domain.Model.Parser
        public ModelCustomStatusSetting parse(final Model.JsonReader jsonReader) {
            final Ref$ObjectRef a02 = a.a0(jsonReader, "reader");
            a02.element = null;
            final Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
            ref$ObjectRef.element = null;
            final Ref$ObjectRef ref$ObjectRef2 = new Ref$ObjectRef();
            ref$ObjectRef2.element = null;
            final Ref$ObjectRef ref$ObjectRef3 = new Ref$ObjectRef();
            ref$ObjectRef3.element = null;
            if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.nextNull();
                return ModelCustomStatusSetting.Companion.getCLEAR();
            }
            jsonReader.nextObject(new Action1<String>() { // from class: com.discord.models.domain.ModelCustomStatusSetting$Parser$parse$1
                public final void call(String str) {
                    if (str != null) {
                        switch (str.hashCode()) {
                            case -833811170:
                                if (str.equals("expires_at")) {
                                    ref$ObjectRef3.element = (T) jsonReader.nextStringOrNull();
                                    return;
                                }
                                break;
                            case 3556653:
                                if (str.equals(NotificationCompat.MessagingStyle.Message.KEY_TEXT)) {
                                    Ref$ObjectRef.this.element = (T) jsonReader.nextStringOrNull();
                                    return;
                                }
                                break;
                            case 749661924:
                                if (str.equals("emoji_name")) {
                                    ref$ObjectRef2.element = (T) jsonReader.nextStringOrNull();
                                    return;
                                }
                                break;
                            case 1162789812:
                                if (str.equals("emoji_id")) {
                                    ref$ObjectRef.element = (T) jsonReader.nextLongOrNull();
                                    return;
                                }
                                break;
                        }
                    }
                    jsonReader.skipValue();
                }
            });
            return new ModelCustomStatusSetting((String) a02.element, (Long) ref$ObjectRef.element, (String) ref$ObjectRef2.element, (String) ref$ObjectRef3.element);
        }
    }

    public ModelCustomStatusSetting(String str, Long l, String str2, String str3) {
        this.text = str;
        this.emojiId = l;
        this.emojiName = str2;
        this.expiresAt = str3;
    }

    public static /* synthetic */ ModelCustomStatusSetting copy$default(ModelCustomStatusSetting modelCustomStatusSetting, String str, Long l, String str2, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = modelCustomStatusSetting.text;
        }
        if ((i & 2) != 0) {
            l = modelCustomStatusSetting.emojiId;
        }
        if ((i & 4) != 0) {
            str2 = modelCustomStatusSetting.emojiName;
        }
        if ((i & 8) != 0) {
            str3 = modelCustomStatusSetting.expiresAt;
        }
        return modelCustomStatusSetting.copy(str, l, str2, str3);
    }

    public final String component1() {
        return this.text;
    }

    public final Long component2() {
        return this.emojiId;
    }

    public final String component3() {
        return this.emojiName;
    }

    public final String component4() {
        return this.expiresAt;
    }

    public final ModelCustomStatusSetting copy(String str, Long l, String str2, String str3) {
        return new ModelCustomStatusSetting(str, l, str2, str3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModelCustomStatusSetting)) {
            return false;
        }
        ModelCustomStatusSetting modelCustomStatusSetting = (ModelCustomStatusSetting) obj;
        return m.areEqual(this.text, modelCustomStatusSetting.text) && m.areEqual(this.emojiId, modelCustomStatusSetting.emojiId) && m.areEqual(this.emojiName, modelCustomStatusSetting.emojiName) && m.areEqual(this.expiresAt, modelCustomStatusSetting.expiresAt);
    }

    public final Long getEmojiId() {
        return this.emojiId;
    }

    public final String getEmojiName() {
        return this.emojiName;
    }

    public final String getExpiresAt() {
        return this.expiresAt;
    }

    public final String getText() {
        return this.text;
    }

    public int hashCode() {
        String str = this.text;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Long l = this.emojiId;
        int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
        String str2 = this.emojiName;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.expiresAt;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode3 + i;
    }

    public String toString() {
        StringBuilder R = a.R("ModelCustomStatusSetting(text=");
        R.append(this.text);
        R.append(", emojiId=");
        R.append(this.emojiId);
        R.append(", emojiName=");
        R.append(this.emojiName);
        R.append(", expiresAt=");
        return a.H(R, this.expiresAt, ")");
    }
}
