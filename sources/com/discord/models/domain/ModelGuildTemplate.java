package com.discord.models.domain;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.guild.Guild;
import com.discord.api.user.User;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ModelGuildTemplate.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0014\n\u0002\u0010\u000b\n\u0002\b\u0014\b\u0086\b\u0018\u00002\u00020\u0001Bk\u0012\n\u0010\u0019\u001a\u00060\u0002j\u0002`\u0003\u0012\b\u0010\u001a\u001a\u0004\u0018\u00010\u0006\u0012\n\u0010\u001b\u001a\u00060\u0002j\u0002`\t\u0012\b\u0010\u001c\u001a\u0004\u0018\u00010\u000b\u0012\n\u0010\u001d\u001a\u00060\u000ej\u0002`\u000f\u0012\n\u0010\u001e\u001a\u00060\u000ej\u0002`\u000f\u0012\u0006\u0010\u001f\u001a\u00020\u0013\u0012\u0006\u0010 \u001a\u00020\u000e\u0012\u0006\u0010!\u001a\u00020\u000e\u0012\u0006\u0010\"\u001a\u00020\u000e¢\u0006\u0004\b:\u0010;J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0014\u0010\n\u001a\u00060\u0002j\u0002`\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u0005J\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0014\u0010\u0010\u001a\u00060\u000ej\u0002`\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0014\u0010\u0012\u001a\u00060\u000ej\u0002`\u000fHÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0011J\u0010\u0010\u0014\u001a\u00020\u0013HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0011J\u0010\u0010\u0017\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0011J\u0010\u0010\u0018\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0011J\u0088\u0001\u0010#\u001a\u00020\u00002\f\b\u0002\u0010\u0019\u001a\u00060\u0002j\u0002`\u00032\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u00062\f\b\u0002\u0010\u001b\u001a\u00060\u0002j\u0002`\t2\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u000b2\f\b\u0002\u0010\u001d\u001a\u00060\u000ej\u0002`\u000f2\f\b\u0002\u0010\u001e\u001a\u00060\u000ej\u0002`\u000f2\b\b\u0002\u0010\u001f\u001a\u00020\u00132\b\b\u0002\u0010 \u001a\u00020\u000e2\b\b\u0002\u0010!\u001a\u00020\u000e2\b\b\u0002\u0010\"\u001a\u00020\u000eHÆ\u0001¢\u0006\u0004\b#\u0010$J\u0010\u0010%\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b%\u0010\u0011J\u0010\u0010&\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b&\u0010\u0015J\u001a\u0010)\u001a\u00020(2\b\u0010'\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b)\u0010*R\u001d\u0010\u0019\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010+\u001a\u0004\b,\u0010\u0005R\u001d\u0010\u001d\u001a\u00060\u000ej\u0002`\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010-\u001a\u0004\b.\u0010\u0011R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010/\u001a\u0004\b0\u0010\bR\u0019\u0010\"\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010-\u001a\u0004\b1\u0010\u0011R\u0019\u0010\u001f\u001a\u00020\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00102\u001a\u0004\b3\u0010\u0015R\u001d\u0010\u001e\u001a\u00060\u000ej\u0002`\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010-\u001a\u0004\b4\u0010\u0011R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u00105\u001a\u0004\b6\u0010\rR\u0019\u0010 \u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010-\u001a\u0004\b7\u0010\u0011R\u0019\u0010!\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010-\u001a\u0004\b8\u0010\u0011R\u001d\u0010\u001b\u001a\u00060\u0002j\u0002`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010+\u001a\u0004\b9\u0010\u0005¨\u0006<"}, d2 = {"Lcom/discord/models/domain/ModelGuildTemplate;", "", "", "Lcom/discord/primitives/UserId;", "component1", "()J", "Lcom/discord/api/user/User;", "component2", "()Lcom/discord/api/user/User;", "Lcom/discord/primitives/GuildId;", "component3", "Lcom/discord/api/guild/Guild;", "component4", "()Lcom/discord/api/guild/Guild;", "", "Lcom/discord/primitives/UtcTimestamp;", "component5", "()Ljava/lang/String;", "component6", "", "component7", "()I", "component8", "component9", "component10", "creatorId", "creator", "sourceGuildId", "serializedSourceGuild", "createdAt", "updatedAt", "usageCount", ModelAuditLogEntry.CHANGE_KEY_CODE, ModelAuditLogEntry.CHANGE_KEY_NAME, ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "copy", "(JLcom/discord/api/user/User;JLcom/discord/api/guild/Guild;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/models/domain/ModelGuildTemplate;", "toString", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getCreatorId", "Ljava/lang/String;", "getCreatedAt", "Lcom/discord/api/user/User;", "getCreator", "getDescription", "I", "getUsageCount", "getUpdatedAt", "Lcom/discord/api/guild/Guild;", "getSerializedSourceGuild", "getCode", "getName", "getSourceGuildId", HookHelper.constructorName, "(JLcom/discord/api/user/User;JLcom/discord/api/guild/Guild;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelGuildTemplate {
    private final String code;
    private final String createdAt;
    private final User creator;
    private final long creatorId;
    private final String description;
    private final String name;
    private final Guild serializedSourceGuild;
    private final long sourceGuildId;
    private final String updatedAt;
    private final int usageCount;

    public ModelGuildTemplate(long j, User user, long j2, Guild guild, String str, String str2, int i, String str3, String str4, String str5) {
        m.checkNotNullParameter(str, "createdAt");
        m.checkNotNullParameter(str2, "updatedAt");
        m.checkNotNullParameter(str3, ModelAuditLogEntry.CHANGE_KEY_CODE);
        m.checkNotNullParameter(str4, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(str5, ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION);
        this.creatorId = j;
        this.creator = user;
        this.sourceGuildId = j2;
        this.serializedSourceGuild = guild;
        this.createdAt = str;
        this.updatedAt = str2;
        this.usageCount = i;
        this.code = str3;
        this.name = str4;
        this.description = str5;
    }

    public final long component1() {
        return this.creatorId;
    }

    public final String component10() {
        return this.description;
    }

    public final User component2() {
        return this.creator;
    }

    public final long component3() {
        return this.sourceGuildId;
    }

    public final Guild component4() {
        return this.serializedSourceGuild;
    }

    public final String component5() {
        return this.createdAt;
    }

    public final String component6() {
        return this.updatedAt;
    }

    public final int component7() {
        return this.usageCount;
    }

    public final String component8() {
        return this.code;
    }

    public final String component9() {
        return this.name;
    }

    public final ModelGuildTemplate copy(long j, User user, long j2, Guild guild, String str, String str2, int i, String str3, String str4, String str5) {
        m.checkNotNullParameter(str, "createdAt");
        m.checkNotNullParameter(str2, "updatedAt");
        m.checkNotNullParameter(str3, ModelAuditLogEntry.CHANGE_KEY_CODE);
        m.checkNotNullParameter(str4, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(str5, ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION);
        return new ModelGuildTemplate(j, user, j2, guild, str, str2, i, str3, str4, str5);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModelGuildTemplate)) {
            return false;
        }
        ModelGuildTemplate modelGuildTemplate = (ModelGuildTemplate) obj;
        return this.creatorId == modelGuildTemplate.creatorId && m.areEqual(this.creator, modelGuildTemplate.creator) && this.sourceGuildId == modelGuildTemplate.sourceGuildId && m.areEqual(this.serializedSourceGuild, modelGuildTemplate.serializedSourceGuild) && m.areEqual(this.createdAt, modelGuildTemplate.createdAt) && m.areEqual(this.updatedAt, modelGuildTemplate.updatedAt) && this.usageCount == modelGuildTemplate.usageCount && m.areEqual(this.code, modelGuildTemplate.code) && m.areEqual(this.name, modelGuildTemplate.name) && m.areEqual(this.description, modelGuildTemplate.description);
    }

    public final String getCode() {
        return this.code;
    }

    public final String getCreatedAt() {
        return this.createdAt;
    }

    public final User getCreator() {
        return this.creator;
    }

    public final long getCreatorId() {
        return this.creatorId;
    }

    public final String getDescription() {
        return this.description;
    }

    public final String getName() {
        return this.name;
    }

    public final Guild getSerializedSourceGuild() {
        return this.serializedSourceGuild;
    }

    public final long getSourceGuildId() {
        return this.sourceGuildId;
    }

    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    public final int getUsageCount() {
        return this.usageCount;
    }

    public int hashCode() {
        long j = this.creatorId;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        User user = this.creator;
        int i2 = 0;
        int hashCode = user != null ? user.hashCode() : 0;
        long j2 = this.sourceGuildId;
        int i3 = (((i + hashCode) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31;
        Guild guild = this.serializedSourceGuild;
        int hashCode2 = (i3 + (guild != null ? guild.hashCode() : 0)) * 31;
        String str = this.createdAt;
        int hashCode3 = (hashCode2 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.updatedAt;
        int hashCode4 = (((hashCode3 + (str2 != null ? str2.hashCode() : 0)) * 31) + this.usageCount) * 31;
        String str3 = this.code;
        int hashCode5 = (hashCode4 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.name;
        int hashCode6 = (hashCode5 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.description;
        if (str5 != null) {
            i2 = str5.hashCode();
        }
        return hashCode6 + i2;
    }

    public String toString() {
        StringBuilder R = a.R("ModelGuildTemplate(creatorId=");
        R.append(this.creatorId);
        R.append(", creator=");
        R.append(this.creator);
        R.append(", sourceGuildId=");
        R.append(this.sourceGuildId);
        R.append(", serializedSourceGuild=");
        R.append(this.serializedSourceGuild);
        R.append(", createdAt=");
        R.append(this.createdAt);
        R.append(", updatedAt=");
        R.append(this.updatedAt);
        R.append(", usageCount=");
        R.append(this.usageCount);
        R.append(", code=");
        R.append(this.code);
        R.append(", name=");
        R.append(this.name);
        R.append(", description=");
        return a.H(R, this.description, ")");
    }
}
