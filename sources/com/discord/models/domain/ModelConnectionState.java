package com.discord.models.domain;

import b.d.b.a.a;
import com.discord.models.domain.Model;
import java.io.IOException;
/* loaded from: classes.dex */
public class ModelConnectionState implements Model {
    private String code;
    private String providerId;
    private String state;

    @Override // com.discord.models.domain.Model
    public void assignField(Model.JsonReader jsonReader) throws IOException {
        String nextName = jsonReader.nextName();
        nextName.hashCode();
        char c = 65535;
        switch (nextName.hashCode()) {
            case 3059181:
                if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_CODE)) {
                    c = 0;
                    break;
                }
                break;
            case 109757585:
                if (nextName.equals("state")) {
                    c = 1;
                    break;
                }
                break;
            case 2064701993:
                if (nextName.equals("provider_id")) {
                    c = 2;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                this.code = jsonReader.nextString("");
                return;
            case 1:
                this.state = jsonReader.nextString("");
                return;
            case 2:
                this.providerId = jsonReader.nextString("");
                return;
            default:
                jsonReader.skipValue();
                return;
        }
    }

    public boolean canEqual(Object obj) {
        return obj instanceof ModelConnectionState;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ModelConnectionState)) {
            return false;
        }
        ModelConnectionState modelConnectionState = (ModelConnectionState) obj;
        if (!modelConnectionState.canEqual(this)) {
            return false;
        }
        String code = getCode();
        String code2 = modelConnectionState.getCode();
        if (code != null ? !code.equals(code2) : code2 != null) {
            return false;
        }
        String state = getState();
        String state2 = modelConnectionState.getState();
        if (state != null ? !state.equals(state2) : state2 != null) {
            return false;
        }
        String providerId = getProviderId();
        String providerId2 = modelConnectionState.getProviderId();
        return providerId != null ? providerId.equals(providerId2) : providerId2 == null;
    }

    public String getCode() {
        return this.code;
    }

    public String getProviderId() {
        return this.providerId;
    }

    public String getState() {
        return this.state;
    }

    public int hashCode() {
        String code = getCode();
        int i = 43;
        int hashCode = code == null ? 43 : code.hashCode();
        String state = getState();
        int hashCode2 = ((hashCode + 59) * 59) + (state == null ? 43 : state.hashCode());
        String providerId = getProviderId();
        int i2 = hashCode2 * 59;
        if (providerId != null) {
            i = providerId.hashCode();
        }
        return i2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("ModelConnectionState(code=");
        R.append(getCode());
        R.append(", state=");
        R.append(getState());
        R.append(", providerId=");
        R.append(getProviderId());
        R.append(")");
        return R.toString();
    }
}
