package com.discord.models.domain;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.utilities.time.TimeUtils;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ModelGuildBoostSlot.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001B;\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0002\u0012\n\u0010\u0013\u001a\u00060\u0005j\u0002`\b\u0012\n\u0010\u0014\u001a\u00060\u0005j\u0002`\n\u0012\b\u0010\u0015\u001a\u0004\u0018\u00010\f\u0012\u0006\u0010\u0016\u001a\u00020\u000f¢\u0006\u0004\b(\u0010)J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\r\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\u0006\u0010\u0007J\u0014\u0010\t\u001a\u00060\u0005j\u0002`\bHÆ\u0003¢\u0006\u0004\b\t\u0010\u0007J\u0014\u0010\u000b\u001a\u00060\u0005j\u0002`\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\u0007J\u0012\u0010\r\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011JN\u0010\u0017\u001a\u00020\u00002\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00022\f\b\u0002\u0010\u0013\u001a\u00060\u0005j\u0002`\b2\f\b\u0002\u0010\u0014\u001a\u00060\u0005j\u0002`\n2\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\f2\b\b\u0002\u0010\u0016\u001a\u00020\u000fHÆ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0019\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u0004J\u0010\u0010\u001b\u001a\u00020\u001aHÖ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u001a\u0010\u001e\u001a\u00020\u000f2\b\u0010\u001d\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001e\u0010\u001fR\u001d\u0010\u0013\u001a\u00060\u0005j\u0002`\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010 \u001a\u0004\b!\u0010\u0007R\u0019\u0010\u0016\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\"\u001a\u0004\b#\u0010\u0011R\u0018\u0010\u0012\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010$R\u001d\u0010\u0014\u001a\u00060\u0005j\u0002`\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010 \u001a\u0004\b%\u0010\u0007R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010&\u001a\u0004\b'\u0010\u000e¨\u0006*"}, d2 = {"Lcom/discord/models/domain/ModelGuildBoostSlot;", "", "", "component1", "()Ljava/lang/String;", "", "getCooldownExpiresAtTimestamp", "()J", "Lcom/discord/primitives/GuildBoostSlotId;", "component2", "Lcom/discord/primitives/AppliedGuildBoostId;", "component3", "Lcom/discord/models/domain/ModelAppliedGuildBoost;", "component4", "()Lcom/discord/models/domain/ModelAppliedGuildBoost;", "", "component5", "()Z", "cooldownEndsAt", ModelAuditLogEntry.CHANGE_KEY_ID, "subscriptionId", "premiumGuildSubscription", "canceled", "copy", "(Ljava/lang/String;JJLcom/discord/models/domain/ModelAppliedGuildBoost;Z)Lcom/discord/models/domain/ModelGuildBoostSlot;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "J", "getId", "Z", "getCanceled", "Ljava/lang/String;", "getSubscriptionId", "Lcom/discord/models/domain/ModelAppliedGuildBoost;", "getPremiumGuildSubscription", HookHelper.constructorName, "(Ljava/lang/String;JJLcom/discord/models/domain/ModelAppliedGuildBoost;Z)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelGuildBoostSlot {
    private final boolean canceled;
    private final String cooldownEndsAt;

    /* renamed from: id  reason: collision with root package name */
    private final long f2692id;
    private final ModelAppliedGuildBoost premiumGuildSubscription;
    private final long subscriptionId;

    public ModelGuildBoostSlot(String str, long j, long j2, ModelAppliedGuildBoost modelAppliedGuildBoost, boolean z2) {
        this.cooldownEndsAt = str;
        this.f2692id = j;
        this.subscriptionId = j2;
        this.premiumGuildSubscription = modelAppliedGuildBoost;
        this.canceled = z2;
    }

    private final String component1() {
        return this.cooldownEndsAt;
    }

    public static /* synthetic */ ModelGuildBoostSlot copy$default(ModelGuildBoostSlot modelGuildBoostSlot, String str, long j, long j2, ModelAppliedGuildBoost modelAppliedGuildBoost, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = modelGuildBoostSlot.cooldownEndsAt;
        }
        if ((i & 2) != 0) {
            j = modelGuildBoostSlot.f2692id;
        }
        long j3 = j;
        if ((i & 4) != 0) {
            j2 = modelGuildBoostSlot.subscriptionId;
        }
        long j4 = j2;
        if ((i & 8) != 0) {
            modelAppliedGuildBoost = modelGuildBoostSlot.premiumGuildSubscription;
        }
        ModelAppliedGuildBoost modelAppliedGuildBoost2 = modelAppliedGuildBoost;
        if ((i & 16) != 0) {
            z2 = modelGuildBoostSlot.canceled;
        }
        return modelGuildBoostSlot.copy(str, j3, j4, modelAppliedGuildBoost2, z2);
    }

    public final long component2() {
        return this.f2692id;
    }

    public final long component3() {
        return this.subscriptionId;
    }

    public final ModelAppliedGuildBoost component4() {
        return this.premiumGuildSubscription;
    }

    public final boolean component5() {
        return this.canceled;
    }

    public final ModelGuildBoostSlot copy(String str, long j, long j2, ModelAppliedGuildBoost modelAppliedGuildBoost, boolean z2) {
        return new ModelGuildBoostSlot(str, j, j2, modelAppliedGuildBoost, z2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModelGuildBoostSlot)) {
            return false;
        }
        ModelGuildBoostSlot modelGuildBoostSlot = (ModelGuildBoostSlot) obj;
        return m.areEqual(this.cooldownEndsAt, modelGuildBoostSlot.cooldownEndsAt) && this.f2692id == modelGuildBoostSlot.f2692id && this.subscriptionId == modelGuildBoostSlot.subscriptionId && m.areEqual(this.premiumGuildSubscription, modelGuildBoostSlot.premiumGuildSubscription) && this.canceled == modelGuildBoostSlot.canceled;
    }

    public final boolean getCanceled() {
        return this.canceled;
    }

    public final long getCooldownExpiresAtTimestamp() {
        return TimeUtils.parseUTCDate(this.cooldownEndsAt);
    }

    public final long getId() {
        return this.f2692id;
    }

    public final ModelAppliedGuildBoost getPremiumGuildSubscription() {
        return this.premiumGuildSubscription;
    }

    public final long getSubscriptionId() {
        return this.subscriptionId;
    }

    public int hashCode() {
        String str = this.cooldownEndsAt;
        int i = 0;
        int hashCode = str != null ? str.hashCode() : 0;
        long j = this.f2692id;
        long j2 = this.subscriptionId;
        int i2 = ((((hashCode * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31;
        ModelAppliedGuildBoost modelAppliedGuildBoost = this.premiumGuildSubscription;
        if (modelAppliedGuildBoost != null) {
            i = modelAppliedGuildBoost.hashCode();
        }
        int i3 = (i2 + i) * 31;
        boolean z2 = this.canceled;
        if (z2) {
            z2 = true;
        }
        int i4 = z2 ? 1 : 0;
        int i5 = z2 ? 1 : 0;
        return i3 + i4;
    }

    public String toString() {
        StringBuilder R = a.R("ModelGuildBoostSlot(cooldownEndsAt=");
        R.append(this.cooldownEndsAt);
        R.append(", id=");
        R.append(this.f2692id);
        R.append(", subscriptionId=");
        R.append(this.subscriptionId);
        R.append(", premiumGuildSubscription=");
        R.append(this.premiumGuildSubscription);
        R.append(", canceled=");
        return a.M(R, this.canceled, ")");
    }
}
