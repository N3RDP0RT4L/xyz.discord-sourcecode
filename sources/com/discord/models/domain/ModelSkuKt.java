package com.discord.models.domain;

import kotlin.Metadata;
/* compiled from: ModelSku.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\"\u0016\u0010\u0001\u001a\u00020\u00008\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0001\u0010\u0002\"\u0016\u0010\u0004\u001a\u00020\u00038\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0004\u0010\u0005\"\u0016\u0010\u0006\u001a\u00020\u00008\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0002\"\u0016\u0010\u0007\u001a\u00020\u00038\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0007\u0010\u0005*&\u0010\u000b\"\u000e\u0012\u0004\u0012\u0002`\t\u0012\u0004\u0012\u00020\n0\b2\u0012\u0012\b\u0012\u00060\u0003j\u0002`\t\u0012\u0004\u0012\u00020\n0\b¨\u0006\f"}, d2 = {"", "PREMIUM_TIER_2_SKU_ID", "J", "", "SKU_FLAG_STICKER", "I", "PREMIUM_TIER_1_SKU_ID", "SKU_FLAG_AVAILABLE", "", "Lcom/discord/primitives/PaymentGatewayId;", "Lcom/discord/models/domain/ModelSku$ExternalSkuStrategy;", "ExternalSkuStrategies", "app_models_release"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelSkuKt {
    public static final long PREMIUM_TIER_1_SKU_ID = 521846918637420545L;
    public static final long PREMIUM_TIER_2_SKU_ID = 521847234246082599L;
    public static final int SKU_FLAG_AVAILABLE = 4;
    public static final int SKU_FLAG_STICKER = 16;
}
