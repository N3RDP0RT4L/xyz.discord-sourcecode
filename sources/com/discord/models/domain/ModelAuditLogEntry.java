package com.discord.models.domain;

import b.d.b.a.a;
import com.discord.models.domain.Model;
import com.discord.models.domain.ModelAuditLogEntry;
import com.google.gson.stream.JsonToken;
import java.io.IOException;
import java.util.List;
/* loaded from: classes.dex */
public class ModelAuditLogEntry implements Model {
    public static final int ACTION_ALL = 0;
    public static final int ACTION_BOT_ADD = 28;
    public static final int ACTION_CHANNEL_CREATE = 10;
    public static final int ACTION_CHANNEL_DELETE = 12;
    public static final int ACTION_CHANNEL_OVERWRITE_CREATE = 13;
    public static final int ACTION_CHANNEL_OVERWRITE_DELETE = 15;
    public static final int ACTION_CHANNEL_OVERWRITE_UPDATE = 14;
    public static final int ACTION_CHANNEL_UPDATE = 11;
    public static final int ACTION_EMOJI_CREATE = 60;
    public static final int ACTION_EMOJI_DELETE = 62;
    public static final int ACTION_EMOJI_UPDATE = 61;
    public static final int ACTION_GUILD_SCHEDULED_EVENT_CREATE = 100;
    public static final int ACTION_GUILD_SCHEDULED_EVENT_DELETE = 102;
    public static final int ACTION_GUILD_SCHEDULED_EVENT_UPDATE = 101;
    public static final int ACTION_GUILD_UPDATE = 1;
    public static final int ACTION_INTEGRATION_CREATE = 80;
    public static final int ACTION_INTEGRATION_DELETE = 82;
    public static final int ACTION_INTEGRATION_UPDATE = 81;
    public static final int ACTION_INVITE_CREATE = 40;
    public static final int ACTION_INVITE_DELETE = 42;
    public static final int ACTION_INVITE_UPDATE = 41;
    public static final int ACTION_MEMBER_BAN_ADD = 22;
    public static final int ACTION_MEMBER_BAN_REMOVE = 23;
    public static final int ACTION_MEMBER_DISCONNECT = 27;
    public static final int ACTION_MEMBER_KICK = 20;
    public static final int ACTION_MEMBER_MOVE = 26;
    public static final int ACTION_MEMBER_PRUNE = 21;
    public static final int ACTION_MEMBER_ROLE_UPDATE = 25;
    public static final int ACTION_MEMBER_UPDATE = 24;
    public static final int ACTION_MESSAGE_BULK_DELETE = 73;
    public static final int ACTION_MESSAGE_DELETE = 72;
    public static final int ACTION_MESSAGE_PIN = 74;
    public static final int ACTION_MESSAGE_UNPIN = 75;
    public static final int ACTION_ROLE_CREATE = 30;
    public static final int ACTION_ROLE_DELETE = 32;
    public static final int ACTION_ROLE_UPDATE = 31;
    public static final int ACTION_STAGE_INSTANCE_CREATE = 83;
    public static final int ACTION_STAGE_INSTANCE_DELETE = 85;
    public static final int ACTION_STAGE_INSTANCE_UPDATE = 84;
    public static final int ACTION_STICKER_CREATE = 90;
    public static final int ACTION_STICKER_DELETE = 92;
    public static final int ACTION_STICKER_UPDATE = 91;
    public static final int ACTION_THREAD_CREATE = 110;
    public static final int ACTION_THREAD_DELETE = 112;
    public static final int ACTION_THREAD_UPDATE = 111;
    public static final int ACTION_WEBHOOK_CREATE = 50;
    public static final int ACTION_WEBHOOK_DELETE = 52;
    public static final int ACTION_WEBHOOK_UPDATE = 51;
    public static final String CHANGE_KEY_AFK_CHANNEL_ID = "afk_channel_id";
    public static final String CHANGE_KEY_AFK_TIMEOUT = "afk_timeout";
    public static final String CHANGE_KEY_APPLICATION_ID = "application_id";
    public static final String CHANGE_KEY_ARCHIVED = "archived";
    public static final String CHANGE_KEY_ASSET = "asset";
    public static final String CHANGE_KEY_AUTO_ARCHIVE_DURATION = "auto_archive_duration";
    public static final String CHANGE_KEY_AVAILABLE = "available";
    public static final String CHANGE_KEY_AVATAR_HASH = "avatar_hash";
    public static final String CHANGE_KEY_BANNER_HASH = "banner_hash";
    public static final String CHANGE_KEY_BITRATE = "bitrate";
    public static final String CHANGE_KEY_CHANNEL_ID = "channel_id";
    public static final String CHANGE_KEY_CODE = "code";
    public static final String CHANGE_KEY_COLOR = "color";
    public static final String CHANGE_KEY_DEAF = "deaf";
    public static final String CHANGE_KEY_DEFAULT_AUTO_ARCHIVE_DURATION = "default_auto_archive_duration";
    public static final String CHANGE_KEY_DEFAULT_MESSAGE_NOTIFICATIONS = "default_message_notifications";
    public static final String CHANGE_KEY_DESCRIPTION = "description";
    public static final String CHANGE_KEY_DISCOVERY_SPLASH_HASH = "discovery_splash_hash";
    public static final String CHANGE_KEY_ENABLE_EMOTICONS = "enable_emoticons";
    public static final String CHANGE_KEY_ENTITY_TYPE = "entity_type";
    public static final String CHANGE_KEY_EXPIRE_BEHAVIOR = "expire_behavior";
    public static final String CHANGE_KEY_EXPIRE_GRACE_PERIOD = "expire_grace_period";
    public static final String CHANGE_KEY_EXPLICIT_CONTENT_FILTER = "explicit_content_filter";
    public static final String CHANGE_KEY_FORMAT_TYPE = "format_type";
    public static final String CHANGE_KEY_GUILD_COMMUNICATION_DISABLED = "communication_disabled_until";
    public static final String CHANGE_KEY_GUILD_ID = "guild_id";
    public static final String CHANGE_KEY_HOIST = "hoist";
    public static final String CHANGE_KEY_ICON_HASH = "icon_hash";
    public static final String CHANGE_KEY_ID = "id";
    public static final String CHANGE_KEY_INVITER_ID = "inviter_id";
    public static final String CHANGE_KEY_LOCATION = "location";
    public static final String CHANGE_KEY_LOCKED = "locked";
    public static final String CHANGE_KEY_MAX_AGE = "max_age";
    public static final String CHANGE_KEY_MAX_USES = "max_uses";
    public static final String CHANGE_KEY_MENTIONABLE = "mentionable";
    public static final String CHANGE_KEY_MFA_LEVEL = "mfa_level";
    public static final String CHANGE_KEY_MUTE = "mute";
    public static final String CHANGE_KEY_NAME = "name";
    public static final String CHANGE_KEY_NICK = "nick";
    public static final String CHANGE_KEY_NSFW = "nsfw";
    public static final String CHANGE_KEY_OWNER_ID = "owner_id";
    public static final String CHANGE_KEY_PERMISSIONS = "permissions";
    public static final String CHANGE_KEY_PERMISSIONS_DENIED = "deny";
    public static final String CHANGE_KEY_PERMISSIONS_GRANTED = "allow";
    public static final String CHANGE_KEY_PERMISSIONS_RESET = "reset";
    public static final String CHANGE_KEY_PERMISSION_OVERWRITES = "permission_overwrites";
    public static final String CHANGE_KEY_POSITION = "position";
    public static final String CHANGE_KEY_PREFERRED_LOCALE = "preferred_locale";
    public static final String CHANGE_KEY_PREMIUM_PROGRESS_BAR_ENABLED = "premium_progress_bar_enabled";
    public static final String CHANGE_KEY_PRIVACY_LEVEL = "privacy_level";
    public static final String CHANGE_KEY_PRUNE_DELETE_DAYS = "prune_delete_days";
    public static final String CHANGE_KEY_RATE_LIMIT_PER_USER = "rate_limit_per_user";
    public static final String CHANGE_KEY_REASON = "reason";
    public static final String CHANGE_KEY_REGION = "region";
    public static final String CHANGE_KEY_REGION_OVERRIDE = "rtc_region";
    public static final String CHANGE_KEY_ROLES_ADD = "$add";
    public static final String CHANGE_KEY_ROLES_REMOVE = "$remove";
    public static final String CHANGE_KEY_RULES_CHANNEL_ID = "rules_channel_id";
    public static final String CHANGE_KEY_SPLASH_HASH = "splash_hash";
    public static final String CHANGE_KEY_STATUS = "status";
    public static final String CHANGE_KEY_SYSTEM_CHANNEL_ID = "system_channel_id";
    public static final String CHANGE_KEY_TAGS = "tags";
    public static final String CHANGE_KEY_TEMPORARY = "temporary";
    public static final String CHANGE_KEY_TOPIC = "topic";
    public static final String CHANGE_KEY_TYPE = "type";
    public static final String CHANGE_KEY_UNICODE_EMOJI = "unicode_emoji";
    public static final String CHANGE_KEY_UPDATES_CHANNEL_ID = "public_updates_channel_id";
    public static final String CHANGE_KEY_USES = "uses";
    public static final String CHANGE_KEY_VANITY_URL_CODE = "vanity_url_code";
    public static final String CHANGE_KEY_VERIFICATION_LEVEL = "verification_level";
    public static final String CHANGE_KEY_VIDEO_QUALITY_MODE = "video_quality_mode";
    public static final String CHANGE_KEY_WIDGET_CHANNEL_ID = "widget_channel_id";
    public static final String CHANGE_KEY_WIDGET_ENABLED = "widget_enabled";
    private int actionTypeId;
    private List<Change> changes;
    private Long guildId;

    /* renamed from: id  reason: collision with root package name */
    private long f2688id;
    private Options options;
    private String reason;
    private long targetId;
    private Long timestampEnd;
    private long userId;

    /* renamed from: com.discord.models.domain.ModelAuditLogEntry$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static /* synthetic */ class AnonymousClass1 {
        public static final /* synthetic */ int[] $SwitchMap$com$google$gson$stream$JsonToken;

        static {
            JsonToken.values();
            int[] iArr = new int[10];
            $SwitchMap$com$google$gson$stream$JsonToken = iArr;
            try {
                iArr[6] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$gson$stream$JsonToken[5] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$gson$stream$JsonToken[2] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    /* loaded from: classes.dex */
    public enum ActionType {
        ALL,
        CREATE,
        UPDATE,
        DELETE
    }

    /* loaded from: classes.dex */
    public static class Change implements Model {
        private String key;
        private Object newValue;
        private Object oldValue;

        public Change() {
        }

        @Override // com.discord.models.domain.Model
        public void assignField(Model.JsonReader jsonReader) throws IOException {
            ValueFactory valueFactory = new ValueFactory(jsonReader);
            String nextName = jsonReader.nextName();
            nextName.hashCode();
            char c = 65535;
            switch (nextName.hashCode()) {
                case -250812910:
                    if (nextName.equals("new_value")) {
                        c = 0;
                        break;
                    }
                    break;
                case 106079:
                    if (nextName.equals("key")) {
                        c = 1;
                        break;
                    }
                    break;
                case 1868769625:
                    if (nextName.equals("old_value")) {
                        c = 2;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    this.newValue = jsonReader.parseUnknown(valueFactory);
                    return;
                case 1:
                    this.key = jsonReader.nextString(this.key);
                    return;
                case 2:
                    this.oldValue = jsonReader.parseUnknown(valueFactory);
                    return;
                default:
                    jsonReader.skipValue();
                    return;
            }
        }

        public boolean canEqual(Object obj) {
            return obj instanceof Change;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Change)) {
                return false;
            }
            Change change = (Change) obj;
            if (!change.canEqual(this)) {
                return false;
            }
            String key = getKey();
            String key2 = change.getKey();
            if (key != null ? !key.equals(key2) : key2 != null) {
                return false;
            }
            Object oldValue = getOldValue();
            Object oldValue2 = change.getOldValue();
            if (oldValue != null ? !oldValue.equals(oldValue2) : oldValue2 != null) {
                return false;
            }
            Object newValue = getNewValue();
            Object newValue2 = change.getNewValue();
            return newValue != null ? newValue.equals(newValue2) : newValue2 == null;
        }

        public String getKey() {
            return this.key;
        }

        public Object getNewValue() {
            return this.newValue;
        }

        public Object getOldValue() {
            return this.oldValue;
        }

        public Object getValue() {
            Object obj = this.newValue;
            return obj != null ? obj : this.oldValue;
        }

        public int hashCode() {
            String key = getKey();
            int i = 43;
            int hashCode = key == null ? 43 : key.hashCode();
            Object oldValue = getOldValue();
            int hashCode2 = ((hashCode + 59) * 59) + (oldValue == null ? 43 : oldValue.hashCode());
            Object newValue = getNewValue();
            int i2 = hashCode2 * 59;
            if (newValue != null) {
                i = newValue.hashCode();
            }
            return i2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("ModelAuditLogEntry.Change(key=");
            R.append(getKey());
            R.append(", oldValue=");
            R.append(getOldValue());
            R.append(", newValue=");
            R.append(getNewValue());
            R.append(")");
            return R.toString();
        }

        public Change(String str, Object obj, Object obj2) {
            this.key = str;
            this.oldValue = obj;
            this.newValue = obj2;
        }
    }

    /* loaded from: classes.dex */
    public static class ChangeNameId implements Model {

        /* renamed from: id  reason: collision with root package name */
        private long f2689id;
        private String name;

        @Override // com.discord.models.domain.Model
        public void assignField(Model.JsonReader jsonReader) throws IOException {
            String nextName = jsonReader.nextName();
            nextName.hashCode();
            if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_ID)) {
                this.f2689id = jsonReader.nextLong(this.f2689id);
            } else if (!nextName.equals(ModelAuditLogEntry.CHANGE_KEY_NAME)) {
                jsonReader.skipValue();
            } else {
                this.name = jsonReader.nextString(this.name);
            }
        }

        public boolean canEqual(Object obj) {
            return obj instanceof ChangeNameId;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof ChangeNameId)) {
                return false;
            }
            ChangeNameId changeNameId = (ChangeNameId) obj;
            if (!changeNameId.canEqual(this) || getId() != changeNameId.getId()) {
                return false;
            }
            String name = getName();
            String name2 = changeNameId.getName();
            return name != null ? name.equals(name2) : name2 == null;
        }

        public long getId() {
            return this.f2689id;
        }

        public String getName() {
            return this.name;
        }

        public int hashCode() {
            long id2 = getId();
            String name = getName();
            return ((((int) (id2 ^ (id2 >>> 32))) + 59) * 59) + (name == null ? 43 : name.hashCode());
        }

        public String toString() {
            StringBuilder R = a.R("ModelAuditLogEntry.ChangeNameId(name=");
            R.append(getName());
            R.append(", id=");
            R.append(getId());
            R.append(")");
            return R.toString();
        }
    }

    /* loaded from: classes.dex */
    public class Options implements Model {
        private long channelId;
        private int count;
        private int deleteMemberDays;

        /* renamed from: id  reason: collision with root package name */
        private long f2690id;
        private int membersRemoved;
        private String roleName;
        private int type;

        public Options() {
        }

        @Override // com.discord.models.domain.Model
        public void assignField(Model.JsonReader jsonReader) throws IOException {
            String nextName = jsonReader.nextName();
            nextName.hashCode();
            char c = 65535;
            switch (nextName.hashCode()) {
                case -1930808873:
                    if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_CHANNEL_ID)) {
                        c = 0;
                        break;
                    }
                    break;
                case -838312134:
                    if (nextName.equals("members_removed")) {
                        c = 1;
                        break;
                    }
                    break;
                case -659527096:
                    if (nextName.equals("delete_member_days")) {
                        c = 2;
                        break;
                    }
                    break;
                case 3355:
                    if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_ID)) {
                        c = 3;
                        break;
                    }
                    break;
                case 3575610:
                    if (nextName.equals("type")) {
                        c = 4;
                        break;
                    }
                    break;
                case 94851343:
                    if (nextName.equals("count")) {
                        c = 5;
                        break;
                    }
                    break;
                case 335842484:
                    if (nextName.equals("role_name")) {
                        c = 6;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    this.channelId = jsonReader.nextLong(this.channelId);
                    return;
                case 1:
                    this.membersRemoved = jsonReader.nextInt(this.membersRemoved);
                    return;
                case 2:
                    this.deleteMemberDays = jsonReader.nextInt(this.deleteMemberDays);
                    return;
                case 3:
                    this.f2690id = jsonReader.nextLong(this.f2690id);
                    return;
                case 4:
                    this.type = jsonReader.nextInt(this.type);
                    return;
                case 5:
                    this.count = jsonReader.nextInt(this.count);
                    return;
                case 6:
                    this.roleName = jsonReader.nextString(this.roleName);
                    return;
                default:
                    jsonReader.skipValue();
                    return;
            }
        }

        public boolean canEqual(Object obj) {
            return obj instanceof Options;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Options)) {
                return false;
            }
            Options options = (Options) obj;
            if (!options.canEqual(this) || getCount() != options.getCount() || getChannelId() != options.getChannelId() || getDeleteMemberDays() != options.getDeleteMemberDays() || getMembersRemoved() != options.getMembersRemoved() || getType() != options.getType() || getId() != options.getId()) {
                return false;
            }
            String roleName = getRoleName();
            String roleName2 = options.getRoleName();
            return roleName != null ? roleName.equals(roleName2) : roleName2 == null;
        }

        public long getChannelId() {
            return this.channelId;
        }

        public int getCount() {
            return this.count;
        }

        public int getDeleteMemberDays() {
            return this.deleteMemberDays;
        }

        public long getId() {
            return this.f2690id;
        }

        public int getMembersRemoved() {
            return this.membersRemoved;
        }

        public String getRoleName() {
            return this.roleName;
        }

        public int getType() {
            return this.type;
        }

        public int hashCode() {
            long channelId = getChannelId();
            int deleteMemberDays = getDeleteMemberDays();
            int membersRemoved = getMembersRemoved();
            int type = getType();
            long id2 = getId();
            int count = ((type + ((membersRemoved + ((deleteMemberDays + ((((getCount() + 59) * 59) + ((int) (channelId ^ (channelId >>> 32)))) * 59)) * 59)) * 59)) * 59) + ((int) ((id2 >>> 32) ^ id2));
            String roleName = getRoleName();
            return (count * 59) + (roleName == null ? 43 : roleName.hashCode());
        }

        public String toString() {
            StringBuilder R = a.R("ModelAuditLogEntry.Options(count=");
            R.append(getCount());
            R.append(", channelId=");
            R.append(getChannelId());
            R.append(", roleName=");
            R.append(getRoleName());
            R.append(", deleteMemberDays=");
            R.append(getDeleteMemberDays());
            R.append(", membersRemoved=");
            R.append(getMembersRemoved());
            R.append(", type=");
            R.append(getType());
            R.append(", id=");
            R.append(getId());
            R.append(")");
            return R.toString();
        }
    }

    /* loaded from: classes.dex */
    public enum TargetType {
        ALL,
        UNKNOWN,
        GUILD,
        CHANNEL,
        CHANNEL_OVERWRITE,
        USER,
        ROLE,
        INVITE,
        WEBHOOK,
        EMOJI,
        INTEGRATION,
        STAGE_INSTANCE,
        GUILD_SCHEDULED_EVENT,
        STICKER,
        THREAD
    }

    /* loaded from: classes.dex */
    public static class ValueFactory implements Model.JsonReader.ItemFactory<Object> {
        private final Model.JsonReader reader;

        public ValueFactory(Model.JsonReader jsonReader) {
            this.reader = jsonReader;
        }

        @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
        public Object get() throws IOException {
            int ordinal = this.reader.peek().ordinal();
            if (ordinal == 2) {
                return this.reader.parse(new ChangeNameId());
            }
            if (ordinal == 5) {
                return this.reader.nextStringOrNull();
            }
            if (ordinal != 6) {
                return null;
            }
            return this.reader.nextLongOrNull();
        }
    }

    public ModelAuditLogEntry() {
    }

    @Override // com.discord.models.domain.Model
    public void assignField(final Model.JsonReader jsonReader) throws IOException {
        String nextName = jsonReader.nextName();
        nextName.hashCode();
        char c = 65535;
        switch (nextName.hashCode()) {
            case -1249474914:
                if (nextName.equals("options")) {
                    c = 0;
                    break;
                }
                break;
            case -934964668:
                if (nextName.equals(CHANGE_KEY_REASON)) {
                    c = 1;
                    break;
                }
                break;
            case -815576439:
                if (nextName.equals("target_id")) {
                    c = 2;
                    break;
                }
                break;
            case -147132913:
                if (nextName.equals("user_id")) {
                    c = 3;
                    break;
                }
                break;
            case 3355:
                if (nextName.equals(CHANGE_KEY_ID)) {
                    c = 4;
                    break;
                }
                break;
            case 738943683:
                if (nextName.equals("changes")) {
                    c = 5;
                    break;
                }
                break;
            case 1583758243:
                if (nextName.equals("action_type")) {
                    c = 6;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                this.options = (Options) jsonReader.parse(new Options());
                return;
            case 1:
                this.reason = jsonReader.nextString(this.reason);
                return;
            case 2:
                this.targetId = jsonReader.nextLong(this.targetId);
                return;
            case 3:
                this.userId = jsonReader.nextLong(this.userId);
                return;
            case 4:
                this.f2688id = jsonReader.nextLong(this.f2688id);
                return;
            case 5:
                this.changes = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.h
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        return (ModelAuditLogEntry.Change) Model.JsonReader.this.parse(new ModelAuditLogEntry.Change());
                    }
                });
                return;
            case 6:
                this.actionTypeId = jsonReader.nextInt(this.actionTypeId);
                return;
            default:
                jsonReader.skipValue();
                return;
        }
    }

    public boolean canEqual(Object obj) {
        return obj instanceof ModelAuditLogEntry;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ModelAuditLogEntry)) {
            return false;
        }
        ModelAuditLogEntry modelAuditLogEntry = (ModelAuditLogEntry) obj;
        if (!modelAuditLogEntry.canEqual(this) || getId() != modelAuditLogEntry.getId() || getActionTypeId() != modelAuditLogEntry.getActionTypeId() || getTargetId() != modelAuditLogEntry.getTargetId() || getUserId() != modelAuditLogEntry.getUserId()) {
            return false;
        }
        Long guildId = getGuildId();
        Long guildId2 = modelAuditLogEntry.getGuildId();
        if (guildId != null ? !guildId.equals(guildId2) : guildId2 != null) {
            return false;
        }
        Long timestampEnd = getTimestampEnd();
        Long timestampEnd2 = modelAuditLogEntry.getTimestampEnd();
        if (timestampEnd != null ? !timestampEnd.equals(timestampEnd2) : timestampEnd2 != null) {
            return false;
        }
        List<Change> changes = getChanges();
        List<Change> changes2 = modelAuditLogEntry.getChanges();
        if (changes != null ? !changes.equals(changes2) : changes2 != null) {
            return false;
        }
        Options options = getOptions();
        Options options2 = modelAuditLogEntry.getOptions();
        if (options != null ? !options.equals(options2) : options2 != null) {
            return false;
        }
        String reason = getReason();
        String reason2 = modelAuditLogEntry.getReason();
        return reason != null ? reason.equals(reason2) : reason2 == null;
    }

    public ActionType getActionType() {
        return getActionType(this.actionTypeId);
    }

    public int getActionTypeId() {
        return this.actionTypeId;
    }

    public List<Change> getChanges() {
        return this.changes;
    }

    public Long getGuildId() {
        return this.guildId;
    }

    public long getId() {
        return this.f2688id;
    }

    public Options getOptions() {
        return this.options;
    }

    public String getReason() {
        return this.reason;
    }

    public long getTargetId() {
        return this.targetId;
    }

    public TargetType getTargetType() {
        return getTargetType(this.actionTypeId);
    }

    public Long getTimestampEnd() {
        return this.timestampEnd;
    }

    public long getUserId() {
        return this.userId;
    }

    public int hashCode() {
        long id2 = getId();
        int actionTypeId = getActionTypeId();
        long targetId = getTargetId();
        long userId = getUserId();
        Long guildId = getGuildId();
        int i = 43;
        int hashCode = ((((((actionTypeId + ((((int) (id2 ^ (id2 >>> 32))) + 59) * 59)) * 59) + ((int) (targetId ^ (targetId >>> 32)))) * 59) + ((int) ((userId >>> 32) ^ userId))) * 59) + (guildId == null ? 43 : guildId.hashCode());
        Long timestampEnd = getTimestampEnd();
        int hashCode2 = (hashCode * 59) + (timestampEnd == null ? 43 : timestampEnd.hashCode());
        List<Change> changes = getChanges();
        int hashCode3 = (hashCode2 * 59) + (changes == null ? 43 : changes.hashCode());
        Options options = getOptions();
        int hashCode4 = (hashCode3 * 59) + (options == null ? 43 : options.hashCode());
        String reason = getReason();
        int i2 = hashCode4 * 59;
        if (reason != null) {
            i = reason.hashCode();
        }
        return i2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("ModelAuditLogEntry(id=");
        R.append(getId());
        R.append(", actionTypeId=");
        R.append(getActionTypeId());
        R.append(", targetId=");
        R.append(getTargetId());
        R.append(", userId=");
        R.append(getUserId());
        R.append(", changes=");
        R.append(getChanges());
        R.append(", options=");
        R.append(getOptions());
        R.append(", reason=");
        R.append(getReason());
        R.append(", guildId=");
        R.append(getGuildId());
        R.append(", timestampEnd=");
        R.append(getTimestampEnd());
        R.append(")");
        return R.toString();
    }

    public ModelAuditLogEntry(long j, int i, long j2, long j3, List<Change> list, Options options, long j4, Long l) {
        this.f2688id = j;
        this.actionTypeId = i;
        this.targetId = j2;
        this.userId = j3;
        this.changes = list;
        this.options = options;
        this.guildId = Long.valueOf(j4);
        this.timestampEnd = l;
    }

    /* JADX WARN: Removed duplicated region for block: B:14:0x0021  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x0027  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static com.discord.models.domain.ModelAuditLogEntry.ActionType getActionType(int r0) {
        /*
            switch(r0) {
                case 1: goto L27;
                case 20: goto L24;
                case 21: goto L24;
                case 22: goto L24;
                case 23: goto L21;
                case 24: goto L27;
                case 25: goto L27;
                case 26: goto L27;
                case 27: goto L24;
                case 28: goto L21;
                case 110: goto L21;
                case 111: goto L27;
                case 112: goto L24;
                default: goto L3;
            }
        L3:
            switch(r0) {
                case 10: goto L21;
                case 11: goto L27;
                case 12: goto L24;
                case 13: goto L21;
                case 14: goto L27;
                case 15: goto L24;
                default: goto L6;
            }
        L6:
            switch(r0) {
                case 30: goto L21;
                case 31: goto L27;
                case 32: goto L24;
                default: goto L9;
            }
        L9:
            switch(r0) {
                case 40: goto L21;
                case 41: goto L27;
                case 42: goto L24;
                default: goto Lc;
            }
        Lc:
            switch(r0) {
                case 50: goto L21;
                case 51: goto L27;
                case 52: goto L24;
                default: goto Lf;
            }
        Lf:
            switch(r0) {
                case 60: goto L21;
                case 61: goto L27;
                case 62: goto L24;
                default: goto L12;
            }
        L12:
            switch(r0) {
                case 72: goto L24;
                case 73: goto L24;
                case 74: goto L21;
                case 75: goto L24;
                default: goto L15;
            }
        L15:
            switch(r0) {
                case 80: goto L21;
                case 81: goto L27;
                case 82: goto L24;
                case 83: goto L21;
                case 84: goto L27;
                case 85: goto L24;
                default: goto L18;
            }
        L18:
            switch(r0) {
                case 90: goto L21;
                case 91: goto L27;
                case 92: goto L24;
                default: goto L1b;
            }
        L1b:
            switch(r0) {
                case 100: goto L21;
                case 101: goto L27;
                case 102: goto L24;
                default: goto L1e;
            }
        L1e:
            com.discord.models.domain.ModelAuditLogEntry$ActionType r0 = com.discord.models.domain.ModelAuditLogEntry.ActionType.ALL
            return r0
        L21:
            com.discord.models.domain.ModelAuditLogEntry$ActionType r0 = com.discord.models.domain.ModelAuditLogEntry.ActionType.CREATE
            return r0
        L24:
            com.discord.models.domain.ModelAuditLogEntry$ActionType r0 = com.discord.models.domain.ModelAuditLogEntry.ActionType.DELETE
            return r0
        L27:
            com.discord.models.domain.ModelAuditLogEntry$ActionType r0 = com.discord.models.domain.ModelAuditLogEntry.ActionType.UPDATE
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.models.domain.ModelAuditLogEntry.getActionType(int):com.discord.models.domain.ModelAuditLogEntry$ActionType");
    }

    public static TargetType getTargetType(int i) {
        if (i == 0) {
            return TargetType.ALL;
        }
        if (i <= 1) {
            return TargetType.GUILD;
        }
        if (i <= 12 || i == 73) {
            return TargetType.CHANNEL;
        }
        if (i <= 15) {
            return TargetType.CHANNEL_OVERWRITE;
        }
        if (i <= 28 || i == 72 || i == 74 || i == 75) {
            return TargetType.USER;
        }
        if (i <= 32) {
            return TargetType.ROLE;
        }
        if (i <= 42) {
            return TargetType.INVITE;
        }
        if (i <= 52) {
            return TargetType.WEBHOOK;
        }
        if (i <= 62) {
            return TargetType.EMOJI;
        }
        if (i <= 82) {
            return TargetType.INTEGRATION;
        }
        if (i <= 85) {
            return TargetType.STAGE_INSTANCE;
        }
        if (i <= 92) {
            return TargetType.STICKER;
        }
        if (i <= 102) {
            return TargetType.GUILD_SCHEDULED_EVENT;
        }
        if (i <= 112) {
            return TargetType.THREAD;
        }
        return TargetType.ALL;
    }
}
