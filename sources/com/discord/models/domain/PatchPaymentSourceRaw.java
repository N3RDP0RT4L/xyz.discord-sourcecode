package com.discord.models.domain;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.billing.ModelBillingAddress;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ModelPaymentSource.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0013\u001a\u00020\u00052\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0007¨\u0006\u001b"}, d2 = {"Lcom/discord/models/domain/PatchPaymentSourceRaw;", "", "Lcom/discord/models/domain/billing/ModelBillingAddress;", "component1", "()Lcom/discord/models/domain/billing/ModelBillingAddress;", "", "component2", "()Z", "billingAddress", "default", "copy", "(Lcom/discord/models/domain/billing/ModelBillingAddress;Z)Lcom/discord/models/domain/PatchPaymentSourceRaw;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/billing/ModelBillingAddress;", "getBillingAddress", "Z", "getDefault", HookHelper.constructorName, "(Lcom/discord/models/domain/billing/ModelBillingAddress;Z)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class PatchPaymentSourceRaw {
    private final ModelBillingAddress billingAddress;

    /* renamed from: default  reason: not valid java name */
    private final boolean f9default;

    public PatchPaymentSourceRaw(ModelBillingAddress modelBillingAddress, boolean z2) {
        m.checkNotNullParameter(modelBillingAddress, "billingAddress");
        this.billingAddress = modelBillingAddress;
        this.f9default = z2;
    }

    public static /* synthetic */ PatchPaymentSourceRaw copy$default(PatchPaymentSourceRaw patchPaymentSourceRaw, ModelBillingAddress modelBillingAddress, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            modelBillingAddress = patchPaymentSourceRaw.billingAddress;
        }
        if ((i & 2) != 0) {
            z2 = patchPaymentSourceRaw.f9default;
        }
        return patchPaymentSourceRaw.copy(modelBillingAddress, z2);
    }

    public final ModelBillingAddress component1() {
        return this.billingAddress;
    }

    public final boolean component2() {
        return this.f9default;
    }

    public final PatchPaymentSourceRaw copy(ModelBillingAddress modelBillingAddress, boolean z2) {
        m.checkNotNullParameter(modelBillingAddress, "billingAddress");
        return new PatchPaymentSourceRaw(modelBillingAddress, z2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PatchPaymentSourceRaw)) {
            return false;
        }
        PatchPaymentSourceRaw patchPaymentSourceRaw = (PatchPaymentSourceRaw) obj;
        return m.areEqual(this.billingAddress, patchPaymentSourceRaw.billingAddress) && this.f9default == patchPaymentSourceRaw.f9default;
    }

    public final ModelBillingAddress getBillingAddress() {
        return this.billingAddress;
    }

    public final boolean getDefault() {
        return this.f9default;
    }

    public int hashCode() {
        ModelBillingAddress modelBillingAddress = this.billingAddress;
        int hashCode = (modelBillingAddress != null ? modelBillingAddress.hashCode() : 0) * 31;
        boolean z2 = this.f9default;
        if (z2) {
            z2 = true;
        }
        int i = z2 ? 1 : 0;
        int i2 = z2 ? 1 : 0;
        return hashCode + i;
    }

    public String toString() {
        StringBuilder R = a.R("PatchPaymentSourceRaw(billingAddress=");
        R.append(this.billingAddress);
        R.append(", default=");
        return a.M(R, this.f9default, ")");
    }
}
