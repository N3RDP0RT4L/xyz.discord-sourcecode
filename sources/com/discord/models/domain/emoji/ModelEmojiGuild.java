package com.discord.models.domain.emoji;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.user.User;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: ModelEmojiGuild.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\b\n\u0002\b\u0015\b\u0086\b\u0018\u00002\u00020\u0001BM\u0012\u0006\u0010\u0014\u001a\u00020\u0002\u0012\u0006\u0010\u0015\u001a\u00020\u0005\u0012\u0006\u0010\u0016\u001a\u00020\b\u0012\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00020\u000b\u0012\u0006\u0010\u0018\u001a\u00020\b\u0012\u0006\u0010\u0019\u001a\u00020\u000f\u0012\u0006\u0010\u001a\u001a\u00020\b\u0012\u0006\u0010\u001b\u001a\u00020\b¢\u0006\u0004\b2\u00103J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0016\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u000e\u0010\nJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u0012\u0010\nJ\u0010\u0010\u0013\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u0013\u0010\nJf\u0010\u001c\u001a\u00020\u00002\b\b\u0002\u0010\u0014\u001a\u00020\u00022\b\b\u0002\u0010\u0015\u001a\u00020\u00052\b\b\u0002\u0010\u0016\u001a\u00020\b2\u000e\b\u0002\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00020\u000b2\b\b\u0002\u0010\u0018\u001a\u00020\b2\b\b\u0002\u0010\u0019\u001a\u00020\u000f2\b\b\u0002\u0010\u001a\u001a\u00020\b2\b\b\u0002\u0010\u001b\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001e\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u001e\u0010\u0007J\u0010\u0010 \u001a\u00020\u001fHÖ\u0001¢\u0006\u0004\b \u0010!J\u001a\u0010#\u001a\u00020\b2\b\u0010\"\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b#\u0010$R\u0019\u0010\u001b\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010%\u001a\u0004\b&\u0010\nR\u0019\u0010\u0016\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010%\u001a\u0004\b'\u0010\nR\u0019\u0010\u001a\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010%\u001a\u0004\b(\u0010\nR\u0019\u0010\u0014\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010)\u001a\u0004\b*\u0010\u0004R\u0019\u0010\u0018\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010%\u001a\u0004\b+\u0010\nR\u0019\u0010\u0015\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010,\u001a\u0004\b-\u0010\u0007R\u001f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010.\u001a\u0004\b/\u0010\rR\u0019\u0010\u0019\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u00100\u001a\u0004\b1\u0010\u0011¨\u00064"}, d2 = {"Lcom/discord/models/domain/emoji/ModelEmojiGuild;", "", "", "component1", "()J", "", "component2", "()Ljava/lang/String;", "", "component3", "()Z", "", "component4", "()Ljava/util/List;", "component5", "Lcom/discord/api/user/User;", "component6", "()Lcom/discord/api/user/User;", "component7", "component8", ModelAuditLogEntry.CHANGE_KEY_ID, ModelAuditLogEntry.CHANGE_KEY_NAME, "managed", "roles", "requiredColons", "user", "animated", ModelAuditLogEntry.CHANGE_KEY_AVAILABLE, "copy", "(JLjava/lang/String;ZLjava/util/List;ZLcom/discord/api/user/User;ZZ)Lcom/discord/models/domain/emoji/ModelEmojiGuild;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getAvailable", "getManaged", "getAnimated", "J", "getId", "getRequiredColons", "Ljava/lang/String;", "getName", "Ljava/util/List;", "getRoles", "Lcom/discord/api/user/User;", "getUser", HookHelper.constructorName, "(JLjava/lang/String;ZLjava/util/List;ZLcom/discord/api/user/User;ZZ)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelEmojiGuild {
    private final boolean animated;
    private final boolean available;

    /* renamed from: id  reason: collision with root package name */
    private final long f2715id;
    private final boolean managed;
    private final String name;
    private final boolean requiredColons;
    private final List<Long> roles;
    private final User user;

    public ModelEmojiGuild(long j, String str, boolean z2, List<Long> list, boolean z3, User user, boolean z4, boolean z5) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(list, "roles");
        m.checkNotNullParameter(user, "user");
        this.f2715id = j;
        this.name = str;
        this.managed = z2;
        this.roles = list;
        this.requiredColons = z3;
        this.user = user;
        this.animated = z4;
        this.available = z5;
    }

    public final long component1() {
        return this.f2715id;
    }

    public final String component2() {
        return this.name;
    }

    public final boolean component3() {
        return this.managed;
    }

    public final List<Long> component4() {
        return this.roles;
    }

    public final boolean component5() {
        return this.requiredColons;
    }

    public final User component6() {
        return this.user;
    }

    public final boolean component7() {
        return this.animated;
    }

    public final boolean component8() {
        return this.available;
    }

    public final ModelEmojiGuild copy(long j, String str, boolean z2, List<Long> list, boolean z3, User user, boolean z4, boolean z5) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(list, "roles");
        m.checkNotNullParameter(user, "user");
        return new ModelEmojiGuild(j, str, z2, list, z3, user, z4, z5);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModelEmojiGuild)) {
            return false;
        }
        ModelEmojiGuild modelEmojiGuild = (ModelEmojiGuild) obj;
        return this.f2715id == modelEmojiGuild.f2715id && m.areEqual(this.name, modelEmojiGuild.name) && this.managed == modelEmojiGuild.managed && m.areEqual(this.roles, modelEmojiGuild.roles) && this.requiredColons == modelEmojiGuild.requiredColons && m.areEqual(this.user, modelEmojiGuild.user) && this.animated == modelEmojiGuild.animated && this.available == modelEmojiGuild.available;
    }

    public final boolean getAnimated() {
        return this.animated;
    }

    public final boolean getAvailable() {
        return this.available;
    }

    public final long getId() {
        return this.f2715id;
    }

    public final boolean getManaged() {
        return this.managed;
    }

    public final String getName() {
        return this.name;
    }

    public final boolean getRequiredColons() {
        return this.requiredColons;
    }

    public final List<Long> getRoles() {
        return this.roles;
    }

    public final User getUser() {
        return this.user;
    }

    public int hashCode() {
        long j = this.f2715id;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        String str = this.name;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        boolean z2 = this.managed;
        int i3 = 1;
        if (z2) {
            z2 = true;
        }
        int i4 = z2 ? 1 : 0;
        int i5 = z2 ? 1 : 0;
        int i6 = (hashCode + i4) * 31;
        List<Long> list = this.roles;
        int hashCode2 = (i6 + (list != null ? list.hashCode() : 0)) * 31;
        boolean z3 = this.requiredColons;
        if (z3) {
            z3 = true;
        }
        int i7 = z3 ? 1 : 0;
        int i8 = z3 ? 1 : 0;
        int i9 = (hashCode2 + i7) * 31;
        User user = this.user;
        if (user != null) {
            i2 = user.hashCode();
        }
        int i10 = (i9 + i2) * 31;
        boolean z4 = this.animated;
        if (z4) {
            z4 = true;
        }
        int i11 = z4 ? 1 : 0;
        int i12 = z4 ? 1 : 0;
        int i13 = (i10 + i11) * 31;
        boolean z5 = this.available;
        if (!z5) {
            i3 = z5 ? 1 : 0;
        }
        return i13 + i3;
    }

    public String toString() {
        StringBuilder R = a.R("ModelEmojiGuild(id=");
        R.append(this.f2715id);
        R.append(", name=");
        R.append(this.name);
        R.append(", managed=");
        R.append(this.managed);
        R.append(", roles=");
        R.append(this.roles);
        R.append(", requiredColons=");
        R.append(this.requiredColons);
        R.append(", user=");
        R.append(this.user);
        R.append(", animated=");
        R.append(this.animated);
        R.append(", available=");
        return a.M(R, this.available, ")");
    }
}
