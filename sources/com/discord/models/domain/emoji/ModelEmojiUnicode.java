package com.discord.models.domain.emoji;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import b.d.b.a.a;
import com.discord.models.domain.Model;
import com.discord.models.domain.emoji.ModelEmojiUnicode;
import com.discord.widgets.chat.input.MentionUtilsKt;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
/* loaded from: classes.dex */
public class ModelEmojiUnicode implements Model, Emoji {
    private static final String FILENAME_FORMAT = "emoji_%s";
    private static final String SKIN_TONE_SUFFIX = "::skin-tone-";
    private static final String URI_FORMAT = "res:///%d";
    private final AtomicReference<Object> codePoints = new AtomicReference<>();
    private List<ModelEmojiUnicode> diversityChildren;
    private boolean hasDiversity;
    private boolean hasDiversityParent;
    private boolean hasMultiDiversity;
    private boolean hasMultiDiversityParent;
    private List<String> names;
    private String surrogates;
    private static final String[] DIVERSITY_MODIFIERS = {"🏻", "🏼", "🏽", "🏾", "🏿"};
    public static final Parcelable.Creator<ModelEmojiUnicode> CREATOR = new Parcelable.Creator<ModelEmojiUnicode>() { // from class: com.discord.models.domain.emoji.ModelEmojiUnicode.1
        /* JADX WARN: Can't rename method to resolve collision */
        @Override // android.os.Parcelable.Creator
        public ModelEmojiUnicode createFromParcel(Parcel parcel) {
            ArrayList arrayList = new ArrayList();
            parcel.readStringList(arrayList);
            String readString = parcel.readString();
            boolean z2 = parcel.readInt() > 0;
            boolean z3 = parcel.readInt() > 0;
            boolean z4 = parcel.readInt() > 0;
            boolean z5 = parcel.readInt() > 0;
            ArrayList arrayList2 = new ArrayList();
            parcel.readTypedList(arrayList2, this);
            return new ModelEmojiUnicode(arrayList, readString, z2, z3, z4, z5, arrayList2);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // android.os.Parcelable.Creator
        public ModelEmojiUnicode[] newArray(int i) {
            return new ModelEmojiUnicode[i];
        }
    };

    /* loaded from: classes.dex */
    public static class Bundle implements Model {
        private Map<EmojiCategory, List<ModelEmojiUnicode>> emojis = new HashMap();

        @Override // com.discord.models.domain.Model
        public void assignField(final Model.JsonReader jsonReader) throws IOException {
            EmojiCategory byString = EmojiCategory.getByString(jsonReader.nextName());
            if (byString != null) {
                this.emojis.put(byString, jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.h0.a
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        return (ModelEmojiUnicode) Model.JsonReader.this.parse(new ModelEmojiUnicode());
                    }
                }));
            } else {
                jsonReader.skipValue();
            }
        }

        public boolean canEqual(Object obj) {
            return obj instanceof Bundle;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Bundle)) {
                return false;
            }
            Bundle bundle = (Bundle) obj;
            if (!bundle.canEqual(this)) {
                return false;
            }
            Map<EmojiCategory, List<ModelEmojiUnicode>> emojis = getEmojis();
            Map<EmojiCategory, List<ModelEmojiUnicode>> emojis2 = bundle.getEmojis();
            return emojis != null ? emojis.equals(emojis2) : emojis2 == null;
        }

        public Map<EmojiCategory, List<ModelEmojiUnicode>> getEmojis() {
            return this.emojis;
        }

        public int hashCode() {
            Map<EmojiCategory, List<ModelEmojiUnicode>> emojis = getEmojis();
            return 59 + (emojis == null ? 43 : emojis.hashCode());
        }

        public String toString() {
            StringBuilder R = a.R("ModelEmojiUnicode.Bundle(emojis=");
            R.append(getEmojis());
            R.append(")");
            return R.toString();
        }
    }

    public ModelEmojiUnicode() {
    }

    private String toCodePoint() {
        int i;
        char charAt;
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        while (true) {
            char c = 0;
            while (i2 < this.surrogates.length()) {
                i = i2 + 1;
                charAt = this.surrogates.charAt(i2);
                if (c != 0) {
                    break;
                }
                if (55296 > charAt || charAt > 56319) {
                    arrayList.add(Integer.toHexString(charAt));
                } else {
                    c = charAt;
                }
                i2 = i;
            }
            return TextUtils.join("_", arrayList);
            arrayList.add(Integer.toHexString((charAt - 56320) + ((c - 55296) << 10) + 65536));
            i2 = i;
        }
    }

    @Override // com.discord.models.domain.Model
    public void assignField(final Model.JsonReader jsonReader) throws IOException {
        String nextName = jsonReader.nextName();
        nextName.hashCode();
        char c = 65535;
        switch (nextName.hashCode()) {
            case -2019572390:
                if (nextName.equals("hasMultiDiversity")) {
                    c = 0;
                    break;
                }
                break;
            case -1212936855:
                if (nextName.equals("hasDiversityParent")) {
                    c = 1;
                    break;
                }
                break;
            case 104585032:
                if (nextName.equals("names")) {
                    c = 2;
                    break;
                }
                break;
            case 341058715:
                if (nextName.equals("surrogates")) {
                    c = 3;
                    break;
                }
                break;
            case 1515109343:
                if (nextName.equals("hasDiversity")) {
                    c = 4;
                    break;
                }
                break;
            case 1727581860:
                if (nextName.equals("hasMultiDiversityParent")) {
                    c = 5;
                    break;
                }
                break;
            case 2016033400:
                if (nextName.equals("diversityChildren")) {
                    c = 6;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                this.hasMultiDiversity = jsonReader.nextBoolean(this.hasMultiDiversity);
                return;
            case 1:
                this.hasDiversityParent = jsonReader.nextBoolean(this.hasDiversityParent);
                return;
            case 2:
                this.names = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.h0.c
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        Model.JsonReader jsonReader2 = Model.JsonReader.this;
                        Parcelable.Creator<ModelEmojiUnicode> creator = ModelEmojiUnicode.CREATOR;
                        return jsonReader2.nextString(null);
                    }
                });
                return;
            case 3:
                this.surrogates = jsonReader.nextString(this.surrogates);
                return;
            case 4:
                this.hasDiversity = jsonReader.nextBoolean(this.hasDiversity);
                return;
            case 5:
                this.hasMultiDiversityParent = jsonReader.nextBoolean(this.hasMultiDiversityParent);
                return;
            case 6:
                this.diversityChildren = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.h0.b
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        return (ModelEmojiUnicode) Model.JsonReader.this.parse(new ModelEmojiUnicode());
                    }
                });
                return;
            default:
                jsonReader.skipValue();
                return;
        }
    }

    public boolean canEqual(Object obj) {
        return obj instanceof ModelEmojiUnicode;
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ModelEmojiUnicode)) {
            return false;
        }
        ModelEmojiUnicode modelEmojiUnicode = (ModelEmojiUnicode) obj;
        if (!modelEmojiUnicode.canEqual(this) || isHasDiversity() != modelEmojiUnicode.isHasDiversity() || isHasMultiDiversity() != modelEmojiUnicode.isHasMultiDiversity() || isHasDiversityParent() != modelEmojiUnicode.isHasDiversityParent() || isHasMultiDiversityParent() != modelEmojiUnicode.isHasMultiDiversityParent()) {
            return false;
        }
        List<String> names = getNames();
        List<String> names2 = modelEmojiUnicode.getNames();
        if (names != null ? !names.equals(names2) : names2 != null) {
            return false;
        }
        String surrogates = getSurrogates();
        String surrogates2 = modelEmojiUnicode.getSurrogates();
        if (surrogates != null ? !surrogates.equals(surrogates2) : surrogates2 != null) {
            return false;
        }
        List<ModelEmojiUnicode> diversityChildren = getDiversityChildren();
        List<ModelEmojiUnicode> diversityChildren2 = modelEmojiUnicode.getDiversityChildren();
        if (diversityChildren != null ? !diversityChildren.equals(diversityChildren2) : diversityChildren2 != null) {
            return false;
        }
        String codePoints = getCodePoints();
        String codePoints2 = modelEmojiUnicode.getCodePoints();
        return codePoints != null ? codePoints.equals(codePoints2) : codePoints2 == null;
    }

    public List<ModelEmojiUnicode> getAsDiverse() {
        List<ModelEmojiUnicode> list = this.diversityChildren;
        return list == null ? Collections.emptyList() : list;
    }

    @Override // com.discord.models.domain.emoji.Emoji
    public String getChatInputText() {
        return getCommand(getFirstName());
    }

    public String getCodePoints() {
        Object obj = this.codePoints.get();
        if (obj == null) {
            synchronized (this.codePoints) {
                obj = this.codePoints.get();
                if (obj == null) {
                    obj = toCodePoint();
                    if (obj == null) {
                        obj = this.codePoints;
                    }
                    this.codePoints.set(obj);
                }
            }
        }
        if (obj == this.codePoints) {
            obj = null;
        }
        return (String) obj;
    }

    @Override // com.discord.models.domain.emoji.Emoji
    public String getCommand(String str) {
        if (str == null) {
            str = getFirstName();
        }
        return String.format(":%s:", str);
    }

    public List<ModelEmojiUnicode> getDiversityChildren() {
        return this.diversityChildren;
    }

    @Override // com.discord.models.domain.emoji.Emoji
    public String getFirstName() {
        return this.names.get(0);
    }

    @Override // com.discord.models.domain.emoji.Emoji
    public String getImageUri(boolean z2, int i, Context context) {
        return getImageUri(getCodePoints(), context);
    }

    @Override // com.discord.models.domain.emoji.Emoji
    public String getMessageContentReplacement() {
        return this.surrogates;
    }

    @Override // com.discord.models.domain.emoji.Emoji
    public List<String> getNames() {
        return this.names;
    }

    @Override // com.discord.models.domain.emoji.Emoji
    public String getReactionKey() {
        return this.surrogates;
    }

    @Override // com.discord.models.domain.emoji.Emoji
    public Pattern getRegex(String str) {
        if (str == null) {
            str = getFirstName();
        }
        try {
            return Pattern.compile("([^\\\\]|^):" + str + MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR);
        } catch (PatternSyntaxException unused) {
            return Pattern.compile("$^");
        }
    }

    public String getSurrogates() {
        return this.surrogates;
    }

    @Override // com.discord.models.domain.emoji.Emoji
    public String getUniqueId() {
        return this.surrogates;
    }

    public int hashCode() {
        int i = 79;
        int i2 = ((((((isHasDiversity() ? 79 : 97) + 59) * 59) + (isHasMultiDiversity() ? 79 : 97)) * 59) + (isHasDiversityParent() ? 79 : 97)) * 59;
        if (!isHasMultiDiversityParent()) {
            i = 97;
        }
        int i3 = i2 + i;
        List<String> names = getNames();
        int i4 = 43;
        int hashCode = (i3 * 59) + (names == null ? 43 : names.hashCode());
        String surrogates = getSurrogates();
        int hashCode2 = (hashCode * 59) + (surrogates == null ? 43 : surrogates.hashCode());
        List<ModelEmojiUnicode> diversityChildren = getDiversityChildren();
        int hashCode3 = (hashCode2 * 59) + (diversityChildren == null ? 43 : diversityChildren.hashCode());
        String codePoints = getCodePoints();
        int i5 = hashCode3 * 59;
        if (codePoints != null) {
            i4 = codePoints.hashCode();
        }
        return i5 + i4;
    }

    @Override // com.discord.models.domain.emoji.Emoji
    public boolean isAvailable() {
        return true;
    }

    public boolean isHasDiversity() {
        return this.hasDiversity;
    }

    public boolean isHasDiversityParent() {
        return this.hasDiversityParent;
    }

    public boolean isHasMultiDiversity() {
        return this.hasMultiDiversity;
    }

    public boolean isHasMultiDiversityParent() {
        return this.hasMultiDiversityParent;
    }

    @Override // com.discord.models.domain.emoji.Emoji
    public boolean isUsable() {
        return true;
    }

    @Override // com.discord.models.domain.emoji.Emoji
    public boolean requiresColons() {
        return true;
    }

    public String toString() {
        StringBuilder R = a.R("ModelEmojiUnicode(names=");
        R.append(getNames());
        R.append(", surrogates=");
        R.append(getSurrogates());
        R.append(", hasDiversity=");
        R.append(isHasDiversity());
        R.append(", hasMultiDiversity=");
        R.append(isHasMultiDiversity());
        R.append(", hasDiversityParent=");
        R.append(isHasDiversityParent());
        R.append(", hasMultiDiversityParent=");
        R.append(isHasMultiDiversityParent());
        R.append(", diversityChildren=");
        R.append(getDiversityChildren());
        R.append(", codePoints=");
        R.append(getCodePoints());
        R.append(")");
        return R.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStringList(this.names);
        parcel.writeString(this.surrogates);
        parcel.writeInt(this.hasDiversity ? 1 : 0);
        parcel.writeInt(this.hasMultiDiversity ? 1 : 0);
        parcel.writeInt(this.hasDiversityParent ? 1 : 0);
        parcel.writeInt(this.hasMultiDiversityParent ? 1 : 0);
        parcel.writeTypedList(this.diversityChildren);
    }

    @SuppressLint({"DefaultLocale"})
    public static String getImageUri(String str, Context context) {
        return String.format(URI_FORMAT, Integer.valueOf(context.getResources().getIdentifier(String.format(FILENAME_FORMAT, str), "raw", context.getPackageName())));
    }

    public ModelEmojiUnicode(List<String> list, String str, boolean z2, boolean z3, boolean z4, boolean z5, List<ModelEmojiUnicode> list2) {
        this.names = list;
        this.surrogates = str;
        this.hasDiversity = z2;
        this.hasMultiDiversity = z3;
        this.hasDiversityParent = z4;
        this.hasMultiDiversityParent = z5;
        this.diversityChildren = list2;
    }
}
