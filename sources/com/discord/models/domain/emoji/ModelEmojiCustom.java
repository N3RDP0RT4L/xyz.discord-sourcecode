package com.discord.models.domain.emoji;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import b.d.b.a.a;
import com.discord.api.emoji.GuildEmoji;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.string.StringUtilsKt;
import com.discord.widgets.chat.input.MentionUtilsKt;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
/* loaded from: classes.dex */
public class ModelEmojiCustom implements Emoji {
    public static final int DISABLED_ALPHA = 100;
    public static final int NORMAL_ALPHA = 255;
    public static String emojiUriFormat;
    private final Boolean available;
    private transient int disambiguationIndex;
    private long guildId;

    /* renamed from: id  reason: collision with root package name */
    private long f2714id;
    private String idStr;
    private boolean isAnimated;
    private transient boolean isUsable;
    private boolean managed;
    private final String name;
    private transient String nameDisambiguated;
    private boolean requireColons;
    private final List<Long> roles;
    private static final List<Long> EMPTY_ROLES = new ArrayList();
    public static final Parcelable.Creator<ModelEmojiCustom> CREATOR = new Parcelable.Creator<ModelEmojiCustom>() { // from class: com.discord.models.domain.emoji.ModelEmojiCustom.1
        /* JADX WARN: Can't rename method to resolve collision */
        @Override // android.os.Parcelable.Creator
        public ModelEmojiCustom createFromParcel(Parcel parcel) {
            long readLong = parcel.readLong();
            String readString = parcel.readString();
            ArrayList arrayList = new ArrayList();
            parcel.readList(arrayList, null);
            return new ModelEmojiCustom(readLong, readString, arrayList, parcel.readInt() > 0, parcel.readInt() > 0, parcel.readInt(), parcel.readString(), parcel.readInt() > 0, parcel.readInt() > 0, parcel.readInt() > 0, parcel.readLong());
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // android.os.Parcelable.Creator
        public ModelEmojiCustom[] newArray(int i) {
            return new ModelEmojiCustom[i];
        }
    };

    public ModelEmojiCustom() {
        this.idStr = "0";
        this.name = "";
        this.roles = new ArrayList();
        this.isUsable = true;
        this.available = Boolean.TRUE;
    }

    private String getColonRegex() {
        return a.G(a.R("((?<!\\\\)):"), getNameDisambiguated(), MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR);
    }

    private String getNameDisambiguated() {
        String str = this.nameDisambiguated;
        return str != null ? str : this.name;
    }

    private String getNoColonRegex() {
        return a.H(a.R("([^\\\\:]|^)"), getNameDisambiguated(), "\\b");
    }

    public static void setCdnUri(String str) {
        emojiUriFormat = a.v(str, "/emojis/%s.%s?size=%s&quality=lossless");
    }

    public boolean canEqual(Object obj) {
        return obj instanceof ModelEmojiCustom;
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ModelEmojiCustom)) {
            return false;
        }
        ModelEmojiCustom modelEmojiCustom = (ModelEmojiCustom) obj;
        if (!modelEmojiCustom.canEqual(this) || getId() != modelEmojiCustom.getId() || isRequireColons() != modelEmojiCustom.isRequireColons() || isManaged() != modelEmojiCustom.isManaged() || isAnimated() != modelEmojiCustom.isAnimated() || getGuildId() != modelEmojiCustom.getGuildId() || isUsable() != modelEmojiCustom.isUsable()) {
            return false;
        }
        Boolean available = getAvailable();
        Boolean available2 = modelEmojiCustom.getAvailable();
        if (available != null ? !available.equals(available2) : available2 != null) {
            return false;
        }
        String idStr = getIdStr();
        String idStr2 = modelEmojiCustom.getIdStr();
        if (idStr != null ? !idStr.equals(idStr2) : idStr2 != null) {
            return false;
        }
        String name = getName();
        String name2 = modelEmojiCustom.getName();
        if (name != null ? !name.equals(name2) : name2 != null) {
            return false;
        }
        List<Long> roles = getRoles();
        List<Long> roles2 = modelEmojiCustom.getRoles();
        return roles != null ? roles.equals(roles2) : roles2 == null;
    }

    public Boolean getAvailable() {
        return this.available;
    }

    @Override // com.discord.models.domain.emoji.Emoji
    public String getChatInputText() {
        return getCommand(getNameDisambiguated());
    }

    @Override // com.discord.models.domain.emoji.Emoji
    public String getCommand(String str) {
        return String.format(":%s:", getNameDisambiguated());
    }

    public int getDisambiguationIndex() {
        return this.disambiguationIndex;
    }

    @Override // com.discord.models.domain.emoji.Emoji
    public String getFirstName() {
        return getNameDisambiguated();
    }

    public long getGuildId() {
        return this.guildId;
    }

    public long getId() {
        return this.f2714id;
    }

    public String getIdStr() {
        return this.idStr;
    }

    @Override // com.discord.models.domain.emoji.Emoji
    public String getImageUri(boolean z2, int i, Context context) {
        return getImageUri(this.f2714id, this.isAnimated && z2, i);
    }

    @Override // com.discord.models.domain.emoji.Emoji
    public String getMessageContentReplacement() {
        String str;
        StringBuilder sb;
        if (this.isAnimated) {
            sb = new StringBuilder();
            str = "<a:";
        } else {
            sb = new StringBuilder();
            str = "<:";
        }
        sb.append(str);
        sb.append(this.name);
        sb.append(MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR);
        sb.append(this.f2714id);
        sb.append('>');
        return sb.toString();
    }

    public String getName() {
        return this.name;
    }

    @Override // com.discord.models.domain.emoji.Emoji
    public List<String> getNames() {
        return Collections.singletonList(getNameDisambiguated());
    }

    @Override // com.discord.models.domain.emoji.Emoji
    public String getReactionKey() {
        return this.name + MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR + this.f2714id;
    }

    @Override // com.discord.models.domain.emoji.Emoji
    public Pattern getRegex(String str) {
        String str2;
        if (this.requireColons) {
            str2 = getColonRegex();
        } else {
            str2 = getColonRegex() + '|' + getNoColonRegex();
        }
        try {
            return Pattern.compile(str2);
        } catch (PatternSyntaxException unused) {
            return Pattern.compile("$^");
        }
    }

    public List<Long> getRoles() {
        List<Long> list = this.roles;
        return list != null ? list : EMPTY_ROLES;
    }

    @Override // com.discord.models.domain.emoji.Emoji
    public String getUniqueId() {
        return this.idStr;
    }

    public int hashCode() {
        long id2 = getId();
        int i = 79;
        int i2 = (((((((int) (id2 ^ (id2 >>> 32))) + 59) * 59) + (isRequireColons() ? 79 : 97)) * 59) + (isManaged() ? 79 : 97)) * 59;
        int i3 = isAnimated() ? 79 : 97;
        long guildId = getGuildId();
        int i4 = (((i2 + i3) * 59) + ((int) ((guildId >>> 32) ^ guildId))) * 59;
        if (!isUsable()) {
            i = 97;
        }
        Boolean available = getAvailable();
        int i5 = 43;
        int hashCode = ((i4 + i) * 59) + (available == null ? 43 : available.hashCode());
        String idStr = getIdStr();
        int hashCode2 = (hashCode * 59) + (idStr == null ? 43 : idStr.hashCode());
        String name = getName();
        int hashCode3 = (hashCode2 * 59) + (name == null ? 43 : name.hashCode());
        List<Long> roles = getRoles();
        int i6 = hashCode3 * 59;
        if (roles != null) {
            i5 = roles.hashCode();
        }
        return i6 + i5;
    }

    public boolean isAnimated() {
        return this.isAnimated;
    }

    @Override // com.discord.models.domain.emoji.Emoji
    public boolean isAvailable() {
        Boolean bool = this.available;
        if (bool != null) {
            return bool.booleanValue();
        }
        return true;
    }

    public boolean isManaged() {
        return this.managed;
    }

    public boolean isRequireColons() {
        return this.requireColons;
    }

    @Override // com.discord.models.domain.emoji.Emoji
    public boolean isUsable() {
        return this.isUsable;
    }

    @Override // com.discord.models.domain.emoji.Emoji
    public boolean requiresColons() {
        return true;
    }

    public GuildEmoji toApiEmoji() {
        return new GuildEmoji(this.f2714id, this.name, this.roles, this.requireColons, this.managed, this.isAnimated, this.available);
    }

    public String toString() {
        StringBuilder R = a.R("ModelEmojiCustom(id=");
        R.append(getId());
        R.append(", idStr=");
        R.append(getIdStr());
        R.append(", name=");
        R.append(getName());
        R.append(", roles=");
        R.append(getRoles());
        R.append(", requireColons=");
        R.append(isRequireColons());
        R.append(", managed=");
        R.append(isManaged());
        R.append(", isAnimated=");
        R.append(isAnimated());
        R.append(", available=");
        R.append(getAvailable());
        R.append(", guildId=");
        R.append(getGuildId());
        R.append(", disambiguationIndex=");
        R.append(getDisambiguationIndex());
        R.append(", nameDisambiguated=");
        R.append(getNameDisambiguated());
        R.append(", isUsable=");
        R.append(isUsable());
        R.append(")");
        return R.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.f2714id);
        parcel.writeString(this.name);
        parcel.writeList(this.roles);
        parcel.writeInt(this.requireColons ? 1 : 0);
        parcel.writeInt(this.managed ? 1 : 0);
        parcel.writeInt(this.disambiguationIndex);
        parcel.writeString(this.nameDisambiguated);
        parcel.writeInt(this.isAnimated ? 1 : 0);
        parcel.writeInt(this.isUsable ? 1 : 0);
        Boolean bool = this.available;
        parcel.writeInt((bool == null || bool.booleanValue()) ? 1 : 0);
        parcel.writeLong(this.guildId);
    }

    public static String getImageUri(long j, boolean z2, int i) {
        String str = emojiUriFormat;
        Object[] objArr = new Object[3];
        objArr[0] = Long.valueOf(j);
        objArr[1] = z2 ? IconUtils.ANIMATED_IMAGE_EXTENSION : StringUtilsKt.getSTATIC_IMAGE_EXTENSION();
        objArr[2] = Integer.valueOf(i);
        return String.format(str, objArr);
    }

    public ModelEmojiCustom(long j, String str, List<Long> list, boolean z2, boolean z3, int i, String str2, boolean z4, boolean z5, boolean z6, long j2) {
        this.idStr = "0";
        this.f2714id = j;
        this.idStr = String.valueOf(j);
        this.name = str;
        this.roles = list;
        this.requireColons = z2;
        this.managed = z3;
        this.disambiguationIndex = i;
        this.nameDisambiguated = str2;
        this.isAnimated = z4;
        this.isUsable = z5;
        this.available = Boolean.valueOf(z6);
        this.guildId = j2;
    }

    public ModelEmojiCustom(ModelEmojiCustom modelEmojiCustom, int i, boolean z2) {
        this.idStr = "0";
        this.f2714id = modelEmojiCustom.f2714id;
        this.idStr = modelEmojiCustom.idStr;
        this.roles = modelEmojiCustom.getRoles();
        this.requireColons = modelEmojiCustom.requireColons;
        this.managed = modelEmojiCustom.managed;
        this.disambiguationIndex = i;
        String str = modelEmojiCustom.name;
        this.name = str;
        this.isAnimated = modelEmojiCustom.isAnimated;
        this.isUsable = z2;
        this.available = modelEmojiCustom.available;
        this.guildId = modelEmojiCustom.guildId;
        if (i > 0) {
            this.nameDisambiguated = str + '-' + i;
        }
    }

    public ModelEmojiCustom(GuildEmoji guildEmoji, long j) {
        this.idStr = "0";
        boolean z2 = true;
        this.isUsable = true;
        this.f2714id = guildEmoji.c();
        this.name = guildEmoji.e();
        this.idStr = String.valueOf(guildEmoji.c());
        this.roles = guildEmoji.g() != null ? guildEmoji.g() : new ArrayList<>();
        this.requireColons = guildEmoji.f();
        this.managed = guildEmoji.d();
        this.isAnimated = guildEmoji.a();
        this.available = Boolean.valueOf(guildEmoji.b() != null ? guildEmoji.b().booleanValue() : z2);
        this.guildId = j;
    }
}
