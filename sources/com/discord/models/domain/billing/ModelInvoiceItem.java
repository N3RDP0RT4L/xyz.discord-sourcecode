package com.discord.models.domain.billing;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: ModelInvoicePreview.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000e\n\u0002\b\u0014\b\u0086\b\u0018\u00002\u00020\u0001BI\u0012\u0006\u0010\u0013\u001a\u00020\u0002\u0012\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0015\u001a\u00020\t\u0012\u0006\u0010\u0016\u001a\u00020\f\u0012\u0006\u0010\u0017\u001a\u00020\u0002\u0012\n\u0010\u0018\u001a\u00060\tj\u0002`\u0010\u0012\u0006\u0010\u0019\u001a\u00020\u0002¢\u0006\u0004\b.\u0010/J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0004J\u0014\u0010\u0011\u001a\u00060\tj\u0002`\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u000bJ\u0010\u0010\u0012\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0004J`\u0010\u001a\u001a\u00020\u00002\b\b\u0002\u0010\u0013\u001a\u00020\u00022\u000e\b\u0002\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0002\u0010\u0015\u001a\u00020\t2\b\b\u0002\u0010\u0016\u001a\u00020\f2\b\b\u0002\u0010\u0017\u001a\u00020\u00022\f\b\u0002\u0010\u0018\u001a\u00060\tj\u0002`\u00102\b\b\u0002\u0010\u0019\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u001d\u001a\u00020\u001cHÖ\u0001¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010\u001f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u001f\u0010\u0004J\u001a\u0010!\u001a\u00020\f2\b\u0010 \u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b!\u0010\"R\u001f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010#\u001a\u0004\b$\u0010\bR\u0019\u0010\u0015\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010%\u001a\u0004\b&\u0010\u000bR\u0019\u0010\u0013\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010'\u001a\u0004\b(\u0010\u0004R\u0019\u0010\u0017\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010'\u001a\u0004\b)\u0010\u0004R\u0019\u0010\u0016\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010*\u001a\u0004\b+\u0010\u000eR\u001d\u0010\u0018\u001a\u00060\tj\u0002`\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010%\u001a\u0004\b,\u0010\u000bR\u0019\u0010\u0019\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010'\u001a\u0004\b-\u0010\u0004¨\u00060"}, d2 = {"Lcom/discord/models/domain/billing/ModelInvoiceItem;", "", "", "component1", "()I", "", "Lcom/discord/models/domain/billing/ModelInvoiceDiscount;", "component2", "()Ljava/util/List;", "", "component3", "()J", "", "component4", "()Z", "component5", "Lcom/discord/primitives/PlanId;", "component6", "component7", "amount", "discount", ModelAuditLogEntry.CHANGE_KEY_ID, "proration", "quantity", "subscriptionPlanId", "subscriptionPlanPrice", "copy", "(ILjava/util/List;JZIJI)Lcom/discord/models/domain/billing/ModelInvoiceItem;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getDiscount", "J", "getId", "I", "getAmount", "getQuantity", "Z", "getProration", "getSubscriptionPlanId", "getSubscriptionPlanPrice", HookHelper.constructorName, "(ILjava/util/List;JZIJI)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelInvoiceItem {
    private final int amount;
    private final List<ModelInvoiceDiscount> discount;

    /* renamed from: id  reason: collision with root package name */
    private final long f2712id;
    private final boolean proration;
    private final int quantity;
    private final long subscriptionPlanId;
    private final int subscriptionPlanPrice;

    public ModelInvoiceItem(int i, List<ModelInvoiceDiscount> list, long j, boolean z2, int i2, long j2, int i3) {
        m.checkNotNullParameter(list, "discount");
        this.amount = i;
        this.discount = list;
        this.f2712id = j;
        this.proration = z2;
        this.quantity = i2;
        this.subscriptionPlanId = j2;
        this.subscriptionPlanPrice = i3;
    }

    public final int component1() {
        return this.amount;
    }

    public final List<ModelInvoiceDiscount> component2() {
        return this.discount;
    }

    public final long component3() {
        return this.f2712id;
    }

    public final boolean component4() {
        return this.proration;
    }

    public final int component5() {
        return this.quantity;
    }

    public final long component6() {
        return this.subscriptionPlanId;
    }

    public final int component7() {
        return this.subscriptionPlanPrice;
    }

    public final ModelInvoiceItem copy(int i, List<ModelInvoiceDiscount> list, long j, boolean z2, int i2, long j2, int i3) {
        m.checkNotNullParameter(list, "discount");
        return new ModelInvoiceItem(i, list, j, z2, i2, j2, i3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModelInvoiceItem)) {
            return false;
        }
        ModelInvoiceItem modelInvoiceItem = (ModelInvoiceItem) obj;
        return this.amount == modelInvoiceItem.amount && m.areEqual(this.discount, modelInvoiceItem.discount) && this.f2712id == modelInvoiceItem.f2712id && this.proration == modelInvoiceItem.proration && this.quantity == modelInvoiceItem.quantity && this.subscriptionPlanId == modelInvoiceItem.subscriptionPlanId && this.subscriptionPlanPrice == modelInvoiceItem.subscriptionPlanPrice;
    }

    public final int getAmount() {
        return this.amount;
    }

    public final List<ModelInvoiceDiscount> getDiscount() {
        return this.discount;
    }

    public final long getId() {
        return this.f2712id;
    }

    public final boolean getProration() {
        return this.proration;
    }

    public final int getQuantity() {
        return this.quantity;
    }

    public final long getSubscriptionPlanId() {
        return this.subscriptionPlanId;
    }

    public final int getSubscriptionPlanPrice() {
        return this.subscriptionPlanPrice;
    }

    public int hashCode() {
        int i = this.amount * 31;
        List<ModelInvoiceDiscount> list = this.discount;
        int hashCode = list != null ? list.hashCode() : 0;
        long j = this.f2712id;
        int i2 = (((i + hashCode) * 31) + ((int) (j ^ (j >>> 32)))) * 31;
        boolean z2 = this.proration;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        long j2 = this.subscriptionPlanId;
        return ((((((i2 + i3) * 31) + this.quantity) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + this.subscriptionPlanPrice;
    }

    public String toString() {
        StringBuilder R = a.R("ModelInvoiceItem(amount=");
        R.append(this.amount);
        R.append(", discount=");
        R.append(this.discount);
        R.append(", id=");
        R.append(this.f2712id);
        R.append(", proration=");
        R.append(this.proration);
        R.append(", quantity=");
        R.append(this.quantity);
        R.append(", subscriptionPlanId=");
        R.append(this.subscriptionPlanId);
        R.append(", subscriptionPlanPrice=");
        return a.A(R, this.subscriptionPlanPrice, ")");
    }
}
