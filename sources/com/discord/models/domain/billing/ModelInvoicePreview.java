package com.discord.models.domain.billing;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: ModelInvoicePreview.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b$\b\u0086\b\u0018\u00002\u00020\u0001BU\u0012\u0006\u0010\u0016\u001a\u00020\u0002\u0012\u0006\u0010\u0017\u001a\u00020\u0005\u0012\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\t0\b\u0012\u0006\u0010\u0019\u001a\u00020\u0002\u0012\u0006\u0010\u001a\u001a\u00020\u0002\u0012\u0006\u0010\u001b\u001a\u00020\u000e\u0012\u0006\u0010\u001c\u001a\u00020\u000e\u0012\u0006\u0010\u001d\u001a\u00020\u0012\u0012\u0006\u0010\u001e\u001a\u00020\u000e¢\u0006\u0004\b4\u00105J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0016\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\bHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\f\u0010\u0004J\u0010\u0010\r\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\r\u0010\u0004J\u0010\u0010\u000f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0010J\u0010\u0010\u0013\u001a\u00020\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0010Jp\u0010\u001f\u001a\u00020\u00002\b\b\u0002\u0010\u0016\u001a\u00020\u00022\b\b\u0002\u0010\u0017\u001a\u00020\u00052\u000e\b\u0002\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\t0\b2\b\b\u0002\u0010\u0019\u001a\u00020\u00022\b\b\u0002\u0010\u001a\u001a\u00020\u00022\b\b\u0002\u0010\u001b\u001a\u00020\u000e2\b\b\u0002\u0010\u001c\u001a\u00020\u000e2\b\b\u0002\u0010\u001d\u001a\u00020\u00122\b\b\u0002\u0010\u001e\u001a\u00020\u000eHÆ\u0001¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010!\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b!\u0010\u0004J\u0010\u0010\"\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\"\u0010\u0010J\u001a\u0010$\u001a\u00020\u00122\b\u0010#\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b$\u0010%R\u0019\u0010\u0017\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010&\u001a\u0004\b'\u0010\u0007R\u0019\u0010\u001e\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010(\u001a\u0004\b)\u0010\u0010R\u001f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\t0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010*\u001a\u0004\b+\u0010\u000bR\u0019\u0010\u001a\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010,\u001a\u0004\b-\u0010\u0004R\u0019\u0010\u0019\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010,\u001a\u0004\b.\u0010\u0004R\u0019\u0010\u001c\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010(\u001a\u0004\b/\u0010\u0010R\u0019\u0010\u001b\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010(\u001a\u0004\b0\u0010\u0010R\u0019\u0010\u001d\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00101\u001a\u0004\b2\u0010\u0014R\u0019\u0010\u0016\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010,\u001a\u0004\b3\u0010\u0004¨\u00066"}, d2 = {"Lcom/discord/models/domain/billing/ModelInvoicePreview;", "", "", "component1", "()Ljava/lang/String;", "", "component2", "()J", "", "Lcom/discord/models/domain/billing/ModelInvoiceItem;", "component3", "()Ljava/util/List;", "component4", "component5", "", "component6", "()I", "component7", "", "component8", "()Z", "component9", "currency", ModelAuditLogEntry.CHANGE_KEY_ID, "invoiceItems", "subscriptionPeriodEnd", "subscriptionPeriodStart", "subtotal", "tax", "taxInclusive", "total", "copy", "(Ljava/lang/String;JLjava/util/List;Ljava/lang/String;Ljava/lang/String;IIZI)Lcom/discord/models/domain/billing/ModelInvoicePreview;", "toString", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "J", "getId", "I", "getTotal", "Ljava/util/List;", "getInvoiceItems", "Ljava/lang/String;", "getSubscriptionPeriodStart", "getSubscriptionPeriodEnd", "getTax", "getSubtotal", "Z", "getTaxInclusive", "getCurrency", HookHelper.constructorName, "(Ljava/lang/String;JLjava/util/List;Ljava/lang/String;Ljava/lang/String;IIZI)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelInvoicePreview {
    private final String currency;

    /* renamed from: id  reason: collision with root package name */
    private final long f2713id;
    private final List<ModelInvoiceItem> invoiceItems;
    private final String subscriptionPeriodEnd;
    private final String subscriptionPeriodStart;
    private final int subtotal;
    private final int tax;
    private final boolean taxInclusive;
    private final int total;

    public ModelInvoicePreview(String str, long j, List<ModelInvoiceItem> list, String str2, String str3, int i, int i2, boolean z2, int i3) {
        m.checkNotNullParameter(str, "currency");
        m.checkNotNullParameter(list, "invoiceItems");
        m.checkNotNullParameter(str2, "subscriptionPeriodEnd");
        m.checkNotNullParameter(str3, "subscriptionPeriodStart");
        this.currency = str;
        this.f2713id = j;
        this.invoiceItems = list;
        this.subscriptionPeriodEnd = str2;
        this.subscriptionPeriodStart = str3;
        this.subtotal = i;
        this.tax = i2;
        this.taxInclusive = z2;
        this.total = i3;
    }

    public final String component1() {
        return this.currency;
    }

    public final long component2() {
        return this.f2713id;
    }

    public final List<ModelInvoiceItem> component3() {
        return this.invoiceItems;
    }

    public final String component4() {
        return this.subscriptionPeriodEnd;
    }

    public final String component5() {
        return this.subscriptionPeriodStart;
    }

    public final int component6() {
        return this.subtotal;
    }

    public final int component7() {
        return this.tax;
    }

    public final boolean component8() {
        return this.taxInclusive;
    }

    public final int component9() {
        return this.total;
    }

    public final ModelInvoicePreview copy(String str, long j, List<ModelInvoiceItem> list, String str2, String str3, int i, int i2, boolean z2, int i3) {
        m.checkNotNullParameter(str, "currency");
        m.checkNotNullParameter(list, "invoiceItems");
        m.checkNotNullParameter(str2, "subscriptionPeriodEnd");
        m.checkNotNullParameter(str3, "subscriptionPeriodStart");
        return new ModelInvoicePreview(str, j, list, str2, str3, i, i2, z2, i3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModelInvoicePreview)) {
            return false;
        }
        ModelInvoicePreview modelInvoicePreview = (ModelInvoicePreview) obj;
        return m.areEqual(this.currency, modelInvoicePreview.currency) && this.f2713id == modelInvoicePreview.f2713id && m.areEqual(this.invoiceItems, modelInvoicePreview.invoiceItems) && m.areEqual(this.subscriptionPeriodEnd, modelInvoicePreview.subscriptionPeriodEnd) && m.areEqual(this.subscriptionPeriodStart, modelInvoicePreview.subscriptionPeriodStart) && this.subtotal == modelInvoicePreview.subtotal && this.tax == modelInvoicePreview.tax && this.taxInclusive == modelInvoicePreview.taxInclusive && this.total == modelInvoicePreview.total;
    }

    public final String getCurrency() {
        return this.currency;
    }

    public final long getId() {
        return this.f2713id;
    }

    public final List<ModelInvoiceItem> getInvoiceItems() {
        return this.invoiceItems;
    }

    public final String getSubscriptionPeriodEnd() {
        return this.subscriptionPeriodEnd;
    }

    public final String getSubscriptionPeriodStart() {
        return this.subscriptionPeriodStart;
    }

    public final int getSubtotal() {
        return this.subtotal;
    }

    public final int getTax() {
        return this.tax;
    }

    public final boolean getTaxInclusive() {
        return this.taxInclusive;
    }

    public final int getTotal() {
        return this.total;
    }

    public int hashCode() {
        String str = this.currency;
        int i = 0;
        int hashCode = str != null ? str.hashCode() : 0;
        long j = this.f2713id;
        int i2 = ((hashCode * 31) + ((int) (j ^ (j >>> 32)))) * 31;
        List<ModelInvoiceItem> list = this.invoiceItems;
        int hashCode2 = (i2 + (list != null ? list.hashCode() : 0)) * 31;
        String str2 = this.subscriptionPeriodEnd;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.subscriptionPeriodStart;
        if (str3 != null) {
            i = str3.hashCode();
        }
        int i3 = (((((hashCode3 + i) * 31) + this.subtotal) * 31) + this.tax) * 31;
        boolean z2 = this.taxInclusive;
        if (z2) {
            z2 = true;
        }
        int i4 = z2 ? 1 : 0;
        int i5 = z2 ? 1 : 0;
        return ((i3 + i4) * 31) + this.total;
    }

    public String toString() {
        StringBuilder R = a.R("ModelInvoicePreview(currency=");
        R.append(this.currency);
        R.append(", id=");
        R.append(this.f2713id);
        R.append(", invoiceItems=");
        R.append(this.invoiceItems);
        R.append(", subscriptionPeriodEnd=");
        R.append(this.subscriptionPeriodEnd);
        R.append(", subscriptionPeriodStart=");
        R.append(this.subscriptionPeriodStart);
        R.append(", subtotal=");
        R.append(this.subtotal);
        R.append(", tax=");
        R.append(this.tax);
        R.append(", taxInclusive=");
        R.append(this.taxInclusive);
        R.append(", total=");
        return a.A(R, this.total, ")");
    }
}
