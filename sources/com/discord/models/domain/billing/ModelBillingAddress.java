package com.discord.models.domain.billing;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ModelBillingAddress.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0012\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B?\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\r\u001a\u00020\u0002\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\u0006\u0010\u0010\u001a\u00020\u0002\u0012\u0006\u0010\u0011\u001a\u00020\u0002¢\u0006\u0004\b$\u0010%J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0004J\u0010\u0010\b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\t\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\u0004J\u0010\u0010\n\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\n\u0010\u0004JV\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\u00022\b\b\u0002\u0010\r\u001a\u00020\u00022\b\b\u0002\u0010\u000e\u001a\u00020\u00022\b\b\u0002\u0010\u000f\u001a\u00020\u00022\b\b\u0002\u0010\u0010\u001a\u00020\u00022\b\b\u0002\u0010\u0011\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0004J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u001a\u0010\u001a\u001a\u00020\u00192\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001a\u0010\u001bR\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001c\u001a\u0004\b\u001d\u0010\u0004R\u0019\u0010\u0010\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001c\u001a\u0004\b\u001e\u0010\u0004R\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001c\u001a\u0004\b\u001f\u0010\u0004R\u0019\u0010\r\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001c\u001a\u0004\b \u0010\u0004R\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001c\u001a\u0004\b!\u0010\u0004R\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001c\u001a\u0004\b\"\u0010\u0004R\u0019\u0010\u0011\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001c\u001a\u0004\b#\u0010\u0004¨\u0006&"}, d2 = {"Lcom/discord/models/domain/billing/ModelBillingAddress;", "", "", "component1", "()Ljava/lang/String;", "component2", "component3", "component4", "component5", "component6", "component7", ModelAuditLogEntry.CHANGE_KEY_NAME, "line_1", "line_2", "city", "state", "country", "postalCode", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/models/domain/billing/ModelBillingAddress;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getState", "getCountry", "getLine_1", "getLine_2", "getCity", "getName", "getPostalCode", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelBillingAddress {
    private final String city;
    private final String country;
    private final String line_1;
    private final String line_2;
    private final String name;
    private final String postalCode;
    private final String state;

    public ModelBillingAddress(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(str2, "line_1");
        m.checkNotNullParameter(str3, "line_2");
        m.checkNotNullParameter(str4, "city");
        m.checkNotNullParameter(str5, "state");
        m.checkNotNullParameter(str6, "country");
        m.checkNotNullParameter(str7, "postalCode");
        this.name = str;
        this.line_1 = str2;
        this.line_2 = str3;
        this.city = str4;
        this.state = str5;
        this.country = str6;
        this.postalCode = str7;
    }

    public static /* synthetic */ ModelBillingAddress copy$default(ModelBillingAddress modelBillingAddress, String str, String str2, String str3, String str4, String str5, String str6, String str7, int i, Object obj) {
        if ((i & 1) != 0) {
            str = modelBillingAddress.name;
        }
        if ((i & 2) != 0) {
            str2 = modelBillingAddress.line_1;
        }
        String str8 = str2;
        if ((i & 4) != 0) {
            str3 = modelBillingAddress.line_2;
        }
        String str9 = str3;
        if ((i & 8) != 0) {
            str4 = modelBillingAddress.city;
        }
        String str10 = str4;
        if ((i & 16) != 0) {
            str5 = modelBillingAddress.state;
        }
        String str11 = str5;
        if ((i & 32) != 0) {
            str6 = modelBillingAddress.country;
        }
        String str12 = str6;
        if ((i & 64) != 0) {
            str7 = modelBillingAddress.postalCode;
        }
        return modelBillingAddress.copy(str, str8, str9, str10, str11, str12, str7);
    }

    public final String component1() {
        return this.name;
    }

    public final String component2() {
        return this.line_1;
    }

    public final String component3() {
        return this.line_2;
    }

    public final String component4() {
        return this.city;
    }

    public final String component5() {
        return this.state;
    }

    public final String component6() {
        return this.country;
    }

    public final String component7() {
        return this.postalCode;
    }

    public final ModelBillingAddress copy(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(str2, "line_1");
        m.checkNotNullParameter(str3, "line_2");
        m.checkNotNullParameter(str4, "city");
        m.checkNotNullParameter(str5, "state");
        m.checkNotNullParameter(str6, "country");
        m.checkNotNullParameter(str7, "postalCode");
        return new ModelBillingAddress(str, str2, str3, str4, str5, str6, str7);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModelBillingAddress)) {
            return false;
        }
        ModelBillingAddress modelBillingAddress = (ModelBillingAddress) obj;
        return m.areEqual(this.name, modelBillingAddress.name) && m.areEqual(this.line_1, modelBillingAddress.line_1) && m.areEqual(this.line_2, modelBillingAddress.line_2) && m.areEqual(this.city, modelBillingAddress.city) && m.areEqual(this.state, modelBillingAddress.state) && m.areEqual(this.country, modelBillingAddress.country) && m.areEqual(this.postalCode, modelBillingAddress.postalCode);
    }

    public final String getCity() {
        return this.city;
    }

    public final String getCountry() {
        return this.country;
    }

    public final String getLine_1() {
        return this.line_1;
    }

    public final String getLine_2() {
        return this.line_2;
    }

    public final String getName() {
        return this.name;
    }

    public final String getPostalCode() {
        return this.postalCode;
    }

    public final String getState() {
        return this.state;
    }

    public int hashCode() {
        String str = this.name;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.line_1;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.line_2;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.city;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.state;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.country;
        int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.postalCode;
        if (str7 != null) {
            i = str7.hashCode();
        }
        return hashCode6 + i;
    }

    public String toString() {
        StringBuilder R = a.R("ModelBillingAddress(name=");
        R.append(this.name);
        R.append(", line_1=");
        R.append(this.line_1);
        R.append(", line_2=");
        R.append(this.line_2);
        R.append(", city=");
        R.append(this.city);
        R.append(", state=");
        R.append(this.state);
        R.append(", country=");
        R.append(this.country);
        R.append(", postalCode=");
        return a.H(R, this.postalCode, ")");
    }
}
