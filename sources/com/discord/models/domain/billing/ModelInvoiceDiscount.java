package com.discord.models.domain.billing;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import kotlin.Metadata;
/* compiled from: ModelInvoicePreview.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J$\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u00022\b\b\u0002\u0010\u0007\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\r\u0010\u0004J\u001a\u0010\u0010\u001a\u00020\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0006\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0012\u001a\u0004\b\u0013\u0010\u0004R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0012\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/models/domain/billing/ModelInvoiceDiscount;", "", "", "component1", "()I", "component2", "amount", "type", "copy", "(II)Lcom/discord/models/domain/billing/ModelInvoiceDiscount;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getAmount", "getType", HookHelper.constructorName, "(II)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelInvoiceDiscount {
    private final int amount;
    private final int type;

    public ModelInvoiceDiscount(int i, int i2) {
        this.amount = i;
        this.type = i2;
    }

    public static /* synthetic */ ModelInvoiceDiscount copy$default(ModelInvoiceDiscount modelInvoiceDiscount, int i, int i2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            i = modelInvoiceDiscount.amount;
        }
        if ((i3 & 2) != 0) {
            i2 = modelInvoiceDiscount.type;
        }
        return modelInvoiceDiscount.copy(i, i2);
    }

    public final int component1() {
        return this.amount;
    }

    public final int component2() {
        return this.type;
    }

    public final ModelInvoiceDiscount copy(int i, int i2) {
        return new ModelInvoiceDiscount(i, i2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModelInvoiceDiscount)) {
            return false;
        }
        ModelInvoiceDiscount modelInvoiceDiscount = (ModelInvoiceDiscount) obj;
        return this.amount == modelInvoiceDiscount.amount && this.type == modelInvoiceDiscount.type;
    }

    public final int getAmount() {
        return this.amount;
    }

    public final int getType() {
        return this.type;
    }

    public int hashCode() {
        return (this.amount * 31) + this.type;
    }

    public String toString() {
        StringBuilder R = a.R("ModelInvoiceDiscount(amount=");
        R.append(this.amount);
        R.append(", type=");
        return a.A(R, this.type, ")");
    }
}
