package com.discord.models.domain;

import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.models.domain.Model;
import com.discord.models.domain.ModelNotificationSettings;
import com.discord.utilities.time.Clock;
import com.discord.utilities.time.ClockFactory;
import com.google.gson.stream.JsonToken;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/* loaded from: classes.dex */
public class ModelNotificationSettings implements Model {
    public static int FREQUENCY_ALL = 0;
    public static int FREQUENCY_MENTIONS = 1;
    public static int FREQUENCY_NOTHING = 2;
    public static int FREQUENCY_UNSET = 3;
    private List<ChannelOverride> channelOverrides;
    private transient boolean derivedMute;
    private long guildId;
    private int messageNotifications;
    private boolean mobilePush;
    private ModelMuteConfig muteConfig;
    private boolean muted;
    private boolean suppressEveryone;
    private boolean suppressRoles;
    private int version;

    public ModelNotificationSettings() {
        this.suppressEveryone = false;
        this.suppressRoles = false;
        this.muted = false;
        this.muteConfig = null;
        this.mobilePush = true;
        this.messageNotifications = FREQUENCY_UNSET;
        this.guildId = 0L;
        this.channelOverrides = new ArrayList();
        this.version = 0;
    }

    @Override // com.discord.models.domain.Model
    public void assignField(final Model.JsonReader jsonReader) throws IOException {
        String nextName = jsonReader.nextName();
        nextName.hashCode();
        char c = 65535;
        switch (nextName.hashCode()) {
            case -2038869976:
                if (nextName.equals("mute_config")) {
                    c = 0;
                    break;
                }
                break;
            case -1313870697:
                if (nextName.equals("mobile_push")) {
                    c = 1;
                    break;
                }
                break;
            case -1306538777:
                if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_GUILD_ID)) {
                    c = 2;
                    break;
                }
                break;
            case -1093751317:
                if (nextName.equals("channel_overrides")) {
                    c = 3;
                    break;
                }
                break;
            case 104264043:
                if (nextName.equals("muted")) {
                    c = 4;
                    break;
                }
                break;
            case 351608024:
                if (nextName.equals("version")) {
                    c = 5;
                    break;
                }
                break;
            case 1406095024:
                if (nextName.equals("message_notifications")) {
                    c = 6;
                    break;
                }
                break;
            case 1479081139:
                if (nextName.equals("suppress_roles")) {
                    c = 7;
                    break;
                }
                break;
            case 1784473525:
                if (nextName.equals("suppress_everyone")) {
                    c = '\b';
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                if (jsonReader.peek() == JsonToken.NULL) {
                    jsonReader.nextNull();
                    return;
                } else {
                    this.muteConfig = ModelMuteConfig.Parser.parse(jsonReader);
                    return;
                }
            case 1:
                this.mobilePush = jsonReader.nextBoolean(this.mobilePush);
                return;
            case 2:
                this.guildId = jsonReader.nextLong(this.guildId);
                return;
            case 3:
                this.channelOverrides = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.k
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        return (ModelNotificationSettings.ChannelOverride) Model.JsonReader.this.parse(new ModelNotificationSettings.ChannelOverride());
                    }
                });
                return;
            case 4:
                this.muted = jsonReader.nextBoolean(this.muted);
                return;
            case 5:
                this.version = jsonReader.nextInt(this.version);
                return;
            case 6:
                this.messageNotifications = jsonReader.nextInt(this.messageNotifications);
                return;
            case 7:
                this.suppressRoles = jsonReader.nextBoolean(this.suppressRoles);
                return;
            case '\b':
                this.suppressEveryone = jsonReader.nextBoolean(this.suppressEveryone);
                return;
            default:
                jsonReader.skipValue();
                return;
        }
    }

    public boolean canEqual(Object obj) {
        return obj instanceof ModelNotificationSettings;
    }

    public ModelNotificationSettings copyAndRecomputeTransientFields(Clock clock) {
        boolean isMuted = NotificationSettingsModelUtils.isMuted(this.muted, this.muteConfig, clock);
        ArrayList arrayList = new ArrayList();
        for (ChannelOverride channelOverride : this.channelOverrides) {
            arrayList.add(channelOverride.copyAndRecomputeTransientFields(clock));
        }
        return new ModelNotificationSettings(this.suppressEveryone, this.suppressRoles, this.muted, this.muteConfig, isMuted, this.mobilePush, this.messageNotifications, this.guildId, arrayList, this.version);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ModelNotificationSettings)) {
            return false;
        }
        ModelNotificationSettings modelNotificationSettings = (ModelNotificationSettings) obj;
        if (!modelNotificationSettings.canEqual(this) || isSuppressEveryone() != modelNotificationSettings.isSuppressEveryone() || isSuppressRoles() != modelNotificationSettings.isSuppressRoles() || isMuted() != modelNotificationSettings.isMuted() || this.derivedMute != modelNotificationSettings.derivedMute || isMobilePush() != modelNotificationSettings.isMobilePush() || getMessageNotifications() != modelNotificationSettings.getMessageNotifications() || getGuildId() != modelNotificationSettings.getGuildId() || getVersion() != modelNotificationSettings.getVersion()) {
            return false;
        }
        ModelMuteConfig muteConfig = getMuteConfig();
        ModelMuteConfig muteConfig2 = modelNotificationSettings.getMuteConfig();
        if (muteConfig != null ? !muteConfig.equals(muteConfig2) : muteConfig2 != null) {
            return false;
        }
        List<ChannelOverride> channelOverrides = getChannelOverrides();
        List<ChannelOverride> channelOverrides2 = modelNotificationSettings.getChannelOverrides();
        return channelOverrides != null ? channelOverrides.equals(channelOverrides2) : channelOverrides2 == null;
    }

    public ChannelOverride getChannelOverride(long j) {
        List<ChannelOverride> list = this.channelOverrides;
        if (list == null) {
            return null;
        }
        for (ChannelOverride channelOverride : list) {
            if (channelOverride.getChannelId() == j) {
                return channelOverride;
            }
        }
        return null;
    }

    public List<ChannelOverride> getChannelOverrides() {
        return this.channelOverrides;
    }

    public long getGuildId() {
        return this.guildId;
    }

    public int getMessageNotifications() {
        return this.messageNotifications;
    }

    public ModelMuteConfig getMuteConfig() {
        return this.muteConfig;
    }

    public String getMuteEndTime() {
        ModelMuteConfig modelMuteConfig = this.muteConfig;
        if (modelMuteConfig != null) {
            return modelMuteConfig.getEndTime();
        }
        return null;
    }

    public long getNextMuteEndTimeMs(Clock clock) {
        Long endTimeMs;
        long currentTimeMillis = clock.currentTimeMillis();
        ModelMuteConfig modelMuteConfig = this.muteConfig;
        long longValue = (modelMuteConfig == null || (endTimeMs = modelMuteConfig.getEndTimeMs()) == null || endTimeMs.longValue() <= currentTimeMillis) ? RecyclerView.FOREVER_NS : endTimeMs.longValue();
        for (ChannelOverride channelOverride : this.channelOverrides) {
            ModelMuteConfig modelMuteConfig2 = channelOverride.muteConfig;
            Long endTimeMs2 = modelMuteConfig2 != null ? modelMuteConfig2.getEndTimeMs() : Long.MIN_VALUE;
            if (endTimeMs2 != null && endTimeMs2.longValue() > currentTimeMillis) {
                longValue = Math.min(longValue, endTimeMs2.longValue());
            }
        }
        return longValue;
    }

    public int getVersion() {
        return this.version;
    }

    public int hashCode() {
        int i = 79;
        int i2 = ((((((((isSuppressEveryone() ? 79 : 97) + 59) * 59) + (isSuppressRoles() ? 79 : 97)) * 59) + (isMuted() ? 79 : 97)) * 59) + (this.derivedMute ? 79 : 97)) * 59;
        if (!isMobilePush()) {
            i = 97;
        }
        int messageNotifications = getMessageNotifications();
        long guildId = getGuildId();
        int version = getVersion() + ((((messageNotifications + ((i2 + i) * 59)) * 59) + ((int) (guildId ^ (guildId >>> 32)))) * 59);
        ModelMuteConfig muteConfig = getMuteConfig();
        int i3 = 43;
        int hashCode = (version * 59) + (muteConfig == null ? 43 : muteConfig.hashCode());
        List<ChannelOverride> channelOverrides = getChannelOverrides();
        int i4 = hashCode * 59;
        if (channelOverrides != null) {
            i3 = channelOverrides.hashCode();
        }
        return i4 + i3;
    }

    public boolean isMobilePush() {
        return this.mobilePush;
    }

    public boolean isMuted() {
        return this.derivedMute;
    }

    public boolean isSuppressEveryone() {
        return this.suppressEveryone;
    }

    public boolean isSuppressRoles() {
        return this.suppressRoles;
    }

    public String toString() {
        StringBuilder R = a.R("ModelNotificationSettings(suppressEveryone=");
        R.append(isSuppressEveryone());
        R.append(", suppressRoles=");
        R.append(isSuppressRoles());
        R.append(", muted=");
        R.append(isMuted());
        R.append(", muteConfig=");
        R.append(getMuteConfig());
        R.append(", derivedMute=");
        R.append(this.derivedMute);
        R.append(", mobilePush=");
        R.append(isMobilePush());
        R.append(", messageNotifications=");
        R.append(getMessageNotifications());
        R.append(", guildId=");
        R.append(getGuildId());
        R.append(", channelOverrides=");
        R.append(getChannelOverrides());
        R.append(", version=");
        R.append(getVersion());
        R.append(")");
        return R.toString();
    }

    /* loaded from: classes.dex */
    public static class ChannelOverride implements Model {
        private long channelId;
        private transient boolean derivedMute;
        private int messageNotifications;
        private ModelMuteConfig muteConfig;
        private boolean muted;

        public ChannelOverride() {
            this.muted = false;
            this.messageNotifications = ModelNotificationSettings.FREQUENCY_UNSET;
            this.channelId = 0L;
        }

        @Override // com.discord.models.domain.Model
        public void assignField(Model.JsonReader jsonReader) throws IOException {
            String nextName = jsonReader.nextName();
            nextName.hashCode();
            char c = 65535;
            switch (nextName.hashCode()) {
                case -2038869976:
                    if (nextName.equals("mute_config")) {
                        c = 0;
                        break;
                    }
                    break;
                case -1930808873:
                    if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_CHANNEL_ID)) {
                        c = 1;
                        break;
                    }
                    break;
                case 104264043:
                    if (nextName.equals("muted")) {
                        c = 2;
                        break;
                    }
                    break;
                case 1406095024:
                    if (nextName.equals("message_notifications")) {
                        c = 3;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    if (jsonReader.peek() != JsonToken.NULL) {
                        this.muteConfig = ModelMuteConfig.Parser.parse(jsonReader);
                        break;
                    } else {
                        jsonReader.nextNull();
                        break;
                    }
                case 1:
                    this.channelId = jsonReader.nextLong(this.channelId);
                    break;
                case 2:
                    this.muted = jsonReader.nextBoolean(this.muted);
                    break;
                case 3:
                    this.messageNotifications = jsonReader.nextInt(this.messageNotifications);
                    break;
                default:
                    jsonReader.skipValue();
                    break;
            }
            this.derivedMute = NotificationSettingsModelUtils.isMuted(this.muted, this.muteConfig, ClockFactory.get());
        }

        public boolean canEqual(Object obj) {
            return obj instanceof ChannelOverride;
        }

        public ChannelOverride copyAndRecomputeTransientFields(Clock clock) {
            return new ChannelOverride(this.muted, this.muteConfig, NotificationSettingsModelUtils.isMuted(this.muted, this.muteConfig, clock), this.messageNotifications, this.channelId);
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof ChannelOverride)) {
                return false;
            }
            ChannelOverride channelOverride = (ChannelOverride) obj;
            if (!channelOverride.canEqual(this) || isMuted() != channelOverride.isMuted() || this.derivedMute != channelOverride.derivedMute || getMessageNotifications() != channelOverride.getMessageNotifications() || getChannelId() != channelOverride.getChannelId()) {
                return false;
            }
            ModelMuteConfig muteConfig = getMuteConfig();
            ModelMuteConfig muteConfig2 = channelOverride.getMuteConfig();
            return muteConfig != null ? muteConfig.equals(muteConfig2) : muteConfig2 == null;
        }

        public long getChannelId() {
            return this.channelId;
        }

        public int getMessageNotifications() {
            return this.messageNotifications;
        }

        public ModelMuteConfig getMuteConfig() {
            return this.muteConfig;
        }

        public String getMuteEndTime() {
            ModelMuteConfig modelMuteConfig = this.muteConfig;
            if (modelMuteConfig != null) {
                return modelMuteConfig.getEndTime();
            }
            return null;
        }

        public int hashCode() {
            int i = 79;
            int i2 = ((isMuted() ? 79 : 97) + 59) * 59;
            if (!this.derivedMute) {
                i = 97;
            }
            int messageNotifications = getMessageNotifications();
            long channelId = getChannelId();
            int i3 = ((messageNotifications + ((i2 + i) * 59)) * 59) + ((int) (channelId ^ (channelId >>> 32)));
            ModelMuteConfig muteConfig = getMuteConfig();
            return (i3 * 59) + (muteConfig == null ? 43 : muteConfig.hashCode());
        }

        public boolean isMuted() {
            return this.derivedMute;
        }

        public String toString() {
            StringBuilder R = a.R("ModelNotificationSettings.ChannelOverride(muted=");
            R.append(isMuted());
            R.append(", muteConfig=");
            R.append(getMuteConfig());
            R.append(", derivedMute=");
            R.append(this.derivedMute);
            R.append(", messageNotifications=");
            R.append(getMessageNotifications());
            R.append(", channelId=");
            R.append(getChannelId());
            R.append(")");
            return R.toString();
        }

        public ChannelOverride(boolean z2, ModelMuteConfig modelMuteConfig, boolean z3, int i, long j) {
            this.muted = z2;
            this.muteConfig = modelMuteConfig;
            this.derivedMute = z3;
            this.messageNotifications = i;
            this.channelId = j;
        }
    }

    public ModelNotificationSettings(boolean z2, boolean z3, boolean z4, ModelMuteConfig modelMuteConfig, boolean z5, boolean z6, int i, long j, List<ChannelOverride> list, int i2) {
        this.suppressEveryone = z2;
        this.suppressRoles = z3;
        this.muted = z4;
        this.muteConfig = modelMuteConfig;
        this.derivedMute = z5;
        this.mobilePush = z6;
        this.messageNotifications = i;
        this.guildId = j;
        this.channelOverrides = list;
        this.version = i2;
    }
}
