package com.discord.models.domain;

import b.d.b.a.a;
import com.discord.models.domain.Model;
import java.io.IOException;
/* loaded from: classes.dex */
public class ModelVoiceRegion implements Model {
    private boolean deprecated;
    private boolean hidden;

    /* renamed from: id  reason: collision with root package name */
    private String f2709id;
    private String name;
    private boolean optimal;
    private String sampleHostname;
    private int samplePort;

    @Override // com.discord.models.domain.Model
    public void assignField(Model.JsonReader jsonReader) throws IOException {
        String nextName = jsonReader.nextName();
        nextName.hashCode();
        char c = 65535;
        switch (nextName.hashCode()) {
            case -1632344653:
                if (nextName.equals("deprecated")) {
                    c = 0;
                    break;
                }
                break;
            case -1249477246:
                if (nextName.equals("optimal")) {
                    c = 1;
                    break;
                }
                break;
            case -1217487446:
                if (nextName.equals("hidden")) {
                    c = 2;
                    break;
                }
                break;
            case 3355:
                if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_ID)) {
                    c = 3;
                    break;
                }
                break;
            case 3373707:
                if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_NAME)) {
                    c = 4;
                    break;
                }
                break;
            case 51894056:
                if (nextName.equals("sample_hostname")) {
                    c = 5;
                    break;
                }
                break;
            case 153146870:
                if (nextName.equals("sample_port")) {
                    c = 6;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                this.deprecated = jsonReader.nextBoolean(this.deprecated);
                return;
            case 1:
                this.optimal = jsonReader.nextBoolean(this.optimal);
                return;
            case 2:
                this.hidden = jsonReader.nextBoolean(this.hidden);
                return;
            case 3:
                this.f2709id = jsonReader.nextString(this.f2709id);
                return;
            case 4:
                this.name = jsonReader.nextString(this.name);
                return;
            case 5:
                this.sampleHostname = jsonReader.nextString(this.sampleHostname);
                return;
            case 6:
                this.samplePort = jsonReader.nextInt(this.samplePort);
                return;
            default:
                jsonReader.skipValue();
                return;
        }
    }

    public boolean canEqual(Object obj) {
        return obj instanceof ModelVoiceRegion;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ModelVoiceRegion)) {
            return false;
        }
        ModelVoiceRegion modelVoiceRegion = (ModelVoiceRegion) obj;
        if (!modelVoiceRegion.canEqual(this) || isOptimal() != modelVoiceRegion.isOptimal() || getSamplePort() != modelVoiceRegion.getSamplePort() || isDeprecated() != modelVoiceRegion.isDeprecated() || isHidden() != modelVoiceRegion.isHidden()) {
            return false;
        }
        String id2 = getId();
        String id3 = modelVoiceRegion.getId();
        if (id2 != null ? !id2.equals(id3) : id3 != null) {
            return false;
        }
        String name = getName();
        String name2 = modelVoiceRegion.getName();
        if (name != null ? !name.equals(name2) : name2 != null) {
            return false;
        }
        String sampleHostname = getSampleHostname();
        String sampleHostname2 = modelVoiceRegion.getSampleHostname();
        return sampleHostname != null ? sampleHostname.equals(sampleHostname2) : sampleHostname2 == null;
    }

    public String getId() {
        return this.f2709id;
    }

    public String getName() {
        return this.name;
    }

    public String getSampleHostname() {
        return this.sampleHostname;
    }

    public int getSamplePort() {
        return this.samplePort;
    }

    public int hashCode() {
        int i = 79;
        int samplePort = (((getSamplePort() + (((isOptimal() ? 79 : 97) + 59) * 59)) * 59) + (isDeprecated() ? 79 : 97)) * 59;
        if (!isHidden()) {
            i = 97;
        }
        String id2 = getId();
        int i2 = (samplePort + i) * 59;
        int i3 = 43;
        int hashCode = i2 + (id2 == null ? 43 : id2.hashCode());
        String name = getName();
        int hashCode2 = (hashCode * 59) + (name == null ? 43 : name.hashCode());
        String sampleHostname = getSampleHostname();
        int i4 = hashCode2 * 59;
        if (sampleHostname != null) {
            i3 = sampleHostname.hashCode();
        }
        return i4 + i3;
    }

    public boolean isDeprecated() {
        return this.deprecated;
    }

    public boolean isHidden() {
        return this.hidden;
    }

    public boolean isOptimal() {
        return this.optimal;
    }

    public String toString() {
        StringBuilder R = a.R("ModelVoiceRegion(id=");
        R.append(getId());
        R.append(", name=");
        R.append(getName());
        R.append(", optimal=");
        R.append(isOptimal());
        R.append(", samplePort=");
        R.append(getSamplePort());
        R.append(", sampleHostname=");
        R.append(getSampleHostname());
        R.append(", deprecated=");
        R.append(isDeprecated());
        R.append(", hidden=");
        R.append(isHidden());
        R.append(")");
        return R.toString();
    }
}
