package com.discord.models.domain;

import androidx.annotation.Nullable;
import b.a.m.a.f0;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.connectedaccounts.ConnectedAccount;
import com.discord.api.guild.Guild;
import com.discord.api.guildjoinrequest.GuildJoinRequest;
import com.discord.api.guildmember.GuildMember;
import com.discord.api.presence.Presence;
import com.discord.api.user.User;
import com.discord.api.user.UserSurvey;
import com.discord.models.deserialization.gson.InboundGatewayGsonParser;
import com.discord.models.domain.Model;
import com.discord.models.domain.ModelPayload;
import com.discord.models.domain.ModelReadState;
import com.discord.models.domain.ModelSession;
import com.discord.models.domain.ModelUserRelationship;
import com.discord.models.experiments.dto.GuildExperimentDto;
import com.discord.models.experiments.dto.UserExperimentDto;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.List;
import java.util.Map;
/* loaded from: classes.dex */
public class ModelPayload implements Model {
    private String analyticsToken;
    private List<ConnectedAccount> connectedAccounts;
    private String countryCode;
    private Map<Long, UserExperimentDto> experiments;
    private int friendSuggestionCount;
    @Nullable
    private List<GuildExperimentDto> guildExperiments;
    private List<GuildJoinRequest> guildJoinRequests;
    private List<List<GuildMember>> guildMembers;
    private List<List<Presence>> guildPresences;
    private List<Guild> guilds;

    /* renamed from: me  reason: collision with root package name */
    private User f2699me;
    private List<Presence> presences;
    private List<Channel> privateChannels;
    private VersionedReadStates readState;
    private List<ModelUserRelationship> relationships;
    private String requiredAction;
    private String sessionId;
    private List<ModelSession> sessions;
    private Object trace;
    private VersionedUserGuildSettings userGuildSettings;
    private ModelUserSettings userSettings;
    private UserSurvey userSurvey;
    private List<User> users;
    private int v;

    /* loaded from: classes.dex */
    public static class Hello implements Model {
        private int heartbeatInterval;
        private List<String> trace;

        @Override // com.discord.models.domain.Model
        public void assignField(final Model.JsonReader jsonReader) throws IOException {
            String nextName = jsonReader.nextName();
            nextName.hashCode();
            if (nextName.equals("_trace")) {
                this.trace = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.o
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        return Model.JsonReader.this.nextString(null);
                    }
                });
            } else if (!nextName.equals("heartbeat_interval")) {
                jsonReader.skipValue();
            } else {
                this.heartbeatInterval = jsonReader.nextInt(this.heartbeatInterval);
            }
        }

        public boolean canEqual(Object obj) {
            return obj instanceof Hello;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Hello)) {
                return false;
            }
            Hello hello = (Hello) obj;
            if (!hello.canEqual(this) || getHeartbeatInterval() != hello.getHeartbeatInterval()) {
                return false;
            }
            List<String> trace = getTrace();
            List<String> trace2 = hello.getTrace();
            return trace != null ? trace.equals(trace2) : trace2 == null;
        }

        public int getHeartbeatInterval() {
            return this.heartbeatInterval;
        }

        public List<String> getTrace() {
            return this.trace;
        }

        public int hashCode() {
            List<String> trace = getTrace();
            return ((getHeartbeatInterval() + 59) * 59) + (trace == null ? 43 : trace.hashCode());
        }

        public String toString() {
            StringBuilder R = a.R("ModelPayload.Hello(heartbeatInterval=");
            R.append(getHeartbeatInterval());
            R.append(", trace=");
            R.append(getTrace());
            R.append(")");
            return R.toString();
        }
    }

    /* loaded from: classes.dex */
    public static class MergedPresences implements Model {
        private List<Presence> friends;
        private List<List<Presence>> guilds;

        @Override // com.discord.models.domain.Model
        public void assignField(final Model.JsonReader jsonReader) throws IOException {
            String nextName = jsonReader.nextName();
            nextName.hashCode();
            if (nextName.equals("guilds")) {
                this.guilds = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.r
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        final Model.JsonReader jsonReader2 = Model.JsonReader.this;
                        return jsonReader2.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.q
                            @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                            public final Object get() {
                                return (Presence) InboundGatewayGsonParser.fromJson(Model.JsonReader.this, Presence.class);
                            }
                        });
                    }
                });
            } else if (!nextName.equals("friends")) {
                jsonReader.skipValue();
            } else {
                this.friends = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.p
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        return (Presence) InboundGatewayGsonParser.fromJson(Model.JsonReader.this, Presence.class);
                    }
                });
            }
        }

        public boolean canEqual(Object obj) {
            return obj instanceof MergedPresences;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof MergedPresences)) {
                return false;
            }
            MergedPresences mergedPresences = (MergedPresences) obj;
            if (!mergedPresences.canEqual(this)) {
                return false;
            }
            List<Presence> friends = getFriends();
            List<Presence> friends2 = mergedPresences.getFriends();
            if (friends != null ? !friends.equals(friends2) : friends2 != null) {
                return false;
            }
            List<List<Presence>> guilds = getGuilds();
            List<List<Presence>> guilds2 = mergedPresences.getGuilds();
            return guilds != null ? guilds.equals(guilds2) : guilds2 == null;
        }

        public List<Presence> getFriends() {
            return this.friends;
        }

        public List<List<Presence>> getGuilds() {
            return this.guilds;
        }

        public int hashCode() {
            List<Presence> friends = getFriends();
            int i = 43;
            int hashCode = friends == null ? 43 : friends.hashCode();
            List<List<Presence>> guilds = getGuilds();
            int i2 = (hashCode + 59) * 59;
            if (guilds != null) {
                i = guilds.hashCode();
            }
            return i2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("ModelPayload.MergedPresences(friends=");
            R.append(getFriends());
            R.append(", guilds=");
            R.append(getGuilds());
            R.append(")");
            return R.toString();
        }
    }

    /* loaded from: classes.dex */
    public static abstract class VersionedModel<T> implements Model {
        public static final int DEFAULT_VERSION = -1;
        private List<T> entries;
        private boolean partial;
        private int version;

        /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
        @Override // com.discord.models.domain.Model
        public void assignField(final Model.JsonReader jsonReader) throws IOException {
            char c;
            String nextName = jsonReader.nextName();
            nextName.hashCode();
            switch (nextName.hashCode()) {
                case -1591573360:
                    if (nextName.equals("entries")) {
                        c = 0;
                        break;
                    }
                    c = 65535;
                    break;
                case -792934015:
                    if (nextName.equals("partial")) {
                        c = 1;
                        break;
                    }
                    c = 65535;
                    break;
                case 351608024:
                    if (nextName.equals("version")) {
                        c = 2;
                        break;
                    }
                    c = 65535;
                    break;
                default:
                    c = 65535;
                    break;
            }
            switch (c) {
                case 0:
                    this.entries = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.t
                        @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                        public final Object get() {
                            return ModelPayload.VersionedModel.this.parseEntry(jsonReader);
                        }
                    });
                    return;
                case 1:
                    this.partial = jsonReader.nextBoolean(this.partial);
                    return;
                case 2:
                    this.version = jsonReader.nextInt(-1);
                    return;
                default:
                    jsonReader.skipValue();
                    return;
            }
        }

        public boolean canEqual(Object obj) {
            return obj instanceof VersionedModel;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof VersionedModel)) {
                return false;
            }
            VersionedModel versionedModel = (VersionedModel) obj;
            if (!versionedModel.canEqual(this) || getVersion() != versionedModel.getVersion() || isPartial() != versionedModel.isPartial()) {
                return false;
            }
            List<T> entries = getEntries();
            List<T> entries2 = versionedModel.getEntries();
            return entries != null ? entries.equals(entries2) : entries2 == null;
        }

        public List<T> getEntries() {
            return this.entries;
        }

        public int getVersion() {
            return this.version;
        }

        public int hashCode() {
            int version = ((getVersion() + 59) * 59) + (isPartial() ? 79 : 97);
            List<T> entries = getEntries();
            return (version * 59) + (entries == null ? 43 : entries.hashCode());
        }

        public boolean isPartial() {
            return this.partial;
        }

        public abstract T parseEntry(Model.JsonReader jsonReader) throws IOException;

        public String toString() {
            StringBuilder R = a.R("ModelPayload.VersionedModel(version=");
            R.append(getVersion());
            R.append(", entries=");
            R.append(getEntries());
            R.append(", partial=");
            R.append(isPartial());
            R.append(")");
            return R.toString();
        }
    }

    /* loaded from: classes.dex */
    public static class VersionedReadStates extends VersionedModel<ModelReadState> {
        /* JADX WARN: Can't rename method to resolve collision */
        @Override // com.discord.models.domain.ModelPayload.VersionedModel
        public ModelReadState parseEntry(Model.JsonReader jsonReader) {
            return ModelReadState.Parser.INSTANCE.parse(jsonReader);
        }
    }

    /* loaded from: classes.dex */
    public static class VersionedUserGuildSettings extends VersionedModel<ModelNotificationSettings> {
        /* JADX WARN: Can't rename method to resolve collision */
        @Override // com.discord.models.domain.ModelPayload.VersionedModel
        public ModelNotificationSettings parseEntry(Model.JsonReader jsonReader) throws IOException {
            return (ModelNotificationSettings) jsonReader.parse(new ModelNotificationSettings());
        }
    }

    public ModelPayload() {
    }

    private Object getTraces(final Model.JsonReader jsonReader) throws IOException {
        jsonReader.getClass();
        List nextList = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.g0
            @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
            public final Object get() {
                return Model.JsonReader.this.nextStringOrNull();
            }
        });
        if (nextList.size() <= 0 || nextList.get(0) == null) {
            return null;
        }
        return new Gson().f((String) nextList.get(0), Object.class);
    }

    @Override // com.discord.models.domain.Model
    public void assignField(final Model.JsonReader jsonReader) throws IOException {
        String nextName = jsonReader.nextName();
        nextName.hashCode();
        char c = 65535;
        switch (nextName.hashCode()) {
            case -1931770397:
                if (nextName.equals("user_guild_settings")) {
                    c = 0;
                    break;
                }
                break;
            case -1464576954:
                if (nextName.equals("_trace")) {
                    c = 1;
                    break;
                }
                break;
            case -1424791802:
                if (nextName.equals("merged_members")) {
                    c = 2;
                    break;
                }
                break;
            case -1394550395:
                if (nextName.equals("merged_presences")) {
                    c = 3;
                    break;
                }
                break;
            case -1234877728:
                if (nextName.equals("guilds")) {
                    c = 4;
                    break;
                }
                break;
            case -921959720:
                if (nextName.equals("presences")) {
                    c = 5;
                    break;
                }
                break;
            case -804615187:
                if (nextName.equals("guild_join_requests")) {
                    c = 6;
                    break;
                }
                break;
            case -157517656:
                if (nextName.equals("read_state")) {
                    c = 7;
                    break;
                }
                break;
            case -149098930:
                if (nextName.equals("user_survey")) {
                    c = '\b';
                    break;
                }
                break;
            case 118:
                if (nextName.equals("v")) {
                    c = '\t';
                    break;
                }
                break;
            case 3599307:
                if (nextName.equals("user")) {
                    c = '\n';
                    break;
                }
                break;
            case 111578632:
                if (nextName.equals("users")) {
                    c = 11;
                    break;
                }
                break;
            case 228545628:
                if (nextName.equals("connected_accounts")) {
                    c = '\f';
                    break;
                }
                break;
            case 419359178:
                if (nextName.equals("guild_experiments")) {
                    c = '\r';
                    break;
                }
                break;
            case 472535355:
                if (nextName.equals("relationships")) {
                    c = 14;
                    break;
                }
                break;
            case 642961110:
                if (nextName.equals("required_action")) {
                    c = 15;
                    break;
                }
                break;
            case 833387520:
                if (nextName.equals("analytics_token")) {
                    c = 16;
                    break;
                }
                break;
            case 862130837:
                if (nextName.equals("friend_suggestion_count")) {
                    c = 17;
                    break;
                }
                break;
            case 1405079709:
                if (nextName.equals("sessions")) {
                    c = 18;
                    break;
                }
                break;
            case 1481071862:
                if (nextName.equals("country_code")) {
                    c = 19;
                    break;
                }
                break;
            case 1485182487:
                if (nextName.equals("user_settings")) {
                    c = 20;
                    break;
                }
                break;
            case 1649517590:
                if (nextName.equals("experiments")) {
                    c = 21;
                    break;
                }
                break;
            case 1661853540:
                if (nextName.equals("session_id")) {
                    c = 22;
                    break;
                }
                break;
            case 1704375052:
                if (nextName.equals("private_channels")) {
                    c = 23;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                this.userGuildSettings = (VersionedUserGuildSettings) jsonReader.parse(new VersionedUserGuildSettings());
                return;
            case 1:
                this.trace = getTraces(jsonReader);
                return;
            case 2:
                this.guildMembers = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.s
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        final Model.JsonReader jsonReader2 = Model.JsonReader.this;
                        return jsonReader2.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.u
                            @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                            public final Object get() {
                                return (GuildMember) InboundGatewayGsonParser.fromJson(Model.JsonReader.this, GuildMember.class);
                            }
                        });
                    }
                });
                return;
            case 3:
                MergedPresences mergedPresences = (MergedPresences) jsonReader.parse(new MergedPresences());
                this.presences = mergedPresences.friends;
                this.guildPresences = mergedPresences.guilds;
                return;
            case 4:
                this.guilds = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.l
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        return (Guild) InboundGatewayGsonParser.fromJson(Model.JsonReader.this, Guild.class);
                    }
                });
                return;
            case 5:
                this.presences = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.v
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        return (Presence) InboundGatewayGsonParser.fromJson(Model.JsonReader.this, Presence.class);
                    }
                });
                return;
            case 6:
                this.guildJoinRequests = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.y
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        return (GuildJoinRequest) InboundGatewayGsonParser.fromJson(Model.JsonReader.this, GuildJoinRequest.class);
                    }
                });
                return;
            case 7:
                this.readState = (VersionedReadStates) jsonReader.parse(new VersionedReadStates());
                return;
            case '\b':
                this.userSurvey = (UserSurvey) InboundGatewayGsonParser.fromJson(jsonReader, UserSurvey.class);
                return;
            case '\t':
                this.v = jsonReader.nextInt(this.v);
                return;
            case '\n':
                this.f2699me = (User) InboundGatewayGsonParser.fromJson(jsonReader, User.class);
                return;
            case 11:
                this.users = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.w
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        return (User) InboundGatewayGsonParser.fromJson(Model.JsonReader.this, User.class);
                    }
                });
                return;
            case '\f':
                this.connectedAccounts = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.n
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        return (ConnectedAccount) InboundGatewayGsonParser.fromJson(Model.JsonReader.this, ConnectedAccount.class);
                    }
                });
                return;
            case '\r':
                this.guildExperiments = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.z
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        return GuildExperimentDto.Parser.parse(Model.JsonReader.this);
                    }
                });
                return;
            case 14:
                this.relationships = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.x
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        return (ModelUserRelationship) Model.JsonReader.this.parse(new ModelUserRelationship());
                    }
                });
                return;
            case 15:
                this.requiredAction = jsonReader.nextString(this.requiredAction);
                return;
            case 16:
                this.analyticsToken = jsonReader.nextString(this.analyticsToken);
                return;
            case 17:
                this.friendSuggestionCount = jsonReader.nextInt(0);
                return;
            case 18:
                this.sessions = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.m
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        return (ModelSession) Model.JsonReader.this.parse(new ModelSession());
                    }
                });
                return;
            case 19:
                this.countryCode = jsonReader.nextString(this.countryCode);
                return;
            case 20:
                this.userSettings = (ModelUserSettings) jsonReader.parse(new ModelUserSettings());
                return;
            case 21:
                this.experiments = jsonReader.nextListAsMap(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.a0
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        return UserExperimentDto.Parser.parse(Model.JsonReader.this);
                    }
                }, f0.a);
                return;
            case 22:
                this.sessionId = jsonReader.nextString(this.sessionId);
                return;
            case 23:
                this.privateChannels = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.b0
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        return (Channel) InboundGatewayGsonParser.fromJson(Model.JsonReader.this, Channel.class);
                    }
                });
                return;
            default:
                jsonReader.skipValue();
                return;
        }
    }

    public boolean canEqual(Object obj) {
        return obj instanceof ModelPayload;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ModelPayload)) {
            return false;
        }
        ModelPayload modelPayload = (ModelPayload) obj;
        if (!modelPayload.canEqual(this) || getV() != modelPayload.getV() || getFriendSuggestionCount() != modelPayload.getFriendSuggestionCount()) {
            return false;
        }
        User me2 = getMe();
        User me3 = modelPayload.getMe();
        if (me2 != null ? !me2.equals(me3) : me3 != null) {
            return false;
        }
        ModelUserSettings userSettings = getUserSettings();
        ModelUserSettings userSettings2 = modelPayload.getUserSettings();
        if (userSettings != null ? !userSettings.equals(userSettings2) : userSettings2 != null) {
            return false;
        }
        VersionedUserGuildSettings userGuildSettings = getUserGuildSettings();
        VersionedUserGuildSettings userGuildSettings2 = modelPayload.getUserGuildSettings();
        if (userGuildSettings != null ? !userGuildSettings.equals(userGuildSettings2) : userGuildSettings2 != null) {
            return false;
        }
        String sessionId = getSessionId();
        String sessionId2 = modelPayload.getSessionId();
        if (sessionId != null ? !sessionId.equals(sessionId2) : sessionId2 != null) {
            return false;
        }
        List<ModelUserRelationship> relationships = getRelationships();
        List<ModelUserRelationship> relationships2 = modelPayload.getRelationships();
        if (relationships != null ? !relationships.equals(relationships2) : relationships2 != null) {
            return false;
        }
        VersionedReadStates readState = getReadState();
        VersionedReadStates readState2 = modelPayload.getReadState();
        if (readState != null ? !readState.equals(readState2) : readState2 != null) {
            return false;
        }
        List<Channel> privateChannels = getPrivateChannels();
        List<Channel> privateChannels2 = modelPayload.getPrivateChannels();
        if (privateChannels != null ? !privateChannels.equals(privateChannels2) : privateChannels2 != null) {
            return false;
        }
        List<Guild> guilds = getGuilds();
        List<Guild> guilds2 = modelPayload.getGuilds();
        if (guilds != null ? !guilds.equals(guilds2) : guilds2 != null) {
            return false;
        }
        List<GuildJoinRequest> guildJoinRequests = getGuildJoinRequests();
        List<GuildJoinRequest> guildJoinRequests2 = modelPayload.getGuildJoinRequests();
        if (guildJoinRequests != null ? !guildJoinRequests.equals(guildJoinRequests2) : guildJoinRequests2 != null) {
            return false;
        }
        List<Presence> presences = getPresences();
        List<Presence> presences2 = modelPayload.getPresences();
        if (presences != null ? !presences.equals(presences2) : presences2 != null) {
            return false;
        }
        String analyticsToken = getAnalyticsToken();
        String analyticsToken2 = modelPayload.getAnalyticsToken();
        if (analyticsToken != null ? !analyticsToken.equals(analyticsToken2) : analyticsToken2 != null) {
            return false;
        }
        String requiredAction = getRequiredAction();
        String requiredAction2 = modelPayload.getRequiredAction();
        if (requiredAction != null ? !requiredAction.equals(requiredAction2) : requiredAction2 != null) {
            return false;
        }
        Map<Long, UserExperimentDto> experiments = getExperiments();
        Map<Long, UserExperimentDto> experiments2 = modelPayload.getExperiments();
        if (experiments != null ? !experiments.equals(experiments2) : experiments2 != null) {
            return false;
        }
        List<GuildExperimentDto> guildExperiments = getGuildExperiments();
        List<GuildExperimentDto> guildExperiments2 = modelPayload.getGuildExperiments();
        if (guildExperiments != null ? !guildExperiments.equals(guildExperiments2) : guildExperiments2 != null) {
            return false;
        }
        List<ConnectedAccount> connectedAccounts = getConnectedAccounts();
        List<ConnectedAccount> connectedAccounts2 = modelPayload.getConnectedAccounts();
        if (connectedAccounts != null ? !connectedAccounts.equals(connectedAccounts2) : connectedAccounts2 != null) {
            return false;
        }
        List<ModelSession> sessions = getSessions();
        List<ModelSession> sessions2 = modelPayload.getSessions();
        if (sessions != null ? !sessions.equals(sessions2) : sessions2 != null) {
            return false;
        }
        List<User> users = getUsers();
        List<User> users2 = modelPayload.getUsers();
        if (users != null ? !users.equals(users2) : users2 != null) {
            return false;
        }
        List<List<Presence>> guildPresences = getGuildPresences();
        List<List<Presence>> guildPresences2 = modelPayload.getGuildPresences();
        if (guildPresences != null ? !guildPresences.equals(guildPresences2) : guildPresences2 != null) {
            return false;
        }
        List<List<GuildMember>> guildMembers = getGuildMembers();
        List<List<GuildMember>> guildMembers2 = modelPayload.getGuildMembers();
        if (guildMembers != null ? !guildMembers.equals(guildMembers2) : guildMembers2 != null) {
            return false;
        }
        String countryCode = getCountryCode();
        String countryCode2 = modelPayload.getCountryCode();
        if (countryCode != null ? !countryCode.equals(countryCode2) : countryCode2 != null) {
            return false;
        }
        Object trace = getTrace();
        Object trace2 = modelPayload.getTrace();
        if (trace != null ? !trace.equals(trace2) : trace2 != null) {
            return false;
        }
        UserSurvey userSurvey = getUserSurvey();
        UserSurvey userSurvey2 = modelPayload.getUserSurvey();
        return userSurvey != null ? userSurvey.equals(userSurvey2) : userSurvey2 == null;
    }

    public String getAnalyticsToken() {
        return this.analyticsToken;
    }

    public List<ConnectedAccount> getConnectedAccounts() {
        return this.connectedAccounts;
    }

    public String getCountryCode() {
        return this.countryCode;
    }

    public Map<Long, UserExperimentDto> getExperiments() {
        return this.experiments;
    }

    public int getFriendSuggestionCount() {
        return this.friendSuggestionCount;
    }

    @Nullable
    public List<GuildExperimentDto> getGuildExperiments() {
        return this.guildExperiments;
    }

    public List<GuildJoinRequest> getGuildJoinRequests() {
        return this.guildJoinRequests;
    }

    public List<List<GuildMember>> getGuildMembers() {
        return this.guildMembers;
    }

    public List<List<Presence>> getGuildPresences() {
        return this.guildPresences;
    }

    public List<Guild> getGuilds() {
        return this.guilds;
    }

    public User getMe() {
        return this.f2699me;
    }

    public List<Presence> getPresences() {
        return this.presences;
    }

    public List<Channel> getPrivateChannels() {
        return this.privateChannels;
    }

    public VersionedReadStates getReadState() {
        return this.readState;
    }

    public List<ModelUserRelationship> getRelationships() {
        return this.relationships;
    }

    public String getRequiredAction() {
        return this.requiredAction;
    }

    public String getSessionId() {
        return this.sessionId;
    }

    public List<ModelSession> getSessions() {
        return this.sessions;
    }

    public Object getTrace() {
        return this.trace;
    }

    public VersionedUserGuildSettings getUserGuildSettings() {
        return this.userGuildSettings;
    }

    public ModelUserSettings getUserSettings() {
        return this.userSettings;
    }

    public UserSurvey getUserSurvey() {
        return this.userSurvey;
    }

    public List<User> getUsers() {
        return this.users;
    }

    public int getV() {
        return this.v;
    }

    public int hashCode() {
        int friendSuggestionCount = getFriendSuggestionCount() + ((getV() + 59) * 59);
        User me2 = getMe();
        int i = 43;
        int hashCode = (friendSuggestionCount * 59) + (me2 == null ? 43 : me2.hashCode());
        ModelUserSettings userSettings = getUserSettings();
        int hashCode2 = (hashCode * 59) + (userSettings == null ? 43 : userSettings.hashCode());
        VersionedUserGuildSettings userGuildSettings = getUserGuildSettings();
        int hashCode3 = (hashCode2 * 59) + (userGuildSettings == null ? 43 : userGuildSettings.hashCode());
        String sessionId = getSessionId();
        int hashCode4 = (hashCode3 * 59) + (sessionId == null ? 43 : sessionId.hashCode());
        List<ModelUserRelationship> relationships = getRelationships();
        int hashCode5 = (hashCode4 * 59) + (relationships == null ? 43 : relationships.hashCode());
        VersionedReadStates readState = getReadState();
        int hashCode6 = (hashCode5 * 59) + (readState == null ? 43 : readState.hashCode());
        List<Channel> privateChannels = getPrivateChannels();
        int hashCode7 = (hashCode6 * 59) + (privateChannels == null ? 43 : privateChannels.hashCode());
        List<Guild> guilds = getGuilds();
        int hashCode8 = (hashCode7 * 59) + (guilds == null ? 43 : guilds.hashCode());
        List<GuildJoinRequest> guildJoinRequests = getGuildJoinRequests();
        int hashCode9 = (hashCode8 * 59) + (guildJoinRequests == null ? 43 : guildJoinRequests.hashCode());
        List<Presence> presences = getPresences();
        int hashCode10 = (hashCode9 * 59) + (presences == null ? 43 : presences.hashCode());
        String analyticsToken = getAnalyticsToken();
        int hashCode11 = (hashCode10 * 59) + (analyticsToken == null ? 43 : analyticsToken.hashCode());
        String requiredAction = getRequiredAction();
        int hashCode12 = (hashCode11 * 59) + (requiredAction == null ? 43 : requiredAction.hashCode());
        Map<Long, UserExperimentDto> experiments = getExperiments();
        int hashCode13 = (hashCode12 * 59) + (experiments == null ? 43 : experiments.hashCode());
        List<GuildExperimentDto> guildExperiments = getGuildExperiments();
        int hashCode14 = (hashCode13 * 59) + (guildExperiments == null ? 43 : guildExperiments.hashCode());
        List<ConnectedAccount> connectedAccounts = getConnectedAccounts();
        int hashCode15 = (hashCode14 * 59) + (connectedAccounts == null ? 43 : connectedAccounts.hashCode());
        List<ModelSession> sessions = getSessions();
        int hashCode16 = (hashCode15 * 59) + (sessions == null ? 43 : sessions.hashCode());
        List<User> users = getUsers();
        int hashCode17 = (hashCode16 * 59) + (users == null ? 43 : users.hashCode());
        List<List<Presence>> guildPresences = getGuildPresences();
        int hashCode18 = (hashCode17 * 59) + (guildPresences == null ? 43 : guildPresences.hashCode());
        List<List<GuildMember>> guildMembers = getGuildMembers();
        int hashCode19 = (hashCode18 * 59) + (guildMembers == null ? 43 : guildMembers.hashCode());
        String countryCode = getCountryCode();
        int hashCode20 = (hashCode19 * 59) + (countryCode == null ? 43 : countryCode.hashCode());
        Object trace = getTrace();
        int hashCode21 = (hashCode20 * 59) + (trace == null ? 43 : trace.hashCode());
        UserSurvey userSurvey = getUserSurvey();
        int i2 = hashCode21 * 59;
        if (userSurvey != null) {
            i = userSurvey.hashCode();
        }
        return i2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("ModelPayload(v=");
        R.append(getV());
        R.append(", me=");
        R.append(getMe());
        R.append(", userSettings=");
        R.append(getUserSettings());
        R.append(", userGuildSettings=");
        R.append(getUserGuildSettings());
        R.append(", sessionId=");
        R.append(getSessionId());
        R.append(", relationships=");
        R.append(getRelationships());
        R.append(", readState=");
        R.append(getReadState());
        R.append(", privateChannels=");
        R.append(getPrivateChannels());
        R.append(", guilds=");
        R.append(getGuilds());
        R.append(", guildJoinRequests=");
        R.append(getGuildJoinRequests());
        R.append(", presences=");
        R.append(getPresences());
        R.append(", analyticsToken=");
        R.append(getAnalyticsToken());
        R.append(", requiredAction=");
        R.append(getRequiredAction());
        R.append(", experiments=");
        R.append(getExperiments());
        R.append(", guildExperiments=");
        R.append(getGuildExperiments());
        R.append(", connectedAccounts=");
        R.append(getConnectedAccounts());
        R.append(", sessions=");
        R.append(getSessions());
        R.append(", users=");
        R.append(getUsers());
        R.append(", guildPresences=");
        R.append(getGuildPresences());
        R.append(", guildMembers=");
        R.append(getGuildMembers());
        R.append(", countryCode=");
        R.append(getCountryCode());
        R.append(", trace=");
        R.append(getTrace());
        R.append(", userSurvey=");
        R.append(getUserSurvey());
        R.append(", friendSuggestionCount=");
        R.append(getFriendSuggestionCount());
        R.append(")");
        return R.toString();
    }

    public ModelPayload withGuilds(List<Guild> list) {
        ModelPayload modelPayload = new ModelPayload(this);
        modelPayload.guilds = list;
        return modelPayload;
    }

    public ModelPayload withHydratedUserData(List<ModelUserRelationship> list, List<Channel> list2, List<List<Presence>> list3, List<List<GuildMember>> list4, List<Presence> list5) {
        ModelPayload modelPayload = new ModelPayload(this);
        modelPayload.relationships = list;
        modelPayload.privateChannels = list2;
        modelPayload.guildPresences = list3;
        modelPayload.guildMembers = list4;
        modelPayload.presences = list5;
        return modelPayload;
    }

    public ModelPayload(ModelPayload modelPayload) {
        this.v = modelPayload.v;
        this.f2699me = modelPayload.f2699me;
        this.userSettings = modelPayload.userSettings;
        this.userGuildSettings = modelPayload.userGuildSettings;
        this.sessionId = modelPayload.sessionId;
        this.relationships = modelPayload.relationships;
        this.readState = modelPayload.readState;
        this.privateChannels = modelPayload.privateChannels;
        this.guilds = modelPayload.guilds;
        this.guildJoinRequests = modelPayload.guildJoinRequests;
        this.presences = modelPayload.presences;
        this.analyticsToken = modelPayload.analyticsToken;
        this.requiredAction = modelPayload.requiredAction;
        this.experiments = modelPayload.experiments;
        this.guildExperiments = modelPayload.guildExperiments;
        this.connectedAccounts = modelPayload.connectedAccounts;
        this.sessions = modelPayload.sessions;
        this.users = modelPayload.users;
        this.guildPresences = modelPayload.guildPresences;
        this.guildMembers = modelPayload.guildMembers;
        this.countryCode = modelPayload.countryCode;
        this.trace = modelPayload.trace;
        this.userSurvey = modelPayload.userSurvey;
        this.friendSuggestionCount = modelPayload.friendSuggestionCount;
    }
}
