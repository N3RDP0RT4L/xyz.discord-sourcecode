package com.discord.models.domain;

import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.user.User;
import com.discord.models.deserialization.gson.InboundGatewayGsonParser;
import com.discord.models.domain.Model;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelGuildIntegration;
import com.discord.models.domain.ModelWebhook;
import java.io.IOException;
import java.util.List;
/* loaded from: classes.dex */
public class ModelAuditLog implements Model {
    private List<ModelAuditLogEntry> auditLogEntries;
    private List<GuildScheduledEvent> guildScheduledEvents;
    private List<ModelGuildIntegration> integrations;
    private List<Channel> threads;
    private List<User> users;
    private List<ModelWebhook> webhooks;

    @Override // com.discord.models.domain.Model
    public void assignField(final Model.JsonReader jsonReader) throws IOException {
        String nextName = jsonReader.nextName();
        nextName.hashCode();
        char c = 65535;
        switch (nextName.hashCode()) {
            case -1337936983:
                if (nextName.equals("threads")) {
                    c = 0;
                    break;
                }
                break;
            case -746742857:
                if (nextName.equals("guild_scheduled_events")) {
                    c = 1;
                    break;
                }
                break;
            case -710289188:
                if (nextName.equals("webhooks")) {
                    c = 2;
                    break;
                }
                break;
            case -443657167:
                if (nextName.equals("audit_log_entries")) {
                    c = 3;
                    break;
                }
                break;
            case 111578632:
                if (nextName.equals("users")) {
                    c = 4;
                    break;
                }
                break;
            case 1487029535:
                if (nextName.equals("integrations")) {
                    c = 5;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                this.threads = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.f
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        return (Channel) InboundGatewayGsonParser.fromJson(Model.JsonReader.this, Channel.class);
                    }
                });
                return;
            case 1:
                this.guildScheduledEvents = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.c
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        return (GuildScheduledEvent) InboundGatewayGsonParser.fromJson(Model.JsonReader.this, GuildScheduledEvent.class);
                    }
                });
                return;
            case 2:
                this.webhooks = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.e
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        return (ModelWebhook) Model.JsonReader.this.parse(new ModelWebhook());
                    }
                });
                return;
            case 3:
                this.auditLogEntries = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.b
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        return (ModelAuditLogEntry) Model.JsonReader.this.parse(new ModelAuditLogEntry());
                    }
                });
                return;
            case 4:
                this.users = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.g
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        return (User) InboundGatewayGsonParser.fromJson(Model.JsonReader.this, User.class);
                    }
                });
                return;
            case 5:
                this.integrations = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.d
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        return (ModelGuildIntegration) Model.JsonReader.this.parse(new ModelGuildIntegration());
                    }
                });
                return;
            default:
                jsonReader.skipValue();
                return;
        }
    }

    public boolean canEqual(Object obj) {
        return obj instanceof ModelAuditLog;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ModelAuditLog)) {
            return false;
        }
        ModelAuditLog modelAuditLog = (ModelAuditLog) obj;
        if (!modelAuditLog.canEqual(this)) {
            return false;
        }
        List<User> users = getUsers();
        List<User> users2 = modelAuditLog.getUsers();
        if (users != null ? !users.equals(users2) : users2 != null) {
            return false;
        }
        List<ModelWebhook> webhooks = getWebhooks();
        List<ModelWebhook> webhooks2 = modelAuditLog.getWebhooks();
        if (webhooks != null ? !webhooks.equals(webhooks2) : webhooks2 != null) {
            return false;
        }
        List<ModelGuildIntegration> integrations = getIntegrations();
        List<ModelGuildIntegration> integrations2 = modelAuditLog.getIntegrations();
        if (integrations != null ? !integrations.equals(integrations2) : integrations2 != null) {
            return false;
        }
        List<ModelAuditLogEntry> auditLogEntries = getAuditLogEntries();
        List<ModelAuditLogEntry> auditLogEntries2 = modelAuditLog.getAuditLogEntries();
        if (auditLogEntries != null ? !auditLogEntries.equals(auditLogEntries2) : auditLogEntries2 != null) {
            return false;
        }
        List<GuildScheduledEvent> guildScheduledEvents = getGuildScheduledEvents();
        List<GuildScheduledEvent> guildScheduledEvents2 = modelAuditLog.getGuildScheduledEvents();
        if (guildScheduledEvents != null ? !guildScheduledEvents.equals(guildScheduledEvents2) : guildScheduledEvents2 != null) {
            return false;
        }
        List<Channel> threads = getThreads();
        List<Channel> threads2 = modelAuditLog.getThreads();
        return threads != null ? threads.equals(threads2) : threads2 == null;
    }

    public List<ModelAuditLogEntry> getAuditLogEntries() {
        return this.auditLogEntries;
    }

    public List<GuildScheduledEvent> getGuildScheduledEvents() {
        return this.guildScheduledEvents;
    }

    public List<ModelGuildIntegration> getIntegrations() {
        return this.integrations;
    }

    public List<Channel> getThreads() {
        return this.threads;
    }

    public List<User> getUsers() {
        return this.users;
    }

    public List<ModelWebhook> getWebhooks() {
        return this.webhooks;
    }

    public int hashCode() {
        List<User> users = getUsers();
        int i = 43;
        int hashCode = users == null ? 43 : users.hashCode();
        List<ModelWebhook> webhooks = getWebhooks();
        int hashCode2 = ((hashCode + 59) * 59) + (webhooks == null ? 43 : webhooks.hashCode());
        List<ModelGuildIntegration> integrations = getIntegrations();
        int hashCode3 = (hashCode2 * 59) + (integrations == null ? 43 : integrations.hashCode());
        List<ModelAuditLogEntry> auditLogEntries = getAuditLogEntries();
        int hashCode4 = (hashCode3 * 59) + (auditLogEntries == null ? 43 : auditLogEntries.hashCode());
        List<GuildScheduledEvent> guildScheduledEvents = getGuildScheduledEvents();
        int hashCode5 = (hashCode4 * 59) + (guildScheduledEvents == null ? 43 : guildScheduledEvents.hashCode());
        List<Channel> threads = getThreads();
        int i2 = hashCode5 * 59;
        if (threads != null) {
            i = threads.hashCode();
        }
        return i2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("ModelAuditLog(users=");
        R.append(getUsers());
        R.append(", webhooks=");
        R.append(getWebhooks());
        R.append(", integrations=");
        R.append(getIntegrations());
        R.append(", auditLogEntries=");
        R.append(getAuditLogEntries());
        R.append(", guildScheduledEvents=");
        R.append(getGuildScheduledEvents());
        R.append(", threads=");
        R.append(getThreads());
        R.append(")");
        return R.toString();
    }
}
