package com.discord.models.domain;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.Model;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Ref$BooleanRef;
import kotlin.jvm.internal.Ref$ObjectRef;
import rx.functions.Action1;
/* compiled from: ModelApplicationStream.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001:\u0001%B7\u0012\n\u0010\u000f\u001a\u00060\u0002j\u0002`\u0003\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\u0011\u001a\u00020\u0007\u0012\u0010\u0010\u0012\u001a\f\u0012\b\u0012\u00060\u000bj\u0002`\f0\n¢\u0006\u0004\b#\u0010$J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0005J\u0010\u0010\b\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\r\u001a\f\u0012\b\u0012\u00060\u000bj\u0002`\f0\nHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJH\u0010\u0013\u001a\u00020\u00002\f\b\u0002\u0010\u000f\u001a\u00060\u0002j\u0002`\u00032\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\u0011\u001a\u00020\u00072\u0012\b\u0002\u0010\u0012\u001a\f\u0012\b\u0012\u00060\u000bj\u0002`\f0\nHÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0005J\u0010\u0010\u0017\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u001a\u0010\u001a\u001a\u00020\u00072\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001a\u0010\u001bR#\u0010\u0012\u001a\f\u0012\b\u0012\u00060\u000bj\u0002`\f0\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u001c\u001a\u0004\b\u001d\u0010\u000eR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001e\u001a\u0004\b\u001f\u0010\u0005R\u001d\u0010\u000f\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001e\u001a\u0004\b \u0010\u0005R\u0019\u0010\u0011\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010!\u001a\u0004\b\"\u0010\t¨\u0006&"}, d2 = {"Lcom/discord/models/domain/StreamCreateOrUpdate;", "", "", "Lcom/discord/primitives/StreamKey;", "component1", "()Ljava/lang/String;", "component2", "", "component3", "()Z", "", "", "Lcom/discord/primitives/UserId;", "component4", "()Ljava/util/List;", "streamKey", "rtcServerId", "paused", "viewerIds", "copy", "(Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;)Lcom/discord/models/domain/StreamCreateOrUpdate;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getViewerIds", "Ljava/lang/String;", "getRtcServerId", "getStreamKey", "Z", "getPaused", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;)V", "Parser", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StreamCreateOrUpdate {
    private final boolean paused;
    private final String rtcServerId;
    private final String streamKey;
    private final List<Long> viewerIds;

    /* compiled from: ModelApplicationStream.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/models/domain/StreamCreateOrUpdate$Parser;", "Lcom/discord/models/domain/Model$Parser;", "Lcom/discord/models/domain/StreamCreateOrUpdate;", "Lcom/discord/models/domain/Model$JsonReader;", "reader", "parse", "(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/StreamCreateOrUpdate;", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Parser implements Model.Parser<StreamCreateOrUpdate> {
        public static final Parser INSTANCE = new Parser();

        private Parser() {
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // com.discord.models.domain.Model.Parser
        public StreamCreateOrUpdate parse(final Model.JsonReader jsonReader) {
            final Ref$ObjectRef a02 = a.a0(jsonReader, "reader");
            a02.element = null;
            final Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
            ref$ObjectRef.element = null;
            final Ref$BooleanRef ref$BooleanRef = new Ref$BooleanRef();
            ref$BooleanRef.element = false;
            final Ref$ObjectRef ref$ObjectRef2 = new Ref$ObjectRef();
            ref$ObjectRef2.element = null;
            jsonReader.nextObject(new Action1<String>() { // from class: com.discord.models.domain.StreamCreateOrUpdate$Parser$parse$1
                public final void call(String str) {
                    if (str != null) {
                        switch (str.hashCode()) {
                            case -1349214453:
                                if (str.equals("viewer_ids")) {
                                    ref$ObjectRef2.element = (T) jsonReader.nextList(new Model.JsonReader.ItemFactory<Long>() { // from class: com.discord.models.domain.StreamCreateOrUpdate$Parser$parse$1.1
                                        /* JADX WARN: Can't rename method to resolve collision */
                                        @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                                        public final Long get() {
                                            return Long.valueOf(jsonReader.nextLong(0L));
                                        }
                                    });
                                    return;
                                }
                                break;
                            case -1194435296:
                                if (str.equals("stream_key")) {
                                    Ref$ObjectRef ref$ObjectRef3 = Ref$ObjectRef.this;
                                    T t = (T) jsonReader.nextStringOrNull();
                                    m.checkNotNull(t);
                                    ref$ObjectRef3.element = t;
                                    return;
                                }
                                break;
                            case -995321554:
                                if (str.equals("paused")) {
                                    Ref$BooleanRef ref$BooleanRef2 = ref$BooleanRef;
                                    Boolean nextBooleanOrNull = jsonReader.nextBooleanOrNull();
                                    m.checkNotNullExpressionValue(nextBooleanOrNull, "reader.nextBooleanOrNull()");
                                    ref$BooleanRef2.element = nextBooleanOrNull.booleanValue();
                                    return;
                                }
                                break;
                            case -490387655:
                                if (str.equals("rtc_server_id")) {
                                    ref$ObjectRef.element = (T) jsonReader.nextStringOrNull();
                                    return;
                                }
                                break;
                        }
                    }
                    jsonReader.skipValue();
                }
            });
            T t = a02.element;
            if (t == 0) {
                m.throwUninitializedPropertyAccessException("streamKey");
            }
            String str = (String) t;
            String str2 = (String) ref$ObjectRef.element;
            boolean z2 = ref$BooleanRef.element;
            List list = (List) ref$ObjectRef2.element;
            if (list == null) {
                list = n.emptyList();
            }
            return new StreamCreateOrUpdate(str, str2, z2, list);
        }
    }

    public StreamCreateOrUpdate(String str, String str2, boolean z2, List<Long> list) {
        m.checkNotNullParameter(str, "streamKey");
        m.checkNotNullParameter(list, "viewerIds");
        this.streamKey = str;
        this.rtcServerId = str2;
        this.paused = z2;
        this.viewerIds = list;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ StreamCreateOrUpdate copy$default(StreamCreateOrUpdate streamCreateOrUpdate, String str, String str2, boolean z2, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            str = streamCreateOrUpdate.streamKey;
        }
        if ((i & 2) != 0) {
            str2 = streamCreateOrUpdate.rtcServerId;
        }
        if ((i & 4) != 0) {
            z2 = streamCreateOrUpdate.paused;
        }
        if ((i & 8) != 0) {
            list = streamCreateOrUpdate.viewerIds;
        }
        return streamCreateOrUpdate.copy(str, str2, z2, list);
    }

    public final String component1() {
        return this.streamKey;
    }

    public final String component2() {
        return this.rtcServerId;
    }

    public final boolean component3() {
        return this.paused;
    }

    public final List<Long> component4() {
        return this.viewerIds;
    }

    public final StreamCreateOrUpdate copy(String str, String str2, boolean z2, List<Long> list) {
        m.checkNotNullParameter(str, "streamKey");
        m.checkNotNullParameter(list, "viewerIds");
        return new StreamCreateOrUpdate(str, str2, z2, list);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StreamCreateOrUpdate)) {
            return false;
        }
        StreamCreateOrUpdate streamCreateOrUpdate = (StreamCreateOrUpdate) obj;
        return m.areEqual(this.streamKey, streamCreateOrUpdate.streamKey) && m.areEqual(this.rtcServerId, streamCreateOrUpdate.rtcServerId) && this.paused == streamCreateOrUpdate.paused && m.areEqual(this.viewerIds, streamCreateOrUpdate.viewerIds);
    }

    public final boolean getPaused() {
        return this.paused;
    }

    public final String getRtcServerId() {
        return this.rtcServerId;
    }

    public final String getStreamKey() {
        return this.streamKey;
    }

    public final List<Long> getViewerIds() {
        return this.viewerIds;
    }

    public int hashCode() {
        String str = this.streamKey;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.rtcServerId;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        boolean z2 = this.paused;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        int i4 = (hashCode2 + i2) * 31;
        List<Long> list = this.viewerIds;
        if (list != null) {
            i = list.hashCode();
        }
        return i4 + i;
    }

    public String toString() {
        StringBuilder R = a.R("StreamCreateOrUpdate(streamKey=");
        R.append(this.streamKey);
        R.append(", rtcServerId=");
        R.append(this.rtcServerId);
        R.append(", paused=");
        R.append(this.paused);
        R.append(", viewerIds=");
        return a.K(R, this.viewerIds, ")");
    }
}
