package com.discord.models.domain;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.Model;
import com.discord.models.domain.ModelSku;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.Ref$ObjectRef;
import rx.functions.Action1;
/* compiled from: ModelStoreListing.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u001cB\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0016\u001a\u0004\b\u0017\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0018\u001a\u0004\b\u0019\u0010\u0004¨\u0006\u001d"}, d2 = {"Lcom/discord/models/domain/ModelStoreListing;", "", "Lcom/discord/models/domain/ModelSku;", "component1", "()Lcom/discord/models/domain/ModelSku;", "", "component2", "()J", "sku", ModelAuditLogEntry.CHANGE_KEY_ID, "copy", "(Lcom/discord/models/domain/ModelSku;J)Lcom/discord/models/domain/ModelStoreListing;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getId", "Lcom/discord/models/domain/ModelSku;", "getSku", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelSku;J)V", "Parser", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelStoreListing {

    /* renamed from: id  reason: collision with root package name */
    private final long f2704id;
    private final ModelSku sku;

    /* compiled from: ModelStoreListing.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/models/domain/ModelStoreListing$Parser;", "Lcom/discord/models/domain/Model$Parser;", "Lcom/discord/models/domain/ModelStoreListing;", "Lcom/discord/models/domain/Model$JsonReader;", "reader", "parse", "(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelStoreListing;", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Parser implements Model.Parser<ModelStoreListing> {
        public static final Parser INSTANCE = new Parser();

        private Parser() {
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // com.discord.models.domain.Model.Parser
        public ModelStoreListing parse(final Model.JsonReader jsonReader) {
            final Ref$ObjectRef a02 = a.a0(jsonReader, "reader");
            a02.element = null;
            final Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
            ref$ObjectRef.element = null;
            jsonReader.nextObject(new Action1<String>() { // from class: com.discord.models.domain.ModelStoreListing$Parser$parse$1
                public final void call(String str) {
                    if (str != null) {
                        int hashCode = str.hashCode();
                        if (hashCode != 3355) {
                            if (hashCode == 113949 && str.equals("sku")) {
                                Ref$ObjectRef.this.element = (T) ModelSku.Parser.INSTANCE.parse(jsonReader);
                                return;
                            }
                        } else if (str.equals(ModelAuditLogEntry.CHANGE_KEY_ID)) {
                            ref$ObjectRef.element = (T) jsonReader.nextLongOrNull();
                            return;
                        }
                    }
                    jsonReader.skipValue();
                }
            });
            ModelSku modelSku = (ModelSku) a02.element;
            m.checkNotNull(modelSku);
            Long l = (Long) ref$ObjectRef.element;
            return new ModelStoreListing(modelSku, l != null ? l.longValue() : 0L);
        }
    }

    public ModelStoreListing(ModelSku modelSku, long j) {
        m.checkNotNullParameter(modelSku, "sku");
        this.sku = modelSku;
        this.f2704id = j;
    }

    public static /* synthetic */ ModelStoreListing copy$default(ModelStoreListing modelStoreListing, ModelSku modelSku, long j, int i, Object obj) {
        if ((i & 1) != 0) {
            modelSku = modelStoreListing.sku;
        }
        if ((i & 2) != 0) {
            j = modelStoreListing.f2704id;
        }
        return modelStoreListing.copy(modelSku, j);
    }

    public final ModelSku component1() {
        return this.sku;
    }

    public final long component2() {
        return this.f2704id;
    }

    public final ModelStoreListing copy(ModelSku modelSku, long j) {
        m.checkNotNullParameter(modelSku, "sku");
        return new ModelStoreListing(modelSku, j);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModelStoreListing)) {
            return false;
        }
        ModelStoreListing modelStoreListing = (ModelStoreListing) obj;
        return m.areEqual(this.sku, modelStoreListing.sku) && this.f2704id == modelStoreListing.f2704id;
    }

    public final long getId() {
        return this.f2704id;
    }

    public final ModelSku getSku() {
        return this.sku;
    }

    public int hashCode() {
        ModelSku modelSku = this.sku;
        int hashCode = modelSku != null ? modelSku.hashCode() : 0;
        long j = this.f2704id;
        return (hashCode * 31) + ((int) (j ^ (j >>> 32)));
    }

    public String toString() {
        StringBuilder R = a.R("ModelStoreListing(sku=");
        R.append(this.sku);
        R.append(", id=");
        return a.B(R, this.f2704id, ")");
    }
}
