package com.discord.models.domain;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.Model;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Ref$ObjectRef;
import rx.functions.Action1;
/* compiled from: ModelGuildFolder.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001:\u0001'BA\u0012\u000e\u0010\u0010\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003\u0012\u0010\u0010\u0011\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00070\u0006\u0012\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\n\u0012\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\r¢\u0006\u0004\b%\u0010&J\u0018\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u001a\u0010\b\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00070\u0006HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0012\u0010\u000b\u001a\u0004\u0018\u00010\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0012\u0010\u000e\u001a\u0004\u0018\u00010\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJN\u0010\u0014\u001a\u00020\u00002\u0010\b\u0002\u0010\u0010\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\u0012\b\u0002\u0010\u0011\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00070\u00062\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\n2\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\rHÆ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u0016\u0010\u000fJ\u0010\u0010\u0017\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u001a\u0010\u001b\u001a\u00020\u001a2\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001b\u0010\u001cR!\u0010\u0010\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001d\u001a\u0004\b\u001e\u0010\u0005R#\u0010\u0011\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00070\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001f\u001a\u0004\b \u0010\tR\u001b\u0010\u0013\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010!\u001a\u0004\b\"\u0010\u000fR\u001b\u0010\u0012\u001a\u0004\u0018\u00010\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010#\u001a\u0004\b$\u0010\f¨\u0006("}, d2 = {"Lcom/discord/models/domain/ModelGuildFolder;", "", "", "Lcom/discord/primitives/FolderId;", "component1", "()Ljava/lang/Long;", "", "Lcom/discord/primitives/GuildId;", "component2", "()Ljava/util/List;", "", "component3", "()Ljava/lang/Integer;", "", "component4", "()Ljava/lang/String;", ModelAuditLogEntry.CHANGE_KEY_ID, "guildIds", ModelAuditLogEntry.CHANGE_KEY_COLOR, ModelAuditLogEntry.CHANGE_KEY_NAME, "copy", "(Ljava/lang/Long;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/String;)Lcom/discord/models/domain/ModelGuildFolder;", "toString", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Long;", "getId", "Ljava/util/List;", "getGuildIds", "Ljava/lang/String;", "getName", "Ljava/lang/Integer;", "getColor", HookHelper.constructorName, "(Ljava/lang/Long;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/String;)V", "Parser", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelGuildFolder {
    private final Integer color;
    private final List<Long> guildIds;

    /* renamed from: id  reason: collision with root package name */
    private final Long f2693id;
    private final String name;

    /* compiled from: ModelGuildFolder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/models/domain/ModelGuildFolder$Parser;", "Lcom/discord/models/domain/Model$Parser;", "Lcom/discord/models/domain/ModelGuildFolder;", "Lcom/discord/models/domain/Model$JsonReader;", "reader", "parse", "(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelGuildFolder;", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Parser implements Model.Parser<ModelGuildFolder> {
        public static final Parser INSTANCE = new Parser();

        private Parser() {
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // com.discord.models.domain.Model.Parser
        public ModelGuildFolder parse(final Model.JsonReader jsonReader) {
            final Ref$ObjectRef a02 = a.a0(jsonReader, "reader");
            a02.element = null;
            final Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
            ref$ObjectRef.element = null;
            final Ref$ObjectRef ref$ObjectRef2 = new Ref$ObjectRef();
            ref$ObjectRef2.element = null;
            final Ref$ObjectRef ref$ObjectRef3 = new Ref$ObjectRef();
            ref$ObjectRef3.element = null;
            jsonReader.nextObject(new Action1<String>() { // from class: com.discord.models.domain.ModelGuildFolder$Parser$parse$1
                public final void call(String str) {
                    if (str != null) {
                        int hashCode = str.hashCode();
                        if (hashCode != -1847996308) {
                            if (hashCode != 3355) {
                                if (hashCode != 3373707) {
                                    if (hashCode == 94842723 && str.equals(ModelAuditLogEntry.CHANGE_KEY_COLOR)) {
                                        ref$ObjectRef2.element = (T) jsonReader.nextIntOrNull();
                                        return;
                                    }
                                } else if (str.equals(ModelAuditLogEntry.CHANGE_KEY_NAME)) {
                                    ref$ObjectRef3.element = (T) jsonReader.nextStringOrNull();
                                    return;
                                }
                            } else if (str.equals(ModelAuditLogEntry.CHANGE_KEY_ID)) {
                                Ref$ObjectRef.this.element = (T) jsonReader.nextLongOrNull();
                                return;
                            }
                        } else if (str.equals("guild_ids")) {
                            ref$ObjectRef.element = (T) jsonReader.nextList(new Model.JsonReader.ItemFactory<Long>() { // from class: com.discord.models.domain.ModelGuildFolder$Parser$parse$1.1
                                /* JADX WARN: Can't rename method to resolve collision */
                                @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                                public final Long get() {
                                    return jsonReader.nextLongOrNull();
                                }
                            });
                            return;
                        }
                    }
                    jsonReader.skipValue();
                }
            });
            List list = (List) ref$ObjectRef.element;
            m.checkNotNull(list);
            return new ModelGuildFolder((Long) a02.element, list, (Integer) ref$ObjectRef2.element, (String) ref$ObjectRef3.element);
        }
    }

    public ModelGuildFolder(Long l, List<Long> list, Integer num, String str) {
        m.checkNotNullParameter(list, "guildIds");
        this.f2693id = l;
        this.guildIds = list;
        this.color = num;
        this.name = str;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ ModelGuildFolder copy$default(ModelGuildFolder modelGuildFolder, Long l, List list, Integer num, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            l = modelGuildFolder.f2693id;
        }
        if ((i & 2) != 0) {
            list = modelGuildFolder.guildIds;
        }
        if ((i & 4) != 0) {
            num = modelGuildFolder.color;
        }
        if ((i & 8) != 0) {
            str = modelGuildFolder.name;
        }
        return modelGuildFolder.copy(l, list, num, str);
    }

    public final Long component1() {
        return this.f2693id;
    }

    public final List<Long> component2() {
        return this.guildIds;
    }

    public final Integer component3() {
        return this.color;
    }

    public final String component4() {
        return this.name;
    }

    public final ModelGuildFolder copy(Long l, List<Long> list, Integer num, String str) {
        m.checkNotNullParameter(list, "guildIds");
        return new ModelGuildFolder(l, list, num, str);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModelGuildFolder)) {
            return false;
        }
        ModelGuildFolder modelGuildFolder = (ModelGuildFolder) obj;
        return m.areEqual(this.f2693id, modelGuildFolder.f2693id) && m.areEqual(this.guildIds, modelGuildFolder.guildIds) && m.areEqual(this.color, modelGuildFolder.color) && m.areEqual(this.name, modelGuildFolder.name);
    }

    public final Integer getColor() {
        return this.color;
    }

    public final List<Long> getGuildIds() {
        return this.guildIds;
    }

    public final Long getId() {
        return this.f2693id;
    }

    public final String getName() {
        return this.name;
    }

    public int hashCode() {
        Long l = this.f2693id;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        List<Long> list = this.guildIds;
        int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
        Integer num = this.color;
        int hashCode3 = (hashCode2 + (num != null ? num.hashCode() : 0)) * 31;
        String str = this.name;
        if (str != null) {
            i = str.hashCode();
        }
        return hashCode3 + i;
    }

    public String toString() {
        StringBuilder R = a.R("ModelGuildFolder(id=");
        R.append(this.f2693id);
        R.append(", guildIds=");
        R.append(this.guildIds);
        R.append(", color=");
        R.append(this.color);
        R.append(", name=");
        return a.H(R, this.name, ")");
    }

    public /* synthetic */ ModelGuildFolder(Long l, List list, Integer num, String str, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(l, list, (i & 4) != 0 ? null : num, (i & 8) != 0 ? null : str);
    }
}
