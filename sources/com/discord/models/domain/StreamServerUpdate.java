package com.discord.models.domain;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.Model;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.Ref$ObjectRef;
import rx.functions.Action1;
/* compiled from: ModelApplicationStream.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u001bB'\u0012\n\u0010\b\u001a\u00060\u0002j\u0002`\u0003\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0005J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0005J6\u0010\u000b\u001a\u00020\u00002\f\b\u0002\u0010\b\u001a\u00060\u0002j\u0002`\u00032\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\r\u0010\u0005J\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u001b\u0010\n\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0015\u001a\u0004\b\u0016\u0010\u0005R\u001d\u0010\b\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0015\u001a\u0004\b\u0017\u0010\u0005R\u001b\u0010\t\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0015\u001a\u0004\b\u0018\u0010\u0005¨\u0006\u001c"}, d2 = {"Lcom/discord/models/domain/StreamServerUpdate;", "", "", "Lcom/discord/primitives/StreamKey;", "component1", "()Ljava/lang/String;", "component2", "component3", "streamKey", "endpoint", "token", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/models/domain/StreamServerUpdate;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getToken", "getStreamKey", "getEndpoint", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "Parser", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StreamServerUpdate {
    private final String endpoint;
    private final String streamKey;
    private final String token;

    /* compiled from: ModelApplicationStream.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/models/domain/StreamServerUpdate$Parser;", "Lcom/discord/models/domain/Model$Parser;", "Lcom/discord/models/domain/StreamServerUpdate;", "Lcom/discord/models/domain/Model$JsonReader;", "reader", "parse", "(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/StreamServerUpdate;", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Parser implements Model.Parser<StreamServerUpdate> {
        public static final Parser INSTANCE = new Parser();

        private Parser() {
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // com.discord.models.domain.Model.Parser
        public StreamServerUpdate parse(final Model.JsonReader jsonReader) {
            final Ref$ObjectRef a02 = a.a0(jsonReader, "reader");
            a02.element = null;
            final Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
            ref$ObjectRef.element = null;
            final Ref$ObjectRef ref$ObjectRef2 = new Ref$ObjectRef();
            ref$ObjectRef2.element = null;
            jsonReader.nextObject(new Action1<String>() { // from class: com.discord.models.domain.StreamServerUpdate$Parser$parse$1
                public final void call(String str) {
                    if (str != null) {
                        int hashCode = str.hashCode();
                        if (hashCode != -1194435296) {
                            if (hashCode != 110541305) {
                                if (hashCode == 1741102485 && str.equals("endpoint")) {
                                    ref$ObjectRef.element = (T) jsonReader.nextStringOrNull();
                                    return;
                                }
                            } else if (str.equals("token")) {
                                ref$ObjectRef2.element = (T) jsonReader.nextStringOrNull();
                                return;
                            }
                        } else if (str.equals("stream_key")) {
                            Ref$ObjectRef ref$ObjectRef3 = Ref$ObjectRef.this;
                            T t = (T) jsonReader.nextStringOrNull();
                            m.checkNotNull(t);
                            ref$ObjectRef3.element = t;
                            return;
                        }
                    }
                    jsonReader.skipValue();
                }
            });
            T t = a02.element;
            if (t == 0) {
                m.throwUninitializedPropertyAccessException("streamKey");
            }
            return new StreamServerUpdate((String) t, (String) ref$ObjectRef.element, (String) ref$ObjectRef2.element);
        }
    }

    public StreamServerUpdate(String str, String str2, String str3) {
        m.checkNotNullParameter(str, "streamKey");
        this.streamKey = str;
        this.endpoint = str2;
        this.token = str3;
    }

    public static /* synthetic */ StreamServerUpdate copy$default(StreamServerUpdate streamServerUpdate, String str, String str2, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = streamServerUpdate.streamKey;
        }
        if ((i & 2) != 0) {
            str2 = streamServerUpdate.endpoint;
        }
        if ((i & 4) != 0) {
            str3 = streamServerUpdate.token;
        }
        return streamServerUpdate.copy(str, str2, str3);
    }

    public final String component1() {
        return this.streamKey;
    }

    public final String component2() {
        return this.endpoint;
    }

    public final String component3() {
        return this.token;
    }

    public final StreamServerUpdate copy(String str, String str2, String str3) {
        m.checkNotNullParameter(str, "streamKey");
        return new StreamServerUpdate(str, str2, str3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StreamServerUpdate)) {
            return false;
        }
        StreamServerUpdate streamServerUpdate = (StreamServerUpdate) obj;
        return m.areEqual(this.streamKey, streamServerUpdate.streamKey) && m.areEqual(this.endpoint, streamServerUpdate.endpoint) && m.areEqual(this.token, streamServerUpdate.token);
    }

    public final String getEndpoint() {
        return this.endpoint;
    }

    public final String getStreamKey() {
        return this.streamKey;
    }

    public final String getToken() {
        return this.token;
    }

    public int hashCode() {
        String str = this.streamKey;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.endpoint;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.token;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("StreamServerUpdate(streamKey=");
        R.append(this.streamKey);
        R.append(", endpoint=");
        R.append(this.endpoint);
        R.append(", token=");
        return a.H(R, this.token, ")");
    }
}
