package com.discord.models.domain;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.Model;
import com.discord.models.domain.ModelMemberVerificationForm;
import d0.z.d.m;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Ref$BooleanRef;
import kotlin.jvm.internal.Ref$ObjectRef;
import rx.functions.Action1;
/* compiled from: ModelMemberVerificationForm.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001:\u0003\u001d\u001e\u001fB\u001d\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\u0004\b\u001b\u0010\u001cJ\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0016\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\bHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ*\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00052\u000e\b\u0002\u0010\r\u001a\b\u0012\u0004\u0012\u00020\t0\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0007J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u001a\u0010\u0015\u001a\u00020\u00022\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u0019\u0010\f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0017\u001a\u0004\b\u0018\u0010\u0007R\u001f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\t0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u0019\u001a\u0004\b\u001a\u0010\u000b¨\u0006 "}, d2 = {"Lcom/discord/models/domain/ModelMemberVerificationForm;", "", "", "isFormOutdated", "()Z", "", "component1", "()Ljava/lang/String;", "", "Lcom/discord/models/domain/ModelMemberVerificationForm$FormField;", "component2", "()Ljava/util/List;", "version", "formFields", "copy", "(Ljava/lang/String;Ljava/util/List;)Lcom/discord/models/domain/ModelMemberVerificationForm;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getVersion", "Ljava/util/List;", "getFormFields", HookHelper.constructorName, "(Ljava/lang/String;Ljava/util/List;)V", "FormField", "MemberVerificationFieldType", "Parser", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelMemberVerificationForm {
    private final List<FormField> formFields;
    private final String version;

    /* compiled from: ModelMemberVerificationForm.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u000e\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001:\u00010BC\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\u0006\u0010\u0010\u001a\u00020\u0002\u0012\u0006\u0010\u0011\u001a\u00020\u0006\u0012\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00020\t\u0012\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00020\t\u0012\u0006\u0010\u0014\u001a\u00020\u0001¢\u0006\u0004\b.\u0010/J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0016\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0016\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00020\tHÆ\u0003¢\u0006\u0004\b\f\u0010\u000bJ\u0010\u0010\r\u001a\u00020\u0001HÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJX\u0010\u0015\u001a\u00020\u00002\b\b\u0002\u0010\u000f\u001a\u00020\u00022\b\b\u0002\u0010\u0010\u001a\u00020\u00022\b\b\u0002\u0010\u0011\u001a\u00020\u00062\u000e\b\u0002\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00020\t2\u000e\b\u0002\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00020\t2\b\b\u0002\u0010\u0014\u001a\u00020\u0001HÆ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0004J\u0010\u0010\u0019\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u001a\u0010\u001c\u001a\u00020\u00062\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u0019\u0010\u0010\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001e\u001a\u0004\b\u001f\u0010\u0004R\u001f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010 \u001a\u0004\b!\u0010\u000bR\u0013\u0010%\u001a\u00020\"8F@\u0006¢\u0006\u0006\u001a\u0004\b#\u0010$R\"\u0010\u0014\u001a\u00020\u00018\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0014\u0010&\u001a\u0004\b'\u0010\u000e\"\u0004\b(\u0010)R\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001e\u001a\u0004\b*\u0010\u0004R\u0019\u0010\u0011\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010+\u001a\u0004\b,\u0010\bR\u001f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010 \u001a\u0004\b-\u0010\u000b¨\u00061"}, d2 = {"Lcom/discord/models/domain/ModelMemberVerificationForm$FormField;", "", "", "component1", "()Ljava/lang/String;", "component2", "", "component3", "()Z", "", "component4", "()Ljava/util/List;", "component5", "component6", "()Ljava/lang/Object;", "fieldType", "label", "required", "values", "choices", "response", "copy", "(Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;Ljava/util/List;Ljava/lang/Object;)Lcom/discord/models/domain/ModelMemberVerificationForm$FormField;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getLabel", "Ljava/util/List;", "getValues", "Lcom/discord/models/domain/ModelMemberVerificationForm$MemberVerificationFieldType;", "getMemberVerificationFieldType", "()Lcom/discord/models/domain/ModelMemberVerificationForm$MemberVerificationFieldType;", "memberVerificationFieldType", "Ljava/lang/Object;", "getResponse", "setResponse", "(Ljava/lang/Object;)V", "getFieldType", "Z", "getRequired", "getChoices", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;Ljava/util/List;Ljava/lang/Object;)V", "Parser", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class FormField {
        private final List<String> choices;
        private final String fieldType;
        private final String label;
        private final boolean required;
        private Object response;
        private final List<String> values;

        /* compiled from: ModelMemberVerificationForm.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/models/domain/ModelMemberVerificationForm$FormField$Parser;", "Lcom/discord/models/domain/Model$Parser;", "Lcom/discord/models/domain/ModelMemberVerificationForm$FormField;", "Lcom/discord/models/domain/Model$JsonReader;", "reader", "parse", "(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelMemberVerificationForm$FormField;", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Parser implements Model.Parser<FormField> {
            public static final Parser INSTANCE = new Parser();

            private Parser() {
            }

            /* JADX WARN: Can't rename method to resolve collision */
            @Override // com.discord.models.domain.Model.Parser
            public FormField parse(final Model.JsonReader jsonReader) {
                final Ref$ObjectRef a02 = a.a0(jsonReader, "reader");
                a02.element = null;
                final Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
                ref$ObjectRef.element = null;
                final Ref$BooleanRef ref$BooleanRef = new Ref$BooleanRef();
                ref$BooleanRef.element = false;
                final Ref$ObjectRef ref$ObjectRef2 = new Ref$ObjectRef();
                ref$ObjectRef2.element = null;
                final Ref$ObjectRef ref$ObjectRef3 = new Ref$ObjectRef();
                ref$ObjectRef3.element = null;
                final Ref$ObjectRef ref$ObjectRef4 = new Ref$ObjectRef();
                ref$ObjectRef4.element = null;
                jsonReader.nextObject(new Action1<String>() { // from class: com.discord.models.domain.ModelMemberVerificationForm$FormField$Parser$parse$1
                    public final void call(String str) {
                        if (str != null) {
                            switch (str.hashCode()) {
                                case -823812830:
                                    if (str.equals("values")) {
                                        Ref$ObjectRef ref$ObjectRef5 = ref$ObjectRef2;
                                        T t = (T) jsonReader.nextList(new Model.JsonReader.ItemFactory<String>() { // from class: com.discord.models.domain.ModelMemberVerificationForm$FormField$Parser$parse$1.1
                                            @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                                            public final String get() {
                                                return jsonReader.nextString("");
                                            }
                                        });
                                        m.checkNotNullExpressionValue(t, "reader.nextList { reader.nextString(\"\") }");
                                        ref$ObjectRef5.element = t;
                                        return;
                                    }
                                    break;
                                case -393139297:
                                    if (str.equals("required")) {
                                        ref$BooleanRef.element = jsonReader.nextBoolean(false);
                                        return;
                                    }
                                    break;
                                case -340323263:
                                    if (str.equals("response")) {
                                        T t2 = Ref$ObjectRef.this.element;
                                        if (t2 == null) {
                                            m.throwUninitializedPropertyAccessException("fieldType");
                                        }
                                        String str2 = (String) t2;
                                        if (str2.hashCode() == 1121961648 && str2.equals("MULTIPLE_CHOICE")) {
                                            ref$ObjectRef4.element = (T) Integer.valueOf(jsonReader.nextInt(-1));
                                            return;
                                        }
                                        Ref$ObjectRef ref$ObjectRef6 = ref$ObjectRef4;
                                        T t3 = (T) jsonReader.nextString("");
                                        m.checkNotNullExpressionValue(t3, "reader.nextString(\"\")");
                                        ref$ObjectRef6.element = t3;
                                        return;
                                    }
                                    break;
                                case 102727412:
                                    if (str.equals("label")) {
                                        Ref$ObjectRef ref$ObjectRef7 = ref$ObjectRef;
                                        T t4 = (T) jsonReader.nextString("");
                                        m.checkNotNullExpressionValue(t4, "reader.nextString(\"\")");
                                        ref$ObjectRef7.element = t4;
                                        return;
                                    }
                                    break;
                                case 576861023:
                                    if (str.equals("field_type")) {
                                        Ref$ObjectRef ref$ObjectRef8 = Ref$ObjectRef.this;
                                        T t5 = (T) jsonReader.nextString("");
                                        m.checkNotNullExpressionValue(t5, "reader.nextString(\"\")");
                                        ref$ObjectRef8.element = t5;
                                        return;
                                    }
                                    break;
                                case 751720178:
                                    if (str.equals("choices")) {
                                        Ref$ObjectRef ref$ObjectRef9 = ref$ObjectRef3;
                                        T t6 = (T) jsonReader.nextList(new Model.JsonReader.ItemFactory<String>() { // from class: com.discord.models.domain.ModelMemberVerificationForm$FormField$Parser$parse$1.2
                                            @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                                            public final String get() {
                                                return jsonReader.nextString("");
                                            }
                                        });
                                        m.checkNotNullExpressionValue(t6, "reader.nextList { reader.nextString(\"\") }");
                                        ref$ObjectRef9.element = t6;
                                        return;
                                    }
                                    break;
                            }
                        }
                        jsonReader.skipValue();
                    }
                });
                T t = a02.element;
                if (t == 0) {
                    m.throwUninitializedPropertyAccessException("fieldType");
                }
                String str = (String) t;
                T t2 = ref$ObjectRef.element;
                if (t2 == 0) {
                    m.throwUninitializedPropertyAccessException("label");
                }
                String str2 = (String) t2;
                boolean z2 = ref$BooleanRef.element;
                T t3 = ref$ObjectRef2.element;
                if (t3 == 0) {
                    m.throwUninitializedPropertyAccessException("values");
                }
                List list = (List) t3;
                T t4 = ref$ObjectRef3.element;
                if (t4 == 0) {
                    m.throwUninitializedPropertyAccessException("choices");
                }
                List list2 = (List) t4;
                T t5 = ref$ObjectRef4.element;
                if (t5 == 0) {
                    m.throwUninitializedPropertyAccessException("response");
                }
                return new FormField(str, str2, z2, list, list2, t5);
            }
        }

        public FormField(String str, String str2, boolean z2, List<String> list, List<String> list2, Object obj) {
            m.checkNotNullParameter(str, "fieldType");
            m.checkNotNullParameter(str2, "label");
            m.checkNotNullParameter(list, "values");
            m.checkNotNullParameter(list2, "choices");
            m.checkNotNullParameter(obj, "response");
            this.fieldType = str;
            this.label = str2;
            this.required = z2;
            this.values = list;
            this.choices = list2;
            this.response = obj;
        }

        public static /* synthetic */ FormField copy$default(FormField formField, String str, String str2, boolean z2, List list, List list2, Object obj, int i, Object obj2) {
            if ((i & 1) != 0) {
                str = formField.fieldType;
            }
            if ((i & 2) != 0) {
                str2 = formField.label;
            }
            String str3 = str2;
            if ((i & 4) != 0) {
                z2 = formField.required;
            }
            boolean z3 = z2;
            List<String> list3 = list;
            if ((i & 8) != 0) {
                list3 = formField.values;
            }
            List list4 = list3;
            List<String> list5 = list2;
            if ((i & 16) != 0) {
                list5 = formField.choices;
            }
            List list6 = list5;
            if ((i & 32) != 0) {
                obj = formField.response;
            }
            return formField.copy(str, str3, z3, list4, list6, obj);
        }

        public final String component1() {
            return this.fieldType;
        }

        public final String component2() {
            return this.label;
        }

        public final boolean component3() {
            return this.required;
        }

        public final List<String> component4() {
            return this.values;
        }

        public final List<String> component5() {
            return this.choices;
        }

        public final Object component6() {
            return this.response;
        }

        public final FormField copy(String str, String str2, boolean z2, List<String> list, List<String> list2, Object obj) {
            m.checkNotNullParameter(str, "fieldType");
            m.checkNotNullParameter(str2, "label");
            m.checkNotNullParameter(list, "values");
            m.checkNotNullParameter(list2, "choices");
            m.checkNotNullParameter(obj, "response");
            return new FormField(str, str2, z2, list, list2, obj);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof FormField)) {
                return false;
            }
            FormField formField = (FormField) obj;
            return m.areEqual(this.fieldType, formField.fieldType) && m.areEqual(this.label, formField.label) && this.required == formField.required && m.areEqual(this.values, formField.values) && m.areEqual(this.choices, formField.choices) && m.areEqual(this.response, formField.response);
        }

        public final List<String> getChoices() {
            return this.choices;
        }

        public final String getFieldType() {
            return this.fieldType;
        }

        public final String getLabel() {
            return this.label;
        }

        public final MemberVerificationFieldType getMemberVerificationFieldType() {
            String str = this.fieldType;
            switch (str.hashCode()) {
                case -708597224:
                    if (str.equals("TEXT_INPUT")) {
                        return MemberVerificationFieldType.TEXT_INPUT;
                    }
                    break;
                case 79712615:
                    if (str.equals("TERMS")) {
                        return MemberVerificationFieldType.TERMS;
                    }
                    break;
                case 440916302:
                    if (str.equals("PARAGRAPH")) {
                        return MemberVerificationFieldType.PARAGRAPH;
                    }
                    break;
                case 1121961648:
                    if (str.equals("MULTIPLE_CHOICE")) {
                        return MemberVerificationFieldType.MULTIPLE_CHOICE;
                    }
                    break;
            }
            return MemberVerificationFieldType.UNKNOWN;
        }

        public final boolean getRequired() {
            return this.required;
        }

        public final Object getResponse() {
            return this.response;
        }

        public final List<String> getValues() {
            return this.values;
        }

        public int hashCode() {
            String str = this.fieldType;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.label;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            boolean z2 = this.required;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (hashCode2 + i2) * 31;
            List<String> list = this.values;
            int hashCode3 = (i4 + (list != null ? list.hashCode() : 0)) * 31;
            List<String> list2 = this.choices;
            int hashCode4 = (hashCode3 + (list2 != null ? list2.hashCode() : 0)) * 31;
            Object obj = this.response;
            if (obj != null) {
                i = obj.hashCode();
            }
            return hashCode4 + i;
        }

        public final void setResponse(Object obj) {
            m.checkNotNullParameter(obj, "<set-?>");
            this.response = obj;
        }

        public String toString() {
            StringBuilder R = a.R("FormField(fieldType=");
            R.append(this.fieldType);
            R.append(", label=");
            R.append(this.label);
            R.append(", required=");
            R.append(this.required);
            R.append(", values=");
            R.append(this.values);
            R.append(", choices=");
            R.append(this.choices);
            R.append(", response=");
            R.append(this.response);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: ModelMemberVerificationForm.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\b\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\t"}, d2 = {"Lcom/discord/models/domain/ModelMemberVerificationForm$MemberVerificationFieldType;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "UNKNOWN", "TERMS", "TEXT_INPUT", "PARAGRAPH", "MULTIPLE_CHOICE", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum MemberVerificationFieldType {
        UNKNOWN,
        TERMS,
        TEXT_INPUT,
        PARAGRAPH,
        MULTIPLE_CHOICE
    }

    /* compiled from: ModelMemberVerificationForm.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/models/domain/ModelMemberVerificationForm$Parser;", "Lcom/discord/models/domain/Model$Parser;", "Lcom/discord/models/domain/ModelMemberVerificationForm;", "Lcom/discord/models/domain/Model$JsonReader;", "reader", "parse", "(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/domain/ModelMemberVerificationForm;", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Parser implements Model.Parser<ModelMemberVerificationForm> {
        public static final Parser INSTANCE = new Parser();

        private Parser() {
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // com.discord.models.domain.Model.Parser
        public ModelMemberVerificationForm parse(final Model.JsonReader jsonReader) {
            final Ref$ObjectRef a02 = a.a0(jsonReader, "reader");
            a02.element = null;
            final Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
            ref$ObjectRef.element = null;
            jsonReader.nextObject(new Action1<String>() { // from class: com.discord.models.domain.ModelMemberVerificationForm$Parser$parse$1
                public final void call(String str) {
                    if (str != null) {
                        int hashCode = str.hashCode();
                        if (hashCode != -708425068) {
                            if (hashCode == 351608024 && str.equals("version")) {
                                Ref$ObjectRef ref$ObjectRef2 = Ref$ObjectRef.this;
                                T t = (T) jsonReader.nextString("");
                                m.checkNotNullExpressionValue(t, "reader.nextString(\"\")");
                                ref$ObjectRef2.element = t;
                                return;
                            }
                        } else if (str.equals("form_fields")) {
                            Ref$ObjectRef ref$ObjectRef3 = ref$ObjectRef;
                            T t2 = (T) jsonReader.nextList(new Model.JsonReader.ItemFactory<ModelMemberVerificationForm.FormField>() { // from class: com.discord.models.domain.ModelMemberVerificationForm$Parser$parse$1.1
                                /* JADX WARN: Can't rename method to resolve collision */
                                @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                                public final ModelMemberVerificationForm.FormField get() {
                                    return ModelMemberVerificationForm.FormField.Parser.INSTANCE.parse(jsonReader);
                                }
                            });
                            m.checkNotNullExpressionValue(t2, "reader.nextList { FormField.Parser.parse(reader) }");
                            ref$ObjectRef3.element = t2;
                            return;
                        }
                    }
                    jsonReader.skipValue();
                }
            });
            T t = a02.element;
            if (t == 0) {
                m.throwUninitializedPropertyAccessException("version");
            }
            String str = (String) t;
            T t2 = ref$ObjectRef.element;
            if (t2 == 0) {
                m.throwUninitializedPropertyAccessException("formFields");
            }
            return new ModelMemberVerificationForm(str, (List) t2);
        }
    }

    public ModelMemberVerificationForm(String str, List<FormField> list) {
        m.checkNotNullParameter(str, "version");
        m.checkNotNullParameter(list, "formFields");
        this.version = str;
        this.formFields = list;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ ModelMemberVerificationForm copy$default(ModelMemberVerificationForm modelMemberVerificationForm, String str, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            str = modelMemberVerificationForm.version;
        }
        if ((i & 2) != 0) {
            list = modelMemberVerificationForm.formFields;
        }
        return modelMemberVerificationForm.copy(str, list);
    }

    public final String component1() {
        return this.version;
    }

    public final List<FormField> component2() {
        return this.formFields;
    }

    public final ModelMemberVerificationForm copy(String str, List<FormField> list) {
        m.checkNotNullParameter(str, "version");
        m.checkNotNullParameter(list, "formFields");
        return new ModelMemberVerificationForm(str, list);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModelMemberVerificationForm)) {
            return false;
        }
        ModelMemberVerificationForm modelMemberVerificationForm = (ModelMemberVerificationForm) obj;
        return m.areEqual(this.version, modelMemberVerificationForm.version) && m.areEqual(this.formFields, modelMemberVerificationForm.formFields);
    }

    public final List<FormField> getFormFields() {
        return this.formFields;
    }

    public final String getVersion() {
        return this.version;
    }

    public int hashCode() {
        String str = this.version;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        List<FormField> list = this.formFields;
        if (list != null) {
            i = list.hashCode();
        }
        return hashCode + i;
    }

    public final boolean isFormOutdated() {
        boolean z2;
        List<FormField> list = this.formFields;
        if (!(list instanceof Collection) || !list.isEmpty()) {
            for (FormField formField : list) {
                if (formField.getMemberVerificationFieldType() == MemberVerificationFieldType.UNKNOWN) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    return true;
                }
            }
        }
        return false;
    }

    public String toString() {
        StringBuilder R = a.R("ModelMemberVerificationForm(version=");
        R.append(this.version);
        R.append(", formFields=");
        return a.K(R, this.formFields, ")");
    }
}
