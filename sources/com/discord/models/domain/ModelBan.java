package com.discord.models.domain;

import androidx.annotation.Nullable;
import b.d.b.a.a;
import com.discord.api.user.User;
import com.discord.models.deserialization.gson.InboundGatewayGsonParser;
import com.discord.models.domain.Model;
import java.io.IOException;
/* loaded from: classes.dex */
public class ModelBan implements Model {
    private long guildId;
    @Nullable
    private String reason;
    private User user;

    @Override // com.discord.models.domain.Model
    public void assignField(Model.JsonReader jsonReader) throws IOException {
        String nextName = jsonReader.nextName();
        nextName.hashCode();
        char c = 65535;
        switch (nextName.hashCode()) {
            case -1306538777:
                if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_GUILD_ID)) {
                    c = 0;
                    break;
                }
                break;
            case -934964668:
                if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_REASON)) {
                    c = 1;
                    break;
                }
                break;
            case 3599307:
                if (nextName.equals("user")) {
                    c = 2;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                this.guildId = jsonReader.nextLong(-1L);
                return;
            case 1:
                this.reason = jsonReader.nextStringOrNull();
                return;
            case 2:
                this.user = (User) InboundGatewayGsonParser.fromJson(jsonReader, User.class);
                return;
            default:
                jsonReader.skipValue();
                return;
        }
    }

    public boolean canEqual(Object obj) {
        return obj instanceof ModelBan;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ModelBan)) {
            return false;
        }
        ModelBan modelBan = (ModelBan) obj;
        if (!modelBan.canEqual(this) || getGuildId() != modelBan.getGuildId()) {
            return false;
        }
        User user = getUser();
        User user2 = modelBan.getUser();
        if (user != null ? !user.equals(user2) : user2 != null) {
            return false;
        }
        String reason = getReason();
        String reason2 = modelBan.getReason();
        return reason != null ? reason.equals(reason2) : reason2 == null;
    }

    public long getGuildId() {
        return this.guildId;
    }

    @Nullable
    public String getReason() {
        return this.reason;
    }

    public User getUser() {
        return this.user;
    }

    public int hashCode() {
        long guildId = getGuildId();
        User user = getUser();
        int i = 43;
        int hashCode = ((((int) (guildId ^ (guildId >>> 32))) + 59) * 59) + (user == null ? 43 : user.hashCode());
        String reason = getReason();
        int i2 = hashCode * 59;
        if (reason != null) {
            i = reason.hashCode();
        }
        return i2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("ModelBan(user=");
        R.append(getUser());
        R.append(", guildId=");
        R.append(getGuildId());
        R.append(", reason=");
        R.append(getReason());
        R.append(")");
        return R.toString();
    }
}
