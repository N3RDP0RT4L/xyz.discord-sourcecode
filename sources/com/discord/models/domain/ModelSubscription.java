package com.discord.models.domain;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.premium.SubscriptionPlanType;
import com.discord.utilities.time.TimeUtils;
import d0.z.d.m;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ModelSubscription.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0010\u0000\n\u0002\b\u001d\b\u0086\b\u0018\u00002\u00020\u0001:\u0006]^_`abB\u008b\u0001\u0012\u0006\u00104\u001a\u00020 \u0012\u0006\u00105\u001a\u00020 \u0012\b\u00106\u001a\u0004\u0018\u00010 \u0012\u0006\u00107\u001a\u00020 \u0012\u0006\u00108\u001a\u00020 \u0012\b\u00109\u001a\u0004\u0018\u00010 \u0012\b\u0010:\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010;\u001a\u0004\u0018\u00010 \u0012\b\u0010<\u001a\u0004\u0018\u00010 \u0012\b\u0010=\u001a\u0004\u0018\u00010,\u0012\f\u0010>\u001a\b\u0012\u0004\u0012\u0002000/\u0012\b\u0010?\u001a\u0004\u0018\u00010 \u0012\u0006\u0010@\u001a\u00020\u0002\u0012\u0006\u0010A\u001a\u00020\u0002¢\u0006\u0004\b[\u0010\\J\u0010\u0010\u0003\u001a\u00020\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÂ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\r\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\u0007\u0010\bJ\r\u0010\n\u001a\u00020\t¢\u0006\u0004\b\n\u0010\u000bJ\r\u0010\r\u001a\u00020\f¢\u0006\u0004\b\r\u0010\u000eJ\r\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b\u0010\u0010\u0011J\r\u0010\u0012\u001a\u00020\u000f¢\u0006\u0004\b\u0012\u0010\u0011J\r\u0010\u0013\u001a\u00020\u000f¢\u0006\u0004\b\u0013\u0010\u0011J\r\u0010\u0014\u001a\u00020\u000f¢\u0006\u0004\b\u0014\u0010\u0011J\r\u0010\u0016\u001a\u00020\u0015¢\u0006\u0004\b\u0016\u0010\u0017J\u001f\u0010\u001b\u001a\u00020\u000f2\u0010\u0010\u001a\u001a\f\u0012\b\u0012\u00060\u0015j\u0002`\u00190\u0018¢\u0006\u0004\b\u001b\u0010\u001cJ\u0019\u0010\u001e\u001a\u00020\u000f2\n\u0010\u001d\u001a\u00060\u0015j\u0002`\u0019¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010!\u001a\u00020 HÆ\u0003¢\u0006\u0004\b!\u0010\"J\u0010\u0010#\u001a\u00020 HÆ\u0003¢\u0006\u0004\b#\u0010\"J\u0012\u0010$\u001a\u0004\u0018\u00010 HÆ\u0003¢\u0006\u0004\b$\u0010\"J\u0010\u0010%\u001a\u00020 HÆ\u0003¢\u0006\u0004\b%\u0010\"J\u0010\u0010&\u001a\u00020 HÆ\u0003¢\u0006\u0004\b&\u0010\"J\u0012\u0010'\u001a\u0004\u0018\u00010 HÆ\u0003¢\u0006\u0004\b'\u0010\"J\u0012\u0010(\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b(\u0010)J\u0012\u0010*\u001a\u0004\u0018\u00010 HÆ\u0003¢\u0006\u0004\b*\u0010\"J\u0012\u0010+\u001a\u0004\u0018\u00010 HÆ\u0003¢\u0006\u0004\b+\u0010\"J\u0012\u0010-\u001a\u0004\u0018\u00010,HÆ\u0003¢\u0006\u0004\b-\u0010.J\u0016\u00101\u001a\b\u0012\u0004\u0012\u0002000/HÆ\u0003¢\u0006\u0004\b1\u00102J\u0012\u00103\u001a\u0004\u0018\u00010 HÆ\u0003¢\u0006\u0004\b3\u0010\"J°\u0001\u0010B\u001a\u00020\u00002\b\b\u0002\u00104\u001a\u00020 2\b\b\u0002\u00105\u001a\u00020 2\n\b\u0002\u00106\u001a\u0004\u0018\u00010 2\b\b\u0002\u00107\u001a\u00020 2\b\b\u0002\u00108\u001a\u00020 2\n\b\u0002\u00109\u001a\u0004\u0018\u00010 2\n\b\u0002\u0010:\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010;\u001a\u0004\u0018\u00010 2\n\b\u0002\u0010<\u001a\u0004\u0018\u00010 2\n\b\u0002\u0010=\u001a\u0004\u0018\u00010,2\u000e\b\u0002\u0010>\u001a\b\u0012\u0004\u0012\u0002000/2\n\b\u0002\u0010?\u001a\u0004\u0018\u00010 2\b\b\u0002\u0010@\u001a\u00020\u00022\b\b\u0002\u0010A\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\bB\u0010CJ\u0010\u0010D\u001a\u00020 HÖ\u0001¢\u0006\u0004\bD\u0010\"J\u0010\u0010E\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\bE\u0010\u0004J\u001a\u0010H\u001a\u00020\u000f2\b\u0010G\u001a\u0004\u0018\u00010FHÖ\u0003¢\u0006\u0004\bH\u0010IR\u001b\u0010;\u001a\u0004\u0018\u00010 8\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010J\u001a\u0004\bK\u0010\"R\u0019\u00104\u001a\u00020 8\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010J\u001a\u0004\bL\u0010\"R\u0016\u0010A\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bA\u0010MR\u001b\u00106\u001a\u0004\u0018\u00010 8\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010J\u001a\u0004\bN\u0010\"R\u001b\u00109\u001a\u0004\u0018\u00010 8\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010J\u001a\u0004\bO\u0010\"R\u001b\u0010<\u001a\u0004\u0018\u00010 8\u0006@\u0006¢\u0006\f\n\u0004\b<\u0010J\u001a\u0004\bP\u0010\"R\u001b\u0010=\u001a\u0004\u0018\u00010,8\u0006@\u0006¢\u0006\f\n\u0004\b=\u0010Q\u001a\u0004\bR\u0010.R\u001b\u0010:\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010S\u001a\u0004\bT\u0010)R\u001b\u0010?\u001a\u0004\u0018\u00010 8\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010J\u001a\u0004\bU\u0010\"R\u0019\u00108\u001a\u00020 8\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010J\u001a\u0004\bV\u0010\"R\u0019\u00107\u001a\u00020 8\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010J\u001a\u0004\bW\u0010\"R\"\u0010>\u001a\b\u0012\u0004\u0012\u0002000/8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b>\u0010X\u001a\u0004\bY\u00102R\u0016\u0010@\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b@\u0010MR\u0019\u00105\u001a\u00020 8\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010J\u001a\u0004\bZ\u0010\"¨\u0006c"}, d2 = {"Lcom/discord/models/domain/ModelSubscription;", "Lcom/discord/models/domain/HasSubscriptionItems;", "", "component13", "()I", "component14", "Lcom/discord/models/domain/premium/SubscriptionPlanType;", "getPlanType", "()Lcom/discord/models/domain/premium/SubscriptionPlanType;", "Lcom/discord/models/domain/ModelSubscription$Status;", "getStatus", "()Lcom/discord/models/domain/ModelSubscription$Status;", "Lcom/discord/models/domain/ModelSubscription$Type;", "getType", "()Lcom/discord/models/domain/ModelSubscription$Type;", "", "isAppleSubscription", "()Z", "isGoogleSubscription", "isMobileManaged", "isNonePlan", "", "getAccountHoldEstimatedExpirationTimestamp", "()J", "", "Lcom/discord/primitives/PlanId;", "planIds", "hasAnyOfPlans", "(Ljava/util/Set;)Z", "planId", "hasPlan", "(J)Z", "", "component1", "()Ljava/lang/String;", "component2", "component3", "component4", "component5", "component6", "component7", "()Ljava/lang/Integer;", "component8", "component9", "Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;", "component10", "()Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;", "", "Lcom/discord/models/domain/ModelSubscription$SubscriptionItem;", "component11", "()Ljava/util/List;", "component12", ModelAuditLogEntry.CHANGE_KEY_ID, "createdAt", "canceledAt", "currentPeriodStart", "currentPeriodEnd", "paymentSourceId", "paymentGateway", "trialId", "trialEndsAt", "renewalMutations", "items", "paymentGatewayPlanId", "status", "type", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;Ljava/util/List;Ljava/lang/String;II)Lcom/discord/models/domain/ModelSubscription;", "toString", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getTrialId", "getId", "I", "getCanceledAt", "getPaymentSourceId", "getTrialEndsAt", "Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;", "getRenewalMutations", "Ljava/lang/Integer;", "getPaymentGateway", "getPaymentGatewayPlanId", "getCurrentPeriodEnd", "getCurrentPeriodStart", "Ljava/util/List;", "getItems", "getCreatedAt", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;Ljava/util/List;Ljava/lang/String;II)V", "PaymentGateway", "Status", "SubscriptionAdditionalPlan", "SubscriptionItem", "SubscriptionRenewalMutations", "Type", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelSubscription extends HasSubscriptionItems {
    private final String canceledAt;
    private final String createdAt;
    private final String currentPeriodEnd;
    private final String currentPeriodStart;

    /* renamed from: id  reason: collision with root package name */
    private final String f2705id;
    private final List<SubscriptionItem> items;
    private final Integer paymentGateway;
    private final String paymentGatewayPlanId;
    private final String paymentSourceId;
    private final SubscriptionRenewalMutations renewalMutations;
    private final int status;
    private final String trialEndsAt;
    private final String trialId;
    private final int type;

    /* compiled from: ModelSubscription.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\r\b\u0086\u0001\u0018\u0000 \n2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\nB\u0019\b\u0002\u0012\u000e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003¢\u0006\u0004\b\b\u0010\tR!\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007j\u0002\b\u000bj\u0002\b\fj\u0002\b\rj\u0002\b\u000ej\u0002\b\u000f¨\u0006\u0010"}, d2 = {"Lcom/discord/models/domain/ModelSubscription$PaymentGateway;", "", "", "Lcom/discord/primitives/PaymentGatewayId;", "intRepresentation", "Ljava/lang/Integer;", "getIntRepresentation", "()Ljava/lang/Integer;", HookHelper.constructorName, "(Ljava/lang/String;ILjava/lang/Integer;)V", "Companion", "STRIPE", "BRAINTREE", "APPLE", "GOOGLE", "UNKNOWN", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum PaymentGateway {
        STRIPE(1),
        BRAINTREE(2),
        APPLE(3),
        GOOGLE(4),
        UNKNOWN(null);
        
        public static final Companion Companion = new Companion(null);
        private final Integer intRepresentation;

        /* compiled from: ModelSubscription.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tJ\u001d\u0010\u0006\u001a\u00020\u00052\u000e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/models/domain/ModelSubscription$PaymentGateway$Companion;", "", "", "Lcom/discord/primitives/PaymentGatewayId;", "paymentGatewayInt", "Lcom/discord/models/domain/ModelSubscription$PaymentGateway;", "from", "(Ljava/lang/Integer;)Lcom/discord/models/domain/ModelSubscription$PaymentGateway;", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Companion {
            private Companion() {
            }

            public final PaymentGateway from(Integer num) {
                PaymentGateway paymentGateway;
                PaymentGateway[] values = PaymentGateway.values();
                int i = 0;
                while (true) {
                    if (i >= 5) {
                        paymentGateway = null;
                        break;
                    }
                    paymentGateway = values[i];
                    if (m.areEqual(paymentGateway.getIntRepresentation(), num)) {
                        break;
                    }
                    i++;
                }
                return paymentGateway != null ? paymentGateway : PaymentGateway.UNKNOWN;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        PaymentGateway(Integer num) {
            this.intRepresentation = num;
        }

        public final Integer getIntRepresentation() {
            return this.intRepresentation;
        }
    }

    /* compiled from: ModelSubscription.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\r\b\u0086\u0001\u0018\u0000 \r2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\rB\u0011\b\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\u000b\u0010\fJ\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\r\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0004R\u0019\u0010\u0007\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\t\u0010\nj\u0002\b\u000ej\u0002\b\u000fj\u0002\b\u0010j\u0002\b\u0011j\u0002\b\u0012¨\u0006\u0013"}, d2 = {"Lcom/discord/models/domain/ModelSubscription$Status;", "", "", "isCanceled", "()Z", "isAccountHold", "", "intRepresentation", "I", "getIntRepresentation", "()I", HookHelper.constructorName, "(Ljava/lang/String;II)V", "Companion", "UNPAID", "ACTIVE", "PAST_DUE", "CANCELED", "ACCOUNT_HOLD", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum Status {
        UNPAID(0),
        ACTIVE(1),
        PAST_DUE(2),
        CANCELED(3),
        ACCOUNT_HOLD(6);
        
        public static final Companion Companion = new Companion(null);
        private final int intRepresentation;

        /* compiled from: ModelSubscription.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/models/domain/ModelSubscription$Status$Companion;", "", "", "statusInt", "Lcom/discord/models/domain/ModelSubscription$Status;", "from", "(I)Lcom/discord/models/domain/ModelSubscription$Status;", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Status from(int i) {
                Status status;
                Status[] values = Status.values();
                int i2 = 0;
                while (true) {
                    if (i2 >= 5) {
                        status = null;
                        break;
                    }
                    status = values[i2];
                    if (status.getIntRepresentation() == i) {
                        break;
                    }
                    i2++;
                }
                if (status != null) {
                    return status;
                }
                throw new IllegalArgumentException(a.p("unsupported subscription status: ", i));
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        Status(int i) {
            this.intRepresentation = i;
        }

        public final int getIntRepresentation() {
            return this.intRepresentation;
        }

        public final boolean isAccountHold() {
            return this == ACCOUNT_HOLD;
        }

        public final boolean isCanceled() {
            return this == CANCELED;
        }
    }

    /* compiled from: ModelSubscription.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u001b\u0012\n\u0010\t\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\n\u001a\u00020\u0006¢\u0006\u0004\b\u0019\u0010\u001aJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ(\u0010\u000b\u001a\u00020\u00002\f\b\u0002\u0010\t\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\n\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0010\u0010\bJ\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u0019\u0010\n\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0015\u001a\u0004\b\u0016\u0010\bR\u001d\u0010\t\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0005¨\u0006\u001b"}, d2 = {"Lcom/discord/models/domain/ModelSubscription$SubscriptionAdditionalPlan;", "", "", "Lcom/discord/primitives/PlanId;", "component1", "()J", "", "component2", "()I", "planId", "quantity", "copy", "(JI)Lcom/discord/models/domain/ModelSubscription$SubscriptionAdditionalPlan;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getQuantity", "J", "getPlanId", HookHelper.constructorName, "(JI)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class SubscriptionAdditionalPlan {
        private final long planId;
        private final int quantity;

        public SubscriptionAdditionalPlan(long j, int i) {
            this.planId = j;
            this.quantity = i;
        }

        public static /* synthetic */ SubscriptionAdditionalPlan copy$default(SubscriptionAdditionalPlan subscriptionAdditionalPlan, long j, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                j = subscriptionAdditionalPlan.planId;
            }
            if ((i2 & 2) != 0) {
                i = subscriptionAdditionalPlan.quantity;
            }
            return subscriptionAdditionalPlan.copy(j, i);
        }

        public final long component1() {
            return this.planId;
        }

        public final int component2() {
            return this.quantity;
        }

        public final SubscriptionAdditionalPlan copy(long j, int i) {
            return new SubscriptionAdditionalPlan(j, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof SubscriptionAdditionalPlan)) {
                return false;
            }
            SubscriptionAdditionalPlan subscriptionAdditionalPlan = (SubscriptionAdditionalPlan) obj;
            return this.planId == subscriptionAdditionalPlan.planId && this.quantity == subscriptionAdditionalPlan.quantity;
        }

        public final long getPlanId() {
            return this.planId;
        }

        public final int getQuantity() {
            return this.quantity;
        }

        public int hashCode() {
            long j = this.planId;
            return (((int) (j ^ (j >>> 32))) * 31) + this.quantity;
        }

        public String toString() {
            StringBuilder R = a.R("SubscriptionAdditionalPlan(planId=");
            R.append(this.planId);
            R.append(", quantity=");
            return a.A(R, this.quantity, ")");
        }
    }

    /* compiled from: ModelSubscription.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\n\u001a\u00020\u0002\u0012\n\u0010\u000b\u001a\u00060\u0002j\u0002`\u0005\u0012\u0006\u0010\f\u001a\u00020\u0007¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0014\u0010\u0006\u001a\u00060\u0002j\u0002`\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J\u0010\u0010\b\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ2\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u00022\f\b\u0002\u0010\u000b\u001a\u00060\u0002j\u0002`\u00052\b\b\u0002\u0010\f\u001a\u00020\u0007HÆ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\u0012\u0010\tJ\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u0019\u0010\f\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0017\u001a\u0004\b\u0018\u0010\tR\u001d\u0010\u000b\u001a\u00060\u0002j\u0002`\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004R\u0019\u0010\n\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0019\u001a\u0004\b\u001b\u0010\u0004¨\u0006\u001e"}, d2 = {"Lcom/discord/models/domain/ModelSubscription$SubscriptionItem;", "", "", "component1", "()J", "Lcom/discord/primitives/PlanId;", "component2", "", "component3", "()I", ModelAuditLogEntry.CHANGE_KEY_ID, "planId", "quantity", "copy", "(JJI)Lcom/discord/models/domain/ModelSubscription$SubscriptionItem;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getQuantity", "J", "getPlanId", "getId", HookHelper.constructorName, "(JJI)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class SubscriptionItem {

        /* renamed from: id  reason: collision with root package name */
        private final long f2706id;
        private final long planId;
        private final int quantity;

        public SubscriptionItem(long j, long j2, int i) {
            this.f2706id = j;
            this.planId = j2;
            this.quantity = i;
        }

        public static /* synthetic */ SubscriptionItem copy$default(SubscriptionItem subscriptionItem, long j, long j2, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                j = subscriptionItem.f2706id;
            }
            long j3 = j;
            if ((i2 & 2) != 0) {
                j2 = subscriptionItem.planId;
            }
            long j4 = j2;
            if ((i2 & 4) != 0) {
                i = subscriptionItem.quantity;
            }
            return subscriptionItem.copy(j3, j4, i);
        }

        public final long component1() {
            return this.f2706id;
        }

        public final long component2() {
            return this.planId;
        }

        public final int component3() {
            return this.quantity;
        }

        public final SubscriptionItem copy(long j, long j2, int i) {
            return new SubscriptionItem(j, j2, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof SubscriptionItem)) {
                return false;
            }
            SubscriptionItem subscriptionItem = (SubscriptionItem) obj;
            return this.f2706id == subscriptionItem.f2706id && this.planId == subscriptionItem.planId && this.quantity == subscriptionItem.quantity;
        }

        public final long getId() {
            return this.f2706id;
        }

        public final long getPlanId() {
            return this.planId;
        }

        public final int getQuantity() {
            return this.quantity;
        }

        public int hashCode() {
            long j = this.f2706id;
            long j2 = this.planId;
            return (((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + this.quantity;
        }

        public String toString() {
            StringBuilder R = a.R("SubscriptionItem(id=");
            R.append(this.f2706id);
            R.append(", planId=");
            R.append(this.planId);
            R.append(", quantity=");
            return a.A(R, this.quantity, ")");
        }
    }

    /* compiled from: ModelSubscription.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\b\u001a\u0010\u001bJ\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ,\u0010\u000b\u001a\u00020\u00002\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u0006HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\r\u0010\bJ\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0011HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\"\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\t\u0010\u0016\u001a\u0004\b\u0017\u0010\u0005R\u001b\u0010\n\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0018\u001a\u0004\b\u0019\u0010\b¨\u0006\u001c"}, d2 = {"Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;", "Lcom/discord/models/domain/HasSubscriptionItems;", "", "Lcom/discord/models/domain/ModelSubscription$SubscriptionItem;", "component1", "()Ljava/util/List;", "", "component2", "()Ljava/lang/String;", "items", "paymentGatewayPlanId", "copy", "(Ljava/util/List;Ljava/lang/String;)Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getItems", "Ljava/lang/String;", "getPaymentGatewayPlanId", HookHelper.constructorName, "(Ljava/util/List;Ljava/lang/String;)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class SubscriptionRenewalMutations extends HasSubscriptionItems {
        private final List<SubscriptionItem> items;
        private final String paymentGatewayPlanId;

        public SubscriptionRenewalMutations(List<SubscriptionItem> list, String str) {
            m.checkNotNullParameter(list, "items");
            this.items = list;
            this.paymentGatewayPlanId = str;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ SubscriptionRenewalMutations copy$default(SubscriptionRenewalMutations subscriptionRenewalMutations, List list, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                list = subscriptionRenewalMutations.getItems();
            }
            if ((i & 2) != 0) {
                str = subscriptionRenewalMutations.paymentGatewayPlanId;
            }
            return subscriptionRenewalMutations.copy(list, str);
        }

        public final List<SubscriptionItem> component1() {
            return getItems();
        }

        public final String component2() {
            return this.paymentGatewayPlanId;
        }

        public final SubscriptionRenewalMutations copy(List<SubscriptionItem> list, String str) {
            m.checkNotNullParameter(list, "items");
            return new SubscriptionRenewalMutations(list, str);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof SubscriptionRenewalMutations)) {
                return false;
            }
            SubscriptionRenewalMutations subscriptionRenewalMutations = (SubscriptionRenewalMutations) obj;
            return m.areEqual(getItems(), subscriptionRenewalMutations.getItems()) && m.areEqual(this.paymentGatewayPlanId, subscriptionRenewalMutations.paymentGatewayPlanId);
        }

        @Override // com.discord.models.domain.HasSubscriptionItems
        public List<SubscriptionItem> getItems() {
            return this.items;
        }

        public final String getPaymentGatewayPlanId() {
            return this.paymentGatewayPlanId;
        }

        public int hashCode() {
            List<SubscriptionItem> items = getItems();
            int i = 0;
            int hashCode = (items != null ? items.hashCode() : 0) * 31;
            String str = this.paymentGatewayPlanId;
            if (str != null) {
                i = str.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("SubscriptionRenewalMutations(items=");
            R.append(getItems());
            R.append(", paymentGatewayPlanId=");
            return a.H(R, this.paymentGatewayPlanId, ")");
        }
    }

    /* compiled from: ModelSubscription.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u0000 \u00042\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0004B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/models/domain/ModelSubscription$Type;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "Companion", "PREMIUM", "GUILD", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum Type {
        PREMIUM,
        GUILD;
        
        public static final Companion Companion = new Companion(null);

        /* compiled from: ModelSubscription.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/models/domain/ModelSubscription$Type$Companion;", "", "", "typeInt", "Lcom/discord/models/domain/ModelSubscription$Type;", "from", "(I)Lcom/discord/models/domain/ModelSubscription$Type;", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Type from(int i) {
                if (i == 1) {
                    return Type.PREMIUM;
                }
                if (i == 2) {
                    return Type.GUILD;
                }
                throw new IllegalArgumentException(a.p("unsupported subscription type: ", i));
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    public ModelSubscription(String str, String str2, String str3, String str4, String str5, String str6, Integer num, String str7, String str8, SubscriptionRenewalMutations subscriptionRenewalMutations, List<SubscriptionItem> list, String str9, int i, int i2) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_ID);
        m.checkNotNullParameter(str2, "createdAt");
        m.checkNotNullParameter(str4, "currentPeriodStart");
        m.checkNotNullParameter(str5, "currentPeriodEnd");
        m.checkNotNullParameter(list, "items");
        this.f2705id = str;
        this.createdAt = str2;
        this.canceledAt = str3;
        this.currentPeriodStart = str4;
        this.currentPeriodEnd = str5;
        this.paymentSourceId = str6;
        this.paymentGateway = num;
        this.trialId = str7;
        this.trialEndsAt = str8;
        this.renewalMutations = subscriptionRenewalMutations;
        this.items = list;
        this.paymentGatewayPlanId = str9;
        this.status = i;
        this.type = i2;
    }

    private final int component13() {
        return this.status;
    }

    private final int component14() {
        return this.type;
    }

    public final String component1() {
        return this.f2705id;
    }

    public final SubscriptionRenewalMutations component10() {
        return this.renewalMutations;
    }

    public final List<SubscriptionItem> component11() {
        return getItems();
    }

    public final String component12() {
        return this.paymentGatewayPlanId;
    }

    public final String component2() {
        return this.createdAt;
    }

    public final String component3() {
        return this.canceledAt;
    }

    public final String component4() {
        return this.currentPeriodStart;
    }

    public final String component5() {
        return this.currentPeriodEnd;
    }

    public final String component6() {
        return this.paymentSourceId;
    }

    public final Integer component7() {
        return this.paymentGateway;
    }

    public final String component8() {
        return this.trialId;
    }

    public final String component9() {
        return this.trialEndsAt;
    }

    public final ModelSubscription copy(String str, String str2, String str3, String str4, String str5, String str6, Integer num, String str7, String str8, SubscriptionRenewalMutations subscriptionRenewalMutations, List<SubscriptionItem> list, String str9, int i, int i2) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_ID);
        m.checkNotNullParameter(str2, "createdAt");
        m.checkNotNullParameter(str4, "currentPeriodStart");
        m.checkNotNullParameter(str5, "currentPeriodEnd");
        m.checkNotNullParameter(list, "items");
        return new ModelSubscription(str, str2, str3, str4, str5, str6, num, str7, str8, subscriptionRenewalMutations, list, str9, i, i2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModelSubscription)) {
            return false;
        }
        ModelSubscription modelSubscription = (ModelSubscription) obj;
        return m.areEqual(this.f2705id, modelSubscription.f2705id) && m.areEqual(this.createdAt, modelSubscription.createdAt) && m.areEqual(this.canceledAt, modelSubscription.canceledAt) && m.areEqual(this.currentPeriodStart, modelSubscription.currentPeriodStart) && m.areEqual(this.currentPeriodEnd, modelSubscription.currentPeriodEnd) && m.areEqual(this.paymentSourceId, modelSubscription.paymentSourceId) && m.areEqual(this.paymentGateway, modelSubscription.paymentGateway) && m.areEqual(this.trialId, modelSubscription.trialId) && m.areEqual(this.trialEndsAt, modelSubscription.trialEndsAt) && m.areEqual(this.renewalMutations, modelSubscription.renewalMutations) && m.areEqual(getItems(), modelSubscription.getItems()) && m.areEqual(this.paymentGatewayPlanId, modelSubscription.paymentGatewayPlanId) && this.status == modelSubscription.status && this.type == modelSubscription.type;
    }

    public final long getAccountHoldEstimatedExpirationTimestamp() {
        return TimeUtils.addDaysToDate(this.currentPeriodEnd, 30);
    }

    public final String getCanceledAt() {
        return this.canceledAt;
    }

    public final String getCreatedAt() {
        return this.createdAt;
    }

    public final String getCurrentPeriodEnd() {
        return this.currentPeriodEnd;
    }

    public final String getCurrentPeriodStart() {
        return this.currentPeriodStart;
    }

    public final String getId() {
        return this.f2705id;
    }

    @Override // com.discord.models.domain.HasSubscriptionItems
    public List<SubscriptionItem> getItems() {
        return this.items;
    }

    public final Integer getPaymentGateway() {
        return this.paymentGateway;
    }

    public final String getPaymentGatewayPlanId() {
        return this.paymentGatewayPlanId;
    }

    public final String getPaymentSourceId() {
        return this.paymentSourceId;
    }

    public final SubscriptionPlanType getPlanType() {
        return SubscriptionPlanType.Companion.from(getPremiumBasePlanId());
    }

    public final SubscriptionRenewalMutations getRenewalMutations() {
        return this.renewalMutations;
    }

    public final Status getStatus() {
        return Status.Companion.from(this.status);
    }

    public final String getTrialEndsAt() {
        return this.trialEndsAt;
    }

    public final String getTrialId() {
        return this.trialId;
    }

    public final Type getType() {
        return Type.Companion.from(this.type);
    }

    public final boolean hasAnyOfPlans(Set<Long> set) {
        m.checkNotNullParameter(set, "planIds");
        List<SubscriptionItem> items = getItems();
        if ((items instanceof Collection) && items.isEmpty()) {
            return false;
        }
        for (SubscriptionItem subscriptionItem : items) {
            if (set.contains(Long.valueOf(subscriptionItem.getPlanId()))) {
                return true;
            }
        }
        return false;
    }

    public final boolean hasPlan(long j) {
        boolean z2;
        List<SubscriptionItem> items = getItems();
        if (!(items instanceof Collection) || !items.isEmpty()) {
            for (SubscriptionItem subscriptionItem : items) {
                if (subscriptionItem.getPlanId() == j) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    return true;
                }
            }
        }
        return false;
    }

    public int hashCode() {
        String str = this.f2705id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.createdAt;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.canceledAt;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.currentPeriodStart;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.currentPeriodEnd;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.paymentSourceId;
        int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
        Integer num = this.paymentGateway;
        int hashCode7 = (hashCode6 + (num != null ? num.hashCode() : 0)) * 31;
        String str7 = this.trialId;
        int hashCode8 = (hashCode7 + (str7 != null ? str7.hashCode() : 0)) * 31;
        String str8 = this.trialEndsAt;
        int hashCode9 = (hashCode8 + (str8 != null ? str8.hashCode() : 0)) * 31;
        SubscriptionRenewalMutations subscriptionRenewalMutations = this.renewalMutations;
        int hashCode10 = (hashCode9 + (subscriptionRenewalMutations != null ? subscriptionRenewalMutations.hashCode() : 0)) * 31;
        List<SubscriptionItem> items = getItems();
        int hashCode11 = (hashCode10 + (items != null ? items.hashCode() : 0)) * 31;
        String str9 = this.paymentGatewayPlanId;
        if (str9 != null) {
            i = str9.hashCode();
        }
        return ((((hashCode11 + i) * 31) + this.status) * 31) + this.type;
    }

    public final boolean isAppleSubscription() {
        return PaymentGateway.Companion.from(this.paymentGateway) == PaymentGateway.APPLE;
    }

    public final boolean isGoogleSubscription() {
        return PaymentGateway.Companion.from(this.paymentGateway) == PaymentGateway.GOOGLE;
    }

    public final boolean isMobileManaged() {
        return isAppleSubscription() || isGoogleSubscription();
    }

    public final boolean isNonePlan() {
        return getPlanType() == SubscriptionPlanType.NONE_MONTH || getPlanType() == SubscriptionPlanType.NONE_YEAR;
    }

    public String toString() {
        StringBuilder R = a.R("ModelSubscription(id=");
        R.append(this.f2705id);
        R.append(", createdAt=");
        R.append(this.createdAt);
        R.append(", canceledAt=");
        R.append(this.canceledAt);
        R.append(", currentPeriodStart=");
        R.append(this.currentPeriodStart);
        R.append(", currentPeriodEnd=");
        R.append(this.currentPeriodEnd);
        R.append(", paymentSourceId=");
        R.append(this.paymentSourceId);
        R.append(", paymentGateway=");
        R.append(this.paymentGateway);
        R.append(", trialId=");
        R.append(this.trialId);
        R.append(", trialEndsAt=");
        R.append(this.trialEndsAt);
        R.append(", renewalMutations=");
        R.append(this.renewalMutations);
        R.append(", items=");
        R.append(getItems());
        R.append(", paymentGatewayPlanId=");
        R.append(this.paymentGatewayPlanId);
        R.append(", status=");
        R.append(this.status);
        R.append(", type=");
        return a.A(R, this.type, ")");
    }
}
