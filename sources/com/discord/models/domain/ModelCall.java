package com.discord.models.domain;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.d.b.a.a;
import com.discord.api.voice.state.VoiceState;
import com.discord.models.deserialization.gson.InboundGatewayGsonParser;
import com.discord.models.domain.Model;
import com.discord.models.domain.ModelCall;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/* loaded from: classes.dex */
public class ModelCall implements Model {
    private static final List<Long> EMPTY_RINGING = new ArrayList();
    public static final /* synthetic */ int j = 0;
    private long channelId;
    private long messageId;
    private String region;
    private List<Long> ringing;
    private boolean unavailable;
    @Nullable
    private List<VoiceState> voiceStates;

    /* loaded from: classes.dex */
    public static class Ringable implements Model {
        private boolean ringable;

        @Override // com.discord.models.domain.Model
        public void assignField(Model.JsonReader jsonReader) throws IOException {
            String nextName = jsonReader.nextName();
            nextName.hashCode();
            if (!nextName.equals("ringable")) {
                jsonReader.skipValue();
            } else {
                this.ringable = jsonReader.nextBoolean(this.ringable);
            }
        }

        public boolean canEqual(Object obj) {
            return obj instanceof Ringable;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Ringable)) {
                return false;
            }
            Ringable ringable = (Ringable) obj;
            return ringable.canEqual(this) && isRingable() == ringable.isRingable();
        }

        public int hashCode() {
            return 59 + (isRingable() ? 79 : 97);
        }

        public boolean isRingable() {
            return this.ringable;
        }

        public String toString() {
            StringBuilder R = a.R("ModelCall.Ringable(ringable=");
            R.append(isRingable());
            R.append(")");
            return R.toString();
        }
    }

    @Override // com.discord.models.domain.Model
    public void assignField(final Model.JsonReader jsonReader) throws IOException {
        String nextName = jsonReader.nextName();
        nextName.hashCode();
        char c = 65535;
        switch (nextName.hashCode()) {
            case -1930808873:
                if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_CHANNEL_ID)) {
                    c = 0;
                    break;
                }
                break;
            case -1690722221:
                if (nextName.equals("message_id")) {
                    c = 1;
                    break;
                }
                break;
            case -934795532:
                if (nextName.equals(ModelAuditLogEntry.CHANGE_KEY_REGION)) {
                    c = 2;
                    break;
                }
                break;
            case -665462704:
                if (nextName.equals("unavailable")) {
                    c = 3;
                    break;
                }
                break;
            case 196513199:
                if (nextName.equals("voice_states")) {
                    c = 4;
                    break;
                }
                break;
            case 1207025586:
                if (nextName.equals("ringing")) {
                    c = 5;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                this.channelId = jsonReader.nextLong(this.channelId);
                return;
            case 1:
                this.messageId = jsonReader.nextLong(this.messageId);
                return;
            case 2:
                this.region = jsonReader.nextString(this.region);
                return;
            case 3:
                this.unavailable = jsonReader.nextBoolean(this.unavailable);
                return;
            case 4:
                this.voiceStates = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.j
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        Model.JsonReader jsonReader2 = Model.JsonReader.this;
                        int i = ModelCall.j;
                        return (VoiceState) InboundGatewayGsonParser.fromJson(jsonReader2, VoiceState.class);
                    }
                });
                return;
            case 5:
                this.ringing = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.i
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        Model.JsonReader jsonReader2 = Model.JsonReader.this;
                        int i = ModelCall.j;
                        return Long.valueOf(jsonReader2.nextLong(1L));
                    }
                });
                return;
            default:
                jsonReader.skipValue();
                return;
        }
    }

    public boolean canEqual(Object obj) {
        return obj instanceof ModelCall;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ModelCall)) {
            return false;
        }
        ModelCall modelCall = (ModelCall) obj;
        if (!modelCall.canEqual(this) || getChannelId() != modelCall.getChannelId() || getMessageId() != modelCall.getMessageId() || isUnavailable() != modelCall.isUnavailable()) {
            return false;
        }
        String region = getRegion();
        String region2 = modelCall.getRegion();
        if (region != null ? !region.equals(region2) : region2 != null) {
            return false;
        }
        List<Long> ringing = getRinging();
        List<Long> ringing2 = modelCall.getRinging();
        if (ringing != null ? !ringing.equals(ringing2) : ringing2 != null) {
            return false;
        }
        List<VoiceState> voiceStates = getVoiceStates();
        List<VoiceState> voiceStates2 = modelCall.getVoiceStates();
        return voiceStates != null ? voiceStates.equals(voiceStates2) : voiceStates2 == null;
    }

    public long getChannelId() {
        return this.channelId;
    }

    public long getMessageId() {
        return this.messageId;
    }

    public String getRegion() {
        return this.region;
    }

    @NonNull
    public List<Long> getRinging() {
        List<Long> list = this.ringing;
        return list != null ? list : EMPTY_RINGING;
    }

    @Nullable
    public List<VoiceState> getVoiceStates() {
        return this.voiceStates;
    }

    public int hashCode() {
        long channelId = getChannelId();
        long messageId = getMessageId();
        int i = ((((((int) (channelId ^ (channelId >>> 32))) + 59) * 59) + ((int) ((messageId >>> 32) ^ messageId))) * 59) + (isUnavailable() ? 79 : 97);
        String region = getRegion();
        int i2 = 43;
        int hashCode = (i * 59) + (region == null ? 43 : region.hashCode());
        List<Long> ringing = getRinging();
        int hashCode2 = (hashCode * 59) + (ringing == null ? 43 : ringing.hashCode());
        List<VoiceState> voiceStates = getVoiceStates();
        int i3 = hashCode2 * 59;
        if (voiceStates != null) {
            i2 = voiceStates.hashCode();
        }
        return i3 + i2;
    }

    public boolean isUnavailable() {
        return this.unavailable;
    }

    public String toString() {
        StringBuilder R = a.R("ModelCall(channelId=");
        R.append(getChannelId());
        R.append(", messageId=");
        R.append(getMessageId());
        R.append(", region=");
        R.append(getRegion());
        R.append(", unavailable=");
        R.append(isUnavailable());
        R.append(", ringing=");
        R.append(getRinging());
        R.append(", voiceStates=");
        R.append(getVoiceStates());
        R.append(")");
        return R.toString();
    }
}
