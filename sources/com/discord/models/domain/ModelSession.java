package com.discord.models.domain;

import b.d.b.a.a;
import com.discord.api.activity.Activity;
import com.discord.models.deserialization.gson.InboundGatewayGsonParser;
import com.discord.models.domain.Model;
import java.io.IOException;
import java.util.List;
/* loaded from: classes.dex */
public class ModelSession implements Model {
    private boolean active;
    private List<Activity> activities;
    private ClientInfo clientInfo;
    private long lastModified;
    private String sessionId;
    private String status;

    /* loaded from: classes.dex */
    public static class ClientInfo implements Model {
        private String client;
        private int number;
        private String os;

        @Override // com.discord.models.domain.Model
        public void assignField(Model.JsonReader jsonReader) throws IOException {
            String nextName = jsonReader.nextName();
            nextName.hashCode();
            char c = 65535;
            switch (nextName.hashCode()) {
                case -1357712437:
                    if (nextName.equals("client")) {
                        c = 0;
                        break;
                    }
                    break;
                case -1034364087:
                    if (nextName.equals("number")) {
                        c = 1;
                        break;
                    }
                    break;
                case 3556:
                    if (nextName.equals("os")) {
                        c = 2;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    this.client = jsonReader.nextString(this.client);
                    return;
                case 1:
                    this.number = jsonReader.nextInt(this.number);
                    return;
                case 2:
                    this.os = jsonReader.nextString(this.os);
                    return;
                default:
                    jsonReader.skipValue();
                    return;
            }
        }

        public boolean canEqual(Object obj) {
            return obj instanceof ClientInfo;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof ClientInfo)) {
                return false;
            }
            ClientInfo clientInfo = (ClientInfo) obj;
            if (!clientInfo.canEqual(this) || getNumber() != clientInfo.getNumber()) {
                return false;
            }
            String os = getOs();
            String os2 = clientInfo.getOs();
            if (os != null ? !os.equals(os2) : os2 != null) {
                return false;
            }
            String client = getClient();
            String client2 = clientInfo.getClient();
            return client != null ? client.equals(client2) : client2 == null;
        }

        public String getClient() {
            return this.client;
        }

        public int getNumber() {
            return this.number;
        }

        public String getOs() {
            return this.os;
        }

        public int hashCode() {
            String os = getOs();
            int i = 43;
            int number = ((getNumber() + 59) * 59) + (os == null ? 43 : os.hashCode());
            String client = getClient();
            int i2 = number * 59;
            if (client != null) {
                i = client.hashCode();
            }
            return i2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("ModelSession.ClientInfo(os=");
            R.append(getOs());
            R.append(", client=");
            R.append(getClient());
            R.append(", number=");
            R.append(getNumber());
            R.append(")");
            return R.toString();
        }
    }

    @Override // com.discord.models.domain.Model
    public void assignField(final Model.JsonReader jsonReader) throws IOException {
        String nextName = jsonReader.nextName();
        nextName.hashCode();
        char c = 65535;
        switch (nextName.hashCode()) {
            case -1422950650:
                if (nextName.equals("active")) {
                    c = 0;
                    break;
                }
                break;
            case -892481550:
                if (nextName.equals("status")) {
                    c = 1;
                    break;
                }
                break;
            case -174010206:
                if (nextName.equals("client_info")) {
                    c = 2;
                    break;
                }
                break;
            case -28366254:
                if (nextName.equals("last_modified")) {
                    c = 3;
                    break;
                }
                break;
            case 1661853540:
                if (nextName.equals("session_id")) {
                    c = 4;
                    break;
                }
                break;
            case 2048605165:
                if (nextName.equals("activities")) {
                    c = 5;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                this.active = jsonReader.nextBoolean(this.active);
                return;
            case 1:
                this.status = jsonReader.nextString(this.status);
                return;
            case 2:
                this.clientInfo = (ClientInfo) jsonReader.parse(new ClientInfo());
                return;
            case 3:
                this.lastModified = jsonReader.nextLong(this.lastModified);
                return;
            case 4:
                this.sessionId = jsonReader.nextString(this.sessionId);
                return;
            case 5:
                this.activities = jsonReader.nextList(new Model.JsonReader.ItemFactory() { // from class: b.a.m.a.c0
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final Object get() {
                        return (Activity) InboundGatewayGsonParser.fromJson(Model.JsonReader.this, Activity.class);
                    }
                });
                return;
            default:
                jsonReader.skipValue();
                return;
        }
    }

    public boolean canEqual(Object obj) {
        return obj instanceof ModelSession;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ModelSession)) {
            return false;
        }
        ModelSession modelSession = (ModelSession) obj;
        if (!modelSession.canEqual(this) || getLastModified() != modelSession.getLastModified() || isActive() != modelSession.isActive()) {
            return false;
        }
        String sessionId = getSessionId();
        String sessionId2 = modelSession.getSessionId();
        if (sessionId != null ? !sessionId.equals(sessionId2) : sessionId2 != null) {
            return false;
        }
        String status = getStatus();
        String status2 = modelSession.getStatus();
        if (status != null ? !status.equals(status2) : status2 != null) {
            return false;
        }
        List<Activity> activities = getActivities();
        List<Activity> activities2 = modelSession.getActivities();
        if (activities != null ? !activities.equals(activities2) : activities2 != null) {
            return false;
        }
        ClientInfo clientInfo = getClientInfo();
        ClientInfo clientInfo2 = modelSession.getClientInfo();
        return clientInfo != null ? clientInfo.equals(clientInfo2) : clientInfo2 == null;
    }

    public List<Activity> getActivities() {
        return this.activities;
    }

    public ClientInfo getClientInfo() {
        return this.clientInfo;
    }

    public long getLastModified() {
        return this.lastModified;
    }

    public String getSessionId() {
        return this.sessionId;
    }

    public String getStatus() {
        return this.status;
    }

    public int hashCode() {
        long lastModified = getLastModified();
        int i = ((((int) (lastModified ^ (lastModified >>> 32))) + 59) * 59) + (isActive() ? 79 : 97);
        String sessionId = getSessionId();
        int i2 = 43;
        int hashCode = (i * 59) + (sessionId == null ? 43 : sessionId.hashCode());
        String status = getStatus();
        int hashCode2 = (hashCode * 59) + (status == null ? 43 : status.hashCode());
        List<Activity> activities = getActivities();
        int hashCode3 = (hashCode2 * 59) + (activities == null ? 43 : activities.hashCode());
        ClientInfo clientInfo = getClientInfo();
        int i3 = hashCode3 * 59;
        if (clientInfo != null) {
            i2 = clientInfo.hashCode();
        }
        return i3 + i2;
    }

    public boolean isActive() {
        return this.active;
    }

    public String toString() {
        StringBuilder R = a.R("ModelSession(sessionId=");
        R.append(getSessionId());
        R.append(", lastModified=");
        R.append(getLastModified());
        R.append(", status=");
        R.append(getStatus());
        R.append(", activities=");
        R.append(getActivities());
        R.append(", clientInfo=");
        R.append(getClientInfo());
        R.append(", active=");
        R.append(isActive());
        R.append(")");
        return R.toString();
    }
}
