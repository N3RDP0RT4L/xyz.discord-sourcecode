package com.discord.models.phone;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: PhoneCountryCode.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u0000 \u001a2\u00020\u0001:\u0001\u001aB\u001f\u0012\u0006\u0010\u0007\u001a\u00020\u0002\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J.\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\u0007\u001a\u00020\u00022\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\f\u0010\u0004J\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0014\u001a\u0004\b\u0015\u0010\u0004R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0014\u001a\u0004\b\u0016\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0014\u001a\u0004\b\u0017\u0010\u0004¨\u0006\u001b"}, d2 = {"Lcom/discord/models/phone/PhoneCountryCode;", "", "", "component1", "()Ljava/lang/String;", "component2", "component3", "alpha2", ModelAuditLogEntry.CHANGE_KEY_CODE, ModelAuditLogEntry.CHANGE_KEY_NAME, "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/models/phone/PhoneCountryCode;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getAlpha2", "getCode", "getName", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class PhoneCountryCode {
    public static final Companion Companion = new Companion(null);
    private static final PhoneCountryCode DEFAULT_COUNTRY_CODE = new PhoneCountryCode("US", "+1", "United States");
    private static final PhoneCountryCode MISSING_COUNTRY_CODE = new PhoneCountryCode("", "", "");
    private final String alpha2;
    private final String code;
    private final String name;

    /* compiled from: PhoneCountryCode.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\t\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0004\u001a\u0004\b\b\u0010\u0006¨\u0006\u000b"}, d2 = {"Lcom/discord/models/phone/PhoneCountryCode$Companion;", "", "Lcom/discord/models/phone/PhoneCountryCode;", "MISSING_COUNTRY_CODE", "Lcom/discord/models/phone/PhoneCountryCode;", "getMISSING_COUNTRY_CODE", "()Lcom/discord/models/phone/PhoneCountryCode;", "DEFAULT_COUNTRY_CODE", "getDEFAULT_COUNTRY_CODE", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public final PhoneCountryCode getDEFAULT_COUNTRY_CODE() {
            return PhoneCountryCode.DEFAULT_COUNTRY_CODE;
        }

        public final PhoneCountryCode getMISSING_COUNTRY_CODE() {
            return PhoneCountryCode.MISSING_COUNTRY_CODE;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public PhoneCountryCode(String str, String str2, String str3) {
        a.n0(str, "alpha2", str2, ModelAuditLogEntry.CHANGE_KEY_CODE, str3, ModelAuditLogEntry.CHANGE_KEY_NAME);
        this.alpha2 = str;
        this.code = str2;
        this.name = str3;
    }

    public static /* synthetic */ PhoneCountryCode copy$default(PhoneCountryCode phoneCountryCode, String str, String str2, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = phoneCountryCode.alpha2;
        }
        if ((i & 2) != 0) {
            str2 = phoneCountryCode.code;
        }
        if ((i & 4) != 0) {
            str3 = phoneCountryCode.name;
        }
        return phoneCountryCode.copy(str, str2, str3);
    }

    public final String component1() {
        return this.alpha2;
    }

    public final String component2() {
        return this.code;
    }

    public final String component3() {
        return this.name;
    }

    public final PhoneCountryCode copy(String str, String str2, String str3) {
        m.checkNotNullParameter(str, "alpha2");
        m.checkNotNullParameter(str2, ModelAuditLogEntry.CHANGE_KEY_CODE);
        m.checkNotNullParameter(str3, ModelAuditLogEntry.CHANGE_KEY_NAME);
        return new PhoneCountryCode(str, str2, str3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PhoneCountryCode)) {
            return false;
        }
        PhoneCountryCode phoneCountryCode = (PhoneCountryCode) obj;
        return m.areEqual(this.alpha2, phoneCountryCode.alpha2) && m.areEqual(this.code, phoneCountryCode.code) && m.areEqual(this.name, phoneCountryCode.name);
    }

    public final String getAlpha2() {
        return this.alpha2;
    }

    public final String getCode() {
        return this.code;
    }

    public final String getName() {
        return this.name;
    }

    public int hashCode() {
        String str = this.alpha2;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.code;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.name;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("PhoneCountryCode(alpha2=");
        R.append(this.alpha2);
        R.append(", code=");
        R.append(this.code);
        R.append(", name=");
        return a.H(R, this.name, ")");
    }
}
