package com.discord.models.hubs;

import d0.t.n;
import java.util.List;
import kotlin.Metadata;
/* compiled from: DirectoryEntryCategory.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\"\u001c\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u00008\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0002\u0010\u0003\"\u001c\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u00008\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0004\u0010\u0003¨\u0006\u0005"}, d2 = {"", "Lcom/discord/models/hubs/DirectoryEntryCategory;", "HQ_DIRECTORY_CATEGORIES", "Ljava/util/List;", "HUB_CATEGORIES", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class DirectoryEntryCategoryKt {
    private static final List<DirectoryEntryCategory> HUB_CATEGORIES = n.listOf((Object[]) new DirectoryEntryCategory[]{DirectoryEntryCategory.SchoolClub, DirectoryEntryCategory.Class, DirectoryEntryCategory.StudySocial, DirectoryEntryCategory.Misc});
    private static final List<DirectoryEntryCategory> HQ_DIRECTORY_CATEGORIES = n.listOf((Object[]) new DirectoryEntryCategory[]{DirectoryEntryCategory.HQSocial, DirectoryEntryCategory.HQErgs, DirectoryEntryCategory.HQMisc, DirectoryEntryCategory.HQArchives});
}
