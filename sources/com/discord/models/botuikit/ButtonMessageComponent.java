package com.discord.models.botuikit;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.botuikit.ButtonStyle;
import com.discord.api.botuikit.ComponentEmoji;
import com.discord.api.botuikit.ComponentType;
import com.discord.models.botuikit.ActionInteractionComponentState;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ButtonMessageComponent.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000f\n\u0002\u0010\u0000\n\u0002\b\u0016\b\u0086\b\u0018\u00002\u00020\u0001Ba\u0012\u0006\u0010\u001a\u001a\u00020\u0002\u0012\n\u0010\u001b\u001a\u00060\u0005j\u0002`\u0006\u0012\b\b\u0002\u0010\u001c\u001a\u00020\t\u0012\b\u0010\u001d\u001a\u0004\u0018\u00010\f\u0012\b\u0010\u001e\u001a\u0004\u0018\u00010\f\u0012\u0006\u0010\u001f\u001a\u00020\u0010\u0012\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u0013\u0012\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\f\u0012\u0006\u0010\"\u001a\u00020\u0017¢\u0006\u0004\b;\u0010<J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0014\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0012\u0010\r\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0012\u0010\u000f\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u000eJ\u0010\u0010\u0011\u001a\u00020\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0012\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0012\u0010\u0016\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\u0016\u0010\u000eJ\u0010\u0010\u0018\u001a\u00020\u0017HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0019Jv\u0010#\u001a\u00020\u00002\b\b\u0002\u0010\u001a\u001a\u00020\u00022\f\b\u0002\u0010\u001b\u001a\u00060\u0005j\u0002`\u00062\b\b\u0002\u0010\u001c\u001a\u00020\t2\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\f2\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\f2\b\b\u0002\u0010\u001f\u001a\u00020\u00102\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u00132\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\f2\b\b\u0002\u0010\"\u001a\u00020\u0017HÆ\u0001¢\u0006\u0004\b#\u0010$J\u0010\u0010%\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b%\u0010\u000eJ\u0010\u0010&\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b&\u0010\bJ\u001a\u0010)\u001a\u00020\u00172\b\u0010(\u001a\u0004\u0018\u00010'HÖ\u0003¢\u0006\u0004\b)\u0010*R\u001b\u0010\u001d\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010+\u001a\u0004\b,\u0010\u000eR\u0019\u0010\"\u001a\u00020\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010-\u001a\u0004\b.\u0010\u0019R\u001c\u0010\u001c\u001a\u00020\t8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001c\u0010/\u001a\u0004\b0\u0010\u000bR\u001b\u0010 \u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b \u00101\u001a\u0004\b2\u0010\u0015R\u001b\u0010!\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010+\u001a\u0004\b3\u0010\u000eR\u001b\u0010\u001e\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010+\u001a\u0004\b4\u0010\u000eR \u0010\u001b\u001a\u00060\u0005j\u0002`\u00068\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001b\u00105\u001a\u0004\b6\u0010\bR\u0019\u0010\u001f\u001a\u00020\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00107\u001a\u0004\b8\u0010\u0012R\u001c\u0010\u001a\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001a\u00109\u001a\u0004\b:\u0010\u0004¨\u0006="}, d2 = {"Lcom/discord/models/botuikit/ButtonMessageComponent;", "Lcom/discord/models/botuikit/ActionMessageComponent;", "Lcom/discord/api/botuikit/ComponentType;", "component1", "()Lcom/discord/api/botuikit/ComponentType;", "", "Lcom/discord/widgets/botuikit/ComponentIndex;", "component2", "()I", "Lcom/discord/models/botuikit/ActionInteractionComponentState;", "component3", "()Lcom/discord/models/botuikit/ActionInteractionComponentState;", "", "component4", "()Ljava/lang/String;", "component5", "Lcom/discord/api/botuikit/ButtonStyle;", "component6", "()Lcom/discord/api/botuikit/ButtonStyle;", "Lcom/discord/api/botuikit/ComponentEmoji;", "component7", "()Lcom/discord/api/botuikit/ComponentEmoji;", "component8", "", "component9", "()Z", "type", "index", "stateInteraction", "customId", "label", "style", "emoji", "url", "emojiAnimationsEnabled", "copy", "(Lcom/discord/api/botuikit/ComponentType;ILcom/discord/models/botuikit/ActionInteractionComponentState;Ljava/lang/String;Ljava/lang/String;Lcom/discord/api/botuikit/ButtonStyle;Lcom/discord/api/botuikit/ComponentEmoji;Ljava/lang/String;Z)Lcom/discord/models/botuikit/ButtonMessageComponent;", "toString", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getCustomId", "Z", "getEmojiAnimationsEnabled", "Lcom/discord/models/botuikit/ActionInteractionComponentState;", "getStateInteraction", "Lcom/discord/api/botuikit/ComponentEmoji;", "getEmoji", "getUrl", "getLabel", "I", "getIndex", "Lcom/discord/api/botuikit/ButtonStyle;", "getStyle", "Lcom/discord/api/botuikit/ComponentType;", "getType", HookHelper.constructorName, "(Lcom/discord/api/botuikit/ComponentType;ILcom/discord/models/botuikit/ActionInteractionComponentState;Ljava/lang/String;Ljava/lang/String;Lcom/discord/api/botuikit/ButtonStyle;Lcom/discord/api/botuikit/ComponentEmoji;Ljava/lang/String;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ButtonMessageComponent extends ActionMessageComponent {
    private final String customId;
    private final ComponentEmoji emoji;
    private final boolean emojiAnimationsEnabled;
    private final int index;
    private final String label;
    private final ActionInteractionComponentState stateInteraction;
    private final ButtonStyle style;
    private final ComponentType type;
    private final String url;

    public /* synthetic */ ButtonMessageComponent(ComponentType componentType, int i, ActionInteractionComponentState actionInteractionComponentState, String str, String str2, ButtonStyle buttonStyle, ComponentEmoji componentEmoji, String str3, boolean z2, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(componentType, i, (i2 & 4) != 0 ? ActionInteractionComponentState.Enabled.INSTANCE : actionInteractionComponentState, str, str2, buttonStyle, (i2 & 64) != 0 ? null : componentEmoji, (i2 & 128) != 0 ? null : str3, z2);
    }

    public final ComponentType component1() {
        return getType();
    }

    public final int component2() {
        return getIndex();
    }

    public final ActionInteractionComponentState component3() {
        return getStateInteraction();
    }

    public final String component4() {
        return this.customId;
    }

    public final String component5() {
        return this.label;
    }

    public final ButtonStyle component6() {
        return this.style;
    }

    public final ComponentEmoji component7() {
        return this.emoji;
    }

    public final String component8() {
        return this.url;
    }

    public final boolean component9() {
        return this.emojiAnimationsEnabled;
    }

    public final ButtonMessageComponent copy(ComponentType componentType, int i, ActionInteractionComponentState actionInteractionComponentState, String str, String str2, ButtonStyle buttonStyle, ComponentEmoji componentEmoji, String str3, boolean z2) {
        m.checkNotNullParameter(componentType, "type");
        m.checkNotNullParameter(actionInteractionComponentState, "stateInteraction");
        m.checkNotNullParameter(buttonStyle, "style");
        return new ButtonMessageComponent(componentType, i, actionInteractionComponentState, str, str2, buttonStyle, componentEmoji, str3, z2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ButtonMessageComponent)) {
            return false;
        }
        ButtonMessageComponent buttonMessageComponent = (ButtonMessageComponent) obj;
        return m.areEqual(getType(), buttonMessageComponent.getType()) && getIndex() == buttonMessageComponent.getIndex() && m.areEqual(getStateInteraction(), buttonMessageComponent.getStateInteraction()) && m.areEqual(this.customId, buttonMessageComponent.customId) && m.areEqual(this.label, buttonMessageComponent.label) && m.areEqual(this.style, buttonMessageComponent.style) && m.areEqual(this.emoji, buttonMessageComponent.emoji) && m.areEqual(this.url, buttonMessageComponent.url) && this.emojiAnimationsEnabled == buttonMessageComponent.emojiAnimationsEnabled;
    }

    public final String getCustomId() {
        return this.customId;
    }

    public final ComponentEmoji getEmoji() {
        return this.emoji;
    }

    public final boolean getEmojiAnimationsEnabled() {
        return this.emojiAnimationsEnabled;
    }

    @Override // com.discord.models.botuikit.MessageComponent
    public int getIndex() {
        return this.index;
    }

    public final String getLabel() {
        return this.label;
    }

    @Override // com.discord.models.botuikit.ActionMessageComponent
    public ActionInteractionComponentState getStateInteraction() {
        return this.stateInteraction;
    }

    public final ButtonStyle getStyle() {
        return this.style;
    }

    @Override // com.discord.models.botuikit.MessageComponent
    public ComponentType getType() {
        return this.type;
    }

    public final String getUrl() {
        return this.url;
    }

    public int hashCode() {
        ComponentType type = getType();
        int i = 0;
        int index = (getIndex() + ((type != null ? type.hashCode() : 0) * 31)) * 31;
        ActionInteractionComponentState stateInteraction = getStateInteraction();
        int hashCode = (index + (stateInteraction != null ? stateInteraction.hashCode() : 0)) * 31;
        String str = this.customId;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.label;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        ButtonStyle buttonStyle = this.style;
        int hashCode4 = (hashCode3 + (buttonStyle != null ? buttonStyle.hashCode() : 0)) * 31;
        ComponentEmoji componentEmoji = this.emoji;
        int hashCode5 = (hashCode4 + (componentEmoji != null ? componentEmoji.hashCode() : 0)) * 31;
        String str3 = this.url;
        if (str3 != null) {
            i = str3.hashCode();
        }
        int i2 = (hashCode5 + i) * 31;
        boolean z2 = this.emojiAnimationsEnabled;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        return i2 + i3;
    }

    public String toString() {
        StringBuilder R = a.R("ButtonMessageComponent(type=");
        R.append(getType());
        R.append(", index=");
        R.append(getIndex());
        R.append(", stateInteraction=");
        R.append(getStateInteraction());
        R.append(", customId=");
        R.append(this.customId);
        R.append(", label=");
        R.append(this.label);
        R.append(", style=");
        R.append(this.style);
        R.append(", emoji=");
        R.append(this.emoji);
        R.append(", url=");
        R.append(this.url);
        R.append(", emojiAnimationsEnabled=");
        return a.M(R, this.emojiAnimationsEnabled, ")");
    }

    public ButtonMessageComponent(ComponentType componentType, int i, ActionInteractionComponentState actionInteractionComponentState, String str, String str2, ButtonStyle buttonStyle, ComponentEmoji componentEmoji, String str3, boolean z2) {
        m.checkNotNullParameter(componentType, "type");
        m.checkNotNullParameter(actionInteractionComponentState, "stateInteraction");
        m.checkNotNullParameter(buttonStyle, "style");
        this.type = componentType;
        this.index = i;
        this.stateInteraction = actionInteractionComponentState;
        this.customId = str;
        this.label = str2;
        this.style = buttonStyle;
        this.emoji = componentEmoji;
        this.url = str3;
        this.emojiAnimationsEnabled = z2;
    }
}
