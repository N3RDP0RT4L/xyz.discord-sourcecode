package com.discord.models.botuikit;

import andhook.lib.HookHelper;
import java.util.List;
import kotlin.Metadata;
/* compiled from: MessageComponent.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0005\b&\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u0005\u0010\u0006J\u0015\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00010\u0002H&¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/models/botuikit/LayoutMessageComponent;", "Lcom/discord/models/botuikit/MessageComponent;", "", "getChildren", "()Ljava/util/List;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public abstract class LayoutMessageComponent implements MessageComponent {
    public abstract List<MessageComponent> getChildren();
}
