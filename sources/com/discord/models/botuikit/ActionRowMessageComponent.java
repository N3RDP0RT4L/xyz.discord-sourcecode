package com.discord.models.botuikit;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.botuikit.ComponentType;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ActionRowMessageComponent.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\u000e\u001a\u00020\u0006\u0012\f\b\u0002\u0010\u000f\u001a\u00060\tj\u0002`\n\u0012\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\"\u0010#J\u0015\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0016¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0014\u0010\u000b\u001a\u00060\tj\u0002`\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0016\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\r\u0010\u0005J8\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u000e\u001a\u00020\u00062\f\b\u0002\u0010\u000f\u001a\u00060\tj\u0002`\n2\u000e\b\u0002\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\u0016\u0010\fJ\u001a\u0010\u001a\u001a\u00020\u00192\b\u0010\u0018\u001a\u0004\u0018\u00010\u0017HÖ\u0003¢\u0006\u0004\b\u001a\u0010\u001bR \u0010\u000f\u001a\u00060\tj\u0002`\n8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u000f\u0010\u001c\u001a\u0004\b\u001d\u0010\fR\u001c\u0010\u000e\u001a\u00020\u00068\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u000e\u0010\u001e\u001a\u0004\b\u001f\u0010\bR\u001f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010 \u001a\u0004\b!\u0010\u0005¨\u0006$"}, d2 = {"Lcom/discord/models/botuikit/ActionRowMessageComponent;", "Lcom/discord/models/botuikit/LayoutMessageComponent;", "", "Lcom/discord/models/botuikit/MessageComponent;", "getChildren", "()Ljava/util/List;", "Lcom/discord/api/botuikit/ComponentType;", "component1", "()Lcom/discord/api/botuikit/ComponentType;", "", "Lcom/discord/widgets/botuikit/ComponentIndex;", "component2", "()I", "component3", "type", "index", "components", "copy", "(Lcom/discord/api/botuikit/ComponentType;ILjava/util/List;)Lcom/discord/models/botuikit/ActionRowMessageComponent;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getIndex", "Lcom/discord/api/botuikit/ComponentType;", "getType", "Ljava/util/List;", "getComponents", HookHelper.constructorName, "(Lcom/discord/api/botuikit/ComponentType;ILjava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ActionRowMessageComponent extends LayoutMessageComponent {
    private final List<MessageComponent> components;
    private final int index;
    private final ComponentType type;

    public /* synthetic */ ActionRowMessageComponent(ComponentType componentType, int i, List list, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(componentType, (i2 & 2) != 0 ? 0 : i, list);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ ActionRowMessageComponent copy$default(ActionRowMessageComponent actionRowMessageComponent, ComponentType componentType, int i, List list, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            componentType = actionRowMessageComponent.getType();
        }
        if ((i2 & 2) != 0) {
            i = actionRowMessageComponent.getIndex();
        }
        if ((i2 & 4) != 0) {
            list = actionRowMessageComponent.components;
        }
        return actionRowMessageComponent.copy(componentType, i, list);
    }

    public final ComponentType component1() {
        return getType();
    }

    public final int component2() {
        return getIndex();
    }

    public final List<MessageComponent> component3() {
        return this.components;
    }

    public final ActionRowMessageComponent copy(ComponentType componentType, int i, List<? extends MessageComponent> list) {
        m.checkNotNullParameter(componentType, "type");
        m.checkNotNullParameter(list, "components");
        return new ActionRowMessageComponent(componentType, i, list);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ActionRowMessageComponent)) {
            return false;
        }
        ActionRowMessageComponent actionRowMessageComponent = (ActionRowMessageComponent) obj;
        return m.areEqual(getType(), actionRowMessageComponent.getType()) && getIndex() == actionRowMessageComponent.getIndex() && m.areEqual(this.components, actionRowMessageComponent.components);
    }

    @Override // com.discord.models.botuikit.LayoutMessageComponent
    public List<MessageComponent> getChildren() {
        return this.components;
    }

    public final List<MessageComponent> getComponents() {
        return this.components;
    }

    @Override // com.discord.models.botuikit.MessageComponent
    public int getIndex() {
        return this.index;
    }

    @Override // com.discord.models.botuikit.MessageComponent
    public ComponentType getType() {
        return this.type;
    }

    public int hashCode() {
        ComponentType type = getType();
        int i = 0;
        int index = (getIndex() + ((type != null ? type.hashCode() : 0) * 31)) * 31;
        List<MessageComponent> list = this.components;
        if (list != null) {
            i = list.hashCode();
        }
        return index + i;
    }

    public String toString() {
        StringBuilder R = a.R("ActionRowMessageComponent(type=");
        R.append(getType());
        R.append(", index=");
        R.append(getIndex());
        R.append(", components=");
        return a.K(R, this.components, ")");
    }

    /* JADX WARN: Multi-variable type inference failed */
    public ActionRowMessageComponent(ComponentType componentType, int i, List<? extends MessageComponent> list) {
        m.checkNotNullParameter(componentType, "type");
        m.checkNotNullParameter(list, "components");
        this.type = componentType;
        this.index = i;
        this.components = list;
    }
}
