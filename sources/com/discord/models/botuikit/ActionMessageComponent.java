package com.discord.models.botuikit;

import andhook.lib.HookHelper;
import kotlin.Metadata;
/* compiled from: MessageComponent.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b&\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0005\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/models/botuikit/ActionMessageComponent;", "Lcom/discord/models/botuikit/MessageComponent;", "Lcom/discord/models/botuikit/ActionInteractionComponentState;", "getStateInteraction", "()Lcom/discord/models/botuikit/ActionInteractionComponentState;", "stateInteraction", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public abstract class ActionMessageComponent implements MessageComponent {
    public abstract ActionInteractionComponentState getStateInteraction();
}
