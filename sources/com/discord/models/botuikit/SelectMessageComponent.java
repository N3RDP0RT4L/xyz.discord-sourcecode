package com.discord.models.botuikit;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.botuikit.ComponentType;
import com.discord.api.botuikit.SelectItem;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: SelectMessageComponent.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0010\n\u0002\u0010\u0000\n\u0002\b\u0016\b\u0086\b\u0018\u00002\u00020\u0001Bi\u0012\u0006\u0010\u001a\u001a\u00020\u0002\u0012\n\u0010\u001b\u001a\u00060\u0005j\u0002`\u0006\u0012\u0006\u0010\u001c\u001a\u00020\t\u0012\u0006\u0010\u001d\u001a\u00020\f\u0012\b\u0010\u001e\u001a\u0004\u0018\u00010\f\u0012\u0006\u0010\u001f\u001a\u00020\u0005\u0012\u0006\u0010 \u001a\u00020\u0005\u0012\f\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012\u0012\f\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012\u0012\u0006\u0010#\u001a\u00020\u0017¢\u0006\u0004\b<\u0010=J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0014\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0012\u0010\u000f\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0010\u0010\bJ\u0010\u0010\u0011\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0011\u0010\bJ\u0016\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0016\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0015J\u0010\u0010\u0018\u001a\u00020\u0017HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0019J\u0086\u0001\u0010$\u001a\u00020\u00002\b\b\u0002\u0010\u001a\u001a\u00020\u00022\f\b\u0002\u0010\u001b\u001a\u00060\u0005j\u0002`\u00062\b\b\u0002\u0010\u001c\u001a\u00020\t2\b\b\u0002\u0010\u001d\u001a\u00020\f2\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\f2\b\b\u0002\u0010\u001f\u001a\u00020\u00052\b\b\u0002\u0010 \u001a\u00020\u00052\u000e\b\u0002\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00130\u00122\u000e\b\u0002\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00130\u00122\b\b\u0002\u0010#\u001a\u00020\u0017HÆ\u0001¢\u0006\u0004\b$\u0010%J\u0010\u0010&\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b&\u0010\u000eJ\u0010\u0010'\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b'\u0010\bJ\u001a\u0010*\u001a\u00020\u00172\b\u0010)\u001a\u0004\u0018\u00010(HÖ\u0003¢\u0006\u0004\b*\u0010+R\u001f\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00130\u00128\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010,\u001a\u0004\b-\u0010\u0015R\u001f\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00130\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010,\u001a\u0004\b.\u0010\u0015R\u0019\u0010 \u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b \u0010/\u001a\u0004\b0\u0010\bR\u001b\u0010\u001e\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00101\u001a\u0004\b2\u0010\u000eR\u0019\u0010\u001f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010/\u001a\u0004\b3\u0010\bR\u0019\u0010#\u001a\u00020\u00178\u0006@\u0006¢\u0006\f\n\u0004\b#\u00104\u001a\u0004\b5\u0010\u0019R \u0010\u001b\u001a\u00060\u0005j\u0002`\u00068\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001b\u0010/\u001a\u0004\b6\u0010\bR\u001c\u0010\u001c\u001a\u00020\t8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001c\u00107\u001a\u0004\b8\u0010\u000bR\u001c\u0010\u001a\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001a\u00109\u001a\u0004\b:\u0010\u0004R\u0019\u0010\u001d\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00101\u001a\u0004\b;\u0010\u000e¨\u0006>"}, d2 = {"Lcom/discord/models/botuikit/SelectMessageComponent;", "Lcom/discord/models/botuikit/ActionMessageComponent;", "Lcom/discord/api/botuikit/ComponentType;", "component1", "()Lcom/discord/api/botuikit/ComponentType;", "", "Lcom/discord/widgets/botuikit/ComponentIndex;", "component2", "()I", "Lcom/discord/models/botuikit/ActionInteractionComponentState;", "component3", "()Lcom/discord/models/botuikit/ActionInteractionComponentState;", "", "component4", "()Ljava/lang/String;", "component5", "component6", "component7", "", "Lcom/discord/api/botuikit/SelectItem;", "component8", "()Ljava/util/List;", "component9", "", "component10", "()Z", "type", "index", "stateInteraction", "customId", "placeholder", "minValues", "maxValues", "options", "selectedOptions", "emojiAnimationsEnabled", "copy", "(Lcom/discord/api/botuikit/ComponentType;ILcom/discord/models/botuikit/ActionInteractionComponentState;Ljava/lang/String;Ljava/lang/String;IILjava/util/List;Ljava/util/List;Z)Lcom/discord/models/botuikit/SelectMessageComponent;", "toString", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getOptions", "getSelectedOptions", "I", "getMaxValues", "Ljava/lang/String;", "getPlaceholder", "getMinValues", "Z", "getEmojiAnimationsEnabled", "getIndex", "Lcom/discord/models/botuikit/ActionInteractionComponentState;", "getStateInteraction", "Lcom/discord/api/botuikit/ComponentType;", "getType", "getCustomId", HookHelper.constructorName, "(Lcom/discord/api/botuikit/ComponentType;ILcom/discord/models/botuikit/ActionInteractionComponentState;Ljava/lang/String;Ljava/lang/String;IILjava/util/List;Ljava/util/List;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class SelectMessageComponent extends ActionMessageComponent {
    private final String customId;
    private final boolean emojiAnimationsEnabled;
    private final int index;
    private final int maxValues;
    private final int minValues;
    private final List<SelectItem> options;
    private final String placeholder;
    private final List<SelectItem> selectedOptions;
    private final ActionInteractionComponentState stateInteraction;
    private final ComponentType type;

    public SelectMessageComponent(ComponentType componentType, int i, ActionInteractionComponentState actionInteractionComponentState, String str, String str2, int i2, int i3, List<SelectItem> list, List<SelectItem> list2, boolean z2) {
        m.checkNotNullParameter(componentType, "type");
        m.checkNotNullParameter(actionInteractionComponentState, "stateInteraction");
        m.checkNotNullParameter(str, "customId");
        m.checkNotNullParameter(list, "options");
        m.checkNotNullParameter(list2, "selectedOptions");
        this.type = componentType;
        this.index = i;
        this.stateInteraction = actionInteractionComponentState;
        this.customId = str;
        this.placeholder = str2;
        this.minValues = i2;
        this.maxValues = i3;
        this.options = list;
        this.selectedOptions = list2;
        this.emojiAnimationsEnabled = z2;
    }

    public final ComponentType component1() {
        return getType();
    }

    public final boolean component10() {
        return this.emojiAnimationsEnabled;
    }

    public final int component2() {
        return getIndex();
    }

    public final ActionInteractionComponentState component3() {
        return getStateInteraction();
    }

    public final String component4() {
        return this.customId;
    }

    public final String component5() {
        return this.placeholder;
    }

    public final int component6() {
        return this.minValues;
    }

    public final int component7() {
        return this.maxValues;
    }

    public final List<SelectItem> component8() {
        return this.options;
    }

    public final List<SelectItem> component9() {
        return this.selectedOptions;
    }

    public final SelectMessageComponent copy(ComponentType componentType, int i, ActionInteractionComponentState actionInteractionComponentState, String str, String str2, int i2, int i3, List<SelectItem> list, List<SelectItem> list2, boolean z2) {
        m.checkNotNullParameter(componentType, "type");
        m.checkNotNullParameter(actionInteractionComponentState, "stateInteraction");
        m.checkNotNullParameter(str, "customId");
        m.checkNotNullParameter(list, "options");
        m.checkNotNullParameter(list2, "selectedOptions");
        return new SelectMessageComponent(componentType, i, actionInteractionComponentState, str, str2, i2, i3, list, list2, z2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SelectMessageComponent)) {
            return false;
        }
        SelectMessageComponent selectMessageComponent = (SelectMessageComponent) obj;
        return m.areEqual(getType(), selectMessageComponent.getType()) && getIndex() == selectMessageComponent.getIndex() && m.areEqual(getStateInteraction(), selectMessageComponent.getStateInteraction()) && m.areEqual(this.customId, selectMessageComponent.customId) && m.areEqual(this.placeholder, selectMessageComponent.placeholder) && this.minValues == selectMessageComponent.minValues && this.maxValues == selectMessageComponent.maxValues && m.areEqual(this.options, selectMessageComponent.options) && m.areEqual(this.selectedOptions, selectMessageComponent.selectedOptions) && this.emojiAnimationsEnabled == selectMessageComponent.emojiAnimationsEnabled;
    }

    public final String getCustomId() {
        return this.customId;
    }

    public final boolean getEmojiAnimationsEnabled() {
        return this.emojiAnimationsEnabled;
    }

    @Override // com.discord.models.botuikit.MessageComponent
    public int getIndex() {
        return this.index;
    }

    public final int getMaxValues() {
        return this.maxValues;
    }

    public final int getMinValues() {
        return this.minValues;
    }

    public final List<SelectItem> getOptions() {
        return this.options;
    }

    public final String getPlaceholder() {
        return this.placeholder;
    }

    public final List<SelectItem> getSelectedOptions() {
        return this.selectedOptions;
    }

    @Override // com.discord.models.botuikit.ActionMessageComponent
    public ActionInteractionComponentState getStateInteraction() {
        return this.stateInteraction;
    }

    @Override // com.discord.models.botuikit.MessageComponent
    public ComponentType getType() {
        return this.type;
    }

    public int hashCode() {
        ComponentType type = getType();
        int i = 0;
        int index = (getIndex() + ((type != null ? type.hashCode() : 0) * 31)) * 31;
        ActionInteractionComponentState stateInteraction = getStateInteraction();
        int hashCode = (index + (stateInteraction != null ? stateInteraction.hashCode() : 0)) * 31;
        String str = this.customId;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.placeholder;
        int hashCode3 = (((((hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31) + this.minValues) * 31) + this.maxValues) * 31;
        List<SelectItem> list = this.options;
        int hashCode4 = (hashCode3 + (list != null ? list.hashCode() : 0)) * 31;
        List<SelectItem> list2 = this.selectedOptions;
        if (list2 != null) {
            i = list2.hashCode();
        }
        int i2 = (hashCode4 + i) * 31;
        boolean z2 = this.emojiAnimationsEnabled;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        return i2 + i3;
    }

    public String toString() {
        StringBuilder R = a.R("SelectMessageComponent(type=");
        R.append(getType());
        R.append(", index=");
        R.append(getIndex());
        R.append(", stateInteraction=");
        R.append(getStateInteraction());
        R.append(", customId=");
        R.append(this.customId);
        R.append(", placeholder=");
        R.append(this.placeholder);
        R.append(", minValues=");
        R.append(this.minValues);
        R.append(", maxValues=");
        R.append(this.maxValues);
        R.append(", options=");
        R.append(this.options);
        R.append(", selectedOptions=");
        R.append(this.selectedOptions);
        R.append(", emojiAnimationsEnabled=");
        return a.M(R, this.emojiAnimationsEnabled, ")");
    }
}
