package com.discord.models.message;

import a0.a.a.b;
import andhook.lib.HookHelper;
import androidx.appcompat.widget.ActivityChooserModel;
import androidx.constraintlayout.solver.widgets.analyzer.BasicMeasure;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.api.application.Application;
import com.discord.api.botuikit.Component;
import com.discord.api.channel.Channel;
import com.discord.api.interaction.Interaction;
import com.discord.api.message.LocalAttachment;
import com.discord.api.message.MessageReference;
import com.discord.api.message.activity.MessageActivity;
import com.discord.api.message.activity.MessageActivityType;
import com.discord.api.message.allowedmentions.MessageAllowedMentions;
import com.discord.api.message.attachment.MessageAttachment;
import com.discord.api.message.call.MessageCall;
import com.discord.api.message.embed.MessageEmbed;
import com.discord.api.message.reaction.MessageReaction;
import com.discord.api.sticker.Sticker;
import com.discord.api.sticker.StickerPartial;
import com.discord.api.user.User;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.g0.w;
import d0.t.h0;
import d0.z.d.m;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: Message.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Þ\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\t\n\u0002\b\u0013\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\bo\b\u0086\b\u0018\u0000 á\u00012\u00020\u0001:\u0002á\u0001BÃ\u0004\u0012\n\u0010v\u001a\u00060\u0017j\u0002`+\u0012\n\u0010w\u001a\u00060\u0017j\u0002`-\u0012\u0010\b\u0002\u0010x\u001a\n\u0018\u00010\u0017j\u0004\u0018\u0001`/\u0012\n\b\u0002\u0010y\u001a\u0004\u0018\u000102\u0012\n\b\u0002\u0010z\u001a\u0004\u0018\u00010\b\u0012\n\b\u0002\u0010{\u001a\u0004\u0018\u000107\u0012\n\b\u0002\u0010|\u001a\u0004\u0018\u000107\u0012\n\b\u0002\u0010}\u001a\u0004\u0018\u00010\f\u0012\n\b\u0002\u0010~\u001a\u0004\u0018\u00010\f\u0012\u0010\b\u0002\u0010\u007f\u001a\n\u0012\u0004\u0012\u000202\u0018\u00010>\u0012\u0015\b\u0002\u0010\u0080\u0001\u001a\u000e\u0012\b\u0012\u00060\u0017j\u0002`A\u0018\u00010>\u0012\u0011\b\u0002\u0010\u0081\u0001\u001a\n\u0012\u0004\u0012\u00020C\u0018\u00010>\u0012\u0011\b\u0002\u0010\u0082\u0001\u001a\n\u0012\u0004\u0012\u00020E\u0018\u00010>\u0012\u0011\b\u0002\u0010\u0083\u0001\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010>\u0012\u000b\b\u0002\u0010\u0084\u0001\u001a\u0004\u0018\u00010\b\u0012\u000b\b\u0002\u0010\u0085\u0001\u001a\u0004\u0018\u00010\f\u0012\u000b\b\u0002\u0010\u0086\u0001\u001a\u0004\u0018\u00010\u0017\u0012\u000b\b\u0002\u0010\u0087\u0001\u001a\u0004\u0018\u00010K\u0012\u000b\b\u0002\u0010\u0088\u0001\u001a\u0004\u0018\u00010N\u0012\u000b\b\u0002\u0010\u0089\u0001\u001a\u0004\u0018\u00010Q\u0012\u0011\b\u0002\u0010\u008a\u0001\u001a\n\u0018\u00010\u0017j\u0004\u0018\u0001`T\u0012\u000b\b\u0002\u0010\u008b\u0001\u001a\u0004\u0018\u00010V\u0012\u000b\b\u0002\u0010\u008c\u0001\u001a\u0004\u0018\u00010\u0017\u0012\u0011\b\u0002\u0010\u008d\u0001\u001a\n\u0012\u0004\u0012\u00020Z\u0018\u00010>\u0012\u0011\b\u0002\u0010\u008e\u0001\u001a\n\u0012\u0004\u0012\u00020\\\u0018\u00010>\u0012\u0011\b\u0002\u0010\u008f\u0001\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003\u0012\u000b\b\u0002\u0010\u0090\u0001\u001a\u0004\u0018\u00010_\u0012\u000b\b\u0002\u0010\u0091\u0001\u001a\u0004\u0018\u00010b\u0012\u0011\b\u0002\u0010\u0092\u0001\u001a\n\u0012\u0004\u0012\u00020e\u0018\u00010>\u0012\u000b\b\u0002\u0010\u0093\u0001\u001a\u0004\u0018\u00010g\u0012\u000b\b\u0002\u0010\u0094\u0001\u001a\u0004\u0018\u00010\f\u0012\t\b\u0002\u0010\u0095\u0001\u001a\u00020\f\u0012\u000b\b\u0002\u0010\u0096\u0001\u001a\u0004\u0018\u00010l\u0012\u000b\b\u0002\u0010\u0097\u0001\u001a\u0004\u0018\u00010K\u0012\u0011\b\u0002\u0010\u0098\u0001\u001a\n\u0018\u00010\u0017j\u0004\u0018\u0001`p\u0012\u0011\b\u0002\u0010\u0099\u0001\u001a\n\u0018\u00010\u0017j\u0004\u0018\u0001`p\u0012\u0011\b\u0002\u0010\u009a\u0001\u001a\n\u0012\u0004\u0012\u00020s\u0018\u00010>\u0012\u000b\b\u0002\u0010\u009b\u0001\u001a\u0004\u0018\u00010\b¢\u0006\u0006\bÝ\u0001\u0010Þ\u0001B\u0018\b\u0016\u0012\u000b\u0010ß\u0001\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0006\bÝ\u0001\u0010à\u0001J\u0019\u0010\u0005\u001a\u00020\u00002\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u0019\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0007¢\u0006\u0004\b\n\u0010\u000bJ\r\u0010\r\u001a\u00020\f¢\u0006\u0004\b\r\u0010\u000eJ\r\u0010\u000f\u001a\u00020\f¢\u0006\u0004\b\u000f\u0010\u000eJ\r\u0010\u0010\u001a\u00020\f¢\u0006\u0004\b\u0010\u0010\u000eJ\r\u0010\u0011\u001a\u00020\f¢\u0006\u0004\b\u0011\u0010\u000eJ\r\u0010\u0012\u001a\u00020\f¢\u0006\u0004\b\u0012\u0010\u000eJ\r\u0010\u0013\u001a\u00020\f¢\u0006\u0004\b\u0013\u0010\u000eJ\r\u0010\u0014\u001a\u00020\f¢\u0006\u0004\b\u0014\u0010\u000eJ\r\u0010\u0015\u001a\u00020\f¢\u0006\u0004\b\u0015\u0010\u000eJ\r\u0010\u0016\u001a\u00020\f¢\u0006\u0004\b\u0016\u0010\u000eJ\u0015\u0010\u0019\u001a\u00020\f2\u0006\u0010\u0018\u001a\u00020\u0017¢\u0006\u0004\b\u0019\u0010\u001aJ\r\u0010\u001b\u001a\u00020\f¢\u0006\u0004\b\u001b\u0010\u000eJ\r\u0010\u001c\u001a\u00020\f¢\u0006\u0004\b\u001c\u0010\u000eJ\r\u0010\u001d\u001a\u00020\f¢\u0006\u0004\b\u001d\u0010\u000eJ\r\u0010\u001e\u001a\u00020\f¢\u0006\u0004\b\u001e\u0010\u000eJ\r\u0010\u001f\u001a\u00020\f¢\u0006\u0004\b\u001f\u0010\u000eJ\r\u0010 \u001a\u00020\u0017¢\u0006\u0004\b \u0010!J\r\u0010\"\u001a\u00020\f¢\u0006\u0004\b\"\u0010\u000eJ\r\u0010#\u001a\u00020\f¢\u0006\u0004\b#\u0010\u000eJ\r\u0010$\u001a\u00020\f¢\u0006\u0004\b$\u0010\u000eJ\r\u0010%\u001a\u00020\f¢\u0006\u0004\b%\u0010\u000eJ\r\u0010&\u001a\u00020\f¢\u0006\u0004\b&\u0010\u000eJ\r\u0010'\u001a\u00020\f¢\u0006\u0004\b'\u0010\u000eJ\r\u0010(\u001a\u00020\f¢\u0006\u0004\b(\u0010\u000eJ\r\u0010)\u001a\u00020\u0002¢\u0006\u0004\b)\u0010*J\u0014\u0010,\u001a\u00060\u0017j\u0002`+HÆ\u0003¢\u0006\u0004\b,\u0010!J\u0014\u0010.\u001a\u00060\u0017j\u0002`-HÆ\u0003¢\u0006\u0004\b.\u0010!J\u0018\u00100\u001a\n\u0018\u00010\u0017j\u0004\u0018\u0001`/HÆ\u0003¢\u0006\u0004\b0\u00101J\u0012\u00103\u001a\u0004\u0018\u000102HÆ\u0003¢\u0006\u0004\b3\u00104J\u0012\u00105\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b5\u00106J\u0012\u00108\u001a\u0004\u0018\u000107HÆ\u0003¢\u0006\u0004\b8\u00109J\u0012\u0010:\u001a\u0004\u0018\u000107HÆ\u0003¢\u0006\u0004\b:\u00109J\u0012\u0010;\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b;\u0010<J\u0012\u0010=\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b=\u0010<J\u0018\u0010?\u001a\n\u0012\u0004\u0012\u000202\u0018\u00010>HÆ\u0003¢\u0006\u0004\b?\u0010@J\u001c\u0010B\u001a\u000e\u0012\b\u0012\u00060\u0017j\u0002`A\u0018\u00010>HÆ\u0003¢\u0006\u0004\bB\u0010@J\u0018\u0010D\u001a\n\u0012\u0004\u0012\u00020C\u0018\u00010>HÆ\u0003¢\u0006\u0004\bD\u0010@J\u0018\u0010F\u001a\n\u0012\u0004\u0012\u00020E\u0018\u00010>HÆ\u0003¢\u0006\u0004\bF\u0010@J\u0018\u0010G\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010>HÆ\u0003¢\u0006\u0004\bG\u0010@J\u0012\u0010H\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\bH\u00106J\u0012\u0010I\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\bI\u0010<J\u0012\u0010J\u001a\u0004\u0018\u00010\u0017HÆ\u0003¢\u0006\u0004\bJ\u00101J\u0012\u0010L\u001a\u0004\u0018\u00010KHÆ\u0003¢\u0006\u0004\bL\u0010MJ\u0012\u0010O\u001a\u0004\u0018\u00010NHÆ\u0003¢\u0006\u0004\bO\u0010PJ\u0012\u0010R\u001a\u0004\u0018\u00010QHÆ\u0003¢\u0006\u0004\bR\u0010SJ\u0018\u0010U\u001a\n\u0018\u00010\u0017j\u0004\u0018\u0001`THÆ\u0003¢\u0006\u0004\bU\u00101J\u0012\u0010W\u001a\u0004\u0018\u00010VHÆ\u0003¢\u0006\u0004\bW\u0010XJ\u0012\u0010Y\u001a\u0004\u0018\u00010\u0017HÆ\u0003¢\u0006\u0004\bY\u00101J\u0018\u0010[\u001a\n\u0012\u0004\u0012\u00020Z\u0018\u00010>HÆ\u0003¢\u0006\u0004\b[\u0010@J\u0018\u0010]\u001a\n\u0012\u0004\u0012\u00020\\\u0018\u00010>HÆ\u0003¢\u0006\u0004\b]\u0010@J\u0018\u0010^\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003HÆ\u0003¢\u0006\u0004\b^\u0010*J\u0012\u0010`\u001a\u0004\u0018\u00010_HÆ\u0003¢\u0006\u0004\b`\u0010aJ\u0012\u0010c\u001a\u0004\u0018\u00010bHÆ\u0003¢\u0006\u0004\bc\u0010dJ\u0018\u0010f\u001a\n\u0012\u0004\u0012\u00020e\u0018\u00010>HÆ\u0003¢\u0006\u0004\bf\u0010@J\u0012\u0010h\u001a\u0004\u0018\u00010gHÆ\u0003¢\u0006\u0004\bh\u0010iJ\u0012\u0010j\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\bj\u0010<J\u0010\u0010k\u001a\u00020\fHÆ\u0003¢\u0006\u0004\bk\u0010\u000eJ\u0012\u0010m\u001a\u0004\u0018\u00010lHÆ\u0003¢\u0006\u0004\bm\u0010nJ\u0012\u0010o\u001a\u0004\u0018\u00010KHÆ\u0003¢\u0006\u0004\bo\u0010MJ\u0018\u0010q\u001a\n\u0018\u00010\u0017j\u0004\u0018\u0001`pHÆ\u0003¢\u0006\u0004\bq\u00101J\u0018\u0010r\u001a\n\u0018\u00010\u0017j\u0004\u0018\u0001`pHÆ\u0003¢\u0006\u0004\br\u00101J\u0018\u0010t\u001a\n\u0012\u0004\u0012\u00020s\u0018\u00010>HÆ\u0003¢\u0006\u0004\bt\u0010@J\u0012\u0010u\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\bu\u00106JÑ\u0004\u0010\u009c\u0001\u001a\u00020\u00002\f\b\u0002\u0010v\u001a\u00060\u0017j\u0002`+2\f\b\u0002\u0010w\u001a\u00060\u0017j\u0002`-2\u0010\b\u0002\u0010x\u001a\n\u0018\u00010\u0017j\u0004\u0018\u0001`/2\n\b\u0002\u0010y\u001a\u0004\u0018\u0001022\n\b\u0002\u0010z\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010{\u001a\u0004\u0018\u0001072\n\b\u0002\u0010|\u001a\u0004\u0018\u0001072\n\b\u0002\u0010}\u001a\u0004\u0018\u00010\f2\n\b\u0002\u0010~\u001a\u0004\u0018\u00010\f2\u0010\b\u0002\u0010\u007f\u001a\n\u0012\u0004\u0012\u000202\u0018\u00010>2\u0015\b\u0002\u0010\u0080\u0001\u001a\u000e\u0012\b\u0012\u00060\u0017j\u0002`A\u0018\u00010>2\u0011\b\u0002\u0010\u0081\u0001\u001a\n\u0012\u0004\u0012\u00020C\u0018\u00010>2\u0011\b\u0002\u0010\u0082\u0001\u001a\n\u0012\u0004\u0012\u00020E\u0018\u00010>2\u0011\b\u0002\u0010\u0083\u0001\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010>2\u000b\b\u0002\u0010\u0084\u0001\u001a\u0004\u0018\u00010\b2\u000b\b\u0002\u0010\u0085\u0001\u001a\u0004\u0018\u00010\f2\u000b\b\u0002\u0010\u0086\u0001\u001a\u0004\u0018\u00010\u00172\u000b\b\u0002\u0010\u0087\u0001\u001a\u0004\u0018\u00010K2\u000b\b\u0002\u0010\u0088\u0001\u001a\u0004\u0018\u00010N2\u000b\b\u0002\u0010\u0089\u0001\u001a\u0004\u0018\u00010Q2\u0011\b\u0002\u0010\u008a\u0001\u001a\n\u0018\u00010\u0017j\u0004\u0018\u0001`T2\u000b\b\u0002\u0010\u008b\u0001\u001a\u0004\u0018\u00010V2\u000b\b\u0002\u0010\u008c\u0001\u001a\u0004\u0018\u00010\u00172\u0011\b\u0002\u0010\u008d\u0001\u001a\n\u0012\u0004\u0012\u00020Z\u0018\u00010>2\u0011\b\u0002\u0010\u008e\u0001\u001a\n\u0012\u0004\u0012\u00020\\\u0018\u00010>2\u0011\b\u0002\u0010\u008f\u0001\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\u000b\b\u0002\u0010\u0090\u0001\u001a\u0004\u0018\u00010_2\u000b\b\u0002\u0010\u0091\u0001\u001a\u0004\u0018\u00010b2\u0011\b\u0002\u0010\u0092\u0001\u001a\n\u0012\u0004\u0012\u00020e\u0018\u00010>2\u000b\b\u0002\u0010\u0093\u0001\u001a\u0004\u0018\u00010g2\u000b\b\u0002\u0010\u0094\u0001\u001a\u0004\u0018\u00010\f2\t\b\u0002\u0010\u0095\u0001\u001a\u00020\f2\u000b\b\u0002\u0010\u0096\u0001\u001a\u0004\u0018\u00010l2\u000b\b\u0002\u0010\u0097\u0001\u001a\u0004\u0018\u00010K2\u0011\b\u0002\u0010\u0098\u0001\u001a\n\u0018\u00010\u0017j\u0004\u0018\u0001`p2\u0011\b\u0002\u0010\u0099\u0001\u001a\n\u0018\u00010\u0017j\u0004\u0018\u0001`p2\u0011\b\u0002\u0010\u009a\u0001\u001a\n\u0012\u0004\u0012\u00020s\u0018\u00010>2\u000b\b\u0002\u0010\u009b\u0001\u001a\u0004\u0018\u00010\bHÆ\u0001¢\u0006\u0006\b\u009c\u0001\u0010\u009d\u0001J\u0012\u0010\u009e\u0001\u001a\u00020\bHÖ\u0001¢\u0006\u0005\b\u009e\u0001\u00106J\u0013\u0010\u009f\u0001\u001a\u00020KHÖ\u0001¢\u0006\u0006\b\u009f\u0001\u0010 \u0001J\u001e\u0010¢\u0001\u001a\u00020\f2\t\u0010¡\u0001\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0006\b¢\u0001\u0010£\u0001R\u001f\u0010\u0094\u0001\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u0094\u0001\u0010¤\u0001\u001a\u0005\b¥\u0001\u0010<R\u001d\u0010~\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\u000e\n\u0005\b~\u0010¤\u0001\u001a\u0005\b¦\u0001\u0010<R%\u0010\u0083\u0001\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010>8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u0083\u0001\u0010§\u0001\u001a\u0005\b¨\u0001\u0010@R\u001f\u0010\u0097\u0001\u001a\u0004\u0018\u00010K8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u0097\u0001\u0010©\u0001\u001a\u0005\bª\u0001\u0010MR#\u0010x\u001a\n\u0018\u00010\u0017j\u0004\u0018\u0001`/8\u0006@\u0006¢\u0006\u000e\n\u0005\bx\u0010«\u0001\u001a\u0005\b¬\u0001\u00101R\u001d\u0010{\u001a\u0004\u0018\u0001078\u0006@\u0006¢\u0006\u000e\n\u0005\b{\u0010\u00ad\u0001\u001a\u0005\b®\u0001\u00109R\u001f\u0010\u008b\u0001\u001a\u0004\u0018\u00010V8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u008b\u0001\u0010¯\u0001\u001a\u0005\b°\u0001\u0010XR\u001d\u0010}\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\u000e\n\u0005\b}\u0010¤\u0001\u001a\u0005\b±\u0001\u0010<R\u001f\u0010\u0088\u0001\u001a\u0004\u0018\u00010N8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u0088\u0001\u0010²\u0001\u001a\u0005\b³\u0001\u0010PR\u001f\u0010\u008c\u0001\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\u000f\n\u0006\b\u008c\u0001\u0010«\u0001\u001a\u0005\b´\u0001\u00101R\u001f\u0010\u0091\u0001\u001a\u0004\u0018\u00010b8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u0091\u0001\u0010µ\u0001\u001a\u0005\b¶\u0001\u0010dR%\u0010\u0082\u0001\u001a\n\u0012\u0004\u0012\u00020E\u0018\u00010>8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u0082\u0001\u0010§\u0001\u001a\u0005\b·\u0001\u0010@R\u001f\u0010\u0085\u0001\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u0085\u0001\u0010¤\u0001\u001a\u0005\b¸\u0001\u0010<R\u001f\u0010\u0089\u0001\u001a\u0004\u0018\u00010Q8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u0089\u0001\u0010¹\u0001\u001a\u0005\bº\u0001\u0010SR\u001d\u0010|\u001a\u0004\u0018\u0001078\u0006@\u0006¢\u0006\u000e\n\u0005\b|\u0010\u00ad\u0001\u001a\u0005\b»\u0001\u00109R#\u0010\u007f\u001a\n\u0012\u0004\u0012\u000202\u0018\u00010>8\u0006@\u0006¢\u0006\u000e\n\u0005\b\u007f\u0010§\u0001\u001a\u0005\b¼\u0001\u0010@R\u001d\u0010y\u001a\u0004\u0018\u0001028\u0006@\u0006¢\u0006\u000e\n\u0005\by\u0010½\u0001\u001a\u0005\b¾\u0001\u00104R%\u0010\u008d\u0001\u001a\n\u0012\u0004\u0012\u00020Z\u0018\u00010>8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u008d\u0001\u0010§\u0001\u001a\u0005\b¿\u0001\u0010@R\u001f\u0010w\u001a\u00060\u0017j\u0002`-8\u0006@\u0006¢\u0006\u000e\n\u0005\bw\u0010À\u0001\u001a\u0005\bÁ\u0001\u0010!R\u001f\u0010\u0093\u0001\u001a\u0004\u0018\u00010g8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u0093\u0001\u0010Â\u0001\u001a\u0005\bÃ\u0001\u0010iR%\u0010\u0098\u0001\u001a\n\u0018\u00010\u0017j\u0004\u0018\u0001`p8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u0098\u0001\u0010«\u0001\u001a\u0005\bÄ\u0001\u00101R%\u0010\u009a\u0001\u001a\n\u0012\u0004\u0012\u00020s\u0018\u00010>8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u009a\u0001\u0010§\u0001\u001a\u0005\bÅ\u0001\u0010@R\u001d\u0010z\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\u000e\n\u0005\bz\u0010Æ\u0001\u001a\u0005\bÇ\u0001\u00106R\u001f\u0010\u0086\u0001\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\u000f\n\u0006\b\u0086\u0001\u0010«\u0001\u001a\u0005\bÈ\u0001\u00101R\u001f\u0010v\u001a\u00060\u0017j\u0002`+8\u0006@\u0006¢\u0006\u000e\n\u0005\bv\u0010À\u0001\u001a\u0005\bÉ\u0001\u0010!R%\u0010\u008a\u0001\u001a\n\u0018\u00010\u0017j\u0004\u0018\u0001`T8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u008a\u0001\u0010«\u0001\u001a\u0005\bÊ\u0001\u00101R'\u0010Ë\u0001\u001a\u0010\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t\u0018\u00010\u00078\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\bË\u0001\u0010Ì\u0001R%\u0010\u0081\u0001\u001a\n\u0012\u0004\u0012\u00020C\u0018\u00010>8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u0081\u0001\u0010§\u0001\u001a\u0005\bÍ\u0001\u0010@R\u001f\u0010\u0087\u0001\u001a\u0004\u0018\u00010K8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u0087\u0001\u0010©\u0001\u001a\u0005\bÎ\u0001\u0010MR%\u0010\u008e\u0001\u001a\n\u0012\u0004\u0012\u00020\\\u0018\u00010>8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u008e\u0001\u0010§\u0001\u001a\u0005\bÏ\u0001\u0010@R)\u0010\u0080\u0001\u001a\u000e\u0012\b\u0012\u00060\u0017j\u0002`A\u0018\u00010>8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u0080\u0001\u0010§\u0001\u001a\u0005\bÐ\u0001\u0010@R\u001f\u0010\u0090\u0001\u001a\u0004\u0018\u00010_8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u0090\u0001\u0010Ñ\u0001\u001a\u0005\bÒ\u0001\u0010aR%\u0010\u0099\u0001\u001a\n\u0018\u00010\u0017j\u0004\u0018\u0001`p8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u0099\u0001\u0010«\u0001\u001a\u0005\bÓ\u0001\u00101R%\u0010\u0092\u0001\u001a\n\u0012\u0004\u0012\u00020e\u0018\u00010>8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u0092\u0001\u0010§\u0001\u001a\u0005\bÔ\u0001\u0010@R%\u0010\u008f\u0001\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00038\u0006@\u0006¢\u0006\u000f\n\u0006\b\u008f\u0001\u0010Õ\u0001\u001a\u0005\bÖ\u0001\u0010*R\u001d\u0010\u0095\u0001\u001a\u00020\f8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u0095\u0001\u0010×\u0001\u001a\u0005\bØ\u0001\u0010\u000eR\u001f\u0010\u0084\u0001\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u0084\u0001\u0010Æ\u0001\u001a\u0005\bÙ\u0001\u00106R\u001f\u0010\u0096\u0001\u001a\u0004\u0018\u00010l8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u0096\u0001\u0010Ú\u0001\u001a\u0005\bÛ\u0001\u0010nR\u001f\u0010\u009b\u0001\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u009b\u0001\u0010Æ\u0001\u001a\u0005\bÜ\u0001\u00106¨\u0006â\u0001"}, d2 = {"Lcom/discord/models/message/Message;", "", "Lcom/discord/api/message/Message;", "Lcom/discord/models/message/ApiMessage;", "new", "merge", "(Lcom/discord/api/message/Message;)Lcom/discord/models/message/Message;", "", "", "Lcom/discord/api/message/reaction/MessageReaction;", "getReactionsMap", "()Ljava/util/Map;", "", "hasEmbeds", "()Z", "hasAttachments", "hasStickers", "isLocal", "isLocalApplicationCommand", "isInteraction", "isEmbeddedMessageType", "isFailed", "canResend", "", "flag", "hasFlag", "(J)Z", "isCrossposted", "isCrosspost", "isUrgent", "isSourceDeleted", "hasThread", "getCallDuration", "()J", "isWebhook", "isUserMessage", "isApplicationMessage", "isSystemMessage", "isEphemeralMessage", "isLoading", "isSpotifyListeningActivity", "synthesizeApiMessage", "()Lcom/discord/api/message/Message;", "Lcom/discord/primitives/MessageId;", "component1", "Lcom/discord/primitives/ChannelId;", "component2", "Lcom/discord/primitives/GuildId;", "component3", "()Ljava/lang/Long;", "Lcom/discord/api/user/User;", "component4", "()Lcom/discord/api/user/User;", "component5", "()Ljava/lang/String;", "Lcom/discord/api/utcdatetime/UtcDateTime;", "component6", "()Lcom/discord/api/utcdatetime/UtcDateTime;", "component7", "component8", "()Ljava/lang/Boolean;", "component9", "", "component10", "()Ljava/util/List;", "Lcom/discord/primitives/RoleId;", "component11", "Lcom/discord/api/message/attachment/MessageAttachment;", "component12", "Lcom/discord/api/message/embed/MessageEmbed;", "component13", "component14", "component15", "component16", "component17", "", "component18", "()Ljava/lang/Integer;", "Lcom/discord/api/message/activity/MessageActivity;", "component19", "()Lcom/discord/api/message/activity/MessageActivity;", "Lcom/discord/api/application/Application;", "component20", "()Lcom/discord/api/application/Application;", "Lcom/discord/primitives/ApplicationId;", "component21", "Lcom/discord/api/message/MessageReference;", "component22", "()Lcom/discord/api/message/MessageReference;", "component23", "Lcom/discord/api/sticker/Sticker;", "component24", "Lcom/discord/api/sticker/StickerPartial;", "component25", "component26", "Lcom/discord/api/interaction/Interaction;", "component27", "()Lcom/discord/api/interaction/Interaction;", "Lcom/discord/api/channel/Channel;", "component28", "()Lcom/discord/api/channel/Channel;", "Lcom/discord/api/botuikit/Component;", "component29", "Lcom/discord/api/message/call/MessageCall;", "component30", "()Lcom/discord/api/message/call/MessageCall;", "component31", "component32", "Lcom/discord/api/message/allowedmentions/MessageAllowedMentions;", "component33", "()Lcom/discord/api/message/allowedmentions/MessageAllowedMentions;", "component34", "Lcom/discord/primitives/Timestamp;", "component35", "component36", "Lcom/discord/api/message/LocalAttachment;", "component37", "component38", ModelAuditLogEntry.CHANGE_KEY_ID, "channelId", "guildId", "author", "content", "timestamp", "editedTimestamp", "tts", "mentionEveryone", "mentions", "mentionRoles", "attachments", "embeds", "reactions", "nonce", "pinned", "webhookId", "type", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "application", "applicationId", "messageReference", "flags", "stickers", "stickerItems", "referencedMessage", "interaction", "thread", "components", NotificationCompat.CATEGORY_CALL, "hit", "hasLocalUploads", "allowedMentions", "numRetries", "lastManualAttemptTimestamp", "initialAttemptTimestamp", "localAttachments", "captchaKey", "copy", "(JJLjava/lang/Long;Lcom/discord/api/user/User;Ljava/lang/String;Lcom/discord/api/utcdatetime/UtcDateTime;Lcom/discord/api/utcdatetime/UtcDateTime;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Integer;Lcom/discord/api/message/activity/MessageActivity;Lcom/discord/api/application/Application;Ljava/lang/Long;Lcom/discord/api/message/MessageReference;Ljava/lang/Long;Ljava/util/List;Ljava/util/List;Lcom/discord/api/message/Message;Lcom/discord/api/interaction/Interaction;Lcom/discord/api/channel/Channel;Ljava/util/List;Lcom/discord/api/message/call/MessageCall;Ljava/lang/Boolean;ZLcom/discord/api/message/allowedmentions/MessageAllowedMentions;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;Ljava/util/List;Ljava/lang/String;)Lcom/discord/models/message/Message;", "toString", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Boolean;", "getHit", "getMentionEveryone", "Ljava/util/List;", "getReactions", "Ljava/lang/Integer;", "getNumRetries", "Ljava/lang/Long;", "getGuildId", "Lcom/discord/api/utcdatetime/UtcDateTime;", "getTimestamp", "Lcom/discord/api/message/MessageReference;", "getMessageReference", "getTts", "Lcom/discord/api/message/activity/MessageActivity;", "getActivity", "getFlags", "Lcom/discord/api/channel/Channel;", "getThread", "getEmbeds", "getPinned", "Lcom/discord/api/application/Application;", "getApplication", "getEditedTimestamp", "getMentions", "Lcom/discord/api/user/User;", "getAuthor", "getStickers", "J", "getChannelId", "Lcom/discord/api/message/call/MessageCall;", "getCall", "getLastManualAttemptTimestamp", "getLocalAttachments", "Ljava/lang/String;", "getContent", "getWebhookId", "getId", "getApplicationId", "cachedReactionsMap", "Ljava/util/Map;", "getAttachments", "getType", "getStickerItems", "getMentionRoles", "Lcom/discord/api/interaction/Interaction;", "getInteraction", "getInitialAttemptTimestamp", "getComponents", "Lcom/discord/api/message/Message;", "getReferencedMessage", "Z", "getHasLocalUploads", "getNonce", "Lcom/discord/api/message/allowedmentions/MessageAllowedMentions;", "getAllowedMentions", "getCaptchaKey", HookHelper.constructorName, "(JJLjava/lang/Long;Lcom/discord/api/user/User;Ljava/lang/String;Lcom/discord/api/utcdatetime/UtcDateTime;Lcom/discord/api/utcdatetime/UtcDateTime;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Integer;Lcom/discord/api/message/activity/MessageActivity;Lcom/discord/api/application/Application;Ljava/lang/Long;Lcom/discord/api/message/MessageReference;Ljava/lang/Long;Ljava/util/List;Ljava/util/List;Lcom/discord/api/message/Message;Lcom/discord/api/interaction/Interaction;Lcom/discord/api/channel/Channel;Ljava/util/List;Lcom/discord/api/message/call/MessageCall;Ljava/lang/Boolean;ZLcom/discord/api/message/allowedmentions/MessageAllowedMentions;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;Ljava/util/List;Ljava/lang/String;)V", "message", "(Lcom/discord/api/message/Message;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class Message {
    public static final Companion Companion = new Companion(null);
    private static final Map<String, MessageReaction> EMPTY_REACTIONS = h0.emptyMap();
    private final MessageActivity activity;
    private final MessageAllowedMentions allowedMentions;
    private final Application application;
    private final Long applicationId;
    private final List<MessageAttachment> attachments;
    private final User author;
    private Map<String, MessageReaction> cachedReactionsMap;
    private final MessageCall call;
    private final String captchaKey;
    private final long channelId;
    private final List<Component> components;
    private final String content;
    private final UtcDateTime editedTimestamp;
    private final List<MessageEmbed> embeds;
    private final Long flags;
    private final Long guildId;
    private final transient boolean hasLocalUploads;
    private final Boolean hit;

    /* renamed from: id  reason: collision with root package name */
    private final long f2720id;
    private final Long initialAttemptTimestamp;
    private final Interaction interaction;
    private final Long lastManualAttemptTimestamp;
    private final List<LocalAttachment> localAttachments;
    private final Boolean mentionEveryone;
    private final List<Long> mentionRoles;
    private final List<User> mentions;
    private final MessageReference messageReference;
    private final String nonce;
    private final Integer numRetries;
    private final Boolean pinned;
    private final List<MessageReaction> reactions;
    private final com.discord.api.message.Message referencedMessage;
    private final List<StickerPartial> stickerItems;
    private final List<Sticker> stickers;
    private final Channel thread;
    private final UtcDateTime timestamp;
    private final Boolean tts;
    private final Integer type;
    private final Long webhookId;

    /* compiled from: Message.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\"\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/models/message/Message$Companion;", "", "", "", "Lcom/discord/api/message/reaction/MessageReaction;", "EMPTY_REACTIONS", "Ljava/util/Map;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public Message(long j, long j2, Long l, User user, String str, UtcDateTime utcDateTime, UtcDateTime utcDateTime2, Boolean bool, Boolean bool2, List<User> list, List<Long> list2, List<MessageAttachment> list3, List<MessageEmbed> list4, List<MessageReaction> list5, String str2, Boolean bool3, Long l2, Integer num, MessageActivity messageActivity, Application application, Long l3, MessageReference messageReference, Long l4, List<Sticker> list6, List<StickerPartial> list7, com.discord.api.message.Message message, Interaction interaction, Channel channel, List<? extends Component> list8, MessageCall messageCall, Boolean bool4, boolean z2, MessageAllowedMentions messageAllowedMentions, Integer num2, Long l5, Long l6, List<LocalAttachment> list9, String str3) {
        this.f2720id = j;
        this.channelId = j2;
        this.guildId = l;
        this.author = user;
        this.content = str;
        this.timestamp = utcDateTime;
        this.editedTimestamp = utcDateTime2;
        this.tts = bool;
        this.mentionEveryone = bool2;
        this.mentions = list;
        this.mentionRoles = list2;
        this.attachments = list3;
        this.embeds = list4;
        this.reactions = list5;
        this.nonce = str2;
        this.pinned = bool3;
        this.webhookId = l2;
        this.type = num;
        this.activity = messageActivity;
        this.application = application;
        this.applicationId = l3;
        this.messageReference = messageReference;
        this.flags = l4;
        this.stickers = list6;
        this.stickerItems = list7;
        this.referencedMessage = message;
        this.interaction = interaction;
        this.thread = channel;
        this.components = list8;
        this.call = messageCall;
        this.hit = bool4;
        this.hasLocalUploads = z2;
        this.allowedMentions = messageAllowedMentions;
        this.numRetries = num2;
        this.lastManualAttemptTimestamp = l5;
        this.initialAttemptTimestamp = l6;
        this.localAttachments = list9;
        this.captchaKey = str3;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ Message copy$default(Message message, long j, long j2, Long l, User user, String str, UtcDateTime utcDateTime, UtcDateTime utcDateTime2, Boolean bool, Boolean bool2, List list, List list2, List list3, List list4, List list5, String str2, Boolean bool3, Long l2, Integer num, MessageActivity messageActivity, Application application, Long l3, MessageReference messageReference, Long l4, List list6, List list7, com.discord.api.message.Message message2, Interaction interaction, Channel channel, List list8, MessageCall messageCall, Boolean bool4, boolean z2, MessageAllowedMentions messageAllowedMentions, Integer num2, Long l5, Long l6, List list9, String str3, int i, int i2, Object obj) {
        return message.copy((i & 1) != 0 ? message.f2720id : j, (i & 2) != 0 ? message.channelId : j2, (i & 4) != 0 ? message.guildId : l, (i & 8) != 0 ? message.author : user, (i & 16) != 0 ? message.content : str, (i & 32) != 0 ? message.timestamp : utcDateTime, (i & 64) != 0 ? message.editedTimestamp : utcDateTime2, (i & 128) != 0 ? message.tts : bool, (i & 256) != 0 ? message.mentionEveryone : bool2, (i & 512) != 0 ? message.mentions : list, (i & 1024) != 0 ? message.mentionRoles : list2, (i & 2048) != 0 ? message.attachments : list3, (i & 4096) != 0 ? message.embeds : list4, (i & 8192) != 0 ? message.reactions : list5, (i & 16384) != 0 ? message.nonce : str2, (i & 32768) != 0 ? message.pinned : bool3, (i & 65536) != 0 ? message.webhookId : l2, (i & 131072) != 0 ? message.type : num, (i & 262144) != 0 ? message.activity : messageActivity, (i & 524288) != 0 ? message.application : application, (i & 1048576) != 0 ? message.applicationId : l3, (i & 2097152) != 0 ? message.messageReference : messageReference, (i & 4194304) != 0 ? message.flags : l4, (i & 8388608) != 0 ? message.stickers : list6, (i & 16777216) != 0 ? message.stickerItems : list7, (i & 33554432) != 0 ? message.referencedMessage : message2, (i & 67108864) != 0 ? message.interaction : interaction, (i & 134217728) != 0 ? message.thread : channel, (i & 268435456) != 0 ? message.components : list8, (i & 536870912) != 0 ? message.call : messageCall, (i & BasicMeasure.EXACTLY) != 0 ? message.hit : bool4, (i & Integer.MIN_VALUE) != 0 ? message.hasLocalUploads : z2, (i2 & 1) != 0 ? message.allowedMentions : messageAllowedMentions, (i2 & 2) != 0 ? message.numRetries : num2, (i2 & 4) != 0 ? message.lastManualAttemptTimestamp : l5, (i2 & 8) != 0 ? message.initialAttemptTimestamp : l6, (i2 & 16) != 0 ? message.localAttachments : list9, (i2 & 32) != 0 ? message.captchaKey : str3);
    }

    public final boolean canResend() {
        Integer num = this.type;
        return num != null && num.intValue() == -2;
    }

    public final long component1() {
        return this.f2720id;
    }

    public final List<User> component10() {
        return this.mentions;
    }

    public final List<Long> component11() {
        return this.mentionRoles;
    }

    public final List<MessageAttachment> component12() {
        return this.attachments;
    }

    public final List<MessageEmbed> component13() {
        return this.embeds;
    }

    public final List<MessageReaction> component14() {
        return this.reactions;
    }

    public final String component15() {
        return this.nonce;
    }

    public final Boolean component16() {
        return this.pinned;
    }

    public final Long component17() {
        return this.webhookId;
    }

    public final Integer component18() {
        return this.type;
    }

    public final MessageActivity component19() {
        return this.activity;
    }

    public final long component2() {
        return this.channelId;
    }

    public final Application component20() {
        return this.application;
    }

    public final Long component21() {
        return this.applicationId;
    }

    public final MessageReference component22() {
        return this.messageReference;
    }

    public final Long component23() {
        return this.flags;
    }

    public final List<Sticker> component24() {
        return this.stickers;
    }

    public final List<StickerPartial> component25() {
        return this.stickerItems;
    }

    public final com.discord.api.message.Message component26() {
        return this.referencedMessage;
    }

    public final Interaction component27() {
        return this.interaction;
    }

    public final Channel component28() {
        return this.thread;
    }

    public final List<Component> component29() {
        return this.components;
    }

    public final Long component3() {
        return this.guildId;
    }

    public final MessageCall component30() {
        return this.call;
    }

    public final Boolean component31() {
        return this.hit;
    }

    public final boolean component32() {
        return this.hasLocalUploads;
    }

    public final MessageAllowedMentions component33() {
        return this.allowedMentions;
    }

    public final Integer component34() {
        return this.numRetries;
    }

    public final Long component35() {
        return this.lastManualAttemptTimestamp;
    }

    public final Long component36() {
        return this.initialAttemptTimestamp;
    }

    public final List<LocalAttachment> component37() {
        return this.localAttachments;
    }

    public final String component38() {
        return this.captchaKey;
    }

    public final User component4() {
        return this.author;
    }

    public final String component5() {
        return this.content;
    }

    public final UtcDateTime component6() {
        return this.timestamp;
    }

    public final UtcDateTime component7() {
        return this.editedTimestamp;
    }

    public final Boolean component8() {
        return this.tts;
    }

    public final Boolean component9() {
        return this.mentionEveryone;
    }

    public final Message copy(long j, long j2, Long l, User user, String str, UtcDateTime utcDateTime, UtcDateTime utcDateTime2, Boolean bool, Boolean bool2, List<User> list, List<Long> list2, List<MessageAttachment> list3, List<MessageEmbed> list4, List<MessageReaction> list5, String str2, Boolean bool3, Long l2, Integer num, MessageActivity messageActivity, Application application, Long l3, MessageReference messageReference, Long l4, List<Sticker> list6, List<StickerPartial> list7, com.discord.api.message.Message message, Interaction interaction, Channel channel, List<? extends Component> list8, MessageCall messageCall, Boolean bool4, boolean z2, MessageAllowedMentions messageAllowedMentions, Integer num2, Long l5, Long l6, List<LocalAttachment> list9, String str3) {
        return new Message(j, j2, l, user, str, utcDateTime, utcDateTime2, bool, bool2, list, list2, list3, list4, list5, str2, bool3, l2, num, messageActivity, application, l3, messageReference, l4, list6, list7, message, interaction, channel, list8, messageCall, bool4, z2, messageAllowedMentions, num2, l5, l6, list9, str3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Message)) {
            return false;
        }
        Message message = (Message) obj;
        return this.f2720id == message.f2720id && this.channelId == message.channelId && m.areEqual(this.guildId, message.guildId) && m.areEqual(this.author, message.author) && m.areEqual(this.content, message.content) && m.areEqual(this.timestamp, message.timestamp) && m.areEqual(this.editedTimestamp, message.editedTimestamp) && m.areEqual(this.tts, message.tts) && m.areEqual(this.mentionEveryone, message.mentionEveryone) && m.areEqual(this.mentions, message.mentions) && m.areEqual(this.mentionRoles, message.mentionRoles) && m.areEqual(this.attachments, message.attachments) && m.areEqual(this.embeds, message.embeds) && m.areEqual(this.reactions, message.reactions) && m.areEqual(this.nonce, message.nonce) && m.areEqual(this.pinned, message.pinned) && m.areEqual(this.webhookId, message.webhookId) && m.areEqual(this.type, message.type) && m.areEqual(this.activity, message.activity) && m.areEqual(this.application, message.application) && m.areEqual(this.applicationId, message.applicationId) && m.areEqual(this.messageReference, message.messageReference) && m.areEqual(this.flags, message.flags) && m.areEqual(this.stickers, message.stickers) && m.areEqual(this.stickerItems, message.stickerItems) && m.areEqual(this.referencedMessage, message.referencedMessage) && m.areEqual(this.interaction, message.interaction) && m.areEqual(this.thread, message.thread) && m.areEqual(this.components, message.components) && m.areEqual(this.call, message.call) && m.areEqual(this.hit, message.hit) && this.hasLocalUploads == message.hasLocalUploads && m.areEqual(this.allowedMentions, message.allowedMentions) && m.areEqual(this.numRetries, message.numRetries) && m.areEqual(this.lastManualAttemptTimestamp, message.lastManualAttemptTimestamp) && m.areEqual(this.initialAttemptTimestamp, message.initialAttemptTimestamp) && m.areEqual(this.localAttachments, message.localAttachments) && m.areEqual(this.captchaKey, message.captchaKey);
    }

    public final MessageActivity getActivity() {
        return this.activity;
    }

    public final MessageAllowedMentions getAllowedMentions() {
        return this.allowedMentions;
    }

    public final Application getApplication() {
        return this.application;
    }

    public final Long getApplicationId() {
        return this.applicationId;
    }

    public final List<MessageAttachment> getAttachments() {
        return this.attachments;
    }

    public final User getAuthor() {
        return this.author;
    }

    public final MessageCall getCall() {
        return this.call;
    }

    public final long getCallDuration() {
        UtcDateTime a;
        UtcDateTime utcDateTime;
        MessageCall messageCall = this.call;
        if (messageCall == null || (a = messageCall.a()) == null || (utcDateTime = this.timestamp) == null) {
            return 0L;
        }
        return a.g() - utcDateTime.g();
    }

    public final String getCaptchaKey() {
        return this.captchaKey;
    }

    public final long getChannelId() {
        return this.channelId;
    }

    public final List<Component> getComponents() {
        return this.components;
    }

    public final String getContent() {
        return this.content;
    }

    public final UtcDateTime getEditedTimestamp() {
        return this.editedTimestamp;
    }

    public final List<MessageEmbed> getEmbeds() {
        return this.embeds;
    }

    public final Long getFlags() {
        return this.flags;
    }

    public final Long getGuildId() {
        return this.guildId;
    }

    public final boolean getHasLocalUploads() {
        return this.hasLocalUploads;
    }

    public final Boolean getHit() {
        return this.hit;
    }

    public final long getId() {
        return this.f2720id;
    }

    public final Long getInitialAttemptTimestamp() {
        return this.initialAttemptTimestamp;
    }

    public final Interaction getInteraction() {
        return this.interaction;
    }

    public final Long getLastManualAttemptTimestamp() {
        return this.lastManualAttemptTimestamp;
    }

    public final List<LocalAttachment> getLocalAttachments() {
        return this.localAttachments;
    }

    public final Boolean getMentionEveryone() {
        return this.mentionEveryone;
    }

    public final List<Long> getMentionRoles() {
        return this.mentionRoles;
    }

    public final List<User> getMentions() {
        return this.mentions;
    }

    public final MessageReference getMessageReference() {
        return this.messageReference;
    }

    public final String getNonce() {
        return this.nonce;
    }

    public final Integer getNumRetries() {
        return this.numRetries;
    }

    public final Boolean getPinned() {
        return this.pinned;
    }

    public final List<MessageReaction> getReactions() {
        return this.reactions;
    }

    public final Map<String, MessageReaction> getReactionsMap() {
        Map<String, MessageReaction> map = this.cachedReactionsMap;
        if (map != null) {
            m.checkNotNull(map);
        } else {
            List<MessageReaction> list = this.reactions;
            if (list == null || !(!list.isEmpty())) {
                map = EMPTY_REACTIONS;
            } else {
                List<MessageReaction> list2 = this.reactions;
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                for (Object obj : list2) {
                    linkedHashMap.put(((MessageReaction) obj).b().c(), obj);
                }
                map = linkedHashMap;
            }
            this.cachedReactionsMap = map;
        }
        return map;
    }

    public final com.discord.api.message.Message getReferencedMessage() {
        return this.referencedMessage;
    }

    public final List<StickerPartial> getStickerItems() {
        return this.stickerItems;
    }

    public final List<Sticker> getStickers() {
        return this.stickers;
    }

    public final Channel getThread() {
        return this.thread;
    }

    public final UtcDateTime getTimestamp() {
        return this.timestamp;
    }

    public final Boolean getTts() {
        return this.tts;
    }

    public final Integer getType() {
        return this.type;
    }

    public final Long getWebhookId() {
        return this.webhookId;
    }

    public final boolean hasAttachments() {
        List<MessageAttachment> list = this.attachments;
        return !(list == null || list.isEmpty());
    }

    public final boolean hasEmbeds() {
        List<MessageEmbed> list = this.embeds;
        return !(list == null || list.isEmpty());
    }

    public final boolean hasFlag(long j) {
        Long l = this.flags;
        return ((l != null ? l.longValue() : 0L) & j) == j;
    }

    public final boolean hasStickers() {
        List<Sticker> list = this.stickers;
        if (list == null || list.isEmpty()) {
            List<StickerPartial> list2 = this.stickerItems;
            if (list2 == null || list2.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    public final boolean hasThread() {
        return hasFlag(32L);
    }

    public int hashCode() {
        int a = (b.a(this.channelId) + (b.a(this.f2720id) * 31)) * 31;
        Long l = this.guildId;
        int i = 0;
        int hashCode = (a + (l != null ? l.hashCode() : 0)) * 31;
        User user = this.author;
        int hashCode2 = (hashCode + (user != null ? user.hashCode() : 0)) * 31;
        String str = this.content;
        int hashCode3 = (hashCode2 + (str != null ? str.hashCode() : 0)) * 31;
        UtcDateTime utcDateTime = this.timestamp;
        int hashCode4 = (hashCode3 + (utcDateTime != null ? utcDateTime.hashCode() : 0)) * 31;
        UtcDateTime utcDateTime2 = this.editedTimestamp;
        int hashCode5 = (hashCode4 + (utcDateTime2 != null ? utcDateTime2.hashCode() : 0)) * 31;
        Boolean bool = this.tts;
        int hashCode6 = (hashCode5 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.mentionEveryone;
        int hashCode7 = (hashCode6 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        List<User> list = this.mentions;
        int hashCode8 = (hashCode7 + (list != null ? list.hashCode() : 0)) * 31;
        List<Long> list2 = this.mentionRoles;
        int hashCode9 = (hashCode8 + (list2 != null ? list2.hashCode() : 0)) * 31;
        List<MessageAttachment> list3 = this.attachments;
        int hashCode10 = (hashCode9 + (list3 != null ? list3.hashCode() : 0)) * 31;
        List<MessageEmbed> list4 = this.embeds;
        int hashCode11 = (hashCode10 + (list4 != null ? list4.hashCode() : 0)) * 31;
        List<MessageReaction> list5 = this.reactions;
        int hashCode12 = (hashCode11 + (list5 != null ? list5.hashCode() : 0)) * 31;
        String str2 = this.nonce;
        int hashCode13 = (hashCode12 + (str2 != null ? str2.hashCode() : 0)) * 31;
        Boolean bool3 = this.pinned;
        int hashCode14 = (hashCode13 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        Long l2 = this.webhookId;
        int hashCode15 = (hashCode14 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Integer num = this.type;
        int hashCode16 = (hashCode15 + (num != null ? num.hashCode() : 0)) * 31;
        MessageActivity messageActivity = this.activity;
        int hashCode17 = (hashCode16 + (messageActivity != null ? messageActivity.hashCode() : 0)) * 31;
        Application application = this.application;
        int hashCode18 = (hashCode17 + (application != null ? application.hashCode() : 0)) * 31;
        Long l3 = this.applicationId;
        int hashCode19 = (hashCode18 + (l3 != null ? l3.hashCode() : 0)) * 31;
        MessageReference messageReference = this.messageReference;
        int hashCode20 = (hashCode19 + (messageReference != null ? messageReference.hashCode() : 0)) * 31;
        Long l4 = this.flags;
        int hashCode21 = (hashCode20 + (l4 != null ? l4.hashCode() : 0)) * 31;
        List<Sticker> list6 = this.stickers;
        int hashCode22 = (hashCode21 + (list6 != null ? list6.hashCode() : 0)) * 31;
        List<StickerPartial> list7 = this.stickerItems;
        int hashCode23 = (hashCode22 + (list7 != null ? list7.hashCode() : 0)) * 31;
        com.discord.api.message.Message message = this.referencedMessage;
        int hashCode24 = (hashCode23 + (message != null ? message.hashCode() : 0)) * 31;
        Interaction interaction = this.interaction;
        int hashCode25 = (hashCode24 + (interaction != null ? interaction.hashCode() : 0)) * 31;
        Channel channel = this.thread;
        int hashCode26 = (hashCode25 + (channel != null ? channel.hashCode() : 0)) * 31;
        List<Component> list8 = this.components;
        int hashCode27 = (hashCode26 + (list8 != null ? list8.hashCode() : 0)) * 31;
        MessageCall messageCall = this.call;
        int hashCode28 = (hashCode27 + (messageCall != null ? messageCall.hashCode() : 0)) * 31;
        Boolean bool4 = this.hit;
        int hashCode29 = (hashCode28 + (bool4 != null ? bool4.hashCode() : 0)) * 31;
        boolean z2 = this.hasLocalUploads;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        int i4 = (hashCode29 + i2) * 31;
        MessageAllowedMentions messageAllowedMentions = this.allowedMentions;
        int hashCode30 = (i4 + (messageAllowedMentions != null ? messageAllowedMentions.hashCode() : 0)) * 31;
        Integer num2 = this.numRetries;
        int hashCode31 = (hashCode30 + (num2 != null ? num2.hashCode() : 0)) * 31;
        Long l5 = this.lastManualAttemptTimestamp;
        int hashCode32 = (hashCode31 + (l5 != null ? l5.hashCode() : 0)) * 31;
        Long l6 = this.initialAttemptTimestamp;
        int hashCode33 = (hashCode32 + (l6 != null ? l6.hashCode() : 0)) * 31;
        List<LocalAttachment> list9 = this.localAttachments;
        int hashCode34 = (hashCode33 + (list9 != null ? list9.hashCode() : 0)) * 31;
        String str3 = this.captchaKey;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode34 + i;
    }

    public final boolean isApplicationMessage() {
        Integer num;
        Integer num2 = this.type;
        return (num2 != null && num2.intValue() == 20) || ((num = this.type) != null && num.intValue() == 23);
    }

    public final boolean isCrosspost() {
        return hasFlag(2L);
    }

    public final boolean isCrossposted() {
        return hasFlag(1L);
    }

    public final boolean isEmbeddedMessageType() {
        Integer num;
        Integer num2 = this.type;
        return (num2 != null && num2.intValue() == 19) || ((num = this.type) != null && num.intValue() == 21);
    }

    public final boolean isEphemeralMessage() {
        return hasFlag(64L);
    }

    public final boolean isFailed() {
        Integer num;
        Integer num2;
        Integer num3 = this.type;
        return (num3 != null && num3.intValue() == -3) || ((num = this.type) != null && num.intValue() == -2) || ((num2 = this.type) != null && num2.intValue() == -4);
    }

    public final boolean isInteraction() {
        Integer num;
        Integer num2 = this.type;
        return (num2 != null && num2.intValue() == -5) || ((num = this.type) != null && num.intValue() == -4) || this.interaction != null;
    }

    public final boolean isLoading() {
        return hasFlag(128L);
    }

    public final boolean isLocal() {
        Integer num;
        Integer num2;
        Integer num3;
        Integer num4 = this.type;
        return (num4 != null && num4.intValue() == -1) || ((num = this.type) != null && num.intValue() == -2) || (((num2 = this.type) != null && num2.intValue() == -3) || isLocalApplicationCommand() || ((num3 = this.type) != null && num3.intValue() == -6));
    }

    public final boolean isLocalApplicationCommand() {
        Integer num;
        Integer num2 = this.type;
        return (num2 != null && num2.intValue() == -5) || ((num = this.type) != null && num.intValue() == -4);
    }

    public final boolean isSourceDeleted() {
        return hasFlag(8L);
    }

    public final boolean isSpotifyListeningActivity() {
        MessageActivity messageActivity = this.activity;
        return messageActivity != null && messageActivity.b() == MessageActivityType.LISTEN && w.contains$default((CharSequence) messageActivity.a(), (CharSequence) "spotify", false, 2, (Object) null);
    }

    public final boolean isSystemMessage() {
        return !isUserMessage() && !isApplicationMessage();
    }

    public final boolean isUrgent() {
        return hasFlag(16L);
    }

    public final boolean isUserMessage() {
        Integer num;
        Integer num2 = this.type;
        return (num2 != null && num2.intValue() == 0) || ((num = this.type) != null && num.intValue() == 19);
    }

    public final boolean isWebhook() {
        return this.webhookId != null;
    }

    public final Message merge(com.discord.api.message.Message message) {
        m.checkNotNullParameter(message, "new");
        long o = message.o();
        long g = message.g() != 0 ? message.g() : this.channelId;
        Long m = message.m();
        if (m == null) {
            m = this.guildId;
        }
        Long l = m;
        User e = message.e();
        if (e == null) {
            e = this.author;
        }
        User user = e;
        String i = message.i();
        if (i == null) {
            i = this.content;
        }
        String str = i;
        UtcDateTime C = message.C();
        if (C == null) {
            C = this.timestamp;
        }
        UtcDateTime utcDateTime = C;
        UtcDateTime j = message.j();
        if (j == null) {
            j = this.editedTimestamp;
        }
        UtcDateTime utcDateTime2 = j;
        List<MessageAttachment> d = message.d();
        if (d == null) {
            d = this.attachments;
        }
        List<MessageAttachment> list = d;
        List<MessageEmbed> k = message.k();
        if (k == null) {
            k = this.embeds;
        }
        List<MessageEmbed> list2 = k;
        Boolean D = message.D();
        if (D == null) {
            D = this.tts;
        }
        Boolean bool = D;
        Boolean r = message.r();
        if (r == null) {
            r = this.mentionEveryone;
        }
        Boolean bool2 = r;
        List<User> t = message.t();
        if (t == null) {
            t = this.mentions;
        }
        List<User> list3 = t;
        List<Long> s2 = message.s();
        if (s2 == null) {
            s2 = this.mentionRoles;
        }
        List<Long> list4 = s2;
        List<MessageReaction> x2 = message.x();
        if (x2 == null) {
            x2 = this.reactions;
        }
        List<MessageReaction> list5 = x2;
        String v = message.v();
        if (v == null) {
            v = this.nonce;
        }
        String str2 = v;
        Boolean w = message.w();
        if (w == null) {
            w = this.pinned;
        }
        Boolean bool3 = w;
        Long F = message.F();
        if (F == null) {
            F = this.webhookId;
        }
        Long l2 = F;
        Integer E = message.E();
        if (E == null) {
            E = this.type;
        }
        Integer num = E;
        MessageActivity a = message.a();
        if (a == null) {
            a = this.activity;
        }
        MessageActivity messageActivity = a;
        Application b2 = message.b();
        if (b2 == null) {
            b2 = this.application;
        }
        Application application = b2;
        Long c = message.c();
        if (c == null) {
            c = this.applicationId;
        }
        Long l3 = c;
        MessageReference u = message.u();
        if (u == null) {
            u = this.messageReference;
        }
        MessageReference messageReference = u;
        Long l4 = message.l();
        if (l4 == null) {
            l4 = this.flags;
        }
        Long l5 = l4;
        List<Sticker> A = message.A();
        if (A == null) {
            A = this.stickers;
        }
        List<Sticker> list6 = A;
        List<StickerPartial> z2 = message.z();
        if (z2 == null) {
            z2 = this.stickerItems;
        }
        List<StickerPartial> list7 = z2;
        com.discord.api.message.Message y2 = message.y();
        if (y2 == null) {
            y2 = this.referencedMessage;
        }
        com.discord.api.message.Message message2 = y2;
        Interaction p = message.p();
        if (p == null) {
            p = this.interaction;
        }
        Interaction interaction = p;
        Channel B = message.B();
        if (B == null) {
            B = this.thread;
        }
        Channel channel = B;
        List<Component> h = message.h();
        if (h == null) {
            h = this.components;
        }
        List<Component> list8 = h;
        MessageCall f = message.f();
        if (f == null) {
            f = this.call;
        }
        return copy$default(this, o, g, l, user, str, utcDateTime, utcDateTime2, bool, bool2, list3, list4, list, list2, list5, str2, bool3, l2, num, messageActivity, application, l3, messageReference, l5, list6, list7, message2, interaction, channel, list8, f, null, false, null, null, null, null, null, null, -1073741824, 63, null);
    }

    public final com.discord.api.message.Message synthesizeApiMessage() {
        return new com.discord.api.message.Message(this.f2720id, this.channelId, this.author, this.content, this.timestamp, this.editedTimestamp, this.tts, this.mentionEveryone, this.mentions, this.mentionRoles, this.attachments, this.embeds, this.reactions, this.nonce, this.pinned, this.webhookId, this.type, this.activity, this.application, this.applicationId, this.messageReference, this.flags, this.stickers, this.stickerItems, this.referencedMessage, this.interaction, this.thread, this.components, this.call, this.guildId, null, null, -1073741824);
    }

    public String toString() {
        StringBuilder R = a.R("Message(id=");
        R.append(this.f2720id);
        R.append(", channelId=");
        R.append(this.channelId);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", author=");
        R.append(this.author);
        R.append(", content=");
        R.append(this.content);
        R.append(", timestamp=");
        R.append(this.timestamp);
        R.append(", editedTimestamp=");
        R.append(this.editedTimestamp);
        R.append(", tts=");
        R.append(this.tts);
        R.append(", mentionEveryone=");
        R.append(this.mentionEveryone);
        R.append(", mentions=");
        R.append(this.mentions);
        R.append(", mentionRoles=");
        R.append(this.mentionRoles);
        R.append(", attachments=");
        R.append(this.attachments);
        R.append(", embeds=");
        R.append(this.embeds);
        R.append(", reactions=");
        R.append(this.reactions);
        R.append(", nonce=");
        R.append(this.nonce);
        R.append(", pinned=");
        R.append(this.pinned);
        R.append(", webhookId=");
        R.append(this.webhookId);
        R.append(", type=");
        R.append(this.type);
        R.append(", activity=");
        R.append(this.activity);
        R.append(", application=");
        R.append(this.application);
        R.append(", applicationId=");
        R.append(this.applicationId);
        R.append(", messageReference=");
        R.append(this.messageReference);
        R.append(", flags=");
        R.append(this.flags);
        R.append(", stickers=");
        R.append(this.stickers);
        R.append(", stickerItems=");
        R.append(this.stickerItems);
        R.append(", referencedMessage=");
        R.append(this.referencedMessage);
        R.append(", interaction=");
        R.append(this.interaction);
        R.append(", thread=");
        R.append(this.thread);
        R.append(", components=");
        R.append(this.components);
        R.append(", call=");
        R.append(this.call);
        R.append(", hit=");
        R.append(this.hit);
        R.append(", hasLocalUploads=");
        R.append(this.hasLocalUploads);
        R.append(", allowedMentions=");
        R.append(this.allowedMentions);
        R.append(", numRetries=");
        R.append(this.numRetries);
        R.append(", lastManualAttemptTimestamp=");
        R.append(this.lastManualAttemptTimestamp);
        R.append(", initialAttemptTimestamp=");
        R.append(this.initialAttemptTimestamp);
        R.append(", localAttachments=");
        R.append(this.localAttachments);
        R.append(", captchaKey=");
        return a.H(R, this.captchaKey, ")");
    }

    public /* synthetic */ Message(long j, long j2, Long l, User user, String str, UtcDateTime utcDateTime, UtcDateTime utcDateTime2, Boolean bool, Boolean bool2, List list, List list2, List list3, List list4, List list5, String str2, Boolean bool3, Long l2, Integer num, MessageActivity messageActivity, Application application, Long l3, MessageReference messageReference, Long l4, List list6, List list7, com.discord.api.message.Message message, Interaction interaction, Channel channel, List list8, MessageCall messageCall, Boolean bool4, boolean z2, MessageAllowedMentions messageAllowedMentions, Integer num2, Long l5, Long l6, List list9, String str3, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(j, j2, (i & 4) != 0 ? null : l, (i & 8) != 0 ? null : user, (i & 16) != 0 ? null : str, (i & 32) != 0 ? null : utcDateTime, (i & 64) != 0 ? null : utcDateTime2, (i & 128) != 0 ? null : bool, (i & 256) != 0 ? null : bool2, (i & 512) != 0 ? null : list, (i & 1024) != 0 ? null : list2, (i & 2048) != 0 ? null : list3, (i & 4096) != 0 ? null : list4, (i & 8192) != 0 ? null : list5, (i & 16384) != 0 ? null : str2, (32768 & i) != 0 ? null : bool3, (65536 & i) != 0 ? null : l2, (131072 & i) != 0 ? null : num, (262144 & i) != 0 ? null : messageActivity, (524288 & i) != 0 ? null : application, (1048576 & i) != 0 ? null : l3, (2097152 & i) != 0 ? null : messageReference, (4194304 & i) != 0 ? null : l4, (8388608 & i) != 0 ? null : list6, (16777216 & i) != 0 ? null : list7, (33554432 & i) != 0 ? null : message, (67108864 & i) != 0 ? null : interaction, (134217728 & i) != 0 ? null : channel, (268435456 & i) != 0 ? null : list8, (536870912 & i) != 0 ? null : messageCall, (1073741824 & i) != 0 ? null : bool4, (i & Integer.MIN_VALUE) != 0 ? false : z2, (i2 & 1) != 0 ? null : messageAllowedMentions, (i2 & 2) != 0 ? null : num2, (i2 & 4) != 0 ? null : l5, (i2 & 8) != 0 ? null : l6, (i2 & 16) != 0 ? null : list9, (i2 & 32) != 0 ? null : str3);
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public Message(com.discord.api.message.Message message) {
        this(message.o(), message.g(), message.m(), message.e(), message.i(), message.C(), message.j(), message.D(), message.r(), message.t(), message.s(), message.d(), message.k(), message.x(), message.v(), message.w(), message.F(), message.E(), message.a(), message.b(), message.c(), message.u(), message.l(), message.A(), message.z(), message.y(), message.p(), message.B(), message.h(), message.f(), message.n(), false, null, null, null, null, null, null, Integer.MIN_VALUE, 63, null);
        m.checkNotNullParameter(message, "message");
    }
}
