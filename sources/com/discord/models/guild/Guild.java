package com.discord.models.guild;

import a0.a.a.b;
import andhook.lib.HookHelper;
import androidx.constraintlayout.solver.widgets.analyzer.BasicMeasure;
import b.d.b.a.a;
import com.discord.api.emoji.GuildEmoji;
import com.discord.api.guild.GuildExplicitContentFilter;
import com.discord.api.guild.GuildFeature;
import com.discord.api.guild.GuildHubType;
import com.discord.api.guild.GuildMaxVideoChannelUsers;
import com.discord.api.guild.GuildVerificationLevel;
import com.discord.api.guild.welcome.GuildWelcomeScreen;
import com.discord.api.role.GuildRole;
import com.discord.api.sticker.Sticker;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelNotificationSettings;
import com.discord.utilities.guilds.GuildUtilsKt;
import d0.t.n;
import d0.t.n0;
import d0.t.u;
import d0.z.d.m;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: Guild.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\"\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b_\b\u0086\b\u0018\u00002\u00020\u0001B\u0091\u0003\u0012\u000e\b\u0002\u0010O\u001a\b\u0012\u0004\u0012\u00020\u00170\u0016\u0012\u000e\b\u0002\u0010P\u001a\b\u0012\u0004\u0012\u00020\u001a0\u0016\u0012\u000e\b\u0002\u0010Q\u001a\b\u0012\u0004\u0012\u00020\u001c0\u0016\u0012\b\b\u0002\u0010R\u001a\u00020\u001e\u0012\n\b\u0002\u0010S\u001a\u0004\u0018\u00010\u001e\u0012\b\b\u0002\u0010T\u001a\u00020\"\u0012\b\b\u0002\u0010U\u001a\u00020\f\u0012\n\b\u0002\u0010V\u001a\u0004\u0018\u00010\u001e\u0012\b\b\u0002\u0010W\u001a\u00020\f\u0012\n\b\u0002\u0010X\u001a\u0004\u0018\u00010\u001e\u0012\b\b\u0002\u0010Y\u001a\u00020*\u0012\b\b\u0002\u0010Z\u001a\u00020-\u0012\b\b\u0002\u0010[\u001a\u00020\u0002\u0012\b\b\u0002\u0010\\\u001a\u00020\"\u0012\b\b\u0002\u0010]\u001a\u00020\"\u0012\n\b\u0002\u0010^\u001a\u0004\u0018\u00010\f\u0012\n\b\u0002\u0010_\u001a\u0004\u0018\u00010\f\u0012\u000e\b\u0002\u0010`\u001a\b\u0012\u0004\u0012\u00020\u000506\u0012\b\b\u0002\u0010a\u001a\u00020\"\u0012\n\b\u0002\u0010b\u001a\u0004\u0018\u00010\u001e\u0012\n\b\u0002\u0010c\u001a\u0004\u0018\u00010\u001e\u0012\b\b\u0002\u0010d\u001a\u00020\"\u0012\b\b\u0002\u0010e\u001a\u00020\"\u0012\b\b\u0002\u0010f\u001a\u00020\"\u0012\n\b\u0002\u0010g\u001a\u0004\u0018\u00010\u001e\u0012\n\b\u0002\u0010h\u001a\u0004\u0018\u00010\f\u0012\n\b\u0002\u0010i\u001a\u0004\u0018\u00010\f\u0012\n\b\u0002\u0010j\u001a\u0004\u0018\u00010\u001e\u0012\n\b\u0002\u0010k\u001a\u0004\u0018\u00010C\u0012\b\b\u0002\u0010l\u001a\u00020F\u0012\n\b\u0002\u0010m\u001a\u0004\u0018\u00010\u001e\u0012\b\b\u0002\u0010n\u001a\u00020\"\u0012\b\b\u0002\u0010o\u001a\u00020\u0002\u0012\n\b\u0002\u0010p\u001a\u0004\u0018\u00010L¢\u0006\u0006\b¨\u0001\u0010©\u0001B\u0013\b\u0016\u0012\u0006\u0010\u0013\u001a\u00020\u0012¢\u0006\u0006\b¨\u0001\u0010ª\u0001J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0015\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\u0007\u0010\bJ\r\u0010\t\u001a\u00020\u0002¢\u0006\u0004\b\t\u0010\u0004J\r\u0010\n\u001a\u00020\u0002¢\u0006\u0004\b\n\u0010\u0004J\r\u0010\u000b\u001a\u00020\u0002¢\u0006\u0004\b\u000b\u0010\u0004J\u0015\u0010\u000e\u001a\u00020\u00022\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u000e\u0010\u000fJ\r\u0010\u0010\u001a\u00020\u0002¢\u0006\u0004\b\u0010\u0010\u0004J\r\u0010\u0011\u001a\u00020\u0002¢\u0006\u0004\b\u0011\u0010\u0004J\u0015\u0010\u0014\u001a\u00020\u00002\u0006\u0010\u0013\u001a\u00020\u0012¢\u0006\u0004\b\u0014\u0010\u0015J\u0016\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00170\u0016HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0019J\u0016\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001a0\u0016HÆ\u0003¢\u0006\u0004\b\u001b\u0010\u0019J\u0016\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001c0\u0016HÆ\u0003¢\u0006\u0004\b\u001d\u0010\u0019J\u0010\u0010\u001f\u001a\u00020\u001eHÆ\u0003¢\u0006\u0004\b\u001f\u0010 J\u0012\u0010!\u001a\u0004\u0018\u00010\u001eHÆ\u0003¢\u0006\u0004\b!\u0010 J\u0010\u0010#\u001a\u00020\"HÆ\u0003¢\u0006\u0004\b#\u0010$J\u0010\u0010%\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b%\u0010&J\u0012\u0010'\u001a\u0004\u0018\u00010\u001eHÆ\u0003¢\u0006\u0004\b'\u0010 J\u0010\u0010(\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b(\u0010&J\u0012\u0010)\u001a\u0004\u0018\u00010\u001eHÆ\u0003¢\u0006\u0004\b)\u0010 J\u0010\u0010+\u001a\u00020*HÆ\u0003¢\u0006\u0004\b+\u0010,J\u0010\u0010.\u001a\u00020-HÆ\u0003¢\u0006\u0004\b.\u0010/J\u0010\u00100\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b0\u0010\u0004J\u0010\u00101\u001a\u00020\"HÆ\u0003¢\u0006\u0004\b1\u0010$J\u0010\u00102\u001a\u00020\"HÆ\u0003¢\u0006\u0004\b2\u0010$J\u0012\u00103\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b3\u00104J\u0012\u00105\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b5\u00104J\u0016\u00107\u001a\b\u0012\u0004\u0012\u00020\u000506HÆ\u0003¢\u0006\u0004\b7\u00108J\u0010\u00109\u001a\u00020\"HÆ\u0003¢\u0006\u0004\b9\u0010$J\u0012\u0010:\u001a\u0004\u0018\u00010\u001eHÆ\u0003¢\u0006\u0004\b:\u0010 J\u0012\u0010;\u001a\u0004\u0018\u00010\u001eHÆ\u0003¢\u0006\u0004\b;\u0010 J\u0010\u0010<\u001a\u00020\"HÆ\u0003¢\u0006\u0004\b<\u0010$J\u0010\u0010=\u001a\u00020\"HÆ\u0003¢\u0006\u0004\b=\u0010$J\u0010\u0010>\u001a\u00020\"HÆ\u0003¢\u0006\u0004\b>\u0010$J\u0012\u0010?\u001a\u0004\u0018\u00010\u001eHÆ\u0003¢\u0006\u0004\b?\u0010 J\u0012\u0010@\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b@\u00104J\u0012\u0010A\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\bA\u00104J\u0012\u0010B\u001a\u0004\u0018\u00010\u001eHÆ\u0003¢\u0006\u0004\bB\u0010 J\u0012\u0010D\u001a\u0004\u0018\u00010CHÆ\u0003¢\u0006\u0004\bD\u0010EJ\u0010\u0010G\u001a\u00020FHÆ\u0003¢\u0006\u0004\bG\u0010HJ\u0012\u0010I\u001a\u0004\u0018\u00010\u001eHÆ\u0003¢\u0006\u0004\bI\u0010 J\u0010\u0010J\u001a\u00020\"HÆ\u0003¢\u0006\u0004\bJ\u0010$J\u0010\u0010K\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\bK\u0010\u0004J\u0012\u0010M\u001a\u0004\u0018\u00010LHÆ\u0003¢\u0006\u0004\bM\u0010NJ\u0098\u0003\u0010q\u001a\u00020\u00002\u000e\b\u0002\u0010O\u001a\b\u0012\u0004\u0012\u00020\u00170\u00162\u000e\b\u0002\u0010P\u001a\b\u0012\u0004\u0012\u00020\u001a0\u00162\u000e\b\u0002\u0010Q\u001a\b\u0012\u0004\u0012\u00020\u001c0\u00162\b\b\u0002\u0010R\u001a\u00020\u001e2\n\b\u0002\u0010S\u001a\u0004\u0018\u00010\u001e2\b\b\u0002\u0010T\u001a\u00020\"2\b\b\u0002\u0010U\u001a\u00020\f2\n\b\u0002\u0010V\u001a\u0004\u0018\u00010\u001e2\b\b\u0002\u0010W\u001a\u00020\f2\n\b\u0002\u0010X\u001a\u0004\u0018\u00010\u001e2\b\b\u0002\u0010Y\u001a\u00020*2\b\b\u0002\u0010Z\u001a\u00020-2\b\b\u0002\u0010[\u001a\u00020\u00022\b\b\u0002\u0010\\\u001a\u00020\"2\b\b\u0002\u0010]\u001a\u00020\"2\n\b\u0002\u0010^\u001a\u0004\u0018\u00010\f2\n\b\u0002\u0010_\u001a\u0004\u0018\u00010\f2\u000e\b\u0002\u0010`\u001a\b\u0012\u0004\u0012\u00020\u0005062\b\b\u0002\u0010a\u001a\u00020\"2\n\b\u0002\u0010b\u001a\u0004\u0018\u00010\u001e2\n\b\u0002\u0010c\u001a\u0004\u0018\u00010\u001e2\b\b\u0002\u0010d\u001a\u00020\"2\b\b\u0002\u0010e\u001a\u00020\"2\b\b\u0002\u0010f\u001a\u00020\"2\n\b\u0002\u0010g\u001a\u0004\u0018\u00010\u001e2\n\b\u0002\u0010h\u001a\u0004\u0018\u00010\f2\n\b\u0002\u0010i\u001a\u0004\u0018\u00010\f2\n\b\u0002\u0010j\u001a\u0004\u0018\u00010\u001e2\n\b\u0002\u0010k\u001a\u0004\u0018\u00010C2\b\b\u0002\u0010l\u001a\u00020F2\n\b\u0002\u0010m\u001a\u0004\u0018\u00010\u001e2\b\b\u0002\u0010n\u001a\u00020\"2\b\b\u0002\u0010o\u001a\u00020\u00022\n\b\u0002\u0010p\u001a\u0004\u0018\u00010LHÆ\u0001¢\u0006\u0004\bq\u0010rJ\u0010\u0010s\u001a\u00020\u001eHÖ\u0001¢\u0006\u0004\bs\u0010 J\u0010\u0010t\u001a\u00020\"HÖ\u0001¢\u0006\u0004\bt\u0010$J\u001a\u0010v\u001a\u00020\u00022\b\u0010u\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\bv\u0010wR\u001b\u0010_\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b_\u0010x\u001a\u0004\by\u00104R\u001f\u0010P\u001a\b\u0012\u0004\u0012\u00020\u001a0\u00168\u0006@\u0006¢\u0006\f\n\u0004\bP\u0010z\u001a\u0004\b{\u0010\u0019R\u0019\u0010U\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\bU\u0010|\u001a\u0004\b}\u0010&R\u0019\u0010\\\u001a\u00020\"8\u0006@\u0006¢\u0006\f\n\u0004\b\\\u0010~\u001a\u0004\b\u007f\u0010$R\u001d\u0010b\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bb\u0010\u0080\u0001\u001a\u0005\b\u0081\u0001\u0010 R\u001c\u0010i\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\r\n\u0004\bi\u0010x\u001a\u0005\b\u0082\u0001\u00104R\u001d\u0010k\u001a\u0004\u0018\u00010C8\u0006@\u0006¢\u0006\u000e\n\u0005\bk\u0010\u0083\u0001\u001a\u0005\b\u0084\u0001\u0010ER \u0010O\u001a\b\u0012\u0004\u0012\u00020\u00170\u00168\u0006@\u0006¢\u0006\r\n\u0004\bO\u0010z\u001a\u0005\b\u0085\u0001\u0010\u0019R\u001b\u0010l\u001a\u00020F8\u0006@\u0006¢\u0006\u000e\n\u0005\bl\u0010\u0086\u0001\u001a\u0005\b\u0087\u0001\u0010HR\u001d\u0010m\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bm\u0010\u0080\u0001\u001a\u0005\b\u0088\u0001\u0010 R\u001a\u0010n\u001a\u00020\"8\u0006@\u0006¢\u0006\r\n\u0004\bn\u0010~\u001a\u0005\b\u0089\u0001\u0010$R\u001a\u0010T\u001a\u00020\"8\u0006@\u0006¢\u0006\r\n\u0004\bT\u0010~\u001a\u0005\b\u008a\u0001\u0010$R\u001a\u0010d\u001a\u00020\"8\u0006@\u0006¢\u0006\r\n\u0004\bd\u0010~\u001a\u0005\b\u008b\u0001\u0010$R\u001d\u0010p\u001a\u0004\u0018\u00010L8\u0006@\u0006¢\u0006\u000e\n\u0005\bp\u0010\u008c\u0001\u001a\u0005\b\u008d\u0001\u0010NR\u001d\u0010\u008e\u0001\u001a\u00020\u001e8\u0006@\u0006¢\u0006\u000f\n\u0006\b\u008e\u0001\u0010\u0080\u0001\u001a\u0005\b\u008f\u0001\u0010 R\u001b\u0010[\u001a\u00020\u00028\u0006@\u0006¢\u0006\u000e\n\u0005\b[\u0010\u0090\u0001\u001a\u0005\b\u0091\u0001\u0010\u0004R\u001b\u0010Y\u001a\u00020*8\u0006@\u0006¢\u0006\u000e\n\u0005\bY\u0010\u0092\u0001\u001a\u0005\b\u0093\u0001\u0010,R\u001d\u0010j\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bj\u0010\u0080\u0001\u001a\u0005\b\u0094\u0001\u0010 R\u001a\u0010e\u001a\u00020\"8\u0006@\u0006¢\u0006\r\n\u0004\be\u0010~\u001a\u0005\b\u0095\u0001\u0010$R\u001a\u0010]\u001a\u00020\"8\u0006@\u0006¢\u0006\r\n\u0004\b]\u0010~\u001a\u0005\b\u0096\u0001\u0010$R!\u0010`\u001a\b\u0012\u0004\u0012\u00020\u0005068\u0006@\u0006¢\u0006\u000e\n\u0005\b`\u0010\u0097\u0001\u001a\u0005\b\u0098\u0001\u00108R\u001d\u0010S\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bS\u0010\u0080\u0001\u001a\u0005\b\u0099\u0001\u0010 R\u001a\u0010W\u001a\u00020\f8\u0006@\u0006¢\u0006\r\n\u0004\bW\u0010|\u001a\u0005\b\u009a\u0001\u0010&R\u001d\u0010X\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bX\u0010\u0080\u0001\u001a\u0005\b\u009b\u0001\u0010 R\u001d\u0010c\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bc\u0010\u0080\u0001\u001a\u0005\b\u009c\u0001\u0010 R\u001d\u0010g\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bg\u0010\u0080\u0001\u001a\u0005\b\u009d\u0001\u0010 R\u001b\u0010o\u001a\u00020\u00028\u0006@\u0006¢\u0006\u000e\n\u0005\bo\u0010\u0090\u0001\u001a\u0005\b\u009e\u0001\u0010\u0004R\u001c\u0010h\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\r\n\u0004\bh\u0010x\u001a\u0005\b\u009f\u0001\u00104R\u001c\u0010^\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\r\n\u0004\b^\u0010x\u001a\u0005\b \u0001\u00104R\u001b\u0010R\u001a\u00020\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bR\u0010\u0080\u0001\u001a\u0005\b¡\u0001\u0010 R \u0010Q\u001a\b\u0012\u0004\u0012\u00020\u001c0\u00168\u0006@\u0006¢\u0006\r\n\u0004\bQ\u0010z\u001a\u0005\b¢\u0001\u0010\u0019R\u001d\u0010V\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bV\u0010\u0080\u0001\u001a\u0005\b£\u0001\u0010 R\u001b\u0010Z\u001a\u00020-8\u0006@\u0006¢\u0006\u000e\n\u0005\bZ\u0010¤\u0001\u001a\u0005\b¥\u0001\u0010/R\u001a\u0010a\u001a\u00020\"8\u0006@\u0006¢\u0006\r\n\u0004\ba\u0010~\u001a\u0005\b¦\u0001\u0010$R\u001a\u0010f\u001a\u00020\"8\u0006@\u0006¢\u0006\r\n\u0004\bf\u0010~\u001a\u0005\b§\u0001\u0010$¨\u0006«\u0001"}, d2 = {"Lcom/discord/models/guild/Guild;", "", "", "canHaveVanityURL", "()Z", "Lcom/discord/api/guild/GuildFeature;", "feature", "hasFeature", "(Lcom/discord/api/guild/GuildFeature;)Z", "canHaveSplash", "canHaveBanner", "canHaveAnimatedBanner", "", "userId", "isOwner", "(J)Z", "isHub", "hasIcon", "Lcom/discord/api/guild/Guild;", "apiGuild", "merge", "(Lcom/discord/api/guild/Guild;)Lcom/discord/models/guild/Guild;", "", "Lcom/discord/api/role/GuildRole;", "component1", "()Ljava/util/List;", "Lcom/discord/api/emoji/GuildEmoji;", "component2", "Lcom/discord/api/sticker/Sticker;", "component3", "", "component4", "()Ljava/lang/String;", "component5", "", "component6", "()I", "component7", "()J", "component8", "component9", "component10", "Lcom/discord/api/guild/GuildVerificationLevel;", "component11", "()Lcom/discord/api/guild/GuildVerificationLevel;", "Lcom/discord/api/guild/GuildExplicitContentFilter;", "component12", "()Lcom/discord/api/guild/GuildExplicitContentFilter;", "component13", "component14", "component15", "component16", "()Ljava/lang/Long;", "component17", "", "component18", "()Ljava/util/Set;", "component19", "component20", "component21", "component22", "component23", "component24", "component25", "component26", "component27", "component28", "Lcom/discord/api/guild/welcome/GuildWelcomeScreen;", "component29", "()Lcom/discord/api/guild/welcome/GuildWelcomeScreen;", "Lcom/discord/api/guild/GuildMaxVideoChannelUsers;", "component30", "()Lcom/discord/api/guild/GuildMaxVideoChannelUsers;", "component31", "component32", "component33", "Lcom/discord/api/guild/GuildHubType;", "component34", "()Lcom/discord/api/guild/GuildHubType;", "roles", "emojis", "stickers", ModelAuditLogEntry.CHANGE_KEY_NAME, ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "defaultMessageNotifications", ModelAuditLogEntry.CHANGE_KEY_ID, ModelAuditLogEntry.CHANGE_KEY_REGION, "ownerId", "icon", "verificationLevel", "explicitContentFilter", "unavailable", "mfaLevel", "afkTimeout", "afkChannelId", "systemChannelId", "features", "memberCount", "banner", "splash", "premiumTier", "premiumSubscriptionCount", "systemChannelFlags", "joinedAt", "rulesChannelId", "publicUpdatesChannelId", "preferredLocale", "welcomeScreen", "maxVideoChannelUsers", "vanityUrlCode", "approximatePresenceCount", ModelAuditLogEntry.CHANGE_KEY_NSFW, "hubType", "copy", "(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;JLjava/lang/String;Lcom/discord/api/guild/GuildVerificationLevel;Lcom/discord/api/guild/GuildExplicitContentFilter;ZIILjava/lang/Long;Ljava/lang/Long;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Lcom/discord/api/guild/welcome/GuildWelcomeScreen;Lcom/discord/api/guild/GuildMaxVideoChannelUsers;Ljava/lang/String;IZLcom/discord/api/guild/GuildHubType;)Lcom/discord/models/guild/Guild;", "toString", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Long;", "getSystemChannelId", "Ljava/util/List;", "getEmojis", "J", "getId", "I", "getMfaLevel", "Ljava/lang/String;", "getBanner", "getPublicUpdatesChannelId", "Lcom/discord/api/guild/welcome/GuildWelcomeScreen;", "getWelcomeScreen", "getRoles", "Lcom/discord/api/guild/GuildMaxVideoChannelUsers;", "getMaxVideoChannelUsers", "getVanityUrlCode", "getApproximatePresenceCount", "getDefaultMessageNotifications", "getPremiumTier", "Lcom/discord/api/guild/GuildHubType;", "getHubType", "shortName", "getShortName", "Z", "getUnavailable", "Lcom/discord/api/guild/GuildVerificationLevel;", "getVerificationLevel", "getPreferredLocale", "getPremiumSubscriptionCount", "getAfkTimeout", "Ljava/util/Set;", "getFeatures", "getDescription", "getOwnerId", "getIcon", "getSplash", "getJoinedAt", "getNsfw", "getRulesChannelId", "getAfkChannelId", "getName", "getStickers", "getRegion", "Lcom/discord/api/guild/GuildExplicitContentFilter;", "getExplicitContentFilter", "getMemberCount", "getSystemChannelFlags", HookHelper.constructorName, "(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;JLjava/lang/String;Lcom/discord/api/guild/GuildVerificationLevel;Lcom/discord/api/guild/GuildExplicitContentFilter;ZIILjava/lang/Long;Ljava/lang/Long;Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Lcom/discord/api/guild/welcome/GuildWelcomeScreen;Lcom/discord/api/guild/GuildMaxVideoChannelUsers;Ljava/lang/String;IZLcom/discord/api/guild/GuildHubType;)V", "(Lcom/discord/api/guild/Guild;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class Guild {
    private final Long afkChannelId;
    private final int afkTimeout;
    private final int approximatePresenceCount;
    private final String banner;
    private final int defaultMessageNotifications;
    private final String description;
    private final List<GuildEmoji> emojis;
    private final GuildExplicitContentFilter explicitContentFilter;
    private final Set<GuildFeature> features;
    private final GuildHubType hubType;
    private final String icon;

    /* renamed from: id  reason: collision with root package name */
    private final long f2719id;
    private final String joinedAt;
    private final GuildMaxVideoChannelUsers maxVideoChannelUsers;
    private final int memberCount;
    private final int mfaLevel;
    private final String name;
    private final boolean nsfw;
    private final long ownerId;
    private final String preferredLocale;
    private final int premiumSubscriptionCount;
    private final int premiumTier;
    private final Long publicUpdatesChannelId;
    private final String region;
    private final List<GuildRole> roles;
    private final Long rulesChannelId;
    private final String shortName;
    private final String splash;
    private final List<Sticker> stickers;
    private final int systemChannelFlags;
    private final Long systemChannelId;
    private final boolean unavailable;
    private final String vanityUrlCode;
    private final GuildVerificationLevel verificationLevel;
    private final GuildWelcomeScreen welcomeScreen;

    public Guild() {
        this(null, null, null, null, null, 0, 0L, null, 0L, null, null, null, false, 0, 0, null, null, null, 0, null, null, 0, 0, 0, null, null, null, null, null, null, null, 0, false, null, -1, 3, null);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public Guild(List<GuildRole> list, List<GuildEmoji> list2, List<Sticker> list3, String str, String str2, int i, long j, String str3, long j2, String str4, GuildVerificationLevel guildVerificationLevel, GuildExplicitContentFilter guildExplicitContentFilter, boolean z2, int i2, int i3, Long l, Long l2, Set<? extends GuildFeature> set, int i4, String str5, String str6, int i5, int i6, int i7, String str7, Long l3, Long l4, String str8, GuildWelcomeScreen guildWelcomeScreen, GuildMaxVideoChannelUsers guildMaxVideoChannelUsers, String str9, int i8, boolean z3, GuildHubType guildHubType) {
        m.checkNotNullParameter(list, "roles");
        m.checkNotNullParameter(list2, "emojis");
        m.checkNotNullParameter(list3, "stickers");
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(guildVerificationLevel, "verificationLevel");
        m.checkNotNullParameter(guildExplicitContentFilter, "explicitContentFilter");
        m.checkNotNullParameter(set, "features");
        m.checkNotNullParameter(guildMaxVideoChannelUsers, "maxVideoChannelUsers");
        this.roles = list;
        this.emojis = list2;
        this.stickers = list3;
        this.name = str;
        this.description = str2;
        this.defaultMessageNotifications = i;
        this.f2719id = j;
        this.region = str3;
        this.ownerId = j2;
        this.icon = str4;
        this.verificationLevel = guildVerificationLevel;
        this.explicitContentFilter = guildExplicitContentFilter;
        this.unavailable = z2;
        this.mfaLevel = i2;
        this.afkTimeout = i3;
        this.afkChannelId = l;
        this.systemChannelId = l2;
        this.features = set;
        this.memberCount = i4;
        this.banner = str5;
        this.splash = str6;
        this.premiumTier = i5;
        this.premiumSubscriptionCount = i6;
        this.systemChannelFlags = i7;
        this.joinedAt = str7;
        this.rulesChannelId = l3;
        this.publicUpdatesChannelId = l4;
        this.preferredLocale = str8;
        this.welcomeScreen = guildWelcomeScreen;
        this.maxVideoChannelUsers = guildMaxVideoChannelUsers;
        this.vanityUrlCode = str9;
        this.approximatePresenceCount = i8;
        this.nsfw = z3;
        this.hubType = guildHubType;
        this.shortName = GuildUtilsKt.computeShortName(str);
    }

    public final boolean canHaveAnimatedBanner() {
        return hasFeature(GuildFeature.ANIMATED_BANNER);
    }

    public final boolean canHaveBanner() {
        return hasFeature(GuildFeature.BANNER) || hasFeature(GuildFeature.VERIFIED) || this.premiumTier >= 2;
    }

    public final boolean canHaveSplash() {
        return hasFeature(GuildFeature.INVITE_SPLASH) || hasFeature(GuildFeature.VERIFIED) || this.premiumTier >= 1;
    }

    public final boolean canHaveVanityURL() {
        return this.features.contains(GuildFeature.VANITY_URL) || this.premiumTier >= 3;
    }

    public final List<GuildRole> component1() {
        return this.roles;
    }

    public final String component10() {
        return this.icon;
    }

    public final GuildVerificationLevel component11() {
        return this.verificationLevel;
    }

    public final GuildExplicitContentFilter component12() {
        return this.explicitContentFilter;
    }

    public final boolean component13() {
        return this.unavailable;
    }

    public final int component14() {
        return this.mfaLevel;
    }

    public final int component15() {
        return this.afkTimeout;
    }

    public final Long component16() {
        return this.afkChannelId;
    }

    public final Long component17() {
        return this.systemChannelId;
    }

    public final Set<GuildFeature> component18() {
        return this.features;
    }

    public final int component19() {
        return this.memberCount;
    }

    public final List<GuildEmoji> component2() {
        return this.emojis;
    }

    public final String component20() {
        return this.banner;
    }

    public final String component21() {
        return this.splash;
    }

    public final int component22() {
        return this.premiumTier;
    }

    public final int component23() {
        return this.premiumSubscriptionCount;
    }

    public final int component24() {
        return this.systemChannelFlags;
    }

    public final String component25() {
        return this.joinedAt;
    }

    public final Long component26() {
        return this.rulesChannelId;
    }

    public final Long component27() {
        return this.publicUpdatesChannelId;
    }

    public final String component28() {
        return this.preferredLocale;
    }

    public final GuildWelcomeScreen component29() {
        return this.welcomeScreen;
    }

    public final List<Sticker> component3() {
        return this.stickers;
    }

    public final GuildMaxVideoChannelUsers component30() {
        return this.maxVideoChannelUsers;
    }

    public final String component31() {
        return this.vanityUrlCode;
    }

    public final int component32() {
        return this.approximatePresenceCount;
    }

    public final boolean component33() {
        return this.nsfw;
    }

    public final GuildHubType component34() {
        return this.hubType;
    }

    public final String component4() {
        return this.name;
    }

    public final String component5() {
        return this.description;
    }

    public final int component6() {
        return this.defaultMessageNotifications;
    }

    public final long component7() {
        return this.f2719id;
    }

    public final String component8() {
        return this.region;
    }

    public final long component9() {
        return this.ownerId;
    }

    public final Guild copy(List<GuildRole> list, List<GuildEmoji> list2, List<Sticker> list3, String str, String str2, int i, long j, String str3, long j2, String str4, GuildVerificationLevel guildVerificationLevel, GuildExplicitContentFilter guildExplicitContentFilter, boolean z2, int i2, int i3, Long l, Long l2, Set<? extends GuildFeature> set, int i4, String str5, String str6, int i5, int i6, int i7, String str7, Long l3, Long l4, String str8, GuildWelcomeScreen guildWelcomeScreen, GuildMaxVideoChannelUsers guildMaxVideoChannelUsers, String str9, int i8, boolean z3, GuildHubType guildHubType) {
        m.checkNotNullParameter(list, "roles");
        m.checkNotNullParameter(list2, "emojis");
        m.checkNotNullParameter(list3, "stickers");
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(guildVerificationLevel, "verificationLevel");
        m.checkNotNullParameter(guildExplicitContentFilter, "explicitContentFilter");
        m.checkNotNullParameter(set, "features");
        m.checkNotNullParameter(guildMaxVideoChannelUsers, "maxVideoChannelUsers");
        return new Guild(list, list2, list3, str, str2, i, j, str3, j2, str4, guildVerificationLevel, guildExplicitContentFilter, z2, i2, i3, l, l2, set, i4, str5, str6, i5, i6, i7, str7, l3, l4, str8, guildWelcomeScreen, guildMaxVideoChannelUsers, str9, i8, z3, guildHubType);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Guild)) {
            return false;
        }
        Guild guild = (Guild) obj;
        return m.areEqual(this.roles, guild.roles) && m.areEqual(this.emojis, guild.emojis) && m.areEqual(this.stickers, guild.stickers) && m.areEqual(this.name, guild.name) && m.areEqual(this.description, guild.description) && this.defaultMessageNotifications == guild.defaultMessageNotifications && this.f2719id == guild.f2719id && m.areEqual(this.region, guild.region) && this.ownerId == guild.ownerId && m.areEqual(this.icon, guild.icon) && m.areEqual(this.verificationLevel, guild.verificationLevel) && m.areEqual(this.explicitContentFilter, guild.explicitContentFilter) && this.unavailable == guild.unavailable && this.mfaLevel == guild.mfaLevel && this.afkTimeout == guild.afkTimeout && m.areEqual(this.afkChannelId, guild.afkChannelId) && m.areEqual(this.systemChannelId, guild.systemChannelId) && m.areEqual(this.features, guild.features) && this.memberCount == guild.memberCount && m.areEqual(this.banner, guild.banner) && m.areEqual(this.splash, guild.splash) && this.premiumTier == guild.premiumTier && this.premiumSubscriptionCount == guild.premiumSubscriptionCount && this.systemChannelFlags == guild.systemChannelFlags && m.areEqual(this.joinedAt, guild.joinedAt) && m.areEqual(this.rulesChannelId, guild.rulesChannelId) && m.areEqual(this.publicUpdatesChannelId, guild.publicUpdatesChannelId) && m.areEqual(this.preferredLocale, guild.preferredLocale) && m.areEqual(this.welcomeScreen, guild.welcomeScreen) && m.areEqual(this.maxVideoChannelUsers, guild.maxVideoChannelUsers) && m.areEqual(this.vanityUrlCode, guild.vanityUrlCode) && this.approximatePresenceCount == guild.approximatePresenceCount && this.nsfw == guild.nsfw && m.areEqual(this.hubType, guild.hubType);
    }

    public final Long getAfkChannelId() {
        return this.afkChannelId;
    }

    public final int getAfkTimeout() {
        return this.afkTimeout;
    }

    public final int getApproximatePresenceCount() {
        return this.approximatePresenceCount;
    }

    public final String getBanner() {
        return this.banner;
    }

    public final int getDefaultMessageNotifications() {
        return this.defaultMessageNotifications;
    }

    public final String getDescription() {
        return this.description;
    }

    public final List<GuildEmoji> getEmojis() {
        return this.emojis;
    }

    public final GuildExplicitContentFilter getExplicitContentFilter() {
        return this.explicitContentFilter;
    }

    public final Set<GuildFeature> getFeatures() {
        return this.features;
    }

    public final GuildHubType getHubType() {
        return this.hubType;
    }

    public final String getIcon() {
        return this.icon;
    }

    public final long getId() {
        return this.f2719id;
    }

    public final String getJoinedAt() {
        return this.joinedAt;
    }

    public final GuildMaxVideoChannelUsers getMaxVideoChannelUsers() {
        return this.maxVideoChannelUsers;
    }

    public final int getMemberCount() {
        return this.memberCount;
    }

    public final int getMfaLevel() {
        return this.mfaLevel;
    }

    public final String getName() {
        return this.name;
    }

    public final boolean getNsfw() {
        return this.nsfw;
    }

    public final long getOwnerId() {
        return this.ownerId;
    }

    public final String getPreferredLocale() {
        return this.preferredLocale;
    }

    public final int getPremiumSubscriptionCount() {
        return this.premiumSubscriptionCount;
    }

    public final int getPremiumTier() {
        return this.premiumTier;
    }

    public final Long getPublicUpdatesChannelId() {
        return this.publicUpdatesChannelId;
    }

    public final String getRegion() {
        return this.region;
    }

    public final List<GuildRole> getRoles() {
        return this.roles;
    }

    public final Long getRulesChannelId() {
        return this.rulesChannelId;
    }

    public final String getShortName() {
        return this.shortName;
    }

    public final String getSplash() {
        return this.splash;
    }

    public final List<Sticker> getStickers() {
        return this.stickers;
    }

    public final int getSystemChannelFlags() {
        return this.systemChannelFlags;
    }

    public final Long getSystemChannelId() {
        return this.systemChannelId;
    }

    public final boolean getUnavailable() {
        return this.unavailable;
    }

    public final String getVanityUrlCode() {
        return this.vanityUrlCode;
    }

    public final GuildVerificationLevel getVerificationLevel() {
        return this.verificationLevel;
    }

    public final GuildWelcomeScreen getWelcomeScreen() {
        return this.welcomeScreen;
    }

    public final boolean hasFeature(GuildFeature guildFeature) {
        m.checkNotNullParameter(guildFeature, "feature");
        return this.features.contains(guildFeature);
    }

    public final boolean hasIcon() {
        String str = this.icon;
        return !(str == null || str.length() == 0);
    }

    public int hashCode() {
        List<GuildRole> list = this.roles;
        int i = 0;
        int hashCode = (list != null ? list.hashCode() : 0) * 31;
        List<GuildEmoji> list2 = this.emojis;
        int hashCode2 = (hashCode + (list2 != null ? list2.hashCode() : 0)) * 31;
        List<Sticker> list3 = this.stickers;
        int hashCode3 = (hashCode2 + (list3 != null ? list3.hashCode() : 0)) * 31;
        String str = this.name;
        int hashCode4 = (hashCode3 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.description;
        int a = (b.a(this.f2719id) + ((((hashCode4 + (str2 != null ? str2.hashCode() : 0)) * 31) + this.defaultMessageNotifications) * 31)) * 31;
        String str3 = this.region;
        int a2 = (b.a(this.ownerId) + ((a + (str3 != null ? str3.hashCode() : 0)) * 31)) * 31;
        String str4 = this.icon;
        int hashCode5 = (a2 + (str4 != null ? str4.hashCode() : 0)) * 31;
        GuildVerificationLevel guildVerificationLevel = this.verificationLevel;
        int hashCode6 = (hashCode5 + (guildVerificationLevel != null ? guildVerificationLevel.hashCode() : 0)) * 31;
        GuildExplicitContentFilter guildExplicitContentFilter = this.explicitContentFilter;
        int hashCode7 = (hashCode6 + (guildExplicitContentFilter != null ? guildExplicitContentFilter.hashCode() : 0)) * 31;
        boolean z2 = this.unavailable;
        int i2 = 1;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        int i5 = (((((hashCode7 + i3) * 31) + this.mfaLevel) * 31) + this.afkTimeout) * 31;
        Long l = this.afkChannelId;
        int hashCode8 = (i5 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.systemChannelId;
        int hashCode9 = (hashCode8 + (l2 != null ? l2.hashCode() : 0)) * 31;
        Set<GuildFeature> set = this.features;
        int hashCode10 = (((hashCode9 + (set != null ? set.hashCode() : 0)) * 31) + this.memberCount) * 31;
        String str5 = this.banner;
        int hashCode11 = (hashCode10 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.splash;
        int hashCode12 = (((((((hashCode11 + (str6 != null ? str6.hashCode() : 0)) * 31) + this.premiumTier) * 31) + this.premiumSubscriptionCount) * 31) + this.systemChannelFlags) * 31;
        String str7 = this.joinedAt;
        int hashCode13 = (hashCode12 + (str7 != null ? str7.hashCode() : 0)) * 31;
        Long l3 = this.rulesChannelId;
        int hashCode14 = (hashCode13 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.publicUpdatesChannelId;
        int hashCode15 = (hashCode14 + (l4 != null ? l4.hashCode() : 0)) * 31;
        String str8 = this.preferredLocale;
        int hashCode16 = (hashCode15 + (str8 != null ? str8.hashCode() : 0)) * 31;
        GuildWelcomeScreen guildWelcomeScreen = this.welcomeScreen;
        int hashCode17 = (hashCode16 + (guildWelcomeScreen != null ? guildWelcomeScreen.hashCode() : 0)) * 31;
        GuildMaxVideoChannelUsers guildMaxVideoChannelUsers = this.maxVideoChannelUsers;
        int hashCode18 = (hashCode17 + (guildMaxVideoChannelUsers != null ? guildMaxVideoChannelUsers.hashCode() : 0)) * 31;
        String str9 = this.vanityUrlCode;
        int hashCode19 = (((hashCode18 + (str9 != null ? str9.hashCode() : 0)) * 31) + this.approximatePresenceCount) * 31;
        boolean z3 = this.nsfw;
        if (!z3) {
            i2 = z3 ? 1 : 0;
        }
        int i6 = (hashCode19 + i2) * 31;
        GuildHubType guildHubType = this.hubType;
        if (guildHubType != null) {
            i = guildHubType.hashCode();
        }
        return i6 + i;
    }

    public final boolean isHub() {
        return hasFeature(GuildFeature.HUB);
    }

    public final boolean isOwner(long j) {
        return this.ownerId == j;
    }

    public final Guild merge(com.discord.api.guild.Guild guild) {
        int i;
        m.checkNotNullParameter(guild, "apiGuild");
        List emptyList = n.emptyList();
        List<GuildEmoji> k = guild.k();
        if (k == null) {
            k = n.emptyList();
        }
        String x2 = guild.x();
        String i2 = guild.i();
        if (i2 == null) {
            i2 = this.description;
        }
        Integer h = guild.h();
        int intValue = h != null ? h.intValue() : this.defaultMessageNotifications;
        long r = guild.r() != 0 ? guild.r() : this.f2719id;
        String F = guild.F();
        if (F == null) {
            F = this.region;
        }
        long z2 = guild.z() != 0 ? guild.z() : this.ownerId;
        String q = guild.q();
        if (q == null) {
            q = this.icon;
        }
        String str = q;
        GuildVerificationLevel Q = guild.Q();
        if (Q == null) {
            Q = this.verificationLevel;
        }
        GuildVerificationLevel guildVerificationLevel = Q;
        GuildExplicitContentFilter l = guild.l();
        if (l == null) {
            l = this.explicitContentFilter;
        }
        GuildExplicitContentFilter guildExplicitContentFilter = l;
        boolean O = guild.O();
        int w = guild.w();
        int c = guild.c() != 0 ? guild.c() : this.afkTimeout;
        Long b2 = guild.b();
        Long M = guild.M();
        Set set = u.toSet(guild.m());
        String e = guild.e();
        String I = guild.I();
        if (I == null) {
            I = this.splash;
        }
        String str2 = I;
        int C = guild.C();
        int B = guild.B();
        int L = guild.L();
        String s2 = guild.s();
        if (s2 == null) {
            s2 = this.joinedAt;
        }
        String str3 = s2;
        Long H = guild.H();
        if (H == null) {
            H = this.rulesChannelId;
        }
        Long l2 = H;
        Long E = guild.E();
        if (E == null) {
            E = this.publicUpdatesChannelId;
        }
        Long l3 = E;
        String A = guild.A();
        if (A == null) {
            A = this.preferredLocale;
        }
        String str4 = A;
        GuildWelcomeScreen S = guild.S();
        if (S == null) {
            S = this.welcomeScreen;
        }
        GuildWelcomeScreen guildWelcomeScreen = S;
        GuildMaxVideoChannelUsers t = guild.t();
        if (t == null) {
            t = this.maxVideoChannelUsers;
        }
        GuildMaxVideoChannelUsers guildMaxVideoChannelUsers = t;
        String P = guild.P();
        if (P == null) {
            P = this.vanityUrlCode;
        }
        String str5 = P;
        boolean y2 = guild.y();
        GuildHubType p = guild.p();
        if (guild.d() != 0) {
            i = guild.d();
        } else {
            i = this.approximatePresenceCount;
        }
        return new Guild(emptyList, k, null, x2, i2, intValue, r, F, z2, str, guildVerificationLevel, guildExplicitContentFilter, O, w, c, b2, M, set, 0, e, str2, C, B, L, str3, l2, l3, str4, guildWelcomeScreen, guildMaxVideoChannelUsers, str5, i, y2, p, 4, 0, null);
    }

    public String toString() {
        StringBuilder R = a.R("Guild(roles=");
        R.append(this.roles);
        R.append(", emojis=");
        R.append(this.emojis);
        R.append(", stickers=");
        R.append(this.stickers);
        R.append(", name=");
        R.append(this.name);
        R.append(", description=");
        R.append(this.description);
        R.append(", defaultMessageNotifications=");
        R.append(this.defaultMessageNotifications);
        R.append(", id=");
        R.append(this.f2719id);
        R.append(", region=");
        R.append(this.region);
        R.append(", ownerId=");
        R.append(this.ownerId);
        R.append(", icon=");
        R.append(this.icon);
        R.append(", verificationLevel=");
        R.append(this.verificationLevel);
        R.append(", explicitContentFilter=");
        R.append(this.explicitContentFilter);
        R.append(", unavailable=");
        R.append(this.unavailable);
        R.append(", mfaLevel=");
        R.append(this.mfaLevel);
        R.append(", afkTimeout=");
        R.append(this.afkTimeout);
        R.append(", afkChannelId=");
        R.append(this.afkChannelId);
        R.append(", systemChannelId=");
        R.append(this.systemChannelId);
        R.append(", features=");
        R.append(this.features);
        R.append(", memberCount=");
        R.append(this.memberCount);
        R.append(", banner=");
        R.append(this.banner);
        R.append(", splash=");
        R.append(this.splash);
        R.append(", premiumTier=");
        R.append(this.premiumTier);
        R.append(", premiumSubscriptionCount=");
        R.append(this.premiumSubscriptionCount);
        R.append(", systemChannelFlags=");
        R.append(this.systemChannelFlags);
        R.append(", joinedAt=");
        R.append(this.joinedAt);
        R.append(", rulesChannelId=");
        R.append(this.rulesChannelId);
        R.append(", publicUpdatesChannelId=");
        R.append(this.publicUpdatesChannelId);
        R.append(", preferredLocale=");
        R.append(this.preferredLocale);
        R.append(", welcomeScreen=");
        R.append(this.welcomeScreen);
        R.append(", maxVideoChannelUsers=");
        R.append(this.maxVideoChannelUsers);
        R.append(", vanityUrlCode=");
        R.append(this.vanityUrlCode);
        R.append(", approximatePresenceCount=");
        R.append(this.approximatePresenceCount);
        R.append(", nsfw=");
        R.append(this.nsfw);
        R.append(", hubType=");
        R.append(this.hubType);
        R.append(")");
        return R.toString();
    }

    public /* synthetic */ Guild(List list, List list2, List list3, String str, String str2, int i, long j, String str3, long j2, String str4, GuildVerificationLevel guildVerificationLevel, GuildExplicitContentFilter guildExplicitContentFilter, boolean z2, int i2, int i3, Long l, Long l2, Set set, int i4, String str5, String str6, int i5, int i6, int i7, String str7, Long l3, Long l4, String str8, GuildWelcomeScreen guildWelcomeScreen, GuildMaxVideoChannelUsers guildMaxVideoChannelUsers, String str9, int i8, boolean z3, GuildHubType guildHubType, int i9, int i10, DefaultConstructorMarker defaultConstructorMarker) {
        this((i9 & 1) != 0 ? n.emptyList() : list, (i9 & 2) != 0 ? n.emptyList() : list2, (i9 & 4) != 0 ? n.emptyList() : list3, (i9 & 8) != 0 ? "" : str, (i9 & 16) != 0 ? null : str2, (i9 & 32) != 0 ? ModelNotificationSettings.FREQUENCY_ALL : i, (i9 & 64) != 0 ? 0L : j, (i9 & 128) != 0 ? null : str3, (i9 & 256) == 0 ? j2 : 0L, (i9 & 512) != 0 ? null : str4, (i9 & 1024) != 0 ? GuildVerificationLevel.NONE : guildVerificationLevel, (i9 & 2048) != 0 ? GuildExplicitContentFilter.NONE : guildExplicitContentFilter, (i9 & 4096) != 0 ? false : z2, (i9 & 8192) != 0 ? 0 : i2, (i9 & 16384) != 0 ? 0 : i3, (i9 & 32768) != 0 ? null : l, (i9 & 65536) != 0 ? null : l2, (i9 & 131072) != 0 ? n0.emptySet() : set, (i9 & 262144) != 0 ? 0 : i4, (i9 & 524288) != 0 ? null : str5, (i9 & 1048576) != 0 ? null : str6, (i9 & 2097152) != 0 ? 0 : i5, (i9 & 4194304) != 0 ? 0 : i6, (i9 & 8388608) != 0 ? 0 : i7, (i9 & 16777216) != 0 ? null : str7, (i9 & 33554432) != 0 ? null : l3, (i9 & 67108864) != 0 ? null : l4, (i9 & 134217728) != 0 ? null : str8, (i9 & 268435456) != 0 ? null : guildWelcomeScreen, (i9 & 536870912) != 0 ? GuildMaxVideoChannelUsers.Unlimited.INSTANCE : guildMaxVideoChannelUsers, (i9 & BasicMeasure.EXACTLY) != 0 ? null : str9, (i9 & Integer.MIN_VALUE) != 0 ? 0 : i8, (i10 & 1) == 0 ? z3 : false, (i10 & 2) != 0 ? null : guildHubType);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public Guild(com.discord.api.guild.Guild r43) {
        /*
            r42 = this;
            java.lang.String r0 = "apiGuild"
            r1 = r43
            d0.z.d.m.checkNotNullParameter(r1, r0)
            java.util.List r0 = r43.G()
            if (r0 == 0) goto Le
            goto L12
        Le:
            java.util.List r0 = d0.t.n.emptyList()
        L12:
            r3 = r0
            java.util.List r0 = r43.k()
            if (r0 == 0) goto L1a
            goto L1e
        L1a:
            java.util.List r0 = d0.t.n.emptyList()
        L1e:
            r4 = r0
            r5 = 0
            java.lang.String r6 = r43.x()
            java.lang.String r7 = r43.i()
            java.lang.Integer r0 = r43.h()
            if (r0 == 0) goto L33
            int r0 = r0.intValue()
            goto L35
        L33:
            int r0 = com.discord.models.domain.ModelNotificationSettings.FREQUENCY_ALL
        L35:
            r8 = r0
            long r9 = r43.r()
            java.lang.String r11 = r43.F()
            long r12 = r43.z()
            java.lang.String r14 = r43.q()
            com.discord.api.guild.GuildVerificationLevel r0 = r43.Q()
            if (r0 == 0) goto L4d
            goto L4f
        L4d:
            com.discord.api.guild.GuildVerificationLevel r0 = com.discord.api.guild.GuildVerificationLevel.NONE
        L4f:
            r15 = r0
            com.discord.api.guild.GuildExplicitContentFilter r0 = r43.l()
            if (r0 == 0) goto L57
            goto L59
        L57:
            com.discord.api.guild.GuildExplicitContentFilter r0 = com.discord.api.guild.GuildExplicitContentFilter.NONE
        L59:
            r16 = r0
            boolean r17 = r43.O()
            int r18 = r43.w()
            int r19 = r43.c()
            java.lang.Long r20 = r43.b()
            java.lang.Long r21 = r43.M()
            java.util.List r0 = r43.m()
            java.util.Set r22 = d0.t.u.toSet(r0)
            int r23 = r43.u()
            java.lang.String r24 = r43.e()
            java.lang.String r25 = r43.I()
            int r26 = r43.C()
            int r27 = r43.B()
            int r28 = r43.L()
            java.lang.String r29 = r43.s()
            java.lang.Long r30 = r43.H()
            java.lang.Long r31 = r43.E()
            java.lang.String r32 = r43.A()
            com.discord.api.guild.welcome.GuildWelcomeScreen r33 = r43.S()
            com.discord.api.guild.GuildMaxVideoChannelUsers r0 = r43.t()
            if (r0 == 0) goto Laa
            goto Lac
        Laa:
            com.discord.api.guild.GuildMaxVideoChannelUsers$Unlimited r0 = com.discord.api.guild.GuildMaxVideoChannelUsers.Unlimited.INSTANCE
        Lac:
            r34 = r0
            java.lang.String r35 = r43.P()
            int r36 = r43.d()
            boolean r37 = r43.y()
            com.discord.api.guild.GuildHubType r38 = r43.p()
            r39 = 4
            r40 = 0
            r41 = 0
            r2 = r42
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r11, r12, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r35, r36, r37, r38, r39, r40, r41)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.models.guild.Guild.<init>(com.discord.api.guild.Guild):void");
    }
}
