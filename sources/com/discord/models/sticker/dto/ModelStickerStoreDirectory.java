package com.discord.models.sticker.dto;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.store.dto.ModelStoreDirectoryLayout;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: ModelStickerStoreDirectory.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\b\u001b\u0010\u001cJ\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ,\u0010\u000b\u001a\u00020\u00002\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u0006HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u001b\u0010\n\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0017\u001a\u0004\b\u0018\u0010\bR\u001f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0019\u001a\u0004\b\u001a\u0010\u0005¨\u0006\u001d"}, d2 = {"Lcom/discord/models/sticker/dto/ModelStickerStoreDirectory;", "", "", "Lcom/discord/models/sticker/dto/ModelStickerPack;", "component1", "()Ljava/util/List;", "Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;", "component2", "()Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;", "stickerPacks", "storeDirectoryLayout", "copy", "(Ljava/util/List;Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;)Lcom/discord/models/sticker/dto/ModelStickerStoreDirectory;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;", "getStoreDirectoryLayout", "Ljava/util/List;", "getStickerPacks", HookHelper.constructorName, "(Ljava/util/List;Lcom/discord/models/store/dto/ModelStoreDirectoryLayout;)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelStickerStoreDirectory {
    private final List<ModelStickerPack> stickerPacks;
    private final ModelStoreDirectoryLayout storeDirectoryLayout;

    public ModelStickerStoreDirectory(List<ModelStickerPack> list, ModelStoreDirectoryLayout modelStoreDirectoryLayout) {
        m.checkNotNullParameter(list, "stickerPacks");
        this.stickerPacks = list;
        this.storeDirectoryLayout = modelStoreDirectoryLayout;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ ModelStickerStoreDirectory copy$default(ModelStickerStoreDirectory modelStickerStoreDirectory, List list, ModelStoreDirectoryLayout modelStoreDirectoryLayout, int i, Object obj) {
        if ((i & 1) != 0) {
            list = modelStickerStoreDirectory.stickerPacks;
        }
        if ((i & 2) != 0) {
            modelStoreDirectoryLayout = modelStickerStoreDirectory.storeDirectoryLayout;
        }
        return modelStickerStoreDirectory.copy(list, modelStoreDirectoryLayout);
    }

    public final List<ModelStickerPack> component1() {
        return this.stickerPacks;
    }

    public final ModelStoreDirectoryLayout component2() {
        return this.storeDirectoryLayout;
    }

    public final ModelStickerStoreDirectory copy(List<ModelStickerPack> list, ModelStoreDirectoryLayout modelStoreDirectoryLayout) {
        m.checkNotNullParameter(list, "stickerPacks");
        return new ModelStickerStoreDirectory(list, modelStoreDirectoryLayout);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModelStickerStoreDirectory)) {
            return false;
        }
        ModelStickerStoreDirectory modelStickerStoreDirectory = (ModelStickerStoreDirectory) obj;
        return m.areEqual(this.stickerPacks, modelStickerStoreDirectory.stickerPacks) && m.areEqual(this.storeDirectoryLayout, modelStickerStoreDirectory.storeDirectoryLayout);
    }

    public final List<ModelStickerPack> getStickerPacks() {
        return this.stickerPacks;
    }

    public final ModelStoreDirectoryLayout getStoreDirectoryLayout() {
        return this.storeDirectoryLayout;
    }

    public int hashCode() {
        List<ModelStickerPack> list = this.stickerPacks;
        int i = 0;
        int hashCode = (list != null ? list.hashCode() : 0) * 31;
        ModelStoreDirectoryLayout modelStoreDirectoryLayout = this.storeDirectoryLayout;
        if (modelStoreDirectoryLayout != null) {
            i = modelStoreDirectoryLayout.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder R = a.R("ModelStickerStoreDirectory(stickerPacks=");
        R.append(this.stickerPacks);
        R.append(", storeDirectoryLayout=");
        R.append(this.storeDirectoryLayout);
        R.append(")");
        return R.toString();
    }
}
