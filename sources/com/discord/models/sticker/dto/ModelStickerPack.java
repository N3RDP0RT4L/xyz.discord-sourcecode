package com.discord.models.sticker.dto;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.sticker.Sticker;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelSku;
import d0.z.d.m;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
/* compiled from: ModelStickerPack.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\b\n\u0002\b\u0015\b\u0086\b\u0018\u00002\u00020\u0001Bc\u0012\n\u0010\u001f\u001a\u00060\u000bj\u0002`\f\u0012\f\u0010 \u001a\b\u0012\u0004\u0012\u00020\b0\u000f\u0012\u0006\u0010!\u001a\u00020\u0012\u0012\b\u0010\"\u001a\u0004\u0018\u00010\u0015\u0012\n\u0010#\u001a\u00060\u000bj\u0002`\u0018\u0012\u000e\u0010$\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\u001a\u0012\b\u0010%\u001a\u0004\u0018\u00010\u0012\u0012\b\u0010&\u001a\u0004\u0018\u00010\u000b¢\u0006\u0004\b=\u0010>J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\r\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0004J\r\u0010\u0006\u001a\u00020\u0002¢\u0006\u0004\b\u0006\u0010\u0004J\r\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\u0004J\r\u0010\t\u001a\u00020\b¢\u0006\u0004\b\t\u0010\nJ\u0014\u0010\r\u001a\u00060\u000bj\u0002`\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0016\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\b0\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0012\u0010\u0016\u001a\u0004\u0018\u00010\u0015HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0014\u0010\u0019\u001a\u00060\u000bj\u0002`\u0018HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u000eJ\u0018\u0010\u001b\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\u001aHÆ\u0003¢\u0006\u0004\b\u001b\u0010\u001cJ\u0012\u0010\u001d\u001a\u0004\u0018\u00010\u0012HÆ\u0003¢\u0006\u0004\b\u001d\u0010\u0014J\u0012\u0010\u001e\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\u001e\u0010\u001cJ|\u0010'\u001a\u00020\u00002\f\b\u0002\u0010\u001f\u001a\u00060\u000bj\u0002`\f2\u000e\b\u0002\u0010 \u001a\b\u0012\u0004\u0012\u00020\b0\u000f2\b\b\u0002\u0010!\u001a\u00020\u00122\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\u00152\f\b\u0002\u0010#\u001a\u00060\u000bj\u0002`\u00182\u0010\b\u0002\u0010$\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\u001a2\n\b\u0002\u0010%\u001a\u0004\u0018\u00010\u00122\n\b\u0002\u0010&\u001a\u0004\u0018\u00010\u000bHÆ\u0001¢\u0006\u0004\b'\u0010(J\u0010\u0010)\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b)\u0010\u0014J\u0010\u0010+\u001a\u00020*HÖ\u0001¢\u0006\u0004\b+\u0010,J\u001a\u0010.\u001a\u00020\u00022\b\u0010-\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b.\u0010/R\u001f\u0010 \u001a\b\u0012\u0004\u0012\u00020\b0\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b \u00100\u001a\u0004\b1\u0010\u0011R\u001d\u0010\u001f\u001a\u00060\u000bj\u0002`\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00102\u001a\u0004\b3\u0010\u000eR\u001b\u0010&\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b&\u00104\u001a\u0004\b5\u0010\u001cR\u0019\u0010!\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b!\u00106\u001a\u0004\b7\u0010\u0014R!\u0010$\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b$\u00104\u001a\u0004\b8\u0010\u001cR\u001b\u0010\"\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00109\u001a\u0004\b:\u0010\u0017R\u001b\u0010%\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b%\u00106\u001a\u0004\b;\u0010\u0014R\u001d\u0010#\u001a\u00060\u000bj\u0002`\u00188\u0006@\u0006¢\u0006\f\n\u0004\b#\u00102\u001a\u0004\b<\u0010\u000e¨\u0006?"}, d2 = {"Lcom/discord/models/sticker/dto/ModelStickerPack;", "", "", "canBePurchased", "()Z", "isAnimatedPack", "isPremiumPack", "isLimitedPack", "Lcom/discord/api/sticker/Sticker;", "getCoverSticker", "()Lcom/discord/api/sticker/Sticker;", "", "Lcom/discord/primitives/StickerPackId;", "component1", "()J", "", "component2", "()Ljava/util/List;", "", "component3", "()Ljava/lang/String;", "Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;", "component4", "()Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;", "Lcom/discord/primitives/SkuId;", "component5", "Lcom/discord/primitives/StickerId;", "component6", "()Ljava/lang/Long;", "component7", "component8", ModelAuditLogEntry.CHANGE_KEY_ID, "stickers", ModelAuditLogEntry.CHANGE_KEY_NAME, "storeListing", "skuId", "coverStickerId", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "bannerAssetId", "copy", "(JLjava/util/List;Ljava/lang/String;Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;JLjava/lang/Long;Ljava/lang/String;Ljava/lang/Long;)Lcom/discord/models/sticker/dto/ModelStickerPack;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getStickers", "J", "getId", "Ljava/lang/Long;", "getBannerAssetId", "Ljava/lang/String;", "getName", "getCoverStickerId", "Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;", "getStoreListing", "getDescription", "getSkuId", HookHelper.constructorName, "(JLjava/util/List;Ljava/lang/String;Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;JLjava/lang/Long;Ljava/lang/String;Ljava/lang/Long;)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelStickerPack {
    private final Long bannerAssetId;
    private final Long coverStickerId;
    private final String description;

    /* renamed from: id  reason: collision with root package name */
    private final long f2721id;
    private final String name;
    private final long skuId;
    private final List<Sticker> stickers;
    private final ModelStickerPackStoreListing storeListing;

    public ModelStickerPack(long j, List<Sticker> list, String str, ModelStickerPackStoreListing modelStickerPackStoreListing, long j2, Long l, String str2, Long l2) {
        m.checkNotNullParameter(list, "stickers");
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        this.f2721id = j;
        this.stickers = list;
        this.name = str;
        this.storeListing = modelStickerPackStoreListing;
        this.skuId = j2;
        this.coverStickerId = l;
        this.description = str2;
        this.bannerAssetId = l2;
    }

    public final boolean canBePurchased() {
        ModelStickerPackStoreListing modelStickerPackStoreListing = this.storeListing;
        return modelStickerPackStoreListing != null && modelStickerPackStoreListing.getSku().isAvailable();
    }

    public final long component1() {
        return this.f2721id;
    }

    public final List<Sticker> component2() {
        return this.stickers;
    }

    public final String component3() {
        return this.name;
    }

    public final ModelStickerPackStoreListing component4() {
        return this.storeListing;
    }

    public final long component5() {
        return this.skuId;
    }

    public final Long component6() {
        return this.coverStickerId;
    }

    public final String component7() {
        return this.description;
    }

    public final Long component8() {
        return this.bannerAssetId;
    }

    public final ModelStickerPack copy(long j, List<Sticker> list, String str, ModelStickerPackStoreListing modelStickerPackStoreListing, long j2, Long l, String str2, Long l2) {
        m.checkNotNullParameter(list, "stickers");
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        return new ModelStickerPack(j, list, str, modelStickerPackStoreListing, j2, l, str2, l2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModelStickerPack)) {
            return false;
        }
        ModelStickerPack modelStickerPack = (ModelStickerPack) obj;
        return this.f2721id == modelStickerPack.f2721id && m.areEqual(this.stickers, modelStickerPack.stickers) && m.areEqual(this.name, modelStickerPack.name) && m.areEqual(this.storeListing, modelStickerPack.storeListing) && this.skuId == modelStickerPack.skuId && m.areEqual(this.coverStickerId, modelStickerPack.coverStickerId) && m.areEqual(this.description, modelStickerPack.description) && m.areEqual(this.bannerAssetId, modelStickerPack.bannerAssetId);
    }

    public final Long getBannerAssetId() {
        return this.bannerAssetId;
    }

    public final Sticker getCoverSticker() {
        Object obj;
        boolean z2;
        Iterator<T> it = this.stickers.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            long id2 = ((Sticker) obj).getId();
            Long l = this.coverStickerId;
            if (l != null && id2 == l.longValue()) {
                z2 = true;
                continue;
            } else {
                z2 = false;
                continue;
            }
            if (z2) {
                break;
            }
        }
        Sticker sticker = (Sticker) obj;
        return sticker != null ? sticker : this.stickers.get(0);
    }

    public final Long getCoverStickerId() {
        return this.coverStickerId;
    }

    public final String getDescription() {
        return this.description;
    }

    public final long getId() {
        return this.f2721id;
    }

    public final String getName() {
        return this.name;
    }

    public final long getSkuId() {
        return this.skuId;
    }

    public final List<Sticker> getStickers() {
        return this.stickers;
    }

    public final ModelStickerPackStoreListing getStoreListing() {
        return this.storeListing;
    }

    public int hashCode() {
        long j = this.f2721id;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        List<Sticker> list = this.stickers;
        int i2 = 0;
        int hashCode = (i + (list != null ? list.hashCode() : 0)) * 31;
        String str = this.name;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        ModelStickerPackStoreListing modelStickerPackStoreListing = this.storeListing;
        int hashCode3 = modelStickerPackStoreListing != null ? modelStickerPackStoreListing.hashCode() : 0;
        long j2 = this.skuId;
        int i3 = (((hashCode2 + hashCode3) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31;
        Long l = this.coverStickerId;
        int hashCode4 = (i3 + (l != null ? l.hashCode() : 0)) * 31;
        String str2 = this.description;
        int hashCode5 = (hashCode4 + (str2 != null ? str2.hashCode() : 0)) * 31;
        Long l2 = this.bannerAssetId;
        if (l2 != null) {
            i2 = l2.hashCode();
        }
        return hashCode5 + i2;
    }

    public final boolean isAnimatedPack() {
        List<Sticker> list = this.stickers;
        if ((list instanceof Collection) && list.isEmpty()) {
            return false;
        }
        for (Sticker sticker : list) {
            if (sticker.l()) {
                return true;
            }
        }
        return false;
    }

    public final boolean isLimitedPack() {
        ModelStickerPackStoreListing modelStickerPackStoreListing = this.storeListing;
        return (modelStickerPackStoreListing != null ? modelStickerPackStoreListing.getUnpublishedAt() : null) != null;
    }

    public final boolean isPremiumPack() {
        ModelSku sku;
        ModelStickerPackStoreListing modelStickerPackStoreListing = this.storeListing;
        if (modelStickerPackStoreListing == null || (sku = modelStickerPackStoreListing.getSku()) == null) {
            return false;
        }
        return sku.getPremium();
    }

    public String toString() {
        StringBuilder R = a.R("ModelStickerPack(id=");
        R.append(this.f2721id);
        R.append(", stickers=");
        R.append(this.stickers);
        R.append(", name=");
        R.append(this.name);
        R.append(", storeListing=");
        R.append(this.storeListing);
        R.append(", skuId=");
        R.append(this.skuId);
        R.append(", coverStickerId=");
        R.append(this.coverStickerId);
        R.append(", description=");
        R.append(this.description);
        R.append(", bannerAssetId=");
        return a.F(R, this.bannerAssetId, ")");
    }
}
