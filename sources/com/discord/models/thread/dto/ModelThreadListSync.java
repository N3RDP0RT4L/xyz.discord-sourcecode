package com.discord.models.thread.dto;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.message.Message;
import com.discord.api.thread.ThreadMember;
import com.discord.models.deserialization.gson.InboundGatewayGsonParser;
import com.discord.models.domain.Model;
import com.discord.models.domain.ModelAuditLogEntry;
import com.google.gson.stream.JsonToken;
import d0.t.n;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Ref$LongRef;
import kotlin.jvm.internal.Ref$ObjectRef;
import rx.functions.Action1;
/* compiled from: ModelThreadListSync.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001:\u0001&BA\u0012\n\u0010\u000e\u001a\u00060\u0002j\u0002`\u0003\u0012\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u000e\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010\u0006\u0012\u000e\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u0006¢\u0006\u0004\b$\u0010%J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0016\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0018\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u000b\u0010\tJ\u0018\u0010\r\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\r\u0010\tJR\u0010\u0012\u001a\u00020\u00002\f\b\u0002\u0010\u000e\u001a\u00060\u0002j\u0002`\u00032\u000e\b\u0002\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0010\b\u0002\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010\u00062\u0010\b\u0002\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u0006HÆ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0018\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u001a\u0010\u001c\u001a\u00020\u001b2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR!\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001e\u001a\u0004\b\u001f\u0010\tR!\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001e\u001a\u0004\b \u0010\tR\u001d\u0010\u000e\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010!\u001a\u0004\b\"\u0010\u0005R\u001f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00070\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001e\u001a\u0004\b#\u0010\t¨\u0006'"}, d2 = {"Lcom/discord/models/thread/dto/ModelThreadListSync;", "", "", "Lcom/discord/primitives/GuildId;", "component1", "()J", "", "Lcom/discord/api/channel/Channel;", "component2", "()Ljava/util/List;", "Lcom/discord/api/thread/ThreadMember;", "component3", "Lcom/discord/api/message/Message;", "component4", "guildId", "threads", "members", "mostRecentMessages", "copy", "(JLjava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/discord/models/thread/dto/ModelThreadListSync;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getMostRecentMessages", "getMembers", "J", "getGuildId", "getThreads", HookHelper.constructorName, "(JLjava/util/List;Ljava/util/List;Ljava/util/List;)V", "Parser", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelThreadListSync {
    private final long guildId;
    private final List<ThreadMember> members;
    private final List<Message> mostRecentMessages;
    private final List<Channel> threads;

    /* compiled from: ModelThreadListSync.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/models/thread/dto/ModelThreadListSync$Parser;", "Lcom/discord/models/domain/Model$Parser;", "Lcom/discord/models/thread/dto/ModelThreadListSync;", "Lcom/discord/models/domain/Model$JsonReader;", "reader", "parse", "(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/thread/dto/ModelThreadListSync;", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Parser implements Model.Parser<ModelThreadListSync> {
        public static final Parser INSTANCE = new Parser();

        private Parser() {
        }

        /* JADX WARN: Can't rename method to resolve collision */
        /* JADX WARN: Type inference failed for: r2v0, types: [T, java.util.ArrayList] */
        @Override // com.discord.models.domain.Model.Parser
        public ModelThreadListSync parse(final Model.JsonReader jsonReader) {
            m.checkNotNullParameter(jsonReader, "reader");
            final Ref$LongRef ref$LongRef = new Ref$LongRef();
            ref$LongRef.element = -1L;
            final Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
            ref$ObjectRef.element = null;
            final Ref$ObjectRef ref$ObjectRef2 = new Ref$ObjectRef();
            ref$ObjectRef2.element = new ArrayList();
            final Ref$ObjectRef ref$ObjectRef3 = new Ref$ObjectRef();
            ref$ObjectRef3.element = null;
            jsonReader.nextObject(new Action1<String>() { // from class: com.discord.models.thread.dto.ModelThreadListSync$Parser$parse$1
                public final void call(String str) {
                    if (str != null) {
                        switch (str.hashCode()) {
                            case -1337936983:
                                if (str.equals("threads")) {
                                    ref$ObjectRef.element = (T) jsonReader.nextList(new Model.JsonReader.ItemFactory<Channel>() { // from class: com.discord.models.thread.dto.ModelThreadListSync$Parser$parse$1.1
                                        /* JADX WARN: Can't rename method to resolve collision */
                                        @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                                        public final Channel get() {
                                            return (Channel) InboundGatewayGsonParser.fromJson(jsonReader, Channel.class);
                                        }
                                    });
                                    return;
                                }
                                break;
                            case -1306538777:
                                if (str.equals(ModelAuditLogEntry.CHANGE_KEY_GUILD_ID)) {
                                    Ref$LongRef ref$LongRef2 = Ref$LongRef.this;
                                    Long nextLongOrNull = jsonReader.nextLongOrNull();
                                    m.checkNotNull(nextLongOrNull);
                                    ref$LongRef2.element = nextLongOrNull.longValue();
                                    return;
                                }
                                break;
                            case 948881689:
                                if (str.equals("members")) {
                                    if (jsonReader.peek() == JsonToken.BEGIN_ARRAY) {
                                        Ref$ObjectRef ref$ObjectRef4 = ref$ObjectRef2;
                                        T t = (T) jsonReader.nextList(new Model.JsonReader.ItemFactory<ThreadMember>() { // from class: com.discord.models.thread.dto.ModelThreadListSync$Parser$parse$1.2
                                            /* JADX WARN: Can't rename method to resolve collision */
                                            @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                                            public final ThreadMember get() {
                                                return (ThreadMember) InboundGatewayGsonParser.fromJson(jsonReader, ThreadMember.class);
                                            }
                                        });
                                        m.checkNotNullExpressionValue(t, "reader.nextList { Inboun…readMember::class.java) }");
                                        ref$ObjectRef4.element = t;
                                        return;
                                    }
                                    jsonReader.nextObject(new Action1<String>() { // from class: com.discord.models.thread.dto.ModelThreadListSync$Parser$parse$1.3
                                        public final void call(String str2) {
                                            ThreadMember threadMember = (ThreadMember) InboundGatewayGsonParser.fromJson(jsonReader, ThreadMember.class);
                                            m.checkNotNullExpressionValue(str2, "threadId");
                                            ((List) ref$ObjectRef2.element).add(new ThreadMember(Long.parseLong(str2), threadMember.f(), threadMember.a(), threadMember.c(), threadMember.e(), threadMember.d()));
                                        }
                                    });
                                    return;
                                }
                                break;
                            case 1472794100:
                                if (str.equals("most_recent_messages")) {
                                    ref$ObjectRef3.element = (T) jsonReader.nextList(new Model.JsonReader.ItemFactory<Message>() { // from class: com.discord.models.thread.dto.ModelThreadListSync$Parser$parse$1.4
                                        /* JADX WARN: Can't rename method to resolve collision */
                                        @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                                        public final Message get() {
                                            return (Message) InboundGatewayGsonParser.fromJson(jsonReader, Message.class);
                                        }
                                    });
                                    return;
                                }
                                break;
                        }
                    }
                    jsonReader.skipValue();
                }
            });
            long j = ref$LongRef.element;
            List list = (List) ref$ObjectRef.element;
            if (list == null) {
                list = n.emptyList();
            }
            return new ModelThreadListSync(j, list, (List) ref$ObjectRef2.element, (List) ref$ObjectRef3.element);
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public ModelThreadListSync(long j, List<Channel> list, List<? extends ThreadMember> list2, List<Message> list3) {
        m.checkNotNullParameter(list, "threads");
        this.guildId = j;
        this.threads = list;
        this.members = list2;
        this.mostRecentMessages = list3;
    }

    public static /* synthetic */ ModelThreadListSync copy$default(ModelThreadListSync modelThreadListSync, long j, List list, List list2, List list3, int i, Object obj) {
        if ((i & 1) != 0) {
            j = modelThreadListSync.guildId;
        }
        long j2 = j;
        List<Channel> list4 = list;
        if ((i & 2) != 0) {
            list4 = modelThreadListSync.threads;
        }
        List list5 = list4;
        List<ThreadMember> list6 = list2;
        if ((i & 4) != 0) {
            list6 = modelThreadListSync.members;
        }
        List list7 = list6;
        List<Message> list8 = list3;
        if ((i & 8) != 0) {
            list8 = modelThreadListSync.mostRecentMessages;
        }
        return modelThreadListSync.copy(j2, list5, list7, list8);
    }

    public final long component1() {
        return this.guildId;
    }

    public final List<Channel> component2() {
        return this.threads;
    }

    public final List<ThreadMember> component3() {
        return this.members;
    }

    public final List<Message> component4() {
        return this.mostRecentMessages;
    }

    public final ModelThreadListSync copy(long j, List<Channel> list, List<? extends ThreadMember> list2, List<Message> list3) {
        m.checkNotNullParameter(list, "threads");
        return new ModelThreadListSync(j, list, list2, list3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ModelThreadListSync)) {
            return false;
        }
        ModelThreadListSync modelThreadListSync = (ModelThreadListSync) obj;
        return this.guildId == modelThreadListSync.guildId && m.areEqual(this.threads, modelThreadListSync.threads) && m.areEqual(this.members, modelThreadListSync.members) && m.areEqual(this.mostRecentMessages, modelThreadListSync.mostRecentMessages);
    }

    public final long getGuildId() {
        return this.guildId;
    }

    public final List<ThreadMember> getMembers() {
        return this.members;
    }

    public final List<Message> getMostRecentMessages() {
        return this.mostRecentMessages;
    }

    public final List<Channel> getThreads() {
        return this.threads;
    }

    public int hashCode() {
        long j = this.guildId;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        List<Channel> list = this.threads;
        int i2 = 0;
        int hashCode = (i + (list != null ? list.hashCode() : 0)) * 31;
        List<ThreadMember> list2 = this.members;
        int hashCode2 = (hashCode + (list2 != null ? list2.hashCode() : 0)) * 31;
        List<Message> list3 = this.mostRecentMessages;
        if (list3 != null) {
            i2 = list3.hashCode();
        }
        return hashCode2 + i2;
    }

    public String toString() {
        StringBuilder R = a.R("ModelThreadListSync(guildId=");
        R.append(this.guildId);
        R.append(", threads=");
        R.append(this.threads);
        R.append(", members=");
        R.append(this.members);
        R.append(", mostRecentMessages=");
        return a.K(R, this.mostRecentMessages, ")");
    }
}
