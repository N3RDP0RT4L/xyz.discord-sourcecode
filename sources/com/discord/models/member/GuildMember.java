package com.discord.models.member;

import a0.a.a.b;
import andhook.lib.HookHelper;
import androidx.annotation.ColorInt;
import androidx.core.view.ViewCompat;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelRecipientNick;
import com.discord.api.role.GuildRole;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.user.User;
import com.discord.stores.StoreGuilds;
import com.discord.utilities.guilds.RoleUtils;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.user.UserUtils;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: GuildMember.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b0\b\u0086\b\u0018\u0000 K2\u00020\u0001:\u0001KB\u009f\u0001\u0012\b\b\u0001\u0010\"\u001a\u00020\r\u0012\n\u0010#\u001a\u00060\u0003j\u0002`\u0004\u0012\u0012\u0010$\u001a\u000e\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0018\u00010\u0002\u0012\b\u0010%\u001a\u0004\u0018\u00010\u0012\u0012\b\u0010&\u001a\u0004\u0018\u00010\u0012\u0012\u0006\u0010'\u001a\u00020\b\u0012\b\u0010(\u001a\u0004\u0018\u00010\u0017\u0012\n\u0010)\u001a\u00060\u0003j\u0002`\u001a\u0012\n\u0010*\u001a\u00060\u0003j\u0002`\u001c\u0012\n\b\u0002\u0010+\u001a\u0004\u0018\u00010\u0012\u0012\n\b\u0002\u0010,\u001a\u0004\u0018\u00010\u0012\u0012\n\b\u0002\u0010-\u001a\u0004\u0018\u00010\u0012\u0012\n\b\u0002\u0010.\u001a\u0004\u0018\u00010\u0017¢\u0006\u0004\bH\u0010IB\t\b\u0016¢\u0006\u0004\bH\u0010JJ\u001c\u0010\u0005\u001a\u000e\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0018\u00010\u0002HÂ\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\u0007\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002¢\u0006\u0004\b\u0007\u0010\u0006J\r\u0010\t\u001a\u00020\b¢\u0006\u0004\b\t\u0010\nJ\r\u0010\u000b\u001a\u00020\b¢\u0006\u0004\b\u000b\u0010\nJ\r\u0010\f\u001a\u00020\b¢\u0006\u0004\b\f\u0010\nJ\u0010\u0010\u000e\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0014\u0010\u0010\u001a\u00060\u0003j\u0002`\u0004HÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0012\u0010\u0015\u001a\u0004\u0018\u00010\u0012HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u0016\u0010\nJ\u0012\u0010\u0018\u001a\u0004\u0018\u00010\u0017HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0019J\u0014\u0010\u001b\u001a\u00060\u0003j\u0002`\u001aHÆ\u0003¢\u0006\u0004\b\u001b\u0010\u0011J\u0014\u0010\u001d\u001a\u00060\u0003j\u0002`\u001cHÆ\u0003¢\u0006\u0004\b\u001d\u0010\u0011J\u0012\u0010\u001e\u001a\u0004\u0018\u00010\u0012HÆ\u0003¢\u0006\u0004\b\u001e\u0010\u0014J\u0012\u0010\u001f\u001a\u0004\u0018\u00010\u0012HÆ\u0003¢\u0006\u0004\b\u001f\u0010\u0014J\u0012\u0010 \u001a\u0004\u0018\u00010\u0012HÆ\u0003¢\u0006\u0004\b \u0010\u0014J\u0012\u0010!\u001a\u0004\u0018\u00010\u0017HÆ\u0003¢\u0006\u0004\b!\u0010\u0019J¸\u0001\u0010/\u001a\u00020\u00002\b\b\u0003\u0010\"\u001a\u00020\r2\f\b\u0002\u0010#\u001a\u00060\u0003j\u0002`\u00042\u0014\b\u0002\u0010$\u001a\u000e\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0018\u00010\u00022\n\b\u0002\u0010%\u001a\u0004\u0018\u00010\u00122\n\b\u0002\u0010&\u001a\u0004\u0018\u00010\u00122\b\b\u0002\u0010'\u001a\u00020\b2\n\b\u0002\u0010(\u001a\u0004\u0018\u00010\u00172\f\b\u0002\u0010)\u001a\u00060\u0003j\u0002`\u001a2\f\b\u0002\u0010*\u001a\u00060\u0003j\u0002`\u001c2\n\b\u0002\u0010+\u001a\u0004\u0018\u00010\u00122\n\b\u0002\u0010,\u001a\u0004\u0018\u00010\u00122\n\b\u0002\u0010-\u001a\u0004\u0018\u00010\u00122\n\b\u0002\u0010.\u001a\u0004\u0018\u00010\u0017HÆ\u0001¢\u0006\u0004\b/\u00100J\u0010\u00101\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b1\u0010\u0014J\u0010\u00102\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b2\u0010\u000fJ\u001a\u00104\u001a\u00020\b2\b\u00103\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b4\u00105R\u001d\u0010*\u001a\u00060\u0003j\u0002`\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b*\u00106\u001a\u0004\b7\u0010\u0011R\u001b\u0010.\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b.\u00108\u001a\u0004\b9\u0010\u0019R\u001b\u0010,\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010:\u001a\u0004\b;\u0010\u0014R\u0019\u0010'\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010<\u001a\u0004\b=\u0010\nR\"\u0010$\u001a\u000e\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010>R\u001b\u0010%\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010:\u001a\u0004\b?\u0010\u0014R\u001b\u0010(\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b(\u00108\u001a\u0004\b@\u0010\u0019R\u001d\u0010#\u001a\u00060\u0003j\u0002`\u00048\u0006@\u0006¢\u0006\f\n\u0004\b#\u00106\u001a\u0004\bA\u0010\u0011R\u001d\u0010)\u001a\u00060\u0003j\u0002`\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b)\u00106\u001a\u0004\bB\u0010\u0011R\u001b\u0010-\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010:\u001a\u0004\bC\u0010\u0014R\u0019\u0010\"\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010D\u001a\u0004\bE\u0010\u000fR\u001b\u0010&\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010:\u001a\u0004\bF\u0010\u0014R\u001b\u0010+\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010:\u001a\u0004\bG\u0010\u0014¨\u0006L"}, d2 = {"Lcom/discord/models/member/GuildMember;", "", "", "", "Lcom/discord/primitives/RoleId;", "component3", "()Ljava/util/List;", "getRoles", "", "hasAvatar", "()Z", "hasBanner", "isCommunicationDisabled", "", "component1", "()I", "component2", "()J", "", "component4", "()Ljava/lang/String;", "component5", "component6", "Lcom/discord/api/utcdatetime/UtcDateTime;", "component7", "()Lcom/discord/api/utcdatetime/UtcDateTime;", "Lcom/discord/primitives/GuildId;", "component8", "Lcom/discord/primitives/UserId;", "component9", "component10", "component11", "component12", "component13", ModelAuditLogEntry.CHANGE_KEY_COLOR, "hoistRoleId", "roles", ModelAuditLogEntry.CHANGE_KEY_NICK, "premiumSince", "pending", "joinedAt", "guildId", "userId", "avatarHash", "bannerHash", "bio", "communicationDisabledUntil", "copy", "(IJLjava/util/List;Ljava/lang/String;Ljava/lang/String;ZLcom/discord/api/utcdatetime/UtcDateTime;JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/discord/api/utcdatetime/UtcDateTime;)Lcom/discord/models/member/GuildMember;", "toString", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "J", "getUserId", "Lcom/discord/api/utcdatetime/UtcDateTime;", "getCommunicationDisabledUntil", "Ljava/lang/String;", "getBannerHash", "Z", "getPending", "Ljava/util/List;", "getNick", "getJoinedAt", "getHoistRoleId", "getGuildId", "getBio", "I", "getColor", "getPremiumSince", "getAvatarHash", HookHelper.constructorName, "(IJLjava/util/List;Ljava/lang/String;Ljava/lang/String;ZLcom/discord/api/utcdatetime/UtcDateTime;JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/discord/api/utcdatetime/UtcDateTime;)V", "()V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildMember {
    public static final Companion Companion = new Companion(null);
    private static final List<Long> emptyRoles = new ArrayList();
    private final String avatarHash;
    private final String bannerHash;
    private final String bio;
    private final int color;
    private final UtcDateTime communicationDisabledUntil;
    private final long guildId;
    private final long hoistRoleId;
    private final UtcDateTime joinedAt;
    private final String nick;
    private final boolean pending;
    private final String premiumSince;
    private final List<Long> roles;
    private final long userId;

    /* compiled from: GuildMember.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0010\t\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b-\u0010.J7\u0010\t\u001a\u0004\u0018\u00010\u00072\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\u0018\u0010\b\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002`\u0006\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0005¢\u0006\u0004\b\t\u0010\nJI\u0010\u0012\u001a\u00020\u00112\u0006\u0010\f\u001a\u00020\u000b2\n\u0010\u000e\u001a\u00060\u0003j\u0002`\r2\u001a\b\u0002\u0010\b\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002`\u0006\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u00052\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u000f¢\u0006\u0004\b\u0012\u0010\u0013J#\u0010\u0017\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u00112\b\b\u0001\u0010\u0016\u001a\u00020\u0015H\u0007¢\u0006\u0004\b\u0017\u0010\u0018J#\u0010\u0017\u001a\u00020\u00152\b\b\u0001\u0010\u0019\u001a\u00020\u00152\b\b\u0001\u0010\u0016\u001a\u00020\u0015H\u0007¢\u0006\u0004\b\u0017\u0010\u001aJ7\u0010 \u001a\u00020\u00152\b\u0010\u001c\u001a\u0004\u0018\u00010\u001b2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001b2\b\u0010\u001e\u001a\u0004\u0018\u00010\u00112\b\u0010\u001f\u001a\u0004\u0018\u00010\u0011H\u0007¢\u0006\u0004\b \u0010!J!\u0010$\u001a\u00020#2\b\u0010\u0014\u001a\u0004\u0018\u00010\u00112\u0006\u0010\"\u001a\u00020\u001bH\u0007¢\u0006\u0004\b$\u0010%J;\u0010$\u001a\u00020#2\u0006\u0010\"\u001a\u00020\u001b2\b\u0010\u0014\u001a\u0004\u0018\u00010\u00112\b\u0010'\u001a\u0004\u0018\u00010&2\u0010\b\u0002\u0010)\u001a\n\u0012\u0004\u0012\u00020(\u0018\u00010\u0002¢\u0006\u0004\b$\u0010*R \u0010+\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00060\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b+\u0010,¨\u0006/"}, d2 = {"Lcom/discord/models/member/GuildMember$Companion;", "", "", "", "roles", "", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "guildRoles", "getHighestRoleIconRole", "(Ljava/util/List;Ljava/util/Map;)Lcom/discord/api/role/GuildRole;", "Lcom/discord/api/guildmember/GuildMember;", "apiGuildMember", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/models/member/GuildMember;", "from", "(Lcom/discord/api/guildmember/GuildMember;JLjava/util/Map;Lcom/discord/stores/StoreGuilds;)Lcom/discord/models/member/GuildMember;", "member", "", "defaultColor", "getColor", "(Lcom/discord/models/member/GuildMember;I)I", ModelAuditLogEntry.CHANGE_KEY_COLOR, "(II)I", "Lcom/discord/models/user/User;", "user1", "user2", "member1", "member2", "compareUserNames", "(Lcom/discord/models/user/User;Lcom/discord/models/user/User;Lcom/discord/models/member/GuildMember;Lcom/discord/models/member/GuildMember;)I", "user", "", "getNickOrUsername", "(Lcom/discord/models/member/GuildMember;Lcom/discord/models/user/User;)Ljava/lang/String;", "Lcom/discord/api/channel/Channel;", "channel", "Lcom/discord/api/channel/ChannelRecipientNick;", "nicks", "(Lcom/discord/models/user/User;Lcom/discord/models/member/GuildMember;Lcom/discord/api/channel/Channel;Ljava/util/List;)Ljava/lang/String;", "emptyRoles", "Ljava/util/List;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ String getNickOrUsername$default(Companion companion, User user, GuildMember guildMember, Channel channel, List list, int i, Object obj) {
            if ((i & 8) != 0) {
                list = null;
            }
            return companion.getNickOrUsername(user, guildMember, channel, list);
        }

        public final int compareUserNames(User user, User user2, GuildMember guildMember, GuildMember guildMember2) {
            String str = null;
            String nick = guildMember != null ? guildMember.getNick() : null;
            if (guildMember2 != null) {
                str = guildMember2.getNick();
            }
            return UserUtils.INSTANCE.compareUserNames(user, user2, nick, str);
        }

        public final GuildMember from(com.discord.api.guildmember.GuildMember guildMember, long j, Map<Long, GuildRole> map, StoreGuilds storeGuilds) {
            Map<Long, GuildRole> map2;
            GuildRole guildRole;
            Map<Long, Map<Long, GuildRole>> roles;
            m.checkNotNullParameter(guildMember, "apiGuildMember");
            List<Long> l = guildMember.l();
            GuildRole guildRole2 = null;
            GuildRole guildRole3 = null;
            for (Long l2 : l) {
                long longValue = l2.longValue();
                if (map != null) {
                    map2 = map;
                } else {
                    map2 = (storeGuilds == null || (roles = storeGuilds.getRoles()) == null) ? null : roles.get(Long.valueOf(j));
                }
                if (!(map2 == null || (guildRole = map2.get(Long.valueOf(longValue))) == null)) {
                    if (!RoleUtils.isDefaultColor(guildRole) && RoleUtils.rankIsHigher(guildRole, guildRole2)) {
                        guildRole2 = guildRole;
                    }
                    if (guildRole.c() && RoleUtils.rankIsHigher(guildRole, guildRole3)) {
                        guildRole3 = guildRole;
                    }
                }
            }
            return new GuildMember(RoleUtils.getOpaqueColor(guildRole2), guildRole3 != null ? guildRole3.getId() : 0L, l.isEmpty() ^ true ? l : null, guildMember.h(), guildMember.j(), guildMember.i(), guildMember.g(), j, guildMember.m().i(), guildMember.b(), guildMember.c(), guildMember.d(), guildMember.e());
        }

        public final int getColor(@ColorInt int i, @ColorInt int i2) {
            return i != -16777216 ? i : i2;
        }

        public final int getColor(GuildMember guildMember, @ColorInt int i) {
            return getColor(guildMember != null ? guildMember.getColor() : ViewCompat.MEASURED_STATE_MASK, i);
        }

        public final GuildRole getHighestRoleIconRole(List<Long> list, Map<Long, GuildRole> map) {
            GuildRole guildRole;
            m.checkNotNullParameter(list, "roles");
            GuildRole guildRole2 = null;
            for (Long l : list) {
                long longValue = l.longValue();
                if (!(map == null || (guildRole = map.get(Long.valueOf(longValue))) == null)) {
                    if (((guildRole.d() == null && guildRole.k() == null) ? false : true) && RoleUtils.rankIsHigher(guildRole, guildRole2)) {
                        guildRole2 = guildRole;
                    }
                }
            }
            return guildRole2;
        }

        public final String getNickOrUsername(GuildMember guildMember, User user) {
            m.checkNotNullParameter(user, "user");
            return getNickOrUsername(user, guildMember, null, null);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final String getNickOrUsername(User user, GuildMember guildMember, Channel channel, List<ChannelRecipientNick> list) {
            ChannelRecipientNick channelRecipientNick;
            String c;
            Object obj;
            boolean z2;
            m.checkNotNullParameter(user, "user");
            String str = null;
            if (list == null) {
                list = channel != null ? channel.n() : null;
            }
            if (list != null) {
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj = null;
                        break;
                    }
                    obj = it.next();
                    if (((ChannelRecipientNick) obj).b() == user.getId()) {
                        z2 = true;
                        continue;
                    } else {
                        z2 = false;
                        continue;
                    }
                    if (z2) {
                        break;
                    }
                }
                channelRecipientNick = (ChannelRecipientNick) obj;
            } else {
                channelRecipientNick = null;
            }
            if (channelRecipientNick != null && (c = channelRecipientNick.c()) != null) {
                str = c;
            } else if (guildMember != null) {
                str = guildMember.getNick();
            }
            return str != null ? str : user.getUsername();
        }
    }

    public GuildMember(@ColorInt int i, long j, List<Long> list, String str, String str2, boolean z2, UtcDateTime utcDateTime, long j2, long j3, String str3, String str4, String str5, UtcDateTime utcDateTime2) {
        this.color = i;
        this.hoistRoleId = j;
        this.roles = list;
        this.nick = str;
        this.premiumSince = str2;
        this.pending = z2;
        this.joinedAt = utcDateTime;
        this.guildId = j2;
        this.userId = j3;
        this.avatarHash = str3;
        this.bannerHash = str4;
        this.bio = str5;
        this.communicationDisabledUntil = utcDateTime2;
    }

    public static final int compareUserNames(User user, User user2, GuildMember guildMember, GuildMember guildMember2) {
        return Companion.compareUserNames(user, user2, guildMember, guildMember2);
    }

    private final List<Long> component3() {
        return this.roles;
    }

    public static final int getColor(@ColorInt int i, @ColorInt int i2) {
        return Companion.getColor(i, i2);
    }

    public static final int getColor(GuildMember guildMember, @ColorInt int i) {
        return Companion.getColor(guildMember, i);
    }

    public static final String getNickOrUsername(GuildMember guildMember, User user) {
        return Companion.getNickOrUsername(guildMember, user);
    }

    public final int component1() {
        return this.color;
    }

    public final String component10() {
        return this.avatarHash;
    }

    public final String component11() {
        return this.bannerHash;
    }

    public final String component12() {
        return this.bio;
    }

    public final UtcDateTime component13() {
        return this.communicationDisabledUntil;
    }

    public final long component2() {
        return this.hoistRoleId;
    }

    public final String component4() {
        return this.nick;
    }

    public final String component5() {
        return this.premiumSince;
    }

    public final boolean component6() {
        return this.pending;
    }

    public final UtcDateTime component7() {
        return this.joinedAt;
    }

    public final long component8() {
        return this.guildId;
    }

    public final long component9() {
        return this.userId;
    }

    public final GuildMember copy(@ColorInt int i, long j, List<Long> list, String str, String str2, boolean z2, UtcDateTime utcDateTime, long j2, long j3, String str3, String str4, String str5, UtcDateTime utcDateTime2) {
        return new GuildMember(i, j, list, str, str2, z2, utcDateTime, j2, j3, str3, str4, str5, utcDateTime2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildMember)) {
            return false;
        }
        GuildMember guildMember = (GuildMember) obj;
        return this.color == guildMember.color && this.hoistRoleId == guildMember.hoistRoleId && m.areEqual(this.roles, guildMember.roles) && m.areEqual(this.nick, guildMember.nick) && m.areEqual(this.premiumSince, guildMember.premiumSince) && this.pending == guildMember.pending && m.areEqual(this.joinedAt, guildMember.joinedAt) && this.guildId == guildMember.guildId && this.userId == guildMember.userId && m.areEqual(this.avatarHash, guildMember.avatarHash) && m.areEqual(this.bannerHash, guildMember.bannerHash) && m.areEqual(this.bio, guildMember.bio) && m.areEqual(this.communicationDisabledUntil, guildMember.communicationDisabledUntil);
    }

    public final String getAvatarHash() {
        return this.avatarHash;
    }

    public final String getBannerHash() {
        return this.bannerHash;
    }

    public final String getBio() {
        return this.bio;
    }

    public final int getColor() {
        return this.color;
    }

    public final UtcDateTime getCommunicationDisabledUntil() {
        return this.communicationDisabledUntil;
    }

    public final long getGuildId() {
        return this.guildId;
    }

    public final long getHoistRoleId() {
        return this.hoistRoleId;
    }

    public final UtcDateTime getJoinedAt() {
        return this.joinedAt;
    }

    public final String getNick() {
        return this.nick;
    }

    public final boolean getPending() {
        return this.pending;
    }

    public final String getPremiumSince() {
        return this.premiumSince;
    }

    public final List<Long> getRoles() {
        List<Long> list = this.roles;
        return list != null ? list : emptyRoles;
    }

    public final long getUserId() {
        return this.userId;
    }

    public final boolean hasAvatar() {
        String str = this.avatarHash;
        if (str == null) {
            return false;
        }
        return (str.length() > 0) && this.guildId > 0 && this.userId > 0;
    }

    public final boolean hasBanner() {
        String str = this.bannerHash;
        if (str == null) {
            return false;
        }
        return (str.length() > 0) && this.guildId > 0 && this.userId > 0;
    }

    public int hashCode() {
        int a = (b.a(this.hoistRoleId) + (this.color * 31)) * 31;
        List<Long> list = this.roles;
        int i = 0;
        int hashCode = (a + (list != null ? list.hashCode() : 0)) * 31;
        String str = this.nick;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.premiumSince;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        boolean z2 = this.pending;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        int i4 = (hashCode3 + i2) * 31;
        UtcDateTime utcDateTime = this.joinedAt;
        int hashCode4 = utcDateTime != null ? utcDateTime.hashCode() : 0;
        int a2 = (b.a(this.userId) + ((b.a(this.guildId) + ((i4 + hashCode4) * 31)) * 31)) * 31;
        String str3 = this.avatarHash;
        int hashCode5 = (a2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.bannerHash;
        int hashCode6 = (hashCode5 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.bio;
        int hashCode7 = (hashCode6 + (str5 != null ? str5.hashCode() : 0)) * 31;
        UtcDateTime utcDateTime2 = this.communicationDisabledUntil;
        if (utcDateTime2 != null) {
            i = utcDateTime2.hashCode();
        }
        return hashCode7 + i;
    }

    public final boolean isCommunicationDisabled() {
        UtcDateTime utcDateTime = this.communicationDisabledUntil;
        return utcDateTime != null && utcDateTime.g() > ClockFactory.get().currentTimeMillis();
    }

    public String toString() {
        StringBuilder R = a.R("GuildMember(color=");
        R.append(this.color);
        R.append(", hoistRoleId=");
        R.append(this.hoistRoleId);
        R.append(", roles=");
        R.append(this.roles);
        R.append(", nick=");
        R.append(this.nick);
        R.append(", premiumSince=");
        R.append(this.premiumSince);
        R.append(", pending=");
        R.append(this.pending);
        R.append(", joinedAt=");
        R.append(this.joinedAt);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", userId=");
        R.append(this.userId);
        R.append(", avatarHash=");
        R.append(this.avatarHash);
        R.append(", bannerHash=");
        R.append(this.bannerHash);
        R.append(", bio=");
        R.append(this.bio);
        R.append(", communicationDisabledUntil=");
        R.append(this.communicationDisabledUntil);
        R.append(")");
        return R.toString();
    }

    public /* synthetic */ GuildMember(int i, long j, List list, String str, String str2, boolean z2, UtcDateTime utcDateTime, long j2, long j3, String str3, String str4, String str5, UtcDateTime utcDateTime2, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(i, j, list, str, str2, z2, utcDateTime, j2, j3, (i2 & 512) != 0 ? null : str3, (i2 & 1024) != 0 ? null : str4, (i2 & 2048) != 0 ? null : str5, (i2 & 4096) != 0 ? null : utcDateTime2);
    }

    public GuildMember() {
        this(0, 0L, null, null, null, false, null, 0L, 0L, null, null, null, null);
    }
}
