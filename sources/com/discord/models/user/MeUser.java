package com.discord.models.user;

import a0.a.a.b;
import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.api.premium.PremiumTier;
import com.discord.api.user.NsfwAllowance;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: MeUser.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u001a\n\u0002\u0010\u0000\n\u0002\b\u001c\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000 W2\u00020\u0001:\u0001WBË\u0001\u0012\n\u0010!\u001a\u00060\u0002j\u0002`\u0003\u0012\b\b\u0002\u0010\"\u001a\u00020\u0006\u0012\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010$\u001a\u0004\u0018\u00010\u0006\u0012\b\b\u0002\u0010%\u001a\u00020\u000b\u0012\b\b\u0002\u0010&\u001a\u00020\u000b\u0012\b\b\u0002\u0010'\u001a\u00020\u000f\u0012\b\b\u0002\u0010(\u001a\u00020\u0012\u0012\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u0006\u0012\b\b\u0002\u0010*\u001a\u00020\u000b\u0012\b\b\u0002\u0010+\u001a\u00020\u000b\u0012\n\b\u0002\u0010,\u001a\u0004\u0018\u00010\u0006\u0012\b\b\u0002\u0010-\u001a\u00020\u000f\u0012\b\b\u0002\u0010.\u001a\u00020\u000f\u0012\n\b\u0002\u0010/\u001a\u0004\u0018\u00010\u0006\u0012\b\b\u0002\u00100\u001a\u00020\u001c\u0012\n\b\u0002\u00101\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u00102\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\bR\u0010SB\u0011\b\u0016\u0012\u0006\u0010U\u001a\u00020T¢\u0006\u0004\bR\u0010VJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\t\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\t\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\n\u0010\bJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\u000e\u0010\rJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0012\u0010\u0015\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0015\u0010\bJ\u0010\u0010\u0016\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\u0016\u0010\rJ\u0010\u0010\u0017\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\u0017\u0010\rJ\u0012\u0010\u0018\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0018\u0010\bJ\u0010\u0010\u0019\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0019\u0010\u0011J\u0010\u0010\u001a\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u001a\u0010\u0011J\u0012\u0010\u001b\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u001b\u0010\bJ\u0010\u0010\u001d\u001a\u00020\u001cHÆ\u0003¢\u0006\u0004\b\u001d\u0010\u001eJ\u0012\u0010\u001f\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u001f\u0010\bJ\u0012\u0010 \u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b \u0010\bJÖ\u0001\u00103\u001a\u00020\u00002\f\b\u0002\u0010!\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\"\u001a\u00020\u00062\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010$\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010%\u001a\u00020\u000b2\b\b\u0002\u0010&\u001a\u00020\u000b2\b\b\u0002\u0010'\u001a\u00020\u000f2\b\b\u0002\u0010(\u001a\u00020\u00122\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010*\u001a\u00020\u000b2\b\b\u0002\u0010+\u001a\u00020\u000b2\n\b\u0002\u0010,\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010-\u001a\u00020\u000f2\b\b\u0002\u0010.\u001a\u00020\u000f2\n\b\u0002\u0010/\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u00100\u001a\u00020\u001c2\n\b\u0002\u00101\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u00102\u001a\u0004\u0018\u00010\u0006HÆ\u0001¢\u0006\u0004\b3\u00104J\u0010\u00105\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b5\u0010\bJ\u0010\u00106\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b6\u0010\u0011J\u001a\u00109\u001a\u00020\u000b2\b\u00108\u001a\u0004\u0018\u000107HÖ\u0003¢\u0006\u0004\b9\u0010:R\u001c\u0010.\u001a\u00020\u000f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b.\u0010;\u001a\u0004\b<\u0010\u0011R\u001b\u0010)\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010=\u001a\u0004\b>\u0010\bR\u001e\u0010$\u001a\u0004\u0018\u00010\u00068\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b$\u0010=\u001a\u0004\b?\u0010\bR\u001c\u0010(\u001a\u00020\u00128\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b(\u0010@\u001a\u0004\bA\u0010\u0014R\u0019\u0010+\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010B\u001a\u0004\b+\u0010\rR\u0019\u0010C\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\bC\u0010B\u001a\u0004\bD\u0010\rR \u0010!\u001a\u00060\u0002j\u0002`\u00038\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b!\u0010E\u001a\u0004\bF\u0010\u0005R\u001e\u0010#\u001a\u0004\u0018\u00010\u00068\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b#\u0010=\u001a\u0004\bG\u0010\bR\u001c\u0010&\u001a\u00020\u000b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b&\u0010B\u001a\u0004\b&\u0010\rR\u001c\u0010'\u001a\u00020\u000f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b'\u0010;\u001a\u0004\bH\u0010\u0011R\u0019\u0010*\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010B\u001a\u0004\bI\u0010\rR\u001c\u0010\"\u001a\u00020\u00068\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\"\u0010=\u001a\u0004\bJ\u0010\bR\u001b\u0010,\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010=\u001a\u0004\bK\u0010\bR\u001c\u0010-\u001a\u00020\u000f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b-\u0010;\u001a\u0004\bL\u0010\u0011R\u001c\u0010%\u001a\u00020\u000b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b%\u0010B\u001a\u0004\b%\u0010\rR\u001b\u0010/\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010=\u001a\u0004\bM\u0010\bR\u0019\u00100\u001a\u00020\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010N\u001a\u0004\bO\u0010\u001eR\u001e\u00101\u001a\u0004\u0018\u00010\u00068\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b1\u0010=\u001a\u0004\bP\u0010\bR\u001e\u00102\u001a\u0004\u0018\u00010\u00068\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b2\u0010=\u001a\u0004\bQ\u0010\b¨\u0006X"}, d2 = {"Lcom/discord/models/user/MeUser;", "Lcom/discord/models/user/User;", "", "Lcom/discord/primitives/UserId;", "component1", "()J", "", "component2", "()Ljava/lang/String;", "component3", "component4", "", "component5", "()Z", "component6", "", "component7", "()I", "Lcom/discord/api/premium/PremiumTier;", "component8", "()Lcom/discord/api/premium/PremiumTier;", "component9", "component10", "component11", "component12", "component13", "component14", "component15", "Lcom/discord/api/user/NsfwAllowance;", "component16", "()Lcom/discord/api/user/NsfwAllowance;", "component17", "component18", ModelAuditLogEntry.CHANGE_KEY_ID, "username", "avatar", "banner", "isBot", "isSystemUser", "discriminator", "premiumTier", NotificationCompat.CATEGORY_EMAIL, "mfaEnabled", "isVerified", "token", "flags", "publicFlags", "phoneNumber", "nsfwAllowance", "bio", "bannerColor", "copy", "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZILcom/discord/api/premium/PremiumTier;Ljava/lang/String;ZZLjava/lang/String;IILjava/lang/String;Lcom/discord/api/user/NsfwAllowance;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/models/user/MeUser;", "toString", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "I", "getPublicFlags", "Ljava/lang/String;", "getEmail", "getBanner", "Lcom/discord/api/premium/PremiumTier;", "getPremiumTier", "Z", "hasBirthday", "getHasBirthday", "J", "getId", "getAvatar", "getDiscriminator", "getMfaEnabled", "getUsername", "getToken", "getFlags", "getPhoneNumber", "Lcom/discord/api/user/NsfwAllowance;", "getNsfwAllowance", "getBio", "getBannerColor", HookHelper.constructorName, "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZILcom/discord/api/premium/PremiumTier;Ljava/lang/String;ZZLjava/lang/String;IILjava/lang/String;Lcom/discord/api/user/NsfwAllowance;Ljava/lang/String;Ljava/lang/String;)V", "Lcom/discord/api/user/User;", "user", "(Lcom/discord/api/user/User;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class MeUser implements User {
    public static final Companion Companion = new Companion(null);
    private final String avatar;
    private final String banner;
    private final String bannerColor;
    private final String bio;
    private final int discriminator;
    private final String email;
    private final int flags;
    private final boolean hasBirthday;

    /* renamed from: id  reason: collision with root package name */
    private final long f2726id;
    private final boolean isBot;
    private final boolean isSystemUser;
    private final boolean isVerified;
    private final boolean mfaEnabled;
    private final NsfwAllowance nsfwAllowance;
    private final String phoneNumber;
    private final PremiumTier premiumTier;
    private final int publicFlags;
    private final String token;
    private final String username;

    /* compiled from: MeUser.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tJ\u001d\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/models/user/MeUser$Companion;", "", "Lcom/discord/models/user/MeUser;", "oldUser", "Lcom/discord/api/user/User;", "newUser", "merge", "(Lcom/discord/models/user/MeUser;Lcom/discord/api/user/User;)Lcom/discord/models/user/MeUser;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX WARN: Removed duplicated region for block: B:66:0x0116  */
        /* JADX WARN: Removed duplicated region for block: B:69:0x0124  */
        /* JADX WARN: Removed duplicated region for block: B:71:0x012f  */
        /* JADX WARN: Removed duplicated region for block: B:78:0x0146  */
        /* JADX WARN: Removed duplicated region for block: B:83:0x0155  */
        /* JADX WARN: Removed duplicated region for block: B:86:0x0176  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public final com.discord.models.user.MeUser merge(com.discord.models.user.MeUser r24, com.discord.api.user.User r25) {
            /*
                Method dump skipped, instructions count: 404
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.models.user.MeUser.Companion.merge(com.discord.models.user.MeUser, com.discord.api.user.User):com.discord.models.user.MeUser");
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public MeUser(long j, String str, String str2, String str3, boolean z2, boolean z3, int i, PremiumTier premiumTier, String str4, boolean z4, boolean z5, String str5, int i2, int i3, String str6, NsfwAllowance nsfwAllowance, String str7, String str8) {
        m.checkNotNullParameter(str, "username");
        m.checkNotNullParameter(premiumTier, "premiumTier");
        m.checkNotNullParameter(nsfwAllowance, "nsfwAllowance");
        this.f2726id = j;
        this.username = str;
        this.avatar = str2;
        this.banner = str3;
        this.isBot = z2;
        this.isSystemUser = z3;
        this.discriminator = i;
        this.premiumTier = premiumTier;
        this.email = str4;
        this.mfaEnabled = z4;
        this.isVerified = z5;
        this.token = str5;
        this.flags = i2;
        this.publicFlags = i3;
        this.phoneNumber = str6;
        this.nsfwAllowance = nsfwAllowance;
        this.bio = str7;
        this.bannerColor = str8;
        this.hasBirthday = nsfwAllowance != NsfwAllowance.UNKNOWN;
    }

    public final long component1() {
        return getId();
    }

    public final boolean component10() {
        return this.mfaEnabled;
    }

    public final boolean component11() {
        return this.isVerified;
    }

    public final String component12() {
        return this.token;
    }

    public final int component13() {
        return getFlags();
    }

    public final int component14() {
        return getPublicFlags();
    }

    public final String component15() {
        return this.phoneNumber;
    }

    public final NsfwAllowance component16() {
        return this.nsfwAllowance;
    }

    public final String component17() {
        return getBio();
    }

    public final String component18() {
        return getBannerColor();
    }

    public final String component2() {
        return getUsername();
    }

    public final String component3() {
        return getAvatar();
    }

    public final String component4() {
        return getBanner();
    }

    public final boolean component5() {
        return isBot();
    }

    public final boolean component6() {
        return isSystemUser();
    }

    public final int component7() {
        return getDiscriminator();
    }

    public final PremiumTier component8() {
        return getPremiumTier();
    }

    public final String component9() {
        return this.email;
    }

    public final MeUser copy(long j, String str, String str2, String str3, boolean z2, boolean z3, int i, PremiumTier premiumTier, String str4, boolean z4, boolean z5, String str5, int i2, int i3, String str6, NsfwAllowance nsfwAllowance, String str7, String str8) {
        m.checkNotNullParameter(str, "username");
        m.checkNotNullParameter(premiumTier, "premiumTier");
        m.checkNotNullParameter(nsfwAllowance, "nsfwAllowance");
        return new MeUser(j, str, str2, str3, z2, z3, i, premiumTier, str4, z4, z5, str5, i2, i3, str6, nsfwAllowance, str7, str8);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MeUser)) {
            return false;
        }
        MeUser meUser = (MeUser) obj;
        return getId() == meUser.getId() && m.areEqual(getUsername(), meUser.getUsername()) && m.areEqual(getAvatar(), meUser.getAvatar()) && m.areEqual(getBanner(), meUser.getBanner()) && isBot() == meUser.isBot() && isSystemUser() == meUser.isSystemUser() && getDiscriminator() == meUser.getDiscriminator() && m.areEqual(getPremiumTier(), meUser.getPremiumTier()) && m.areEqual(this.email, meUser.email) && this.mfaEnabled == meUser.mfaEnabled && this.isVerified == meUser.isVerified && m.areEqual(this.token, meUser.token) && getFlags() == meUser.getFlags() && getPublicFlags() == meUser.getPublicFlags() && m.areEqual(this.phoneNumber, meUser.phoneNumber) && m.areEqual(this.nsfwAllowance, meUser.nsfwAllowance) && m.areEqual(getBio(), meUser.getBio()) && m.areEqual(getBannerColor(), meUser.getBannerColor());
    }

    @Override // com.discord.models.user.User
    public String getAvatar() {
        return this.avatar;
    }

    @Override // com.discord.models.user.User
    public String getBanner() {
        return this.banner;
    }

    @Override // com.discord.models.user.User
    public String getBannerColor() {
        return this.bannerColor;
    }

    @Override // com.discord.models.user.User
    public String getBio() {
        return this.bio;
    }

    @Override // com.discord.models.user.User
    public int getDiscriminator() {
        return this.discriminator;
    }

    public final String getEmail() {
        return this.email;
    }

    @Override // com.discord.models.user.User
    public int getFlags() {
        return this.flags;
    }

    public final boolean getHasBirthday() {
        return this.hasBirthday;
    }

    @Override // com.discord.models.user.User
    public long getId() {
        return this.f2726id;
    }

    public final boolean getMfaEnabled() {
        return this.mfaEnabled;
    }

    public final NsfwAllowance getNsfwAllowance() {
        return this.nsfwAllowance;
    }

    public final String getPhoneNumber() {
        return this.phoneNumber;
    }

    @Override // com.discord.models.user.User
    public PremiumTier getPremiumTier() {
        return this.premiumTier;
    }

    @Override // com.discord.models.user.User
    public int getPublicFlags() {
        return this.publicFlags;
    }

    public final String getToken() {
        return this.token;
    }

    @Override // com.discord.models.user.User
    public String getUsername() {
        return this.username;
    }

    public int hashCode() {
        int a = b.a(getId()) * 31;
        String username = getUsername();
        int i = 0;
        int hashCode = (a + (username != null ? username.hashCode() : 0)) * 31;
        String avatar = getAvatar();
        int hashCode2 = (hashCode + (avatar != null ? avatar.hashCode() : 0)) * 31;
        String banner = getBanner();
        int hashCode3 = (hashCode2 + (banner != null ? banner.hashCode() : 0)) * 31;
        boolean isBot = isBot();
        int i2 = 1;
        if (isBot) {
            isBot = true;
        }
        int i3 = isBot ? 1 : 0;
        int i4 = isBot ? 1 : 0;
        int i5 = (hashCode3 + i3) * 31;
        boolean isSystemUser = isSystemUser();
        if (isSystemUser) {
            isSystemUser = true;
        }
        int i6 = isSystemUser ? 1 : 0;
        int i7 = isSystemUser ? 1 : 0;
        int discriminator = (getDiscriminator() + ((i5 + i6) * 31)) * 31;
        PremiumTier premiumTier = getPremiumTier();
        int hashCode4 = (discriminator + (premiumTier != null ? premiumTier.hashCode() : 0)) * 31;
        String str = this.email;
        int hashCode5 = (hashCode4 + (str != null ? str.hashCode() : 0)) * 31;
        boolean z2 = this.mfaEnabled;
        if (z2) {
            z2 = true;
        }
        int i8 = z2 ? 1 : 0;
        int i9 = z2 ? 1 : 0;
        int i10 = (hashCode5 + i8) * 31;
        boolean z3 = this.isVerified;
        if (!z3) {
            i2 = z3 ? 1 : 0;
        }
        int i11 = (i10 + i2) * 31;
        String str2 = this.token;
        int hashCode6 = str2 != null ? str2.hashCode() : 0;
        int publicFlags = (getPublicFlags() + ((getFlags() + ((i11 + hashCode6) * 31)) * 31)) * 31;
        String str3 = this.phoneNumber;
        int hashCode7 = (publicFlags + (str3 != null ? str3.hashCode() : 0)) * 31;
        NsfwAllowance nsfwAllowance = this.nsfwAllowance;
        int hashCode8 = (hashCode7 + (nsfwAllowance != null ? nsfwAllowance.hashCode() : 0)) * 31;
        String bio = getBio();
        int hashCode9 = (hashCode8 + (bio != null ? bio.hashCode() : 0)) * 31;
        String bannerColor = getBannerColor();
        if (bannerColor != null) {
            i = bannerColor.hashCode();
        }
        return hashCode9 + i;
    }

    @Override // com.discord.models.user.User
    public boolean isBot() {
        return this.isBot;
    }

    @Override // com.discord.models.user.User
    public boolean isSystemUser() {
        return this.isSystemUser;
    }

    public final boolean isVerified() {
        return this.isVerified;
    }

    public String toString() {
        StringBuilder R = a.R("MeUser(id=");
        R.append(getId());
        R.append(", username=");
        R.append(getUsername());
        R.append(", avatar=");
        R.append(getAvatar());
        R.append(", banner=");
        R.append(getBanner());
        R.append(", isBot=");
        R.append(isBot());
        R.append(", isSystemUser=");
        R.append(isSystemUser());
        R.append(", discriminator=");
        R.append(getDiscriminator());
        R.append(", premiumTier=");
        R.append(getPremiumTier());
        R.append(", email=");
        R.append(this.email);
        R.append(", mfaEnabled=");
        R.append(this.mfaEnabled);
        R.append(", isVerified=");
        R.append(this.isVerified);
        R.append(", token=");
        R.append(this.token);
        R.append(", flags=");
        R.append(getFlags());
        R.append(", publicFlags=");
        R.append(getPublicFlags());
        R.append(", phoneNumber=");
        R.append(this.phoneNumber);
        R.append(", nsfwAllowance=");
        R.append(this.nsfwAllowance);
        R.append(", bio=");
        R.append(getBio());
        R.append(", bannerColor=");
        R.append(getBannerColor());
        R.append(")");
        return R.toString();
    }

    public /* synthetic */ MeUser(long j, String str, String str2, String str3, boolean z2, boolean z3, int i, PremiumTier premiumTier, String str4, boolean z4, boolean z5, String str5, int i2, int i3, String str6, NsfwAllowance nsfwAllowance, String str7, String str8, int i4, DefaultConstructorMarker defaultConstructorMarker) {
        this(j, (i4 & 2) != 0 ? "" : str, (i4 & 4) != 0 ? null : str2, (i4 & 8) != 0 ? null : str3, (i4 & 16) != 0 ? false : z2, (i4 & 32) != 0 ? false : z3, (i4 & 64) != 0 ? 0 : i, (i4 & 128) != 0 ? PremiumTier.NONE : premiumTier, (i4 & 256) != 0 ? null : str4, (i4 & 512) != 0 ? false : z4, (i4 & 1024) != 0 ? false : z5, (i4 & 2048) != 0 ? null : str5, (i4 & 4096) != 0 ? 0 : i2, (i4 & 8192) != 0 ? 0 : i3, (i4 & 16384) != 0 ? null : str6, (32768 & i4) != 0 ? NsfwAllowance.UNKNOWN : nsfwAllowance, (65536 & i4) != 0 ? null : str7, (i4 & 131072) != 0 ? null : str8);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public MeUser(com.discord.api.user.User r24) {
        /*
            Method dump skipped, instructions count: 272
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.models.user.MeUser.<init>(com.discord.api.user.User):void");
    }
}
