package com.discord.models.user;

import com.discord.api.premium.PremiumTier;
import com.discord.models.domain.ModelAuditLogEntry;
import java.io.Serializable;
import kotlin.Metadata;
/* compiled from: User.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u000b\bf\u0018\u00002\u00020\u0001R\u0018\u0010\u0005\u001a\u0004\u0018\u00010\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004R\u0018\u0010\u0007\u001a\u0004\u0018\u00010\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0004R\u0016\u0010\u000b\u001a\u00020\b8&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\nR\u0016\u0010\r\u001a\u00020\f8&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u000eR\u0018\u0010\u0010\u001a\u0004\u0018\u00010\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0004R\u0016\u0010\u0012\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u0004R\u0016\u0010\u0016\u001a\u00020\u00138&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0015R\u001a\u0010\u001b\u001a\u00060\u0017j\u0002`\u00188&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001d\u001a\u00020\u00138&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u001c\u0010\u0015R\u0016\u0010\u001f\u001a\u00020\u00138&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u001e\u0010\u0015R\u0016\u0010 \u001a\u00020\f8&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b \u0010\u000eR\u0018\u0010\"\u001a\u0004\u0018\u00010\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b!\u0010\u0004¨\u0006#"}, d2 = {"Lcom/discord/models/user/User;", "Ljava/io/Serializable;", "", "getBannerColor", "()Ljava/lang/String;", "bannerColor", "getBanner", "banner", "Lcom/discord/api/premium/PremiumTier;", "getPremiumTier", "()Lcom/discord/api/premium/PremiumTier;", "premiumTier", "", "isBot", "()Z", "getBio", "bio", "getUsername", "username", "", "getFlags", "()I", "flags", "", "Lcom/discord/primitives/UserId;", "getId", "()J", ModelAuditLogEntry.CHANGE_KEY_ID, "getPublicFlags", "publicFlags", "getDiscriminator", "discriminator", "isSystemUser", "getAvatar", "avatar", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public interface User extends Serializable {
    String getAvatar();

    String getBanner();

    String getBannerColor();

    String getBio();

    int getDiscriminator();

    int getFlags();

    long getId();

    PremiumTier getPremiumTier();

    int getPublicFlags();

    String getUsername();

    boolean isBot();

    boolean isSystemUser();
}
