package com.discord.models.user;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.premium.PremiumTier;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: CoreUser.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0016\n\u0002\u0010\u0000\n\u0002\b\u0014\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000 A2\u00020\u0001:\u0001AB\u0089\u0001\u0012\n\u0010\u0019\u001a\u00060\u0002j\u0002`\u0003\u0012\b\b\u0002\u0010\u001a\u001a\u00020\u0006\u0012\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u0006\u0012\b\b\u0002\u0010\u001d\u001a\u00020\u000b\u0012\b\b\u0002\u0010\u001e\u001a\u00020\u000b\u0012\b\b\u0002\u0010\u001f\u001a\u00020\u000f\u0012\b\b\u0002\u0010 \u001a\u00020\u0012\u0012\b\b\u0002\u0010!\u001a\u00020\u000f\u0012\b\b\u0002\u0010\"\u001a\u00020\u000f\u0012\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010$\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\b<\u0010=B\u0011\b\u0016\u0012\u0006\u0010?\u001a\u00020>¢\u0006\u0004\b<\u0010@J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\t\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\t\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\n\u0010\bJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\u000e\u0010\rJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0011J\u0010\u0010\u0016\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0011J\u0012\u0010\u0017\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0017\u0010\bJ\u0012\u0010\u0018\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0018\u0010\bJ\u0094\u0001\u0010%\u001a\u00020\u00002\f\b\u0002\u0010\u0019\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u001a\u001a\u00020\u00062\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010\u001d\u001a\u00020\u000b2\b\b\u0002\u0010\u001e\u001a\u00020\u000b2\b\b\u0002\u0010\u001f\u001a\u00020\u000f2\b\b\u0002\u0010 \u001a\u00020\u00122\b\b\u0002\u0010!\u001a\u00020\u000f2\b\b\u0002\u0010\"\u001a\u00020\u000f2\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010$\u001a\u0004\u0018\u00010\u0006HÆ\u0001¢\u0006\u0004\b%\u0010&J\u0010\u0010'\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b'\u0010\bJ\u0010\u0010(\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b(\u0010\u0011J\u001a\u0010+\u001a\u00020\u000b2\b\u0010*\u001a\u0004\u0018\u00010)HÖ\u0003¢\u0006\u0004\b+\u0010,R \u0010\u0019\u001a\u00060\u0002j\u0002`\u00038\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0019\u0010-\u001a\u0004\b.\u0010\u0005R\u001c\u0010\u001a\u001a\u00020\u00068\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001a\u0010/\u001a\u0004\b0\u0010\bR\u001c\u0010 \u001a\u00020\u00128\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b \u00101\u001a\u0004\b2\u0010\u0014R\u001e\u0010#\u001a\u0004\u0018\u00010\u00068\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b#\u0010/\u001a\u0004\b3\u0010\bR\u001c\u0010!\u001a\u00020\u000f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b!\u00104\u001a\u0004\b5\u0010\u0011R\u001c\u0010\"\u001a\u00020\u000f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\"\u00104\u001a\u0004\b6\u0010\u0011R\u001e\u0010$\u001a\u0004\u0018\u00010\u00068\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b$\u0010/\u001a\u0004\b7\u0010\bR\u001c\u0010\u001d\u001a\u00020\u000b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001d\u00108\u001a\u0004\b\u001d\u0010\rR\u001e\u0010\u001b\u001a\u0004\u0018\u00010\u00068\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001b\u0010/\u001a\u0004\b9\u0010\bR\u001e\u0010\u001c\u001a\u0004\u0018\u00010\u00068\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001c\u0010/\u001a\u0004\b:\u0010\bR\u001c\u0010\u001e\u001a\u00020\u000b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001e\u00108\u001a\u0004\b\u001e\u0010\rR\u001c\u0010\u001f\u001a\u00020\u000f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001f\u00104\u001a\u0004\b;\u0010\u0011¨\u0006B"}, d2 = {"Lcom/discord/models/user/CoreUser;", "Lcom/discord/models/user/User;", "", "Lcom/discord/primitives/UserId;", "component1", "()J", "", "component2", "()Ljava/lang/String;", "component3", "component4", "", "component5", "()Z", "component6", "", "component7", "()I", "Lcom/discord/api/premium/PremiumTier;", "component8", "()Lcom/discord/api/premium/PremiumTier;", "component9", "component10", "component11", "component12", ModelAuditLogEntry.CHANGE_KEY_ID, "username", "avatar", "banner", "isBot", "isSystemUser", "discriminator", "premiumTier", "flags", "publicFlags", "bio", "bannerColor", "copy", "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZILcom/discord/api/premium/PremiumTier;IILjava/lang/String;Ljava/lang/String;)Lcom/discord/models/user/CoreUser;", "toString", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "J", "getId", "Ljava/lang/String;", "getUsername", "Lcom/discord/api/premium/PremiumTier;", "getPremiumTier", "getBio", "I", "getFlags", "getPublicFlags", "getBannerColor", "Z", "getAvatar", "getBanner", "getDiscriminator", HookHelper.constructorName, "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZILcom/discord/api/premium/PremiumTier;IILjava/lang/String;Ljava/lang/String;)V", "Lcom/discord/api/user/User;", "user", "(Lcom/discord/api/user/User;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class CoreUser implements User {
    public static final Companion Companion = new Companion(null);
    private final String avatar;
    private final String banner;
    private final String bannerColor;
    private final String bio;
    private final int discriminator;
    private final int flags;

    /* renamed from: id  reason: collision with root package name */
    private final long f2725id;
    private final boolean isBot;
    private final boolean isSystemUser;
    private final PremiumTier premiumTier;
    private final int publicFlags;
    private final String username;

    /* compiled from: CoreUser.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tJ\u001d\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/models/user/CoreUser$Companion;", "", "Lcom/discord/models/user/CoreUser;", "oldUser", "Lcom/discord/api/user/User;", "newUser", "merge", "(Lcom/discord/models/user/CoreUser;Lcom/discord/api/user/User;)Lcom/discord/models/user/CoreUser;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX WARN: Removed duplicated region for block: B:57:0x00e6  */
        /* JADX WARN: Removed duplicated region for block: B:60:0x00fd  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public final com.discord.models.user.CoreUser merge(com.discord.models.user.CoreUser r18, com.discord.api.user.User r19) {
            /*
                Method dump skipped, instructions count: 277
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.models.user.CoreUser.Companion.merge(com.discord.models.user.CoreUser, com.discord.api.user.User):com.discord.models.user.CoreUser");
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public CoreUser(long j, String str, String str2, String str3, boolean z2, boolean z3, int i, PremiumTier premiumTier, int i2, int i3, String str4, String str5) {
        m.checkNotNullParameter(str, "username");
        m.checkNotNullParameter(premiumTier, "premiumTier");
        this.f2725id = j;
        this.username = str;
        this.avatar = str2;
        this.banner = str3;
        this.isBot = z2;
        this.isSystemUser = z3;
        this.discriminator = i;
        this.premiumTier = premiumTier;
        this.flags = i2;
        this.publicFlags = i3;
        this.bio = str4;
        this.bannerColor = str5;
    }

    public final long component1() {
        return getId();
    }

    public final int component10() {
        return getPublicFlags();
    }

    public final String component11() {
        return getBio();
    }

    public final String component12() {
        return getBannerColor();
    }

    public final String component2() {
        return getUsername();
    }

    public final String component3() {
        return getAvatar();
    }

    public final String component4() {
        return getBanner();
    }

    public final boolean component5() {
        return isBot();
    }

    public final boolean component6() {
        return isSystemUser();
    }

    public final int component7() {
        return getDiscriminator();
    }

    public final PremiumTier component8() {
        return getPremiumTier();
    }

    public final int component9() {
        return getFlags();
    }

    public final CoreUser copy(long j, String str, String str2, String str3, boolean z2, boolean z3, int i, PremiumTier premiumTier, int i2, int i3, String str4, String str5) {
        m.checkNotNullParameter(str, "username");
        m.checkNotNullParameter(premiumTier, "premiumTier");
        return new CoreUser(j, str, str2, str3, z2, z3, i, premiumTier, i2, i3, str4, str5);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CoreUser)) {
            return false;
        }
        CoreUser coreUser = (CoreUser) obj;
        return getId() == coreUser.getId() && m.areEqual(getUsername(), coreUser.getUsername()) && m.areEqual(getAvatar(), coreUser.getAvatar()) && m.areEqual(getBanner(), coreUser.getBanner()) && isBot() == coreUser.isBot() && isSystemUser() == coreUser.isSystemUser() && getDiscriminator() == coreUser.getDiscriminator() && m.areEqual(getPremiumTier(), coreUser.getPremiumTier()) && getFlags() == coreUser.getFlags() && getPublicFlags() == coreUser.getPublicFlags() && m.areEqual(getBio(), coreUser.getBio()) && m.areEqual(getBannerColor(), coreUser.getBannerColor());
    }

    @Override // com.discord.models.user.User
    public String getAvatar() {
        return this.avatar;
    }

    @Override // com.discord.models.user.User
    public String getBanner() {
        return this.banner;
    }

    @Override // com.discord.models.user.User
    public String getBannerColor() {
        return this.bannerColor;
    }

    @Override // com.discord.models.user.User
    public String getBio() {
        return this.bio;
    }

    @Override // com.discord.models.user.User
    public int getDiscriminator() {
        return this.discriminator;
    }

    @Override // com.discord.models.user.User
    public int getFlags() {
        return this.flags;
    }

    @Override // com.discord.models.user.User
    public long getId() {
        return this.f2725id;
    }

    @Override // com.discord.models.user.User
    public PremiumTier getPremiumTier() {
        return this.premiumTier;
    }

    @Override // com.discord.models.user.User
    public int getPublicFlags() {
        return this.publicFlags;
    }

    @Override // com.discord.models.user.User
    public String getUsername() {
        return this.username;
    }

    public int hashCode() {
        int a = b.a(getId()) * 31;
        String username = getUsername();
        int i = 0;
        int hashCode = (a + (username != null ? username.hashCode() : 0)) * 31;
        String avatar = getAvatar();
        int hashCode2 = (hashCode + (avatar != null ? avatar.hashCode() : 0)) * 31;
        String banner = getBanner();
        int hashCode3 = (hashCode2 + (banner != null ? banner.hashCode() : 0)) * 31;
        boolean isBot = isBot();
        int i2 = 1;
        if (isBot) {
            isBot = true;
        }
        int i3 = isBot ? 1 : 0;
        int i4 = isBot ? 1 : 0;
        int i5 = (hashCode3 + i3) * 31;
        boolean isSystemUser = isSystemUser();
        if (!isSystemUser) {
            i2 = isSystemUser;
        }
        int discriminator = (getDiscriminator() + ((i5 + i2) * 31)) * 31;
        PremiumTier premiumTier = getPremiumTier();
        int hashCode4 = premiumTier != null ? premiumTier.hashCode() : 0;
        int publicFlags = (getPublicFlags() + ((getFlags() + ((discriminator + hashCode4) * 31)) * 31)) * 31;
        String bio = getBio();
        int hashCode5 = (publicFlags + (bio != null ? bio.hashCode() : 0)) * 31;
        String bannerColor = getBannerColor();
        if (bannerColor != null) {
            i = bannerColor.hashCode();
        }
        return hashCode5 + i;
    }

    @Override // com.discord.models.user.User
    public boolean isBot() {
        return this.isBot;
    }

    @Override // com.discord.models.user.User
    public boolean isSystemUser() {
        return this.isSystemUser;
    }

    public String toString() {
        StringBuilder R = a.R("CoreUser(id=");
        R.append(getId());
        R.append(", username=");
        R.append(getUsername());
        R.append(", avatar=");
        R.append(getAvatar());
        R.append(", banner=");
        R.append(getBanner());
        R.append(", isBot=");
        R.append(isBot());
        R.append(", isSystemUser=");
        R.append(isSystemUser());
        R.append(", discriminator=");
        R.append(getDiscriminator());
        R.append(", premiumTier=");
        R.append(getPremiumTier());
        R.append(", flags=");
        R.append(getFlags());
        R.append(", publicFlags=");
        R.append(getPublicFlags());
        R.append(", bio=");
        R.append(getBio());
        R.append(", bannerColor=");
        R.append(getBannerColor());
        R.append(")");
        return R.toString();
    }

    public /* synthetic */ CoreUser(long j, String str, String str2, String str3, boolean z2, boolean z3, int i, PremiumTier premiumTier, int i2, int i3, String str4, String str5, int i4, DefaultConstructorMarker defaultConstructorMarker) {
        this(j, (i4 & 2) != 0 ? "" : str, (i4 & 4) != 0 ? null : str2, (i4 & 8) != 0 ? null : str3, (i4 & 16) != 0 ? false : z2, (i4 & 32) != 0 ? false : z3, (i4 & 64) != 0 ? 0 : i, (i4 & 128) != 0 ? PremiumTier.NONE : premiumTier, (i4 & 256) != 0 ? 0 : i2, (i4 & 512) != 0 ? 0 : i3, (i4 & 1024) != 0 ? null : str4, (i4 & 2048) != 0 ? null : str5);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public CoreUser(com.discord.api.user.User r17) {
        /*
            r16 = this;
            java.lang.String r0 = "user"
            r1 = r17
            d0.z.d.m.checkNotNullParameter(r1, r0)
            long r2 = r17.i()
            java.lang.String r4 = r17.r()
            com.discord.nullserializable.NullSerializable r0 = r17.a()
            r5 = 0
            if (r0 == 0) goto L1e
            java.lang.Object r0 = r0.a()
            java.lang.String r0 = (java.lang.String) r0
            goto L1f
        L1e:
            r0 = r5
        L1f:
            com.discord.nullserializable.NullSerializable r6 = r17.b()
            if (r6 == 0) goto L2c
            java.lang.Object r6 = r6.a()
            java.lang.String r6 = (java.lang.String) r6
            goto L2d
        L2c:
            r6 = r5
        L2d:
            java.lang.Boolean r7 = r17.e()
            r8 = 0
            if (r7 == 0) goto L39
            boolean r7 = r7.booleanValue()
            goto L3a
        L39:
            r7 = 0
        L3a:
            java.lang.Boolean r9 = r17.p()
            if (r9 == 0) goto L45
            boolean r9 = r9.booleanValue()
            goto L46
        L45:
            r9 = 0
        L46:
            java.lang.String r10 = r17.f()
            java.lang.Integer r10 = d0.g0.s.toIntOrNull(r10)
            if (r10 == 0) goto L55
            int r10 = r10.intValue()
            goto L56
        L55:
            r10 = 0
        L56:
            com.discord.api.premium.PremiumTier r11 = r17.n()
            if (r11 == 0) goto L5d
            goto L5f
        L5d:
            com.discord.api.premium.PremiumTier r11 = com.discord.api.premium.PremiumTier.NONE
        L5f:
            java.lang.Integer r12 = r17.h()
            if (r12 == 0) goto L6a
            int r12 = r12.intValue()
            goto L6b
        L6a:
            r12 = 0
        L6b:
            java.lang.Integer r13 = r17.o()
            if (r13 == 0) goto L77
            int r8 = r13.intValue()
            r13 = r8
            goto L78
        L77:
            r13 = 0
        L78:
            com.discord.nullserializable.NullSerializable r8 = r17.d()
            if (r8 == 0) goto L86
            java.lang.Object r8 = r8.a()
            java.lang.String r8 = (java.lang.String) r8
            r14 = r8
            goto L87
        L86:
            r14 = r5
        L87:
            com.discord.nullserializable.NullSerializable r1 = r17.c()
            if (r1 == 0) goto L95
            java.lang.Object r1 = r1.a()
            java.lang.String r1 = (java.lang.String) r1
            r15 = r1
            goto L96
        L95:
            r15 = r5
        L96:
            r1 = r16
            r5 = r0
            r8 = r9
            r9 = r10
            r10 = r11
            r11 = r12
            r12 = r13
            r13 = r14
            r14 = r15
            r1.<init>(r2, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.models.user.CoreUser.<init>(com.discord.api.user.User):void");
    }
}
