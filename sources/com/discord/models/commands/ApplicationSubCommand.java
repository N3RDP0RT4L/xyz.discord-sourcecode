package com.discord.models.commands;

import andhook.lib.HookHelper;
import com.discord.models.domain.ModelAuditLogEntry;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ApplicationCommand.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\n\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0095\u0001\u0012\u0006\u0010\t\u001a\u00020\u0001\u0012\u0006\u0010\u0007\u001a\u00020\u0002\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\n\u0010\u000f\u001a\u00060\rj\u0002`\u000e\u0012\u0006\u0010\u0010\u001a\u00020\u0002\u0012\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0012\u0012\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00150\u0014\u0012\u000e\u0010\u0018\u001a\n\u0018\u00010\rj\u0004\u0018\u0001`\u0017\u0012\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u0019\u0012\u0010\b\u0002\u0010\u001c\u001a\n\u0012\u0004\u0012\u00020\u001b\u0018\u00010\u0014\u0012\b\u0010\u001d\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u001e\u0010\u001fR\u001b\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0004\u001a\u0004\b\b\u0010\u0006R\u0019\u0010\t\u001a\u00020\u00018\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\f¨\u0006 "}, d2 = {"Lcom/discord/models/commands/ApplicationSubCommand;", "Lcom/discord/models/commands/ApplicationCommand;", "", "parentGroupName", "Ljava/lang/String;", "getParentGroupName", "()Ljava/lang/String;", "subCommandName", "getSubCommandName", "rootCommand", "Lcom/discord/models/commands/ApplicationCommand;", "getRootCommand", "()Lcom/discord/models/commands/ApplicationCommand;", "", "Lcom/discord/primitives/ApplicationId;", "applicationId", ModelAuditLogEntry.CHANGE_KEY_NAME, ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "", "descriptionRes", "", "Lcom/discord/models/commands/ApplicationCommandOption;", "options", "Lcom/discord/primitives/GuildId;", "guildId", "", "defaultPermissions", "Lcom/discord/api/commands/ApplicationCommandPermission;", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS, "version", HookHelper.constructorName, "(Lcom/discord/models/commands/ApplicationCommand;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ApplicationSubCommand extends ApplicationCommand {
    private final String parentGroupName;
    private final ApplicationCommand rootCommand;
    private final String subCommandName;

    public /* synthetic */ ApplicationSubCommand(ApplicationCommand applicationCommand, String str, String str2, long j, String str3, String str4, Integer num, List list, Long l, Boolean bool, List list2, String str5, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(applicationCommand, str, (i & 4) != 0 ? null : str2, j, str3, (i & 32) != 0 ? null : str4, (i & 64) != 0 ? null : num, list, l, (i & 512) != 0 ? null : bool, (i & 1024) != 0 ? null : list2, str5);
    }

    public final String getParentGroupName() {
        return this.parentGroupName;
    }

    public final ApplicationCommand getRootCommand() {
        return this.rootCommand;
    }

    public final String getSubCommandName() {
        return this.subCommandName;
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public ApplicationSubCommand(com.discord.models.commands.ApplicationCommand r21, java.lang.String r22, java.lang.String r23, long r24, java.lang.String r26, java.lang.String r27, java.lang.Integer r28, java.util.List<com.discord.models.commands.ApplicationCommandOption> r29, java.lang.Long r30, java.lang.Boolean r31, java.util.List<com.discord.api.commands.ApplicationCommandPermission> r32, java.lang.String r33) {
        /*
            r20 = this;
            r15 = r20
            r14 = r21
            r13 = r22
            r12 = r23
            r0 = r32
            java.lang.String r1 = "rootCommand"
            d0.z.d.m.checkNotNullParameter(r14, r1)
            java.lang.String r1 = "subCommandName"
            d0.z.d.m.checkNotNullParameter(r13, r1)
            java.lang.String r1 = "name"
            r4 = r26
            d0.z.d.m.checkNotNullParameter(r4, r1)
            java.lang.String r1 = "options"
            r7 = r29
            d0.z.d.m.checkNotNullParameter(r7, r1)
            r1 = 32
            if (r12 == 0) goto L47
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = r21.getId()
            r2.append(r3)
            r2.append(r1)
            r2.append(r12)
            r2.append(r1)
            r2.append(r13)
            java.lang.String r1 = r2.toString()
            goto L5d
        L47:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = r21.getId()
            r2.append(r3)
            r2.append(r1)
            r2.append(r13)
            java.lang.String r1 = r2.toString()
        L5d:
            r8 = 0
            if (r0 == 0) goto L98
            java.util.ArrayList r2 = new java.util.ArrayList
            r3 = 10
            int r3 = d0.t.o.collectionSizeOrDefault(r0, r3)
            r2.<init>(r3)
            java.util.Iterator r0 = r32.iterator()
        L6f:
            boolean r3 = r0.hasNext()
            if (r3 == 0) goto L93
            java.lang.Object r3 = r0.next()
            com.discord.api.commands.ApplicationCommandPermission r3 = (com.discord.api.commands.ApplicationCommandPermission) r3
            long r5 = r3.a()
            java.lang.Long r5 = java.lang.Long.valueOf(r5)
            boolean r3 = r3.b()
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r3)
            kotlin.Pair r3 = d0.o.to(r5, r3)
            r2.add(r3)
            goto L6f
        L93:
            java.util.Map r0 = d0.t.h0.toMap(r2)
            goto L99
        L98:
            r0 = 0
        L99:
            r16 = r0
            r17 = 0
            r18 = 2112(0x840, float:2.96E-42)
            r19 = 0
            r0 = r20
            r2 = r24
            r4 = r26
            r5 = r27
            r6 = r28
            r7 = r29
            r9 = r30
            r10 = r33
            r11 = r31
            r12 = r16
            r13 = r17
            r14 = r18
            r15 = r19
            r0.<init>(r1, r2, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)
            r1 = r21
            r0.rootCommand = r1
            r1 = r22
            r0.subCommandName = r1
            r1 = r23
            r0.parentGroupName = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.models.commands.ApplicationSubCommand.<init>(com.discord.models.commands.ApplicationCommand, java.lang.String, java.lang.String, long, java.lang.String, java.lang.String, java.lang.Integer, java.util.List, java.lang.Long, java.lang.Boolean, java.util.List, java.lang.String):void");
    }
}
