package com.discord.models.commands;

import andhook.lib.HookHelper;
import com.discord.models.domain.ModelAuditLogEntry;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ApplicationCommand.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\u0000\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u009b\u0001\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0002\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0002\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\t\u0012\u000e\u0010\r\u001a\n\u0018\u00010\u0004j\u0004\u0018\u0001`\f\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u000f\u0012\u0010\b\u0002\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u0011\u0018\u00010\t\u0012$\b\u0002\u0010\u0016\u001a\u001e\u0012\u0012\u0012\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0006\u0012\u0004\u0018\u00010\u00150\u0014\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u0013¢\u0006\u0004\b\u0017\u0010\u0018¨\u0006\u0019"}, d2 = {"Lcom/discord/models/commands/RemoteApplicationCommand;", "Lcom/discord/models/commands/ApplicationCommand;", "", ModelAuditLogEntry.CHANGE_KEY_ID, "", "Lcom/discord/primitives/ApplicationId;", "applicationId", ModelAuditLogEntry.CHANGE_KEY_NAME, ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "", "Lcom/discord/models/commands/ApplicationCommandOption;", "options", "Lcom/discord/primitives/GuildId;", "guildId", "version", "", "defaultPermissions", "Lcom/discord/api/commands/ApplicationCommandPermission;", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS, "Lkotlin/Function1;", "", "", "execute", HookHelper.constructorName, "(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Lkotlin/jvm/functions/Function1;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class RemoteApplicationCommand extends ApplicationCommand {
    public /* synthetic */ RemoteApplicationCommand(String str, long j, String str2, String str3, List list, Long l, String str4, Boolean bool, List list2, Function1 function1, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(str, j, str2, (i & 8) != 0 ? null : str3, list, l, str4, (i & 128) != 0 ? null : bool, (i & 256) != 0 ? null : list2, (i & 512) != 0 ? null : function1);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public RemoteApplicationCommand(java.lang.String r19, long r20, java.lang.String r22, java.lang.String r23, java.util.List<com.discord.models.commands.ApplicationCommandOption> r24, java.lang.Long r25, java.lang.String r26, java.lang.Boolean r27, java.util.List<com.discord.api.commands.ApplicationCommandPermission> r28, kotlin.jvm.functions.Function1<? super java.util.Map<java.lang.String, ? extends java.lang.Object>, java.lang.String> r29) {
        /*
            r18 = this;
            r0 = r28
            java.lang.String r1 = "id"
            r3 = r19
            d0.z.d.m.checkNotNullParameter(r3, r1)
            java.lang.String r1 = "name"
            r6 = r22
            d0.z.d.m.checkNotNullParameter(r6, r1)
            java.lang.String r1 = "options"
            r9 = r24
            d0.z.d.m.checkNotNullParameter(r9, r1)
            if (r0 == 0) goto L53
            java.util.ArrayList r1 = new java.util.ArrayList
            r2 = 10
            int r2 = d0.t.o.collectionSizeOrDefault(r0, r2)
            r1.<init>(r2)
            java.util.Iterator r0 = r28.iterator()
        L2a:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L4e
            java.lang.Object r2 = r0.next()
            com.discord.api.commands.ApplicationCommandPermission r2 = (com.discord.api.commands.ApplicationCommandPermission) r2
            long r4 = r2.a()
            java.lang.Long r4 = java.lang.Long.valueOf(r4)
            boolean r2 = r2.b()
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            kotlin.Pair r2 = d0.o.to(r4, r2)
            r1.add(r2)
            goto L2a
        L4e:
            java.util.Map r0 = d0.t.h0.toMap(r1)
            goto L54
        L53:
            r0 = 0
        L54:
            r14 = r0
            r16 = 80
            r17 = 0
            r8 = 0
            r10 = 0
            r2 = r18
            r3 = r19
            r4 = r20
            r6 = r22
            r7 = r23
            r9 = r24
            r11 = r25
            r12 = r26
            r13 = r27
            r15 = r29
            r2.<init>(r3, r4, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.models.commands.RemoteApplicationCommand.<init>(java.lang.String, long, java.lang.String, java.lang.String, java.util.List, java.lang.Long, java.lang.String, java.lang.Boolean, java.util.List, kotlin.jvm.functions.Function1):void");
    }
}
