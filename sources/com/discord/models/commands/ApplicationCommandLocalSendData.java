package com.discord.models.commands;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.commands.ApplicationCommandData;
import com.discord.restapi.RestAPIParams;
import com.discord.widgets.chat.input.models.ApplicationCommandValue;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ApplicationCommandLocalSendData.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0017\b\u0086\b\u0018\u00002\u00020\u0001B\u0081\u0001\u0012\u0006\u0010\u001f\u001a\u00020\b\u0012\n\u0010 \u001a\u00060\bj\u0002`\u000b\u0012\u000e\u0010!\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\r\u0012\u000e\u0010\"\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\r\u0012\u0006\u0010#\u001a\u00020\u0011\u0012\b\u0010$\u001a\u0004\u0018\u00010\u0014\u0012\u0006\u0010%\u001a\u00020\u0014\u0012\u0006\u0010&\u001a\u00020\u0014\u0012\f\u0010'\u001a\b\u0012\u0004\u0012\u00020\u001a0\u0019\u0012\b\u0010(\u001a\u0004\u0018\u00010\u0014\u0012\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\b¢\u0006\u0004\bF\u0010GJ\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\r\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0014\u0010\f\u001a\u00060\bj\u0002`\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\nJ\u0018\u0010\u000e\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0018\u0010\u0010\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\rHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u000fJ\u0010\u0010\u0012\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u0012\u0010\u0015\u001a\u0004\u0018\u00010\u0014HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\u0014HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0016J\u0010\u0010\u0018\u001a\u00020\u0014HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0016J\u0016\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001a0\u0019HÆ\u0003¢\u0006\u0004\b\u001b\u0010\u001cJ\u0012\u0010\u001d\u001a\u0004\u0018\u00010\u0014HÆ\u0003¢\u0006\u0004\b\u001d\u0010\u0016J\u0012\u0010\u001e\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\u001e\u0010\u000fJ\u009e\u0001\u0010*\u001a\u00020\u00002\b\b\u0002\u0010\u001f\u001a\u00020\b2\f\b\u0002\u0010 \u001a\u00060\bj\u0002`\u000b2\u0010\b\u0002\u0010!\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\r2\u0010\b\u0002\u0010\"\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\r2\b\b\u0002\u0010#\u001a\u00020\u00112\n\b\u0002\u0010$\u001a\u0004\u0018\u00010\u00142\b\b\u0002\u0010%\u001a\u00020\u00142\b\b\u0002\u0010&\u001a\u00020\u00142\u000e\b\u0002\u0010'\u001a\b\u0012\u0004\u0012\u00020\u001a0\u00192\n\b\u0002\u0010(\u001a\u0004\u0018\u00010\u00142\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\bHÆ\u0001¢\u0006\u0004\b*\u0010+J\u0010\u0010,\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b,\u0010\u0016J\u0010\u0010.\u001a\u00020-HÖ\u0001¢\u0006\u0004\b.\u0010/J\u001a\u00102\u001a\u0002012\b\u00100\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b2\u00103R\u0019\u0010#\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b#\u00104\u001a\u0004\b5\u0010\u0013R\u001b\u0010(\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b(\u00106\u001a\u0004\b7\u0010\u0016R\u0019\u0010&\u001a\u00020\u00148\u0006@\u0006¢\u0006\f\n\u0004\b&\u00106\u001a\u0004\b8\u0010\u0016R!\u0010!\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\r8\u0006@\u0006¢\u0006\f\n\u0004\b!\u00109\u001a\u0004\b:\u0010\u000fR\u0019\u0010;\u001a\u00020\u00148\u0006@\u0006¢\u0006\f\n\u0004\b;\u00106\u001a\u0004\b<\u0010\u0016R!\u0010\"\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\r8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00109\u001a\u0004\b=\u0010\u000fR\u001b\u0010)\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b)\u00109\u001a\u0004\b>\u0010\u000fR\u001d\u0010 \u001a\u00060\bj\u0002`\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010?\u001a\u0004\b@\u0010\nR\u0019\u0010\u001f\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010?\u001a\u0004\bA\u0010\nR\u0019\u0010%\u001a\u00020\u00148\u0006@\u0006¢\u0006\f\n\u0004\b%\u00106\u001a\u0004\bB\u0010\u0016R\u001b\u0010$\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b$\u00106\u001a\u0004\bC\u0010\u0016R\u001f\u0010'\u001a\b\u0012\u0004\u0012\u00020\u001a0\u00198\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010D\u001a\u0004\bE\u0010\u001c¨\u0006H"}, d2 = {"Lcom/discord/models/commands/ApplicationCommandLocalSendData;", "", "Lcom/discord/restapi/RestAPIParams$ApplicationCommand;", "toRestApiParam", "()Lcom/discord/restapi/RestAPIParams$ApplicationCommand;", "Lcom/discord/api/commands/ApplicationCommandData;", "toRestApiApplicationCommandDataParam", "()Lcom/discord/api/commands/ApplicationCommandData;", "", "component1", "()J", "Lcom/discord/primitives/ChannelId;", "component2", "Lcom/discord/primitives/GuildId;", "component3", "()Ljava/lang/Long;", "component4", "Lcom/discord/models/commands/Application;", "component5", "()Lcom/discord/models/commands/Application;", "", "component6", "()Ljava/lang/String;", "component7", "component8", "", "Lcom/discord/widgets/chat/input/models/ApplicationCommandValue;", "component9", "()Ljava/util/List;", "component10", "component11", "nonce", "channelId", "guildId", "commandGuildId", "application", "sessionId", "applicationCommandName", "applicationCommandId", "applicationCommandsValues", "version", "interactionId", "copy", "(JJLjava/lang/Long;Ljava/lang/Long;Lcom/discord/models/commands/Application;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/Long;)Lcom/discord/models/commands/ApplicationCommandLocalSendData;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/commands/Application;", "getApplication", "Ljava/lang/String;", "getVersion", "getApplicationCommandId", "Ljava/lang/Long;", "getGuildId", "nonceString", "getNonceString", "getCommandGuildId", "getInteractionId", "J", "getChannelId", "getNonce", "getApplicationCommandName", "getSessionId", "Ljava/util/List;", "getApplicationCommandsValues", HookHelper.constructorName, "(JJLjava/lang/Long;Ljava/lang/Long;Lcom/discord/models/commands/Application;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/Long;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ApplicationCommandLocalSendData {
    private final Application application;
    private final String applicationCommandId;
    private final String applicationCommandName;
    private final List<ApplicationCommandValue> applicationCommandsValues;
    private final long channelId;
    private final Long commandGuildId;
    private final Long guildId;
    private final Long interactionId;
    private final long nonce;
    private final String nonceString;
    private final String sessionId;
    private final String version;

    public ApplicationCommandLocalSendData(long j, long j2, Long l, Long l2, Application application, String str, String str2, String str3, List<ApplicationCommandValue> list, String str4, Long l3) {
        m.checkNotNullParameter(application, "application");
        m.checkNotNullParameter(str2, "applicationCommandName");
        m.checkNotNullParameter(str3, "applicationCommandId");
        m.checkNotNullParameter(list, "applicationCommandsValues");
        this.nonce = j;
        this.channelId = j2;
        this.guildId = l;
        this.commandGuildId = l2;
        this.application = application;
        this.sessionId = str;
        this.applicationCommandName = str2;
        this.applicationCommandId = str3;
        this.applicationCommandsValues = list;
        this.version = str4;
        this.interactionId = l3;
        this.nonceString = String.valueOf(j);
    }

    public final long component1() {
        return this.nonce;
    }

    public final String component10() {
        return this.version;
    }

    public final Long component11() {
        return this.interactionId;
    }

    public final long component2() {
        return this.channelId;
    }

    public final Long component3() {
        return this.guildId;
    }

    public final Long component4() {
        return this.commandGuildId;
    }

    public final Application component5() {
        return this.application;
    }

    public final String component6() {
        return this.sessionId;
    }

    public final String component7() {
        return this.applicationCommandName;
    }

    public final String component8() {
        return this.applicationCommandId;
    }

    public final List<ApplicationCommandValue> component9() {
        return this.applicationCommandsValues;
    }

    public final ApplicationCommandLocalSendData copy(long j, long j2, Long l, Long l2, Application application, String str, String str2, String str3, List<ApplicationCommandValue> list, String str4, Long l3) {
        m.checkNotNullParameter(application, "application");
        m.checkNotNullParameter(str2, "applicationCommandName");
        m.checkNotNullParameter(str3, "applicationCommandId");
        m.checkNotNullParameter(list, "applicationCommandsValues");
        return new ApplicationCommandLocalSendData(j, j2, l, l2, application, str, str2, str3, list, str4, l3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ApplicationCommandLocalSendData)) {
            return false;
        }
        ApplicationCommandLocalSendData applicationCommandLocalSendData = (ApplicationCommandLocalSendData) obj;
        return this.nonce == applicationCommandLocalSendData.nonce && this.channelId == applicationCommandLocalSendData.channelId && m.areEqual(this.guildId, applicationCommandLocalSendData.guildId) && m.areEqual(this.commandGuildId, applicationCommandLocalSendData.commandGuildId) && m.areEqual(this.application, applicationCommandLocalSendData.application) && m.areEqual(this.sessionId, applicationCommandLocalSendData.sessionId) && m.areEqual(this.applicationCommandName, applicationCommandLocalSendData.applicationCommandName) && m.areEqual(this.applicationCommandId, applicationCommandLocalSendData.applicationCommandId) && m.areEqual(this.applicationCommandsValues, applicationCommandLocalSendData.applicationCommandsValues) && m.areEqual(this.version, applicationCommandLocalSendData.version) && m.areEqual(this.interactionId, applicationCommandLocalSendData.interactionId);
    }

    public final Application getApplication() {
        return this.application;
    }

    public final String getApplicationCommandId() {
        return this.applicationCommandId;
    }

    public final String getApplicationCommandName() {
        return this.applicationCommandName;
    }

    public final List<ApplicationCommandValue> getApplicationCommandsValues() {
        return this.applicationCommandsValues;
    }

    public final long getChannelId() {
        return this.channelId;
    }

    public final Long getCommandGuildId() {
        return this.commandGuildId;
    }

    public final Long getGuildId() {
        return this.guildId;
    }

    public final Long getInteractionId() {
        return this.interactionId;
    }

    public final long getNonce() {
        return this.nonce;
    }

    public final String getNonceString() {
        return this.nonceString;
    }

    public final String getSessionId() {
        return this.sessionId;
    }

    public final String getVersion() {
        return this.version;
    }

    public int hashCode() {
        int a = (b.a(this.channelId) + (b.a(this.nonce) * 31)) * 31;
        Long l = this.guildId;
        int i = 0;
        int hashCode = (a + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.commandGuildId;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Application application = this.application;
        int hashCode3 = (hashCode2 + (application != null ? application.hashCode() : 0)) * 31;
        String str = this.sessionId;
        int hashCode4 = (hashCode3 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.applicationCommandName;
        int hashCode5 = (hashCode4 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.applicationCommandId;
        int hashCode6 = (hashCode5 + (str3 != null ? str3.hashCode() : 0)) * 31;
        List<ApplicationCommandValue> list = this.applicationCommandsValues;
        int hashCode7 = (hashCode6 + (list != null ? list.hashCode() : 0)) * 31;
        String str4 = this.version;
        int hashCode8 = (hashCode7 + (str4 != null ? str4.hashCode() : 0)) * 31;
        Long l3 = this.interactionId;
        if (l3 != null) {
            i = l3.hashCode();
        }
        return hashCode8 + i;
    }

    public final ApplicationCommandData toRestApiApplicationCommandDataParam() {
        String str = this.version;
        String str2 = this.applicationCommandId;
        Long l = this.commandGuildId;
        return new ApplicationCommandData(str, l != null ? String.valueOf(l.longValue()) : null, str2, this.applicationCommandName, ApplicationCommandLocalSendDataKt.toRestParams(this.applicationCommandsValues));
    }

    public final RestAPIParams.ApplicationCommand toRestApiParam() {
        String valueOf = String.valueOf(this.channelId);
        String valueOf2 = String.valueOf(this.application.getId());
        Long l = this.guildId;
        String valueOf3 = l != null ? String.valueOf(l.longValue()) : null;
        ApplicationCommandData restApiApplicationCommandDataParam = toRestApiApplicationCommandDataParam();
        String str = this.sessionId;
        return new RestAPIParams.ApplicationCommand(2L, valueOf, valueOf2, valueOf3, restApiApplicationCommandDataParam, str != null ? str : null, this.nonceString);
    }

    public String toString() {
        StringBuilder R = a.R("ApplicationCommandLocalSendData(nonce=");
        R.append(this.nonce);
        R.append(", channelId=");
        R.append(this.channelId);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", commandGuildId=");
        R.append(this.commandGuildId);
        R.append(", application=");
        R.append(this.application);
        R.append(", sessionId=");
        R.append(this.sessionId);
        R.append(", applicationCommandName=");
        R.append(this.applicationCommandName);
        R.append(", applicationCommandId=");
        R.append(this.applicationCommandId);
        R.append(", applicationCommandsValues=");
        R.append(this.applicationCommandsValues);
        R.append(", version=");
        R.append(this.version);
        R.append(", interactionId=");
        return a.F(R, this.interactionId, ")");
    }

    public /* synthetic */ ApplicationCommandLocalSendData(long j, long j2, Long l, Long l2, Application application, String str, String str2, String str3, List list, String str4, Long l3, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(j, j2, l, l2, application, str, str2, str3, list, str4, (i & 1024) != 0 ? null : l3);
    }
}
