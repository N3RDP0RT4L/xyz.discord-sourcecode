package com.discord.models.commands;

import com.discord.api.commands.ApplicationCommandValue;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
/* compiled from: ApplicationCommandLocalSendData.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u001a!\u0010\u0004\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\u0000*\b\u0012\u0004\u0012\u00020\u00010\u0000¢\u0006\u0004\b\u0004\u0010\u0005\u001a\u0015\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003*\u00020\u0001¢\u0006\u0004\b\u0006\u0010\u0007*\n\u0010\b\"\u00020\u00022\u00020\u0002¨\u0006\t"}, d2 = {"", "Lcom/discord/widgets/chat/input/models/ApplicationCommandValue;", "Lcom/discord/api/commands/ApplicationCommandValue;", "Lcom/discord/models/commands/ApiApplicationCommandValue;", "toRestParams", "(Ljava/util/List;)Ljava/util/List;", "toRestParam", "(Lcom/discord/widgets/chat/input/models/ApplicationCommandValue;)Lcom/discord/api/commands/ApplicationCommandValue;", "ApiApplicationCommandValue", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ApplicationCommandLocalSendDataKt {
    public static final ApplicationCommandValue toRestParam(com.discord.widgets.chat.input.models.ApplicationCommandValue applicationCommandValue) {
        m.checkNotNullParameter(applicationCommandValue, "$this$toRestParam");
        String name = applicationCommandValue.getName();
        Object value = applicationCommandValue.getValue();
        List<com.discord.widgets.chat.input.models.ApplicationCommandValue> options = applicationCommandValue.getOptions();
        return new ApplicationCommandValue(name, value, applicationCommandValue.getType(), options != null ? toRestParams(options) : null, applicationCommandValue.getFocused());
    }

    public static final List<ApplicationCommandValue> toRestParams(List<com.discord.widgets.chat.input.models.ApplicationCommandValue> list) {
        m.checkNotNullParameter(list, "$this$toRestParams");
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
        for (com.discord.widgets.chat.input.models.ApplicationCommandValue applicationCommandValue : list) {
            arrayList.add(toRestParam(applicationCommandValue));
        }
        return arrayList;
    }
}
