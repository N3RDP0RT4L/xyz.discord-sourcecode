package com.discord.models.commands;

import andhook.lib.HookHelper;
import d0.z.d.m;
import java.util.Comparator;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ModelApplicationComparator.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\u0018\u0000 \u00042\u00020\u0001:\u0001\u0004B\u0007¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0005"}, d2 = {"Lcom/discord/models/commands/ModelApplicationComparator;", "", HookHelper.constructorName, "()V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ModelApplicationComparator {
    public static final Companion Companion = new Companion(null);

    /* compiled from: ModelApplicationComparator.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u0012\u0012\u0004\u0012\u00020\u00020\u0001j\b\u0012\u0004\u0012\u00020\u0002`\u0003B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/models/commands/ModelApplicationComparator$Companion;", "Ljava/util/Comparator;", "Lcom/discord/models/commands/Application;", "Lkotlin/Comparator;", "a", "b", "", "compare", "(Lcom/discord/models/commands/Application;Lcom/discord/models/commands/Application;)I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion implements Comparator<Application> {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public int compare(Application application, Application application2) {
            m.checkNotNullParameter(application, "a");
            m.checkNotNullParameter(application2, "b");
            int i = (application.getBuiltIn() ? 1 : 0) - (application2.getBuiltIn() ? 1 : 0);
            return i != 0 ? i : application.getName().compareTo(application2.getName());
        }
    }
}
