package com.discord.models.commands;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.commands.ApplicationCommandType;
import com.discord.api.commands.CommandChoice;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ApplicationCommandOption.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0004\n\u0002\b,\b\u0086\b\u0018\u00002\u00020\u0001B\u0097\u0001\u0012\u0006\u0010\u001b\u001a\u00020\u0002\u0012\u0006\u0010\u001c\u001a\u00020\u0005\u0012\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\t\u0012\u0006\u0010\u001f\u001a\u00020\f\u0012\u0006\u0010 \u001a\u00020\f\u0012\u0010\b\u0002\u0010!\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u0010\u0012\u0010\b\u0002\u0010\"\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u0010\u0012\u0010\b\u0002\u0010#\u001a\n\u0012\u0004\u0012\u00020\u0000\u0018\u00010\u0010\u0012\b\b\u0002\u0010$\u001a\u00020\f\u0012\n\b\u0002\u0010%\u001a\u0004\u0018\u00010\u0017\u0012\n\b\u0002\u0010&\u001a\u0004\u0018\u00010\u0017¢\u0006\u0004\bA\u0010BJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\b\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u000eJ\u0018\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0018\u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u0010HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0012J\u0018\u0010\u0015\u001a\n\u0012\u0004\u0012\u00020\u0000\u0018\u00010\u0010HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0012J\u0010\u0010\u0016\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\u0016\u0010\u000eJ\u0012\u0010\u0018\u001a\u0004\u0018\u00010\u0017HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0019J\u0012\u0010\u001a\u001a\u0004\u0018\u00010\u0017HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u0019J¨\u0001\u0010'\u001a\u00020\u00002\b\b\u0002\u0010\u001b\u001a\u00020\u00022\b\b\u0002\u0010\u001c\u001a\u00020\u00052\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\t2\b\b\u0002\u0010\u001f\u001a\u00020\f2\b\b\u0002\u0010 \u001a\u00020\f2\u0010\b\u0002\u0010!\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u00102\u0010\b\u0002\u0010\"\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u00102\u0010\b\u0002\u0010#\u001a\n\u0012\u0004\u0012\u00020\u0000\u0018\u00010\u00102\b\b\u0002\u0010$\u001a\u00020\f2\n\b\u0002\u0010%\u001a\u0004\u0018\u00010\u00172\n\b\u0002\u0010&\u001a\u0004\u0018\u00010\u0017HÆ\u0001¢\u0006\u0004\b'\u0010(J\u0010\u0010)\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b)\u0010\u0007J\u0010\u0010*\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b*\u0010+J\u001a\u0010-\u001a\u00020\f2\b\u0010,\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b-\u0010.R\u0019\u0010\u001b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010/\u001a\u0004\b0\u0010\u0004R!\u0010!\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b!\u00101\u001a\u0004\b2\u0010\u0012R\u0019\u0010 \u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b \u00103\u001a\u0004\b4\u0010\u000eR!\u0010\"\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00101\u001a\u0004\b5\u0010\u0012R\u0019\u0010\u001f\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00103\u001a\u0004\b6\u0010\u000eR\u001b\u0010%\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b%\u00107\u001a\u0004\b8\u0010\u0019R\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00109\u001a\u0004\b:\u0010\u0007R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010;\u001a\u0004\b<\u0010\u000bR\u001b\u0010&\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b&\u00107\u001a\u0004\b=\u0010\u0019R\u0019\u0010$\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b$\u00103\u001a\u0004\b>\u0010\u000eR!\u0010#\u001a\n\u0012\u0004\u0012\u00020\u0000\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b#\u00101\u001a\u0004\b?\u0010\u0012R\u0019\u0010\u001c\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u00109\u001a\u0004\b@\u0010\u0007¨\u0006C"}, d2 = {"Lcom/discord/models/commands/ApplicationCommandOption;", "", "Lcom/discord/api/commands/ApplicationCommandType;", "component1", "()Lcom/discord/api/commands/ApplicationCommandType;", "", "component2", "()Ljava/lang/String;", "component3", "", "component4", "()Ljava/lang/Integer;", "", "component5", "()Z", "component6", "", "component7", "()Ljava/util/List;", "Lcom/discord/api/commands/CommandChoice;", "component8", "component9", "component10", "", "component11", "()Ljava/lang/Number;", "component12", "type", ModelAuditLogEntry.CHANGE_KEY_NAME, ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "descriptionRes", "required", "default", "channelTypes", "choices", "options", "autocomplete", "minValue", "maxValue", "copy", "(Lcom/discord/api/commands/ApplicationCommandType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;ZZLjava/util/List;Ljava/util/List;Ljava/util/List;ZLjava/lang/Number;Ljava/lang/Number;)Lcom/discord/models/commands/ApplicationCommandOption;", "toString", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/commands/ApplicationCommandType;", "getType", "Ljava/util/List;", "getChannelTypes", "Z", "getDefault", "getChoices", "getRequired", "Ljava/lang/Number;", "getMinValue", "Ljava/lang/String;", "getDescription", "Ljava/lang/Integer;", "getDescriptionRes", "getMaxValue", "getAutocomplete", "getOptions", "getName", HookHelper.constructorName, "(Lcom/discord/api/commands/ApplicationCommandType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;ZZLjava/util/List;Ljava/util/List;Ljava/util/List;ZLjava/lang/Number;Ljava/lang/Number;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ApplicationCommandOption {
    private final boolean autocomplete;
    private final List<Integer> channelTypes;
    private final List<CommandChoice> choices;

    /* renamed from: default  reason: not valid java name */
    private final boolean f5default;
    private final String description;
    private final Integer descriptionRes;
    private final Number maxValue;
    private final Number minValue;
    private final String name;
    private final List<ApplicationCommandOption> options;
    private final boolean required;
    private final ApplicationCommandType type;

    public ApplicationCommandOption(ApplicationCommandType applicationCommandType, String str, String str2, Integer num, boolean z2, boolean z3, List<Integer> list, List<CommandChoice> list2, List<ApplicationCommandOption> list3, boolean z4, Number number, Number number2) {
        m.checkNotNullParameter(applicationCommandType, "type");
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        this.type = applicationCommandType;
        this.name = str;
        this.description = str2;
        this.descriptionRes = num;
        this.required = z2;
        this.f5default = z3;
        this.channelTypes = list;
        this.choices = list2;
        this.options = list3;
        this.autocomplete = z4;
        this.minValue = number;
        this.maxValue = number2;
    }

    public final ApplicationCommandType component1() {
        return this.type;
    }

    public final boolean component10() {
        return this.autocomplete;
    }

    public final Number component11() {
        return this.minValue;
    }

    public final Number component12() {
        return this.maxValue;
    }

    public final String component2() {
        return this.name;
    }

    public final String component3() {
        return this.description;
    }

    public final Integer component4() {
        return this.descriptionRes;
    }

    public final boolean component5() {
        return this.required;
    }

    public final boolean component6() {
        return this.f5default;
    }

    public final List<Integer> component7() {
        return this.channelTypes;
    }

    public final List<CommandChoice> component8() {
        return this.choices;
    }

    public final List<ApplicationCommandOption> component9() {
        return this.options;
    }

    public final ApplicationCommandOption copy(ApplicationCommandType applicationCommandType, String str, String str2, Integer num, boolean z2, boolean z3, List<Integer> list, List<CommandChoice> list2, List<ApplicationCommandOption> list3, boolean z4, Number number, Number number2) {
        m.checkNotNullParameter(applicationCommandType, "type");
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        return new ApplicationCommandOption(applicationCommandType, str, str2, num, z2, z3, list, list2, list3, z4, number, number2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ApplicationCommandOption)) {
            return false;
        }
        ApplicationCommandOption applicationCommandOption = (ApplicationCommandOption) obj;
        return m.areEqual(this.type, applicationCommandOption.type) && m.areEqual(this.name, applicationCommandOption.name) && m.areEqual(this.description, applicationCommandOption.description) && m.areEqual(this.descriptionRes, applicationCommandOption.descriptionRes) && this.required == applicationCommandOption.required && this.f5default == applicationCommandOption.f5default && m.areEqual(this.channelTypes, applicationCommandOption.channelTypes) && m.areEqual(this.choices, applicationCommandOption.choices) && m.areEqual(this.options, applicationCommandOption.options) && this.autocomplete == applicationCommandOption.autocomplete && m.areEqual(this.minValue, applicationCommandOption.minValue) && m.areEqual(this.maxValue, applicationCommandOption.maxValue);
    }

    public final boolean getAutocomplete() {
        return this.autocomplete;
    }

    public final List<Integer> getChannelTypes() {
        return this.channelTypes;
    }

    public final List<CommandChoice> getChoices() {
        return this.choices;
    }

    public final boolean getDefault() {
        return this.f5default;
    }

    public final String getDescription() {
        return this.description;
    }

    public final Integer getDescriptionRes() {
        return this.descriptionRes;
    }

    public final Number getMaxValue() {
        return this.maxValue;
    }

    public final Number getMinValue() {
        return this.minValue;
    }

    public final String getName() {
        return this.name;
    }

    public final List<ApplicationCommandOption> getOptions() {
        return this.options;
    }

    public final boolean getRequired() {
        return this.required;
    }

    public final ApplicationCommandType getType() {
        return this.type;
    }

    public int hashCode() {
        ApplicationCommandType applicationCommandType = this.type;
        int i = 0;
        int hashCode = (applicationCommandType != null ? applicationCommandType.hashCode() : 0) * 31;
        String str = this.name;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.description;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        Integer num = this.descriptionRes;
        int hashCode4 = (hashCode3 + (num != null ? num.hashCode() : 0)) * 31;
        boolean z2 = this.required;
        int i2 = 1;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        int i5 = (hashCode4 + i3) * 31;
        boolean z3 = this.f5default;
        if (z3) {
            z3 = true;
        }
        int i6 = z3 ? 1 : 0;
        int i7 = z3 ? 1 : 0;
        int i8 = (i5 + i6) * 31;
        List<Integer> list = this.channelTypes;
        int hashCode5 = (i8 + (list != null ? list.hashCode() : 0)) * 31;
        List<CommandChoice> list2 = this.choices;
        int hashCode6 = (hashCode5 + (list2 != null ? list2.hashCode() : 0)) * 31;
        List<ApplicationCommandOption> list3 = this.options;
        int hashCode7 = (hashCode6 + (list3 != null ? list3.hashCode() : 0)) * 31;
        boolean z4 = this.autocomplete;
        if (!z4) {
            i2 = z4 ? 1 : 0;
        }
        int i9 = (hashCode7 + i2) * 31;
        Number number = this.minValue;
        int hashCode8 = (i9 + (number != null ? number.hashCode() : 0)) * 31;
        Number number2 = this.maxValue;
        if (number2 != null) {
            i = number2.hashCode();
        }
        return hashCode8 + i;
    }

    public String toString() {
        StringBuilder R = a.R("ApplicationCommandOption(type=");
        R.append(this.type);
        R.append(", name=");
        R.append(this.name);
        R.append(", description=");
        R.append(this.description);
        R.append(", descriptionRes=");
        R.append(this.descriptionRes);
        R.append(", required=");
        R.append(this.required);
        R.append(", default=");
        R.append(this.f5default);
        R.append(", channelTypes=");
        R.append(this.channelTypes);
        R.append(", choices=");
        R.append(this.choices);
        R.append(", options=");
        R.append(this.options);
        R.append(", autocomplete=");
        R.append(this.autocomplete);
        R.append(", minValue=");
        R.append(this.minValue);
        R.append(", maxValue=");
        R.append(this.maxValue);
        R.append(")");
        return R.toString();
    }

    public /* synthetic */ ApplicationCommandOption(ApplicationCommandType applicationCommandType, String str, String str2, Integer num, boolean z2, boolean z3, List list, List list2, List list3, boolean z4, Number number, Number number2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(applicationCommandType, str, (i & 4) != 0 ? null : str2, (i & 8) != 0 ? null : num, z2, z3, (i & 64) != 0 ? n.emptyList() : list, (i & 128) != 0 ? null : list2, (i & 256) != 0 ? null : list3, (i & 512) != 0 ? false : z4, (i & 1024) != 0 ? null : number, (i & 2048) != 0 ? null : number2);
    }
}
