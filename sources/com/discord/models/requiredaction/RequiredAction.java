package com.discord.models.requiredaction;

import andhook.lib.HookHelper;
import d0.z.d.m;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: RequiredAction.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\t\b\u0086\u0001\u0018\u0000 \u00042\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0004B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\t¨\u0006\n"}, d2 = {"Lcom/discord/models/requiredaction/RequiredAction;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "Companion", "AGREEMENTS", "REQUIRE_VERIFIED_EMAIL", "REQUIRE_VERIFIED_PHONE", "REQUIRE_CAPTCHA", "NONE", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public enum RequiredAction {
    AGREEMENTS,
    REQUIRE_VERIFIED_EMAIL,
    REQUIRE_VERIFIED_PHONE,
    REQUIRE_CAPTCHA,
    NONE;
    
    public static final Companion Companion = new Companion(null);

    /* compiled from: RequiredAction.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/models/requiredaction/RequiredAction$Companion;", "", "", "requiredAction", "Lcom/discord/models/requiredaction/RequiredAction;", "fromApiString", "(Ljava/lang/String;)Lcom/discord/models/requiredaction/RequiredAction;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public final RequiredAction fromApiString(String str) {
            if (str != null) {
                try {
                    Locale locale = Locale.ROOT;
                    m.checkNotNullExpressionValue(locale, "Locale.ROOT");
                    String upperCase = str.toUpperCase(locale);
                    m.checkNotNullExpressionValue(upperCase, "(this as java.lang.String).toUpperCase(locale)");
                    RequiredAction valueOf = RequiredAction.valueOf(upperCase);
                    if (valueOf != null) {
                        return valueOf;
                    }
                } catch (IllegalArgumentException unused) {
                    return RequiredAction.NONE;
                } catch (NullPointerException unused2) {
                    return RequiredAction.NONE;
                }
            }
            return RequiredAction.NONE;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
