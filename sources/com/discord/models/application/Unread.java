package com.discord.models.application;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.message.Message;
import d0.t.s;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: Unread.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\b\u0018\u0000  2\u00020\u0001:\u0002 !B\u001d\b\u0007\u0012\b\b\u0002\u0010\n\u001a\u00020\u0004\u0012\b\b\u0002\u0010\u000b\u001a\u00020\u0007¢\u0006\u0004\b\u001a\u0010\u001bB\u001f\b\u0016\u0012\u0006\u0010\n\u001a\u00020\u0004\u0012\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u001d0\u001c¢\u0006\u0004\b\u001a\u0010\u001fJ\r\u0010\u0002\u001a\u00020\u0000¢\u0006\u0004\b\u0002\u0010\u0003J\u0010\u0010\u0005\u001a\u00020\u0004HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u0010\u0010\b\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ$\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u00042\b\b\u0002\u0010\u000b\u001a\u00020\u0007HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\u0011\u0010\tJ\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\n\u001a\u00020\u00048\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0016\u001a\u0004\b\u0017\u0010\u0006R\u0019\u0010\u000b\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0018\u001a\u0004\b\u0019\u0010\t¨\u0006\""}, d2 = {"Lcom/discord/models/application/Unread;", "", "createWithEmptyCount", "()Lcom/discord/models/application/Unread;", "Lcom/discord/models/application/Unread$Marker;", "component1", "()Lcom/discord/models/application/Unread$Marker;", "", "component2", "()I", "marker", "count", "copy", "(Lcom/discord/models/application/Unread$Marker;I)Lcom/discord/models/application/Unread;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/application/Unread$Marker;", "getMarker", "I", "getCount", HookHelper.constructorName, "(Lcom/discord/models/application/Unread$Marker;I)V", "", "Lcom/discord/models/message/Message;", "messages", "(Lcom/discord/models/application/Unread$Marker;Ljava/util/List;)V", "Companion", "Marker", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class Unread {
    public static final Companion Companion = new Companion(null);
    private final int count;
    private final Marker marker;

    /* compiled from: Unread.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bJ%\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H\u0002¢\u0006\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/models/application/Unread$Companion;", "", "Lcom/discord/models/application/Unread$Marker;", "marker", "", "Lcom/discord/models/message/Message;", "messages", "", "getTotalMessages", "(Lcom/discord/models/application/Unread$Marker;Ljava/util/List;)I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final int getTotalMessages(Marker marker, List<Message> list) {
            int i = 0;
            if (!list.isEmpty() && marker.getMessageId() != null) {
                for (Message message : s.asReversed(list)) {
                    long id2 = message.getId();
                    Long messageId = marker.getMessageId();
                    if (messageId != null && id2 == messageId.longValue()) {
                        return i;
                    }
                    i++;
                }
            }
            return i;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: Unread.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B#\u0012\n\u0010\t\u001a\u00060\u0002j\u0002`\u0003\u0012\u000e\u0010\n\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0006¢\u0006\u0004\b\u001b\u0010\u001cB\t\b\u0016¢\u0006\u0004\b\u001b\u0010\u001dB1\b\u0016\u0012\n\u0010\t\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010\n\u001a\u00060\u0002j\u0002`\u0006\u0012\u000e\u0010\u001e\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0006¢\u0006\u0004\b\u001b\u0010\u001fJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0018\u0010\u0007\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ0\u0010\u000b\u001a\u00020\u00002\f\b\u0002\u0010\t\u001a\u00060\u0002j\u0002`\u00032\u0010\b\u0002\u0010\n\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0006HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u001d\u0010\t\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0005R!\u0010\n\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0019\u001a\u0004\b\u001a\u0010\b¨\u0006 "}, d2 = {"Lcom/discord/models/application/Unread$Marker;", "", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "Lcom/discord/primitives/MessageId;", "component2", "()Ljava/lang/Long;", "channelId", "messageId", "copy", "(JLjava/lang/Long;)Lcom/discord/models/application/Unread$Marker;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getChannelId", "Ljava/lang/Long;", "getMessageId", HookHelper.constructorName, "(JLjava/lang/Long;)V", "()V", "mostRecentMessageId", "(JJLjava/lang/Long;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Marker {
        private final long channelId;
        private final Long messageId;

        public Marker(long j, Long l) {
            this.channelId = j;
            this.messageId = l;
        }

        public static /* synthetic */ Marker copy$default(Marker marker, long j, Long l, int i, Object obj) {
            if ((i & 1) != 0) {
                j = marker.channelId;
            }
            if ((i & 2) != 0) {
                l = marker.messageId;
            }
            return marker.copy(j, l);
        }

        public final long component1() {
            return this.channelId;
        }

        public final Long component2() {
            return this.messageId;
        }

        public final Marker copy(long j, Long l) {
            return new Marker(j, l);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Marker)) {
                return false;
            }
            Marker marker = (Marker) obj;
            return this.channelId == marker.channelId && m.areEqual(this.messageId, marker.messageId);
        }

        public final long getChannelId() {
            return this.channelId;
        }

        public final Long getMessageId() {
            return this.messageId;
        }

        public int hashCode() {
            int a = b.a(this.channelId) * 31;
            Long l = this.messageId;
            return a + (l != null ? l.hashCode() : 0);
        }

        public String toString() {
            StringBuilder R = a.R("Marker(channelId=");
            R.append(this.channelId);
            R.append(", messageId=");
            return a.F(R, this.messageId, ")");
        }

        public Marker() {
            this(0L, 0L);
        }

        public Marker(long j, long j2, Long l) {
            this(j, (l == null || l.longValue() != j2) ? Long.valueOf(j2) : null);
        }
    }

    public Unread() {
        this(null, 0, 3, null);
    }

    public Unread(Marker marker) {
        this(marker, 0, 2, null);
    }

    public Unread(Marker marker, int i) {
        m.checkNotNullParameter(marker, "marker");
        this.marker = marker;
        this.count = i;
    }

    public static /* synthetic */ Unread copy$default(Unread unread, Marker marker, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            marker = unread.marker;
        }
        if ((i2 & 2) != 0) {
            i = unread.count;
        }
        return unread.copy(marker, i);
    }

    public final Marker component1() {
        return this.marker;
    }

    public final int component2() {
        return this.count;
    }

    public final Unread copy(Marker marker, int i) {
        m.checkNotNullParameter(marker, "marker");
        return new Unread(marker, i);
    }

    public final Unread createWithEmptyCount() {
        return new Unread(this.marker, 0);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Unread)) {
            return false;
        }
        Unread unread = (Unread) obj;
        return m.areEqual(this.marker, unread.marker) && this.count == unread.count;
    }

    public final int getCount() {
        return this.count;
    }

    public final Marker getMarker() {
        return this.marker;
    }

    public int hashCode() {
        Marker marker = this.marker;
        return ((marker != null ? marker.hashCode() : 0) * 31) + this.count;
    }

    public String toString() {
        StringBuilder R = a.R("Unread(marker=");
        R.append(this.marker);
        R.append(", count=");
        return a.A(R, this.count, ")");
    }

    public /* synthetic */ Unread(Marker marker, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this((i2 & 1) != 0 ? new Marker() : marker, (i2 & 2) != 0 ? 0 : i);
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public Unread(Marker marker, List<Message> list) {
        this(marker, Companion.getTotalMessages(marker, list));
        m.checkNotNullParameter(marker, "marker");
        m.checkNotNullParameter(list, "messages");
    }
}
