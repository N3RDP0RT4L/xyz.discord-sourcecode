package com.discord.models.experiments.dto;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.Model;
import com.discord.models.experiments.dto.GuildExperimentBucketDto;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Ref$IntRef;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlin.ranges.IntRange;
import rx.functions.Action1;
/* compiled from: GuildExperimentDto.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u001bB\u001d\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ*\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\t\u001a\u00020\u00022\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0004J\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004R\u001f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0017\u001a\u0004\b\u0018\u0010\b¨\u0006\u001c"}, d2 = {"Lcom/discord/models/experiments/dto/GuildExperimentBucketDto;", "", "", "component1", "()I", "", "Lkotlin/ranges/IntRange;", "component2", "()Ljava/util/List;", "bucket", "positions", "copy", "(ILjava/util/List;)Lcom/discord/models/experiments/dto/GuildExperimentBucketDto;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getBucket", "Ljava/util/List;", "getPositions", HookHelper.constructorName, "(ILjava/util/List;)V", "Parser", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildExperimentBucketDto {
    private final int bucket;
    private final List<IntRange> positions;

    /* compiled from: GuildExperimentDto.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0015\u0010\t\u001a\u00020\b2\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\t\u0010\n¨\u0006\r"}, d2 = {"Lcom/discord/models/experiments/dto/GuildExperimentBucketDto$Parser;", "", "Lcom/discord/models/domain/Model$JsonReader;", "jsonReader", "Lkotlin/ranges/IntRange;", "parsePosition", "(Lcom/discord/models/domain/Model$JsonReader;)Lkotlin/ranges/IntRange;", "reader", "Lcom/discord/models/experiments/dto/GuildExperimentBucketDto;", "parse", "(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/experiments/dto/GuildExperimentBucketDto;", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Parser {
        public static final Parser INSTANCE = new Parser();

        private Parser() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final IntRange parsePosition(final Model.JsonReader jsonReader) {
            final Ref$IntRef ref$IntRef = new Ref$IntRef();
            ref$IntRef.element = -1;
            final Ref$IntRef ref$IntRef2 = new Ref$IntRef();
            ref$IntRef2.element = -1;
            jsonReader.nextObject(new Action1<String>() { // from class: com.discord.models.experiments.dto.GuildExperimentBucketDto$Parser$parsePosition$1
                public final void call(String str) {
                    if (str != null) {
                        int hashCode = str.hashCode();
                        if (hashCode != 101) {
                            if (hashCode == 115 && str.equals("s")) {
                                Ref$IntRef ref$IntRef3 = Ref$IntRef.this;
                                ref$IntRef3.element = jsonReader.nextInt(ref$IntRef3.element);
                                return;
                            }
                        } else if (str.equals("e")) {
                            Ref$IntRef ref$IntRef4 = ref$IntRef2;
                            ref$IntRef4.element = jsonReader.nextInt(ref$IntRef4.element);
                            return;
                        }
                    }
                    jsonReader.skipValue();
                }
            });
            return new IntRange(ref$IntRef.element, ref$IntRef2.element);
        }

        /* JADX WARN: Type inference failed for: r2v0, types: [java.util.List, T] */
        public final GuildExperimentBucketDto parse(final Model.JsonReader jsonReader) {
            m.checkNotNullParameter(jsonReader, "reader");
            final Ref$IntRef ref$IntRef = new Ref$IntRef();
            ref$IntRef.element = -1;
            final Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
            ref$ObjectRef.element = n.emptyList();
            jsonReader.nextListIndexed(new Runnable() { // from class: com.discord.models.experiments.dto.GuildExperimentBucketDto$Parser$parse$1
                @Override // java.lang.Runnable
                public final void run() {
                    Ref$IntRef ref$IntRef2 = Ref$IntRef.this;
                    ref$IntRef2.element = jsonReader.nextInt(ref$IntRef2.element);
                }
            }, new Runnable() { // from class: com.discord.models.experiments.dto.GuildExperimentBucketDto$Parser$parse$2
                /* JADX WARN: Type inference failed for: r1v1, types: [java.util.List, T, java.lang.Object] */
                @Override // java.lang.Runnable
                public final void run() {
                    Ref$ObjectRef ref$ObjectRef2 = Ref$ObjectRef.this;
                    ?? nextList = jsonReader.nextList(new Model.JsonReader.ItemFactory<IntRange>() { // from class: com.discord.models.experiments.dto.GuildExperimentBucketDto$Parser$parse$2.1
                        @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                        public final IntRange get() {
                            IntRange parsePosition;
                            parsePosition = GuildExperimentBucketDto.Parser.INSTANCE.parsePosition(jsonReader);
                            return parsePosition;
                        }
                    });
                    m.checkNotNullExpressionValue(nextList, "reader.nextList { parsePosition(reader) }");
                    ref$ObjectRef2.element = nextList;
                }
            });
            return new GuildExperimentBucketDto(ref$IntRef.element, (List) ref$ObjectRef.element);
        }
    }

    public GuildExperimentBucketDto(int i, List<IntRange> list) {
        m.checkNotNullParameter(list, "positions");
        this.bucket = i;
        this.positions = list;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ GuildExperimentBucketDto copy$default(GuildExperimentBucketDto guildExperimentBucketDto, int i, List list, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = guildExperimentBucketDto.bucket;
        }
        if ((i2 & 2) != 0) {
            list = guildExperimentBucketDto.positions;
        }
        return guildExperimentBucketDto.copy(i, list);
    }

    public final int component1() {
        return this.bucket;
    }

    public final List<IntRange> component2() {
        return this.positions;
    }

    public final GuildExperimentBucketDto copy(int i, List<IntRange> list) {
        m.checkNotNullParameter(list, "positions");
        return new GuildExperimentBucketDto(i, list);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildExperimentBucketDto)) {
            return false;
        }
        GuildExperimentBucketDto guildExperimentBucketDto = (GuildExperimentBucketDto) obj;
        return this.bucket == guildExperimentBucketDto.bucket && m.areEqual(this.positions, guildExperimentBucketDto.positions);
    }

    public final int getBucket() {
        return this.bucket;
    }

    public final List<IntRange> getPositions() {
        return this.positions;
    }

    public int hashCode() {
        int i = this.bucket * 31;
        List<IntRange> list = this.positions;
        return i + (list != null ? list.hashCode() : 0);
    }

    public String toString() {
        StringBuilder R = a.R("GuildExperimentBucketDto(bucket=");
        R.append(this.bucket);
        R.append(", positions=");
        return a.K(R, this.positions, ")");
    }
}
