package com.discord.models.experiments.dto;

import com.discord.api.guild.GuildHubType;
import com.discord.models.deserialization.gson.InboundGatewayGsonParser;
import com.discord.models.domain.Model;
import d0.t.u;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Ref$ObjectRef;
/* compiled from: GuildExperimentFilter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "run", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildExperimentFilter$Parser$parseGuildHubTypesFeatureFilter$1 implements Runnable {
    public final /* synthetic */ Ref$ObjectRef $hubTypes;
    public final /* synthetic */ Model.JsonReader $reader;

    public GuildExperimentFilter$Parser$parseGuildHubTypesFeatureFilter$1(Model.JsonReader jsonReader, Ref$ObjectRef ref$ObjectRef) {
        this.$reader = jsonReader;
        this.$hubTypes = ref$ObjectRef;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.$reader.nextListIndexed(new Runnable() { // from class: com.discord.models.experiments.dto.GuildExperimentFilter$Parser$parseGuildHubTypesFeatureFilter$1.1
            @Override // java.lang.Runnable
            public final void run() {
                GuildExperimentFilter$Parser$parseGuildHubTypesFeatureFilter$1.this.$reader.skipValue();
            }
        }, new Runnable() { // from class: com.discord.models.experiments.dto.GuildExperimentFilter$Parser$parseGuildHubTypesFeatureFilter$1.2
            /* JADX WARN: Type inference failed for: r0v3, types: [T, java.util.Set] */
            @Override // java.lang.Runnable
            public final void run() {
                GuildExperimentFilter$Parser$parseGuildHubTypesFeatureFilter$1 guildExperimentFilter$Parser$parseGuildHubTypesFeatureFilter$1 = GuildExperimentFilter$Parser$parseGuildHubTypesFeatureFilter$1.this;
                Ref$ObjectRef ref$ObjectRef = guildExperimentFilter$Parser$parseGuildHubTypesFeatureFilter$1.$hubTypes;
                List nextList = guildExperimentFilter$Parser$parseGuildHubTypesFeatureFilter$1.$reader.nextList(new Model.JsonReader.ItemFactory<GuildHubType>() { // from class: com.discord.models.experiments.dto.GuildExperimentFilter.Parser.parseGuildHubTypesFeatureFilter.1.2.1
                    /* JADX WARN: Can't rename method to resolve collision */
                    @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                    public final GuildHubType get() {
                        return (GuildHubType) InboundGatewayGsonParser.fromJson(GuildExperimentFilter$Parser$parseGuildHubTypesFeatureFilter$1.this.$reader, GuildHubType.class);
                    }
                });
                m.checkNotNullExpressionValue(nextList, "reader.nextList {\n      …ass.java)\n              }");
                ref$ObjectRef.element = u.toSet(nextList);
            }
        });
    }
}
