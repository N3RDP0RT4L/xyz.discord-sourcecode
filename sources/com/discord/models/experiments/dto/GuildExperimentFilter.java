package com.discord.models.experiments.dto;

import andhook.lib.HookHelper;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.api.guild.GuildFeature;
import com.discord.api.guild.GuildHubType;
import com.discord.models.domain.Model;
import com.discord.models.experiments.domain.ExperimentHash;
import com.discord.models.experiments.dto.GuildExperimentFilter;
import d0.o;
import d0.t.h0;
import d0.t.n0;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlin.ranges.LongRange;
/* compiled from: GuildExperimentFilter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0006\u0004\u0005\u0006\u0007\b\tB\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0005\n\u000b\f\r\u000e¨\u0006\u000f"}, d2 = {"Lcom/discord/models/experiments/dto/GuildExperimentFilter;", "", HookHelper.constructorName, "()V", "GuildHasFeatureFilter", "GuildHubTypesFeatureFilter", "GuildIdRangeFilter", "GuildIdsFilter", "GuildMemberCountRangeFilter", "Parser", "Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdsFilter;", "Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdRangeFilter;", "Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildMemberCountRangeFilter;", "Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildHasFeatureFilter;", "Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildHubTypesFeatureFilter;", "app_models_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public abstract class GuildExperimentFilter {

    /* compiled from: GuildExperimentFilter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J \u0010\u0007\u001a\u00020\u00002\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildHasFeatureFilter;", "Lcom/discord/models/experiments/dto/GuildExperimentFilter;", "", "Lcom/discord/api/guild/GuildFeature;", "component1", "()Ljava/util/Set;", "features", "copy", "(Ljava/util/Set;)Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildHasFeatureFilter;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Set;", "getFeatures", HookHelper.constructorName, "(Ljava/util/Set;)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class GuildHasFeatureFilter extends GuildExperimentFilter {
        private final Set<GuildFeature> features;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        /* JADX WARN: Multi-variable type inference failed */
        public GuildHasFeatureFilter(Set<? extends GuildFeature> set) {
            super(null);
            m.checkNotNullParameter(set, "features");
            this.features = set;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ GuildHasFeatureFilter copy$default(GuildHasFeatureFilter guildHasFeatureFilter, Set set, int i, Object obj) {
            if ((i & 1) != 0) {
                set = guildHasFeatureFilter.features;
            }
            return guildHasFeatureFilter.copy(set);
        }

        public final Set<GuildFeature> component1() {
            return this.features;
        }

        public final GuildHasFeatureFilter copy(Set<? extends GuildFeature> set) {
            m.checkNotNullParameter(set, "features");
            return new GuildHasFeatureFilter(set);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof GuildHasFeatureFilter) && m.areEqual(this.features, ((GuildHasFeatureFilter) obj).features);
            }
            return true;
        }

        public final Set<GuildFeature> getFeatures() {
            return this.features;
        }

        public int hashCode() {
            Set<GuildFeature> set = this.features;
            if (set != null) {
                return set.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder R = a.R("GuildHasFeatureFilter(features=");
            R.append(this.features);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: GuildExperimentFilter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J \u0010\u0007\u001a\u00020\u00002\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildHubTypesFeatureFilter;", "Lcom/discord/models/experiments/dto/GuildExperimentFilter;", "", "Lcom/discord/api/guild/GuildHubType;", "component1", "()Ljava/util/Set;", "hubTypes", "copy", "(Ljava/util/Set;)Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildHubTypesFeatureFilter;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Set;", "getHubTypes", HookHelper.constructorName, "(Ljava/util/Set;)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class GuildHubTypesFeatureFilter extends GuildExperimentFilter {
        private final Set<GuildHubType> hubTypes;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        /* JADX WARN: Multi-variable type inference failed */
        public GuildHubTypesFeatureFilter(Set<? extends GuildHubType> set) {
            super(null);
            m.checkNotNullParameter(set, "hubTypes");
            this.hubTypes = set;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ GuildHubTypesFeatureFilter copy$default(GuildHubTypesFeatureFilter guildHubTypesFeatureFilter, Set set, int i, Object obj) {
            if ((i & 1) != 0) {
                set = guildHubTypesFeatureFilter.hubTypes;
            }
            return guildHubTypesFeatureFilter.copy(set);
        }

        public final Set<GuildHubType> component1() {
            return this.hubTypes;
        }

        public final GuildHubTypesFeatureFilter copy(Set<? extends GuildHubType> set) {
            m.checkNotNullParameter(set, "hubTypes");
            return new GuildHubTypesFeatureFilter(set);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof GuildHubTypesFeatureFilter) && m.areEqual(this.hubTypes, ((GuildHubTypesFeatureFilter) obj).hubTypes);
            }
            return true;
        }

        public final Set<GuildHubType> getHubTypes() {
            return this.hubTypes;
        }

        public int hashCode() {
            Set<GuildHubType> set = this.hubTypes;
            if (set != null) {
                return set.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder R = a.R("GuildHubTypesFeatureFilter(hubTypes=");
            R.append(this.hubTypes);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: GuildExperimentFilter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdRangeFilter;", "Lcom/discord/models/experiments/dto/GuildExperimentFilter;", "Lkotlin/ranges/LongRange;", "component1", "()Lkotlin/ranges/LongRange;", "range", "copy", "(Lkotlin/ranges/LongRange;)Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdRangeFilter;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lkotlin/ranges/LongRange;", "getRange", HookHelper.constructorName, "(Lkotlin/ranges/LongRange;)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class GuildIdRangeFilter extends GuildExperimentFilter {
        private final LongRange range;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public GuildIdRangeFilter(LongRange longRange) {
            super(null);
            m.checkNotNullParameter(longRange, "range");
            this.range = longRange;
        }

        public static /* synthetic */ GuildIdRangeFilter copy$default(GuildIdRangeFilter guildIdRangeFilter, LongRange longRange, int i, Object obj) {
            if ((i & 1) != 0) {
                longRange = guildIdRangeFilter.range;
            }
            return guildIdRangeFilter.copy(longRange);
        }

        public final LongRange component1() {
            return this.range;
        }

        public final GuildIdRangeFilter copy(LongRange longRange) {
            m.checkNotNullParameter(longRange, "range");
            return new GuildIdRangeFilter(longRange);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof GuildIdRangeFilter) && m.areEqual(this.range, ((GuildIdRangeFilter) obj).range);
            }
            return true;
        }

        public final LongRange getRange() {
            return this.range;
        }

        public int hashCode() {
            LongRange longRange = this.range;
            if (longRange != null) {
                return longRange.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder R = a.R("GuildIdRangeFilter(range=");
            R.append(this.range);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: GuildExperimentFilter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\u0010\u0010\u0007\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u001a\u0010\u0005\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0006J$\u0010\b\u001a\u00020\u00002\u0012\b\u0002\u0010\u0007\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R#\u0010\u0007\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0015\u001a\u0004\b\u0016\u0010\u0006¨\u0006\u0019"}, d2 = {"Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdsFilter;", "Lcom/discord/models/experiments/dto/GuildExperimentFilter;", "", "", "Lcom/discord/primitives/GuildId;", "component1", "()Ljava/util/Set;", "guildIds", "copy", "(Ljava/util/Set;)Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdsFilter;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Set;", "getGuildIds", HookHelper.constructorName, "(Ljava/util/Set;)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class GuildIdsFilter extends GuildExperimentFilter {
        private final Set<Long> guildIds;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public GuildIdsFilter(Set<Long> set) {
            super(null);
            m.checkNotNullParameter(set, "guildIds");
            this.guildIds = set;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ GuildIdsFilter copy$default(GuildIdsFilter guildIdsFilter, Set set, int i, Object obj) {
            if ((i & 1) != 0) {
                set = guildIdsFilter.guildIds;
            }
            return guildIdsFilter.copy(set);
        }

        public final Set<Long> component1() {
            return this.guildIds;
        }

        public final GuildIdsFilter copy(Set<Long> set) {
            m.checkNotNullParameter(set, "guildIds");
            return new GuildIdsFilter(set);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof GuildIdsFilter) && m.areEqual(this.guildIds, ((GuildIdsFilter) obj).guildIds);
            }
            return true;
        }

        public final Set<Long> getGuildIds() {
            return this.guildIds;
        }

        public int hashCode() {
            Set<Long> set = this.guildIds;
            if (set != null) {
                return set.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder R = a.R("GuildIdsFilter(guildIds=");
            R.append(this.guildIds);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: GuildExperimentFilter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildMemberCountRangeFilter;", "Lcom/discord/models/experiments/dto/GuildExperimentFilter;", "Lkotlin/ranges/LongRange;", "component1", "()Lkotlin/ranges/LongRange;", "range", "copy", "(Lkotlin/ranges/LongRange;)Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildMemberCountRangeFilter;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lkotlin/ranges/LongRange;", "getRange", HookHelper.constructorName, "(Lkotlin/ranges/LongRange;)V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class GuildMemberCountRangeFilter extends GuildExperimentFilter {
        private final LongRange range;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public GuildMemberCountRangeFilter(LongRange longRange) {
            super(null);
            m.checkNotNullParameter(longRange, "range");
            this.range = longRange;
        }

        public static /* synthetic */ GuildMemberCountRangeFilter copy$default(GuildMemberCountRangeFilter guildMemberCountRangeFilter, LongRange longRange, int i, Object obj) {
            if ((i & 1) != 0) {
                longRange = guildMemberCountRangeFilter.range;
            }
            return guildMemberCountRangeFilter.copy(longRange);
        }

        public final LongRange component1() {
            return this.range;
        }

        public final GuildMemberCountRangeFilter copy(LongRange longRange) {
            m.checkNotNullParameter(longRange, "range");
            return new GuildMemberCountRangeFilter(longRange);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof GuildMemberCountRangeFilter) && m.areEqual(this.range, ((GuildMemberCountRangeFilter) obj).range);
            }
            return true;
        }

        public final LongRange getRange() {
            return this.range;
        }

        public int hashCode() {
            LongRange longRange = this.range;
            if (longRange != null) {
                return longRange.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder R = a.R("GuildMemberCountRangeFilter(range=");
            R.append(this.range);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: GuildExperimentFilter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0007\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b$\u0010%J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u001b\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00170\u00162\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0018\u0010\u0019R\u0016\u0010\u001b\u001a\u00020\u001a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\u0016\u0010\u001d\u001a\u00020\u001a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u001cR3\u0010 \u001a\u001c\u0012\u0004\u0012\u00020\u001a\u0012\u0012\u0012\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0006\u0012\u0004\u0018\u00010\u00170\u001f0\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#¨\u0006&"}, d2 = {"Lcom/discord/models/experiments/dto/GuildExperimentFilter$Parser;", "", "Lcom/discord/models/domain/Model$JsonReader;", "reader", "Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdsFilter;", "parseGuildIdsFilter", "(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdsFilter;", "Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdRangeFilter;", "parseGuildIdRangeFilter", "(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildIdRangeFilter;", "Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildMemberCountRangeFilter;", "parseGuildMemberCountRangeFilter", "(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildMemberCountRangeFilter;", "Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildHasFeatureFilter;", "parseGuildHasFeatureFilter", "(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildHasFeatureFilter;", "Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildHubTypesFeatureFilter;", "parseGuildHubTypesFeatureFilter", "(Lcom/discord/models/domain/Model$JsonReader;)Lcom/discord/models/experiments/dto/GuildExperimentFilter$GuildHubTypesFeatureFilter;", "Lkotlin/ranges/LongRange;", "parseRange", "(Lcom/discord/models/domain/Model$JsonReader;)Lkotlin/ranges/LongRange;", "", "Lcom/discord/models/experiments/dto/GuildExperimentFilter;", "parseFilters", "(Lcom/discord/models/domain/Model$JsonReader;)Ljava/util/List;", "", "MIN_ID_HASH", "J", "MAX_ID_HASH", "", "Lkotlin/Function1;", "parsers", "Ljava/util/Map;", "getParsers", "()Ljava/util/Map;", HookHelper.constructorName, "()V", "app_models_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Parser {
        public static final Parser INSTANCE;
        private static final long MAX_ID_HASH;
        private static final long MIN_ID_HASH;
        private static final Map<Long, Function1<Model.JsonReader, GuildExperimentFilter>> parsers;

        static {
            Parser parser = new Parser();
            INSTANCE = parser;
            ExperimentHash experimentHash = ExperimentHash.INSTANCE;
            parsers = h0.mapOf(o.to(Long.valueOf(experimentHash.from("guild_ids")), new GuildExperimentFilter$Parser$parsers$1(parser)), o.to(Long.valueOf(experimentHash.from("guild_id_range")), new GuildExperimentFilter$Parser$parsers$2(parser)), o.to(Long.valueOf(experimentHash.from("guild_member_count_range")), new GuildExperimentFilter$Parser$parsers$3(parser)), o.to(Long.valueOf(experimentHash.from("guild_has_feature")), new GuildExperimentFilter$Parser$parsers$4(parser)), o.to(Long.valueOf(experimentHash.from("guild_hub_types")), new GuildExperimentFilter$Parser$parsers$5(parser)));
            MIN_ID_HASH = experimentHash.from("min_id");
            MAX_ID_HASH = experimentHash.from("max_id");
        }

        private Parser() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        /* JADX WARN: Type inference failed for: r1v0, types: [T, java.util.Set] */
        public final GuildHasFeatureFilter parseGuildHasFeatureFilter(Model.JsonReader jsonReader) {
            Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
            ref$ObjectRef.element = n0.emptySet();
            jsonReader.nextListIndexed(new GuildExperimentFilter$Parser$parseGuildHasFeatureFilter$1(jsonReader, ref$ObjectRef));
            return new GuildHasFeatureFilter((Set) ref$ObjectRef.element);
        }

        /* JADX INFO: Access modifiers changed from: private */
        /* JADX WARN: Type inference failed for: r1v0, types: [T, java.util.Set] */
        public final GuildHubTypesFeatureFilter parseGuildHubTypesFeatureFilter(Model.JsonReader jsonReader) {
            Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
            ref$ObjectRef.element = n0.emptySet();
            jsonReader.nextListIndexed(new GuildExperimentFilter$Parser$parseGuildHubTypesFeatureFilter$1(jsonReader, ref$ObjectRef));
            return new GuildHubTypesFeatureFilter((Set) ref$ObjectRef.element);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final GuildIdRangeFilter parseGuildIdRangeFilter(Model.JsonReader jsonReader) {
            return new GuildIdRangeFilter(parseRange(jsonReader));
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final GuildIdsFilter parseGuildIdsFilter(Model.JsonReader jsonReader) {
            Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
            ref$ObjectRef.element = null;
            jsonReader.nextListIndexed(new GuildExperimentFilter$Parser$parseGuildIdsFilter$1(jsonReader, ref$ObjectRef));
            List list = (List) ref$ObjectRef.element;
            m.checkNotNull(list);
            return new GuildIdsFilter(u.toSet(list));
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final GuildMemberCountRangeFilter parseGuildMemberCountRangeFilter(Model.JsonReader jsonReader) {
            return new GuildMemberCountRangeFilter(parseRange(jsonReader));
        }

        private final LongRange parseRange(final Model.JsonReader jsonReader) {
            final Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
            ref$ObjectRef.element = null;
            final Ref$ObjectRef ref$ObjectRef2 = new Ref$ObjectRef();
            ref$ObjectRef2.element = null;
            jsonReader.nextList(new Model.JsonReader.ItemFactory<Unit>() { // from class: com.discord.models.experiments.dto.GuildExperimentFilter$Parser$parseRange$1
                @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                /* renamed from: get  reason: avoid collision after fix types in other method */
                public final void get2() {
                    final Ref$ObjectRef ref$ObjectRef3 = new Ref$ObjectRef();
                    ref$ObjectRef3.element = null;
                    Model.JsonReader.this.nextListIndexed(new Runnable() { // from class: com.discord.models.experiments.dto.GuildExperimentFilter$Parser$parseRange$1.1
                        @Override // java.lang.Runnable
                        public final void run() {
                            ref$ObjectRef3.element = (T) Model.JsonReader.this.nextLongOrNull();
                        }
                    }, new Runnable() { // from class: com.discord.models.experiments.dto.GuildExperimentFilter$Parser$parseRange$1.2
                        @Override // java.lang.Runnable
                        public final void run() {
                            long j;
                            long j2;
                            Long l = (Long) ref$ObjectRef3.element;
                            GuildExperimentFilter.Parser parser = GuildExperimentFilter.Parser.INSTANCE;
                            j = GuildExperimentFilter.Parser.MIN_ID_HASH;
                            if (l != null && l.longValue() == j) {
                                GuildExperimentFilter$Parser$parseRange$1 guildExperimentFilter$Parser$parseRange$1 = GuildExperimentFilter$Parser$parseRange$1.this;
                                ref$ObjectRef.element = (T) Model.JsonReader.this.nextLongOrNull();
                            } else {
                                Long l2 = (Long) ref$ObjectRef3.element;
                                j2 = GuildExperimentFilter.Parser.MAX_ID_HASH;
                                if (l2 != null && l2.longValue() == j2) {
                                    GuildExperimentFilter$Parser$parseRange$1 guildExperimentFilter$Parser$parseRange$12 = GuildExperimentFilter$Parser$parseRange$1.this;
                                    ref$ObjectRef2.element = (T) Model.JsonReader.this.nextLongOrNull();
                                }
                            }
                            ref$ObjectRef3.element = null;
                        }
                    });
                }
            });
            Long l = (Long) ref$ObjectRef.element;
            long longValue = l != null ? l.longValue() : 0L;
            Long l2 = (Long) ref$ObjectRef2.element;
            return new LongRange(longValue, l2 != null ? l2.longValue() : RecyclerView.FOREVER_NS);
        }

        public final Map<Long, Function1<Model.JsonReader, GuildExperimentFilter>> getParsers() {
            return parsers;
        }

        public final List<GuildExperimentFilter> parseFilters(final Model.JsonReader jsonReader) {
            m.checkNotNullParameter(jsonReader, "reader");
            final ArrayList arrayList = new ArrayList();
            jsonReader.nextList(new Model.JsonReader.ItemFactory<Unit>() { // from class: com.discord.models.experiments.dto.GuildExperimentFilter$Parser$parseFilters$1
                @Override // com.discord.models.domain.Model.JsonReader.ItemFactory
                /* renamed from: get  reason: avoid collision after fix types in other method */
                public final void get2() {
                    final Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
                    ref$ObjectRef.element = null;
                    Model.JsonReader.this.nextListIndexed(new Runnable() { // from class: com.discord.models.experiments.dto.GuildExperimentFilter$Parser$parseFilters$1.1
                        @Override // java.lang.Runnable
                        public final void run() {
                            ref$ObjectRef.element = (T) Model.JsonReader.this.nextLongOrNull();
                        }
                    }, new Runnable() { // from class: com.discord.models.experiments.dto.GuildExperimentFilter$Parser$parseFilters$1.2
                        @Override // java.lang.Runnable
                        public final void run() {
                            GuildExperimentFilter invoke;
                            Function1<Model.JsonReader, GuildExperimentFilter> function1 = GuildExperimentFilter.Parser.INSTANCE.getParsers().get((Long) ref$ObjectRef.element);
                            if (!(function1 == null || (invoke = function1.invoke(Model.JsonReader.this)) == null)) {
                                arrayList.add(invoke);
                            }
                            ref$ObjectRef.element = null;
                        }
                    });
                }
            });
            return arrayList;
        }
    }

    private GuildExperimentFilter() {
    }

    public /* synthetic */ GuildExperimentFilter(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }
}
