package com.discord.utilities.textprocessing;

import android.text.style.CharacterStyle;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import androidx.exifinterface.media.ExifInterface;
import d0.t.n;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: Rules.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\"\u0004\b\u0000\u0010\u0000\"\u0004\b\u0001\u0010\u0001H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {ExifInterface.GPS_DIRECTION_TRUE, ExifInterface.LATITUDE_SOUTH, "", "Landroid/text/style/CharacterStyle;", "invoke", "()Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class Rules$createStrikethroughColoredRule$1 extends o implements Function0<List<? extends CharacterStyle>> {
    public final /* synthetic */ int $color;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public Rules$createStrikethroughColoredRule$1(int i) {
        super(0);
        this.$color = i;
    }

    @Override // kotlin.jvm.functions.Function0
    public final List<? extends CharacterStyle> invoke() {
        return n.listOf((Object[]) new CharacterStyle[]{new ForegroundColorSpan(this.$color), new StrikethroughSpan()});
    }
}
