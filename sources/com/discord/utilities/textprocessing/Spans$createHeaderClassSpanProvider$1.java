package com.discord.utilities.textprocessing;

import android.content.Context;
import android.text.style.TextAppearanceSpan;
import com.discord.utilities.spans.VerticalPaddingSpan;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: Spans.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "className", "", "invoke", "(Ljava/lang/String;)Ljava/lang/Object;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class Spans$createHeaderClassSpanProvider$1 extends o implements Function1<String, Object> {
    public final /* synthetic */ Context $context;
    public final /* synthetic */ int $marginTopPx;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public Spans$createHeaderClassSpanProvider$1(Context context, int i) {
        super(1);
        this.$context = context;
        this.$marginTopPx = i;
    }

    public final Object invoke(String str) {
        m.checkNotNullParameter(str, "className");
        switch (str.hashCode()) {
            case -1269237627:
                if (str.equals("changelogSpecial")) {
                    return new TextAppearanceSpan(this.$context, R.style.Markdown_Header1_ChangelogSpecial);
                }
                break;
            case -1044792121:
                if (str.equals("marginTop")) {
                    return new VerticalPaddingSpan(0, this.$marginTopPx);
                }
                break;
            case -1001078227:
                if (str.equals("progress")) {
                    return new TextAppearanceSpan(this.$context, R.style.Markdown_Header1_Progress);
                }
                break;
            case -419685396:
                if (str.equals("improved")) {
                    return new TextAppearanceSpan(this.$context, R.style.Markdown_Header1_Improved);
                }
                break;
            case 92659968:
                if (str.equals("added")) {
                    return new TextAppearanceSpan(this.$context, R.style.Markdown_Header1_Added);
                }
                break;
            case 97445748:
                if (str.equals("fixed")) {
                    return new TextAppearanceSpan(this.$context, R.style.Markdown_Header1_Fixed);
                }
                break;
        }
        return null;
    }
}
