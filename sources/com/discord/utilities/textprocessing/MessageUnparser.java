package com.discord.utilities.textprocessing;

import andhook.lib.HookHelper;
import androidx.exifinterface.media.ExifInterface;
import b.a.t.b.b.e;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.role.GuildRole;
import com.discord.models.domain.emoji.Emoji;
import com.discord.models.domain.emoji.EmojiSet;
import com.discord.models.guild.Guild;
import com.discord.models.user.User;
import com.discord.simpleast.core.node.Node;
import com.discord.simpleast.core.parser.ParseSpec;
import com.discord.simpleast.core.parser.Parser;
import com.discord.simpleast.core.parser.Rule;
import com.discord.utilities.user.UserUtils;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.g0.s;
import d0.t.n;
import d0.z.d.m;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kotlin.Metadata;
/* compiled from: MessageUnparser.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\t\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b&\u0010'JQ\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u00062\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\n0\u00062\u0006\u0010\r\u001a\u00020\fH\u0007¢\u0006\u0004\b\u000f\u0010\u0010JG\u0010\u0015\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0014\u0012\u0004\u0012\u00028\u00010\u0013\"\u0004\b\u0000\u0010\u0011\"\u0004\b\u0001\u0010\u00122\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\n0\u0006H\u0002¢\u0006\u0004\b\u0015\u0010\u0016JA\u0010\u001a\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0014\u0012\u0004\u0012\u00028\u00010\u0013\"\u0004\b\u0000\u0010\u0011\"\u0004\b\u0001\u0010\u00122\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00180\u0017H\u0002¢\u0006\u0004\b\u001a\u0010\u001bJG\u0010\u001c\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0014\u0012\u0004\u0012\u00028\u00010\u0013\"\u0004\b\u0000\u0010\u0011\"\u0004\b\u0001\u0010\u00122\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006H\u0002¢\u0006\u0004\b\u001c\u0010\u0016J;\u0010\u001d\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0014\u0012\u0004\u0012\u00028\u00010\u0013\"\u0004\b\u0000\u0010\u0011\"\u0004\b\u0001\u0010\u00122\u0006\u0010\r\u001a\u00020\fH\u0002¢\u0006\u0004\b\u001d\u0010\u001eR\u001e\u0010!\u001a\n  *\u0004\u0018\u00010\u001f0\u001f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"R\u001e\u0010#\u001a\n  *\u0004\u0018\u00010\u001f0\u001f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010\"R\u001e\u0010$\u001a\n  *\u0004\u0018\u00010\u001f0\u001f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010\"R\u001e\u0010%\u001a\n  *\u0004\u0018\u00010\u001f0\u001f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b%\u0010\"¨\u0006("}, d2 = {"Lcom/discord/utilities/textprocessing/MessageUnparser;", "", "", "rawMessageContent", "Lcom/discord/models/guild/Guild;", "guild", "", "", "Lcom/discord/api/channel/Channel;", "channels", "Lcom/discord/models/user/User;", "users", "Lcom/discord/models/domain/emoji/EmojiSet;", "emojiSet", "", "unparse", "(Ljava/lang/String;Lcom/discord/models/guild/Guild;Ljava/util/Map;Ljava/util/Map;Lcom/discord/models/domain/emoji/EmojiSet;)Ljava/lang/CharSequence;", ExifInterface.GPS_DIRECTION_TRUE, ExifInterface.LATITUDE_SOUTH, "Lcom/discord/simpleast/core/parser/Rule;", "Lcom/discord/simpleast/core/node/Node;", "getUserMentionRule", "(Ljava/util/Map;)Lcom/discord/simpleast/core/parser/Rule;", "", "Lcom/discord/api/role/GuildRole;", "guildRoles", "getRoleMentionRule", "(Ljava/util/List;)Lcom/discord/simpleast/core/parser/Rule;", "getChannelMentionRule", "getCustomEmojiRule", "(Lcom/discord/models/domain/emoji/EmojiSet;)Lcom/discord/simpleast/core/parser/Rule;", "Ljava/util/regex/Pattern;", "kotlin.jvm.PlatformType", "PATTERN_CUSTOM_EMOJI", "Ljava/util/regex/Pattern;", "PATTERN_USER_MENTION", "PATTERN_CHANNEL_MENTION", "PATTERN_ROLE_MENTION", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MessageUnparser {
    public static final MessageUnparser INSTANCE = new MessageUnparser();
    private static final Pattern PATTERN_USER_MENTION = Pattern.compile("^<@!?(\\d+)>");
    private static final Pattern PATTERN_ROLE_MENTION = Pattern.compile("^<@&?(\\d+)>");
    private static final Pattern PATTERN_CHANNEL_MENTION = Pattern.compile("^<#?(\\d+)>");
    private static final Pattern PATTERN_CUSTOM_EMOJI = Pattern.compile("^<(a)?:(\\w+):(\\d+)>");

    private MessageUnparser() {
    }

    private final <T, S> Rule<T, Node<T>, S> getChannelMentionRule(final Map<Long, Channel> map) {
        final Pattern pattern = PATTERN_CHANNEL_MENTION;
        m.checkNotNullExpressionValue(pattern, "PATTERN_CHANNEL_MENTION");
        return new Rule<T, Node<T>, S>(pattern) { // from class: com.discord.utilities.textprocessing.MessageUnparser$getChannelMentionRule$1
            @Override // com.discord.simpleast.core.parser.Rule
            public ParseSpec<T, S> parse(Matcher matcher, Parser<T, ? super Node<T>, S> parser, S s2) {
                String str;
                Long longOrNull;
                m.checkNotNullParameter(matcher, "matcher");
                m.checkNotNullParameter(parser, "parser");
                String group = matcher.group(1);
                Channel channel = (Channel) map.get(Long.valueOf((group == null || (longOrNull = s.toLongOrNull(group)) == null) ? -1L : longOrNull.longValue()));
                if (channel != null) {
                    StringBuilder O = a.O(MentionUtilsKt.CHANNELS_CHAR);
                    O.append(ChannelUtils.c(channel));
                    str = O.toString();
                } else {
                    str = matcher.group();
                }
                m.checkNotNullExpressionValue(str, "content");
                b.a.t.b.a.a aVar = new b.a.t.b.a.a(str);
                m.checkNotNullParameter(aVar, "node");
                return new ParseSpec<>(aVar, s2);
            }
        };
    }

    private final <T, S> Rule<T, Node<T>, S> getCustomEmojiRule(final EmojiSet emojiSet) {
        final Pattern pattern = PATTERN_CUSTOM_EMOJI;
        m.checkNotNullExpressionValue(pattern, "PATTERN_CUSTOM_EMOJI");
        return new Rule<T, Node<T>, S>(pattern) { // from class: com.discord.utilities.textprocessing.MessageUnparser$getCustomEmojiRule$1
            @Override // com.discord.simpleast.core.parser.Rule
            public ParseSpec<T, S> parse(Matcher matcher, Parser<T, ? super Node<T>, S> parser, S s2) {
                String str;
                m.checkNotNullParameter(matcher, "matcher");
                m.checkNotNullParameter(parser, "parser");
                Emoji emoji = emojiSet.emojiIndex.get(matcher.group(3));
                if (emoji != null) {
                    str = emoji.getFirstName();
                } else {
                    str = matcher.group(2);
                }
                b.a.t.b.a.a aVar = new b.a.t.b.a.a(MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR + str + MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR);
                m.checkNotNullParameter(aVar, "node");
                return new ParseSpec<>(aVar, s2);
            }
        };
    }

    private final <T, S> Rule<T, Node<T>, S> getRoleMentionRule(final List<GuildRole> list) {
        final Pattern pattern = PATTERN_ROLE_MENTION;
        m.checkNotNullExpressionValue(pattern, "PATTERN_ROLE_MENTION");
        return new Rule<T, Node<T>, S>(pattern) { // from class: com.discord.utilities.textprocessing.MessageUnparser$getRoleMentionRule$1
            @Override // com.discord.simpleast.core.parser.Rule
            public ParseSpec<T, S> parse(Matcher matcher, Parser<T, ? super Node<T>, S> parser, S s2) {
                Object obj;
                String str;
                boolean z2;
                Long longOrNull;
                m.checkNotNullParameter(matcher, "matcher");
                m.checkNotNullParameter(parser, "parser");
                String group = matcher.group(1);
                long longValue = (group == null || (longOrNull = s.toLongOrNull(group)) == null) ? -1L : longOrNull.longValue();
                Iterator it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj = null;
                        break;
                    }
                    obj = it.next();
                    if (((GuildRole) obj).getId() == longValue) {
                        z2 = true;
                        continue;
                    } else {
                        z2 = false;
                        continue;
                    }
                    if (z2) {
                        break;
                    }
                }
                GuildRole guildRole = (GuildRole) obj;
                if (guildRole != null) {
                    StringBuilder O = a.O(MentionUtilsKt.MENTIONS_CHAR);
                    O.append(guildRole.g());
                    str = O.toString();
                } else {
                    str = matcher.group();
                }
                m.checkNotNullExpressionValue(str, "content");
                b.a.t.b.a.a aVar = new b.a.t.b.a.a(str);
                m.checkNotNullParameter(aVar, "node");
                return new ParseSpec<>(aVar, s2);
            }
        };
    }

    private final <T, S> Rule<T, Node<T>, S> getUserMentionRule(final Map<Long, ? extends User> map) {
        final Pattern pattern = PATTERN_USER_MENTION;
        m.checkNotNullExpressionValue(pattern, "PATTERN_USER_MENTION");
        return new Rule<T, Node<T>, S>(pattern) { // from class: com.discord.utilities.textprocessing.MessageUnparser$getUserMentionRule$1
            @Override // com.discord.simpleast.core.parser.Rule
            public ParseSpec<T, S> parse(Matcher matcher, Parser<T, ? super Node<T>, S> parser, S s2) {
                String str;
                Long longOrNull;
                m.checkNotNullParameter(matcher, "matcher");
                m.checkNotNullParameter(parser, "parser");
                String group = matcher.group(1);
                User user = (User) map.get(Long.valueOf((group == null || (longOrNull = s.toLongOrNull(group)) == null) ? -1L : longOrNull.longValue()));
                if (user != null) {
                    StringBuilder O = a.O(MentionUtilsKt.MENTIONS_CHAR);
                    O.append(user.getUsername());
                    O.append(UserUtils.INSTANCE.getDiscriminatorWithPadding(user));
                    str = O.toString();
                } else {
                    str = matcher.group();
                }
                m.checkNotNullExpressionValue(str, "content");
                b.a.t.b.a.a aVar = new b.a.t.b.a.a(str);
                m.checkNotNullParameter(aVar, "node");
                return new ParseSpec<>(aVar, s2);
            }
        };
    }

    public static final CharSequence unparse(String str, Guild guild, Map<Long, Channel> map, Map<Long, ? extends User> map2, EmojiSet emojiSet) {
        List<GuildRole> list;
        m.checkNotNullParameter(str, "rawMessageContent");
        m.checkNotNullParameter(map, "channels");
        m.checkNotNullParameter(map2, "users");
        m.checkNotNullParameter(emojiSet, "emojiSet");
        Parser parser = new Parser(false, 1, null);
        MessageUnparser messageUnparser = INSTANCE;
        Parser addRule = parser.addRule(messageUnparser.getUserMentionRule(map2));
        if (guild == null || (list = guild.getRoles()) == null) {
            list = n.emptyList();
        }
        return AstRenderer.render(Parser.parse$default(addRule.addRule(messageUnparser.getRoleMentionRule(list)).addRule(messageUnparser.getChannelMentionRule(map)).addRule(messageUnparser.getCustomEmojiRule(emojiSet)).addRule(e.h.d()), str, null, null, 4, null), null);
    }
}
