package com.discord.utilities.textprocessing;

import andhook.lib.HookHelper;
import android.content.Context;
import b.a.t.b.b.b;
import b.a.t.b.b.e;
import com.discord.simpleast.core.node.Node;
import com.discord.simpleast.core.parser.Parser;
import com.discord.utilities.textprocessing.node.EditedMessageNode;
import com.discord.utilities.textprocessing.node.ZeroSpaceWidthNode;
import com.facebook.drawee.span.DraweeSpanStringBuilder;
import d0.z.d.m;
import java.util.List;
import java.util.regex.Pattern;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
/* compiled from: DiscordParser.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001\u001fB\t\b\u0002¢\u0006\u0004\b\u001d\u0010\u001eJA\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\fH\u0007¢\u0006\u0004\b\u000f\u0010\u0010JA\u0010\u0017\u001a\u001a\u0012\u0004\u0012\u00020\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u0015\u0012\u0004\u0012\u00020\u00160\u00142\u0006\u0010\u0011\u001a\u00020\f2\u0006\u0010\u0012\u001a\u00020\f2\b\b\u0002\u0010\u0013\u001a\u00020\fH\u0007¢\u0006\u0004\b\u0017\u0010\u0018R.\u0010\u0019\u001a\u001a\u0012\u0004\u0012\u00020\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u0015\u0012\u0004\u0012\u00020\u00160\u00148\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR.\u0010\u001b\u001a\u001a\u0012\u0004\u0012\u00020\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u0015\u0012\u0004\u0012\u00020\u00160\u00148\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001aR.\u0010\u001c\u001a\u001a\u0012\u0004\u0012\u00020\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u0015\u0012\u0004\u0012\u00020\u00160\u00148\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001a¨\u0006 "}, d2 = {"Lcom/discord/utilities/textprocessing/DiscordParser;", "", "Landroid/content/Context;", "context", "", "messageText", "Lcom/discord/utilities/textprocessing/MessageRenderContext;", "messageRenderContext", "Lcom/discord/utilities/textprocessing/MessagePreprocessor;", "preprocessor", "Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;", "parserOptions", "", "isEdited", "Lcom/facebook/drawee/span/DraweeSpanStringBuilder;", "parseChannelMessage", "(Landroid/content/Context;Ljava/lang/String;Lcom/discord/utilities/textprocessing/MessageRenderContext;Lcom/discord/utilities/textprocessing/MessagePreprocessor;Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;Z)Lcom/facebook/drawee/span/DraweeSpanStringBuilder;", "allowMaskedLinks", "supportEntityMentions", "renderBlockQuotes", "Lcom/discord/simpleast/core/parser/Parser;", "Lcom/discord/simpleast/core/node/Node;", "Lcom/discord/utilities/textprocessing/MessageParseState;", "createParser", "(ZZZ)Lcom/discord/simpleast/core/parser/Parser;", "SAFE_LINK_PARSER", "Lcom/discord/simpleast/core/parser/Parser;", "MASKED_LINK_PARSER", "REPLY_PARSER", HookHelper.constructorName, "()V", "ParserOptions", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class DiscordParser {
    public static final DiscordParser INSTANCE = new DiscordParser();
    private static final Parser<MessageRenderContext, Node<MessageRenderContext>, MessageParseState> SAFE_LINK_PARSER = createParser$default(false, true, false, 4, null);
    private static final Parser<MessageRenderContext, Node<MessageRenderContext>, MessageParseState> MASKED_LINK_PARSER = createParser$default(true, true, false, 4, null);
    private static final Parser<MessageRenderContext, Node<MessageRenderContext>, MessageParseState> REPLY_PARSER = createParser(false, true, false);

    /* compiled from: DiscordParser.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/utilities/textprocessing/DiscordParser$ParserOptions;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "DEFAULT", "ALLOW_MASKED_LINKS", "REPLY", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum ParserOptions {
        DEFAULT,
        ALLOW_MASKED_LINKS,
        REPLY
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            ParserOptions.values();
            int[] iArr = new int[3];
            $EnumSwitchMapping$0 = iArr;
            iArr[ParserOptions.DEFAULT.ordinal()] = 1;
            iArr[ParserOptions.ALLOW_MASKED_LINKS.ordinal()] = 2;
            iArr[ParserOptions.REPLY.ordinal()] = 3;
        }
    }

    private DiscordParser() {
    }

    public static final Parser<MessageRenderContext, Node<MessageRenderContext>, MessageParseState> createParser(boolean z2, boolean z3, boolean z4) {
        Parser<MessageRenderContext, Node<MessageRenderContext>, MessageParseState> parser = new Parser<>(false, 1, null);
        Rules rules = Rules.INSTANCE;
        parser.addRule(rules.createSoftHyphenRule());
        e eVar = e.h;
        Pattern pattern = e.f;
        m.checkNotNullExpressionValue(pattern, "PATTERN_ESCAPE");
        parser.addRule(new b(eVar, pattern));
        if (z4) {
            parser.addRule(rules.createBlockQuoteRule());
        }
        parser.addRule(rules.createCodeBlockRule());
        parser.addRule(rules.createInlineCodeRule());
        parser.addRule(rules.createSpoilerRule());
        if (z2) {
            parser.addRule(rules.createMaskedLinkRule());
        }
        parser.addRule(rules.createUrlNoEmbedRule());
        parser.addRule(rules.createUrlRule());
        parser.addRule(rules.createCustomEmojiRule());
        parser.addRule(rules.createNamedEmojiRule());
        parser.addRule(rules.createUnescapeEmoticonRule());
        if (z3) {
            parser.addRule(rules.createChannelMentionRule());
            parser.addRule(rules.createRoleMentionRule());
            parser.addRule(rules.createUserMentionRule());
        }
        parser.addRule(rules.createUnicodeEmojiRule());
        parser.addRule(rules.createTimestampRule());
        parser.addRules(e.a(false, false));
        parser.addRule(rules.createTextReplacementRule());
        return parser;
    }

    public static /* synthetic */ Parser createParser$default(boolean z2, boolean z3, boolean z4, int i, Object obj) {
        if ((i & 4) != 0) {
            z4 = true;
        }
        return createParser(z2, z3, z4);
    }

    public static final DraweeSpanStringBuilder parseChannelMessage(Context context, String str, MessageRenderContext messageRenderContext, MessagePreprocessor messagePreprocessor, ParserOptions parserOptions, boolean z2) {
        Parser<MessageRenderContext, Node<MessageRenderContext>, MessageParseState> parser;
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(messageRenderContext, "messageRenderContext");
        m.checkNotNullParameter(messagePreprocessor, "preprocessor");
        m.checkNotNullParameter(parserOptions, "parserOptions");
        int ordinal = parserOptions.ordinal();
        if (ordinal == 0) {
            parser = SAFE_LINK_PARSER;
        } else if (ordinal == 1) {
            parser = MASKED_LINK_PARSER;
        } else if (ordinal == 2) {
            parser = REPLY_PARSER;
        } else {
            throw new NoWhenBranchMatchedException();
        }
        Parser<MessageRenderContext, Node<MessageRenderContext>, MessageParseState> parser2 = parser;
        if (str == null) {
            str = "";
        }
        List parse$default = Parser.parse$default(parser2, str, MessageParseState.Companion.getInitialState(), null, 4, null);
        messagePreprocessor.process(parse$default);
        if (z2) {
            parse$default.add(new EditedMessageNode(context));
        }
        parse$default.add(new ZeroSpaceWidthNode());
        return AstRenderer.render(parse$default, messageRenderContext);
    }
}
