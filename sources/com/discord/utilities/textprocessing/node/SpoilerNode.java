package com.discord.utilities.textprocessing.node;

import andhook.lib.HookHelper;
import android.content.Context;
import android.text.SpannableStringBuilder;
import androidx.exifinterface.media.ExifInterface;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.simpleast.core.node.Node;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.spans.ClickableSpan;
import com.discord.utilities.textprocessing.node.SpoilerNode.RenderContext;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: SpoilerNode.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0010\u0018\u0000*\b\b\u0000\u0010\u0002*\u00020\u00012\b\u0012\u0004\u0012\u00028\u00000\u00032\u00020\u0004:\u0001$B\u000f\u0012\u0006\u0010\u0016\u001a\u00020\u0015¢\u0006\u0004\b\"\u0010#J\u001d\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0005\u001a\u00028\u0000H\u0002¢\u0006\u0004\b\b\u0010\tJ\u001d\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\u0013\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0005\u001a\u00028\u0000H\u0016¢\u0006\u0004\b\u0013\u0010\u0014R\u0019\u0010\u0016\u001a\u00020\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R$\u0010\u000b\u001a\u00020\n2\u0006\u0010\u001a\u001a\u00020\n8\u0006@BX\u0086\u000e¢\u0006\f\n\u0004\b\u000b\u0010\u001b\u001a\u0004\b\u001c\u0010\u001dR\"\u0010\r\u001a\u00020\f8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b\r\u0010\u001e\u001a\u0004\b\r\u0010\u001f\"\u0004\b \u0010!¨\u0006%"}, d2 = {"Lcom/discord/utilities/textprocessing/node/SpoilerNode;", "Lcom/discord/utilities/textprocessing/node/SpoilerNode$RenderContext;", ExifInterface.GPS_DIRECTION_TRUE, "Lcom/discord/simpleast/core/node/Node;", "Lcom/discord/utilities/textprocessing/node/Spoilerable;", "renderContext", "", "", "createStyles", "(Lcom/discord/utilities/textprocessing/node/SpoilerNode$RenderContext;)Ljava/util/List;", "", ModelAuditLogEntry.CHANGE_KEY_ID, "", "isRevealed", "", "updateState", "(IZ)V", "Landroid/text/SpannableStringBuilder;", "builder", "render", "(Landroid/text/SpannableStringBuilder;Lcom/discord/utilities/textprocessing/node/SpoilerNode$RenderContext;)V", "", "content", "Ljava/lang/String;", "getContent", "()Ljava/lang/String;", "<set-?>", "I", "getId", "()I", "Z", "()Z", "setRevealed", "(Z)V", HookHelper.constructorName, "(Ljava/lang/String;)V", "RenderContext", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SpoilerNode<T extends RenderContext> extends Node<T> implements Spoilerable {
    private final String content;

    /* renamed from: id  reason: collision with root package name */
    private int f2794id = -1;
    private boolean isRevealed;

    /* compiled from: SpoilerNode.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\bf\u0018\u00002\u00020\u0001R\u0016\u0010\u0005\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004R(\u0010\u000b\u001a\u0014\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u0007\u0012\u0004\u0012\u00020\b\u0018\u00010\u00068&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\nR\u0016\u0010\r\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\u0004¨\u0006\u000e"}, d2 = {"Lcom/discord/utilities/textprocessing/node/SpoilerNode$RenderContext;", "Lcom/discord/utilities/textprocessing/node/BasicRenderContext;", "", "getSpoilerColorRes", "()I", "spoilerColorRes", "Lkotlin/Function1;", "Lcom/discord/utilities/textprocessing/node/SpoilerNode;", "", "getSpoilerOnClick", "()Lkotlin/jvm/functions/Function1;", "spoilerOnClick", "getSpoilerRevealedColorRes", "spoilerRevealedColorRes", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface RenderContext extends BasicRenderContext {
        int getSpoilerColorRes();

        Function1<SpoilerNode<?>, Unit> getSpoilerOnClick();

        int getSpoilerRevealedColorRes();
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SpoilerNode(String str) {
        super(null, 1, null);
        m.checkNotNullParameter(str, "content");
        this.content = str;
    }

    private final List<Object> createStyles(T t) {
        Context context = t.getContext();
        SpoilerSpan spoilerSpan = new SpoilerSpan(t.getSpoilerColorRes(), t.getSpoilerRevealedColorRes(), isRevealed());
        if (isRevealed()) {
            return d0.t.m.listOf(spoilerSpan);
        }
        Object[] objArr = new Object[2];
        objArr[0] = spoilerSpan;
        Function1<SpoilerNode<?>, Unit> spoilerOnClick = t.getSpoilerOnClick();
        objArr[1] = spoilerOnClick != null ? new ClickableSpan(Integer.valueOf(ColorCompat.getColor(context, (int) R.color.transparent)), false, null, new SpoilerNode$createStyles$$inlined$let$lambda$1(spoilerOnClick, this, context), 4, null) : null;
        return n.listOfNotNull(objArr);
    }

    public final String getContent() {
        return this.content;
    }

    public final int getId() {
        return this.f2794id;
    }

    @Override // com.discord.utilities.textprocessing.node.Spoilerable
    public boolean isRevealed() {
        return this.isRevealed;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.discord.simpleast.core.node.Node
    public /* bridge */ /* synthetic */ void render(SpannableStringBuilder spannableStringBuilder, Object obj) {
        render(spannableStringBuilder, (SpannableStringBuilder) ((RenderContext) obj));
    }

    @Override // com.discord.utilities.textprocessing.node.Spoilerable
    public void setRevealed(boolean z2) {
        this.isRevealed = z2;
    }

    public final void updateState(int i, boolean z2) {
        this.f2794id = i;
        setRevealed(z2);
    }

    public void render(SpannableStringBuilder spannableStringBuilder, T t) {
        m.checkNotNullParameter(spannableStringBuilder, "builder");
        m.checkNotNullParameter(t, "renderContext");
        int length = spannableStringBuilder.length();
        Iterable<Node> children = getChildren();
        if (children != null) {
            for (Node node : children) {
                node.render(spannableStringBuilder, t);
            }
        }
        if (!isRevealed()) {
            Object[] spans = spannableStringBuilder.getSpans(length, spannableStringBuilder.length(), ClickableSpan.class);
            m.checkNotNullExpressionValue(spans, "builder.getSpans(startIn…lickableSpan::class.java)");
            for (Object obj : spans) {
                spannableStringBuilder.removeSpan((ClickableSpan) obj);
            }
        }
        for (Object obj2 : createStyles(t)) {
            spannableStringBuilder.setSpan(obj2, length, spannableStringBuilder.length(), 33);
        }
    }
}
