package com.discord.utilities.textprocessing.node;

import andhook.lib.HookHelper;
import android.content.Context;
import android.text.style.CharacterStyle;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import com.discord.simpleast.core.node.StyleNode;
import com.discord.utilities.color.ColorCompat;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: EditedMessageNode.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \b*\u0004\b\u0000\u0010\u00012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00030\u0002:\u0001\bB\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\t"}, d2 = {"Lcom/discord/utilities/textprocessing/node/EditedMessageNode;", "RC", "Lcom/discord/simpleast/core/node/StyleNode;", "Landroid/text/style/CharacterStyle;", "Landroid/content/Context;", "context", HookHelper.constructorName, "(Landroid/content/Context;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class EditedMessageNode<RC> extends StyleNode<RC, CharacterStyle> {
    public static final Companion Companion = new Companion(null);

    /* compiled from: EditedMessageNode.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u0019\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\n\u001a\u00020\t2\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0015\u0010\r\u001a\u00020\f2\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lcom/discord/utilities/textprocessing/node/EditedMessageNode$Companion;", "", "", "relativeSize", "Landroid/text/style/RelativeSizeSpan;", "getRelativeSizeSpan", "(F)Landroid/text/style/RelativeSizeSpan;", "Landroid/content/Context;", "context", "Landroid/text/style/ForegroundColorSpan;", "getForegroundColorSpan", "(Landroid/content/Context;)Landroid/text/style/ForegroundColorSpan;", "", "getEditedString", "(Landroid/content/Context;)Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final ForegroundColorSpan getForegroundColorSpan(Context context) {
            return new ForegroundColorSpan(ColorCompat.getThemedColor(context, (int) R.attr.colorTextMuted));
        }

        private final RelativeSizeSpan getRelativeSizeSpan(float f) {
            return new RelativeSizeSpan(f);
        }

        public static /* synthetic */ RelativeSizeSpan getRelativeSizeSpan$default(Companion companion, float f, int i, Object obj) {
            if ((i & 1) != 0) {
                f = 0.75f;
            }
            return companion.getRelativeSizeSpan(f);
        }

        public final String getEditedString(Context context) {
            m.checkNotNullParameter(context, "context");
            String string = context.getString(R.string.message_edited);
            m.checkNotNullExpressionValue(string, "context.getString(R.string.message_edited)");
            return " (" + string + ')';
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public EditedMessageNode(android.content.Context r6) {
        /*
            r5 = this;
            java.lang.String r0 = "context"
            d0.z.d.m.checkNotNullParameter(r6, r0)
            r0 = 2
            java.lang.Object[] r0 = new java.lang.Object[r0]
            com.discord.utilities.textprocessing.node.EditedMessageNode$Companion r1 = com.discord.utilities.textprocessing.node.EditedMessageNode.Companion
            r2 = 0
            r3 = 1
            r4 = 0
            android.text.style.RelativeSizeSpan r2 = com.discord.utilities.textprocessing.node.EditedMessageNode.Companion.getRelativeSizeSpan$default(r1, r2, r3, r4)
            r4 = 0
            r0[r4] = r2
            android.text.style.ForegroundColorSpan r2 = com.discord.utilities.textprocessing.node.EditedMessageNode.Companion.access$getForegroundColorSpan(r1, r6)
            r0[r3] = r2
            java.util.List r0 = d0.t.n.listOf(r0)
            r5.<init>(r0)
            b.a.t.b.a.a r0 = new b.a.t.b.a.a
            java.lang.String r6 = r1.getEditedString(r6)
            r0.<init>(r6)
            r5.addChild(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.textprocessing.node.EditedMessageNode.<init>(android.content.Context):void");
    }
}
