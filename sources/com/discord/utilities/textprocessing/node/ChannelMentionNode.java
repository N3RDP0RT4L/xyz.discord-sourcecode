package com.discord.utilities.textprocessing.node;

import andhook.lib.HookHelper;
import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.style.BackgroundColorSpan;
import android.text.style.CharacterStyle;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import androidx.exifinterface.media.ExifInterface;
import com.discord.api.channel.Channel;
import com.discord.simpleast.core.node.Node;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.spans.ClickableSpan;
import com.discord.utilities.textprocessing.node.ChannelMentionNode.RenderContext;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: ChannelMentionNode.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000*\b\b\u0000\u0010\u0002*\u00020\u00012\b\u0012\u0004\u0012\u00028\u00000\u0003:\u0001\u0017B\u0013\u0012\n\u0010\u0011\u001a\u00060\u000fj\u0002`\u0010¢\u0006\u0004\b\u0015\u0010\u0016J\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00028\u0000H\u0016¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\r\u001a\u00020\f2\b\u0010\u000b\u001a\u0004\u0018\u00010\nH\u0096\u0002¢\u0006\u0004\b\r\u0010\u000eR\u001d\u0010\u0011\u001a\u00060\u000fj\u0002`\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014¨\u0006\u0018"}, d2 = {"Lcom/discord/utilities/textprocessing/node/ChannelMentionNode;", "Lcom/discord/utilities/textprocessing/node/ChannelMentionNode$RenderContext;", ExifInterface.GPS_DIRECTION_TRUE, "Lcom/discord/simpleast/core/node/Node;", "Landroid/text/SpannableStringBuilder;", "builder", "renderContext", "", "render", "(Landroid/text/SpannableStringBuilder;Lcom/discord/utilities/textprocessing/node/ChannelMentionNode$RenderContext;)V", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "Lcom/discord/primitives/ChannelId;", "channelId", "J", "getChannelId", "()J", HookHelper.constructorName, "(J)V", "RenderContext", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ChannelMentionNode<T extends RenderContext> extends Node<T> {
    private final long channelId;

    /* compiled from: ChannelMentionNode.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001R(\u0010\b\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R(\u0010\r\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\n\u0018\u00010\t8&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\f¨\u0006\u000e"}, d2 = {"Lcom/discord/utilities/textprocessing/node/ChannelMentionNode$RenderContext;", "Lcom/discord/utilities/textprocessing/node/BasicRenderContext;", "", "", "Lcom/discord/primitives/ChannelId;", "", "getChannelNames", "()Ljava/util/Map;", "channelNames", "Lkotlin/Function1;", "", "getChannelMentionOnClick", "()Lkotlin/jvm/functions/Function1;", "channelMentionOnClick", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface RenderContext extends BasicRenderContext {
        Function1<Long, Unit> getChannelMentionOnClick();

        Map<Long, String> getChannelNames();
    }

    public ChannelMentionNode(long j) {
        super(null, 1, null);
        this.channelId = j;
    }

    public boolean equals(Object obj) {
        return (obj instanceof ChannelMentionNode) && ((ChannelMentionNode) obj).channelId == this.channelId;
    }

    public final long getChannelId() {
        return this.channelId;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.discord.simpleast.core.node.Node
    public /* bridge */ /* synthetic */ void render(SpannableStringBuilder spannableStringBuilder, Object obj) {
        render(spannableStringBuilder, (SpannableStringBuilder) ((RenderContext) obj));
    }

    public void render(SpannableStringBuilder spannableStringBuilder, T t) {
        String str;
        m.checkNotNullParameter(spannableStringBuilder, "builder");
        m.checkNotNullParameter(t, "renderContext");
        Context context = t.getContext();
        int length = spannableStringBuilder.length();
        ArrayList arrayList = new ArrayList(3);
        arrayList.add(new StyleSpan(1));
        Map<Long, String> channelNames = t.getChannelNames();
        if (channelNames == null || (str = channelNames.get(Long.valueOf(this.channelId))) == null) {
            str = "deleted-channel";
        }
        Function1<Long, Unit> channelMentionOnClick = t.getChannelMentionOnClick();
        if (channelMentionOnClick != null) {
            arrayList.add(new ClickableSpan(Integer.valueOf(ColorCompat.getThemedColor(context, (int) R.attr.theme_chat_mention_foreground)), false, null, new ChannelMentionNode$render$1(this, channelMentionOnClick), 4, null));
        } else {
            arrayList.add(new ForegroundColorSpan(ColorCompat.getThemedColor(context, (int) R.attr.theme_chat_mention_foreground)));
        }
        arrayList.add(new BackgroundColorSpan(ColorCompat.getThemedColor(context, (int) R.attr.theme_chat_mention_background)));
        StringBuilder sb = new StringBuilder();
        m.checkNotNullParameter(Channel.Companion, "$this$DISPLAY_PREFIX_GUILD");
        sb.append("#");
        sb.append(str);
        spannableStringBuilder.append((CharSequence) sb.toString());
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            spannableStringBuilder.setSpan((CharacterStyle) it.next(), length, spannableStringBuilder.length(), 33);
        }
    }
}
