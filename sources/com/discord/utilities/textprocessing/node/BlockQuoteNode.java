package com.discord.utilities.textprocessing.node;

import andhook.lib.HookHelper;
import android.text.SpannableStringBuilder;
import android.text.style.AbsoluteSizeSpan;
import androidx.exifinterface.media.ExifInterface;
import com.discord.simpleast.core.node.Node;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.spans.QuoteSpan;
import com.discord.utilities.textprocessing.node.BasicRenderContext;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: BlockQuoteNode.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\u0018\u0000 \f*\b\b\u0000\u0010\u0002*\u00020\u00012\b\u0012\u0004\u0012\u00028\u00000\u0003:\u0001\fB\u0007¢\u0006\u0004\b\n\u0010\u000bJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00028\u0000H\u0016¢\u0006\u0004\b\b\u0010\t¨\u0006\r"}, d2 = {"Lcom/discord/utilities/textprocessing/node/BlockQuoteNode;", "Lcom/discord/utilities/textprocessing/node/BasicRenderContext;", ExifInterface.GPS_DIRECTION_TRUE, "Lcom/discord/simpleast/core/node/Node;", "Landroid/text/SpannableStringBuilder;", "builder", "renderContext", "", "render", "(Landroid/text/SpannableStringBuilder;Lcom/discord/utilities/textprocessing/node/BasicRenderContext;)V", HookHelper.constructorName, "()V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class BlockQuoteNode<T extends BasicRenderContext> extends Node<T> {
    public static final Companion Companion = new Companion(null);
    private static final int GAP_WIDTH;
    private static final int STRIPE_WIDTH;
    private static final int TOTAL_LEFT_MARGIN;

    /* compiled from: BlockQuoteNode.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\t\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0007\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0007\u0010\u0004R\u0016\u0010\b\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\u0004¨\u0006\u000b"}, d2 = {"Lcom/discord/utilities/textprocessing/node/BlockQuoteNode$Companion;", "", "", "TOTAL_LEFT_MARGIN", "I", "getTOTAL_LEFT_MARGIN", "()I", "GAP_WIDTH", "STRIPE_WIDTH", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final int getTOTAL_LEFT_MARGIN() {
            return BlockQuoteNode.TOTAL_LEFT_MARGIN;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    static {
        int dpToPixels = DimenUtils.dpToPixels(3);
        STRIPE_WIDTH = dpToPixels;
        int dpToPixels2 = DimenUtils.dpToPixels(8);
        GAP_WIDTH = dpToPixels2;
        TOTAL_LEFT_MARGIN = dpToPixels + dpToPixels2;
    }

    public BlockQuoteNode() {
        super(null, 1, null);
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.discord.simpleast.core.node.Node
    public /* bridge */ /* synthetic */ void render(SpannableStringBuilder spannableStringBuilder, Object obj) {
        render(spannableStringBuilder, (SpannableStringBuilder) ((BasicRenderContext) obj));
    }

    public void render(SpannableStringBuilder spannableStringBuilder, T t) {
        m.checkNotNullParameter(spannableStringBuilder, "builder");
        m.checkNotNullParameter(t, "renderContext");
        spannableStringBuilder.append('\n');
        int dpToPixels = DimenUtils.dpToPixels(8);
        spannableStringBuilder.setSpan(new AbsoluteSizeSpan(dpToPixels), spannableStringBuilder.length() - 1, spannableStringBuilder.length(), 33);
        int length = spannableStringBuilder.length();
        Iterable<Node> children = getChildren();
        if (children != null) {
            for (Node node : children) {
                node.render(spannableStringBuilder, t);
            }
        }
        if (spannableStringBuilder.length() == length) {
            spannableStringBuilder.append(' ');
        }
        spannableStringBuilder.setSpan(new QuoteSpan(ColorCompat.getThemedColor(t.getContext(), (int) R.attr.theme_chat_block_quote_divider), STRIPE_WIDTH, GAP_WIDTH), length, spannableStringBuilder.length(), 13107233);
        spannableStringBuilder.append('\n');
        spannableStringBuilder.setSpan(new AbsoluteSizeSpan(dpToPixels), spannableStringBuilder.length() - 1, spannableStringBuilder.length(), 13107233);
    }
}
