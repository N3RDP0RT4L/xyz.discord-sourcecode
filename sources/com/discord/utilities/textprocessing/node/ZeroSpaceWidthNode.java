package com.discord.utilities.textprocessing.node;

import andhook.lib.HookHelper;
import androidx.exifinterface.media.ExifInterface;
import b.a.t.b.a.a;
import kotlin.Metadata;
/* compiled from: ZeroSpaceWidthNode.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u00028\u00000\u0002B\u0007¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/utilities/textprocessing/node/ZeroSpaceWidthNode;", ExifInterface.GPS_DIRECTION_TRUE, "Lb/a/t/b/a/a;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ZeroSpaceWidthNode<T> extends a<T> {
    public ZeroSpaceWidthNode() {
        super("\u200b");
    }
}
