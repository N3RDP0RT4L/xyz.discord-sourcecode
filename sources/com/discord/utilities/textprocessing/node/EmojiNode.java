package com.discord.utilities.textprocessing.node;

import a0.a.a.b;
import andhook.lib.HookHelper;
import andhook.lib.xposed.ClassUtils;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.SpannableStringBuilder;
import androidx.exifinterface.media.ExifInterface;
import b.a.t.b.a.a;
import b.f.g.a.a.d;
import b.f.g.e.v;
import com.discord.api.activity.ActivityEmoji;
import com.discord.api.message.reaction.MessageReactionEmoji;
import com.discord.app.AppLog;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.emoji.ModelEmojiCustom;
import com.discord.models.domain.emoji.ModelEmojiUnicode;
import com.discord.stores.StoreStream;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.spans.ClickableSpan;
import com.discord.utilities.textprocessing.node.EmojiNode;
import com.discord.utilities.textprocessing.node.EmojiNode.RenderContext;
import com.discord.utilities.textprocessing.node.SpoilerNode;
import com.discord.utilities.view.text.SimpleDraweeSpanTextView;
import com.facebook.drawee.controller.AbstractDraweeController;
import com.facebook.drawee.drawable.ScalingUtils$ScaleType;
import com.facebook.drawee.span.DraweeSpanStringBuilder;
import d0.z.d.m;
import java.io.Serializable;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: EmojiNode.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\f\u0018\u0000 &*\b\b\u0000\u0010\u0002*\u00020\u00012\b\u0012\u0004\u0012\u00028\u00000\u00032\u00020\u0004:\u0003&'(BM\b\u0007\u0012\u0006\u0010#\u001a\u00020\u001d\u0012\u001e\u0010\u001e\u001a\u001a\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u001d0\u001b\u0012\u0006\u0010\u0016\u001a\u00020\u0015\u0012\b\b\u0002\u0010\"\u001a\u00020\u0018\u0012\b\b\u0002\u0010\u0019\u001a\u00020\u0018¢\u0006\u0004\b$\u0010%J\u001f\u0010\t\u001a\u00020\b2\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00028\u0000H\u0016¢\u0006\u0004\b\t\u0010\nJ\u001a\u0010\u000e\u001a\u00020\r2\b\u0010\f\u001a\u0004\u0018\u00010\u000bH\u0096\u0002¢\u0006\u0004\b\u000e\u0010\u000fR\"\u0010\u0010\u001a\u00020\r8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0010\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0016\u001a\u00020\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0019\u001a\u00020\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR.\u0010\u001e\u001a\u001a\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u001d0\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR\"\u0010 \u001a\u00020\r8\u0016@\u0016X\u0096\u000e¢\u0006\u0012\n\u0004\b \u0010\u0011\u001a\u0004\b \u0010\u0012\"\u0004\b!\u0010\u0014R\u0016\u0010\"\u001a\u00020\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010\u001a¨\u0006)"}, d2 = {"Lcom/discord/utilities/textprocessing/node/EmojiNode;", "Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;", ExifInterface.GPS_DIRECTION_TRUE, "Lb/a/t/b/a/a;", "Lcom/discord/utilities/textprocessing/node/Spoilerable;", "Landroid/text/SpannableStringBuilder;", "builder", "renderContext", "", "render", "(Landroid/text/SpannableStringBuilder;Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;)V", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "isJumbo", "Z", "()Z", "setJumbo", "(Z)V", "Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;", "emojiIdAndType", "Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;", "", "height", "I", "Lkotlin/Function3;", "Landroid/content/Context;", "", "urlProvider", "Lkotlin/jvm/functions/Function3;", "isRevealed", "setRevealed", "width", "emojiName", HookHelper.constructorName, "(Ljava/lang/String;Lkotlin/jvm/functions/Function3;Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;II)V", "Companion", "EmojiIdAndType", "RenderContext", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class EmojiNode<T extends RenderContext> extends a<T> implements Spoilerable {
    public static final Companion Companion = new Companion(null);
    private static final int EMOJI_SIZE = DimenUtils.dpToPixels(16);
    private static final int JUMBOIFY_SCALE_FACTOR = 3;
    private final EmojiIdAndType emojiIdAndType;
    private final int height;
    private boolean isJumbo;
    private boolean isRevealed;
    private final Function3<Boolean, Integer, Context, String> urlProvider;
    private final int width;

    /* compiled from: EmojiNode.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000p\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b*\u0010+JG\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00010\u000e\"\b\b\u0001\u0010\u0003*\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\fH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J/\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00010\u000e\"\b\b\u0001\u0010\u0003*\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u00112\b\b\u0002\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\u000f\u0010\u0013J/\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00010\u000e\"\b\b\u0001\u0010\u0003*\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u00142\b\b\u0002\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\u000f\u0010\u0016J1\u0010\u000f\u001a\n\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u000e\"\b\b\u0001\u0010\u0003*\u00020\u00022\b\b\u0002\u0010\u000b\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u000f\u0010\u0017J/\u0010\u001d\u001a\u00020\u001c*\u00020\u00182\b\u0010\u001a\u001a\u0004\u0018\u00010\u00192\u0006\u0010\u001b\u001a\u00020\b2\b\b\u0002\u0010\u000b\u001a\u00020\nH\u0007¢\u0006\u0004\b\u001d\u0010\u001eJ\u0011\u0010 \u001a\u00020\u001f*\u00020\u0011¢\u0006\u0004\b \u0010!J\u0011\u0010 \u001a\u00020\"*\u00020\u0014¢\u0006\u0004\b \u0010#J\u0011\u0010 \u001a\u00020\f*\u00020\u0019¢\u0006\u0004\b \u0010$J\u0011\u0010 \u001a\u00020\f*\u00020%¢\u0006\u0004\b \u0010&R\u0016\u0010'\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R\u0016\u0010)\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b)\u0010(¨\u0006,"}, d2 = {"Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;", "", "Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;", ExifInterface.GPS_DIRECTION_TRUE, "", "emojiName", "", "emojiId", "", "isAnimated", "", "sizePx", "Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;", "emojiIdAndType", "Lcom/discord/utilities/textprocessing/node/EmojiNode;", "from", "(Ljava/lang/String;JZILcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;)Lcom/discord/utilities/textprocessing/node/EmojiNode;", "Lcom/discord/models/domain/emoji/ModelEmojiCustom;", "customEmoji", "(Lcom/discord/models/domain/emoji/ModelEmojiCustom;I)Lcom/discord/utilities/textprocessing/node/EmojiNode;", "Lcom/discord/models/domain/emoji/ModelEmojiUnicode;", "unicodeEmoji", "(Lcom/discord/models/domain/emoji/ModelEmojiUnicode;I)Lcom/discord/utilities/textprocessing/node/EmojiNode;", "(ILcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;)Lcom/discord/utilities/textprocessing/node/EmojiNode;", "Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;", "Lcom/discord/api/message/reaction/MessageReactionEmoji;", "emoji", "isAnimationEnabled", "", "renderEmoji", "(Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;Lcom/discord/api/message/reaction/MessageReactionEmoji;ZI)V", "Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType$Custom;", "generateEmojiIdAndType", "(Lcom/discord/models/domain/emoji/ModelEmojiCustom;)Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType$Custom;", "Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType$Unicode;", "(Lcom/discord/models/domain/emoji/ModelEmojiUnicode;)Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType$Unicode;", "(Lcom/discord/api/message/reaction/MessageReactionEmoji;)Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;", "Lcom/discord/api/activity/ActivityEmoji;", "(Lcom/discord/api/activity/ActivityEmoji;)Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;", "EMOJI_SIZE", "I", "JUMBOIFY_SCALE_FACTOR", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ EmojiNode from$default(Companion companion, ModelEmojiCustom modelEmojiCustom, int i, int i2, Object obj) {
            if ((i2 & 2) != 0) {
                i = EmojiNode.EMOJI_SIZE;
            }
            return companion.from(modelEmojiCustom, i);
        }

        public static /* synthetic */ void renderEmoji$default(Companion companion, SimpleDraweeSpanTextView simpleDraweeSpanTextView, MessageReactionEmoji messageReactionEmoji, boolean z2, int i, int i2, Object obj) {
            if ((i2 & 4) != 0) {
                i = EmojiNode.EMOJI_SIZE;
            }
            companion.renderEmoji(simpleDraweeSpanTextView, messageReactionEmoji, z2, i);
        }

        public final <T extends RenderContext> EmojiNode<T> from(ModelEmojiCustom modelEmojiCustom, int i) {
            m.checkNotNullParameter(modelEmojiCustom, "customEmoji");
            String name = modelEmojiCustom.getName();
            m.checkNotNullExpressionValue(name, "customEmoji.name");
            return from(name, modelEmojiCustom.getId(), modelEmojiCustom.isAnimated(), i, generateEmojiIdAndType(modelEmojiCustom));
        }

        public final EmojiIdAndType.Custom generateEmojiIdAndType(ModelEmojiCustom modelEmojiCustom) {
            m.checkNotNullParameter(modelEmojiCustom, "$this$generateEmojiIdAndType");
            long id2 = modelEmojiCustom.getId();
            boolean isAnimated = modelEmojiCustom.isAnimated();
            String name = modelEmojiCustom.getName();
            m.checkNotNullExpressionValue(name, "this.name");
            return new EmojiIdAndType.Custom(id2, isAnimated, name);
        }

        public final void renderEmoji(SimpleDraweeSpanTextView simpleDraweeSpanTextView, MessageReactionEmoji messageReactionEmoji, boolean z2) {
            renderEmoji$default(this, simpleDraweeSpanTextView, messageReactionEmoji, z2, 0, 4, null);
        }

        public final void renderEmoji(SimpleDraweeSpanTextView simpleDraweeSpanTextView, MessageReactionEmoji messageReactionEmoji, boolean z2, int i) {
            EmojiIdAndType emojiIdAndType;
            String d;
            m.checkNotNullParameter(simpleDraweeSpanTextView, "$this$renderEmoji");
            String str = "";
            if (messageReactionEmoji == null || !messageReactionEmoji.e()) {
                if (!(messageReactionEmoji == null || (d = messageReactionEmoji.d()) == null)) {
                    str = d;
                }
                emojiIdAndType = new EmojiIdAndType.Unicode(str);
            } else {
                String b2 = messageReactionEmoji.b();
                long parseLong = b2 != null ? Long.parseLong(b2) : 0L;
                boolean a = messageReactionEmoji.a();
                String d2 = messageReactionEmoji.d();
                if (d2 != null) {
                    str = d2;
                }
                emojiIdAndType = new EmojiIdAndType.Custom(parseLong, a, str);
            }
            String str2 = null;
            EmojiNode<T> from = messageReactionEmoji != null ? EmojiNode.Companion.from(i, emojiIdAndType) : null;
            DraweeSpanStringBuilder draweeSpanStringBuilder = new DraweeSpanStringBuilder();
            draweeSpanStringBuilder.append((char) 8202);
            if (from != null) {
                from.render((SpannableStringBuilder) draweeSpanStringBuilder, (DraweeSpanStringBuilder) new RenderContext(z2) { // from class: com.discord.utilities.textprocessing.node.EmojiNode$Companion$renderEmoji$1
                    public final /* synthetic */ boolean $isAnimationEnabled;
                    private final Context context;
                    private final boolean isAnimationEnabled;

                    {
                        this.$isAnimationEnabled = z2;
                        this.context = SimpleDraweeSpanTextView.this.getContext();
                        this.isAnimationEnabled = z2;
                    }

                    @Override // com.discord.utilities.textprocessing.node.EmojiNode.RenderContext
                    public Context getContext() {
                        return this.context;
                    }

                    @Override // com.discord.utilities.textprocessing.node.EmojiNode.RenderContext
                    public boolean isAnimationEnabled() {
                        return this.isAnimationEnabled;
                    }

                    @Override // com.discord.utilities.textprocessing.node.EmojiNode.RenderContext
                    public void onEmojiClicked(EmojiNode.EmojiIdAndType emojiIdAndType2) {
                        m.checkNotNullParameter(emojiIdAndType2, "emojiIdAndType");
                        EmojiNode.RenderContext.DefaultImpls.onEmojiClicked(this, emojiIdAndType2);
                    }
                });
                simpleDraweeSpanTextView.setDraweeSpanStringBuilder(draweeSpanStringBuilder);
                return;
            }
            if (messageReactionEmoji != null) {
                str2 = messageReactionEmoji.d();
            }
            simpleDraweeSpanTextView.setText(str2);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public static /* synthetic */ EmojiNode from$default(Companion companion, ModelEmojiUnicode modelEmojiUnicode, int i, int i2, Object obj) {
            if ((i2 & 2) != 0) {
                i = EmojiNode.EMOJI_SIZE;
            }
            return companion.from(modelEmojiUnicode, i);
        }

        public final EmojiIdAndType.Unicode generateEmojiIdAndType(ModelEmojiUnicode modelEmojiUnicode) {
            m.checkNotNullParameter(modelEmojiUnicode, "$this$generateEmojiIdAndType");
            String firstName = modelEmojiUnicode.getFirstName();
            m.checkNotNullExpressionValue(firstName, "this.firstName");
            return new EmojiIdAndType.Unicode(firstName);
        }

        public static /* synthetic */ EmojiNode from$default(Companion companion, int i, EmojiIdAndType emojiIdAndType, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                i = EmojiNode.EMOJI_SIZE;
            }
            return companion.from(i, emojiIdAndType);
        }

        public final EmojiIdAndType generateEmojiIdAndType(MessageReactionEmoji messageReactionEmoji) {
            EmojiIdAndType emojiIdAndType;
            m.checkNotNullParameter(messageReactionEmoji, "$this$generateEmojiIdAndType");
            String str = "";
            if (messageReactionEmoji.e()) {
                String b2 = messageReactionEmoji.b();
                long parseLong = b2 != null ? Long.parseLong(b2) : 0L;
                boolean a = messageReactionEmoji.a();
                String d = messageReactionEmoji.d();
                if (d != null) {
                    str = d;
                }
                emojiIdAndType = new EmojiIdAndType.Custom(parseLong, a, str);
            } else {
                String d2 = messageReactionEmoji.d();
                if (d2 != null) {
                    str = d2;
                }
                emojiIdAndType = new EmojiIdAndType.Unicode(str);
            }
            return emojiIdAndType;
        }

        public final <T extends RenderContext> EmojiNode<T> from(ModelEmojiUnicode modelEmojiUnicode, int i) {
            m.checkNotNullParameter(modelEmojiUnicode, "unicodeEmoji");
            String codePoints = modelEmojiUnicode.getCodePoints();
            String firstName = modelEmojiUnicode.getFirstName();
            m.checkNotNullExpressionValue(firstName, "unicodeEmoji.firstName");
            return new EmojiNode<>(firstName, new EmojiNode$Companion$from$1(codePoints), generateEmojiIdAndType(modelEmojiUnicode), i, i);
        }

        public final EmojiIdAndType generateEmojiIdAndType(ActivityEmoji activityEmoji) {
            EmojiIdAndType emojiIdAndType;
            m.checkNotNullParameter(activityEmoji, "$this$generateEmojiIdAndType");
            String str = "";
            if (activityEmoji.b() != null) {
                String b2 = activityEmoji.b();
                m.checkNotNull(b2);
                long parseLong = Long.parseLong(b2);
                boolean a = activityEmoji.a();
                String c = activityEmoji.c();
                if (c != null) {
                    str = c;
                }
                emojiIdAndType = new EmojiIdAndType.Custom(parseLong, a, str);
            } else {
                String c2 = activityEmoji.c();
                if (c2 != null) {
                    str = c2;
                }
                emojiIdAndType = new EmojiIdAndType.Unicode(str);
            }
            return emojiIdAndType;
        }

        public final <T extends RenderContext> EmojiNode<T> from(int i, EmojiIdAndType emojiIdAndType) {
            m.checkNotNullParameter(emojiIdAndType, "emojiIdAndType");
            if (emojiIdAndType instanceof EmojiIdAndType.Unicode) {
                ModelEmojiUnicode modelEmojiUnicode = StoreStream.Companion.getEmojis().getUnicodeEmojiSurrogateMap().get(((EmojiIdAndType.Unicode) emojiIdAndType).getName());
                if (modelEmojiUnicode != null) {
                    return EmojiNode.Companion.from(modelEmojiUnicode, i);
                }
                return null;
            } else if (emojiIdAndType instanceof EmojiIdAndType.Custom) {
                EmojiIdAndType.Custom custom = (EmojiIdAndType.Custom) emojiIdAndType;
                return from(custom.getName(), custom.getId(), custom.isAnimated(), i, emojiIdAndType);
            } else {
                throw new NoWhenBranchMatchedException();
            }
        }

        private final <T extends RenderContext> EmojiNode<T> from(String str, long j, boolean z2, int i, EmojiIdAndType emojiIdAndType) {
            return new EmojiNode<>(str, new EmojiNode$Companion$from$3(j, z2), emojiIdAndType, i, i);
        }
    }

    /* compiled from: EmojiNode.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;", "Ljava/io/Serializable;", HookHelper.constructorName, "()V", "Custom", "Unicode", "Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType$Custom;", "Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType$Unicode;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class EmojiIdAndType implements Serializable {

        /* compiled from: EmojiNode.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\b¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ.\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\u00052\b\b\u0002\u0010\r\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u0010\u0010\nJ\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u001a\u0010\u0016\u001a\u00020\u00052\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u0019\u0010\r\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u0018\u001a\u0004\b\u0019\u0010\nR\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001a\u001a\u0004\b\u001b\u0010\u0004R\u0019\u0010\f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001c\u001a\u0004\b\f\u0010\u0007¨\u0006\u001f"}, d2 = {"Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType$Custom;", "Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;", "", "component1", "()J", "", "component2", "()Z", "", "component3", "()Ljava/lang/String;", ModelAuditLogEntry.CHANGE_KEY_ID, "isAnimated", ModelAuditLogEntry.CHANGE_KEY_NAME, "copy", "(JZLjava/lang/String;)Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType$Custom;", "toString", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getName", "J", "getId", "Z", HookHelper.constructorName, "(JZLjava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Custom extends EmojiIdAndType {

            /* renamed from: id */
            private final long f2793id;
            private final boolean isAnimated;
            private final String name;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Custom(long j, boolean z2, String str) {
                super(null);
                m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
                this.f2793id = j;
                this.isAnimated = z2;
                this.name = str;
            }

            public static /* synthetic */ Custom copy$default(Custom custom, long j, boolean z2, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = custom.f2793id;
                }
                if ((i & 2) != 0) {
                    z2 = custom.isAnimated;
                }
                if ((i & 4) != 0) {
                    str = custom.name;
                }
                return custom.copy(j, z2, str);
            }

            public final long component1() {
                return this.f2793id;
            }

            public final boolean component2() {
                return this.isAnimated;
            }

            public final String component3() {
                return this.name;
            }

            public final Custom copy(long j, boolean z2, String str) {
                m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
                return new Custom(j, z2, str);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Custom)) {
                    return false;
                }
                Custom custom = (Custom) obj;
                return this.f2793id == custom.f2793id && this.isAnimated == custom.isAnimated && m.areEqual(this.name, custom.name);
            }

            public final long getId() {
                return this.f2793id;
            }

            public final String getName() {
                return this.name;
            }

            public int hashCode() {
                int a = b.a(this.f2793id) * 31;
                boolean z2 = this.isAnimated;
                if (z2) {
                    z2 = true;
                }
                int i = z2 ? 1 : 0;
                int i2 = z2 ? 1 : 0;
                int i3 = (a + i) * 31;
                String str = this.name;
                return i3 + (str != null ? str.hashCode() : 0);
            }

            public final boolean isAnimated() {
                return this.isAnimated;
            }

            public String toString() {
                StringBuilder R = b.d.b.a.a.R("Custom(id=");
                R.append(this.f2793id);
                R.append(", isAnimated=");
                R.append(this.isAnimated);
                R.append(", name=");
                return b.d.b.a.a.H(R, this.name, ")");
            }
        }

        /* compiled from: EmojiNode.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType$Unicode;", "Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;", "", "component1", "()Ljava/lang/String;", ModelAuditLogEntry.CHANGE_KEY_NAME, "copy", "(Ljava/lang/String;)Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType$Unicode;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getName", HookHelper.constructorName, "(Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Unicode extends EmojiIdAndType {
            private final String name;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Unicode(String str) {
                super(null);
                m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
                this.name = str;
            }

            public static /* synthetic */ Unicode copy$default(Unicode unicode, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = unicode.name;
                }
                return unicode.copy(str);
            }

            public final String component1() {
                return this.name;
            }

            public final Unicode copy(String str) {
                m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
                return new Unicode(str);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Unicode) && m.areEqual(this.name, ((Unicode) obj).name);
                }
                return true;
            }

            public final String getName() {
                return this.name;
            }

            public int hashCode() {
                String str = this.name;
                if (str != null) {
                    return str.hashCode();
                }
                return 0;
            }

            public String toString() {
                return b.d.b.a.a.H(b.d.b.a.a.R("Unicode(name="), this.name, ")");
            }
        }

        private EmojiIdAndType() {
        }

        public /* synthetic */ EmojiIdAndType(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: EmojiNode.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\n\u001a\u00020\u00078&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\tR\u0016\u0010\f\u001a\u00020\u000b8&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\r¨\u0006\u000e"}, d2 = {"Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;", "", "Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;", "emojiIdAndType", "", "onEmojiClicked", "(Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;)V", "Landroid/content/Context;", "getContext", "()Landroid/content/Context;", "context", "", "isAnimationEnabled", "()Z", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface RenderContext {

        /* compiled from: EmojiNode.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class DefaultImpls {
            public static void onEmojiClicked(RenderContext renderContext, EmojiIdAndType emojiIdAndType) {
                m.checkNotNullParameter(emojiIdAndType, "emojiIdAndType");
            }
        }

        Context getContext();

        boolean isAnimationEnabled();

        void onEmojiClicked(EmojiIdAndType emojiIdAndType);
    }

    public EmojiNode(String str, Function3<? super Boolean, ? super Integer, ? super Context, String> function3, EmojiIdAndType emojiIdAndType) {
        this(str, function3, emojiIdAndType, 0, 0, 24, null);
    }

    public EmojiNode(String str, Function3<? super Boolean, ? super Integer, ? super Context, String> function3, EmojiIdAndType emojiIdAndType, int i) {
        this(str, function3, emojiIdAndType, i, 0, 16, null);
    }

    public /* synthetic */ EmojiNode(String str, Function3 function3, EmojiIdAndType emojiIdAndType, int i, int i2, int i3, DefaultConstructorMarker defaultConstructorMarker) {
        this(str, function3, emojiIdAndType, (i3 & 8) != 0 ? EMOJI_SIZE : i, (i3 & 16) != 0 ? EMOJI_SIZE : i2);
    }

    public static final void renderEmoji(SimpleDraweeSpanTextView simpleDraweeSpanTextView, MessageReactionEmoji messageReactionEmoji, boolean z2) {
        Companion.renderEmoji$default(Companion, simpleDraweeSpanTextView, messageReactionEmoji, z2, 0, 4, null);
    }

    public static final void renderEmoji(SimpleDraweeSpanTextView simpleDraweeSpanTextView, MessageReactionEmoji messageReactionEmoji, boolean z2, int i) {
        Companion.renderEmoji(simpleDraweeSpanTextView, messageReactionEmoji, z2, i);
    }

    public boolean equals(Object obj) {
        if (obj instanceof EmojiNode) {
            EmojiNode emojiNode = (EmojiNode) obj;
            if (m.areEqual(emojiNode.getContent(), getContent()) && emojiNode.width == this.width && emojiNode.height == this.height && emojiNode.isJumbo == this.isJumbo) {
                return true;
            }
        }
        return false;
    }

    public final boolean isJumbo() {
        return this.isJumbo;
    }

    @Override // com.discord.utilities.textprocessing.node.Spoilerable
    public boolean isRevealed() {
        return this.isRevealed;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // b.a.t.b.a.a, com.discord.simpleast.core.node.Node
    public /* bridge */ /* synthetic */ void render(SpannableStringBuilder spannableStringBuilder, Object obj) {
        render(spannableStringBuilder, (SpannableStringBuilder) ((RenderContext) obj));
    }

    public final void setJumbo(boolean z2) {
        this.isJumbo = z2;
    }

    @Override // com.discord.utilities.textprocessing.node.Spoilerable
    public void setRevealed(boolean z2) {
        this.isRevealed = z2;
    }

    /* JADX WARN: Type inference failed for: r1v8, types: [REQUEST, com.facebook.imagepipeline.request.ImageRequest] */
    public void render(SpannableStringBuilder spannableStringBuilder, T t) {
        m.checkNotNullParameter(spannableStringBuilder, "builder");
        m.checkNotNullParameter(t, "renderContext");
        Context context = t.getContext();
        DraweeSpanStringBuilder draweeSpanStringBuilder = (DraweeSpanStringBuilder) spannableStringBuilder;
        int length = spannableStringBuilder.length();
        spannableStringBuilder.append((CharSequence) getContent());
        boolean z2 = this.isJumbo;
        int i = this.width;
        if (z2) {
            i *= 3;
        }
        int i2 = i;
        int i3 = this.height;
        if (z2) {
            i3 *= 3;
        }
        int i4 = i3;
        int i5 = z2 ? 1 : 2;
        ?? a = MGImages.getImageRequest(this.urlProvider.invoke(Boolean.valueOf(t.isAnimationEnabled()), Integer.valueOf(IconUtils.getMediaProxySize(i2)), t.getContext()), 0, 0, true).a();
        d a2 = b.f.g.a.a.b.a();
        a2.h = a;
        a2.m = isRevealed();
        AbstractDraweeController a3 = a2.a();
        b.f.g.f.a aVar = new b.f.g.f.a(context.getResources());
        aVar.f = new ColorDrawable(0);
        ScalingUtils$ScaleType scalingUtils$ScaleType = ScalingUtils$ScaleType.a;
        aVar.n = v.l;
        if (!isRevealed()) {
            SpoilerNode.RenderContext renderContext = (SpoilerNode.RenderContext) (!(t instanceof SpoilerNode.RenderContext) ? null : t);
            aVar.b(new ColorDrawable(renderContext != null ? renderContext.getSpoilerColorRes() : ColorCompat.getThemedColor(context, (int) R.attr.theme_chat_spoiler_bg)));
        }
        int length2 = spannableStringBuilder.length() - 1;
        ((DraweeSpanStringBuilder) spannableStringBuilder).c(context, aVar.a(), a3, length, length2, i2, i4, false, i5);
        if (!(getContent().length() == 0)) {
            ClickableSpan clickableSpan = new ClickableSpan(Integer.valueOf(ColorCompat.getThemedColor(context, (int) R.attr.color_brand)), false, null, new EmojiNode$render$clickableSpan$1(this, t), 4, null);
            if (length <= length2) {
                spannableStringBuilder.setSpan(clickableSpan, length, length2 + 1, 33);
                return;
            }
            AppLog appLog = AppLog.g;
            StringBuilder R = b.d.b.a.a.R("Span content: ");
            R.append(getContent());
            R.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
            Logger.e$default(appLog, "Unable to render emoji tappable span.", new Exception(R.toString()), null, 4, null);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public EmojiNode(String str, Function3<? super Boolean, ? super Integer, ? super Context, String> function3, EmojiIdAndType emojiIdAndType, int i, int i2) {
        super(str);
        m.checkNotNullParameter(str, "emojiName");
        m.checkNotNullParameter(function3, "urlProvider");
        m.checkNotNullParameter(emojiIdAndType, "emojiIdAndType");
        this.urlProvider = function3;
        this.emojiIdAndType = emojiIdAndType;
        this.width = i;
        this.height = i2;
        this.isRevealed = true;
    }
}
