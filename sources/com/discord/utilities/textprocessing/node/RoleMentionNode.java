package com.discord.utilities.textprocessing.node;

import andhook.lib.HookHelper;
import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import androidx.core.graphics.ColorUtils;
import androidx.exifinterface.media.ExifInterface;
import b.d.b.a.a;
import com.discord.api.role.GuildRole;
import com.discord.simpleast.core.node.Node;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.guilds.RoleUtils;
import com.discord.utilities.textprocessing.node.RoleMentionNode.RenderContext;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: RoleMentionNode.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\b\u0018\u0000*\b\b\u0000\u0010\u0002*\u00020\u00012\b\u0012\u0004\u0012\u00028\u00000\u0003:\u0001\u0016B\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b\u0014\u0010\u0015J\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00028\u0000H\u0016¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\r\u001a\u00020\f2\b\u0010\u000b\u001a\u0004\u0018\u00010\nH\u0096\u0002¢\u0006\u0004\b\r\u0010\u000eR\u0019\u0010\u0010\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013¨\u0006\u0017"}, d2 = {"Lcom/discord/utilities/textprocessing/node/RoleMentionNode;", "Lcom/discord/utilities/textprocessing/node/RoleMentionNode$RenderContext;", ExifInterface.GPS_DIRECTION_TRUE, "Lcom/discord/simpleast/core/node/Node;", "Landroid/text/SpannableStringBuilder;", "builder", "renderContext", "", "render", "(Landroid/text/SpannableStringBuilder;Lcom/discord/utilities/textprocessing/node/RoleMentionNode$RenderContext;)V", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "roleId", "J", "getRoleId", "()J", HookHelper.constructorName, "(J)V", "RenderContext", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class RoleMentionNode<T extends RenderContext> extends Node<T> {
    private final long roleId;

    /* compiled from: RoleMentionNode.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001R(\u0010\b\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0016\u0010\f\u001a\u00020\t8&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u000b¨\u0006\r"}, d2 = {"Lcom/discord/utilities/textprocessing/node/RoleMentionNode$RenderContext;", "", "", "", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "getRoles", "()Ljava/util/Map;", "roles", "Landroid/content/Context;", "getContext", "()Landroid/content/Context;", "context", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface RenderContext {
        Context getContext();

        Map<Long, GuildRole> getRoles();
    }

    public RoleMentionNode(long j) {
        super(null, 1, null);
        this.roleId = j;
    }

    public boolean equals(Object obj) {
        return (obj instanceof RoleMentionNode) && ((RoleMentionNode) obj).roleId == this.roleId;
    }

    public final long getRoleId() {
        return this.roleId;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.discord.simpleast.core.node.Node
    public /* bridge */ /* synthetic */ void render(SpannableStringBuilder spannableStringBuilder, Object obj) {
        render(spannableStringBuilder, (SpannableStringBuilder) ((RenderContext) obj));
    }

    public void render(SpannableStringBuilder spannableStringBuilder, T t) {
        m.checkNotNullParameter(spannableStringBuilder, "builder");
        m.checkNotNullParameter(t, "renderContext");
        int length = spannableStringBuilder.length();
        Map<Long, GuildRole> roles = t.getRoles();
        GuildRole guildRole = roles != null ? roles.get(Long.valueOf(this.roleId)) : null;
        if (guildRole == null) {
            spannableStringBuilder.append("deleted-role");
            return;
        }
        StringBuilder O = a.O(MentionUtilsKt.MENTIONS_CHAR);
        O.append(guildRole.g());
        String sb = O.toString();
        List<Object> listOf = n.listOf(new StyleSpan(1), new ForegroundColorSpan(!RoleUtils.isDefaultColor(guildRole) ? ColorUtils.setAlphaComponent(guildRole.b(), 255) : ColorCompat.getThemedColor(t.getContext(), (int) R.attr.theme_chat_mention_foreground)), new BackgroundColorSpan(!RoleUtils.isDefaultColor(guildRole) ? ColorUtils.setAlphaComponent(guildRole.b(), 25) : ColorCompat.getThemedColor(t.getContext(), (int) R.attr.theme_chat_mention_background)));
        spannableStringBuilder.append((CharSequence) sb);
        for (Object obj : listOf) {
            spannableStringBuilder.setSpan(obj, length, spannableStringBuilder.length(), 33);
        }
    }
}
