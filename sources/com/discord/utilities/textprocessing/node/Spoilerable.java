package com.discord.utilities.textprocessing.node;

import kotlin.Metadata;
/* compiled from: Spoilerable.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\bf\u0018\u00002\u00020\u0001R\u001c\u0010\u0003\u001a\u00020\u00028&@&X¦\u000e¢\u0006\f\u001a\u0004\b\u0003\u0010\u0004\"\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/utilities/textprocessing/node/Spoilerable;", "", "", "isRevealed", "()Z", "setRevealed", "(Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public interface Spoilerable {
    boolean isRevealed();

    void setRevealed(boolean z2);
}
