package com.discord.utilities.textprocessing.node;

import andhook.lib.HookHelper;
import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.style.BackgroundColorSpan;
import android.text.style.CharacterStyle;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import androidx.exifinterface.media.ExifInterface;
import b.d.b.a.a;
import com.discord.simpleast.core.node.Node;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.spans.ClickableSpan;
import com.discord.utilities.textprocessing.node.UserMentionNode.RenderContext;
import d0.t.n;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: UserMentionNode.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000*\b\b\u0000\u0010\u0002*\u00020\u00012\b\u0012\u0004\u0012\u00028\u00000\u0003:\u0002\u001d\u001eB\u001d\u0012\u0006\u0010\u0017\u001a\u00020\u0016\u0012\f\b\u0002\u0010\u0012\u001a\u00060\u0010j\u0002`\u0011¢\u0006\u0004\b\u001b\u0010\u001cJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00028\u0000H\u0002¢\u0006\u0004\b\b\u0010\tJ\u001f\u0010\n\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00028\u0000H\u0016¢\u0006\u0004\b\n\u0010\tJ\u001a\u0010\u000e\u001a\u00020\r2\b\u0010\f\u001a\u0004\u0018\u00010\u000bH\u0096\u0002¢\u0006\u0004\b\u000e\u0010\u000fR\u001d\u0010\u0012\u001a\u00060\u0010j\u0002`\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u0019\u0010\u0017\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001a¨\u0006\u001f"}, d2 = {"Lcom/discord/utilities/textprocessing/node/UserMentionNode;", "Lcom/discord/utilities/textprocessing/node/UserMentionNode$RenderContext;", ExifInterface.GPS_DIRECTION_TRUE, "Lcom/discord/simpleast/core/node/Node;", "Landroid/text/SpannableStringBuilder;", "builder", "renderContext", "", "renderUserMention", "(Landroid/text/SpannableStringBuilder;Lcom/discord/utilities/textprocessing/node/UserMentionNode$RenderContext;)V", "render", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "Lcom/discord/primitives/UserId;", "userId", "J", "getUserId", "()J", "Lcom/discord/utilities/textprocessing/node/UserMentionNode$Type;", "type", "Lcom/discord/utilities/textprocessing/node/UserMentionNode$Type;", "getType", "()Lcom/discord/utilities/textprocessing/node/UserMentionNode$Type;", HookHelper.constructorName, "(Lcom/discord/utilities/textprocessing/node/UserMentionNode$Type;J)V", "RenderContext", "Type", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class UserMentionNode<T extends RenderContext> extends Node<T> {
    private final Type type;
    private final long userId;

    /* compiled from: UserMentionNode.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001R(\u0010\b\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u001a\u0010\u000b\u001a\u00060\u0003j\u0002`\u00048&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\nR(\u0010\u0010\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\r\u0018\u00010\f8&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0014\u001a\u00020\u00118&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0013¨\u0006\u0015"}, d2 = {"Lcom/discord/utilities/textprocessing/node/UserMentionNode$RenderContext;", "", "Lkotlin/Function1;", "", "Lcom/discord/primitives/UserId;", "", "getUserMentionOnClick", "()Lkotlin/jvm/functions/Function1;", "userMentionOnClick", "getMyId", "()J", "myId", "", "", "getUserNames", "()Ljava/util/Map;", "userNames", "Landroid/content/Context;", "getContext", "()Landroid/content/Context;", "context", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface RenderContext {
        Context getContext();

        long getMyId();

        Function1<Long, Unit> getUserMentionOnClick();

        Map<Long, String> getUserNames();
    }

    /* compiled from: UserMentionNode.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/utilities/textprocessing/node/UserMentionNode$Type;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "USER", "HERE", "EVERYONE", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum Type {
        USER,
        HERE,
        EVERYONE
    }

    public /* synthetic */ UserMentionNode(Type type, long j, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(type, (i & 2) != 0 ? -1L : j);
    }

    private final void renderUserMention(SpannableStringBuilder spannableStringBuilder, T t) {
        String str;
        Map<Long, String> userNames = t.getUserNames();
        Context context = t.getContext();
        int length = spannableStringBuilder.length();
        boolean containsKey = userNames != null ? userNames.containsKey(Long.valueOf(this.userId)) : false;
        StringBuilder R = a.R("@");
        if (userNames == null || (str = userNames.get(Long.valueOf(this.userId))) == null) {
            str = "invalid-user";
        }
        R.append(str);
        String sb = R.toString();
        ArrayList<CharacterStyle> arrayList = new ArrayList();
        arrayList.add(new StyleSpan(1));
        arrayList.add(new BackgroundColorSpan(ColorCompat.getThemedColor(context, (int) R.attr.theme_chat_mention_background)));
        Function1<Long, Unit> userMentionOnClick = t.getUserMentionOnClick();
        if (!containsKey || userMentionOnClick == null) {
            arrayList.add(new ForegroundColorSpan(ColorCompat.getThemedColor(context, (int) R.attr.theme_chat_mention_foreground)));
        } else {
            arrayList.add(new ClickableSpan(Integer.valueOf(ColorCompat.getThemedColor(context, (int) R.attr.theme_chat_mention_foreground)), false, null, new UserMentionNode$renderUserMention$1(this, userMentionOnClick), 4, null));
        }
        spannableStringBuilder.append((CharSequence) sb);
        for (CharacterStyle characterStyle : arrayList) {
            spannableStringBuilder.setSpan(characterStyle, length, spannableStringBuilder.length(), 33);
        }
    }

    public boolean equals(Object obj) {
        return (obj instanceof UserMentionNode) && ((UserMentionNode) obj).userId == this.userId;
    }

    public final Type getType() {
        return this.type;
    }

    public final long getUserId() {
        return this.userId;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.discord.simpleast.core.node.Node
    public /* bridge */ /* synthetic */ void render(SpannableStringBuilder spannableStringBuilder, Object obj) {
        render(spannableStringBuilder, (SpannableStringBuilder) ((RenderContext) obj));
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public UserMentionNode(Type type, long j) {
        super(null, 1, null);
        m.checkNotNullParameter(type, "type");
        this.type = type;
        this.userId = j;
    }

    public void render(SpannableStringBuilder spannableStringBuilder, T t) {
        m.checkNotNullParameter(spannableStringBuilder, "builder");
        m.checkNotNullParameter(t, "renderContext");
        if (this.type == Type.USER) {
            renderUserMention(spannableStringBuilder, t);
            return;
        }
        List<Object> listOf = n.listOf(new StyleSpan(1), new BackgroundColorSpan(ColorCompat.getThemedColor(t.getContext(), (int) R.attr.theme_chat_mention_background)), new ForegroundColorSpan(ColorCompat.getThemedColor(t.getContext(), (int) R.attr.theme_chat_mention_foreground)));
        int length = spannableStringBuilder.length();
        spannableStringBuilder.append((CharSequence) (this.type == Type.HERE ? "@here" : "@everyone"));
        for (Object obj : listOf) {
            spannableStringBuilder.setSpan(obj, length, spannableStringBuilder.length(), 33);
        }
    }
}
