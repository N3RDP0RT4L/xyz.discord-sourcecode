package com.discord.utilities.textprocessing;

import andhook.lib.HookHelper;
import b.a.t.b.c.a;
import b.c.a.a0.d;
import com.discord.api.message.embed.MessageEmbed;
import com.discord.simpleast.core.node.Node;
import com.discord.stores.StoreMessageState;
import com.discord.utilities.embed.EmbedResourceUtils;
import com.discord.utilities.textprocessing.node.BasicRenderContext;
import com.discord.utilities.textprocessing.node.BlockQuoteNode;
import com.discord.utilities.textprocessing.node.SpoilerNode;
import com.discord.utilities.textprocessing.node.UrlNode;
import d0.t.u;
import d0.z.d.e0;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: MessagePreprocessor.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u001f\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 <2\u00020\u0001:\u0002<=BM\u0012\n\u0010*\u001a\u00060(j\u0002`)\u0012\u0010\b\u0002\u0010.\u001a\n\u0012\u0004\u0012\u00020 \u0018\u00010\u0004\u0012\u0010\b\u0002\u0010\u001e\u001a\n\u0012\u0004\u0012\u00020\u001d\u0018\u00010\u001c\u0012\b\b\u0002\u0010'\u001a\u00020\u0019\u0012\n\b\u0002\u0010!\u001a\u0004\u0018\u00010 ¢\u0006\u0004\b6\u00107B\u001f\b\u0016\u0012\n\u0010*\u001a\u00060(j\u0002`)\u0012\b\u00109\u001a\u0004\u0018\u000108¢\u0006\u0004\b6\u0010:BA\b\u0016\u0012\n\u0010*\u001a\u00060(j\u0002`)\u0012\b\u00109\u001a\u0004\u0018\u000108\u0012\u000e\u0010\u001e\u001a\n\u0012\u0004\u0012\u00020\u001d\u0018\u00010\u001c\u0012\u0006\u0010'\u001a\u00020\u0019\u0012\b\u0010!\u001a\u0004\u0018\u00010 ¢\u0006\u0004\b6\u0010;J-\u0010\b\u001a\u00020\u0007\"\b\b\u0000\u0010\u0003*\u00020\u00022\u0012\u0010\u0006\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u00050\u0004H\u0002¢\u0006\u0004\b\b\u0010\tJ-\u0010\f\u001a\u00020\u0007\"\b\b\u0000\u0010\u0003*\u00020\u00022\u0012\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u00050\nH\u0002¢\u0006\u0004\b\f\u0010\tJ\u000f\u0010\r\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\r\u0010\u000eJ#\u0010\u0010\u001a\u00020\u00072\u0012\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\u00050\nH\u0002¢\u0006\u0004\b\u0010\u0010\tJ-\u0010\u0011\u001a\u00020\u0007\"\b\b\u0000\u0010\u0003*\u00020\u00022\u0012\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u00050\u0004H\u0002¢\u0006\u0004\b\u0011\u0010\tJ5\u0010\u0011\u001a\u00020\u0007\"\b\b\u0000\u0010\u0003*\u00020\u00022\u0012\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u00050\n2\u0006\u0010\u0013\u001a\u00020\u0012H\u0002¢\u0006\u0004\b\u0011\u0010\u0014J+\u0010\u0015\u001a\u00020\u0007\"\b\b\u0000\u0010\u0003*\u00020\u00022\u0012\u0010\u0006\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u00050\u0004¢\u0006\u0004\b\u0015\u0010\tJ\u001b\u0010\u0017\u001a\u00020\u00072\n\u0010\u0016\u001a\u0006\u0012\u0002\b\u00030\u0005H\u0016¢\u0006\u0004\b\u0017\u0010\u0018R\u0013\u0010\u001a\u001a\u00020\u00198F@\u0006¢\u0006\u0006\u001a\u0004\b\u001a\u0010\u001bR\u001e\u0010\u001e\u001a\n\u0012\u0004\u0012\u00020\u001d\u0018\u00010\u001c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR\u0018\u0010!\u001a\u0004\u0018\u00010 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"R\u0016\u0010#\u001a\u00020\u00198\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b#\u0010$R\u0016\u0010%\u001a\u00020 8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b%\u0010&R\u0016\u0010'\u001a\u00020\u00198\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b'\u0010$R\u001d\u0010*\u001a\u00060(j\u0002`)8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010+\u001a\u0004\b,\u0010-R\u001e\u0010.\u001a\n\u0012\u0004\u0012\u00020 \u0018\u00010\u00048\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b.\u0010/R<\u00103\u001a\u000e\u0012\b\u0012\u0006\u0012\u0002\b\u000301\u0018\u0001002\u0012\u00102\u001a\u000e\u0012\b\u0012\u0006\u0012\u0002\b\u000301\u0018\u0001008\u0006@BX\u0086\u000e¢\u0006\f\n\u0004\b3\u0010\u001f\u001a\u0004\b4\u00105¨\u0006>"}, d2 = {"Lcom/discord/utilities/textprocessing/MessagePreprocessor;", "Lb/a/t/b/c/a;", "Lcom/discord/utilities/textprocessing/node/BasicRenderContext;", "R", "", "Lcom/discord/simpleast/core/node/Node;", "ast", "", "processQuoteChildren", "(Ljava/util/Collection;)V", "", "nodes", "mergeConsecutiveQuoteNodes", "processSpoilerChildren", "()V", "Lcom/discord/utilities/textprocessing/MessageRenderContext;", "stripSimpleEmbedLink", "constrainAST", "Lcom/discord/utilities/textprocessing/MessagePreprocessor$ConstrainState;", "state", "(Ljava/util/Collection;Lcom/discord/utilities/textprocessing/MessagePreprocessor$ConstrainState;)V", "process", "node", "processNode", "(Lcom/discord/simpleast/core/node/Node;)V", "", "isLinkifyConflicting", "()Z", "", "Lcom/discord/api/message/embed/MessageEmbed;", "embeds", "Ljava/util/List;", "", "maxNodes", "Ljava/lang/Integer;", "hasLinkConflictingNode", "Z", "customEmojiCount", "I", "shouldJumboify", "", "Lcom/discord/primitives/UserId;", "myUserId", "J", "getMyUserId", "()J", "visibleSpoilerNodeIndices", "Ljava/util/Collection;", "", "Lcom/discord/utilities/textprocessing/node/SpoilerNode;", "<set-?>", "spoilers", "getSpoilers", "()Ljava/util/List;", HookHelper.constructorName, "(JLjava/util/Collection;Ljava/util/List;ZLjava/lang/Integer;)V", "Lcom/discord/stores/StoreMessageState$State;", "messageState", "(JLcom/discord/stores/StoreMessageState$State;)V", "(JLcom/discord/stores/StoreMessageState$State;Ljava/util/List;ZLjava/lang/Integer;)V", "Companion", "ConstrainState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MessagePreprocessor implements a {
    public static final Companion Companion = new Companion(null);
    private static final int MAX_JUMBO_EMOJI_COUNT = 27;
    private int customEmojiCount;
    private final List<MessageEmbed> embeds;
    private boolean hasLinkConflictingNode;
    private final Integer maxNodes;
    private final long myUserId;
    private boolean shouldJumboify;
    private List<SpoilerNode<?>> spoilers;
    private final Collection<Integer> visibleSpoilerNodeIndices;

    /* compiled from: MessagePreprocessor.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/utilities/textprocessing/MessagePreprocessor$Companion;", "", "", "MAX_JUMBO_EMOJI_COUNT", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: MessagePreprocessor.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\b\b\u0082\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0014\u0010\u0013J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000e\u001a\u00020\r2\b\u0010\f\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u000e\u0010\u000fR\"\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0005\u0010\u0010\u001a\u0004\b\u0011\u0010\u0004\"\u0004\b\u0012\u0010\u0013¨\u0006\u0015"}, d2 = {"Lcom/discord/utilities/textprocessing/MessagePreprocessor$ConstrainState;", "", "", "component1", "()I", "limit", "copy", "(I)Lcom/discord/utilities/textprocessing/MessagePreprocessor$ConstrainState;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getLimit", "setLimit", "(I)V", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ConstrainState {
        private int limit;

        public ConstrainState(int i) {
            this.limit = i;
        }

        public static /* synthetic */ ConstrainState copy$default(ConstrainState constrainState, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                i = constrainState.limit;
            }
            return constrainState.copy(i);
        }

        public final int component1() {
            return this.limit;
        }

        public final ConstrainState copy(int i) {
            return new ConstrainState(i);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof ConstrainState) && this.limit == ((ConstrainState) obj).limit;
            }
            return true;
        }

        public final int getLimit() {
            return this.limit;
        }

        public int hashCode() {
            return this.limit;
        }

        public final void setLimit(int i) {
            this.limit = i;
        }

        public String toString() {
            return b.d.b.a.a.A(b.d.b.a.a.R("ConstrainState(limit="), this.limit, ")");
        }
    }

    public MessagePreprocessor(long j, Collection<Integer> collection, List<MessageEmbed> list, boolean z2, Integer num) {
        this.myUserId = j;
        this.visibleSpoilerNodeIndices = collection;
        this.embeds = list;
        this.shouldJumboify = z2;
        this.maxNodes = num;
    }

    private final <R extends BasicRenderContext> void constrainAST(Collection<? extends Node<R>> collection) {
        if (this.maxNodes != null) {
            Objects.requireNonNull(collection, "null cannot be cast to non-null type kotlin.collections.MutableCollection<com.discord.simpleast.core.node.Node<R>>");
            constrainAST(e0.asMutableCollection(collection), new ConstrainState(this.maxNodes.intValue()));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final <R extends BasicRenderContext> void mergeConsecutiveQuoteNodes(Collection<Node<R>> collection) {
        ArrayList arrayList = new ArrayList();
        BlockQuoteNode blockQuoteNode = null;
        while (true) {
            for (Node<R> node : collection) {
                if (!(node instanceof BlockQuoteNode)) {
                    break;
                } else if (blockQuoteNode == null) {
                    blockQuoteNode = (BlockQuoteNode) node;
                } else {
                    arrayList.add(node);
                    Collection<Node<R>> children = node.getChildren();
                    if (children != null) {
                        for (Node<R> node2 : children) {
                            blockQuoteNode.addChild(node2);
                        }
                    }
                }
            }
            collection.removeAll(arrayList);
            return;
        }
    }

    private final <R extends BasicRenderContext> void processQuoteChildren(Collection<? extends Node<R>> collection) {
        Objects.requireNonNull(collection, "null cannot be cast to non-null type kotlin.collections.MutableCollection<com.discord.simpleast.core.node.Node<R>>");
        mergeConsecutiveQuoteNodes(e0.asMutableCollection(collection));
        a messagePreprocessor$processQuoteChildren$1 = new a() { // from class: com.discord.utilities.textprocessing.MessagePreprocessor$processQuoteChildren$1
            @Override // b.a.t.b.c.a
            public final void processNode(Node<Object> node) {
                Collection<Node<Object>> children = node.getChildren();
                if (children != null) {
                    MessagePreprocessor.this.mergeConsecutiveQuoteNodes(e0.asMutableCollection(children));
                }
            }
        };
        for (Node<R> node : collection) {
            d.k2(node, messagePreprocessor$processQuoteChildren$1);
        }
    }

    private final void processSpoilerChildren() {
        List<SpoilerNode<?>> list = this.spoilers;
        if (!(!(list == null || list.isEmpty()))) {
            list = null;
        }
        if (list != null) {
            ArrayList<SpoilerNode> arrayList = new ArrayList();
            for (Object obj : list) {
                if (!((SpoilerNode) obj).isRevealed()) {
                    arrayList.add(obj);
                }
            }
            for (SpoilerNode spoilerNode : arrayList) {
                d.i2(spoilerNode.getChildren(), MessagePreprocessor$processSpoilerChildren$3$1.INSTANCE);
            }
        }
    }

    private final void stripSimpleEmbedLink(Collection<Node<MessageRenderContext>> collection) {
        List<MessageEmbed> list;
        if (collection.size() == 1 && (list = this.embeds) != null && list.size() == 1) {
            MessageEmbed messageEmbed = this.embeds.get(0);
            if ((((Node) u.elementAt(collection, 0)) instanceof UrlNode) && EmbedResourceUtils.INSTANCE.isSimpleEmbed(messageEmbed)) {
                collection.clear();
            }
        }
    }

    public final long getMyUserId() {
        return this.myUserId;
    }

    public final List<SpoilerNode<?>> getSpoilers() {
        return this.spoilers;
    }

    public final boolean isLinkifyConflicting() {
        if (!this.hasLinkConflictingNode) {
            List<SpoilerNode<?>> list = this.spoilers;
            if (list == null || list.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    public final <R extends BasicRenderContext> void process(Collection<? extends Node<R>> collection) {
        m.checkNotNullParameter(collection, "ast");
        stripSimpleEmbedLink(e0.asMutableCollection(collection));
        d.i2(collection, this);
        if (this.shouldJumboify) {
            d.i2(collection, MessagePreprocessor$process$1.INSTANCE);
        }
        processSpoilerChildren();
        processQuoteChildren(collection);
        constrainAST(collection);
    }

    /* JADX WARN: Code restructure failed: missing block: B:7:0x0016, code lost:
        if (r0 <= 27) goto L11;
     */
    /* JADX WARN: Removed duplicated region for block: B:20:0x003b  */
    /* JADX WARN: Removed duplicated region for block: B:21:0x003e  */
    @Override // b.a.t.b.c.a
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void processNode(com.discord.simpleast.core.node.Node<?> r5) {
        /*
            r4 = this;
            java.lang.String r0 = "node"
            d0.z.d.m.checkNotNullParameter(r5, r0)
            boolean r0 = r4.shouldJumboify
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L34
            boolean r0 = r5 instanceof com.discord.utilities.textprocessing.node.EmojiNode
            if (r0 == 0) goto L1b
            int r0 = r4.customEmojiCount
            int r0 = r0 + r2
            r4.customEmojiCount = r0
            r3 = 27
            if (r0 > r3) goto L19
            goto L1f
        L19:
            r0 = 0
            goto L30
        L1b:
            boolean r0 = r5 instanceof com.discord.simpleast.core.node.StyleNode
            if (r0 == 0) goto L21
        L1f:
            r0 = 1
            goto L30
        L21:
            boolean r0 = r5 instanceof b.a.t.b.a.a
            if (r0 == 0) goto L19
            r0 = r5
            b.a.t.b.a.a r0 = (b.a.t.b.a.a) r0
            java.lang.String r0 = r0.getContent()
            boolean r0 = d0.g0.t.isBlank(r0)
        L30:
            if (r0 == 0) goto L34
            r0 = 1
            goto L35
        L34:
            r0 = 0
        L35:
            r4.shouldJumboify = r0
            boolean r0 = r5 instanceof com.discord.simpleast.code.CodeNode
            if (r0 == 0) goto L3e
            r4.hasLinkConflictingNode = r2
            goto L6d
        L3e:
            boolean r0 = r5 instanceof com.discord.utilities.textprocessing.node.UrlNode
            if (r0 == 0) goto L45
            r4.hasLinkConflictingNode = r2
            goto L6d
        L45:
            boolean r0 = r5 instanceof com.discord.utilities.textprocessing.node.SpoilerNode
            if (r0 == 0) goto L6d
            java.util.List<com.discord.utilities.textprocessing.node.SpoilerNode<?>> r0 = r4.spoilers
            if (r0 == 0) goto L4e
            goto L55
        L4e:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r4.spoilers = r0
        L55:
            int r2 = r0.size()
            r0.add(r5)
            java.util.Collection<java.lang.Integer> r0 = r4.visibleSpoilerNodeIndices
            if (r0 == 0) goto L68
            java.lang.Integer r1 = java.lang.Integer.valueOf(r2)
            boolean r1 = r0.contains(r1)
        L68:
            com.discord.utilities.textprocessing.node.SpoilerNode r5 = (com.discord.utilities.textprocessing.node.SpoilerNode) r5
            r5.updateState(r2, r1)
        L6d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.textprocessing.MessagePreprocessor.processNode(com.discord.simpleast.core.node.Node):void");
    }

    public /* synthetic */ MessagePreprocessor(long j, Collection collection, List list, boolean z2, Integer num, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(j, (i & 2) != 0 ? null : collection, (i & 4) != 0 ? null : list, (i & 8) != 0 ? true : z2, (i & 16) != 0 ? null : num);
    }

    public MessagePreprocessor(long j, StoreMessageState.State state) {
        this(j, state != null ? state.getVisibleSpoilerNodeIndices() : null, null, false, null, 24, null);
    }

    private final <R extends BasicRenderContext> void constrainAST(Collection<Node<R>> collection, ConstrainState constrainState) {
        Iterator<Node<R>> it = collection.iterator();
        while (it.hasNext()) {
            Node<R> next = it.next();
            if (!m.areEqual(next.getClass(), b.a.t.b.a.a.class)) {
                constrainState.setLimit(constrainState.getLimit() - 1);
            }
            if (constrainState.getLimit() <= 0) {
                it.remove();
            } else if (next.hasChildren()) {
                Collection<Node<R>> children = next.getChildren();
                Objects.requireNonNull(children, "null cannot be cast to non-null type kotlin.collections.MutableCollection<com.discord.simpleast.core.node.Node<R>>");
                constrainAST(e0.asMutableCollection(children), constrainState);
                if (!next.hasChildren()) {
                    it.remove();
                }
            }
        }
    }

    public MessagePreprocessor(long j, StoreMessageState.State state, List<MessageEmbed> list, boolean z2, Integer num) {
        this(j, state != null ? state.getVisibleSpoilerNodeIndices() : null, list, z2, num);
    }
}
