package com.discord.utilities.textprocessing;

import andhook.lib.HookHelper;
import android.content.Context;
import android.text.style.CharacterStyle;
import android.text.style.ParagraphStyle;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.spans.VerticalPaddingSpan;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: Spans.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0016\u0010\u0017J!\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bJ!\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\t\u0010\bJ!\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\n2\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\r\u0010\u000eJ!\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\n2\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u000f\u0010\u000eJ#\u0010\u0011\u001a\u0010\u0012\u0004\u0012\u00020\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0011\u0010\bJ!\u0010\u0013\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u000b0\n2\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u000eR\u0016\u0010\u0014\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015¨\u0006\u0018"}, d2 = {"Lcom/discord/utilities/textprocessing/Spans;", "", "Landroid/content/Context;", "context", "Lkotlin/Function1;", "", "Landroid/text/style/CharacterStyle;", "createHeaderStyleSpanProvider", "(Landroid/content/Context;)Lkotlin/jvm/functions/Function1;", "createSpecialHeaderStyleSpanProvider", "Lkotlin/Function0;", "", "Lcom/discord/utilities/spans/VerticalPaddingSpan;", "createHeaderPaddingSpansProvider", "(Landroid/content/Context;)Lkotlin/jvm/functions/Function0;", "createChangelogSpecialHeaderPaddingSpansProvider", "", "createHeaderClassSpanProvider", "Landroid/text/style/ParagraphStyle;", "createMarkdownBulletSpansProvider", "MARKDOWN_BULLET_RADIUS", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class Spans {
    public static final Spans INSTANCE = new Spans();
    private static final int MARKDOWN_BULLET_RADIUS = 8;

    private Spans() {
    }

    public final Function0<List<VerticalPaddingSpan>> createChangelogSpecialHeaderPaddingSpansProvider(Context context) {
        m.checkNotNullParameter(context, "context");
        return new Spans$createChangelogSpecialHeaderPaddingSpansProvider$1(context.getResources().getDimensionPixelSize(R.dimen.markdown_header_1_changelog_special_top_padding), context.getResources().getDimensionPixelSize(R.dimen.markdown_header_1_bottom_padding));
    }

    public final Function1<String, Object> createHeaderClassSpanProvider(Context context) {
        m.checkNotNullParameter(context, "context");
        return new Spans$createHeaderClassSpanProvider$1(context, context.getResources().getDimensionPixelSize(R.dimen.markdown_header_class_marginTop));
    }

    public final Function0<List<VerticalPaddingSpan>> createHeaderPaddingSpansProvider(Context context) {
        m.checkNotNullParameter(context, "context");
        return new Spans$createHeaderPaddingSpansProvider$1(context.getResources().getDimensionPixelSize(R.dimen.markdown_header_1_top_padding), context.getResources().getDimensionPixelSize(R.dimen.markdown_header_1_bottom_padding));
    }

    public final Function1<Integer, CharacterStyle> createHeaderStyleSpanProvider(Context context) {
        m.checkNotNullParameter(context, "context");
        return new Spans$createHeaderStyleSpanProvider$1(context);
    }

    public final Function0<List<ParagraphStyle>> createMarkdownBulletSpansProvider(Context context) {
        m.checkNotNullParameter(context, "context");
        return new Spans$createMarkdownBulletSpansProvider$1$1(context.getResources().getDimensionPixelSize(R.dimen.markdown_bullet_vertical_padding), context.getResources().getDimensionPixelSize(R.dimen.markdown_bullet_gap), ColorCompat.getThemedColor(context, (int) R.attr.color_brand_500));
    }

    public final Function1<Integer, CharacterStyle> createSpecialHeaderStyleSpanProvider(Context context) {
        m.checkNotNullParameter(context, "context");
        return new Spans$createSpecialHeaderStyleSpanProvider$1(context);
    }
}
