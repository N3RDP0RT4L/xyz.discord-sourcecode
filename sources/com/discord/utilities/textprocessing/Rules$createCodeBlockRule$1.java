package com.discord.utilities.textprocessing;

import android.text.SpannableStringBuilder;
import android.text.style.BackgroundColorSpan;
import androidx.exifinterface.media.ExifInterface;
import com.discord.simpleast.code.CodeNode;
import com.discord.simpleast.core.node.Node;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.textprocessing.Rules;
import com.discord.utilities.textprocessing.node.BasicRenderContext;
import com.discord.utilities.textprocessing.node.BlockBackgroundNode;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function3;
import xyz.discord.R;
/* compiled from: Rules.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\f\u001a\b\u0012\u0004\u0012\u00028\u00000\t\"\b\b\u0000\u0010\u0001*\u00020\u0000\"\u000e\b\u0001\u0010\u0003*\b\u0012\u0004\u0012\u00028\u00010\u00022\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00028\u00000\u00042\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00028\u0001H\n¢\u0006\u0004\b\n\u0010\u000b"}, d2 = {"Lcom/discord/utilities/textprocessing/node/BasicRenderContext;", "RC", "Lcom/discord/utilities/textprocessing/Rules$BlockQuoteState;", ExifInterface.LATITUDE_SOUTH, "Lcom/discord/simpleast/code/CodeNode;", "codeNode", "", "block", "state", "Lcom/discord/simpleast/core/node/Node;", "invoke", "(Lcom/discord/simpleast/code/CodeNode;ZLcom/discord/utilities/textprocessing/Rules$BlockQuoteState;)Lcom/discord/simpleast/core/node/Node;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class Rules$createCodeBlockRule$1 extends o implements Function3<CodeNode<RC>, Boolean, S, Node<RC>> {
    public static final Rules$createCodeBlockRule$1 INSTANCE = new Rules$createCodeBlockRule$1();

    public Rules$createCodeBlockRule$1() {
        super(3);
    }

    @Override // kotlin.jvm.functions.Function3
    public /* bridge */ /* synthetic */ Object invoke(Object obj, Boolean bool, Object obj2) {
        return invoke((CodeNode) obj, bool.booleanValue(), (Rules.BlockQuoteState) obj2);
    }

    /* JADX WARN: Incorrect types in method signature: (Lcom/discord/simpleast/code/CodeNode<TRC;>;ZTS;)Lcom/discord/simpleast/core/node/Node<TRC;>; */
    public final Node invoke(final CodeNode codeNode, boolean z2, Rules.BlockQuoteState blockQuoteState) {
        m.checkNotNullParameter(codeNode, "codeNode");
        m.checkNotNullParameter(blockQuoteState, "state");
        if (!z2) {
            return new Node.a<RC>(new Node[]{codeNode}) { // from class: com.discord.utilities.textprocessing.Rules$createCodeBlockRule$1.1
                /* JADX WARN: Incorrect types in method signature: (Landroid/text/SpannableStringBuilder;TRC;)V */
                public void render(SpannableStringBuilder spannableStringBuilder, BasicRenderContext basicRenderContext) {
                    m.checkNotNullParameter(spannableStringBuilder, "builder");
                    m.checkNotNullParameter(basicRenderContext, "renderContext");
                    int length = spannableStringBuilder.length();
                    super.render(spannableStringBuilder, (SpannableStringBuilder) basicRenderContext);
                    spannableStringBuilder.setSpan(new BackgroundColorSpan(ColorCompat.getThemedColor(basicRenderContext.getContext(), (int) R.attr.theme_chat_code)), length, spannableStringBuilder.length(), 33);
                }
            };
        }
        return new BlockBackgroundNode(blockQuoteState.isInQuote(), codeNode);
    }
}
