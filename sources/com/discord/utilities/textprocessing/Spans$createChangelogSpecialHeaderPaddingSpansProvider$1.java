package com.discord.utilities.textprocessing;

import com.discord.utilities.spans.VerticalPaddingSpan;
import d0.t.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: Spans.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"", "Lcom/discord/utilities/spans/VerticalPaddingSpan;", "invoke", "()Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class Spans$createChangelogSpecialHeaderPaddingSpansProvider$1 extends o implements Function0<List<? extends VerticalPaddingSpan>> {
    public final /* synthetic */ int $headerBottomPx;
    public final /* synthetic */ int $headerTopPx;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public Spans$createChangelogSpecialHeaderPaddingSpansProvider$1(int i, int i2) {
        super(0);
        this.$headerTopPx = i;
        this.$headerBottomPx = i2;
    }

    @Override // kotlin.jvm.functions.Function0
    public final List<? extends VerticalPaddingSpan> invoke() {
        return m.listOf(new VerticalPaddingSpan(this.$headerTopPx, this.$headerBottomPx));
    }
}
