package com.discord.utilities.textprocessing;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import java.util.Set;
import kotlin.Metadata;
/* compiled from: TagsBuilder.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B=\u0012\u0010\u0010\u000b\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002\u0012\u0010\u0010\f\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00070\u0002\u0012\u0010\u0010\r\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\t0\u0002¢\u0006\u0004\b!\u0010\"J\u001a\u0010\u0005\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u001a\u0010\b\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00070\u0002HÆ\u0003¢\u0006\u0004\b\b\u0010\u0006J\u001a\u0010\n\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\t0\u0002HÆ\u0003¢\u0006\u0004\b\n\u0010\u0006JL\u0010\u000e\u001a\u00020\u00002\u0012\b\u0002\u0010\u000b\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00022\u0012\b\u0002\u0010\f\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00070\u00022\u0012\b\u0002\u0010\r\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\t0\u0002HÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0018\u001a\u00020\u00172\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R#\u0010\r\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\t0\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001a\u001a\u0004\b\u001b\u0010\u0006R#\u0010\f\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00070\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001a\u001a\u0004\b\u001c\u0010\u0006R#\u0010\u000b\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001a\u001a\u0004\b\u001d\u0010\u0006R\u0019\u0010\u001e\u001a\u00020\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b\u001e\u0010 ¨\u0006#"}, d2 = {"Lcom/discord/utilities/textprocessing/Tags;", "", "", "", "Lcom/discord/primitives/UserId;", "component1", "()Ljava/util/Set;", "Lcom/discord/primitives/ChannelId;", "component2", "Lcom/discord/primitives/RoleId;", "component3", "users", "channels", "roles", "copy", "(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)Lcom/discord/utilities/textprocessing/Tags;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Set;", "getRoles", "getChannels", "getUsers", "isEmpty", "Z", "()Z", HookHelper.constructorName, "(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class Tags {
    private final Set<Long> channels;
    private final boolean isEmpty;
    private final Set<Long> roles;
    private final Set<Long> users;

    public Tags(Set<Long> set, Set<Long> set2, Set<Long> set3) {
        m.checkNotNullParameter(set, "users");
        m.checkNotNullParameter(set2, "channels");
        m.checkNotNullParameter(set3, "roles");
        this.users = set;
        this.channels = set2;
        this.roles = set3;
        this.isEmpty = set.isEmpty() && set3.isEmpty() && set2.isEmpty();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ Tags copy$default(Tags tags, Set set, Set set2, Set set3, int i, Object obj) {
        if ((i & 1) != 0) {
            set = tags.users;
        }
        if ((i & 2) != 0) {
            set2 = tags.channels;
        }
        if ((i & 4) != 0) {
            set3 = tags.roles;
        }
        return tags.copy(set, set2, set3);
    }

    public final Set<Long> component1() {
        return this.users;
    }

    public final Set<Long> component2() {
        return this.channels;
    }

    public final Set<Long> component3() {
        return this.roles;
    }

    public final Tags copy(Set<Long> set, Set<Long> set2, Set<Long> set3) {
        m.checkNotNullParameter(set, "users");
        m.checkNotNullParameter(set2, "channels");
        m.checkNotNullParameter(set3, "roles");
        return new Tags(set, set2, set3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Tags)) {
            return false;
        }
        Tags tags = (Tags) obj;
        return m.areEqual(this.users, tags.users) && m.areEqual(this.channels, tags.channels) && m.areEqual(this.roles, tags.roles);
    }

    public final Set<Long> getChannels() {
        return this.channels;
    }

    public final Set<Long> getRoles() {
        return this.roles;
    }

    public final Set<Long> getUsers() {
        return this.users;
    }

    public int hashCode() {
        Set<Long> set = this.users;
        int i = 0;
        int hashCode = (set != null ? set.hashCode() : 0) * 31;
        Set<Long> set2 = this.channels;
        int hashCode2 = (hashCode + (set2 != null ? set2.hashCode() : 0)) * 31;
        Set<Long> set3 = this.roles;
        if (set3 != null) {
            i = set3.hashCode();
        }
        return hashCode2 + i;
    }

    public final boolean isEmpty() {
        return this.isEmpty;
    }

    public String toString() {
        StringBuilder R = a.R("Tags(users=");
        R.append(this.users);
        R.append(", channels=");
        R.append(this.channels);
        R.append(", roles=");
        R.append(this.roles);
        R.append(")");
        return R.toString();
    }
}
