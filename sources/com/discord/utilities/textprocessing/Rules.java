package com.discord.utilities.textprocessing;

import andhook.lib.HookHelper;
import android.text.TextUtils;
import android.text.style.CharacterStyle;
import androidx.annotation.ColorInt;
import androidx.exifinterface.media.ExifInterface;
import b.a.t.a.c;
import b.a.t.a.f;
import b.a.t.a.h;
import b.a.t.a.i;
import b.a.t.a.j;
import b.a.t.a.k;
import b.a.t.a.l;
import b.a.t.a.o;
import b.a.t.a.p;
import b.a.t.a.q;
import b.a.t.a.r;
import b.a.t.a.t;
import b.a.t.a.u;
import b.a.t.a.v;
import b.a.t.a.w;
import b.a.t.b.b.e;
import b.a.t.c.a;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.emoji.ModelEmojiUnicode;
import com.discord.simpleast.core.node.Node;
import com.discord.simpleast.core.node.StyleNode;
import com.discord.simpleast.core.parser.ParseSpec;
import com.discord.simpleast.core.parser.Parser;
import com.discord.simpleast.core.parser.Rule;
import com.discord.utilities.textprocessing.Rules;
import com.discord.utilities.textprocessing.node.BasicRenderContext;
import com.discord.utilities.textprocessing.node.BlockQuoteNode;
import com.discord.utilities.textprocessing.node.ChannelMentionNode;
import com.discord.utilities.textprocessing.node.EmojiNode;
import com.discord.utilities.textprocessing.node.RoleMentionNode;
import com.discord.utilities.textprocessing.node.SpoilerNode;
import com.discord.utilities.textprocessing.node.TimestampNode;
import com.discord.utilities.textprocessing.node.UrlNode;
import com.discord.utilities.textprocessing.node.UserMentionNode;
import d0.g;
import d0.g0.s;
import d0.t.h0;
import d0.t.n;
import d0.t.n0;
import d0.z.d.m;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: Rules.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000¶\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\"\n\u0002\u0010\f\n\u0002\b\u001a\bÆ\u0002\u0018\u00002\u00020\u0001:\u0004cdefB\t\b\u0002¢\u0006\u0004\ba\u0010bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\r\u001a\u00020\u000b*\u0004\u0018\u00010\u00072\b\b\u0002\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ;\u0010\u0015\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0014\u0012\u0004\u0012\u00028\u00010\u0013\"\u0004\b\u0000\u0010\u000f\"\u0004\b\u0001\u0010\u00102\b\b\u0001\u0010\u0012\u001a\u00020\u0011¢\u0006\u0004\b\u0015\u0010\u0016J;\u0010\u0017\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0014\u0012\u0004\u0012\u00028\u00010\u0013\"\u0004\b\u0000\u0010\u000f\"\u0004\b\u0001\u0010\u00102\b\b\u0001\u0010\u0012\u001a\u00020\u0011¢\u0006\u0004\b\u0017\u0010\u0016J?\u0010\u001b\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0014\u0012\u0004\u0012\u00028\u00010\u0013\"\b\b\u0000\u0010\u0019*\u00020\u0018\"\u000e\b\u0001\u0010\u0010*\b\u0012\u0004\u0012\u00028\u00010\u001a¢\u0006\u0004\b\u001b\u0010\u001cJ?\u0010\u001d\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0014\u0012\u0004\u0012\u00028\u00010\u0013\"\b\b\u0000\u0010\u0019*\u00020\u0018\"\u000e\b\u0001\u0010\u0010*\b\u0012\u0004\u0012\u00028\u00010\u001a¢\u0006\u0004\b\u001d\u0010\u001cJ?\u0010 \u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u001f\u0012\u0004\u0012\u00028\u00010\u001e\"\b\b\u0000\u0010\u000f*\u00020\u0018\"\u000e\b\u0001\u0010\u0010*\b\u0012\u0004\u0012\u00028\u00010\u001a¢\u0006\u0004\b \u0010!J5\u0010$\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000#\u0012\u0004\u0012\u00028\u00010\u0013\"\b\b\u0000\u0010\u000f*\u00020\"\"\u0004\b\u0001\u0010\u0010¢\u0006\u0004\b$\u0010\u001cJ5\u0010'\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000&\u0012\u0004\u0012\u00028\u00010\u0013\"\b\b\u0000\u0010\u000f*\u00020%\"\u0004\b\u0001\u0010\u0010¢\u0006\u0004\b'\u0010\u001cJ5\u0010)\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0014\u0012\u0004\u0012\u00028\u00010\u0013\"\b\b\u0000\u0010\u000f*\u00020(\"\u0004\b\u0001\u0010\u0010¢\u0006\u0004\b)\u0010\u001cJ5\u0010+\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0014\u0012\u0004\u0012\u00028\u00010\u0013\"\b\b\u0000\u0010\u000f*\u00020*\"\u0004\b\u0001\u0010\u0010¢\u0006\u0004\b+\u0010\u001cJ5\u0010-\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000,\u0012\u0004\u0012\u00028\u00010\u0013\"\b\b\u0000\u0010\u000f*\u00020*\"\u0004\b\u0001\u0010\u0010¢\u0006\u0004\b-\u0010\u001cJ5\u0010.\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0014\u0012\u0004\u0012\u00028\u00010\u0013\"\b\b\u0000\u0010\u000f*\u00020*\"\u0004\b\u0001\u0010\u0010¢\u0006\u0004\b.\u0010\u001cJ1\u00100\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000/\u0012\u0004\u0012\u00028\u00010\u0013\"\u0004\b\u0000\u0010\u000f\"\u0004\b\u0001\u0010\u0010¢\u0006\u0004\b0\u0010\u001cJ5\u00103\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u000002\u0012\u0004\u0012\u00028\u00010\u0013\"\b\b\u0000\u0010\u000f*\u000201\"\u0004\b\u0001\u0010\u0010¢\u0006\u0004\b3\u0010\u001cJ5\u00106\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u000005\u0012\u0004\u0012\u00028\u00010\u0013\"\b\b\u0000\u0010\u000f*\u000204\"\u0004\b\u0001\u0010\u0010¢\u0006\u0004\b6\u0010\u001cJ5\u00107\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u000005\u0012\u0004\u0012\u00028\u00010\u0013\"\b\b\u0000\u0010\u000f*\u000204\"\u0004\b\u0001\u0010\u0010¢\u0006\u0004\b7\u0010\u001cJ5\u00108\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u000005\u0012\u0004\u0012\u00028\u00010\u0013\"\b\b\u0000\u0010\u000f*\u000204\"\u0004\b\u0001\u0010\u0010¢\u0006\u0004\b8\u0010\u001cJ1\u00109\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000/\u0012\u0004\u0012\u00028\u00010\u0013\"\u0004\b\u0000\u0010\u000f\"\u0004\b\u0001\u0010\u0010¢\u0006\u0004\b9\u0010\u001cJ5\u0010<\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000;\u0012\u0004\u0012\u00028\u00010\u0013\"\b\b\u0000\u0010\u000f*\u00020:\"\u0004\b\u0001\u0010\u0010¢\u0006\u0004\b<\u0010\u001cJ5\u0010=\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0014\u0012\u0004\u0012\u00028\u00010\u0013\"\b\b\u0000\u0010\u000f*\u00020*\"\u0004\b\u0001\u0010\u0010¢\u0006\u0004\b=\u0010\u001cJ1\u0010>\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0014\u0012\u0004\u0012\u00028\u00010\u0013\"\u0004\b\u0000\u0010\u000f\"\u0004\b\u0001\u0010\u0010¢\u0006\u0004\b>\u0010\u001cR\u001e\u0010A\u001a\n @*\u0004\u0018\u00010?0?8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bA\u0010BR%\u0010G\u001a\n @*\u0004\u0018\u00010?0?8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bC\u0010D\u001a\u0004\bE\u0010FR\u001e\u0010H\u001a\n @*\u0004\u0018\u00010?0?8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bH\u0010BR\u0016\u0010I\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\bI\u0010JR\u001e\u0010K\u001a\n @*\u0004\u0018\u00010?0?8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bK\u0010BR\u001c\u0010N\u001a\b\u0012\u0004\u0012\u00020M0L8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bN\u0010OR\u001e\u0010P\u001a\n @*\u0004\u0018\u00010?0?8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bP\u0010BR\u001e\u0010Q\u001a\n @*\u0004\u0018\u00010?0?8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bQ\u0010BR\u0016\u0010R\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\bR\u0010JR\u001e\u0010S\u001a\n @*\u0004\u0018\u00010?0?8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bS\u0010BR\u001e\u0010T\u001a\n @*\u0004\u0018\u00010?0?8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bT\u0010BR\u0016\u0010U\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\bU\u0010JR\u001e\u0010V\u001a\n @*\u0004\u0018\u00010?0?8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bV\u0010BR\u001e\u0010W\u001a\n @*\u0004\u0018\u00010?0?8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bW\u0010BR\u0016\u0010X\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\bX\u0010JR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u0003\u0010YR\u001e\u0010Z\u001a\n @*\u0004\u0018\u00010?0?8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bZ\u0010BR\u001e\u0010[\u001a\n @*\u0004\u0018\u00010?0?8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b[\u0010BR\u001e\u0010\\\u001a\n @*\u0004\u0018\u00010?0?8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\\\u0010BR\u0016\u0010]\u001a\u00020\u00078\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b]\u0010JR\u001e\u0010^\u001a\n @*\u0004\u0018\u00010?0?8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b^\u0010BR\u0016\u0010_\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b_\u0010JR\u001e\u0010`\u001a\n @*\u0004\u0018\u00010?0?8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b`\u0010B¨\u0006g"}, d2 = {"Lcom/discord/utilities/textprocessing/Rules;", "", "Lcom/discord/utilities/textprocessing/Rules$EmojiDataProvider;", "emojiDataProvider", "", "setEmojiDataProvider", "(Lcom/discord/utilities/textprocessing/Rules$EmojiDataProvider;)V", "", "originalText", "replaceEmojiSurrogates", "(Ljava/lang/String;)Ljava/lang/String;", "", "default", "toLongOrDefault", "(Ljava/lang/String;J)J", ExifInterface.GPS_DIRECTION_TRUE, ExifInterface.LATITUDE_SOUTH, "", ModelAuditLogEntry.CHANGE_KEY_COLOR, "Lcom/discord/simpleast/core/parser/Rule;", "Lcom/discord/simpleast/core/node/Node;", "createBoldColoredRule", "(I)Lcom/discord/simpleast/core/parser/Rule;", "createStrikethroughColoredRule", "Lcom/discord/utilities/textprocessing/node/BasicRenderContext;", "RC", "Lcom/discord/utilities/textprocessing/Rules$BlockQuoteState;", "createCodeBlockRule", "()Lcom/discord/simpleast/core/parser/Rule;", "createInlineCodeRule", "Lcom/discord/simpleast/core/parser/Rule$BlockRule;", "Lcom/discord/utilities/textprocessing/node/BlockQuoteNode;", "createBlockQuoteRule", "()Lcom/discord/simpleast/core/parser/Rule$BlockRule;", "Lcom/discord/utilities/textprocessing/node/ChannelMentionNode$RenderContext;", "Lcom/discord/utilities/textprocessing/node/ChannelMentionNode;", "createChannelMentionRule", "Lcom/discord/utilities/textprocessing/node/RoleMentionNode$RenderContext;", "Lcom/discord/utilities/textprocessing/node/RoleMentionNode;", "createRoleMentionRule", "Lcom/discord/utilities/textprocessing/node/UserMentionNode$RenderContext;", "createUserMentionRule", "Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;", "createUnicodeEmojiRule", "Lcom/discord/utilities/textprocessing/node/EmojiNode;", "createCustomEmojiRule", "createNamedEmojiRule", "Lb/a/t/b/a/a;", "createUnescapeEmoticonRule", "Lcom/discord/utilities/textprocessing/node/TimestampNode$RenderContext;", "Lcom/discord/utilities/textprocessing/node/TimestampNode;", "createTimestampRule", "Lcom/discord/utilities/textprocessing/node/UrlNode$RenderContext;", "Lcom/discord/utilities/textprocessing/node/UrlNode;", "createUrlRule", "createMaskedLinkRule", "createUrlNoEmbedRule", "createSoftHyphenRule", "Lcom/discord/utilities/textprocessing/node/SpoilerNode$RenderContext;", "Lcom/discord/utilities/textprocessing/node/SpoilerNode;", "createSpoilerRule", "createTextReplacementRule", "createHookedLinkRule", "Ljava/util/regex/Pattern;", "kotlin.jvm.PlatformType", "PATTERN_MENTION", "Ljava/util/regex/Pattern;", "PATTERN_UNICODE_EMOJI$delegate", "Lkotlin/Lazy;", "getPATTERN_UNICODE_EMOJI", "()Ljava/util/regex/Pattern;", "PATTERN_UNICODE_EMOJI", "PATTERN_NAMED_EMOJI", "REGEX_LINK_INSIDE", "Ljava/lang/String;", "PATTERN_MASKED_LINK", "", "", "PATHOLOGICAL_MASKED_LINK_ATTACK_SUSPICIOUS_CHARS", "Ljava/util/Set;", "PATTERN_UNESCAPE_EMOTICON", "PATTERN_HOOKED_LINK", "HOOKED_LINK", "PATTERN_URL", "PATTERN_CUSTOM_EMOJI", "REGEX_LINK_HREF_AND_TITLE", "PATTERN_URL_NO_EMBED", "PATTERN_SOFT_HYPHEN", "LINK", "Lcom/discord/utilities/textprocessing/Rules$EmojiDataProvider;", "PATTERN_CHANNEL_MENTION", "PATTERN_BLOCK_QUOTE", "PATTERN_ROLE_MENTION", "REGEX_CUSTOM_EMOJI", "PATTERN_TIMESTAMP", "REGEX_URL", "PATTERN_SPOILER", HookHelper.constructorName, "()V", "BlockQuoteState", "EmojiDataProvider", "HeaderLineClassedRule", "MarkdownListItemRule", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class Rules {
    public static final String REGEX_CUSTOM_EMOJI = "<(a)?:([a-zA-Z_0-9]+):(\\d+)>";
    private static final String REGEX_LINK_HREF_AND_TITLE = "\\s*<?((?:[^\\s\\\\]|\\\\.)*?)>?(?:\\s+['\"]([\\s\\S]*?)['\"])?\\s*";
    private static final String REGEX_LINK_INSIDE = "(?:\\[[^]]*]|[^]]|](?=[^\\[]*]))*";
    private static final String REGEX_URL = "(https?://[^\\s<]+[^<.,:;\"')\\]\\s])";
    private static EmojiDataProvider emojiDataProvider;
    public static final Rules INSTANCE = new Rules();
    private static final Pattern PATTERN_BLOCK_QUOTE = Pattern.compile("^(?: *>>> +(.*)| *>(?!>>) +([^\\n]*\\n?))", 32);
    private static final Pattern PATTERN_CHANNEL_MENTION = Pattern.compile("^<#(\\d+)>");
    private static final Pattern PATTERN_ROLE_MENTION = Pattern.compile("^<@&(\\d+)>");
    private static final Pattern PATTERN_MENTION = Pattern.compile("^<@!?(\\d+)>|^@(everyone|here)");
    private static final Lazy PATTERN_UNICODE_EMOJI$delegate = g.lazy(Rules$PATTERN_UNICODE_EMOJI$2.INSTANCE);
    private static final Pattern PATTERN_CUSTOM_EMOJI = Pattern.compile("^<(a)?:([a-zA-Z_0-9]+):(\\d+)>");
    private static final Pattern PATTERN_NAMED_EMOJI = Pattern.compile("^:([^\\s:]+?(?:::skin-tone-\\d)?):");
    private static final Pattern PATTERN_UNESCAPE_EMOTICON = Pattern.compile("^(¯\\\\_\\(ツ\\)_/¯)");
    private static final Pattern PATTERN_TIMESTAMP = Pattern.compile("^<t:(-?\\d{1,17})(?::(t|T|d|D|f|F|R))?>");
    private static final Pattern PATTERN_URL = Pattern.compile("^(https?://[^\\s<]+[^<.,:;\"')\\]\\s])");
    private static final String LINK = "^\\[((?:\\[[^]]*]|[^]]|](?=[^\\[]*]))*)]\\(\\s*<?((?:[^\\s\\\\]|\\\\.)*?)>?(?:\\s+['\"]([\\s\\S]*?)['\"])?\\s*\\)";
    private static final Pattern PATTERN_MASKED_LINK = Pattern.compile(LINK);
    private static final Set<Character> PATHOLOGICAL_MASKED_LINK_ATTACK_SUSPICIOUS_CHARS = n0.setOf((Object[]) new Character[]{'[', ']'});
    private static final Pattern PATTERN_URL_NO_EMBED = Pattern.compile("^<(https?://[^\\s<]+[^<.,:;\"')\\]\\s])>");
    private static final Pattern PATTERN_SOFT_HYPHEN = Pattern.compile("^\\u00AD");
    private static final Pattern PATTERN_SPOILER = Pattern.compile("^\\|\\|([\\s\\S]+?)\\|\\|");
    private static final String HOOKED_LINK = "^\\$\\[((?:\\[[^]]*]|[^]]|](?=[^\\[]*]))*)?]\\(\\s*<?((?:[^\\s\\\\]|\\\\.)*?)>?(?:\\s+['\"]([\\s\\S]*?)['\"])?\\s*\\)";
    private static final Pattern PATTERN_HOOKED_LINK = Pattern.compile(HOOKED_LINK);

    /* compiled from: Rules.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\bf\u0018\u0000*\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u00028\u00000\u00002\u00020\u0002J\u0017\u0010\u0005\u001a\u00028\u00002\u0006\u0010\u0004\u001a\u00020\u0003H&¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0004\u001a\u00020\u00038&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0007¨\u0006\b"}, d2 = {"Lcom/discord/utilities/textprocessing/Rules$BlockQuoteState;", "Self", "", "", "isInQuote", "newBlockQuoteState", "(Z)Lcom/discord/utilities/textprocessing/Rules$BlockQuoteState;", "()Z", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface BlockQuoteState<Self extends BlockQuoteState<Self>> {
        boolean isInQuote();

        Self newBlockQuoteState(boolean z2);
    }

    /* compiled from: Rules.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\u001b\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H&¢\u0006\u0004\b\u0005\u0010\u0006J\u001b\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H&¢\u0006\u0004\b\u0007\u0010\u0006J\u000f\u0010\t\u001a\u00020\bH&¢\u0006\u0004\b\t\u0010\n¨\u0006\u000b"}, d2 = {"Lcom/discord/utilities/textprocessing/Rules$EmojiDataProvider;", "", "", "", "Lcom/discord/models/domain/emoji/ModelEmojiUnicode;", "getUnicodeEmojisNamesMap", "()Ljava/util/Map;", "getUnicodeEmojiSurrogateMap", "Ljava/util/regex/Pattern;", "getUnicodeEmojisPattern", "()Ljava/util/regex/Pattern;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface EmojiDataProvider {
        Map<String, ModelEmojiUnicode> getUnicodeEmojiSurrogateMap();

        Map<String, ModelEmojiUnicode> getUnicodeEmojisNamesMap();

        Pattern getUnicodeEmojisPattern();
    }

    /* compiled from: Rules.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u0002*\u0004\b\u0002\u0010\u00032\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00028\u00020\u0004BE\u0012\u0012\u0010\u0011\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00100\u000f\u0012\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\u00150\u0013\u0012\u0014\u0010\u0018\u001a\u0010\u0012\u0004\u0012\u00020\u0017\u0012\u0006\u0012\u0004\u0018\u00018\u00010\u0013¢\u0006\u0004\b\u0019\u0010\u001aJM\u0010\r\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00020\f2\u0006\u0010\u0007\u001a\u00020\u00062 \u0010\n\u001a\u001c\u0012\u0004\u0012\u00028\u0000\u0012\f\b\u0000\u0012\b\u0012\u0004\u0012\u00028\u00000\t\u0012\u0004\u0012\u00028\u00020\b2\u0006\u0010\u000b\u001a\u00028\u0002H\u0016¢\u0006\u0004\b\r\u0010\u000eR\"\u0010\u0011\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00100\u000f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012¨\u0006\u001b"}, d2 = {"Lcom/discord/utilities/textprocessing/Rules$HeaderLineClassedRule;", "RC", ExifInterface.GPS_DIRECTION_TRUE, ExifInterface.LATITUDE_SOUTH, "Lb/a/t/c/a$a;", "", "Ljava/util/regex/Matcher;", "matcher", "Lcom/discord/simpleast/core/parser/Parser;", "Lcom/discord/simpleast/core/node/Node;", "parser", "state", "Lcom/discord/simpleast/core/parser/ParseSpec;", "parse", "(Ljava/util/regex/Matcher;Lcom/discord/simpleast/core/parser/Parser;Ljava/lang/Object;)Lcom/discord/simpleast/core/parser/ParseSpec;", "Lkotlin/Function0;", "", "headerPaddingSpanProvider", "Lkotlin/jvm/functions/Function0;", "Lkotlin/Function1;", "", "Landroid/text/style/CharacterStyle;", "styleSpanProvider", "", "classSpanProvider", HookHelper.constructorName, "(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class HeaderLineClassedRule<RC, T, S> extends a.C0054a<RC, Object, S> {
        private final Function0<List<Object>> headerPaddingSpanProvider;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        /* JADX WARN: Multi-variable type inference failed */
        public HeaderLineClassedRule(Function0<? extends List<? extends Object>> function0, Function1<? super Integer, ? extends CharacterStyle> function1, Function1<? super String, ? extends T> function12) {
            super(function1, function12);
            m.checkNotNullParameter(function0, "headerPaddingSpanProvider");
            m.checkNotNullParameter(function1, "styleSpanProvider");
            m.checkNotNullParameter(function12, "classSpanProvider");
            this.headerPaddingSpanProvider = function0;
        }

        @Override // b.a.t.c.a.C0054a, b.a.t.c.a.b, b.a.t.c.a.c, com.discord.simpleast.core.parser.Rule
        public ParseSpec<RC, S> parse(Matcher matcher, Parser<RC, ? super Node<RC>, S> parser, S s2) {
            m.checkNotNullParameter(matcher, "matcher");
            m.checkNotNullParameter(parser, "parser");
            Node<RC> node = super.parse(matcher, parser, s2).a;
            StyleNode styleNode = new StyleNode(this.headerPaddingSpanProvider.invoke());
            styleNode.addChild(node);
            m.checkNotNullParameter(styleNode, "node");
            return new ParseSpec<>(styleNode, s2);
        }
    }

    /* compiled from: Rules.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0010\u0000\n\u0002\b\u0005\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u00022\u001a\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0004\u0012\u0004\u0012\u00028\u00010\u0003B\u001b\u0012\u0012\u0010\u0010\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\u000e0\r¢\u0006\u0004\b\u0012\u0010\u0013JM\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\n2\u0006\u0010\u0006\u001a\u00020\u00052 \u0010\b\u001a\u001c\u0012\u0004\u0012\u00028\u0000\u0012\f\b\u0000\u0012\b\u0012\u0004\u0012\u00028\u00000\u0004\u0012\u0004\u0012\u00028\u00010\u00072\u0006\u0010\t\u001a\u00028\u0001H\u0016¢\u0006\u0004\b\u000b\u0010\fR\"\u0010\u0010\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\u000e0\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011¨\u0006\u0014"}, d2 = {"Lcom/discord/utilities/textprocessing/Rules$MarkdownListItemRule;", "RC", ExifInterface.LATITUDE_SOUTH, "Lcom/discord/simpleast/core/parser/Rule$BlockRule;", "Lcom/discord/simpleast/core/node/Node;", "Ljava/util/regex/Matcher;", "matcher", "Lcom/discord/simpleast/core/parser/Parser;", "parser", "state", "Lcom/discord/simpleast/core/parser/ParseSpec;", "parse", "(Ljava/util/regex/Matcher;Lcom/discord/simpleast/core/parser/Parser;Ljava/lang/Object;)Lcom/discord/simpleast/core/parser/ParseSpec;", "Lkotlin/Function0;", "", "", "spansProvider", "Lkotlin/jvm/functions/Function0;", HookHelper.constructorName, "(Lkotlin/jvm/functions/Function0;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class MarkdownListItemRule<RC, S> extends Rule.BlockRule<RC, Node<RC>, S> {
        private final Function0<List<Object>> spansProvider;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        /* JADX WARN: Multi-variable type inference failed */
        public MarkdownListItemRule(Function0<? extends List<? extends Object>> function0) {
            super(a.a);
            m.checkNotNullParameter(function0, "spansProvider");
            a aVar = a.e;
            this.spansProvider = function0;
        }

        @Override // com.discord.simpleast.core.parser.Rule
        public ParseSpec<RC, S> parse(Matcher matcher, Parser<RC, ? super Node<RC>, S> parser, S s2) {
            m.checkNotNullParameter(matcher, "matcher");
            m.checkNotNullParameter(parser, "parser");
            StyleNode styleNode = new StyleNode(this.spansProvider.invoke());
            int start = matcher.start(1);
            int end = matcher.end(1);
            m.checkNotNullParameter(styleNode, "node");
            return new ParseSpec<>(styleNode, s2, start, end);
        }
    }

    private Rules() {
    }

    public static final /* synthetic */ EmojiDataProvider access$getEmojiDataProvider$p(Rules rules) {
        EmojiDataProvider emojiDataProvider2 = emojiDataProvider;
        if (emojiDataProvider2 == null) {
            m.throwUninitializedPropertyAccessException("emojiDataProvider");
        }
        return emojiDataProvider2;
    }

    private final Pattern getPATTERN_UNICODE_EMOJI() {
        return (Pattern) PATTERN_UNICODE_EMOJI$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final String replaceEmojiSurrogates(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        EmojiDataProvider emojiDataProvider2 = emojiDataProvider;
        if (emojiDataProvider2 == null) {
            m.throwUninitializedPropertyAccessException("emojiDataProvider");
        }
        Matcher matcher = emojiDataProvider2.getUnicodeEmojisPattern().matcher(str);
        while (matcher.find()) {
            String group = matcher.group();
            EmojiDataProvider emojiDataProvider3 = emojiDataProvider;
            if (emojiDataProvider3 == null) {
                m.throwUninitializedPropertyAccessException("emojiDataProvider");
            }
            ModelEmojiUnicode modelEmojiUnicode = emojiDataProvider3.getUnicodeEmojiSurrogateMap().get(group);
            if (modelEmojiUnicode != null) {
                StringBuilder R = b.d.b.a.a.R(":");
                R.append(modelEmojiUnicode.getFirstName());
                R.append(":");
                matcher.appendReplacement(stringBuffer, R.toString());
            }
        }
        matcher.appendTail(stringBuffer);
        String stringBuffer2 = stringBuffer.toString();
        m.checkNotNullExpressionValue(stringBuffer2, "stringBuffer.toString()");
        return stringBuffer2;
    }

    public static final void setEmojiDataProvider(EmojiDataProvider emojiDataProvider2) {
        m.checkNotNullParameter(emojiDataProvider2, "emojiDataProvider");
        emojiDataProvider = emojiDataProvider2;
    }

    private final long toLongOrDefault(String str, long j) {
        Long longOrNull;
        return (str == null || (longOrNull = s.toLongOrNull(str)) == null) ? j : longOrNull.longValue();
    }

    public static /* synthetic */ long toLongOrDefault$default(Rules rules, String str, long j, int i, Object obj) {
        if ((i & 1) != 0) {
            j = -1;
        }
        return rules.toLongOrDefault(str, j);
    }

    public final <T extends BasicRenderContext, S extends BlockQuoteState<S>> Rule.BlockRule<T, BlockQuoteNode<T>, S> createBlockQuoteRule() {
        final Pattern pattern = PATTERN_BLOCK_QUOTE;
        m.checkNotNullExpressionValue(pattern, "PATTERN_BLOCK_QUOTE");
        return (Rule.BlockRule<T, BlockQuoteNode<T>, S>) new Rule.BlockRule<T, BlockQuoteNode<T>, S>(pattern) { // from class: com.discord.utilities.textprocessing.Rules$createBlockQuoteRule$1
            /* JADX WARN: Incorrect types in method signature: (Ljava/lang/CharSequence;Ljava/lang/String;TS;)Ljava/util/regex/Matcher; */
            public Matcher match(CharSequence charSequence, String str, Rules.BlockQuoteState blockQuoteState) {
                m.checkNotNullParameter(charSequence, "inspectionSource");
                m.checkNotNullParameter(blockQuoteState, "state");
                if (blockQuoteState.isInQuote()) {
                    return null;
                }
                return super.match(charSequence, str, (String) blockQuoteState);
            }

            /* JADX WARN: Incorrect types in method signature: (Ljava/util/regex/Matcher;Lcom/discord/simpleast/core/parser/Parser<TT;-Lcom/discord/utilities/textprocessing/node/BlockQuoteNode<TT;>;TS;>;TS;)Lcom/discord/simpleast/core/parser/ParseSpec<TT;TS;>; */
            public ParseSpec parse(Matcher matcher, Parser parser, Rules.BlockQuoteState blockQuoteState) {
                m.checkNotNullParameter(matcher, "matcher");
                m.checkNotNullParameter(parser, "parser");
                m.checkNotNullParameter(blockQuoteState, "state");
                int i = matcher.group(1) != null ? 1 : 2;
                Rules.BlockQuoteState newBlockQuoteState = blockQuoteState.newBlockQuoteState(true);
                BlockQuoteNode blockQuoteNode = new BlockQuoteNode();
                int start = matcher.start(i);
                int end = matcher.end(i);
                m.checkNotNullParameter(blockQuoteNode, "node");
                return new ParseSpec(blockQuoteNode, newBlockQuoteState, start, end);
            }
        };
    }

    public final <T, S> Rule<T, Node<T>, S> createBoldColoredRule(@ColorInt int i) {
        e eVar = e.h;
        Pattern pattern = e.a;
        m.checkNotNullExpressionValue(pattern, "PATTERN_BOLD");
        return e.c(pattern, new Rules$createBoldColoredRule$1(i));
    }

    public final <T extends ChannelMentionNode.RenderContext, S> Rule<T, ChannelMentionNode<T>, S> createChannelMentionRule() {
        final Pattern pattern = PATTERN_CHANNEL_MENTION;
        m.checkNotNullExpressionValue(pattern, "PATTERN_CHANNEL_MENTION");
        return (Rule<T, ChannelMentionNode<T>, S>) new Rule<T, ChannelMentionNode<T>, S>(pattern) { // from class: com.discord.utilities.textprocessing.Rules$createChannelMentionRule$1
            @Override // com.discord.simpleast.core.parser.Rule
            public ParseSpec<T, S> parse(Matcher matcher, Parser<T, ? super ChannelMentionNode<T>, S> parser, S s2) {
                m.checkNotNullParameter(matcher, "matcher");
                m.checkNotNullParameter(parser, "parser");
                ChannelMentionNode channelMentionNode = new ChannelMentionNode(Rules.toLongOrDefault$default(Rules.INSTANCE, matcher.group(1), 0L, 1, null));
                m.checkNotNullParameter(channelMentionNode, "node");
                return new ParseSpec<>(channelMentionNode, s2);
            }
        };
    }

    public final <RC extends BasicRenderContext, S extends BlockQuoteState<S>> Rule<RC, Node<RC>, S> createCodeBlockRule() {
        List a;
        List a2;
        List a3;
        List a4;
        List a5;
        List a6;
        f fVar = new f(Rules$createCodeBlockRule$codeStyleProviders$1.INSTANCE, Rules$createCodeBlockRule$codeStyleProviders$2.INSTANCE, Rules$createCodeBlockRule$codeStyleProviders$3.INSTANCE, Rules$createCodeBlockRule$codeStyleProviders$4.INSTANCE, Rules$createCodeBlockRule$codeStyleProviders$5.INSTANCE, Rules$createCodeBlockRule$codeStyleProviders$6.INSTANCE, Rules$createCodeBlockRule$codeStyleProviders$7.INSTANCE, Rules$createCodeBlockRule$codeStyleProviders$8.INSTANCE);
        b.a.t.a.e eVar = b.a.t.a.e.f;
        m.checkNotNullParameter(fVar, "codeStyleProviders");
        o oVar = o.f;
        m.checkNotNullParameter(fVar, "codeStyleProviders");
        Pattern pattern = o.c;
        m.checkNotNullExpressionValue(pattern, "PATTERN_KOTLIN_COMMENTS");
        Pattern pattern2 = o.e;
        m.checkNotNullExpressionValue(pattern2, "PATTERN_KOTLIN_STRINGS");
        Pattern pattern3 = o.d;
        m.checkNotNullExpressionValue(pattern3, "PATTERN_KOTLIN_ANNOTATION");
        o.a.C0051a aVar = o.a.f295b;
        m.checkNotNullParameter(fVar, "codeStyleProviders");
        Pattern pattern4 = o.a.a;
        m.checkNotNullExpressionValue(pattern4, "PATTERN_KOTLIN_FIELD");
        o.b.a aVar2 = o.b.f296b;
        m.checkNotNullParameter(fVar, "codeStyleProviders");
        a = eVar.a(fVar, n.listOf((Object[]) new Rule[]{b.a.t.a.e.e(eVar, pattern, 0, fVar.f287b, 1), b.a.t.a.e.e(eVar, pattern2, 0, fVar.c, 1), b.a.t.a.e.e(eVar, pattern3, 0, fVar.g, 1), new b.a.t.a.n(aVar, fVar, pattern4), new p(aVar2, fVar, o.b.a)}), new String[]{"object", "class", "interface"}, o.f294b, o.a, (r14 & 32) != 0 ? new String[]{" "} : null);
        Pattern c = eVar.c("//");
        m.checkNotNullExpressionValue(c, "createSingleLineCommentPattern(\"//\")");
        Pattern compile = Pattern.compile("^\"[\\s\\S]*?(?<!\\\\)\"(?=\\W|\\s|$)");
        m.checkNotNullExpressionValue(compile, "Pattern.compile(\"\"\"^\"[\\s…*?(?<!\\\\)\"(?=\\W|\\s|$)\"\"\")");
        a2 = eVar.a(fVar, n.listOf((Object[]) new Rule[]{b.a.t.a.e.e(eVar, c, 0, fVar.f287b, 1), b.a.t.a.e.e(eVar, compile, 0, fVar.c, 1)}), new String[]{"message|enum|extend|service"}, new String[]{"true|false", "string|bool|double|float|bytes", "int32|uint32|sint32|int64|unit64|sint64", "map"}, new String[]{"required|repeated|optional|option|oneof|default|reserved", "package|import", "rpc|returns"}, (r14 & 32) != 0 ? new String[]{" "} : null);
        Pattern c2 = eVar.c("#");
        m.checkNotNullExpressionValue(c2, "createSingleLineCommentPattern(\"#\")");
        Pattern compile2 = Pattern.compile("^\"[\\s\\S]*?(?<!\\\\)\"(?=\\W|\\s|$)");
        m.checkNotNullExpressionValue(compile2, "Pattern.compile(\"\"\"^\"[\\s…*?(?<!\\\\)\"(?=\\W|\\s|$)\"\"\")");
        Pattern compile3 = Pattern.compile("^'[\\s\\S]*?(?<!\\\\)'(?=\\W|\\s|$)");
        m.checkNotNullExpressionValue(compile3, "Pattern.compile(\"\"\"^'[\\s…*?(?<!\\\\)'(?=\\W|\\s|$)\"\"\")");
        Pattern compile4 = Pattern.compile("^@(\\w+)");
        m.checkNotNullExpressionValue(compile4, "Pattern.compile(\"\"\"^@(\\w+)\"\"\")");
        a3 = eVar.a(fVar, n.listOf((Object[]) new Rule[]{b.a.t.a.e.e(eVar, c2, 0, fVar.f287b, 1), b.a.t.a.e.e(eVar, compile2, 0, fVar.c, 1), b.a.t.a.e.e(eVar, compile3, 0, fVar.c, 1), b.a.t.a.e.e(eVar, compile4, 0, fVar.g, 1)}), new String[]{"class", "def", "lambda"}, new String[]{"True|False|None"}, new String[]{"from|import|global|nonlocal", "async|await|class|self|cls|def|lambda", "for|while|if|else|elif|break|continue|return", "try|except|finally|raise|pass|yeild", "in|as|is|del", "and|or|not|assert"}, (r14 & 32) != 0 ? new String[]{" "} : null);
        Pattern c3 = eVar.c("//");
        m.checkNotNullExpressionValue(c3, "createSingleLineCommentPattern(\"//\")");
        Pattern compile5 = Pattern.compile("^\"[\\s\\S]*?(?<!\\\\)\"(?=\\W|\\s|$)");
        m.checkNotNullExpressionValue(compile5, "Pattern.compile(\"\"\"^\"[\\s…*?(?<!\\\\)\"(?=\\W|\\s|$)\"\"\")");
        Pattern compile6 = Pattern.compile("^#!?\\[.*?\\]\\n");
        m.checkNotNullExpressionValue(compile6, "Pattern.compile(\"\"\"^#!?\\[.*?\\]\\n\"\"\")");
        a4 = eVar.a(fVar, n.listOf((Object[]) new Rule[]{b.a.t.a.e.e(eVar, c3, 0, fVar.f287b, 1), b.a.t.a.e.e(eVar, compile5, 0, fVar.c, 1), b.a.t.a.e.e(eVar, compile6, 0, fVar.g, 1)}), new String[]{"struct", "trait", "mod"}, new String[]{"Self|Result|Ok|Err|Option|None|Some", "Copy|Clone|Eq|Hash|Send|Sync|Sized|Debug|Display", "Arc|Rc|Box|Pin|Future", "true|false|bool|usize|i64|u64|u32|i32|str|String"}, new String[]{"let|mut|static|const|unsafe", "crate|mod|extern|pub|pub(super)|use", "struct|enum|trait|type|where|impl|dyn|async|await|move|self|fn", "for|while|loop|if|else|match|break|continue|return|try", "in|as|ref"}, (r14 & 32) != 0 ? new String[]{" "} : null);
        v vVar = v.c;
        m.checkNotNullParameter(fVar, "codeStyleProviders");
        Pattern pattern5 = b.a.t.a.e.c;
        Pattern pattern6 = b.a.t.a.e.d;
        List listOf = n.listOf((Object[]) new Rule[]{b.a.t.a.e.e(eVar, v.a, 0, fVar.f287b, 1), new w(vVar, fVar, v.f302b), b.a.t.a.e.e(eVar, pattern5, 0, null, 3), b.a.t.a.e.e(eVar, pattern6, 0, null, 3)});
        Pattern c4 = eVar.c("#");
        m.checkNotNullExpressionValue(c4, "createSingleLineCommentPattern(\"#\")");
        Pattern compile7 = Pattern.compile("^\"[\\s\\S]*?(?<!\\\\)\"(?=\\W|\\s|$)");
        m.checkNotNullExpressionValue(compile7, "Pattern.compile(\"\"\"^\"[\\s…*?(?<!\\\\)\"(?=\\W|\\s|$)\"\"\")");
        String pattern7 = eVar.d("true|false|null").pattern();
        m.checkNotNullExpressionValue(pattern7, "createWordPattern(\"true|false|null\").pattern()");
        Pattern compile8 = Pattern.compile(pattern7, 2);
        m.checkNotNullExpressionValue(compile8, "java.util.regex.Pattern.compile(this, flags)");
        String pattern8 = eVar.d("select|from|join|where|and|as|distinct|count|avg", "order by|group by|desc|sum|min|max", "like|having|in|is|not").pattern();
        m.checkNotNullExpressionValue(pattern8, "createWordPattern(\n     …ing|in|is|not\").pattern()");
        Pattern compile9 = Pattern.compile(pattern8, 2);
        m.checkNotNullExpressionValue(compile9, "java.util.regex.Pattern.compile(this, flags)");
        List listOf2 = n.listOf((Object[]) new Rule[]{b.a.t.a.e.e(eVar, c4, 0, fVar.f287b, 1), b.a.t.a.e.e(eVar, compile7, 0, fVar.c, 1), b.a.t.a.e.e(eVar, compile8, 0, fVar.g, 1), b.a.t.a.e.e(eVar, compile9, 0, fVar.d, 1), b.a.t.a.e.e(eVar, b.a.t.a.e.e, 0, fVar.c, 1), b.a.t.a.e.e(eVar, pattern5, 0, null, 3), b.a.t.a.e.e(eVar, pattern6, 0, null, 3)});
        i iVar = i.h;
        m.checkNotNullParameter(fVar, "codeStyleProviders");
        Pattern pattern9 = i.c;
        m.checkNotNullExpressionValue(pattern9, "PATTERN_CRYSTAL_COMMENTS");
        Pattern pattern10 = i.e;
        m.checkNotNullExpressionValue(pattern10, "PATTERN_CRYSTAL_STRINGS");
        Pattern pattern11 = i.f;
        m.checkNotNullExpressionValue(pattern11, "PATTERN_CRYSTAL_REGEX");
        Pattern pattern12 = i.d;
        m.checkNotNullExpressionValue(pattern12, "PATTERN_CRYSTAL_ANNOTATION");
        Pattern pattern13 = i.g;
        m.checkNotNullExpressionValue(pattern13, "PATTERN_CRYSTAL_SYMBOL");
        i.a.C0049a aVar3 = i.a.f289b;
        m.checkNotNullParameter(fVar, "codeStyleProviders");
        Pattern pattern14 = i.a.a;
        m.checkNotNullExpressionValue(pattern14, "PATTERN_CRYSTAL_FUNC");
        a5 = eVar.a(fVar, n.listOf((Object[]) new Rule[]{b.a.t.a.e.e(eVar, pattern9, 0, fVar.f287b, 1), b.a.t.a.e.e(eVar, pattern10, 0, fVar.c, 1), b.a.t.a.e.e(eVar, pattern11, 0, fVar.c, 1), b.a.t.a.e.e(eVar, pattern12, 0, fVar.g, 1), b.a.t.a.e.e(eVar, pattern13, 0, fVar.c, 1), new h(aVar3, fVar, pattern14)}), new String[]{"def", "class"}, i.f288b, i.a, (r14 & 32) != 0 ? new String[]{" "} : null);
        k kVar = k.g;
        m.checkNotNullParameter(fVar, "codeStyleProviders");
        Pattern pattern15 = k.e;
        m.checkNotNullExpressionValue(pattern15, "PATTERN_JAVASCRIPT_COMMENTS");
        Pattern pattern16 = k.f;
        m.checkNotNullExpressionValue(pattern16, "PATTERN_JAVASCRIPT_STRINGS");
        k.c.a aVar4 = k.c.f293b;
        m.checkNotNullParameter(fVar, "codeStyleProviders");
        Pattern pattern17 = k.c.a;
        m.checkNotNullExpressionValue(pattern17, "PATTERN_JAVASCRIPT_OBJECT_PROPERTY");
        Pattern pattern18 = k.d;
        m.checkNotNullExpressionValue(pattern18, "PATTERN_JAVASCRIPT_GENERIC");
        Pattern pattern19 = k.c;
        m.checkNotNullExpressionValue(pattern19, "PATTERN_JAVASCRIPT_REGEX");
        k.a.C0050a aVar5 = k.a.f291b;
        m.checkNotNullParameter(fVar, "codeStyleProviders");
        Pattern pattern20 = k.a.a;
        m.checkNotNullExpressionValue(pattern20, "PATTERN_JAVASCRIPT_FIELD");
        k.b.a aVar6 = k.b.f292b;
        m.checkNotNullParameter(fVar, "codeStyleProviders");
        a6 = eVar.a(fVar, n.listOf((Object[]) new Rule[]{b.a.t.a.e.e(eVar, pattern15, 0, fVar.f287b, 1), b.a.t.a.e.e(eVar, pattern16, 0, fVar.c, 1), new b.a.t.a.m(aVar4, fVar, pattern17), b.a.t.a.e.e(eVar, pattern18, 0, fVar.g, 1), b.a.t.a.e.e(eVar, pattern19, 0, fVar.c, 1), new j(aVar5, fVar, pattern20), new l(aVar6, fVar, k.b.a)}), new String[]{"class"}, k.f290b, k.a, (r14 & 32) != 0 ? new String[]{" "} : null);
        r rVar = r.g;
        m.checkNotNullParameter(fVar, "codeStyleProviders");
        Pattern pattern21 = r.e;
        m.checkNotNullExpressionValue(pattern21, "PATTERN_TYPESCRIPT_COMMENTS");
        Pattern pattern22 = r.f;
        m.checkNotNullExpressionValue(pattern22, "PATTERN_TYPESCRIPT_STRINGS");
        r.d.a aVar7 = r.d.f301b;
        m.checkNotNullParameter(fVar, "codeStyleProviders");
        Pattern pattern23 = r.d.a;
        m.checkNotNullExpressionValue(pattern23, "PATTERN_TYPESCRIPT_OBJECT_PROPERTY");
        Pattern pattern24 = r.d;
        m.checkNotNullExpressionValue(pattern24, "PATTERN_TYPESCRIPT_REGEX");
        r.b.a aVar8 = r.b.f299b;
        m.checkNotNullParameter(fVar, "codeStyleProviders");
        Pattern pattern25 = r.b.a;
        m.checkNotNullExpressionValue(pattern25, "PATTERN_TYPESCRIPT_FIELD");
        r.c.a aVar9 = r.c.f300b;
        m.checkNotNullParameter(fVar, "codeStyleProviders");
        r.a.C0052a aVar10 = r.a.f298b;
        m.checkNotNullParameter(fVar, "codeStyleProviders");
        Pattern pattern26 = r.a.a;
        m.checkNotNullExpressionValue(pattern26, "PATTERN_TYPESCRIPT_DECORATOR");
        List a7 = eVar.a(fVar, n.listOf((Object[]) new Rule[]{b.a.t.a.e.e(eVar, pattern21, 0, fVar.f287b, 1), b.a.t.a.e.e(eVar, pattern22, 0, fVar.c, 1), new u(aVar7, fVar, pattern23), b.a.t.a.e.e(eVar, pattern24, 0, fVar.c, 1), new b.a.t.a.s(aVar8, fVar, pattern25), new t(aVar9, fVar, r.c.a), new q(aVar10, fVar, pattern26)}), new String[]{"class", "interface", "enum", "namespace", "module", "type"}, r.f297b, r.a, r.c);
        Map mapOf = h0.mapOf(d0.o.to("kt", a), d0.o.to("kotlin", a), d0.o.to("protobuf", a2), d0.o.to("proto", a2), d0.o.to("pb", a2), d0.o.to("py", a3), d0.o.to("python", a3), d0.o.to("rs", a4), d0.o.to("rust", a4), d0.o.to("cql", listOf2), d0.o.to("sql", listOf2), d0.o.to("xml", listOf), d0.o.to("http", listOf), d0.o.to("cr", a5), d0.o.to("crystal", a5), d0.o.to("js", a6), d0.o.to("javascript", a6), d0.o.to("ts", a7), d0.o.to("typescript", a7));
        b.a.t.a.e eVar2 = b.a.t.a.e.f;
        StyleNode.a<R> aVar11 = fVar.a;
        Rules$createCodeBlockRule$1 rules$createCodeBlockRule$1 = Rules$createCodeBlockRule$1.INSTANCE;
        m.checkNotNullParameter(aVar11, "textStyleProvider");
        m.checkNotNullParameter(mapOf, "languageMap");
        m.checkNotNullParameter(rules$createCodeBlockRule$1, "wrapperNodeProvider");
        return new b.a.t.a.a(eVar2, mapOf, aVar11, rules$createCodeBlockRule$1, b.a.t.a.e.a);
    }

    public final <T extends EmojiNode.RenderContext, S> Rule<T, EmojiNode<T>, S> createCustomEmojiRule() {
        final Pattern pattern = PATTERN_CUSTOM_EMOJI;
        m.checkNotNullExpressionValue(pattern, "PATTERN_CUSTOM_EMOJI");
        return (Rule<T, EmojiNode<T>, S>) new Rule<T, EmojiNode<T>, S>(pattern) { // from class: com.discord.utilities.textprocessing.Rules$createCustomEmojiRule$1
            @Override // com.discord.simpleast.core.parser.Rule
            public ParseSpec<T, S> parse(Matcher matcher, Parser<T, ? super EmojiNode<T>, S> parser, S s2) {
                m.checkNotNullParameter(matcher, "matcher");
                m.checkNotNullParameter(parser, "parser");
                boolean isEmpty = true ^ TextUtils.isEmpty(matcher.group(1));
                String group = matcher.group(2);
                m.checkNotNull(group);
                long longOrDefault$default = Rules.toLongOrDefault$default(Rules.INSTANCE, matcher.group(3), 0L, 1, null);
                EmojiNode emojiNode = new EmojiNode(group, new Rules$createCustomEmojiRule$1$parse$emojiNode$1(longOrDefault$default, isEmpty), new EmojiNode.EmojiIdAndType.Custom(longOrDefault$default, isEmpty, group), 0, 0, 24, null);
                m.checkNotNullParameter(emojiNode, "node");
                return new ParseSpec<>(emojiNode, s2);
            }
        };
    }

    public final <T, S> Rule<T, Node<T>, S> createHookedLinkRule() {
        final Pattern pattern = PATTERN_HOOKED_LINK;
        m.checkNotNullExpressionValue(pattern, "PATTERN_HOOKED_LINK");
        return new Rule<T, Node<T>, S>(pattern) { // from class: com.discord.utilities.textprocessing.Rules$createHookedLinkRule$1
            @Override // com.discord.simpleast.core.parser.Rule
            public ParseSpec<T, S> parse(Matcher matcher, Parser<T, ? super Node<T>, S> parser, S s2) {
                m.checkNotNullParameter(matcher, "matcher");
                m.checkNotNullParameter(parser, "parser");
                StyleNode styleNode = new StyleNode(n.emptyList());
                int start = matcher.start(1);
                int end = matcher.end(1);
                m.checkNotNullParameter(styleNode, "node");
                return new ParseSpec<>(styleNode, s2, start, end);
            }
        };
    }

    public final <RC extends BasicRenderContext, S extends BlockQuoteState<S>> Rule<RC, Node<RC>, S> createInlineCodeRule() {
        b.a.t.a.e eVar = b.a.t.a.e.f;
        Rules$createInlineCodeRule$1 rules$createInlineCodeRule$1 = Rules$createInlineCodeRule$1.INSTANCE;
        Rules$createInlineCodeRule$2 rules$createInlineCodeRule$2 = Rules$createInlineCodeRule$2.INSTANCE;
        m.checkNotNullParameter(rules$createInlineCodeRule$1, "textStyleProvider");
        m.checkNotNullParameter(rules$createInlineCodeRule$2, "bgStyleProvider");
        return new c(eVar, rules$createInlineCodeRule$1, rules$createInlineCodeRule$2, b.a.t.a.e.f286b);
    }

    public final <T extends UrlNode.RenderContext, S> Rule<T, UrlNode<T>, S> createMaskedLinkRule() {
        final Pattern pattern = PATTERN_MASKED_LINK;
        m.checkNotNullExpressionValue(pattern, "PATTERN_MASKED_LINK");
        return (Rule<T, UrlNode<T>, S>) new Rule<T, UrlNode<T>, S>(pattern) { // from class: com.discord.utilities.textprocessing.Rules$createMaskedLinkRule$1
            private final boolean isLikelyPathologicalAttack(CharSequence charSequence) {
                Set set;
                if (charSequence.length() < 30) {
                    return false;
                }
                double length = charSequence.length() * 0.3d;
                int length2 = charSequence.length();
                int i = 0;
                for (int i2 = 0; i2 < length2; i2++) {
                    Rules rules = Rules.INSTANCE;
                    set = Rules.PATHOLOGICAL_MASKED_LINK_ATTACK_SUSPICIOUS_CHARS;
                    if (set.contains(Character.valueOf(charSequence.charAt(i2)))) {
                        i++;
                        if (i > length) {
                            return true;
                        }
                    }
                }
                return false;
            }

            @Override // com.discord.simpleast.core.parser.Rule
            public Matcher match(CharSequence charSequence, String str, S s2) {
                m.checkNotNullParameter(charSequence, "inspectionSource");
                if (isLikelyPathologicalAttack(charSequence)) {
                    return null;
                }
                return super.match(charSequence, str, s2);
            }

            @Override // com.discord.simpleast.core.parser.Rule
            public ParseSpec<T, S> parse(Matcher matcher, Parser<T, ? super UrlNode<T>, S> parser, S s2) {
                m.checkNotNullParameter(matcher, "matcher");
                m.checkNotNullParameter(parser, "parser");
                String group = matcher.group(1);
                m.checkNotNull(group);
                String group2 = matcher.group(2);
                m.checkNotNull(group2);
                UrlNode urlNode = new UrlNode(group2, group);
                m.checkNotNullParameter(urlNode, "node");
                return new ParseSpec<>(urlNode, s2);
            }
        };
    }

    public final <T extends EmojiNode.RenderContext, S> Rule<T, Node<T>, S> createNamedEmojiRule() {
        final Pattern pattern = PATTERN_NAMED_EMOJI;
        m.checkNotNullExpressionValue(pattern, "PATTERN_NAMED_EMOJI");
        return (Rule<T, Node<T>, S>) new Rule<T, Node<T>, S>(pattern) { // from class: com.discord.utilities.textprocessing.Rules$createNamedEmojiRule$1
            @Override // com.discord.simpleast.core.parser.Rule
            public ParseSpec<T, S> parse(Matcher matcher, Parser<T, ? super Node<T>, S> parser, S s2) {
                m.checkNotNullParameter(matcher, "matcher");
                m.checkNotNullParameter(parser, "parser");
                String group = matcher.group(1);
                m.checkNotNull(group);
                ModelEmojiUnicode modelEmojiUnicode = Rules.access$getEmojiDataProvider$p(Rules.INSTANCE).getUnicodeEmojisNamesMap().get(group);
                if (modelEmojiUnicode != null) {
                    EmojiNode from$default = EmojiNode.Companion.from$default(EmojiNode.Companion, modelEmojiUnicode, 0, 2, (Object) null);
                    m.checkNotNullParameter(from$default, "node");
                    return new ParseSpec<>(from$default, s2);
                }
                String group2 = matcher.group();
                m.checkNotNullExpressionValue(group2, "matcher.group()");
                b.a.t.b.a.a aVar = new b.a.t.b.a.a(group2);
                m.checkNotNullParameter(aVar, "node");
                return new ParseSpec<>(aVar, s2);
            }
        };
    }

    public final <T extends RoleMentionNode.RenderContext, S> Rule<T, RoleMentionNode<T>, S> createRoleMentionRule() {
        final Pattern pattern = PATTERN_ROLE_MENTION;
        m.checkNotNullExpressionValue(pattern, "PATTERN_ROLE_MENTION");
        return (Rule<T, RoleMentionNode<T>, S>) new Rule<T, RoleMentionNode<T>, S>(pattern) { // from class: com.discord.utilities.textprocessing.Rules$createRoleMentionRule$1
            @Override // com.discord.simpleast.core.parser.Rule
            public ParseSpec<T, S> parse(Matcher matcher, Parser<T, ? super RoleMentionNode<T>, S> parser, S s2) {
                m.checkNotNullParameter(matcher, "matcher");
                m.checkNotNullParameter(parser, "parser");
                RoleMentionNode roleMentionNode = new RoleMentionNode(Rules.toLongOrDefault$default(Rules.INSTANCE, matcher.group(1), 0L, 1, null));
                m.checkNotNullParameter(roleMentionNode, "node");
                return new ParseSpec<>(roleMentionNode, s2);
            }
        };
    }

    public final <T, S> Rule<T, b.a.t.b.a.a<T>, S> createSoftHyphenRule() {
        final Pattern pattern = PATTERN_SOFT_HYPHEN;
        m.checkNotNullExpressionValue(pattern, "PATTERN_SOFT_HYPHEN");
        return new Rule<T, b.a.t.b.a.a<T>, S>(pattern) { // from class: com.discord.utilities.textprocessing.Rules$createSoftHyphenRule$1
            @Override // com.discord.simpleast.core.parser.Rule
            public ParseSpec<T, S> parse(Matcher matcher, Parser<T, ? super b.a.t.b.a.a<T>, S> parser, S s2) {
                m.checkNotNullParameter(matcher, "matcher");
                m.checkNotNullParameter(parser, "parser");
                b.a.t.b.a.a aVar = new b.a.t.b.a.a("");
                m.checkNotNullParameter(aVar, "node");
                return new ParseSpec<>(aVar, s2);
            }
        };
    }

    public final <T extends SpoilerNode.RenderContext, S> Rule<T, SpoilerNode<T>, S> createSpoilerRule() {
        final Pattern pattern = PATTERN_SPOILER;
        m.checkNotNullExpressionValue(pattern, "PATTERN_SPOILER");
        return (Rule<T, SpoilerNode<T>, S>) new Rule<T, SpoilerNode<T>, S>(pattern) { // from class: com.discord.utilities.textprocessing.Rules$createSpoilerRule$1
            @Override // com.discord.simpleast.core.parser.Rule
            public ParseSpec<T, S> parse(Matcher matcher, Parser<T, ? super SpoilerNode<T>, S> parser, S s2) {
                m.checkNotNullParameter(matcher, "matcher");
                m.checkNotNullParameter(parser, "parser");
                String group = matcher.group(1);
                m.checkNotNull(group);
                SpoilerNode spoilerNode = new SpoilerNode(group);
                int start = matcher.start(1);
                int end = matcher.end(1);
                m.checkNotNullParameter(spoilerNode, "node");
                return new ParseSpec<>(spoilerNode, s2, start, end);
            }
        };
    }

    public final <T, S> Rule<T, Node<T>, S> createStrikethroughColoredRule(@ColorInt int i) {
        e eVar = e.h;
        Pattern pattern = e.c;
        m.checkNotNullExpressionValue(pattern, "PATTERN_STRIKETHRU");
        return e.c(pattern, new Rules$createStrikethroughColoredRule$1(i));
    }

    public final <T extends EmojiNode.RenderContext, S> Rule<T, Node<T>, S> createTextReplacementRule() {
        e eVar = e.h;
        final Pattern pattern = e.e;
        m.checkNotNullExpressionValue(pattern, "SimpleMarkdownRules.PATTERN_TEXT");
        return (Rule<T, Node<T>, S>) new Rule<T, Node<T>, S>(pattern) { // from class: com.discord.utilities.textprocessing.Rules$createTextReplacementRule$1
            private final List<Rule<T, Node<T>, S>> innerRules = n.listOf((Object[]) new Rule[]{Rules.INSTANCE.createNamedEmojiRule(), e.h.d()});

            public final List<Rule<T, Node<T>, S>> getInnerRules() {
                return this.innerRules;
            }

            @Override // com.discord.simpleast.core.parser.Rule
            public ParseSpec<T, S> parse(Matcher matcher, Parser<T, ? super Node<T>, S> parser, S s2) {
                String replaceEmojiSurrogates;
                Node node;
                m.checkNotNullParameter(matcher, "matcher");
                m.checkNotNullParameter(parser, "parser");
                Rules rules = Rules.INSTANCE;
                String group = matcher.group();
                m.checkNotNullExpressionValue(group, "matcher.group()");
                replaceEmojiSurrogates = rules.replaceEmojiSurrogates(group);
                List parse = parser.parse(replaceEmojiSurrogates, s2, this.innerRules);
                if (parse.size() == 1) {
                    Object first = d0.t.u.first((List<? extends Object>) parse);
                    Objects.requireNonNull(first, "null cannot be cast to non-null type com.discord.simpleast.core.node.Node<T>");
                    node = (Node) first;
                } else {
                    StyleNode styleNode = new StyleNode(n.emptyList());
                    for (Object obj : parse) {
                        Objects.requireNonNull(obj, "null cannot be cast to non-null type com.discord.simpleast.core.node.Node<T>");
                        styleNode.addChild((Node) obj);
                    }
                    node = styleNode;
                }
                m.checkNotNullParameter(node, "node");
                return new ParseSpec<>(node, s2);
            }
        };
    }

    public final <T extends TimestampNode.RenderContext, S> Rule<T, TimestampNode<T>, S> createTimestampRule() {
        final Pattern pattern = PATTERN_TIMESTAMP;
        m.checkNotNullExpressionValue(pattern, "PATTERN_TIMESTAMP");
        return (Rule<T, TimestampNode<T>, S>) new Rule<T, TimestampNode<T>, S>(pattern) { // from class: com.discord.utilities.textprocessing.Rules$createTimestampRule$1
            @Override // com.discord.simpleast.core.parser.Rule
            public ParseSpec<T, S> parse(Matcher matcher, Parser<T, ? super TimestampNode<T>, S> parser, S s2) {
                m.checkNotNullParameter(matcher, "matcher");
                m.checkNotNullParameter(parser, "parser");
                String group = matcher.group(1);
                m.checkNotNull(group);
                TimestampNode timestampNode = new TimestampNode(group, matcher.group(2));
                m.checkNotNullParameter(timestampNode, "node");
                return new ParseSpec<>(timestampNode, s2);
            }
        };
    }

    public final <T, S> Rule<T, b.a.t.b.a.a<T>, S> createUnescapeEmoticonRule() {
        final Pattern pattern = PATTERN_UNESCAPE_EMOTICON;
        m.checkNotNullExpressionValue(pattern, "PATTERN_UNESCAPE_EMOTICON");
        return new Rule<T, b.a.t.b.a.a<T>, S>(pattern) { // from class: com.discord.utilities.textprocessing.Rules$createUnescapeEmoticonRule$1
            @Override // com.discord.simpleast.core.parser.Rule
            public ParseSpec<T, S> parse(Matcher matcher, Parser<T, ? super b.a.t.b.a.a<T>, S> parser, S s2) {
                m.checkNotNullParameter(matcher, "matcher");
                m.checkNotNullParameter(parser, "parser");
                String group = matcher.group(1);
                m.checkNotNull(group);
                b.a.t.b.a.a aVar = new b.a.t.b.a.a(group);
                m.checkNotNullParameter(aVar, "node");
                return new ParseSpec<>(aVar, s2);
            }
        };
    }

    public final <T extends EmojiNode.RenderContext, S> Rule<T, Node<T>, S> createUnicodeEmojiRule() {
        final Pattern pattern_unicode_emoji = getPATTERN_UNICODE_EMOJI();
        m.checkNotNullExpressionValue(pattern_unicode_emoji, "PATTERN_UNICODE_EMOJI");
        return (Rule<T, Node<T>, S>) new Rule<T, Node<T>, S>(pattern_unicode_emoji) { // from class: com.discord.utilities.textprocessing.Rules$createUnicodeEmojiRule$1
            @Override // com.discord.simpleast.core.parser.Rule
            public ParseSpec<T, S> parse(Matcher matcher, Parser<T, ? super Node<T>, S> parser, S s2) {
                m.checkNotNullParameter(matcher, "matcher");
                m.checkNotNullParameter(parser, "parser");
                String group = matcher.group();
                ModelEmojiUnicode modelEmojiUnicode = Rules.access$getEmojiDataProvider$p(Rules.INSTANCE).getUnicodeEmojiSurrogateMap().get(group);
                if (modelEmojiUnicode != null) {
                    EmojiNode from$default = EmojiNode.Companion.from$default(EmojiNode.Companion, modelEmojiUnicode, 0, 2, (Object) null);
                    m.checkNotNullParameter(from$default, "node");
                    return new ParseSpec<>(from$default, s2);
                }
                m.checkNotNullExpressionValue(group, "match");
                b.a.t.b.a.a aVar = new b.a.t.b.a.a(group);
                m.checkNotNullParameter(aVar, "node");
                return new ParseSpec<>(aVar, s2);
            }
        };
    }

    public final <T extends UrlNode.RenderContext, S> Rule<T, UrlNode<T>, S> createUrlNoEmbedRule() {
        final Pattern pattern = PATTERN_URL_NO_EMBED;
        m.checkNotNullExpressionValue(pattern, "PATTERN_URL_NO_EMBED");
        return (Rule<T, UrlNode<T>, S>) new Rule<T, UrlNode<T>, S>(pattern) { // from class: com.discord.utilities.textprocessing.Rules$createUrlNoEmbedRule$1
            @Override // com.discord.simpleast.core.parser.Rule
            public ParseSpec<T, S> parse(Matcher matcher, Parser<T, ? super UrlNode<T>, S> parser, S s2) {
                m.checkNotNullParameter(matcher, "matcher");
                m.checkNotNullParameter(parser, "parser");
                String group = matcher.group(1);
                m.checkNotNull(group);
                UrlNode urlNode = new UrlNode(group, null, 2, null);
                m.checkNotNullParameter(urlNode, "node");
                return new ParseSpec<>(urlNode, s2);
            }
        };
    }

    public final <T extends UrlNode.RenderContext, S> Rule<T, UrlNode<T>, S> createUrlRule() {
        final Pattern pattern = PATTERN_URL;
        m.checkNotNullExpressionValue(pattern, "PATTERN_URL");
        return (Rule<T, UrlNode<T>, S>) new Rule<T, UrlNode<T>, S>(pattern) { // from class: com.discord.utilities.textprocessing.Rules$createUrlRule$1
            @Override // com.discord.simpleast.core.parser.Rule
            public ParseSpec<T, S> parse(Matcher matcher, Parser<T, ? super UrlNode<T>, S> parser, S s2) {
                m.checkNotNullParameter(matcher, "matcher");
                m.checkNotNullParameter(parser, "parser");
                String group = matcher.group(1);
                m.checkNotNull(group);
                UrlNode urlNode = new UrlNode(group, null, 2, null);
                m.checkNotNullParameter(urlNode, "node");
                return new ParseSpec<>(urlNode, s2);
            }
        };
    }

    public final <T extends UserMentionNode.RenderContext, S> Rule<T, Node<T>, S> createUserMentionRule() {
        final Pattern pattern = PATTERN_MENTION;
        m.checkNotNullExpressionValue(pattern, "PATTERN_MENTION");
        return (Rule<T, Node<T>, S>) new Rule<T, Node<T>, S>(pattern) { // from class: com.discord.utilities.textprocessing.Rules$createUserMentionRule$1
            @Override // com.discord.simpleast.core.parser.Rule
            public ParseSpec<T, S> parse(Matcher matcher, Parser<T, ? super Node<T>, S> parser, S s2) {
                UserMentionNode.Type type;
                m.checkNotNullParameter(matcher, "matcher");
                m.checkNotNullParameter(parser, "parser");
                if (!TextUtils.isEmpty(matcher.group(1))) {
                    UserMentionNode userMentionNode = new UserMentionNode(UserMentionNode.Type.USER, Rules.toLongOrDefault$default(Rules.INSTANCE, matcher.group(1), 0L, 1, null));
                    m.checkNotNullParameter(userMentionNode, "node");
                    return new ParseSpec<>(userMentionNode, s2);
                }
                String group = matcher.group(2);
                m.checkNotNull(group);
                if (group.charAt(0) != 'e') {
                    type = UserMentionNode.Type.HERE;
                } else {
                    type = UserMentionNode.Type.EVERYONE;
                }
                UserMentionNode userMentionNode2 = new UserMentionNode(type, 0L, 2, null);
                m.checkNotNullParameter(userMentionNode2, "node");
                return new ParseSpec<>(userMentionNode2, s2);
            }
        };
    }
}
