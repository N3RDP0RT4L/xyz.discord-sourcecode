package com.discord.utilities.textprocessing;

import andhook.lib.HookHelper;
import android.graphics.Paint;
import android.text.style.ReplacementSpan;
import androidx.core.app.NotificationCompat;
import com.discord.utilities.string.StringUtilsKt;
import d0.g0.a;
import d0.g0.y;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: SimpleRoundedBackgroundSpan.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0014\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\r\u0018\u00002\u00020\u0001Bi\u0012\u0006\u0010%\u001a\u00020\u0002\u0012\u0006\u0010&\u001a\u00020\u0002\u0012\u0006\u0010(\u001a\u00020\u0002\u0012\u0006\u0010\u001d\u001a\u00020\u0002\u0012\u0006\u0010'\u001a\u00020\u0002\u0012\u0006\u0010)\u001a\u00020\u0015\u0012\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u0002\u0012\b\b\u0002\u0010+\u001a\u00020\u0006\u0012\u001a\b\u0002\u0010#\u001a\u0014\u0012\u0006\u0012\u0004\u0018\u00010\"\u0012\u0006\u0012\u0004\u0018\u00010\"\u0018\u00010!¢\u0006\u0004\b-\u0010.J!\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ)\u0010\u000b\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004H\u0002¢\u0006\u0004\b\u000b\u0010\fJ;\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u000e\u001a\u00020\r2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\t\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fH\u0016¢\u0006\u0004\b\u0011\u0010\u0012JY\u0010\u001b\u001a\u00020\u001a2\u0006\u0010\u0014\u001a\u00020\u00132\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\t\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u00022\u0006\u0010\u0019\u001a\u00020\u00022\u0006\u0010\u000e\u001a\u00020\rH\u0016¢\u0006\u0004\b\u001b\u0010\u001cR\u0016\u0010\u001d\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u0018\u0010\u001f\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 R(\u0010#\u001a\u0014\u0012\u0006\u0012\u0004\u0018\u00010\"\u0012\u0006\u0012\u0004\u0018\u00010\"\u0018\u00010!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$R\u0016\u0010%\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b%\u0010\u001eR\u0016\u0010&\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b&\u0010\u001eR\u0016\u0010'\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010\u001eR\u0016\u0010(\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u0010\u001eR\u0016\u0010)\u001a\u00020\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b)\u0010*R\u0016\u0010+\u001a\u00020\u00068\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b+\u0010,¨\u0006/"}, d2 = {"Lcom/discord/utilities/textprocessing/SimpleRoundedBackgroundSpan;", "Landroid/text/style/ReplacementSpan;", "", "end", "", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "", "isAtEndEdge", "(ILjava/lang/CharSequence;)Z", "start", "", "calculateCornerRadius", "(IILjava/lang/CharSequence;)[F", "Landroid/graphics/Paint;", "paint", "Landroid/graphics/Paint$FontMetricsInt;", "fm", "getSize", "(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I", "Landroid/graphics/Canvas;", "canvas", "", "x", "top", "y", "bottom", "", "draw", "(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V", "edgeHorizontalMargin", "I", "textColor", "Ljava/lang/Integer;", "Lkotlin/Function1;", "", "transformSpannedText", "Lkotlin/jvm/functions/Function1;", "startIndex", "endIndex", "backgroundColor", "edgeHorizontalPadding", "cornerRadius", "F", "isTrimEnabled", "Z", HookHelper.constructorName, "(IIIIIFLjava/lang/Integer;ZLkotlin/jvm/functions/Function1;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SimpleRoundedBackgroundSpan extends ReplacementSpan {
    private final int backgroundColor;
    private final float cornerRadius;
    private final int edgeHorizontalMargin;
    private final int edgeHorizontalPadding;
    private final int endIndex;
    private final boolean isTrimEnabled;
    private final int startIndex;
    private final Integer textColor;
    private final Function1<String, String> transformSpannedText;

    public /* synthetic */ SimpleRoundedBackgroundSpan(int i, int i2, int i3, int i4, int i5, float f, Integer num, boolean z2, Function1 function1, int i6, DefaultConstructorMarker defaultConstructorMarker) {
        this(i, i2, i3, i4, i5, f, (i6 & 64) != 0 ? null : num, (i6 & 128) != 0 ? true : z2, (i6 & 256) != 0 ? null : function1);
    }

    private final float[] calculateCornerRadius(int i, int i2, CharSequence charSequence) {
        float f = 0.0f;
        float f2 = i == this.startIndex ? this.cornerRadius : 0.0f;
        if (isAtEndEdge(i2, charSequence)) {
            f = this.cornerRadius;
        }
        return new float[]{f2, f2, f, f, f, f, f2, f2};
    }

    private final boolean isAtEndEdge(int i, CharSequence charSequence) {
        Character orNull;
        int i2 = this.endIndex;
        if (i != i2) {
            return this.isTrimEnabled && i2 - i == 1 && charSequence != null && (orNull = y.getOrNull(charSequence, i)) != null && a.isWhitespace(orNull.charValue());
        }
        return true;
    }

    /* JADX WARN: Removed duplicated region for block: B:21:0x0070  */
    @Override // android.text.style.ReplacementSpan
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void draw(android.graphics.Canvas r6, java.lang.CharSequence r7, int r8, int r9, float r10, int r11, int r12, int r13, android.graphics.Paint r14) {
        /*
            r5 = this;
            java.lang.String r0 = "canvas"
            d0.z.d.m.checkNotNullParameter(r6, r0)
            java.lang.String r0 = "paint"
            d0.z.d.m.checkNotNullParameter(r14, r0)
            r0 = 0
            if (r7 == 0) goto L16
            java.lang.CharSequence r1 = r7.subSequence(r8, r9)
            java.lang.String r1 = r1.toString()
            goto L17
        L16:
            r1 = r0
        L17:
            kotlin.jvm.functions.Function1<java.lang.String, java.lang.String> r2 = r5.transformSpannedText
            java.lang.String r1 = com.discord.utilities.string.StringUtilsKt.transformOrEmpty(r1, r2)
            boolean r2 = d0.g0.t.isBlank(r1)
            r2 = r2 ^ 1
            if (r2 == 0) goto L26
            r0 = r1
        L26:
            if (r0 == 0) goto L7b
            int r1 = r5.edgeHorizontalPadding
            float r2 = r14.measureText(r0)
            int r3 = r5.startIndex
            if (r8 != r3) goto L3c
            int r3 = r5.edgeHorizontalMargin
            float r3 = (float) r3
            float r10 = r10 + r3
            int r3 = r5.edgeHorizontalPadding
        L38:
            float r4 = (float) r3
            float r4 = r4 + r10
            int r1 = r1 + r3
            goto L47
        L3c:
            r3 = 0
            int r3 = (r10 > r3 ? 1 : (r10 == r3 ? 0 : -1))
            if (r3 != 0) goto L46
            if (r11 == 0) goto L46
            int r3 = r5.edgeHorizontalPadding
            goto L38
        L46:
            r4 = r10
        L47:
            android.graphics.RectF r3 = new android.graphics.RectF
            float r11 = (float) r11
            float r2 = r2 + r10
            float r1 = (float) r1
            float r2 = r2 + r1
            float r13 = (float) r13
            r3.<init>(r10, r11, r2, r13)
            android.graphics.Path r10 = new android.graphics.Path
            r10.<init>()
            float[] r7 = r5.calculateCornerRadius(r8, r9, r7)
            android.graphics.Path$Direction r8 = android.graphics.Path.Direction.CW
            r10.addRoundRect(r3, r7, r8)
            android.graphics.Paint r7 = new android.graphics.Paint
            r7.<init>(r14)
            int r8 = r5.backgroundColor
            r7.setColor(r8)
            r6.drawPath(r10, r7)
            java.lang.Integer r7 = r5.textColor
            if (r7 == 0) goto L77
            int r7 = r7.intValue()
            r14.setColor(r7)
        L77:
            float r7 = (float) r12
            r6.drawText(r0, r4, r7, r14)
        L7b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.textprocessing.SimpleRoundedBackgroundSpan.draw(android.graphics.Canvas, java.lang.CharSequence, int, int, float, int, int, int, android.graphics.Paint):void");
    }

    @Override // android.text.style.ReplacementSpan
    public int getSize(Paint paint, CharSequence charSequence, int i, int i2, Paint.FontMetricsInt fontMetricsInt) {
        m.checkNotNullParameter(paint, "paint");
        int roundToInt = d0.a0.a.roundToInt(paint.measureText(StringUtilsKt.transformOrEmpty(charSequence != null ? charSequence.subSequence(i, i2).toString() : null, this.transformSpannedText)));
        if (i == this.startIndex) {
            roundToInt += this.edgeHorizontalMargin + this.edgeHorizontalPadding;
        }
        if (isAtEndEdge(i2, charSequence)) {
            roundToInt += this.edgeHorizontalMargin;
        }
        return roundToInt + this.edgeHorizontalPadding;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public SimpleRoundedBackgroundSpan(int i, int i2, int i3, int i4, int i5, float f, Integer num, boolean z2, Function1<? super String, String> function1) {
        this.startIndex = i;
        this.endIndex = i2;
        this.edgeHorizontalPadding = i3;
        this.edgeHorizontalMargin = i4;
        this.backgroundColor = i5;
        this.cornerRadius = f;
        this.textColor = num;
        this.isTrimEnabled = z2;
        this.transformSpannedText = function1;
    }
}
