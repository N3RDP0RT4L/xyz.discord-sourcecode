package com.discord.utilities.textprocessing;

import b.a.t.b.c.a;
import com.discord.simpleast.core.node.Node;
import com.discord.utilities.textprocessing.node.EmojiNode;
import kotlin.Metadata;
/* compiled from: MessagePreprocessor.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\t\u001a\u00020\u0006\"\b\b\u0000\u0010\u0001*\u00020\u00002\u001a\u0010\u0005\u001a\u0016\u0012\u0006\u0012\u0004\u0018\u00010\u0003 \u0004*\b\u0012\u0002\b\u0003\u0018\u00010\u00020\u0002H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"Lcom/discord/utilities/textprocessing/node/BasicRenderContext;", "R", "Lcom/discord/simpleast/core/node/Node;", "", "kotlin.jvm.PlatformType", "it", "", "processNode", "(Lcom/discord/simpleast/core/node/Node;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MessagePreprocessor$process$1 implements a {
    public static final MessagePreprocessor$process$1 INSTANCE = new MessagePreprocessor$process$1();

    @Override // b.a.t.b.c.a
    public final void processNode(Node<Object> node) {
        if (node instanceof EmojiNode) {
            ((EmojiNode) node).setJumbo(true);
        }
    }
}
