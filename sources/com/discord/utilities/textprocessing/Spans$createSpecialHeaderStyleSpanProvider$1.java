package com.discord.utilities.textprocessing;

import android.content.Context;
import android.text.style.CharacterStyle;
import android.text.style.StyleSpan;
import android.text.style.TextAppearanceSpan;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: Spans.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "header", "Landroid/text/style/CharacterStyle;", "invoke", "(I)Landroid/text/style/CharacterStyle;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class Spans$createSpecialHeaderStyleSpanProvider$1 extends o implements Function1<Integer, CharacterStyle> {
    public final /* synthetic */ Context $context;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public Spans$createSpecialHeaderStyleSpanProvider$1(Context context) {
        super(1);
        this.$context = context;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ CharacterStyle invoke(Integer num) {
        return invoke(num.intValue());
    }

    public final CharacterStyle invoke(int i) {
        if (i == 1) {
            return new TextAppearanceSpan(this.$context, R.style.Markdown_Header1_ChangelogSpecial);
        }
        if (i != 4) {
            return new StyleSpan(3);
        }
        return new TextAppearanceSpan(this.$context, R.style.Markdown_Header4);
    }
}
