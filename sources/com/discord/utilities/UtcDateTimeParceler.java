package com.discord.utilities;

import andhook.lib.HookHelper;
import android.os.Parcel;
import com.discord.api.utcdatetime.UtcDateTime;
import d0.j;
import d0.z.d.m;
import kotlin.Metadata;
import s.b.a;
/* compiled from: Parcelers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rJ\u0017\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J#\u0010\n\u001a\u00020\t*\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/utilities/UtcDateTimeParceler;", "Ls/b/a;", "Lcom/discord/api/utcdatetime/UtcDateTime;", "Landroid/os/Parcel;", "parcel", "create", "(Landroid/os/Parcel;)Lcom/discord/api/utcdatetime/UtcDateTime;", "", "flags", "", "write", "(Lcom/discord/api/utcdatetime/UtcDateTime;Landroid/os/Parcel;I)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class UtcDateTimeParceler implements a<UtcDateTime> {
    public static final UtcDateTimeParceler INSTANCE = new UtcDateTimeParceler();

    private UtcDateTimeParceler() {
    }

    public UtcDateTime[] newArray(int i) {
        m.checkNotNullParameter(this, "this");
        throw new j("Generated by Android Extensions automatically");
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // s.b.a
    public UtcDateTime create(Parcel parcel) {
        m.checkNotNullParameter(parcel, "parcel");
        return new UtcDateTime(parcel.readLong());
    }

    public void write(UtcDateTime utcDateTime, Parcel parcel, int i) {
        m.checkNotNullParameter(utcDateTime, "$this$write");
        m.checkNotNullParameter(parcel, "parcel");
        parcel.writeLong(utcDateTime.g());
    }
}
