package com.discord.utilities.persister;

import andhook.lib.HookHelper;
import android.content.Context;
import b.a.d.g;
import b.a.d.k;
import b.a.d.l;
import b.a.e.d;
import com.discord.app.AppLog;
import com.discord.utilities.persister.Persister;
import com.discord.utilities.time.Clock;
import d0.z.d.m;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: PersisterConfig.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\b\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\bR\u001c\u0010\r\u001a\b\u0012\u0004\u0012\u00020\n0\t8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR:\u0010\u000f\u001a&\u0012\f\u0012\n \u000e*\u0004\u0018\u00010\n0\n \u000e*\u0012\u0012\f\u0012\n \u000e*\u0004\u0018\u00010\n0\n\u0018\u00010\t0\t8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\f¨\u0006\u0012"}, d2 = {"Lcom/discord/utilities/persister/PersisterConfig;", "", "Landroid/content/Context;", "context", "Lcom/discord/utilities/time/Clock;", "clock", "", "init", "(Landroid/content/Context;Lcom/discord/utilities/time/Clock;)V", "Lrx/Observable;", "", "getPersistenceStrategy", "()Lrx/Observable;", "persistenceStrategy", "kotlin.jvm.PlatformType", "isNotActive", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class PersisterConfig {
    public static final PersisterConfig INSTANCE = new PersisterConfig();

    private PersisterConfig() {
    }

    private final Observable<Boolean> getPersistenceStrategy() {
        Observable<Boolean> H = Observable.H(isNotActive(), Observable.d0(1L, TimeUnit.MINUTES).F(PersisterConfig$persistenceStrategy$1.INSTANCE));
        m.checkNotNullExpressionValue(H, "Observable\n        .merg…  .map { true }\n        )");
        return H;
    }

    private final Observable<Boolean> isNotActive() {
        Observable<Boolean> S = d.d.a().S(1);
        l lVar = l.c;
        Observable q = l.f59b.F(k.j).q();
        m.checkNotNullExpressionValue(q, "numGatewayConnectionCons…  .distinctUntilChanged()");
        return Observable.j(S, q, PersisterConfig$isNotActive$1.INSTANCE).q();
    }

    public final void init(Context context, Clock clock) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(clock, "clock");
        Persister.Companion companion = Persister.Companion;
        companion.setKryoConfig(PersisterConfig$init$1.INSTANCE);
        Objects.requireNonNull(AppLog.g);
        m.checkNotNullParameter("[Persister]", "tag");
        companion.setLogger(new g("[Persister]"));
        companion.init(context, clock, getPersistenceStrategy());
    }
}
