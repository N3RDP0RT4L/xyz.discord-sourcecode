package com.discord.utilities.persister;

import androidx.exifinterface.media.ExifInterface;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import j0.l.e.k;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import rx.subjects.BehaviorSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;
/* compiled from: Persister.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00000\u0002\"\b\b\u0000\u0010\u0001*\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", ExifInterface.GPS_DIRECTION_TRUE, "Lrx/subjects/Subject;", "invoke", "()Lrx/subjects/Subject;", "createSubject"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class Persister$getObservable$1 extends o implements Function0<Subject<T, T>> {
    public final /* synthetic */ Persister this$0;

    /* compiled from: Persister.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0001\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u0004\"\b\b\u0000\u0010\u0001*\u00020\u00002\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", ExifInterface.GPS_DIRECTION_TRUE, "", "it", "", "invoke", "(Ljava/lang/Void;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.utilities.persister.Persister$getObservable$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1 {
        public final /* synthetic */ SerializedSubject $subject;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(SerializedSubject serializedSubject) {
            super(1);
            this.$subject = serializedSubject;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            invoke((Void) obj);
            return Unit.a;
        }

        public final void invoke(Void r2) {
            SerializedSubject serializedSubject = this.$subject;
            serializedSubject.k.onNext(Persister$getObservable$1.this.this$0.get());
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public Persister$getObservable$1(Persister persister) {
        super(0);
        this.this$0 = persister;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Subject<T, T> invoke() {
        SerializedSubject serializedSubject = new SerializedSubject(BehaviorSubject.k0());
        k kVar = new k(null);
        m.checkNotNullExpressionValue(kVar, "Observable\n          .just(null)");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.computationBuffered(kVar), this.this$0.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1(serializedSubject));
        return serializedSubject;
    }
}
