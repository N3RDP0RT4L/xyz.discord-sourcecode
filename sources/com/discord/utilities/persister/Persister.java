package com.discord.utilities.persister;

import andhook.lib.HookHelper;
import andhook.lib.xposed.ClassUtils;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import androidx.appcompat.widget.ActivityChooserModel;
import androidx.core.app.NotificationCompat;
import androidx.exifinterface.media.ExifInterface;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.time.Clock;
import com.discord.utilities.time.TimeElapsed;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import d0.t.n;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import j0.l.e.k;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;
/* compiled from: Persister.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u0000 4*\b\b\u0000\u0010\u0002*\u00020\u00012\u00020\u0001:\u000245B\u0017\u0012\u0006\u0010\u001d\u001a\u00020\u001c\u0012\u0006\u0010(\u001a\u00028\u0000¢\u0006\u0004\b2\u00103J\u000f\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\r\u0010\u0006\u001a\u00028\u0000¢\u0006\u0004\b\u0006\u0010\u0007J\u0013\u0010\t\u001a\b\u0012\u0004\u0012\u00028\u00000\b¢\u0006\u0004\b\t\u0010\nJ!\u0010\r\u001a\u00028\u00002\u0006\u0010\u000b\u001a\u00028\u00002\b\b\u0002\u0010\u0004\u001a\u00020\fH\u0007¢\u0006\u0004\b\r\u0010\u000eJ-\u0010\u0011\u001a\u00028\u00002\b\b\u0002\u0010\u0004\u001a\u00020\f2\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00000\u000fH\u0007¢\u0006\u0004\b\u0011\u0010\u0012J\u0019\u0010\u0013\u001a\u00028\u00002\b\b\u0002\u0010\u0004\u001a\u00020\fH\u0007¢\u0006\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0015\u001a\u00020\f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0017\u001a\u00020\f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0017\u0010\u0016R\u0016\u0010\u001b\u001a\u00020\u00188B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u0019\u0010\u001aR\u0019\u0010\u001d\u001a\u00020\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 R$\u0010\"\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0000\u0018\u00010!8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\"\u0010#R\u0016\u0010'\u001a\u00020$8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b%\u0010&R\u0016\u0010(\u001a\u00028\u00008\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u0010)R\u0016\u0010*\u001a\u00028\u00008\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b*\u0010)R\u0018\u0010.\u001a\u0004\u0018\u00010+8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b,\u0010-R\u001c\u00101\u001a\u00028\u00008B@\u0002X\u0082\u0004¢\u0006\f\u0012\u0004\b0\u0010\u0005\u001a\u0004\b/\u0010\u0007¨\u00066"}, d2 = {"Lcom/discord/utilities/persister/Persister;", "", ExifInterface.GPS_DIRECTION_TRUE, "", "persist", "()V", "get", "()Ljava/lang/Object;", "Lrx/Observable;", "getObservable", "()Lrx/Observable;", "newValue", "", "set", "(Ljava/lang/Object;Z)Ljava/lang/Object;", "Lkotlin/Function1;", "setter", "getAndSet", "(ZLkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "clear", "(Z)Ljava/lang/Object;", "valueUnset", "Z", "valueDirty", "Ljava/io/FileOutputStream;", "getFileOutput", "()Ljava/io/FileOutputStream;", "fileOutput", "", "key", "Ljava/lang/String;", "getKey", "()Ljava/lang/String;", "Lrx/subjects/Subject;", "valueSubject", "Lrx/subjects/Subject;", "Ljava/io/File;", "getFileInput", "()Ljava/io/File;", "fileInput", "defaultValue", "Ljava/lang/Object;", "value", "Lcom/esotericsoftware/kryo/io/Input;", "getFileInputStream", "()Lcom/esotericsoftware/kryo/io/Input;", "fileInputStream", "getFileValue", "getFileValue$annotations", "fileValue", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/Object;)V", "Companion", "Preloader", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class Persister<T> {
    private static Clock clock;
    @SuppressLint({"StaticFieldLeak"})
    private static Context context;
    private static boolean initialized;
    private static Preloader preferencesPreloader;
    private final T defaultValue;
    private final String key;
    private T value;
    private boolean valueDirty;
    private Subject<T, T> valueSubject;
    private boolean valueUnset = true;
    public static final Companion Companion = new Companion(null);
    private static Function3<? super Integer, ? super String, ? super Exception, Unit> logger = Persister$Companion$logger$1.INSTANCE;
    private static Function1<? super Kryo, Unit> kryoConfig = Persister$Companion$kryoConfig$1.INSTANCE;
    private static final Persister$Companion$kryos$1 kryos = new ThreadLocal<Kryo>() { // from class: com.discord.utilities.persister.Persister$Companion$kryos$1
        /* JADX WARN: Can't rename method to resolve collision */
        @Override // java.lang.ThreadLocal
        public Kryo initialValue() {
            Kryo kryo = new Kryo();
            Persister.Companion.getKryoConfig().invoke(kryo);
            return kryo;
        }
    };
    private static final List<WeakReference<Persister<?>>> preferences = new CopyOnWriteArrayList();

    /* compiled from: Persister.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000s\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\b\u0003\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004*\u0001+\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b6\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0006\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J+\u0010\u000f\u001a\u00020\u00022\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\n2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\f¢\u0006\u0004\b\u000f\u0010\u0010J\u0015\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\r0\fH\u0007¢\u0006\u0004\b\u0011\u0010\u0012J\u000f\u0010\u0013\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0013\u0010\u0004R.\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u00020\u00148\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bRB\u0010!\u001a\"\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\u001e\u0012\f\u0012\n\u0018\u00010\u001fj\u0004\u0018\u0001` \u0012\u0004\u0012\u00020\u00020\u001c8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010$\"\u0004\b%\u0010&R\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u000b\u0010'R\u0016\u0010\t\u001a\u00020\b8\u0002@\u0002X\u0083.¢\u0006\u0006\n\u0004\b\t\u0010(R\u0016\u0010)\u001a\u00020\r8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b)\u0010*R\u0016\u0010,\u001a\u00020+8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b,\u0010-R&\u00101\u001a\u0012\u0012\u000e\u0012\f\u0012\b\u0012\u0006\u0012\u0002\b\u0003000/0.8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b1\u00102R\u0016\u00104\u001a\u0002038\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b4\u00105¨\u00067"}, d2 = {"Lcom/discord/utilities/persister/Persister$Companion;", "", "", "persistAll", "()V", "", "availableBytes", "()J", "Landroid/content/Context;", "context", "Lcom/discord/utilities/time/Clock;", "clock", "Lrx/Observable;", "", "persistenceStrategy", "init", "(Landroid/content/Context;Lcom/discord/utilities/time/Clock;Lrx/Observable;)V", "isPreloaded", "()Lrx/Observable;", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_RESET, "Lkotlin/Function1;", "Lcom/esotericsoftware/kryo/Kryo;", "kryoConfig", "Lkotlin/jvm/functions/Function1;", "getKryoConfig", "()Lkotlin/jvm/functions/Function1;", "setKryoConfig", "(Lkotlin/jvm/functions/Function1;)V", "Lkotlin/Function3;", "", "", "Ljava/lang/Exception;", "Lkotlin/Exception;", "logger", "Lkotlin/jvm/functions/Function3;", "getLogger", "()Lkotlin/jvm/functions/Function3;", "setLogger", "(Lkotlin/jvm/functions/Function3;)V", "Lcom/discord/utilities/time/Clock;", "Landroid/content/Context;", "initialized", "Z", "com/discord/utilities/persister/Persister$Companion$kryos$1", "kryos", "Lcom/discord/utilities/persister/Persister$Companion$kryos$1;", "", "Ljava/lang/ref/WeakReference;", "Lcom/discord/utilities/persister/Persister;", "preferences", "Ljava/util/List;", "Lcom/discord/utilities/persister/Persister$Preloader;", "preferencesPreloader", "Lcom/discord/utilities/persister/Persister$Preloader;", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final long availableBytes() {
            Context context = Persister.context;
            if (context == null) {
                m.throwUninitializedPropertyAccessException("context");
            }
            Object systemService = context.getSystemService(ActivityChooserModel.ATTRIBUTE_ACTIVITY);
            if (!(systemService instanceof ActivityManager)) {
                systemService = null;
            }
            ActivityManager activityManager = (ActivityManager) systemService;
            if (activityManager == null) {
                return RecyclerView.FOREVER_NS;
            }
            ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
            activityManager.getMemoryInfo(memoryInfo);
            return memoryInfo.availMem;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final void persistAll() {
            for (WeakReference weakReference : Persister.preferences) {
                k kVar = new k(weakReference.get());
                m.checkNotNullExpressionValue(kVar, "Observable\n          .just(weakPreference.get())");
                ObservableExtensionsKt.appSubscribe$default(ObservableExtensionsKt.computationBuffered(kVar), Persister.class, (Context) null, (Function1) null, (Function1) null, (Function0) null, (Function0) null, Persister$Companion$persistAll$1$1.INSTANCE, 62, (Object) null);
            }
        }

        public final Function1<Kryo, Unit> getKryoConfig() {
            return Persister.kryoConfig;
        }

        public final Function3<Integer, String, Exception, Unit> getLogger() {
            return Persister.logger;
        }

        public final void init(Context context, Clock clock, Observable<Boolean> observable) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(clock, "clock");
            m.checkNotNullParameter(observable, "persistenceStrategy");
            if (!Persister.initialized) {
                Persister.initialized = true;
                Persister.context = context;
                Persister.clock = clock;
                Persister.preferencesPreloader = new Preloader(clock);
                Observable<Boolean> x2 = observable.x(Persister$Companion$init$1.INSTANCE);
                m.checkNotNullExpressionValue(x2, "persistenceStrategy\n    …er { persist -> persist }");
                ObservableExtensionsKt.appSubscribe$default(ObservableExtensionsKt.computationBuffered(x2), Persister.class, (Context) null, (Function1) null, (Function1) null, (Function0) null, (Function0) null, Persister$Companion$init$2.INSTANCE, 62, (Object) null);
            }
        }

        public final Observable<Boolean> isPreloaded() {
            Preloader preloader = Persister.preferencesPreloader;
            if (preloader == null) {
                m.throwUninitializedPropertyAccessException("preferencesPreloader");
            }
            return preloader.isPreloaded();
        }

        public final void reset() {
            for (WeakReference weakReference : Persister.preferences) {
                Persister persister = (Persister) weakReference.get();
                if (persister != null) {
                    Persister.clear$default(persister, false, 1, null);
                }
            }
        }

        public final void setKryoConfig(Function1<? super Kryo, Unit> function1) {
            m.checkNotNullParameter(function1, "<set-?>");
            Persister.kryoConfig = function1;
        }

        public final void setLogger(Function3<? super Integer, ? super String, ? super Exception, Unit> function3) {
            m.checkNotNullParameter(function3, "<set-?>");
            Persister.logger = function3;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: Persister.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010!\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0002\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0019\u001a\u00020\u0018¢\u0006\u0004\b\u001a\u0010\u001bJ#\u0010\u0005\u001a\u00020\u0004\"\b\b\u0001\u0010\u0002*\u00020\u0001*\b\u0012\u0004\u0012\u00028\u00010\u0003H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0013\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\u0004\b\t\u0010\nJ%\u0010\f\u001a\u00020\u0004\"\b\b\u0001\u0010\u0002*\u00020\u00012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00028\u00010\u0003¢\u0006\u0004\b\f\u0010\u0006R\u001c\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010RB\u0010\u0013\u001a.\u0012\u0014\u0012\u0012\u0012\u0002\b\u0003 \u0012*\b\u0012\u0002\b\u0003\u0018\u00010\u00030\u0003\u0012\u0014\u0012\u0012\u0012\u0002\b\u0003 \u0012*\b\u0012\u0002\b\u0003\u0018\u00010\u00030\u00030\u00118\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0016\u001a\u00020\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017¨\u0006\u001c"}, d2 = {"Lcom/discord/utilities/persister/Persister$Preloader;", "", ExifInterface.GPS_DIRECTION_TRUE, "Lcom/discord/utilities/persister/Persister;", "", "handlePreload", "(Lcom/discord/utilities/persister/Persister;)V", "Lrx/Observable;", "", "isPreloaded", "()Lrx/Observable;", "preference", "preload", "", "", "preloadCacheKeys", "Ljava/util/List;", "Lrx/subjects/SerializedSubject;", "kotlin.jvm.PlatformType", "preloadSubject", "Lrx/subjects/SerializedSubject;", "Lcom/discord/utilities/time/TimeElapsed;", "preloadTime", "Lcom/discord/utilities/time/TimeElapsed;", "Lcom/discord/utilities/time/Clock;", "clock", HookHelper.constructorName, "(Lcom/discord/utilities/time/Clock;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Preloader {
        private final List<String> preloadCacheKeys = n.mutableListOf("STORE_USER_RELATIONSHIPS_V9", "STORE_CHANNELS_V26", "STORE_GUILDS_V34");
        private final SerializedSubject<Persister<?>, Persister<?>> preloadSubject;
        private final TimeElapsed preloadTime;

        /* compiled from: Persister.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\b\u001a\n \u0003*\u0004\u0018\u00010\u00050\u0005\"\b\b\u0000\u0010\u0001*\u00020\u00002\u0016\u0010\u0004\u001a\u0012\u0012\u0002\b\u0003 \u0003*\b\u0012\u0002\b\u0003\u0018\u00010\u00020\u0002H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", ExifInterface.GPS_DIRECTION_TRUE, "Lcom/discord/utilities/persister/Persister;", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/utilities/persister/Persister;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.utilities.persister.Persister$Preloader$1  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass1<T, R> implements b<Persister<?>, Boolean> {
            public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

            public final Boolean call(Persister<?> persister) {
                return Boolean.valueOf(persister == null);
            }
        }

        /* compiled from: Persister.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\b\u001a\u00020\u0005\"\b\b\u0000\u0010\u0001*\u00020\u00002\u0016\u0010\u0004\u001a\u0012\u0012\u0002\b\u0003 \u0003*\b\u0012\u0002\b\u0003\u0018\u00010\u00020\u0002H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", ExifInterface.GPS_DIRECTION_TRUE, "Lcom/discord/utilities/persister/Persister;", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Lcom/discord/utilities/persister/Persister;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.utilities.persister.Persister$Preloader$2  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass2 extends o implements Function1<Persister<?>, Unit> {
            public AnonymousClass2() {
                super(1);
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Persister<?> persister) {
                invoke2(persister);
                return Unit.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Persister<?> persister) {
                if (persister != null) {
                    Preloader.this.handlePreload(persister);
                }
            }
        }

        public Preloader(Clock clock) {
            m.checkNotNullParameter(clock, "clock");
            this.preloadTime = new TimeElapsed(clock, 0L, 2, null);
            SerializedSubject<Persister<?>, Persister<?>> serializedSubject = new SerializedSubject<>(BehaviorSubject.k0());
            this.preloadSubject = serializedSubject;
            Observable<Persister<?>> b02 = serializedSubject.b0(AnonymousClass1.INSTANCE);
            m.checkNotNullExpressionValue(b02, "preloadSubject\n          .takeUntil { it == null }");
            ObservableExtensionsKt.appSubscribe$default(ObservableExtensionsKt.computationBuffered(b02), Preloader.class, (Context) null, (Function1) null, (Function1) null, (Function0) null, (Function0) null, new AnonymousClass2(), 62, (Object) null);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final synchronized <T> void handlePreload(Persister<T> persister) {
            if (this.preloadCacheKeys.contains(persister.getKey())) {
                this.preloadCacheKeys.remove(persister.getKey());
                persister.get();
            }
            if (this.preloadCacheKeys.isEmpty()) {
                this.preloadSubject.k.onNext(null);
                Function3<Integer, String, Exception, Unit> logger = Persister.Companion.getLogger();
                logger.invoke(4, "Preloaded preferences in " + this.preloadTime.getSeconds() + " seconds.", null);
            }
        }

        public final Observable<Boolean> isPreloaded() {
            Observable<Boolean> q = this.preloadSubject.F(Persister$Preloader$isPreloaded$1.INSTANCE).q();
            m.checkNotNullExpressionValue(q, "preloadSubject\n         …  .distinctUntilChanged()");
            return q;
        }

        public final synchronized <T> void preload(Persister<T> persister) {
            m.checkNotNullParameter(persister, "preference");
            if (this.preloadCacheKeys.contains(persister.getKey())) {
                this.preloadSubject.k.onNext(persister);
            }
        }
    }

    public Persister(String str, T t) {
        m.checkNotNullParameter(str, "key");
        m.checkNotNullParameter(t, "defaultValue");
        this.key = str;
        this.defaultValue = t;
        this.value = t;
        preferences.add(new WeakReference<>(this));
        Preloader preloader = preferencesPreloader;
        if (preloader == null) {
            m.throwUninitializedPropertyAccessException("preferencesPreloader");
        }
        preloader.preload(this);
    }

    public static /* synthetic */ Object clear$default(Persister persister, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = true;
        }
        return persister.clear(z2);
    }

    public static /* synthetic */ Object getAndSet$default(Persister persister, boolean z2, Function1 function1, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = false;
        }
        return persister.getAndSet(z2, function1);
    }

    private final File getFileInput() {
        StringBuilder sb = new StringBuilder();
        Context context2 = context;
        if (context2 == null) {
            m.throwUninitializedPropertyAccessException("context");
        }
        sb.append(context2.getFilesDir());
        sb.append(MentionUtilsKt.SLASH_CHAR);
        sb.append(this.key);
        return new File(sb.toString());
    }

    private final Input getFileInputStream() {
        File fileInput = getFileInput();
        if (!fileInput.exists() || fileInput.length() >= Companion.availableBytes()) {
            return null;
        }
        return new Input(new FileInputStream(fileInput));
    }

    private final FileOutputStream getFileOutput() {
        Context context2 = context;
        if (context2 == null) {
            m.throwUninitializedPropertyAccessException("context");
        }
        FileOutputStream openFileOutput = context2.openFileOutput(this.key, 0);
        m.checkNotNullExpressionValue(openFileOutput, "context.openFileOutput(key, Context.MODE_PRIVATE)");
        return openFileOutput;
    }

    private final T getFileValue() {
        T t;
        try {
            Input fileInputStream = getFileInputStream();
            if (fileInputStream != null) {
                Kryo kryo = kryos.get();
                if (kryo != null) {
                    Object readClassAndObject = kryo.readClassAndObject(fileInputStream);
                    t = !(readClassAndObject instanceof Object) ? null : (T) readClassAndObject;
                    if (t == null) {
                        t = this.value;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Found " + readClassAndObject.getClass() + " for " + this.key + ", ");
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("but expecting ");
                        sb2.append(t.getClass());
                        sb2.append('\"');
                        sb.append(sb2.toString());
                        String sb3 = sb.toString();
                        m.checkNotNullExpressionValue(sb3, "StringBuilder()\n        …              .toString()");
                        Function3<? super Integer, ? super String, ? super Exception, Unit> function3 = logger;
                        function3.invoke(6, "Unable to cast deserialized preference " + this.key + ClassUtils.PACKAGE_SEPARATOR_CHAR, new Exception(sb3));
                    }
                } else {
                    t = this.value;
                }
                d0.y.b.closeFinally(fileInputStream, null);
                if (t != null) {
                    return t;
                }
            }
            return this.value;
        } catch (Exception e) {
            logger.invoke(5, a.G(a.R("Unable to deserialize preference "), this.key, ClassUtils.PACKAGE_SEPARATOR_CHAR), new Exception(this.key, e));
            return this.value;
        }
    }

    private static /* synthetic */ void getFileValue$annotations() {
    }

    public static final Observable<Boolean> isPreloaded() {
        return Companion.isPreloaded();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void persist() {
        if (this.valueDirty) {
            this.valueDirty = false;
            try {
                Output output = new Output(getFileOutput());
                th = null;
                try {
                    Kryo kryo = kryos.get();
                    if (kryo != null) {
                        kryo.writeClassAndObject(output, this.value);
                    }
                } finally {
                    try {
                        throw th;
                    } finally {
                    }
                }
            } catch (Exception e) {
                Function3<? super Integer, ? super String, ? super Exception, Unit> function3 = logger;
                function3.invoke(5, "Unable to serialize preference " + this.key + ClassUtils.PACKAGE_SEPARATOR_CHAR, new Exception(this.key, e));
            }
        }
    }

    public static final void reset() {
        Companion.reset();
    }

    public static /* synthetic */ Object set$default(Persister persister, Object obj, boolean z2, int i, Object obj2) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        return persister.set(obj, z2);
    }

    public final T clear() {
        return (T) clear$default(this, false, 1, null);
    }

    public final T clear(boolean z2) {
        return set(this.defaultValue, z2);
    }

    public final synchronized T get() {
        if (this.valueUnset) {
            this.valueUnset = false;
            this.value = getFileValue();
        }
        return this.value;
    }

    public final T getAndSet(Function1<? super T, ? extends T> function1) {
        return (T) getAndSet$default(this, false, function1, 1, null);
    }

    public final synchronized T getAndSet(boolean z2, Function1<? super T, ? extends T> function1) {
        m.checkNotNullParameter(function1, "setter");
        return set(function1.invoke((T) get()), z2);
    }

    public final String getKey() {
        return this.key;
    }

    public final synchronized Observable<T> getObservable() {
        Subject<T, T> subject;
        Persister$getObservable$1 persister$getObservable$1 = new Persister$getObservable$1(this);
        subject = this.valueSubject;
        if (subject == null) {
            subject = persister$getObservable$1.invoke();
            this.valueSubject = subject;
        }
        return subject;
    }

    public final T set(T t) {
        return (T) set$default(this, t, false, 2, null);
    }

    public final synchronized T set(T t, boolean z2) {
        T t2;
        m.checkNotNullParameter(t, "newValue");
        this.valueDirty = true;
        this.valueUnset = false;
        t2 = this.value;
        this.value = t;
        Subject<T, T> subject = this.valueSubject;
        if (subject != null) {
            subject.onNext(t);
        }
        if (z2) {
            persist();
        }
        return t2;
    }
}
