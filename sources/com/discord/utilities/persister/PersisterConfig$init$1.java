package com.discord.utilities.persister;

import com.discord.utilities.collections.LeastRecentlyAddedSet;
import com.discord.utilities.collections.SnowflakePartitionMap;
import com.discord.utilities.kryo.LeastRecentlyAddedSetSerializer;
import com.discord.utilities.kryo.SnowflakePartitionMapSerializer;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.serializers.FieldSerializerConfig;
import d0.z.d.m;
import d0.z.d.o;
import h0.b.b.c;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: PersisterConfig.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/esotericsoftware/kryo/Kryo;", "it", "", "invoke", "(Lcom/esotericsoftware/kryo/Kryo;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class PersisterConfig$init$1 extends o implements Function1<Kryo, Unit> {
    public static final PersisterConfig$init$1 INSTANCE = new PersisterConfig$init$1();

    public PersisterConfig$init$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Kryo kryo) {
        invoke2(kryo);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Kryo kryo) {
        m.checkNotNullParameter(kryo, "it");
        kryo.setInstantiatorStrategy(new Kryo.DefaultInstantiatorStrategy(new c()));
        FieldSerializerConfig fieldSerializerConfig = kryo.getFieldSerializerConfig();
        m.checkNotNullExpressionValue(fieldSerializerConfig, "it.fieldSerializerConfig");
        fieldSerializerConfig.setOptimizedGenerics(true);
        kryo.register(LeastRecentlyAddedSet.class, new LeastRecentlyAddedSetSerializer());
        kryo.register(SnowflakePartitionMap.class, new SnowflakePartitionMapSerializer());
    }
}
