package com.discord.utilities.persister;

import androidx.exifinterface.media.ExifInterface;
import b.d.b.a.a;
import com.discord.utilities.time.Clock;
import com.discord.utilities.time.TimeElapsed;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function3;
/* compiled from: Persister.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u0004\"\b\b\u0000\u0010\u0001*\u00020\u00002\f\u0010\u0003\u001a\b\u0012\u0002\b\u0003\u0018\u00010\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", ExifInterface.GPS_DIRECTION_TRUE, "Lcom/discord/utilities/persister/Persister;", "preference", "", "invoke", "(Lcom/discord/utilities/persister/Persister;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class Persister$Companion$persistAll$1$1 extends o implements Function1<Persister<?>, Unit> {
    public static final Persister$Companion$persistAll$1$1 INSTANCE = new Persister$Companion$persistAll$1$1();

    public Persister$Companion$persistAll$1$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Persister<?> persister) {
        invoke2(persister);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Persister<?> persister) {
        Clock clock;
        if (persister != null) {
            clock = Persister.clock;
            if (clock == null) {
                m.throwUninitializedPropertyAccessException("clock");
            }
            TimeElapsed timeElapsed = new TimeElapsed(clock, 0L, 2, null);
            persister.persist();
            if (timeElapsed.getMilliseconds() > 100) {
                Function3<Integer, String, Exception, Unit> logger = Persister.Companion.getLogger();
                StringBuilder R = a.R("Cached ");
                R.append(persister.getKey());
                R.append(" in ");
                R.append(timeElapsed.getSeconds());
                R.append(" seconds.");
                logger.invoke(4, R.toString(), null);
            }
        }
    }
}
