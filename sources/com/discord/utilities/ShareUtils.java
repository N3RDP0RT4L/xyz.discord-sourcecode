package com.discord.utilities;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import androidx.core.app.Person;
import androidx.core.content.LocusIdCompat;
import androidx.core.content.pm.ShortcutInfoCompat;
import androidx.core.content.pm.ShortcutManagerCompat;
import androidx.core.graphics.drawable.IconCompat;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.models.user.User;
import com.discord.stores.StoreStream;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.images.MGImagesBitmap;
import com.discord.utilities.intent.IntentUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.f0.q;
import d0.g0.t;
import d0.t.g0;
import d0.t.n;
import d0.t.n0;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.sequences.Sequence;
import rx.Observable;
import rx.functions.Func2;
/* compiled from: ShareUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001'B\t\b\u0002¢\u0006\u0004\b%\u0010&J-\u0010\n\u001a\u00020\t*\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00032\b\u0010\u0006\u001a\u0004\u0018\u00010\u00052\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0015\u0010\r\u001a\u00020\f2\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0004\b\r\u0010\u000eJ)\u0010\u0012\u001a\u00020\f2\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\t2\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0010¢\u0006\u0004\b\u0012\u0010\u0013JY\u0010\n\u001a\u00020\t2\u0006\u0010\u0004\u001a\u00020\u00032\n\u0010\u0016\u001a\u00060\u0014j\u0002`\u00152\u0006\u0010\u0018\u001a\u00020\u00172\b\b\u0002\u0010\u0019\u001a\u00020\u00172\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\u000e\b\u0002\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001a2\b\b\u0002\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\n\u0010\u001dJ\u001d\u0010#\u001a\u00020\"2\u0006\u0010\u001f\u001a\u00020\u001e2\u0006\u0010!\u001a\u00020 ¢\u0006\u0004\b#\u0010$¨\u0006("}, d2 = {"Lcom/discord/utilities/ShareUtils;", "", "Lcom/discord/api/channel/Channel;", "Landroid/content/Context;", "context", "Landroidx/core/graphics/drawable/IconCompat;", "icon", "", "rank", "Landroidx/core/content/pm/ShortcutInfoCompat;", "toShortcutInfo", "(Lcom/discord/api/channel/Channel;Landroid/content/Context;Landroidx/core/graphics/drawable/IconCompat;I)Landroidx/core/content/pm/ShortcutInfoCompat;", "", "updateDirectShareTargets", "(Landroid/content/Context;)V", "shortcutInfo", "Landroidx/core/app/NotificationCompat$Builder;", "notificationBuilder", "addShortcut", "(Landroid/content/Context;Landroidx/core/content/pm/ShortcutInfoCompat;Landroidx/core/app/NotificationCompat$Builder;)V", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "shortLabel", "longLabel", "", "Landroidx/core/app/Person;", "persons", "(Landroid/content/Context;JLjava/lang/CharSequence;Ljava/lang/CharSequence;Landroidx/core/graphics/drawable/IconCompat;Ljava/util/List;I)Landroidx/core/content/pm/ShortcutInfoCompat;", "Landroid/content/Intent;", "intent", "", "deleteExtras", "Lcom/discord/utilities/ShareUtils$SharedContent;", "getSharedContent", "(Landroid/content/Intent;Z)Lcom/discord/utilities/ShareUtils$SharedContent;", HookHelper.constructorName, "()V", "SharedContent", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ShareUtils {
    public static final ShareUtils INSTANCE = new ShareUtils();

    /* compiled from: ShareUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B!\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0002\u0012\u000e\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005¢\u0006\u0004\b\u001b\u0010\u001cJ\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0018\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ.\u0010\u000b\u001a\u00020\u00002\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00022\u0010\b\u0002\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R!\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0017\u001a\u0004\b\u0018\u0010\bR\u001b\u0010\t\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004¨\u0006\u001d"}, d2 = {"Lcom/discord/utilities/ShareUtils$SharedContent;", "", "", "component1", "()Ljava/lang/CharSequence;", "", "Landroid/net/Uri;", "component2", "()Ljava/util/List;", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "uris", "copy", "(Ljava/lang/CharSequence;Ljava/util/List;)Lcom/discord/utilities/ShareUtils$SharedContent;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getUris", "Ljava/lang/CharSequence;", "getText", HookHelper.constructorName, "(Ljava/lang/CharSequence;Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class SharedContent {
        private final CharSequence text;
        private final List<Uri> uris;

        /* JADX WARN: Multi-variable type inference failed */
        public SharedContent(CharSequence charSequence, List<? extends Uri> list) {
            this.text = charSequence;
            this.uris = list;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ SharedContent copy$default(SharedContent sharedContent, CharSequence charSequence, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                charSequence = sharedContent.text;
            }
            if ((i & 2) != 0) {
                list = sharedContent.uris;
            }
            return sharedContent.copy(charSequence, list);
        }

        public final CharSequence component1() {
            return this.text;
        }

        public final List<Uri> component2() {
            return this.uris;
        }

        public final SharedContent copy(CharSequence charSequence, List<? extends Uri> list) {
            return new SharedContent(charSequence, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof SharedContent)) {
                return false;
            }
            SharedContent sharedContent = (SharedContent) obj;
            return m.areEqual(this.text, sharedContent.text) && m.areEqual(this.uris, sharedContent.uris);
        }

        public final CharSequence getText() {
            return this.text;
        }

        public final List<Uri> getUris() {
            return this.uris;
        }

        public int hashCode() {
            CharSequence charSequence = this.text;
            int i = 0;
            int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
            List<Uri> list = this.uris;
            if (list != null) {
                i = list.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("SharedContent(text=");
            R.append(this.text);
            R.append(", uris=");
            return a.K(R, this.uris, ")");
        }
    }

    private ShareUtils() {
    }

    public static /* synthetic */ void addShortcut$default(ShareUtils shareUtils, Context context, ShortcutInfoCompat shortcutInfoCompat, NotificationCompat.Builder builder, int i, Object obj) {
        if ((i & 4) != 0) {
            builder = null;
        }
        shareUtils.addShortcut(context, shortcutInfoCompat, builder);
    }

    public static /* synthetic */ ShortcutInfoCompat toShortcutInfo$default(ShareUtils shareUtils, Context context, long j, CharSequence charSequence, CharSequence charSequence2, IconCompat iconCompat, List list, int i, int i2, Object obj) {
        return shareUtils.toShortcutInfo(context, j, charSequence, (i2 & 8) != 0 ? charSequence : charSequence2, (i2 & 16) != 0 ? null : iconCompat, (i2 & 32) != 0 ? n.emptyList() : list, (i2 & 64) != 0 ? 0 : i);
    }

    public final void addShortcut(Context context, ShortcutInfoCompat shortcutInfoCompat, NotificationCompat.Builder builder) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(shortcutInfoCompat, "shortcutInfo");
        if (builder != null) {
            builder.setShortcutId(shortcutInfoCompat.getId());
        }
        if (builder != null) {
            builder.setLocusId(new LocusIdCompat(shortcutInfoCompat.getId()));
        }
        List<ShortcutInfoCompat> dynamicShortcuts = ShortcutManagerCompat.getDynamicShortcuts(context);
        m.checkNotNullExpressionValue(dynamicShortcuts, "ShortcutManagerCompat.getDynamicShortcuts(context)");
        boolean z2 = false;
        if (!(dynamicShortcuts instanceof Collection) || !dynamicShortcuts.isEmpty()) {
            Iterator<T> it = dynamicShortcuts.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                ShortcutInfoCompat shortcutInfoCompat2 = (ShortcutInfoCompat) it.next();
                m.checkNotNullExpressionValue(shortcutInfoCompat2, "it");
                if (m.areEqual(shortcutInfoCompat2.getId(), shortcutInfoCompat.getId())) {
                    z2 = true;
                    break;
                }
            }
        }
        if (!z2) {
            ShortcutManagerCompat.pushDynamicShortcut(context, shortcutInfoCompat);
        }
    }

    public final SharedContent getSharedContent(Intent intent, boolean z2) {
        Uri uri;
        m.checkNotNullParameter(intent, "intent");
        CharSequence charSequenceExtra = intent.getCharSequenceExtra("android.intent.extra.TEXT");
        String stringExtra = intent.getStringExtra("android.intent.extra.SUBJECT");
        ArrayList arrayList = null;
        if (charSequenceExtra == null || t.isBlank(charSequenceExtra)) {
            charSequenceExtra = !(stringExtra == null || t.isBlank(stringExtra)) ? stringExtra : null;
        }
        String action = intent.getAction();
        if (action != null) {
            int hashCode = action.hashCode();
            if (hashCode != -1173264947) {
                if (hashCode == -58484670 && action.equals("android.intent.action.SEND_MULTIPLE")) {
                    arrayList = intent.getParcelableArrayListExtra("android.intent.extra.STREAM");
                }
            } else if (action.equals("android.intent.action.SEND") && (uri = (Uri) intent.getParcelableExtra("android.intent.extra.STREAM")) != null) {
                arrayList = n.arrayListOf(uri);
            }
        }
        if (z2) {
            intent.removeExtra("android.intent.extra.TEXT");
            intent.removeExtra("android.intent.extra.SUBJECT");
            intent.removeExtra("android.intent.extra.STREAM");
        }
        return new SharedContent(charSequenceExtra, arrayList);
    }

    public final ShortcutInfoCompat toShortcutInfo(Context context, long j, CharSequence charSequence, CharSequence charSequence2, IconCompat iconCompat, List<? extends Person> list, int i) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(charSequence, "shortLabel");
        m.checkNotNullParameter(charSequence2, "longLabel");
        m.checkNotNullParameter(list, "persons");
        ShortcutInfoCompat.Builder categories = new ShortcutInfoCompat.Builder(context, String.valueOf(j)).setLocusId(new LocusIdCompat(String.valueOf(j))).setShortLabel(charSequence).setLongLabel(charSequence2).setIcon(iconCompat).setIntent(IntentUtils.RouteBuilders.selectChannel$default(j, 0L, null, 6, null).setPackage(context.getPackageName())).setRank(i).setLongLived(true).setCategories(n0.setOf((Object[]) new String[]{"com.discord.intent.category.DIRECT_SHARE_TARGET", "android.shortcut.conversation"}));
        Object[] array = list.toArray(new Person[0]);
        Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
        ShortcutInfoCompat build = categories.setPersons((Person[]) array).build();
        m.checkNotNullExpressionValue(build, "ShortcutInfoCompat.Build…ray())\n          .build()");
        return build;
    }

    public final void updateDirectShareTargets(final Context context) {
        m.checkNotNullParameter(context, "context");
        final int dpToPixels = DimenUtils.dpToPixels(72);
        final int dpToPixels2 = DimenUtils.dpToPixels(108);
        StoreStream.Companion companion = StoreStream.Companion;
        Observable F = ObservableExtensionsKt.takeSingleUntilTimeout$default(companion.getChannels().observeDirectShareCandidates(context), 0L, false, 1, null).F(ShareUtils$updateDirectShareTargets$1.INSTANCE);
        Observable<Map<Long, Channel>> x2 = companion.getChannels().observeGuildAndPrivateChannels().x(ShareUtils$updateDirectShareTargets$2.INSTANCE);
        m.checkNotNullExpressionValue(x2, "StoreStream\n            …ilter { it.isNotEmpty() }");
        Observable j = Observable.j(F, ObservableExtensionsKt.takeSingleUntilTimeout$default(x2, 0L, false, 1, null), new Func2<List<? extends ChannelShortcutInfo>, Map<Long, ? extends Channel>, List<? extends Pair<? extends ChannelShortcutInfo, ? extends String>>>() { // from class: com.discord.utilities.ShareUtils$updateDirectShareTargets$3
            @Override // rx.functions.Func2
            public /* bridge */ /* synthetic */ List<? extends Pair<? extends ChannelShortcutInfo, ? extends String>> call(List<? extends ChannelShortcutInfo> list, Map<Long, ? extends Channel> map) {
                return call2((List<ChannelShortcutInfo>) list, (Map<Long, Channel>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final List<Pair<ChannelShortcutInfo, String>> call2(List<ChannelShortcutInfo> list, Map<Long, Channel> map) {
                Sequence sequence;
                List<ShortcutInfo> list2;
                m.checkNotNullExpressionValue(list, "directShareChannels");
                ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
                for (ChannelShortcutInfo channelShortcutInfo : list) {
                    arrayList.add(Long.valueOf(channelShortcutInfo.getChannel().h()));
                }
                Set set = u.toSet(arrayList);
                if (Build.VERSION.SDK_INT >= 25) {
                    ShortcutManager shortcutManager = (ShortcutManager) context.getSystemService(ShortcutManager.class);
                    if (shortcutManager == null || (list2 = shortcutManager.getDynamicShortcuts()) == null) {
                        list2 = n.emptyList();
                    }
                    sequence = q.map(q.mapNotNull(q.filter(q.map(q.filter(u.asSequence(list2), ShareUtils$updateDirectShareTargets$3$pinnedOnlyShortcutChannels$1.INSTANCE), ShareUtils$updateDirectShareTargets$3$pinnedOnlyShortcutChannels$2.INSTANCE), new ShareUtils$updateDirectShareTargets$3$pinnedOnlyShortcutChannels$3(set)), new ShareUtils$updateDirectShareTargets$3$pinnedOnlyShortcutChannels$4(map)), ShareUtils$updateDirectShareTargets$3$pinnedOnlyShortcutChannels$5.INSTANCE);
                } else {
                    sequence = d0.f0.n.emptySequence();
                }
                List<ChannelShortcutInfo> plus = u.plus((Collection) list, sequence);
                ArrayList arrayList2 = new ArrayList();
                for (ChannelShortcutInfo channelShortcutInfo2 : plus) {
                    Pair pair = null;
                    String forChannel$default = IconUtils.getForChannel$default(channelShortcutInfo2.getChannel(), null, 2, null);
                    if (forChannel$default != null) {
                        pair = d0.o.to(channelShortcutInfo2, forChannel$default);
                    }
                    if (pair != null) {
                        arrayList2.add(pair);
                    }
                }
                return arrayList2;
            }
        });
        m.checkNotNullExpressionValue(j, "Observable.combineLatest…o to icon }\n      }\n    }");
        Observable z2 = ObservableExtensionsKt.computationLatest(j).z(new b<List<? extends Pair<? extends ChannelShortcutInfo, ? extends String>>, Observable<? extends Pair<? extends List<? extends Pair<? extends ChannelShortcutInfo, ? extends String>>, ? extends MGImagesBitmap.CloseableBitmaps>>>() { // from class: com.discord.utilities.ShareUtils$updateDirectShareTargets$4
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Observable<? extends Pair<? extends List<? extends Pair<? extends ChannelShortcutInfo, ? extends String>>, ? extends MGImagesBitmap.CloseableBitmaps>> call(List<? extends Pair<? extends ChannelShortcutInfo, ? extends String>> list) {
                return call2((List<Pair<ChannelShortcutInfo, String>>) list);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Observable<? extends Pair<List<Pair<ChannelShortcutInfo, String>>, MGImagesBitmap.CloseableBitmaps>> call2(final List<Pair<ChannelShortcutInfo, String>> list) {
                LinkedHashSet linkedHashSet = new LinkedHashSet();
                m.checkNotNullExpressionValue(list, "channelAndIconUriPairs");
                Iterator<T> it = list.iterator();
                while (it.hasNext()) {
                    linkedHashSet.add(new MGImagesBitmap.ImageRequest((String) ((Pair) it.next()).getSecond(), false));
                }
                return (Observable<R>) MGImagesBitmap.getBitmaps(linkedHashSet).F(new b<MGImagesBitmap.CloseableBitmaps, MGImagesBitmap.CloseableBitmaps>() { // from class: com.discord.utilities.ShareUtils$updateDirectShareTargets$4.2
                    public final MGImagesBitmap.CloseableBitmaps call(MGImagesBitmap.CloseableBitmaps closeableBitmaps) {
                        m.checkNotNullExpressionValue(closeableBitmaps, "iconBitmaps");
                        LinkedHashMap linkedHashMap = new LinkedHashMap(g0.mapCapacity(closeableBitmaps.size()));
                        Iterator<T> it2 = closeableBitmaps.entrySet().iterator();
                        while (it2.hasNext()) {
                            Map.Entry entry = (Map.Entry) it2.next();
                            Object key = entry.getKey();
                            MGImages mGImages = MGImages.INSTANCE;
                            Bitmap bitmap = (Bitmap) entry.getValue();
                            ShareUtils$updateDirectShareTargets$4 shareUtils$updateDirectShareTargets$4 = ShareUtils$updateDirectShareTargets$4.this;
                            int i = dpToPixels;
                            int i2 = dpToPixels2;
                            linkedHashMap.put(key, mGImages.centerBitmapInTransparentBitmap(bitmap, i, i, i2, i2));
                        }
                        return new MGImagesBitmap.CloseableBitmaps(linkedHashMap, Build.VERSION.SDK_INT > 24);
                    }
                }).F(new b<MGImagesBitmap.CloseableBitmaps, Pair<? extends List<? extends Pair<? extends ChannelShortcutInfo, ? extends String>>, ? extends MGImagesBitmap.CloseableBitmaps>>() { // from class: com.discord.utilities.ShareUtils$updateDirectShareTargets$4.3
                    public final Pair<List<Pair<ChannelShortcutInfo, String>>, MGImagesBitmap.CloseableBitmaps> call(MGImagesBitmap.CloseableBitmaps closeableBitmaps) {
                        return d0.o.to(list, closeableBitmaps);
                    }
                });
            }
        });
        m.checkNotNullExpressionValue(z2, "Observable.combineLatest…riPairs to it }\n        }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(z2), ShareUtils.class, (r18 & 2) != 0 ? null : context, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new ShareUtils$updateDirectShareTargets$5(context));
        if (Build.VERSION.SDK_INT >= 25) {
            Observable<Map<Long, Channel>> x3 = companion.getChannels().observeGuildAndPrivateChannels().x(ShareUtils$updateDirectShareTargets$6.INSTANCE);
            m.checkNotNullExpressionValue(x3, "StoreStream.getChannels(…ilter { it.isNotEmpty() }");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(ObservableExtensionsKt.takeSingleUntilTimeout$default(x3, 0L, false, 1, null)), ShareUtils.class, (r18 & 2) != 0 ? null : context, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new ShareUtils$updateDirectShareTargets$7((ShortcutManager) context.getSystemService(ShortcutManager.class), context));
        }
    }

    public final ShortcutInfoCompat toShortcutInfo(Channel channel, Context context, IconCompat iconCompat, int i) {
        long h = channel.h();
        String c = ChannelUtils.c(channel);
        List<User> g = ChannelUtils.g(channel);
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(g, 10));
        for (User user : g) {
            Person build = new Person.Builder().setName(user.getUsername()).setKey(user.toString()).setBot(user.isBot()).build();
            m.checkNotNullExpressionValue(build, "Person.Builder()\n       …\n                .build()");
            arrayList.add(build);
        }
        return toShortcutInfo$default(this, context, h, c, null, iconCompat, arrayList, i, 8, null);
    }
}
