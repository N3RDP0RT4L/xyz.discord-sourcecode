package com.discord.utilities.debug;

import andhook.lib.HookHelper;
import android.app.ActivityManager;
import android.app.ApplicationExitInfo;
import android.os.Build;
import androidx.appcompat.widget.ActivityChooserModel;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.lifecycle.ApplicationProvider;
import d0.g;
import d0.o;
import d0.t.h0;
import d0.t.u;
import d0.z.d.m;
import java.util.List;
import java.util.Map;
import kotlin.Lazy;
import kotlin.Metadata;
/* compiled from: HistoricalProcessExitReason.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\n\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001\u000bB\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ\u0011\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004R\u001f\u0010\b\u001a\u0004\u0018\u00010\u00028F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u0005\u0010\u0006\u001a\u0004\b\u0007\u0010\u0004¨\u0006\f"}, d2 = {"Lcom/discord/utilities/debug/HistoricalProcessExitReason;", "", "Lcom/discord/utilities/debug/HistoricalProcessExitReason$Reason;", "createLastReason", "()Lcom/discord/utilities/debug/HistoricalProcessExitReason$Reason;", "lastReason$delegate", "Lkotlin/Lazy;", "getLastReason", "lastReason", HookHelper.constructorName, "()V", "Reason", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class HistoricalProcessExitReason {
    public static final HistoricalProcessExitReason INSTANCE = new HistoricalProcessExitReason();
    private static final Lazy lastReason$delegate = g.lazy(HistoricalProcessExitReason$lastReason$2.INSTANCE);

    /* compiled from: HistoricalProcessExitReason.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J&\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u00022\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\n\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\n\u0010\u0004J\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0010\u001a\u00020\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0006\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0012\u001a\u0004\b\u0013\u0010\u0004R\u001b\u0010\u0007\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0012\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/utilities/debug/HistoricalProcessExitReason$Reason;", "", "", "component1", "()Ljava/lang/String;", "component2", ModelAuditLogEntry.CHANGE_KEY_REASON, ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "copy", "(Ljava/lang/String;Ljava/lang/String;)Lcom/discord/utilities/debug/HistoricalProcessExitReason$Reason;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getReason", "getDescription", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;)V", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Reason {
        private final String description;
        private final String reason;

        public Reason(String str, String str2) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_REASON);
            this.reason = str;
            this.description = str2;
        }

        public static /* synthetic */ Reason copy$default(Reason reason, String str, String str2, int i, Object obj) {
            if ((i & 1) != 0) {
                str = reason.reason;
            }
            if ((i & 2) != 0) {
                str2 = reason.description;
            }
            return reason.copy(str, str2);
        }

        public final String component1() {
            return this.reason;
        }

        public final String component2() {
            return this.description;
        }

        public final Reason copy(String str, String str2) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_REASON);
            return new Reason(str, str2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Reason)) {
                return false;
            }
            Reason reason = (Reason) obj;
            return m.areEqual(this.reason, reason.reason) && m.areEqual(this.description, reason.description);
        }

        public final String getDescription() {
            return this.description;
        }

        public final String getReason() {
            return this.reason;
        }

        public int hashCode() {
            String str = this.reason;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.description;
            if (str2 != null) {
                i = str2.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("Reason(reason=");
            R.append(this.reason);
            R.append(", description=");
            return a.H(R, this.description, ")");
        }
    }

    private HistoricalProcessExitReason() {
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Reason createLastReason() {
        Reason reason = null;
        if (Build.VERSION.SDK_INT < 30) {
            return null;
        }
        Map mapOf = h0.mapOf(o.to(6, "ANR"), o.to(4, "CRASH"), o.to(5, "CRASH_NATIVE"), o.to(12, "DEPENDENCY_DIED"), o.to(9, "EXCESSIVE_RESOURCE_USAGE"), o.to(1, "EXIT_SELF"), o.to(7, "INITIALIZATION_FAILURE"), o.to(3, "LOW_MEMORY"), o.to(13, "OTHER"), o.to(8, "PERMISSION_CHANGE"), o.to(2, "SIGNALED"), o.to(0, "UNKNOWN"), o.to(10, "USER_REQUESTED"), o.to(11, "USER_STOPPED"));
        Object systemService = ApplicationProvider.INSTANCE.get().getSystemService(ActivityChooserModel.ATTRIBUTE_ACTIVITY);
        if (!(systemService instanceof ActivityManager)) {
            systemService = null;
        }
        ActivityManager activityManager = (ActivityManager) systemService;
        if (activityManager != null) {
            List<ApplicationExitInfo> historicalProcessExitReasons = activityManager.getHistoricalProcessExitReasons(null, 0, 1);
            m.checkNotNullExpressionValue(historicalProcessExitReasons, "activityManager.getHisto…ssExitReasons(null, 0, 1)");
            ApplicationExitInfo applicationExitInfo = (ApplicationExitInfo) u.firstOrNull((List<? extends Object>) historicalProcessExitReasons);
            if (applicationExitInfo != null) {
                String str = (String) mapOf.get(Integer.valueOf(applicationExitInfo.getReason()));
                if (str == null) {
                    StringBuilder R = a.R("Unknown ");
                    R.append(applicationExitInfo.getReason());
                    str = R.toString();
                }
                reason = new Reason(str, applicationExitInfo.getDescription());
            }
        }
        return reason;
    }

    public final Reason getLastReason() {
        return (Reason) lastReason$delegate.getValue();
    }
}
