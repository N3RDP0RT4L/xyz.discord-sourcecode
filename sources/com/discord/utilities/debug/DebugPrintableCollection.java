package com.discord.utilities.debug;

import andhook.lib.HookHelper;
import android.os.Build;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.logging.LoggingProvider;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: DebugPrintable.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \"2\u00020\u0001:\u0001\"B\u0007¢\u0006\u0004\b!\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u0007\u0010\bJ%\u0010\u000f\u001a\u00060\rj\u0002`\u000e2\u0006\u0010\n\u001a\u00020\t2\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u000b¢\u0006\u0004\b\u000f\u0010\u0010J\u0019\u0010\u0012\u001a\u00020\u00022\n\u0010\u0011\u001a\u00060\rj\u0002`\u000e¢\u0006\u0004\b\u0012\u0010\u0013J\u0019\u0010\u0017\u001a\u00020\u00022\n\u0010\u0016\u001a\u00060\u0014j\u0002`\u0015¢\u0006\u0004\b\u0017\u0010\u0018R&\u0010\u001b\u001a\u0012\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u0004\u0012\u00020\u001a0\u00198\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\u001a\u0010\u001d\u001a\u00060\rj\u0002`\u000e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u0016\u0010\u001f\u001a\u00020\u00018\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 ¨\u0006#"}, d2 = {"Lcom/discord/utilities/debug/DebugPrintableCollection;", "", "", "removeDeadReferencesLocked", "()V", "Lcom/discord/utilities/debug/DebugPrintBuilder;", "dp", "addSystemEntry", "(Lcom/discord/utilities/debug/DebugPrintBuilder;)V", "Lcom/discord/utilities/debug/DebugPrintable;", "ref", "", "tag", "", "Lcom/discord/utilities/debug/DebugPrintableId;", "add", "(Lcom/discord/utilities/debug/DebugPrintable;Ljava/lang/String;)J", "debugPrintableId", "remove", "(J)V", "Ljava/lang/StringBuilder;", "Lkotlin/text/StringBuilder;", "sb", "debugPrint", "(Ljava/lang/StringBuilder;)V", "", "Lcom/discord/utilities/debug/DebugPrintableRef;", "collection", "Ljava/util/Map;", "idCounter", "J", "sync", "Ljava/lang/Object;", HookHelper.constructorName, "Companion", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class DebugPrintableCollection {
    public static final Companion Companion = new Companion(null);
    private static String libdiscordVersion = null;
    private static final int maxDebugPrintableStringLength = 524288;
    private long idCounter = 1;
    private final Map<Long, DebugPrintableRef> collection = new LinkedHashMap();
    private final Object sync = new Object();

    /* compiled from: DebugPrintable.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0003\u0010\u0007R\u0016\u0010\t\u001a\u00020\b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\t\u0010\n¨\u0006\r"}, d2 = {"Lcom/discord/utilities/debug/DebugPrintableCollection$Companion;", "", "", "libdiscordVersion", "", "initialize", "(Ljava/lang/String;)V", "Ljava/lang/String;", "", "maxDebugPrintableStringLength", "I", HookHelper.constructorName, "()V", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void initialize(String str) {
            m.checkNotNullParameter(str, "libdiscordVersion");
            DebugPrintableCollection.libdiscordVersion = str;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public static /* synthetic */ long add$default(DebugPrintableCollection debugPrintableCollection, DebugPrintable debugPrintable, String str, int i, Object obj) {
        if ((i & 2) != 0) {
            str = null;
        }
        return debugPrintableCollection.add(debugPrintable, str);
    }

    private final void addSystemEntry(DebugPrintBuilder debugPrintBuilder) {
        debugPrintBuilder.appendLine("System:");
        debugPrintBuilder.appendKeyValue("libdiscordVersion", libdiscordVersion);
        debugPrintBuilder.appendKeyValue("SDK_INT", Integer.valueOf(Build.VERSION.SDK_INT));
        debugPrintBuilder.append("\n");
    }

    private final void removeDeadReferencesLocked() {
        ArrayList<Number> arrayList = new ArrayList();
        for (Map.Entry<Long, DebugPrintableRef> entry : this.collection.entrySet()) {
            long longValue = entry.getKey().longValue();
            if (entry.getValue().getReference().get() == null) {
                arrayList.add(Long.valueOf(longValue));
            }
        }
        for (Number number : arrayList) {
            this.collection.remove(Long.valueOf(number.longValue()));
        }
    }

    public final long add(DebugPrintable debugPrintable, String str) {
        long j;
        m.checkNotNullParameter(debugPrintable, "ref");
        synchronized (this.sync) {
            j = this.idCounter;
            this.idCounter = j + 1;
            long j2 = 0;
            while (this.collection.containsKey(Long.valueOf(j))) {
                j = this.idCounter;
                this.idCounter = j + 1;
                j2++;
                if (j2 < 0) {
                    break;
                }
            }
            this.collection.put(Long.valueOf(j), new DebugPrintableRef(str, debugPrintable));
            removeDeadReferencesLocked();
        }
        return j;
    }

    /* JADX WARN: Code restructure failed: missing block: B:18:0x0089, code lost:
        r9.delete(524288, r9.length() - 1);
        r9.append(" {truncated}");
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void debugPrint(java.lang.StringBuilder r9) {
        /*
            r8 = this;
            java.lang.String r0 = "sb"
            d0.z.d.m.checkNotNullParameter(r9, r0)
            com.discord.utilities.debug.DebugPrintBuilder r0 = new com.discord.utilities.debug.DebugPrintBuilder
            r0.<init>(r9)
            java.lang.Object r1 = r8.sync
            monitor-enter(r1)
            r8.addSystemEntry(r0)     // Catch: java.lang.Throwable -> L99
            java.util.Map<java.lang.Long, com.discord.utilities.debug.DebugPrintableRef> r2 = r8.collection     // Catch: java.lang.Throwable -> L99
            java.util.Set r2 = r2.entrySet()     // Catch: java.lang.Throwable -> L99
            java.util.Iterator r2 = r2.iterator()     // Catch: java.lang.Throwable -> L99
        L1b:
            boolean r3 = r2.hasNext()     // Catch: java.lang.Throwable -> L99
            if (r3 == 0) goto L97
            java.lang.Object r3 = r2.next()     // Catch: java.lang.Throwable -> L99
            java.util.Map$Entry r3 = (java.util.Map.Entry) r3     // Catch: java.lang.Throwable -> L99
            java.lang.Object r4 = r3.getKey()     // Catch: java.lang.Throwable -> L99
            java.lang.Number r4 = (java.lang.Number) r4     // Catch: java.lang.Throwable -> L99
            long r4 = r4.longValue()     // Catch: java.lang.Throwable -> L99
            java.lang.Object r3 = r3.getValue()     // Catch: java.lang.Throwable -> L99
            com.discord.utilities.debug.DebugPrintableRef r3 = (com.discord.utilities.debug.DebugPrintableRef) r3     // Catch: java.lang.Throwable -> L99
            java.lang.ref.WeakReference r6 = r3.getReference()     // Catch: java.lang.Throwable -> L99
            java.lang.Object r6 = r6.get()     // Catch: java.lang.Throwable -> L99
            com.discord.utilities.debug.DebugPrintable r6 = (com.discord.utilities.debug.DebugPrintable) r6     // Catch: java.lang.Throwable -> L99
            if (r6 == 0) goto L1b
            java.lang.String r7 = "printableRef.reference.get() ?: continue"
            d0.z.d.m.checkNotNullExpressionValue(r6, r7)     // Catch: java.lang.Throwable -> L99
            r9.append(r4)     // Catch: java.lang.Throwable -> L99
            java.lang.String r4 = r3.getTag()     // Catch: java.lang.Throwable -> L99
            if (r4 == 0) goto L5e
            r4 = 32
            r9.append(r4)     // Catch: java.lang.Throwable -> L99
            java.lang.String r3 = r3.getTag()     // Catch: java.lang.Throwable -> L99
            r9.append(r3)     // Catch: java.lang.Throwable -> L99
        L5e:
            java.lang.String r3 = ":\n"
            r9.append(r3)     // Catch: java.lang.Throwable -> L99
            r6.debugPrint(r0)     // Catch: java.lang.Exception -> L67 java.lang.Throwable -> L99
            goto L7c
        L67:
            r3 = move-exception
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch: java.lang.Throwable -> L99
            r4.<init>()     // Catch: java.lang.Throwable -> L99
            java.lang.String r5 = "Exception: "
            r4.append(r5)     // Catch: java.lang.Throwable -> L99
            r4.append(r3)     // Catch: java.lang.Throwable -> L99
            java.lang.String r3 = r4.toString()     // Catch: java.lang.Throwable -> L99
            r9.append(r3)     // Catch: java.lang.Throwable -> L99
        L7c:
            r3 = 10
            r9.append(r3)     // Catch: java.lang.Throwable -> L99
            int r3 = r9.length()     // Catch: java.lang.Throwable -> L99
            r4 = 524288(0x80000, float:7.34684E-40)
            if (r3 <= r4) goto L1b
            int r0 = r9.length()     // Catch: java.lang.Throwable -> L99
            int r0 = r0 + (-1)
            r9.delete(r4, r0)     // Catch: java.lang.Throwable -> L99
            java.lang.String r0 = " {truncated}"
            r9.append(r0)     // Catch: java.lang.Throwable -> L99
        L97:
            monitor-exit(r1)
            return
        L99:
            r9 = move-exception
            monitor-exit(r1)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.debug.DebugPrintableCollection.debugPrint(java.lang.StringBuilder):void");
    }

    public final void remove(long j) {
        synchronized (this.sync) {
            if (this.collection.remove(Long.valueOf(j)) == null) {
                Logger logger = LoggingProvider.INSTANCE.get();
                Logger.w$default(logger, "DebugPrintable", "Unable to locate tag '" + j + '\'', null, 4, null);
            }
        }
    }
}
