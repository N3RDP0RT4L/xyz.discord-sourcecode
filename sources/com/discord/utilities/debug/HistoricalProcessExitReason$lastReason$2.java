package com.discord.utilities.debug;

import com.discord.utilities.debug.HistoricalProcessExitReason;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: HistoricalProcessExitReason.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/utilities/debug/HistoricalProcessExitReason$Reason;", "invoke", "()Lcom/discord/utilities/debug/HistoricalProcessExitReason$Reason;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class HistoricalProcessExitReason$lastReason$2 extends o implements Function0<HistoricalProcessExitReason.Reason> {
    public static final HistoricalProcessExitReason$lastReason$2 INSTANCE = new HistoricalProcessExitReason$lastReason$2();

    public HistoricalProcessExitReason$lastReason$2() {
        super(0);
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final HistoricalProcessExitReason.Reason invoke() {
        HistoricalProcessExitReason.Reason createLastReason;
        createLastReason = HistoricalProcessExitReason.INSTANCE.createLastReason();
        return createLastReason;
    }
}
