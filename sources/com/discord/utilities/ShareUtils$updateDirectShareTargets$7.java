package com.discord.utilities;

import android.content.Context;
import android.content.pm.ShortcutManager;
import com.discord.api.channel.Channel;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: ShareUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\t\u001a\u00020\u00062.\u0010\u0005\u001a*\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003 \u0004*\u0014\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "kotlin.jvm.PlatformType", "channels", "", "invoke", "(Ljava/util/Map;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ShareUtils$updateDirectShareTargets$7 extends o implements Function1<Map<Long, ? extends Channel>, Unit> {
    public final /* synthetic */ Context $context;
    public final /* synthetic */ ShortcutManager $shortcutManager;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ShareUtils$updateDirectShareTargets$7(ShortcutManager shortcutManager, Context context) {
        super(1);
        this.$shortcutManager = shortcutManager;
        this.$context = context;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Map<Long, ? extends Channel> map) {
        invoke2((Map<Long, Channel>) map);
        return Unit.a;
    }

    /* JADX WARN: Removed duplicated region for block: B:26:0x004c A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:29:0x0018 A[SYNTHETIC] */
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void invoke2(java.util.Map<java.lang.Long, com.discord.api.channel.Channel> r6) {
        /*
            r5 = this;
            android.content.pm.ShortcutManager r0 = r5.$shortcutManager
            if (r0 == 0) goto Lb
            java.util.List r0 = r0.getDynamicShortcuts()
            if (r0 == 0) goto Lb
            goto Lf
        Lb:
            java.util.List r0 = d0.t.n.emptyList()
        Lf:
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.util.Iterator r0 = r0.iterator()
        L18:
            boolean r2 = r0.hasNext()
            java.lang.String r3 = "it"
            if (r2 == 0) goto L50
            java.lang.Object r2 = r0.next()
            r4 = r2
            android.content.pm.ShortcutInfo r4 = (android.content.pm.ShortcutInfo) r4
            d0.z.d.m.checkNotNullExpressionValue(r4, r3)
            boolean r3 = r4.isPinned()
            if (r3 == 0) goto L49
            java.lang.String r3 = r4.getId()
            java.lang.String r4 = "it.id"
            d0.z.d.m.checkNotNullExpressionValue(r3, r4)
            long r3 = java.lang.Long.parseLong(r3)
            java.lang.Long r3 = java.lang.Long.valueOf(r3)
            boolean r3 = r6.containsKey(r3)
            if (r3 != 0) goto L49
            r3 = 1
            goto L4a
        L49:
            r3 = 0
        L4a:
            if (r3 == 0) goto L18
            r1.add(r2)
            goto L18
        L50:
            java.util.ArrayList r6 = new java.util.ArrayList
            r0 = 10
            int r0 = d0.t.o.collectionSizeOrDefault(r1, r0)
            r6.<init>(r0)
            java.util.Iterator r0 = r1.iterator()
        L5f:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L76
            java.lang.Object r1 = r0.next()
            android.content.pm.ShortcutInfo r1 = (android.content.pm.ShortcutInfo) r1
            d0.z.d.m.checkNotNullExpressionValue(r1, r3)
            java.lang.String r1 = r1.getId()
            r6.add(r1)
            goto L5f
        L76:
            android.content.Context r0 = r5.$context
            androidx.core.content.pm.ShortcutManagerCompat.removeDynamicShortcuts(r0, r6)
            android.content.pm.ShortcutManager r0 = r5.$shortcutManager
            if (r0 == 0) goto L82
            r0.disableShortcuts(r6)
        L82:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.ShareUtils$updateDirectShareTargets$7.invoke2(java.util.Map):void");
    }
}
