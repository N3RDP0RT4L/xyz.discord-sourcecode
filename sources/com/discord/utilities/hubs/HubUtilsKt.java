package com.discord.utilities.hubs;

import android.content.Intent;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.fragment.app.FragmentManager;
import com.discord.app.AppFragment;
import com.discord.widgets.hubs.AddServerConfirmationArgs;
import com.discord.widgets.hubs.DescriptionResult;
import com.discord.widgets.hubs.WidgetHubAddServerConfirmationDialog;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: HubUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u001a\u001f\u0010\u0004\u001a\u0010\u0012\f\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u0001*\u00020\u0000¢\u0006\u0004\b\u0004\u0010\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/app/AppFragment;", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "kotlin.jvm.PlatformType", "getAddServerActivityResultHandler", "(Lcom/discord/app/AppFragment;)Landroidx/activity/result/ActivityResultLauncher;", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class HubUtilsKt {
    public static final ActivityResultLauncher<Intent> getAddServerActivityResultHandler(final AppFragment appFragment) {
        m.checkNotNullParameter(appFragment, "$this$getAddServerActivityResultHandler");
        ActivityResultLauncher<Intent> registerForActivityResult = appFragment.registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() { // from class: com.discord.utilities.hubs.HubUtilsKt$getAddServerActivityResultHandler$1
            public final void onActivityResult(ActivityResult activityResult) {
                Intent data;
                if (activityResult != null && (data = activityResult.getData()) != null) {
                    if (!(activityResult.getResultCode() == -1)) {
                        data = null;
                    }
                    if (data != null) {
                        m.checkNotNullParameter(data, "$this$getArgsFromIntent");
                        DescriptionResult descriptionResult = (DescriptionResult) data.getParcelableExtra("intent_args_key");
                        if (descriptionResult != null) {
                            WidgetHubAddServerConfirmationDialog.Companion companion = WidgetHubAddServerConfirmationDialog.Companion;
                            FragmentManager parentFragmentManager = AppFragment.this.getParentFragmentManager();
                            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                            companion.show(parentFragmentManager, new AddServerConfirmationArgs(descriptionResult.getGuildId(), descriptionResult.getHubName()));
                        }
                    }
                }
            }
        });
        m.checkNotNullExpressionValue(registerForActivityResult, "registerForActivityResul…      )\n          }\n    }");
        return registerForActivityResult;
    }
}
