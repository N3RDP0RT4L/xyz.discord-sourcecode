package com.discord.utilities.threads;

import andhook.lib.HookHelper;
import android.content.Context;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guild.GuildFeature;
import com.discord.api.permission.Permission;
import com.discord.api.thread.ThreadMetadata;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelNotificationSettings;
import com.discord.models.guild.Guild;
import com.discord.models.message.Message;
import com.discord.models.user.User;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreThreadsActiveJoined;
import com.discord.stores.StoreThreadsJoined;
import com.discord.utilities.SnowflakeUtils;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.time.Clock;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.time.TimeUtils;
import com.discord.widgets.chat.list.CreateThreadsFeatureFlag;
import com.discord.widgets.chat.list.NewThreadsPermissionsFeatureFlag;
import com.discord.widgets.chat.list.ViewThreadsFeatureFlag;
import d0.o;
import d0.t.g0;
import d0.t.h0;
import d0.t.n;
import d0.t.t;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import kotlin.Metadata;
import rx.Observable;
import xyz.discord.R;
/* compiled from: ThreadUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000ª\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\t\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001^B\t\b\u0002¢\u0006\u0004\b\\\u0010]JK\u0010\t\u001a\u0018\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u00022\u001c\u0010\u0007\u001a\u0018\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u00022\u0006\u0010\b\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\t\u0010\nJM\u0010\r\u001a\u0018\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u00022&\u0010\f\u001a\"\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u000b0\u00020\u0002H\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u001f\u0010\u0011\u001a\u0012\u0012\u0004\u0012\u00020\u000b0\u000fj\b\u0012\u0004\u0012\u00020\u000b`\u0010H\u0002¢\u0006\u0004\b\u0011\u0010\u0012JU\u0010\u0014\u001a\u0018\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u00022&\u0010\f\u001a\"\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u000b0\u00020\u00022\b\u0010\u0013\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\b\u0014\u0010\nJ;\u0010\u001a\u001a\u00020\u0003*\u00020\u00062\u000e\u0010\u0016\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00152\u000e\u0010\u0017\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00152\b\b\u0002\u0010\u0019\u001a\u00020\u0018¢\u0006\u0004\b\u001a\u0010\u001bJ)\u0010\"\u001a\u00020 2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001c2\u0006\u0010\u001f\u001a\u00020\u001e2\b\u0010!\u001a\u0004\u0018\u00010 ¢\u0006\u0004\b\"\u0010#J\u001d\u0010&\u001a\u00020\u001e2\u000e\u0010%\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`$¢\u0006\u0004\b&\u0010'J-\u0010+\u001a\u00020\u001e2\u0006\u0010)\u001a\u00020(2\u0006\u0010*\u001a\u00020\u00062\u000e\u0010%\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`$¢\u0006\u0004\b+\u0010,J\u0019\u0010/\u001a\u00020\u001e2\n\u0010.\u001a\u00060\u0003j\u0002`-¢\u0006\u0004\b/\u00100J;\u00107\u001a\u00020\u001e2\u000e\u00101\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`$2\b\u00102\u001a\u0004\u0018\u00010\u00062\b\u00104\u001a\u0004\u0018\u0001032\b\u00106\u001a\u0004\u0018\u000105¢\u0006\u0004\b7\u00108J;\u0010:\u001a\u00020\u001e2\u000e\u00101\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`$2\b\u00102\u001a\u0004\u0018\u00010\u00062\b\u00106\u001a\u0004\u0018\u0001052\b\b\u0002\u00109\u001a\u00020\u001e¢\u0006\u0004\b:\u0010;J;\u0010<\u001a\u00020\u001e2\u000e\u00101\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`$2\b\u00102\u001a\u0004\u0018\u00010\u00062\b\u00104\u001a\u0004\u0018\u0001032\b\u00106\u001a\u0004\u0018\u000105¢\u0006\u0004\b<\u00108J%\u0010=\u001a\u00020\u001e2\u0006\u0010*\u001a\u00020\u00062\u000e\u0010%\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`$¢\u0006\u0004\b=\u0010>J'\u0010?\u001a\u00020\u001e2\b\u0010*\u001a\u0004\u0018\u00010\u00062\u000e\u0010%\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`$¢\u0006\u0004\b?\u0010>J\u001d\u0010@\u001a\u00020\u001e2\u000e\u0010%\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`$¢\u0006\u0004\b@\u0010'J\u001d\u0010E\u001a\u00020D2\u0006\u0010B\u001a\u00020A2\u0006\u0010C\u001a\u00020 ¢\u0006\u0004\bE\u0010FJ\u0015\u0010G\u001a\u00020\u00032\u0006\u0010*\u001a\u00020\u0006¢\u0006\u0004\bG\u0010HJU\u0010Q\u001a\b\u0012\u0004\u0012\u00020\u00060P2\n\u0010I\u001a\u00060\u0003j\u0002`\u00042\u000e\u0010K\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`J2\u0006\u0010L\u001a\u00020 2\u0006\u0010M\u001a\u00020D2\n\b\u0002\u0010N\u001a\u0004\u0018\u00010 2\b\u0010O\u001a\u0004\u0018\u00010D¢\u0006\u0004\bQ\u0010RR\u0016\u0010S\u001a\u00020 8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bS\u0010TR5\u0010W\u001a\u001e\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020 0Uj\u000e\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020 `V8\u0006@\u0006¢\u0006\f\n\u0004\bW\u0010X\u001a\u0004\bY\u0010ZR\u0016\u0010[\u001a\u00020 8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b[\u0010T¨\u0006_"}, d2 = {"Lcom/discord/utilities/threads/ThreadUtils;", "", "", "", "Lcom/discord/primitives/ChannelId;", "", "Lcom/discord/api/channel/Channel;", "sortedThreadsMap", "selectedThread", "prependSelectedThread", "(Ljava/util/Map;Lcom/discord/api/channel/Channel;)Ljava/util/Map;", "Lcom/discord/stores/StoreThreadsActiveJoined$ActiveJoinedThread;", "threadsMap", "sortThreadsByJoinedDate", "(Ljava/util/Map;)Ljava/util/Map;", "Ljava/util/Comparator;", "Lkotlin/Comparator;", "getThreadTimestampComparator", "()Ljava/util/Comparator;", "selectedChannel", "sortThreadsForChannelList", "Lcom/discord/primitives/Timestamp;", "guildJoinedAt", "threadJoinedAt", "Lcom/discord/utilities/time/Clock;", "clock", "getThreadAckMessageTimestamp", "(Lcom/discord/api/channel/Channel;Ljava/lang/Long;Ljava/lang/Long;Lcom/discord/utilities/time/Clock;)J", "Lcom/discord/stores/StoreThreadsJoined$JoinedThread;", "joinedThread", "", "isGuildOrCategoryOrParentMuted", "", "parentNotificationSetting", "computeThreadNotificationSetting", "(Lcom/discord/stores/StoreThreadsJoined$JoinedThread;ZLjava/lang/Integer;)I", "Lcom/discord/api/permission/PermissionBit;", "channelPermissions", "isThreadModerator", "(Ljava/lang/Long;)Z", "Lcom/discord/models/user/User;", "user", "channel", "canManageThread", "(Lcom/discord/models/user/User;Lcom/discord/api/channel/Channel;Ljava/lang/Long;)Z", "Lcom/discord/primitives/GuildId;", "guildId", "isThreadsEnabled", "(J)Z", "parentChannelPermissions", "parentChannel", "Lcom/discord/models/message/Message;", "message", "Lcom/discord/models/guild/Guild;", "guild", "canCreatePublicThread", "(Ljava/lang/Long;Lcom/discord/api/channel/Channel;Lcom/discord/models/message/Message;Lcom/discord/models/guild/Guild;)Z", "checkGuildFeature", "canCreatePrivateThread", "(Ljava/lang/Long;Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;Z)Z", "canCreateThread", "canUnarchiveThread", "(Lcom/discord/api/channel/Channel;Ljava/lang/Long;)Z", "canViewAllPublicArchivedThreads", "canViewAllPrivateThreads", "Landroid/content/Context;", "context", "minutes", "", "autoArchiveDurationName", "(Landroid/content/Context;I)Ljava/lang/String;", "computeThreadAutoArchiveTimeMs", "(Lcom/discord/api/channel/Channel;)J", "parentChannelId", "Lcom/discord/primitives/MessageId;", "parentMessageId", "type", ModelAuditLogEntry.CHANGE_KEY_NAME, "autoArchiveDuration", ModelAuditLogEntry.CHANGE_KEY_LOCATION, "Lrx/Observable;", "createThread", "(JLjava/lang/Long;ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Lrx/Observable;", "DEFAULT_AUTO_ARCHIVE_DURATION", "I", "Ljava/util/LinkedHashMap;", "Lkotlin/collections/LinkedHashMap;", "THREAD_AUTO_ARCHIVE_DURATION_NAMES", "Ljava/util/LinkedHashMap;", "getTHREAD_AUTO_ARCHIVE_DURATION_NAMES", "()Ljava/util/LinkedHashMap;", "MAX_DISPLAYED_THREAD_BROWSER_ICON_COUNT", HookHelper.constructorName, "()V", "ThreadArchiveDurations", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ThreadUtils {
    public static final int DEFAULT_AUTO_ARCHIVE_DURATION = 1440;
    public static final int MAX_DISPLAYED_THREAD_BROWSER_ICON_COUNT = 99;
    public static final ThreadUtils INSTANCE = new ThreadUtils();
    private static final LinkedHashMap<Integer, Integer> THREAD_AUTO_ARCHIVE_DURATION_NAMES = h0.linkedMapOf(o.to(60, Integer.valueOf((int) R.string.auto_archive_duration_1_hour)), o.to(1440, Integer.valueOf((int) R.string.auto_archive_duration_24_hours)), o.to(Integer.valueOf((int) ThreadArchiveDurations.THREE_DAYS_IN_MINUTES), Integer.valueOf((int) R.string.auto_archive_duration_3_days)), o.to(Integer.valueOf((int) ThreadArchiveDurations.SEVEN_DAYS_IN_MINUTES), Integer.valueOf((int) R.string.auto_archive_duration_1_week)));

    /* compiled from: ThreadUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\b\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0007\u0010\u0004¨\u0006\n"}, d2 = {"Lcom/discord/utilities/threads/ThreadUtils$ThreadArchiveDurations;", "", "", "THREE_DAYS_IN_MINUTES", "I", "ONE_HOUR_IN_MINUTES", "ONE_DAY_IN_MINUTES", "SEVEN_DAYS_IN_MINUTES", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ThreadArchiveDurations {
        public static final ThreadArchiveDurations INSTANCE = new ThreadArchiveDurations();
        public static final int ONE_DAY_IN_MINUTES = 1440;
        public static final int ONE_HOUR_IN_MINUTES = 60;
        public static final int SEVEN_DAYS_IN_MINUTES = 10080;
        public static final int THREE_DAYS_IN_MINUTES = 4320;

        private ThreadArchiveDurations() {
        }
    }

    private ThreadUtils() {
    }

    public static /* synthetic */ boolean canCreatePrivateThread$default(ThreadUtils threadUtils, Long l, Channel channel, Guild guild, boolean z2, int i, Object obj) {
        if ((i & 8) != 0) {
            z2 = true;
        }
        return threadUtils.canCreatePrivateThread(l, channel, guild, z2);
    }

    public static /* synthetic */ long getThreadAckMessageTimestamp$default(ThreadUtils threadUtils, Channel channel, Long l, Long l2, Clock clock, int i, Object obj) {
        if ((i & 4) != 0) {
            clock = ClockFactory.get();
        }
        return threadUtils.getThreadAckMessageTimestamp(channel, l, l2, clock);
    }

    private final Comparator<StoreThreadsActiveJoined.ActiveJoinedThread> getThreadTimestampComparator() {
        return ThreadUtils$getThreadTimestampComparator$1.INSTANCE;
    }

    /* JADX WARN: Multi-variable type inference failed */
    private final Map<Long, Collection<Channel>> prependSelectedThread(Map<Long, ? extends Collection<Channel>> map, Channel channel) {
        boolean z2;
        boolean z3;
        Collection<Channel> collection = (Collection) map.get(Long.valueOf(channel.r()));
        if (collection == null) {
            collection = n.emptyList();
        }
        if (!(collection instanceof Collection) || !collection.isEmpty()) {
            for (Channel channel2 : collection) {
                if (channel2.h() == channel.h()) {
                    z3 = true;
                    continue;
                } else {
                    z3 = false;
                    continue;
                }
                if (z3) {
                    z2 = true;
                    break;
                }
            }
        }
        z2 = false;
        if (z2) {
            return map;
        }
        Map<Long, Collection<Channel>> mutableMap = h0.toMutableMap(map);
        Long valueOf = Long.valueOf(channel.r());
        List mutableListOf = n.mutableListOf(channel);
        mutableListOf.addAll(collection);
        mutableMap.put(valueOf, mutableListOf);
        return mutableMap;
    }

    private final Map<Long, Collection<Channel>> sortThreadsByJoinedDate(Map<Long, ? extends Map<Long, StoreThreadsActiveJoined.ActiveJoinedThread>> map) {
        LinkedHashMap linkedHashMap = new LinkedHashMap(g0.mapCapacity(map.size()));
        Iterator<T> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            Object key = entry.getKey();
            SortedSet<StoreThreadsActiveJoined.ActiveJoinedThread> sortedSet = t.toSortedSet(((Map) entry.getValue()).values(), INSTANCE.getThreadTimestampComparator());
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(sortedSet, 10));
            for (StoreThreadsActiveJoined.ActiveJoinedThread activeJoinedThread : sortedSet) {
                arrayList.add(activeJoinedThread.getChannel());
            }
            linkedHashMap.put(key, arrayList);
        }
        return linkedHashMap;
    }

    public final String autoArchiveDurationName(Context context, int i) {
        m.checkNotNullParameter(context, "context");
        Integer num = THREAD_AUTO_ARCHIVE_DURATION_NAMES.get(Integer.valueOf(i));
        if (num != null) {
            m.checkNotNullExpressionValue(num, "it");
            String string = context.getString(num.intValue());
            if (string != null) {
                return string;
            }
        }
        String quantityString = context.getResources().getQuantityString(R.plurals.duration_hours_hours, i / 60);
        m.checkNotNullExpressionValue(quantityString, "context.resources.getQua…ours_hours, minutes / 60)");
        return quantityString;
    }

    public final boolean canCreatePrivateThread(Long l, Channel channel, Guild guild, boolean z2) {
        if (guild == null || channel == null) {
            return false;
        }
        boolean hasAccessWrite = PermissionUtils.INSTANCE.hasAccessWrite(channel, l);
        boolean can = PermissionUtils.can(Permission.CREATE_PRIVATE_THREADS, l);
        if (!NewThreadsPermissionsFeatureFlag.Companion.getINSTANCE().isEnabled(guild.getId())) {
            can = hasAccessWrite && can;
        }
        return isThreadsEnabled(guild.getId()) && can && (!ChannelUtils.C(channel) && !ChannelUtils.E(channel)) && (!z2 || guild.hasFeature(GuildFeature.PRIVATE_THREADS)) && !ChannelUtils.i(channel);
    }

    public final boolean canCreatePublicThread(Long l, Channel channel, Message message, Guild guild) {
        if (guild == null || channel == null) {
            return false;
        }
        boolean hasAccessWrite = PermissionUtils.INSTANCE.hasAccessWrite(channel, l);
        boolean can = PermissionUtils.can(Permission.CREATE_PUBLIC_THREADS, l);
        boolean z2 = message == null || PermissionUtils.can(Permission.READ_MESSAGE_HISTORY, l);
        return isThreadsEnabled(guild.getId()) && (!NewThreadsPermissionsFeatureFlag.Companion.getINSTANCE().isEnabled(guild.getId()) ? !(!hasAccessWrite || !can || !z2) : !(!can || !z2)) && ((message == null || !message.isSystemMessage()) && !ChannelUtils.C(channel) && ((message == null || !message.hasThread()) && !ChannelUtils.E(channel)));
    }

    public final boolean canCreateThread(Long l, Channel channel, Message message, Guild guild) {
        return canCreatePublicThread(l, channel, message, guild) || canCreatePrivateThread$default(this, l, channel, guild, false, 8, null);
    }

    public final boolean canManageThread(User user, Channel channel, Long l) {
        m.checkNotNullParameter(user, "user");
        m.checkNotNullParameter(channel, "channel");
        if (channel.q() != user.getId() || !PermissionUtils.INSTANCE.hasAccessWrite(channel, l)) {
            return PermissionUtils.can(Permission.MANAGE_THREADS, l) && PermissionUtils.INSTANCE.hasAccess(channel, l);
        }
        return true;
    }

    public final boolean canUnarchiveThread(Channel channel, Long l) {
        m.checkNotNullParameter(channel, "channel");
        ThreadMetadata y2 = channel.y();
        if (m.areEqual(y2 != null ? Boolean.valueOf(y2.d()) : null, Boolean.TRUE)) {
            return PermissionUtils.can(Permission.MANAGE_THREADS, l) && PermissionUtils.INSTANCE.hasAccessWrite(channel, l);
        }
        return PermissionUtils.INSTANCE.hasAccessWrite(channel, l);
    }

    public final boolean canViewAllPrivateThreads(Long l) {
        return PermissionUtils.can(Permission.MANAGE_THREADS, l) && PermissionUtils.can(Permission.READ_MESSAGE_HISTORY, l);
    }

    public final boolean canViewAllPublicArchivedThreads(Channel channel, Long l) {
        return channel != null && PermissionUtils.INSTANCE.hasAccess(channel, l) && PermissionUtils.can(Permission.READ_MESSAGE_HISTORY, l);
    }

    public final long computeThreadAutoArchiveTimeMs(Channel channel) {
        String a;
        m.checkNotNullParameter(channel, "channel");
        long i = (channel.i() >>> 22) + SnowflakeUtils.DISCORD_EPOCH;
        ThreadMetadata y2 = channel.y();
        int c = (y2 != null ? y2.c() : 0) * 60 * 1000;
        ThreadMetadata y3 = channel.y();
        return Math.max(i, (y3 == null || (a = y3.a()) == null) ? 0L : TimeUtils.parseUTCDate(a)) + c;
    }

    public final int computeThreadNotificationSetting(StoreThreadsJoined.JoinedThread joinedThread, boolean z2, Integer num) {
        Integer valueOf = joinedThread != null ? Integer.valueOf(joinedThread.getFlags()) : null;
        if (valueOf != null) {
            if ((valueOf.intValue() & 2) == 0) {
                if ((valueOf.intValue() & 4) != 0) {
                    return 4;
                }
                if ((valueOf.intValue() & 8) == 0 && !z2) {
                    int i = ModelNotificationSettings.FREQUENCY_MENTIONS;
                    if (num != null && num.intValue() == i) {
                        return 4;
                    }
                    int i2 = ModelNotificationSettings.FREQUENCY_NOTHING;
                    if (num != null && num.intValue() == i2) {
                    }
                }
            }
            return 2;
        }
        return 8;
    }

    public final Observable<Channel> createThread(long j, Long l, int i, String str, Integer num, String str2) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        RestAPIParams.ThreadCreationSettings threadCreationSettings = new RestAPIParams.ThreadCreationSettings(str, i, num);
        if (l == null) {
            return RestAPI.Companion.getApi().createThread(j, str2, threadCreationSettings);
        }
        return RestAPI.Companion.getApi().createThreadFromMessage(j, l.longValue(), str2, threadCreationSettings);
    }

    public final LinkedHashMap<Integer, Integer> getTHREAD_AUTO_ARCHIVE_DURATION_NAMES() {
        return THREAD_AUTO_ARCHIVE_DURATION_NAMES;
    }

    public final long getThreadAckMessageTimestamp(Channel channel, Long l, Long l2, Clock clock) {
        String a;
        m.checkNotNullParameter(channel, "$this$getThreadAckMessageTimestamp");
        m.checkNotNullParameter(clock, "clock");
        long longValue = l != null ? l.longValue() : clock.currentTimeMillis();
        long longValue2 = l2 != null ? l2.longValue() - 5000 : 0L;
        ThreadMetadata y2 = channel.y();
        long max = Math.max(longValue2, (y2 == null || (a = y2.a()) == null) ? 0L : TimeUtils.parseUTCDate(a));
        if (max == 0) {
            max = TimeUtils.parseSnowflake(Long.valueOf(channel.h()));
        }
        return Math.max(longValue, max);
    }

    public final boolean isThreadModerator(Long l) {
        return PermissionUtils.can(Permission.MANAGE_THREADS, l);
    }

    public final boolean isThreadsEnabled(long j) {
        return ViewThreadsFeatureFlag.Companion.getINSTANCE().isEnabled() && CreateThreadsFeatureFlag.Companion.getINSTANCE().isEnabled(j);
    }

    public final Map<Long, Collection<Channel>> sortThreadsForChannelList(Map<Long, ? extends Map<Long, StoreThreadsActiveJoined.ActiveJoinedThread>> map, Channel channel) {
        m.checkNotNullParameter(map, "threadsMap");
        if (!ViewThreadsFeatureFlag.Companion.getINSTANCE().isEnabled()) {
            return h0.emptyMap();
        }
        Map<Long, Collection<Channel>> sortThreadsByJoinedDate = sortThreadsByJoinedDate(map);
        return (channel == null || !ChannelUtils.C(channel)) ? sortThreadsByJoinedDate : prependSelectedThread(sortThreadsByJoinedDate, channel);
    }
}
