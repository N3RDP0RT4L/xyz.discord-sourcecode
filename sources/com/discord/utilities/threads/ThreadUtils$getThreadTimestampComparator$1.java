package com.discord.utilities.threads;

import com.discord.stores.StoreThreadsActiveJoined;
import java.util.Comparator;
import kotlin.Metadata;
/* compiled from: ThreadUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0003\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/stores/StoreThreadsActiveJoined$ActiveJoinedThread;", "kotlin.jvm.PlatformType", "o1", "o2", "", "compare", "(Lcom/discord/stores/StoreThreadsActiveJoined$ActiveJoinedThread;Lcom/discord/stores/StoreThreadsActiveJoined$ActiveJoinedThread;)I", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ThreadUtils$getThreadTimestampComparator$1<T> implements Comparator<StoreThreadsActiveJoined.ActiveJoinedThread> {
    public static final ThreadUtils$getThreadTimestampComparator$1 INSTANCE = new ThreadUtils$getThreadTimestampComparator$1();

    public final int compare(StoreThreadsActiveJoined.ActiveJoinedThread activeJoinedThread, StoreThreadsActiveJoined.ActiveJoinedThread activeJoinedThread2) {
        return (activeJoinedThread2.getJoinTimestamp().g() > activeJoinedThread.getJoinTimestamp().g() ? 1 : (activeJoinedThread2.getJoinTimestamp().g() == activeJoinedThread.getJoinTimestamp().g() ? 0 : -1));
    }
}
