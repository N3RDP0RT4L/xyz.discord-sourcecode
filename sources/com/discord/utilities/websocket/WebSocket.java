package com.discord.utilities.websocket;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import androidx.core.view.PointerIconCompat;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import f0.e0.k.h;
import f0.x;
import j0.p.a;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import javax.net.ssl.SSLSocketFactory;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.DefaultConstructorMarker;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocketListener;
import rx.Observable;
import rx.Scheduler;
import rx.functions.Action1;
import rx.subjects.PublishSubject;
/* compiled from: WebSocket.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000¬\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0003\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 Y2\u00020\u0001:\u0005ZY[\\]BQ\u0012\b\u0010V\u001a\u0004\u0018\u00010U\u00124\u00104\u001a0\u0012\u0004\u0012\u00020\u000b\u0012\b\u0012\u000600j\u0002`1\u0012\u0012\u0012\u0010\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000b\u0018\u000102\u0012\u0004\u0012\u00020\b0/j\u0002`3\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\r¢\u0006\u0004\bW\u0010XJ)\u0010\t\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\t\u0010\nJ)\u0010\u0012\u001a\u00020\u00112\u0006\u0010\f\u001a\u00020\u000b2\b\u0010\u000e\u001a\u0004\u0018\u00010\r2\u0006\u0010\u0010\u001a\u00020\u000fH\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u001d\u0010\u0016\u001a\u00020\b2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\b0\u0014H\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0015\u0010\u0018\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\u0018\u0010\u0019J%\u0010\u001d\u001a\u00020\b2\b\b\u0002\u0010\u001b\u001a\u00020\u001a2\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u000bH\u0007¢\u0006\u0004\b\u001d\u0010\u001eJ\r\u0010\u001f\u001a\u00020\b¢\u0006\u0004\b\u001f\u0010 J\u0015\u0010!\u001a\u00020\b2\u0006\u0010!\u001a\u00020\u000b¢\u0006\u0004\b!\u0010\u0019R.\u0010$\u001a\u000e\u0012\u0004\u0012\u00020#\u0012\u0004\u0012\u00020\b0\"8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'\"\u0004\b(\u0010)RR\u0010,\u001a>\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\b +*\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u00140\u0014 +*\u001e\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\b +*\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u00140\u0014\u0018\u00010*0*8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b,\u0010-R\u0018\u0010\u000e\u001a\u0004\u0018\u00010\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010.RD\u00104\u001a0\u0012\u0004\u0012\u00020\u000b\u0012\b\u0012\u000600j\u0002`1\u0012\u0012\u0012\u0010\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000b\u0018\u000102\u0012\u0004\u0012\u00020\b0/j\u0002`38\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b4\u00105R$\u00108\u001a\u0002062\u0006\u00107\u001a\u0002068\u0006@BX\u0086\u000e¢\u0006\f\n\u0004\b8\u00109\u001a\u0004\b:\u0010;R\u0018\u0010<\u001a\u0004\u0018\u00010\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b<\u0010=R.\u0010?\u001a\u000e\u0012\u0004\u0012\u00020>\u0012\u0004\u0012\u00020\b0\"8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b?\u0010%\u001a\u0004\b@\u0010'\"\u0004\bA\u0010)R4\u0010D\u001a\u0014\u0012\u0004\u0012\u00020C\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020\b0B8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\bD\u0010E\u001a\u0004\bF\u0010G\"\u0004\bH\u0010IR.\u0010K\u001a\u000e\u0012\u0004\u0012\u00020J\u0012\u0004\u0012\u00020\b0\"8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\bK\u0010%\u001a\u0004\bL\u0010'\"\u0004\bM\u0010)R$\u0010O\u001a\u0004\u0018\u00010N8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\bO\u0010P\u001a\u0004\bQ\u0010R\"\u0004\bS\u0010T¨\u0006^"}, d2 = {"Lcom/discord/utilities/websocket/WebSocket;", "", "", "throwable", "Lokhttp3/Response;", "response", "", "closed", "", "handleOnFailure", "(Ljava/lang/Throwable;Lokhttp3/Response;Z)V", "", "url", "Ljavax/net/ssl/SSLSocketFactory;", "socketFactory", "Lokhttp3/WebSocketListener;", "listener", "Lokhttp3/WebSocket;", "createWebSocket", "(Ljava/lang/String;Ljavax/net/ssl/SSLSocketFactory;Lokhttp3/WebSocketListener;)Lokhttp3/WebSocket;", "Lkotlin/Function0;", "callback", "schedule", "(Lkotlin/jvm/functions/Function0;)V", "connect", "(Ljava/lang/String;)V", "", ModelAuditLogEntry.CHANGE_KEY_CODE, ModelAuditLogEntry.CHANGE_KEY_REASON, "disconnect", "(ILjava/lang/String;)V", "resetListeners", "()V", "message", "Lkotlin/Function1;", "Lcom/discord/utilities/websocket/WebSocket$Closed;", "onClosed", "Lkotlin/jvm/functions/Function1;", "getOnClosed", "()Lkotlin/jvm/functions/Function1;", "setOnClosed", "(Lkotlin/jvm/functions/Function1;)V", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "schedulerSubject", "Lrx/subjects/PublishSubject;", "Ljavax/net/ssl/SSLSocketFactory;", "Lkotlin/Function3;", "Ljava/lang/Exception;", "Lkotlin/Exception;", "", "Lcom/discord/utilities/websocket/ErrorLogger;", "errorLogger", "Lkotlin/jvm/functions/Function3;", "Lcom/discord/utilities/websocket/WebSocket$State;", "<set-?>", "state", "Lcom/discord/utilities/websocket/WebSocket$State;", "getState", "()Lcom/discord/utilities/websocket/WebSocket$State;", "client", "Lokhttp3/WebSocket;", "Lcom/discord/utilities/websocket/WebSocket$Error;", "onError", "getOnError", "setOnError", "Lkotlin/Function2;", "Ljava/io/InputStreamReader;", "onMessage", "Lkotlin/jvm/functions/Function2;", "getOnMessage", "()Lkotlin/jvm/functions/Function2;", "setOnMessage", "(Lkotlin/jvm/functions/Function2;)V", "Lcom/discord/utilities/websocket/WebSocket$Opened;", "onOpened", "getOnOpened", "setOnOpened", "Lcom/discord/utilities/websocket/RawMessageHandler;", "rawMessageHandler", "Lcom/discord/utilities/websocket/RawMessageHandler;", "getRawMessageHandler", "()Lcom/discord/utilities/websocket/RawMessageHandler;", "setRawMessageHandler", "(Lcom/discord/utilities/websocket/RawMessageHandler;)V", "Lrx/Scheduler;", "scheduler", HookHelper.constructorName, "(Lrx/Scheduler;Lkotlin/jvm/functions/Function3;Ljavax/net/ssl/SSLSocketFactory;)V", "Companion", "Closed", "Error", "Opened", "State", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WebSocket {
    private static final int CLOSE_NORMAL = 1000;
    public static final Companion Companion = new Companion(null);
    private okhttp3.WebSocket client;
    private final Function3<String, Exception, Map<String, String>, Unit> errorLogger;
    private RawMessageHandler rawMessageHandler;
    private final PublishSubject<Function0<Unit>> schedulerSubject;
    private final SSLSocketFactory socketFactory;
    private Function1<? super Opened, Unit> onOpened = WebSocket$onOpened$1.INSTANCE;
    private Function1<? super Closed, Unit> onClosed = WebSocket$onClosed$1.INSTANCE;
    private Function2<? super InputStreamReader, ? super Integer, Unit> onMessage = WebSocket$onMessage$1.INSTANCE;
    private Function1<? super Error, Unit> onError = WebSocket$onError$1.INSTANCE;
    private State state = State.NOT_YET_CONNECTED;

    /* compiled from: WebSocket.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\u0010\u0006\u001a\u00020\u00012\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lkotlin/Function0;", "", "kotlin.jvm.PlatformType", "it", NotificationCompat.CATEGORY_CALL, "(Lkotlin/jvm/functions/Function0;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.utilities.websocket.WebSocket$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1<T> implements Action1<Function0<? extends Unit>> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        @Override // rx.functions.Action1
        public /* bridge */ /* synthetic */ void call(Function0<? extends Unit> function0) {
            call2((Function0<Unit>) function0);
        }

        /* renamed from: call  reason: avoid collision after fix types in other method */
        public final void call2(Function0<Unit> function0) {
            function0.invoke();
        }
    }

    /* compiled from: WebSocket.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0007¢\u0006\u0004\b\f\u0010\rR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u001b\u0010\b\u001a\u0004\u0018\u00010\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/utilities/websocket/WebSocket$Closed;", "", "", ModelAuditLogEntry.CHANGE_KEY_CODE, "I", "getCode", "()I", "", ModelAuditLogEntry.CHANGE_KEY_REASON, "Ljava/lang/String;", "getReason", "()Ljava/lang/String;", HookHelper.constructorName, "(ILjava/lang/String;)V", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Closed {
        private final int code;
        private final String reason;

        public Closed(int i, String str) {
            this.code = i;
            this.reason = str;
        }

        public final int getCode() {
            return this.code;
        }

        public final String getReason() {
            return this.reason;
        }
    }

    /* compiled from: WebSocket.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/utilities/websocket/WebSocket$Companion;", "", "", "CLOSE_NORMAL", "I", HookHelper.constructorName, "()V", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WebSocket.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0003\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0019\u0010\b\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/utilities/websocket/WebSocket$Error;", "", "Lokhttp3/Response;", "response", "Lokhttp3/Response;", "getResponse", "()Lokhttp3/Response;", "", "throwable", "Ljava/lang/Throwable;", "getThrowable", "()Ljava/lang/Throwable;", HookHelper.constructorName, "(Ljava/lang/Throwable;Lokhttp3/Response;)V", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Error {
        private final Response response;
        private final Throwable throwable;

        public Error(Throwable th, Response response) {
            m.checkNotNullParameter(th, "throwable");
            this.throwable = th;
            this.response = response;
        }

        public final Response getResponse() {
            return this.response;
        }

        public final Throwable getThrowable() {
            return this.throwable;
        }
    }

    /* compiled from: WebSocket.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0011\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0007\u0010\bR\u001b\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/utilities/websocket/WebSocket$Opened;", "", "Lokhttp3/Response;", "response", "Lokhttp3/Response;", "getResponse", "()Lokhttp3/Response;", HookHelper.constructorName, "(Lokhttp3/Response;)V", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Opened {
        private final Response response;

        public Opened(Response response) {
            this.response = response;
        }

        public final Response getResponse() {
            return this.response;
        }
    }

    /* compiled from: WebSocket.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\b\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\t"}, d2 = {"Lcom/discord/utilities/websocket/WebSocket$State;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "NOT_YET_CONNECTED", "CONNECTING", "CONNECTED", "CLOSING", "CLOSED", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum State {
        NOT_YET_CONNECTED,
        CONNECTING,
        CONNECTED,
        CLOSING,
        CLOSED
    }

    /* JADX WARN: Multi-variable type inference failed */
    public WebSocket(Scheduler scheduler, Function3<? super String, ? super Exception, ? super Map<String, String>, Unit> function3, SSLSocketFactory sSLSocketFactory) {
        m.checkNotNullParameter(function3, "errorLogger");
        this.errorLogger = function3;
        this.socketFactory = sSLSocketFactory;
        PublishSubject<Function0<Unit>> k0 = PublishSubject.k0();
        this.schedulerSubject = k0;
        Observable<Function0<Unit>> J = k0.J();
        if (scheduler == null) {
            AtomicReference<a> atomicReference = a.a;
            scheduler = j0.l.c.m.a;
        }
        J.I(scheduler).W(AnonymousClass1.INSTANCE, new Action1<Throwable>() { // from class: com.discord.utilities.websocket.WebSocket.2
            public final void call(Throwable th) {
                WebSocket webSocket = WebSocket.this;
                m.checkNotNullExpressionValue(th, "it");
                webSocket.handleOnFailure(th, null, WebSocket.this.getState() == State.CLOSED);
            }
        });
    }

    private final okhttp3.WebSocket createWebSocket(String str, SSLSocketFactory sSLSocketFactory, WebSocketListener webSocketListener) {
        x.a aVar = new x.a();
        if (sSLSocketFactory != null) {
            h.a aVar2 = h.c;
            aVar.b(sSLSocketFactory, h.a.n());
        }
        aVar.a(1L, TimeUnit.MINUTES);
        x xVar = new x(aVar);
        Request.a aVar3 = new Request.a();
        aVar3.f(str);
        return xVar.g(aVar3.a(), webSocketListener);
    }

    public static /* synthetic */ void disconnect$default(WebSocket webSocket, int i, String str, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = 1000;
        }
        if ((i2 & 2) != 0) {
            str = null;
        }
        webSocket.disconnect(i, str);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleOnFailure(Throwable th, Response response, boolean z2) {
        if (!z2) {
            disconnect(PointerIconCompat.TYPE_COPY, "Closing due to failure " + th + ", " + response);
        } else {
            this.state = State.CLOSED;
        }
        this.onError.invoke(new Error(th, response));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void schedule(Function0<Unit> function0) {
        this.schedulerSubject.k.onNext(function0);
    }

    public final void connect(String str) {
        m.checkNotNullParameter(str, "url");
        disconnect(1000, "Closing existing connection.");
        this.state = State.CONNECTING;
        this.client = createWebSocket(str, this.socketFactory, new ZLibWebSocketListener(new WebSocket$connect$1(this)));
    }

    public final void disconnect() {
        disconnect$default(this, 0, null, 3, null);
    }

    public final void disconnect(int i) {
        disconnect$default(this, i, null, 2, null);
    }

    public final void disconnect(int i, String str) {
        okhttp3.WebSocket webSocket = this.client;
        if (webSocket != null) {
            this.state = State.CLOSING;
            try {
                try {
                    webSocket.e(i, str);
                } catch (Exception e) {
                    handleOnFailure(e, null, true);
                }
            } finally {
                this.client = null;
            }
        }
    }

    public final Function1<Closed, Unit> getOnClosed() {
        return this.onClosed;
    }

    public final Function1<Error, Unit> getOnError() {
        return this.onError;
    }

    public final Function2<InputStreamReader, Integer, Unit> getOnMessage() {
        return this.onMessage;
    }

    public final Function1<Opened, Unit> getOnOpened() {
        return this.onOpened;
    }

    public final RawMessageHandler getRawMessageHandler() {
        return this.rawMessageHandler;
    }

    public final State getState() {
        return this.state;
    }

    public final void message(String str) {
        m.checkNotNullParameter(str, "message");
        okhttp3.WebSocket webSocket = this.client;
        if (webSocket != null && this.state == State.CONNECTED) {
            try {
                webSocket.a(str);
            } catch (IllegalStateException e) {
                handleOnFailure(e, null, true);
            }
        }
    }

    public final void resetListeners() {
        this.onOpened = WebSocket$resetListeners$1.INSTANCE;
        this.onClosed = WebSocket$resetListeners$2.INSTANCE;
        this.onMessage = WebSocket$resetListeners$3.INSTANCE;
        this.rawMessageHandler = null;
        this.onError = WebSocket$resetListeners$4.INSTANCE;
    }

    public final void setOnClosed(Function1<? super Closed, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.onClosed = function1;
    }

    public final void setOnError(Function1<? super Error, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.onError = function1;
    }

    public final void setOnMessage(Function2<? super InputStreamReader, ? super Integer, Unit> function2) {
        m.checkNotNullParameter(function2, "<set-?>");
        this.onMessage = function2;
    }

    public final void setOnOpened(Function1<? super Opened, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.onOpened = function1;
    }

    public final void setRawMessageHandler(RawMessageHandler rawMessageHandler) {
        this.rawMessageHandler = rawMessageHandler;
    }
}
