package com.discord.utilities.websocket;

import com.discord.utilities.websocket.WebSocket;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import okhttp3.Response;
import okhttp3.WebSocket;
/* compiled from: WebSocket.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WebSocket$connect$1$onOpen$1 extends o implements Function0<Unit> {
    public final /* synthetic */ Response $response;
    public final /* synthetic */ WebSocket $webSocket;
    public final /* synthetic */ WebSocket$connect$1 this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WebSocket$connect$1$onOpen$1(WebSocket$connect$1 webSocket$connect$1, WebSocket webSocket, Response response) {
        super(0);
        this.this$0 = webSocket$connect$1;
        this.$webSocket = webSocket;
        this.$response = response;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        this.this$0.this$0.client = this.$webSocket;
        this.this$0.this$0.state = WebSocket.State.CONNECTED;
        this.this$0.this$0.getOnOpened().invoke(new WebSocket.Opened(this.$response));
    }
}
