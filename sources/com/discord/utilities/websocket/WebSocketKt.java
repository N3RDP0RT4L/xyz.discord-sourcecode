package com.discord.utilities.websocket;

import kotlin.Metadata;
/* compiled from: WebSocket.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002*\\\b\u0002\u0010\u0006\"(\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u0002`\u0002\u0012\u0012\u0012\u0010\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u0003\u0012\u0004\u0012\u00020\u00040\u00002,\u0012\u0004\u0012\u00020\u0001\u0012\b\u0012\u00060\u0005j\u0002`\u0002\u0012\u0012\u0012\u0010\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u0003\u0012\u0004\u0012\u00020\u00040\u0000¨\u0006\u0007"}, d2 = {"Lkotlin/Function3;", "", "Lkotlin/Exception;", "", "", "Ljava/lang/Exception;", "ErrorLogger", "utils_release"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WebSocketKt {
}
