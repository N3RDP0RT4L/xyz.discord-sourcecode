package com.discord.utilities.websocket;

import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.websocket.WebSocket;
import com.discord.utilities.websocket.ZLibWebSocketListener;
import d0.o;
import d0.t.g0;
import d0.t.h0;
import d0.z.d.m;
import java.io.InputStreamReader;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function3;
import okhttp3.Response;
import okhttp3.WebSocket;
/* compiled from: WebSocket.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000O\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0003\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J/\u0010\t\u001a\u00020\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u00052\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0005H\u0002¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\u000f\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\rH\u0016¢\u0006\u0004\b\u000f\u0010\u0010J)\u0010\u0013\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\u0012\u001a\u00020\u00112\b\u0010\u000e\u001a\u0004\u0018\u00010\rH\u0016¢\u0006\u0004\b\u0013\u0010\u0014J'\u0010\u0019\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0018\u001a\u00020\u0017H\u0016¢\u0006\u0004\b\u0019\u0010\u001aJ'\u0010\u001d\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\u001b\u001a\u00020\u00172\u0006\u0010\u001c\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u001d\u0010\u001eJ'\u0010\u001f\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\u001b\u001a\u00020\u00172\u0006\u0010\u001c\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u001f\u0010\u001eJ\u001b\u0010!\u001a\u00020\b2\n\u0010 \u001a\u00060\u0002j\u0002`\u0003H\u0016¢\u0006\u0004\b!\u0010\"R\u001e\u0010$\u001a\u0004\u0018\u00010#8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'¨\u0006("}, d2 = {"com/discord/utilities/websocket/WebSocket$connect$1", "Lcom/discord/utilities/websocket/ZLibWebSocketListener$Listener;", "Ljava/lang/Exception;", "Lkotlin/Exception;", "exception", "", "errorMessage", "rawData", "", "handleError", "(Ljava/lang/Exception;Ljava/lang/String;Ljava/lang/String;)V", "Lokhttp3/WebSocket;", "webSocket", "Lokhttp3/Response;", "response", "onOpen", "(Lokhttp3/WebSocket;Lokhttp3/Response;)V", "", "t", "onFailure", "(Lokhttp3/WebSocket;Ljava/lang/Throwable;Lokhttp3/Response;)V", "Ljava/io/InputStreamReader;", "reader", "", "compressedByteLength", "onInflatedMessage", "(Lokhttp3/WebSocket;Ljava/io/InputStreamReader;I)V", ModelAuditLogEntry.CHANGE_KEY_CODE, ModelAuditLogEntry.CHANGE_KEY_REASON, "onClosing", "(Lokhttp3/WebSocket;ILjava/lang/String;)V", "onClosed", "e", "onInflateError", "(Ljava/lang/Exception;)V", "Lcom/discord/utilities/websocket/RawMessageHandler;", "rawMessageHandler", "Lcom/discord/utilities/websocket/RawMessageHandler;", "getRawMessageHandler", "()Lcom/discord/utilities/websocket/RawMessageHandler;", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WebSocket$connect$1 extends ZLibWebSocketListener.Listener {
    private final RawMessageHandler rawMessageHandler;
    public final /* synthetic */ WebSocket this$0;

    public WebSocket$connect$1(WebSocket webSocket) {
        this.this$0 = webSocket;
        this.rawMessageHandler = webSocket.getRawMessageHandler();
    }

    private final void handleError(Exception exc, String str, String str2) {
        Map map;
        Function3 function3;
        if (str2 == null || (map = g0.mapOf(o.to("raw_data_string", str2))) == null) {
            map = h0.emptyMap();
        }
        function3 = this.this$0.errorLogger;
        function3.invoke(str, exc, map);
        this.this$0.handleOnFailure(exc, null, false);
    }

    public static /* synthetic */ void handleError$default(WebSocket$connect$1 webSocket$connect$1, Exception exc, String str, String str2, int i, Object obj) {
        if ((i & 4) != 0) {
            str2 = null;
        }
        webSocket$connect$1.handleError(exc, str, str2);
    }

    @Override // com.discord.utilities.websocket.ZLibWebSocketListener.Listener
    public RawMessageHandler getRawMessageHandler() {
        return this.rawMessageHandler;
    }

    @Override // okhttp3.WebSocketListener
    public void onClosed(WebSocket webSocket, int i, String str) {
        m.checkNotNullParameter(webSocket, "webSocket");
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_REASON);
        this.this$0.schedule(new WebSocket$connect$1$onClosed$1(this, i, str));
    }

    @Override // okhttp3.WebSocketListener
    public void onClosing(WebSocket webSocket, int i, String str) {
        m.checkNotNullParameter(webSocket, "webSocket");
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_REASON);
        this.this$0.state = WebSocket.State.CLOSING;
        this.this$0.getOnClosed().invoke(new WebSocket.Closed(i, str));
    }

    @Override // okhttp3.WebSocketListener
    public void onFailure(okhttp3.WebSocket webSocket, Throwable th, Response response) {
        m.checkNotNullParameter(webSocket, "webSocket");
        m.checkNotNullParameter(th, "t");
        this.this$0.schedule(new WebSocket$connect$1$onFailure$1(this, th, response));
    }

    @Override // com.discord.utilities.websocket.ZLibWebSocketListener.Listener
    public void onInflateError(Exception exc) {
        m.checkNotNullParameter(exc, "e");
        handleError$default(this, exc, "Unable to inflate message.", null, 4, null);
    }

    @Override // com.discord.utilities.websocket.ZLibWebSocketListener.Listener
    public void onInflatedMessage(okhttp3.WebSocket webSocket, InputStreamReader inputStreamReader, int i) {
        m.checkNotNullParameter(webSocket, "webSocket");
        m.checkNotNullParameter(inputStreamReader, "reader");
        try {
            this.this$0.getOnMessage().invoke(inputStreamReader, Integer.valueOf(i));
        } catch (Exception e) {
            handleError$default(this, e, "Unable to parse model.", null, 4, null);
        }
    }

    @Override // okhttp3.WebSocketListener
    public void onOpen(okhttp3.WebSocket webSocket, Response response) {
        m.checkNotNullParameter(webSocket, "webSocket");
        m.checkNotNullParameter(response, "response");
        this.this$0.schedule(new WebSocket$connect$1$onOpen$1(this, webSocket, response));
    }
}
