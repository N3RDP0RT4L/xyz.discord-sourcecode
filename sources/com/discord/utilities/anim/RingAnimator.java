package com.discord.utilities.anim;

import andhook.lib.HookHelper;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: RingAnimator.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e¢\u0006\u0004\b\u0014\u0010\u0015J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004R\u0019\u0010\u0006\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0007\u001a\u0004\b\b\u0010\tR\u001e\u0010\f\u001a\n \u000b*\u0004\u0018\u00010\n0\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\rR\u001c\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0012\u001a\u00020\u000f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013¨\u0006\u0016"}, d2 = {"Lcom/discord/utilities/anim/RingAnimator;", "", "", "onUpdate", "()V", "Landroid/view/View;", "view", "Landroid/view/View;", "getView", "()Landroid/view/View;", "Landroid/animation/ObjectAnimator;", "kotlin.jvm.PlatformType", "fadeAnim", "Landroid/animation/ObjectAnimator;", "Lkotlin/Function0;", "", "ringingPredicate", "Lkotlin/jvm/functions/Function0;", "isAnimating", "Z", HookHelper.constructorName, "(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class RingAnimator {
    private final ObjectAnimator fadeAnim;
    private boolean isAnimating;
    private final Function0<Boolean> ringingPredicate;
    private final View view;

    public RingAnimator(View view, Function0<Boolean> function0) {
        m.checkNotNullParameter(view, "view");
        m.checkNotNullParameter(function0, "ringingPredicate");
        this.view = view;
        this.ringingPredicate = function0;
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, View.ALPHA, 1.0f, 0.1f);
        ofFloat.setDuration(1000L);
        ofFloat.setRepeatMode(2);
        ofFloat.setRepeatCount(1);
        ofFloat.setInterpolator(new AccelerateDecelerateInterpolator());
        ofFloat.addListener(new Animator.AnimatorListener() { // from class: com.discord.utilities.anim.RingAnimator$$special$$inlined$apply$lambda$1
            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationCancel(Animator animator) {
                m.checkNotNullParameter(animator, "animator");
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationEnd(Animator animator) {
                m.checkNotNullParameter(animator, "animator");
                RingAnimator.this.isAnimating = false;
                RingAnimator.this.onUpdate();
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationRepeat(Animator animator) {
                m.checkNotNullParameter(animator, "animator");
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationStart(Animator animator) {
                m.checkNotNullParameter(animator, "animator");
            }
        });
        this.fadeAnim = ofFloat;
    }

    public final View getView() {
        return this.view;
    }

    public final void onUpdate() {
        boolean booleanValue = this.ringingPredicate.invoke().booleanValue();
        boolean z2 = this.isAnimating;
        if (!z2 && booleanValue) {
            this.isAnimating = true;
            this.fadeAnim.start();
        } else if (z2 && !booleanValue) {
            this.fadeAnim.cancel();
            ObjectAnimator objectAnimator = this.fadeAnim;
            m.checkNotNullExpressionValue(objectAnimator, "fadeAnim");
            objectAnimator.setDuration(0L);
        }
    }
}
