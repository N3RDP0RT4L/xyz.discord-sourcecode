package com.discord.utilities.premium;

import andhook.lib.HookHelper;
import andhook.lib.xposed.callbacks.XCallback;
import android.content.Context;
import android.view.View;
import androidx.annotation.DrawableRes;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.FragmentManager;
import b.a.k.b;
import b.d.b.a.a;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.SkuDetails;
import com.discord.api.guild.GuildFeature;
import com.discord.api.premium.PremiumTier;
import com.discord.app.AppLog;
import com.discord.models.domain.ModelGift;
import com.discord.models.domain.ModelSku;
import com.discord.models.domain.ModelStoreListing;
import com.discord.models.domain.ModelSubscription;
import com.discord.models.domain.premium.SubscriptionPlanType;
import com.discord.models.guild.Guild;
import com.discord.models.user.User;
import com.discord.stores.StoreGooglePlaySkuDetails;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.billing.GooglePlayBillingManager;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.gifting.GiftStyle;
import com.discord.utilities.gifting.GiftStyleKt;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.uri.UriHandler;
import d0.t.n0;
import d0.t.o0;
import d0.z.d.m;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: PremiumUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Â\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0007\n\u0002\b\u0005\n\u0002\u0010\"\n\u0002\b\u000b\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001mB\t\b\u0002¢\u0006\u0004\bk\u0010lJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0007¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\t\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\t\u0010\nJ\u0019\u0010\t\u001a\u00020\u00062\b\u0010\f\u001a\u0004\u0018\u00010\u000bH\u0007¢\u0006\u0004\b\t\u0010\rJ\u0015\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\u000f\u0010\u0010J\u0011\u0010\u0012\u001a\u00020\u0006*\u00020\u0011¢\u0006\u0004\b\u0012\u0010\u0013J\u0015\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0015\u0010\u0016J\u001f\u0010\u0019\u001a\u00020\u00142\u0006\u0010\u0005\u001a\u00020\u00042\b\u0010\u0018\u001a\u0004\u0018\u00010\u0017¢\u0006\u0004\b\u0019\u0010\u001aJ\u001d\u0010\u001e\u001a\u00020\u001d2\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u001c\u001a\u00020\u001b¢\u0006\u0004\b\u001e\u0010\u001fJ\u001b\u0010\"\u001a\u0004\u0018\u00010\u00172\n\u0010!\u001a\u00060\u0017j\u0002` ¢\u0006\u0004\b\"\u0010#J'\u0010'\u001a\u0004\u0018\u00010%2\u000e\u0010&\u001a\n\u0012\u0004\u0012\u00020%\u0018\u00010$2\u0006\u0010\u0018\u001a\u00020\u0017¢\u0006\u0004\b'\u0010(J%\u0010*\u001a\u0004\u0018\u00010\u00112\f\u0010)\u001a\b\u0012\u0004\u0012\u00020\u00110$2\u0006\u0010\u0018\u001a\u00020\u0017¢\u0006\u0004\b*\u0010+J\u0015\u0010.\u001a\u00020\u00062\u0006\u0010-\u001a\u00020,¢\u0006\u0004\b.\u0010/J\u001d\u00103\u001a\u00020\u000e2\u0006\u00101\u001a\u0002002\u0006\u00102\u001a\u000200¢\u0006\u0004\b3\u00104J\u0017\u00107\u001a\u00020\u00062\b\u00106\u001a\u0004\u0018\u000105¢\u0006\u0004\b7\u00108J\u0015\u0010:\u001a\u00020\u00062\u0006\u00109\u001a\u00020\u0006¢\u0006\u0004\b:\u0010;J\u0015\u0010>\u001a\u00020\u00062\u0006\u0010=\u001a\u00020<¢\u0006\u0004\b>\u0010?J\u001d\u0010>\u001a\u00020\u00062\u0006\u00109\u001a\u00020\u00062\u0006\u0010@\u001a\u00020\u000e¢\u0006\u0004\b>\u0010AJ\u001d\u0010C\u001a\u00020\u00062\u0006\u00109\u001a\u00020\u00062\u0006\u0010B\u001a\u00020\u000e¢\u0006\u0004\bC\u0010AJ\u0017\u0010F\u001a\u0004\u0018\u00010\u00062\u0006\u0010E\u001a\u00020D¢\u0006\u0004\bF\u0010GJ!\u0010H\u001a\u0004\u0018\u00010\u001d2\u0006\u0010\u0005\u001a\u00020\u00042\b\u00101\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\bH\u0010IJ\u001f\u0010K\u001a\u00020\u000e2\b\u0010=\u001a\u0004\u0018\u00010<2\u0006\u0010J\u001a\u00020D¢\u0006\u0004\bK\u0010LJc\u0010W\u001a\u00020V2\b\u0010=\u001a\u0004\u0018\u00010<2\u000e\u0010N\u001a\n\u0018\u00010,j\u0004\u0018\u0001`M2\u0006\u0010E\u001a\u00020D2\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010P\u001a\u00020O2\u0016\b\u0002\u0010S\u001a\u0010\u0012\u0004\u0012\u00020R\u0012\u0004\u0012\u00020\u0014\u0018\u00010Q2\n\b\u0002\u0010U\u001a\u0004\u0018\u00010T¢\u0006\u0004\bW\u0010XR\u0016\u0010Y\u001a\u00020\u00178\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\bY\u0010ZR\u0016\u0010[\u001a\u00020\u00068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b[\u0010\\R\u0016\u0010^\u001a\u00020]8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b^\u0010_R\u0016\u0010`\u001a\u00020\u00068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b`\u0010\\R\u0016\u0010a\u001a\u00020,8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\ba\u0010bR\u001c\u0010d\u001a\b\u0012\u0004\u0012\u00020D0c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bd\u0010eR\u0016\u0010f\u001a\u00020\u00068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bf\u0010\\R\u0016\u0010g\u001a\u00020]8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bg\u0010_R\u001c\u0010h\u001a\b\u0012\u0004\u0012\u00020D0c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bh\u0010eR\u0016\u0010i\u001a\u00020\u00178\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\bi\u0010ZR\u001c\u0010j\u001a\b\u0012\u0004\u0012\u00020D0c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bj\u0010e¨\u0006n"}, d2 = {"Lcom/discord/utilities/premium/PremiumUtils;", "", "Lcom/discord/models/domain/ModelGift;", "gift", "Landroid/content/Context;", "context", "", "getNitroGiftBackground", "(Lcom/discord/models/domain/ModelGift;Landroid/content/Context;)I", "getNitroGiftIcon", "(Lcom/discord/models/domain/ModelGift;)I", "Lcom/discord/models/domain/ModelSku;", "sku", "(Lcom/discord/models/domain/ModelSku;)I", "", "isNitroSku", "(Lcom/discord/models/domain/ModelSku;)Z", "Lcom/discord/models/domain/ModelSubscription;", "getGuildBoostCountFromSubscription", "(Lcom/discord/models/domain/ModelSubscription;)I", "", "openAppleBilling", "(Landroid/content/Context;)V", "", "skuName", "openGooglePlayBilling", "(Landroid/content/Context;Ljava/lang/String;)V", "Lcom/android/billingclient/api/SkuDetails;", "skuDetails", "", "getPlanPriceText", "(Landroid/content/Context;Lcom/android/billingclient/api/SkuDetails;)Ljava/lang/CharSequence;", "Lcom/discord/primitives/PaymentGatewaySkuId;", "paymentGatewaySkuId", "getSkuPrice", "(Ljava/lang/String;)Ljava/lang/String;", "", "Lcom/android/billingclient/api/Purchase;", "purchases", "findPurchaseForSkuName", "(Ljava/util/List;Ljava/lang/String;)Lcom/android/billingclient/api/Purchase;", "subscriptions", "findSubscriptionForSku", "(Ljava/util/List;Ljava/lang/String;)Lcom/discord/models/domain/ModelSubscription;", "", "amount", "microAmountToMinor", "(J)I", "Lcom/discord/api/premium/PremiumTier;", "premiumTier", "minimumLevel", "isPremiumTierAtLeast", "(Lcom/discord/api/premium/PremiumTier;Lcom/discord/api/premium/PremiumTier;)Z", "Lcom/discord/models/user/User;", "user", "getMaxFileSizeMB", "(Lcom/discord/models/user/User;)I", "guildPremiumTier", "getGuildMaxFileSizeMB", "(I)I", "Lcom/discord/models/guild/Guild;", "guild", "getGuildEmojiMaxCount", "(Lcom/discord/models/guild/Guild;)I", "hasMoreEmoji", "(IZ)I", "hasMoreSticker", "getGuildStickerMaxCount", "Lcom/discord/api/guild/GuildFeature;", "guildFeature", "getMinimumBoostTierForGuildFeature", "(Lcom/discord/api/guild/GuildFeature;)Ljava/lang/Integer;", "getBoostTierShortText", "(Landroid/content/Context;Ljava/lang/Integer;)Ljava/lang/CharSequence;", "feature", "doesGuildHaveEnoughBoostsForFeature", "(Lcom/discord/models/guild/Guild;Lcom/discord/api/guild/GuildFeature;)Z", "Lcom/discord/primitives/ChannelId;", "channelId", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Lkotlin/Function1;", "Landroid/view/View;", "onEnabledClickListener", "Lcom/discord/utilities/analytics/Traits$Location;", "locationTrait", "Lcom/discord/utilities/premium/PremiumUtils$BoostFeatureBadgeData;", "getBoostFeatureBadgeDataForGuildFeature", "(Lcom/discord/models/guild/Guild;Ljava/lang/Long;Lcom/discord/api/guild/GuildFeature;Landroid/content/Context;Landroidx/fragment/app/FragmentManager;Lkotlin/jvm/functions/Function1;Lcom/discord/utilities/analytics/Traits$Location;)Lcom/discord/utilities/premium/PremiumUtils$BoostFeatureBadgeData;", "YEARLY_ISO8601", "Ljava/lang/String;", "MAX_PREMIUM_GUILD_COUNT", "I", "", "GUILD_BOOST_FOR_PREMIUM_USER_DISCOUNT_PERCENT", "F", "MAX_NON_PREMIUM_GUILD_COUNT", "PREMIUM_APPLICATION_ID", "J", "", "tier3BoostFeatures", "Ljava/util/Set;", "NUM_FREE_GUILD_BOOSTS_WITH_PREMIUM", "PREMIUM_YEARLY_DISCOUNT_PERCENT", "tier1BoostFeatures", "MONTHLY_ISO8601", "tier2BoostFeatures", HookHelper.constructorName, "()V", "BoostFeatureBadgeData", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class PremiumUtils {
    public static final float GUILD_BOOST_FOR_PREMIUM_USER_DISCOUNT_PERCENT = 0.3f;
    public static final PremiumUtils INSTANCE = new PremiumUtils();
    public static final int MAX_NON_PREMIUM_GUILD_COUNT = 100;
    public static final int MAX_PREMIUM_GUILD_COUNT = 200;
    private static final String MONTHLY_ISO8601 = "P1M";
    public static final int NUM_FREE_GUILD_BOOSTS_WITH_PREMIUM = 2;
    public static final long PREMIUM_APPLICATION_ID = 521842831262875670L;
    public static final float PREMIUM_YEARLY_DISCOUNT_PERCENT = 0.16f;
    private static final String YEARLY_ISO8601 = "P1Y";
    private static final Set<GuildFeature> tier1BoostFeatures;
    private static final Set<GuildFeature> tier2BoostFeatures;
    private static final Set<GuildFeature> tier3BoostFeatures;

    /* compiled from: PremiumUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B5\u0012\u0014\u0010\u000e\u001a\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0002\u0012\u0006\u0010\u000f\u001a\u00020\u0007\u0012\u0006\u0010\u0010\u001a\u00020\n\u0012\u0006\u0010\u0011\u001a\u00020\u0007¢\u0006\u0004\b#\u0010$J\u001e\u0010\u0005\u001a\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u0010\u0010\b\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\r\u0010\tJF\u0010\u0012\u001a\u00020\u00002\u0016\b\u0002\u0010\u000e\u001a\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00022\b\b\u0002\u0010\u000f\u001a\u00020\u00072\b\b\u0002\u0010\u0010\u001a\u00020\n2\b\b\u0002\u0010\u0011\u001a\u00020\u0007HÆ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\u0017\u0010\tJ\u001a\u0010\u001a\u001a\u00020\u00192\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001a\u0010\u001bR\u0019\u0010\u0011\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001c\u001a\u0004\b\u001d\u0010\tR'\u0010\u000e\u001a\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001e\u001a\u0004\b\u001f\u0010\u0006R\u0019\u0010\u0010\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010 \u001a\u0004\b!\u0010\fR\u0019\u0010\u000f\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001c\u001a\u0004\b\"\u0010\t¨\u0006%"}, d2 = {"Lcom/discord/utilities/premium/PremiumUtils$BoostFeatureBadgeData;", "", "Lkotlin/Function1;", "Landroid/view/View;", "", "component1", "()Lkotlin/jvm/functions/Function1;", "", "component2", "()I", "", "component3", "()Ljava/lang/CharSequence;", "component4", "onClickListener", "iconColor", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "textColor", "copy", "(Lkotlin/jvm/functions/Function1;ILjava/lang/CharSequence;I)Lcom/discord/utilities/premium/PremiumUtils$BoostFeatureBadgeData;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getTextColor", "Lkotlin/jvm/functions/Function1;", "getOnClickListener", "Ljava/lang/CharSequence;", "getText", "getIconColor", HookHelper.constructorName, "(Lkotlin/jvm/functions/Function1;ILjava/lang/CharSequence;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class BoostFeatureBadgeData {
        private final int iconColor;
        private final Function1<View, Unit> onClickListener;
        private final CharSequence text;
        private final int textColor;

        /* JADX WARN: Multi-variable type inference failed */
        public BoostFeatureBadgeData(Function1<? super View, Unit> function1, int i, CharSequence charSequence, int i2) {
            m.checkNotNullParameter(charSequence, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
            this.onClickListener = function1;
            this.iconColor = i;
            this.text = charSequence;
            this.textColor = i2;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ BoostFeatureBadgeData copy$default(BoostFeatureBadgeData boostFeatureBadgeData, Function1 function1, int i, CharSequence charSequence, int i2, int i3, Object obj) {
            if ((i3 & 1) != 0) {
                function1 = boostFeatureBadgeData.onClickListener;
            }
            if ((i3 & 2) != 0) {
                i = boostFeatureBadgeData.iconColor;
            }
            if ((i3 & 4) != 0) {
                charSequence = boostFeatureBadgeData.text;
            }
            if ((i3 & 8) != 0) {
                i2 = boostFeatureBadgeData.textColor;
            }
            return boostFeatureBadgeData.copy(function1, i, charSequence, i2);
        }

        public final Function1<View, Unit> component1() {
            return this.onClickListener;
        }

        public final int component2() {
            return this.iconColor;
        }

        public final CharSequence component3() {
            return this.text;
        }

        public final int component4() {
            return this.textColor;
        }

        public final BoostFeatureBadgeData copy(Function1<? super View, Unit> function1, int i, CharSequence charSequence, int i2) {
            m.checkNotNullParameter(charSequence, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
            return new BoostFeatureBadgeData(function1, i, charSequence, i2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof BoostFeatureBadgeData)) {
                return false;
            }
            BoostFeatureBadgeData boostFeatureBadgeData = (BoostFeatureBadgeData) obj;
            return m.areEqual(this.onClickListener, boostFeatureBadgeData.onClickListener) && this.iconColor == boostFeatureBadgeData.iconColor && m.areEqual(this.text, boostFeatureBadgeData.text) && this.textColor == boostFeatureBadgeData.textColor;
        }

        public final int getIconColor() {
            return this.iconColor;
        }

        public final Function1<View, Unit> getOnClickListener() {
            return this.onClickListener;
        }

        public final CharSequence getText() {
            return this.text;
        }

        public final int getTextColor() {
            return this.textColor;
        }

        public int hashCode() {
            Function1<View, Unit> function1 = this.onClickListener;
            int i = 0;
            int hashCode = (((function1 != null ? function1.hashCode() : 0) * 31) + this.iconColor) * 31;
            CharSequence charSequence = this.text;
            if (charSequence != null) {
                i = charSequence.hashCode();
            }
            return ((hashCode + i) * 31) + this.textColor;
        }

        public String toString() {
            StringBuilder R = a.R("BoostFeatureBadgeData(onClickListener=");
            R.append(this.onClickListener);
            R.append(", iconColor=");
            R.append(this.iconColor);
            R.append(", text=");
            R.append(this.text);
            R.append(", textColor=");
            return a.A(R, this.textColor, ")");
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            ModelSku.SkuCategory.values();
            int[] iArr = new int[3];
            $EnumSwitchMapping$0 = iArr;
            iArr[ModelSku.SkuCategory.NITRO_CLASSIC.ordinal()] = 1;
            iArr[ModelSku.SkuCategory.NITRO.ordinal()] = 2;
        }
    }

    static {
        Set<GuildFeature> of = n0.setOf((Object[]) new GuildFeature[]{GuildFeature.INVITE_SPLASH, GuildFeature.ANIMATED_ICON, GuildFeature.THREE_DAY_THREAD_ARCHIVE});
        tier1BoostFeatures = of;
        Set<GuildFeature> plus = o0.plus((Set) of, (Iterable) n0.setOf((Object[]) new GuildFeature[]{GuildFeature.BANNER, GuildFeature.SEVEN_DAY_THREAD_ARCHIVE, GuildFeature.PRIVATE_THREADS, GuildFeature.ROLE_ICONS}));
        tier2BoostFeatures = plus;
        tier3BoostFeatures = o0.plus((Set) plus, (Iterable) n0.setOf((Object[]) new GuildFeature[]{GuildFeature.VANITY_URL, GuildFeature.ANIMATED_BANNER}));
    }

    private PremiumUtils() {
    }

    public final boolean doesGuildHaveEnoughBoostsForFeature(Guild guild, GuildFeature guildFeature) {
        m.checkNotNullParameter(guildFeature, "feature");
        int premiumTier = guild != null ? guild.getPremiumTier() : -1;
        Integer minimumBoostTierForGuildFeature = getMinimumBoostTierForGuildFeature(guildFeature);
        return premiumTier >= (minimumBoostTierForGuildFeature != null ? minimumBoostTierForGuildFeature.intValue() : 0);
    }

    public final Purchase findPurchaseForSkuName(List<? extends Purchase> list, String str) {
        m.checkNotNullParameter(str, "skuName");
        Object obj = null;
        if (list == null) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            if (((Purchase) next).b().contains(str)) {
                obj = next;
                break;
            }
        }
        return (Purchase) obj;
    }

    public final ModelSubscription findSubscriptionForSku(List<ModelSubscription> list, String str) {
        Object obj;
        m.checkNotNullParameter(list, "subscriptions");
        m.checkNotNullParameter(str, "skuName");
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (m.areEqual(((ModelSubscription) obj).getPaymentGatewayPlanId(), str)) {
                break;
            }
        }
        return (ModelSubscription) obj;
    }

    public final BoostFeatureBadgeData getBoostFeatureBadgeDataForGuildFeature(Guild guild, Long l, GuildFeature guildFeature, Context context, FragmentManager fragmentManager, Function1<? super View, Unit> function1, Traits.Location location) {
        m.checkNotNullParameter(guildFeature, "guildFeature");
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        if (guild == null || !guild.hasFeature(guildFeature)) {
            Integer minimumBoostTierForGuildFeature = getMinimumBoostTierForGuildFeature(guildFeature);
            PremiumUtils$getBoostFeatureBadgeDataForGuildFeature$1 premiumUtils$getBoostFeatureBadgeDataForGuildFeature$1 = new PremiumUtils$getBoostFeatureBadgeDataForGuildFeature$1(guild, fragmentManager, minimumBoostTierForGuildFeature, location, l);
            CharSequence boostTierShortText = getBoostTierShortText(context, minimumBoostTierForGuildFeature);
            if (boostTierShortText == null) {
                boostTierShortText = "";
            }
            return new BoostFeatureBadgeData(premiumUtils$getBoostFeatureBadgeDataForGuildFeature$1, ColorCompat.getThemedColor(context, (int) R.attr.colorInteractiveNormal), boostTierShortText, ColorCompat.getThemedColor(context, (int) R.attr.colorTextMuted));
        }
        String string = context.getString(R.string.guild_settings_premium_guild_unlocked);
        m.checkNotNullExpressionValue(string, "context.getString(R.stri…s_premium_guild_unlocked)");
        return new BoostFeatureBadgeData(function1, ColorCompat.getColor(context, (int) R.color.guild_boosting_pink), string, ColorCompat.getThemedColor(context, (int) R.attr.colorTextNormal));
    }

    public final CharSequence getBoostTierShortText(Context context, Integer num) {
        CharSequence b2;
        CharSequence b3;
        CharSequence b4;
        m.checkNotNullParameter(context, "context");
        if (num != null && num.intValue() == 1) {
            b4 = b.b(context, R.string.premium_guild_tier_1_short, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            return b4;
        } else if (num != null && num.intValue() == 2) {
            b3 = b.b(context, R.string.premium_guild_tier_2_short, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            return b3;
        } else if (num == null || num.intValue() != 3) {
            return null;
        } else {
            b2 = b.b(context, R.string.premium_guild_tier_3_short, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            return b2;
        }
    }

    public final int getGuildBoostCountFromSubscription(ModelSubscription modelSubscription) {
        m.checkNotNullParameter(modelSubscription, "$this$getGuildBoostCountFromSubscription");
        return (modelSubscription.getPremiumBasePlanId() == SubscriptionPlanType.PREMIUM_MONTH_TIER_2.getPlanId() || modelSubscription.getPremiumBasePlanId() == SubscriptionPlanType.PREMIUM_YEAR_TIER_2.getPlanId()) ? 2 : 0;
    }

    public final int getGuildEmojiMaxCount(int i, boolean z2) {
        if (z2 && i <= 2) {
            return 200;
        }
        if (i == 1) {
            return 100;
        }
        if (i != 2) {
            return i != 3 ? 50 : 250;
        }
        return 150;
    }

    public final int getGuildEmojiMaxCount(Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        return getGuildEmojiMaxCount(guild.getPremiumTier(), guild.getFeatures().contains(GuildFeature.MORE_EMOJI));
    }

    public final int getGuildMaxFileSizeMB(int i) {
        if (i == 0 || i == 1) {
            return 8;
        }
        return i != 2 ? 100 : 50;
    }

    public final int getGuildStickerMaxCount(int i, boolean z2) {
        if (z2 && i <= 2) {
            return 60;
        }
        if (i == 1) {
            return 15;
        }
        if (i != 2) {
            return i != 3 ? 0 : 60;
        }
        return 30;
    }

    public final int getMaxFileSizeMB(User user) {
        PremiumTier premiumTier = null;
        if ((user != null ? user.getPremiumTier() : null) == PremiumTier.TIER_1) {
            return 50;
        }
        if (user != null) {
            premiumTier = user.getPremiumTier();
        }
        return premiumTier == PremiumTier.TIER_2 ? 100 : 8;
    }

    public final Integer getMinimumBoostTierForGuildFeature(GuildFeature guildFeature) {
        m.checkNotNullParameter(guildFeature, "guildFeature");
        if (tier1BoostFeatures.contains(guildFeature)) {
            return 1;
        }
        if (tier2BoostFeatures.contains(guildFeature)) {
            return 2;
        }
        return tier3BoostFeatures.contains(guildFeature) ? 3 : null;
    }

    @DrawableRes
    public final int getNitroGiftBackground(ModelGift modelGift, Context context) {
        m.checkNotNullParameter(modelGift, "gift");
        m.checkNotNullParameter(context, "context");
        if (GiftStyleKt.hasCustomStyle(modelGift)) {
            return ColorCompat.getThemedColor(context, (int) R.attr.colorBackgroundSecondaryAlt);
        }
        return DrawableCompat.getThemedDrawableRes(context, (int) R.attr.gift_nitro_splash, (int) R.drawable.img_nitro_splash_dark);
    }

    @DrawableRes
    public final int getNitroGiftIcon(ModelGift modelGift) {
        m.checkNotNullParameter(modelGift, "gift");
        GiftStyle customStyle = GiftStyleKt.getCustomStyle(modelGift);
        if (customStyle != null) {
            return customStyle.getStaticRes();
        }
        ModelStoreListing storeListing = modelGift.getStoreListing();
        return getNitroGiftIcon(storeListing != null ? storeListing.getSku() : null);
    }

    public final CharSequence getPlanPriceText(Context context, SkuDetails skuDetails) {
        CharSequence b2;
        CharSequence b3;
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(skuDetails, "skuDetails");
        String optString = skuDetails.f2002b.optString("subscriptionPeriod");
        int hashCode = optString.hashCode();
        if (hashCode != 78476) {
            if (hashCode == 78488 && optString.equals(YEARLY_ISO8601)) {
                b3 = b.b(context, R.string.billing_price_per_year, new Object[]{skuDetails.b()}, (r4 & 4) != 0 ? b.C0034b.j : null);
                return b3;
            }
        } else if (optString.equals(MONTHLY_ISO8601)) {
            b2 = b.b(context, R.string.billing_price_per_month, new Object[]{skuDetails.b()}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b2;
        }
        return "";
    }

    public final String getSkuPrice(String str) {
        m.checkNotNullParameter(str, "paymentGatewaySkuId");
        StoreGooglePlaySkuDetails.State state = StoreStream.Companion.getGooglePlaySkuDetails().getState();
        if (!(state instanceof StoreGooglePlaySkuDetails.State.Loaded)) {
            Logger.w$default(AppLog.g, a.v("SkuDetails not loaded. Unable to get price for sku id ", str), null, 2, null);
            return null;
        }
        SkuDetails skuDetails = ((StoreGooglePlaySkuDetails.State.Loaded) state).getSkuDetails().get(str);
        if (skuDetails != null) {
            return skuDetails.b();
        }
        return null;
    }

    public final boolean isNitroSku(ModelSku modelSku) {
        m.checkNotNullParameter(modelSku, "sku");
        return modelSku.getSkuCategory() == ModelSku.SkuCategory.NITRO_CLASSIC || modelSku.getSkuCategory() == ModelSku.SkuCategory.NITRO;
    }

    public final boolean isPremiumTierAtLeast(PremiumTier premiumTier, PremiumTier premiumTier2) {
        m.checkNotNullParameter(premiumTier, "premiumTier");
        m.checkNotNullParameter(premiumTier2, "minimumLevel");
        return premiumTier.ordinal() >= premiumTier2.ordinal();
    }

    public final int microAmountToMinor(long j) {
        return (int) (j / ((long) XCallback.PRIORITY_HIGHEST));
    }

    public final void openAppleBilling(Context context) {
        CharSequence b2;
        m.checkNotNullParameter(context, "context");
        UriHandler uriHandler = UriHandler.INSTANCE;
        b2 = b.b(context, R.string.apple_billing_url, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        UriHandler.handle$default(uriHandler, context, b2.toString(), null, 4, null);
    }

    public final void openGooglePlayBilling(Context context, String str) {
        String str2;
        m.checkNotNullParameter(context, "context");
        if (str == null || (str2 = a.N(new Object[]{str, context.getPackageName()}, 2, GooglePlayBillingManager.PLAY_STORE_SUBSCRIPTION_DEEPLINK_URL, "java.lang.String.format(this, *args)")) == null) {
            str2 = GooglePlayBillingManager.PLAY_STORE_SUBSCRIPTION_URL;
        }
        UriHandler.handle$default(UriHandler.INSTANCE, context, str2, null, 4, null);
    }

    @DrawableRes
    public final int getNitroGiftIcon(ModelSku modelSku) {
        ModelSku.SkuCategory skuCategory = modelSku != null ? modelSku.getSkuCategory() : null;
        if (skuCategory != null) {
            int ordinal = skuCategory.ordinal();
            if (ordinal == 1) {
                return R.drawable.drawable_ic_nitro;
            }
            if (ordinal == 2) {
                return R.drawable.drawable_ic_nitro_classic;
            }
        }
        return 0;
    }
}
