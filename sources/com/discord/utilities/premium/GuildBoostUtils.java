package com.discord.utilities.premium;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import com.discord.app.AppLog;
import com.discord.models.domain.ModelGuildBoostSlot;
import com.discord.models.domain.ModelSubscription;
import com.discord.models.domain.premium.SubscriptionPlanType;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreGuildBoost;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.premium.GuildBoostUtils;
import com.discord.utilities.rest.RestAPI;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import j0.p.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import rx.Observable;
import rx.functions.Action1;
/* compiled from: GuildBoostUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000e\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\n\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001*B\t\b\u0002¢\u0006\u0004\b(\u0010)JA\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u0013\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0011H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0017\u0010\u0015\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0011H\u0002¢\u0006\u0004\b\u0015\u0010\u0014J7\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u000e0\r2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\u0016\u0010\u0017J7\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u000e0\r2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\u0018\u0010\u0017J\u001d\u0010\u001a\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0019\u001a\u00020\u0011¢\u0006\u0004\b\u001a\u0010\u001bJ\u001d\u0010\u001c\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0019\u001a\u00020\u0011¢\u0006\u0004\b\u001c\u0010\u001bJ\u0015\u0010\u001e\u001a\u00020\u00112\u0006\u0010\u001d\u001a\u00020\u0011¢\u0006\u0004\b\u001e\u0010\u0014J%\u0010\"\u001a\n\u0012\u0004\u0012\u00020!\u0018\u00010 2\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\u001f\u001a\u00020\u0011¢\u0006\u0004\b\"\u0010#R\u0016\u0010$\u001a\u00020\u00118\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b$\u0010%R\u0016\u0010&\u001a\u00020\u00118\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b&\u0010%R\u0016\u0010'\u001a\u00020\u00118\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b'\u0010%¨\u0006+"}, d2 = {"Lcom/discord/utilities/premium/GuildBoostUtils;", "", "Lcom/discord/utilities/rest/RestAPI;", "api", "", "Lcom/discord/primitives/GuildBoostSlotId;", "slotId", "Lcom/discord/models/domain/ModelSubscription;", Traits.Payment.Type.SUBSCRIPTION, "", "cancel", "Lcom/discord/stores/StoreGuildBoost;", "storeGuildBoost", "Lrx/Observable;", "Lcom/discord/utilities/premium/GuildBoostUtils$ModifyGuildBoostSlotResult;", "modifyGuildBoostSlot", "(Lcom/discord/utilities/rest/RestAPI;JLcom/discord/models/domain/ModelSubscription;ZLcom/discord/stores/StoreGuildBoost;)Lrx/Observable;", "", "premiumTier", "getNextTierSubs", "(I)I", "getCurrentTierSubs", "cancelGuildBoostSlot", "(Lcom/discord/utilities/rest/RestAPI;JLcom/discord/models/domain/ModelSubscription;Lcom/discord/stores/StoreGuildBoost;)Lrx/Observable;", "uncancelGuildBoostSlot", "subscriptionCount", "calculateTotalProgress", "(II)I", "calculatePercentToNextTier", "guildBoostCount", "getBoostTier", "guildBoostAdjustment", "", "Lcom/discord/models/domain/ModelSubscription$SubscriptionAdditionalPlan;", "calculateAdditionalPlansWithGuildBoostAdjustment", "(Lcom/discord/models/domain/ModelSubscription;I)Ljava/util/List;", "DEFAULT_GUILD_BOOST_SLOT_COUNT", "I", "GUILD_BOOST_COOLDOWN_DAYS", "DEFAULT_GUILD_BOOST_GUILD_COUNT", HookHelper.constructorName, "()V", "ModifyGuildBoostSlotResult", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildBoostUtils {
    public static final int DEFAULT_GUILD_BOOST_GUILD_COUNT = 1;
    public static final int DEFAULT_GUILD_BOOST_SLOT_COUNT = 1;
    public static final int GUILD_BOOST_COOLDOWN_DAYS = 7;
    public static final GuildBoostUtils INSTANCE = new GuildBoostUtils();

    /* compiled from: GuildBoostUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/utilities/premium/GuildBoostUtils$ModifyGuildBoostSlotResult;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "SUCCESS", "FAILURE_MODIFYING_SLOT", "FAILURE_UPDATING_SUBSCRIPTION", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum ModifyGuildBoostSlotResult {
        SUCCESS,
        FAILURE_MODIFYING_SLOT,
        FAILURE_UPDATING_SUBSCRIPTION
    }

    private GuildBoostUtils() {
    }

    private final int getCurrentTierSubs(int i) {
        if (i == 1) {
            return 2;
        }
        if (i != 2) {
            return i != 3 ? 0 : 14;
        }
        return 7;
    }

    private final int getNextTierSubs(int i) {
        if (i == 1) {
            return 7;
        }
        if (i != 2) {
            return i != 3 ? 2 : 0;
        }
        return 14;
    }

    private final Observable<ModifyGuildBoostSlotResult> modifyGuildBoostSlot(final RestAPI restAPI, long j, final ModelSubscription modelSubscription, final boolean z2, final StoreGuildBoost storeGuildBoost) {
        Observable<ModelGuildBoostSlot> observable;
        if (z2) {
            observable = restAPI.cancelSubscriptionSlot(j);
        } else {
            observable = restAPI.uncancelSubscriptionSlot(j);
        }
        Observable<ModifyGuildBoostSlotResult> z3 = observable.X(a.c()).t(new Action1<ModelGuildBoostSlot>() { // from class: com.discord.utilities.premium.GuildBoostUtils$modifyGuildBoostSlot$1
            public final void call(ModelGuildBoostSlot modelGuildBoostSlot) {
                StoreGuildBoost storeGuildBoost2 = StoreGuildBoost.this;
                m.checkNotNullExpressionValue(modelGuildBoostSlot, "it");
                storeGuildBoost2.updateGuildBoostSlot(modelGuildBoostSlot);
            }
        }).F(GuildBoostUtils$modifyGuildBoostSlot$2.INSTANCE).L(GuildBoostUtils$modifyGuildBoostSlot$3.INSTANCE).z(new b<Boolean, Observable<? extends ModifyGuildBoostSlotResult>>() { // from class: com.discord.utilities.premium.GuildBoostUtils$modifyGuildBoostSlot$4

            /* compiled from: GuildBoostUtils.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0006\u001a\n \u0003*\u0004\u0018\u00010\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Ljava/lang/Void;", "it", "Lcom/discord/utilities/premium/GuildBoostUtils$ModifyGuildBoostSlotResult;", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Void;)Lcom/discord/utilities/premium/GuildBoostUtils$ModifyGuildBoostSlotResult;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.utilities.premium.GuildBoostUtils$modifyGuildBoostSlot$4$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1<T, R> implements b<Void, GuildBoostUtils.ModifyGuildBoostSlotResult> {
                public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                public final GuildBoostUtils.ModifyGuildBoostSlotResult call(Void r1) {
                    return GuildBoostUtils.ModifyGuildBoostSlotResult.SUCCESS;
                }
            }

            /* compiled from: GuildBoostUtils.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "it", "Lcom/discord/utilities/premium/GuildBoostUtils$ModifyGuildBoostSlotResult;", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Throwable;)Lcom/discord/utilities/premium/GuildBoostUtils$ModifyGuildBoostSlotResult;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.utilities.premium.GuildBoostUtils$modifyGuildBoostSlot$4$2  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass2<T, R> implements b<Throwable, GuildBoostUtils.ModifyGuildBoostSlotResult> {
                public static final AnonymousClass2 INSTANCE = new AnonymousClass2();

                public final GuildBoostUtils.ModifyGuildBoostSlotResult call(Throwable th) {
                    return GuildBoostUtils.ModifyGuildBoostSlotResult.FAILURE_UPDATING_SUBSCRIPTION;
                }
            }

            public final Observable<? extends GuildBoostUtils.ModifyGuildBoostSlotResult> call(Boolean bool) {
                if (!bool.booleanValue()) {
                    return new k(GuildBoostUtils.ModifyGuildBoostSlotResult.FAILURE_MODIFYING_SLOT);
                }
                return (Observable<R>) RestAPI.this.updateSubscription(modelSubscription.getId(), new RestAPIParams.UpdateSubscription(null, null, null, GuildBoostUtils.INSTANCE.calculateAdditionalPlansWithGuildBoostAdjustment(modelSubscription, z2 ? -1 : 1), 7, null)).F(AnonymousClass1.INSTANCE).L(AnonymousClass2.INSTANCE);
            }
        });
        m.checkNotNullExpressionValue(z3, "apiObs\n        .subscrib…N }\n          }\n        }");
        return z3;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final List<ModelSubscription.SubscriptionAdditionalPlan> calculateAdditionalPlansWithGuildBoostAdjustment(ModelSubscription modelSubscription, int i) {
        List<ModelSubscription.SubscriptionAdditionalPlan> list;
        boolean z2;
        m.checkNotNullParameter(modelSubscription, Traits.Payment.Type.SUBSCRIPTION);
        ModelSubscription.SubscriptionRenewalMutations renewalMutations = modelSubscription.getRenewalMutations();
        if (renewalMutations == null) {
            list = modelSubscription.getPremiumAdditionalPlans();
        } else {
            list = renewalMutations.getPremiumAdditionalPlans();
            if (list == null) {
                list = n.emptyList();
            }
        }
        ModelSubscription.SubscriptionAdditionalPlan subscriptionAdditionalPlan = null;
        if (list != null) {
            Iterator<T> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Object next = it.next();
                ModelSubscription.SubscriptionAdditionalPlan subscriptionAdditionalPlan2 = (ModelSubscription.SubscriptionAdditionalPlan) next;
                if (subscriptionAdditionalPlan2.getPlanId() == SubscriptionPlanType.PREMIUM_GUILD_MONTH.getPlanId() || subscriptionAdditionalPlan2.getPlanId() == SubscriptionPlanType.PREMIUM_GUILD_YEAR.getPlanId()) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    subscriptionAdditionalPlan = next;
                    break;
                }
            }
            subscriptionAdditionalPlan = subscriptionAdditionalPlan;
        }
        int quantity = (subscriptionAdditionalPlan != null ? subscriptionAdditionalPlan.getQuantity() : 0) + i;
        if (quantity < 0 || subscriptionAdditionalPlan == null) {
            AppLog appLog = AppLog.g;
            Logger.e$default(appLog, "Error calculating additional_plans adjustment, new sub count:" + quantity, null, null, 6, null);
            return modelSubscription.getPremiumAdditionalPlans();
        }
        ArrayList arrayList = new ArrayList();
        for (Object obj : list) {
            ModelSubscription.SubscriptionAdditionalPlan subscriptionAdditionalPlan3 = (ModelSubscription.SubscriptionAdditionalPlan) obj;
            if ((subscriptionAdditionalPlan3.getPlanId() == SubscriptionPlanType.PREMIUM_GUILD_MONTH.getPlanId() || subscriptionAdditionalPlan3.getPlanId() == SubscriptionPlanType.PREMIUM_GUILD_YEAR.getPlanId()) ? false : true) {
                arrayList.add(obj);
            }
        }
        return quantity == 0 ? arrayList : u.plus((Collection) arrayList, (Iterable) d0.t.m.listOf(new ModelSubscription.SubscriptionAdditionalPlan(subscriptionAdditionalPlan.getPlanId(), quantity)));
    }

    public final int calculatePercentToNextTier(int i, int i2) {
        if (i >= 3) {
            return 100;
        }
        return d0.a0.a.roundToInt(((i2 - getCurrentTierSubs(i)) / getNextTierSubs(i)) * 100);
    }

    public final int calculateTotalProgress(int i, int i2) {
        if (i >= 3) {
            return 100;
        }
        int nextTierSubs = getNextTierSubs(i);
        int currentTierSubs = getCurrentTierSubs(i);
        return d0.a0.a.roundToInt((((i2 - currentTierSubs) / (nextTierSubs - currentTierSubs)) * 33.3f) + (i * 33.3f));
    }

    public final Observable<ModifyGuildBoostSlotResult> cancelGuildBoostSlot(RestAPI restAPI, long j, ModelSubscription modelSubscription, StoreGuildBoost storeGuildBoost) {
        m.checkNotNullParameter(restAPI, "api");
        m.checkNotNullParameter(modelSubscription, Traits.Payment.Type.SUBSCRIPTION);
        m.checkNotNullParameter(storeGuildBoost, "storeGuildBoost");
        return modifyGuildBoostSlot(restAPI, j, modelSubscription, true, storeGuildBoost);
    }

    public final int getBoostTier(int i) {
        if (i >= 14) {
            return 3;
        }
        if (i >= 7) {
            return 2;
        }
        return i >= 2 ? 1 : 0;
    }

    public final Observable<ModifyGuildBoostSlotResult> uncancelGuildBoostSlot(RestAPI restAPI, long j, ModelSubscription modelSubscription, StoreGuildBoost storeGuildBoost) {
        m.checkNotNullParameter(restAPI, "api");
        m.checkNotNullParameter(modelSubscription, Traits.Payment.Type.SUBSCRIPTION);
        m.checkNotNullParameter(storeGuildBoost, "storeGuildBoost");
        return modifyGuildBoostSlot(restAPI, j, modelSubscription, false, storeGuildBoost);
    }
}
