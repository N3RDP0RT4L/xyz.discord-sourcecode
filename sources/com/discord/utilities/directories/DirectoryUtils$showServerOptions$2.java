package com.discord.utilities.directories;

import android.content.Context;
import android.view.View;
import androidx.fragment.app.FragmentManager;
import b.a.a.e;
import b.a.d.j;
import b.a.k.b;
import com.discord.api.directory.DirectoryEntryGuild;
import com.discord.app.AppFragment;
import com.discord.dialogs.SimpleConfirmationDialogArgs;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.directories.DirectoryUtils;
import com.discord.widgets.hubs.HubDescriptionArgs;
import com.discord.widgets.hubs.WidgetHubDescription;
import com.discord.widgets.mobile_reports.WidgetMobileReports;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: DirectoryUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", ModelAuditLogEntry.CHANGE_KEY_POSITION, "", "invoke", "(I)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class DirectoryUtils$showServerOptions$2 extends o implements Function1<Integer, Unit> {
    public final /* synthetic */ Context $context;
    public final /* synthetic */ DirectoryEntryGuild $directoryEntry;
    public final /* synthetic */ AppFragment $fragment;
    public final /* synthetic */ String $hubName;
    public final /* synthetic */ List $options;
    public final /* synthetic */ Function0 $removeGuildListener;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public DirectoryUtils$showServerOptions$2(List list, Context context, DirectoryEntryGuild directoryEntryGuild, String str, AppFragment appFragment, Function0 function0) {
        super(1);
        this.$options = list;
        this.$context = context;
        this.$directoryEntry = directoryEntryGuild;
        this.$hubName = str;
        this.$fragment = appFragment;
        this.$removeGuildListener = function0;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
        invoke(num.intValue());
        return Unit.a;
    }

    public final void invoke(int i) {
        CharSequence b2;
        DirectoryUtils.DirectoryServerMenuOptions directoryServerMenuOptions = (DirectoryUtils.DirectoryServerMenuOptions) u.getOrNull(this.$options, i);
        if (directoryServerMenuOptions != null) {
            int ordinal = directoryServerMenuOptions.ordinal();
            if (ordinal == 0) {
                j.d(this.$context, WidgetHubDescription.class, new HubDescriptionArgs(this.$directoryEntry.e().h(), this.$directoryEntry.c(), true, this.$hubName, false, this.$directoryEntry.b(), Integer.valueOf(this.$directoryEntry.f()), 16, null));
            } else if (ordinal == 1) {
                e.c cVar = e.k;
                FragmentManager childFragmentManager = this.$fragment.getChildFragmentManager();
                m.checkNotNullExpressionValue(childFragmentManager, "fragment.childFragmentManager");
                String string = this.$fragment.getString(R.string.hub_entry_remove);
                b2 = b.b(this.$context, R.string.hub_entry_remove_body, new Object[]{this.$directoryEntry.e().i()}, (r4 & 4) != 0 ? b.C0034b.j : null);
                cVar.a(childFragmentManager, new SimpleConfirmationDialogArgs(string, b2.toString(), this.$fragment.getString(R.string.remove), this.$fragment.getString(R.string.cancel)), new View.OnClickListener() { // from class: com.discord.utilities.directories.DirectoryUtils$showServerOptions$2.1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        DirectoryUtils$showServerOptions$2.this.$removeGuildListener.invoke();
                    }
                });
            } else if (ordinal == 2) {
                WidgetMobileReports.Companion.launchDirectoryServerReport(this.$context, this.$directoryEntry.e().h(), this.$directoryEntry.d(), this.$directoryEntry.c());
            }
        }
    }
}
