package com.discord.utilities.directories;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;
import androidx.fragment.app.FragmentManager;
import b.a.y.c0;
import b.a.y.d0;
import com.discord.analytics.generated.events.TrackHubEventCtaClicked;
import com.discord.analytics.generated.traits.TrackGuild;
import com.discord.analytics.utils.hubs.HubGuildScheduledEventClickType;
import com.discord.api.directory.DirectoryEntryGuild;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.app.AppBottomSheet;
import com.discord.app.AppFragment;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.AnalyticsUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.widgets.guilds.join.GuildJoinHelperKt;
import d0.o;
import d0.t.m;
import d0.t.n;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import xyz.discord.R;
/* compiled from: DirectoryUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\t\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001.B\t\b\u0002¢\u0006\u0004\b,\u0010-J;\u0010\r\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\u0004\b\r\u0010\u000eJS\u0010\u001b\u001a\u00020\u000b2\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\b2\n\u0010\u0016\u001a\u00060\u0014j\u0002`\u00152\n\u0010\u0018\u001a\u00060\u0014j\u0002`\u00172\u0006\u0010\u0019\u001a\u00020\b2\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\u0004\b\u001b\u0010\u001cJ9\u0010#\u001a\u00020\u000b2\n\u0010\u001e\u001a\u00060\u0014j\u0002`\u001d2\n\u0010\u001f\u001a\u00060\u0014j\u0002`\u00152\n\u0010 \u001a\u00060\u0014j\u0002`\u00152\u0006\u0010\"\u001a\u00020!¢\u0006\u0004\b#\u0010$R\u001c\u0010'\u001a\b\u0012\u0004\u0012\u00020&0%8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R\u001c\u0010)\u001a\b\u0012\u0004\u0012\u00020&0%8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b)\u0010(R\u0016\u0010*\u001a\u00020\u00068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b*\u0010+¨\u0006/"}, d2 = {"Lcom/discord/utilities/directories/DirectoryUtils;", "", "Lcom/discord/app/AppFragment;", "fragment", "Lcom/discord/api/directory/DirectoryEntryGuild;", "directoryEntry", "", "hubName", "", "isServerOwner", "Lkotlin/Function0;", "", "removeGuildListener", "showServerOptions", "(Lcom/discord/app/AppFragment;Lcom/discord/api/directory/DirectoryEntryGuild;Ljava/lang/String;ZLkotlin/jvm/functions/Function0;)V", "Lcom/discord/app/AppBottomSheet;", "bottomSheet", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "guildScheduledEvent", "isInGuild", "", "Lcom/discord/primitives/GuildId;", "hubGuildId", "Lcom/discord/primitives/ChannelId;", "directoryChannelId", "shouldToggleRsvp", "toggleRsvp", "maybeJoinAndGoToGuild", "(Lcom/discord/app/AppBottomSheet;Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;ZJJZLkotlin/jvm/functions/Function0;)V", "Lcom/discord/primitives/GuildScheduledEventId;", "guildScheduledEventId", "guildScheduledEventGuildId", "guildId", "Lcom/discord/analytics/utils/hubs/HubGuildScheduledEventClickType;", "clickType", "logGuildScheduledEventClickAction", "(JJJLcom/discord/analytics/utils/hubs/HubGuildScheduledEventClickType;)V", "", "Lcom/discord/utilities/directories/DirectoryUtils$DirectoryServerMenuOptions;", "NON_SERVER_OWNER_MENU_OPTIONS", "Ljava/util/List;", "SERVER_OWNER_MENU_OPTIONS", "JOIN_GUILD_SOURCE", "Ljava/lang/String;", HookHelper.constructorName, "()V", "DirectoryServerMenuOptions", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class DirectoryUtils {
    public static final String JOIN_GUILD_SOURCE = "Directory Channel Entry";
    public static final DirectoryUtils INSTANCE = new DirectoryUtils();
    private static final List<DirectoryServerMenuOptions> SERVER_OWNER_MENU_OPTIONS = n.listOf((Object[]) new DirectoryServerMenuOptions[]{DirectoryServerMenuOptions.Edit, DirectoryServerMenuOptions.Remove});
    private static final List<DirectoryServerMenuOptions> NON_SERVER_OWNER_MENU_OPTIONS = m.listOf(DirectoryServerMenuOptions.Report);

    /* compiled from: DirectoryUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\b\n\u0002\b\f\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u001d\b\u0002\u0012\b\b\u0001\u0010\u0007\u001a\u00020\u0002\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\t\u0010\nR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0004\u001a\u0004\b\b\u0010\u0006j\u0002\b\u000bj\u0002\b\fj\u0002\b\r¨\u0006\u000e"}, d2 = {"Lcom/discord/utilities/directories/DirectoryUtils$DirectoryServerMenuOptions;", "", "", "iconRes", "I", "getIconRes", "()I", "titleRes", "getTitleRes", HookHelper.constructorName, "(Ljava/lang/String;III)V", "Edit", "Remove", "Report", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum DirectoryServerMenuOptions {
        Edit(R.string.hub_entry_update, R.drawable.ic_edit_24dp),
        Remove(R.string.hub_entry_remove, R.drawable.ic_delete_24dp),
        Report(R.string.report, R.drawable.ic_flag_24dp);
        
        private final int iconRes;
        private final int titleRes;

        DirectoryServerMenuOptions(@StringRes int i, @DrawableRes int i2) {
            this.titleRes = i;
            this.iconRes = i2;
        }

        public final int getIconRes() {
            return this.iconRes;
        }

        public final int getTitleRes() {
            return this.titleRes;
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            DirectoryServerMenuOptions.values();
            int[] iArr = new int[3];
            $EnumSwitchMapping$0 = iArr;
            iArr[DirectoryServerMenuOptions.Edit.ordinal()] = 1;
            iArr[DirectoryServerMenuOptions.Remove.ordinal()] = 2;
            iArr[DirectoryServerMenuOptions.Report.ordinal()] = 3;
        }
    }

    private DirectoryUtils() {
    }

    public final void logGuildScheduledEventClickAction(long j, long j2, long j3, HubGuildScheduledEventClickType hubGuildScheduledEventClickType) {
        d0.z.d.m.checkNotNullParameter(hubGuildScheduledEventClickType, "clickType");
        AnalyticsUtils.Tracker tracker = AnalyticsTracker.INSTANCE.getTracker();
        TrackHubEventCtaClicked trackHubEventCtaClicked = new TrackHubEventCtaClicked(Long.valueOf(j), Long.valueOf(j2), hubGuildScheduledEventClickType.getType());
        trackHubEventCtaClicked.c(new TrackGuild(Long.valueOf(j3), null, null, null, null, null, null, null, null, null, null, 2046));
        tracker.track(trackHubEventCtaClicked);
    }

    public final void maybeJoinAndGoToGuild(AppBottomSheet appBottomSheet, GuildScheduledEvent guildScheduledEvent, boolean z2, long j, long j2, boolean z3, Function0<Unit> function0) {
        d0.z.d.m.checkNotNullParameter(appBottomSheet, "bottomSheet");
        d0.z.d.m.checkNotNullParameter(guildScheduledEvent, "guildScheduledEvent");
        d0.z.d.m.checkNotNullParameter(function0, "toggleRsvp");
        if (z2) {
            StoreStream.Companion.getGuildSelected().set(guildScheduledEvent.h());
            appBottomSheet.dismiss();
            return;
        }
        logGuildScheduledEventClickAction(guildScheduledEvent.i(), guildScheduledEvent.h(), j, HubGuildScheduledEventClickType.JoinServer);
        Context requireContext = appBottomSheet.requireContext();
        d0.z.d.m.checkNotNullExpressionValue(requireContext, "bottomSheet.requireContext()");
        GuildJoinHelperKt.joinGuild(requireContext, guildScheduledEvent.h(), false, (r27 & 8) != 0 ? null : null, (r27 & 16) != 0 ? null : Long.valueOf(j2), (r27 & 32) != 0 ? null : RestAPI.Companion.getApi().jsonObjectOf(o.to("source", JOIN_GUILD_SOURCE)), DirectoryUtils.class, (r27 & 128) != 0 ? null : null, (r27 & 256) != 0 ? null : null, (r27 & 512) != 0 ? null : null, new DirectoryUtils$maybeJoinAndGoToGuild$1(z3, function0, guildScheduledEvent, appBottomSheet));
    }

    public final void showServerOptions(AppFragment appFragment, DirectoryEntryGuild directoryEntryGuild, String str, boolean z2, Function0<Unit> function0) {
        List<DirectoryServerMenuOptions> list;
        d0.z.d.m.checkNotNullParameter(appFragment, "fragment");
        d0.z.d.m.checkNotNullParameter(directoryEntryGuild, "directoryEntry");
        d0.z.d.m.checkNotNullParameter(str, "hubName");
        d0.z.d.m.checkNotNullParameter(function0, "removeGuildListener");
        Context context = appFragment.getContext();
        if (context != null) {
            d0.z.d.m.checkNotNullExpressionValue(context, "fragment.context ?: return");
            if (z2) {
                list = SERVER_OWNER_MENU_OPTIONS;
            } else {
                list = NON_SERVER_OWNER_MENU_OPTIONS;
            }
            List<DirectoryServerMenuOptions> list2 = list;
            c0.a aVar = c0.k;
            FragmentManager childFragmentManager = appFragment.getChildFragmentManager();
            d0.z.d.m.checkNotNullExpressionValue(childFragmentManager, "fragment.childFragmentManager");
            String string = appFragment.getString(R.string.server_settings);
            d0.z.d.m.checkNotNullExpressionValue(string, "fragment.getString(R.string.server_settings)");
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(list2, 10));
            for (DirectoryServerMenuOptions directoryServerMenuOptions : list2) {
                arrayList.add(new d0(appFragment.getString(directoryServerMenuOptions.getTitleRes()), null, Integer.valueOf(directoryServerMenuOptions.getIconRes()), null, null, null, null, 122));
            }
            aVar.a(childFragmentManager, string, arrayList, false, new DirectoryUtils$showServerOptions$2(list2, context, directoryEntryGuild, str, appFragment, function0));
        }
    }
}
