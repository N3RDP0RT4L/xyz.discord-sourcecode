package com.discord.utilities.directories;

import com.discord.api.directory.DirectoryEntryGuild;
import com.discord.models.hubs.DirectoryEntryCategory;
import com.discord.widgets.directories.DirectoryEntryData;
import d0.t.u;
import d0.u.a;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import kotlin.Metadata;
/* compiled from: DirectoryUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\t\u001a%\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000*\b\u0012\u0004\u0012\u00020\u00010\u00002\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0004\u0010\u0005\u001a\u001d\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000*\b\u0012\u0004\u0012\u00020\u00010\u0000¢\u0006\u0004\b\u0006\u0010\u0007\u001a)\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000*\b\u0012\u0004\u0012\u00020\u00010\u00002\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\t\u0010\n\u001a\u0011\u0010\r\u001a\u00020\f*\u00020\u000b¢\u0006\u0004\b\r\u0010\u000e\"\u0016\u0010\u000f\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010\"\u0016\u0010\u0011\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0011\u0010\u0010\"\u0016\u0010\u0012\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0012\u0010\u0010\"\u0016\u0010\u0013\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0013\u0010\u0010\"\u0016\u0010\u0014\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0014\u0010\u0010¨\u0006\u0015"}, d2 = {"", "Lcom/discord/widgets/directories/DirectoryEntryData;", "", "numberToChoose", "sortByRanking", "(Ljava/util/List;I)Ljava/util/List;", "sortByAdded", "(Ljava/util/List;)Ljava/util/List;", "idealSize", "rank", "(Ljava/util/List;Ljava/lang/Integer;)Ljava/util/List;", "Lcom/discord/api/directory/DirectoryEntryGuild;", "", "hasMinimumMemberCount", "(Lcom/discord/api/directory/DirectoryEntryGuild;)Z", "MAXIMUM_SIZE", "I", "MINIMUM_MEMBER_COUNT", "MINIMUM_SIZE", "MAX_HOME_CATEGORY_COUNT", "MAX_RECOMMENDATION_COUNT", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class DirectoryUtilsKt {
    private static final int MAXIMUM_SIZE = 200;
    public static final int MAX_HOME_CATEGORY_COUNT = 5;
    public static final int MAX_RECOMMENDATION_COUNT = 5;
    private static final int MINIMUM_MEMBER_COUNT = 5;
    private static final int MINIMUM_SIZE = 5;

    public static final boolean hasMinimumMemberCount(DirectoryEntryGuild directoryEntryGuild) {
        m.checkNotNullParameter(directoryEntryGuild, "$this$hasMinimumMemberCount");
        Integer a = directoryEntryGuild.e().a();
        return (a != null ? a.intValue() : 0) >= 5;
    }

    public static final List<DirectoryEntryData> rank(List<DirectoryEntryData> list, final Integer num) {
        m.checkNotNullParameter(list, "$this$rank");
        return u.sortedWith(list, new Comparator() { // from class: com.discord.utilities.directories.DirectoryUtilsKt$rank$$inlined$sortedBy$1
            @Override // java.util.Comparator
            public final int compare(T t, T t2) {
                int i;
                int i2;
                DirectoryEntryData directoryEntryData = (DirectoryEntryData) t;
                Integer num2 = num;
                int i3 = 0;
                int intValue = num2 != null ? num2.intValue() : DirectoryEntryCategory.Companion.findByKey(directoryEntryData.getEntry().f(), false).getIdealSize();
                Integer a = directoryEntryData.getEntry().e().a();
                int intValue2 = a != null ? a.intValue() : 0;
                if (intValue2 >= intValue) {
                    i = (intValue2 - intValue) / (200 - intValue2);
                } else {
                    i = (intValue - intValue2) / (intValue - 5);
                }
                Integer valueOf = Integer.valueOf(i);
                DirectoryEntryData directoryEntryData2 = (DirectoryEntryData) t2;
                Integer num3 = num;
                int intValue3 = num3 != null ? num3.intValue() : DirectoryEntryCategory.Companion.findByKey(directoryEntryData2.getEntry().f(), false).getIdealSize();
                Integer a2 = directoryEntryData2.getEntry().e().a();
                if (a2 != null) {
                    i3 = a2.intValue();
                }
                if (i3 >= intValue3) {
                    i2 = (i3 - intValue3) / (200 - i3);
                } else {
                    i2 = (intValue3 - i3) / (intValue3 - 5);
                }
                return a.compareValues(valueOf, Integer.valueOf(i2));
            }
        });
    }

    public static /* synthetic */ List rank$default(List list, Integer num, int i, Object obj) {
        if ((i & 1) != 0) {
            num = null;
        }
        return rank(list, num);
    }

    public static final List<DirectoryEntryData> sortByAdded(List<DirectoryEntryData> list) {
        m.checkNotNullParameter(list, "$this$sortByAdded");
        ArrayList arrayList = new ArrayList();
        for (Object obj : list) {
            DirectoryEntryData directoryEntryData = (DirectoryEntryData) obj;
            if (m.areEqual(directoryEntryData.getEntry().e().e(), Boolean.TRUE) && hasMinimumMemberCount(directoryEntryData.getEntry())) {
                arrayList.add(obj);
            }
        }
        return u.take(u.sortedWith(arrayList, new Comparator() { // from class: com.discord.utilities.directories.DirectoryUtilsKt$sortByAdded$$inlined$sortedByDescending$1
            @Override // java.util.Comparator
            public final int compare(T t, T t2) {
                return a.compareValues(((DirectoryEntryData) t2).getEntry().a(), ((DirectoryEntryData) t).getEntry().a());
            }
        }), 5);
    }

    public static final List<DirectoryEntryData> sortByRanking(List<DirectoryEntryData> list, int i) {
        m.checkNotNullParameter(list, "$this$sortByRanking");
        ArrayList arrayList = new ArrayList();
        for (Object obj : list) {
            if (m.areEqual(((DirectoryEntryData) obj).getEntry().e().e(), Boolean.TRUE)) {
                arrayList.add(obj);
            }
        }
        return u.take(rank$default(arrayList, null, 1, null), i);
    }
}
