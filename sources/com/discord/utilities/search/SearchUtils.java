package com.discord.utilities.search;

import andhook.lib.HookHelper;
import d0.g0.w;
import d0.t.n0;
import d0.t.u;
import d0.z.d.m;
import java.util.Set;
import kotlin.Metadata;
import kotlin.text.Regex;
/* compiled from: SearchUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\"\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rJ\u001b\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u001d\u0010\n\u001a\u00020\t2\u0006\u0010\u0007\u001a\u00020\u00022\u0006\u0010\b\u001a\u00020\u0002¢\u0006\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/utilities/search/SearchUtils;", "", "", "searchText", "", "getQueriesFromSearchText", "(Ljava/lang/String;)Ljava/util/Set;", "query", "target", "", "fuzzyMatch", "(Ljava/lang/String;Ljava/lang/String;)Z", HookHelper.constructorName, "()V", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SearchUtils {
    public static final SearchUtils INSTANCE = new SearchUtils();

    private SearchUtils() {
    }

    public final boolean fuzzyMatch(String str, String str2) {
        m.checkNotNullParameter(str, "query");
        m.checkNotNullParameter(str2, "target");
        if (str.length() > str2.length()) {
            return false;
        }
        if (str.length() == str2.length()) {
            return m.areEqual(str, str2);
        }
        int length = str.length();
        int i = 0;
        int i2 = 0;
        while (i < length) {
            char charAt = str.charAt(i);
            while (i2 < str2.length()) {
                int i3 = i2 + 1;
                char charAt2 = str2.charAt(i2);
                if (charAt2 == charAt || (charAt == ' ' && charAt2 == '-')) {
                    i++;
                    i2 = i3;
                } else {
                    i2 = i3;
                }
            }
            return false;
        }
        return true;
    }

    public final Set<String> getQueriesFromSearchText(String str) {
        m.checkNotNullParameter(str, "searchText");
        return str.length() == 0 ? n0.emptySet() : u.toSet(w.split$default((CharSequence) new Regex("(\\n|\\t|\\s)").replace(new Regex("([!.;,\\-—–?\"'])").replace(w.trim(str).toString(), ""), " "), new char[]{' '}, false, 0, 6, (Object) null));
    }
}
