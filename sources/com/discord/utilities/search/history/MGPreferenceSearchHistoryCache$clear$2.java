package com.discord.utilities.search.history;

import com.discord.utilities.persister.Persister;
import com.discord.utilities.search.history.MGPreferenceSearchHistoryCache;
import d0.z.d.m;
import d0.z.d.o;
import java.util.LinkedList;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: MGPreferenceSearchHistoryCache.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Ljava/util/LinkedList;", "Lcom/discord/utilities/search/history/MGPreferenceSearchHistoryCache$TargetHistory;", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Ljava/util/LinkedList;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MGPreferenceSearchHistoryCache$clear$2 extends o implements Function1<LinkedList<MGPreferenceSearchHistoryCache.TargetHistory>, Unit> {
    public final /* synthetic */ MGPreferenceSearchHistoryCache this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MGPreferenceSearchHistoryCache$clear$2(MGPreferenceSearchHistoryCache mGPreferenceSearchHistoryCache) {
        super(1);
        this.this$0 = mGPreferenceSearchHistoryCache;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(LinkedList<MGPreferenceSearchHistoryCache.TargetHistory> linkedList) {
        invoke2(linkedList);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(LinkedList<MGPreferenceSearchHistoryCache.TargetHistory> linkedList) {
        Persister persister;
        persister = this.this$0.backingCache;
        m.checkNotNullExpressionValue(linkedList, "it");
        Persister.set$default(persister, linkedList, false, 2, null);
    }
}
