package com.discord.utilities.search.history;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.stores.StoreSearch;
import com.discord.utilities.persister.Persister;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$3;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$4;
import com.discord.utilities.search.history.MGPreferenceSearchHistoryCache;
import com.discord.utilities.search.query.node.QueryNode;
import d0.t.u;
import d0.z.d.m;
import j0.k.b;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: MGPreferenceSearchHistoryCache.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u001e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0001!B\u0007¢\u0006\u0004\b\u001f\u0010 J5\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002*\b\u0012\u0004\u0012\u00020\u00030\u00022\u0006\u0010\u0005\u001a\u00020\u00042\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H\u0002¢\u0006\u0004\b\t\u0010\nJ'\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002*\b\u0012\u0004\u0012\u00020\u00030\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000b\u0010\fJ-\u0010\u000e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u00060\u0002*\b\u0012\u0004\u0012\u00020\u00030\u00022\u0006\u0010\r\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000e\u0010\fJ%\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0005\u001a\u00020\u00042\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H\u0016¢\u0006\u0004\b\u0010\u0010\u0011J\u0017\u0010\u0012\u001a\u00020\u000f2\u0006\u0010\u0005\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0012\u0010\u0013J)\u0010\u0016\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u00060\u00150\u00142\u0006\u0010\u0005\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0019\u001a\u00020\u00188\u0002@\u0002X\u0082D¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001b\u001a\u00020\u00188\u0002@\u0002X\u0082D¢\u0006\u0006\n\u0004\b\u001b\u0010\u001aR\"\u0010\u001d\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00020\u001c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u001e¨\u0006\""}, d2 = {"Lcom/discord/utilities/search/history/MGPreferenceSearchHistoryCache;", "Lcom/discord/utilities/search/history/SearchHistoryCache;", "Ljava/util/LinkedList;", "Lcom/discord/utilities/search/history/MGPreferenceSearchHistoryCache$TargetHistory;", "Lcom/discord/stores/StoreSearch$SearchTarget;", "searchTarget", "", "Lcom/discord/utilities/search/query/node/QueryNode;", "query", "putAndCopy", "(Ljava/util/LinkedList;Lcom/discord/stores/StoreSearch$SearchTarget;Ljava/util/List;)Ljava/util/LinkedList;", "removeAndCopy", "(Ljava/util/LinkedList;Lcom/discord/stores/StoreSearch$SearchTarget;)Ljava/util/LinkedList;", "key", "find", "", "persistQuery", "(Lcom/discord/stores/StoreSearch$SearchTarget;Ljava/util/List;)V", "clear", "(Lcom/discord/stores/StoreSearch$SearchTarget;)V", "Lrx/Observable;", "", "getHistory", "(Lcom/discord/stores/StoreSearch$SearchTarget;)Lrx/Observable;", "", "MAX_SEARCH_TARGETS", "I", "MAX_QUERIES_PER_TARGET", "Lcom/discord/utilities/persister/Persister;", "backingCache", "Lcom/discord/utilities/persister/Persister;", HookHelper.constructorName, "()V", "TargetHistory", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MGPreferenceSearchHistoryCache implements SearchHistoryCache {
    private final int MAX_SEARCH_TARGETS = 10;
    private final int MAX_QUERIES_PER_TARGET = 5;
    private final Persister<LinkedList<TargetHistory>> backingCache = new Persister<>("SEARCH_HISTORY_V2", new LinkedList());

    /* compiled from: MGPreferenceSearchHistoryCache.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0082\b\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\n\u001a\u00020\u0002\u0012\u0012\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u00060\u0005¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001c\u0010\b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u00060\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ0\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u00022\u0014\b\u0002\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u00060\u0005HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R%\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0018\u001a\u0004\b\u0019\u0010\tR\u0019\u0010\n\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u001a\u001a\u0004\b\u001b\u0010\u0004¨\u0006\u001e"}, d2 = {"Lcom/discord/utilities/search/history/MGPreferenceSearchHistoryCache$TargetHistory;", "", "Lcom/discord/stores/StoreSearch$SearchTarget;", "component1", "()Lcom/discord/stores/StoreSearch$SearchTarget;", "Ljava/util/LinkedList;", "", "Lcom/discord/utilities/search/query/node/QueryNode;", "component2", "()Ljava/util/LinkedList;", "searchTarget", "recentQueries", "copy", "(Lcom/discord/stores/StoreSearch$SearchTarget;Ljava/util/LinkedList;)Lcom/discord/utilities/search/history/MGPreferenceSearchHistoryCache$TargetHistory;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/LinkedList;", "getRecentQueries", "Lcom/discord/stores/StoreSearch$SearchTarget;", "getSearchTarget", HookHelper.constructorName, "(Lcom/discord/stores/StoreSearch$SearchTarget;Ljava/util/LinkedList;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class TargetHistory {
        private final LinkedList<List<QueryNode>> recentQueries;
        private final StoreSearch.SearchTarget searchTarget;

        public TargetHistory(StoreSearch.SearchTarget searchTarget, LinkedList<List<QueryNode>> linkedList) {
            m.checkNotNullParameter(searchTarget, "searchTarget");
            m.checkNotNullParameter(linkedList, "recentQueries");
            this.searchTarget = searchTarget;
            this.recentQueries = linkedList;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ TargetHistory copy$default(TargetHistory targetHistory, StoreSearch.SearchTarget searchTarget, LinkedList linkedList, int i, Object obj) {
            if ((i & 1) != 0) {
                searchTarget = targetHistory.searchTarget;
            }
            if ((i & 2) != 0) {
                linkedList = targetHistory.recentQueries;
            }
            return targetHistory.copy(searchTarget, linkedList);
        }

        public final StoreSearch.SearchTarget component1() {
            return this.searchTarget;
        }

        public final LinkedList<List<QueryNode>> component2() {
            return this.recentQueries;
        }

        public final TargetHistory copy(StoreSearch.SearchTarget searchTarget, LinkedList<List<QueryNode>> linkedList) {
            m.checkNotNullParameter(searchTarget, "searchTarget");
            m.checkNotNullParameter(linkedList, "recentQueries");
            return new TargetHistory(searchTarget, linkedList);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof TargetHistory)) {
                return false;
            }
            TargetHistory targetHistory = (TargetHistory) obj;
            return m.areEqual(this.searchTarget, targetHistory.searchTarget) && m.areEqual(this.recentQueries, targetHistory.recentQueries);
        }

        public final LinkedList<List<QueryNode>> getRecentQueries() {
            return this.recentQueries;
        }

        public final StoreSearch.SearchTarget getSearchTarget() {
            return this.searchTarget;
        }

        public int hashCode() {
            StoreSearch.SearchTarget searchTarget = this.searchTarget;
            int i = 0;
            int hashCode = (searchTarget != null ? searchTarget.hashCode() : 0) * 31;
            LinkedList<List<QueryNode>> linkedList = this.recentQueries;
            if (linkedList != null) {
                i = linkedList.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("TargetHistory(searchTarget=");
            R.append(this.searchTarget);
            R.append(", recentQueries=");
            R.append(this.recentQueries);
            R.append(")");
            return R.toString();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final LinkedList<List<QueryNode>> find(LinkedList<TargetHistory> linkedList, StoreSearch.SearchTarget searchTarget) {
        Iterator<TargetHistory> it = linkedList.iterator();
        m.checkNotNullExpressionValue(it, "iterator()");
        while (it.hasNext()) {
            TargetHistory next = it.next();
            m.checkNotNullExpressionValue(next, "iter.next()");
            TargetHistory targetHistory = next;
            if (m.areEqual(targetHistory.getSearchTarget(), searchTarget)) {
                return targetHistory.getRecentQueries();
            }
        }
        return new LinkedList<>();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final LinkedList<TargetHistory> putAndCopy(LinkedList<TargetHistory> linkedList, StoreSearch.SearchTarget searchTarget, List<? extends QueryNode> list) {
        TargetHistory targetHistory;
        Iterator<TargetHistory> it = linkedList.iterator();
        m.checkNotNullExpressionValue(it, "iterator()");
        while (true) {
            if (!it.hasNext()) {
                targetHistory = null;
                break;
            }
            TargetHistory next = it.next();
            m.checkNotNullExpressionValue(next, "targetsIter.next()");
            targetHistory = next;
            if (m.areEqual(targetHistory.getSearchTarget(), searchTarget)) {
                it.remove();
                break;
            }
        }
        if (targetHistory == null) {
            targetHistory = new TargetHistory(searchTarget, new LinkedList());
        }
        Iterator<List<QueryNode>> it2 = targetHistory.getRecentQueries().iterator();
        m.checkNotNullExpressionValue(it2, "targetHistory.recentQueries.iterator()");
        while (it2.hasNext()) {
            List<QueryNode> next2 = it2.next();
            m.checkNotNullExpressionValue(next2, "queriesIter.next()");
            if (m.areEqual(next2, list)) {
                it2.remove();
            }
        }
        targetHistory.getRecentQueries().push(list);
        linkedList.push(new TargetHistory(searchTarget, new LinkedList(u.take(targetHistory.getRecentQueries(), this.MAX_QUERIES_PER_TARGET))));
        return new LinkedList<>(u.take(linkedList, this.MAX_SEARCH_TARGETS));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final LinkedList<TargetHistory> removeAndCopy(LinkedList<TargetHistory> linkedList, StoreSearch.SearchTarget searchTarget) {
        Iterator<TargetHistory> it = linkedList.iterator();
        m.checkNotNullExpressionValue(it, "iterator()");
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            TargetHistory next = it.next();
            m.checkNotNullExpressionValue(next, "iter.next()");
            if (m.areEqual(next.getSearchTarget(), searchTarget)) {
                it.remove();
                break;
            }
        }
        return new LinkedList<>(linkedList);
    }

    @Override // com.discord.utilities.search.history.SearchHistoryCache
    public void clear(final StoreSearch.SearchTarget searchTarget) {
        m.checkNotNullParameter(searchTarget, "searchTarget");
        Observable<R> F = this.backingCache.getObservable().Z(1).F(new b<LinkedList<TargetHistory>, LinkedList<TargetHistory>>() { // from class: com.discord.utilities.search.history.MGPreferenceSearchHistoryCache$clear$1
            public final LinkedList<MGPreferenceSearchHistoryCache.TargetHistory> call(LinkedList<MGPreferenceSearchHistoryCache.TargetHistory> linkedList) {
                LinkedList<MGPreferenceSearchHistoryCache.TargetHistory> removeAndCopy;
                MGPreferenceSearchHistoryCache mGPreferenceSearchHistoryCache = MGPreferenceSearchHistoryCache.this;
                m.checkNotNullExpressionValue(linkedList, "it");
                removeAndCopy = mGPreferenceSearchHistoryCache.removeAndCopy(linkedList, searchTarget);
                return removeAndCopy;
            }
        });
        m.checkNotNullExpressionValue(F, "backingCache\n        .ge…veAndCopy(searchTarget) }");
        ObservableExtensionsKt.appSubscribe(F, (r18 & 1) != 0 ? null : null, "clear history", (r18 & 4) != 0 ? null : null, new MGPreferenceSearchHistoryCache$clear$2(this), (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
    }

    @Override // com.discord.utilities.search.history.SearchHistoryCache
    public Observable<Collection<List<QueryNode>>> getHistory(final StoreSearch.SearchTarget searchTarget) {
        m.checkNotNullParameter(searchTarget, "searchTarget");
        Observable F = this.backingCache.getObservable().F(new b<LinkedList<TargetHistory>, Collection<? extends List<? extends QueryNode>>>() { // from class: com.discord.utilities.search.history.MGPreferenceSearchHistoryCache$getHistory$1
            public final Collection<List<QueryNode>> call(LinkedList<MGPreferenceSearchHistoryCache.TargetHistory> linkedList) {
                LinkedList find;
                MGPreferenceSearchHistoryCache mGPreferenceSearchHistoryCache = MGPreferenceSearchHistoryCache.this;
                m.checkNotNullExpressionValue(linkedList, "it");
                find = mGPreferenceSearchHistoryCache.find(linkedList, searchTarget);
                return find;
            }
        });
        m.checkNotNullExpressionValue(F, "backingCache\n        .ge…{ it.find(searchTarget) }");
        return F;
    }

    @Override // com.discord.utilities.search.history.SearchHistoryCache
    public void persistQuery(final StoreSearch.SearchTarget searchTarget, final List<? extends QueryNode> list) {
        m.checkNotNullParameter(searchTarget, "searchTarget");
        m.checkNotNullParameter(list, "query");
        Observable<R> F = this.backingCache.getObservable().Z(1).F(new b<LinkedList<TargetHistory>, LinkedList<TargetHistory>>() { // from class: com.discord.utilities.search.history.MGPreferenceSearchHistoryCache$persistQuery$1
            public final LinkedList<MGPreferenceSearchHistoryCache.TargetHistory> call(LinkedList<MGPreferenceSearchHistoryCache.TargetHistory> linkedList) {
                LinkedList<MGPreferenceSearchHistoryCache.TargetHistory> putAndCopy;
                MGPreferenceSearchHistoryCache mGPreferenceSearchHistoryCache = MGPreferenceSearchHistoryCache.this;
                m.checkNotNullExpressionValue(linkedList, "it");
                putAndCopy = mGPreferenceSearchHistoryCache.putAndCopy(linkedList, searchTarget, list);
                return putAndCopy;
            }
        });
        m.checkNotNullExpressionValue(F, "backingCache\n        .ge…py(searchTarget, query) }");
        ObservableExtensionsKt.appSubscribe(F, (r18 & 1) != 0 ? null : null, "persist query", (r18 & 4) != 0 ? null : null, new MGPreferenceSearchHistoryCache$persistQuery$2(this), (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
    }
}
