package com.discord.utilities.search.suggestion;

import androidx.core.app.NotificationCompat;
import j0.k.b;
import kotlin.Metadata;
/* compiled from: SearchSuggestionEngine.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/CharSequence;)Ljava/lang/String;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SearchSuggestionEngine$setupMemberRequestSubscription$1<T, R> implements b<CharSequence, String> {
    public static final SearchSuggestionEngine$setupMemberRequestSubscription$1 INSTANCE = new SearchSuggestionEngine$setupMemberRequestSubscription$1();

    public final String call(CharSequence charSequence) {
        return charSequence.toString();
    }
}
