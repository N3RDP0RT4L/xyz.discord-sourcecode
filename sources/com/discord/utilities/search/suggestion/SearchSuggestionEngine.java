package com.discord.utilities.search.suggestion;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.permission.Permission;
import com.discord.models.guild.UserGuildMember;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.search.query.FilterType;
import com.discord.utilities.search.query.node.QueryNode;
import com.discord.utilities.search.query.node.answer.HasAnswerOption;
import com.discord.utilities.search.query.node.content.ContentNode;
import com.discord.utilities.search.query.node.filter.FilterNode;
import com.discord.utilities.search.strings.SearchStringProvider;
import com.discord.utilities.search.suggestion.entries.ChannelSuggestion;
import com.discord.utilities.search.suggestion.entries.FilterSuggestion;
import com.discord.utilities.search.suggestion.entries.HasSuggestion;
import com.discord.utilities.search.suggestion.entries.RecentQuerySuggestion;
import com.discord.utilities.search.suggestion.entries.SearchSuggestion;
import com.discord.utilities.search.suggestion.entries.UserSuggestion;
import com.discord.utilities.search.validation.SearchData;
import d0.g0.w;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import rx.Observable;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
/* compiled from: SearchSuggestionEngine.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0088\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u000b\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b?\u00100JG\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\u00022\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00072\u0012\u0010\n\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00020\tH\u0007¢\u0006\u0004\b\f\u0010\rJ)\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000b0\t2\u0012\u0010\n\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00020\tH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ-\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u000b0\u00022\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u00122\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J9\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u001b0\t2\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0016\u001a\u00020\u00122\u0012\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u00190\u0017H\u0002¢\u0006\u0004\b\u001c\u0010\u001dJU\u0010$\u001a\b\u0012\u0004\u0012\u00020#0\u00022\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0016\u001a\u00020\u00122\u0012\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u001e0\u00172\u001a\u0010\"\u001a\u0016\u0012\b\u0012\u00060\u0018j\u0002` \u0012\b\u0012\u00060\u0018j\u0002`!0\u0017H\u0002¢\u0006\u0004\b$\u0010%J-\u0010(\u001a\b\u0012\u0004\u0012\u00020\u000b0\u00022\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010'\u001a\u00020&H\u0002¢\u0006\u0004\b(\u0010)J\u001d\u0010*\u001a\u00020\u00102\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0002¢\u0006\u0004\b*\u0010+J\u001f\u0010,\u001a\u0004\u0018\u00010\u00122\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0002¢\u0006\u0004\b,\u0010-J\u000f\u0010/\u001a\u00020.H\u0002¢\u0006\u0004\b/\u00100R2\u00103\u001a\u001e\u0012\f\u0012\n 2*\u0004\u0018\u00010\u00100\u0010\u0012\f\u0012\n 2*\u0004\u0018\u00010\u00100\u0010018\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b3\u00104R\u0016\u00106\u001a\u0002058\u0002@\u0002X\u0082D¢\u0006\u0006\n\u0004\b6\u00107R\u0016\u00108\u001a\u0002058\u0002@\u0002X\u0082D¢\u0006\u0006\n\u0004\b8\u00107R$\u00109\u001a\u0004\u0018\u00010\u00188\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b9\u0010:\u001a\u0004\b;\u0010<\"\u0004\b=\u0010>¨\u0006@"}, d2 = {"Lcom/discord/utilities/search/suggestion/SearchSuggestionEngine;", "", "", "Lcom/discord/utilities/search/query/node/QueryNode;", "input", "Lcom/discord/utilities/search/validation/SearchData;", "searchData", "Lcom/discord/utilities/search/strings/SearchStringProvider;", "searchStringProvider", "", "recentQueries", "Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion;", "getSuggestions", "(Ljava/util/List;Lcom/discord/utilities/search/validation/SearchData;Lcom/discord/utilities/search/strings/SearchStringProvider;Ljava/util/Collection;)Ljava/util/List;", "getHistorySuggestions", "(Ljava/util/Collection;)Ljava/util/Collection;", "", "rawContent", "Lcom/discord/utilities/search/query/FilterType;", "currentFilterType", "getHasSuggestions", "(Ljava/lang/CharSequence;Lcom/discord/utilities/search/query/FilterType;Lcom/discord/utilities/search/strings/SearchStringProvider;)Ljava/util/List;", "currentFilter", "", "", "Lcom/discord/models/guild/UserGuildMember;", "users", "Lcom/discord/utilities/search/suggestion/entries/UserSuggestion;", "getUserSuggestions", "(Ljava/lang/CharSequence;Lcom/discord/utilities/search/query/FilterType;Ljava/util/Map;)Ljava/util/Collection;", "Lcom/discord/api/channel/Channel;", "channels", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/permission/PermissionBit;", "channelPermissions", "Lcom/discord/utilities/search/suggestion/entries/ChannelSuggestion;", "getChannelSuggestions", "(Ljava/lang/CharSequence;Lcom/discord/utilities/search/query/FilterType;Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;", "", "includeInChannels", "getFilterSuggestions", "(Ljava/lang/CharSequence;Lcom/discord/utilities/search/strings/SearchStringProvider;Z)Ljava/util/List;", "getRawContent", "(Ljava/util/List;)Ljava/lang/CharSequence;", "getCurrentFilterType", "(Ljava/util/List;)Lcom/discord/utilities/search/query/FilterType;", "", "setupMemberRequestSubscription", "()V", "Lrx/subjects/SerializedSubject;", "kotlin.jvm.PlatformType", "membersRequestSubject", "Lrx/subjects/SerializedSubject;", "", "MAX_USER_SORTING_THRESHOLD", "I", "MAX_ENTRY_TYPE_COUNT", "targetGuildId", "Ljava/lang/Long;", "getTargetGuildId", "()Ljava/lang/Long;", "setTargetGuildId", "(Ljava/lang/Long;)V", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SearchSuggestionEngine {
    public static final SearchSuggestionEngine INSTANCE;
    private static final int MAX_ENTRY_TYPE_COUNT = 10;
    private static final int MAX_USER_SORTING_THRESHOLD = 100;
    private static final SerializedSubject<CharSequence, CharSequence> membersRequestSubject = new SerializedSubject<>(PublishSubject.k0());
    private static Long targetGuildId;

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            FilterType.values();
            int[] iArr = new int[4];
            $EnumSwitchMapping$0 = iArr;
            iArr[FilterType.FROM.ordinal()] = 1;
            iArr[FilterType.MENTIONS.ordinal()] = 2;
        }
    }

    static {
        SearchSuggestionEngine searchSuggestionEngine = new SearchSuggestionEngine();
        INSTANCE = searchSuggestionEngine;
        searchSuggestionEngine.setupMemberRequestSubscription();
    }

    private SearchSuggestionEngine() {
    }

    private final List<ChannelSuggestion> getChannelSuggestions(CharSequence charSequence, FilterType filterType, Map<Long, Channel> map, Map<Long, Long> map2) {
        if (filterType != FilterType.IN) {
            return n.emptyList();
        }
        Collection<Channel> values = map.values();
        ArrayList arrayList = new ArrayList();
        for (Object obj : values) {
            if (PermissionUtils.can(Permission.VIEW_CHANNEL, (Long) a.c((Channel) obj, map2))) {
                arrayList.add(obj);
            }
        }
        ArrayList arrayList2 = new ArrayList();
        for (Object obj2 : arrayList) {
            if (ChannelSuggestion.Companion.canComplete(ChannelUtils.c((Channel) obj2), charSequence)) {
                arrayList2.add(obj2);
            }
        }
        List<Channel> sortedWith = u.sortedWith(arrayList2, ChannelUtils.h(Channel.Companion));
        ArrayList arrayList3 = new ArrayList(o.collectionSizeOrDefault(sortedWith, 10));
        for (Channel channel : sortedWith) {
            arrayList3.add(new ChannelSuggestion(ChannelUtils.c(channel), channel.h()));
        }
        return u.take(arrayList3, MAX_ENTRY_TYPE_COUNT);
    }

    private final FilterType getCurrentFilterType(List<? extends QueryNode> list) {
        if (list.isEmpty()) {
            return null;
        }
        QueryNode queryNode = (QueryNode) u.last((List<? extends Object>) list);
        if (queryNode instanceof FilterNode) {
            return ((FilterNode) queryNode).getFilterType();
        }
        if (list.size() == 1) {
            return null;
        }
        QueryNode queryNode2 = list.get(n.getLastIndex(list) - 1);
        if (!(queryNode instanceof ContentNode) || !(queryNode2 instanceof FilterNode)) {
            return null;
        }
        return ((FilterNode) queryNode2).getFilterType();
    }

    private final List<SearchSuggestion> getFilterSuggestions(CharSequence charSequence, SearchStringProvider searchStringProvider, boolean z2) {
        FilterType[] values = FilterType.values();
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < 4; i++) {
            FilterType filterType = values[i];
            if (z2 || filterType != FilterType.IN) {
                arrayList.add(filterType);
            }
        }
        ArrayList<FilterType> arrayList2 = new ArrayList();
        for (Object obj : arrayList) {
            if (FilterSuggestion.Companion.canComplete(charSequence, (FilterType) obj, searchStringProvider)) {
                arrayList2.add(obj);
            }
        }
        ArrayList arrayList3 = new ArrayList(o.collectionSizeOrDefault(arrayList2, 10));
        for (FilterType filterType2 : arrayList2) {
            arrayList3.add(new FilterSuggestion(filterType2));
        }
        return arrayList3;
    }

    private final List<SearchSuggestion> getHasSuggestions(CharSequence charSequence, FilterType filterType, SearchStringProvider searchStringProvider) {
        if (filterType != FilterType.HAS) {
            return n.emptyList();
        }
        HasAnswerOption[] values = HasAnswerOption.values();
        ArrayList<HasAnswerOption> arrayList = new ArrayList();
        for (int i = 0; i < 7; i++) {
            HasAnswerOption hasAnswerOption = values[i];
            if (HasSuggestion.Companion.canComplete(charSequence, hasAnswerOption, searchStringProvider)) {
                arrayList.add(hasAnswerOption);
            }
        }
        ArrayList arrayList2 = new ArrayList();
        for (HasAnswerOption hasAnswerOption2 : arrayList) {
            arrayList2.add(new HasSuggestion(hasAnswerOption2));
        }
        return arrayList2;
    }

    private final Collection<SearchSuggestion> getHistorySuggestions(Collection<? extends List<? extends QueryNode>> collection) {
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(collection, 10));
        Iterator<T> it = collection.iterator();
        while (it.hasNext()) {
            arrayList.add(new RecentQuerySuggestion((List) it.next()));
        }
        return arrayList;
    }

    private final CharSequence getRawContent(List<? extends QueryNode> list) {
        if (list.isEmpty()) {
            return "";
        }
        QueryNode queryNode = (QueryNode) u.last((List<? extends Object>) list);
        if (!(queryNode instanceof ContentNode)) {
            return "";
        }
        String obj = ((ContentNode) queryNode).getContent().toString();
        Objects.requireNonNull(obj, "null cannot be cast to non-null type kotlin.CharSequence");
        return w.trim(obj).toString();
    }

    public static final List<SearchSuggestion> getSuggestions(List<? extends QueryNode> list, SearchData searchData, SearchStringProvider searchStringProvider, Collection<? extends List<? extends QueryNode>> collection) {
        m.checkNotNullParameter(list, "input");
        m.checkNotNullParameter(searchData, "searchData");
        m.checkNotNullParameter(searchStringProvider, "searchStringProvider");
        m.checkNotNullParameter(collection, "recentQueries");
        ArrayList arrayList = new ArrayList();
        SearchSuggestionEngine searchSuggestionEngine = INSTANCE;
        FilterType currentFilterType = searchSuggestionEngine.getCurrentFilterType(list);
        CharSequence rawContent = searchSuggestionEngine.getRawContent(list);
        if (currentFilterType != null) {
            arrayList.addAll(searchSuggestionEngine.getUserSuggestions(rawContent, currentFilterType, searchData.getUsers()));
            arrayList.addAll(searchSuggestionEngine.getChannelSuggestions(rawContent, currentFilterType, searchData.getChannels(), searchData.getChannelPermissions()));
            arrayList.addAll(searchSuggestionEngine.getHasSuggestions(rawContent, currentFilterType, searchStringProvider));
        } else {
            arrayList.addAll(searchSuggestionEngine.getFilterSuggestions(rawContent, searchStringProvider, !searchData.getChannels().isEmpty()));
        }
        if (list.isEmpty()) {
            arrayList.addAll(searchSuggestionEngine.getHistorySuggestions(collection));
        }
        return arrayList;
    }

    private final Collection<UserSuggestion> getUserSuggestions(CharSequence charSequence, FilterType filterType, Map<Long, UserGuildMember> map) {
        UserSuggestion.TargetType targetType;
        int ordinal = filterType.ordinal();
        boolean z2 = true;
        if (ordinal == 0) {
            targetType = UserSuggestion.TargetType.FROM;
        } else if (ordinal != 1) {
            return n.emptyList();
        } else {
            targetType = UserSuggestion.TargetType.MENTIONS;
        }
        membersRequestSubject.k.onNext(charSequence);
        TreeSet treeSet = new TreeSet();
        if (map.size() >= MAX_USER_SORTING_THRESHOLD) {
            z2 = false;
        }
        for (Map.Entry<Long, UserGuildMember> entry : map.entrySet()) {
            UserGuildMember value = entry.getValue();
            User user = value.getUser();
            GuildMember guildMember = value.getGuildMember();
            if (UserSuggestion.Companion.canComplete(user.getUsername(), user.getDiscriminator(), value.getNickname(), charSequence)) {
                treeSet.add(new UserSuggestion(user, targetType, guildMember));
                if (!z2 && treeSet.size() >= MAX_ENTRY_TYPE_COUNT) {
                    return treeSet;
                }
            }
        }
        return u.take(treeSet, MAX_ENTRY_TYPE_COUNT);
    }

    private final void setupMemberRequestSubscription() {
        Observable<R> F = membersRequestSubject.O(750L, TimeUnit.MILLISECONDS).F(SearchSuggestionEngine$setupMemberRequestSubscription$1.INSTANCE);
        final SearchSuggestionEngine$setupMemberRequestSubscription$2 searchSuggestionEngine$setupMemberRequestSubscription$2 = SearchSuggestionEngine$setupMemberRequestSubscription$2.INSTANCE;
        Object obj = searchSuggestionEngine$setupMemberRequestSubscription$2;
        if (searchSuggestionEngine$setupMemberRequestSubscription$2 != null) {
            obj = new b() { // from class: com.discord.utilities.search.suggestion.SearchSuggestionEngine$sam$rx_functions_Func1$0
                @Override // j0.k.b
                public final /* synthetic */ Object call(Object obj2) {
                    return Function1.this.invoke(obj2);
                }
            };
        }
        Observable q = F.x((b) obj).q();
        m.checkNotNullExpressionValue(q, "membersRequestSubject\n  …  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(q, SearchSuggestionEngine.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, SearchSuggestionEngine$setupMemberRequestSubscription$3.INSTANCE);
    }

    public final Long getTargetGuildId() {
        return targetGuildId;
    }

    public final void setTargetGuildId(Long l) {
        targetGuildId = l;
    }
}
