package com.discord.utilities.search.suggestion;

import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: SearchSuggestionEngine.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "p1", "", "invoke", "(Ljava/lang/String;)Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class SearchSuggestionEngine$setupMemberRequestSubscription$2 extends k implements Function1<CharSequence, Boolean> {
    public static final SearchSuggestionEngine$setupMemberRequestSubscription$2 INSTANCE = new SearchSuggestionEngine$setupMemberRequestSubscription$2();

    public SearchSuggestionEngine$setupMemberRequestSubscription$2() {
        super(1, d0.g0.k.class, "isNotEmpty", "isNotEmpty(Ljava/lang/CharSequence;)Z", 1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Boolean invoke(CharSequence charSequence) {
        return Boolean.valueOf(invoke((String) charSequence));
    }

    public final boolean invoke(String str) {
        m.checkNotNullParameter(str, "p1");
        return str.length() > 0;
    }
}
