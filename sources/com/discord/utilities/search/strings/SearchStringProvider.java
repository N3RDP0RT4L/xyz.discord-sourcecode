package com.discord.utilities.search.strings;

import kotlin.Metadata;
/* compiled from: SearchStringProvider.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0018\bf\u0018\u00002\u00020\u0001R\u0016\u0010\u0005\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0004R\u0016\u0010\t\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\u0004R\u0016\u0010\u000b\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u0004R\u0016\u0010\r\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\u0004R\u0016\u0010\u000f\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u0004R\u0016\u0010\u0011\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u0004R\u0016\u0010\u0013\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0004R\u0016\u0010\u0015\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0004R\u0016\u0010\u0017\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u0004R\u0016\u0010\u0019\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0018\u0010\u0004¨\u0006\u001a"}, d2 = {"Lcom/discord/utilities/search/strings/SearchStringProvider;", "", "", "getStickerAnswerString", "()Ljava/lang/String;", "stickerAnswerString", "getEmbedAnswerString", "embedAnswerString", "getInFilterString", "inFilterString", "getLinkAnswerString", "linkAnswerString", "getFromFilterString", "fromFilterString", "getMentionsFilterString", "mentionsFilterString", "getVideoAnswerString", "videoAnswerString", "getHasFilterString", "hasFilterString", "getImageAnswerString", "imageAnswerString", "getSoundAnswerString", "soundAnswerString", "getFileAnswerString", "fileAnswerString", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public interface SearchStringProvider {
    String getEmbedAnswerString();

    String getFileAnswerString();

    String getFromFilterString();

    String getHasFilterString();

    String getImageAnswerString();

    String getInFilterString();

    String getLinkAnswerString();

    String getMentionsFilterString();

    String getSoundAnswerString();

    String getStickerAnswerString();

    String getVideoAnswerString();
}
