package com.discord.utilities.search.network.state;

import andhook.lib.HookHelper;
import kotlin.Metadata;
/* compiled from: QueryFetchState.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\t\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\t¨\u0006\n"}, d2 = {"Lcom/discord/utilities/search/network/state/QueryFetchState;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "NONE", "INDEXING", "IN_PROGRESS", "LOADING_MORE", "COMPLETED", "FAILED", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public enum QueryFetchState {
    NONE,
    INDEXING,
    IN_PROGRESS,
    LOADING_MORE,
    COMPLETED,
    FAILED
}
