package com.discord.utilities.search.network;

import andhook.lib.HookHelper;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelSearchResponse;
import com.discord.stores.StoreSearch;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import d0.z.d.m;
import j0.k.b;
import j0.l.a.j;
import j0.l.a.y;
import j0.l.e.e;
import j0.l.e.k;
import j0.p.a;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func0;
/* compiled from: SearchFetcher.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0016\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u0013\u0010\u0014J/\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0013\u0010\r\u001a\u00020\f*\u00020\tH\u0002¢\u0006\u0004\b\r\u0010\u000eJ/\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\u000f\u0010\u000bR\u0016\u0010\u0011\u001a\u00020\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012¨\u0006\u0015"}, d2 = {"Lcom/discord/utilities/search/network/SearchFetcher;", "", "Lcom/discord/stores/StoreSearch$SearchTarget;", "searchTarget", "", "oldestMessageId", "Lcom/discord/utilities/search/network/SearchQuery;", "searchQuery", "Lrx/Observable;", "Lcom/discord/models/domain/ModelSearchResponse;", "getRestObservable", "(Lcom/discord/stores/StoreSearch$SearchTarget;Ljava/lang/Long;Lcom/discord/utilities/search/network/SearchQuery;)Lrx/Observable;", "", "isIndexing", "(Lcom/discord/models/domain/ModelSearchResponse;)Z", "makeQuery", "Ljava/util/concurrent/atomic/AtomicInteger;", "indexingRetryCount", "Ljava/util/concurrent/atomic/AtomicInteger;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public class SearchFetcher {
    private AtomicInteger indexingRetryCount = new AtomicInteger(0);

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            StoreSearch.SearchTarget.Type.values();
            int[] iArr = new int[2];
            $EnumSwitchMapping$0 = iArr;
            iArr[StoreSearch.SearchTarget.Type.GUILD.ordinal()] = 1;
            iArr[StoreSearch.SearchTarget.Type.CHANNEL.ordinal()] = 2;
        }
    }

    private final Observable<ModelSearchResponse> getRestObservable(final StoreSearch.SearchTarget searchTarget, final Long l, final SearchQuery searchQuery) {
        SearchFetcher$getRestObservable$1 searchFetcher$getRestObservable$1 = SearchFetcher$getRestObservable$1.INSTANCE;
        final Map<String, List<String>> params = searchQuery.getParams();
        Observable z2 = Observable.h0(new j(new Func0<Observable<Integer>>() { // from class: com.discord.utilities.search.network.SearchFetcher$getRestObservable$2
            @Override // rx.functions.Func0, java.util.concurrent.Callable
            public final Observable<Integer> call() {
                AtomicInteger atomicInteger;
                SearchFetcher$getRestObservable$1 searchFetcher$getRestObservable$12 = SearchFetcher$getRestObservable$1.INSTANCE;
                atomicInteger = SearchFetcher.this.indexingRetryCount;
                return new k(searchFetcher$getRestObservable$12.invoke(atomicInteger));
            }
        })).z(new b<Integer, Observable<? extends ModelSearchResponse>>() { // from class: com.discord.utilities.search.network.SearchFetcher$getRestObservable$3
            public final Observable<? extends ModelSearchResponse> call(Integer num) {
                int ordinal = StoreSearch.SearchTarget.this.getType().ordinal();
                if (ordinal == 0) {
                    return RestAPI.Companion.getApi().searchGuildMessages(StoreSearch.SearchTarget.this.getId(), l, (List) params.get("author_id"), (List) params.get("mentions"), (List) params.get(ModelAuditLogEntry.CHANGE_KEY_CHANNEL_ID), (List) params.get("has"), (List) params.get("content"), num, Boolean.valueOf(searchQuery.getIncludeNsfw()));
                }
                if (ordinal == 1) {
                    return RestAPI.Companion.getApi().searchChannelMessages(StoreSearch.SearchTarget.this.getId(), l, (List) params.get("author_id"), (List) params.get("mentions"), (List) params.get("has"), (List) params.get("content"), num, Boolean.valueOf(searchQuery.getIncludeNsfw()));
                }
                throw new NoWhenBranchMatchedException();
            }
        });
        m.checkNotNullExpressionValue(z2, "Observable\n        .defe…  )\n          }\n        }");
        return ObservableExtensionsKt.restSubscribeOn$default(z2, false, 1, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final boolean isIndexing(ModelSearchResponse modelSearchResponse) {
        Integer errorCode = modelSearchResponse.getErrorCode();
        return errorCode != null && errorCode.intValue() == 111000;
    }

    public Observable<ModelSearchResponse> makeQuery(StoreSearch.SearchTarget searchTarget, Long l, SearchQuery searchQuery) {
        m.checkNotNullParameter(searchTarget, "searchTarget");
        m.checkNotNullParameter(searchQuery, "searchQuery");
        final AtomicLong atomicLong = new AtomicLong();
        Observable<ModelSearchResponse> t = getRestObservable(searchTarget, l, searchQuery).t(new Action1<ModelSearchResponse>() { // from class: com.discord.utilities.search.network.SearchFetcher$makeQuery$1
            public final void call(ModelSearchResponse modelSearchResponse) {
                boolean isIndexing;
                AtomicInteger atomicInteger;
                SearchFetcher searchFetcher = SearchFetcher.this;
                m.checkNotNullExpressionValue(modelSearchResponse, "searchResponse");
                isIndexing = searchFetcher.isIndexing(modelSearchResponse);
                if (isIndexing) {
                    atomicLong.set(modelSearchResponse.getRetryMillis());
                    atomicInteger = SearchFetcher.this.indexingRetryCount;
                    atomicInteger.incrementAndGet();
                }
            }
        });
        e.c cVar = new e.c(new b<Observable<? extends Void>, Observable<?>>() { // from class: com.discord.utilities.search.network.SearchFetcher$makeQuery$2
            public final Observable<?> call(Observable<? extends Void> observable) {
                return (Observable<R>) observable.z(new b<Void, Observable<? extends Long>>() { // from class: com.discord.utilities.search.network.SearchFetcher$makeQuery$2.1
                    public final Observable<? extends Long> call(Void r3) {
                        return Observable.d0(atomicLong.get(), TimeUnit.MILLISECONDS);
                    }
                });
            }
        });
        AtomicReference<a> atomicReference = a.a;
        Observable<ModelSearchResponse> b02 = Observable.h0(new y(t, cVar, false, true, j0.l.c.m.a)).b0(new b<ModelSearchResponse, Boolean>() { // from class: com.discord.utilities.search.network.SearchFetcher$makeQuery$3
            public final Boolean call(ModelSearchResponse modelSearchResponse) {
                boolean isIndexing;
                SearchFetcher searchFetcher = SearchFetcher.this;
                m.checkNotNullExpressionValue(modelSearchResponse, "it");
                isIndexing = searchFetcher.isIndexing(modelSearchResponse);
                return Boolean.valueOf(!isIndexing);
            }
        });
        m.checkNotNullExpressionValue(b02, "getRestObservable(search…ntil { !it.isIndexing() }");
        return b02;
    }
}
