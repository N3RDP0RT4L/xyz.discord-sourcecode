package com.discord.utilities.search.network;

import d0.z.d.m;
import d0.z.d.o;
import java.util.concurrent.atomic.AtomicInteger;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: SearchFetcher.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0003\u0010\u0004\u001a\u0004\u0018\u00010\u0001*\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Ljava/util/concurrent/atomic/AtomicInteger;", "", "invoke", "(Ljava/util/concurrent/atomic/AtomicInteger;)Ljava/lang/Integer;", "getNullIfZero"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SearchFetcher$getRestObservable$1 extends o implements Function1<AtomicInteger, Integer> {
    public static final SearchFetcher$getRestObservable$1 INSTANCE = new SearchFetcher$getRestObservable$1();

    public SearchFetcher$getRestObservable$1() {
        super(1);
    }

    public final Integer invoke(AtomicInteger atomicInteger) {
        m.checkNotNullParameter(atomicInteger, "$this$getNullIfZero");
        int i = atomicInteger.get();
        if (i != 0) {
            return Integer.valueOf(i);
        }
        return null;
    }
}
