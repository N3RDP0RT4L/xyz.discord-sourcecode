package com.discord.utilities.search.query.node.filter;

import andhook.lib.HookHelper;
import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.utilities.font.FontUtils;
import com.discord.utilities.search.query.FilterType;
import com.discord.utilities.search.query.node.QueryNode;
import com.discord.utilities.spans.TypefaceSpanCompat;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: FilterNode.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u000f\u001a\u00020\t\u0012\u0006\u0010\u0010\u001a\u00020\f¢\u0006\u0004\b\"\u0010#J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ$\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u000f\u001a\u00020\t2\b\b\u0002\u0010\u0010\u001a\u00020\fHÆ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0017\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u001a\u0010\u001c\u001a\u00020\u001b2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0019HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u001c\u0010\u0010\u001a\u00020\f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0010\u0010\u001e\u001a\u0004\b\u001f\u0010\u000eR\u0019\u0010\u000f\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010 \u001a\u0004\b!\u0010\u000b¨\u0006$"}, d2 = {"Lcom/discord/utilities/search/query/node/filter/FilterNode;", "Lcom/discord/utilities/search/query/node/QueryNode;", "Landroid/text/SpannableStringBuilder;", "builder", "Landroid/content/Context;", "renderContext", "", "render", "(Landroid/text/SpannableStringBuilder;Landroid/content/Context;)V", "Lcom/discord/utilities/search/query/FilterType;", "component1", "()Lcom/discord/utilities/search/query/FilterType;", "", "component2", "()Ljava/lang/CharSequence;", "filterType", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "copy", "(Lcom/discord/utilities/search/query/FilterType;Ljava/lang/CharSequence;)Lcom/discord/utilities/search/query/node/filter/FilterNode;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/CharSequence;", "getText", "Lcom/discord/utilities/search/query/FilterType;", "getFilterType", HookHelper.constructorName, "(Lcom/discord/utilities/search/query/FilterType;Ljava/lang/CharSequence;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class FilterNode extends QueryNode {
    private final FilterType filterType;
    private final CharSequence text;

    public FilterNode(FilterType filterType, CharSequence charSequence) {
        m.checkNotNullParameter(filterType, "filterType");
        m.checkNotNullParameter(charSequence, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
        this.filterType = filterType;
        this.text = charSequence;
    }

    public static /* synthetic */ FilterNode copy$default(FilterNode filterNode, FilterType filterType, CharSequence charSequence, int i, Object obj) {
        if ((i & 1) != 0) {
            filterType = filterNode.filterType;
        }
        if ((i & 2) != 0) {
            charSequence = filterNode.getText();
        }
        return filterNode.copy(filterType, charSequence);
    }

    public final FilterType component1() {
        return this.filterType;
    }

    public final CharSequence component2() {
        return getText();
    }

    public final FilterNode copy(FilterType filterType, CharSequence charSequence) {
        m.checkNotNullParameter(filterType, "filterType");
        m.checkNotNullParameter(charSequence, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
        return new FilterNode(filterType, charSequence);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof FilterNode)) {
            return false;
        }
        FilterNode filterNode = (FilterNode) obj;
        return m.areEqual(this.filterType, filterNode.filterType) && m.areEqual(getText(), filterNode.getText());
    }

    public final FilterType getFilterType() {
        return this.filterType;
    }

    @Override // com.discord.utilities.search.query.node.QueryNode
    public CharSequence getText() {
        return this.text;
    }

    public int hashCode() {
        FilterType filterType = this.filterType;
        int i = 0;
        int hashCode = (filterType != null ? filterType.hashCode() : 0) * 31;
        CharSequence text = getText();
        if (text != null) {
            i = text.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder R = a.R("FilterNode(filterType=");
        R.append(this.filterType);
        R.append(", text=");
        R.append(getText());
        R.append(")");
        return R.toString();
    }

    public void render(SpannableStringBuilder spannableStringBuilder, Context context) {
        m.checkNotNullParameter(spannableStringBuilder, "builder");
        m.checkNotNullParameter(context, "renderContext");
        Typeface themedFont = FontUtils.INSTANCE.getThemedFont(context, R.attr.font_primary_bold);
        TypefaceSpanCompat typefaceSpanCompat = themedFont != null ? new TypefaceSpanCompat(themedFont) : null;
        int length = spannableStringBuilder.length();
        StringBuilder sb = new StringBuilder();
        sb.append(getText());
        sb.append(MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR);
        spannableStringBuilder.append((CharSequence) sb.toString());
        spannableStringBuilder.setSpan(typefaceSpanCompat, length, spannableStringBuilder.length(), 33);
    }
}
