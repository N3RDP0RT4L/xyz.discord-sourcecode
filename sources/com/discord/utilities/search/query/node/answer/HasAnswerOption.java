package com.discord.utilities.search.query.node.answer;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.utilities.search.strings.SearchStringProvider;
import com.discord.widgets.chat.AutocompleteSelectionTypes;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.webrtc.MediaStreamTrack;
/* compiled from: HasNode.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0011\b\u0086\u0001\u0018\u0000 \r2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\rB\u0011\b\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0004¢\u0006\u0004\b\u000b\u0010\fJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0019\u0010\u0007\u001a\u00020\u00048\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\t\u0010\nj\u0002\b\u000ej\u0002\b\u000fj\u0002\b\u0010j\u0002\b\u0011j\u0002\b\u0012j\u0002\b\u0013j\u0002\b\u0014¨\u0006\u0015"}, d2 = {"Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;", "", "Lcom/discord/utilities/search/strings/SearchStringProvider;", "searchStringProvider", "", "getLocalizedInputText", "(Lcom/discord/utilities/search/strings/SearchStringProvider;)Ljava/lang/String;", "restParamValue", "Ljava/lang/String;", "getRestParamValue", "()Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;ILjava/lang/String;)V", "Companion", "LINK", "EMBED", "FILE", "VIDEO", "IMAGE", "SOUND", AutocompleteSelectionTypes.STICKER, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public enum HasAnswerOption {
    LINK("link"),
    EMBED("embed"),
    FILE("file"),
    VIDEO(MediaStreamTrack.VIDEO_TRACK_KIND),
    IMAGE("image"),
    SOUND("sound"),
    STICKER("sticker");
    
    public static final Companion Companion = new Companion(null);
    private final String restParamValue;

    /* compiled from: HasNode.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/utilities/search/query/node/answer/HasAnswerOption$Companion;", "", "", "match", "Lcom/discord/utilities/search/strings/SearchStringProvider;", "searchStringProvider", "Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;", "getOptionFromString", "(Ljava/lang/String;Lcom/discord/utilities/search/strings/SearchStringProvider;)Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final HasAnswerOption getOptionFromString(String str, SearchStringProvider searchStringProvider) {
            m.checkNotNullParameter(str, "match");
            m.checkNotNullParameter(searchStringProvider, "searchStringProvider");
            if (m.areEqual(str, searchStringProvider.getLinkAnswerString())) {
                return HasAnswerOption.LINK;
            }
            if (m.areEqual(str, searchStringProvider.getEmbedAnswerString())) {
                return HasAnswerOption.EMBED;
            }
            if (m.areEqual(str, searchStringProvider.getFileAnswerString())) {
                return HasAnswerOption.FILE;
            }
            if (m.areEqual(str, searchStringProvider.getVideoAnswerString())) {
                return HasAnswerOption.VIDEO;
            }
            if (m.areEqual(str, searchStringProvider.getImageAnswerString())) {
                return HasAnswerOption.IMAGE;
            }
            if (m.areEqual(str, searchStringProvider.getSoundAnswerString())) {
                return HasAnswerOption.SOUND;
            }
            if (m.areEqual(str, searchStringProvider.getStickerAnswerString())) {
                return HasAnswerOption.STICKER;
            }
            throw new IllegalArgumentException(a.v("invalid match: ", str));
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            HasAnswerOption.values();
            int[] iArr = new int[7];
            $EnumSwitchMapping$0 = iArr;
            iArr[HasAnswerOption.LINK.ordinal()] = 1;
            iArr[HasAnswerOption.EMBED.ordinal()] = 2;
            iArr[HasAnswerOption.FILE.ordinal()] = 3;
            iArr[HasAnswerOption.VIDEO.ordinal()] = 4;
            iArr[HasAnswerOption.IMAGE.ordinal()] = 5;
            iArr[HasAnswerOption.SOUND.ordinal()] = 6;
            iArr[HasAnswerOption.STICKER.ordinal()] = 7;
        }
    }

    HasAnswerOption(String str) {
        this.restParamValue = str;
    }

    public final String getLocalizedInputText(SearchStringProvider searchStringProvider) {
        m.checkNotNullParameter(searchStringProvider, "searchStringProvider");
        switch (ordinal()) {
            case 0:
                return searchStringProvider.getLinkAnswerString();
            case 1:
                return searchStringProvider.getEmbedAnswerString();
            case 2:
                return searchStringProvider.getFileAnswerString();
            case 3:
                return searchStringProvider.getVideoAnswerString();
            case 4:
                return searchStringProvider.getImageAnswerString();
            case 5:
                return searchStringProvider.getSoundAnswerString();
            case 6:
                return searchStringProvider.getStickerAnswerString();
            default:
                throw new NoWhenBranchMatchedException();
        }
    }

    public final String getRestParamValue() {
        return this.restParamValue;
    }
}
