package com.discord.utilities.search.query;

import andhook.lib.HookHelper;
import com.discord.widgets.chat.AutocompleteTypes;
import kotlin.Metadata;
/* compiled from: FilterType.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/discord/utilities/search/query/FilterType;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "FROM", AutocompleteTypes.MENTIONS, "HAS", "IN", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public enum FilterType {
    FROM,
    MENTIONS,
    HAS,
    IN
}
