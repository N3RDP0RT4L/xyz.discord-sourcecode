package com.discord.utilities.search.query.parsing;

import andhook.lib.HookHelper;
import android.content.Context;
import com.discord.simpleast.core.parser.ParseSpec;
import com.discord.simpleast.core.parser.Parser;
import com.discord.simpleast.core.parser.Rule;
import com.discord.utilities.search.query.FilterType;
import com.discord.utilities.search.query.node.QueryNode;
import com.discord.utilities.search.query.node.answer.ChannelNode;
import com.discord.utilities.search.query.node.answer.HasAnswerOption;
import com.discord.utilities.search.query.node.answer.HasNode;
import com.discord.utilities.search.query.node.answer.UserNode;
import com.discord.utilities.search.query.node.content.ContentNode;
import com.discord.utilities.search.query.node.filter.FilterNode;
import com.discord.utilities.search.strings.SearchStringProvider;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: QueryParser.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \t2\u0016\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0001:\u0001\tB\u000f\u0012\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\u0007\u0010\b¨\u0006\n"}, d2 = {"Lcom/discord/utilities/search/query/parsing/QueryParser;", "Lcom/discord/simpleast/core/parser/Parser;", "Landroid/content/Context;", "Lcom/discord/utilities/search/query/node/QueryNode;", "", "Lcom/discord/utilities/search/strings/SearchStringProvider;", "searchStringProvider", HookHelper.constructorName, "(Lcom/discord/utilities/search/strings/SearchStringProvider;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class QueryParser extends Parser<Context, QueryNode, Object> {
    private static final String ANY_TOKEN_REGEX = "^[\\s]*[\\S]*[\\s]*";
    public static final Companion Companion = new Companion(null);
    private static final String IN_ANSWER_REGEX = "(?:\\s*#([^ ]+))";
    private static final String USER_REGEX = "(?:\\s*([^@#:]+)#([0-9]{4}))";

    /* compiled from: QueryParser.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0012\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001e\u0010\u001fJ+\u0010\u0007\u001a\u0016\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0007\u0010\bJ#\u0010\t\u001a\u0016\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u0004H\u0002¢\u0006\u0004\b\t\u0010\nJ#\u0010\u000b\u001a\u0016\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u0004H\u0002¢\u0006\u0004\b\u000b\u0010\nJ\u0017\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\r\u001a\u00020\fH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J)\u0010\u0012\u001a\u0016\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u00042\u0006\u0010\u0011\u001a\u00020\u0002¢\u0006\u0004\b\u0012\u0010\bJ)\u0010\u0014\u001a\u0016\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u00042\u0006\u0010\u0013\u001a\u00020\u0002¢\u0006\u0004\b\u0014\u0010\bJ)\u0010\u0016\u001a\u0016\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u00042\u0006\u0010\u0015\u001a\u00020\u0002¢\u0006\u0004\b\u0016\u0010\bJ)\u0010\u0017\u001a\u0016\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u00042\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u0017\u0010\u0018J!\u0010\u0019\u001a\u0016\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u0004¢\u0006\u0004\b\u0019\u0010\nR\u0016\u0010\u001a\u001a\u00020\u000e8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001a\u0010\u001bR\u0016\u0010\u001c\u001a\u00020\u000e8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001c\u0010\u001bR\u0016\u0010\u001d\u001a\u00020\u000e8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001d\u0010\u001b¨\u0006 "}, d2 = {"Lcom/discord/utilities/search/query/parsing/QueryParser$Companion;", "", "", "localizedFrom", "Lcom/discord/simpleast/core/parser/Rule;", "Landroid/content/Context;", "Lcom/discord/utilities/search/query/node/QueryNode;", "getFromFilterRule", "(Ljava/lang/CharSequence;)Lcom/discord/simpleast/core/parser/Rule;", "getUserRule", "()Lcom/discord/simpleast/core/parser/Rule;", "getContentRule", "Lcom/discord/utilities/search/strings/SearchStringProvider;", "searchStringProvider", "", "createHasAnswerRegex", "(Lcom/discord/utilities/search/strings/SearchStringProvider;)Ljava/lang/String;", "localizedMentions", "getMentionsFilterRule", "localizedIn", "getInFilterRule", "localizedHas", "getHasFilterRule", "getHasAnswerRule", "(Lcom/discord/utilities/search/strings/SearchStringProvider;)Lcom/discord/simpleast/core/parser/Rule;", "getInAnswerRule", "ANY_TOKEN_REGEX", "Ljava/lang/String;", "IN_ANSWER_REGEX", "USER_REGEX", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        private final String createHasAnswerRegex(SearchStringProvider searchStringProvider) {
            HasAnswerOption.values();
            ArrayList arrayList = new ArrayList(7);
            HasAnswerOption[] values = HasAnswerOption.values();
            for (int i = 0; i < 7; i++) {
                arrayList.add(values[i].getLocalizedInputText(searchStringProvider));
            }
            StringBuilder sb = new StringBuilder("(?:\\s*(");
            int size = arrayList.size() - 1;
            for (int i2 = 0; i2 < size; i2++) {
                sb.append((CharSequence) arrayList.get(i2));
                sb.append('|');
            }
            sb.append((CharSequence) arrayList.get(arrayList.size() - 1));
            sb.append("))");
            String sb2 = sb.toString();
            m.checkNotNullExpressionValue(sb2, "builder.toString()");
            return sb2;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Rule<Context, QueryNode, Object> getContentRule() {
            final Pattern compile = Pattern.compile(QueryParser.ANY_TOKEN_REGEX, 64);
            m.checkNotNullExpressionValue(compile, "simpleTextPattern");
            return new Rule<Context, QueryNode, Object>(compile) { // from class: com.discord.utilities.search.query.parsing.QueryParser$Companion$getContentRule$1
                @Override // com.discord.simpleast.core.parser.Rule
                public ParseSpec<Context, Object> parse(Matcher matcher, Parser<Context, ? super QueryNode, Object> parser, Object obj) {
                    m.checkNotNullParameter(matcher, "matcher");
                    m.checkNotNullParameter(parser, "parser");
                    String group = matcher.group();
                    m.checkNotNullExpressionValue(group, "matcher.group()");
                    int length = group.length() - 1;
                    int i = 0;
                    boolean z2 = false;
                    while (i <= length) {
                        boolean z3 = m.compare(group.charAt(!z2 ? i : length), 32) <= 0;
                        if (!z2) {
                            if (!z3) {
                                z2 = true;
                            } else {
                                i++;
                            }
                        } else if (!z3) {
                            break;
                        } else {
                            length--;
                        }
                    }
                    ContentNode contentNode = new ContentNode(group.subSequence(i, length + 1).toString());
                    m.checkNotNullParameter(contentNode, "node");
                    return new ParseSpec<>(contentNode, obj);
                }
            };
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Rule<Context, QueryNode, Object> getFromFilterRule(final CharSequence charSequence) {
            final Pattern compile = Pattern.compile('^' + ("[\\s]*?(" + charSequence + "):"), 64);
            m.checkNotNullExpressionValue(compile, "fromFilterPattern");
            return new Rule<Context, QueryNode, Object>(compile) { // from class: com.discord.utilities.search.query.parsing.QueryParser$Companion$getFromFilterRule$1
                @Override // com.discord.simpleast.core.parser.Rule
                public ParseSpec<Context, Object> parse(Matcher matcher, Parser<Context, ? super QueryNode, Object> parser, Object obj) {
                    m.checkNotNullParameter(matcher, "matcher");
                    m.checkNotNullParameter(parser, "parser");
                    FilterNode filterNode = new FilterNode(FilterType.FROM, charSequence);
                    m.checkNotNullParameter(filterNode, "node");
                    return new ParseSpec<>(filterNode, obj);
                }
            };
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Rule<Context, QueryNode, Object> getUserRule() {
            final Pattern compile = Pattern.compile("^(?:\\s*([^@#:]+)#([0-9]{4}))", 64);
            m.checkNotNullExpressionValue(compile, "fromUserPattern");
            return new Rule<Context, QueryNode, Object>(compile) { // from class: com.discord.utilities.search.query.parsing.QueryParser$Companion$getUserRule$1
                @Override // com.discord.simpleast.core.parser.Rule
                public ParseSpec<Context, Object> parse(Matcher matcher, Parser<Context, ? super QueryNode, Object> parser, Object obj) {
                    m.checkNotNullParameter(matcher, "matcher");
                    m.checkNotNullParameter(parser, "parser");
                    String group = matcher.group(1);
                    m.checkNotNull(group);
                    String group2 = matcher.group(2);
                    m.checkNotNull(group2);
                    UserNode userNode = new UserNode(group, Integer.parseInt(group2));
                    m.checkNotNullParameter(userNode, "node");
                    return new ParseSpec<>(userNode, obj);
                }
            };
        }

        public final Rule<Context, QueryNode, Object> getHasAnswerRule(final SearchStringProvider searchStringProvider) {
            m.checkNotNullParameter(searchStringProvider, "searchStringProvider");
            String createHasAnswerRegex = createHasAnswerRegex(searchStringProvider);
            final Pattern compile = Pattern.compile('^' + createHasAnswerRegex, 64);
            m.checkNotNullExpressionValue(compile, "hasAnswerPattern");
            return new Rule<Context, QueryNode, Object>(compile) { // from class: com.discord.utilities.search.query.parsing.QueryParser$Companion$getHasAnswerRule$1
                @Override // com.discord.simpleast.core.parser.Rule
                public ParseSpec<Context, Object> parse(Matcher matcher, Parser<Context, ? super QueryNode, Object> parser, Object obj) {
                    m.checkNotNullParameter(matcher, "matcher");
                    m.checkNotNullParameter(parser, "parser");
                    String group = matcher.group(1);
                    m.checkNotNull(group);
                    HasNode hasNode = new HasNode(group, SearchStringProvider.this);
                    m.checkNotNullParameter(hasNode, "node");
                    return new ParseSpec<>(hasNode, obj);
                }
            };
        }

        public final Rule<Context, QueryNode, Object> getHasFilterRule(final CharSequence charSequence) {
            m.checkNotNullParameter(charSequence, "localizedHas");
            final Pattern compile = Pattern.compile('^' + ("^[\\s]*?" + charSequence + MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR), 64);
            m.checkNotNullExpressionValue(compile, "hasFilterPattern");
            return new Rule<Context, QueryNode, Object>(compile) { // from class: com.discord.utilities.search.query.parsing.QueryParser$Companion$getHasFilterRule$1
                @Override // com.discord.simpleast.core.parser.Rule
                public ParseSpec<Context, Object> parse(Matcher matcher, Parser<Context, ? super QueryNode, Object> parser, Object obj) {
                    m.checkNotNullParameter(matcher, "matcher");
                    m.checkNotNullParameter(parser, "parser");
                    FilterNode filterNode = new FilterNode(FilterType.HAS, charSequence);
                    m.checkNotNullParameter(filterNode, "node");
                    return new ParseSpec<>(filterNode, obj);
                }
            };
        }

        public final Rule<Context, QueryNode, Object> getInAnswerRule() {
            final Pattern compile = Pattern.compile("^(?:\\s*#([^ ]+))", 64);
            m.checkNotNullExpressionValue(compile, "inAnswerPattern");
            return new Rule<Context, QueryNode, Object>(compile) { // from class: com.discord.utilities.search.query.parsing.QueryParser$Companion$getInAnswerRule$1
                @Override // com.discord.simpleast.core.parser.Rule
                public ParseSpec<Context, Object> parse(Matcher matcher, Parser<Context, ? super QueryNode, Object> parser, Object obj) {
                    m.checkNotNullParameter(matcher, "matcher");
                    m.checkNotNullParameter(parser, "parser");
                    String group = matcher.group(1);
                    m.checkNotNull(group);
                    ChannelNode channelNode = new ChannelNode(group);
                    m.checkNotNullParameter(channelNode, "node");
                    return new ParseSpec<>(channelNode, obj);
                }
            };
        }

        public final Rule<Context, QueryNode, Object> getInFilterRule(final CharSequence charSequence) {
            m.checkNotNullParameter(charSequence, "localizedIn");
            final Pattern compile = Pattern.compile('^' + ("^[\\s]*?" + charSequence + MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR), 64);
            m.checkNotNullExpressionValue(compile, "fromUserPattern");
            return new Rule<Context, QueryNode, Object>(compile) { // from class: com.discord.utilities.search.query.parsing.QueryParser$Companion$getInFilterRule$1
                @Override // com.discord.simpleast.core.parser.Rule
                public ParseSpec<Context, Object> parse(Matcher matcher, Parser<Context, ? super QueryNode, Object> parser, Object obj) {
                    m.checkNotNullParameter(matcher, "matcher");
                    m.checkNotNullParameter(parser, "parser");
                    FilterNode filterNode = new FilterNode(FilterType.IN, charSequence);
                    m.checkNotNullParameter(filterNode, "node");
                    return new ParseSpec<>(filterNode, obj);
                }
            };
        }

        public final Rule<Context, QueryNode, Object> getMentionsFilterRule(final CharSequence charSequence) {
            m.checkNotNullParameter(charSequence, "localizedMentions");
            final Pattern compile = Pattern.compile('^' + ("^[\\s]*?" + charSequence + MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR), 64);
            m.checkNotNullExpressionValue(compile, "mentionsFilterPattern");
            return new Rule<Context, QueryNode, Object>(compile) { // from class: com.discord.utilities.search.query.parsing.QueryParser$Companion$getMentionsFilterRule$1
                @Override // com.discord.simpleast.core.parser.Rule
                public ParseSpec<Context, Object> parse(Matcher matcher, Parser<Context, ? super QueryNode, Object> parser, Object obj) {
                    m.checkNotNullParameter(matcher, "matcher");
                    m.checkNotNullParameter(parser, "parser");
                    FilterNode filterNode = new FilterNode(FilterType.MENTIONS, charSequence);
                    m.checkNotNullParameter(filterNode, "node");
                    return new ParseSpec<>(filterNode, obj);
                }
            };
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public QueryParser(SearchStringProvider searchStringProvider) {
        super(false, 1, null);
        m.checkNotNullParameter(searchStringProvider, "searchStringProvider");
        Companion companion = Companion;
        addRule(companion.getUserRule()).addRule(companion.getFromFilterRule(searchStringProvider.getFromFilterString())).addRule(companion.getMentionsFilterRule(searchStringProvider.getMentionsFilterString())).addRule(companion.getHasFilterRule(searchStringProvider.getHasFilterString())).addRule(companion.getHasAnswerRule(searchStringProvider)).addRule(companion.getInFilterRule(searchStringProvider.getInFilterString())).addRule(companion.getInAnswerRule()).addRule(companion.getContentRule());
    }
}
