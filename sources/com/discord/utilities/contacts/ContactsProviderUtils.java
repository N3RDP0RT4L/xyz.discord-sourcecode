package com.discord.utilities.contacts;

import andhook.lib.HookHelper;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import androidx.core.content.ContextCompat;
import d0.z.d.m;
import java.util.HashSet;
import java.util.Set;
import kotlin.Metadata;
/* compiled from: ContactsProviderUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0002\b\n\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u001b\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\f\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u000b\u001a\u00020\b¢\u0006\u0004\b\f\u0010\rR\u0016\u0010\u000e\u001a\u00020\b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000e\u0010\u000f¨\u0006\u0012"}, d2 = {"Lcom/discord/utilities/contacts/ContactsProviderUtils;", "", "Landroid/content/Context;", "ctx", "", "hasContactPermissions", "(Landroid/content/Context;)Z", "", "", "getAllContactPhoneNumbers", "(Landroid/content/Context;)Ljava/util/Set;", "number", "getOwnName", "(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;", "CONTACTS_PERMISSION", "Ljava/lang/String;", HookHelper.constructorName, "()V", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ContactsProviderUtils {
    private static final String CONTACTS_PERMISSION = "android.permission.READ_CONTACTS";
    public static final ContactsProviderUtils INSTANCE = new ContactsProviderUtils();

    private ContactsProviderUtils() {
    }

    public final Set<String> getAllContactPhoneNumbers(Context context) {
        m.checkNotNullParameter(context, "ctx");
        ContentResolver contentResolver = context.getContentResolver();
        HashSet hashSet = new HashSet();
        Cursor query = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        if (query != null) {
            while (query.moveToNext()) {
                String string = query.getString(query.getColumnIndex("data4"));
                if (string != null) {
                    hashSet.add(string);
                }
            }
            query.close();
        }
        return hashSet;
    }

    public final String getOwnName(Context context, String str) {
        m.checkNotNullParameter(context, "ctx");
        m.checkNotNullParameter(str, "number");
        ContentResolver contentResolver = context.getContentResolver();
        Uri withAppendedPath = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(str));
        m.checkNotNullExpressionValue(withAppendedPath, "Uri.withAppendedPath(Pho…_URI, Uri.encode(number))");
        Cursor query = contentResolver.query(withAppendedPath, new String[]{"display_name"}, null, null, null);
        if (query != null) {
            m.checkNotNullExpressionValue(query, "contentResolver.query(ur…ull, null) ?: return null");
            while (query.moveToNext()) {
                String string = query.getString(0);
                if (string != null) {
                    return string;
                }
            }
            query.close();
        }
        return null;
    }

    public final boolean hasContactPermissions(Context context) {
        m.checkNotNullParameter(context, "ctx");
        return ContextCompat.checkSelfPermission(context, CONTACTS_PERMISSION) == 0;
    }
}
