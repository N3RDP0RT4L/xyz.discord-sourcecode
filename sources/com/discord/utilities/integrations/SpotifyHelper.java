package com.discord.utilities.integrations;

import andhook.lib.HookHelper;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;
import androidx.appcompat.widget.ActivityChooserModel;
import com.discord.api.activity.Activity;
import com.discord.utilities.receiver.spotify.SpotifyMetadataReceiver;
import com.discord.utilities.receiver.spotify.SpotifyPlayingStateReceiver;
import com.discord.utilities.uri.UriHandler;
import d0.y.b;
import d0.z.d.m;
import java.io.IOException;
import java.net.URL;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: SpotifyHelper.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\"\u0010#J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0005\u0010\u0006J%\u0010\t\u001a\u00020\u00042\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u00072\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\r\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u001f\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0010\u001a\u0004\u0018\u00010\u000f¢\u0006\u0004\b\u0011\u0010\u0012J3\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0010\u001a\u0004\u0018\u00010\u000f2\n\u0010\u0015\u001a\u00060\u0013j\u0002`\u00142\u0006\u0010\u0017\u001a\u00020\u0016¢\u0006\u0004\b\u0018\u0010\u0019J\u0015\u0010\u001a\u001a\u00020\u00162\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u001a\u0010\u001bJ\u0015\u0010\u001c\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u001c\u0010\u0006J\u0015\u0010\u001d\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u001d\u0010\u0006R\u0016\u0010\u001f\u001a\u00020\u001e8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u0016\u0010!\u001a\u00020\u001e8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b!\u0010 ¨\u0006$"}, d2 = {"Lcom/discord/utilities/integrations/SpotifyHelper;", "", "Landroid/content/Context;", "context", "", "registerSpotifyBroadcastReceivers", "(Landroid/content/Context;)V", "Lkotlin/Function0;", "action", "runIfSpotifyInstalled", "(Lkotlin/jvm/functions/Function0;Landroid/content/Context;)V", "Landroid/content/Intent;", "intent", "launchSpotifyIntent", "(Landroid/content/Context;Landroid/content/Intent;)V", "Lcom/discord/api/activity/Activity;", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "launchTrack", "(Landroid/content/Context;Lcom/discord/api/activity/Activity;)V", "", "Lcom/discord/primitives/UserId;", "userId", "", "isMe", "launchAlbum", "(Landroid/content/Context;Lcom/discord/api/activity/Activity;JZ)V", "isSpotifyInstalled", "(Landroid/content/Context;)Z", "openPlayStoreForSpotify", "openSpotifyApp", "", "SPOTIFY_UTM_PARAMS", "Ljava/lang/String;", "SPOTIFY_PACKAGE_NAME", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SpotifyHelper {
    public static final SpotifyHelper INSTANCE = new SpotifyHelper();
    private static final String SPOTIFY_PACKAGE_NAME = "com.spotify.music";
    private static final String SPOTIFY_UTM_PARAMS = "utm_source=discord&utm_medium=mobile";

    private SpotifyHelper() {
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void launchSpotifyIntent(Context context, Intent intent) {
        try {
            intent.putExtra("android.intent.extra.REFERRER", Uri.parse("android-app://" + context.getPackageName()));
            context.startActivity(intent);
        } catch (ActivityNotFoundException unused) {
            if (!isSpotifyInstalled(context)) {
                openPlayStoreForSpotify(context);
            }
        }
    }

    public static final void registerSpotifyBroadcastReceivers(Context context) {
        m.checkNotNullParameter(context, "context");
        context.registerReceiver(new SpotifyMetadataReceiver(), new IntentFilter("com.spotify.music.metadatachanged"));
        context.registerReceiver(new SpotifyPlayingStateReceiver(), new IntentFilter("com.spotify.music.playbackstatechanged"));
    }

    private final void runIfSpotifyInstalled(Function0<Unit> function0, Context context) {
        try {
            function0.invoke();
        } catch (ActivityNotFoundException unused) {
            if (!isSpotifyInstalled(context)) {
                openPlayStoreForSpotify(context);
            }
        }
    }

    public final boolean isSpotifyInstalled(Context context) {
        m.checkNotNullParameter(context, "context");
        try {
            context.getPackageManager().getPackageInfo(SPOTIFY_PACKAGE_NAME, 0);
            return true;
        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }

    public final void launchAlbum(Context context, Activity activity, long j, boolean z2) {
        String k;
        Long a;
        m.checkNotNullParameter(context, "context");
        if (z2) {
            openSpotifyApp(context);
        } else if (activity != null && (k = activity.k()) != null && (a = activity.a()) != null) {
            runIfSpotifyInstalled(new SpotifyHelper$launchAlbum$1(j, k, a.longValue(), context), context);
        }
    }

    public final void launchTrack(Context context, Activity activity) {
        String n;
        m.checkNotNullParameter(context, "context");
        if (activity != null && (n = activity.n()) != null) {
            INSTANCE.runIfSpotifyInstalled(new SpotifyHelper$launchTrack$$inlined$let$lambda$1(n, context), context);
        }
    }

    public final void openPlayStoreForSpotify(final Context context) {
        m.checkNotNullParameter(context, "context");
        new Thread(new Runnable() { // from class: com.discord.utilities.integrations.SpotifyHelper$openPlayStoreForSpotify$1
            @Override // java.lang.Runnable
            public final void run() {
                try {
                    b.closeFinally(new URL("https://app.adjust.com/ndjczk?campaign=" + context.getPackageName()).openStream(), null);
                } catch (IOException e) {
                    Log.w("SPOTIFY", "Couldn't open tracking url", e);
                }
            }
        }).start();
        UriHandler.handle$default(UriHandler.INSTANCE, context, "https://play.google.com/store/apps/details?id=com.spotify.music&utm_source=discord&utm_medium=mobile", null, 4, null);
    }

    public final void openSpotifyApp(Context context) {
        m.checkNotNullParameter(context, "context");
        Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(SPOTIFY_PACKAGE_NAME);
        if (launchIntentForPackage != null) {
            context.startActivity(launchIntentForPackage);
        }
    }
}
