package com.discord.utilities.integrations;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.discord.api.activity.ActivityMetadata;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$3;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$4;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: SpotifyHelper.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SpotifyHelper$launchAlbum$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $applicationId;
    public final /* synthetic */ Context $context;
    public final /* synthetic */ String $sessionId;
    public final /* synthetic */ long $userId;

    /* compiled from: SpotifyHelper.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/activity/ActivityMetadata;", "metaData", "", "invoke", "(Lcom/discord/api/activity/ActivityMetadata;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.utilities.integrations.SpotifyHelper$launchAlbum$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<ActivityMetadata, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(ActivityMetadata activityMetadata) {
            invoke2(activityMetadata);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(ActivityMetadata activityMetadata) {
            String a;
            if (activityMetadata != null && (a = activityMetadata.a()) != null) {
                String encode = Uri.encode(a);
                SpotifyHelper.INSTANCE.launchSpotifyIntent(SpotifyHelper$launchAlbum$1.this.$context, new Intent("android.intent.action.VIEW", Uri.parse("spotify:album:" + encode + "?utm_source=discord&utm_medium=mobile")));
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SpotifyHelper$launchAlbum$1(long j, String str, long j2, Context context) {
        super(0);
        this.$userId = j;
        this.$sessionId = str;
        this.$applicationId = j2;
        this.$context = context;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().getActivityMetadata(this.$userId, this.$sessionId, this.$applicationId), false, 1, null)), (r18 & 1) != 0 ? null : this.$context, "REST: Spotify GetActivityMetadata", (r18 & 4) != 0 ? null : null, new AnonymousClass1(), (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
    }
}
