package com.discord.utilities.analytics;

import andhook.lib.HookHelper;
import com.discord.widgets.chat.AutocompleteSelectionTypes;
import kotlin.Metadata;
/* compiled from: ChatInputComponentTypes.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\r\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eR\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0007\u0010\u0004R\u0016\u0010\b\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\b\u0010\u0004R\u0016\u0010\t\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\t\u0010\u0004R\u0016\u0010\n\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\n\u0010\u0004R\u0016\u0010\u000b\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000b\u0010\u0004R\u0016\u0010\f\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\f\u0010\u0004¨\u0006\u000f"}, d2 = {"Lcom/discord/utilities/analytics/ChatInputComponentTypes;", "", "", "CAMERA", "Ljava/lang/String;", "GIF_SEARCH", "EMOJI_SEARCH", "FILES", AutocompleteSelectionTypes.STICKER, AutocompleteSelectionTypes.EMOJI, "STICKER_SEARCH", "MEDIA_PICKER", "GIF", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ChatInputComponentTypes {
    public static final String CAMERA = "camera";
    public static final String EMOJI = "emoji";
    public static final String EMOJI_SEARCH = "emoji search";
    public static final String FILES = "files";
    public static final String GIF = "GIF";
    public static final String GIF_SEARCH = "gif search";
    public static final ChatInputComponentTypes INSTANCE = new ChatInputComponentTypes();
    public static final String MEDIA_PICKER = "media picker";
    public static final String STICKER = "sticker";
    public static final String STICKER_SEARCH = "sticker search";

    private ChatInputComponentTypes() {
    }
}
