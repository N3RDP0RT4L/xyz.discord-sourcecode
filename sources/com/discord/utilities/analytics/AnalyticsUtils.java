package com.discord.utilities.analytics;

import andhook.lib.HookHelper;
import android.app.Application;
import android.os.Bundle;
import androidx.annotation.MainThread;
import com.discord.api.channel.Channel;
import com.discord.api.science.AnalyticsSchema;
import com.discord.api.science.Science;
import com.discord.api.user.User;
import com.discord.app.AppLog;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.rtcconnection.RtcConnection;
import com.discord.stores.StoreStream;
import com.discord.utilities.device.RtcCameraConfig;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.Clock;
import com.google.firebase.analytics.FirebaseAnalytics;
import d0.g;
import d0.g0.t;
import d0.o;
import d0.t.g0;
import d0.t.h0;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: AnalyticsUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001\u0014B\t\b\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0005\u0010\u0006R\u0018\u0010\b\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\b\u0010\tR&\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\f0\u000b*\u00020\n8@@\u0000X\u0080\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u000eR&\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u00010\u000b*\u00020\u00108@@\u0000X\u0080\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u0011¨\u0006\u0015"}, d2 = {"Lcom/discord/utilities/analytics/AnalyticsUtils;", "", "Landroid/app/Application;", "context", "", "initAppOpen", "(Landroid/app/Application;)V", "Lcom/google/firebase/analytics/FirebaseAnalytics;", "fireBaseInstance", "Lcom/google/firebase/analytics/FirebaseAnalytics;", "Lcom/discord/rtcconnection/RtcConnection;", "", "", "getProperties$app_productionGoogleRelease", "(Lcom/discord/rtcconnection/RtcConnection;)Ljava/util/Map;", "properties", "Lcom/discord/api/channel/Channel;", "(Lcom/discord/api/channel/Channel;)Ljava/util/Map;", HookHelper.constructorName, "()V", "Tracker", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AnalyticsUtils {
    public static final AnalyticsUtils INSTANCE = new AnalyticsUtils();
    private static FirebaseAnalytics fireBaseInstance;

    private AnalyticsUtils() {
    }

    public final Map<String, Object> getProperties$app_productionGoogleRelease(Channel channel) {
        m.checkNotNullParameter(channel, "$this$properties");
        Pair[] pairArr = new Pair[3];
        int i = 0;
        pairArr[0] = o.to(ModelAuditLogEntry.CHANGE_KEY_CHANNEL_ID, Long.valueOf(channel.h()));
        pairArr[1] = o.to("channel_type", Integer.valueOf(channel.A()));
        List<User> w = channel.w();
        if (w != null) {
            i = w.size();
        }
        pairArr[2] = o.to("channel_size_total", Integer.valueOf(i));
        return h0.mapOf(pairArr);
    }

    @MainThread
    public final void initAppOpen(Application application) {
        m.checkNotNullParameter(application, "context");
        if (fireBaseInstance == null) {
            fireBaseInstance = FirebaseAnalytics.getInstance(application);
            ObservableExtensionsKt.appSubscribe(StoreStream.Companion.getUsers().observeMe(true), AnalyticsUtils.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, AnalyticsUtils$initAppOpen$1.INSTANCE);
            RtcCameraConfig.INSTANCE.init();
        }
    }

    /* compiled from: AnalyticsUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 82\u00020\u0001:\u00018B%\u0012\u0006\u0010$\u001a\u00020#\u0012\u0006\u00100\u001a\u00020/\u0012\f\u00104\u001a\b\u0012\u0004\u0012\u00020 03¢\u0006\u0004\b6\u00107J)\u0010\b\u001a\u00020\u00072\u0018\u0010\u0006\u001a\u0014\u0012\u0004\u0012\u00020\u0003\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0002j\u0002`\u0005H\u0002¢\u0006\u0004\b\b\u0010\tJ'\u0010\r\u001a\u00020\n*\u00020\n2\u0012\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00010\u000bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u0010\u001a\u00020\u000fH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J!\u0010\u0014\u001a\u00020\u000f2\b\u0010\u0012\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0013\u001a\u00020\u0007¢\u0006\u0004\b\u0014\u0010\u0015JI\u0010\u0019\u001a\u00020\u000f2\u0018\u0010\u0006\u001a\u0014\u0012\u0004\u0012\u00020\u0003\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0002j\u0002`\u00052\u0006\u0010\u0016\u001a\u00020\u00042\u0018\u0010\u0018\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00010\u000b0\u0017¢\u0006\u0004\b\u0019\u0010\u001aJ/\u0010\u0019\u001a\u00020\u000f2\u0006\u0010\u001b\u001a\u00020\u00032\u0016\b\u0002\u0010\f\u001a\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u000bH\u0007¢\u0006\u0004\b\u0019\u0010\u001cJ\u0015\u0010\u0019\u001a\u00020\u000f2\u0006\u0010\u001e\u001a\u00020\u001d¢\u0006\u0004\b\u0019\u0010\u001fJ\u0015\u0010\u0019\u001a\u00020\u000f2\u0006\u0010\u001b\u001a\u00020 ¢\u0006\u0004\b\u0019\u0010!J)\u0010\"\u001a\u00020\u000f2\u0006\u0010\u001b\u001a\u00020\u00032\u0012\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00010\u000b¢\u0006\u0004\b\"\u0010\u001cR\u0016\u0010$\u001a\u00020#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%R4\u0010'\u001a \u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00020\u0003\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0002j\u0002`\u0005\u0012\u0004\u0012\u00020\u00040&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R\u0018\u0010\u0012\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0012\u0010)R\u0016\u0010,\u001a\u00020\u00078B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b*\u0010+R\u0016\u0010.\u001a\u00020\u00078@@\u0000X\u0080\u0004¢\u0006\u0006\u001a\u0004\b-\u0010+R\u0016\u00100\u001a\u00020/8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b0\u00101R\u0016\u0010\u0013\u001a\u00020\u00078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0013\u00102R\u001c\u00104\u001a\b\u0012\u0004\u0012\u00020 038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b4\u00105¨\u00069"}, d2 = {"Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;", "", "Lkotlin/Pair;", "", "", "Lcom/discord/utilities/analytics/ThrottleKey;", "throttleKey", "", "isEventThrottled", "(Lkotlin/Pair;)Z", "Landroid/os/Bundle;", "", "properties", "putMap", "(Landroid/os/Bundle;Ljava/util/Map;)Landroid/os/Bundle;", "", "drainEventsQueue", "()V", "analyticsToken", "fingerprinted", "setTrackingData", "(Ljava/lang/String;Z)V", "throttleTimeMs", "Lkotlin/Function0;", "lazyPropertyProvider", "track", "(Lkotlin/Pair;JLkotlin/jvm/functions/Function0;)V", "event", "(Ljava/lang/String;Ljava/util/Map;)V", "Lcom/discord/api/science/AnalyticsSchema;", "analyticsSchema", "(Lcom/discord/api/science/AnalyticsSchema;)V", "Lcom/discord/api/science/Science$Event;", "(Lcom/discord/api/science/Science$Event;)V", "trackFireBase", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "Ljava/util/concurrent/ConcurrentHashMap;", "eventsThrottledUntilMillis", "Ljava/util/concurrent/ConcurrentHashMap;", "Ljava/lang/String;", "getCanDrain", "()Z", "canDrain", "isAuthed$app_productionGoogleRelease", "isAuthed", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "Z", "Ljava/util/concurrent/ConcurrentLinkedQueue;", "eventsQueue", "Ljava/util/concurrent/ConcurrentLinkedQueue;", HookHelper.constructorName, "(Lcom/discord/utilities/time/Clock;Lcom/discord/utilities/rest/RestAPI;Ljava/util/concurrent/ConcurrentLinkedQueue;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Tracker {
        public static final Companion Companion = new Companion(null);
        private static final Lazy instance$delegate = g.lazy(AnalyticsUtils$Tracker$Companion$instance$2.INSTANCE);
        private String analyticsToken;
        private final Clock clock;
        private final ConcurrentLinkedQueue<Science.Event> eventsQueue;
        private final ConcurrentHashMap<Pair<String, Long>, Long> eventsThrottledUntilMillis = new ConcurrentHashMap<>();
        private boolean fingerprinted;
        private final RestAPI restAPI;

        /* compiled from: AnalyticsUtils.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tR\u001d\u0010\u0007\u001a\u00020\u00028F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\n"}, d2 = {"Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker$Companion;", "", "Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;", "instance$delegate", "Lkotlin/Lazy;", "getInstance", "()Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;", "instance", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Tracker getInstance() {
                Lazy lazy = Tracker.instance$delegate;
                Companion companion = Tracker.Companion;
                return (Tracker) lazy.getValue();
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Tracker(Clock clock, RestAPI restAPI, ConcurrentLinkedQueue<Science.Event> concurrentLinkedQueue) {
            m.checkNotNullParameter(clock, "clock");
            m.checkNotNullParameter(restAPI, "restAPI");
            m.checkNotNullParameter(concurrentLinkedQueue, "eventsQueue");
            this.clock = clock;
            this.restAPI = restAPI;
            this.eventsQueue = concurrentLinkedQueue;
        }

        public final synchronized void drainEventsQueue() {
            if (getCanDrain()) {
                ArrayList arrayList = new ArrayList(this.eventsQueue);
                this.eventsQueue.clear();
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.science(new Science(this.analyticsToken, arrayList)), false, 1, null), getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new AnalyticsUtils$Tracker$drainEventsQueue$2(this, arrayList), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, AnalyticsUtils$Tracker$drainEventsQueue$1.INSTANCE);
            }
        }

        private final boolean getCanDrain() {
            return (this.eventsQueue.isEmpty() ^ true) && (this.fingerprinted || isAuthed$app_productionGoogleRelease());
        }

        private final boolean isEventThrottled(Pair<String, Long> pair) {
            long currentTimeMillis = this.clock.currentTimeMillis();
            Long l = this.eventsThrottledUntilMillis.get(pair);
            if (l == null) {
                l = 0L;
            }
            m.checkNotNullExpressionValue(l, "eventsThrottledUntilMillis[throttleKey] ?: 0");
            return currentTimeMillis < l.longValue();
        }

        private final Bundle putMap(Bundle bundle, Map<String, ? extends Object> map) {
            try {
                for (Map.Entry<String, ? extends Object> entry : map.entrySet()) {
                    String key = entry.getKey();
                    Object value = entry.getValue();
                    if (value instanceof String) {
                        bundle.putString(key, (String) value);
                    } else if (value instanceof Integer) {
                        bundle.putInt(key, ((Number) value).intValue());
                    } else if (value instanceof Long) {
                        bundle.putLong(key, ((Number) value).longValue());
                    } else if (value instanceof Double) {
                        bundle.putDouble(key, ((Number) value).doubleValue());
                    } else if (value instanceof Float) {
                        bundle.putFloat(key, ((Number) value).floatValue());
                    }
                }
            } catch (Throwable th) {
                AppLog appLog = AppLog.g;
                Logger.e$default(appLog, bundle.getClass().getSimpleName() + " putMap", th, null, 4, null);
            }
            return bundle;
        }

        public static /* synthetic */ void setTrackingData$default(Tracker tracker, String str, boolean z2, int i, Object obj) {
            if ((i & 2) != 0) {
                z2 = false;
            }
            tracker.setTrackingData(str, z2);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ void track$default(Tracker tracker, String str, Map map, int i, Object obj) {
            if ((i & 2) != 0) {
                map = null;
            }
            tracker.track(str, map);
        }

        public final boolean isAuthed$app_productionGoogleRelease() {
            String str = this.analyticsToken;
            return !(str == null || t.isBlank(str));
        }

        public final synchronized void setTrackingData(String str, boolean z2) {
            drainEventsQueue();
            this.analyticsToken = str;
            this.fingerprinted = z2;
            drainEventsQueue();
        }

        public final void track(Pair<String, Long> pair, long j, Function0<? extends Map<String, ? extends Object>> function0) {
            m.checkNotNullParameter(pair, "throttleKey");
            m.checkNotNullParameter(function0, "lazyPropertyProvider");
            if (!isEventThrottled(pair)) {
                track(pair.component1(), function0.invoke());
                this.eventsThrottledUntilMillis.put(pair, Long.valueOf(this.clock.currentTimeMillis() + j));
            }
        }

        public final void trackFireBase(String str, Map<String, ? extends Object> map) {
            m.checkNotNullParameter(str, "event");
            m.checkNotNullParameter(map, "properties");
            Bundle putMap = putMap(new Bundle(), map);
            AnalyticsUtils analyticsUtils = AnalyticsUtils.INSTANCE;
            FirebaseAnalytics firebaseAnalytics = AnalyticsUtils.fireBaseInstance;
            if (firebaseAnalytics != null) {
                firebaseAnalytics.f3081b.c(null, str, putMap, false, true, null);
            }
        }

        public final void track(String str, Map<String, ? extends Object> map) {
            m.checkNotNullParameter(str, "event");
            if (map == null) {
                map = h0.emptyMap();
            }
            track(new Science.Event.MapObject(str, map));
        }

        public final void track(AnalyticsSchema analyticsSchema) {
            m.checkNotNullParameter(analyticsSchema, "analyticsSchema");
            track(new Science.Event.SchemaObject(analyticsSchema));
        }

        public final void track(Science.Event event) {
            m.checkNotNullParameter(event, "event");
            this.eventsQueue.add(event);
            Observable<Long> d02 = Observable.d0(1500L, TimeUnit.MILLISECONDS);
            m.checkNotNullExpressionValue(d02, "Observable\n          .ti…0, TimeUnit.MILLISECONDS)");
            ObservableExtensionsKt.appSubscribe(d02, Tracker.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnalyticsUtils$Tracker$track$1(this));
        }
    }

    public final Map<String, String> getProperties$app_productionGoogleRelease(RtcConnection rtcConnection) {
        m.checkNotNullParameter(rtcConnection, "$this$properties");
        return g0.mapOf(o.to("rtc_connection_id", rtcConnection.m));
    }
}
