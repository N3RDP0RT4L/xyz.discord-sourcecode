package com.discord.utilities.analytics;

import b.i.a.f.h.l.g;
import b.i.a.f.h.l.n;
import com.discord.app.AppLog;
import com.discord.models.user.MeUser;
import com.discord.utilities.user.UserUtils;
import com.google.firebase.analytics.FirebaseAnalytics;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: AnalyticsUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/user/MeUser;", "meUser", "", "invoke", "(Lcom/discord/models/user/MeUser;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AnalyticsUtils$initAppOpen$1 extends o implements Function1<MeUser, Unit> {
    public static final AnalyticsUtils$initAppOpen$1 INSTANCE = new AnalyticsUtils$initAppOpen$1();

    public AnalyticsUtils$initAppOpen$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(MeUser meUser) {
        invoke2(meUser);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(MeUser meUser) {
        m.checkNotNullParameter(meUser, "meUser");
        if (m.areEqual(meUser, UserUtils.INSTANCE.getEMPTY_USER())) {
            AppLog.g(0L, null, null);
            AnalyticsUtils analyticsUtils = AnalyticsUtils.INSTANCE;
            FirebaseAnalytics firebaseAnalytics = AnalyticsUtils.fireBaseInstance;
            if (firebaseAnalytics != null) {
                g gVar = firebaseAnalytics.f3081b;
                Objects.requireNonNull(gVar);
                gVar.e.execute(new n(gVar, null));
                return;
            }
            return;
        }
        AnalyticsUtils analyticsUtils2 = AnalyticsUtils.INSTANCE;
        FirebaseAnalytics firebaseAnalytics2 = AnalyticsUtils.fireBaseInstance;
        if (firebaseAnalytics2 != null) {
            String valueOf = String.valueOf(meUser.getId());
            g gVar2 = firebaseAnalytics2.f3081b;
            Objects.requireNonNull(gVar2);
            gVar2.e.execute(new n(gVar2, valueOf));
        }
        AppLog.g(Long.valueOf(meUser.getId()), meUser.getEmail(), meUser.getUsername());
    }
}
