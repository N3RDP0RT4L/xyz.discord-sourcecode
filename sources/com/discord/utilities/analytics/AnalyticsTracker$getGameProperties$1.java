package com.discord.utilities.analytics;

import com.discord.api.activity.Activity;
import com.discord.models.presence.Presence;
import com.discord.utilities.presence.PresenceUtils;
import d0.z.d.m;
import kotlin.Metadata;
import rx.functions.Func2;
/* compiled from: AnalyticsTracker.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\b\u001a\u0004\u0018\u00010\u00052\u0018\u0010\u0003\u001a\u0014 \u0002*\n\u0018\u00010\u0000j\u0004\u0018\u0001`\u00010\u0000j\u0002`\u00012\u0018\u0010\u0004\u001a\u0014 \u0002*\n\u0018\u00010\u0000j\u0004\u0018\u0001`\u00010\u0000j\u0002`\u0001H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Lcom/discord/models/presence/Presence;", "Lcom/discord/stores/AppPresence;", "kotlin.jvm.PlatformType", "localPresence", "externalPresence", "Lcom/discord/api/activity/Activity;", "invoke", "(Lcom/discord/models/presence/Presence;Lcom/discord/models/presence/Presence;)Lcom/discord/api/activity/Activity;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AnalyticsTracker$getGameProperties$1<T1, T2, R> implements Func2<Presence, Presence, Activity> {
    public static final AnalyticsTracker$getGameProperties$1 INSTANCE = new AnalyticsTracker$getGameProperties$1();

    /* renamed from: invoke */
    public final Activity call(Presence presence, Presence presence2) {
        PresenceUtils presenceUtils = PresenceUtils.INSTANCE;
        m.checkNotNullExpressionValue(presence, "localPresence");
        Activity playingActivity = presenceUtils.getPlayingActivity(presence);
        if (playingActivity != null) {
            return playingActivity;
        }
        m.checkNotNullExpressionValue(presence2, "externalPresence");
        return presenceUtils.getPlayingActivity(presence2);
    }
}
