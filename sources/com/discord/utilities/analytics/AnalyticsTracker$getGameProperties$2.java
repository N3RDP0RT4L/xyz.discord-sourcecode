package com.discord.utilities.analytics;

import androidx.core.app.NotificationCompat;
import com.discord.api.activity.Activity;
import com.discord.utilities.collections.CollectionExtensionsKt;
import com.discord.utilities.presence.ActivityUtilsKt;
import d0.o;
import d0.t.h0;
import j0.k.b;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Pair;
/* compiled from: AnalyticsTracker.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\u0010\u0000\n\u0002\b\u0004\u0010\b\u001a\"\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004 \u0005*\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Lcom/discord/api/activity/Activity;", "gameActivity", "", "", "", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/api/activity/Activity;)Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AnalyticsTracker$getGameProperties$2<T, R> implements b<Activity, Map<String, Object>> {
    public static final AnalyticsTracker$getGameProperties$2 INSTANCE = new AnalyticsTracker$getGameProperties$2();

    public final Map<String, Object> call(Activity activity) {
        Pair[] pairArr = new Pair[3];
        Long l = null;
        pairArr[0] = o.to("game_platform", activity != null ? ActivityUtilsKt.getGamePlatform(activity) : null);
        pairArr[1] = o.to("game_name", activity != null ? activity.h() : null);
        if (activity != null) {
            l = activity.a();
        }
        pairArr[2] = o.to("game_id", l);
        return CollectionExtensionsKt.filterNonNullValues(h0.mapOf(pairArr));
    }
}
