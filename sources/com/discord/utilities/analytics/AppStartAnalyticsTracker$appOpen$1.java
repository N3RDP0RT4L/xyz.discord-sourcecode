package com.discord.utilities.analytics;

import android.net.Uri;
import com.adjust.sdk.Constants;
import com.discord.stores.StoreUserSettingsSystem;
import com.discord.utilities.time.Clock;
import d0.t.h0;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.functions.Function0;
/* compiled from: AppStartAnalyticsTracker.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0010\u0000\n\u0002\b\u0003\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "", "", "invoke", "()Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AppStartAnalyticsTracker$appOpen$1 extends o implements Function0<Map<String, ? extends Object>> {
    public final /* synthetic */ boolean $isNotificationRoute;
    public final /* synthetic */ Uri $uri;
    public final /* synthetic */ boolean $uriCanBeRouted;
    public final /* synthetic */ AppStartAnalyticsTracker this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AppStartAnalyticsTracker$appOpen$1(AppStartAnalyticsTracker appStartAnalyticsTracker, boolean z2, boolean z3, Uri uri) {
        super(0);
        this.this$0 = appStartAnalyticsTracker;
        this.$isNotificationRoute = z2;
        this.$uriCanBeRouted = z3;
        this.$uri = uri;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Map<String, ? extends Object> invoke() {
        String str;
        StoreUserSettingsSystem storeUserSettingsSystem;
        Long l;
        String str2;
        Clock clock;
        Pair[] pairArr = new Pair[2];
        if (this.$isNotificationRoute) {
            str = "notification";
        } else {
            str = this.$uriCanBeRouted ? Constants.DEEPLINK : "launcher";
        }
        pairArr[0] = d0.o.to("opened_from", str);
        storeUserSettingsSystem = this.this$0.storeUserSettingsSystem;
        pairArr[1] = d0.o.to("theme", storeUserSettingsSystem.getTheme());
        Map<String, ? extends Object> mutableMapOf = h0.mutableMapOf(pairArr);
        AppStartAnalyticsTracker.Companion.insertUriProperties(mutableMapOf, this.$uri);
        l = this.this$0.appOpenTimestamp;
        if (l == null) {
            str2 = this.this$0.openAppLoadId;
            mutableMapOf.put("load_id", str2);
            AppStartAnalyticsTracker appStartAnalyticsTracker = this.this$0;
            clock = appStartAnalyticsTracker.clock;
            appStartAnalyticsTracker.appOpenTimestamp = Long.valueOf(clock.currentTimeMillis());
        }
        return mutableMapOf;
    }
}
