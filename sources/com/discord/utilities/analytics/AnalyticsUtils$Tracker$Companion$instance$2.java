package com.discord.utilities.analytics;

import com.discord.utilities.analytics.AnalyticsUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.time.ClockFactory;
import d0.z.d.o;
import java.util.concurrent.ConcurrentLinkedQueue;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: AnalyticsUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;", "invoke", "()Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AnalyticsUtils$Tracker$Companion$instance$2 extends o implements Function0<AnalyticsUtils.Tracker> {
    public static final AnalyticsUtils$Tracker$Companion$instance$2 INSTANCE = new AnalyticsUtils$Tracker$Companion$instance$2();

    public AnalyticsUtils$Tracker$Companion$instance$2() {
        super(0);
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AnalyticsUtils.Tracker invoke() {
        return new AnalyticsUtils.Tracker(ClockFactory.get(), RestAPI.Companion.getApi(), new ConcurrentLinkedQueue());
    }
}
