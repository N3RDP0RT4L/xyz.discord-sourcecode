package com.discord.utilities.analytics;

import andhook.lib.HookHelper;
import android.os.Build;
import android.util.Base64;
import com.discord.BuildConfig;
import com.discord.utilities.accessibility.AccessibilityFeatureFlags;
import com.google.gson.Gson;
import d0.b0.a;
import d0.g0.c;
import d0.g0.t;
import d0.o;
import d0.t.g0;
import d0.t.h0;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.s;
import java.nio.charset.Charset;
import java.util.EnumSet;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.properties.ReadWriteProperty;
import kotlin.reflect.KProperty;
/* compiled from: AnalyticSuperProperties.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0002\b,\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\bC\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J#\u0010\b\u001a\u00020\u00022\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00010\u0005H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0015\u0010\u000b\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\u0006¢\u0006\u0004\b\u000b\u0010\fJ\u0015\u0010\u000e\u001a\u00020\u00022\u0006\u0010\r\u001a\u00020\u0006¢\u0006\u0004\b\u000e\u0010\fJ#\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u000f2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011¢\u0006\u0004\b\u0014\u0010\u0015J%\u0010\u001b\u001a\u00020\u00022\u0006\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\u0016¢\u0006\u0004\b\u001b\u0010\u001cR\u0016\u0010\u001d\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u0016\u0010\u001f\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001f\u0010\u001eR\u0016\u0010 \u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b \u0010\u001eR\u0016\u0010!\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b!\u0010\u001eR\u0016\u0010\"\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\"\u0010\u001eR\u0016\u0010#\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b#\u0010\u001eR\u0016\u0010$\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b$\u0010\u001eR\u0016\u0010%\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b%\u0010\u001eR\u0016\u0010&\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b&\u0010\u001eR\u0016\u0010'\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b'\u0010\u001eR\u0016\u0010(\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b(\u0010\u001eR\u0016\u0010)\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b)\u0010\u001eR\u0016\u0010*\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b*\u0010\u001eR$\u0010,\u001a\u00020\u00062\u0006\u0010+\u001a\u00020\u00068\u0006@BX\u0086\u000e¢\u0006\f\n\u0004\b,\u0010\u001e\u001a\u0004\b-\u0010.R\u0016\u0010/\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b/\u0010\u001eR\u0016\u00100\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b0\u0010\u001eR\u0016\u00101\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b1\u0010\u001eR\u0016\u00102\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b2\u0010\u001eR\u0016\u00103\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b3\u0010\u001eR\u0016\u00104\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b4\u0010\u001eR\u0016\u00105\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b5\u0010\u001eR\u0016\u00106\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b6\u0010\u001eR\u0016\u00107\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b7\u0010\u001eR\u0016\u00108\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b8\u0010\u001eR\u0016\u00109\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b9\u0010\u001eR\u0016\u0010:\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b:\u0010\u001eRC\u0010@\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00010\u00052\u0012\u0010+\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00010\u00058F@BX\u0086\u008e\u0002¢\u0006\u0012\n\u0004\b;\u0010<\u001a\u0004\b=\u0010>\"\u0004\b?\u0010\tR$\u0010A\u001a\u00020\u00062\u0006\u0010+\u001a\u00020\u00068\u0006@BX\u0086\u000e¢\u0006\f\n\u0004\bA\u0010\u001e\u001a\u0004\bB\u0010.¨\u0006D"}, d2 = {"Lcom/discord/utilities/analytics/AnalyticSuperProperties;", "", "", "setBaselineProperties", "()V", "", "", "extraSuperProperties", "updateSuperProperties", "(Ljava/util/Map;)V", "advertiserId", "setAdvertiserId", "(Ljava/lang/String;)V", "referrerUrl", "setCampaignProperties", "", "accessibilitySupportEnabled", "Ljava/util/EnumSet;", "Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;", "features", "setAccessibilityProperties", "(ZLjava/util/EnumSet;)V", "", "cpu", "", "memoryKb", "cpuCoreCount", "setClientPerformanceProperties", "(IJI)V", "PROPERTY_OS_VERSION", "Ljava/lang/String;", "PROPERTY_CLIENT_BUILD_NUMBER", "PROPERTY_UTM_CONTENT", "PROPERTY_MP_KEYWORD", "PROPERTY_SYSTEM_LOCALE", "PROPERTY_CLIENT_VERSION", "PROPERTY_LOCATION", "PROPERTY_ACCESSIBILITY_SUPPORT_ENABLED", "PROPERTY_SEARCH_ENGINE", "PROPERTY_DEVICE_ADVERTISER_ID", "PROPERTY_ACCESSIBILITY_FEATURES", "PROPERTY_BROWSER", "PROPERTY_DEVICE", "<set-?>", "superPropertiesString", "getSuperPropertiesString", "()Ljava/lang/String;", "PROPERTY_BROWSER_USER_AGENT", "PROPERTY_CPU_CORE_COUNT", "PROPERTY_UTM_TERM", "PROPERTY_UTM_CAMPAIGN", "PROPERTY_CLIENT_PERFORMANCE_CPU", "PROPERTY_OS", "PROPERTY_REFERRER", "PROPERTY_OS_SDK_VERSION", "PROPERTY_UTM_SOURCE", "PROPERTY_UTM_MEDIUM", "PROPERTY_CLIENT_PERFORMANCE_MEMORY", "PROPERTY_REFERRING_DOMAIN", "superProperties$delegate", "Lkotlin/properties/ReadWriteProperty;", "getSuperProperties", "()Ljava/util/Map;", "setSuperProperties", "superProperties", "superPropertiesStringBase64", "getSuperPropertiesStringBase64", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AnalyticSuperProperties {
    public static final AnalyticSuperProperties INSTANCE;
    private static final String PROPERTY_ACCESSIBILITY_FEATURES = "accessibility_features";
    private static final String PROPERTY_ACCESSIBILITY_SUPPORT_ENABLED = "accessibility_support_enabled";
    private static final String PROPERTY_BROWSER = "browser";
    private static final String PROPERTY_BROWSER_USER_AGENT = "browser_user_agent";
    private static final String PROPERTY_CLIENT_BUILD_NUMBER = "client_build_number";
    private static final String PROPERTY_CLIENT_PERFORMANCE_CPU = "client_performance_cpu";
    private static final String PROPERTY_CLIENT_PERFORMANCE_MEMORY = "client_performance_memory";
    private static final String PROPERTY_CLIENT_VERSION = "client_version";
    private static final String PROPERTY_CPU_CORE_COUNT = "cpu_core_count";
    private static final String PROPERTY_DEVICE = "device";
    private static final String PROPERTY_DEVICE_ADVERTISER_ID = "device_advertiser_id";
    private static final String PROPERTY_LOCATION = "location";
    private static final String PROPERTY_MP_KEYWORD = "mp_keyword";
    private static final String PROPERTY_OS = "os";
    private static final String PROPERTY_OS_SDK_VERSION = "os_sdk_version";
    private static final String PROPERTY_OS_VERSION = "os_version";
    private static final String PROPERTY_REFERRER = "referrer";
    private static final String PROPERTY_REFERRING_DOMAIN = "referring_domain";
    private static final String PROPERTY_SEARCH_ENGINE = "search_engine";
    private static final String PROPERTY_SYSTEM_LOCALE = "system_locale";
    private static final String PROPERTY_UTM_CAMPAIGN = "utm_campaign";
    private static final String PROPERTY_UTM_CONTENT = "utm_content";
    private static final String PROPERTY_UTM_MEDIUM = "utm_medium";
    private static final String PROPERTY_UTM_SOURCE = "utm_source";
    private static final String PROPERTY_UTM_TERM = "utm_term";
    private static final ReadWriteProperty superProperties$delegate;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a0.mutableProperty1(new s(AnalyticSuperProperties.class, "superProperties", "getSuperProperties()Ljava/util/Map;", 0))};
    private static String superPropertiesString = "";
    private static String superPropertiesStringBase64 = "";

    static {
        AnalyticSuperProperties analyticSuperProperties = new AnalyticSuperProperties();
        INSTANCE = analyticSuperProperties;
        final Map emptyMap = h0.emptyMap();
        superProperties$delegate = new a<Map<String, ? extends Object>>(emptyMap) { // from class: com.discord.utilities.analytics.AnalyticSuperProperties$$special$$inlined$observable$1
            @Override // d0.b0.a
            public void afterChange(KProperty<?> kProperty, Map<String, ? extends Object> map, Map<String, ? extends Object> map2) {
                m.checkNotNullParameter(kProperty, "property");
                AnalyticSuperProperties analyticSuperProperties2 = AnalyticSuperProperties.INSTANCE;
                String m = new Gson().m(map2);
                if (m == null) {
                    m = "{}";
                }
                AnalyticSuperProperties.superPropertiesString = m;
                String superPropertiesString2 = analyticSuperProperties2.getSuperPropertiesString();
                Charset charset = c.a;
                Objects.requireNonNull(superPropertiesString2, "null cannot be cast to non-null type java.lang.String");
                byte[] bytes = superPropertiesString2.getBytes(charset);
                m.checkNotNullExpressionValue(bytes, "(this as java.lang.String).getBytes(charset)");
                String encodeToString = Base64.encodeToString(bytes, 2);
                m.checkNotNullExpressionValue(encodeToString, "Base64.encodeToString(su…eArray(), Base64.NO_WRAP)");
                AnalyticSuperProperties.superPropertiesStringBase64 = encodeToString;
            }
        };
        analyticSuperProperties.setBaselineProperties();
    }

    private AnalyticSuperProperties() {
    }

    private final void setBaselineProperties() {
        String locale = Locale.getDefault().toString();
        m.checkNotNullExpressionValue(locale, "Locale.getDefault().toString()");
        updateSuperProperties(h0.mapOf(o.to(PROPERTY_BROWSER, "Discord Android"), o.to(PROPERTY_BROWSER_USER_AGENT, BuildConfig.USER_AGENT), o.to(PROPERTY_CLIENT_BUILD_NUMBER, Integer.valueOf((int) BuildConfig.VERSION_CODE)), o.to(PROPERTY_CLIENT_VERSION, BuildConfig.VERSION_NAME), o.to(PROPERTY_DEVICE, Build.MODEL + ", " + Build.PRODUCT), o.to(PROPERTY_OS, "Android"), o.to(PROPERTY_OS_SDK_VERSION, String.valueOf(Build.VERSION.SDK_INT)), o.to(PROPERTY_OS_VERSION, Build.VERSION.RELEASE), o.to(PROPERTY_SYSTEM_LOCALE, t.replace$default(locale, "_", "-", false, 4, (Object) null))));
    }

    private final void setSuperProperties(Map<String, ? extends Object> map) {
        superProperties$delegate.setValue(this, $$delegatedProperties[0], map);
    }

    private final synchronized void updateSuperProperties(Map<String, ? extends Object> map) {
        setSuperProperties(h0.plus(getSuperProperties(), map));
    }

    public final Map<String, Object> getSuperProperties() {
        return (Map) superProperties$delegate.getValue(this, $$delegatedProperties[0]);
    }

    public final String getSuperPropertiesString() {
        return superPropertiesString;
    }

    public final String getSuperPropertiesStringBase64() {
        return superPropertiesStringBase64;
    }

    public final void setAccessibilityProperties(boolean z2, EnumSet<AccessibilityFeatureFlags> enumSet) {
        m.checkNotNullParameter(enumSet, "features");
        long j = 0;
        for (AccessibilityFeatureFlags accessibilityFeatureFlags : enumSet) {
            j |= accessibilityFeatureFlags.getValue();
        }
        updateSuperProperties(h0.mapOf(o.to(PROPERTY_ACCESSIBILITY_SUPPORT_ENABLED, Boolean.valueOf(z2)), o.to(PROPERTY_ACCESSIBILITY_FEATURES, Long.valueOf(j))));
    }

    public final void setAdvertiserId(String str) {
        m.checkNotNullParameter(str, "advertiserId");
        updateSuperProperties(g0.mapOf(o.to(PROPERTY_DEVICE_ADVERTISER_ID, str)));
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Code restructure failed: missing block: B:21:0x00a5, code lost:
        if (r5.equals(com.discord.utilities.analytics.AnalyticSuperProperties.PROPERTY_UTM_SOURCE) != false) goto L38;
     */
    /* JADX WARN: Code restructure failed: missing block: B:23:0x00ae, code lost:
        if (r5.equals("location") != false) goto L38;
     */
    /* JADX WARN: Code restructure failed: missing block: B:25:0x00b8, code lost:
        if (r5.equals(com.discord.utilities.analytics.AnalyticSuperProperties.PROPERTY_UTM_MEDIUM) != false) goto L38;
     */
    /* JADX WARN: Code restructure failed: missing block: B:27:0x00c2, code lost:
        if (r5.equals(com.discord.utilities.analytics.AnalyticSuperProperties.PROPERTY_SEARCH_ENGINE) != false) goto L38;
     */
    /* JADX WARN: Code restructure failed: missing block: B:29:0x00cc, code lost:
        if (r5.equals(com.discord.utilities.analytics.AnalyticSuperProperties.PROPERTY_MP_KEYWORD) != false) goto L38;
     */
    /* JADX WARN: Code restructure failed: missing block: B:31:0x00d6, code lost:
        if (r5.equals(com.discord.utilities.analytics.AnalyticSuperProperties.PROPERTY_UTM_TERM) != false) goto L38;
     */
    /* JADX WARN: Code restructure failed: missing block: B:33:0x00e0, code lost:
        if (r5.equals(com.discord.utilities.analytics.AnalyticSuperProperties.PROPERTY_UTM_CAMPAIGN) != false) goto L38;
     */
    /* JADX WARN: Code restructure failed: missing block: B:35:0x00ea, code lost:
        if (r5.equals(com.discord.utilities.analytics.AnalyticSuperProperties.PROPERTY_REFERRING_DOMAIN) != false) goto L38;
     */
    /* JADX WARN: Code restructure failed: missing block: B:37:0x00f4, code lost:
        if (r5.equals(com.discord.utilities.analytics.AnalyticSuperProperties.PROPERTY_UTM_CONTENT) != false) goto L38;
     */
    /* JADX WARN: Code restructure failed: missing block: B:38:0x00f6, code lost:
        r5 = true;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void setCampaignProperties(java.lang.String r12) {
        /*
            Method dump skipped, instructions count: 330
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.analytics.AnalyticSuperProperties.setCampaignProperties(java.lang.String):void");
    }

    public final void setClientPerformanceProperties(int i, long j, int i2) {
        updateSuperProperties(h0.mapOf(o.to(PROPERTY_CLIENT_PERFORMANCE_CPU, Integer.valueOf(i)), o.to(PROPERTY_CLIENT_PERFORMANCE_MEMORY, Long.valueOf(j)), o.to(PROPERTY_CPU_CORE_COUNT, Integer.valueOf(i2))));
    }
}
