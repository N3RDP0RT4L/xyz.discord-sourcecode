package com.discord.utilities.analytics;

import com.discord.api.channel.Channel;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: AnalyticsTracker.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00020\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "", "", "gameProperties", "", "invoke", "(Ljava/util/Map;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AnalyticsTracker$trackUserVoiceEvent$1 extends o implements Function1<Map<String, Object>, Unit> {
    public final /* synthetic */ String $inputMode;
    public final /* synthetic */ Channel $this_trackUserVoiceEvent;
    public final /* synthetic */ Pair $throttleKey;

    /* compiled from: AnalyticsTracker.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0010\u0000\n\u0002\b\u0003\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "", "", "invoke", "()Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.utilities.analytics.AnalyticsTracker$trackUserVoiceEvent$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Map<String, ? extends Object>> {
        public final /* synthetic */ Map $gameProperties;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Map map) {
            super(0);
            this.$gameProperties = map;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Map<String, ? extends Object> invoke() {
            this.$gameProperties.put("mode", AnalyticsTracker$trackUserVoiceEvent$1.this.$inputMode);
            this.$gameProperties.put("channel", Long.valueOf(AnalyticsTracker$trackUserVoiceEvent$1.this.$this_trackUserVoiceEvent.h()));
            this.$gameProperties.put("channel_type", Integer.valueOf(AnalyticsTracker$trackUserVoiceEvent$1.this.$this_trackUserVoiceEvent.A()));
            this.$gameProperties.put("server", Long.valueOf(AnalyticsTracker$trackUserVoiceEvent$1.this.$this_trackUserVoiceEvent.f()));
            return this.$gameProperties;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AnalyticsTracker$trackUserVoiceEvent$1(Channel channel, Pair pair, String str) {
        super(1);
        this.$this_trackUserVoiceEvent = channel;
        this.$throttleKey = pair;
        this.$inputMode = str;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Map<String, Object> map) {
        invoke2(map);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Map<String, Object> map) {
        m.checkNotNullParameter(map, "gameProperties");
        AnalyticsTracker.INSTANCE.getTracker().track(this.$throttleKey, 900000L, new AnonymousClass1(map));
    }
}
