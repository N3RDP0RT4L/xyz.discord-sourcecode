package com.discord.utilities.fcm;

import andhook.lib.HookHelper;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.RemoteInput;
import androidx.work.BackoffPolicy;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import b.d.b.a.a;
import com.discord.app.AppLog;
import com.discord.app.DiscordConnectService;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.logging.Logger;
import com.discord.workers.CallActionWorker;
import com.discord.workers.MessageAckWorker;
import com.discord.workers.MessageSendWorker;
import com.discord.workers.TimedMuteWorker;
import d0.g0.s;
import d0.g0.t;
import d0.o;
import d0.t.h0;
import d0.z.d.m;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: NotificationActions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\f\u0018\u0000 \u00132\u00020\u0001:\u0001\u0013B\u0007¢\u0006\u0004\b\u0011\u0010\u0012J'\u0010\t\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0006H\u0003¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\u000b\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u001f\u0010\r\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\r\u0010\fJ\u001f\u0010\u000e\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000e\u0010\fJ\u001f\u0010\u000f\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0017¢\u0006\u0004\b\u000f\u0010\fJ\u001f\u0010\u0010\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0007¢\u0006\u0004\b\u0010\u0010\f¨\u0006\u0014"}, d2 = {"Lcom/discord/utilities/fcm/NotificationActions;", "Landroid/content/BroadcastReceiver;", "Landroid/content/Context;", "context", "Landroid/content/Intent;", "intent", "", "isAcceptingCall", "", "executeCallAction", "(Landroid/content/Context;Landroid/content/Intent;Z)V", "enqueueAckMessage", "(Landroid/content/Context;Landroid/content/Intent;)V", "enqueueDirectReply", "enqueueTimedMute", "onReceive", "enqueue", HookHelper.constructorName, "()V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class NotificationActions extends BroadcastReceiver {
    public static final Companion Companion = new Companion(null);
    private static final String NOTIFICATION_CHANNEL_ID = "com.discord.NOTIFICATION_DELETED_CHANNEL_ID";
    private static final String NOTIFICATION_ID = "com.discord.NOTIFICATION_ID";

    /* compiled from: NotificationActions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b$\u0010%J!\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u0005¢\u0006\u0004\b\b\u0010\tJ\u001d\u0010\f\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\f\u0010\rJ-\u0010\u0010\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\n\u0010\u000f\u001a\u00060\u0004j\u0002`\u000e¢\u0006\u0004\b\u0010\u0010\u0011J+\u0010\u0014\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012¢\u0006\u0004\b\u0014\u0010\u0015J9\u0010\u001a\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0017\u001a\u00060\u0004j\u0002`\u00162\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\n\u0010\u0019\u001a\u00060\u0004j\u0002`\u0018¢\u0006\u0004\b\u001a\u0010\u001bJ5\u0010\u001e\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\n\u0010\u000f\u001a\u00060\u0004j\u0002`\u000e2\u0006\u0010\u001d\u001a\u00020\u001c¢\u0006\u0004\b\u001e\u0010\u001fR\u0016\u0010!\u001a\u00020 8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b!\u0010\"R\u0016\u0010#\u001a\u00020 8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b#\u0010\"¨\u0006&"}, d2 = {"Lcom/discord/utilities/fcm/NotificationActions$Companion;", "", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Landroid/content/Intent;", "delete", "(Landroid/content/Context;J)Landroid/content/Intent;", "", "notificationId", "cancel", "(Landroid/content/Context;I)Landroid/content/Intent;", "Lcom/discord/primitives/MessageId;", "messageId", "markAsRead", "(Landroid/content/Context;JJ)Landroid/content/Intent;", "", "channelName", "directReply", "(Landroid/content/Context;JLjava/lang/CharSequence;)Landroid/content/Intent;", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/Timestamp;", "untilTimestamp", "timedMute", "(Landroid/content/Context;JJJ)Landroid/content/Intent;", "", "isAcceptingCall", "callAction", "(Landroid/content/Context;JJZ)Landroid/content/Intent;", "", "NOTIFICATION_CHANNEL_ID", "Ljava/lang/String;", "NOTIFICATION_ID", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final Intent callAction(Context context, long j, long j2, boolean z2) {
            m.checkNotNullParameter(context, "context");
            String str = z2 ? "accept" : "decline";
            return new Intent("com.discord.intent.action.ENQUEUE_WORK", Uri.parse("discord://action/channel/call/" + str + "?channelId=" + j + "&messageId=" + j2), context, NotificationActions.class);
        }

        public final Intent cancel(Context context, int i) {
            m.checkNotNullParameter(context, "context");
            Intent putExtra = new Intent("com.discord.intent.action.NOTIFICATION_CANCEL", Uri.parse("discord://action/notif/cancel?id=" + i), context, NotificationActions.class).putExtra(NotificationActions.NOTIFICATION_ID, i);
            m.checkNotNullExpressionValue(putExtra, "Intent(\n          Intent…ATION_ID, notificationId)");
            return putExtra;
        }

        public final Intent delete(Context context, long j) {
            m.checkNotNullParameter(context, "context");
            Intent putExtra = new Intent("com.discord.intent.action.NOTIFICATION_DELETED", Uri.parse("discord://action/notif/delete?channelId=" + j), context, NotificationActions.class).putExtra(NotificationActions.NOTIFICATION_CHANNEL_ID, j);
            m.checkNotNullExpressionValue(putExtra, "Intent(\n          Intent…ON_CHANNEL_ID, channelId)");
            return putExtra;
        }

        public final Intent directReply(Context context, long j, CharSequence charSequence) {
            m.checkNotNullParameter(context, "context");
            Intent intent = new Intent("com.discord.intent.action.ENQUEUE_WORK", Uri.parse("discord://action/message/reply?channelId=" + j), context, NotificationActions.class);
            intent.putExtra("com.discord.intent.extra.EXTRA_CHANNEL_NAME", charSequence);
            return intent;
        }

        public final Intent markAsRead(Context context, long j, long j2) {
            m.checkNotNullParameter(context, "context");
            return new Intent("com.discord.intent.action.ENQUEUE_WORK", Uri.parse("discord://action/message/ack?channelId=" + j + "&messageId=" + j2), context, NotificationActions.class);
        }

        public final Intent timedMute(Context context, long j, long j2, long j3) {
            m.checkNotNullParameter(context, "context");
            return new Intent("com.discord.intent.action.ENQUEUE_WORK", Uri.parse("discord://action/channel/mute").buildUpon().appendQueryParameter("guildId", String.valueOf(j)).appendQueryParameter("channelId", String.valueOf(j2)).appendQueryParameter("until", String.valueOf(j3)).build(), context, NotificationActions.class);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    private final void enqueueAckMessage(Context context, Intent intent) {
        Long longOrNull;
        Long longOrNull2;
        Uri data = intent.getData();
        if (data != null) {
            m.checkNotNullExpressionValue(data, "intent.data ?: return");
            String queryParameter = data.getQueryParameter("channelId");
            if (queryParameter != null && (longOrNull = s.toLongOrNull(queryParameter)) != null) {
                long longValue = longOrNull.longValue();
                String queryParameter2 = data.getQueryParameter("messageId");
                if (queryParameter2 != null && (longOrNull2 = s.toLongOrNull(queryParameter2)) != null) {
                    long longValue2 = longOrNull2.longValue();
                    m.checkNotNullParameter(context, "context");
                    Data build = new Data.Builder().putAll(h0.mapOf(o.to("com.discord.intent.extra.EXTRA_CHANNEL_ID", Long.valueOf(longValue)), o.to("com.discord.intent.extra.EXTRA_MESSAGE_ID", Long.valueOf(longValue2)))).build();
                    m.checkNotNullExpressionValue(build, "Data.Builder()\n         …     )\n          .build()");
                    OneTimeWorkRequest build2 = new OneTimeWorkRequest.Builder(MessageAckWorker.class).setInputData(build).setBackoffCriteria(BackoffPolicy.LINEAR, 1L, TimeUnit.SECONDS).addTag("message").addTag("ack").setConstraints(new Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()).build();
                    m.checkNotNullExpressionValue(build2, "OneTimeWorkRequestBuilde…     )\n          .build()");
                    WorkManager.getInstance(context).enqueue(build2);
                    NotificationClient.clear$default(NotificationClient.INSTANCE, longValue, context, false, 4, null);
                }
            }
        }
    }

    private final void enqueueDirectReply(Context context, Intent intent) {
        String queryParameter;
        Long longOrNull;
        CharSequence charSequence;
        String obj;
        Uri data = intent.getData();
        if (data != null && (queryParameter = data.getQueryParameter("channelId")) != null && (longOrNull = s.toLongOrNull(queryParameter)) != null) {
            long longValue = longOrNull.longValue();
            CharSequence charSequenceExtra = intent.getCharSequenceExtra("com.discord.intent.extra.EXTRA_CHANNEL_NAME");
            Bundle resultsFromIntent = RemoteInput.getResultsFromIntent(intent);
            if (!(resultsFromIntent == null || (charSequence = resultsFromIntent.getCharSequence("discord_notif_text_input")) == null)) {
                m.checkNotNullExpressionValue(charSequence, "it");
                if (!(!t.isBlank(charSequence))) {
                    charSequence = null;
                }
                if (!(charSequence == null || (obj = charSequence.toString()) == null)) {
                    NotificationCache.INSTANCE.setIgnoreNextClearForAck(longValue, true);
                    m.checkNotNullParameter(context, "context");
                    m.checkNotNullParameter(obj, "message");
                    Data.Builder builder = new Data.Builder();
                    Pair[] pairArr = new Pair[5];
                    pairArr[0] = o.to("com.discord.intent.extra.EXTRA_CHANNEL_ID", Long.valueOf(longValue));
                    pairArr[1] = o.to("com.discord.intent.extra.EXTRA_CHANNEL_NAME", charSequenceExtra != null ? charSequenceExtra.toString() : null);
                    pairArr[2] = o.to("com.discord.intent.extra.EXTRA_MESSAGE_ID", null);
                    pairArr[3] = o.to("MESSAGE_CONTENT", obj);
                    pairArr[4] = o.to("com.discord.intent.extra.EXTRA_STICKER_ID", null);
                    Data build = builder.putAll(h0.mapOf(pairArr)).build();
                    m.checkNotNullExpressionValue(build, "Data.Builder()\n         …     )\n          .build()");
                    OneTimeWorkRequest build2 = new OneTimeWorkRequest.Builder(MessageSendWorker.class).setInputData(build).addTag("message").addTag("send").setConstraints(new Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()).build();
                    m.checkNotNullExpressionValue(build2, "OneTimeWorkRequestBuilde…     )\n          .build()");
                    WorkManager.getInstance(context).enqueue(build2);
                    return;
                }
            }
            NotificationClient.clear$default(NotificationClient.INSTANCE, longValue, context, false, 4, null);
        }
    }

    private final void enqueueTimedMute(Context context, Intent intent) {
        Long longOrNull;
        Long longOrNull2;
        Long longOrNull3;
        Uri data = intent.getData();
        if (data != null) {
            m.checkNotNullExpressionValue(data, "intent.data ?: return");
            String queryParameter = data.getQueryParameter("guildId");
            if (queryParameter != null && (longOrNull = s.toLongOrNull(queryParameter)) != null) {
                long longValue = longOrNull.longValue();
                String queryParameter2 = data.getQueryParameter("channelId");
                if (queryParameter2 != null && (longOrNull2 = s.toLongOrNull(queryParameter2)) != null) {
                    long longValue2 = longOrNull2.longValue();
                    String queryParameter3 = data.getQueryParameter("until");
                    if (queryParameter3 != null && (longOrNull3 = s.toLongOrNull(queryParameter3)) != null) {
                        long longValue3 = longOrNull3.longValue();
                        m.checkNotNullParameter(context, "context");
                        Data build = new Data.Builder().putAll(h0.mapOf(o.to("com.discord.intent.extra.EXTRA_GUILD_ID", Long.valueOf(longValue)), o.to("com.discord.intent.extra.EXTRA_CHANNEL_ID", Long.valueOf(longValue2)), o.to("com.discord.intent.extra.EXTRA_UNTIL_TIMESTAMP_MS", Long.valueOf(longValue3)))).build();
                        m.checkNotNullExpressionValue(build, "Data.Builder()\n         …     )\n          .build()");
                        OneTimeWorkRequest build2 = new OneTimeWorkRequest.Builder(TimedMuteWorker.class).setInputData(build).setBackoffCriteria(BackoffPolicy.LINEAR, 1L, TimeUnit.SECONDS).addTag("channel").addTag(ModelAuditLogEntry.CHANGE_KEY_MUTE).setConstraints(new Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()).build();
                        m.checkNotNullExpressionValue(build2, "OneTimeWorkRequestBuilde…     )\n          .build()");
                        WorkManager.getInstance(context).enqueue(build2);
                        NotificationClient.clear$default(NotificationClient.INSTANCE, longValue2, context, false, 4, null);
                    }
                }
            }
        }
    }

    @RequiresApi(24)
    private final void executeCallAction(Context context, Intent intent, boolean z2) {
        Long longOrNull;
        Long longOrNull2;
        Uri data = intent.getData();
        if (data != null) {
            m.checkNotNullExpressionValue(data, "intent.data ?: return");
            String queryParameter = data.getQueryParameter("channelId");
            if (queryParameter != null && (longOrNull = s.toLongOrNull(queryParameter)) != null) {
                long longValue = longOrNull.longValue();
                String queryParameter2 = data.getQueryParameter("messageId");
                if (queryParameter2 != null && (longOrNull2 = s.toLongOrNull(queryParameter2)) != null) {
                    long longValue2 = longOrNull2.longValue();
                    if (z2) {
                        DiscordConnectService.j.b(context, longValue);
                    } else {
                        m.checkNotNullParameter(context, "context");
                        Data build = new Data.Builder().putAll(h0.mapOf(o.to("com.discord.intent.extra.EXTRA_CHANNEL_ID", Long.valueOf(longValue)), o.to("com.discord.intent.extra.EXTRA_MESSAGE_ID", Long.valueOf(longValue2)))).build();
                        m.checkNotNullExpressionValue(build, "Data.Builder()\n         …     )\n          .build()");
                        BackoffPolicy backoffPolicy = BackoffPolicy.LINEAR;
                        TimeUnit timeUnit = TimeUnit.SECONDS;
                        OneTimeWorkRequest build2 = new OneTimeWorkRequest.Builder(CallActionWorker.class).setInputData(build).setBackoffCriteria(backoffPolicy, 1L, timeUnit).addTag(NotificationCompat.CATEGORY_CALL).addTag("decline").setConstraints(new Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).setTriggerContentMaxDelay(10L, timeUnit).build()).build();
                        m.checkNotNullExpressionValue(build2, "OneTimeWorkRequestBuilde…     )\n          .build()");
                        WorkManager.getInstance(context).enqueue(build2);
                    }
                    NotificationClient.clear$default(NotificationClient.INSTANCE, longValue, context, false, 4, null);
                }
            }
        }
    }

    @RequiresApi(24)
    public final void enqueue(Context context, Intent intent) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(intent, "intent");
        Uri data = intent.getData();
        if (!m.areEqual(data != null ? data.getAuthority() : null, "action")) {
            data = null;
        }
        if (data != null) {
            m.checkNotNullExpressionValue(data, "intent.data.takeIf { it?…y == \"action\" } ?: return");
            String path = data.getPath();
            if (path != null) {
                switch (path.hashCode()) {
                    case -1030684332:
                        if (path.equals("/channel/mute")) {
                            enqueueTimedMute(context, intent);
                            return;
                        }
                        break;
                    case -581403885:
                        if (path.equals("/message/reply")) {
                            enqueueDirectReply(context, intent);
                            return;
                        }
                        break;
                    case 254025278:
                        if (path.equals("/channel/call/accept")) {
                            executeCallAction(context, intent, true);
                            return;
                        }
                        break;
                    case 897701618:
                        if (path.equals("/message/ack")) {
                            enqueueAckMessage(context, intent);
                            return;
                        }
                        break;
                    case 2004820096:
                        if (path.equals("/channel/call/decline")) {
                            executeCallAction(context, intent, false);
                            return;
                        }
                        break;
                }
            }
            AppLog appLog = AppLog.g;
            StringBuilder R = a.R("Unknown work action ");
            R.append(data.getPath());
            Logger.w$default(appLog, R.toString(), null, 2, null);
        }
    }

    @Override // android.content.BroadcastReceiver
    @RequiresApi(24)
    public void onReceive(Context context, Intent intent) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(intent, "intent");
        AppLog.i("Got notification action: " + intent);
        String action = intent.getAction();
        if (action != null) {
            int hashCode = action.hashCode();
            if (hashCode != -1599864135) {
                if (hashCode != -1350900838) {
                    if (hashCode == -26919171 && action.equals("com.discord.intent.action.ENQUEUE_WORK")) {
                        enqueue(context, intent);
                    }
                } else if (action.equals("com.discord.intent.action.NOTIFICATION_DELETED")) {
                    AppLog.i("Got notification deleted: " + intent);
                    Bundle extras = intent.getExtras();
                    if (extras != null) {
                        NotificationClient.clear$default(NotificationClient.INSTANCE, extras.getLong(NOTIFICATION_CHANNEL_ID), context, false, 4, null);
                    }
                }
            } else if (action.equals("com.discord.intent.action.NOTIFICATION_CANCEL")) {
                AppLog.i("Got notification cancel: " + intent);
                Bundle extras2 = intent.getExtras();
                if (extras2 != null) {
                    NotificationManagerCompat.from(context).cancel(extras2.getInt(NOTIFICATION_ID));
                }
            }
        }
    }
}
