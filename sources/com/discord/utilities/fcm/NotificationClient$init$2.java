package com.discord.utilities.fcm;

import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: NotificationClient.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000e\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()Ljava/lang/String;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class NotificationClient$init$2 extends o implements Function0<String> {
    public static final NotificationClient$init$2 INSTANCE = new NotificationClient$init$2();

    public NotificationClient$init$2() {
        super(0);
    }

    @Override // kotlin.jvm.functions.Function0
    public final String invoke() {
        return NotificationClient.INSTANCE.getSettings$app_productionGoogleRelease().getLocale();
    }
}
