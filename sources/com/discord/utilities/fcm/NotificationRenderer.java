package com.discord.utilities.fcm;

import andhook.lib.HookHelper;
import android.app.AlarmManager;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.SystemClock;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.Person;
import androidx.core.graphics.drawable.IconCompat;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppActivity;
import com.discord.app.AppLog;
import com.discord.utilities.ShareUtils;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.extensions.PendingIntentExtensionsKt;
import com.discord.utilities.fcm.NotificationClient;
import com.discord.utilities.fcm.NotificationData;
import com.discord.utilities.images.MGImagesBitmap;
import com.discord.utilities.intent.IntentUtils;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.media.AppSound;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$3;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$4;
import com.discord.utilities.time.ClockFactory;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.discord.widgets.notice.NoticePopup;
import d0.g0.t;
import d0.t.h0;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import j0.l.a.d;
import j0.l.a.d1;
import j0.l.a.f1;
import j0.l.a.r;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import rx.Observable;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;
import xyz.discord.R;
/* compiled from: NotificationRenderer.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010 \n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\r\n\u0002\b\u0011\bÀ\u0002\u0018\u00002\u00020\u0001:\u0001?B\t\b\u0002¢\u0006\u0004\b=\u0010>J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ'\u0010\r\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ3\u0010\u0013\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u0010\u001a\u00020\u000f2\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u0011H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J'\u0010\u0016\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u000fH\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u001f\u0010\u001b\u001a\u00020\u00042\u0006\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\u0018H\u0002¢\u0006\u0004\b\u001b\u0010\u001cJA\u0010$\u001a\u00020#2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\t2\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\t0\u001d2\u0012\u0010\"\u001a\u000e\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020!0\u001fH\u0002¢\u0006\u0004\b$\u0010%J\u0017\u0010'\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020&H\u0007¢\u0006\u0004\b'\u0010(J+\u0010-\u001a\u00020\u00062\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\n\u0010+\u001a\u00060)j\u0002`*2\u0006\u0010,\u001a\u00020\u0018¢\u0006\u0004\b-\u0010.J9\u00102\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010+\u001a\u00060)j\u0002`*2\u0006\u00100\u001a\u00020/2\u0006\u00101\u001a\u00020\u00182\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b2\u00103J%\u00105\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u00104\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b5\u0010\u000eJ\u001d\u00106\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u00104\u001a\u00020\t¢\u0006\u0004\b6\u00107R\u0016\u00108\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b8\u00109R\u0016\u0010:\u001a\u00020)8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b:\u0010;R\u0016\u0010<\u001a\u00020)8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b<\u0010;¨\u0006@"}, d2 = {"Lcom/discord/utilities/fcm/NotificationRenderer;", "", "Landroid/content/Context;", "context", "", "notificationId", "", "autoDismissNotification", "(Landroid/content/Context;I)V", "Lcom/discord/utilities/fcm/NotificationData;", "notification", "Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;", "settings", "displayAndUpdateCache", "(Landroid/content/Context;Lcom/discord/utilities/fcm/NotificationData;Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;)V", "Landroidx/core/app/NotificationCompat$Builder;", "notificationBuilder", "Landroidx/core/graphics/drawable/IconCompat;", "icon", "pushShortcut", "(Landroid/content/Context;Lcom/discord/utilities/fcm/NotificationData;Landroidx/core/app/NotificationCompat$Builder;Landroidx/core/graphics/drawable/IconCompat;)V", "payloadId", "displayNotification", "(Landroid/content/Context;ILandroidx/core/app/NotificationCompat$Builder;)V", "", "disableSound", "disableVibrate", "getNotificationDefaults", "(ZZ)I", "", "lines", "", "", "Landroid/graphics/Bitmap;", "bitmapsForIconUris", "Landroidx/core/app/NotificationCompat$MessagingStyle;", "getMessageStyle", "(Landroid/content/Context;Lcom/discord/utilities/fcm/NotificationData;Ljava/util/List;Ljava/util/Map;)Landroidx/core/app/NotificationCompat$MessagingStyle;", "Landroid/app/Application;", "initNotificationChannels", "(Landroid/app/Application;)V", "", "Lcom/discord/primitives/ChannelId;", "channelId", "isAckRequest", "clear", "(Landroid/content/Context;JZ)V", "", "channelName", "success", "displaySent", "(Landroid/content/Context;JLjava/lang/CharSequence;ZI)V", "notificationData", "display", "displayInApp", "(Landroid/content/Context;Lcom/discord/utilities/fcm/NotificationData;)V", "NOTIFICATION_LIGHT_PERIOD", "I", "NOTIFICATION_AUTO_DISMISS", "J", "NOTIFICATION_ICON_FETCH_DELAY_MS", HookHelper.constructorName, "()V", "NotificationDisplaySubscriptionManager", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class NotificationRenderer {
    public static final NotificationRenderer INSTANCE = new NotificationRenderer();
    private static final long NOTIFICATION_AUTO_DISMISS = 3000;
    private static final long NOTIFICATION_ICON_FETCH_DELAY_MS = 250;
    private static final int NOTIFICATION_LIGHT_PERIOD = 1500;

    /* compiled from: NotificationRenderer.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\bÂ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\t\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\t\u0010\nR2\u0010\r\u001a\u001e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u000bj\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0004`\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lcom/discord/utilities/fcm/NotificationRenderer$NotificationDisplaySubscriptionManager;", "", "", "displayPayloadId", "Lrx/Subscription;", Traits.Payment.Type.SUBSCRIPTION, "", "add", "(ILrx/Subscription;)V", "cancel", "(I)V", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "notificationDisplaySubscriptions", "Ljava/util/HashMap;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class NotificationDisplaySubscriptionManager {
        public static final NotificationDisplaySubscriptionManager INSTANCE = new NotificationDisplaySubscriptionManager();
        private static final HashMap<Integer, Subscription> notificationDisplaySubscriptions = new HashMap<>();

        private NotificationDisplaySubscriptionManager() {
        }

        public final void add(int i, Subscription subscription) {
            m.checkNotNullParameter(subscription, Traits.Payment.Type.SUBSCRIPTION);
            HashMap<Integer, Subscription> hashMap = notificationDisplaySubscriptions;
            synchronized (hashMap) {
                INSTANCE.cancel(i);
                hashMap.put(Integer.valueOf(i), subscription);
            }
        }

        public final void cancel(int i) {
            HashMap<Integer, Subscription> hashMap = notificationDisplaySubscriptions;
            synchronized (hashMap) {
                Subscription remove = hashMap.remove(Integer.valueOf(i));
                if (remove != null) {
                    remove.unsubscribe();
                }
            }
        }
    }

    private NotificationRenderer() {
    }

    private final void autoDismissNotification(Context context, int i) {
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 0, NotificationActions.Companion.cancel(context, i), PendingIntentExtensionsKt.immutablePendingIntentFlag$default(0, 1, null));
        Object systemService = context.getSystemService(NotificationCompat.CATEGORY_ALARM);
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.app.AlarmManager");
        ((AlarmManager) systemService).set(2, SystemClock.elapsedRealtime() + NOTIFICATION_AUTO_DISMISS, broadcast);
    }

    private final void displayAndUpdateCache(Context context, NotificationData notificationData, NotificationClient.SettingsV2 settingsV2) {
        NotificationCompat.Builder contentIntent = new NotificationCompat.Builder(context, notificationData.getNotificationChannelId()).setAutoCancel(true).setOnlyAlertOnce(true).setSmallIcon(notificationData.getSmallIcon()).setColor(ColorCompat.getThemedColor(context, (int) R.attr.color_brand_500)).setCategory(notificationData.getNotificationCategory()).setContentTitle(notificationData.getTitle(context)).setContentText(notificationData.getContent(context)).setDefaults(getNotificationDefaults(settingsV2.isDisableSound(), settingsV2.isDisableVibrate())).setDeleteIntent(notificationData.getDeleteIntent(context)).setContentIntent(notificationData.getContentIntent(context));
        m.checkNotNullExpressionValue(contentIntent, "NotificationCompat.Build…etContentIntent(context))");
        contentIntent.setGroup(notificationData.getGroupKey());
        NotificationData.DisplayPayload andUpdate = NotificationCache.INSTANCE.getAndUpdate(notificationData);
        List<NotificationData> extras = andUpdate.getExtras();
        PendingIntent fullScreenIntent = notificationData.getFullScreenIntent(context);
        if (fullScreenIntent != null) {
            contentIntent.setFullScreenIntent(fullScreenIntent, true);
            contentIntent.setVisibility(1);
        }
        Uri uri = null;
        if (!extras.isEmpty()) {
            contentIntent.setNumber(extras.size());
            contentIntent.setStyle(getMessageStyle(context, notificationData, extras, new MGImagesBitmap.CloseableBitmaps(h0.emptyMap(), false, 2, null)));
        }
        if (notificationData.getShouldUseBigText()) {
            contentIntent.setStyle(new NotificationCompat.BigTextStyle().bigText(notificationData.getContent(context)));
        }
        int i = Build.VERSION.SDK_INT;
        if (i < 26) {
            contentIntent.setPriority(notificationData.getNotificationPriority()).setVibrate(new long[]{0});
            if (!settingsV2.isDisableBlink()) {
                contentIntent.setLights(ColorCompat.getThemedColor(context, (int) R.attr.color_brand), NOTIFICATION_LIGHT_PERIOD, NOTIFICATION_LIGHT_PERIOD);
            }
            Uri notificationSound = notificationData.getNotificationSound(context);
            if (notificationSound != null) {
                if (!settingsV2.isDisableSound()) {
                    uri = notificationSound;
                }
                if (uri != null) {
                    contentIntent.setSound(uri).setDefaults(INSTANCE.getNotificationDefaults(settingsV2.isDisableSound(), settingsV2.isDisableVibrate()) & (-2));
                }
            }
        }
        if (i >= 24) {
            for (NotificationCompat.Action action : n.listOf((Object[]) new NotificationCompat.Action[]{notificationData.getMarkAsReadAction(context), notificationData.getDirectReplyAction(context, settingsV2.getSendBlockedChannels()), notificationData.getTimedMute(context, ClockFactory.get(), extras.size()), notificationData.getCallAction(context, false), notificationData.getCallAction(context, true)})) {
                if (action != null) {
                    contentIntent.addAction(action);
                }
            }
        }
        NotificationDisplaySubscriptionManager.INSTANCE.cancel(andUpdate.getId());
        List<NotificationData> extras2 = andUpdate.getExtras();
        ArrayList arrayList = new ArrayList();
        for (NotificationData notificationData2 : extras2) {
            String iconUrlForAvatar = notificationData2.getIconUrlForAvatar();
            if (iconUrlForAvatar != null) {
                arrayList.add(iconUrlForAvatar);
            }
        }
        List<String> plus = u.plus((Collection<? extends String>) arrayList, notificationData.getIconUrl());
        ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(plus, 10));
        for (String str : plus) {
            arrayList2.add(new MGImagesBitmap.ImageRequest(str, true));
        }
        Set mutableSet = u.toMutableSet(arrayList2);
        Observable<MGImagesBitmap.CloseableBitmaps> Q = MGImagesBitmap.getBitmaps(mutableSet).Q();
        CompositeSubscription compositeSubscription = new CompositeSubscription();
        Observable<Long> a02 = Observable.d0(NOTIFICATION_ICON_FETCH_DELAY_MS, TimeUnit.MILLISECONDS).a0(Observable.h0(new r(Q.j, new f1(new d1(d.k)))));
        m.checkNotNullExpressionValue(a02, "Observable\n        .time…Next(Observable.never()))");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(a02), (r18 & 1) != 0 ? null : null, "Unable to display notification, timeout.", (r18 & 4) != 0 ? null : new NotificationRenderer$displayAndUpdateCache$4(compositeSubscription), new NotificationRenderer$displayAndUpdateCache$5(context, andUpdate, contentIntent, notificationData), (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
        m.checkNotNullExpressionValue(Q, "bitmapsObservable");
        Observable ui = ObservableExtensionsKt.ui(Q);
        StringBuilder R = a.R("Unable to display notification multi-fetch ");
        R.append(mutableSet.size());
        R.append(" bitmaps.");
        ObservableExtensionsKt.appSubscribe(ui, (r18 & 1) != 0 ? null : null, R.toString(), (r18 & 4) != 0 ? null : new NotificationRenderer$displayAndUpdateCache$6(compositeSubscription), new NotificationRenderer$displayAndUpdateCache$7(notificationData, contentIntent, extras, context, andUpdate), (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
        NotificationDisplaySubscriptionManager.INSTANCE.add(andUpdate.getId(), compositeSubscription);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void displayNotification(Context context, int i, NotificationCompat.Builder builder) {
        try {
            NotificationManagerCompat.from(context).notify(i, builder.build());
        } catch (IllegalStateException e) {
            Logger.e$default(AppLog.g, "NotifyError", e, null, 4, null);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final NotificationCompat.MessagingStyle getMessageStyle(Context context, NotificationData notificationData, List<NotificationData> list, Map<String, Bitmap> map) {
        Person build = new Person.Builder().setName(context.getString(R.string.f3833me)).setKey("me").build();
        m.checkNotNullExpressionValue(build, "Person.Builder()\n       …ey(\"me\")\n        .build()");
        NotificationCompat.MessagingStyle groupConversation = new NotificationCompat.MessagingStyle(build).setConversationTitle(notificationData.getConversationTitle(context)).setGroupConversation(notificationData.isGroupConversation());
        m.checkNotNullExpressionValue(groupConversation, "NotificationCompat.Messa…tion.isGroupConversation)");
        for (NotificationData notificationData2 : list) {
            Bitmap bitmap = map.get(notificationData2.getIconUrlForAvatar());
            IconCompat createWithBitmap = bitmap != null ? IconCompat.createWithBitmap(bitmap) : null;
            Person build2 = notificationData2.getSender(context).toBuilder().setIcon(createWithBitmap).build();
            m.checkNotNullExpressionValue(build2, "person.toBuilder()\n     …(icon)\n          .build()");
            groupConversation.addMessage(notificationData2.getContent(context), notificationData2.getMessageIdTimestamp() + (createWithBitmap == null ? 0 : 1), build2);
        }
        return groupConversation;
    }

    private final int getNotificationDefaults(boolean z2, boolean z3) {
        int i = !z2 ? 1 : 0;
        return !z3 ? i | 2 : i;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void pushShortcut(Context context, NotificationData notificationData, NotificationCompat.Builder builder, IconCompat iconCompat) {
        ShareUtils shareUtils = ShareUtils.INSTANCE;
        shareUtils.addShortcut(context, ShareUtils.toShortcutInfo$default(shareUtils, context, notificationData.getChannelId(), notificationData.getTitle(context), null, iconCompat, null, 0, 104, null), builder);
    }

    public static /* synthetic */ void pushShortcut$default(NotificationRenderer notificationRenderer, Context context, NotificationData notificationData, NotificationCompat.Builder builder, IconCompat iconCompat, int i, Object obj) {
        if ((i & 8) != 0) {
            iconCompat = null;
        }
        notificationRenderer.pushShortcut(context, notificationData, builder, iconCompat);
    }

    public final void clear(Context context, long j, boolean z2) {
        NotificationCache.INSTANCE.remove(j, z2, new NotificationRenderer$clear$1(context));
    }

    public final void display(Context context, NotificationData notificationData, NotificationClient.SettingsV2 settingsV2) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(notificationData, "notificationData");
        m.checkNotNullParameter(settingsV2, "settings");
        try {
            displayAndUpdateCache(context, notificationData, settingsV2);
        } catch (Exception e) {
            Logger.e$default(AppLog.g, "Unable to display notification.", e, null, 4, null);
        }
    }

    public final void displayInApp(Context context, NotificationData notificationData) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(notificationData, "notificationData");
        if (notificationData.canDisplayInApp()) {
            CharSequence title = notificationData.getTitle(context);
            CharSequence content = notificationData.getContent(context);
            if (content == null) {
                content = "";
            }
            CharSequence charSequence = content;
            if (!t.isBlank(title) || !t.isBlank(charSequence)) {
                NoticePopup noticePopup = NoticePopup.INSTANCE;
                StringBuilder R = a.R("InAppNotif#");
                R.append(ClockFactory.get().currentTimeMillis());
                NoticePopup.enqueue$default(noticePopup, R.toString(), title, null, charSequence, null, null, null, null, notificationData.getIconUrl(), null, null, null, null, null, new NotificationRenderer$displayInApp$1(notificationData, context), 16116, null);
            }
        }
    }

    public final void displaySent(Context context, long j, CharSequence charSequence, boolean z2, int i) {
        CharSequence b2;
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(charSequence, "channelName");
        int i2 = z2 ? R.string.notification_reply_success : R.string.notification_reply_failed;
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 0, NotificationActions.Companion.delete(context, j), PendingIntentExtensionsKt.immutablePendingIntentFlag(134217728));
        Intent selectChannel$default = IntentUtils.RouteBuilders.selectChannel$default(j, 0L, null, 6, null);
        selectChannel$default.setClass(context, AppActivity.Main.class);
        PendingIntent activity = PendingIntent.getActivity(context, 0, selectChannel$default, PendingIntentExtensionsKt.immutablePendingIntentFlag(134217728));
        NotificationCompat.Builder category = new NotificationCompat.Builder(context, NotificationClient.NOTIF_CHANNEL_MESSAGES).setAutoCancel(true).setSmallIcon(R.drawable.ic_notification_message_24dp).setCategory(NotificationCompat.CATEGORY_MESSAGE);
        b2 = b.b(context, i2, new Object[]{charSequence}, (r4 & 4) != 0 ? b.C0034b.j : null);
        Notification build = category.setContentText(b2).setDeleteIntent(broadcast).setContentIntent(activity).build();
        m.checkNotNullExpressionValue(build, "NotificationCompat.Build…ent)\n            .build()");
        NotificationManagerCompat.from(context).notify(i, build);
        autoDismissNotification(context, i);
    }

    @RequiresApi(26)
    public final void initNotificationChannels(Application application) {
        m.checkNotNullParameter(application, "context");
        NotificationChannel notificationChannel = new NotificationChannel(NotificationClient.NOTIF_CHANNEL_CALLS, application.getString(R.string.call), 4);
        NotificationChannel notificationChannel2 = new NotificationChannel(NotificationClient.NOTIF_CHANNEL_MEDIA_CONNECTIONS, application.getString(R.string.voice), 2);
        List<NotificationChannel> listOf = n.listOf((Object[]) new NotificationChannel[]{notificationChannel, notificationChannel2, new NotificationChannel(NotificationClient.NOTIF_CHANNEL_MESSAGES, application.getString(R.string.messages), 4), new NotificationChannel(NotificationClient.NOTIF_CHANNEL_MESSAGES_DIRECT, application.getString(R.string.direct_messages), 4), new NotificationChannel(NotificationClient.NOTIF_CHANNEL_SOCIAL, application.getString(R.string.friends), 2), new NotificationChannel(NotificationClient.NOTIF_CHANNEL_GAME_DETECTION, application.getString(R.string.game_detection_service), 1), new NotificationChannel(NotificationClient.NOTIF_CHANNEL_STAGE_START, application.getString(R.string.stage_start_notification_category), 4), new NotificationChannel(NotificationClient.NOTIF_GUILD_SCHEDULED_EVENT_START, application.getString(R.string.guild_scheduled_event_live), 4), new NotificationChannel(NotificationClient.NOTIF_GENERAL, application.getString(R.string.other), 2), new NotificationChannel(NotificationClient.NOTIF_GENERAL_HIGH_PRIO, application.getString(R.string.other_high_priority), 4)});
        for (NotificationChannel notificationChannel3 : listOf) {
            notificationChannel3.setShowBadge(true);
            notificationChannel3.enableVibration(true);
            notificationChannel3.enableLights(true);
            notificationChannel3.setLightColor(ColorCompat.getThemedColor(application, (int) R.attr.color_brand));
        }
        notificationChannel.setDescription(application.getString(R.string.call));
        notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        notificationChannel.setShowBadge(false);
        StringBuilder sb = new StringBuilder();
        sb.append("android.resource://");
        sb.append(application.getPackageName() + MentionUtilsKt.SLASH_CHAR + AppSound.Companion.getSOUND_CALL_RINGING().getResId());
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "StringBuilder()\n        …)\n            .toString()");
        notificationChannel.setSound(Uri.parse(sb2), new AudioAttributes.Builder().setUsage(7).setContentType(2).build());
        notificationChannel2.setShowBadge(false);
        notificationChannel2.enableVibration(false);
        Object systemService = application.getSystemService("notification");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.app.NotificationManager");
        ((NotificationManager) systemService).createNotificationChannels(listOf);
    }
}
