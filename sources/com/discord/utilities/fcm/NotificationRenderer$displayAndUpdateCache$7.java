package com.discord.utilities.fcm;

import android.content.Context;
import android.graphics.Bitmap;
import androidx.core.app.NotificationCompat;
import androidx.core.graphics.drawable.IconCompat;
import com.discord.utilities.fcm.NotificationData;
import com.discord.utilities.images.MGImagesBitmap;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: NotificationRenderer.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/utilities/images/MGImagesBitmap$CloseableBitmaps;", "kotlin.jvm.PlatformType", "bitmapsForIconUris", "", "invoke", "(Lcom/discord/utilities/images/MGImagesBitmap$CloseableBitmaps;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class NotificationRenderer$displayAndUpdateCache$7 extends o implements Function1<MGImagesBitmap.CloseableBitmaps, Unit> {
    public final /* synthetic */ Context $context;
    public final /* synthetic */ NotificationData.DisplayPayload $displayPayload;
    public final /* synthetic */ List $displayPayloadExtras;
    public final /* synthetic */ NotificationData $notification;
    public final /* synthetic */ NotificationCompat.Builder $notificationBuilder;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public NotificationRenderer$displayAndUpdateCache$7(NotificationData notificationData, NotificationCompat.Builder builder, List list, Context context, NotificationData.DisplayPayload displayPayload) {
        super(1);
        this.$notification = notificationData;
        this.$notificationBuilder = builder;
        this.$displayPayloadExtras = list;
        this.$context = context;
        this.$displayPayload = displayPayload;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(MGImagesBitmap.CloseableBitmaps closeableBitmaps) {
        invoke2(closeableBitmaps);
        return Unit.a;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(MGImagesBitmap.CloseableBitmaps closeableBitmaps) {
        NotificationCompat.MessagingStyle messageStyle;
        try {
            Bitmap bitmap = (Bitmap) closeableBitmaps.get((Object) this.$notification.getIconUrl());
            if (bitmap != null) {
                this.$notificationBuilder.setLargeIcon(bitmap);
            }
            Throwable th = null;
            if (!this.$displayPayloadExtras.isEmpty()) {
                this.$notificationBuilder.setNumber(this.$displayPayloadExtras.size());
                NotificationCompat.Builder builder = this.$notificationBuilder;
                NotificationRenderer notificationRenderer = NotificationRenderer.INSTANCE;
                Context context = this.$context;
                NotificationData notificationData = this.$notification;
                List list = this.$displayPayloadExtras;
                m.checkNotNullExpressionValue(closeableBitmaps, "bitmapsForIconUris");
                messageStyle = notificationRenderer.getMessageStyle(context, notificationData, list, closeableBitmaps);
                builder.setStyle(messageStyle);
                notificationRenderer.pushShortcut(this.$context, this.$notification, this.$notificationBuilder, bitmap != null ? IconCompat.createWithBitmap(bitmap) : th);
            }
            NotificationRenderer.INSTANCE.displayNotification(this.$context, this.$displayPayload.getId(), this.$notificationBuilder);
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }
}
