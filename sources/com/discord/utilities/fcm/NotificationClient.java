package com.discord.utilities.fcm;

import andhook.lib.HookHelper;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.collection.ArrayMap;
import androidx.core.app.FrameMetricsAggregator;
import b.d.b.a.a;
import b.i.c.c;
import b.i.c.w.i;
import com.discord.app.AppLog;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.persister.Persister;
import com.discord.utilities.rest.RestAPI;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import d0.o;
import d0.t.h0;
import d0.z.d.m;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: NotificationClient.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0013\n\u0002\u0018\u0002\n\u0002\b\n\bÇ\u0002\u0018\u00002\u00020\u0001:\u0002<=B\t\b\u0002¢\u0006\u0004\b:\u0010;J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0005\u0010\u0006J\r\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\b\u0010\tJ#\u0010\r\u001a\u00020\u00042\u0014\u0010\f\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u000b\u0012\u0004\u0012\u00020\u00040\n¢\u0006\u0004\b\r\u0010\u000eJ\u001f\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u0007H\u0000¢\u0006\u0004\b\u0012\u0010\u0013J#\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00010\u00172\b\u0010\u0016\u001a\u0004\u0018\u00010\u0015¢\u0006\u0004\b\u0018\u0010\u0019J\u0017\u0010\u001b\u001a\u00020\u00042\b\u0010\u001a\u001a\u0004\u0018\u00010\u000b¢\u0006\u0004\b\u001b\u0010\u001cJ/\u0010#\u001a\u00020\u00042\n\u0010\u001f\u001a\u00060\u001dj\u0002`\u001e2\n\b\u0002\u0010!\u001a\u0004\u0018\u00010 2\b\b\u0002\u0010\"\u001a\u00020\u0007¢\u0006\u0004\b#\u0010$J\u000f\u0010'\u001a\u00020\u000fH\u0000¢\u0006\u0004\b%\u0010&R\u0016\u0010(\u001a\u00020\u000b8\u0000@\u0000X\u0080T¢\u0006\u0006\n\u0004\b(\u0010)R\u0016\u0010*\u001a\u00020\u000b8\u0000@\u0000X\u0080T¢\u0006\u0006\n\u0004\b*\u0010)R\u0016\u0010+\u001a\u00020\u000b8\u0000@\u0000X\u0080T¢\u0006\u0006\n\u0004\b+\u0010)R\u0016\u0010,\u001a\u00020\u000b8\u0000@\u0000X\u0080T¢\u0006\u0006\n\u0004\b,\u0010)R\u0016\u0010-\u001a\u00020\u000b8\u0000@\u0000X\u0080T¢\u0006\u0006\n\u0004\b-\u0010)R$\u0010.\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u000b\u0012\u0004\u0012\u00020\u00040\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b.\u0010/R\u0013\u00100\u001a\u00020\u00078F@\u0006¢\u0006\u0006\u001a\u0004\b0\u0010\tR\u0016\u00101\u001a\u00020\u000b8\u0000@\u0000X\u0080T¢\u0006\u0006\n\u0004\b1\u0010)R\u0016\u00102\u001a\u00020\u000b8\u0000@\u0000X\u0080T¢\u0006\u0006\n\u0004\b2\u0010)R\u0016\u00103\u001a\u00020\u000b8\u0000@\u0000X\u0080T¢\u0006\u0006\n\u0004\b3\u0010)R\u001c\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f048\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u00105R\u0016\u00106\u001a\u00020\u000b8\u0000@\u0000X\u0080T¢\u0006\u0006\n\u0004\b6\u0010)R\u0018\u0010\u001a\u001a\u0004\u0018\u00010\u000b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001a\u0010)R\u0018\u0010!\u001a\u0004\u0018\u00010 8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b!\u00107R\u0016\u0010\u0011\u001a\u00020\u00078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0011\u00108R\u0016\u00109\u001a\u00020\u000b8\u0000@\u0000X\u0080T¢\u0006\u0006\n\u0004\b9\u0010)¨\u0006>"}, d2 = {"Lcom/discord/utilities/fcm/NotificationClient;", "", "Landroid/app/Application;", "application", "", "init", "(Landroid/app/Application;)V", "", "isOsLevelNotificationEnabled", "()Z", "Lkotlin/Function1;", "", "onDeviceRegistrationIdReceived", "setRegistrationIdReceived", "(Lkotlin/jvm/functions/Function1;)V", "Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;", "settings", "isBackgrounded", "updateSettings$app_productionGoogleRelease", "(Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;Z)V", "updateSettings", "Landroid/content/Intent;", "intent", "", "buildTrackingData", "(Landroid/content/Intent;)Ljava/util/Map;", "token", "onNewToken", "(Ljava/lang/String;)V", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Landroid/content/Context;", "context", "isAckRequest", "clear", "(JLandroid/content/Context;Z)V", "getSettings$app_productionGoogleRelease", "()Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;", "getSettings", "NOTIF_CHANNEL_GAME_DETECTION", "Ljava/lang/String;", "NOTIF_CHANNEL_SOCIAL", "NOTIF_CHANNEL_MESSAGES_DIRECT", "NOTIF_CHANNEL_STAGE_START", "NOTIF_GENERAL_HIGH_PRIO", "tokenCallback", "Lkotlin/jvm/functions/Function1;", "isAuthed", "NOTIF_GUILD_SCHEDULED_EVENT_START", "NOTIF_CHANNEL_CALLS", "NOTIF_CHANNEL_MEDIA_CONNECTIONS", "Lcom/discord/utilities/persister/Persister;", "Lcom/discord/utilities/persister/Persister;", "NOTIF_GENERAL", "Landroid/content/Context;", "Z", "NOTIF_CHANNEL_MESSAGES", HookHelper.constructorName, "()V", "FCMMessagingService", "SettingsV2", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
@SuppressLint({"StaticFieldLeak"})
/* loaded from: classes2.dex */
public final class NotificationClient {
    public static final String NOTIF_CHANNEL_CALLS = "Calls";
    public static final String NOTIF_CHANNEL_GAME_DETECTION = "Game Detection";
    public static final String NOTIF_CHANNEL_MEDIA_CONNECTIONS = "Media Connections";
    public static final String NOTIF_CHANNEL_MESSAGES = "Messages";
    public static final String NOTIF_CHANNEL_MESSAGES_DIRECT = "DirectMessages";
    public static final String NOTIF_CHANNEL_SOCIAL = "Social";
    public static final String NOTIF_CHANNEL_STAGE_START = "Stage Live";
    public static final String NOTIF_GENERAL = "General";
    public static final String NOTIF_GENERAL_HIGH_PRIO = "GeneralHigh";
    public static final String NOTIF_GUILD_SCHEDULED_EVENT_START = "Guild Event Live";
    private static Context context;
    private static String token;
    public static final NotificationClient INSTANCE = new NotificationClient();
    private static final Persister<SettingsV2> settings = new Persister<>("NOTIFICATION_CLIENT_SETTINGS_V3", new SettingsV2(false, false, false, false, false, false, null, null, null, FrameMetricsAggregator.EVERY_DURATION, null));
    private static Function1<? super String, Unit> tokenCallback = NotificationClient$tokenCallback$1.INSTANCE;
    private static boolean isBackgrounded = true;

    /* compiled from: NotificationClient.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\t\u0010\n¨\u0006\r"}, d2 = {"Lcom/discord/utilities/fcm/NotificationClient$FCMMessagingService;", "Lcom/google/firebase/messaging/FirebaseMessagingService;", "", "token", "", "onNewToken", "(Ljava/lang/String;)V", "Lcom/google/firebase/messaging/RemoteMessage;", "remoteMessage", "onMessageReceived", "(Lcom/google/firebase/messaging/RemoteMessage;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class FCMMessagingService extends FirebaseMessagingService {
        @Override // com.google.firebase.messaging.FirebaseMessagingService
        public void onMessageReceived(RemoteMessage remoteMessage) {
            m.checkNotNullParameter(remoteMessage, "remoteMessage");
            super.onMessageReceived(remoteMessage);
            if (remoteMessage.k == null) {
                Bundle bundle = remoteMessage.j;
                ArrayMap arrayMap = new ArrayMap();
                for (String str : bundle.keySet()) {
                    Object obj = bundle.get(str);
                    if (obj instanceof String) {
                        String str2 = (String) obj;
                        if (!str.startsWith("google.") && !str.startsWith("gcm.") && !str.equals("from") && !str.equals("message_type") && !str.equals("collapse_key")) {
                            arrayMap.put(str, str2);
                        }
                    }
                }
                remoteMessage.k = arrayMap;
            }
            Map<String, String> map = remoteMessage.k;
            m.checkNotNullExpressionValue(map, "remoteMessage.data");
            AppLog.i("Got notification: " + map);
            NotificationData notificationData = new NotificationData(map);
            SettingsV2 settings$app_productionGoogleRelease = NotificationClient.INSTANCE.getSettings$app_productionGoogleRelease();
            for (Long l : notificationData.getAckChannelIds()) {
                NotificationClient.INSTANCE.clear(l.longValue(), NotificationClient.context, true);
            }
            NotificationClient notificationClient = NotificationClient.INSTANCE;
            Context context = NotificationClient.context;
            if (context == null) {
                Logger.e$default(AppLog.g, "Not showing notification because context was null.", null, null, 6, null);
            } else if (notificationData.isValid() && settings$app_productionGoogleRelease.isAuthed()) {
                if (notificationData.getChannelId() != -1) {
                    NotificationCache.INSTANCE.setIgnoreNextClearForAck(notificationData.getChannelId(), false);
                }
                if (NotificationClient.isBackgrounded && settings$app_productionGoogleRelease.isEnabled()) {
                    NotificationRenderer.INSTANCE.display(context, notificationData, settings$app_productionGoogleRelease);
                } else if (!NotificationClient.isBackgrounded && settings$app_productionGoogleRelease.isEnabledInApp()) {
                    NotificationRenderer.INSTANCE.displayInApp(context, notificationData);
                }
            } else if (m.areEqual(notificationData.getType(), NotificationData.TYPE_MESSAGE_CREATE)) {
                Logger.e$default(AppLog.g, "Not showing invalid notification", null, h0.mapOf(o.to("messageId", String.valueOf(notificationData.getMessageId())), o.to("channelId", String.valueOf(notificationData.getChannelId())), o.to("isAuthed", String.valueOf(settings$app_productionGoogleRelease.isAuthed())), o.to("type", notificationData.getType())), 2, null);
            }
        }

        @Override // com.google.firebase.messaging.FirebaseMessagingService
        public void onNewToken(String str) {
            m.checkNotNullParameter(str, "token");
            NotificationClient.INSTANCE.onNewToken(str);
        }
    }

    private NotificationClient() {
    }

    public static /* synthetic */ void clear$default(NotificationClient notificationClient, long j, Context context2, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            context2 = null;
        }
        if ((i & 4) != 0) {
            z2 = false;
        }
        notificationClient.clear(j, context2, z2);
    }

    public final Map<String, Object> buildTrackingData(Intent intent) {
        return NotificationData.Companion.buildTrackingData(intent);
    }

    public final void clear(long j, Context context2, boolean z2) {
        NotificationRenderer notificationRenderer = NotificationRenderer.INSTANCE;
        if (context2 == null) {
            context2 = context;
        }
        notificationRenderer.clear(context2, j, z2);
    }

    public final synchronized SettingsV2 getSettings$app_productionGoogleRelease() {
        return settings.get();
    }

    @TargetApi(26)
    public final void init(Application application) {
        FirebaseMessaging firebaseMessaging;
        m.checkNotNullParameter(application, "application");
        context = application;
        if (isOsLevelNotificationEnabled()) {
            NotificationRenderer.INSTANCE.initNotificationChannels(application);
        }
        RestAPI.AppHeadersProvider.authTokenProvider = NotificationClient$init$1.INSTANCE;
        RestAPI.AppHeadersProvider.localeProvider = NotificationClient$init$2.INSTANCE;
        try {
            synchronized (FirebaseMessaging.class) {
                firebaseMessaging = FirebaseMessaging.getInstance(c.b());
            }
            m.checkNotNullExpressionValue(firebaseMessaging, "FirebaseMessaging.getInstance()");
            m.checkNotNullExpressionValue(firebaseMessaging.d.f().h(i.a).b(NotificationClient$init$3.INSTANCE), "FirebaseMessaging.getIns…eption)\n        }\n      }");
        } catch (IllegalStateException e) {
            AppLog.g.w("FCM service start error", e);
        }
    }

    public final synchronized boolean isAuthed() {
        return settings.get().isAuthed();
    }

    public final boolean isOsLevelNotificationEnabled() {
        return Build.VERSION.SDK_INT >= 26;
    }

    public final synchronized void onNewToken(String str) {
        AppLog appLog = AppLog.g;
        StringBuilder sb = new StringBuilder();
        sb.append("FCM token received. hash=");
        sb.append(str != null ? Integer.valueOf(str.hashCode()) : null);
        Logger.d$default(appLog, sb.toString(), null, 2, null);
        token = str;
        tokenCallback.invoke(str);
    }

    public final synchronized void setRegistrationIdReceived(Function1<? super String, Unit> function1) {
        m.checkNotNullParameter(function1, "onDeviceRegistrationIdReceived");
        tokenCallback = function1;
        function1.invoke(token);
    }

    public final synchronized void updateSettings$app_productionGoogleRelease(SettingsV2 settingsV2, boolean z2) {
        m.checkNotNullParameter(settingsV2, "settings");
        settings.set(settingsV2, z2);
        isBackgrounded = z2;
    }

    /* compiled from: NotificationClient.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\b\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001Bm\u0012\b\b\u0002\u0010\u0013\u001a\u00020\u0002\u0012\b\b\u0002\u0010\u0014\u001a\u00020\u0002\u0012\b\b\u0002\u0010\u0015\u001a\u00020\u0002\u0012\b\b\u0002\u0010\u0016\u001a\u00020\u0002\u0012\b\b\u0002\u0010\u0017\u001a\u00020\u0002\u0012\b\b\u0002\u0010\u0018\u001a\u00020\u0002\u0012\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\n\u0012\b\b\u0002\u0010\u001a\u001a\u00020\n\u0012\u0012\b\u0002\u0010\u001b\u001a\f\u0012\b\u0012\u00060\u000fj\u0002`\u00100\u000e¢\u0006\u0004\b,\u0010-J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0004J\u0010\u0010\b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\t\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\u0004J\u0012\u0010\u000b\u001a\u0004\u0018\u00010\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\r\u0010\fJ\u001a\u0010\u0011\u001a\f\u0012\b\u0012\u00060\u000fj\u0002`\u00100\u000eHÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012Jv\u0010\u001c\u001a\u00020\u00002\b\b\u0002\u0010\u0013\u001a\u00020\u00022\b\b\u0002\u0010\u0014\u001a\u00020\u00022\b\b\u0002\u0010\u0015\u001a\u00020\u00022\b\b\u0002\u0010\u0016\u001a\u00020\u00022\b\b\u0002\u0010\u0017\u001a\u00020\u00022\b\b\u0002\u0010\u0018\u001a\u00020\u00022\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\n2\b\b\u0002\u0010\u001a\u001a\u00020\n2\u0012\b\u0002\u0010\u001b\u001a\f\u0012\b\u0012\u00060\u000fj\u0002`\u00100\u000eHÆ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001e\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u001e\u0010\fJ\u0010\u0010 \u001a\u00020\u001fHÖ\u0001¢\u0006\u0004\b \u0010!J\u001a\u0010#\u001a\u00020\u00022\b\u0010\"\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b#\u0010$R\u0019\u0010\u0014\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010%\u001a\u0004\b\u0014\u0010\u0004R#\u0010\u001b\u001a\f\u0012\b\u0012\u00060\u000fj\u0002`\u00100\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010&\u001a\u0004\b'\u0010\u0012R\u0019\u0010(\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010%\u001a\u0004\b(\u0010\u0004R\u0019\u0010\u001a\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010)\u001a\u0004\b*\u0010\fR\u0019\u0010\u0013\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010%\u001a\u0004\b\u0013\u0010\u0004R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010)\u001a\u0004\b+\u0010\fR\u0019\u0010\u0016\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010%\u001a\u0004\b\u0016\u0010\u0004R\u0019\u0010\u0017\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010%\u001a\u0004\b\u0017\u0010\u0004R\u0019\u0010\u0015\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010%\u001a\u0004\b\u0015\u0010\u0004R\u0019\u0010\u0018\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010%\u001a\u0004\b\u0018\u0010\u0004¨\u0006."}, d2 = {"Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;", "", "", "component1", "()Z", "component2", "component3", "component4", "component5", "component6", "", "component7", "()Ljava/lang/String;", "component8", "", "", "Lcom/discord/primitives/ChannelId;", "component9", "()Ljava/util/Set;", "isEnabled", "isEnabledInApp", "isWake", "isDisableBlink", "isDisableSound", "isDisableVibrate", "token", "locale", "sendBlockedChannels", "copy", "(ZZZZZZLjava/lang/String;Ljava/lang/String;Ljava/util/Set;)Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Ljava/util/Set;", "getSendBlockedChannels", "isAuthed", "Ljava/lang/String;", "getLocale", "getToken", HookHelper.constructorName, "(ZZZZZZLjava/lang/String;Ljava/lang/String;Ljava/util/Set;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class SettingsV2 {
        private final boolean isAuthed;
        private final boolean isDisableBlink;
        private final boolean isDisableSound;
        private final boolean isDisableVibrate;
        private final boolean isEnabled;
        private final boolean isEnabledInApp;
        private final boolean isWake;
        private final String locale;
        private final Set<Long> sendBlockedChannels;
        private final String token;

        public SettingsV2() {
            this(false, false, false, false, false, false, null, null, null, FrameMetricsAggregator.EVERY_DURATION, null);
        }

        public SettingsV2(boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, String str, String str2, Set<Long> set) {
            m.checkNotNullParameter(str2, "locale");
            m.checkNotNullParameter(set, "sendBlockedChannels");
            this.isEnabled = z2;
            this.isEnabledInApp = z3;
            this.isWake = z4;
            this.isDisableBlink = z5;
            this.isDisableSound = z6;
            this.isDisableVibrate = z7;
            this.token = str;
            this.locale = str2;
            this.sendBlockedChannels = set;
            this.isAuthed = str != null;
        }

        public final boolean component1() {
            return this.isEnabled;
        }

        public final boolean component2() {
            return this.isEnabledInApp;
        }

        public final boolean component3() {
            return this.isWake;
        }

        public final boolean component4() {
            return this.isDisableBlink;
        }

        public final boolean component5() {
            return this.isDisableSound;
        }

        public final boolean component6() {
            return this.isDisableVibrate;
        }

        public final String component7() {
            return this.token;
        }

        public final String component8() {
            return this.locale;
        }

        public final Set<Long> component9() {
            return this.sendBlockedChannels;
        }

        public final SettingsV2 copy(boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, String str, String str2, Set<Long> set) {
            m.checkNotNullParameter(str2, "locale");
            m.checkNotNullParameter(set, "sendBlockedChannels");
            return new SettingsV2(z2, z3, z4, z5, z6, z7, str, str2, set);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof SettingsV2)) {
                return false;
            }
            SettingsV2 settingsV2 = (SettingsV2) obj;
            return this.isEnabled == settingsV2.isEnabled && this.isEnabledInApp == settingsV2.isEnabledInApp && this.isWake == settingsV2.isWake && this.isDisableBlink == settingsV2.isDisableBlink && this.isDisableSound == settingsV2.isDisableSound && this.isDisableVibrate == settingsV2.isDisableVibrate && m.areEqual(this.token, settingsV2.token) && m.areEqual(this.locale, settingsV2.locale) && m.areEqual(this.sendBlockedChannels, settingsV2.sendBlockedChannels);
        }

        public final String getLocale() {
            return this.locale;
        }

        public final Set<Long> getSendBlockedChannels() {
            return this.sendBlockedChannels;
        }

        public final String getToken() {
            return this.token;
        }

        public int hashCode() {
            boolean z2 = this.isEnabled;
            int i = 1;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = i2 * 31;
            boolean z3 = this.isEnabledInApp;
            if (z3) {
                z3 = true;
            }
            int i5 = z3 ? 1 : 0;
            int i6 = z3 ? 1 : 0;
            int i7 = (i4 + i5) * 31;
            boolean z4 = this.isWake;
            if (z4) {
                z4 = true;
            }
            int i8 = z4 ? 1 : 0;
            int i9 = z4 ? 1 : 0;
            int i10 = (i7 + i8) * 31;
            boolean z5 = this.isDisableBlink;
            if (z5) {
                z5 = true;
            }
            int i11 = z5 ? 1 : 0;
            int i12 = z5 ? 1 : 0;
            int i13 = (i10 + i11) * 31;
            boolean z6 = this.isDisableSound;
            if (z6) {
                z6 = true;
            }
            int i14 = z6 ? 1 : 0;
            int i15 = z6 ? 1 : 0;
            int i16 = (i13 + i14) * 31;
            boolean z7 = this.isDisableVibrate;
            if (!z7) {
                i = z7 ? 1 : 0;
            }
            int i17 = (i16 + i) * 31;
            String str = this.token;
            int i18 = 0;
            int hashCode = (i17 + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.locale;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            Set<Long> set = this.sendBlockedChannels;
            if (set != null) {
                i18 = set.hashCode();
            }
            return hashCode2 + i18;
        }

        public final boolean isAuthed() {
            return this.isAuthed;
        }

        public final boolean isDisableBlink() {
            return this.isDisableBlink;
        }

        public final boolean isDisableSound() {
            return this.isDisableSound;
        }

        public final boolean isDisableVibrate() {
            return this.isDisableVibrate;
        }

        public final boolean isEnabled() {
            return this.isEnabled;
        }

        public final boolean isEnabledInApp() {
            return this.isEnabledInApp;
        }

        public final boolean isWake() {
            return this.isWake;
        }

        public String toString() {
            StringBuilder R = a.R("SettingsV2(isEnabled=");
            R.append(this.isEnabled);
            R.append(", isEnabledInApp=");
            R.append(this.isEnabledInApp);
            R.append(", isWake=");
            R.append(this.isWake);
            R.append(", isDisableBlink=");
            R.append(this.isDisableBlink);
            R.append(", isDisableSound=");
            R.append(this.isDisableSound);
            R.append(", isDisableVibrate=");
            R.append(this.isDisableVibrate);
            R.append(", token=");
            R.append(this.token);
            R.append(", locale=");
            R.append(this.locale);
            R.append(", sendBlockedChannels=");
            R.append(this.sendBlockedChannels);
            R.append(")");
            return R.toString();
        }

        public /* synthetic */ SettingsV2(boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, String str, String str2, Set set, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? true : z2, (i & 2) == 0 ? z3 : true, (i & 4) != 0 ? false : z4, (i & 8) != 0 ? false : z5, (i & 16) != 0 ? false : z6, (i & 32) != 0 ? false : z7, (i & 64) != 0 ? null : str, (i & 128) != 0 ? "" : str2, (i & 256) != 0 ? new HashSet(0) : set);
        }
    }
}
