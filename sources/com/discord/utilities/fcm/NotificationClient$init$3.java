package com.discord.utilities.fcm;

import b.i.a.f.n.c;
import com.discord.app.AppLog;
import com.google.android.gms.tasks.Task;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: NotificationClient.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042*\u0010\u0003\u001a&\u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00010\u0001 \u0002*\u0012\u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00010\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/google/android/gms/tasks/Task;", "", "kotlin.jvm.PlatformType", "task", "", "onComplete", "(Lcom/google/android/gms/tasks/Task;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class NotificationClient$init$3<TResult> implements c<String> {
    public static final NotificationClient$init$3 INSTANCE = new NotificationClient$init$3();

    @Override // b.i.a.f.n.c
    public final void onComplete(Task<String> task) {
        m.checkNotNullExpressionValue(task, "task");
        if (task.p()) {
            NotificationClient.INSTANCE.onNewToken(task.l());
        } else {
            AppLog.g.w("Fetching FCM registration token failed", task.k());
        }
    }
}
