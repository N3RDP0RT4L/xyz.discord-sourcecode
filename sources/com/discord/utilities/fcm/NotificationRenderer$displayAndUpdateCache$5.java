package com.discord.utilities.fcm;

import android.content.Context;
import androidx.core.app.NotificationCompat;
import com.discord.utilities.fcm.NotificationData;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: NotificationRenderer.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Ljava/lang/Long;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class NotificationRenderer$displayAndUpdateCache$5 extends o implements Function1<Long, Unit> {
    public final /* synthetic */ Context $context;
    public final /* synthetic */ NotificationData.DisplayPayload $displayPayload;
    public final /* synthetic */ NotificationData $notification;
    public final /* synthetic */ NotificationCompat.Builder $notificationBuilder;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public NotificationRenderer$displayAndUpdateCache$5(Context context, NotificationData.DisplayPayload displayPayload, NotificationCompat.Builder builder, NotificationData notificationData) {
        super(1);
        this.$context = context;
        this.$displayPayload = displayPayload;
        this.$notificationBuilder = builder;
        this.$notification = notificationData;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Long l) {
        invoke2(l);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Long l) {
        NotificationRenderer notificationRenderer = NotificationRenderer.INSTANCE;
        notificationRenderer.displayNotification(this.$context, this.$displayPayload.getId(), this.$notificationBuilder);
        NotificationRenderer.pushShortcut$default(notificationRenderer, this.$context, this.$notification, this.$notificationBuilder, null, 8, null);
    }
}
