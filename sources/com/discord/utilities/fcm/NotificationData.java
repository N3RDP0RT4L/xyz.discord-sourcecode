package com.discord.utilities.fcm;

import andhook.lib.HookHelper;
import andhook.lib.xposed.ClassUtils;
import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.text.Html;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.Person;
import androidx.core.app.RemoteInput;
import androidx.core.graphics.drawable.IconCompat;
import b.a.k.b;
import b.d.b.a.a;
import com.adjust.sdk.Constants;
import com.discord.api.application.Application;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guildmember.GuildMember;
import com.discord.api.guildscheduledevent.GuildScheduledEventEntityType;
import com.discord.api.message.Message;
import com.discord.api.message.activity.MessageActivity;
import com.discord.api.message.activity.MessageActivityType;
import com.discord.api.sticker.Sticker;
import com.discord.api.user.User;
import com.discord.app.AppActivity;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.nullserializable.NullSerializable;
import com.discord.utilities.SnowflakeUtils;
import com.discord.utilities.extensions.PendingIntentExtensionsKt;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.media.AppSound;
import com.discord.utilities.message.MessageUtils;
import com.discord.utilities.time.Clock;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.d0.f;
import d0.g0.s;
import d0.g0.t;
import d0.g0.w;
import d0.t.g0;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: NotificationData.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Ö\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\"\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010$\n\u0002\b\u0005\b\u0000\u0018\u0000 ¡\u00012\u00020\u0001:\u0004¡\u0001¢\u0001B÷\u0002\u0012\u0006\u0010F\u001a\u00020\n\u0012\n\u0010f\u001a\u00060\u0014j\u0002`\u0018\u0012\b\u0010^\u001a\u0004\u0018\u00010]\u0012\b\u0010W\u001a\u0004\u0018\u00010\n\u0012\u0007\u0010\u0091\u0001\u001a\u00020:\u0012\t\u0010\u009a\u0001\u001a\u0004\u0018\u00010\n\u0012\u0006\u0010h\u001a\u00020g\u0012\u0011\u0010\u0082\u0001\u001a\f\u0012\b\u0012\u00060\u0014j\u0002`\u00150\u001b\u0012\n\u0010J\u001a\u00060\u0014j\u0002`I\u0012\b\u0010D\u001a\u0004\u0018\u00010\n\u0012\t\u0010\u0087\u0001\u001a\u0004\u0018\u00010:\u0012\b\u0010j\u001a\u0004\u0018\u00010\n\u0012\b\u0010k\u001a\u0004\u0018\u00010\n\u0012\u0006\u0010l\u001a\u00020:\u0012\b\u0010L\u001a\u0004\u0018\u00010\n\u0012\f\u0010\u0093\u0001\u001a\u00070\u0014j\u0003`\u0092\u0001\u0012\b\u0010B\u001a\u0004\u0018\u00010\n\u0012\t\u0010\u0095\u0001\u001a\u0004\u0018\u00010\n\u0012\u0006\u0010X\u001a\u00020:\u0012\t\u0010\u0080\u0001\u001a\u0004\u0018\u00010\n\u0012\f\u0010\u008a\u0001\u001a\u00070\u0014j\u0003`\u0089\u0001\u0012\b\u0010m\u001a\u0004\u0018\u00010\n\u0012\t\u0010\u0081\u0001\u001a\u0004\u0018\u00010\n\u0012\b\u0010|\u001a\u0004\u0018\u00010{\u0012\b\u0010V\u001a\u0004\u0018\u00010\n\u0012\t\u0010\u008f\u0001\u001a\u0004\u0018\u00010\n\u0012\b\u0010E\u001a\u0004\u0018\u00010\n\u0012\t\u0010\u0086\u0001\u001a\u0004\u0018\u00010\n\u0012\b\u0010`\u001a\u0004\u0018\u00010\n\u0012\b\u0010[\u001a\u0004\u0018\u00010Z\u0012\u000e\u0010o\u001a\n\u0018\u00010\u0014j\u0004\u0018\u0001`n\u0012\b\u0010t\u001a\u0004\u0018\u00010s\u0012\t\u0010\u0090\u0001\u001a\u0004\u0018\u00010\n\u0012\b\u0010e\u001a\u0004\u0018\u00010\n¢\u0006\u0006\b\u009b\u0001\u0010\u009c\u0001B\u000b\b\u0016¢\u0006\u0006\b\u009b\u0001\u0010\u009d\u0001B!\b\u0016\u0012\u0014\u0010\u009f\u0001\u001a\u000f\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n0\u009e\u0001¢\u0006\u0006\b\u009b\u0001\u0010 \u0001J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J)\u0010\u000e\u001a\u00020\r2\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\n2\b\b\u0002\u0010\f\u001a\u00020\nH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u0015\u0010\u0012\u001a\u00020\u00002\u0006\u0010\u0011\u001a\u00020\u0010¢\u0006\u0004\b\u0012\u0010\u0013J\u0011\u0010\u0016\u001a\u00060\u0014j\u0002`\u0015¢\u0006\u0004\b\u0016\u0010\u0017J\u0011\u0010\u0019\u001a\u00060\u0014j\u0002`\u0018¢\u0006\u0004\b\u0019\u0010\u0017J\r\u0010\u001a\u001a\u00020\u0014¢\u0006\u0004\b\u001a\u0010\u0017J\u0017\u0010\u001c\u001a\f\u0012\b\u0012\u00060\u0014j\u0002`\u00150\u001b¢\u0006\u0004\b\u001c\u0010\u001dJ\r\u0010\u001f\u001a\u00020\u001e¢\u0006\u0004\b\u001f\u0010 J\u0017\u0010!\u001a\u0004\u0018\u00010\r2\u0006\u0010\t\u001a\u00020\b¢\u0006\u0004\b!\u0010\"J\u0015\u0010#\u001a\u00020\r2\u0006\u0010\t\u001a\u00020\b¢\u0006\u0004\b#\u0010\"J\u0015\u0010%\u001a\u00020$2\u0006\u0010\t\u001a\u00020\b¢\u0006\u0004\b%\u0010&J\u0017\u0010'\u001a\u0004\u0018\u00010\r2\u0006\u0010\t\u001a\u00020\b¢\u0006\u0004\b'\u0010\"J\u0017\u0010)\u001a\u0004\u0018\u00010(2\u0006\u0010\t\u001a\u00020\b¢\u0006\u0004\b)\u0010*J\u0017\u0010,\u001a\u00020+2\u0006\u0010\t\u001a\u00020\bH\u0007¢\u0006\u0004\b,\u0010-J\u0019\u0010.\u001a\u0004\u0018\u00010+2\u0006\u0010\t\u001a\u00020\bH\u0007¢\u0006\u0004\b.\u0010-J\u0017\u0010/\u001a\u00020+2\u0006\u0010\t\u001a\u00020\bH\u0007¢\u0006\u0004\b/\u0010-J+\u00104\u001a\u0004\u0018\u0001032\u0006\u00100\u001a\u00020\b2\u0010\u00102\u001a\f\u0012\b\u0012\u00060\u0014j\u0002`\u001501H\u0007¢\u0006\u0004\b4\u00105J\u0019\u00106\u001a\u0004\u0018\u0001032\u0006\u00100\u001a\u00020\bH\u0007¢\u0006\u0004\b6\u00107J)\u0010<\u001a\u0004\u0018\u0001032\u0006\u00100\u001a\u00020\b2\u0006\u00109\u001a\u0002082\u0006\u0010;\u001a\u00020:H\u0007¢\u0006\u0004\b<\u0010=J!\u0010?\u001a\u0004\u0018\u0001032\u0006\u00100\u001a\u00020\b2\u0006\u0010>\u001a\u00020\u001eH\u0007¢\u0006\u0004\b?\u0010@J\u0017\u0010A\u001a\u00020+2\u0006\u0010\t\u001a\u00020\bH\u0007¢\u0006\u0004\bA\u0010-R\u0018\u0010B\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bB\u0010CR\u0018\u0010D\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bD\u0010CR\u0018\u0010E\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bE\u0010CR\u0019\u0010F\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\bF\u0010C\u001a\u0004\bG\u0010HR\u001a\u0010J\u001a\u00060\u0014j\u0002`I8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bJ\u0010KR\u0018\u0010L\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bL\u0010CR\u0015\u0010N\u001a\u0004\u0018\u00010\n8F@\u0006¢\u0006\u0006\u001a\u0004\bM\u0010HR\u0013\u0010Q\u001a\u00020:8F@\u0006¢\u0006\u0006\u001a\u0004\bO\u0010PR\u0019\u0010R\u001a\u00020\u001e8\u0006@\u0006¢\u0006\f\n\u0004\bR\u0010S\u001a\u0004\bR\u0010 R\u0013\u0010U\u001a\u00020:8F@\u0006¢\u0006\u0006\u001a\u0004\bT\u0010PR\u0018\u0010V\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bV\u0010CR\u0018\u0010W\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bW\u0010CR\u0016\u0010X\u001a\u00020:8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bX\u0010YR\u0018\u0010[\u001a\u0004\u0018\u00010Z8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b[\u0010\\R\u0018\u0010^\u001a\u0004\u0018\u00010]8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b^\u0010_R\u0018\u0010`\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b`\u0010CR\u0013\u0010b\u001a\u00020\n8F@\u0006¢\u0006\u0006\u001a\u0004\ba\u0010HR\u0015\u0010d\u001a\u0004\u0018\u00010\n8F@\u0006¢\u0006\u0006\u001a\u0004\bc\u0010HR\u0018\u0010e\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\be\u0010CR\u001a\u0010f\u001a\u00060\u0014j\u0002`\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bf\u0010KR\u0016\u0010h\u001a\u00020g8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bh\u0010iR\u0018\u0010j\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bj\u0010CR\u0018\u0010k\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bk\u0010CR\u0016\u0010l\u001a\u00020:8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bl\u0010YR\u0018\u0010m\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bm\u0010CR\u001e\u0010o\u001a\n\u0018\u00010\u0014j\u0004\u0018\u0001`n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bo\u0010pR\u0013\u0010r\u001a\u00020\u001e8F@\u0006¢\u0006\u0006\u001a\u0004\bq\u0010 R\u0018\u0010t\u001a\u0004\u0018\u00010s8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bt\u0010uR\u0013\u0010w\u001a\u00020\n8F@\u0006¢\u0006\u0006\u001a\u0004\bv\u0010HR\u0013\u0010y\u001a\u00020\n8F@\u0006¢\u0006\u0006\u001a\u0004\bx\u0010HR\u0013\u0010z\u001a\u00020\u001e8F@\u0006¢\u0006\u0006\u001a\u0004\bz\u0010 R\u001b\u0010|\u001a\u0004\u0018\u00010{8\u0006@\u0006¢\u0006\f\n\u0004\b|\u0010}\u001a\u0004\b~\u0010\u007fR\u001a\u0010\u0080\u0001\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b\u0080\u0001\u0010CR\u001a\u0010\u0081\u0001\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b\u0081\u0001\u0010CR#\u0010\u0082\u0001\u001a\f\u0012\b\u0012\u00060\u0014j\u0002`\u00150\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0082\u0001\u0010\u0083\u0001R\u0015\u0010\u0085\u0001\u001a\u00020\n8F@\u0006¢\u0006\u0007\u001a\u0005\b\u0084\u0001\u0010HR\u001a\u0010\u0086\u0001\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b\u0086\u0001\u0010CR\u001b\u0010\u0087\u0001\u001a\u0004\u0018\u00010:8\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0087\u0001\u0010\u0088\u0001R\u001d\u0010\u008a\u0001\u001a\u00070\u0014j\u0003`\u0089\u00018\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b\u008a\u0001\u0010KR\u001a\u0010\u008c\u0001\u001a\u0004\u0018\u00010\n8B@\u0002X\u0082\u0004¢\u0006\u0007\u001a\u0005\b\u008b\u0001\u0010HR\u001a\u0010\u008e\u0001\u001a\u0004\u0018\u00010\n8B@\u0002X\u0082\u0004¢\u0006\u0007\u001a\u0005\b\u008d\u0001\u0010HR\u001a\u0010\u008f\u0001\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b\u008f\u0001\u0010CR\u001a\u0010\u0090\u0001\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b\u0090\u0001\u0010CR\u0018\u0010\u0091\u0001\u001a\u00020:8\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b\u0091\u0001\u0010YR!\u0010\u0093\u0001\u001a\u00070\u0014j\u0003`\u0092\u00018\u0006@\u0006¢\u0006\u000e\n\u0005\b\u0093\u0001\u0010K\u001a\u0005\b\u0094\u0001\u0010\u0017R\u001a\u0010\u0095\u0001\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b\u0095\u0001\u0010CR\u0015\u0010\u0097\u0001\u001a\u00020\u001e8F@\u0006¢\u0006\u0007\u001a\u0005\b\u0096\u0001\u0010 R\u0015\u0010\u0099\u0001\u001a\u00020\n8F@\u0006¢\u0006\u0007\u001a\u0005\b\u0098\u0001\u0010HR\u001a\u0010\u009a\u0001\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b\u009a\u0001\u0010C¨\u0006£\u0001"}, d2 = {"Lcom/discord/utilities/fcm/NotificationData;", "", "Landroid/content/Intent;", "getContentIntentInternal", "()Landroid/content/Intent;", "intent", "addTrackingData", "(Landroid/content/Intent;)Landroid/content/Intent;", "Landroid/content/Context;", "context", "", "resourceName", "fallback", "", "resNameToString", "(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;", "Lcom/discord/api/message/Message;", "message", "copyForDirectReply", "(Lcom/discord/api/message/Message;)Lcom/discord/utilities/fcm/NotificationData;", "", "Lcom/discord/primitives/ChannelId;", "getChannelId", "()J", "Lcom/discord/primitives/MessageId;", "getMessageId", "getMessageIdTimestamp", "", "getAckChannelIds", "()Ljava/util/List;", "", "canDisplayInApp", "()Z", "getConversationTitle", "(Landroid/content/Context;)Ljava/lang/CharSequence;", "getTitle", "Landroidx/core/app/Person;", "getSender", "(Landroid/content/Context;)Landroidx/core/app/Person;", "getContent", "Landroid/net/Uri;", "getNotificationSound", "(Landroid/content/Context;)Landroid/net/Uri;", "Landroid/app/PendingIntent;", "getContentIntent", "(Landroid/content/Context;)Landroid/app/PendingIntent;", "getFullScreenIntent", "getContentIntentInApp", "ctx", "", "deniedChannels", "Landroidx/core/app/NotificationCompat$Action;", "getDirectReplyAction", "(Landroid/content/Context;Ljava/util/Set;)Landroidx/core/app/NotificationCompat$Action;", "getMarkAsReadAction", "(Landroid/content/Context;)Landroidx/core/app/NotificationCompat$Action;", "Lcom/discord/utilities/time/Clock;", "clock", "", "numMessages", "getTimedMute", "(Landroid/content/Context;Lcom/discord/utilities/time/Clock;I)Landroidx/core/app/NotificationCompat$Action;", "isAcceptAction", "getCallAction", "(Landroid/content/Context;Z)Landroidx/core/app/NotificationCompat$Action;", "getDeleteIntent", "guildName", "Ljava/lang/String;", "userUsername", "titleResName", "type", "getType", "()Ljava/lang/String;", "Lcom/discord/primitives/UserId;", "userId", "J", Constants.DEEPLINK, "getIconUrlForUser", "iconUrlForUser", "getSmallIcon", "()I", "smallIcon", "isGroupConversation", "Z", "getNotificationPriority", "notificationPriority", "title", "messageApplicationName", "activityType", "I", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;", "guildScheduledEventEntityType", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;", "Lcom/discord/api/message/activity/MessageActivityType;", "messageActivityType", "Lcom/discord/api/message/activity/MessageActivityType;", "stageInstanceTopic", "getIconUrl", "iconUrl", "getIconUrlForAvatar", "iconUrlForAvatar", "notificationChannel", "messageId", "Lcom/discord/api/channel/Channel;", "channel", "Lcom/discord/api/channel/Channel;", "userAvatar", "guildMemberAvatar", "relationshipType", "applicationName", "Lcom/discord/primitives/GuildScheduledEventId;", "guildScheduledEventId", "Ljava/lang/Long;", "getShouldUseBigText", "shouldUseBigText", "Lcom/discord/utilities/fcm/NotificationType;", "notificationType", "Lcom/discord/utilities/fcm/NotificationType;", "getKey", "key", "getGroupKey", "groupKey", "isValid", "Lcom/discord/api/sticker/Sticker;", "sticker", "Lcom/discord/api/sticker/Sticker;", "getSticker", "()Lcom/discord/api/sticker/Sticker;", "activityName", "applicationIcon", "ackChannelIds", "Ljava/util/List;", "getNotificationCategory", "notificationCategory", "subtitleResName", "userDiscriminator", "Ljava/lang/Integer;", "Lcom/discord/primitives/ApplicationId;", "applicationId", "getIconUrlForGuildMember", "iconUrlForGuildMember", "getIconUrlForChannel", "iconUrlForChannel", "subtitle", "trackingType", "messageType", "Lcom/discord/primitives/GuildId;", "guildId", "getGuildId", "guildIcon", "getShouldGroup", "shouldGroup", "getNotificationChannelId", "notificationChannelId", "messageContent", HookHelper.constructorName, "(Ljava/lang/String;JLcom/discord/api/message/activity/MessageActivityType;Ljava/lang/String;ILjava/lang/String;Lcom/discord/api/channel/Channel;Ljava/util/List;JLjava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JLjava/lang/String;Ljava/lang/String;ILjava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/discord/api/sticker/Sticker;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;Ljava/lang/Long;Lcom/discord/utilities/fcm/NotificationType;Ljava/lang/String;Ljava/lang/String;)V", "()V", "", "data", "(Ljava/util/Map;)V", "Companion", "DisplayPayload", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class NotificationData {
    private static final int ACTIVITY_TYPE_PLAYING = 0;
    private static final int ACTIVITY_TYPE_STREAMING = 1;
    private static final String ANALYTICS_ACTIVITY_NAME = "activity_name";
    private static final String ANALYTICS_ACTIVITY_TYPE = "activity_type";
    private static final String ANALYTICS_CHANNEL_ID = "channel_id";
    private static final String ANALYTICS_CHANNEL_TYPE = "channel_type";
    private static final String ANALYTICS_GUILD_ID = "guild_id";
    private static final String ANALYTICS_MESSAGE_ID = "message_id";
    private static final String ANALYTICS_MESSAGE_TYPE = "message_type";
    private static final String ANALYTICS_NOTIF_IN_APP = "notif_in_app";
    private static final String ANALYTICS_NOTIF_TYPE = "notif_type";
    private static final String ANALYTICS_NOTIF_USER_ID = "notif_user_id";
    private static final String ANALYTICS_REL_TYPE = "rel_type";
    public static final Companion Companion = new Companion(null);
    private static final String GROUP_KEY_PREFIX = "GROUP_";
    private static final int MSG_MUTE_MIN_COUNT = 2;
    public static final String REPLYING_TO_UNTRANSLATED = "replying to";
    public static final String TYPE_ACTIVITY_START = "ACTIVITY_START";
    public static final String TYPE_APPLICATION_LIBRARY_INSTALL_COMPLETE = "APPLICATION_LIBRARY_INSTALL_COMPLETE";
    public static final String TYPE_CALL_RING = "CALL_RING";
    public static final String TYPE_GENERIC_PUSH_NOTIFICATION_SENT = "GENERIC_PUSH_NOTIFICATION_SENT";
    public static final String TYPE_GUILD_SCHEDULED_EVENT_UPDATE = "GUILD_SCHEDULED_EVENT_UPDATE";
    public static final String TYPE_MESSAGE_CREATE = "MESSAGE_CREATE";
    public static final String TYPE_RELATIONSHIP_ADD = "RELATIONSHIP_ADD";
    public static final String TYPE_STAGE_INSTANCE_CREATE = "STAGE_INSTANCE_CREATE";
    public static final int UNSET_INT = -1;
    public static final long UNSET_LONG = -1;
    private final List<Long> ackChannelIds;
    private final String activityName;
    private final int activityType;
    private final String applicationIcon;
    private final long applicationId;
    private final String applicationName;
    private final Channel channel;
    private final String deeplink;
    private final String guildIcon;
    private final long guildId;
    private final String guildMemberAvatar;
    private final String guildName;
    private final GuildScheduledEventEntityType guildScheduledEventEntityType;
    private final Long guildScheduledEventId;
    private final boolean isGroupConversation;
    private final MessageActivityType messageActivityType;
    private final String messageApplicationName;
    private final String messageContent;
    private final long messageId;
    private final int messageType;
    private final String notificationChannel;
    private final NotificationType notificationType;
    private final int relationshipType;
    private final String stageInstanceTopic;
    private final Sticker sticker;
    private final String subtitle;
    private final String subtitleResName;
    private final String title;
    private final String titleResName;
    private final String trackingType;
    private final String type;
    private final String userAvatar;
    private final Integer userDiscriminator;
    private final long userId;
    private final String userUsername;

    /* compiled from: NotificationData.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b \b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b3\u00104J1\u0010\b\u001a\u00020\u0007*\u0004\u0018\u00010\u00022\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00010\u00032\u0006\u0010\u0006\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\b\u0010\tJ1\u0010\n\u001a\u00020\u0007*\u0004\u0018\u00010\u00022\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00010\u00032\u0006\u0010\u0006\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\n\u0010\tJ1\u0010\u000b\u001a\u00020\u0007*\u0004\u0018\u00010\u00022\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00010\u00032\u0006\u0010\u0006\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000b\u0010\tJ\u001f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r2\b\u0010\f\u001a\u0004\u0018\u00010\u0004H\u0002¢\u0006\u0004\b\u000f\u0010\u0010J#\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00010\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0016\u001a\u00020\u00158\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0018\u001a\u00020\u00158\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0018\u0010\u0017R\u0016\u0010\u0019\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001b\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001b\u0010\u001aR\u0016\u0010\u001c\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001c\u0010\u001aR\u0016\u0010\u001d\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001d\u0010\u001aR\u0016\u0010\u001e\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001e\u0010\u001aR\u0016\u0010\u001f\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001f\u0010\u001aR\u0016\u0010 \u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b \u0010\u001aR\u0016\u0010!\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b!\u0010\u001aR\u0016\u0010\"\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\"\u0010\u001aR\u0016\u0010#\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b#\u0010\u001aR\u0016\u0010$\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b$\u0010\u001aR\u0016\u0010%\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b%\u0010\u001aR\u0016\u0010&\u001a\u00020\u00158\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b&\u0010\u0017R\u0016\u0010'\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b'\u0010\u001aR\u0016\u0010(\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b(\u0010\u001aR\u0016\u0010)\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b)\u0010\u001aR\u0016\u0010*\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b*\u0010\u001aR\u0016\u0010+\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b+\u0010\u001aR\u0016\u0010,\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b,\u0010\u001aR\u0016\u0010-\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b-\u0010\u001aR\u0016\u0010.\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b.\u0010\u001aR\u0016\u0010/\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b/\u0010\u001aR\u0016\u00100\u001a\u00020\u00158\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b0\u0010\u0017R\u0016\u00101\u001a\u00020\u000e8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b1\u00102¨\u00065"}, d2 = {"Lcom/discord/utilities/fcm/NotificationData$Companion;", "", "Landroid/content/Intent;", "", "", "target", "key", "", "copyIntIntoMap", "(Landroid/content/Intent;Ljava/util/Map;Ljava/lang/String;)V", "copyLongIntoMap", "copyStringIntoMap", "longStringsConcatenated", "", "", "parseAckChannelIds", "(Ljava/lang/String;)Ljava/util/List;", "intent", "", "buildTrackingData", "(Landroid/content/Intent;)Ljava/util/Map;", "", "ACTIVITY_TYPE_PLAYING", "I", "ACTIVITY_TYPE_STREAMING", "ANALYTICS_ACTIVITY_NAME", "Ljava/lang/String;", "ANALYTICS_ACTIVITY_TYPE", "ANALYTICS_CHANNEL_ID", "ANALYTICS_CHANNEL_TYPE", "ANALYTICS_GUILD_ID", "ANALYTICS_MESSAGE_ID", "ANALYTICS_MESSAGE_TYPE", "ANALYTICS_NOTIF_IN_APP", "ANALYTICS_NOTIF_TYPE", "ANALYTICS_NOTIF_USER_ID", "ANALYTICS_REL_TYPE", "GROUP_KEY_PREFIX", "MSG_MUTE_MIN_COUNT", "REPLYING_TO_UNTRANSLATED", "TYPE_ACTIVITY_START", "TYPE_APPLICATION_LIBRARY_INSTALL_COMPLETE", "TYPE_CALL_RING", "TYPE_GENERIC_PUSH_NOTIFICATION_SENT", "TYPE_GUILD_SCHEDULED_EVENT_UPDATE", "TYPE_MESSAGE_CREATE", "TYPE_RELATIONSHIP_ADD", "TYPE_STAGE_INSTANCE_CREATE", "UNSET_INT", "UNSET_LONG", "J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        private final void copyIntIntoMap(Intent intent, Map<String, Object> map, String str) {
            int i = -1;
            if (intent != null) {
                i = intent.getIntExtra(str, -1);
            }
            if (i >= 0) {
                map.put(str, Integer.valueOf(i));
            }
        }

        private final void copyLongIntoMap(Intent intent, Map<String, Object> map, String str) {
            long j = -1;
            if (intent != null) {
                j = intent.getLongExtra(str, -1L);
            }
            if (j >= 0) {
                map.put(str, Long.valueOf(j));
            }
        }

        private final void copyStringIntoMap(Intent intent, Map<String, Object> map, String str) {
            String stringExtra;
            if (intent != null && (stringExtra = intent.getStringExtra(str)) != null) {
                m.checkNotNullExpressionValue(stringExtra, "this?.getStringExtra(key) ?: return");
                if (!(stringExtra.length() == 0)) {
                    map.put(str, stringExtra);
                }
            }
        }

        public final List<Long> parseAckChannelIds(String str) {
            if (str == null) {
                str = "";
            }
            List split$default = w.split$default((CharSequence) str, new String[]{","}, false, 0, 6, (Object) null);
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(split$default, 10));
            Iterator it = split$default.iterator();
            while (true) {
                long j = -1;
                if (!it.hasNext()) {
                    break;
                }
                Long longOrNull = s.toLongOrNull((String) it.next());
                if (longOrNull != null) {
                    j = longOrNull.longValue();
                }
                arrayList.add(Long.valueOf(j));
            }
            ArrayList arrayList2 = new ArrayList();
            for (Object obj : arrayList) {
                if (((Number) obj).longValue() != -1) {
                    arrayList2.add(obj);
                }
            }
            return arrayList2;
        }

        public final Map<String, Object> buildTrackingData(Intent intent) {
            String stringExtra;
            HashMap hashMap = new HashMap();
            copyStringIntoMap(intent, hashMap, NotificationData.ANALYTICS_NOTIF_TYPE);
            copyLongIntoMap(intent, hashMap, NotificationData.ANALYTICS_NOTIF_USER_ID);
            copyLongIntoMap(intent, hashMap, NotificationData.ANALYTICS_MESSAGE_ID);
            copyLongIntoMap(intent, hashMap, "guild_id");
            copyLongIntoMap(intent, hashMap, "channel_id");
            copyIntIntoMap(intent, hashMap, NotificationData.ANALYTICS_MESSAGE_TYPE);
            copyIntIntoMap(intent, hashMap, NotificationData.ANALYTICS_CHANNEL_TYPE);
            copyIntIntoMap(intent, hashMap, NotificationData.ANALYTICS_REL_TYPE);
            copyIntIntoMap(intent, hashMap, NotificationData.ANALYTICS_ACTIVITY_TYPE);
            copyStringIntoMap(intent, hashMap, NotificationData.ANALYTICS_ACTIVITY_NAME);
            if (!(intent == null || (stringExtra = intent.getStringExtra("com.discord.intent.ORIGIN_SOURCE")) == null)) {
                hashMap.put(NotificationData.ANALYTICS_NOTIF_IN_APP, Boolean.valueOf(t.equals(stringExtra, "com.discord.intent.ORIGIN_NOTIF_INAPP", true)));
            }
            return hashMap;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;
        public static final /* synthetic */ int[] $EnumSwitchMapping$2;
        public static final /* synthetic */ int[] $EnumSwitchMapping$3;
        public static final /* synthetic */ int[] $EnumSwitchMapping$4;
        public static final /* synthetic */ int[] $EnumSwitchMapping$5;
        public static final /* synthetic */ int[] $EnumSwitchMapping$6;

        static {
            GuildScheduledEventEntityType.values();
            int[] iArr = new int[5];
            $EnumSwitchMapping$0 = iArr;
            GuildScheduledEventEntityType guildScheduledEventEntityType = GuildScheduledEventEntityType.STAGE_INSTANCE;
            iArr[guildScheduledEventEntityType.ordinal()] = 1;
            GuildScheduledEventEntityType guildScheduledEventEntityType2 = GuildScheduledEventEntityType.VOICE;
            iArr[guildScheduledEventEntityType2.ordinal()] = 2;
            GuildScheduledEventEntityType guildScheduledEventEntityType3 = GuildScheduledEventEntityType.EXTERNAL;
            iArr[guildScheduledEventEntityType3.ordinal()] = 3;
            GuildScheduledEventEntityType.values();
            int[] iArr2 = new int[5];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[guildScheduledEventEntityType.ordinal()] = 1;
            iArr2[guildScheduledEventEntityType2.ordinal()] = 2;
            iArr2[guildScheduledEventEntityType3.ordinal()] = 3;
            GuildScheduledEventEntityType.values();
            int[] iArr3 = new int[5];
            $EnumSwitchMapping$2 = iArr3;
            iArr3[guildScheduledEventEntityType.ordinal()] = 1;
            iArr3[guildScheduledEventEntityType2.ordinal()] = 2;
            iArr3[guildScheduledEventEntityType3.ordinal()] = 3;
            GuildScheduledEventEntityType.values();
            int[] iArr4 = new int[5];
            $EnumSwitchMapping$3 = iArr4;
            iArr4[guildScheduledEventEntityType.ordinal()] = 1;
            iArr4[guildScheduledEventEntityType2.ordinal()] = 2;
            iArr4[guildScheduledEventEntityType3.ordinal()] = 3;
            NotificationType.values();
            int[] iArr5 = new int[1];
            $EnumSwitchMapping$4 = iArr5;
            iArr5[NotificationType.Reminder.ordinal()] = 1;
            GuildScheduledEventEntityType.values();
            int[] iArr6 = new int[5];
            $EnumSwitchMapping$5 = iArr6;
            iArr6[guildScheduledEventEntityType.ordinal()] = 1;
            iArr6[guildScheduledEventEntityType2.ordinal()] = 2;
            iArr6[guildScheduledEventEntityType3.ordinal()] = 3;
            GuildScheduledEventEntityType.values();
            int[] iArr7 = new int[5];
            $EnumSwitchMapping$6 = iArr7;
            iArr7[guildScheduledEventEntityType.ordinal()] = 1;
            iArr7[guildScheduledEventEntityType2.ordinal()] = 2;
            iArr7[guildScheduledEventEntityType3.ordinal()] = 3;
        }
    }

    public NotificationData(String str, long j, MessageActivityType messageActivityType, String str2, int i, String str3, Channel channel, List<Long> list, long j2, String str4, Integer num, String str5, String str6, int i2, String str7, long j3, String str8, String str9, int i3, String str10, long j4, String str11, String str12, Sticker sticker, String str13, String str14, String str15, String str16, String str17, GuildScheduledEventEntityType guildScheduledEventEntityType, Long l, NotificationType notificationType, String str18, String str19) {
        m.checkNotNullParameter(str, "type");
        m.checkNotNullParameter(channel, "channel");
        m.checkNotNullParameter(list, "ackChannelIds");
        this.type = str;
        this.messageId = j;
        this.messageActivityType = messageActivityType;
        this.messageApplicationName = str2;
        this.messageType = i;
        this.messageContent = str3;
        this.channel = channel;
        this.ackChannelIds = list;
        this.userId = j2;
        this.userUsername = str4;
        this.userDiscriminator = num;
        this.userAvatar = str5;
        this.guildMemberAvatar = str6;
        this.relationshipType = i2;
        this.deeplink = str7;
        this.guildName = str8;
        this.guildIcon = str9;
        this.activityType = i3;
        this.activityName = str10;
        this.applicationId = j4;
        this.applicationName = str11;
        this.applicationIcon = str12;
        this.sticker = sticker;
        this.title = str13;
        this.subtitle = str14;
        this.titleResName = str15;
        this.subtitleResName = str16;
        this.stageInstanceTopic = str17;
        this.guildScheduledEventEntityType = guildScheduledEventEntityType;
        this.guildScheduledEventId = l;
        this.notificationType = notificationType;
        this.trackingType = str18;
        this.notificationChannel = str19;
        Long valueOf = Long.valueOf(j3);
        boolean z2 = false;
        long j5 = -1;
        valueOf = (valueOf.longValue() > (-1L) ? 1 : (valueOf.longValue() == (-1L) ? 0 : -1)) == 0 ? null : valueOf;
        if (valueOf != null) {
            j5 = valueOf.longValue();
        } else {
            int A = channel.A();
            if (A == 1 || A == 3) {
                j5 = 0;
            }
        }
        this.guildId = j5;
        if (m.areEqual(str, TYPE_MESSAGE_CREATE) && (channel.A() == 0 || channel.A() == 3 || channel.A() == 10 || channel.A() == 11 || channel.A() == 12)) {
            z2 = true;
        }
        this.isGroupConversation = z2;
    }

    private final Intent addTrackingData(Intent intent) {
        String str = this.trackingType;
        if (str == null) {
            str = this.type;
        }
        Intent putExtra = intent.putExtra(ANALYTICS_NOTIF_TYPE, str).putExtra(ANALYTICS_NOTIF_USER_ID, this.userId).putExtra(ANALYTICS_MESSAGE_ID, this.messageId).putExtra(ANALYTICS_MESSAGE_TYPE, this.messageType).putExtra("guild_id", this.guildId).putExtra("channel_id", this.channel.h()).putExtra(ANALYTICS_CHANNEL_TYPE, this.channel.A()).putExtra(ANALYTICS_REL_TYPE, this.relationshipType).putExtra(ANALYTICS_ACTIVITY_TYPE, this.activityType).putExtra(ANALYTICS_ACTIVITY_NAME, this.activityName);
        m.checkNotNullExpressionValue(putExtra, "intent\n        .putExtra…IVITY_NAME, activityName)");
        return putExtra;
    }

    /* JADX WARN: Code restructure failed: missing block: B:13:0x0045, code lost:
        if (r0.equals(com.discord.utilities.fcm.NotificationData.TYPE_RELATIONSHIP_ADD) != false) goto L14;
     */
    /* JADX WARN: Code restructure failed: missing block: B:15:0x004d, code lost:
        if (r8.channel.A() == 1) goto L17;
     */
    /* JADX WARN: Code restructure failed: missing block: B:41:?, code lost:
        return com.discord.utilities.intent.IntentUtils.RouteBuilders.selectChannel$default(r8.channel.h(), r8.guildId, null, 4, null);
     */
    /* JADX WARN: Code restructure failed: missing block: B:42:?, code lost:
        return com.discord.utilities.intent.IntentUtils.RouteBuilders.selectChannel$default(r8.channel.h(), 0, null, 4, null);
     */
    /* JADX WARN: Code restructure failed: missing block: B:5:0x0014, code lost:
        if (r0.equals(com.discord.utilities.fcm.NotificationData.TYPE_MESSAGE_CREATE) != false) goto L14;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final android.content.Intent getContentIntentInternal() {
        /*
            r8 = this;
            java.lang.String r0 = r8.type
            int r1 = r0.hashCode()
            r2 = 1
            java.lang.String r3 = "android.intent.action.VIEW"
            switch(r1) {
                case -1502317553: goto Lab;
                case -1489275252: goto L70;
                case -1327124998: goto L3f;
                case -1263316859: goto L29;
                case 974015250: goto L17;
                case 998188116: goto Le;
                default: goto Lc;
            }
        Lc:
            goto Lc4
        Le:
            java.lang.String r1 = "MESSAGE_CREATE"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto Lc4
            goto L47
        L17:
            java.lang.String r1 = "ACTIVITY_START"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto Lc4
            com.discord.utilities.intent.IntentUtils$RouteBuilders r0 = com.discord.utilities.intent.IntentUtils.RouteBuilders.INSTANCE
            long r1 = r8.userId
            android.content.Intent r0 = r0.selectDirectMessage(r1)
            goto Ld3
        L29:
            java.lang.String r1 = "STAGE_INSTANCE_CREATE"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto Lc4
            com.discord.utilities.intent.IntentUtils$RouteBuilders r0 = com.discord.utilities.intent.IntentUtils.RouteBuilders.INSTANCE
            com.discord.api.channel.Channel r1 = r8.channel
            long r1 = r1.h()
            android.content.Intent r0 = r0.connectVoice(r1)
            goto Ld3
        L3f:
            java.lang.String r1 = "RELATIONSHIP_ADD"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto Lc4
        L47:
            com.discord.api.channel.Channel r0 = r8.channel
            int r0 = r0.A()
            if (r0 == r2) goto L60
            com.discord.api.channel.Channel r0 = r8.channel
            long r1 = r0.h()
            long r3 = r8.guildId
            r5 = 0
            r6 = 4
            r7 = 0
            android.content.Intent r0 = com.discord.utilities.intent.IntentUtils.RouteBuilders.selectChannel$default(r1, r3, r5, r6, r7)
            goto Ld3
        L60:
            com.discord.api.channel.Channel r0 = r8.channel
            long r1 = r0.h()
            r3 = 0
            r5 = 0
            r6 = 4
            r7 = 0
            android.content.Intent r0 = com.discord.utilities.intent.IntentUtils.RouteBuilders.selectChannel$default(r1, r3, r5, r6, r7)
            goto Ld3
        L70:
            java.lang.String r1 = "GUILD_SCHEDULED_EVENT_UPDATE"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto Lc4
            com.discord.api.guildscheduledevent.GuildScheduledEventEntityType r0 = r8.guildScheduledEventEntityType
            if (r0 != 0) goto L7d
            goto L89
        L7d:
            int r0 = r0.ordinal()
            if (r0 == r2) goto L9e
            r1 = 2
            if (r0 == r1) goto L9e
            r1 = 3
            if (r0 == r1) goto L95
        L89:
            android.content.Intent r0 = new android.content.Intent
            com.discord.utilities.intent.IntentUtils$RouteBuilders$Uris r1 = com.discord.utilities.intent.IntentUtils.RouteBuilders.Uris.INSTANCE
            android.net.Uri r1 = r1.getApp()
            r0.<init>(r3, r1)
            goto Ld3
        L95:
            long r0 = r8.guildId
            java.lang.Long r2 = r8.guildScheduledEventId
            android.content.Intent r0 = com.discord.utilities.intent.IntentUtils.RouteBuilders.selectExternalEvent(r0, r2)
            goto Ld3
        L9e:
            com.discord.utilities.intent.IntentUtils$RouteBuilders r0 = com.discord.utilities.intent.IntentUtils.RouteBuilders.INSTANCE
            com.discord.api.channel.Channel r1 = r8.channel
            long r1 = r1.h()
            android.content.Intent r0 = r0.connectVoice(r1)
            goto Ld3
        Lab:
            java.lang.String r1 = "GENERIC_PUSH_NOTIFICATION_SENT"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto Lc4
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r1 = r8.deeplink
            if (r1 == 0) goto Lba
            goto Lbc
        Lba:
            java.lang.String r1 = ""
        Lbc:
            android.net.Uri r1 = android.net.Uri.parse(r1)
            r0.<init>(r3, r1)
            goto Ld3
        Lc4:
            com.discord.api.channel.Channel r0 = r8.channel
            long r1 = r0.h()
            long r3 = r8.guildId
            r5 = 0
            r6 = 4
            r7 = 0
            android.content.Intent r0 = com.discord.utilities.intent.IntentUtils.RouteBuilders.selectChannel$default(r1, r3, r5, r6, r7)
        Ld3:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.fcm.NotificationData.getContentIntentInternal():android.content.Intent");
    }

    private final String getIconUrlForChannel() {
        int A = this.channel.A();
        if (A != 0) {
            if (A == 1) {
                return getIconUrlForUser();
            }
            if (A != 5) {
                switch (A) {
                    case 10:
                    case 11:
                    case 12:
                        break;
                    default:
                        return IconUtils.getForChannel$default(this.channel.h(), this.channel.g(), this.channel.A(), true, null, 16, null);
                }
            }
        }
        return IconUtils.getForGuild$default(Long.valueOf(this.guildId), this.guildIcon, "", false, null, 24, null);
    }

    private final String getIconUrlForGuildMember() {
        String forGuildMember;
        forGuildMember = IconUtils.INSTANCE.getForGuildMember(this.guildMemberAvatar, this.guildId, this.userId, (r17 & 8) != 0 ? null : null, (r17 & 16) != 0 ? false : false);
        return forGuildMember;
    }

    private final CharSequence resNameToString(Context context, String str, String str2) {
        CharSequence b2;
        Resources resources = context.getResources();
        int identifier = resources != null ? resources.getIdentifier(str, "string", context.getPackageName()) : 0;
        if (identifier == 0) {
            return str2;
        }
        b2 = b.b(context, identifier, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        return b2;
    }

    public static /* synthetic */ CharSequence resNameToString$default(NotificationData notificationData, Context context, String str, String str2, int i, Object obj) {
        if ((i & 4) != 0) {
            str2 = "";
        }
        return notificationData.resNameToString(context, str, str2);
    }

    public final boolean canDisplayInApp() {
        int ordinal;
        if (m.areEqual(this.type, TYPE_STAGE_INSTANCE_CREATE)) {
            return true;
        }
        if (!m.areEqual(this.type, TYPE_GUILD_SCHEDULED_EVENT_UPDATE)) {
            return this.channel.h() == -1;
        }
        GuildScheduledEventEntityType guildScheduledEventEntityType = this.guildScheduledEventEntityType;
        return guildScheduledEventEntityType != null && ((ordinal = guildScheduledEventEntityType.ordinal()) == 1 || ordinal == 2 || ordinal == 3);
    }

    public final NotificationData copyForDirectReply(Message message) {
        NullSerializable<String> a;
        String a2;
        String f;
        String r;
        m.checkNotNullParameter(message, "message");
        long o = message.o();
        MessageActivity a3 = message.a();
        MessageActivityType b2 = a3 != null ? a3.b() : null;
        Application b3 = message.b();
        String h = b3 != null ? b3.h() : null;
        Integer E = message.E();
        m.checkNotNull(E);
        int intValue = E.intValue();
        String i = message.i();
        Channel a4 = Channel.a(this.channel, null, 0, 0L, null, 0L, message.g(), 0L, null, null, 0, null, 0, 0, null, 0L, 0L, null, false, 0L, null, 0, null, null, null, null, null, null, null, null, 536870879);
        List emptyList = n.emptyList();
        User e = message.e();
        long i2 = e != null ? e.i() : 0L;
        User e2 = message.e();
        String str = (e2 == null || (r = e2.r()) == null) ? "" : r;
        User e3 = message.e();
        Integer intOrNull = (e3 == null || (f = e3.f()) == null) ? null : s.toIntOrNull(f);
        User e4 = message.e();
        String str2 = (e4 == null || (a = e4.a()) == null || (a2 = a.a()) == null) ? "" : a2;
        GuildMember q = message.q();
        String b4 = q != null ? q.b() : null;
        long j = i2;
        long j2 = this.guildId;
        String str3 = this.guildName;
        String str4 = this.guildIcon;
        List<Sticker> A = message.A();
        return new NotificationData(TYPE_MESSAGE_CREATE, o, b2, h, intValue, i, a4, emptyList, j, str, intOrNull, str2, b4, -1, null, j2, str3, str4, -1, null, -1L, null, null, A != null ? (Sticker) u.firstOrNull((List<? extends Object>) A) : null, null, null, null, null, null, null, null, null, null, null);
    }

    public final List<Long> getAckChannelIds() {
        return this.ackChannelIds;
    }

    @RequiresApi(24)
    @SuppressLint({"UnspecifiedImmutableFlag"})
    public final NotificationCompat.Action getCallAction(Context context, boolean z2) {
        Pair pair;
        CharSequence b2;
        m.checkNotNullParameter(context, "ctx");
        if (!m.areEqual(this.type, TYPE_CALL_RING)) {
            return null;
        }
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 0, addTrackingData(NotificationActions.Companion.callAction(context, this.channel.h(), this.messageId, z2)), PendingIntentExtensionsKt.immutablePendingIntentFlag$default(0, 1, null));
        if (z2) {
            pair = d0.o.to(Integer.valueOf((int) R.string.join_call), Integer.valueOf((int) R.color.status_green_600));
        } else {
            pair = d0.o.to(Integer.valueOf((int) R.string.decline), Integer.valueOf((int) R.color.status_red_500));
        }
        int intValue = ((Number) pair.component1()).intValue();
        int intValue2 = ((Number) pair.component2()).intValue();
        StringBuilder R = a.R("**");
        b2 = b.b(context, intValue, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        R.append(b2);
        R.append("**");
        return new NotificationCompat.Action.Builder(z2 ? R.drawable.ic_call_24dp : R.drawable.ic_call_disconnect_24dp, b.g(R.toString(), new Object[0], new NotificationData$getCallAction$coloredString$1(context, intValue2)), broadcast).build();
    }

    public final long getChannelId() {
        return this.channel.h();
    }

    public final CharSequence getContent(Context context) {
        CharSequence charSequence;
        GuildScheduledEventEntityType guildScheduledEventEntityType;
        CharSequence b2;
        CharSequence b3;
        CharSequence b4;
        CharSequence b5;
        CharSequence b6;
        CharSequence b7;
        CharSequence b8;
        CharSequence b9;
        CharSequence b10;
        CharSequence b11;
        CharSequence b12;
        CharSequence b13;
        CharSequence b14;
        CharSequence g;
        m.checkNotNullParameter(context, "context");
        String str = this.type;
        String str2 = "";
        switch (str.hashCode()) {
            case -1502317553:
                if (!str.equals(TYPE_GENERIC_PUSH_NOTIFICATION_SENT)) {
                    return str2;
                }
                String str3 = this.subtitleResName;
                if (str3 != null) {
                    String str4 = this.subtitle;
                    if (str4 != null) {
                        str2 = str4;
                    }
                    return resNameToString(context, str3, str2);
                }
                charSequence = this.subtitle;
                if (charSequence == null) {
                    return str2;
                }
                break;
            case -1489275252:
                if (!str.equals(TYPE_GUILD_SCHEDULED_EVENT_UPDATE) || (guildScheduledEventEntityType = this.guildScheduledEventEntityType) == null) {
                    return str2;
                }
                int ordinal = guildScheduledEventEntityType.ordinal();
                if (ordinal == 1 || ordinal == 2) {
                    b2 = b.b(context, R.string.guild_scheduled_event_stage_start_body, new Object[]{this.stageInstanceTopic, this.userUsername}, (r4 & 4) != 0 ? b.C0034b.j : null);
                    return b2;
                } else if (ordinal != 3) {
                    return str2;
                } else {
                    b3 = b.b(context, R.string.guild_scheduled_event_external_start_body, new Object[]{this.stageInstanceTopic}, (r4 & 4) != 0 ? b.C0034b.j : null);
                    return b3;
                }
            case -1327124998:
                if (!str.equals(TYPE_RELATIONSHIP_ADD)) {
                    return str2;
                }
                int i = this.relationshipType;
                if (i == 1) {
                    b4 = b.b(context, R.string.notification_accepted_friend_request, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                    return b4;
                } else if (i != 3) {
                    return str2;
                } else {
                    b5 = b.b(context, R.string.notification_pending_friend_request, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                    return b5;
                }
            case -1263316859:
                if (!str.equals(TYPE_STAGE_INSTANCE_CREATE)) {
                    return str2;
                }
                b6 = b.b(context, R.string.stage_start_push_notification_body, new Object[]{this.userUsername, this.stageInstanceTopic}, (r4 & 4) != 0 ? b.C0034b.j : null);
                return b6;
            case -1237752112:
                if (!str.equals(TYPE_APPLICATION_LIBRARY_INSTALL_COMPLETE)) {
                    return str2;
                }
                b7 = b.b(context, R.string.game_library_notification_game_installed_body, new Object[]{this.applicationName}, (r4 & 4) != 0 ? b.C0034b.j : null);
                return b7;
            case 974015250:
                if (!str.equals(TYPE_ACTIVITY_START) || this.activityType != 0) {
                    return str2;
                }
                b8 = b.b(context, R.string.notification_body_start_game, new Object[]{this.userUsername, this.activityName}, (r4 & 4) != 0 ? b.C0034b.j : null);
                return b8;
            case 998188116:
                if (!str.equals(TYPE_MESSAGE_CREATE)) {
                    return str2;
                }
                int A = this.channel.A();
                if (A != 0) {
                    if (A == 1 || A == 3) {
                        if (this.messageActivityType == MessageActivityType.JOIN) {
                            b13 = b.b(context, R.string.notification_message_create_dm_activity_join, new Object[]{this.userUsername, this.messageApplicationName}, (r4 & 4) != 0 ? b.C0034b.j : null);
                            return b13;
                        }
                        Sticker sticker = this.sticker;
                        if (sticker != null) {
                            b12 = b.b(context, R.string.sticker_notification_body, new Object[]{sticker.h()}, (r4 & 4) != 0 ? b.C0034b.j : null);
                            return b12;
                        }
                        String str5 = this.messageContent;
                        if (str5 != null) {
                            charSequence = b.g(str5, new Object[0], (r3 & 2) != 0 ? b.e.j : null);
                            break;
                        }
                        return null;
                    } else if (A != 5) {
                        switch (A) {
                            case 10:
                            case 11:
                            case 12:
                                break;
                            default:
                                return str2;
                        }
                    }
                }
                if (this.messageType == 7) {
                    b11 = b.b(context, MessageUtils.INSTANCE.getSystemMessageUserJoin(context, this.messageId), new Object[]{this.userUsername}, (r4 & 4) != 0 ? b.C0034b.j : null);
                    return b11;
                } else if (this.messageActivityType == MessageActivityType.JOIN) {
                    b10 = b.b(context, R.string.notification_message_create_guild_activity_join, new Object[]{this.userUsername, this.messageApplicationName}, (r4 & 4) != 0 ? b.C0034b.j : null);
                    return b10;
                } else {
                    Sticker sticker2 = this.sticker;
                    if (sticker2 != null) {
                        b9 = b.b(context, R.string.sticker_notification_body, new Object[]{sticker2.h()}, (r4 & 4) != 0 ? b.C0034b.j : null);
                        return b9;
                    }
                    String str6 = this.messageContent;
                    if (str6 != null) {
                        charSequence = b.g(str6, new Object[0], (r3 & 2) != 0 ? b.e.j : null);
                        break;
                    }
                    return null;
                }
            case 1770025841:
                if (!str.equals(TYPE_CALL_RING)) {
                    return str2;
                }
                b14 = b.b(context, R.string.overlay_friend_calling, new Object[]{a.H(a.R("**"), this.userUsername, "**")}, (r4 & 4) != 0 ? b.C0034b.j : null);
                g = b.g(b14, new Object[0], (r3 & 2) != 0 ? b.e.j : null);
                return g;
            default:
                return str2;
        }
        return charSequence;
    }

    @SuppressLint({"UnspecifiedImmutableFlag"})
    public final PendingIntent getContentIntent(Context context) {
        m.checkNotNullParameter(context, "context");
        Intent contentIntentInternal = getContentIntentInternal();
        contentIntentInternal.putExtra("com.discord.intent.ORIGIN_SOURCE", "com.discord.intent.ORIGIN_NOTIF");
        contentIntentInternal.setClass(context, AppActivity.Main.class);
        PendingIntent activity = PendingIntent.getActivity(context, 0, addTrackingData(contentIntentInternal), PendingIntentExtensionsKt.immutablePendingIntentFlag(134217728));
        m.checkNotNullExpressionValue(activity, "PendingIntent.getActivit…LAG_UPDATE_CURRENT)\n    )");
        return activity;
    }

    @SuppressLint({"UnspecifiedImmutableFlag"})
    public final PendingIntent getContentIntentInApp(Context context) {
        m.checkNotNullParameter(context, "context");
        Intent contentIntentInternal = getContentIntentInternal();
        contentIntentInternal.putExtra("com.discord.intent.ORIGIN_SOURCE", "com.discord.intent.ORIGIN_NOTIF_INAPP");
        contentIntentInternal.setClass(context, AppActivity.Main.class);
        PendingIntent activity = PendingIntent.getActivity(context, 0, addTrackingData(contentIntentInternal), PendingIntentExtensionsKt.immutablePendingIntentFlag(134217728));
        m.checkNotNullExpressionValue(activity, "PendingIntent.getActivit…LAG_UPDATE_CURRENT)\n    )");
        return activity;
    }

    public final CharSequence getConversationTitle(Context context) {
        CharSequence g;
        m.checkNotNullParameter(context, "context");
        String str = this.type;
        if (str.hashCode() != 998188116 || !str.equals(TYPE_MESSAGE_CREATE)) {
            return null;
        }
        int A = this.channel.A();
        if (A != 0) {
            if (A == 3) {
                return ChannelUtils.e(this.channel, context, false, 2);
            }
            if (A != 5) {
                switch (A) {
                    case 10:
                    case 11:
                    case 12:
                        break;
                    default:
                        return null;
                }
            }
        }
        g = b.g(this.guildName + " _" + ChannelUtils.e(this.channel, context, false, 2) + '_', new Object[0], (r3 & 2) != 0 ? b.e.j : null);
        return g;
    }

    @SuppressLint({"UnspecifiedImmutableFlag"})
    public final PendingIntent getDeleteIntent(Context context) {
        m.checkNotNullParameter(context, "context");
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 0, NotificationActions.Companion.delete(context, this.channel.h()), PendingIntentExtensionsKt.immutablePendingIntentFlag(134217728));
        m.checkNotNullExpressionValue(broadcast, "PendingIntent.getBroadca…LAG_UPDATE_CURRENT)\n    )");
        return broadcast;
    }

    @RequiresApi(24)
    @SuppressLint({"UnspecifiedImmutableFlag"})
    public final NotificationCompat.Action getDirectReplyAction(Context context, Set<Long> set) {
        CharSequence b2;
        CharSequence b3;
        m.checkNotNullParameter(context, "ctx");
        m.checkNotNullParameter(set, "deniedChannels");
        if ((!m.areEqual(this.type, TYPE_MESSAGE_CREATE)) || set.contains(Long.valueOf(this.channel.h()))) {
            return null;
        }
        RemoteInput.Builder builder = new RemoteInput.Builder("discord_notif_text_input");
        b2 = b.b(context, R.string.send_message, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        RemoteInput build = builder.setLabel(b2).build();
        m.checkNotNullExpressionValue(build, "RemoteInput.Builder(Inte…essage))\n        .build()");
        PendingIntent broadcast = PendingIntent.getBroadcast(context, (int) this.channel.h(), addTrackingData(NotificationActions.Companion.directReply(context, this.channel.h(), getTitle(context))), PendingIntentExtensionsKt.mutablePendingIntentFlag(134217728));
        b3 = b.b(context, R.string.notification_reply, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        return new NotificationCompat.Action.Builder((int) R.drawable.ic_send_white_24dp, b3, broadcast).addRemoteInput(build).setAllowGeneratedReplies(true).build();
    }

    @SuppressLint({"UnspecifiedImmutableFlag"})
    public final PendingIntent getFullScreenIntent(Context context) {
        m.checkNotNullParameter(context, "context");
        if (!m.areEqual(getNotificationCategory(), NotificationCompat.CATEGORY_CALL)) {
            return null;
        }
        Intent intent = new Intent(context, AppActivity.IncomingCall.class);
        intent.addFlags(268435456);
        return PendingIntent.getActivity(context, 0, addTrackingData(intent), PendingIntentExtensionsKt.immutablePendingIntentFlag(134217728));
    }

    public final String getGroupKey() {
        StringBuilder R = a.R(GROUP_KEY_PREFIX);
        R.append(this.type);
        return R.toString();
    }

    public final long getGuildId() {
        return this.guildId;
    }

    /* JADX WARN: Removed duplicated region for block: B:20:0x004c  */
    /* JADX WARN: Removed duplicated region for block: B:29:? A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:34:? A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:37:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.lang.String getIconUrl() {
        /*
            r10 = this;
            java.lang.String r0 = r10.type
            int r1 = r0.hashCode()
            java.lang.String r2 = ""
            switch(r1) {
                case -1489275252: goto L56;
                case -1327124998: goto L40;
                case -1263316859: goto L37;
                case -1237752112: goto L2e;
                case 974015250: goto L25;
                case 998188116: goto L16;
                case 1770025841: goto Ld;
                default: goto Lb;
            }
        Lb:
            goto L74
        Ld:
            java.lang.String r1 = "CALL_RING"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L74
            goto L1e
        L16:
            java.lang.String r1 = "MESSAGE_CREATE"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L74
        L1e:
            java.lang.String r0 = r10.getIconUrlForChannel()
            if (r0 == 0) goto L74
            goto L73
        L25:
            java.lang.String r1 = "ACTIVITY_START"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L74
            goto L48
        L2e:
            java.lang.String r1 = "APPLICATION_LIBRARY_INSTALL_COMPLETE"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L74
            goto L48
        L37:
            java.lang.String r1 = "STAGE_INSTANCE_CREATE"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L74
            goto L5e
        L40:
            java.lang.String r1 = "RELATIONSHIP_ADD"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L74
        L48:
            java.lang.String r5 = r10.applicationIcon
            if (r5 == 0) goto L74
            long r3 = r10.applicationId
            r6 = 0
            r7 = 4
            r8 = 0
            java.lang.String r2 = com.discord.utilities.icon.IconUtils.getApplicationIcon$default(r3, r5, r6, r7, r8)
            goto L74
        L56:
            java.lang.String r1 = "GUILD_SCHEDULED_EVENT_UPDATE"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L74
        L5e:
            long r0 = r10.guildId
            java.lang.Long r3 = java.lang.Long.valueOf(r0)
            java.lang.String r4 = r10.guildIcon
            r6 = 0
            r7 = 0
            r8 = 24
            r9 = 0
            java.lang.String r5 = ""
            java.lang.String r0 = com.discord.utilities.icon.IconUtils.getForGuild$default(r3, r4, r5, r6, r7, r8, r9)
            if (r0 == 0) goto L74
        L73:
            r2 = r0
        L74:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.fcm.NotificationData.getIconUrl():java.lang.String");
    }

    public final String getIconUrlForAvatar() {
        String iconUrlForGuildMember = getIconUrlForGuildMember();
        return iconUrlForGuildMember != null ? iconUrlForGuildMember : getIconUrlForUser();
    }

    public final String getIconUrlForUser() {
        return IconUtils.getForUser$default(Long.valueOf(this.userId), this.userAvatar, this.userDiscriminator, false, null, 24, null);
    }

    public final String getKey() {
        GuildScheduledEventEntityType guildScheduledEventEntityType;
        String str = this.type;
        switch (str.hashCode()) {
            case -1502317553:
                if (!str.equals(TYPE_GENERIC_PUSH_NOTIFICATION_SENT)) {
                    return "";
                }
                return this.type + this.deeplink;
            case -1489275252:
                if (!str.equals(TYPE_GUILD_SCHEDULED_EVENT_UPDATE) || (guildScheduledEventEntityType = this.guildScheduledEventEntityType) == null) {
                    return "";
                }
                int ordinal = guildScheduledEventEntityType.ordinal();
                if (ordinal == 1 || ordinal == 2) {
                    return this.type + this.channel.h();
                } else if (ordinal != 3) {
                    return "";
                } else {
                    return this.type + this.guildId;
                }
            case -1327124998:
                if (!str.equals(TYPE_RELATIONSHIP_ADD)) {
                    return "";
                }
                NotificationType notificationType = this.notificationType;
                if (notificationType != null && notificationType.ordinal() == 0) {
                    return this.type + this.notificationType;
                }
                return this.type + this.userId;
            case -1263316859:
                if (!str.equals(TYPE_STAGE_INSTANCE_CREATE)) {
                    return "";
                }
                break;
            case -1237752112:
                if (!str.equals(TYPE_APPLICATION_LIBRARY_INSTALL_COMPLETE)) {
                    return "";
                }
                return this.type + this.applicationId;
            case 974015250:
                if (!str.equals(TYPE_ACTIVITY_START)) {
                    return "";
                }
                return this.type + this.activityType + this.activityName;
            case 998188116:
                if (!str.equals(TYPE_MESSAGE_CREATE)) {
                    return "";
                }
                break;
            case 1770025841:
                if (!str.equals(TYPE_CALL_RING)) {
                    return "";
                }
                break;
            default:
                return "";
        }
        return this.type + this.channel.h();
    }

    @RequiresApi(24)
    @SuppressLint({"UnspecifiedImmutableFlag"})
    public final NotificationCompat.Action getMarkAsReadAction(Context context) {
        CharSequence b2;
        m.checkNotNullParameter(context, "ctx");
        if (!m.areEqual(this.type, TYPE_MESSAGE_CREATE)) {
            return null;
        }
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 0, addTrackingData(NotificationActions.Companion.markAsRead(context, this.channel.h(), this.messageId)), PendingIntentExtensionsKt.immutablePendingIntentFlag$default(0, 1, null));
        b2 = b.b(context, R.string.mark_as_read, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        return new NotificationCompat.Action.Builder((int) R.drawable.ic_check_grey_24dp, b2, broadcast).build();
    }

    public final long getMessageId() {
        return this.messageId;
    }

    public final long getMessageIdTimestamp() {
        return (this.messageId >>> 22) + SnowflakeUtils.DISCORD_EPOCH;
    }

    /* JADX WARN: Removed duplicated region for block: B:20:0x004c A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:21:0x004f A[ORIG_RETURN, RETURN] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.lang.String getNotificationCategory() {
        /*
            r2 = this;
            java.lang.String r0 = r2.type
            int r1 = r0.hashCode()
            switch(r1) {
                case -1502317553: goto L44;
                case -1489275252: goto L3b;
                case -1327124998: goto L32;
                case -1263316859: goto L29;
                case -1237752112: goto L1e;
                case 974015250: goto L15;
                case 1770025841: goto La;
                default: goto L9;
            }
        L9:
            goto L4f
        La:
            java.lang.String r1 = "CALL_RING"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L4f
            java.lang.String r0 = "call"
            goto L51
        L15:
            java.lang.String r1 = "ACTIVITY_START"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L4f
            goto L4c
        L1e:
            java.lang.String r1 = "APPLICATION_LIBRARY_INSTALL_COMPLETE"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L4f
            java.lang.String r0 = "progress"
            goto L51
        L29:
            java.lang.String r1 = "STAGE_INSTANCE_CREATE"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L4f
            goto L4c
        L32:
            java.lang.String r1 = "RELATIONSHIP_ADD"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L4f
            goto L4c
        L3b:
            java.lang.String r1 = "GUILD_SCHEDULED_EVENT_UPDATE"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L4f
            goto L4c
        L44:
            java.lang.String r1 = "GENERIC_PUSH_NOTIFICATION_SENT"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L4f
        L4c:
            java.lang.String r0 = "social"
            goto L51
        L4f:
            java.lang.String r0 = "msg"
        L51:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.fcm.NotificationData.getNotificationCategory():java.lang.String");
    }

    public final String getNotificationChannelId() {
        String str = this.type;
        switch (str.hashCode()) {
            case -1502317553:
                if (str.equals(TYPE_GENERIC_PUSH_NOTIFICATION_SENT)) {
                    String str2 = this.notificationChannel;
                    return str2 != null ? str2 : NotificationClient.NOTIF_GENERAL;
                }
                break;
            case -1489275252:
                if (str.equals(TYPE_GUILD_SCHEDULED_EVENT_UPDATE)) {
                    return NotificationClient.NOTIF_GUILD_SCHEDULED_EVENT_START;
                }
                break;
            case -1263316859:
                if (str.equals(TYPE_STAGE_INSTANCE_CREATE)) {
                    return NotificationClient.NOTIF_CHANNEL_STAGE_START;
                }
                break;
            case 998188116:
                if (str.equals(TYPE_MESSAGE_CREATE)) {
                    int A = this.channel.A();
                    return (A == 1 || A == 3) ? NotificationClient.NOTIF_CHANNEL_MESSAGES_DIRECT : NotificationClient.NOTIF_CHANNEL_MESSAGES;
                }
                break;
            case 1770025841:
                if (str.equals(TYPE_CALL_RING)) {
                    return NotificationClient.NOTIF_CHANNEL_CALLS;
                }
                break;
        }
        return NotificationClient.NOTIF_CHANNEL_SOCIAL;
    }

    /* JADX WARN: Removed duplicated region for block: B:15:0x0042 A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:20:0x0055 A[ORIG_RETURN, RETURN] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final int getNotificationPriority() {
        /*
            r5 = this;
            java.lang.String r0 = r5.getNotificationChannelId()
            int r1 = r0.hashCode()
            r2 = 2
            r3 = 0
            r4 = -1
            switch(r1) {
                case -1917463435: goto L4b;
                case -1813183603: goto L44;
                case -1241096946: goto L3a;
                case -397449876: goto L31;
                case 64872885: goto L28;
                case 526428889: goto L1f;
                case 803834207: goto L16;
                case 1584505032: goto Lf;
                default: goto Le;
            }
        Le:
            goto L55
        Lf:
            java.lang.String r1 = "General"
            boolean r0 = r0.equals(r1)
            goto L55
        L16:
            java.lang.String r1 = "Guild Event Live"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L55
            goto L39
        L1f:
            java.lang.String r1 = "Media Connections"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L55
            goto L56
        L28:
            java.lang.String r1 = "Calls"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L55
            goto L56
        L31:
            java.lang.String r1 = "Messages"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L55
        L39:
            goto L42
        L3a:
            java.lang.String r1 = "Stage Live"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L55
        L42:
            r2 = 0
            goto L56
        L44:
            java.lang.String r1 = "Social"
            boolean r0 = r0.equals(r1)
            goto L55
        L4b:
            java.lang.String r1 = "DirectMessages"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L55
            r2 = 1
            goto L56
        L55:
            r2 = -1
        L56:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.fcm.NotificationData.getNotificationPriority():int");
    }

    public final Uri getNotificationSound(Context context) {
        m.checkNotNullParameter(context, "context");
        String str = this.type;
        if (str.hashCode() != 1770025841 || !str.equals(TYPE_CALL_RING)) {
            return null;
        }
        StringBuilder R = a.R("android.resource://");
        R.append(context.getPackageName() + MentionUtilsKt.SLASH_CHAR + AppSound.Companion.getSOUND_CALL_RINGING().getResId());
        String sb = R.toString();
        m.checkNotNullExpressionValue(sb, "StringBuilder()\n        …)\n            .toString()");
        return Uri.parse(sb);
    }

    public final Person getSender(Context context) {
        CharSequence charSequence;
        String replaceAfterLast$default;
        MessageActivityType messageActivityType;
        m.checkNotNullParameter(context, "context");
        IconCompat iconCompat = null;
        boolean z2 = false;
        if (!m.areEqual(this.type, TYPE_MESSAGE_CREATE) || this.messageType == 7 || (messageActivityType = this.messageActivityType) == MessageActivityType.SPECTATE || messageActivityType == MessageActivityType.JOIN) {
            charSequence = b.b(context, R.string.discord, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        } else {
            charSequence = this.userUsername;
        }
        String valueOf = String.valueOf(this.userId);
        String str = this.subtitle;
        if (!(str == null || charSequence == null)) {
            if (this.messageType == 19 && str.length() > 11) {
                String str2 = this.subtitle;
                Objects.requireNonNull(str2, "null cannot be cast to non-null type java.lang.String");
                String substring = str2.substring(0, 11);
                m.checkNotNullExpressionValue(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                if (m.areEqual(substring, REPLYING_TO_UNTRANSLATED)) {
                    StringBuilder R = a.R("<b>");
                    R.append(Html.escapeHtml(charSequence));
                    R.append("</b> ");
                    String str3 = this.subtitle;
                    Objects.requireNonNull(str3, "null cannot be cast to non-null type java.lang.String");
                    String substring2 = str3.substring(0, 11);
                    m.checkNotNullExpressionValue(substring2, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                    R.append(Html.escapeHtml(substring2));
                    R.append("<b>");
                    String str4 = this.subtitle;
                    Objects.requireNonNull(str4, "null cannot be cast to non-null type java.lang.String");
                    String substring3 = str4.substring(11);
                    m.checkNotNullExpressionValue(substring3, "(this as java.lang.String).substring(startIndex)");
                    R.append(Html.escapeHtml(substring3));
                    R.append("</b>");
                    charSequence = Html.fromHtml(R.toString());
                    StringBuilder R2 = a.R(valueOf);
                    R2.append(this.subtitle);
                    valueOf = R2.toString();
                }
            }
            StringBuilder R3 = a.R("<b>");
            R3.append(Html.escapeHtml(charSequence));
            R3.append("</b> ");
            R3.append(Html.escapeHtml(this.subtitle));
            charSequence = Html.fromHtml(R3.toString());
            StringBuilder R22 = a.R(valueOf);
            R22.append(this.subtitle);
            valueOf = R22.toString();
        }
        String forUser$default = IconUtils.getForUser$default(Long.valueOf(this.userId), this.userAvatar, null, false, null, 28, null);
        if (!(forUser$default == null || (replaceAfterLast$default = w.replaceAfterLast$default(forUser$default, ClassUtils.PACKAGE_SEPARATOR_CHAR, "jpg", null, 4, null)) == null)) {
            iconCompat = IconCompat.createWithContentUri(replaceAfterLast$default);
        }
        Person.Builder icon = new Person.Builder().setName(charSequence).setKey(valueOf).setIcon(iconCompat);
        if (this.relationshipType == 1) {
            z2 = true;
        }
        Person build = icon.setImportant(z2).build();
        m.checkNotNullExpressionValue(build, "Person.Builder()\n       …_FRIEND)\n        .build()");
        return build;
    }

    public final boolean getShouldGroup() {
        String str = this.type;
        int hashCode = str.hashCode();
        return hashCode == 974015250 ? str.equals(TYPE_ACTIVITY_START) : !(hashCode != 998188116 || !str.equals(TYPE_MESSAGE_CREATE));
    }

    public final boolean getShouldUseBigText() {
        String str = this.type;
        int hashCode = str.hashCode();
        return hashCode == -1489275252 ? str.equals(TYPE_GUILD_SCHEDULED_EVENT_UPDATE) : !(hashCode != -1263316859 || !str.equals(TYPE_STAGE_INSTANCE_CREATE));
    }

    public final int getSmallIcon() {
        String str = this.type;
        int hashCode = str.hashCode();
        if (hashCode != -1327124998) {
            if (hashCode != 998188116) {
                if (hashCode == 1770025841 && str.equals(TYPE_CALL_RING)) {
                    return R.drawable.ic_notification_call_24dp;
                }
            } else if (str.equals(TYPE_MESSAGE_CREATE)) {
                return R.drawable.ic_notification_message_24dp;
            }
        } else if (str.equals(TYPE_RELATIONSHIP_ADD)) {
            return R.drawable.ic_notification_friends_24dp;
        }
        return R.drawable.ic_notification_24dp;
    }

    public final Sticker getSticker() {
        return this.sticker;
    }

    @RequiresApi(24)
    @SuppressLint({"UnspecifiedImmutableFlag"})
    public final NotificationCompat.Action getTimedMute(Context context, Clock clock, int i) {
        CharSequence b2;
        m.checkNotNullParameter(context, "ctx");
        m.checkNotNullParameter(clock, "clock");
        if ((!m.areEqual(this.type, TYPE_MESSAGE_CREATE)) || i < 2) {
            return null;
        }
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 0, addTrackingData(NotificationActions.Companion.timedMute(context, this.guildId, this.channel.h(), clock.currentTimeMillis() + 3600000)), PendingIntentExtensionsKt.immutablePendingIntentFlag$default(0, 1, null));
        b2 = b.b(context, R.string.notification_mute_1_hour, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        return new NotificationCompat.Action.Builder((int) R.drawable.ic_notifications_off_grey_24dp, b2, broadcast).build();
    }

    public final CharSequence getTitle(Context context) {
        String str;
        GuildScheduledEventEntityType guildScheduledEventEntityType;
        CharSequence b2;
        CharSequence b3;
        CharSequence b4;
        CharSequence b5;
        CharSequence g;
        CharSequence b6;
        m.checkNotNullParameter(context, "context");
        String str2 = this.type;
        String str3 = "";
        switch (str2.hashCode()) {
            case -1502317553:
                if (!str2.equals(TYPE_GENERIC_PUSH_NOTIFICATION_SENT)) {
                    return str3;
                }
                String str4 = this.titleResName;
                if (str4 != null) {
                    String str5 = this.title;
                    if (str5 != null) {
                        str3 = str5;
                    }
                    return resNameToString(context, str4, str3);
                }
                str = this.title;
                if (str == null) {
                    return str3;
                }
                break;
            case -1489275252:
                if (!str2.equals(TYPE_GUILD_SCHEDULED_EVENT_UPDATE) || (guildScheduledEventEntityType = this.guildScheduledEventEntityType) == null) {
                    return str3;
                }
                int ordinal = guildScheduledEventEntityType.ordinal();
                if (ordinal != 1 && ordinal != 2 && ordinal != 3) {
                    return str3;
                }
                b2 = b.b(context, R.string.guild_scheduled_event_stage_start_title, new Object[]{this.guildName}, (r4 & 4) != 0 ? b.C0034b.j : null);
                return b2;
            case -1327124998:
                if (!str2.equals(TYPE_RELATIONSHIP_ADD)) {
                    return str3;
                }
                if ((this.notificationType != NotificationType.Reminder || (str = this.title) == null) && (str = this.userUsername) == null) {
                    return str3;
                }
                break;
            case -1263316859:
                if (!str2.equals(TYPE_STAGE_INSTANCE_CREATE)) {
                    return str3;
                }
                b3 = b.b(context, R.string.stage_start_push_notification_title, new Object[]{this.guildName}, (r4 & 4) != 0 ? b.C0034b.j : null);
                return b3;
            case -1237752112:
                if (!str2.equals(TYPE_APPLICATION_LIBRARY_INSTALL_COMPLETE)) {
                    return str3;
                }
                b4 = b.b(context, R.string.game_library_notification_game_installed_title, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                return b4;
            case 974015250:
                if (!str2.equals(TYPE_ACTIVITY_START) || this.activityType != 0) {
                    return str3;
                }
                b5 = b.b(context, R.string.notification_title_start_game, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                return b5;
            case 998188116:
                if (!str2.equals(TYPE_MESSAGE_CREATE)) {
                    return str3;
                }
                int A = this.channel.A();
                if (A != 0) {
                    if (A == 1) {
                        str = this.userUsername;
                        if (str == null) {
                            return str3;
                        }
                    } else if (A == 3) {
                        return ChannelUtils.e(this.channel, context, false, 2);
                    } else {
                        if (A != 5) {
                            switch (A) {
                                case 10:
                                case 11:
                                case 12:
                                    break;
                                default:
                                    return str3;
                            }
                        }
                    }
                }
                g = b.g(this.guildName + " _" + ChannelUtils.e(this.channel, context, false, 2) + '_', new Object[0], (r3 & 2) != 0 ? b.e.j : null);
                return g;
            case 1770025841:
                if (!str2.equals(TYPE_CALL_RING)) {
                    return str3;
                }
                if (this.channel.A() != 1) {
                    return ChannelUtils.c(this.channel);
                }
                b6 = b.b(context, R.string.incoming_call, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                return b6;
            default:
                return str3;
        }
        return str;
    }

    public final String getType() {
        return this.type;
    }

    public final boolean isGroupConversation() {
        return this.isGroupConversation;
    }

    public final boolean isValid() {
        GuildScheduledEventEntityType guildScheduledEventEntityType;
        String str = this.type;
        switch (str.hashCode()) {
            case -1502317553:
                if (!str.equals(TYPE_GENERIC_PUSH_NOTIFICATION_SENT)) {
                    return false;
                }
                break;
            case -1489275252:
                if (!str.equals(TYPE_GUILD_SCHEDULED_EVENT_UPDATE) || (guildScheduledEventEntityType = this.guildScheduledEventEntityType) == null) {
                    return false;
                }
                int ordinal = guildScheduledEventEntityType.ordinal();
                if (ordinal != 1 && ordinal != 2 && ordinal != 3) {
                    return false;
                }
                break;
            case -1327124998:
                if (!str.equals(TYPE_RELATIONSHIP_ADD)) {
                    return false;
                }
                break;
            case -1263316859:
                if (!str.equals(TYPE_STAGE_INSTANCE_CREATE)) {
                    return false;
                }
                break;
            case -1237752112:
                if (!str.equals(TYPE_APPLICATION_LIBRARY_INSTALL_COMPLETE)) {
                    return false;
                }
                break;
            case 974015250:
                if (!str.equals(TYPE_ACTIVITY_START) || this.activityType == 1) {
                    return false;
                }
                break;
            case 998188116:
                if (!str.equals(TYPE_MESSAGE_CREATE)) {
                    return false;
                }
                break;
            case 1770025841:
                if (!str.equals(TYPE_CALL_RING)) {
                    return false;
                }
                break;
            default:
                return false;
        }
        return true;
    }

    /* compiled from: NotificationData.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\r\b\u0000\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\"\u0010\u0016B\t\b\u0016¢\u0006\u0004\b\"\u0010#J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u001f\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00072\b\b\u0002\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\t\u0010\nJ\u0013\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00070\u000b¢\u0006\u0004\b\f\u0010\rR\"\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00070\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\"\u0010\b\u001a\u00020\u00078\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\b\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\"\u0010\u0018\u001a\u00020\u00178\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001dR\u0019\u0010\u001e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010!¨\u0006$"}, d2 = {"Lcom/discord/utilities/fcm/NotificationData$DisplayPayload;", "", "", "maxExtras", "", "trimExtrasWhenOversized", "(I)V", "Lcom/discord/utilities/fcm/NotificationData;", "data", "update", "(Lcom/discord/utilities/fcm/NotificationData;I)V", "", "getExtras", "()Ljava/util/List;", "Ljava/util/LinkedHashMap;", "", NotificationCompat.MessagingStyle.Message.KEY_EXTRAS_BUNDLE, "Ljava/util/LinkedHashMap;", "Lcom/discord/utilities/fcm/NotificationData;", "getData", "()Lcom/discord/utilities/fcm/NotificationData;", "setData", "(Lcom/discord/utilities/fcm/NotificationData;)V", "", "ignoreNextClearForAck", "Z", "getIgnoreNextClearForAck", "()Z", "setIgnoreNextClearForAck", "(Z)V", ModelAuditLogEntry.CHANGE_KEY_ID, "I", "getId", "()I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class DisplayPayload {
        private NotificationData data;
        private final LinkedHashMap<Long, NotificationData> extras;

        /* renamed from: id */
        private final int f2789id;
        private boolean ignoreNextClearForAck;

        public DisplayPayload(NotificationData notificationData) {
            m.checkNotNullParameter(notificationData, "data");
            this.data = notificationData;
            this.f2789id = new Random().nextInt(Integer.MAX_VALUE);
            this.extras = new LinkedHashMap<>();
        }

        private final void trimExtrasWhenOversized(int i) {
            if (this.extras.size() >= i) {
                Set<Map.Entry<Long, NotificationData>> entrySet = this.extras.entrySet();
                m.checkNotNullExpressionValue(entrySet, "extras\n          .entries");
                List<Map.Entry> drop = u.drop(entrySet, this.extras.size() - (i / 2));
                LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(o.collectionSizeOrDefault(drop, 10)), 16));
                for (Map.Entry entry : drop) {
                    linkedHashMap.put((Long) entry.getKey(), (NotificationData) entry.getValue());
                }
                this.extras.clear();
                this.extras.putAll(linkedHashMap);
            }
        }

        public static /* synthetic */ void update$default(DisplayPayload displayPayload, NotificationData notificationData, int i, int i2, Object obj) {
            if ((i2 & 2) != 0) {
                i = 20;
            }
            displayPayload.update(notificationData, i);
        }

        public final NotificationData getData() {
            return this.data;
        }

        public final synchronized List<NotificationData> getExtras() {
            Collection<NotificationData> values;
            values = this.extras.values();
            m.checkNotNullExpressionValue(values, "extras.values");
            return u.toList(values);
        }

        public final int getId() {
            return this.f2789id;
        }

        public final boolean getIgnoreNextClearForAck() {
            return this.ignoreNextClearForAck;
        }

        public final void setData(NotificationData notificationData) {
            m.checkNotNullParameter(notificationData, "<set-?>");
            this.data = notificationData;
        }

        public final void setIgnoreNextClearForAck(boolean z2) {
            this.ignoreNextClearForAck = z2;
        }

        public final synchronized void update(NotificationData notificationData, int i) {
            m.checkNotNullParameter(notificationData, "data");
            this.data = notificationData;
            trimExtrasWhenOversized(i);
            if (notificationData.getShouldGroup()) {
                this.extras.put(Long.valueOf(notificationData.getMessageId()), notificationData);
            } else {
                this.extras.clear();
            }
        }

        public DisplayPayload() {
            this(new NotificationData());
        }
    }

    public NotificationData() {
        this(new HashMap());
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public NotificationData(java.util.Map<java.lang.String, java.lang.String> r51) {
        /*
            Method dump skipped, instructions count: 694
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.fcm.NotificationData.<init>(java.util.Map):void");
    }
}
