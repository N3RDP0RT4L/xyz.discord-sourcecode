package com.discord.utilities.channel;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ChannelPermissionUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0015\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001Bo\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b-\u0010.J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\b\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\u0004J\u0012\u0010\n\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\n\u0010\u0007J\u0012\u0010\u000b\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u000b\u0010\u0007J\u0012\u0010\f\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\f\u0010\u0007J\u0012\u0010\r\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\r\u0010\u0004J\u0012\u0010\u000e\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u000e\u0010\u0004Jz\u0010\u0018\u001a\u00020\u00002\b\b\u0002\u0010\u000f\u001a\u00020\u00022\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u001a\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u001a\u0010\u0007J\u0010\u0010\u001c\u001a\u00020\u001bHÖ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u001a\u0010 \u001a\u00020\u001f2\b\u0010\u001e\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b \u0010!R\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\"\u001a\u0004\b#\u0010\u0004R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\"\u001a\u0004\b$\u0010\u0004R\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010%\u001a\u0004\b&\u0010\u0007R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010%\u001a\u0004\b'\u0010\u0007R\u001b\u0010\u0014\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010%\u001a\u0004\b(\u0010\u0007R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010%\u001a\u0004\b)\u0010\u0007R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\"\u001a\u0004\b*\u0010\u0004R\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010%\u001a\u0004\b+\u0010\u0007R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\"\u001a\u0004\b,\u0010\u0004¨\u0006/"}, d2 = {"Lcom/discord/utilities/channel/PermissionLabelOverrides;", "", "", "component1", "()Ljava/lang/CharSequence;", "", "component2", "()Ljava/lang/String;", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "categorySubtext", "categoryLabel", "voiceChannelSubtext", "stageChannelSubtext", "textChannelEveryoneSubtext", "voiceChannelEveryoneSubtext", "stageChannelEveryoneSubtext", "categoryEveryoneSubtext", "announcementChannelSubtext", "copy", "(Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/discord/utilities/channel/PermissionLabelOverrides;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/CharSequence;", "getCategorySubtext", "getAnnouncementChannelSubtext", "Ljava/lang/String;", "getCategoryLabel", "getTextChannelEveryoneSubtext", "getVoiceChannelEveryoneSubtext", "getStageChannelEveryoneSubtext", "getCategoryEveryoneSubtext", "getVoiceChannelSubtext", "getStageChannelSubtext", HookHelper.constructorName, "(Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class PermissionLabelOverrides {
    private final CharSequence announcementChannelSubtext;
    private final CharSequence categoryEveryoneSubtext;
    private final String categoryLabel;
    private final CharSequence categorySubtext;
    private final String stageChannelEveryoneSubtext;
    private final CharSequence stageChannelSubtext;
    private final String textChannelEveryoneSubtext;
    private final String voiceChannelEveryoneSubtext;
    private final String voiceChannelSubtext;

    public PermissionLabelOverrides(CharSequence charSequence, String str, String str2, CharSequence charSequence2, String str3, String str4, String str5, CharSequence charSequence3, CharSequence charSequence4) {
        m.checkNotNullParameter(charSequence, "categorySubtext");
        this.categorySubtext = charSequence;
        this.categoryLabel = str;
        this.voiceChannelSubtext = str2;
        this.stageChannelSubtext = charSequence2;
        this.textChannelEveryoneSubtext = str3;
        this.voiceChannelEveryoneSubtext = str4;
        this.stageChannelEveryoneSubtext = str5;
        this.categoryEveryoneSubtext = charSequence3;
        this.announcementChannelSubtext = charSequence4;
    }

    public final CharSequence component1() {
        return this.categorySubtext;
    }

    public final String component2() {
        return this.categoryLabel;
    }

    public final String component3() {
        return this.voiceChannelSubtext;
    }

    public final CharSequence component4() {
        return this.stageChannelSubtext;
    }

    public final String component5() {
        return this.textChannelEveryoneSubtext;
    }

    public final String component6() {
        return this.voiceChannelEveryoneSubtext;
    }

    public final String component7() {
        return this.stageChannelEveryoneSubtext;
    }

    public final CharSequence component8() {
        return this.categoryEveryoneSubtext;
    }

    public final CharSequence component9() {
        return this.announcementChannelSubtext;
    }

    public final PermissionLabelOverrides copy(CharSequence charSequence, String str, String str2, CharSequence charSequence2, String str3, String str4, String str5, CharSequence charSequence3, CharSequence charSequence4) {
        m.checkNotNullParameter(charSequence, "categorySubtext");
        return new PermissionLabelOverrides(charSequence, str, str2, charSequence2, str3, str4, str5, charSequence3, charSequence4);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PermissionLabelOverrides)) {
            return false;
        }
        PermissionLabelOverrides permissionLabelOverrides = (PermissionLabelOverrides) obj;
        return m.areEqual(this.categorySubtext, permissionLabelOverrides.categorySubtext) && m.areEqual(this.categoryLabel, permissionLabelOverrides.categoryLabel) && m.areEqual(this.voiceChannelSubtext, permissionLabelOverrides.voiceChannelSubtext) && m.areEqual(this.stageChannelSubtext, permissionLabelOverrides.stageChannelSubtext) && m.areEqual(this.textChannelEveryoneSubtext, permissionLabelOverrides.textChannelEveryoneSubtext) && m.areEqual(this.voiceChannelEveryoneSubtext, permissionLabelOverrides.voiceChannelEveryoneSubtext) && m.areEqual(this.stageChannelEveryoneSubtext, permissionLabelOverrides.stageChannelEveryoneSubtext) && m.areEqual(this.categoryEveryoneSubtext, permissionLabelOverrides.categoryEveryoneSubtext) && m.areEqual(this.announcementChannelSubtext, permissionLabelOverrides.announcementChannelSubtext);
    }

    public final CharSequence getAnnouncementChannelSubtext() {
        return this.announcementChannelSubtext;
    }

    public final CharSequence getCategoryEveryoneSubtext() {
        return this.categoryEveryoneSubtext;
    }

    public final String getCategoryLabel() {
        return this.categoryLabel;
    }

    public final CharSequence getCategorySubtext() {
        return this.categorySubtext;
    }

    public final String getStageChannelEveryoneSubtext() {
        return this.stageChannelEveryoneSubtext;
    }

    public final CharSequence getStageChannelSubtext() {
        return this.stageChannelSubtext;
    }

    public final String getTextChannelEveryoneSubtext() {
        return this.textChannelEveryoneSubtext;
    }

    public final String getVoiceChannelEveryoneSubtext() {
        return this.voiceChannelEveryoneSubtext;
    }

    public final String getVoiceChannelSubtext() {
        return this.voiceChannelSubtext;
    }

    public int hashCode() {
        CharSequence charSequence = this.categorySubtext;
        int i = 0;
        int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
        String str = this.categoryLabel;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.voiceChannelSubtext;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        CharSequence charSequence2 = this.stageChannelSubtext;
        int hashCode4 = (hashCode3 + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
        String str3 = this.textChannelEveryoneSubtext;
        int hashCode5 = (hashCode4 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.voiceChannelEveryoneSubtext;
        int hashCode6 = (hashCode5 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.stageChannelEveryoneSubtext;
        int hashCode7 = (hashCode6 + (str5 != null ? str5.hashCode() : 0)) * 31;
        CharSequence charSequence3 = this.categoryEveryoneSubtext;
        int hashCode8 = (hashCode7 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
        CharSequence charSequence4 = this.announcementChannelSubtext;
        if (charSequence4 != null) {
            i = charSequence4.hashCode();
        }
        return hashCode8 + i;
    }

    public String toString() {
        StringBuilder R = a.R("PermissionLabelOverrides(categorySubtext=");
        R.append(this.categorySubtext);
        R.append(", categoryLabel=");
        R.append(this.categoryLabel);
        R.append(", voiceChannelSubtext=");
        R.append(this.voiceChannelSubtext);
        R.append(", stageChannelSubtext=");
        R.append(this.stageChannelSubtext);
        R.append(", textChannelEveryoneSubtext=");
        R.append(this.textChannelEveryoneSubtext);
        R.append(", voiceChannelEveryoneSubtext=");
        R.append(this.voiceChannelEveryoneSubtext);
        R.append(", stageChannelEveryoneSubtext=");
        R.append(this.stageChannelEveryoneSubtext);
        R.append(", categoryEveryoneSubtext=");
        R.append(this.categoryEveryoneSubtext);
        R.append(", announcementChannelSubtext=");
        return a.D(R, this.announcementChannelSubtext, ")");
    }

    public /* synthetic */ PermissionLabelOverrides(CharSequence charSequence, String str, String str2, CharSequence charSequence2, String str3, String str4, String str5, CharSequence charSequence3, CharSequence charSequence4, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(charSequence, (i & 2) != 0 ? null : str, (i & 4) != 0 ? null : str2, (i & 8) != 0 ? null : charSequence2, (i & 16) != 0 ? null : str3, (i & 32) != 0 ? null : str4, (i & 64) != 0 ? null : str5, (i & 128) != 0 ? null : charSequence3, (i & 256) == 0 ? charSequence4 : null);
    }
}
