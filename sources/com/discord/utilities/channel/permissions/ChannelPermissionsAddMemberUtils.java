package com.discord.utilities.channel.permissions;

import andhook.lib.HookHelper;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.permission.PermissionOverwrite;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreStream;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import d0.z.d.m;
import j0.l.a.q;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import rx.Observable;
/* compiled from: ChannelPermissionsAddMemberUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000f\u0010\u0010JG\u0010\r\u001a\u0010\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\f0\u000b0\n2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00060\u00052\n\u0010\t\u001a\u00060\u0002j\u0002`\b¢\u0006\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lcom/discord/utilities/channel/permissions/ChannelPermissionsAddMemberUtils;", "", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "Lcom/discord/api/permission/PermissionOverwrite$Type;", "selectedItems", "Lcom/discord/api/permission/PermissionBit;", "permission", "Lrx/Observable;", "", "Ljava/lang/Void;", "addPermissionOverwrites", "(JLjava/util/Map;J)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ChannelPermissionsAddMemberUtils {
    public static final ChannelPermissionsAddMemberUtils INSTANCE = new ChannelPermissionsAddMemberUtils();

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            PermissionOverwrite.Type.values();
            int[] iArr = new int[2];
            $EnumSwitchMapping$0 = iArr;
            iArr[PermissionOverwrite.Type.ROLE.ordinal()] = 1;
            iArr[PermissionOverwrite.Type.MEMBER.ordinal()] = 2;
        }
    }

    private ChannelPermissionsAddMemberUtils() {
    }

    public final Observable<List<Void>> addPermissionOverwrites(long j, Map<Long, ? extends PermissionOverwrite.Type> map, long j2) {
        RestAPIParams.ChannelPermissionOverwrites channelPermissionOverwrites;
        m.checkNotNullParameter(map, "selectedItems");
        Channel channel = StoreStream.Companion.getChannels().getChannel(j);
        ArrayList arrayList = new ArrayList(map.size());
        for (Map.Entry<Long, ? extends PermissionOverwrite.Type> entry : map.entrySet()) {
            long longValue = entry.getKey().longValue();
            PermissionOverwrite.Type value = entry.getValue();
            PermissionOverwrite f = channel != null ? ChannelUtils.f(channel, longValue) : null;
            long j3 = 0;
            long c = f != null ? f.c() : 0L;
            if (f != null) {
                j3 = f.d();
            }
            int ordinal = value.ordinal();
            if (ordinal == 0) {
                channelPermissionOverwrites = RestAPIParams.ChannelPermissionOverwrites.Companion.createForRole(longValue, Long.valueOf(c | j2), Long.valueOf((~j2) & j3));
            } else if (ordinal == 1) {
                channelPermissionOverwrites = RestAPIParams.ChannelPermissionOverwrites.Companion.createForMember(longValue, Long.valueOf(c | j2), Long.valueOf((~j2) & j3));
            } else {
                throw new NoWhenBranchMatchedException();
            }
            arrayList.add(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().updatePermissionOverwrites(j, longValue, channelPermissionOverwrites), false, 1, null));
        }
        Observable<List<Void>> f02 = Observable.l(Observable.h0(new q(arrayList))).f0();
        m.checkNotNullExpressionValue(f02, "Observable.concat(overwr…quests)\n        .toList()");
        return f02;
    }
}
