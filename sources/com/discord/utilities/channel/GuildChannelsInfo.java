package com.discord.utilities.channel;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.channel.ChannelUtils$getSortByNameAndType$1;
import com.discord.api.role.GuildRole;
import com.discord.models.domain.ModelNotificationSettings;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreStream;
import com.discord.utilities.permissions.ManageGuildContext;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rx.ObservableWithLeadingEdgeThrottle;
import d0.t.n;
import d0.t.r;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: GuildChannelsInfo.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0019\b\u0086\b\u0018\u0000 M2\u00020\u0001:\u0001MB\u0089\u0001\u0012\b\u0010%\u001a\u0004\u0018\u00010\r\u0012\b\u0010&\u001a\u0004\u0018\u00010\u0010\u0012\u0006\u0010'\u001a\u00020\u0013\u0012\u0006\u0010(\u001a\u00020\u0016\u0012\u001a\u0010)\u001a\u0016\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\b\u0012\u00060\u0003j\u0002`\u00190\u0002\u0012\u0006\u0010*\u001a\u00020\u0016\u0012\u0006\u0010+\u001a\u00020\u0016\u0012\u0006\u0010,\u001a\u00020\u0016\u0012\u0006\u0010-\u001a\u00020\u001f\u0012\u0006\u0010.\u001a\u00020\u0016\u0012\u0018\u0010/\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002`#\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u0002¢\u0006\u0004\bK\u0010LJ+\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u00072\u0016\u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0002¢\u0006\u0004\b\b\u0010\tJ=\u0010\u000b\u001a\u0018\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\n0\u00022\u0018\u0010\u0006\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0012\u0010\u000e\u001a\u0004\u0018\u00010\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0017\u001a\u00020\u0016HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0018J$\u0010\u001a\u001a\u0016\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\b\u0012\u00060\u0003j\u0002`\u00190\u0002HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u001c\u001a\u00020\u0016HÆ\u0003¢\u0006\u0004\b\u001c\u0010\u0018J\u0010\u0010\u001d\u001a\u00020\u0016HÆ\u0003¢\u0006\u0004\b\u001d\u0010\u0018J\u0010\u0010\u001e\u001a\u00020\u0016HÆ\u0003¢\u0006\u0004\b\u001e\u0010\u0018J\u0010\u0010 \u001a\u00020\u001fHÆ\u0003¢\u0006\u0004\b \u0010!J\u0010\u0010\"\u001a\u00020\u0016HÆ\u0003¢\u0006\u0004\b\"\u0010\u0018J\"\u0010$\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002`#\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b$\u0010\u001bJ¨\u0001\u00100\u001a\u00020\u00002\n\b\u0002\u0010%\u001a\u0004\u0018\u00010\r2\n\b\u0002\u0010&\u001a\u0004\u0018\u00010\u00102\b\b\u0002\u0010'\u001a\u00020\u00132\b\b\u0002\u0010(\u001a\u00020\u00162\u001c\b\u0002\u0010)\u001a\u0016\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\b\u0012\u00060\u0003j\u0002`\u00190\u00022\b\b\u0002\u0010*\u001a\u00020\u00162\b\b\u0002\u0010+\u001a\u00020\u00162\b\b\u0002\u0010,\u001a\u00020\u00162\b\b\u0002\u0010-\u001a\u00020\u001f2\b\b\u0002\u0010.\u001a\u00020\u00162\u001a\b\u0002\u0010/\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002`#\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b0\u00101J\u0010\u00103\u001a\u000202HÖ\u0001¢\u0006\u0004\b3\u00104J\u0010\u00106\u001a\u000205HÖ\u0001¢\u0006\u0004\b6\u00107J\u001a\u00109\u001a\u00020\u00162\b\u00108\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b9\u0010:R\u0019\u0010+\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010;\u001a\u0004\b<\u0010\u0018R\u0019\u0010,\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010;\u001a\u0004\b,\u0010\u0018R+\u0010/\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002`#\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010=\u001a\u0004\b>\u0010\u001bR\u0019\u0010.\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010;\u001a\u0004\b?\u0010\u0018R\u0019\u0010(\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010;\u001a\u0004\b@\u0010\u0018R\u0019\u0010-\u001a\u00020\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010A\u001a\u0004\bB\u0010!R\u0019\u0010*\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010;\u001a\u0004\bC\u0010\u0018R\u001b\u0010%\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010D\u001a\u0004\bE\u0010\u000fR\u0019\u0010'\u001a\u00020\u00138\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010F\u001a\u0004\bG\u0010\u0015R-\u0010)\u001a\u0016\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\b\u0012\u00060\u0003j\u0002`\u00190\u00028\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010=\u001a\u0004\bH\u0010\u001bR\u001b\u0010&\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010I\u001a\u0004\bJ\u0010\u0012¨\u0006N"}, d2 = {"Lcom/discord/utilities/channel/GuildChannelsInfo;", "", "", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "guildChannels", "", "getSortedVisibleChannels", "(Ljava/util/Map;)Ljava/util/List;", "", "getSortedCategories", "(Ljava/util/Map;)Ljava/util/Map;", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/api/role/GuildRole;", "component2", "()Lcom/discord/api/role/GuildRole;", "Lcom/discord/models/domain/ModelNotificationSettings;", "component3", "()Lcom/discord/models/domain/ModelNotificationSettings;", "", "component4", "()Z", "Lcom/discord/api/permission/PermissionBit;", "component5", "()Ljava/util/Map;", "component6", "component7", "component8", "Lcom/discord/utilities/permissions/ManageGuildContext;", "component9", "()Lcom/discord/utilities/permissions/ManageGuildContext;", "component10", "Lcom/discord/primitives/RoleId;", "component11", "guild", "everyoneRole", "notificationSettings", "hideMutedChannels", "channelPermissions", "ableToInstantInvite", "unelevated", "isVerifiedServer", "manageGuildContext", "canChangeNickname", "guildRoles", "copy", "(Lcom/discord/models/guild/Guild;Lcom/discord/api/role/GuildRole;Lcom/discord/models/domain/ModelNotificationSettings;ZLjava/util/Map;ZZZLcom/discord/utilities/permissions/ManageGuildContext;ZLjava/util/Map;)Lcom/discord/utilities/channel/GuildChannelsInfo;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getUnelevated", "Ljava/util/Map;", "getGuildRoles", "getCanChangeNickname", "getHideMutedChannels", "Lcom/discord/utilities/permissions/ManageGuildContext;", "getManageGuildContext", "getAbleToInstantInvite", "Lcom/discord/models/guild/Guild;", "getGuild", "Lcom/discord/models/domain/ModelNotificationSettings;", "getNotificationSettings", "getChannelPermissions", "Lcom/discord/api/role/GuildRole;", "getEveryoneRole", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Lcom/discord/api/role/GuildRole;Lcom/discord/models/domain/ModelNotificationSettings;ZLjava/util/Map;ZZZLcom/discord/utilities/permissions/ManageGuildContext;ZLjava/util/Map;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildChannelsInfo {
    public static final Companion Companion = new Companion(null);
    private final boolean ableToInstantInvite;
    private final boolean canChangeNickname;
    private final Map<Long, Long> channelPermissions;
    private final GuildRole everyoneRole;
    private final Guild guild;
    private final Map<Long, GuildRole> guildRoles;
    private final boolean hideMutedChannels;
    private final boolean isVerifiedServer;
    private final ManageGuildContext manageGuildContext;
    private final ModelNotificationSettings notificationSettings;
    private final boolean unelevated;

    /* compiled from: GuildChannelsInfo.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/utilities/channel/GuildChannelsInfo$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lrx/Observable;", "Lcom/discord/utilities/channel/GuildChannelsInfo;", "get", "(J)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public final Observable<GuildChannelsInfo> get(long j) {
            StoreStream.Companion companion = StoreStream.Companion;
            Observable<GuildChannelsInfo> q = ObservableWithLeadingEdgeThrottle.combineLatest(companion.getUsers().observeMe(true), companion.getGuilds().observeGuild(j), companion.getUserGuildSettings().observeGuildSettings(j), companion.getUserGuildSettings().observeHideMutedChannels(j), companion.getGuilds().observeRoles(j), companion.getPermissions().observePermissionsForGuild(j), companion.getPermissions().observeChannelPermissionsForGuild(j), companion.getChannels().observeChannelCategories(j), GuildChannelsInfo$Companion$get$1.INSTANCE, 500L, TimeUnit.MILLISECONDS).q();
            m.checkNotNullExpressionValue(q, "ObservableWithLeadingEdg…  .distinctUntilChanged()");
            return q;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public GuildChannelsInfo(Guild guild, GuildRole guildRole, ModelNotificationSettings modelNotificationSettings, boolean z2, Map<Long, Long> map, boolean z3, boolean z4, boolean z5, ManageGuildContext manageGuildContext, boolean z6, Map<Long, GuildRole> map2) {
        m.checkNotNullParameter(modelNotificationSettings, "notificationSettings");
        m.checkNotNullParameter(map, "channelPermissions");
        m.checkNotNullParameter(manageGuildContext, "manageGuildContext");
        this.guild = guild;
        this.everyoneRole = guildRole;
        this.notificationSettings = modelNotificationSettings;
        this.hideMutedChannels = z2;
        this.channelPermissions = map;
        this.ableToInstantInvite = z3;
        this.unelevated = z4;
        this.isVerifiedServer = z5;
        this.manageGuildContext = manageGuildContext;
        this.canChangeNickname = z6;
        this.guildRoles = map2;
    }

    public final Guild component1() {
        return this.guild;
    }

    public final boolean component10() {
        return this.canChangeNickname;
    }

    public final Map<Long, GuildRole> component11() {
        return this.guildRoles;
    }

    public final GuildRole component2() {
        return this.everyoneRole;
    }

    public final ModelNotificationSettings component3() {
        return this.notificationSettings;
    }

    public final boolean component4() {
        return this.hideMutedChannels;
    }

    public final Map<Long, Long> component5() {
        return this.channelPermissions;
    }

    public final boolean component6() {
        return this.ableToInstantInvite;
    }

    public final boolean component7() {
        return this.unelevated;
    }

    public final boolean component8() {
        return this.isVerifiedServer;
    }

    public final ManageGuildContext component9() {
        return this.manageGuildContext;
    }

    public final GuildChannelsInfo copy(Guild guild, GuildRole guildRole, ModelNotificationSettings modelNotificationSettings, boolean z2, Map<Long, Long> map, boolean z3, boolean z4, boolean z5, ManageGuildContext manageGuildContext, boolean z6, Map<Long, GuildRole> map2) {
        m.checkNotNullParameter(modelNotificationSettings, "notificationSettings");
        m.checkNotNullParameter(map, "channelPermissions");
        m.checkNotNullParameter(manageGuildContext, "manageGuildContext");
        return new GuildChannelsInfo(guild, guildRole, modelNotificationSettings, z2, map, z3, z4, z5, manageGuildContext, z6, map2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildChannelsInfo)) {
            return false;
        }
        GuildChannelsInfo guildChannelsInfo = (GuildChannelsInfo) obj;
        return m.areEqual(this.guild, guildChannelsInfo.guild) && m.areEqual(this.everyoneRole, guildChannelsInfo.everyoneRole) && m.areEqual(this.notificationSettings, guildChannelsInfo.notificationSettings) && this.hideMutedChannels == guildChannelsInfo.hideMutedChannels && m.areEqual(this.channelPermissions, guildChannelsInfo.channelPermissions) && this.ableToInstantInvite == guildChannelsInfo.ableToInstantInvite && this.unelevated == guildChannelsInfo.unelevated && this.isVerifiedServer == guildChannelsInfo.isVerifiedServer && m.areEqual(this.manageGuildContext, guildChannelsInfo.manageGuildContext) && this.canChangeNickname == guildChannelsInfo.canChangeNickname && m.areEqual(this.guildRoles, guildChannelsInfo.guildRoles);
    }

    public final boolean getAbleToInstantInvite() {
        return this.ableToInstantInvite;
    }

    public final boolean getCanChangeNickname() {
        return this.canChangeNickname;
    }

    public final Map<Long, Long> getChannelPermissions() {
        return this.channelPermissions;
    }

    public final GuildRole getEveryoneRole() {
        return this.everyoneRole;
    }

    public final Guild getGuild() {
        return this.guild;
    }

    public final Map<Long, GuildRole> getGuildRoles() {
        return this.guildRoles;
    }

    public final boolean getHideMutedChannels() {
        return this.hideMutedChannels;
    }

    public final ManageGuildContext getManageGuildContext() {
        return this.manageGuildContext;
    }

    public final ModelNotificationSettings getNotificationSettings() {
        return this.notificationSettings;
    }

    public final Map<Long, Collection<Channel>> getSortedCategories(final Map<Long, Channel> map) {
        m.checkNotNullParameter(map, "guildChannels");
        TreeMap treeMap = new TreeMap(new Comparator<Long>() { // from class: com.discord.utilities.channel.GuildChannelsInfo$getSortedCategories$sortedCategories$1
            /* JADX WARN: Multi-variable type inference failed */
            public final int compare(Long l, Long l2) {
                return ((ChannelUtils$getSortByNameAndType$1) ChannelUtils.h(Channel.Companion)).compare(map.get(l), map.get(l2));
            }
        });
        for (Channel channel : u.filterNotNull(map.values())) {
            Long valueOf = Long.valueOf(ChannelUtils.k(channel) ? channel.h() : channel.r());
            Object obj = treeMap.get(valueOf);
            if (obj == null) {
                obj = new TreeSet(ChannelUtils.h(Channel.Companion));
                treeMap.put(valueOf, obj);
            }
            ((Set) obj).add(channel);
        }
        return treeMap;
    }

    public final List<Channel> getSortedVisibleChannels(Map<Long, Channel> map) {
        m.checkNotNullParameter(map, "guildChannels");
        Map<Long, Collection<Channel>> sortedCategories = getSortedCategories(map);
        ArrayList arrayList = new ArrayList();
        for (Map.Entry<Long, Collection<Channel>> entry : sortedCategories.entrySet()) {
            List arrayList2 = new ArrayList();
            for (Object obj : entry.getValue()) {
                Channel channel = (Channel) obj;
                if (PermissionUtils.INSTANCE.hasAccess(channel, (Long) a.c(channel, this.channelPermissions))) {
                    arrayList2.add(obj);
                }
            }
            if (arrayList2.size() == 1) {
                arrayList2 = n.emptyList();
            }
            r.addAll(arrayList, arrayList2);
        }
        return arrayList;
    }

    public final boolean getUnelevated() {
        return this.unelevated;
    }

    public int hashCode() {
        Guild guild = this.guild;
        int i = 0;
        int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
        GuildRole guildRole = this.everyoneRole;
        int hashCode2 = (hashCode + (guildRole != null ? guildRole.hashCode() : 0)) * 31;
        ModelNotificationSettings modelNotificationSettings = this.notificationSettings;
        int hashCode3 = (hashCode2 + (modelNotificationSettings != null ? modelNotificationSettings.hashCode() : 0)) * 31;
        boolean z2 = this.hideMutedChannels;
        int i2 = 1;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        int i5 = (hashCode3 + i3) * 31;
        Map<Long, Long> map = this.channelPermissions;
        int hashCode4 = (i5 + (map != null ? map.hashCode() : 0)) * 31;
        boolean z3 = this.ableToInstantInvite;
        if (z3) {
            z3 = true;
        }
        int i6 = z3 ? 1 : 0;
        int i7 = z3 ? 1 : 0;
        int i8 = (hashCode4 + i6) * 31;
        boolean z4 = this.unelevated;
        if (z4) {
            z4 = true;
        }
        int i9 = z4 ? 1 : 0;
        int i10 = z4 ? 1 : 0;
        int i11 = (i8 + i9) * 31;
        boolean z5 = this.isVerifiedServer;
        if (z5) {
            z5 = true;
        }
        int i12 = z5 ? 1 : 0;
        int i13 = z5 ? 1 : 0;
        int i14 = (i11 + i12) * 31;
        ManageGuildContext manageGuildContext = this.manageGuildContext;
        int hashCode5 = (i14 + (manageGuildContext != null ? manageGuildContext.hashCode() : 0)) * 31;
        boolean z6 = this.canChangeNickname;
        if (!z6) {
            i2 = z6 ? 1 : 0;
        }
        int i15 = (hashCode5 + i2) * 31;
        Map<Long, GuildRole> map2 = this.guildRoles;
        if (map2 != null) {
            i = map2.hashCode();
        }
        return i15 + i;
    }

    public final boolean isVerifiedServer() {
        return this.isVerifiedServer;
    }

    public String toString() {
        StringBuilder R = a.R("GuildChannelsInfo(guild=");
        R.append(this.guild);
        R.append(", everyoneRole=");
        R.append(this.everyoneRole);
        R.append(", notificationSettings=");
        R.append(this.notificationSettings);
        R.append(", hideMutedChannels=");
        R.append(this.hideMutedChannels);
        R.append(", channelPermissions=");
        R.append(this.channelPermissions);
        R.append(", ableToInstantInvite=");
        R.append(this.ableToInstantInvite);
        R.append(", unelevated=");
        R.append(this.unelevated);
        R.append(", isVerifiedServer=");
        R.append(this.isVerifiedServer);
        R.append(", manageGuildContext=");
        R.append(this.manageGuildContext);
        R.append(", canChangeNickname=");
        R.append(this.canChangeNickname);
        R.append(", guildRoles=");
        return a.L(R, this.guildRoles, ")");
    }
}
