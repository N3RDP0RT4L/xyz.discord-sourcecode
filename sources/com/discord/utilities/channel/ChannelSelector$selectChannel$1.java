package com.discord.utilities.channel;

import com.discord.stores.SelectedChannelAnalyticsLocation;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: ChannelSelector.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u0001\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "it", "", "invoke", "(Ljava/lang/Void;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ChannelSelector$selectChannel$1 extends o implements Function1 {
    public final /* synthetic */ SelectedChannelAnalyticsLocation $analyticsLocation;
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ Long $peekParent;
    public final /* synthetic */ ChannelSelector this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ChannelSelector$selectChannel$1(ChannelSelector channelSelector, long j, long j2, Long l, SelectedChannelAnalyticsLocation selectedChannelAnalyticsLocation) {
        super(1);
        this.this$0 = channelSelector;
        this.$guildId = j;
        this.$channelId = j2;
        this.$peekParent = l;
        this.$analyticsLocation = selectedChannelAnalyticsLocation;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Void) obj);
        return Unit.a;
    }

    public final void invoke(Void r8) {
        this.this$0.gotoChannel(this.$guildId, this.$channelId, this.$peekParent, this.$analyticsLocation);
    }
}
