package com.discord.utilities.channel;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.discord.api.channel.Channel;
import com.discord.widgets.channels.WidgetGroupInviteFriends;
import com.discord.widgets.channels.invite.GroupInviteFriendsSheet;
import com.discord.widgets.channels.invite.GroupInviteFriendsSheetFeatureFlag;
import com.discord.widgets.guilds.invite.WidgetGuildInviteShare;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ChannelInviteLaunchUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ?\u0010\f\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u00062\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\b\f\u0010\r¨\u0006\u0010"}, d2 = {"Lcom/discord/utilities/channel/ChannelInviteLaunchUtils;", "", "Landroidx/fragment/app/Fragment;", "fragment", "Lcom/discord/api/channel/Channel;", "channel", "", "source", "", "eventId", "inviteStoreKey", "", "inviteToChannel", "(Landroidx/fragment/app/Fragment;Lcom/discord/api/channel/Channel;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ChannelInviteLaunchUtils {
    public static final ChannelInviteLaunchUtils INSTANCE = new ChannelInviteLaunchUtils();

    private ChannelInviteLaunchUtils() {
    }

    public static /* synthetic */ void inviteToChannel$default(ChannelInviteLaunchUtils channelInviteLaunchUtils, Fragment fragment, Channel channel, String str, Long l, String str2, int i, Object obj) {
        channelInviteLaunchUtils.inviteToChannel(fragment, channel, str, (i & 8) != 0 ? null : l, (i & 16) != 0 ? null : str2);
    }

    public final void inviteToChannel(Fragment fragment, Channel channel, String str, Long l, String str2) {
        m.checkNotNullParameter(fragment, "fragment");
        m.checkNotNullParameter(str, "source");
        if (channel != null) {
            long h = channel.h();
            if (channel.f() != 0) {
                WidgetGuildInviteShare.Companion companion = WidgetGuildInviteShare.Companion;
                Context requireContext = fragment.requireContext();
                m.checkNotNullExpressionValue(requireContext, "fragment.requireContext()");
                FragmentManager childFragmentManager = fragment.getChildFragmentManager();
                m.checkNotNullExpressionValue(childFragmentManager, "fragment.childFragmentManager");
                companion.launch(requireContext, childFragmentManager, channel.f(), (r22 & 8) != 0 ? null : Long.valueOf(h), (r22 & 16) != 0 ? false : false, (r22 & 32) != 0 ? null : l, (r22 & 64) != 0 ? null : str2, str);
            } else if (GroupInviteFriendsSheetFeatureFlag.Companion.getINSTANCE().isEnabled()) {
                GroupInviteFriendsSheet.Companion companion2 = GroupInviteFriendsSheet.Companion;
                FragmentManager childFragmentManager2 = fragment.getChildFragmentManager();
                m.checkNotNullExpressionValue(childFragmentManager2, "fragment.childFragmentManager");
                companion2.show(childFragmentManager2, h, str);
            } else {
                WidgetGroupInviteFriends.Companion companion3 = WidgetGroupInviteFriends.Companion;
                Context requireContext2 = fragment.requireContext();
                m.checkNotNullExpressionValue(requireContext2, "fragment.requireContext()");
                companion3.launch(requireContext2, h, str);
            }
        }
    }
}
