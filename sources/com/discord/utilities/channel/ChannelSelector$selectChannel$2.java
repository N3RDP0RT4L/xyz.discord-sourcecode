package com.discord.utilities.channel;

import com.discord.api.channel.Channel;
import com.discord.stores.SelectedChannelAnalyticsLocation;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: ChannelSelector.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/api/channel/Channel;", "kotlin.jvm.PlatformType", "channel", "", "invoke", "(Lcom/discord/api/channel/Channel;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ChannelSelector$selectChannel$2 extends o implements Function1<Channel, Unit> {
    public final /* synthetic */ SelectedChannelAnalyticsLocation $analyticsLocation;
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ Long $peekParent;
    public final /* synthetic */ ChannelSelector this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ChannelSelector$selectChannel$2(ChannelSelector channelSelector, long j, long j2, Long l, SelectedChannelAnalyticsLocation selectedChannelAnalyticsLocation) {
        super(1);
        this.this$0 = channelSelector;
        this.$guildId = j;
        this.$channelId = j2;
        this.$peekParent = l;
        this.$analyticsLocation = selectedChannelAnalyticsLocation;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Channel channel) {
        invoke2(channel);
        return Unit.a;
    }

    /* JADX WARN: Code restructure failed: missing block: B:5:0x001d, code lost:
        if (com.discord.api.channel.ChannelUtils.E(r10) == false) goto L6;
     */
    /* JADX WARN: Removed duplicated region for block: B:11:0x002d  */
    /* JADX WARN: Removed duplicated region for block: B:12:0x0030  */
    /* JADX WARN: Removed duplicated region for block: B:15:0x0040  */
    /* JADX WARN: Removed duplicated region for block: B:17:? A[RETURN, SYNTHETIC] */
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void invoke2(com.discord.api.channel.Channel r10) {
        /*
            r9 = this;
            com.discord.widgets.chat.list.TextInVoiceFeatureFlag$Companion r0 = com.discord.widgets.chat.list.TextInVoiceFeatureFlag.Companion
            com.discord.widgets.chat.list.TextInVoiceFeatureFlag r0 = r0.getINSTANCE()
            long r1 = r10.f()
            java.lang.Long r1 = java.lang.Long.valueOf(r1)
            boolean r0 = r0.isEnabled(r1)
            java.lang.String r1 = "channel"
            if (r0 != 0) goto L1f
            d0.z.d.m.checkNotNullExpressionValue(r10, r1)
            boolean r0 = com.discord.api.channel.ChannelUtils.E(r10)
            if (r0 != 0) goto L28
        L1f:
            d0.z.d.m.checkNotNullExpressionValue(r10, r1)
            boolean r10 = com.discord.api.channel.ChannelUtils.z(r10)
            if (r10 == 0) goto L2a
        L28:
            r10 = 1
            goto L2b
        L2a:
            r10 = 0
        L2b:
            if (r10 == 0) goto L30
            long r0 = r9.$guildId
            goto L32
        L30:
            long r0 = r9.$channelId
        L32:
            r5 = r0
            com.discord.utilities.channel.ChannelSelector r2 = r9.this$0
            long r3 = r9.$guildId
            java.lang.Long r7 = r9.$peekParent
            com.discord.stores.SelectedChannelAnalyticsLocation r8 = r9.$analyticsLocation
            com.discord.utilities.channel.ChannelSelector.access$gotoChannel(r2, r3, r5, r7, r8)
            if (r10 == 0) goto L4f
            com.discord.utilities.channel.ChannelSelector r10 = r9.this$0
            com.discord.stores.StoreStream r10 = r10.getStream()
            com.discord.stores.StoreVoiceChannelSelected r10 = r10.getVoiceChannelSelected$app_productionGoogleRelease()
            long r0 = r9.$channelId
            r10.selectVoiceChannel(r0)
        L4f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.channel.ChannelSelector$selectChannel$2.invoke2(com.discord.api.channel.Channel):void");
    }
}
