package com.discord.utilities.channel;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.fragment.app.FragmentManager;
import com.discord.api.channel.Channel;
import com.discord.stores.Dispatcher;
import com.discord.stores.SelectedChannelAnalyticsLocation;
import com.discord.stores.StoreNavigation;
import com.discord.stores.StoreStream;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$1;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$2;
import d0.z.d.m;
import j0.l.e.k;
import java.lang.ref.WeakReference;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: ChannelSelector.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 72\u00020\u0001:\u00017B\u001f\u0012\u0006\u0010'\u001a\u00020&\u0012\u0006\u00101\u001a\u000200\u0012\u0006\u0010,\u001a\u00020+¢\u0006\u0004\b5\u00106JA\u0010\u000b\u001a\u00020\n2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u00052\u000e\u0010\u0007\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00052\b\u0010\t\u001a\u0004\u0018\u00010\bH\u0002¢\u0006\u0004\b\u000b\u0010\fJ5\u0010\u000f\u001a\u00020\n2\b\u0010\u000e\u001a\u0004\u0018\u00010\r2\u0010\b\u0002\u0010\u0007\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00052\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\b¢\u0006\u0004\b\u000f\u0010\u0010JC\u0010\u000f\u001a\u00020\n2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u00052\u0010\b\u0002\u0010\u0007\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00052\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\b¢\u0006\u0004\b\u000f\u0010\fJ\u0019\u0010\u0011\u001a\u00020\n2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0011\u0010\u0012J#\u0010\u0017\u001a\u00020\n2\u0006\u0010\u0014\u001a\u00020\u00132\f\b\u0002\u0010\u0016\u001a\u00060\u0002j\u0002`\u0015¢\u0006\u0004\b\u0017\u0010\u0018J#\u0010\u0019\u001a\u00020\n2\b\u0010\u0014\u001a\u0004\u0018\u00010\u00132\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u0005¢\u0006\u0004\b\u0019\u0010\u0018JA\u0010\u001e\u001a\u00020\n2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u00052\u0010\b\u0002\u0010\u001b\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u001a2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001c¢\u0006\u0004\b\u001e\u0010\u001fJ\r\u0010 \u001a\u00020\n¢\u0006\u0004\b \u0010!J!\u0010$\u001a\u00020\n2\u0006\u0010#\u001a\u00020\"2\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u0005¢\u0006\u0004\b$\u0010%R\u0019\u0010'\u001a\u00020&8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*R\u0019\u0010,\u001a\u00020+8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010-\u001a\u0004\b.\u0010/R\u0019\u00101\u001a\u0002008\u0006@\u0006¢\u0006\f\n\u0004\b1\u00102\u001a\u0004\b3\u00104¨\u00068"}, d2 = {"Lcom/discord/utilities/channel/ChannelSelector;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/ChannelId;", "channelId", "peekParent", "Lcom/discord/stores/SelectedChannelAnalyticsLocation;", "analyticsLocation", "", "gotoChannel", "(JJLjava/lang/Long;Lcom/discord/stores/SelectedChannelAnalyticsLocation;)V", "Lcom/discord/api/channel/Channel;", "channel", "selectChannel", "(Lcom/discord/api/channel/Channel;Ljava/lang/Long;Lcom/discord/stores/SelectedChannelAnalyticsLocation;)V", "selectPreviousChannel", "(J)V", "Landroid/content/Context;", "context", "Lcom/discord/primitives/UserId;", "userId", "findAndSetDirectMessage", "(Landroid/content/Context;J)V", "findAndSet", "Lcom/discord/primitives/MessageId;", "parentMessageId", "", "startThreadLocation", "openCreateThread", "(JJLjava/lang/Long;Ljava/lang/String;)V", "dismissCreateThread", "()V", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "previewVoiceChannel", "(Landroidx/fragment/app/FragmentManager;J)V", "Lcom/discord/stores/StoreStream;", "stream", "Lcom/discord/stores/StoreStream;", "getStream", "()Lcom/discord/stores/StoreStream;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "getObservationDeck", "()Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "getDispatcher", "()Lcom/discord/stores/Dispatcher;", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ChannelSelector {
    public static final Companion Companion = new Companion(null);
    private static ChannelSelector INSTANCE;
    private final Dispatcher dispatcher;
    private final ObservationDeck observationDeck;
    private final StoreStream stream;

    /* compiled from: ChannelSelector.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u000f\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0003\u0010\u0004J%\u0010\f\u001a\u00020\u000b2\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t¢\u0006\u0004\b\f\u0010\rR\u0016\u0010\u000e\u001a\u00020\u00028\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u000e\u0010\u000f¨\u0006\u0012"}, d2 = {"Lcom/discord/utilities/channel/ChannelSelector$Companion;", "", "Lcom/discord/utilities/channel/ChannelSelector;", "getInstance", "()Lcom/discord/utilities/channel/ChannelSelector;", "Lcom/discord/stores/StoreStream;", "stream", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "", "init", "(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V", "INSTANCE", "Lcom/discord/utilities/channel/ChannelSelector;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public final ChannelSelector getInstance() {
            ChannelSelector channelSelector = ChannelSelector.INSTANCE;
            if (channelSelector == null) {
                m.throwUninitializedPropertyAccessException("INSTANCE");
            }
            return channelSelector;
        }

        public final void init(StoreStream storeStream, Dispatcher dispatcher, ObservationDeck observationDeck) {
            m.checkNotNullParameter(storeStream, "stream");
            m.checkNotNullParameter(dispatcher, "dispatcher");
            m.checkNotNullParameter(observationDeck, "observationDeck");
            ChannelSelector.INSTANCE = new ChannelSelector(storeStream, dispatcher, observationDeck);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public ChannelSelector(StoreStream storeStream, Dispatcher dispatcher, ObservationDeck observationDeck) {
        m.checkNotNullParameter(storeStream, "stream");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.stream = storeStream;
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
    }

    public static /* synthetic */ void findAndSetDirectMessage$default(ChannelSelector channelSelector, Context context, long j, int i, Object obj) {
        if ((i & 2) != 0) {
            j = 0;
        }
        channelSelector.findAndSetDirectMessage(context, j);
    }

    public static final ChannelSelector getInstance() {
        return Companion.getInstance();
    }

    public final void gotoChannel(long j, long j2, Long l, SelectedChannelAnalyticsLocation selectedChannelAnalyticsLocation) {
        this.dispatcher.schedule(new ChannelSelector$gotoChannel$1(this, j, j2, l, selectedChannelAnalyticsLocation));
    }

    public static /* synthetic */ void openCreateThread$default(ChannelSelector channelSelector, long j, long j2, Long l, String str, int i, Object obj) {
        if ((i & 4) != 0) {
            l = null;
        }
        channelSelector.openCreateThread(j, j2, l, str);
    }

    public static /* synthetic */ void selectChannel$default(ChannelSelector channelSelector, Channel channel, Long l, SelectedChannelAnalyticsLocation selectedChannelAnalyticsLocation, int i, Object obj) {
        if ((i & 2) != 0) {
            l = null;
        }
        if ((i & 4) != 0) {
            selectedChannelAnalyticsLocation = null;
        }
        channelSelector.selectChannel(channel, l, selectedChannelAnalyticsLocation);
    }

    public final void dismissCreateThread() {
        this.dispatcher.schedule(new ChannelSelector$dismissCreateThread$1(this));
    }

    public final void findAndSet(Context context, long j) {
        if (j > 0) {
            Observable x2 = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this.stream.getChannels$app_productionGoogleRelease(), this.stream.getPermissions$app_productionGoogleRelease()}, false, null, null, new ChannelSelector$findAndSet$1(this, j, context), 14, null).x(ChannelSelector$findAndSet$2.INSTANCE);
            m.checkNotNullExpressionValue(x2, "observationDeck\n        …   .filter { it != null }");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.computationLatest(ObservableExtensionsKt.takeSingleUntilTimeout$default(x2, 0L, true, 1, null)), ChannelSelector.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new ChannelSelector$findAndSet$3(this));
        }
    }

    public final void findAndSetDirectMessage(Context context, long j) {
        m.checkNotNullParameter(context, "context");
        if (j > 0) {
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(ObservableExtensionsKt.computationBuffered(RestAPI.Companion.getApi().createOrFetchDM(j))), ChannelSelector.class, (r18 & 2) != 0 ? null : context, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new ChannelSelector$findAndSetDirectMessage$1(this, context));
        }
    }

    public final Dispatcher getDispatcher() {
        return this.dispatcher;
    }

    public final ObservationDeck getObservationDeck() {
        return this.observationDeck;
    }

    public final StoreStream getStream() {
        return this.stream;
    }

    public final void openCreateThread(long j, long j2, Long l, String str) {
        this.dispatcher.schedule(new ChannelSelector$openCreateThread$1(this, j, j2, l, str));
    }

    public final void previewVoiceChannel(FragmentManager fragmentManager, long j) {
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        WeakReference weakReference = new WeakReference(fragmentManager);
        Observable F = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this.stream.getChannels$app_productionGoogleRelease(), this.stream.getPermissions$app_productionGoogleRelease()}, false, null, null, new ChannelSelector$previewVoiceChannel$1(this, j), 14, null).x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
        m.checkNotNullExpressionValue(F, "filter { it != null }.map { it!! }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(ObservableExtensionsKt.takeSingleUntilTimeout$default(F, 0L, false, 3, null)), ChannelSelector.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new ChannelSelector$previewVoiceChannel$2(weakReference, j));
    }

    public final void selectChannel(Channel channel, Long l, SelectedChannelAnalyticsLocation selectedChannelAnalyticsLocation) {
        if (channel != null) {
            selectChannel(channel.f(), channel.h(), l, selectedChannelAnalyticsLocation);
        }
    }

    public final void selectPreviousChannel(long j) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.takeSingleUntilTimeout$default(this.stream.getChannelsSelected$app_productionGoogleRelease().observePreviousId(), 0L, false, 3, null), ChannelSelector.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new ChannelSelector$selectPreviousChannel$1(this, j));
    }

    public static /* synthetic */ void selectChannel$default(ChannelSelector channelSelector, long j, long j2, Long l, SelectedChannelAnalyticsLocation selectedChannelAnalyticsLocation, int i, Object obj) {
        channelSelector.selectChannel(j, j2, (i & 4) != 0 ? null : l, (i & 8) != 0 ? null : selectedChannelAnalyticsLocation);
    }

    public final void selectChannel(long j, long j2, Long l, SelectedChannelAnalyticsLocation selectedChannelAnalyticsLocation) {
        StoreNavigation.setNavigationPanelAction$default(this.stream.getNavigation$app_productionGoogleRelease(), StoreNavigation.PanelAction.CLOSE, null, 2, null);
        if (j == 0 || j2 == 0) {
            k kVar = new k(null);
            m.checkNotNullExpressionValue(kVar, "Observable.just(null)");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.computationLatest(kVar), ChannelSelector.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new ChannelSelector$selectChannel$1(this, j, j2, l, selectedChannelAnalyticsLocation));
            return;
        }
        Observable<R> F = this.stream.getChannels$app_productionGoogleRelease().observeChannel(j2).x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
        m.checkNotNullExpressionValue(F, "filter { it != null }.map { it!! }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.takeSingleUntilTimeout$default(F, 0L, false, 3, null), ChannelSelector.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new ChannelSelector$selectChannel$2(this, j, j2, l, selectedChannelAnalyticsLocation));
    }
}
