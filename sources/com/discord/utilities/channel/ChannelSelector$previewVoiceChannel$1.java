package com.discord.utilities.channel;

import com.discord.api.channel.Channel;
import com.discord.utilities.permissions.PermissionUtils;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: ChannelSelector.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/api/channel/Channel;", "invoke", "()Lcom/discord/api/channel/Channel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ChannelSelector$previewVoiceChannel$1 extends o implements Function0<Channel> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ ChannelSelector this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ChannelSelector$previewVoiceChannel$1(ChannelSelector channelSelector, long j) {
        super(0);
        this.this$0 = channelSelector;
        this.$channelId = j;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final Channel invoke() {
        Channel findChannelById = this.this$0.getStream().getChannels$app_productionGoogleRelease().findChannelById(this.$channelId);
        if (findChannelById != null && PermissionUtils.INSTANCE.hasAccess(findChannelById, this.this$0.getStream().getPermissions$app_productionGoogleRelease().getPermissionsByChannel().get(Long.valueOf(this.$channelId)))) {
            return findChannelById;
        }
        return null;
    }
}
