package com.discord.utilities.channel;

import androidx.annotation.DrawableRes;
import com.discord.api.channel.Channel;
import com.discord.api.permission.Permission;
import com.discord.api.permission.PermissionOverwrite;
import com.discord.utilities.PermissionOverwriteUtilsKt;
import com.discord.utilities.channel.GuildChannelIconType;
import d0.z.d.m;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import xyz.discord.R;
/* compiled from: GuildChannelIconUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\u001a\u0017\u0010\u0003\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000¢\u0006\u0004\b\u0003\u0010\u0004\u001a\u0017\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0007\u0010\b\u001a\u0017\u0010\t\u001a\u00020\u00062\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000¢\u0006\u0004\b\t\u0010\n¨\u0006\u000b"}, d2 = {"Lcom/discord/api/channel/Channel;", "channel", "Lcom/discord/utilities/channel/GuildChannelIconType;", "getChannelType", "(Lcom/discord/api/channel/Channel;)Lcom/discord/utilities/channel/GuildChannelIconType;", "guildChannelIconType", "", "mapGuildChannelTypeToIcon", "(Lcom/discord/utilities/channel/GuildChannelIconType;)I", "guildChannelIcon", "(Lcom/discord/api/channel/Channel;)I", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildChannelIconUtilsKt {
    public static final GuildChannelIconType getChannelType(Channel channel) {
        boolean z2;
        boolean z3;
        if (channel == null) {
            return GuildChannelIconType.Text.INSTANCE;
        }
        int A = channel.A();
        if (A == 2) {
            return channel.o() ? GuildChannelIconType.NSFW.Voice.INSTANCE : GuildChannelIconType.Voice.INSTANCE;
        }
        Object obj = null;
        if (A != 5) {
            if (A == 14) {
                return GuildChannelIconType.Directory.INSTANCE;
            }
            switch (A) {
                case 10:
                    return channel.o() ? GuildChannelIconType.NSFW.Thread.INSTANCE : GuildChannelIconType.Thread.INSTANCE;
                case 11:
                    return channel.o() ? GuildChannelIconType.NSFW.Thread.INSTANCE : GuildChannelIconType.Thread.INSTANCE;
                case 12:
                    return GuildChannelIconType.Locked.Thread.INSTANCE;
                default:
                    if (channel.o()) {
                        return GuildChannelIconType.NSFW.Text.INSTANCE;
                    }
                    List<PermissionOverwrite> s2 = channel.s();
                    if (s2 != null) {
                        Iterator<T> it = s2.iterator();
                        while (true) {
                            if (it.hasNext()) {
                                Object next = it.next();
                                if (((PermissionOverwrite) next).e() == channel.f()) {
                                    z3 = true;
                                    continue;
                                } else {
                                    z3 = false;
                                    continue;
                                }
                                if (z3) {
                                    obj = next;
                                }
                            }
                        }
                        PermissionOverwrite permissionOverwrite = (PermissionOverwrite) obj;
                        if (permissionOverwrite != null && PermissionOverwriteUtilsKt.denies(permissionOverwrite, Permission.VIEW_CHANNEL)) {
                            return GuildChannelIconType.Locked.Text.INSTANCE;
                        }
                    }
                    return GuildChannelIconType.Text.INSTANCE;
            }
        } else if (channel.o()) {
            return GuildChannelIconType.NSFW.Announcements.INSTANCE;
        } else {
            List<PermissionOverwrite> s3 = channel.s();
            if (s3 != null) {
                Iterator<T> it2 = s3.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    Object next2 = it2.next();
                    if (((PermissionOverwrite) next2).e() == channel.f()) {
                        z2 = true;
                        continue;
                    } else {
                        z2 = false;
                        continue;
                    }
                    if (z2) {
                        obj = next2;
                        break;
                    }
                }
                PermissionOverwrite permissionOverwrite2 = (PermissionOverwrite) obj;
                if (permissionOverwrite2 != null && PermissionOverwriteUtilsKt.denies(permissionOverwrite2, Permission.VIEW_CHANNEL)) {
                    return GuildChannelIconType.Locked.Announcements.INSTANCE;
                }
            }
            return GuildChannelIconType.Announcements.INSTANCE;
        }
    }

    public static final int guildChannelIcon(Channel channel) {
        return mapGuildChannelTypeToIcon(getChannelType(channel));
    }

    @DrawableRes
    public static final int mapGuildChannelTypeToIcon(GuildChannelIconType guildChannelIconType) {
        m.checkNotNullParameter(guildChannelIconType, "guildChannelIconType");
        if (m.areEqual(guildChannelIconType, GuildChannelIconType.Text.INSTANCE)) {
            return R.drawable.ic_channel_text;
        }
        if (guildChannelIconType instanceof GuildChannelIconType.Thread) {
            return R.drawable.ic_thread;
        }
        if (guildChannelIconType instanceof GuildChannelIconType.Announcements) {
            return R.drawable.ic_channel_announcements;
        }
        if (guildChannelIconType instanceof GuildChannelIconType.NSFW.Text) {
            return R.drawable.ic_channel_text_nsfw;
        }
        if (guildChannelIconType instanceof GuildChannelIconType.NSFW.Announcements) {
            return R.drawable.ic_channel_announcements_nsfw;
        }
        if (guildChannelIconType instanceof GuildChannelIconType.NSFW.Thread) {
            return R.drawable.ic_thread_locked;
        }
        if (guildChannelIconType instanceof GuildChannelIconType.NSFW.Voice) {
            return R.drawable.ic_voice_nsfw;
        }
        if (guildChannelIconType instanceof GuildChannelIconType.Locked.Text) {
            return R.drawable.ic_channel_text_locked;
        }
        if (guildChannelIconType instanceof GuildChannelIconType.Locked.Announcements) {
            return R.drawable.ic_channel_announcements_locked;
        }
        if (guildChannelIconType instanceof GuildChannelIconType.Locked.Thread) {
            return R.drawable.ic_thread_locked;
        }
        if (guildChannelIconType instanceof GuildChannelIconType.Directory) {
            return R.drawable.ic_hub_24dp;
        }
        if (guildChannelIconType instanceof GuildChannelIconType.Voice) {
            return R.drawable.ic_channel_voice;
        }
        throw new NoWhenBranchMatchedException();
    }
}
