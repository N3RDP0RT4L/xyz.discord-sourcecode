package com.discord.utilities.channel;

import andhook.lib.HookHelper;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: GuildChannelIconUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0007\u0004\u0005\u0006\u0007\b\t\nB\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\f\u000b\f\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016¨\u0006\u0017"}, d2 = {"Lcom/discord/utilities/channel/GuildChannelIconType;", "", HookHelper.constructorName, "()V", "Announcements", "Directory", "Locked", "NSFW", "Text", "Thread", "Voice", "Lcom/discord/utilities/channel/GuildChannelIconType$NSFW$Text;", "Lcom/discord/utilities/channel/GuildChannelIconType$NSFW$Announcements;", "Lcom/discord/utilities/channel/GuildChannelIconType$NSFW$Thread;", "Lcom/discord/utilities/channel/GuildChannelIconType$NSFW$Voice;", "Lcom/discord/utilities/channel/GuildChannelIconType$Locked$Text;", "Lcom/discord/utilities/channel/GuildChannelIconType$Locked$Announcements;", "Lcom/discord/utilities/channel/GuildChannelIconType$Locked$Thread;", "Lcom/discord/utilities/channel/GuildChannelIconType$Thread;", "Lcom/discord/utilities/channel/GuildChannelIconType$Text;", "Lcom/discord/utilities/channel/GuildChannelIconType$Announcements;", "Lcom/discord/utilities/channel/GuildChannelIconType$Directory;", "Lcom/discord/utilities/channel/GuildChannelIconType$Voice;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public abstract class GuildChannelIconType {

    /* compiled from: GuildChannelIconUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/utilities/channel/GuildChannelIconType$Announcements;", "Lcom/discord/utilities/channel/GuildChannelIconType;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Announcements extends GuildChannelIconType {
        public static final Announcements INSTANCE = new Announcements();

        private Announcements() {
            super(null);
        }
    }

    /* compiled from: GuildChannelIconUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/utilities/channel/GuildChannelIconType$Directory;", "Lcom/discord/utilities/channel/GuildChannelIconType;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Directory extends GuildChannelIconType {
        public static final Directory INSTANCE = new Directory();

        private Directory() {
            super(null);
        }
    }

    /* compiled from: GuildChannelIconUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0006\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0007"}, d2 = {"Lcom/discord/utilities/channel/GuildChannelIconType$Locked;", "", HookHelper.constructorName, "()V", "Announcements", "Text", "Thread", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class Locked {

        /* compiled from: GuildChannelIconUtils.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/utilities/channel/GuildChannelIconType$Locked$Announcements;", "Lcom/discord/utilities/channel/GuildChannelIconType;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Announcements extends GuildChannelIconType {
            public static final Announcements INSTANCE = new Announcements();

            private Announcements() {
                super(null);
            }
        }

        /* compiled from: GuildChannelIconUtils.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/utilities/channel/GuildChannelIconType$Locked$Text;", "Lcom/discord/utilities/channel/GuildChannelIconType;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Text extends GuildChannelIconType {
            public static final Text INSTANCE = new Text();

            private Text() {
                super(null);
            }
        }

        /* compiled from: GuildChannelIconUtils.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/utilities/channel/GuildChannelIconType$Locked$Thread;", "Lcom/discord/utilities/channel/GuildChannelIconType;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Thread extends GuildChannelIconType {
            public static final Thread INSTANCE = new Thread();

            private Thread() {
                super(null);
            }
        }

        private Locked() {
        }
    }

    /* compiled from: GuildChannelIconUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0007\b6\u0018\u00002\u00020\u0001:\u0004\u0004\u0005\u0006\u0007B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\b"}, d2 = {"Lcom/discord/utilities/channel/GuildChannelIconType$NSFW;", "", HookHelper.constructorName, "()V", "Announcements", "Text", "Thread", "Voice", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class NSFW {

        /* compiled from: GuildChannelIconUtils.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/utilities/channel/GuildChannelIconType$NSFW$Announcements;", "Lcom/discord/utilities/channel/GuildChannelIconType;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Announcements extends GuildChannelIconType {
            public static final Announcements INSTANCE = new Announcements();

            private Announcements() {
                super(null);
            }
        }

        /* compiled from: GuildChannelIconUtils.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/utilities/channel/GuildChannelIconType$NSFW$Text;", "Lcom/discord/utilities/channel/GuildChannelIconType;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Text extends GuildChannelIconType {
            public static final Text INSTANCE = new Text();

            private Text() {
                super(null);
            }
        }

        /* compiled from: GuildChannelIconUtils.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/utilities/channel/GuildChannelIconType$NSFW$Thread;", "Lcom/discord/utilities/channel/GuildChannelIconType;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Thread extends GuildChannelIconType {
            public static final Thread INSTANCE = new Thread();

            private Thread() {
                super(null);
            }
        }

        /* compiled from: GuildChannelIconUtils.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/utilities/channel/GuildChannelIconType$NSFW$Voice;", "Lcom/discord/utilities/channel/GuildChannelIconType;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Voice extends GuildChannelIconType {
            public static final Voice INSTANCE = new Voice();

            private Voice() {
                super(null);
            }
        }

        private NSFW() {
        }
    }

    /* compiled from: GuildChannelIconUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/utilities/channel/GuildChannelIconType$Text;", "Lcom/discord/utilities/channel/GuildChannelIconType;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Text extends GuildChannelIconType {
        public static final Text INSTANCE = new Text();

        private Text() {
            super(null);
        }
    }

    /* compiled from: GuildChannelIconUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/utilities/channel/GuildChannelIconType$Thread;", "Lcom/discord/utilities/channel/GuildChannelIconType;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Thread extends GuildChannelIconType {
        public static final Thread INSTANCE = new Thread();

        private Thread() {
            super(null);
        }
    }

    /* compiled from: GuildChannelIconUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/utilities/channel/GuildChannelIconType$Voice;", "Lcom/discord/utilities/channel/GuildChannelIconType;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Voice extends GuildChannelIconType {
        public static final Voice INSTANCE = new Voice();

        private Voice() {
            super(null);
        }
    }

    private GuildChannelIconType() {
    }

    public /* synthetic */ GuildChannelIconType(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }
}
