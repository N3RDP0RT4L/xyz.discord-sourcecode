package com.discord.utilities.channel;

import androidx.core.app.NotificationCompat;
import com.discord.api.channel.Channel;
import com.discord.api.role.GuildRole;
import com.discord.models.domain.ModelNotificationSettings;
import com.discord.models.guild.Guild;
import com.discord.models.user.MeUser;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import rx.functions.Func8;
/* compiled from: GuildChannelsInfo.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0018\u001a\n \u0001*\u0004\u0018\u00010\u00150\u00152\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\b\u0010\u0004\u001a\u0004\u0018\u00010\u00032\u000e\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00050\u00052\u000e\u0010\b\u001a\n \u0001*\u0004\u0018\u00010\u00070\u00072.\u0010\r\u001a*\u0012\b\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\f \u0001*\u0014\u0012\b\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\f\u0018\u00010\t0\t2\u000e\u0010\u000f\u001a\n\u0018\u00010\nj\u0004\u0018\u0001`\u000e26\u0010\u0011\u001a2\u0012\b\u0012\u00060\nj\u0002`\u0010\u0012\b\u0012\u00060\nj\u0002`\u000e \u0001*\u0018\u0012\b\u0012\u00060\nj\u0002`\u0010\u0012\b\u0012\u00060\nj\u0002`\u000e\u0018\u00010\t0\t2\u001a\u0010\u0014\u001a\u0016\u0012\u0004\u0012\u00020\u0013 \u0001*\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u00120\u0012H\n¢\u0006\u0004\b\u0016\u0010\u0017"}, d2 = {"Lcom/discord/models/user/MeUser;", "kotlin.jvm.PlatformType", "me", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/models/domain/ModelNotificationSettings;", "guildSettings", "", "hideMuted", "", "", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "guildRoles", "Lcom/discord/api/permission/PermissionBit;", "guildPermissions", "Lcom/discord/primitives/ChannelId;", "channelPermissions", "", "Lcom/discord/api/channel/Channel;", "categories", "Lcom/discord/utilities/channel/GuildChannelsInfo;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/user/MeUser;Lcom/discord/models/guild/Guild;Lcom/discord/models/domain/ModelNotificationSettings;Ljava/lang/Boolean;Ljava/util/Map;Ljava/lang/Long;Ljava/util/Map;Ljava/util/List;)Lcom/discord/utilities/channel/GuildChannelsInfo;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildChannelsInfo$Companion$get$1<T1, T2, T3, T4, T5, T6, T7, T8, R> implements Func8<MeUser, Guild, ModelNotificationSettings, Boolean, Map<Long, ? extends GuildRole>, Long, Map<Long, ? extends Long>, List<? extends Channel>, GuildChannelsInfo> {
    public static final GuildChannelsInfo$Companion$get$1 INSTANCE = new GuildChannelsInfo$Companion$get$1();

    @Override // rx.functions.Func8
    public /* bridge */ /* synthetic */ GuildChannelsInfo call(MeUser meUser, Guild guild, ModelNotificationSettings modelNotificationSettings, Boolean bool, Map<Long, ? extends GuildRole> map, Long l, Map<Long, ? extends Long> map2, List<? extends Channel> list) {
        return call2(meUser, guild, modelNotificationSettings, bool, (Map<Long, GuildRole>) map, l, (Map<Long, Long>) map2, (List<Channel>) list);
    }

    /* JADX WARN: Removed duplicated region for block: B:26:0x0046  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x0050  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x0079  */
    /* JADX WARN: Removed duplicated region for block: B:48:0x009f  */
    /* JADX WARN: Removed duplicated region for block: B:49:0x00a5  */
    /* renamed from: call  reason: avoid collision after fix types in other method */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final com.discord.utilities.channel.GuildChannelsInfo call2(com.discord.models.user.MeUser r19, com.discord.models.guild.Guild r20, com.discord.models.domain.ModelNotificationSettings r21, java.lang.Boolean r22, java.util.Map<java.lang.Long, com.discord.api.role.GuildRole> r23, java.lang.Long r24, java.util.Map<java.lang.Long, java.lang.Long> r25, java.util.List<com.discord.api.channel.Channel> r26) {
        /*
            Method dump skipped, instructions count: 224
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.channel.GuildChannelsInfo$Companion$get$1.call2(com.discord.models.user.MeUser, com.discord.models.guild.Guild, com.discord.models.domain.ModelNotificationSettings, java.lang.Boolean, java.util.Map, java.lang.Long, java.util.Map, java.util.List):com.discord.utilities.channel.GuildChannelsInfo");
    }
}
