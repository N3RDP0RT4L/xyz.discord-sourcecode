package com.discord.utilities.channel;

import com.discord.utilities.analytics.AnalyticsTracker;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: ChannelSelector.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ChannelSelector$openCreateThread$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ Long $parentMessageId;
    public final /* synthetic */ String $startThreadLocation;
    public final /* synthetic */ ChannelSelector this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ChannelSelector$openCreateThread$1(ChannelSelector channelSelector, long j, long j2, Long l, String str) {
        super(0);
        this.this$0 = channelSelector;
        this.$guildId = j;
        this.$channelId = j2;
        this.$parentMessageId = l;
        this.$startThreadLocation = str;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        this.this$0.getStream().getThreadDraft$app_productionGoogleRelease().clearDraftState();
        this.this$0.getStream().getChannelsSelected$app_productionGoogleRelease().openCreateThread(this.$guildId, this.$channelId, this.$parentMessageId, this.$startThreadLocation);
        AnalyticsTracker.INSTANCE.threadCreationStarted(this.$channelId, this.$guildId, this.$startThreadLocation);
    }
}
