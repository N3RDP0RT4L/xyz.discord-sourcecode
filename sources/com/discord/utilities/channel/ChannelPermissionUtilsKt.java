package com.discord.utilities.channel;

import android.content.Context;
import androidx.annotation.StringRes;
import b.a.d.f;
import b.a.k.b;
import com.discord.api.role.GuildRole;
import d0.o;
import d0.t.h0;
import d0.z.d.m;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Pair;
import xyz.discord.R;
/* compiled from: ChannelPermissionUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u001a)\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\b\u001a\u0017\u0010\u000b\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\tH\u0007¢\u0006\u0004\b\u000b\u0010\f¨\u0006\r"}, d2 = {"", "useNewThreadsPermissions", "Landroid/content/Context;", "context", "", "", "Lcom/discord/utilities/channel/PermissionLabelOverrides;", "getCategoryLabels", "(ZLandroid/content/Context;)Ljava/util/Map;", "Lcom/discord/api/role/GuildRole;", "role", "getChannelPermissionOwnerRoleLabel", "(Lcom/discord/api/role/GuildRole;)I", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ChannelPermissionUtilsKt {
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v92, types: [java.lang.CharSequence] */
    /* JADX WARN: Type inference failed for: r5v34, types: [java.lang.CharSequence] */
    /* JADX WARN: Type inference failed for: r5v35, types: [java.lang.CharSequence] */
    /* JADX WARN: Type inference failed for: r6v29, types: [java.lang.CharSequence] */
    /* JADX WARN: Type inference failed for: r7v25, types: [java.lang.CharSequence] */
    /* JADX WARN: Type inference failed for: r8v22, types: [java.lang.CharSequence] */
    public static final Map<Integer, PermissionLabelOverrides> getCategoryLabels(boolean z2, Context context) {
        CharSequence b2;
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        String str8;
        String str9;
        CharSequence b3;
        CharSequence b4;
        CharSequence b5;
        CharSequence b6;
        CharSequence b7;
        CharSequence b8;
        CharSequence b9;
        ?? b10;
        ?? b11;
        ?? b12;
        ?? b13;
        ?? b14;
        ?? b15;
        m.checkNotNullParameter(context, "context");
        Pair[] pairArr = new Pair[29];
        Integer valueOf = Integer.valueOf((int) R.id.channel_permission_text_read_messages);
        String string = context.getString(R.string.role_permissions_view_channel);
        String string2 = context.getString(R.string.role_permissions_view_channel_description_category);
        m.checkNotNullExpressionValue(string2, "context.getString(R.stri…nel_description_category)");
        pairArr[0] = o.to(valueOf, new PermissionLabelOverrides(string2, string, null, null, context.getString(R.string.role_permissions_view_channel_description_text_everyone), context.getString(R.string.role_permissions_view_channel_description_voice_everyone), null, context.getString(R.string.role_permissions_view_channel_description_category_everyone), null, 332, null));
        Integer valueOf2 = Integer.valueOf((int) R.id.channel_permission_general_manage_channel);
        String string3 = context.getString(R.string.manage_channels);
        String string4 = context.getString(R.string.role_permissions_manage_channel_description_category);
        m.checkNotNullExpressionValue(string4, "context.getString(R.stri…nel_description_category)");
        pairArr[1] = o.to(valueOf2, new PermissionLabelOverrides(string4, string3, context.getString(R.string.role_permissions_manage_channel_description_voice), context.getString(R.string.role_permissions_manage_channel_description_stage), null, null, null, null, null, 496, null));
        Integer valueOf3 = Integer.valueOf((int) R.id.channel_permission_general_manage_threads);
        String string5 = context.getString(R.string.role_permissions_manage_threads_description_category);
        m.checkNotNullExpressionValue(string5, "context.getString(R.stri…ads_description_category)");
        pairArr[2] = o.to(valueOf3, new PermissionLabelOverrides(string5, null, null, null, null, null, null, context.getString(R.string.role_permissions_manage_threads_description_category), null, 382, null));
        Integer valueOf4 = Integer.valueOf((int) R.id.channel_permission_general_manage_permissions);
        String string6 = context.getString(R.string.role_permissions_manage_roles_description_category);
        m.checkNotNullExpressionValue(string6, "context.getString(R.stri…les_description_category)");
        pairArr[3] = o.to(valueOf4, new PermissionLabelOverrides(string6, null, null, context.getString(R.string.role_permissions_manage_roles_description_stage), null, null, null, null, null, 502, null));
        Integer valueOf5 = Integer.valueOf((int) R.id.channel_permission_general_manage_webhooks);
        String string7 = context.getString(R.string.role_permissions_manage_webhooks_description_category);
        m.checkNotNullExpressionValue(string7, "context.getString(R.stri…oks_description_category)");
        pairArr[4] = o.to(valueOf5, new PermissionLabelOverrides(string7, null, null, null, null, null, null, null, null, 510, null));
        Integer valueOf6 = Integer.valueOf((int) R.id.channel_permission_text_send_messages);
        String string8 = context.getString(R.string.role_permissions_send_messages_description_category);
        m.checkNotNullExpressionValue(string8, "context.getString(R.stri…ges_description_category)");
        f fVar = f.a;
        b2 = b.b(context, R.string.role_permissions_send_messages_description_announcement, new Object[]{fVar.a(360032008192L, null)}, (r4 & 4) != 0 ? b.C0034b.j : null);
        pairArr[5] = o.to(valueOf6, new PermissionLabelOverrides(string8, null, null, null, null, null, null, null, b2, 254, null));
        Integer valueOf7 = Integer.valueOf((int) R.id.channel_permission_text_send_messages_in_threads);
        if (z2) {
            str2 = context.getString(R.string.role_permissions_send_messages_in_threads_description_category);
            str = "context.getString(R.stri…ads_description_category)";
            m.checkNotNullExpressionValue(str2, str);
        } else {
            str = "context.getString(R.stri…ads_description_category)";
            b15 = b.b(context, R.string.interim_role_permissions_send_messages_in_threads_description_category, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            str2 = b15;
        }
        if (z2) {
            String string9 = context.getString(R.string.role_permissions_send_messages_in_threads_description_category);
            m.checkNotNullExpressionValue(string9, str);
            str3 = string9;
        } else {
            b14 = b.b(context, R.string.interim_role_permissions_send_messages_in_threads_description_category, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            str3 = b14;
        }
        String str10 = str;
        pairArr[6] = o.to(valueOf7, new PermissionLabelOverrides(str2, null, null, null, null, null, null, str3, null, 382, null));
        Integer valueOf8 = Integer.valueOf((int) R.id.channel_permission_text_create_public_threads);
        if (z2) {
            str5 = context.getString(R.string.role_permissions_create_public_threads_description_category);
            str4 = str10;
            m.checkNotNullExpressionValue(str5, str4);
        } else {
            str4 = str10;
            b13 = b.b(context, R.string.interim_role_permissions_create_public_threads_description_category, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            str5 = b13;
        }
        if (z2) {
            String string10 = context.getString(R.string.role_permissions_create_public_threads_description_category);
            m.checkNotNullExpressionValue(string10, str4);
            str6 = string10;
        } else {
            b12 = b.b(context, R.string.interim_role_permissions_create_public_threads_description_category, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            str6 = b12;
        }
        String str11 = str6;
        String str12 = str4;
        pairArr[7] = o.to(valueOf8, new PermissionLabelOverrides(str5, null, null, null, null, null, null, str11, null, 382, null));
        Integer valueOf9 = Integer.valueOf((int) R.id.channel_permission_text_create_private_threads);
        if (z2) {
            str8 = context.getString(R.string.role_permissions_create_private_threads_description_category);
            str7 = str12;
            m.checkNotNullExpressionValue(str8, str7);
        } else {
            str7 = str12;
            b11 = b.b(context, R.string.interim_role_permissions_create_private_threads_description_category, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            str8 = b11;
        }
        if (z2) {
            String string11 = context.getString(R.string.role_permissions_create_private_threads_description_category);
            m.checkNotNullExpressionValue(string11, str7);
            str9 = string11;
        } else {
            b10 = b.b(context, R.string.interim_role_permissions_create_private_threads_description_category, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            str9 = b10;
        }
        pairArr[8] = o.to(valueOf9, new PermissionLabelOverrides(str8, null, null, null, null, null, null, str9, null, 382, null));
        Integer valueOf10 = Integer.valueOf((int) R.id.channel_permission_general_create_instant_invite);
        String string12 = context.getString(R.string.role_permissions_create_instant_invite_description_category);
        m.checkNotNullExpressionValue(string12, "context.getString(R.stri…ite_description_category)");
        pairArr[9] = o.to(valueOf10, new PermissionLabelOverrides(string12, null, context.getString(R.string.role_permissions_create_instant_invite_description_voice), null, null, null, null, null, null, 506, null));
        Integer valueOf11 = Integer.valueOf((int) R.id.channel_permission_text_embed_links);
        String string13 = context.getString(R.string.role_permissions_embed_links_description_category);
        m.checkNotNullExpressionValue(string13, "context.getString(R.stri…nks_description_category)");
        pairArr[10] = o.to(valueOf11, new PermissionLabelOverrides(string13, null, null, null, null, null, null, null, null, 510, null));
        Integer valueOf12 = Integer.valueOf((int) R.id.channel_permission_text_attach_files);
        String string14 = context.getString(R.string.role_permissions_attach_files_description_category);
        m.checkNotNullExpressionValue(string14, "context.getString(R.stri…les_description_category)");
        pairArr[11] = o.to(valueOf12, new PermissionLabelOverrides(string14, null, null, null, null, null, null, null, null, 510, null));
        Integer valueOf13 = Integer.valueOf((int) R.id.channel_permission_text_add_reactions);
        String string15 = context.getString(R.string.role_permissions_add_reactions_description_category);
        m.checkNotNullExpressionValue(string15, "context.getString(R.stri…ons_description_category)");
        pairArr[12] = o.to(valueOf13, new PermissionLabelOverrides(string15, null, null, null, null, null, null, null, null, 510, null));
        Integer valueOf14 = Integer.valueOf((int) R.id.channel_permission_text_use_external_emojis);
        String string16 = context.getString(R.string.role_permissions_use_external_emojis_description_category);
        m.checkNotNullExpressionValue(string16, "context.getString(R.stri…jis_description_category)");
        pairArr[13] = o.to(valueOf14, new PermissionLabelOverrides(string16, null, null, null, null, null, null, null, null, 510, null));
        Integer valueOf15 = Integer.valueOf((int) R.id.channel_permission_text_use_external_stickers);
        String string17 = context.getString(R.string.role_permissions_use_external_stickers_description_category);
        m.checkNotNullExpressionValue(string17, "context.getString(R.stri…ers_description_category)");
        pairArr[14] = o.to(valueOf15, new PermissionLabelOverrides(string17, null, null, null, null, null, null, null, null, 510, null));
        Integer valueOf16 = Integer.valueOf((int) R.id.channel_permission_text_mention_everyone);
        String string18 = context.getString(R.string.role_permissions_mention_everyone_description_category);
        m.checkNotNullExpressionValue(string18, "context.getString(R.stri…one_description_category)");
        pairArr[15] = o.to(valueOf16, new PermissionLabelOverrides(string18, null, null, null, null, null, null, null, null, 510, null));
        Integer valueOf17 = Integer.valueOf((int) R.id.channel_permission_text_manage_messages);
        String string19 = context.getString(R.string.role_permissions_manage_messages_description_category);
        m.checkNotNullExpressionValue(string19, "context.getString(R.stri…ges_description_category)");
        b3 = b.b(context, R.string.role_permissions_manage_messages_description_announcement, new Object[]{fVar.a(360032008192L, null)}, (r4 & 4) != 0 ? b.C0034b.j : null);
        pairArr[16] = o.to(valueOf17, new PermissionLabelOverrides(string19, null, null, null, null, null, null, null, b3, 254, null));
        Integer valueOf18 = Integer.valueOf((int) R.id.channel_permission_text_read_message_history);
        String string20 = context.getString(R.string.role_permissions_read_message_history_description_category);
        m.checkNotNullExpressionValue(string20, "context.getString(R.stri…ory_description_category)");
        pairArr[17] = o.to(valueOf18, new PermissionLabelOverrides(string20, null, null, null, null, null, null, null, null, 510, null));
        Integer valueOf19 = Integer.valueOf((int) R.id.channel_permission_text_send_tts_messages);
        String string21 = context.getString(R.string.role_permissions_send_tts_messages_description_category);
        m.checkNotNullExpressionValue(string21, "context.getString(R.stri…ges_description_category)");
        pairArr[18] = o.to(valueOf19, new PermissionLabelOverrides(string21, null, null, null, null, null, null, null, null, 510, null));
        Integer valueOf20 = Integer.valueOf((int) R.id.channel_permission_voice_connect);
        String string22 = context.getString(R.string.role_permissions_connect_description_category);
        m.checkNotNullExpressionValue(string22, "context.getString(R.stri…ect_description_category)");
        pairArr[19] = o.to(valueOf20, new PermissionLabelOverrides(string22, null, null, context.getString(R.string.role_permissions_connect_description_stage), null, context.getString(R.string.role_permissions_connect_description_voice_everyone), context.getString(R.string.role_permissions_connect_description_stage_everyone), context.getString(R.string.role_permissions_connect_description_category_everyone), null, 278, null));
        Integer valueOf21 = Integer.valueOf((int) R.id.channel_permission_voice_speak);
        String string23 = context.getString(R.string.role_permissions_speak_description_category);
        m.checkNotNullExpressionValue(string23, "context.getString(R.stri…eak_description_category)");
        pairArr[20] = o.to(valueOf21, new PermissionLabelOverrides(string23, null, null, context.getString(R.string.role_permissions_speak_description_stage), null, null, null, null, null, 502, null));
        Integer valueOf22 = Integer.valueOf((int) R.id.channel_permission_voice_video);
        String string24 = context.getString(R.string.role_permissions_stream_description_category);
        m.checkNotNullExpressionValue(string24, "context.getString(R.stri…eam_description_category)");
        pairArr[21] = o.to(valueOf22, new PermissionLabelOverrides(string24, null, null, null, null, null, null, null, null, 510, null));
        Integer valueOf23 = Integer.valueOf((int) R.id.channel_permission_voice_use_vad);
        String string25 = context.getString(R.string.role_permissions_use_vad_description_category);
        m.checkNotNullExpressionValue(string25, "context.getString(R.stri…vad_description_category)");
        pairArr[22] = o.to(valueOf23, new PermissionLabelOverrides(string25, null, null, context.getString(R.string.role_permissions_use_vad_description_stage), null, null, null, null, null, 502, null));
        Integer valueOf24 = Integer.valueOf((int) R.id.channel_permission_voice_priority_speaker);
        b4 = b.b(context, R.string.keybind_push_to_talk_priority, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        b5 = b.b(context, R.string.role_permissions_priority_speaker_description_category_mobile, new Object[]{b4}, (r4 & 4) != 0 ? b.C0034b.j : null);
        b6 = b.b(context, R.string.keybind_push_to_talk_priority, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        b7 = b.b(context, R.string.role_permissions_priority_speaker_description_category_mobile, new Object[]{b6}, (r4 & 4) != 0 ? b.C0034b.j : null);
        pairArr[23] = o.to(valueOf24, new PermissionLabelOverrides(b5, null, null, null, null, null, null, b7, null, 382, null));
        Integer valueOf25 = Integer.valueOf((int) R.id.channel_permission_voice_mute_members);
        b8 = b.b(context, R.string.role_permissions_mute_members_description_category, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        b9 = b.b(context, R.string.role_permissions_mute_members_description_stage, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        pairArr[24] = o.to(valueOf25, new PermissionLabelOverrides(b8, null, null, b9, null, null, null, null, null, 502, null));
        Integer valueOf26 = Integer.valueOf((int) R.id.channel_permission_voice_deafen_members);
        String string26 = context.getString(R.string.role_permissions_deafen_members_description_category);
        m.checkNotNullExpressionValue(string26, "context.getString(R.stri…ers_description_category)");
        pairArr[25] = o.to(valueOf26, new PermissionLabelOverrides(string26, null, null, null, null, null, null, null, null, 510, null));
        Integer valueOf27 = Integer.valueOf((int) R.id.channel_permission_voice_move_members);
        String string27 = context.getString(R.string.role_permissions_move_members_description_category);
        m.checkNotNullExpressionValue(string27, "context.getString(R.stri…ers_description_category)");
        pairArr[26] = o.to(valueOf27, new PermissionLabelOverrides(string27, null, null, context.getString(R.string.role_permissions_move_members_description_stage), null, null, null, null, null, 502, null));
        Integer valueOf28 = Integer.valueOf((int) R.id.channel_permission_stage_request_to_speak);
        String string28 = context.getString(R.string.role_permissions_request_to_speak_description_category);
        m.checkNotNullExpressionValue(string28, "context.getString(R.stri…eak_description_category)");
        pairArr[27] = o.to(valueOf28, new PermissionLabelOverrides(string28, null, null, null, null, null, null, null, null, 510, null));
        Integer valueOf29 = Integer.valueOf((int) R.id.channel_permission_events_manage_events);
        String string29 = context.getString(R.string.role_permissions_manage_events_description_category);
        m.checkNotNullExpressionValue(string29, "context.getString(R.stri…nts_description_category)");
        pairArr[28] = o.to(valueOf29, new PermissionLabelOverrides(string29, null, null, null, null, null, null, null, null, 510, null));
        return h0.hashMapOf(pairArr);
    }

    @StringRes
    public static final int getChannelPermissionOwnerRoleLabel(GuildRole guildRole) {
        m.checkNotNullParameter(guildRole, "role");
        return (guildRole.h() & 8) == 8 ? R.string.private_channel_add_members_modal_row_administrator : R.string.private_channel_add_members_modal_row_role;
    }
}
