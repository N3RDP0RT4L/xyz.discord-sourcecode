package com.discord.utilities.rest;

import android.os.Build;
import android.os.LocaleList;
import d0.d0.f;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: RestAPI.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000e\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()Ljava/lang/String;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class RestAPI$AppHeadersProvider$acceptLanguageProvider$1 extends o implements Function0<String> {
    public static final RestAPI$AppHeadersProvider$acceptLanguageProvider$1 INSTANCE = new RestAPI$AppHeadersProvider$acceptLanguageProvider$1();

    public RestAPI$AppHeadersProvider$acceptLanguageProvider$1() {
        super(0);
    }

    @Override // kotlin.jvm.functions.Function0
    public final String invoke() {
        ArrayList arrayList = new ArrayList();
        if (Build.VERSION.SDK_INT >= 24) {
            LocaleList adjustedDefault = LocaleList.getAdjustedDefault();
            m.checkNotNullExpressionValue(adjustedDefault, "LocaleList.getAdjustedDefault()");
            int i = 10;
            int size = adjustedDefault.size();
            for (int i2 = 0; i2 < size; i2++) {
                String languageTag = adjustedDefault.get(i2).toLanguageTag();
                if (i2 != 0) {
                    arrayList.add(languageTag + ";q=0." + i);
                } else {
                    arrayList.add(languageTag);
                }
                i = f.coerceAtLeast(1, i - 1);
            }
        } else {
            arrayList.add(Locale.getDefault().toLanguageTag());
        }
        return u.joinToString$default(arrayList, ",", null, null, 0, null, null, 62, null);
    }
}
