package com.discord.utilities.rest;

import androidx.core.app.NotificationCompat;
import com.discord.api.channel.Channel;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreStream;
import d0.z.d.m;
import kotlin.Metadata;
import rx.functions.Action1;
/* compiled from: RestAPI.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/api/channel/Channel;", "kotlin.jvm.PlatformType", "channel", "", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/api/channel/Channel;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class RestAPI$createGroupDM$1<T> implements Action1<Channel> {
    public static final RestAPI$createGroupDM$1 INSTANCE = new RestAPI$createGroupDM$1();

    public final void call(Channel channel) {
        StoreChannels channels = StoreStream.Companion.getChannels();
        m.checkNotNullExpressionValue(channel, "channel");
        channels.onGroupCreated(channel);
    }
}
