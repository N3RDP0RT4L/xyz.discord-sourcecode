package com.discord.utilities.rest;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import androidx.core.app.NotificationCompat;
import com.discord.app.AppLog;
import com.discord.utilities.logging.Logger;
import d0.y.b;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: SendUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0003\u001a\u001d\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006\u001a\u001d\u0010\b\u001a\u00020\u00072\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\b\u0010\t¨\u0006\n"}, d2 = {"Landroid/net/Uri;", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "Landroid/content/ContentResolver;", "contentResolver", "", "computeFileSizeBytes", "(Landroid/net/Uri;Landroid/content/ContentResolver;)J", "", "computeFileSizeMegabytes", "(Landroid/net/Uri;Landroid/content/ContentResolver;)F", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SendUtilsKt {
    public static final long computeFileSizeBytes(Uri uri, ContentResolver contentResolver) {
        long j;
        Long valueOf;
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(contentResolver, "contentResolver");
        try {
            Cursor query = contentResolver.query(uri, null, null, null, null);
            if (query != null) {
                try {
                    int columnIndex = query.getColumnIndex("_size");
                    query.moveToFirst();
                    valueOf = Long.valueOf(query.getLong(columnIndex));
                } catch (Exception unused) {
                    j = -1;
                }
            } else {
                valueOf = null;
            }
            if (valueOf == null || valueOf.longValue() <= 0) {
                ParcelFileDescriptor openFileDescriptor = contentResolver.openFileDescriptor(uri, "r");
                Long valueOf2 = openFileDescriptor != null ? Long.valueOf(openFileDescriptor.getStatSize()) : null;
                if (openFileDescriptor != null) {
                    openFileDescriptor.close();
                }
                long longValue = valueOf2 != null ? valueOf2.longValue() : -1L;
                b.closeFinally(query, null);
                return longValue;
            }
            j = valueOf.longValue();
            b.closeFinally(query, null);
            return j;
        } catch (Exception e) {
            Logger.e$default(AppLog.g, "Failed querying size of file " + uri, e, null, 4, null);
            return -1L;
        }
    }

    public static final float computeFileSizeMegabytes(Uri uri, ContentResolver contentResolver) {
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(contentResolver, "contentResolver");
        return ((float) computeFileSizeBytes(uri, contentResolver)) / 1048576;
    }
}
