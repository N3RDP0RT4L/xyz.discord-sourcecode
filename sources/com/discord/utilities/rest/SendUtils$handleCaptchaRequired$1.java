package com.discord.utilities.rest;

import com.discord.app.AppFragment;
import com.discord.stores.StoreStream;
import com.discord.utilities.messagesend.MessageResult;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: SendUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/app/AppFragment;", "<anonymous parameter 0>", "", "captchaToken", "", "invoke", "(Lcom/discord/app/AppFragment;Ljava/lang/String;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SendUtils$handleCaptchaRequired$1 extends o implements Function2<AppFragment, String, Unit> {
    public final /* synthetic */ MessageResult.CaptchaRequired $messageResult;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SendUtils$handleCaptchaRequired$1(MessageResult.CaptchaRequired captchaRequired) {
        super(2);
        this.$messageResult = captchaRequired;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(AppFragment appFragment, String str) {
        invoke2(appFragment, str);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(AppFragment appFragment, String str) {
        m.checkNotNullParameter(appFragment, "<anonymous parameter 0>");
        m.checkNotNullParameter(str, "captchaToken");
        StoreStream.Companion.getMessages().resendMessageWithCaptcha(this.$messageResult.getChannelId(), this.$messageResult.getNonce(), str);
    }
}
