package com.discord.utilities.rest;

import com.lytefast.flexinput.model.Attachment;
import d0.f0.k;
import d0.l;
import d0.w.h.c;
import d0.w.i.a.b;
import d0.w.i.a.e;
import d0.w.i.a.j;
import d0.z.d.m;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
/* compiled from: SendUtils.kt */
@e(c = "com.discord.utilities.rest.SendUtils$uniqueifyNames$1", f = "SendUtils.kt", l = {202, 204}, m = "invokeSuspend")
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u0003*\u0010\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u00010\u0000H\u008a@¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Ld0/f0/k;", "Lcom/lytefast/flexinput/model/Attachment;", "", "", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SendUtils$uniqueifyNames$1 extends j implements Function2<k<? super Attachment<? extends Object>>, Continuation<? super Unit>, Object> {
    public final /* synthetic */ List $attachmentParts;
    private /* synthetic */ Object L$0;
    public Object L$1;
    public Object L$2;
    public int label;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SendUtils$uniqueifyNames$1(List list, Continuation continuation) {
        super(2, continuation);
        this.$attachmentParts = list;
    }

    @Override // d0.w.i.a.a
    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        m.checkNotNullParameter(continuation, "completion");
        SendUtils$uniqueifyNames$1 sendUtils$uniqueifyNames$1 = new SendUtils$uniqueifyNames$1(this.$attachmentParts, continuation);
        sendUtils$uniqueifyNames$1.L$0 = obj;
        return sendUtils$uniqueifyNames$1;
    }

    @Override // kotlin.jvm.functions.Function2
    public final Object invoke(k<? super Attachment<? extends Object>> kVar, Continuation<? super Unit> continuation) {
        return ((SendUtils$uniqueifyNames$1) create(kVar, continuation)).invokeSuspend(Unit.a);
    }

    @Override // d0.w.i.a.a
    public final Object invokeSuspend(Object obj) {
        SendUtils$uniqueifyNames$1 sendUtils$uniqueifyNames$1;
        k kVar;
        Map map;
        Iterator it;
        Object coroutine_suspended = c.getCOROUTINE_SUSPENDED();
        int i = this.label;
        if (i == 0) {
            l.throwOnFailure(obj);
            kVar = (k) this.L$0;
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            it = this.$attachmentParts.iterator();
            sendUtils$uniqueifyNames$1 = this;
            map = linkedHashMap;
        } else if (i == 1 || i == 2) {
            it = (Iterator) this.L$2;
            map = (Map) this.L$1;
            kVar = (k) this.L$0;
            l.throwOnFailure(obj);
            sendUtils$uniqueifyNames$1 = this;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        while (it.hasNext()) {
            Attachment attachment = (Attachment) it.next();
            Object obj2 = map.get(attachment.getDisplayName());
            if (obj2 == null) {
                obj2 = b.boxInt(0);
            }
            int intValue = ((Number) obj2).intValue();
            map.put(attachment.getDisplayName(), b.boxInt(intValue + 1));
            if (intValue == 0) {
                sendUtils$uniqueifyNames$1.L$0 = kVar;
                sendUtils$uniqueifyNames$1.L$1 = map;
                sendUtils$uniqueifyNames$1.L$2 = it;
                sendUtils$uniqueifyNames$1.label = 1;
                if (kVar.yield(attachment, sendUtils$uniqueifyNames$1) == coroutine_suspended) {
                    return coroutine_suspended;
                }
            } else {
                Attachment renamedWithDuplicateCount = SendUtils.INSTANCE.renamedWithDuplicateCount(attachment, intValue);
                sendUtils$uniqueifyNames$1.L$0 = kVar;
                sendUtils$uniqueifyNames$1.L$1 = map;
                sendUtils$uniqueifyNames$1.L$2 = it;
                sendUtils$uniqueifyNames$1.label = 2;
                if (kVar.yield(renamedWithDuplicateCount, sendUtils$uniqueifyNames$1) == coroutine_suspended) {
                    return coroutine_suspended;
                }
            }
        }
        return Unit.a;
    }
}
