package com.discord.utilities.rest;

import andhook.lib.HookHelper;
import android.content.Context;
import b.a.k.b;
import com.discord.restapi.RestAPIAbortCodes;
import com.discord.utilities.error.Error;
import d0.o;
import d0.t.h0;
import d0.z.d.m;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import xyz.discord.R;
/* compiled from: RestAPIAbortMessages.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001\u0014B\t\b\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u0019\u0010\u0004\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0004\u0010\u0005J3\u0010\r\u001a\u00020\t2\u0006\u0010\u0007\u001a\u00020\u00062\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u000e\b\u0002\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\b¢\u0006\u0004\b\r\u0010\u000eR\"\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00020\u000f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011¨\u0006\u0015"}, d2 = {"Lcom/discord/utilities/rest/RestAPIAbortMessages;", "", "", "abortCode", "getAbortCodeMessageResId", "(I)Ljava/lang/Integer;", "Lcom/discord/utilities/error/Error;", "errorResponse", "Lkotlin/Function0;", "", "onHandle", "", "onDefault", "handleAbortCodeOrDefault", "(Lcom/discord/utilities/error/Error;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V", "", "MESSAGES", "Ljava/util/Map;", HookHelper.constructorName, "()V", "ResponseResolver", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class RestAPIAbortMessages {
    public static final RestAPIAbortMessages INSTANCE = new RestAPIAbortMessages();
    private static final Map<Integer, Integer> MESSAGES;

    /* compiled from: RestAPIAbortMessages.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\r\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fJ)\u0010\t\u001a\u0004\u0018\u00010\b2\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\t\u0010\n¨\u0006\r"}, d2 = {"Lcom/discord/utilities/rest/RestAPIAbortMessages$ResponseResolver;", "", "Landroid/content/Context;", "context", "", "abortCode", "", "username", "", "getRelationshipResponse", "(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/CharSequence;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ResponseResolver {
        public static final ResponseResolver INSTANCE = new ResponseResolver();

        private ResponseResolver() {
        }

        public final CharSequence getRelationshipResponse(Context context, int i, String str) {
            CharSequence b2;
            CharSequence b3;
            m.checkNotNullParameter(str, "username");
            Integer abortCodeMessageResId = RestAPIAbortMessages.getAbortCodeMessageResId(i);
            int intValue = abortCodeMessageResId != null ? abortCodeMessageResId.intValue() : R.string.add_friend_error_other;
            if (i != 80000) {
                if (context == null) {
                    return null;
                }
                b3 = b.b(context, intValue, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                return b3;
            } else if (context == null) {
                return null;
            } else {
                b2 = b.b(context, intValue, new Object[]{str}, (r4 & 4) != 0 ? b.C0034b.j : null);
                return b2;
            }
        }
    }

    static {
        Integer valueOf = Integer.valueOf((int) RestAPIAbortCodes.RELATIONSHIP_INCOMING_DISABLED);
        Integer valueOf2 = Integer.valueOf((int) RestAPIAbortCodes.RELATIONSHIP_INCOMING_BLOCKED);
        Integer valueOf3 = Integer.valueOf((int) R.string.add_friend_error_other);
        MESSAGES = h0.mapOf(o.to(valueOf, Integer.valueOf((int) R.string.bot_requires_email_verification)), o.to(Integer.valueOf((int) RestAPIAbortCodes.INVALID_MESSAGE_SEND_USER), Integer.valueOf((int) R.string.bot_dm_send_failed)), o.to(Integer.valueOf((int) RestAPIAbortCodes.RATE_LIMIT_DM_OPEN), Integer.valueOf((int) R.string.bot_dm_rate_limited)), o.to(Integer.valueOf((int) RestAPIAbortCodes.SEND_MESSAGE_TEMPORARILY_DISABLED), Integer.valueOf((int) R.string.bot_dm_send_message_temporarily_disabled)), o.to(valueOf, Integer.valueOf((int) R.string.add_friend_error_invalid_discord_tag)), o.to(Integer.valueOf((int) RestAPIAbortCodes.TOO_MANY_FRIENDS), Integer.valueOf((int) R.string.add_friend_error_too_many_friends)), o.to(valueOf2, valueOf3), o.to(Integer.valueOf((int) RestAPIAbortCodes.RELATIONSHIP_INVALID_SELF), valueOf3), o.to(Integer.valueOf((int) RestAPIAbortCodes.RELATIONSHIP_INVALID_USER_BOT), valueOf3), o.to(Integer.valueOf((int) RestAPIAbortCodes.RELATIONSHIP_INVALID_DISCORD_TAG), valueOf3), o.to(Integer.valueOf((int) RestAPIAbortCodes.RELATIONSHIP_ALREADY_FRIENDS), Integer.valueOf((int) R.string.add_friend_error_already_friends)), o.to(Integer.valueOf((int) RestAPIAbortCodes.TOO_MANY_THREADS), Integer.valueOf((int) R.string.too_many_threads_message)), o.to(Integer.valueOf((int) RestAPIAbortCodes.TOO_MANY_ANNOUNCEMENT_THREADS), Integer.valueOf((int) R.string.too_many_announcement_threads_message)), o.to(Integer.valueOf((int) RestAPIAbortCodes.SLOWMODE_RATE_LIMITED), Integer.valueOf((int) R.string.channel_slowmode_desc_short)));
    }

    private RestAPIAbortMessages() {
    }

    public static final Integer getAbortCodeMessageResId(int i) {
        return MESSAGES.get(Integer.valueOf(i));
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void handleAbortCodeOrDefault$default(RestAPIAbortMessages restAPIAbortMessages, Error error, Function0 function0, Function0 function02, int i, Object obj) {
        if ((i & 4) != 0) {
            function02 = RestAPIAbortMessages$handleAbortCodeOrDefault$1.INSTANCE;
        }
        restAPIAbortMessages.handleAbortCodeOrDefault(error, function0, function02);
    }

    public final void handleAbortCodeOrDefault(Error error, Function0<Unit> function0, Function0<Boolean> function02) {
        m.checkNotNullParameter(error, "errorResponse");
        m.checkNotNullParameter(function0, "onHandle");
        m.checkNotNullParameter(function02, "onDefault");
        if (error.getType() == Error.Type.DISCORD_BAD_REQUEST) {
            error.setShowErrorToasts(false);
            function0.invoke();
            return;
        }
        error.setShowErrorToasts(function02.invoke().booleanValue());
    }
}
