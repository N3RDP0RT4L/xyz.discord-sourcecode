package com.discord.utilities.rest;

import andhook.lib.HookHelper;
import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.utilities.attachments.AttachmentUtilsKt;
import com.lytefast.flexinput.model.Attachment;
import d0.t.u;
import d0.y.b;
import d0.z.d.m;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: SendUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u0000 \u001e2\u00020\u0001:\u0001\u001eB3\u0012\u0010\u0010\n\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00030\u0002\u0012\u0010\u0010\u000b\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00030\u0002\u0012\u0006\u0010\f\u001a\u00020\u0007¢\u0006\u0004\b\u001c\u0010\u001dJ\u001a\u0010\u0004\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u001a\u0010\u0006\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0005J\u0010\u0010\b\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJB\u0010\r\u001a\u00020\u00002\u0012\b\u0002\u0010\n\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00030\u00022\u0012\b\u0002\u0010\u000b\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00030\u00022\b\b\u0002\u0010\f\u001a\u00020\u0007HÆ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\u000f\u0010\tJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u0019\u0010\f\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0017\u001a\u0004\b\u0018\u0010\tR#\u0010\n\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0019\u001a\u0004\b\u001a\u0010\u0005R#\u0010\u000b\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0019\u001a\u0004\b\u001b\u0010\u0005¨\u0006\u001f"}, d2 = {"Lcom/discord/utilities/rest/ProcessedMessageContent;", "", "", "Lcom/lytefast/flexinput/model/Attachment;", "component1", "()Ljava/util/List;", "component2", "", "component3", "()Ljava/lang/String;", "validAttachments", "invalidAttachments", "content", "copy", "(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Lcom/discord/utilities/rest/ProcessedMessageContent;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getContent", "Ljava/util/List;", "getValidAttachments", "getInvalidAttachments", HookHelper.constructorName, "(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ProcessedMessageContent {
    public static final Companion Companion = new Companion(null);
    private final String content;
    private final List<Attachment<?>> invalidAttachments;
    private final List<Attachment<?>> validAttachments;

    /* compiled from: SendUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ/\u0010\u0011\u001a\u00020\u00102\u0010\u0010\u000b\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\n0\t2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u000e¢\u0006\u0004\b\u0011\u0010\u0012¨\u0006\u0015"}, d2 = {"Lcom/discord/utilities/rest/ProcessedMessageContent$Companion;", "", "Landroid/net/Uri;", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "Landroid/content/ContentResolver;", "contentResolver", "", "isUriValid", "(Landroid/net/Uri;Landroid/content/ContentResolver;)Z", "", "Lcom/lytefast/flexinput/model/Attachment;", "attachments", "", "content", "Landroid/content/Context;", "context", "Lcom/discord/utilities/rest/ProcessedMessageContent;", "fromAttachments", "(Ljava/util/List;Ljava/lang/String;Landroid/content/Context;)Lcom/discord/utilities/rest/ProcessedMessageContent;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        private final boolean isUriValid(Uri uri, ContentResolver contentResolver) {
            try {
                InputStream openInputStream = contentResolver.openInputStream(uri);
                if (openInputStream != null) {
                    b.closeFinally(openInputStream, null);
                }
                return true;
            } catch (Exception unused) {
                return false;
            }
        }

        public final ProcessedMessageContent fromAttachments(List<? extends Attachment<?>> list, String str, Context context) {
            m.checkNotNullParameter(list, "attachments");
            m.checkNotNullParameter(str, "content");
            m.checkNotNullParameter(context, "context");
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ContentResolver contentResolver = context.getContentResolver();
            m.checkNotNullExpressionValue(contentResolver, "context.contentResolver");
            List<Attachment<?>> extractLinks = AttachmentUtilsKt.extractLinks(list, contentResolver);
            String appendLinks = AttachmentUtilsKt.appendLinks(str, extractLinks);
            for (Attachment attachment : u.minus((Iterable) list, (Iterable) extractLinks)) {
                Uri uri = attachment.getUri();
                ContentResolver contentResolver2 = context.getContentResolver();
                m.checkNotNullExpressionValue(contentResolver2, "context.contentResolver");
                if (isUriValid(uri, contentResolver2)) {
                    arrayList.add(attachment);
                } else {
                    arrayList2.add(attachment);
                }
            }
            return new ProcessedMessageContent(arrayList, arrayList2, appendLinks);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public ProcessedMessageContent(List<? extends Attachment<?>> list, List<? extends Attachment<?>> list2, String str) {
        m.checkNotNullParameter(list, "validAttachments");
        m.checkNotNullParameter(list2, "invalidAttachments");
        m.checkNotNullParameter(str, "content");
        this.validAttachments = list;
        this.invalidAttachments = list2;
        this.content = str;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ ProcessedMessageContent copy$default(ProcessedMessageContent processedMessageContent, List list, List list2, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            list = processedMessageContent.validAttachments;
        }
        if ((i & 2) != 0) {
            list2 = processedMessageContent.invalidAttachments;
        }
        if ((i & 4) != 0) {
            str = processedMessageContent.content;
        }
        return processedMessageContent.copy(list, list2, str);
    }

    public final List<Attachment<?>> component1() {
        return this.validAttachments;
    }

    public final List<Attachment<?>> component2() {
        return this.invalidAttachments;
    }

    public final String component3() {
        return this.content;
    }

    public final ProcessedMessageContent copy(List<? extends Attachment<?>> list, List<? extends Attachment<?>> list2, String str) {
        m.checkNotNullParameter(list, "validAttachments");
        m.checkNotNullParameter(list2, "invalidAttachments");
        m.checkNotNullParameter(str, "content");
        return new ProcessedMessageContent(list, list2, str);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ProcessedMessageContent)) {
            return false;
        }
        ProcessedMessageContent processedMessageContent = (ProcessedMessageContent) obj;
        return m.areEqual(this.validAttachments, processedMessageContent.validAttachments) && m.areEqual(this.invalidAttachments, processedMessageContent.invalidAttachments) && m.areEqual(this.content, processedMessageContent.content);
    }

    public final String getContent() {
        return this.content;
    }

    public final List<Attachment<?>> getInvalidAttachments() {
        return this.invalidAttachments;
    }

    public final List<Attachment<?>> getValidAttachments() {
        return this.validAttachments;
    }

    public int hashCode() {
        List<Attachment<?>> list = this.validAttachments;
        int i = 0;
        int hashCode = (list != null ? list.hashCode() : 0) * 31;
        List<Attachment<?>> list2 = this.invalidAttachments;
        int hashCode2 = (hashCode + (list2 != null ? list2.hashCode() : 0)) * 31;
        String str = this.content;
        if (str != null) {
            i = str.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("ProcessedMessageContent(validAttachments=");
        R.append(this.validAttachments);
        R.append(", invalidAttachments=");
        R.append(this.invalidAttachments);
        R.append(", content=");
        return a.H(R, this.content, ")");
    }
}
