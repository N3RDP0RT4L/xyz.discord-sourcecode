package com.discord.utilities.rest;

import andhook.lib.HookHelper;
import android.content.ContentResolver;
import b.d.b.a.a;
import com.discord.app.AppLog;
import com.discord.utilities.attachments.AttachmentUtilsKt;
import com.lytefast.flexinput.model.Attachment;
import d0.y.b;
import d0.z.d.m;
import g0.n;
import g0.y;
import java.io.File;
import java.io.IOException;
import kotlin.Metadata;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.BufferedSink;
/* compiled from: AttachmentRequestBody.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0013\u001a\u00020\u0012\u0012\n\u0010\u0016\u001a\u0006\u0012\u0002\b\u00030\u0015¢\u0006\u0004\b\u0018\u0010\u0019J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u0011\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0016¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\u000b\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\bH\u0016¢\u0006\u0004\b\u000b\u0010\fJ\u000f\u0010\u000e\u001a\u00020\rH\u0016¢\u0006\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0010\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0016\u001a\u0006\u0012\u0002\b\u00030\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017¨\u0006\u001a"}, d2 = {"Lcom/discord/utilities/rest/AttachmentRequestBody;", "Lokhttp3/RequestBody;", "", "contentLength", "()J", "Lokhttp3/MediaType;", "contentType", "()Lokhttp3/MediaType;", "Lokio/BufferedSink;", "sink", "", "writeTo", "(Lokio/BufferedSink;)V", "", "toString", "()Ljava/lang/String;", "size", "J", "Landroid/content/ContentResolver;", "contentResolver", "Landroid/content/ContentResolver;", "Lcom/lytefast/flexinput/model/Attachment;", "attachment", "Lcom/lytefast/flexinput/model/Attachment;", HookHelper.constructorName, "(Landroid/content/ContentResolver;Lcom/lytefast/flexinput/model/Attachment;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class AttachmentRequestBody extends RequestBody {
    private final Attachment<?> attachment;
    private final ContentResolver contentResolver;
    private final long size;

    public AttachmentRequestBody(ContentResolver contentResolver, Attachment<?> attachment) {
        m.checkNotNullParameter(contentResolver, "contentResolver");
        m.checkNotNullParameter(attachment, "attachment");
        this.contentResolver = contentResolver;
        this.attachment = attachment;
        Object data = attachment.getData();
        File file = (File) (!(data instanceof File) ? null : data);
        this.size = file != null ? file.length() : SendUtilsKt.computeFileSizeBytes(attachment.getUri(), contentResolver);
    }

    @Override // okhttp3.RequestBody
    public long contentLength() {
        return this.size;
    }

    @Override // okhttp3.RequestBody
    public MediaType contentType() {
        MediaType.a aVar = MediaType.c;
        return MediaType.a.b(AttachmentUtilsKt.getMimeType(this.attachment, this.contentResolver));
    }

    public String toString() {
        StringBuilder R = a.R("AttachmentRequestBody(attachment=");
        R.append(this.attachment);
        R.append(", size=");
        R.append(this.size);
        R.append(')');
        return R.toString();
    }

    @Override // okhttp3.RequestBody
    public void writeTo(BufferedSink bufferedSink) throws IOException {
        m.checkNotNullParameter(bufferedSink, "sink");
        try {
            n openInputStream = this.contentResolver.openInputStream(this.attachment.getUri());
            if (openInputStream != null) {
                m.checkNotNullExpressionValue(openInputStream, "inputStream");
                m.checkParameterIsNotNull(openInputStream, "$this$source");
                openInputStream = new n(openInputStream, new y());
                try {
                    bufferedSink.P(openInputStream);
                    th = null;
                    b.closeFinally(openInputStream, th);
                } catch (Throwable th) {
                    try {
                        throw th;
                    } finally {
                        b.closeFinally(openInputStream, th);
                    }
                }
            }
        } catch (IOException e) {
            AppLog.g.i("Could not write to sink", e);
            throw e;
        }
    }
}
