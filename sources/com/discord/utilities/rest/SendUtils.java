package com.discord.utilities.rest;

import a0.a.a.b;
import andhook.lib.HookHelper;
import andhook.lib.xposed.ClassUtils;
import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import androidx.annotation.MainThread;
import androidx.annotation.VisibleForTesting;
import androidx.exifinterface.media.ExifInterface;
import b.d.b.a.a;
import b.i.a.f.e.o.f;
import com.discord.api.commands.ApplicationCommandType;
import com.discord.models.commands.ApplicationCommandLocalSendData;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.restapi.RestAPIParams;
import com.discord.restapi.utils.CountingRequestBody;
import com.discord.utilities.attachments.AttachmentUtilsKt;
import com.discord.utilities.captcha.CaptchaErrorBody;
import com.discord.utilities.error.Error;
import com.discord.utilities.messagesend.MessageResult;
import com.discord.utilities.rest.SendUtils;
import com.discord.widgets.captcha.WidgetCaptchaBottomSheet;
import com.discord.widgets.chat.input.models.ApplicationCommandValue;
import com.lytefast.flexinput.model.Attachment;
import d0.f0.l;
import d0.g0.w;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import j0.l.a.q;
import j0.l.e.k;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlinx.coroutines.CoroutineDispatcher;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Emitter;
import rx.Observable;
import rx.functions.Action1;
import s.a.a.n;
import s.a.k0;
import s.a.x0;
import xyz.discord.R;
/* compiled from: SendUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000b\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002<=B\t\b\u0002¢\u0006\u0004\b:\u0010;J)\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\u0010\u0010\u0006\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00050\u0004H\u0002¢\u0006\u0004\b\b\u0010\tJ!\u0010\f\u001a\u0010\u0012\u0004\u0012\u00020\n\u0012\u0006\u0012\u0004\u0018\u00010\n0\u000b*\u00020\nH\u0002¢\u0006\u0004\b\f\u0010\rJK\u0010\u0012\u001a&\u0012\f\u0012\n \u0011*\u0004\u0018\u00010\u00100\u0010 \u0011*\u0012\u0012\f\u0012\n \u0011*\u0004\u0018\u00010\u00100\u0010\u0018\u00010\u000f0\u000f*\u0006\u0012\u0002\b\u00030\u00052\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u000e\u001a\u00020\nH\u0002¢\u0006\u0004\b\u0012\u0010\u0013J7\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00170\u000f2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u00142\u0012\u0010\u0016\u001a\u000e\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u0005\u0018\u00010\u0004¢\u0006\u0004\b\u0018\u0010\u0019J7\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00170\u000f2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u001a2\u0012\u0010\u0016\u001a\u000e\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u0005\u0018\u00010\u0004¢\u0006\u0004\b\u001b\u0010\u001cJ+\u0010\u001e\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00050\u00042\u0010\u0010\u001d\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00050\u0004H\u0007¢\u0006\u0004\b\u001e\u0010\u001fJ-\u0010#\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005\"\u0004\b\u0000\u0010 *\b\u0012\u0004\u0012\u00028\u00000\u00052\u0006\u0010\"\u001a\u00020!H\u0007¢\u0006\u0004\b#\u0010$JC\u0010-\u001a\u00020*2\u0006\u0010&\u001a\u00020%2\u0006\u0010(\u001a\u00020'2\u0010\b\u0002\u0010+\u001a\n\u0012\u0004\u0012\u00020*\u0018\u00010)2\u0010\b\u0002\u0010,\u001a\n\u0012\u0004\u0012\u00020*\u0018\u00010)H\u0007¢\u0006\u0004\b-\u0010.J\u0015\u00101\u001a\u00020*2\u0006\u00100\u001a\u00020/¢\u0006\u0004\b1\u00102JE\u00105\u001a\u00020*2\u0006\u0010(\u001a\u00020'2\u0010\u0010\u0016\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00050\u00042\u001c\u00104\u001a\u0018\u0012\u000e\u0012\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00050\u0004\u0012\u0004\u0012\u00020*03¢\u0006\u0004\b5\u00106R\u0016\u00107\u001a\u00020!8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b7\u00108R\u0016\u00109\u001a\u00020!8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b9\u00108¨\u0006>"}, d2 = {"Lcom/discord/utilities/rest/SendUtils;", "", "Landroid/content/ContentResolver;", "contentResolver", "", "Lcom/lytefast/flexinput/model/Attachment;", "fileAttachments", "Lcom/discord/utilities/rest/SendUtils$SendPayload$Preprocessing;", "getPreprocessingFromAttachments", "(Landroid/content/ContentResolver;Ljava/util/List;)Lcom/discord/utilities/rest/SendUtils$SendPayload$Preprocessing;", "", "Lkotlin/Pair;", "splitFileExtension", "(Ljava/lang/String;)Lkotlin/Pair;", ModelAuditLogEntry.CHANGE_KEY_NAME, "Lrx/Observable;", "Lcom/discord/utilities/rest/SendUtils$FileUpload;", "kotlin.jvm.PlatformType", "getPart", "(Lcom/lytefast/flexinput/model/Attachment;Landroid/content/ContentResolver;Ljava/lang/String;)Lrx/Observable;", "Lcom/discord/restapi/RestAPIParams$Message;", "apiParamMessage", "attachments", "Lcom/discord/utilities/rest/SendUtils$SendPayload;", "getSendPayload", "(Landroid/content/ContentResolver;Lcom/discord/restapi/RestAPIParams$Message;Ljava/util/List;)Lrx/Observable;", "Lcom/discord/models/commands/ApplicationCommandLocalSendData;", "getSendCommandPayload", "(Landroid/content/ContentResolver;Lcom/discord/models/commands/ApplicationCommandLocalSendData;Ljava/util/List;)Lrx/Observable;", "attachmentParts", "uniqueifyNames", "(Ljava/util/List;)Ljava/util/List;", ExifInterface.GPS_DIRECTION_TRUE, "", "count", "renamedWithDuplicateCount", "(Lcom/lytefast/flexinput/model/Attachment;I)Lcom/lytefast/flexinput/model/Attachment;", "Lcom/discord/utilities/error/Error;", "error", "Landroid/content/Context;", "context", "Lkotlin/Function0;", "", "filesTooLargeCallback", "failedDeliveryToRecipientCallback", "handleSendError", "(Lcom/discord/utilities/error/Error;Landroid/content/Context;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V", "Lcom/discord/utilities/messagesend/MessageResult$CaptchaRequired;", "messageResult", "handleCaptchaRequired", "(Lcom/discord/utilities/messagesend/MessageResult$CaptchaRequired;)V", "Lkotlin/Function1;", "onCompressed", "compressImageAttachments", "(Landroid/content/Context;Ljava/util/List;Lkotlin/jvm/functions/Function1;)V", "MAX_MESSAGE_CHARACTER_COUNT", "I", "MAX_MESSAGE_CHARACTER_COUNT_PREMIUM", HookHelper.constructorName, "()V", "FileUpload", "SendPayload", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SendUtils {
    public static final SendUtils INSTANCE = new SendUtils();
    public static final int MAX_MESSAGE_CHARACTER_COUNT = 2000;
    public static final int MAX_MESSAGE_CHARACTER_COUNT_PREMIUM = 4000;

    /* compiled from: SendUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000f\b\u0086\b\u0018\u0000 )2\u00020\u0001:\u0001)B5\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\u0006\u0010\u0010\u001a\u00020\u0005\u0012\u0006\u0010\u0011\u001a\u00020\b\u0012\u0006\u0010\u0012\u001a\u00020\u0002\u0012\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00050\f¢\u0006\u0004\b'\u0010(J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u000b\u0010\u0004J\u0016\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00050\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJH\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u000f\u001a\u00020\u00022\b\b\u0002\u0010\u0010\u001a\u00020\u00052\b\b\u0002\u0010\u0011\u001a\u00020\b2\b\b\u0002\u0010\u0012\u001a\u00020\u00022\u000e\b\u0002\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00050\fHÆ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0004J\u0010\u0010\u0018\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u001a\u0010\u001c\u001a\u00020\u001b2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u0019\u0010\u0010\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001e\u001a\u0004\b\u001f\u0010\u0007R\u0019\u0010\u0011\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010 \u001a\u0004\b!\u0010\nR\u0019\u0010\u0012\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\"\u001a\u0004\b#\u0010\u0004R\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\"\u001a\u0004\b$\u0010\u0004R\u001f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00050\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010%\u001a\u0004\b&\u0010\u000e¨\u0006*"}, d2 = {"Lcom/discord/utilities/rest/SendUtils$FileUpload;", "", "", "component1", "()Ljava/lang/String;", "", "component2", "()J", "Lokhttp3/MultipartBody$Part;", "component3", "()Lokhttp3/MultipartBody$Part;", "component4", "Lrx/Observable;", "component5", "()Lrx/Observable;", ModelAuditLogEntry.CHANGE_KEY_NAME, "contentLength", "part", "mimeType", "bytesWrittenObservable", "copy", "(Ljava/lang/String;JLokhttp3/MultipartBody$Part;Ljava/lang/String;Lrx/Observable;)Lcom/discord/utilities/rest/SendUtils$FileUpload;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getContentLength", "Lokhttp3/MultipartBody$Part;", "getPart", "Ljava/lang/String;", "getMimeType", "getName", "Lrx/Observable;", "getBytesWrittenObservable", HookHelper.constructorName, "(Ljava/lang/String;JLokhttp3/MultipartBody$Part;Ljava/lang/String;Lrx/Observable;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class FileUpload {
        public static final Companion Companion = new Companion(null);
        public static final long SIZE_UNKNOWN = -1;
        private final Observable<Long> bytesWrittenObservable;
        private final long contentLength;
        private final String mimeType;
        private final String name;
        private final MultipartBody.Part part;

        /* compiled from: SendUtils.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/utilities/rest/SendUtils$FileUpload$Companion;", "", "", "SIZE_UNKNOWN", "J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public FileUpload(String str, long j, MultipartBody.Part part, String str2, Observable<Long> observable) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkNotNullParameter(part, "part");
            m.checkNotNullParameter(str2, "mimeType");
            m.checkNotNullParameter(observable, "bytesWrittenObservable");
            this.name = str;
            this.contentLength = j;
            this.part = part;
            this.mimeType = str2;
            this.bytesWrittenObservable = observable;
        }

        public static /* synthetic */ FileUpload copy$default(FileUpload fileUpload, String str, long j, MultipartBody.Part part, String str2, Observable observable, int i, Object obj) {
            if ((i & 1) != 0) {
                str = fileUpload.name;
            }
            if ((i & 2) != 0) {
                j = fileUpload.contentLength;
            }
            long j2 = j;
            if ((i & 4) != 0) {
                part = fileUpload.part;
            }
            MultipartBody.Part part2 = part;
            if ((i & 8) != 0) {
                str2 = fileUpload.mimeType;
            }
            String str3 = str2;
            Observable<Long> observable2 = observable;
            if ((i & 16) != 0) {
                observable2 = fileUpload.bytesWrittenObservable;
            }
            return fileUpload.copy(str, j2, part2, str3, observable2);
        }

        public final String component1() {
            return this.name;
        }

        public final long component2() {
            return this.contentLength;
        }

        public final MultipartBody.Part component3() {
            return this.part;
        }

        public final String component4() {
            return this.mimeType;
        }

        public final Observable<Long> component5() {
            return this.bytesWrittenObservable;
        }

        public final FileUpload copy(String str, long j, MultipartBody.Part part, String str2, Observable<Long> observable) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkNotNullParameter(part, "part");
            m.checkNotNullParameter(str2, "mimeType");
            m.checkNotNullParameter(observable, "bytesWrittenObservable");
            return new FileUpload(str, j, part, str2, observable);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof FileUpload)) {
                return false;
            }
            FileUpload fileUpload = (FileUpload) obj;
            return m.areEqual(this.name, fileUpload.name) && this.contentLength == fileUpload.contentLength && m.areEqual(this.part, fileUpload.part) && m.areEqual(this.mimeType, fileUpload.mimeType) && m.areEqual(this.bytesWrittenObservable, fileUpload.bytesWrittenObservable);
        }

        public final Observable<Long> getBytesWrittenObservable() {
            return this.bytesWrittenObservable;
        }

        public final long getContentLength() {
            return this.contentLength;
        }

        public final String getMimeType() {
            return this.mimeType;
        }

        public final String getName() {
            return this.name;
        }

        public final MultipartBody.Part getPart() {
            return this.part;
        }

        public int hashCode() {
            String str = this.name;
            int i = 0;
            int a = (b.a(this.contentLength) + ((str != null ? str.hashCode() : 0) * 31)) * 31;
            MultipartBody.Part part = this.part;
            int hashCode = (a + (part != null ? part.hashCode() : 0)) * 31;
            String str2 = this.mimeType;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            Observable<Long> observable = this.bytesWrittenObservable;
            if (observable != null) {
                i = observable.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("FileUpload(name=");
            R.append(this.name);
            R.append(", contentLength=");
            R.append(this.contentLength);
            R.append(", part=");
            R.append(this.part);
            R.append(", mimeType=");
            R.append(this.mimeType);
            R.append(", bytesWrittenObservable=");
            R.append(this.bytesWrittenObservable);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: SendUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/utilities/rest/SendUtils$SendPayload;", "", HookHelper.constructorName, "()V", "Preprocessing", "ReadyToSend", "ReadyToSendCommand", "Lcom/discord/utilities/rest/SendUtils$SendPayload$Preprocessing;", "Lcom/discord/utilities/rest/SendUtils$SendPayload$ReadyToSend;", "Lcom/discord/utilities/rest/SendUtils$SendPayload$ReadyToSendCommand;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class SendPayload {

        /* compiled from: SendUtils.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\n\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\b\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J2\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\t\u001a\u00020\u00022\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u000e\u0010\u0007J\u0010\u0010\u000f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0004J\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u001b\u0010\n\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0015\u001a\u0004\b\u0016\u0010\u0007R\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0004R\u001b\u0010\u000b\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0015\u001a\u0004\b\u0019\u0010\u0007¨\u0006\u001c"}, d2 = {"Lcom/discord/utilities/rest/SendUtils$SendPayload$Preprocessing;", "Lcom/discord/utilities/rest/SendUtils$SendPayload;", "", "component1", "()I", "", "component2", "()Ljava/lang/String;", "component3", "numFiles", ModelAuditLogEntry.CHANGE_KEY_NAME, "mimeType", "copy", "(ILjava/lang/String;Ljava/lang/String;)Lcom/discord/utilities/rest/SendUtils$SendPayload$Preprocessing;", "toString", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getName", "I", "getNumFiles", "getMimeType", HookHelper.constructorName, "(ILjava/lang/String;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Preprocessing extends SendPayload {
            private final String mimeType;
            private final String name;
            private final int numFiles;

            public Preprocessing(int i, String str, String str2) {
                super(null);
                this.numFiles = i;
                this.name = str;
                this.mimeType = str2;
            }

            public static /* synthetic */ Preprocessing copy$default(Preprocessing preprocessing, int i, String str, String str2, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = preprocessing.numFiles;
                }
                if ((i2 & 2) != 0) {
                    str = preprocessing.name;
                }
                if ((i2 & 4) != 0) {
                    str2 = preprocessing.mimeType;
                }
                return preprocessing.copy(i, str, str2);
            }

            public final int component1() {
                return this.numFiles;
            }

            public final String component2() {
                return this.name;
            }

            public final String component3() {
                return this.mimeType;
            }

            public final Preprocessing copy(int i, String str, String str2) {
                return new Preprocessing(i, str, str2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Preprocessing)) {
                    return false;
                }
                Preprocessing preprocessing = (Preprocessing) obj;
                return this.numFiles == preprocessing.numFiles && m.areEqual(this.name, preprocessing.name) && m.areEqual(this.mimeType, preprocessing.mimeType);
            }

            public final String getMimeType() {
                return this.mimeType;
            }

            public final String getName() {
                return this.name;
            }

            public final int getNumFiles() {
                return this.numFiles;
            }

            public int hashCode() {
                int i = this.numFiles * 31;
                String str = this.name;
                int i2 = 0;
                int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
                String str2 = this.mimeType;
                if (str2 != null) {
                    i2 = str2.hashCode();
                }
                return hashCode + i2;
            }

            public String toString() {
                StringBuilder R = a.R("Preprocessing(numFiles=");
                R.append(this.numFiles);
                R.append(", name=");
                R.append(this.name);
                R.append(", mimeType=");
                return a.H(R, this.mimeType, ")");
            }
        }

        /* compiled from: SendUtils.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ*\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\t\u001a\u00020\u00022\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u001f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0018\u001a\u0004\b\u0019\u0010\bR\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u001a\u001a\u0004\b\u001b\u0010\u0004¨\u0006\u001e"}, d2 = {"Lcom/discord/utilities/rest/SendUtils$SendPayload$ReadyToSend;", "Lcom/discord/utilities/rest/SendUtils$SendPayload;", "Lcom/discord/restapi/RestAPIParams$Message;", "component1", "()Lcom/discord/restapi/RestAPIParams$Message;", "", "Lcom/discord/utilities/rest/SendUtils$FileUpload;", "component2", "()Ljava/util/List;", "message", "uploads", "copy", "(Lcom/discord/restapi/RestAPIParams$Message;Ljava/util/List;)Lcom/discord/utilities/rest/SendUtils$SendPayload$ReadyToSend;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getUploads", "Lcom/discord/restapi/RestAPIParams$Message;", "getMessage", HookHelper.constructorName, "(Lcom/discord/restapi/RestAPIParams$Message;Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ReadyToSend extends SendPayload {
            private final RestAPIParams.Message message;
            private final List<FileUpload> uploads;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public ReadyToSend(RestAPIParams.Message message, List<FileUpload> list) {
                super(null);
                m.checkNotNullParameter(message, "message");
                m.checkNotNullParameter(list, "uploads");
                this.message = message;
                this.uploads = list;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ ReadyToSend copy$default(ReadyToSend readyToSend, RestAPIParams.Message message, List list, int i, Object obj) {
                if ((i & 1) != 0) {
                    message = readyToSend.message;
                }
                if ((i & 2) != 0) {
                    list = readyToSend.uploads;
                }
                return readyToSend.copy(message, list);
            }

            public final RestAPIParams.Message component1() {
                return this.message;
            }

            public final List<FileUpload> component2() {
                return this.uploads;
            }

            public final ReadyToSend copy(RestAPIParams.Message message, List<FileUpload> list) {
                m.checkNotNullParameter(message, "message");
                m.checkNotNullParameter(list, "uploads");
                return new ReadyToSend(message, list);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof ReadyToSend)) {
                    return false;
                }
                ReadyToSend readyToSend = (ReadyToSend) obj;
                return m.areEqual(this.message, readyToSend.message) && m.areEqual(this.uploads, readyToSend.uploads);
            }

            public final RestAPIParams.Message getMessage() {
                return this.message;
            }

            public final List<FileUpload> getUploads() {
                return this.uploads;
            }

            public int hashCode() {
                RestAPIParams.Message message = this.message;
                int i = 0;
                int hashCode = (message != null ? message.hashCode() : 0) * 31;
                List<FileUpload> list = this.uploads;
                if (list != null) {
                    i = list.hashCode();
                }
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("ReadyToSend(message=");
                R.append(this.message);
                R.append(", uploads=");
                return a.K(R, this.uploads, ")");
            }
        }

        /* compiled from: SendUtils.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ*\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\t\u001a\u00020\u00022\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u001f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0018\u001a\u0004\b\u0019\u0010\bR\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u001a\u001a\u0004\b\u001b\u0010\u0004¨\u0006\u001e"}, d2 = {"Lcom/discord/utilities/rest/SendUtils$SendPayload$ReadyToSendCommand;", "Lcom/discord/utilities/rest/SendUtils$SendPayload;", "Lcom/discord/models/commands/ApplicationCommandLocalSendData;", "component1", "()Lcom/discord/models/commands/ApplicationCommandLocalSendData;", "", "Lcom/discord/utilities/rest/SendUtils$FileUpload;", "component2", "()Ljava/util/List;", "command", "uploads", "copy", "(Lcom/discord/models/commands/ApplicationCommandLocalSendData;Ljava/util/List;)Lcom/discord/utilities/rest/SendUtils$SendPayload$ReadyToSendCommand;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getUploads", "Lcom/discord/models/commands/ApplicationCommandLocalSendData;", "getCommand", HookHelper.constructorName, "(Lcom/discord/models/commands/ApplicationCommandLocalSendData;Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ReadyToSendCommand extends SendPayload {
            private final ApplicationCommandLocalSendData command;
            private final List<FileUpload> uploads;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public ReadyToSendCommand(ApplicationCommandLocalSendData applicationCommandLocalSendData, List<FileUpload> list) {
                super(null);
                m.checkNotNullParameter(applicationCommandLocalSendData, "command");
                m.checkNotNullParameter(list, "uploads");
                this.command = applicationCommandLocalSendData;
                this.uploads = list;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ ReadyToSendCommand copy$default(ReadyToSendCommand readyToSendCommand, ApplicationCommandLocalSendData applicationCommandLocalSendData, List list, int i, Object obj) {
                if ((i & 1) != 0) {
                    applicationCommandLocalSendData = readyToSendCommand.command;
                }
                if ((i & 2) != 0) {
                    list = readyToSendCommand.uploads;
                }
                return readyToSendCommand.copy(applicationCommandLocalSendData, list);
            }

            public final ApplicationCommandLocalSendData component1() {
                return this.command;
            }

            public final List<FileUpload> component2() {
                return this.uploads;
            }

            public final ReadyToSendCommand copy(ApplicationCommandLocalSendData applicationCommandLocalSendData, List<FileUpload> list) {
                m.checkNotNullParameter(applicationCommandLocalSendData, "command");
                m.checkNotNullParameter(list, "uploads");
                return new ReadyToSendCommand(applicationCommandLocalSendData, list);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof ReadyToSendCommand)) {
                    return false;
                }
                ReadyToSendCommand readyToSendCommand = (ReadyToSendCommand) obj;
                return m.areEqual(this.command, readyToSendCommand.command) && m.areEqual(this.uploads, readyToSendCommand.uploads);
            }

            public final ApplicationCommandLocalSendData getCommand() {
                return this.command;
            }

            public final List<FileUpload> getUploads() {
                return this.uploads;
            }

            public int hashCode() {
                ApplicationCommandLocalSendData applicationCommandLocalSendData = this.command;
                int i = 0;
                int hashCode = (applicationCommandLocalSendData != null ? applicationCommandLocalSendData.hashCode() : 0) * 31;
                List<FileUpload> list = this.uploads;
                if (list != null) {
                    i = list.hashCode();
                }
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("ReadyToSendCommand(command=");
                R.append(this.command);
                R.append(", uploads=");
                return a.K(R, this.uploads, ")");
            }
        }

        private SendPayload() {
        }

        public /* synthetic */ SendPayload(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    private SendUtils() {
    }

    private final Observable<FileUpload> getPart(final Attachment<?> attachment, final ContentResolver contentResolver, final String str) {
        return Observable.n(new Action1<Emitter<FileUpload>>() { // from class: com.discord.utilities.rest.SendUtils$getPart$1

            /* compiled from: SendUtils.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\t\u001a\u00020\u00062\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"Lokhttp3/RequestBody;", "requestBody", "", "mimeType", "Landroid/graphics/Bitmap$CompressFormat;", "compressFormat", "Lcom/discord/utilities/rest/SendUtils$FileUpload;", "invoke", "(Lokhttp3/RequestBody;Ljava/lang/String;Landroid/graphics/Bitmap$CompressFormat;)Lcom/discord/utilities/rest/SendUtils$FileUpload;", "getFileUpload"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.utilities.rest.SendUtils$getPart$1$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends o implements Function3<RequestBody, String, Bitmap.CompressFormat, SendUtils.FileUpload> {
                public AnonymousClass1() {
                    super(3);
                }

                public final SendUtils.FileUpload invoke(RequestBody requestBody, String str, Bitmap.CompressFormat compressFormat) {
                    m.checkNotNullParameter(requestBody, "requestBody");
                    m.checkNotNullParameter(str, "mimeType");
                    CountingRequestBody countingRequestBody = new CountingRequestBody(requestBody, 0);
                    String sanitizedFileName = AttachmentUtilsKt.getSanitizedFileName(Attachment.Companion.a(Attachment.this), compressFormat);
                    return new SendUtils.FileUpload(sanitizedFileName, countingRequestBody.getEstimatedContentLength(), MultipartBody.Part.b(str, sanitizedFileName, countingRequestBody), str, countingRequestBody.getBytesWrittenObservable());
                }
            }

            public final void call(Emitter<SendUtils.FileUpload> emitter) {
                emitter.onNext(new AnonymousClass1().invoke((RequestBody) new AttachmentRequestBody(contentResolver, Attachment.this), AttachmentUtilsKt.getMimeType(Attachment.this, contentResolver), (Bitmap.CompressFormat) null));
                emitter.onCompleted();
            }
        }, Emitter.BackpressureMode.BUFFER);
    }

    private final SendPayload.Preprocessing getPreprocessingFromAttachments(ContentResolver contentResolver, List<? extends Attachment<?>> list) {
        if (list.size() != 1) {
            return new SendPayload.Preprocessing(list.size(), null, null);
        }
        Attachment<?> attachment = (Attachment) u.first((List<? extends Object>) list);
        return new SendPayload.Preprocessing(1, Attachment.Companion.a(attachment), AttachmentUtilsKt.getMimeType(contentResolver, attachment.getUri(), attachment.getDisplayName()));
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void handleSendError$default(SendUtils sendUtils, Error error, Context context, Function0 function0, Function0 function02, int i, Object obj) {
        if ((i & 4) != 0) {
            function0 = null;
        }
        if ((i & 8) != 0) {
            function02 = null;
        }
        sendUtils.handleSendError(error, context, function0, function02);
    }

    private final Pair<String, String> splitFileExtension(String str) {
        int lastIndexOf$default = w.lastIndexOf$default((CharSequence) str, (char) ClassUtils.PACKAGE_SEPARATOR_CHAR, 0, false, 6, (Object) null);
        if (lastIndexOf$default == -1) {
            return d0.o.to(str, null);
        }
        Objects.requireNonNull(str, "null cannot be cast to non-null type java.lang.String");
        String substring = str.substring(0, lastIndexOf$default);
        m.checkNotNullExpressionValue(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        String substring2 = str.substring(lastIndexOf$default + 1);
        m.checkNotNullExpressionValue(substring2, "(this as java.lang.String).substring(startIndex)");
        return d0.o.to(substring, substring2);
    }

    public final void compressImageAttachments(Context context, List<? extends Attachment<?>> list, Function1<? super List<? extends Attachment<?>>, Unit> function1) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(list, "attachments");
        m.checkNotNullParameter(function1, "onCompressed");
        x0 x0Var = x0.j;
        CoroutineDispatcher coroutineDispatcher = k0.a;
        f.H0(x0Var, n.f3802b, null, new SendUtils$compressImageAttachments$1(list, context, function1, null), 2, null);
    }

    public final Observable<SendPayload> getSendCommandPayload(ContentResolver contentResolver, ApplicationCommandLocalSendData applicationCommandLocalSendData, List<? extends Attachment<?>> list) {
        m.checkNotNullParameter(contentResolver, "contentResolver");
        m.checkNotNullParameter(applicationCommandLocalSendData, "apiParamMessage");
        if (list != null) {
            List<Attachment<?>> uniqueifyNames = uniqueifyNames(list);
            List<ApplicationCommandValue> applicationCommandsValues = applicationCommandLocalSendData.getApplicationCommandsValues();
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(applicationCommandsValues, 10));
            for (ApplicationCommandValue applicationCommandValue : applicationCommandsValues) {
                if (applicationCommandValue.getType() == ApplicationCommandType.ATTACHMENT.getType()) {
                    Iterator<T> it = uniqueifyNames.iterator();
                    while (it.hasNext()) {
                        Attachment attachment = (Attachment) it.next();
                        if (m.areEqual(attachment.getUri().toString(), applicationCommandValue.getValue())) {
                            applicationCommandValue = ApplicationCommandValue.copy$default(applicationCommandValue, null, attachment.getDisplayName(), 0, null, null, 29, null);
                        }
                    }
                    throw new NoSuchElementException("Collection contains no element matching the predicate.");
                }
                arrayList.add(applicationCommandValue);
            }
            final ApplicationCommandLocalSendData copy$default = ApplicationCommandLocalSendData.copy$default(applicationCommandLocalSendData, 0L, 0L, null, null, null, null, null, null, arrayList, null, null, 1791, null);
            if (!uniqueifyNames.isEmpty()) {
                k kVar = new k(getPreprocessingFromAttachments(contentResolver, uniqueifyNames));
                ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(uniqueifyNames, 10));
                int i = 0;
                for (Object obj : uniqueifyNames) {
                    i++;
                    if (i < 0) {
                        d0.t.n.throwIndexOverflow();
                    }
                    arrayList2.add(INSTANCE.getPart((Attachment) obj, contentResolver, a.p("file", i)));
                }
                Observable<SendPayload> m = Observable.m(kVar, Observable.l(Observable.h0(new q(arrayList2))).f0().F(new j0.k.b<List<FileUpload>, SendPayload.ReadyToSendCommand>() { // from class: com.discord.utilities.rest.SendUtils$getSendCommandPayload$2
                    public final SendUtils.SendPayload.ReadyToSendCommand call(List<SendUtils.FileUpload> list2) {
                        ApplicationCommandLocalSendData applicationCommandLocalSendData2 = ApplicationCommandLocalSendData.this;
                        m.checkNotNullExpressionValue(list2, "attachmentParts");
                        return new SendUtils.SendPayload.ReadyToSendCommand(applicationCommandLocalSendData2, list2);
                    }
                }));
                m.checkNotNullExpressionValue(m, "Observable.concat(\n     …)\n              }\n      )");
                return m;
            }
            k kVar2 = new k(new SendPayload.ReadyToSendCommand(copy$default, d0.t.n.emptyList()));
            m.checkNotNullExpressionValue(kVar2, "Observable.just(SendPayl…tedMessage, emptyList()))");
            return kVar2;
        }
        k kVar3 = new k(new SendPayload.ReadyToSendCommand(applicationCommandLocalSendData, d0.t.n.emptyList()));
        m.checkNotNullExpressionValue(kVar3, "Observable.just(SendPayl…ramMessage, emptyList()))");
        return kVar3;
    }

    public final Observable<SendPayload> getSendPayload(ContentResolver contentResolver, RestAPIParams.Message message, List<? extends Attachment<?>> list) {
        final RestAPIParams.Message message2 = message;
        m.checkNotNullParameter(contentResolver, "contentResolver");
        m.checkNotNullParameter(message2, "apiParamMessage");
        if (list == null) {
            k kVar = new k(new SendPayload.ReadyToSend(message2, d0.t.n.emptyList()));
            m.checkNotNullExpressionValue(kVar, "Observable.just(SendPayl…ramMessage, emptyList()))");
            return kVar;
        }
        List<Attachment<?>> extractLinks = AttachmentUtilsKt.extractLinks(list, contentResolver);
        String content = message.getContent();
        if (content == null) {
            content = "";
        }
        String appendLinks = AttachmentUtilsKt.appendLinks(content, extractLinks);
        if (!m.areEqual(appendLinks, message.getContent())) {
            message2 = message.copy((r18 & 1) != 0 ? message.content : appendLinks, (r18 & 2) != 0 ? message.nonce : null, (r18 & 4) != 0 ? message.applicationId : null, (r18 & 8) != 0 ? message.activity : null, (r18 & 16) != 0 ? message.stickerIds : null, (r18 & 32) != 0 ? message.messageReference : null, (r18 & 64) != 0 ? message.allowedMentions : null, (r18 & 128) != 0 ? message.captchaKey : null);
        }
        List<Attachment<?>> uniqueifyNames = uniqueifyNames(u.minus((Iterable) list, (Iterable) extractLinks));
        if (!uniqueifyNames.isEmpty()) {
            k kVar2 = new k(getPreprocessingFromAttachments(contentResolver, uniqueifyNames));
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(uniqueifyNames, 10));
            int i = 0;
            for (Object obj : uniqueifyNames) {
                i++;
                if (i < 0) {
                    d0.t.n.throwIndexOverflow();
                }
                arrayList.add(INSTANCE.getPart((Attachment) obj, contentResolver, a.p("file", i)));
            }
            Observable<SendPayload> m = Observable.m(kVar2, Observable.l(Observable.h0(new q(arrayList))).f0().F(new j0.k.b<List<FileUpload>, SendPayload.ReadyToSend>() { // from class: com.discord.utilities.rest.SendUtils$getSendPayload$2
                public final SendUtils.SendPayload.ReadyToSend call(List<SendUtils.FileUpload> list2) {
                    RestAPIParams.Message message3 = RestAPIParams.Message.this;
                    m.checkNotNullExpressionValue(list2, "attachmentParts");
                    return new SendUtils.SendPayload.ReadyToSend(message3, list2);
                }
            }));
            m.checkNotNullExpressionValue(m, "Observable.concat(\n     …)\n              }\n      )");
            return m;
        }
        k kVar3 = new k(new SendPayload.ReadyToSend(message2, d0.t.n.emptyList()));
        m.checkNotNullExpressionValue(kVar3, "Observable.just(SendPayl…ramMessage, emptyList()))");
        return kVar3;
    }

    public final void handleCaptchaRequired(MessageResult.CaptchaRequired captchaRequired) {
        m.checkNotNullParameter(captchaRequired, "messageResult");
        WidgetCaptchaBottomSheet.Companion.enqueue("Message Captcha", new SendUtils$handleCaptchaRequired$1(captchaRequired), new SendUtils$handleCaptchaRequired$2(captchaRequired), CaptchaErrorBody.Companion.createFromError(captchaRequired.getError()));
    }

    @MainThread
    public final void handleSendError(Error error, Context context, Function0<Unit> function0, Function0<Unit> function02) {
        m.checkNotNullParameter(error, "error");
        m.checkNotNullParameter(context, "context");
        error.setShowErrorToasts(false);
        Error.Response response = error.getResponse();
        m.checkNotNullExpressionValue(response, "error.response");
        Integer abortCodeMessageResId = RestAPIAbortMessages.getAbortCodeMessageResId(response.getCode());
        if (abortCodeMessageResId != null) {
            b.a.d.m.g(context, abortCodeMessageResId.intValue(), 0, null, 12);
        } else if (error.getType() == Error.Type.REQUEST_TOO_LARGE) {
            if (function0 != null) {
                function0.invoke();
            }
        } else if (error.getType() != Error.Type.FORBIDDEN_DISCORD) {
            Error.Response response2 = error.getResponse();
            m.checkNotNullExpressionValue(response2, "error.response");
            if (response2.isKnownResponse() || error.getType() == Error.Type.NETWORK) {
                error.setShowErrorToasts(true);
            } else {
                b.a.d.m.g(context, R.string.network_error_bad_request, 0, null, 12);
            }
        } else if (function02 != null) {
            function02.invoke();
        }
        error.showToasts(context);
    }

    @VisibleForTesting
    public final <T> Attachment<T> renamedWithDuplicateCount(Attachment<? extends T> attachment, int i) {
        String str;
        m.checkNotNullParameter(attachment, "$this$renamedWithDuplicateCount");
        long id2 = attachment.getId();
        Uri uri = attachment.getUri();
        Pair<String, String> splitFileExtension = splitFileExtension(attachment.getDisplayName());
        String component1 = splitFileExtension.component1();
        String component2 = splitFileExtension.component2();
        if (component2 != null) {
            str = component1 + '-' + i + ClassUtils.PACKAGE_SEPARATOR_CHAR + component2;
        } else {
            str = component1 + '-' + i;
        }
        return new Attachment<>(id2, uri, str, attachment.getData(), false, 16, null);
    }

    @VisibleForTesting
    public final List<Attachment<?>> uniqueifyNames(List<? extends Attachment<?>> list) {
        m.checkNotNullParameter(list, "attachmentParts");
        return d0.f0.q.toList(l.sequence(new SendUtils$uniqueifyNames$1(list, null)));
    }
}
