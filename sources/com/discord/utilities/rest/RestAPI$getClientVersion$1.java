package com.discord.utilities.rest;

import androidx.core.app.NotificationCompat;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import j0.k.b;
import kotlin.Metadata;
/* compiled from: RestAPI.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/google/gson/JsonObject;", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Lcom/google/gson/JsonObject;)Ljava/lang/Integer;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class RestAPI$getClientVersion$1<T, R> implements b<JsonObject, Integer> {
    public static final RestAPI$getClientVersion$1 INSTANCE = new RestAPI$getClientVersion$1();

    public final Integer call(JsonObject jsonObject) {
        int i;
        if (jsonObject != null) {
            LinkedTreeMap.e<String, JsonElement> c = jsonObject.a.c("discord_android_min_version");
            JsonElement jsonElement = c != null ? c.p : null;
            if (jsonElement != null) {
                i = jsonElement.c();
                return Integer.valueOf(i);
            }
        }
        i = 0;
        return Integer.valueOf(i);
    }
}
