package com.discord.utilities.spotify;

import com.discord.models.domain.ModelConnectionAccessToken;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.time.Clock;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: SpotifyApiClient.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/ModelConnectionAccessToken;", "spotifyToken", "", "invoke", "(Lcom/discord/models/domain/ModelConnectionAccessToken;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SpotifyApiClient$refreshSpotifyToken$1 extends o implements Function1<ModelConnectionAccessToken, Unit> {
    public final /* synthetic */ SpotifyApiClient this$0;

    /* compiled from: SpotifyApiClient.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000e\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()Ljava/lang/String;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.utilities.spotify.SpotifyApiClient$refreshSpotifyToken$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function0<String> {
        public final /* synthetic */ ModelConnectionAccessToken $spotifyToken;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(ModelConnectionAccessToken modelConnectionAccessToken) {
            super(0);
            this.$spotifyToken = modelConnectionAccessToken;
        }

        @Override // kotlin.jvm.functions.Function0
        public final String invoke() {
            return this.$spotifyToken.getAccessToken();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SpotifyApiClient$refreshSpotifyToken$1(SpotifyApiClient spotifyApiClient) {
        super(1);
        this.this$0 = spotifyApiClient;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ModelConnectionAccessToken modelConnectionAccessToken) {
        invoke2(modelConnectionAccessToken);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ModelConnectionAccessToken modelConnectionAccessToken) {
        Clock clock;
        String trackIdToFetch;
        m.checkNotNullParameter(modelConnectionAccessToken, "spotifyToken");
        RestAPI.AppHeadersProvider.spotifyTokenProvider = new AnonymousClass1(modelConnectionAccessToken);
        SpotifyApiClient spotifyApiClient = this.this$0;
        clock = spotifyApiClient.clock;
        spotifyApiClient.setTokenExpiresAt(clock.currentTimeMillis() + 3600000);
        trackIdToFetch = this.this$0.getTrackIdToFetch();
        if (trackIdToFetch != null) {
            this.this$0.fetchSpotifyTrack(trackIdToFetch);
            this.this$0.setTrackIdToFetch(null);
        }
    }
}
