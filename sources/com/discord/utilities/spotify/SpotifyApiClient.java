package com.discord.utilities.spotify;

import andhook.lib.HookHelper;
import android.annotation.SuppressLint;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.spotify.ModelSpotifyTrack;
import com.discord.utilities.platform.Platform;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.Clock;
import d0.z.d.m;
import java.util.HashMap;
import java.util.Objects;
import kotlin.Metadata;
import rx.Observable;
import rx.Subscription;
import rx.subjects.BehaviorSubject;
/* compiled from: SpotifyApiClient.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010'\u001a\u00020&¢\u0006\u0004\b3\u00104J\u0019\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\b2\u0006\u0010\u0007\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0011\u0010\u000b\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0019\u0010\r\u001a\u00020\b2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u0010\u001a\u00020\u000fH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u0017\u0010\u0013\u001a\u00020\b2\u0006\u0010\u0012\u001a\u00020\u000fH\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u000f\u0010\u0016\u001a\u00020\u0015H\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u000f\u0010\u0018\u001a\u00020\bH\u0003¢\u0006\u0004\b\u0018\u0010\u0019J\u0015\u0010\u001b\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u001a¢\u0006\u0004\b\u001b\u0010\u001cJ\u0015\u0010\u001d\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u001d\u0010\u000eJ\u0017\u0010\u001f\u001a\u00020\b2\b\u0010\u001e\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u001f\u0010\u000eR2\u0010\"\u001a\u001e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040 j\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0004`!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010#R\u0018\u0010$\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b$\u0010%R\u0016\u0010'\u001a\u00020&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R:\u0010+\u001a&\u0012\f\u0012\n **\u0004\u0018\u00010\u00040\u0004 **\u0012\u0012\f\u0012\n **\u0004\u0018\u00010\u00040\u0004\u0018\u00010)0)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b+\u0010,R\u0016\u0010-\u001a\u00020\u000f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b-\u0010.R\u0018\u00100\u001a\u0004\u0018\u00010/8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b0\u00101R\u0018\u00102\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b2\u0010%¨\u00065"}, d2 = {"Lcom/discord/utilities/spotify/SpotifyApiClient;", "", "", "trackId", "Lcom/discord/models/domain/spotify/ModelSpotifyTrack;", "getCachedTrack", "(Ljava/lang/String;)Lcom/discord/models/domain/spotify/ModelSpotifyTrack;", "track", "", "setCachedTrack", "(Lcom/discord/models/domain/spotify/ModelSpotifyTrack;)V", "getTrackIdToFetch", "()Ljava/lang/String;", "setTrackIdToFetch", "(Ljava/lang/String;)V", "", "getTokenExpiresAt", "()J", "expiresAt", "setTokenExpiresAt", "(J)V", "", "isTokenExpiring", "()Z", "refreshSpotifyToken", "()V", "Lrx/Observable;", "getSpotifyTrack", "()Lrx/Observable;", "fetchSpotifyTrack", ModelAuditLogEntry.CHANGE_KEY_ID, "setSpotifyAccountId", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "spotifyTracks", "Ljava/util/HashMap;", "spotifyAccountId", "Ljava/lang/String;", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "Lrx/subjects/BehaviorSubject;", "kotlin.jvm.PlatformType", "spotifyTrackSubject", "Lrx/subjects/BehaviorSubject;", "tokenExpiresAt", "J", "Lrx/Subscription;", "tokenSubscription", "Lrx/Subscription;", "trackIdToFetch", HookHelper.constructorName, "(Lcom/discord/utilities/time/Clock;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SpotifyApiClient {
    private final Clock clock;
    private String spotifyAccountId;
    private long tokenExpiresAt;
    private Subscription tokenSubscription;
    private String trackIdToFetch;
    private final HashMap<String, ModelSpotifyTrack> spotifyTracks = new HashMap<>();
    private final BehaviorSubject<ModelSpotifyTrack> spotifyTrackSubject = BehaviorSubject.k0();

    public SpotifyApiClient(Clock clock) {
        m.checkNotNullParameter(clock, "clock");
        this.clock = clock;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized ModelSpotifyTrack getCachedTrack(String str) {
        return this.spotifyTracks.get(str);
    }

    private final synchronized long getTokenExpiresAt() {
        return this.tokenExpiresAt;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized String getTrackIdToFetch() {
        return this.trackIdToFetch;
    }

    private final boolean isTokenExpiring() {
        return this.clock.currentTimeMillis() >= getTokenExpiresAt() - 10000;
    }

    /* JADX INFO: Access modifiers changed from: private */
    @SuppressLint({"DefaultLocale"})
    public final void refreshSpotifyToken() {
        String str;
        Subscription subscription = this.tokenSubscription;
        if ((subscription == null || (subscription != null && subscription.isUnsubscribed())) && (str = this.spotifyAccountId) != null) {
            RestAPI api = RestAPI.Companion.getApi();
            String name = Platform.SPOTIFY.name();
            Objects.requireNonNull(name, "null cannot be cast to non-null type java.lang.String");
            String lowerCase = name.toLowerCase();
            m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase()");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(api.getConnectionAccessToken(lowerCase, str), false, 1, null), SpotifyApiClient.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new SpotifyApiClient$refreshSpotifyToken$2(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new SpotifyApiClient$refreshSpotifyToken$1(this));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void setCachedTrack(ModelSpotifyTrack modelSpotifyTrack) {
        this.spotifyTracks.put(modelSpotifyTrack.getId(), modelSpotifyTrack);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void setTokenExpiresAt(long j) {
        this.tokenExpiresAt = j;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void setTrackIdToFetch(String str) {
        this.trackIdToFetch = str;
    }

    public final void fetchSpotifyTrack(String str) {
        m.checkNotNullParameter(str, "trackId");
        if (getCachedTrack(str) != null) {
            this.spotifyTrackSubject.onNext(getCachedTrack(str));
            return;
        }
        this.spotifyTrackSubject.onNext(null);
        if (isTokenExpiring()) {
            setTrackIdToFetch(str);
            refreshSpotifyToken();
            return;
        }
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApiSpotify().getSpotifyTrack(str), false, 1, null), SpotifyApiClient.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new SpotifyApiClient$fetchSpotifyTrack$1(this, str), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new SpotifyApiClient$fetchSpotifyTrack$2(this, str));
    }

    public final Observable<ModelSpotifyTrack> getSpotifyTrack() {
        BehaviorSubject<ModelSpotifyTrack> behaviorSubject = this.spotifyTrackSubject;
        m.checkNotNullExpressionValue(behaviorSubject, "spotifyTrackSubject");
        return behaviorSubject;
    }

    public final void setSpotifyAccountId(String str) {
        this.spotifyAccountId = str;
    }
}
