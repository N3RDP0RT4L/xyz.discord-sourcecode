package com.discord.utilities.stage;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.core.view.PointerIconCompat;
import androidx.fragment.app.FragmentManager;
import b.c.a.a0.d;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.permission.Permission;
import com.discord.api.stageinstance.StageInstance;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreStageChannels;
import com.discord.stores.StoreStageInstances;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.guilds.MemberVerificationUtils;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.widgets.stage.StageChannelJoinHelper;
import com.discord.widgets.stage.StageRoles;
import com.discord.widgets.stage.start.StageEventsGuildsFeatureFlag;
import com.discord.widgets.stage.start.WidgetModeratorStartStage;
import d0.z.d.m;
import java.util.Map;
import kotlin.Metadata;
/* compiled from: StageChannelUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b#\u0010$J?\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\n\u0010\b\u001a\u00060\u0006j\u0002`\u00072\n\u0010\n\u001a\u00060\u0006j\u0002`\t2\u0006\u0010\f\u001a\u00020\u000bH\u0007¢\u0006\u0004\b\u000e\u0010\u000fJA\u0010\u001a\u001a\u00020\r2\n\u0010\u0011\u001a\u00060\u0006j\u0002`\u00102\b\b\u0002\u0010\u0013\u001a\u00020\u00122\b\b\u0002\u0010\u0015\u001a\u00020\u00142\b\b\u0002\u0010\u0017\u001a\u00020\u00162\b\b\u0002\u0010\u0019\u001a\u00020\u0018¢\u0006\u0004\b\u001a\u0010\u001bJ%\u0010!\u001a\u00020 2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\u001d\u001a\u00020\u001c2\u0006\u0010\u001f\u001a\u00020\u001e¢\u0006\u0004\b!\u0010\"¨\u0006%"}, d2 = {"Lcom/discord/utilities/stage/StageChannelUtils;", "", "Lcom/discord/stores/StorePermissions;", "permissionStore", "Lcom/discord/stores/StoreGuilds;", "guildStore", "", "Lcom/discord/primitives/UserId;", "meId", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/api/channel/Channel;", "channel", "", "computeCanEditStageModerators", "(Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreGuilds;JJLcom/discord/api/channel/Channel;)Z", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/stores/StoreChannels;", "channelsStore", "Lcom/discord/stores/StoreStageChannels;", "stageChannelsStore", "Lcom/discord/stores/StoreStageInstances;", "stageInstanceStore", "Lcom/discord/stores/StoreUser;", "userStore", "shouldShowEndStageBottomSheet", "(JLcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreStageChannels;Lcom/discord/stores/StoreStageInstances;Lcom/discord/stores/StoreUser;)Z", "Landroid/content/Context;", "context", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "connectToStageChannel", "(Lcom/discord/api/channel/Channel;Landroid/content/Context;Landroidx/fragment/app/FragmentManager;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StageChannelUtils {
    public static final StageChannelUtils INSTANCE = new StageChannelUtils();

    private StageChannelUtils() {
    }

    public static final boolean computeCanEditStageModerators(StorePermissions storePermissions, StoreGuilds storeGuilds, long j, long j2, Channel channel) {
        m.checkNotNullParameter(storePermissions, "permissionStore");
        m.checkNotNullParameter(storeGuilds, "guildStore");
        m.checkNotNullParameter(channel, "channel");
        Guild guild = storeGuilds.getGuilds().get(Long.valueOf(j2));
        if ((guild != null && guild.isOwner(j)) || d.H0((Long) a.c(channel, storePermissions.getPermissionsByChannel()), 8L) || d.H0(storePermissions.getGuildPermissions().get(Long.valueOf(j2)), 289406992L)) {
            return true;
        }
        PermissionUtils permissionUtils = PermissionUtils.INSTANCE;
        Map<Long, GuildMember> map = storeGuilds.getMembers().get(Long.valueOf(j2));
        return d.H0(Long.valueOf(permissionUtils.computeChannelOverwrite(j, j2, map != null ? map.get(Long.valueOf(j)) : null, channel.s())), Permission.MANAGE_ROLES);
    }

    public static /* synthetic */ boolean shouldShowEndStageBottomSheet$default(StageChannelUtils stageChannelUtils, long j, StoreChannels storeChannels, StoreStageChannels storeStageChannels, StoreStageInstances storeStageInstances, StoreUser storeUser, int i, Object obj) {
        if ((i & 2) != 0) {
            storeChannels = StoreStream.Companion.getChannels();
        }
        StoreChannels storeChannels2 = storeChannels;
        if ((i & 4) != 0) {
            storeStageChannels = StoreStream.Companion.getStageChannels();
        }
        StoreStageChannels storeStageChannels2 = storeStageChannels;
        if ((i & 8) != 0) {
            storeStageInstances = StoreStream.Companion.getStageInstances();
        }
        StoreStageInstances storeStageInstances2 = storeStageInstances;
        if ((i & 16) != 0) {
            storeUser = StoreStream.Companion.getUsers();
        }
        return stageChannelUtils.shouldShowEndStageBottomSheet(j, storeChannels2, storeStageChannels2, storeStageInstances2, storeUser);
    }

    public final void connectToStageChannel(Channel channel, Context context, FragmentManager fragmentManager) {
        m.checkNotNullParameter(channel, "channel");
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        if (channel.A() == 13) {
            StoreStream.Companion companion = StoreStream.Companion;
            StageInstance stageInstanceForChannel = companion.getStageInstances().getStageInstanceForChannel(channel.h());
            Long l = (Long) a.c(channel, companion.getPermissions().getPermissionsByChannel());
            if (stageInstanceForChannel != null && d.W0(stageInstanceForChannel)) {
                StageChannelJoinHelper.connectToStage$default(StageChannelJoinHelper.INSTANCE, context, fragmentManager, channel.h(), false, false, null, null, null, null, null, PointerIconCompat.TYPE_TOP_RIGHT_DIAGONAL_DOUBLE_ARROW, null);
            } else if (stageInstanceForChannel != null || l == null || !PermissionUtils.can(20971536L, Long.valueOf(l.longValue())) || !StageEventsGuildsFeatureFlag.Companion.getINSTANCE().canGuildAccessStageEvents(channel.f())) {
                MemberVerificationUtils.maybeShowVerificationGate$default(MemberVerificationUtils.INSTANCE, context, fragmentManager, channel.f(), "Guild Voice", null, null, new StageChannelUtils$connectToStageChannel$2(context, fragmentManager, channel), 48, null);
            } else {
                WidgetModeratorStartStage.Companion.launch(context, channel.h(), channel.f());
            }
        }
    }

    public final boolean shouldShowEndStageBottomSheet(long j, StoreChannels storeChannels, StoreStageChannels storeStageChannels, StoreStageInstances storeStageInstances, StoreUser storeUser) {
        StageRoles stageRoles;
        Map<Long, StageRoles> channelRoles;
        m.checkNotNullParameter(storeChannels, "channelsStore");
        m.checkNotNullParameter(storeStageChannels, "stageChannelsStore");
        m.checkNotNullParameter(storeStageInstances, "stageInstanceStore");
        m.checkNotNullParameter(storeUser, "userStore");
        Channel channel = storeChannels.getChannel(j);
        if (channel == null || !ChannelUtils.z(channel) || storeStageInstances.getStageInstanceForChannel(channel.h()) == null || (stageRoles = storeStageChannels.m10getMyRolesvisDeB4(channel.h())) == null || !StageRoles.m26isModeratorimpl(stageRoles.m29unboximpl()) || (channelRoles = storeStageChannels.getChannelRoles(channel.h())) == null) {
            return false;
        }
        long id2 = storeUser.getMe().getId();
        boolean z2 = false;
        boolean z3 = false;
        for (Map.Entry<Long, StageRoles> entry : channelRoles.entrySet()) {
            long longValue = entry.getKey().longValue();
            int i = entry.getValue().m29unboximpl();
            if (longValue != id2 && StageRoles.m26isModeratorimpl(i)) {
                if (StageRoles.m27isSpeakerimpl(i)) {
                    z2 = true;
                    z3 = true;
                } else {
                    z2 = true;
                }
            }
        }
        if (!z2) {
            return true;
        }
        if (!StageRoles.m27isSpeakerimpl(stageRoles.m29unboximpl())) {
            return false;
        }
        return !z3;
    }
}
