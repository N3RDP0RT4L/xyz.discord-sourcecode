package com.discord.utilities.recycler;

import androidx.exifinterface.media.ExifInterface;
import androidx.recyclerview.widget.DiffUtil;
import com.discord.utilities.recycler.DiffCreator;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: DiffCreator.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\r\u001a\u00020\n\"\u000e\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u00020\u00010\u0000\"\b\b\u0001\u0010\u0004*\u00020\u00032j\u0010\t\u001af\u0012$\u0012\"\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001 \u0007*\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00060\u0006\u0012\u0006\u0012\u0004\u0018\u00010\b \u0007*2\u0012$\u0012\"\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001 \u0007*\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00060\u0006\u0012\u0006\u0012\u0004\u0018\u00010\b\u0018\u00010\u00050\u0005H\n¢\u0006\u0004\b\u000b\u0010\f"}, d2 = {"", "Lcom/discord/utilities/recycler/DiffKeyProvider;", ExifInterface.GPS_DIRECTION_TRUE, "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "VH", "Lkotlin/Pair;", "Lcom/discord/utilities/recycler/DiffCreator$UpdateRequest;", "kotlin.jvm.PlatformType", "Landroidx/recyclerview/widget/DiffUtil$DiffResult;", "<name for destructuring parameter 0>", "", "invoke", "(Lkotlin/Pair;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class DiffCreator$subscribeToAsyncUpdateRequests$3 extends o implements Function1<Pair<? extends DiffCreator.UpdateRequest<T, VH>, ? extends DiffUtil.DiffResult>, Unit> {
    public final /* synthetic */ DiffCreator this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public DiffCreator$subscribeToAsyncUpdateRequests$3(DiffCreator diffCreator) {
        super(1);
        this.this$0 = diffCreator;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Object obj) {
        invoke((Pair) obj);
        return Unit.a;
    }

    public final void invoke(Pair<DiffCreator.UpdateRequest<T, VH>, ? extends DiffUtil.DiffResult> pair) {
        DiffCreator.UpdateRequest updateRequest = (DiffCreator.UpdateRequest) pair.component1();
        this.this$0.setItemsAndDispatchUpdate(updateRequest.getAdapter(), updateRequest.getSetItems(), (List) updateRequest.getNewItems(), (DiffUtil.DiffResult) pair.component2());
    }
}
