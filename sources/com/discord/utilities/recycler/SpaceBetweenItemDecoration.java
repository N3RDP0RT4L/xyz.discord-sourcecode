package com.discord.utilities.recycler;

import andhook.lib.HookHelper;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ShapeDrawable;
import android.view.View;
import androidx.annotation.ColorRes;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.dimen.DimenUtils;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: SpaceBetweenItemDecoration.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B7\u0012\u0006\u0010\u0015\u001a\u00020\u0014\u0012\u0006\u0010\u0016\u001a\u00020\r\u0012\n\u0010\u0011\u001a\u0006\u0012\u0002\b\u00030\u0010\u0012\b\b\u0002\u0010\u000e\u001a\u00020\r\u0012\b\b\u0003\u0010\u0013\u001a\u00020\r¢\u0006\u0004\b\u0017\u0010\u0018J/\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\bH\u0016¢\u0006\u0004\b\u000b\u0010\fR\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u001a\u0010\u0011\u001a\u0006\u0012\u0002\b\u00030\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0013\u001a\u00020\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u000f¨\u0006\u0019"}, d2 = {"Lcom/discord/utilities/recycler/SpaceBetweenItemDecoration;", "Landroidx/recyclerview/widget/DividerItemDecoration;", "Landroid/graphics/Rect;", "outRect", "Landroid/view/View;", "view", "Landroidx/recyclerview/widget/RecyclerView;", "parent", "Landroidx/recyclerview/widget/RecyclerView$State;", "state", "", "getItemOffsets", "(Landroid/graphics/Rect;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$State;)V", "", "sizePx", "I", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "adapter", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", ModelAuditLogEntry.CHANGE_KEY_COLOR, "Landroid/content/Context;", "context", "orientation", HookHelper.constructorName, "(Landroid/content/Context;ILandroidx/recyclerview/widget/RecyclerView$Adapter;II)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SpaceBetweenItemDecoration extends DividerItemDecoration {
    private final RecyclerView.Adapter<?> adapter;
    private final int color;
    private final int sizePx;

    public /* synthetic */ SpaceBetweenItemDecoration(Context context, int i, RecyclerView.Adapter adapter, int i2, int i3, int i4, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, i, adapter, (i4 & 8) != 0 ? DimenUtils.dpToPixels(16) : i2, (i4 & 16) != 0 ? 17170445 : i3);
    }

    @Override // androidx.recyclerview.widget.DividerItemDecoration, androidx.recyclerview.widget.RecyclerView.ItemDecoration
    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
        m.checkNotNullParameter(rect, "outRect");
        m.checkNotNullParameter(view, "view");
        m.checkNotNullParameter(recyclerView, "parent");
        m.checkNotNullParameter(state, "state");
        if (recyclerView.getChildAdapterPosition(view) != this.adapter.getItemCount() - 1) {
            super.getItemOffsets(rect, view, recyclerView, state);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SpaceBetweenItemDecoration(Context context, int i, RecyclerView.Adapter<?> adapter, int i2, @ColorRes int i3) {
        super(context, i);
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(adapter, "adapter");
        this.adapter = adapter;
        this.sizePx = i2;
        this.color = i3;
        ShapeDrawable shapeDrawable = new ShapeDrawable();
        shapeDrawable.setIntrinsicWidth(i2);
        shapeDrawable.setIntrinsicHeight(i2);
        Paint paint = shapeDrawable.getPaint();
        m.checkNotNullExpressionValue(paint, "paint");
        paint.setColor(ContextCompat.getColor(context, i3));
        setDrawable(shapeDrawable);
    }
}
