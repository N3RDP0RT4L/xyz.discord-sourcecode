package com.discord.utilities.recycler;

import androidx.exifinterface.media.ExifInterface;
import com.discord.utilities.analytics.Traits;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.Subscription;
/* compiled from: DiffCreator.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\n\u001a\u00020\u0007\"\u000e\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u00020\u00010\u0000\"\b\b\u0001\u0010\u0004*\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\n¢\u0006\u0004\b\b\u0010\t"}, d2 = {"", "Lcom/discord/utilities/recycler/DiffKeyProvider;", ExifInterface.GPS_DIRECTION_TRUE, "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "VH", "Lrx/Subscription;", Traits.Payment.Type.SUBSCRIPTION, "", "invoke", "(Lrx/Subscription;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class DiffCreator$subscribeToAsyncUpdateRequests$2 extends o implements Function1<Subscription, Unit> {
    public final /* synthetic */ DiffCreator this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public DiffCreator$subscribeToAsyncUpdateRequests$2(DiffCreator diffCreator) {
        super(1);
        this.this$0 = diffCreator;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Subscription subscription) {
        invoke2(subscription);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Subscription subscription) {
        m.checkNotNullParameter(subscription, Traits.Payment.Type.SUBSCRIPTION);
        this.this$0.asyncDiffSubscription = subscription;
    }
}
