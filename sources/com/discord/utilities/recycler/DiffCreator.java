package com.discord.utilities.recycler;

import andhook.lib.HookHelper;
import android.annotation.SuppressLint;
import androidx.annotation.MainThread;
import androidx.exifinterface.media.ExifInterface;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import b.d.b.a.a;
import com.discord.app.AppComponent;
import com.discord.utilities.collections.ShallowPartitionMap;
import com.discord.utilities.recycler.DiffCreator;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Subscription;
import rx.subjects.PublishSubject;
/* compiled from: DiffCreator.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000_\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007*\u0001\u0016\u0018\u0000 +*\u000e\b\u0000\u0010\u0003*\b\u0012\u0004\u0012\u00020\u00020\u0001*\b\b\u0001\u0010\u0005*\u00020\u00042\u00020\u0006:\u0002+,B\u0011\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0007¢\u0006\u0004\b*\u0010\u000bJ\u0017\u0010\n\u001a\u00020\t2\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\n\u0010\u000bJC\u0010\u0013\u001a\u00020\t2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00010\f2\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\t0\u000e2\u0006\u0010\u0010\u001a\u00028\u00002\b\u0010\u0012\u001a\u0004\u0018\u00010\u0011H\u0003¢\u0006\u0004\b\u0013\u0010\u0014J+\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00162\u0006\u0010\u0015\u001a\u00028\u00002\u0006\u0010\u0010\u001a\u00028\u0000H\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u001f\u0010\u001a\u001a\u00020\u00192\u0006\u0010\u0015\u001a\u00028\u00002\u0006\u0010\u0010\u001a\u00028\u0000H\u0002¢\u0006\u0004\b\u001a\u0010\u001bJA\u0010\u001c\u001a\u00020\t2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00010\f2\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\t0\u000e2\u0006\u0010\u0015\u001a\u00028\u00002\u0006\u0010\u0010\u001a\u00028\u0000H\u0007¢\u0006\u0004\b\u001c\u0010\u001dJ?\u0010\u001e\u001a\u00020\t2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00010\f2\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\t0\u000e2\u0006\u0010\u0015\u001a\u00028\u00002\u0006\u0010\u0010\u001a\u00028\u0000¢\u0006\u0004\b\u001e\u0010\u001dJ\u001f\u0010\u001f\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0015\u001a\u00028\u00002\u0006\u0010\u0010\u001a\u00028\u0000¢\u0006\u0004\b\u001f\u0010 R\u0018\u0010\b\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010!R\u0018\u0010#\u001a\u0004\u0018\u00010\"8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b#\u0010$Rj\u0010(\u001aV\u0012$\u0012\"\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001 '*\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010&0& '**\u0012$\u0012\"\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001 '*\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010&0&\u0018\u00010%0%8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u0010)¨\u0006-"}, d2 = {"Lcom/discord/utilities/recycler/DiffCreator;", "", "Lcom/discord/utilities/recycler/DiffKeyProvider;", ExifInterface.GPS_DIRECTION_TRUE, "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "VH", "", "Lcom/discord/app/AppComponent;", "asyncDiffSubscriptionScope", "", "subscribeToAsyncUpdateRequests", "(Lcom/discord/app/AppComponent;)V", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "adapter", "Lkotlin/Function1;", "setItems", "newItems", "Landroidx/recyclerview/widget/DiffUtil$DiffResult;", "diffResult", "setItemsAndDispatchUpdate", "(Landroidx/recyclerview/widget/RecyclerView$Adapter;Lkotlin/jvm/functions/Function1;Ljava/util/List;Landroidx/recyclerview/widget/DiffUtil$DiffResult;)V", "oldItems", "com/discord/utilities/recycler/DiffCreator$createDiffUtilCallback$1", "createDiffUtilCallback", "(Ljava/util/List;Ljava/util/List;)Lcom/discord/utilities/recycler/DiffCreator$createDiffUtilCallback$1;", "", "isExpensiveDiff", "(Ljava/util/List;Ljava/util/List;)Z", "dispatchDiffUpdatesAsync", "(Landroidx/recyclerview/widget/RecyclerView$Adapter;Lkotlin/jvm/functions/Function1;Ljava/util/List;Ljava/util/List;)V", "dispatchDiffUpdates", "calculateDiffResult", "(Ljava/util/List;Ljava/util/List;)Landroidx/recyclerview/widget/DiffUtil$DiffResult;", "Lcom/discord/app/AppComponent;", "Lrx/Subscription;", "asyncDiffSubscription", "Lrx/Subscription;", "Lrx/subjects/PublishSubject;", "Lcom/discord/utilities/recycler/DiffCreator$UpdateRequest;", "kotlin.jvm.PlatformType", "updateRequestsSubject", "Lrx/subjects/PublishSubject;", HookHelper.constructorName, "Companion", "UpdateRequest", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class DiffCreator<T extends List<? extends DiffKeyProvider>, VH extends RecyclerView.ViewHolder> {
    public static final Companion Companion = new Companion(null);
    private static final int EXPENSIVE_DIFF_THRESHOLD = 225;
    private Subscription asyncDiffSubscription;
    private final AppComponent asyncDiffSubscriptionScope;
    private final PublishSubject<UpdateRequest<T, VH>> updateRequestsSubject = PublishSubject.k0();

    /* compiled from: DiffCreator.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/utilities/recycler/DiffCreator$Companion;", "", "", "EXPENSIVE_DIFF_THRESHOLD", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: DiffCreator.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u000b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u0000*\u0004\b\u0002\u0010\u0001*\b\b\u0003\u0010\u0003*\u00020\u00022\u00020\u0004B9\u0012\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00030\u0005\u0012\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00020\t0\b\u0012\u0006\u0010\u0011\u001a\u00028\u0002\u0012\u0006\u0010\u0012\u001a\u00028\u0002¢\u0006\u0004\b&\u0010'J\u0016\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00030\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u001c\u0010\n\u001a\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00020\t0\bHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00028\u0002HÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00028\u0002HÆ\u0003¢\u0006\u0004\b\u000e\u0010\rJV\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u00002\u000e\b\u0002\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00030\u00052\u0014\b\u0002\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00020\t0\b2\b\b\u0002\u0010\u0011\u001a\u00028\u00022\b\b\u0002\u0010\u0012\u001a\u00028\u0002HÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u001a\u0010\u001d\u001a\u00020\u001c2\b\u0010\u001b\u001a\u0004\u0018\u00010\u0004HÖ\u0003¢\u0006\u0004\b\u001d\u0010\u001eR\u0019\u0010\u0012\u001a\u00028\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u001f\u001a\u0004\b \u0010\rR%\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00020\t0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010!\u001a\u0004\b\"\u0010\u000bR\u0019\u0010\u0011\u001a\u00028\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001f\u001a\u0004\b#\u0010\rR\u001f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00030\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010$\u001a\u0004\b%\u0010\u0007¨\u0006("}, d2 = {"Lcom/discord/utilities/recycler/DiffCreator$UpdateRequest;", ExifInterface.GPS_DIRECTION_TRUE, "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "VH", "", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "component1", "()Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lkotlin/Function1;", "", "component2", "()Lkotlin/jvm/functions/Function1;", "component3", "()Ljava/lang/Object;", "component4", "adapter", "setItems", "oldItems", "newItems", "copy", "(Landroidx/recyclerview/widget/RecyclerView$Adapter;Lkotlin/jvm/functions/Function1;Ljava/lang/Object;Ljava/lang/Object;)Lcom/discord/utilities/recycler/DiffCreator$UpdateRequest;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Object;", "getNewItems", "Lkotlin/jvm/functions/Function1;", "getSetItems", "getOldItems", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "getAdapter", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView$Adapter;Lkotlin/jvm/functions/Function1;Ljava/lang/Object;Ljava/lang/Object;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class UpdateRequest<T, VH extends RecyclerView.ViewHolder> {
        private final RecyclerView.Adapter<VH> adapter;
        private final T newItems;
        private final T oldItems;
        private final Function1<T, Unit> setItems;

        /* JADX WARN: Multi-variable type inference failed */
        public UpdateRequest(RecyclerView.Adapter<VH> adapter, Function1<? super T, Unit> function1, T t, T t2) {
            m.checkNotNullParameter(adapter, "adapter");
            m.checkNotNullParameter(function1, "setItems");
            this.adapter = adapter;
            this.setItems = function1;
            this.oldItems = t;
            this.newItems = t2;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ UpdateRequest copy$default(UpdateRequest updateRequest, RecyclerView.Adapter adapter, Function1 function1, Object obj, Object obj2, int i, Object obj3) {
            if ((i & 1) != 0) {
                adapter = updateRequest.adapter;
            }
            if ((i & 2) != 0) {
                function1 = updateRequest.setItems;
            }
            if ((i & 4) != 0) {
                obj = updateRequest.oldItems;
            }
            if ((i & 8) != 0) {
                obj2 = updateRequest.newItems;
            }
            return updateRequest.copy(adapter, function1, obj, obj2);
        }

        public final RecyclerView.Adapter<VH> component1() {
            return this.adapter;
        }

        public final Function1<T, Unit> component2() {
            return this.setItems;
        }

        public final T component3() {
            return this.oldItems;
        }

        public final T component4() {
            return this.newItems;
        }

        public final UpdateRequest<T, VH> copy(RecyclerView.Adapter<VH> adapter, Function1<? super T, Unit> function1, T t, T t2) {
            m.checkNotNullParameter(adapter, "adapter");
            m.checkNotNullParameter(function1, "setItems");
            return new UpdateRequest<>(adapter, function1, t, t2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof UpdateRequest)) {
                return false;
            }
            UpdateRequest updateRequest = (UpdateRequest) obj;
            return m.areEqual(this.adapter, updateRequest.adapter) && m.areEqual(this.setItems, updateRequest.setItems) && m.areEqual(this.oldItems, updateRequest.oldItems) && m.areEqual(this.newItems, updateRequest.newItems);
        }

        public final RecyclerView.Adapter<VH> getAdapter() {
            return this.adapter;
        }

        public final T getNewItems() {
            return this.newItems;
        }

        public final T getOldItems() {
            return this.oldItems;
        }

        public final Function1<T, Unit> getSetItems() {
            return this.setItems;
        }

        public int hashCode() {
            RecyclerView.Adapter<VH> adapter = this.adapter;
            int i = 0;
            int hashCode = (adapter != null ? adapter.hashCode() : 0) * 31;
            Function1<T, Unit> function1 = this.setItems;
            int hashCode2 = (hashCode + (function1 != null ? function1.hashCode() : 0)) * 31;
            T t = this.oldItems;
            int hashCode3 = (hashCode2 + (t != null ? t.hashCode() : 0)) * 31;
            T t2 = this.newItems;
            if (t2 != null) {
                i = t2.hashCode();
            }
            return hashCode3 + i;
        }

        public String toString() {
            StringBuilder R = a.R("UpdateRequest(adapter=");
            R.append(this.adapter);
            R.append(", setItems=");
            R.append(this.setItems);
            R.append(", oldItems=");
            R.append(this.oldItems);
            R.append(", newItems=");
            R.append(this.newItems);
            R.append(")");
            return R.toString();
        }
    }

    public DiffCreator(AppComponent appComponent) {
        this.asyncDiffSubscriptionScope = appComponent;
        if (appComponent != null) {
            subscribeToAsyncUpdateRequests(appComponent);
        }
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [com.discord.utilities.recycler.DiffCreator$createDiffUtilCallback$1] */
    private final DiffCreator$createDiffUtilCallback$1 createDiffUtilCallback(final T t, final T t2) {
        return new DiffUtil.Callback() { // from class: com.discord.utilities.recycler.DiffCreator$createDiffUtilCallback$1
            @Override // androidx.recyclerview.widget.DiffUtil.Callback
            public boolean areContentsTheSame(int i, int i2) {
                return m.areEqual((DiffKeyProvider) t.get(i), (DiffKeyProvider) t2.get(i2));
            }

            @Override // androidx.recyclerview.widget.DiffUtil.Callback
            public boolean areItemsTheSame(int i, int i2) {
                return m.areEqual(((DiffKeyProvider) t.get(i)).getKey(), ((DiffKeyProvider) t2.get(i2)).getKey());
            }

            @Override // androidx.recyclerview.widget.DiffUtil.Callback
            public int getNewListSize() {
                return t2.size();
            }

            @Override // androidx.recyclerview.widget.DiffUtil.Callback
            public int getOldListSize() {
                return t.size();
            }
        };
    }

    private final boolean isExpensiveDiff(T t, T t2) {
        if (Math.abs(t2.size() - t.size()) > 225) {
            return true;
        }
        ShallowPartitionMap create$default = ShallowPartitionMap.Companion.create$default(ShallowPartitionMap.Companion, t.size(), 0, 0, null, 14, null);
        ArrayList arrayList = new ArrayList();
        Iterator it = t.iterator();
        while (it.hasNext()) {
            create$default.put(((DiffKeyProvider) it.next()).getKey(), Unit.a);
        }
        Iterator it2 = t2.iterator();
        while (it2.hasNext()) {
            String key = ((DiffKeyProvider) it2.next()).getKey();
            if (!create$default.containsKey(key)) {
                arrayList.add(key);
            }
            create$default.remove(key);
        }
        return arrayList.size() + create$default.size() > 225;
    }

    /* JADX INFO: Access modifiers changed from: private */
    @SuppressLint({"NotifyDataSetChanged"})
    public final void setItemsAndDispatchUpdate(RecyclerView.Adapter<VH> adapter, Function1<? super T, Unit> function1, T t, DiffUtil.DiffResult diffResult) {
        function1.invoke(t);
        if (diffResult != null) {
            diffResult.dispatchUpdatesTo(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    private final void subscribeToAsyncUpdateRequests(AppComponent appComponent) {
        PublishSubject<UpdateRequest<T, VH>> publishSubject = this.updateRequestsSubject;
        m.checkNotNullExpressionValue(publishSubject, "updateRequestsSubject");
        Observable F = ObservableExtensionsKt.computationBuffered(publishSubject).F(new b<UpdateRequest<T, VH>, Pair<? extends UpdateRequest<T, VH>, ? extends DiffUtil.DiffResult>>() { // from class: com.discord.utilities.recycler.DiffCreator$subscribeToAsyncUpdateRequests$1
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Object call(Object obj) {
                return call((DiffCreator.UpdateRequest) ((DiffCreator.UpdateRequest) obj));
            }

            public final Pair<DiffCreator.UpdateRequest<T, VH>, DiffUtil.DiffResult> call(DiffCreator.UpdateRequest<T, VH> updateRequest) {
                return new Pair<>(updateRequest, DiffCreator.this.calculateDiffResult((List) updateRequest.getOldItems(), (List) updateRequest.getNewItems()));
            }
        });
        m.checkNotNullExpressionValue(F, "updateRequestsSubject\n  …ms)\n          )\n        }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(F, appComponent, null, 2, null), DiffCreator.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new DiffCreator$subscribeToAsyncUpdateRequests$2(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new DiffCreator$subscribeToAsyncUpdateRequests$3(this));
    }

    public final DiffUtil.DiffResult calculateDiffResult(T t, T t2) {
        m.checkNotNullParameter(t, "oldItems");
        m.checkNotNullParameter(t2, "newItems");
        if (isExpensiveDiff(t, t2)) {
            return null;
        }
        return DiffUtil.calculateDiff(createDiffUtilCallback(t, t2), true);
    }

    public final void dispatchDiffUpdates(RecyclerView.Adapter<VH> adapter, Function1<? super T, Unit> function1, T t, T t2) {
        m.checkNotNullParameter(adapter, "adapter");
        m.checkNotNullParameter(function1, "setItems");
        m.checkNotNullParameter(t, "oldItems");
        m.checkNotNullParameter(t2, "newItems");
        setItemsAndDispatchUpdate(adapter, function1, t2, calculateDiffResult(t, t2));
    }

    @MainThread
    public final void dispatchDiffUpdatesAsync(RecyclerView.Adapter<VH> adapter, Function1<? super T, Unit> function1, T t, T t2) {
        m.checkNotNullParameter(adapter, "adapter");
        m.checkNotNullParameter(function1, "setItems");
        m.checkNotNullParameter(t, "oldItems");
        m.checkNotNullParameter(t2, "newItems");
        if (this.asyncDiffSubscriptionScope != null) {
            Subscription subscription = this.asyncDiffSubscription;
            if (subscription == null || subscription.isUnsubscribed()) {
                subscribeToAsyncUpdateRequests(this.asyncDiffSubscriptionScope);
            }
            PublishSubject<UpdateRequest<T, VH>> publishSubject = this.updateRequestsSubject;
            publishSubject.k.onNext(new UpdateRequest<>(adapter, function1, t, t2));
            return;
        }
        throw new IllegalStateException("to use async diffs, provide an asyncDiffSubscriptionScope");
    }
}
