package com.discord.utilities.recycler;

import andhook.lib.HookHelper;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.utilities.time.Clock;
import com.discord.utilities.time.ClockFactory;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: SpeedOnScrollListener.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 $2\u00020\u0001:\u0001$B;\u0012\b\b\u0002\u0010\u001a\u001a\u00020\r\u0012\u0014\b\u0002\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00070\u0010\u0012\b\b\u0002\u0010\u0016\u001a\u00020\u0004\u0012\b\b\u0002\u0010\u001e\u001a\u00020\u001d¢\u0006\u0004\b\"\u0010#J'\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\b\u0010\tJ\u001f\u0010\u000b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u000b\u0010\fR\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR%\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00070\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u0019\u0010\u0016\u001a\u00020\u00048\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u0019\u0010\u001a\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u000f\u001a\u0004\b\u001b\u0010\u001cR\u0019\u0010\u001e\u001a\u00020\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010!¨\u0006%"}, d2 = {"Lcom/discord/utilities/recycler/SpeedOnScrollListener;", "Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;", "Landroidx/recyclerview/widget/RecyclerView;", "recyclerView", "", "dx", "dy", "", "onScrolled", "(Landroidx/recyclerview/widget/RecyclerView;II)V", "newState", "onScrollStateChanged", "(Landroidx/recyclerview/widget/RecyclerView;I)V", "", "timeStamp", "J", "Lkotlin/Function1;", "", "thresholdCallback", "Lkotlin/jvm/functions/Function1;", "getThresholdCallback", "()Lkotlin/jvm/functions/Function1;", "orientation", "I", "getOrientation", "()I", "maxScrolledPxPerMs", "getMaxScrolledPxPerMs", "()J", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "getClock", "()Lcom/discord/utilities/time/Clock;", HookHelper.constructorName, "(JLkotlin/jvm/functions/Function1;ILcom/discord/utilities/time/Clock;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SpeedOnScrollListener extends RecyclerView.OnScrollListener {
    public static final Companion Companion = new Companion(null);
    private static final long INIT_TIMESTAMP = -1;
    private final Clock clock;
    private final long maxScrolledPxPerMs;
    private final int orientation;
    private final Function1<Boolean, Unit> thresholdCallback;
    private long timeStamp;

    /* compiled from: SpeedOnScrollListener.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "it", "", "invoke", "(Z)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.utilities.recycler.SpeedOnScrollListener$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<Boolean, Unit> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Boolean bool) {
            invoke(bool.booleanValue());
            return Unit.a;
        }

        public final void invoke(boolean z2) {
        }
    }

    /* compiled from: SpeedOnScrollListener.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/utilities/recycler/SpeedOnScrollListener$Companion;", "", "", "INIT_TIMESTAMP", "J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public SpeedOnScrollListener() {
        this(0L, null, 0, null, 15, null);
    }

    public /* synthetic */ SpeedOnScrollListener(long j, Function1 function1, int i, Clock clock, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this((i2 & 1) != 0 ? 1L : j, (i2 & 2) != 0 ? AnonymousClass1.INSTANCE : function1, (i2 & 4) != 0 ? 1 : i, (i2 & 8) != 0 ? ClockFactory.get() : clock);
    }

    public final Clock getClock() {
        return this.clock;
    }

    public final long getMaxScrolledPxPerMs() {
        return this.maxScrolledPxPerMs;
    }

    public final int getOrientation() {
        return this.orientation;
    }

    public final Function1<Boolean, Unit> getThresholdCallback() {
        return this.thresholdCallback;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
    public void onScrollStateChanged(RecyclerView recyclerView, int i) {
        m.checkNotNullParameter(recyclerView, "recyclerView");
        if (i == 0) {
            this.thresholdCallback.invoke(Boolean.valueOf(0 < this.maxScrolledPxPerMs));
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
    public void onScrolled(RecyclerView recyclerView, int i, int i2) {
        m.checkNotNullParameter(recyclerView, "recyclerView");
        long currentTimeMillis = this.clock.currentTimeMillis();
        long j = this.timeStamp;
        if (j == -1) {
            this.timeStamp = currentTimeMillis;
            return;
        }
        boolean z2 = true;
        if (this.orientation == 1) {
            i = i2;
        }
        float abs = Math.abs(i / ((float) (currentTimeMillis - j)));
        this.timeStamp = currentTimeMillis;
        Function1<Boolean, Unit> function1 = this.thresholdCallback;
        if (abs > ((float) this.maxScrolledPxPerMs)) {
            z2 = false;
        }
        function1.invoke(Boolean.valueOf(z2));
    }

    /* JADX WARN: Multi-variable type inference failed */
    public SpeedOnScrollListener(long j, Function1<? super Boolean, Unit> function1, int i, Clock clock) {
        m.checkNotNullParameter(function1, "thresholdCallback");
        m.checkNotNullParameter(clock, "clock");
        this.maxScrolledPxPerMs = j;
        this.thresholdCallback = function1;
        this.orientation = i;
        this.clock = clock;
        this.timeStamp = -1L;
    }
}
