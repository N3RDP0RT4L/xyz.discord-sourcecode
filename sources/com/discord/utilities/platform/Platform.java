package com.discord.utilities.platform;

import andhook.lib.HookHelper;
import androidx.annotation.AttrRes;
import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import b.d.b.a.a;
import com.discord.api.activity.ActivityPlatform;
import com.discord.api.connectedaccounts.ConnectedAccount;
import d0.g0.w;
import d0.z.d.m;
import java.util.Locale;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.objectweb.asm.Opcodes;
import xyz.discord.R;
/* compiled from: Platform.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u001f\b\u0086\u0001\u0018\u0000 !2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001!BU\b\u0002\u0012\b\b\u0002\u0010\u0007\u001a\u00020\u0004\u0012\n\b\u0003\u0010\u0017\u001a\u0004\u0018\u00010\u0012\u0012\n\b\u0003\u0010\u0013\u001a\u0004\u0018\u00010\u0012\u0012\n\b\u0003\u0010\u001b\u001a\u0004\u0018\u00010\u0012\u0012\b\b\u0002\u0010\u0019\u001a\u00020\u000b\u0012\b\b\u0002\u0010\u001d\u001a\u00020\u000b\u0012\b\b\u0002\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\u001f\u0010 J\u0017\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0019\u0010\u0007\u001a\u00020\u00048\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\t\u0010\nR\u0019\u0010\f\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000fR\u0019\u0010\u0010\u001a\u00020\u00048\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\b\u001a\u0004\b\u0011\u0010\nR\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0014\u001a\u0004\b\u0018\u0010\u0016R\u0019\u0010\u0019\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\r\u001a\u0004\b\u001a\u0010\u000fR\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u0014\u001a\u0004\b\u001c\u0010\u0016R\u0019\u0010\u001d\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\r\u001a\u0004\b\u001e\u0010\u000fj\u0002\b\"j\u0002\b#j\u0002\b$j\u0002\b%j\u0002\b&j\u0002\b'j\u0002\b(j\u0002\b)j\u0002\b*j\u0002\b+j\u0002\b,j\u0002\b-j\u0002\b.j\u0002\b/j\u0002\b0¨\u00061"}, d2 = {"Lcom/discord/utilities/platform/Platform;", "", "Lcom/discord/api/connectedaccounts/ConnectedAccount;", "connectedAccount", "", "getProfileUrl", "(Lcom/discord/api/connectedaccounts/ConnectedAccount;)Ljava/lang/String;", "properName", "Ljava/lang/String;", "getProperName", "()Ljava/lang/String;", "", "enabled", "Z", "getEnabled", "()Z", "platformId", "getPlatformId", "", "themedPlatformImage", "Ljava/lang/Integer;", "getThemedPlatformImage", "()Ljava/lang/Integer;", "colorResId", "getColorResId", "canSyncFriends", "getCanSyncFriends", "whitePlatformImage", "getWhitePlatformImage", "canShowActivity", "getCanShowActivity", HookHelper.constructorName, "(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;ZZZ)V", "Companion", "BATTLENET", "FACEBOOK", "GITHUB", "LEAGUEOFLEGENDS", "PLAYSTATION", "REDDIT", "SAMSUNG", "SKYPE", "SPOTIFY", "STEAM", "TWITCH", "TWITTER", "XBOX", "YOUTUBE", "NONE", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public enum Platform {
    BATTLENET("Battle.net", Integer.valueOf((int) R.color.battlenet), Integer.valueOf((int) R.attr.ic_account_battlenet), Integer.valueOf((int) R.drawable.ic_account_battlenet_white_24dp), false, false, false, 112, null),
    FACEBOOK("Facebook", Integer.valueOf((int) R.color.facebook), Integer.valueOf((int) R.attr.ic_account_facebook), Integer.valueOf((int) R.drawable.ic_account_facebook_white_24dp), false, false, true, 48, null),
    GITHUB("GitHub", Integer.valueOf((int) R.color.github), Integer.valueOf((int) R.attr.ic_account_github), Integer.valueOf((int) R.drawable.ic_account_github_white_24dp), false, false, true, 48, null),
    LEAGUEOFLEGENDS("League of Legends", Integer.valueOf((int) R.color.league_of_legends), Integer.valueOf((int) R.attr.ic_account_league_of_legends), Integer.valueOf((int) R.drawable.ic_account_league_of_legends_white_24dp), false, false, false, 112, null),
    PLAYSTATION("PlayStation Network", Integer.valueOf((int) R.color.playstation), Integer.valueOf((int) R.attr.ic_account_playstation), Integer.valueOf((int) R.drawable.ic_account_playstation_white_24dp), false, true, true, 16, null),
    REDDIT("Reddit", Integer.valueOf((int) R.color.reddit), Integer.valueOf((int) R.attr.ic_account_reddit), Integer.valueOf((int) R.drawable.ic_account_reddit_white_24dp), false, false, true, 48, null),
    SAMSUNG("Samsung Galaxy", null, Integer.valueOf((int) R.attr.ic_account_samsung), Integer.valueOf((int) R.drawable.ic_account_samsung_white_24dp), false, true, false, 82, null),
    SKYPE("Skype", Integer.valueOf((int) R.color.skype), Integer.valueOf((int) R.attr.ic_account_skype), Integer.valueOf((int) R.drawable.ic_account_skype_white_24dp), false, false, false, 112, null),
    SPOTIFY("Spotify", Integer.valueOf((int) R.color.spotify), Integer.valueOf((int) R.attr.ic_account_spotify), Integer.valueOf((int) R.drawable.ic_account_spotify_white_24dp), false, true, true, 16, null),
    STEAM("Steam", Integer.valueOf((int) R.color.steam), Integer.valueOf((int) R.attr.ic_account_steam), Integer.valueOf((int) R.drawable.ic_account_steam_white_24dp), false, false, true, 48, null),
    TWITCH("Twitch", Integer.valueOf((int) R.color.twitch), Integer.valueOf((int) R.attr.ic_account_twitch), Integer.valueOf((int) R.drawable.ic_account_twitch_white_24dp), false, false, true, 48, null),
    TWITTER("Twitter", Integer.valueOf((int) R.color.twitter), Integer.valueOf((int) R.attr.ic_account_twitter), Integer.valueOf((int) R.drawable.ic_account_twitter_white_24dp), false, false, true, 48, null),
    XBOX("Xbox", Integer.valueOf((int) R.color.xbox), Integer.valueOf((int) R.attr.ic_account_xbox), Integer.valueOf((int) R.drawable.ic_account_xbox_white_24dp), false, true, true, 16, null),
    YOUTUBE("YouTube", Integer.valueOf((int) R.color.youtube), Integer.valueOf((int) R.attr.ic_account_youtube), Integer.valueOf((int) R.drawable.ic_account_youtube_white_24dp), false, false, true, 48, null),
    NONE(null, null, null, null, false, false, false, Opcodes.LAND, null);
    
    public static final Companion Companion = new Companion(null);
    private final boolean canShowActivity;
    private final boolean canSyncFriends;
    private final Integer colorResId;
    private final boolean enabled;
    private final String platformId;
    private final String properName;
    private final Integer themedPlatformImage;
    private final Integer whitePlatformImage;

    /* compiled from: Platform.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0007¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0007¢\u0006\u0004\b\u0005\u0010\tJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\nH\u0007¢\u0006\u0004\b\u0005\u0010\f¨\u0006\u000f"}, d2 = {"Lcom/discord/utilities/platform/Platform$Companion;", "", "Lcom/discord/api/activity/ActivityPlatform;", "activityPlatform", "Lcom/discord/utilities/platform/Platform;", "from", "(Lcom/discord/api/activity/ActivityPlatform;)Lcom/discord/utilities/platform/Platform;", "", "platformName", "(Ljava/lang/String;)Lcom/discord/utilities/platform/Platform;", "Lcom/discord/api/connectedaccounts/ConnectedAccount;", "connectedAccount", "(Lcom/discord/api/connectedaccounts/ConnectedAccount;)Lcom/discord/utilities/platform/Platform;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {

        @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public final /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                ActivityPlatform.values();
                int[] iArr = new int[9];
                $EnumSwitchMapping$0 = iArr;
                iArr[ActivityPlatform.SAMSUNG.ordinal()] = 1;
                iArr[ActivityPlatform.SPOTIFY.ordinal()] = 2;
                iArr[ActivityPlatform.XBOX.ordinal()] = 3;
                iArr[ActivityPlatform.PS4.ordinal()] = 4;
                iArr[ActivityPlatform.PS5.ordinal()] = 5;
            }
        }

        private Companion() {
        }

        public final Platform from(ActivityPlatform activityPlatform) {
            if (activityPlatform != null) {
                int ordinal = activityPlatform.ordinal();
                if (ordinal == 1) {
                    return Platform.XBOX;
                }
                if (ordinal == 4) {
                    return Platform.SAMSUNG;
                }
                if (ordinal == 5) {
                    return Platform.SPOTIFY;
                }
                if (ordinal == 6) {
                    return Platform.PLAYSTATION;
                }
                if (ordinal == 7) {
                    return Platform.PLAYSTATION;
                }
            }
            return Platform.NONE;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final Platform from(String str) {
            m.checkNotNullParameter(str, "platformName");
            try {
                String obj = w.trim(str).toString();
                Locale locale = Locale.ENGLISH;
                m.checkNotNullExpressionValue(locale, "Locale.ENGLISH");
                if (obj != null) {
                    String upperCase = obj.toUpperCase(locale);
                    m.checkNotNullExpressionValue(upperCase, "(this as java.lang.String).toUpperCase(locale)");
                    return Platform.valueOf(upperCase);
                }
                throw new NullPointerException("null cannot be cast to non-null type java.lang.String");
            } catch (Exception unused) {
                return Platform.NONE;
            }
        }

        public final Platform from(ConnectedAccount connectedAccount) {
            m.checkNotNullParameter(connectedAccount, "connectedAccount");
            return from(connectedAccount.g());
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            Platform.values();
            int[] iArr = new int[15];
            $EnumSwitchMapping$0 = iArr;
            iArr[Platform.FACEBOOK.ordinal()] = 1;
            iArr[Platform.GITHUB.ordinal()] = 2;
            iArr[Platform.REDDIT.ordinal()] = 3;
            iArr[Platform.SPOTIFY.ordinal()] = 4;
            iArr[Platform.STEAM.ordinal()] = 5;
            iArr[Platform.TWITCH.ordinal()] = 6;
            iArr[Platform.TWITTER.ordinal()] = 7;
            iArr[Platform.YOUTUBE.ordinal()] = 8;
        }
    }

    Platform(String str, @ColorRes Integer num, @AttrRes Integer num2, @DrawableRes Integer num3, boolean z2, boolean z3, boolean z4) {
        this.properName = str;
        this.colorResId = num;
        this.themedPlatformImage = num2;
        this.whitePlatformImage = num3;
        this.canSyncFriends = z2;
        this.canShowActivity = z3;
        this.enabled = z4;
        String name = name();
        Locale locale = Locale.ENGLISH;
        m.checkNotNullExpressionValue(locale, "Locale.ENGLISH");
        Objects.requireNonNull(name, "null cannot be cast to non-null type java.lang.String");
        String lowerCase = name.toLowerCase(locale);
        m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
        this.platformId = lowerCase;
    }

    public static final Platform from(ActivityPlatform activityPlatform) {
        return Companion.from(activityPlatform);
    }

    public static final Platform from(ConnectedAccount connectedAccount) {
        return Companion.from(connectedAccount);
    }

    public static final Platform from(String str) {
        return Companion.from(str);
    }

    public final boolean getCanShowActivity() {
        return this.canShowActivity;
    }

    public final boolean getCanSyncFriends() {
        return this.canSyncFriends;
    }

    public final Integer getColorResId() {
        return this.colorResId;
    }

    public final boolean getEnabled() {
        return this.enabled;
    }

    public final String getPlatformId() {
        return this.platformId;
    }

    public final String getProfileUrl(ConnectedAccount connectedAccount) {
        m.checkNotNullParameter(connectedAccount, "connectedAccount");
        int ordinal = ordinal();
        if (ordinal == 1) {
            StringBuilder R = a.R("https://www.facebook.com/");
            R.append(connectedAccount.b());
            return R.toString();
        } else if (ordinal == 2) {
            StringBuilder R2 = a.R("https://github.com/");
            R2.append(connectedAccount.d());
            return R2.toString();
        } else if (ordinal == 5) {
            StringBuilder R3 = a.R("https://reddit.com/u/");
            R3.append(connectedAccount.d());
            return R3.toString();
        } else if (ordinal != 13) {
            switch (ordinal) {
                case 8:
                    StringBuilder R4 = a.R("https://open.spotify.com/user/");
                    R4.append(connectedAccount.b());
                    return R4.toString();
                case 9:
                    StringBuilder R5 = a.R("https://steamcommunity.com/profiles/");
                    R5.append(connectedAccount.b());
                    return R5.toString();
                case 10:
                    StringBuilder R6 = a.R("https://twitch.tv/");
                    R6.append(connectedAccount.d());
                    return R6.toString();
                case 11:
                    StringBuilder R7 = a.R("https://www.twitter.com/");
                    R7.append(connectedAccount.d());
                    return R7.toString();
                default:
                    return null;
            }
        } else {
            StringBuilder R8 = a.R("https://youtube.com/channel/");
            R8.append(connectedAccount.b());
            return R8.toString();
        }
    }

    public final String getProperName() {
        return this.properName;
    }

    public final Integer getThemedPlatformImage() {
        return this.themedPlatformImage;
    }

    public final Integer getWhitePlatformImage() {
        return this.whitePlatformImage;
    }

    /* synthetic */ Platform(String str, Integer num, Integer num2, Integer num3, boolean z2, boolean z3, boolean z4, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? "" : str, (i & 2) != 0 ? null : num, (i & 4) != 0 ? null : num2, (i & 8) != 0 ? null : num3, (i & 16) != 0 ? false : z2, (i & 32) != 0 ? false : z3, (i & 64) != 0 ? false : z4);
    }
}
