package com.discord.utilities.systemlog;

import b.d.b.a.a;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: SystemLogUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u0005\n\u0000\n\u0002\u0010\r\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "it", "", "invoke", "(B)Ljava/lang/CharSequence;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SystemLogUtils$hashString$1 extends o implements Function1<Byte, CharSequence> {
    public static final SystemLogUtils$hashString$1 INSTANCE = new SystemLogUtils$hashString$1();

    public SystemLogUtils$hashString$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ CharSequence invoke(Byte b2) {
        return invoke(b2.byteValue());
    }

    public final CharSequence invoke(byte b2) {
        return a.N(new Object[]{Byte.valueOf(b2)}, 1, "%02X", "java.lang.String.format(this, *args)");
    }
}
