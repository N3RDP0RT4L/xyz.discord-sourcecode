package com.discord.utilities.systemlog;

import andhook.lib.HookHelper;
import com.discord.utilities.collections.FixedSizeLineBuffer;
import d0.g0.c;
import d0.g0.w;
import d0.v.a;
import d0.z.d.m;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: SystemLogCapture.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 \u00152\u00020\u0001:\u0001\u0015B\u0007¢\u0006\u0004\b\u0014\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\r\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\u0006\u0010\u0007J\r\u0010\t\u001a\u00020\b¢\u0006\u0004\b\t\u0010\nJ\u0019\u0010\u000e\u001a\u00020\u00022\n\u0010\r\u001a\u00060\u000bj\u0002`\f¢\u0006\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0011\u001a\u00020\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0013\u001a\u00020\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0012¨\u0006\u0016"}, d2 = {"Lcom/discord/utilities/systemlog/SystemLogCapture;", "", "", "start", "()V", "Ljava/lang/Thread;", "startThread", "()Ljava/lang/Thread;", "", "getOutput", "()Ljava/lang/String;", "Ljava/lang/StringBuilder;", "Lkotlin/text/StringBuilder;", "sb", "appendOutput", "(Ljava/lang/StringBuilder;)V", "Lcom/discord/utilities/collections/FixedSizeLineBuffer;", "buffer", "Lcom/discord/utilities/collections/FixedSizeLineBuffer;", "tombstoneBuffer", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SystemLogCapture {
    public static final Companion Companion = new Companion(null);
    private final FixedSizeLineBuffer buffer = new FixedSizeLineBuffer(524288);
    private final FixedSizeLineBuffer tombstoneBuffer = new FixedSizeLineBuffer(102400);

    /* compiled from: SystemLogCapture.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0000¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\n"}, d2 = {"Lcom/discord/utilities/systemlog/SystemLogCapture$Companion;", "", "", "line", "", "shouldIncludeLogLine$app_productionGoogleRelease", "(Ljava/lang/String;)Z", "shouldIncludeLogLine", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final boolean shouldIncludeLogLine$app_productionGoogleRelease(String str) {
            m.checkNotNullParameter(str, "line");
            return !w.contains$default((CharSequence) str, (CharSequence) "chatty  : uid=", false, 2, (Object) null);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void start() {
        if (!new File(SystemLogUtils.logcatPath).exists()) {
            this.buffer.addLine("Unable to locate '/system/bin/logcat'");
            return;
        }
        while (true) {
            Process process = null;
            try {
                try {
                    process = new ProcessBuilder(SystemLogUtils.logcatPath).redirectErrorStream(true).start();
                    m.checkNotNullExpressionValue(process, "logcatProcess");
                    InputStream inputStream = process.getInputStream();
                    m.checkNotNullExpressionValue(inputStream, "logcatProcess.inputStream");
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream, c.a);
                    BufferedReader bufferedReader = inputStreamReader instanceof BufferedReader ? (BufferedReader) inputStreamReader : new BufferedReader(inputStreamReader, 8192);
                    while (true) {
                        try {
                            String readLine = bufferedReader.readLine();
                            if (readLine == null) {
                                break;
                            }
                            if (Companion.shouldIncludeLogLine$app_productionGoogleRelease(readLine)) {
                                this.buffer.addLine(readLine);
                            }
                            if (SystemLogUtils.INSTANCE.getRegexExtractTombstone$app_productionGoogleRelease().matches(readLine)) {
                                this.tombstoneBuffer.addLine(readLine);
                            }
                        } catch (IOException unused) {
                        }
                    }
                    bufferedReader.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    this.buffer.addLine("Exception getting system logs '" + e + '\'');
                    StackTraceElement[] stackTrace = e.getStackTrace();
                    m.checkNotNullExpressionValue(stackTrace, "e.stackTrace");
                    for (StackTraceElement stackTraceElement : stackTrace) {
                        this.buffer.addLine("    " + stackTraceElement);
                    }
                    if (process == null) {
                    }
                }
                process.destroy();
                Thread.sleep(1000L);
            } catch (Throwable th) {
                if (process != null) {
                    process.destroy();
                }
                throw th;
            }
        }
    }

    public final void appendOutput(StringBuilder sb) {
        m.checkNotNullParameter(sb, "sb");
        this.tombstoneBuffer.appendString(sb);
        this.buffer.appendString(sb);
    }

    public final String getOutput() {
        return this.tombstoneBuffer.getString() + '\n' + this.buffer.getString();
    }

    public final Thread startThread() {
        Thread thread;
        thread = a.thread((r12 & 1) != 0 ? true : true, (r12 & 2) != 0 ? false : true, (r12 & 4) != 0 ? null : null, (r12 & 8) != 0 ? null : SystemLogCapture.class.getSimpleName(), (r12 & 16) != 0 ? -1 : 0, new SystemLogCapture$startThread$1(this));
        return thread;
    }
}
