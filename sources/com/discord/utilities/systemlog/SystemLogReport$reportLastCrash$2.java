package com.discord.utilities.systemlog;

import com.discord.app.AppLog;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.systemlog.SystemLogUtils;
import d0.g0.w;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: SystemLogReport.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/systemlog/SystemLogUtils$Tombstone;", "crash", "", "invoke", "(Lcom/discord/utilities/systemlog/SystemLogUtils$Tombstone;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SystemLogReport$reportLastCrash$2 extends o implements Function1<SystemLogUtils.Tombstone, Unit> {
    public static final SystemLogReport$reportLastCrash$2 INSTANCE = new SystemLogReport$reportLastCrash$2();

    public SystemLogReport$reportLastCrash$2() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(SystemLogUtils.Tombstone tombstone) {
        invoke2(tombstone);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(SystemLogUtils.Tombstone tombstone) {
        boolean checkHashChanged;
        m.checkNotNullParameter(tombstone, "crash");
        for (String str : w.split$default((CharSequence) tombstone.getText(), new String[]{"\n"}, false, 0, 6, (Object) null)) {
            AppLog.g.recordBreadcrumb(str, "Tombstone");
        }
        AppLog appLog = AppLog.g;
        appLog.recordBreadcrumb(tombstone.getGroupHash(), "Tombstone-Hash");
        SystemLogReport systemLogReport = SystemLogReport.INSTANCE;
        checkHashChanged = systemLogReport.checkHashChanged(tombstone.getTextHash());
        if (checkHashChanged) {
            Logger.e$default(appLog, "Tombstone", null, null, 6, null);
        }
        if (!checkHashChanged) {
            tombstone = null;
        }
        systemLogReport.sendReport(tombstone);
    }
}
