package com.discord.utilities.systemlog;

import andhook.lib.HookHelper;
import android.content.SharedPreferences;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.cache.SharedPreferencesProvider;
import com.discord.utilities.debug.HistoricalProcessExitReason;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.systemlog.SystemLogUtils;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: SystemLogReport.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0014\u0010\rJ\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\n\u001a\u00020\t2\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\n\u0010\u000bJ\r\u0010\f\u001a\u00020\u0004¢\u0006\u0004\b\f\u0010\rR\u0016\u0010\u000e\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0013\u001a\u00020\u00108B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u0012¨\u0006\u0015"}, d2 = {"Lcom/discord/utilities/systemlog/SystemLogReport;", "", "Lcom/discord/utilities/systemlog/SystemLogUtils$Tombstone;", "tombstone", "", "sendReport", "(Lcom/discord/utilities/systemlog/SystemLogUtils$Tombstone;)V", "", "hash", "", "checkHashChanged", "(Ljava/lang/String;)Z", "reportLastCrash", "()V", "CACHE_KEY_LAST_STORED_TOMBSTONE_HASH", "Ljava/lang/String;", "Landroid/content/SharedPreferences;", "getPrefsSessionDurable", "()Landroid/content/SharedPreferences;", "prefsSessionDurable", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SystemLogReport {
    private static final String CACHE_KEY_LAST_STORED_TOMBSTONE_HASH = "CACHE_KEY_SYSTEM_LOG_STORE_LAST_STORED_TOMBSTONE_HASH";
    public static final SystemLogReport INSTANCE = new SystemLogReport();

    private SystemLogReport() {
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final boolean checkHashChanged(String str) {
        if (m.areEqual(getPrefsSessionDurable().getString(CACHE_KEY_LAST_STORED_TOMBSTONE_HASH, null), str)) {
            return false;
        }
        SharedPreferences.Editor edit = getPrefsSessionDurable().edit();
        m.checkNotNullExpressionValue(edit, "editor");
        edit.putString(CACHE_KEY_LAST_STORED_TOMBSTONE_HASH, str);
        edit.apply();
        return true;
    }

    private final SharedPreferences getPrefsSessionDurable() {
        return SharedPreferencesProvider.INSTANCE.getPrefsSessionDurable();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void sendReport(SystemLogUtils.Tombstone tombstone) {
        boolean didCrashOnPreviousExecution = FirebaseCrashlytics.getInstance().didCrashOnPreviousExecution();
        HistoricalProcessExitReason.Reason lastReason = HistoricalProcessExitReason.INSTANCE.getLastReason();
        AnalyticsTracker.INSTANCE.appNativeCrash(didCrashOnPreviousExecution, lastReason != null ? lastReason.getReason() : null, lastReason != null ? lastReason.getReason() : null, tombstone != null ? tombstone.getGroupHash() : null, tombstone != null ? tombstone.getCause() : null);
    }

    public final void reportLastCrash() {
        ObservableExtensionsKt.appSubscribe(SystemLogUtils.INSTANCE.fetchLastTombstone(), SystemLogReport.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : SystemLogReport$reportLastCrash$1.INSTANCE, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, SystemLogReport$reportLastCrash$2.INSTANCE);
    }
}
