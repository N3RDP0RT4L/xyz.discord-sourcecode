package com.discord.utilities.systemlog;

import androidx.core.app.NotificationCompat;
import com.discord.utilities.systemlog.SystemLogUtils;
import d0.z.d.m;
import j0.k.b;
import j0.l.a.c;
import j0.l.e.k;
import java.util.LinkedList;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: SystemLogUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\b\u001a*\u0012\u000e\b\u0001\u0012\n \u0002*\u0004\u0018\u00010\u00050\u0005 \u0002*\u0014\u0012\u000e\b\u0001\u0012\n \u0002*\u0004\u0018\u00010\u00050\u0005\u0018\u00010\u00040\u00042\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Ljava/util/LinkedList;", "", "kotlin.jvm.PlatformType", "crashes", "Lrx/Observable;", "Lcom/discord/utilities/systemlog/SystemLogUtils$Tombstone;", NotificationCompat.CATEGORY_CALL, "(Ljava/util/LinkedList;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SystemLogUtils$fetchLastTombstone$1<T, R> implements b<LinkedList<String>, Observable<? extends SystemLogUtils.Tombstone>> {
    public static final SystemLogUtils$fetchLastTombstone$1 INSTANCE = new SystemLogUtils$fetchLastTombstone$1();

    public final Observable<? extends SystemLogUtils.Tombstone> call(LinkedList<String> linkedList) {
        SystemLogUtils systemLogUtils = SystemLogUtils.INSTANCE;
        m.checkNotNullExpressionValue(linkedList, "crashes");
        SystemLogUtils.Tombstone fetchLastTombstone$app_productionGoogleRelease = systemLogUtils.fetchLastTombstone$app_productionGoogleRelease(linkedList);
        if (fetchLastTombstone$app_productionGoogleRelease == null) {
            return c.k;
        }
        return new k(fetchLastTombstone$app_productionGoogleRelease);
    }
}
