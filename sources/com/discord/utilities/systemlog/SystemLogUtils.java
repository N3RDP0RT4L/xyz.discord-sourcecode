package com.discord.utilities.systemlog;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.adjust.sdk.Constants;
import com.discord.utilities.debug.DebugPrintableCollection;
import d0.f0.q;
import d0.g0.c;
import d0.g0.i;
import d0.g0.p;
import d0.g0.t;
import d0.g0.w;
import d0.t.k;
import d0.t.n0;
import d0.z.d.m;
import java.io.File;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.sequences.Sequence;
import kotlin.text.MatchResult;
import kotlin.text.Regex;
import rx.Observable;
/* compiled from: SystemLogUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\u00020\u0001:\u00015B\t\b\u0002¢\u0006\u0004\b3\u00104J%\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\n\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\r\u001a\u00020\fH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\r\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\u0007\u0010\u0011J\r\u0010\u0013\u001a\u00020\u0012¢\u0006\u0004\b\u0013\u0010\u0014J\u0013\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00150\u0004¢\u0006\u0004\b\u0016\u0010\u0017J\u001f\u0010\u0016\u001a\u0004\u0018\u00010\u00152\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00060\u0018H\u0000¢\u0006\u0004\b\u001a\u0010\u001bJ/\u0010!\u001a\u00020\u000e2\u0006\u0010\u001d\u001a\u00020\u001c2\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0000¢\u0006\u0004\b\u001f\u0010 R\u0016\u0010\"\u001a\u00020\u00068\u0000@\u0000X\u0080T¢\u0006\u0006\n\u0004\b\"\u0010#R\u001c\u0010%\u001a\u00020$8\u0000@\u0000X\u0080\u0004¢\u0006\f\n\u0004\b%\u0010&\u001a\u0004\b'\u0010(R\u0016\u0010*\u001a\u00020)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010+R\u001c\u0010,\u001a\u00020\u00028\u0000@\u0000X\u0080\u0004¢\u0006\f\n\u0004\b,\u0010-\u001a\u0004\b.\u0010/R\u0016\u00101\u001a\u0002008\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b1\u00102¨\u00066"}, d2 = {"Lcom/discord/utilities/systemlog/SystemLogUtils;", "", "Lkotlin/text/Regex;", "filter", "Lrx/Observable;", "Ljava/util/LinkedList;", "", "fetch", "(Lkotlin/text/Regex;)Lrx/Observable;", "input", "hashString", "(Ljava/lang/String;)Ljava/lang/String;", "Ljava/lang/Process;", "process", "", "waitFor", "(Ljava/lang/Process;)V", "()Ljava/lang/String;", "Ljava/lang/Thread;", "initSystemLogCapture", "()Ljava/lang/Thread;", "Lcom/discord/utilities/systemlog/SystemLogUtils$Tombstone;", "fetchLastTombstone", "()Lrx/Observable;", "", "crashes", "fetchLastTombstone$app_productionGoogleRelease", "(Ljava/util/Collection;)Lcom/discord/utilities/systemlog/SystemLogUtils$Tombstone;", "Ljava/io/BufferedReader;", "reader", "output", "processLogs$app_productionGoogleRelease", "(Ljava/io/BufferedReader;Ljava/util/LinkedList;Lkotlin/text/Regex;)V", "processLogs", "logcatPath", "Ljava/lang/String;", "Lcom/discord/utilities/debug/DebugPrintableCollection;", "debugPrintables", "Lcom/discord/utilities/debug/DebugPrintableCollection;", "getDebugPrintables$app_productionGoogleRelease", "()Lcom/discord/utilities/debug/DebugPrintableCollection;", "Lcom/discord/utilities/systemlog/SystemLogCapture;", "systemLogCapture", "Lcom/discord/utilities/systemlog/SystemLogCapture;", "regexExtractTombstone", "Lkotlin/text/Regex;", "getRegexExtractTombstone$app_productionGoogleRelease", "()Lkotlin/text/Regex;", "", "maxLogSize", "I", HookHelper.constructorName, "()V", "Tombstone", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SystemLogUtils {
    public static final String logcatPath = "/system/bin/logcat";
    private static final int maxLogSize = 2500000;
    private static final Regex regexExtractTombstone;
    public static final SystemLogUtils INSTANCE = new SystemLogUtils();
    private static final DebugPrintableCollection debugPrintables = new DebugPrintableCollection();
    private static final SystemLogCapture systemLogCapture = new SystemLogCapture();

    /* compiled from: SystemLogUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0010\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B9\u0012\u0006\u0010\n\u001a\u00020\u0002\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\r\u001a\u00020\u0002\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\u0006\u0010\u000f\u001a\u00020\u0002¢\u0006\u0004\b!\u0010\"J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0004J\u0010\u0010\b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\t\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\u0004JN\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u00022\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\f\u001a\u00020\u00022\b\b\u0002\u0010\r\u001a\u00020\u00022\b\b\u0002\u0010\u000e\u001a\u00020\u00022\b\b\u0002\u0010\u000f\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0004J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0018\u001a\u00020\u00172\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001a\u001a\u0004\b\u001b\u0010\u0004R\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001a\u001a\u0004\b\u001c\u0010\u0004R\u0019\u0010\n\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u001a\u001a\u0004\b\u001d\u0010\u0004R\u0019\u0010\r\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001a\u001a\u0004\b\u001e\u0010\u0004R\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001a\u001a\u0004\b\u001f\u0010\u0004R\u001b\u0010\u000b\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001a\u001a\u0004\b \u0010\u0004¨\u0006#"}, d2 = {"Lcom/discord/utilities/systemlog/SystemLogUtils$Tombstone;", "", "", "component1", "()Ljava/lang/String;", "component2", "component3", "component4", "component5", "component6", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "cause", "groupBy", "origin", "groupHash", "textHash", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/utilities/systemlog/SystemLogUtils$Tombstone;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getGroupBy", "getGroupHash", "getText", "getOrigin", "getTextHash", "getCause", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Tombstone {
        private final String cause;
        private final String groupBy;
        private final String groupHash;
        private final String origin;
        private final String text;
        private final String textHash;

        public Tombstone(String str, String str2, String str3, String str4, String str5, String str6) {
            m.checkNotNullParameter(str, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
            m.checkNotNullParameter(str3, "groupBy");
            m.checkNotNullParameter(str4, "origin");
            m.checkNotNullParameter(str5, "groupHash");
            m.checkNotNullParameter(str6, "textHash");
            this.text = str;
            this.cause = str2;
            this.groupBy = str3;
            this.origin = str4;
            this.groupHash = str5;
            this.textHash = str6;
        }

        public static /* synthetic */ Tombstone copy$default(Tombstone tombstone, String str, String str2, String str3, String str4, String str5, String str6, int i, Object obj) {
            if ((i & 1) != 0) {
                str = tombstone.text;
            }
            if ((i & 2) != 0) {
                str2 = tombstone.cause;
            }
            String str7 = str2;
            if ((i & 4) != 0) {
                str3 = tombstone.groupBy;
            }
            String str8 = str3;
            if ((i & 8) != 0) {
                str4 = tombstone.origin;
            }
            String str9 = str4;
            if ((i & 16) != 0) {
                str5 = tombstone.groupHash;
            }
            String str10 = str5;
            if ((i & 32) != 0) {
                str6 = tombstone.textHash;
            }
            return tombstone.copy(str, str7, str8, str9, str10, str6);
        }

        public final String component1() {
            return this.text;
        }

        public final String component2() {
            return this.cause;
        }

        public final String component3() {
            return this.groupBy;
        }

        public final String component4() {
            return this.origin;
        }

        public final String component5() {
            return this.groupHash;
        }

        public final String component6() {
            return this.textHash;
        }

        public final Tombstone copy(String str, String str2, String str3, String str4, String str5, String str6) {
            m.checkNotNullParameter(str, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
            m.checkNotNullParameter(str3, "groupBy");
            m.checkNotNullParameter(str4, "origin");
            m.checkNotNullParameter(str5, "groupHash");
            m.checkNotNullParameter(str6, "textHash");
            return new Tombstone(str, str2, str3, str4, str5, str6);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Tombstone)) {
                return false;
            }
            Tombstone tombstone = (Tombstone) obj;
            return m.areEqual(this.text, tombstone.text) && m.areEqual(this.cause, tombstone.cause) && m.areEqual(this.groupBy, tombstone.groupBy) && m.areEqual(this.origin, tombstone.origin) && m.areEqual(this.groupHash, tombstone.groupHash) && m.areEqual(this.textHash, tombstone.textHash);
        }

        public final String getCause() {
            return this.cause;
        }

        public final String getGroupBy() {
            return this.groupBy;
        }

        public final String getGroupHash() {
            return this.groupHash;
        }

        public final String getOrigin() {
            return this.origin;
        }

        public final String getText() {
            return this.text;
        }

        public final String getTextHash() {
            return this.textHash;
        }

        public int hashCode() {
            String str = this.text;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.cause;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.groupBy;
            int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
            String str4 = this.origin;
            int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
            String str5 = this.groupHash;
            int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
            String str6 = this.textHash;
            if (str6 != null) {
                i = str6.hashCode();
            }
            return hashCode5 + i;
        }

        public String toString() {
            StringBuilder R = a.R("Tombstone(text=");
            R.append(this.text);
            R.append(", cause=");
            R.append(this.cause);
            R.append(", groupBy=");
            R.append(this.groupBy);
            R.append(", origin=");
            R.append(this.origin);
            R.append(", groupHash=");
            R.append(this.groupHash);
            R.append(", textHash=");
            return a.H(R, this.textHash, ")");
        }
    }

    static {
        StringBuilder R = a.R("(?:^(?:[^\\s]+\\s+){4}F\\s+DEBUG\\s+:\\s(.+))|(^.+(\\[");
        R.append(Regex.j.escape("libdiscord_version"));
        R.append("\\].+))");
        regexExtractTombstone = new Regex(R.toString());
    }

    private SystemLogUtils() {
    }

    private final String hashString(String str) {
        MessageDigest messageDigest = MessageDigest.getInstance(Constants.SHA1);
        Charset charset = c.a;
        Objects.requireNonNull(str, "null cannot be cast to non-null type java.lang.String");
        byte[] bytes = str.getBytes(charset);
        m.checkNotNullExpressionValue(bytes, "(this as java.lang.String).getBytes(charset)");
        byte[] digest = messageDigest.digest(bytes);
        m.checkNotNullExpressionValue(digest, "bytes");
        return k.joinToString$default(digest, "", (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, SystemLogUtils$hashString$1.INSTANCE, 30, (Object) null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void waitFor(Process process) {
        long nanos = TimeUnit.SECONDS.toNanos(15L) + System.nanoTime();
        do {
            try {
                process.exitValue();
                return;
            } catch (IllegalThreadStateException unused) {
                Thread.sleep(100L);
                if (System.nanoTime() >= nanos) {
                }
            }
        } while (System.nanoTime() >= nanos);
    }

    public final String fetch() {
        StringBuilder sb = new StringBuilder();
        debugPrintables.debugPrint(sb);
        systemLogCapture.appendOutput(sb);
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "sb.toString()");
        return sb2;
    }

    public final Observable<Tombstone> fetchLastTombstone() {
        Observable z2 = fetch(regexExtractTombstone).z(SystemLogUtils$fetchLastTombstone$1.INSTANCE);
        m.checkNotNullExpressionValue(z2, "fetch(regexExtractTombst…ble.just(tombstone)\n    }");
        return z2;
    }

    public final Tombstone fetchLastTombstone$app_productionGoogleRelease(Collection<String> collection) {
        String str;
        List<String> groupValues;
        m.checkNotNullParameter(collection, "crashes");
        String str2 = null;
        if (collection.isEmpty()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        String str3 = null;
        String str4 = null;
        for (String str5 : collection) {
            if (w.contains$default((CharSequence) str5, (CharSequence) "libdiscord_version", false, 2, (Object) null)) {
                str3 = str5;
            } else {
                if (w.contains$default((CharSequence) str5, (CharSequence) "*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***", false, 2, (Object) null)) {
                    p.clear(sb);
                    str4 = str3;
                }
                sb.append(str5);
                m.checkNotNullExpressionValue(sb, "append(value)");
                sb.append('\n');
                m.checkNotNullExpressionValue(sb, "append('\\n')");
            }
        }
        if (!t.isBlank(sb)) {
            sb.append('\n');
            m.checkNotNullExpressionValue(sb, "append('\\n')");
            sb.append("Tombstone's libdiscord_version: ");
            if (str4 == null) {
                str4 = "Unknown libdiscord_version";
            }
            sb.append(str4);
            m.checkNotNullExpressionValue(sb, "append(value)");
            sb.append('\n');
            m.checkNotNullExpressionValue(sb, "append('\\n')");
        }
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "StringBuilder().also { s…\n      }\n    }.toString()");
        if (t.isBlank(sb2)) {
            return null;
        }
        i iVar = i.MULTILINE;
        Regex regex = new Regex("^Cause: (.+)$", iVar);
        Sequence map = q.map(Regex.findAll$default(new Regex("^\\s+#\\d+ pc .+/(.+? .+?)\\+?[+)]", iVar), sb2, 0, 2, null), new SystemLogUtils$fetchLastTombstone$extractedGroups$1(new Regex("classes\\d+.dex")));
        HashSet hashSetOf = n0.hashSetOf("libc.so (abort", "libart.so (art::Runtime::Abort(char const*", "libbase.so (android::base::LogMessage::~LogMessage(");
        try {
        } catch (NoSuchElementException unused) {
            str = "Unknown";
        }
        for (Object obj : map) {
            if (!hashSetOf.contains((String) obj)) {
                str = (String) obj;
                String joinToString$default = q.joinToString$default(map, "\n", null, null, 0, null, null, 62, null);
                MatchResult find$default = Regex.find$default(regex, sb2, 0, 2, null);
                if (!(find$default == null || (groupValues = find$default.getGroupValues()) == null)) {
                    str2 = groupValues.get(1);
                }
                return new Tombstone(sb2, str2, joinToString$default, str, hashString(joinToString$default), hashString(sb2));
            }
        }
        throw new NoSuchElementException("Sequence contains no element matching the predicate.");
    }

    public final DebugPrintableCollection getDebugPrintables$app_productionGoogleRelease() {
        return debugPrintables;
    }

    public final Regex getRegexExtractTombstone$app_productionGoogleRelease() {
        return regexExtractTombstone;
    }

    public final Thread initSystemLogCapture() {
        return systemLogCapture.startThread();
    }

    /* JADX WARN: Code restructure failed: missing block: B:20:0x004f, code lost:
        throw new java.util.NoSuchElementException("List contains no element matching the predicate.");
     */
    /* JADX WARN: Removed duplicated region for block: B:41:0x0098  */
    /* JADX WARN: Removed duplicated region for block: B:63:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void processLogs$app_productionGoogleRelease(java.io.BufferedReader r10, java.util.LinkedList<java.lang.String> r11, kotlin.text.Regex r12) {
        /*
            r9 = this;
            java.lang.String r0 = "reader"
            d0.z.d.m.checkNotNullParameter(r10, r0)
            java.lang.String r0 = "output"
            d0.z.d.m.checkNotNullParameter(r11, r0)
            r0 = 0
            r1 = 0
        Lc:
            r2 = 2500000(0x2625a0, float:3.503246E-39)
            java.lang.String r3 = r10.readLine()     // Catch: java.lang.Exception -> L7c
            if (r3 == 0) goto L96
            r4 = 0
            r5 = 2
            r6 = 1
            if (r12 == 0) goto L50
            kotlin.text.MatchResult r3 = kotlin.text.Regex.find$default(r12, r3, r0, r5, r4)     // Catch: java.lang.Exception -> L7c
            if (r3 == 0) goto Lc
            java.util.List r3 = r3.getGroupValues()     // Catch: java.lang.Exception -> L7c
            int r7 = r3.size()     // Catch: java.lang.Exception -> L7c
            java.util.ListIterator r3 = r3.listIterator(r7)     // Catch: java.lang.Exception -> L7c
        L2c:
            boolean r7 = r3.hasPrevious()     // Catch: java.lang.Exception -> L7c
            if (r7 == 0) goto L48
            java.lang.Object r7 = r3.previous()     // Catch: java.lang.Exception -> L7c
            r8 = r7
            java.lang.String r8 = (java.lang.String) r8     // Catch: java.lang.Exception -> L7c
            int r8 = r8.length()     // Catch: java.lang.Exception -> L7c
            if (r8 <= 0) goto L41
            r8 = 1
            goto L42
        L41:
            r8 = 0
        L42:
            if (r8 == 0) goto L2c
            r3 = r7
            java.lang.String r3 = (java.lang.String) r3     // Catch: java.lang.Exception -> L7c
            goto L50
        L48:
            java.util.NoSuchElementException r10 = new java.util.NoSuchElementException     // Catch: java.lang.Exception -> L7c
            java.lang.String r0 = "List contains no element matching the predicate."
            r10.<init>(r0)     // Catch: java.lang.Exception -> L7c
            throw r10     // Catch: java.lang.Exception -> L7c
        L50:
            boolean r7 = d0.g0.t.isBlank(r3)     // Catch: java.lang.Exception -> L7c
            if (r7 == 0) goto L57
            goto Lc
        L57:
            java.lang.String r7 = "Accessing hidden method"
            boolean r4 = d0.g0.w.contains$default(r3, r7, r0, r5, r4)     // Catch: java.lang.Exception -> L7c
            if (r4 == 0) goto L60
            goto Lc
        L60:
            int r4 = r3.length()     // Catch: java.lang.Exception -> L7c
            int r4 = r4 + r6
            int r4 = r4 + r1
            r11.add(r3)     // Catch: java.lang.Exception -> L79
            if (r4 <= r2) goto L77
            java.lang.Object r1 = r11.pop()     // Catch: java.lang.Exception -> L79
            java.lang.String r1 = (java.lang.String) r1     // Catch: java.lang.Exception -> L79
            int r1 = r1.length()     // Catch: java.lang.Exception -> L79
            int r1 = r1 - r6
            int r4 = r4 - r1
        L77:
            r1 = r4
            goto Lc
        L79:
            r10 = move-exception
            r1 = r4
            goto L7d
        L7c:
            r10 = move-exception
        L7d:
            r10.printStackTrace()
            if (r12 != 0) goto L96
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r0 = "IOException: "
            r12.append(r0)
            r12.append(r10)
            java.lang.String r10 = r12.toString()
            r11.add(r10)
        L96:
            if (r1 <= r2) goto La1
            java.lang.Object r10 = r11.pop()
            java.lang.String r10 = (java.lang.String) r10
            r10.length()
        La1:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.systemlog.SystemLogUtils.processLogs$app_productionGoogleRelease(java.io.BufferedReader, java.util.LinkedList, kotlin.text.Regex):void");
    }

    private final Observable<LinkedList<String>> fetch(final Regex regex) {
        final LinkedList linkedList = new LinkedList();
        final boolean z2 = regex == null;
        if (!new File(logcatPath).exists()) {
            if (z2) {
                linkedList.add("Unable to locate '/system/bin/logcat'");
            }
            j0.l.e.k kVar = new j0.l.e.k(linkedList);
            m.checkNotNullExpressionValue(kVar, "Observable.just(output)");
            return kVar;
        }
        Observable<LinkedList<String>> X = Observable.C(new Callable<LinkedList<String>>() { // from class: com.discord.utilities.systemlog.SystemLogUtils$fetch$1
            /* JADX WARN: Code restructure failed: missing block: B:17:0x009b, code lost:
                if (r0 != null) goto L8;
             */
            @Override // java.util.concurrent.Callable
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public final java.util.LinkedList<java.lang.String> call() {
                /*
                    r8 = this;
                    r0 = 0
                    java.lang.ProcessBuilder r1 = new java.lang.ProcessBuilder     // Catch: java.lang.Throwable -> L4e java.lang.Exception -> L50
                    java.lang.String r2 = "/system/bin/logcat"
                    java.lang.String r3 = "-d"
                    java.lang.String[] r2 = new java.lang.String[]{r2, r3}     // Catch: java.lang.Throwable -> L4e java.lang.Exception -> L50
                    r1.<init>(r2)     // Catch: java.lang.Throwable -> L4e java.lang.Exception -> L50
                    r2 = 1
                    java.lang.ProcessBuilder r1 = r1.redirectErrorStream(r2)     // Catch: java.lang.Throwable -> L4e java.lang.Exception -> L50
                    java.lang.Process r0 = r1.start()     // Catch: java.lang.Throwable -> L4e java.lang.Exception -> L50
                    com.discord.utilities.systemlog.SystemLogUtils r1 = com.discord.utilities.systemlog.SystemLogUtils.INSTANCE     // Catch: java.lang.Throwable -> L4e java.lang.Exception -> L50
                    java.lang.String r2 = "logcatProcess"
                    d0.z.d.m.checkNotNullExpressionValue(r0, r2)     // Catch: java.lang.Throwable -> L4e java.lang.Exception -> L50
                    com.discord.utilities.systemlog.SystemLogUtils.access$waitFor(r1, r0)     // Catch: java.lang.Throwable -> L4e java.lang.Exception -> L50
                    java.io.InputStream r2 = r0.getInputStream()     // Catch: java.lang.Throwable -> L4e java.lang.Exception -> L50
                    java.lang.String r3 = "logcatProcess.inputStream"
                    d0.z.d.m.checkNotNullExpressionValue(r2, r3)     // Catch: java.lang.Throwable -> L4e java.lang.Exception -> L50
                    java.nio.charset.Charset r3 = d0.g0.c.a     // Catch: java.lang.Throwable -> L4e java.lang.Exception -> L50
                    java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch: java.lang.Throwable -> L4e java.lang.Exception -> L50
                    r4.<init>(r2, r3)     // Catch: java.lang.Throwable -> L4e java.lang.Exception -> L50
                    r2 = 8192(0x2000, float:1.14794E-41)
                    boolean r3 = r4 instanceof java.io.BufferedReader     // Catch: java.lang.Throwable -> L4e java.lang.Exception -> L50
                    if (r3 == 0) goto L3a
                    java.io.BufferedReader r4 = (java.io.BufferedReader) r4     // Catch: java.lang.Throwable -> L4e java.lang.Exception -> L50
                    goto L40
                L3a:
                    java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch: java.lang.Throwable -> L4e java.lang.Exception -> L50
                    r3.<init>(r4, r2)     // Catch: java.lang.Throwable -> L4e java.lang.Exception -> L50
                    r4 = r3
                L40:
                    java.util.LinkedList r2 = r1     // Catch: java.lang.Throwable -> L4e java.lang.Exception -> L50
                    kotlin.text.Regex r3 = r2     // Catch: java.lang.Throwable -> L4e java.lang.Exception -> L50
                    r1.processLogs$app_productionGoogleRelease(r4, r2, r3)     // Catch: java.lang.Throwable -> L4e java.lang.Exception -> L50
                    r4.close()     // Catch: java.lang.Throwable -> L4e java.lang.Exception -> L50
                L4a:
                    r0.destroy()
                    goto L9e
                L4e:
                    r1 = move-exception
                    goto La1
                L50:
                    r1 = move-exception
                    r1.printStackTrace()     // Catch: java.lang.Throwable -> L4e
                    boolean r2 = r3     // Catch: java.lang.Throwable -> L4e
                    if (r2 == 0) goto L9b
                    java.util.LinkedList r2 = r1     // Catch: java.lang.Throwable -> L4e
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch: java.lang.Throwable -> L4e
                    r3.<init>()     // Catch: java.lang.Throwable -> L4e
                    java.lang.String r4 = "Exception getting system logs '"
                    r3.append(r4)     // Catch: java.lang.Throwable -> L4e
                    r3.append(r1)     // Catch: java.lang.Throwable -> L4e
                    r4 = 39
                    r3.append(r4)     // Catch: java.lang.Throwable -> L4e
                    java.lang.String r3 = r3.toString()     // Catch: java.lang.Throwable -> L4e
                    r2.add(r3)     // Catch: java.lang.Throwable -> L4e
                    java.lang.StackTraceElement[] r1 = r1.getStackTrace()     // Catch: java.lang.Throwable -> L4e
                    java.lang.String r2 = "e.stackTrace"
                    d0.z.d.m.checkNotNullExpressionValue(r1, r2)     // Catch: java.lang.Throwable -> L4e
                    int r2 = r1.length     // Catch: java.lang.Throwable -> L4e
                    r3 = 0
                L7e:
                    if (r3 >= r2) goto L9b
                    r4 = r1[r3]     // Catch: java.lang.Throwable -> L4e
                    java.util.LinkedList r5 = r1     // Catch: java.lang.Throwable -> L4e
                    java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch: java.lang.Throwable -> L4e
                    r6.<init>()     // Catch: java.lang.Throwable -> L4e
                    java.lang.String r7 = "    "
                    r6.append(r7)     // Catch: java.lang.Throwable -> L4e
                    r6.append(r4)     // Catch: java.lang.Throwable -> L4e
                    java.lang.String r4 = r6.toString()     // Catch: java.lang.Throwable -> L4e
                    r5.add(r4)     // Catch: java.lang.Throwable -> L4e
                    int r3 = r3 + 1
                    goto L7e
                L9b:
                    if (r0 == 0) goto L9e
                    goto L4a
                L9e:
                    java.util.LinkedList r0 = r1
                    return r0
                La1:
                    if (r0 == 0) goto La6
                    r0.destroy()
                La6:
                    throw r1
                */
                throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.systemlog.SystemLogUtils$fetch$1.call():java.util.LinkedList");
            }
        }).X(j0.p.a.b().d);
        m.checkNotNullExpressionValue(X, "Observable.fromCallable …n(Schedulers.newThread())");
        return X;
    }
}
