package com.discord.utilities.io;

import andhook.lib.HookHelper;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import androidx.annotation.MainThread;
import androidx.annotation.RequiresPermission;
import androidx.core.app.NotificationCompat;
import com.discord.utilities.auth.GoogleSmartLockManager;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.string.StringUtilsKt;
import d0.t.j;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: NetworkUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\t\bÆ\u0002\u0018\u00002\u00020\u0001:\u0004#$%&B\t\b\u0002¢\u0006\u0004\b!\u0010\"J]\u0010\u000e\u001a\u00020\n2\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u00042\b\u0010\u0007\u001a\u0004\u0018\u00010\u00062\b\u0010\b\u001a\u0004\u0018\u00010\u00062\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\n0\t2\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\n0\tH\u0007¢\u0006\u0004\b\u000e\u0010\u000fJ-\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0003\u001a\u00020\u00022\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00102\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0012¢\u0006\u0004\b\u0015\u0010\u0016J\u0015\u0010\u0017\u001a\u00020\u00142\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u0017\u0010\u001a\u001a\u0004\u0018\u00010\u00192\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u001a\u0010\u001bR(\u0010\u001f\u001a\u0014\u0012\u0004\u0012\u00020\u001d\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\u001e0\u001c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 ¨\u0006'"}, d2 = {"Lcom/discord/utilities/io/NetworkUtils;", "", "Landroid/content/Context;", "context", "Landroid/net/Uri;", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "", "fileName", "desc", "Lkotlin/Function1;", "", "onSuccess", "", "onError", "downloadFile", "(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "Landroid/content/Intent;", "intent", "Lcom/discord/utilities/logging/Logger;", "logger", "", "isDeviceConnected", "(Landroid/content/Context;Landroid/content/Intent;Lcom/discord/utilities/logging/Logger;)Z", "isAirplaneModeOn", "(Landroid/content/Context;)Z", "", "getNetworkType", "(Landroid/content/Context;)Ljava/lang/Integer;", "", "", "Lkotlin/Function0;", "onDownloadListeners", "Ljava/util/Map;", HookHelper.constructorName, "()V", "DownloadManagerEnqueueFailure", "DownloadManagerFileNoFound", "DownloadManagerNotFound", "NetworkDownloadReceiver", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class NetworkUtils {
    public static final NetworkUtils INSTANCE = new NetworkUtils();
    private static final Map<Long, Function0<Unit>> onDownloadListeners = new LinkedHashMap();

    /* compiled from: NetworkUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0003\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/utilities/io/NetworkUtils$DownloadManagerEnqueueFailure;", "", HookHelper.constructorName, "()V", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class DownloadManagerEnqueueFailure extends Throwable {
    }

    /* compiled from: NetworkUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0003\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/utilities/io/NetworkUtils$DownloadManagerFileNoFound;", "", HookHelper.constructorName, "()V", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class DownloadManagerFileNoFound extends Throwable {
    }

    /* compiled from: NetworkUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0003\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/utilities/io/NetworkUtils$DownloadManagerNotFound;", "", HookHelper.constructorName, "()V", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class DownloadManagerNotFound extends Throwable {
    }

    /* compiled from: NetworkUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\t\u0010\nJ!\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/utilities/io/NetworkUtils$NetworkDownloadReceiver;", "Landroid/content/BroadcastReceiver;", "Landroid/content/Context;", "context", "Landroid/content/Intent;", "intent", "", "onReceive", "(Landroid/content/Context;Landroid/content/Intent;)V", HookHelper.constructorName, "()V", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class NetworkDownloadReceiver extends BroadcastReceiver {
        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            m.checkNotNullParameter(context, "context");
            String action = intent != null ? intent.getAction() : null;
            if (action != null && action.hashCode() == 1248865515 && action.equals("android.intent.action.DOWNLOAD_COMPLETE")) {
                long longExtra = intent.getLongExtra("extra_download_id", 0L);
                NetworkUtils networkUtils = NetworkUtils.INSTANCE;
                Function0 function0 = (Function0) NetworkUtils.onDownloadListeners.get(Long.valueOf(longExtra));
                if (function0 != null) {
                    Unit unit = (Unit) function0.invoke();
                }
            }
        }
    }

    private NetworkUtils() {
    }

    @RequiresPermission(conditional = GoogleSmartLockManager.SET_DISCORD_ACCOUNT_DETAILS, value = "android.permission.WRITE_EXTERNAL_STORAGE")
    @MainThread
    public static final void downloadFile(Context context, Uri uri, String str, String str2, Function1<? super String, Unit> function1, Function1<? super Throwable, Unit> function12) {
        long j;
        String str3;
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(function1, "onSuccess");
        m.checkNotNullParameter(function12, "onError");
        DownloadManager downloadManager = null;
        Object systemService = context != null ? context.getSystemService("download") : null;
        if (systemService instanceof DownloadManager) {
            downloadManager = systemService;
        }
        DownloadManager downloadManager2 = downloadManager;
        if (downloadManager2 != null) {
            if (str == null) {
                try {
                    str = uri.getLastPathSegment();
                } catch (Exception e) {
                    function12.invoke(e);
                    return;
                }
            }
            DownloadManager.Request notificationVisibility = new DownloadManager.Request(uri).setTitle(str).setDescription(str2).setNotificationVisibility(1);
            String str4 = Environment.DIRECTORY_DOWNLOADS;
            if (str == null || (str3 = StringUtilsKt.filenameSanitized(str)) == null) {
                str3 = "file";
            }
            DownloadManager.Request destinationInExternalPublicDir = notificationVisibility.setDestinationInExternalPublicDir(str4, str3);
            destinationInExternalPublicDir.allowScanningByMediaScanner();
            j = downloadManager2.enqueue(destinationInExternalPublicDir);
        } else {
            j = 1;
        }
        if (j == 1) {
            function12.invoke(new DownloadManagerNotFound());
        } else if (j == 0) {
            function12.invoke(new DownloadManagerEnqueueFailure());
        } else {
            onDownloadListeners.put(Long.valueOf(j), new NetworkUtils$downloadFile$1(j, downloadManager2, function1, function12));
        }
    }

    public static /* synthetic */ boolean isDeviceConnected$default(NetworkUtils networkUtils, Context context, Intent intent, Logger logger, int i, Object obj) {
        if ((i & 2) != 0) {
            intent = null;
        }
        if ((i & 4) != 0) {
            logger = null;
        }
        return networkUtils.isDeviceConnected(context, intent, logger);
    }

    public final Integer getNetworkType(Context context) {
        m.checkNotNullParameter(context, "context");
        try {
            Object systemService = context.getSystemService("connectivity");
            if (systemService != null) {
                NetworkInfo activeNetworkInfo = ((ConnectivityManager) systemService).getActiveNetworkInfo();
                if (activeNetworkInfo != null) {
                    return Integer.valueOf(activeNetworkInfo.getType());
                }
                return null;
            }
            throw new NullPointerException("null cannot be cast to non-null type android.net.ConnectivityManager");
        } catch (Exception unused) {
            return null;
        }
    }

    public final boolean isAirplaneModeOn(Context context) {
        m.checkNotNullParameter(context, "context");
        return Settings.Global.getInt(context.getContentResolver(), "airplane_mode_on", 0) != 0;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v3 */
    /* JADX WARN: Type inference failed for: r8v4, types: [java.lang.Object[]] */
    public final boolean isDeviceConnected(Context context, Intent intent, Logger logger) {
        m.checkNotNullParameter(context, "context");
        try {
            Object systemService = context.getSystemService("connectivity");
            if (systemService != null) {
                ConnectivityManager connectivityManager = (ConnectivityManager) systemService;
                NetworkInfo networkInfo = null;
                if (Build.VERSION.SDK_INT >= 23) {
                    Network[] allNetworks = connectivityManager.getAllNetworks();
                    m.checkNotNullExpressionValue(allNetworks, "connectivityManager.allNetworks");
                    int length = allNetworks.length;
                    int i = 0;
                    while (true) {
                        if (i >= length) {
                            break;
                        }
                        Network network = allNetworks[i];
                        NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(network);
                        if ((networkCapabilities != null && networkCapabilities.hasCapability(12)) || (networkCapabilities != null && networkCapabilities.hasTransport(1)) || ((networkCapabilities != null && networkCapabilities.hasTransport(0)) || ((networkCapabilities != null && networkCapabilities.hasTransport(3)) || (networkCapabilities != null && networkCapabilities.hasTransport(4))))) {
                            networkInfo = network;
                            break;
                        }
                        i++;
                    }
                    if (networkInfo != null) {
                        return true;
                    }
                } else {
                    NetworkInfo[] networkInfoArr = {connectivityManager.getActiveNetworkInfo(), intent != null ? (NetworkInfo) intent.getParcelableExtra("networkInfo") : null};
                    Network[] allNetworks2 = connectivityManager.getAllNetworks();
                    m.checkNotNullExpressionValue(allNetworks2, "connectivityManager.allNetworks");
                    ArrayList arrayList = new ArrayList(allNetworks2.length);
                    for (Network network2 : allNetworks2) {
                        arrayList.add(connectivityManager.getNetworkInfo(network2));
                    }
                    ?? plus = j.plus(networkInfoArr, arrayList);
                    int length2 = plus.length;
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length2) {
                            break;
                        }
                        ?? r1 = plus[i2];
                        NetworkInfo networkInfo2 = (NetworkInfo) r1;
                        if (networkInfo2 != null && networkInfo2.isConnectedOrConnecting()) {
                            networkInfo = r1;
                            break;
                        }
                        i2++;
                    }
                    if (networkInfo != null) {
                        return true;
                    }
                }
                return false;
            }
            throw new NullPointerException("null cannot be cast to non-null type android.net.ConnectivityManager");
        } catch (Exception unused) {
            return true;
        }
    }
}
