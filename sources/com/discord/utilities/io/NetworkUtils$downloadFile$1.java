package com.discord.utilities.io;

import android.app.DownloadManager;
import android.database.Cursor;
import com.discord.utilities.io.NetworkUtils;
import d0.y.b;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: NetworkUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class NetworkUtils$downloadFile$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $downloadId;
    public final /* synthetic */ DownloadManager $manager;
    public final /* synthetic */ Function1 $onError;
    public final /* synthetic */ Function1 $onSuccess;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public NetworkUtils$downloadFile$1(long j, DownloadManager downloadManager, Function1 function1, Function1 function12) {
        super(0);
        this.$downloadId = j;
        this.$manager = downloadManager;
        this.$onSuccess = function1;
        this.$onError = function12;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        Cursor query;
        NetworkUtils networkUtils = NetworkUtils.INSTANCE;
        NetworkUtils.onDownloadListeners.remove(Long.valueOf(this.$downloadId));
        DownloadManager downloadManager = this.$manager;
        if (downloadManager != null && (query = downloadManager.query(new DownloadManager.Query().setFilterById(this.$downloadId))) != null) {
            th = null;
            try {
                if (query.moveToFirst()) {
                    int columnIndex = query.getColumnIndex("title");
                    String string = columnIndex > 0 ? query.getString(columnIndex) : "";
                    Function1 function1 = this.$onSuccess;
                    m.checkNotNullExpressionValue(string, "downloadedFileName");
                    function1.invoke(string);
                } else {
                    this.$onError.invoke(new NetworkUtils.DownloadManagerFileNoFound());
                }
            } catch (Throwable th) {
                try {
                    throw th;
                } finally {
                    b.closeFinally(query, th);
                }
            }
        }
    }
}
