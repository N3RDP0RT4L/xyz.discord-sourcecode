package com.discord.utilities.guilds;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.annotation.ColorInt;
import androidx.core.view.ViewCompat;
import com.discord.api.role.GuildRole;
import com.discord.api.role.GuildRoleTags;
import com.discord.models.member.GuildMember;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.time.TimeUtils;
import d0.g0.t;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: RoleUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u001e\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b2\u0010-J'\u0010\u0007\u001a\u00020\u0005*\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00032\n\b\u0003\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0007¢\u0006\u0004\b\u0007\u0010\bJ#\u0010\f\u001a\u00020\u000b2\b\u0010\t\u001a\u0004\u0018\u00010\u00022\b\u0010\n\u001a\u0004\u0018\u00010\u0002H\u0007¢\u0006\u0004\b\f\u0010\rJ#\u0010\u000e\u001a\u00020\u000b2\b\u0010\t\u001a\u0004\u0018\u00010\u00022\b\u0010\n\u001a\u0004\u0018\u00010\u0002H\u0007¢\u0006\u0004\b\u000e\u0010\rJ/\u0010\u0014\u001a\u0004\u0018\u00010\u00022\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00020\u000f2\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012H\u0007¢\u0006\u0004\b\u0014\u0010\u0015J/\u0010\u0016\u001a\u0004\u0018\u00010\u00022\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00020\u000f2\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012H\u0007¢\u0006\u0004\b\u0016\u0010\u0015J5\u0010\u0014\u001a\u0004\u0018\u00010\u00022\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00020\u000f2\u000e\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u0017H\u0007¢\u0006\u0004\b\u0014\u0010\u0019J\u0019\u0010\u001c\u001a\u00020\u000b2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001aH\u0007¢\u0006\u0004\b\u001c\u0010\u001dJ#\u0010\u001f\u001a\u00020\u00052\b\u0010\u001e\u001a\u0004\u0018\u00010\u00022\b\b\u0001\u0010\u0006\u001a\u00020\u0005H\u0007¢\u0006\u0004\b\u001f\u0010 J\u0019\u0010\u001f\u001a\u00020\u00052\b\u0010\u001e\u001a\u0004\u0018\u00010\u0002H\u0007¢\u0006\u0004\b\u001f\u0010!J\u0013\u0010\"\u001a\u00020\u000b*\u00020\u0002H\u0007¢\u0006\u0004\b\"\u0010#J\u0013\u0010$\u001a\u00020\u000b*\u00020\u0002H\u0007¢\u0006\u0004\b$\u0010#J\u0013\u0010%\u001a\u00020\u000b*\u00020\u0002H\u0007¢\u0006\u0004\b%\u0010#R2\u0010(\u001a\u0012\u0012\u0004\u0012\u00020\u00020&j\b\u0012\u0004\u0012\u00020\u0002`'8\u0006@\u0007X\u0087\u0004¢\u0006\u0012\n\u0004\b(\u0010)\u0012\u0004\b,\u0010-\u001a\u0004\b*\u0010+R\u001e\u00100\u001a\n /*\u0004\u0018\u00010.0.8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b0\u00101¨\u00063"}, d2 = {"Lcom/discord/utilities/guilds/RoleUtils;", "", "Lcom/discord/api/role/GuildRole;", "Landroid/content/Context;", "context", "", "defaultColor", "getRoleColor", "(Lcom/discord/api/role/GuildRole;Landroid/content/Context;Ljava/lang/Integer;)I", "role1", "role2", "", "rankIsHigher", "(Lcom/discord/api/role/GuildRole;Lcom/discord/api/role/GuildRole;)Z", "rankEquals", "", "", "guildRoles", "Lcom/discord/models/member/GuildMember;", "member", "getHighestRole", "(Ljava/util/Map;Lcom/discord/models/member/GuildMember;)Lcom/discord/api/role/GuildRole;", "getHighestHoistedRole", "", "roleIds", "(Ljava/util/Map;Ljava/util/Collection;)Lcom/discord/api/role/GuildRole;", "", "input", "containsRoleMentions", "(Ljava/lang/String;)Z", "role", "getOpaqueColor", "(Lcom/discord/api/role/GuildRole;I)I", "(Lcom/discord/api/role/GuildRole;)I", "isDefaultColor", "(Lcom/discord/api/role/GuildRole;)Z", "hasSubscriptionListingId", "isSubscriptionRolePurchasableOrHasSubscribers", "Ljava/util/Comparator;", "Lkotlin/Comparator;", "ROLE_COMPARATOR", "Ljava/util/Comparator;", "getROLE_COMPARATOR", "()Ljava/util/Comparator;", "getROLE_COMPARATOR$annotations", "()V", "Ljava/util/regex/Pattern;", "kotlin.jvm.PlatformType", "ROLE_MENTION_RE", "Ljava/util/regex/Pattern;", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class RoleUtils {
    public static final RoleUtils INSTANCE = new RoleUtils();
    private static final Pattern ROLE_MENTION_RE = Pattern.compile("<@&(\\d+)>");
    private static final Comparator<GuildRole> ROLE_COMPARATOR = new Comparator<GuildRole>() { // from class: com.discord.utilities.guilds.RoleUtils$ROLE_COMPARATOR$1
        public int compare(GuildRole guildRole, GuildRole guildRole2) {
            if (guildRole != null && guildRole2 != null) {
                int i = guildRole.i();
                int i2 = guildRole2.i();
                return i2 != i ? i2 - i : (TimeUtils.parseSnowflake(Long.valueOf(guildRole.getId())) > TimeUtils.parseSnowflake(Long.valueOf(guildRole2.getId())) ? 1 : (TimeUtils.parseSnowflake(Long.valueOf(guildRole.getId())) == TimeUtils.parseSnowflake(Long.valueOf(guildRole2.getId())) ? 0 : -1));
            } else if (guildRole == null || guildRole2 != null) {
                return (guildRole != null || guildRole2 == null) ? 0 : 1;
            } else {
                return -1;
            }
        }
    };

    private RoleUtils() {
    }

    public static final boolean containsRoleMentions(String str) {
        return str != null && ROLE_MENTION_RE.matcher(str).find();
    }

    public static final GuildRole getHighestHoistedRole(Map<Long, GuildRole> map, GuildMember guildMember) {
        m.checkNotNullParameter(map, "guildRoles");
        if (guildMember == null) {
            return null;
        }
        List<Long> roles = guildMember.getRoles();
        ArrayList arrayList = new ArrayList();
        for (Object obj : roles) {
            GuildRole guildRole = map.get(Long.valueOf(((Number) obj).longValue()));
            boolean z2 = true;
            if (guildRole == null || !guildRole.c()) {
                z2 = false;
            }
            if (z2) {
                arrayList.add(obj);
            }
        }
        return getHighestRole(map, arrayList);
    }

    public static final GuildRole getHighestRole(Map<Long, GuildRole> map, GuildMember guildMember) {
        m.checkNotNullParameter(map, "guildRoles");
        if (guildMember == null) {
            return null;
        }
        return getHighestRole(map, guildMember.getRoles());
    }

    @ColorInt
    public static final int getOpaqueColor(GuildRole guildRole, @ColorInt int i) {
        if (guildRole != null) {
            try {
                if (guildRole.b() != 0) {
                    i = guildRole.b();
                }
            } catch (Exception unused) {
                return ViewCompat.MEASURED_STATE_MASK;
            }
        }
        return ViewCompat.MEASURED_STATE_MASK + i;
    }

    public static final Comparator<GuildRole> getROLE_COMPARATOR() {
        return ROLE_COMPARATOR;
    }

    public static /* synthetic */ void getROLE_COMPARATOR$annotations() {
    }

    @ColorInt
    public static final int getRoleColor(GuildRole guildRole, Context context) {
        return getRoleColor$default(guildRole, context, null, 2, null);
    }

    @ColorInt
    public static final int getRoleColor(GuildRole guildRole, Context context, @ColorInt Integer num) {
        m.checkNotNullParameter(guildRole, "$this$getRoleColor");
        m.checkNotNullParameter(context, "context");
        if (isDefaultColor(guildRole)) {
            return num != null ? num.intValue() : ColorCompat.getColor(context, (int) R.color.primary_300);
        }
        return getOpaqueColor(guildRole);
    }

    public static /* synthetic */ int getRoleColor$default(GuildRole guildRole, Context context, Integer num, int i, Object obj) {
        if ((i & 2) != 0) {
            num = null;
        }
        return getRoleColor(guildRole, context, num);
    }

    public static final boolean hasSubscriptionListingId(GuildRole guildRole) {
        m.checkNotNullParameter(guildRole, "$this$hasSubscriptionListingId");
        GuildRoleTags j = guildRole.j();
        String a = j != null ? j.a() : null;
        return !(a == null || t.isBlank(a));
    }

    public static final boolean isDefaultColor(GuildRole guildRole) {
        m.checkNotNullParameter(guildRole, "$this$isDefaultColor");
        return guildRole.b() == 0;
    }

    public static final boolean isSubscriptionRolePurchasableOrHasSubscribers(GuildRole guildRole) {
        GuildRoleTags j;
        m.checkNotNullParameter(guildRole, "$this$isSubscriptionRolePurchasableOrHasSubscribers");
        return hasSubscriptionListingId(guildRole) && (j = guildRole.j()) != null && j.b();
    }

    public static final boolean rankEquals(GuildRole guildRole, GuildRole guildRole2) {
        return ROLE_COMPARATOR.compare(guildRole, guildRole2) == 0;
    }

    public static final boolean rankIsHigher(GuildRole guildRole, GuildRole guildRole2) {
        return ROLE_COMPARATOR.compare(guildRole, guildRole2) < 0;
    }

    public static final GuildRole getHighestRole(Map<Long, GuildRole> map, Collection<Long> collection) {
        m.checkNotNullParameter(map, "guildRoles");
        GuildRole guildRole = null;
        if (collection == null) {
            return null;
        }
        for (Long l : collection) {
            GuildRole guildRole2 = map.get(Long.valueOf(l.longValue()));
            if (guildRole2 != null && rankIsHigher(guildRole2, guildRole)) {
                guildRole = guildRole2;
            }
        }
        return guildRole;
    }

    @ColorInt
    public static final int getOpaqueColor(GuildRole guildRole) {
        return getOpaqueColor(guildRole, 0);
    }
}
