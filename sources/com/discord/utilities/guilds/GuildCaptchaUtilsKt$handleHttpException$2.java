package com.discord.utilities.guilds;

import android.content.Context;
import com.discord.app.AppFragment;
import com.discord.widgets.guilds.join.GuildJoinHelperKt;
import com.discord.widgets.guilds.join.JoinArgs;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: GuildCaptchaUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/app/AppFragment;", "<anonymous parameter 0>", "", "captchaToken", "", "invoke", "(Lcom/discord/app/AppFragment;Ljava/lang/String;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildCaptchaUtilsKt$handleHttpException$2 extends o implements Function2<AppFragment, String, Unit> {
    public final /* synthetic */ JoinArgs $args;
    public final /* synthetic */ Context $context;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildCaptchaUtilsKt$handleHttpException$2(Context context, JoinArgs joinArgs) {
        super(2);
        this.$context = context;
        this.$args = joinArgs;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(AppFragment appFragment, String str) {
        invoke2(appFragment, str);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(AppFragment appFragment, String str) {
        m.checkNotNullParameter(appFragment, "<anonymous parameter 0>");
        m.checkNotNullParameter(str, "captchaToken");
        Context context = this.$context;
        long guildId = this.$args.getGuildId();
        boolean isLurker = this.$args.isLurker();
        String sessionId = this.$args.getSessionId();
        Long directoryChannelId = this.$args.getDirectoryChannelId();
        GuildJoinHelperKt.joinGuild(context, guildId, isLurker, (r27 & 8) != 0 ? null : sessionId, (r27 & 16) != 0 ? null : directoryChannelId, (r27 & 32) != 0 ? null : null, this.$args.getErrorClass(), (r27 & 128) != 0 ? null : this.$args.getSubscriptionHandler(), (r27 & 256) != 0 ? null : this.$args.getErrorHandler(), (r27 & 512) != 0 ? null : str, this.$args.getOnNext());
    }
}
