package com.discord.utilities.guilds;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.fragment.app.FragmentManager;
import com.discord.api.guild.GuildFeature;
import com.discord.api.guildjoinrequest.ApplicationStatus;
import com.discord.api.guildjoinrequest.GuildJoinRequest;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelInvite;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreStream;
import com.discord.widgets.servers.member_verification.MemberVerificationPendingDialog;
import com.discord.widgets.servers.member_verification.MemberVerificationSuccessDialog;
import com.discord.widgets.servers.member_verification.WidgetMemberVerification;
import d0.j;
import d0.t.n0;
import d0.z.d.m;
import java.util.Collection;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: MemberVerificationUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001e\u0010\u001fJ#\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u0005H\u0002¢\u0006\u0004\b\b\u0010\tJ%\u0010\u000e\u001a\u00020\u00072\b\u0010\u000b\u001a\u0004\u0018\u00010\n2\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\fH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ7\u0010\u0014\u001a\u00020\u00072\u0006\u0010\u0011\u001a\u00020\u00102\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010\u0013\u001a\u00020\u00122\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\fH\u0002¢\u0006\u0004\b\u0014\u0010\u0015J[\u0010\u0019\u001a\u00020\u00072\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010\u0013\u001a\u00020\u00122\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\f2\u000e\b\u0002\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00070\u00162\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00070\u0016¢\u0006\u0004\b\u0019\u0010\u001aJ%\u0010\u001c\u001a\u00020\u001b2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\n2\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\f¢\u0006\u0004\b\u001c\u0010\u001d¨\u0006 "}, d2 = {"Lcom/discord/utilities/guilds/MemberVerificationUtils;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "showMemberVerificationPendingDialog", "(Landroidx/fragment/app/FragmentManager;J)V", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/models/domain/ModelInvite;", "invite", "showMemberVerificationSuccessDialog", "(Lcom/discord/models/guild/Guild;Lcom/discord/models/domain/ModelInvite;)V", "Landroid/content/Context;", "context", "", ModelAuditLogEntry.CHANGE_KEY_LOCATION, "showMemberVerificationWidget", "(Landroid/content/Context;JLjava/lang/String;Lcom/discord/models/domain/ModelInvite;)V", "Lkotlin/Function0;", "onMembershipGated", "onFullMembership", "maybeShowVerificationGate", "(Landroid/content/Context;Landroidx/fragment/app/FragmentManager;JLjava/lang/String;Lcom/discord/models/domain/ModelInvite;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V", "", "hasVerificationGate", "(Lcom/discord/models/guild/Guild;Lcom/discord/models/domain/ModelInvite;)Z", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MemberVerificationUtils {
    public static final MemberVerificationUtils INSTANCE = new MemberVerificationUtils();

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            ApplicationStatus.values();
            int[] iArr = new int[5];
            $EnumSwitchMapping$0 = iArr;
            iArr[ApplicationStatus.APPROVED.ordinal()] = 1;
            iArr[ApplicationStatus.PENDING.ordinal()] = 2;
            iArr[ApplicationStatus.REJECTED.ordinal()] = 3;
            iArr[ApplicationStatus.STARTED.ordinal()] = 4;
            iArr[ApplicationStatus.UNKNOWN.ordinal()] = 5;
        }
    }

    private MemberVerificationUtils() {
    }

    public static /* synthetic */ boolean hasVerificationGate$default(MemberVerificationUtils memberVerificationUtils, Guild guild, ModelInvite modelInvite, int i, Object obj) {
        if ((i & 1) != 0) {
            guild = null;
        }
        if ((i & 2) != 0) {
            modelInvite = null;
        }
        return memberVerificationUtils.hasVerificationGate(guild, modelInvite);
    }

    public static /* synthetic */ void maybeShowVerificationGate$default(MemberVerificationUtils memberVerificationUtils, Context context, FragmentManager fragmentManager, long j, String str, ModelInvite modelInvite, Function0 function0, Function0 function02, int i, Object obj) {
        memberVerificationUtils.maybeShowVerificationGate(context, fragmentManager, j, str, (i & 16) != 0 ? null : modelInvite, (i & 32) != 0 ? MemberVerificationUtils$maybeShowVerificationGate$1.INSTANCE : function0, function02);
    }

    private final void showMemberVerificationPendingDialog(FragmentManager fragmentManager, long j) {
        MemberVerificationPendingDialog.Companion.show$default(MemberVerificationPendingDialog.Companion, fragmentManager, j, null, 4, null);
    }

    private final void showMemberVerificationSuccessDialog(Guild guild, ModelInvite modelInvite) {
        String str;
        com.discord.api.guild.Guild guild2;
        if (guild == null || (str = guild.getName()) == null) {
            str = (modelInvite == null || (guild2 = modelInvite.guild) == null) ? null : guild2.x();
        }
        if (str != null && guild != null) {
            MemberVerificationSuccessDialog.Companion.enqueue(guild.getId());
        }
    }

    public static /* synthetic */ void showMemberVerificationSuccessDialog$default(MemberVerificationUtils memberVerificationUtils, Guild guild, ModelInvite modelInvite, int i, Object obj) {
        if ((i & 2) != 0) {
            modelInvite = null;
        }
        memberVerificationUtils.showMemberVerificationSuccessDialog(guild, modelInvite);
    }

    private final void showMemberVerificationWidget(Context context, long j, String str, ModelInvite modelInvite) {
        WidgetMemberVerification.Companion.create(context, j, str, modelInvite);
    }

    public static /* synthetic */ void showMemberVerificationWidget$default(MemberVerificationUtils memberVerificationUtils, Context context, long j, String str, ModelInvite modelInvite, int i, Object obj) {
        if ((i & 8) != 0) {
            modelInvite = null;
        }
        memberVerificationUtils.showMemberVerificationWidget(context, j, str, modelInvite);
    }

    public final boolean hasVerificationGate(Guild guild, ModelInvite modelInvite) {
        Collection collection;
        com.discord.api.guild.Guild guild2;
        if (guild == null || (collection = guild.getFeatures()) == null) {
            collection = (modelInvite == null || (guild2 = modelInvite.guild) == null) ? null : guild2.m();
        }
        if (collection == null) {
            collection = n0.emptySet();
        }
        return collection.contains(GuildFeature.MEMBER_VERIFICATION_GATE_ENABLED) && collection.contains(GuildFeature.COMMUNITY);
    }

    public final void maybeShowVerificationGate(Context context, FragmentManager fragmentManager, long j, String str, ModelInvite modelInvite, Function0<Unit> function0, Function0<Unit> function02) {
        int ordinal;
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        m.checkNotNullParameter(function0, "onMembershipGated");
        m.checkNotNullParameter(function02, "onFullMembership");
        StoreStream.Companion companion = StoreStream.Companion;
        StoreGuilds guilds = companion.getGuilds();
        GuildMember member = guilds.getMember(j, companion.getUsers().getMe().getId());
        if (member == null || member.getPending()) {
            Guild guild = guilds.getGuild(j);
            if (!hasVerificationGate(guild, modelInvite)) {
                function02.invoke();
                return;
            }
            GuildJoinRequest guildJoinRequest = companion.getGuildJoinRequests().getGuildJoinRequest(j);
            ApplicationStatus a = guildJoinRequest != null ? guildJoinRequest.a() : null;
            if (a == null || (ordinal = a.ordinal()) == 0) {
                function0.invoke();
                showMemberVerificationWidget(context, j, str, modelInvite);
            } else if (ordinal == 1) {
                function0.invoke();
                showMemberVerificationPendingDialog(fragmentManager, j);
            } else if (ordinal == 2) {
                function0.invoke();
                showMemberVerificationPendingDialog(fragmentManager, j);
            } else if (ordinal == 3) {
                function02.invoke();
                showMemberVerificationSuccessDialog(guild, modelInvite);
            } else if (ordinal == 4) {
                throw new j(null, 1, null);
            }
        } else {
            function02.invoke();
        }
    }
}
