package com.discord.utilities.guilds;

import andhook.lib.HookHelper;
import com.discord.api.guild.GuildVerificationLevel;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.guildmember.GuildMemberUtilsKt;
import com.discord.utilities.rx.LeadingEdgeThrottle;
import com.discord.utilities.time.Clock;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.user.UserUtils;
import d0.z.d.m;
import j0.l.a.r;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import rx.Observable;
import rx.functions.Func5;
/* compiled from: GuildVerificationLevelUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001c\u0010\u001dJG\u0010\u000f\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\b\u0010\u0007\u001a\u0004\u0018\u00010\u00062\u0006\u0010\t\u001a\u00020\b2\n\u0010\f\u001a\u00060\nj\u0002`\u000b2\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J7\u0010\u0017\u001a\u00020\b2\n\u0010\u0012\u001a\u00060\nj\u0002`\u00112\b\b\u0002\u0010\u0014\u001a\u00020\u00132\b\b\u0002\u0010\u0016\u001a\u00020\u00152\b\b\u0002\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b\u0017\u0010\u0018J=\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\b0\u00192\n\u0010\u0012\u001a\u00060\nj\u0002`\u00112\b\b\u0002\u0010\u0014\u001a\u00020\u00132\b\b\u0002\u0010\u0016\u001a\u00020\u00152\b\b\u0002\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b\u001a\u0010\u001b¨\u0006\u001e"}, d2 = {"Lcom/discord/utilities/guilds/GuildVerificationLevelUtils;", "", "Lcom/discord/models/user/MeUser;", "me", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/models/member/GuildMember;", "member", "Lcom/discord/api/guild/GuildVerificationLevel;", "verificationLevel", "", "Lcom/discord/primitives/Timestamp;", "joinedAt", "Lcom/discord/utilities/time/Clock;", "clock", "computeVerificationLevelTriggered", "(Lcom/discord/models/user/MeUser;Lcom/discord/models/guild/Guild;Lcom/discord/models/member/GuildMember;Lcom/discord/api/guild/GuildVerificationLevel;JLcom/discord/utilities/time/Clock;)Lcom/discord/api/guild/GuildVerificationLevel;", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/stores/StoreGuilds;", "guildStore", "Lcom/discord/stores/StoreUser;", "userStore", "getVerificationLevelTriggered", "(JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUser;Lcom/discord/utilities/time/Clock;)Lcom/discord/api/guild/GuildVerificationLevel;", "Lrx/Observable;", "observeVerificationLevelTriggered", "(JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUser;Lcom/discord/utilities/time/Clock;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildVerificationLevelUtils {
    public static final GuildVerificationLevelUtils INSTANCE = new GuildVerificationLevelUtils();

    private GuildVerificationLevelUtils() {
    }

    public final GuildVerificationLevel computeVerificationLevelTriggered(MeUser meUser, Guild guild, GuildMember guildMember, GuildVerificationLevel guildVerificationLevel, long j, Clock clock) {
        List<Long> roles;
        boolean z2 = false;
        boolean z3 = guild != null && guild.isOwner(meUser.getId());
        if (!(guildMember == null || (roles = guildMember.getRoles()) == null || !(!roles.isEmpty()))) {
            z2 = true;
        }
        UserUtils userUtils = UserUtils.INSTANCE;
        boolean hasPhone = userUtils.getHasPhone(meUser);
        boolean isStaff = userUtils.isStaff(meUser);
        if (z3 || z2 || hasPhone || isStaff) {
            return GuildVerificationLevel.NONE;
        }
        GuildVerificationLevel guildVerificationLevel2 = GuildVerificationLevel.HIGHEST;
        if (guildVerificationLevel == guildVerificationLevel2) {
            return guildVerificationLevel2;
        }
        GuildVerificationLevel guildVerificationLevel3 = GuildVerificationLevel.HIGH;
        if (guildVerificationLevel == guildVerificationLevel3 && !GuildMemberUtilsKt.isGuildMemberOldEnough(j)) {
            return guildVerificationLevel3;
        }
        GuildVerificationLevel guildVerificationLevel4 = GuildVerificationLevel.MEDIUM;
        if (guildVerificationLevel == guildVerificationLevel4 && !userUtils.isAccountOldEnough(meUser, clock)) {
            return guildVerificationLevel4;
        }
        GuildVerificationLevel guildVerificationLevel5 = GuildVerificationLevel.LOW;
        return (guildVerificationLevel != guildVerificationLevel5 || meUser.isVerified()) ? GuildVerificationLevel.NONE : guildVerificationLevel5;
    }

    public static /* synthetic */ GuildVerificationLevel getVerificationLevelTriggered$default(GuildVerificationLevelUtils guildVerificationLevelUtils, long j, StoreGuilds storeGuilds, StoreUser storeUser, Clock clock, int i, Object obj) {
        if ((i & 2) != 0) {
            storeGuilds = StoreStream.Companion.getGuilds();
        }
        StoreGuilds storeGuilds2 = storeGuilds;
        if ((i & 4) != 0) {
            storeUser = StoreStream.Companion.getUsers();
        }
        StoreUser storeUser2 = storeUser;
        if ((i & 8) != 0) {
            clock = ClockFactory.get();
        }
        return guildVerificationLevelUtils.getVerificationLevelTriggered(j, storeGuilds2, storeUser2, clock);
    }

    public static /* synthetic */ Observable observeVerificationLevelTriggered$default(GuildVerificationLevelUtils guildVerificationLevelUtils, long j, StoreGuilds storeGuilds, StoreUser storeUser, Clock clock, int i, Object obj) {
        if ((i & 2) != 0) {
            storeGuilds = StoreStream.Companion.getGuilds();
        }
        StoreGuilds storeGuilds2 = storeGuilds;
        if ((i & 4) != 0) {
            storeUser = StoreStream.Companion.getUsers();
        }
        StoreUser storeUser2 = storeUser;
        if ((i & 8) != 0) {
            clock = ClockFactory.get();
        }
        return guildVerificationLevelUtils.observeVerificationLevelTriggered(j, storeGuilds2, storeUser2, clock);
    }

    public final GuildVerificationLevel getVerificationLevelTriggered(long j, StoreGuilds storeGuilds, StoreUser storeUser, Clock clock) {
        GuildVerificationLevel guildVerificationLevel;
        m.checkNotNullParameter(storeGuilds, "guildStore");
        m.checkNotNullParameter(storeUser, "userStore");
        m.checkNotNullParameter(clock, "clock");
        Guild guild = storeGuilds.getGuild(j);
        Map<Long, GuildMember> map = storeGuilds.getMembers().get(Long.valueOf(j));
        MeUser me2 = storeUser.getMe();
        GuildMember guildMember = map != null ? map.get(Long.valueOf(me2.getId())) : null;
        if (guild == null || (guildVerificationLevel = guild.getVerificationLevel()) == null) {
            guildVerificationLevel = GuildVerificationLevel.NONE;
        }
        return computeVerificationLevelTriggered(me2, guild, guildMember, guildVerificationLevel, GuildMemberUtilsKt.getJoinedAtOrNow(storeGuilds.getGuildsJoinedAt().get(Long.valueOf(j))), clock);
    }

    public final Observable<GuildVerificationLevel> observeVerificationLevelTriggered(long j, StoreGuilds storeGuilds, StoreUser storeUser, final Clock clock) {
        m.checkNotNullParameter(storeGuilds, "guildStore");
        m.checkNotNullParameter(storeUser, "userStore");
        m.checkNotNullParameter(clock, "clock");
        Observable<Long> observeJoinedAt = storeGuilds.observeJoinedAt(j);
        Observable<Guild> observeGuild = storeGuilds.observeGuild(j);
        Observable<GuildVerificationLevel> observeVerificationLevel = storeGuilds.observeVerificationLevel(j);
        Observable<Map<Long, GuildMember>> observeComputed = storeGuilds.observeComputed(j);
        Observable<GuildVerificationLevel> q = Observable.g(observeJoinedAt, observeGuild, observeVerificationLevel, Observable.h0(new r(observeComputed.j, new LeadingEdgeThrottle(1500L, TimeUnit.MILLISECONDS))), storeUser.observeMe(true), new Func5<Long, Guild, GuildVerificationLevel, Map<Long, ? extends GuildMember>, MeUser, GuildVerificationLevel>() { // from class: com.discord.utilities.guilds.GuildVerificationLevelUtils$observeVerificationLevelTriggered$1
            @Override // rx.functions.Func5
            public /* bridge */ /* synthetic */ GuildVerificationLevel call(Long l, Guild guild, GuildVerificationLevel guildVerificationLevel, Map<Long, ? extends GuildMember> map, MeUser meUser) {
                return call2(l, guild, guildVerificationLevel, (Map<Long, GuildMember>) map, meUser);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final GuildVerificationLevel call2(Long l, Guild guild, GuildVerificationLevel guildVerificationLevel, Map<Long, GuildMember> map, MeUser meUser) {
                GuildVerificationLevel computeVerificationLevelTriggered;
                GuildVerificationLevelUtils guildVerificationLevelUtils = GuildVerificationLevelUtils.INSTANCE;
                m.checkNotNullExpressionValue(meUser, "me");
                m.checkNotNullExpressionValue(map, "members");
                m.checkNotNullExpressionValue(guildVerificationLevel, "verificationLevel");
                m.checkNotNullExpressionValue(l, "joinedAt");
                computeVerificationLevelTriggered = guildVerificationLevelUtils.computeVerificationLevelTriggered(meUser, guild, map.get(Long.valueOf(meUser.getId())), guildVerificationLevel, l.longValue(), Clock.this);
                return computeVerificationLevelTriggered;
            }
        }).q();
        m.checkNotNullExpressionValue(q, "Observable\n        .comb…  .distinctUntilChanged()");
        return q;
    }
}
