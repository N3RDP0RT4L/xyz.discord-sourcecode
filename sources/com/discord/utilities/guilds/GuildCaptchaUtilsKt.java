package com.discord.utilities.guilds;

import android.content.Context;
import androidx.fragment.app.Fragment;
import com.discord.utilities.captcha.CaptchaErrorBody;
import com.discord.utilities.captcha.CaptchaHelper;
import com.discord.utilities.error.Error;
import com.discord.widgets.guilds.join.InviteArgs;
import com.discord.widgets.guilds.join.JoinArgs;
import com.discord.widgets.guilds.join.WidgetGuildJoinCaptchaBottomSheet;
import d0.g0.w;
import d0.t.u;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import retrofit2.HttpException;
/* compiled from: GuildCaptchaUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\u001a%\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\b\u001a%\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u0005\u001a\u00020\u000b¢\u0006\u0004\b\u0007\u0010\f\"\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000e\u0010\u000f¨\u0006\u0010"}, d2 = {"Lcom/discord/utilities/error/Error;", "error", "Landroidx/fragment/app/Fragment;", "fragment", "Lcom/discord/widgets/guilds/join/InviteArgs;", "args", "", "handleHttpException", "(Lcom/discord/utilities/error/Error;Landroidx/fragment/app/Fragment;Lcom/discord/widgets/guilds/join/InviteArgs;)V", "Landroid/content/Context;", "context", "Lcom/discord/widgets/guilds/join/JoinArgs;", "(Lcom/discord/utilities/error/Error;Landroid/content/Context;Lcom/discord/widgets/guilds/join/JoinArgs;)V", "", GuildCaptchaUtilsKt.REQUEST_KEY_GUILD_CAPTCHA, "Ljava/lang/String;", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildCaptchaUtilsKt {
    private static final String REQUEST_KEY_GUILD_CAPTCHA = "REQUEST_KEY_GUILD_CAPTCHA";

    public static final void handleHttpException(Error error, Fragment fragment, InviteArgs inviteArgs) {
        m.checkNotNullParameter(error, "error");
        m.checkNotNullParameter(fragment, "fragment");
        m.checkNotNullParameter(inviteArgs, "args");
        if (error.getThrowable() instanceof HttpException) {
            String bodyText = error.getBodyText();
            String str = null;
            if (m.areEqual(bodyText != null ? Boolean.valueOf(w.contains$default((CharSequence) bodyText, (CharSequence) CaptchaHelper.CAPTCHA_KEY, false, 2, (Object) null)) : null, Boolean.TRUE)) {
                WidgetGuildJoinCaptchaBottomSheet.Companion.enqueue(REQUEST_KEY_GUILD_CAPTCHA, new GuildCaptchaUtilsKt$handleHttpException$1(inviteArgs), CaptchaErrorBody.Companion.createFromError(error));
                return;
            }
            Error.Response response = error.getResponse();
            m.checkNotNullExpressionValue(response, "error.response");
            List list = (List) u.firstOrNull(response.getMessages().values());
            if (list != null) {
                str = (String) u.firstOrNull((List<? extends Object>) list);
            }
            if (str != null) {
                b.a.d.m.j(fragment, str, 0, 4);
            }
        }
    }

    public static final void handleHttpException(Error error, Context context, JoinArgs joinArgs) {
        String str;
        m.checkNotNullParameter(error, "error");
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(joinArgs, "args");
        if (error.getThrowable() instanceof HttpException) {
            String bodyText = error.getBodyText();
            if (m.areEqual(bodyText != null ? Boolean.valueOf(w.contains$default((CharSequence) bodyText, (CharSequence) CaptchaHelper.CAPTCHA_KEY, false, 2, (Object) null)) : null, Boolean.TRUE)) {
                WidgetGuildJoinCaptchaBottomSheet.Companion.enqueue(REQUEST_KEY_GUILD_CAPTCHA, new GuildCaptchaUtilsKt$handleHttpException$2(context, joinArgs), CaptchaErrorBody.Companion.createFromError(error));
                return;
            }
            Error.Response response = error.getResponse();
            m.checkNotNullExpressionValue(response, "error.response");
            List list = (List) u.firstOrNull(response.getMessages().values());
            if (list != null && (str = (String) u.firstOrNull((List<? extends Object>) list)) != null) {
                b.a.d.m.h(context, str, 0, null, 12);
            }
        }
    }
}
