package com.discord.utilities.permissions;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.permission.Permission;
import com.discord.api.role.GuildRole;
import com.discord.models.guild.Guild;
import com.discord.models.user.MeUser;
import com.discord.models.user.User;
import com.discord.utilities.guilds.RoleUtils;
import d0.z.d.m;
import java.util.Collection;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: PermissionsContexts.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0016\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0012\b\u0086\b\u0018\u0000 -2\u00020\u0001:\u0001-BO\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\u0006\u0010\u0010\u001a\u00020\u0002\u0012\u0006\u0010\u0011\u001a\u00020\u0002\u0012\u0006\u0010\u0012\u001a\u00020\u0002\u0012\u0006\u0010\u0013\u001a\u00020\u0002\u0012\u0006\u0010\u0014\u001a\u00020\u0002\u0012\u0006\u0010\u0015\u001a\u00020\u0002\u0012\u0006\u0010\u0016\u001a\u00020\u0002¢\u0006\u0004\b+\u0010,J\u0010\u0010\u0003\u001a\u00020\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\r\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0004J\u0010\u0010\b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\t\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\u0004J\u0010\u0010\n\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\n\u0010\u0004J\u0010\u0010\u000b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u000b\u0010\u0004J\u0010\u0010\f\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\f\u0010\u0004J\u0010\u0010\r\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\r\u0010\u0004Jj\u0010\u0017\u001a\u00020\u00002\b\b\u0002\u0010\u000e\u001a\u00020\u00022\b\b\u0002\u0010\u000f\u001a\u00020\u00022\b\b\u0002\u0010\u0010\u001a\u00020\u00022\b\b\u0002\u0010\u0011\u001a\u00020\u00022\b\b\u0002\u0010\u0012\u001a\u00020\u00022\b\b\u0002\u0010\u0013\u001a\u00020\u00022\b\b\u0002\u0010\u0014\u001a\u00020\u00022\b\b\u0002\u0010\u0015\u001a\u00020\u00022\b\b\u0002\u0010\u0016\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u001a\u001a\u00020\u0019HÖ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u001d\u001a\u00020\u001cHÖ\u0001¢\u0006\u0004\b\u001d\u0010\u001eJ\u001a\u0010 \u001a\u00020\u00022\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b \u0010!R\u0019\u0010\u0012\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\"\u001a\u0004\b#\u0010\u0004R\u0016\u0010\u0016\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\"R\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\"\u001a\u0004\b$\u0010\u0004R\u0019\u0010\u0015\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\"\u001a\u0004\b%\u0010\u0004R\u0019\u0010\u0011\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\"\u001a\u0004\b&\u0010\u0004R\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\"\u001a\u0004\b'\u0010\u0004R\u0019\u0010\u0014\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\"\u001a\u0004\b(\u0010\u0004R\u0019\u0010\u0013\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\"\u001a\u0004\b)\u0010\u0004R\u0019\u0010\u0010\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\"\u001a\u0004\b*\u0010\u0004¨\u0006."}, d2 = {"Lcom/discord/utilities/permissions/ManageUserContext;", "", "", "component9", "()Z", "canManage", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "canManageRoles", "canKick", "canBan", "canMute", "canDeafen", "canMove", "canChangeNickname", "canDisableCommunication", "canTransferOwnership", "copy", "(ZZZZZZZZZ)Lcom/discord/utilities/permissions/ManageUserContext;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getCanDeafen", "getCanKick", "getCanDisableCommunication", "getCanMute", "getCanManageRoles", "getCanChangeNickname", "getCanMove", "getCanBan", HookHelper.constructorName, "(ZZZZZZZZZ)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ManageUserContext {
    public static final Companion Companion = new Companion(null);
    private final boolean canBan;
    private final boolean canChangeNickname;
    private final boolean canDeafen;
    private final boolean canDisableCommunication;
    private final boolean canKick;
    private final boolean canManageRoles;
    private final boolean canMove;
    private final boolean canMute;
    private final boolean canTransferOwnership;

    /* compiled from: PermissionsContexts.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0014\u0010\u0015Jg\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\b2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u000e\u0010\r\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\f2\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u000f0\u000eH\u0007¢\u0006\u0004\b\u0012\u0010\u0013¨\u0006\u0016"}, d2 = {"Lcom/discord/utilities/permissions/ManageUserContext$Companion;", "", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/models/user/MeUser;", "actingUser", "Lcom/discord/models/user/User;", "targetUser", "", "", "actingUserRoles", "userRoles", "Lcom/discord/api/permission/PermissionBit;", "myPermissions", "", "Lcom/discord/api/role/GuildRole;", "guildRoles", "Lcom/discord/utilities/permissions/ManageUserContext;", "from", "(Lcom/discord/models/guild/Guild;Lcom/discord/models/user/MeUser;Lcom/discord/models/user/User;Ljava/util/Collection;Ljava/util/Collection;Ljava/lang/Long;Ljava/util/Map;)Lcom/discord/utilities/permissions/ManageUserContext;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final ManageUserContext from(Guild guild, MeUser meUser, User user, Collection<Long> collection, Collection<Long> collection2, Long l, Map<Long, GuildRole> map) {
            m.checkNotNullParameter(guild, "guild");
            m.checkNotNullParameter(meUser, "actingUser");
            m.checkNotNullParameter(user, "targetUser");
            m.checkNotNullParameter(collection, "actingUserRoles");
            m.checkNotNullParameter(collection2, "userRoles");
            m.checkNotNullParameter(map, "guildRoles");
            long id2 = user.getId();
            long id3 = meUser.getId();
            boolean z2 = id3 == id2;
            boolean z3 = id3 == guild.getOwnerId();
            boolean z4 = z3 || (!((id2 > guild.getOwnerId() ? 1 : (id2 == guild.getOwnerId() ? 0 : -1)) == 0) && RoleUtils.rankIsHigher(RoleUtils.getHighestRole(map, collection), RoleUtils.getHighestRole(map, collection2)));
            int mfaLevel = guild.getMfaLevel();
            boolean mfaEnabled = meUser.getMfaEnabled();
            boolean z5 = z3 || PermissionUtils.canAndIsElevated(8L, l, mfaEnabled, mfaLevel);
            boolean z6 = (z2 || z4) && (z5 || PermissionUtils.canAndIsElevated(Permission.MANAGE_ROLES, l, mfaEnabled, mfaLevel));
            boolean z7 = !z2 && z4 && (z5 || PermissionUtils.canAndIsElevated(2L, l, mfaEnabled, mfaLevel));
            boolean z8 = !z2 && z4 && (z5 || PermissionUtils.canAndIsElevated(4L, l, mfaEnabled, mfaLevel));
            boolean z9 = !z2 && z4 && (z5 || PermissionUtils.canAndIsElevated(Permission.MODERATE_MEMBERS, l, mfaEnabled, mfaLevel));
            return new ManageUserContext(z6, z7, z8, z5 || PermissionUtils.can(Permission.MUTE_MEMBERS, l), z5 || PermissionUtils.can(Permission.DEAFEN_MEMBERS, l), z5 || PermissionUtils.can(Permission.MOVE_MEMBERS, l), !z2 ? !(!z4 || (!z5 && !PermissionUtils.canAndIsElevated(Permission.MANAGE_NICKNAMES, l, mfaEnabled, mfaLevel))) : !(!z5 && !PermissionUtils.can(Permission.CHANGE_NICKNAME, l)), z9, !z2 && z3);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public ManageUserContext(boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9, boolean z10) {
        this.canManageRoles = z2;
        this.canKick = z3;
        this.canBan = z4;
        this.canMute = z5;
        this.canDeafen = z6;
        this.canMove = z7;
        this.canChangeNickname = z8;
        this.canDisableCommunication = z9;
        this.canTransferOwnership = z10;
    }

    private final boolean component9() {
        return this.canTransferOwnership;
    }

    public static final ManageUserContext from(Guild guild, MeUser meUser, User user, Collection<Long> collection, Collection<Long> collection2, Long l, Map<Long, GuildRole> map) {
        return Companion.from(guild, meUser, user, collection, collection2, l, map);
    }

    public final boolean canManage() {
        return this.canManageRoles || this.canKick || this.canBan || this.canMute || this.canDeafen || this.canMove || this.canChangeNickname || this.canTransferOwnership;
    }

    public final boolean component1() {
        return this.canManageRoles;
    }

    public final boolean component2() {
        return this.canKick;
    }

    public final boolean component3() {
        return this.canBan;
    }

    public final boolean component4() {
        return this.canMute;
    }

    public final boolean component5() {
        return this.canDeafen;
    }

    public final boolean component6() {
        return this.canMove;
    }

    public final boolean component7() {
        return this.canChangeNickname;
    }

    public final boolean component8() {
        return this.canDisableCommunication;
    }

    public final ManageUserContext copy(boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9, boolean z10) {
        return new ManageUserContext(z2, z3, z4, z5, z6, z7, z8, z9, z10);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ManageUserContext)) {
            return false;
        }
        ManageUserContext manageUserContext = (ManageUserContext) obj;
        return this.canManageRoles == manageUserContext.canManageRoles && this.canKick == manageUserContext.canKick && this.canBan == manageUserContext.canBan && this.canMute == manageUserContext.canMute && this.canDeafen == manageUserContext.canDeafen && this.canMove == manageUserContext.canMove && this.canChangeNickname == manageUserContext.canChangeNickname && this.canDisableCommunication == manageUserContext.canDisableCommunication && this.canTransferOwnership == manageUserContext.canTransferOwnership;
    }

    public final boolean getCanBan() {
        return this.canBan;
    }

    public final boolean getCanChangeNickname() {
        return this.canChangeNickname;
    }

    public final boolean getCanDeafen() {
        return this.canDeafen;
    }

    public final boolean getCanDisableCommunication() {
        return this.canDisableCommunication;
    }

    public final boolean getCanKick() {
        return this.canKick;
    }

    public final boolean getCanManageRoles() {
        return this.canManageRoles;
    }

    public final boolean getCanMove() {
        return this.canMove;
    }

    public final boolean getCanMute() {
        return this.canMute;
    }

    public int hashCode() {
        boolean z2 = this.canManageRoles;
        int i = 1;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        int i4 = i2 * 31;
        boolean z3 = this.canKick;
        if (z3) {
            z3 = true;
        }
        int i5 = z3 ? 1 : 0;
        int i6 = z3 ? 1 : 0;
        int i7 = (i4 + i5) * 31;
        boolean z4 = this.canBan;
        if (z4) {
            z4 = true;
        }
        int i8 = z4 ? 1 : 0;
        int i9 = z4 ? 1 : 0;
        int i10 = (i7 + i8) * 31;
        boolean z5 = this.canMute;
        if (z5) {
            z5 = true;
        }
        int i11 = z5 ? 1 : 0;
        int i12 = z5 ? 1 : 0;
        int i13 = (i10 + i11) * 31;
        boolean z6 = this.canDeafen;
        if (z6) {
            z6 = true;
        }
        int i14 = z6 ? 1 : 0;
        int i15 = z6 ? 1 : 0;
        int i16 = (i13 + i14) * 31;
        boolean z7 = this.canMove;
        if (z7) {
            z7 = true;
        }
        int i17 = z7 ? 1 : 0;
        int i18 = z7 ? 1 : 0;
        int i19 = (i16 + i17) * 31;
        boolean z8 = this.canChangeNickname;
        if (z8) {
            z8 = true;
        }
        int i20 = z8 ? 1 : 0;
        int i21 = z8 ? 1 : 0;
        int i22 = (i19 + i20) * 31;
        boolean z9 = this.canDisableCommunication;
        if (z9) {
            z9 = true;
        }
        int i23 = z9 ? 1 : 0;
        int i24 = z9 ? 1 : 0;
        int i25 = (i22 + i23) * 31;
        boolean z10 = this.canTransferOwnership;
        if (!z10) {
            i = z10 ? 1 : 0;
        }
        return i25 + i;
    }

    public String toString() {
        StringBuilder R = a.R("ManageUserContext(canManageRoles=");
        R.append(this.canManageRoles);
        R.append(", canKick=");
        R.append(this.canKick);
        R.append(", canBan=");
        R.append(this.canBan);
        R.append(", canMute=");
        R.append(this.canMute);
        R.append(", canDeafen=");
        R.append(this.canDeafen);
        R.append(", canMove=");
        R.append(this.canMove);
        R.append(", canChangeNickname=");
        R.append(this.canChangeNickname);
        R.append(", canDisableCommunication=");
        R.append(this.canDisableCommunication);
        R.append(", canTransferOwnership=");
        return a.M(R, this.canTransferOwnership, ")");
    }
}
