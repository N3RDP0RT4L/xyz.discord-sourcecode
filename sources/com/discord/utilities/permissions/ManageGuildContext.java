package com.discord.utilities.permissions;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.permission.Permission;
import d0.z.d.m;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: PermissionsContexts.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0016\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0012\b\u0086\b\u0018\u0000 -2\u00020\u0001:\u0001-BO\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\u0006\u0010\u0010\u001a\u00020\u0002\u0012\u0006\u0010\u0011\u001a\u00020\u0002\u0012\u0006\u0010\u0012\u001a\u00020\u0002\u0012\u0006\u0010\u0013\u001a\u00020\u0002\u0012\u0006\u0010\u0014\u001a\u00020\u0002\u0012\u0006\u0010\u0015\u001a\u00020\u0002\u0012\u0006\u0010\u0016\u001a\u00020\u0002¢\u0006\u0004\b+\u0010,J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0004J\u0010\u0010\b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\t\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\u0004J\u0010\u0010\n\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\n\u0010\u0004J\u0010\u0010\u000b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u000b\u0010\u0004J\u0010\u0010\f\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\f\u0010\u0004J\u0010\u0010\r\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\r\u0010\u0004Jj\u0010\u0017\u001a\u00020\u00002\b\b\u0002\u0010\u000e\u001a\u00020\u00022\b\b\u0002\u0010\u000f\u001a\u00020\u00022\b\b\u0002\u0010\u0010\u001a\u00020\u00022\b\b\u0002\u0010\u0011\u001a\u00020\u00022\b\b\u0002\u0010\u0012\u001a\u00020\u00022\b\b\u0002\u0010\u0013\u001a\u00020\u00022\b\b\u0002\u0010\u0014\u001a\u00020\u00022\b\b\u0002\u0010\u0015\u001a\u00020\u00022\b\b\u0002\u0010\u0016\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u001a\u001a\u00020\u0019HÖ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u001d\u001a\u00020\u001cHÖ\u0001¢\u0006\u0004\b\u001d\u0010\u001eJ\u001a\u0010 \u001a\u00020\u00022\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b \u0010!R\u0019\u0010\u0016\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\"\u001a\u0004\b\u0016\u0010\u0004R\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\"\u001a\u0004\b#\u0010\u0004R\u0019\u0010\u0012\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\"\u001a\u0004\b$\u0010\u0004R\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\"\u001a\u0004\b%\u0010\u0004R\u0019\u0010\u0013\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\"\u001a\u0004\b&\u0010\u0004R\u0019\u0010\u0010\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\"\u001a\u0004\b'\u0010\u0004R\u0019\u0010\u0015\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\"\u001a\u0004\b(\u0010\u0004R\u0019\u0010\u0011\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\"\u001a\u0004\b)\u0010\u0004R\u0019\u0010\u0014\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\"\u001a\u0004\b*\u0010\u0004¨\u0006."}, d2 = {"Lcom/discord/utilities/permissions/ManageGuildContext;", "", "", "canManage", "()Z", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "canManageServer", "canManageChannels", "canManageRoles", "canManageBans", "canManageNicknames", "canManageEmojisAndStickers", "canViewAuditLogs", "canManageEvents", "isOwnerWithRequiredMFALevel", "copy", "(ZZZZZZZZZ)Lcom/discord/utilities/permissions/ManageGuildContext;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getCanManageServer", "getCanManageNicknames", "getCanManageChannels", "getCanManageEmojisAndStickers", "getCanManageRoles", "getCanManageEvents", "getCanManageBans", "getCanViewAuditLogs", HookHelper.constructorName, "(ZZZZZZZZZ)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ManageGuildContext {
    public static final Companion Companion = new Companion(null);
    private final boolean canManageBans;
    private final boolean canManageChannels;
    private final boolean canManageEmojisAndStickers;
    private final boolean canManageEvents;
    private final boolean canManageNicknames;
    private final boolean canManageRoles;
    private final boolean canManageServer;
    private final boolean canViewAuditLogs;
    private final boolean isOwnerWithRequiredMFALevel;

    /* compiled from: PermissionsContexts.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0013\u0010\u0014Ja\u0010\u0011\u001a\u00020\u00102\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\u0006\u0010\u0006\u001a\u00020\u00052\u000e\u0010\t\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\b2\u001a\u0010\f\u001a\u0016\u0012\b\u0012\u00060\u0007j\u0002`\u000b\u0012\b\u0012\u00060\u0007j\u0002`\b0\n2\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u0005H\u0007¢\u0006\u0004\b\u0011\u0010\u0012¨\u0006\u0015"}, d2 = {"Lcom/discord/utilities/permissions/ManageGuildContext$Companion;", "", "", "Lcom/discord/api/channel/Channel;", "categories", "", "isOwner", "", "Lcom/discord/api/permission/PermissionBit;", "guildPermissions", "", "Lcom/discord/primitives/ChannelId;", "channelPermissions", "", "guildMfaLevel", "isMeMfaEnabled", "Lcom/discord/utilities/permissions/ManageGuildContext;", "from", "(Ljava/util/List;ZLjava/lang/Long;Ljava/util/Map;IZ)Lcom/discord/utilities/permissions/ManageGuildContext;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final ManageGuildContext from(List<Channel> list, boolean z2, Long l, Map<Long, Long> map, int i, boolean z3) {
            boolean z4;
            m.checkNotNullParameter(list, "categories");
            m.checkNotNullParameter(map, "channelPermissions");
            if (!(list instanceof Collection) || !list.isEmpty()) {
                for (Channel channel : list) {
                    if (PermissionUtils.can(16L, (Long) a.c(channel, map))) {
                        z4 = true;
                        break;
                    }
                }
            }
            z4 = false;
            return new ManageGuildContext(z2 || PermissionUtils.can(32L, l), z2 || PermissionUtils.can(16L, l) || z4, z2 || PermissionUtils.can(Permission.MANAGE_ROLES, l), z2 || PermissionUtils.can(4L, l), z2 || PermissionUtils.can(Permission.MANAGE_NICKNAMES, l), z2 || PermissionUtils.can(Permission.MANAGE_EMOJIS_AND_STICKERS, l), z2 || PermissionUtils.can(128L, l), z2 || PermissionUtils.can(Permission.MANAGE_EVENTS, l), z2 && PermissionUtils.isElevated(z3, i));
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public ManageGuildContext(boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9, boolean z10) {
        this.canManageServer = z2;
        this.canManageChannels = z3;
        this.canManageRoles = z4;
        this.canManageBans = z5;
        this.canManageNicknames = z6;
        this.canManageEmojisAndStickers = z7;
        this.canViewAuditLogs = z8;
        this.canManageEvents = z9;
        this.isOwnerWithRequiredMFALevel = z10;
    }

    public static final ManageGuildContext from(List<Channel> list, boolean z2, Long l, Map<Long, Long> map, int i, boolean z3) {
        return Companion.from(list, z2, l, map, i, z3);
    }

    public final boolean canManage() {
        return this.canManageServer || this.canManageChannels || this.canManageRoles || this.canManageBans || this.canViewAuditLogs || this.canManageEmojisAndStickers || this.isOwnerWithRequiredMFALevel;
    }

    public final boolean component1() {
        return this.canManageServer;
    }

    public final boolean component2() {
        return this.canManageChannels;
    }

    public final boolean component3() {
        return this.canManageRoles;
    }

    public final boolean component4() {
        return this.canManageBans;
    }

    public final boolean component5() {
        return this.canManageNicknames;
    }

    public final boolean component6() {
        return this.canManageEmojisAndStickers;
    }

    public final boolean component7() {
        return this.canViewAuditLogs;
    }

    public final boolean component8() {
        return this.canManageEvents;
    }

    public final boolean component9() {
        return this.isOwnerWithRequiredMFALevel;
    }

    public final ManageGuildContext copy(boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9, boolean z10) {
        return new ManageGuildContext(z2, z3, z4, z5, z6, z7, z8, z9, z10);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ManageGuildContext)) {
            return false;
        }
        ManageGuildContext manageGuildContext = (ManageGuildContext) obj;
        return this.canManageServer == manageGuildContext.canManageServer && this.canManageChannels == manageGuildContext.canManageChannels && this.canManageRoles == manageGuildContext.canManageRoles && this.canManageBans == manageGuildContext.canManageBans && this.canManageNicknames == manageGuildContext.canManageNicknames && this.canManageEmojisAndStickers == manageGuildContext.canManageEmojisAndStickers && this.canViewAuditLogs == manageGuildContext.canViewAuditLogs && this.canManageEvents == manageGuildContext.canManageEvents && this.isOwnerWithRequiredMFALevel == manageGuildContext.isOwnerWithRequiredMFALevel;
    }

    public final boolean getCanManageBans() {
        return this.canManageBans;
    }

    public final boolean getCanManageChannels() {
        return this.canManageChannels;
    }

    public final boolean getCanManageEmojisAndStickers() {
        return this.canManageEmojisAndStickers;
    }

    public final boolean getCanManageEvents() {
        return this.canManageEvents;
    }

    public final boolean getCanManageNicknames() {
        return this.canManageNicknames;
    }

    public final boolean getCanManageRoles() {
        return this.canManageRoles;
    }

    public final boolean getCanManageServer() {
        return this.canManageServer;
    }

    public final boolean getCanViewAuditLogs() {
        return this.canViewAuditLogs;
    }

    public int hashCode() {
        boolean z2 = this.canManageServer;
        int i = 1;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        int i4 = i2 * 31;
        boolean z3 = this.canManageChannels;
        if (z3) {
            z3 = true;
        }
        int i5 = z3 ? 1 : 0;
        int i6 = z3 ? 1 : 0;
        int i7 = (i4 + i5) * 31;
        boolean z4 = this.canManageRoles;
        if (z4) {
            z4 = true;
        }
        int i8 = z4 ? 1 : 0;
        int i9 = z4 ? 1 : 0;
        int i10 = (i7 + i8) * 31;
        boolean z5 = this.canManageBans;
        if (z5) {
            z5 = true;
        }
        int i11 = z5 ? 1 : 0;
        int i12 = z5 ? 1 : 0;
        int i13 = (i10 + i11) * 31;
        boolean z6 = this.canManageNicknames;
        if (z6) {
            z6 = true;
        }
        int i14 = z6 ? 1 : 0;
        int i15 = z6 ? 1 : 0;
        int i16 = (i13 + i14) * 31;
        boolean z7 = this.canManageEmojisAndStickers;
        if (z7) {
            z7 = true;
        }
        int i17 = z7 ? 1 : 0;
        int i18 = z7 ? 1 : 0;
        int i19 = (i16 + i17) * 31;
        boolean z8 = this.canViewAuditLogs;
        if (z8) {
            z8 = true;
        }
        int i20 = z8 ? 1 : 0;
        int i21 = z8 ? 1 : 0;
        int i22 = (i19 + i20) * 31;
        boolean z9 = this.canManageEvents;
        if (z9) {
            z9 = true;
        }
        int i23 = z9 ? 1 : 0;
        int i24 = z9 ? 1 : 0;
        int i25 = (i22 + i23) * 31;
        boolean z10 = this.isOwnerWithRequiredMFALevel;
        if (!z10) {
            i = z10 ? 1 : 0;
        }
        return i25 + i;
    }

    public final boolean isOwnerWithRequiredMFALevel() {
        return this.isOwnerWithRequiredMFALevel;
    }

    public String toString() {
        StringBuilder R = a.R("ManageGuildContext(canManageServer=");
        R.append(this.canManageServer);
        R.append(", canManageChannels=");
        R.append(this.canManageChannels);
        R.append(", canManageRoles=");
        R.append(this.canManageRoles);
        R.append(", canManageBans=");
        R.append(this.canManageBans);
        R.append(", canManageNicknames=");
        R.append(this.canManageNicknames);
        R.append(", canManageEmojisAndStickers=");
        R.append(this.canManageEmojisAndStickers);
        R.append(", canViewAuditLogs=");
        R.append(this.canViewAuditLogs);
        R.append(", canManageEvents=");
        R.append(this.canManageEvents);
        R.append(", isOwnerWithRequiredMFALevel=");
        return a.M(R, this.isOwnerWithRequiredMFALevel, ")");
    }
}
