package com.discord.utilities.permissions;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.permission.Permission;
import com.discord.api.sticker.Sticker;
import com.discord.api.sticker.StickerPartial;
import com.discord.api.user.User;
import com.discord.models.message.Message;
import com.discord.models.user.MeUser;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: PermissionsContexts.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u000f\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0010\b\u0086\b\u0018\u0000 $2\u00020\u0001:\u0001$B7\u0012\u0006\u0010\n\u001a\u00020\u0002\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\r\u001a\u00020\u0002\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\u0006\u0010\u000f\u001a\u00020\u0002¢\u0006\u0004\b\"\u0010#J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0004J\u0010\u0010\b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\t\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\u0004JL\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u00022\b\b\u0002\u0010\u000b\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\u00022\b\b\u0002\u0010\r\u001a\u00020\u00022\b\b\u0002\u0010\u000e\u001a\u00020\u00022\b\b\u0002\u0010\u000f\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u001a\u0010\u0019\u001a\u00020\u00022\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0019\u0010\u001aR\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001b\u001a\u0004\b\u001c\u0010\u0004R\u0019\u0010\r\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001b\u001a\u0004\b\u001d\u0010\u0004R\u0019\u0010\n\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u001b\u001a\u0004\b\u001e\u0010\u0004R\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001b\u001a\u0004\b\u001f\u0010\u0004R\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001b\u001a\u0004\b \u0010\u0004R\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001b\u001a\u0004\b!\u0010\u0004¨\u0006%"}, d2 = {"Lcom/discord/utilities/permissions/ManageMessageContext;", "", "", "component1", "()Z", "component2", "component3", "component4", "component5", "component6", "canManageMessages", "canEdit", "canDelete", "canAddReactions", "canTogglePinned", "canMarkUnread", "copy", "(ZZZZZZ)Lcom/discord/utilities/permissions/ManageMessageContext;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getCanEdit", "getCanAddReactions", "getCanManageMessages", "getCanDelete", "getCanMarkUnread", "getCanTogglePinned", HookHelper.constructorName, "(ZZZZZZ)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ManageMessageContext {
    public static final Companion Companion = new Companion(null);
    private final boolean canAddReactions;
    private final boolean canDelete;
    private final boolean canEdit;
    private final boolean canManageMessages;
    private final boolean canMarkUnread;
    private final boolean canTogglePinned;

    /* compiled from: PermissionsContexts.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0012\u0010\u0013JQ\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0003\u001a\u00020\u00022\u000e\u0010\u0006\u001a\n\u0018\u00010\u0004j\u0004\u0018\u0001`\u00052\u0006\u0010\b\u001a\u00020\u00072\b\u0010\n\u001a\u0004\u0018\u00010\t2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\u000bH\u0007¢\u0006\u0004\b\u0010\u0010\u0011¨\u0006\u0014"}, d2 = {"Lcom/discord/utilities/permissions/ManageMessageContext$Companion;", "", "Lcom/discord/models/message/Message;", "message", "", "Lcom/discord/api/permission/PermissionBit;", "myPermissions", "Lcom/discord/models/user/MeUser;", "meUser", "", "guildMfaLevel", "", "isPrivateChannel", "isSystemDM", "isArchivedThread", "Lcom/discord/utilities/permissions/ManageMessageContext;", "from", "(Lcom/discord/models/message/Message;Ljava/lang/Long;Lcom/discord/models/user/MeUser;Ljava/lang/Integer;ZZZ)Lcom/discord/utilities/permissions/ManageMessageContext;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final ManageMessageContext from(Message message, Long l, MeUser meUser, Integer num, boolean z2, boolean z3, boolean z4) {
            boolean z5;
            m.checkNotNullParameter(message, "message");
            m.checkNotNullParameter(meUser, "meUser");
            boolean isUserMessage = message.isUserMessage();
            User author = message.getAuthor();
            boolean z6 = false;
            boolean z7 = author != null && author.i() == meUser.getId();
            boolean z8 = z2 && !z3;
            boolean z9 = num != null && PermissionUtils.canAndIsElevated(Permission.MANAGE_MESSAGES, l, meUser.getMfaEnabled(), num.intValue()) && !z4;
            boolean z10 = z9 || z8;
            boolean z11 = z9 || z7;
            if (z7 && isUserMessage) {
                List<Sticker> stickers = message.getStickers();
                if (stickers == null || stickers.isEmpty()) {
                    List<StickerPartial> stickerItems = message.getStickerItems();
                    if ((stickerItems == null || stickerItems.isEmpty()) && !z4) {
                        z5 = true;
                        boolean z12 = !z11 && !z4 && PermissionsContextsKt.isDeleteable(message);
                        boolean z13 = message.isLocal() && (z8 || PermissionUtils.can(64L, l)) && !z4;
                        if (z10 && isUserMessage && !z3 && !z4) {
                            z6 = true;
                        }
                        return new ManageMessageContext(z9, z5, z12, z13, z6, !z4);
                    }
                }
            }
            z5 = false;
            if (!z11) {
            }
            if (message.isLocal()) {
            }
            if (z10) {
                z6 = true;
            }
            return new ManageMessageContext(z9, z5, z12, z13, z6, !z4);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public ManageMessageContext(boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7) {
        this.canManageMessages = z2;
        this.canEdit = z3;
        this.canDelete = z4;
        this.canAddReactions = z5;
        this.canTogglePinned = z6;
        this.canMarkUnread = z7;
    }

    public static /* synthetic */ ManageMessageContext copy$default(ManageMessageContext manageMessageContext, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = manageMessageContext.canManageMessages;
        }
        if ((i & 2) != 0) {
            z3 = manageMessageContext.canEdit;
        }
        boolean z8 = z3;
        if ((i & 4) != 0) {
            z4 = manageMessageContext.canDelete;
        }
        boolean z9 = z4;
        if ((i & 8) != 0) {
            z5 = manageMessageContext.canAddReactions;
        }
        boolean z10 = z5;
        if ((i & 16) != 0) {
            z6 = manageMessageContext.canTogglePinned;
        }
        boolean z11 = z6;
        if ((i & 32) != 0) {
            z7 = manageMessageContext.canMarkUnread;
        }
        return manageMessageContext.copy(z2, z8, z9, z10, z11, z7);
    }

    public static final ManageMessageContext from(Message message, Long l, MeUser meUser, Integer num, boolean z2, boolean z3, boolean z4) {
        return Companion.from(message, l, meUser, num, z2, z3, z4);
    }

    public final boolean component1() {
        return this.canManageMessages;
    }

    public final boolean component2() {
        return this.canEdit;
    }

    public final boolean component3() {
        return this.canDelete;
    }

    public final boolean component4() {
        return this.canAddReactions;
    }

    public final boolean component5() {
        return this.canTogglePinned;
    }

    public final boolean component6() {
        return this.canMarkUnread;
    }

    public final ManageMessageContext copy(boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7) {
        return new ManageMessageContext(z2, z3, z4, z5, z6, z7);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ManageMessageContext)) {
            return false;
        }
        ManageMessageContext manageMessageContext = (ManageMessageContext) obj;
        return this.canManageMessages == manageMessageContext.canManageMessages && this.canEdit == manageMessageContext.canEdit && this.canDelete == manageMessageContext.canDelete && this.canAddReactions == manageMessageContext.canAddReactions && this.canTogglePinned == manageMessageContext.canTogglePinned && this.canMarkUnread == manageMessageContext.canMarkUnread;
    }

    public final boolean getCanAddReactions() {
        return this.canAddReactions;
    }

    public final boolean getCanDelete() {
        return this.canDelete;
    }

    public final boolean getCanEdit() {
        return this.canEdit;
    }

    public final boolean getCanManageMessages() {
        return this.canManageMessages;
    }

    public final boolean getCanMarkUnread() {
        return this.canMarkUnread;
    }

    public final boolean getCanTogglePinned() {
        return this.canTogglePinned;
    }

    public int hashCode() {
        boolean z2 = this.canManageMessages;
        int i = 1;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        int i4 = i2 * 31;
        boolean z3 = this.canEdit;
        if (z3) {
            z3 = true;
        }
        int i5 = z3 ? 1 : 0;
        int i6 = z3 ? 1 : 0;
        int i7 = (i4 + i5) * 31;
        boolean z4 = this.canDelete;
        if (z4) {
            z4 = true;
        }
        int i8 = z4 ? 1 : 0;
        int i9 = z4 ? 1 : 0;
        int i10 = (i7 + i8) * 31;
        boolean z5 = this.canAddReactions;
        if (z5) {
            z5 = true;
        }
        int i11 = z5 ? 1 : 0;
        int i12 = z5 ? 1 : 0;
        int i13 = (i10 + i11) * 31;
        boolean z6 = this.canTogglePinned;
        if (z6) {
            z6 = true;
        }
        int i14 = z6 ? 1 : 0;
        int i15 = z6 ? 1 : 0;
        int i16 = (i13 + i14) * 31;
        boolean z7 = this.canMarkUnread;
        if (!z7) {
            i = z7 ? 1 : 0;
        }
        return i16 + i;
    }

    public String toString() {
        StringBuilder R = a.R("ManageMessageContext(canManageMessages=");
        R.append(this.canManageMessages);
        R.append(", canEdit=");
        R.append(this.canEdit);
        R.append(", canDelete=");
        R.append(this.canDelete);
        R.append(", canAddReactions=");
        R.append(this.canAddReactions);
        R.append(", canTogglePinned=");
        R.append(this.canTogglePinned);
        R.append(", canMarkUnread=");
        return a.M(R, this.canMarkUnread, ")");
    }
}
