package com.discord.utilities.permissions;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.permission.Permission;
import com.discord.api.permission.PermissionOverwrite;
import com.discord.api.role.GuildRole;
import com.discord.api.stageinstance.StageInstance;
import com.discord.api.stageinstance.StageInstancePrivacyLevel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.member.GuildMember;
import com.discord.stores.StoreSlowMode;
import com.discord.utilities.PermissionOverwriteUtilsKt;
import com.discord.utilities.guildmember.GuildMemberUtilsKt;
import com.discord.widgets.chat.list.NewThreadsPermissionsFeatureFlag;
import d0.t.h0;
import d0.t.n;
import d0.z.d.m;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
/* compiled from: PermissionUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\b\u001c\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\bO\u0010PJ\u001f\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0007¢\u0006\u0004\b\u0006\u0010\u0007J+\u0010\u0006\u001a\u00020\u00022\n\u0010\n\u001a\u00060\bj\u0002`\t2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0007¢\u0006\u0004\b\u0006\u0010\u000bJ;\u0010\r\u001a\u00020\u00022\n\u0010\n\u001a\u00060\bj\u0002`\t2\u000e\u0010\f\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0007¢\u0006\u0004\b\r\u0010\u000eJ+\u0010\u000f\u001a\u00020\u00022\n\u0010\n\u001a\u00060\bj\u0002`\t2\u000e\u0010\f\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\tH\u0007¢\u0006\u0004\b\u000f\u0010\u0010JC\u0010\u0017\u001a\u00020\u00022\n\u0010\n\u001a\u00060\bj\u0002`\t2\u0006\u0010\u0012\u001a\u00020\u00112\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00112\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00150\u0014H\u0007¢\u0006\u0004\b\u0017\u0010\u0018J7\u0010\u001b\u001a\u00020\u00022\u0006\u0010\u0019\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u000e\u0010\u001a\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\tH\u0007¢\u0006\u0004\b\u001b\u0010\u001cJw\u0010&\u001a\u00060\bj\u0002`\t2\u0006\u0010\u001d\u001a\u00020\b2\u0006\u0010\u0012\u001a\u00020\u00112\b\u0010\u0013\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u001e\u001a\u00020\b2\b\u0010 \u001a\u0004\u0018\u00010\u001f2\u0014\u0010!\u001a\u0010\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u0015\u0018\u00010\u00142\u0018\u0010$\u001a\u0014\u0012\b\u0012\u00060\bj\u0002`\"\u0012\u0004\u0012\u00020#\u0018\u00010\u00142\u0006\u0010%\u001a\u00020\u0002H\u0007¢\u0006\u0004\b&\u0010'J[\u0010,\u001a\u00060\bj\u0002`\t2\u0006\u0010\u001d\u001a\u00020\b2\u0006\u0010(\u001a\u00020\b2\u0006\u0010\u001e\u001a\u00020\b2\b\u0010 \u001a\u0004\u0018\u00010\u001f2\u0014\u0010!\u001a\u0010\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u0015\u0018\u00010\u00142\u000e\u0010+\u001a\n\u0012\u0004\u0012\u00020*\u0018\u00010)H\u0007¢\u0006\u0004\b,\u0010-J[\u00100\u001a\u00060\bj\u0002`\t2\u0006\u0010\u001d\u001a\u00020\b2\u0006\u0010.\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u00112\u0006\u0010\u001e\u001a\u00020\b2\b\u0010 \u001a\u0004\u0018\u00010\u001f2\u0014\u0010!\u001a\u0010\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u0015\u0018\u00010\u00142\u0006\u0010/\u001a\u00020\u0002H\u0007¢\u0006\u0004\b0\u00101J+\u00102\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u00112\u0012\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\b0\u0014H\u0007¢\u0006\u0004\b2\u00103J1\u00104\u001a\u00060\bj\u0002`\t2\u0006\u0010(\u001a\u00020\b2\u0014\u0010!\u001a\u0010\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u0015\u0018\u00010\u0014H\u0002¢\u0006\u0004\b4\u00105J?\u00106\u001a\u00060\bj\u0002`\t2\b\u0010 \u001a\u0004\u0018\u00010\u001f2\u0014\u0010!\u001a\u0010\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u0015\u0018\u00010\u00142\n\u0010\u001a\u001a\u00060\bj\u0002`\tH\u0002¢\u0006\u0004\b6\u00107J5\u00109\u001a\u00060\bj\u0002`\t2\u0006\u0010(\u001a\u00020\b2\f\u00108\u001a\b\u0012\u0004\u0012\u00020*0)2\n\u0010\u001a\u001a\u00060\bj\u0002`\tH\u0002¢\u0006\u0004\b9\u0010:J?\u0010;\u001a\u00060\bj\u0002`\t2\b\u0010 \u001a\u0004\u0018\u00010\u001f2\f\u00108\u001a\b\u0012\u0004\u0012\u00020*0)2\n\u0010\u001a\u001a\u00060\bj\u0002`\t2\u0006\u0010\u001d\u001a\u00020\bH\u0002¢\u0006\u0004\b;\u0010<J-\u0010?\u001a\u00020\u00022\n\u0010\n\u001a\u00060\bj\u0002`\t2\b\u0010=\u001a\u0004\u0018\u00010\u00152\b\u0010>\u001a\u0004\u0018\u00010*¢\u0006\u0004\b?\u0010@J5\u0010A\u001a\u00020\u00022\n\u0010\n\u001a\u00060\bj\u0002`\t2\u0006\u0010\u0012\u001a\u00020\u00112\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00150\u0014¢\u0006\u0004\bA\u0010BJ%\u00102\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u00112\u000e\u0010C\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t¢\u0006\u0004\b2\u0010DJ%\u0010E\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u00112\u000e\u0010C\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t¢\u0006\u0004\bE\u0010DJ%\u0010I\u001a\u00020\u00022\u000e\u0010F\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t2\u0006\u0010H\u001a\u00020G¢\u0006\u0004\bI\u0010JJC\u0010M\u001a\u00060\bj\u0002`\t2\n\u0010\u001d\u001a\u00060\bj\u0002`K2\n\u0010(\u001a\u00060\bj\u0002`L2\b\u0010 \u001a\u0004\u0018\u00010\u001f2\u000e\u0010+\u001a\n\u0012\u0004\u0012\u00020*\u0018\u00010)¢\u0006\u0004\bM\u0010N¨\u0006Q"}, d2 = {"Lcom/discord/utilities/permissions/PermissionUtils;", "", "", "userMfaEnabled", "", "guildMfaLevel", "isElevated", "(ZI)Z", "", "Lcom/discord/api/permission/PermissionBit;", "permission", "(JZI)Z", "computedPermissions", "canAndIsElevated", "(JLjava/lang/Long;ZI)Z", "can", "(JLjava/lang/Long;)Z", "Lcom/discord/api/channel/Channel;", "channel", "parentChannel", "", "Lcom/discord/api/role/GuildRole;", "roles", "canEveryone", "(JLcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;Ljava/util/Map;)Z", "isOwner", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS, "canManageGuildMembers", "(ZZILjava/lang/Long;)Z", "userId", "guildOwnerId", "Lcom/discord/models/member/GuildMember;", "member", "guildRoles", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/stageinstance/StageInstance;", "stageInstances", "hasJoinedThread", "computePermissions", "(JLcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;JLcom/discord/models/member/GuildMember;Ljava/util/Map;Ljava/util/Map;Z)J", "guildId", "", "Lcom/discord/api/permission/PermissionOverwrite;", "permissionOverwrites", "computeNonThreadPermissions", "(JJJLcom/discord/models/member/GuildMember;Ljava/util/Map;Ljava/util/Collection;)J", "thread", "hasJoined", "computeThreadPermissions", "(JLcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;JLcom/discord/models/member/GuildMember;Ljava/util/Map;Z)J", "hasAccess", "(Lcom/discord/api/channel/Channel;Ljava/util/Map;)Z", "applyEveryone", "(JLjava/util/Map;)J", "applyRoles", "(Lcom/discord/models/member/GuildMember;Ljava/util/Map;J)J", "overwrites", "applyEveryoneOverwrites", "(JLjava/util/Collection;J)J", "applyRoleOverwrites", "(Lcom/discord/models/member/GuildMember;Ljava/util/Collection;JJ)J", "role", "roleOverwrite", "canRole", "(JLcom/discord/api/role/GuildRole;Lcom/discord/api/permission/PermissionOverwrite;)Z", "canEveryoneRole", "(JLcom/discord/api/channel/Channel;Ljava/util/Map;)Z", "computedPermission", "(Lcom/discord/api/channel/Channel;Ljava/lang/Long;)Z", "hasAccessWrite", "channelPermissions", "Lcom/discord/stores/StoreSlowMode$Type;", "type", "hasBypassSlowmodePermissions", "(Ljava/lang/Long;Lcom/discord/stores/StoreSlowMode$Type;)Z", "Lcom/discord/primitives/UserId;", "Lcom/discord/primitives/GuildId;", "computeChannelOverwrite", "(JJLcom/discord/models/member/GuildMember;Ljava/util/Collection;)J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class PermissionUtils {
    public static final PermissionUtils INSTANCE = new PermissionUtils();

    private PermissionUtils() {
    }

    private final long applyEveryone(long j, Map<Long, GuildRole> map) {
        GuildRole guildRole = map != null ? map.get(Long.valueOf(j)) : null;
        return guildRole != null ? guildRole.h() : Permission.DEFAULT;
    }

    private final long applyEveryoneOverwrites(long j, Collection<PermissionOverwrite> collection, long j2) {
        Object obj;
        boolean z2;
        Iterator<T> it = collection.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (((PermissionOverwrite) obj).e() == j) {
                z2 = true;
                continue;
            } else {
                z2 = false;
                continue;
            }
            if (z2) {
                break;
            }
        }
        PermissionOverwrite permissionOverwrite = (PermissionOverwrite) obj;
        return permissionOverwrite != null ? ((permissionOverwrite.d() & j2) ^ j2) | permissionOverwrite.c() : j2;
    }

    private final long applyRoleOverwrites(GuildMember guildMember, Collection<PermissionOverwrite> collection, long j, long j2) {
        long j3;
        boolean z2;
        Object obj;
        boolean z3;
        Object obj2 = null;
        long j4 = 0;
        if (guildMember != null) {
            long j5 = 0;
            for (Long l : guildMember.getRoles()) {
                long longValue = l.longValue();
                Iterator<T> it = collection.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj = null;
                        break;
                    }
                    obj = it.next();
                    if (((PermissionOverwrite) obj).e() == longValue) {
                        z3 = true;
                        continue;
                    } else {
                        z3 = false;
                        continue;
                    }
                    if (z3) {
                        break;
                    }
                }
                PermissionOverwrite permissionOverwrite = (PermissionOverwrite) obj;
                if (permissionOverwrite != null) {
                    j4 |= permissionOverwrite.c();
                    j5 |= permissionOverwrite.d();
                }
            }
            j3 = j4;
            j4 = j5;
        } else {
            j3 = 0;
        }
        long j6 = (j ^ (j & j4)) | j3;
        Iterator<T> it2 = collection.iterator();
        while (true) {
            if (!it2.hasNext()) {
                break;
            }
            Object next = it2.next();
            if (((PermissionOverwrite) next).e() == j2) {
                z2 = true;
                continue;
            } else {
                z2 = false;
                continue;
            }
            if (z2) {
                obj2 = next;
                break;
            }
        }
        PermissionOverwrite permissionOverwrite2 = (PermissionOverwrite) obj2;
        return permissionOverwrite2 != null ? ((permissionOverwrite2.d() & j6) ^ j6) | permissionOverwrite2.c() : j6;
    }

    private final long applyRoles(GuildMember guildMember, Map<Long, GuildRole> map, long j) {
        if (guildMember != null) {
            for (Long l : guildMember.getRoles()) {
                GuildRole guildRole = map != null ? map.get(Long.valueOf(l.longValue())) : null;
                if (guildRole != null) {
                    j |= guildRole.h();
                }
            }
        }
        return j;
    }

    public static final boolean can(long j, Long l) {
        return ((l != null ? l.longValue() : 0L) & j) == j;
    }

    public static final boolean canAndIsElevated(long j, Long l, boolean z2, int i) {
        return can(j, l) && isElevated(j, z2, i);
    }

    public static final boolean canEveryone(long j, Channel channel, Channel channel2, Map<Long, GuildRole> map) {
        m.checkNotNullParameter(channel, "channel");
        m.checkNotNullParameter(map, "roles");
        if (!ChannelUtils.C(channel)) {
            GuildRole guildRole = (GuildRole) a.u0(channel, map);
            if (!(guildRole == null || (guildRole.h() & j) == j)) {
                return false;
            }
            List<PermissionOverwrite> s2 = channel.s();
            if (s2 == null) {
                return true;
            }
            for (PermissionOverwrite permissionOverwrite : s2) {
                if (PermissionOverwriteUtilsKt.denies(permissionOverwrite, j)) {
                    return false;
                }
            }
            return true;
        } else if (channel2 == null || ChannelUtils.y(channel)) {
            return false;
        } else {
            return canEveryone(j, channel2, null, map);
        }
    }

    public static /* synthetic */ boolean canEveryone$default(long j, Channel channel, Channel channel2, Map map, int i, Object obj) {
        if ((i & 4) != 0) {
            channel2 = null;
        }
        return canEveryone(j, channel, channel2, map);
    }

    public static final boolean canManageGuildMembers(boolean z2, boolean z3, int i, Long l) {
        return z2 || canAndIsElevated(8L, l, z3, i) || canAndIsElevated(2L, l, z3, i) || canAndIsElevated(4L, l, z3, i) || canAndIsElevated(Permission.MANAGE_ROLES, l, z3, i) || can(Permission.MANAGE_NICKNAMES, l);
    }

    public static final long computeNonThreadPermissions(long j, long j2, long j3, GuildMember guildMember, Map<Long, GuildRole> map, Collection<PermissionOverwrite> collection) {
        if (j3 == j) {
            return Permission.ALL;
        }
        PermissionUtils permissionUtils = INSTANCE;
        long applyRoles = permissionUtils.applyRoles(guildMember, map, permissionUtils.applyEveryone(j2, map));
        return (applyRoles & 8) == 8 ? Permission.ALL : (collection == null || !(collection.isEmpty() ^ true)) ? applyRoles : permissionUtils.applyRoleOverwrites(guildMember, collection, permissionUtils.applyEveryoneOverwrites(j2, collection, applyRoles), j);
    }

    public static final long computePermissions(long j, Channel channel, Channel channel2, long j2, GuildMember guildMember, Map<Long, GuildRole> map, Map<Long, StageInstance> map2, boolean z2) {
        long j3;
        m.checkNotNullParameter(channel, "channel");
        if (ChannelUtils.C(channel)) {
            j3 = channel2 == null ? 0L : computeThreadPermissions(j, channel, channel2, j2, guildMember, map, z2);
        } else {
            long f = channel.f();
            List<PermissionOverwrite> s2 = channel.s();
            if (s2 == null) {
                s2 = n.emptyList();
            }
            j3 = computeNonThreadPermissions(j, f, j2, guildMember, map, s2);
            if (channel.A() == 15) {
                j3 &= -2049;
            }
        }
        long j4 = 66560;
        if (!GuildMemberUtilsKt.isLurker(guildMember)) {
            return (guildMember == null || !guildMember.isCommunicationDisabled()) ? j3 : j3 & 66560;
        }
        Map<Long, StageInstance> emptyMap = map2 != null ? map2 : h0.emptyMap();
        m.checkNotNullParameter(channel, "$this$computeLurkerPermissionsAllowList");
        m.checkNotNullParameter(emptyMap, "stageInstances");
        if (ChannelUtils.z(channel)) {
            StageInstance stageInstance = (StageInstance) a.c(channel, emptyMap);
            if ((stageInstance != null ? stageInstance.e() : null) == StageInstancePrivacyLevel.PUBLIC) {
                j4 = Permission.AllowList.LURKER_STAGE_CHANNEL;
            }
        }
        return j3 & j4;
    }

    public static final long computeThreadPermissions(long j, Channel channel, Channel channel2, long j2, GuildMember guildMember, Map<Long, GuildRole> map, boolean z2) {
        m.checkNotNullParameter(channel, "thread");
        m.checkNotNullParameter(channel2, "parentChannel");
        long f = channel2.f();
        List<PermissionOverwrite> s2 = channel2.s();
        if (s2 == null) {
            s2 = n.emptyList();
        }
        long computeNonThreadPermissions = computeNonThreadPermissions(j, f, j2, guildMember, map, s2);
        if (NewThreadsPermissionsFeatureFlag.Companion.getINSTANCE().isEnabled(channel.f())) {
            if (ChannelUtils.y(channel) && !z2) {
                can(Permission.MANAGE_THREADS, Long.valueOf(computeNonThreadPermissions));
            }
            return can(Permission.SEND_MESSAGES_IN_THREADS, Long.valueOf(computeNonThreadPermissions)) ? computeNonThreadPermissions | Permission.SEND_MESSAGES : (-2049) & computeNonThreadPermissions;
        }
        if (ChannelUtils.y(channel)) {
            if (can(Permission.CREATE_PRIVATE_THREADS, Long.valueOf(computeNonThreadPermissions))) {
                computeNonThreadPermissions |= Permission.SEND_MESSAGES;
            }
            if (!z2 && !can(Permission.MANAGE_THREADS, Long.valueOf(computeNonThreadPermissions))) {
                return 0L;
            }
        } else if (can(Permission.CREATE_PUBLIC_THREADS, Long.valueOf(computeNonThreadPermissions))) {
            computeNonThreadPermissions |= Permission.SEND_MESSAGES;
        }
        return computeNonThreadPermissions;
    }

    public static final boolean hasAccess(Channel channel, Map<Long, Long> map) {
        m.checkNotNullParameter(channel, "channel");
        m.checkNotNullParameter(map, ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS);
        return INSTANCE.hasAccess(channel, (Long) a.c(channel, map));
    }

    public static final boolean isElevated(long j, boolean z2, int i) {
        if ((Permission.ELEVATED & j) != j) {
            return true;
        }
        return isElevated(z2, i);
    }

    public static final boolean isElevated(boolean z2, int i) {
        if (i == 0) {
            return true;
        }
        return z2;
    }

    public final boolean canEveryoneRole(long j, Channel channel, Map<Long, GuildRole> map) {
        m.checkNotNullParameter(channel, "channel");
        m.checkNotNullParameter(map, "roles");
        long f = channel.f();
        GuildRole guildRole = map.get(Long.valueOf(f));
        return canRole(j, guildRole, guildRole != null ? ChannelUtils.f(channel, f) : null);
    }

    public final boolean canRole(long j, GuildRole guildRole, PermissionOverwrite permissionOverwrite) {
        if (guildRole == null) {
            return false;
        }
        long h = guildRole.h();
        long j2 = 0;
        long c = permissionOverwrite != null ? permissionOverwrite.c() : 0L;
        if (permissionOverwrite != null) {
            j2 = permissionOverwrite.d();
        }
        return ((((h & j2) ^ h) | c) & j) == j;
    }

    public final long computeChannelOverwrite(long j, long j2, GuildMember guildMember, Collection<PermissionOverwrite> collection) {
        if (collection == null || !(!collection.isEmpty())) {
            return 0L;
        }
        return applyRoleOverwrites(guildMember, collection, applyEveryoneOverwrites(j2, collection, 0L), j);
    }

    public final boolean hasAccessWrite(Channel channel, Long l) {
        m.checkNotNullParameter(channel, "channel");
        return ChannelUtils.x(channel) || can(3072L, l);
    }

    public final boolean hasBypassSlowmodePermissions(Long l, StoreSlowMode.Type type) {
        m.checkNotNullParameter(type, "type");
        if (m.areEqual(type, StoreSlowMode.Type.MessageSend.INSTANCE)) {
            return can(16L, l) || can(Permission.MANAGE_MESSAGES, l);
        }
        return can(Permission.MANAGE_THREADS, l);
    }

    public final boolean hasAccess(Channel channel, Long l) {
        m.checkNotNullParameter(channel, "channel");
        if (ChannelUtils.x(channel)) {
            return true;
        }
        return can((ChannelUtils.s(channel) || ChannelUtils.k(channel)) ? Permission.VIEW_CHANNEL : Permission.CONNECT, l);
    }
}
