package com.discord.utilities.colors;

import android.graphics.Bitmap;
import androidx.exifinterface.media.ExifInterface;
import com.discord.app.AppLog;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.colors.RepresentativeColors;
import d0.l;
import d0.o;
import d0.t.h0;
import d0.w.h.c;
import d0.w.i.a.b;
import d0.w.i.a.e;
import d0.w.i.a.k;
import d0.z.d.m;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineScope;
import rx.subjects.BehaviorSubject;
/* compiled from: RepresentativeColors.kt */
@e(c = "com.discord.utilities.colors.RepresentativeColors$handleBitmap$1", f = "RepresentativeColors.kt", l = {59}, m = "invokeSuspend")
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u0002\"\u0004\b\u0000\u0010\u0000*\u00020\u0001H\u008a@¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {ExifInterface.GPS_DIRECTION_TRUE, "Lkotlinx/coroutines/CoroutineScope;", "", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class RepresentativeColors$handleBitmap$1 extends k implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
    public final /* synthetic */ Bitmap $bitmap;
    public final /* synthetic */ Object $id;
    public final /* synthetic */ String $url;
    public int label;
    public final /* synthetic */ RepresentativeColors this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public RepresentativeColors$handleBitmap$1(RepresentativeColors representativeColors, Object obj, Bitmap bitmap, String str, Continuation continuation) {
        super(2, continuation);
        this.this$0 = representativeColors;
        this.$id = obj;
        this.$bitmap = bitmap;
        this.$url = str;
    }

    @Override // d0.w.i.a.a
    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        m.checkNotNullParameter(continuation, "completion");
        return new RepresentativeColors$handleBitmap$1(this.this$0, this.$id, this.$bitmap, this.$url, continuation);
    }

    @Override // kotlin.jvm.functions.Function2
    public final Object invoke(CoroutineScope coroutineScope, Continuation<? super Unit> continuation) {
        return ((RepresentativeColors$handleBitmap$1) create(coroutineScope, continuation)).invokeSuspend(Unit.a);
    }

    @Override // d0.w.i.a.a
    public final Object invokeSuspend(Object obj) {
        int i;
        HashMap hashMap;
        BehaviorSubject behaviorSubject;
        HashMap hashMap2;
        int i2;
        HashMap hashMap3;
        Object coroutine_suspended = c.getCOROUTINE_SUSPENDED();
        int i3 = this.label;
        if (i3 == 0) {
            l.throwOnFailure(obj);
            hashMap3 = this.this$0.representativeColors;
            if (hashMap3.containsKey(this.$id)) {
                return Unit.a;
            }
            if (this.$bitmap.isRecycled()) {
                i = RepresentativeColors.BLURPLE;
                hashMap = this.this$0.representativeColors;
                hashMap.put(this.$id, b.boxInt(i));
                behaviorSubject = this.this$0.representativeColorsSubject;
                hashMap2 = this.this$0.representativeColors;
                behaviorSubject.onNext(hashMap2);
                return Unit.a;
            }
            Bitmap copy = this.$bitmap.copy(Bitmap.Config.ARGB_8888, false);
            RepresentativeColors representativeColors = this.this$0;
            m.checkNotNullExpressionValue(copy, "copiedBitmap");
            this.label = 1;
            obj = representativeColors.getRepresentativeColorAsync(copy, this);
            if (obj == coroutine_suspended) {
                return coroutine_suspended;
            }
        } else if (i3 == 1) {
            l.throwOnFailure(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        RepresentativeColors.RepresentativeColorResult representativeColorResult = (RepresentativeColors.RepresentativeColorResult) obj;
        if (representativeColorResult instanceof RepresentativeColors.RepresentativeColorResult.Success) {
            i2 = ((RepresentativeColors.RepresentativeColorResult.Success) representativeColorResult).getColor();
        } else if (representativeColorResult instanceof RepresentativeColors.RepresentativeColorResult.Failure) {
            i2 = RepresentativeColors.BLURPLE;
        } else {
            throw new NoWhenBranchMatchedException();
        }
        if (representativeColorResult instanceof RepresentativeColors.RepresentativeColorResult.Failure) {
            AppLog appLog = AppLog.g;
            Exception exception = ((RepresentativeColors.RepresentativeColorResult.Failure) representativeColorResult).getException();
            Pair[] pairArr = new Pair[4];
            pairArr[0] = o.to(ModelAuditLogEntry.CHANGE_KEY_ID, String.valueOf(this.$id));
            pairArr[1] = o.to("bitmapWidth", String.valueOf(this.$bitmap.getWidth()));
            pairArr[2] = o.to("bitmapHeight", String.valueOf(this.$bitmap.getHeight()));
            String str = this.$url;
            if (str == null) {
                str = "not provided";
            }
            pairArr[3] = o.to("url", str);
            appLog.e("Failed to get representative color for entity", exception, h0.mapOf(pairArr));
        }
        i = i2;
        hashMap = this.this$0.representativeColors;
        hashMap.put(this.$id, b.boxInt(i));
        behaviorSubject = this.this$0.representativeColorsSubject;
        hashMap2 = this.this$0.representativeColors;
        behaviorSubject.onNext(hashMap2);
        return Unit.a;
    }
}
