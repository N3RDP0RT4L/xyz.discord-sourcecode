package com.discord.utilities.colors;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.Resources;
import androidx.annotation.ColorInt;
import androidx.annotation.StringRes;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.font.FontUtils;
import com.jaredrummler.android.colorpicker.ColorPickerDialog;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: ColorPickerUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001d\u0010\u0006\u001a\u00020\u0005*\u00020\u00022\b\b\u0001\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J)\u0010\f\u001a\u00020\u000b2\u0006\u0010\t\u001a\u00020\b2\b\b\u0001\u0010\n\u001a\u00020\u00032\b\b\u0001\u0010\u0004\u001a\u00020\u0003¢\u0006\u0004\b\f\u0010\r¨\u0006\u0010"}, d2 = {"Lcom/discord/utilities/colors/ColorPickerUtils;", "", "Landroid/content/res/Resources;", "", "initialColor", "", "getColorsToDisplayForPicker", "(Landroid/content/res/Resources;I)[I", "Landroid/content/Context;", "context", "titleResId", "Lcom/jaredrummler/android/colorpicker/ColorPickerDialog;", "buildColorPickerDialog", "(Landroid/content/Context;II)Lcom/jaredrummler/android/colorpicker/ColorPickerDialog;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ColorPickerUtils {
    public static final ColorPickerUtils INSTANCE = new ColorPickerUtils();

    private ColorPickerUtils() {
    }

    @ColorInt
    private static final int[] getColorsToDisplayForPicker(Resources resources, @ColorInt int i) {
        int[] intArray = resources.getIntArray(R.array.color_picker_palette);
        m.checkNotNullExpressionValue(intArray, "getIntArray(com.discord.…ray.color_picker_palette)");
        ArrayList arrayList = new ArrayList();
        boolean z2 = false;
        for (int i2 : intArray) {
            arrayList.add(Integer.valueOf(i2));
            if (i2 == i) {
                z2 = true;
            }
        }
        if (!z2) {
            arrayList.add(Integer.valueOf(i));
        }
        return u.toIntArray(arrayList);
    }

    public final ColorPickerDialog buildColorPickerDialog(Context context, @StringRes int i, @ColorInt int i2) {
        m.checkNotNullParameter(context, "context");
        int[] iArr = ColorPickerDialog.j;
        ColorPickerDialog.k kVar = new ColorPickerDialog.k();
        kVar.h = i2;
        kVar.f3120s = ColorCompat.getThemedColor(context, (int) R.attr.colorBackgroundPrimary);
        kVar.i = false;
        Resources resources = context.getResources();
        m.checkNotNullExpressionValue(resources, "context.resources");
        kVar.g = getColorsToDisplayForPicker(resources, i2);
        kVar.a = i;
        kVar.r = ColorCompat.getThemedColor(context, (int) R.attr.colorHeaderPrimary);
        FontUtils fontUtils = FontUtils.INSTANCE;
        kVar.f3121x = fontUtils.getThemedFontResId(context, R.attr.font_display_bold);
        kVar.o = ColorCompat.getThemedColor(context, (int) R.attr.colorBackgroundAccent);
        kVar.c = R.string.color_picker_custom;
        kVar.v = ColorCompat.getColor(context, (int) R.color.white);
        kVar.f3119b = R.string.color_picker_presets;
        kVar.p = ColorCompat.getThemedColor(context, (int) R.attr.color_brand);
        kVar.d = R.string.select;
        kVar.l = true;
        kVar.e = R.string.reset;
        kVar.w = ColorCompat.getColor(context, (int) R.color.white);
        kVar.f3122y = fontUtils.getThemedFontResId(context, R.attr.font_primary_semibold);
        kVar.q = ColorCompat.getThemedColor(context, (int) R.attr.colorBackgroundModifierAccent);
        kVar.t = ColorCompat.getThemedColor(context, (int) R.attr.colorTextMuted);
        kVar.u = R.drawable.drawable_cpv_edit_text_background;
        kVar.f3123z = fontUtils.getThemedFontResId(context, R.attr.font_primary_normal);
        ColorPickerDialog a = kVar.a();
        m.checkNotNullExpressionValue(a, "ColorPickerDialog.newBui…mal))\n          .create()");
        return a;
    }
}
