package com.discord.utilities.colors;

import andhook.lib.HookHelper;
import android.graphics.Bitmap;
import android.graphics.Color;
import androidx.annotation.ColorInt;
import androidx.core.graphics.ColorUtils;
import androidx.exifinterface.media.ExifInterface;
import b.a.g.a;
import b.a.g.b;
import b.a.g.d;
import b.d.b.a.a;
import b.i.a.f.e.o.f;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.coroutines.Continuation;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlinx.coroutines.CoroutineDispatcher;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import s.a.a.n;
import s.a.k0;
import s.a.x0;
/* compiled from: RepresentativeColors.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\b\t\u0018\u0000 &*\u0004\b\u0000\u0010\u00012\u00020\u0002:\u0003&'(B\u0007¢\u0006\u0004\b$\u0010%J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u001f\u0010\u000b\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\bH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u001d\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\b0\r2\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001d\u0010\u0012\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00112\u0006\u0010\u0010\u001a\u00028\u0000¢\u0006\u0004\b\u0012\u0010\u0013J'\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0010\u001a\u00028\u00002\u0006\u0010\u0004\u001a\u00020\u00032\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014¢\u0006\u0004\b\u0017\u0010\u0018J\u001b\u0010\u0019\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0082@ø\u0001\u0000¢\u0006\u0004\b\u0019\u0010\u001aR2\u0010\u001d\u001a\u001e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\b0\u001bj\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\b`\u001c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eRj\u0010\"\u001aV\u0012$\u0012\"\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\b !*\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\b\u0018\u00010 0  !**\u0012$\u0012\"\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\b !*\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\b\u0018\u00010 0 \u0018\u00010\u001f0\u001f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010#\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006)"}, d2 = {"Lcom/discord/utilities/colors/RepresentativeColors;", ExifInterface.GPS_DIRECTION_TRUE, "", "Landroid/graphics/Bitmap;", "bitmap", "Lcom/discord/utilities/colors/RepresentativeColors$RepresentativeColorResult;", "getRepresentativeColor", "(Landroid/graphics/Bitmap;)Lcom/discord/utilities/colors/RepresentativeColors$RepresentativeColorResult;", "", "c1", "c2", "getColorDistance", "(II)I", "", "getPrimaryColorsForBitmap", "(Landroid/graphics/Bitmap;)Ljava/util/List;", ModelAuditLogEntry.CHANGE_KEY_ID, "Lrx/Observable;", "observeRepresentativeColor", "(Ljava/lang/Object;)Lrx/Observable;", "", "url", "", "handleBitmap", "(Ljava/lang/Object;Landroid/graphics/Bitmap;Ljava/lang/String;)V", "getRepresentativeColorAsync", "(Landroid/graphics/Bitmap;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "representativeColors", "Ljava/util/HashMap;", "Lrx/subjects/BehaviorSubject;", "", "kotlin.jvm.PlatformType", "representativeColorsSubject", "Lrx/subjects/BehaviorSubject;", HookHelper.constructorName, "()V", "Companion", "NoSwatchesFoundException", "RepresentativeColorResult", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class RepresentativeColors<T> {
    private final HashMap<T, Integer> representativeColors = new HashMap<>();
    private final BehaviorSubject<Map<T, Integer>> representativeColorsSubject = BehaviorSubject.k0();
    public static final Companion Companion = new Companion(null);
    private static final int BLURPLE = Color.parseColor("#5865f2");

    /* compiled from: RepresentativeColors.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/utilities/colors/RepresentativeColors$Companion;", "", "", "BLURPLE", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: RepresentativeColors.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0002\u0018\u00002\u00060\u0001j\u0002`\u0002B\u0007¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/utilities/colors/RepresentativeColors$NoSwatchesFoundException;", "Ljava/lang/RuntimeException;", "Lkotlin/RuntimeException;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class NoSwatchesFoundException extends RuntimeException {
    }

    /* compiled from: RepresentativeColors.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/utilities/colors/RepresentativeColors$RepresentativeColorResult;", "", HookHelper.constructorName, "()V", "Failure", "Success", "Lcom/discord/utilities/colors/RepresentativeColors$RepresentativeColorResult$Success;", "Lcom/discord/utilities/colors/RepresentativeColors$RepresentativeColorResult$Failure;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class RepresentativeColorResult {

        /* compiled from: RepresentativeColors.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u001e\u0010\u0007\u001a\u00020\u00002\f\b\u0002\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001d\u0010\u0006\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/utilities/colors/RepresentativeColors$RepresentativeColorResult$Failure;", "Lcom/discord/utilities/colors/RepresentativeColors$RepresentativeColorResult;", "Ljava/lang/Exception;", "Lkotlin/Exception;", "component1", "()Ljava/lang/Exception;", "exception", "copy", "(Ljava/lang/Exception;)Lcom/discord/utilities/colors/RepresentativeColors$RepresentativeColorResult$Failure;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Exception;", "getException", HookHelper.constructorName, "(Ljava/lang/Exception;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Failure extends RepresentativeColorResult {
            private final Exception exception;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Failure(Exception exc) {
                super(null);
                m.checkNotNullParameter(exc, "exception");
                this.exception = exc;
            }

            public static /* synthetic */ Failure copy$default(Failure failure, Exception exc, int i, Object obj) {
                if ((i & 1) != 0) {
                    exc = failure.exception;
                }
                return failure.copy(exc);
            }

            public final Exception component1() {
                return this.exception;
            }

            public final Failure copy(Exception exc) {
                m.checkNotNullParameter(exc, "exception");
                return new Failure(exc);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Failure) && m.areEqual(this.exception, ((Failure) obj).exception);
                }
                return true;
            }

            public final Exception getException() {
                return this.exception;
            }

            public int hashCode() {
                Exception exc = this.exception;
                if (exc != null) {
                    return exc.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Failure(exception=");
                R.append(this.exception);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: RepresentativeColors.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0011\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0003\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/utilities/colors/RepresentativeColors$RepresentativeColorResult$Success;", "Lcom/discord/utilities/colors/RepresentativeColors$RepresentativeColorResult;", "", "component1", "()I", ModelAuditLogEntry.CHANGE_KEY_COLOR, "copy", "(I)Lcom/discord/utilities/colors/RepresentativeColors$RepresentativeColorResult$Success;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getColor", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Success extends RepresentativeColorResult {
            private final int color;

            public Success(@ColorInt int i) {
                super(null);
                this.color = i;
            }

            public static /* synthetic */ Success copy$default(Success success, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = success.color;
                }
                return success.copy(i);
            }

            public final int component1() {
                return this.color;
            }

            public final Success copy(@ColorInt int i) {
                return new Success(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Success) && this.color == ((Success) obj).color;
                }
                return true;
            }

            public final int getColor() {
                return this.color;
            }

            public int hashCode() {
                return this.color;
            }

            public String toString() {
                return a.A(a.R("Success(color="), this.color, ")");
            }
        }

        private RepresentativeColorResult() {
        }

        public /* synthetic */ RepresentativeColorResult(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    private final int getColorDistance(int i, int i2) {
        int i3 = ((i >> 16) & 255) - ((i2 >> 16) & 255);
        int i4 = ((i >> 8) & 255) - ((i2 >> 8) & 255);
        int i5 = (i & 255) - (i2 & 255);
        return Math.abs((i5 * i5) + (i4 * i4) + (i3 * i3));
    }

    private final List<Integer> getPrimaryColorsForBitmap(Bitmap bitmap) {
        a.b bVar = b.a.g.a.a;
        m.checkNotNullParameter(bitmap, "bitmap");
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int[] iArr = new int[width * height];
        bitmap.getPixels(iArr, 0, width, 0, 0, width, height);
        List<d> list = new b.a.g.a(new b(iArr), 2, null).e;
        if (!list.isEmpty()) {
            return d0.t.m.listOf(Integer.valueOf(list.get(0).d));
        }
        throw new NoSwatchesFoundException();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final RepresentativeColorResult getRepresentativeColor(Bitmap bitmap) {
        try {
            int alphaComponent = ColorUtils.setAlphaComponent(getPrimaryColorsForBitmap(bitmap).get(0).intValue(), 255);
            int i = Integer.MAX_VALUE;
            int width = bitmap.getWidth();
            int i2 = alphaComponent;
            for (int i3 = 0; i3 < width; i3++) {
                int height = bitmap.getHeight();
                for (int i4 = 0; i4 < height; i4++) {
                    int alphaComponent2 = ColorUtils.setAlphaComponent(bitmap.getPixel(i3, i4), 255);
                    int colorDistance = getColorDistance(alphaComponent, alphaComponent2);
                    if (colorDistance < i) {
                        i2 = alphaComponent2;
                        i = colorDistance;
                    }
                }
            }
            return new RepresentativeColorResult.Success(i2);
        } catch (Exception e) {
            return new RepresentativeColorResult.Failure(e);
        }
    }

    public final Object getRepresentativeColorAsync(Bitmap bitmap, Continuation<? super RepresentativeColorResult> continuation) {
        return f.C1(k0.a, new RepresentativeColors$getRepresentativeColorAsync$2(this, bitmap, null), continuation);
    }

    public final void handleBitmap(T t, Bitmap bitmap, String str) {
        m.checkNotNullParameter(bitmap, "bitmap");
        x0 x0Var = x0.j;
        CoroutineDispatcher coroutineDispatcher = k0.a;
        f.H0(x0Var, n.f3802b, null, new RepresentativeColors$handleBitmap$1(this, t, bitmap, str, null), 2, null);
    }

    public final Observable<Integer> observeRepresentativeColor(final T t) {
        Observable<Integer> q = this.representativeColorsSubject.F(new j0.k.b<Map<T, ? extends Integer>, Integer>() { // from class: com.discord.utilities.colors.RepresentativeColors$observeRepresentativeColor$1
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Integer call(Object obj) {
                return call((Map) ((Map) obj));
            }

            public final Integer call(Map<T, Integer> map) {
                return map.get(t);
            }
        }).q();
        m.checkNotNullExpressionValue(q, "representativeColorsSubj…  .distinctUntilChanged()");
        return q;
    }
}
