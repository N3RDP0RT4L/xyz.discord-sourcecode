package com.discord.utilities.colors;

import android.graphics.Bitmap;
import androidx.exifinterface.media.ExifInterface;
import com.discord.utilities.colors.RepresentativeColors;
import d0.l;
import d0.w.h.c;
import d0.w.i.a.e;
import d0.w.i.a.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineScope;
/* compiled from: RepresentativeColors.kt */
@e(c = "com.discord.utilities.colors.RepresentativeColors$getRepresentativeColorAsync$2", f = "RepresentativeColors.kt", l = {}, m = "invokeSuspend")
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u0002\"\u0004\b\u0000\u0010\u0000*\u00020\u0001H\u008a@¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {ExifInterface.GPS_DIRECTION_TRUE, "Lkotlinx/coroutines/CoroutineScope;", "Lcom/discord/utilities/colors/RepresentativeColors$RepresentativeColorResult;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class RepresentativeColors$getRepresentativeColorAsync$2 extends k implements Function2<CoroutineScope, Continuation<? super RepresentativeColors.RepresentativeColorResult>, Object> {
    public final /* synthetic */ Bitmap $bitmap;
    public int label;
    public final /* synthetic */ RepresentativeColors this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public RepresentativeColors$getRepresentativeColorAsync$2(RepresentativeColors representativeColors, Bitmap bitmap, Continuation continuation) {
        super(2, continuation);
        this.this$0 = representativeColors;
        this.$bitmap = bitmap;
    }

    @Override // d0.w.i.a.a
    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        m.checkNotNullParameter(continuation, "completion");
        return new RepresentativeColors$getRepresentativeColorAsync$2(this.this$0, this.$bitmap, continuation);
    }

    @Override // kotlin.jvm.functions.Function2
    public final Object invoke(CoroutineScope coroutineScope, Continuation<? super RepresentativeColors.RepresentativeColorResult> continuation) {
        return ((RepresentativeColors$getRepresentativeColorAsync$2) create(coroutineScope, continuation)).invokeSuspend(Unit.a);
    }

    @Override // d0.w.i.a.a
    public final Object invokeSuspend(Object obj) {
        RepresentativeColors.RepresentativeColorResult representativeColor;
        c.getCOROUTINE_SUSPENDED();
        if (this.label == 0) {
            l.throwOnFailure(obj);
            representativeColor = this.this$0.getRepresentativeColor(this.$bitmap);
            return representativeColor;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
