package com.discord.utilities.viewcontroller;

import androidx.exifinterface.media.ExifInterface;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: RxViewController.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u0004\"\b\b\u0000\u0010\u0001*\u00020\u0000\"\u0004\b\u0001\u0010\u00022\u0006\u0010\u0003\u001a\u00028\u0001H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Landroid/view/View;", ExifInterface.GPS_MEASUREMENT_INTERRUPTED, ExifInterface.GPS_DIRECTION_TRUE, "it", "", "invoke", "(Ljava/lang/Object;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class RxViewController$bind$1 extends o implements Function1<T, Unit> {
    public final /* synthetic */ RxViewController this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public RxViewController$bind$1(RxViewController rxViewController) {
        super(1);
        this.this$0 = rxViewController;
    }

    @Override // kotlin.jvm.functions.Function1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(T t) {
        this.this$0.configureView(t);
    }
}
