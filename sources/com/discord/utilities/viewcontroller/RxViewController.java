package com.discord.utilities.viewcontroller;

import andhook.lib.HookHelper;
import android.view.View;
import androidx.annotation.MainThread;
import androidx.exifinterface.media.ExifInterface;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import j0.k.b;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Ref$ObjectRef;
import rx.Observable;
import rx.Subscription;
/* compiled from: RxViewController.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0011\b&\u0018\u0000*\b\b\u0000\u0010\u0002*\u00020\u0001*\u0004\b\u0001\u0010\u00032\u00020\u0004B\u000f\u0012\u0006\u0010\u0005\u001a\u00028\u0000¢\u0006\u0004\b\u001a\u0010\u001bJ?\u0010\f\u001a\u00020\u000b\"\u0004\b\u0002\u0010\u00032\u0006\u0010\u0005\u001a\u00020\u00012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00020\u00062\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00020\t0\bH\u0002¢\u0006\u0004\b\f\u0010\rJ\u0017\u0010\u000f\u001a\u00020\t2\u0006\u0010\u000e\u001a\u00028\u0001H&¢\u0006\u0004\b\u000f\u0010\u0010J\u0015\u0010\u0011\u001a\b\u0012\u0004\u0012\u00028\u00010\u0006H&¢\u0006\u0004\b\u0011\u0010\u0012J\u000f\u0010\u0013\u001a\u00020\tH\u0007¢\u0006\u0004\b\u0013\u0010\u0014R\u001c\u0010\u0005\u001a\u00028\u00008\u0004@\u0004X\u0084\u0004¢\u0006\f\n\u0004\b\u0005\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017R\u0018\u0010\u0018\u001a\u0004\u0018\u00010\u000b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019¨\u0006\u001c"}, d2 = {"Lcom/discord/utilities/viewcontroller/RxViewController;", "Landroid/view/View;", ExifInterface.GPS_MEASUREMENT_INTERRUPTED, ExifInterface.GPS_DIRECTION_TRUE, "", "view", "Lrx/Observable;", "observable", "Lkotlin/Function1;", "", "onNext", "Lrx/Subscription;", "connectViewRx", "(Landroid/view/View;Lrx/Observable;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;", "viewState", "configureView", "(Ljava/lang/Object;)V", "observeState", "()Lrx/Observable;", "bind", "()V", "Landroid/view/View;", "getView", "()Landroid/view/View;", Traits.Payment.Type.SUBSCRIPTION, "Lrx/Subscription;", HookHelper.constructorName, "(Landroid/view/View;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class RxViewController<V extends View, T> {
    private Subscription subscription;
    private final V view;

    public RxViewController(V v) {
        m.checkNotNullParameter(v, "view");
        this.view = v;
    }

    private final <T> Subscription connectViewRx(final View view, Observable<T> observable, Function1<? super T, Unit> function1) {
        Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        ref$ObjectRef.element = null;
        Observable<T> a02 = observable.a0(new ViewDetachedFromWindowObservable(view).observe());
        m.checkNotNullExpressionValue(a02, "observable\n        .take…servable(view).observe())");
        Observable<T> x2 = ObservableExtensionsKt.ui(ObservableExtensionsKt.computationLatest(a02)).x(new b<T, Boolean>() { // from class: com.discord.utilities.viewcontroller.RxViewController$connectViewRx$1
            /* JADX WARN: Can't rename method to resolve collision */
            @Override // j0.k.b
            public final Boolean call(T t) {
                return Boolean.valueOf(view.isAttachedToWindow());
            }
        });
        m.checkNotNullExpressionValue(x2, "observable\n        .take…view.isAttachedToWindow }");
        ObservableExtensionsKt.appSubscribe(x2, getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new RxViewController$connectViewRx$2(ref$ObjectRef), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new RxViewController$connectViewRx$3(function1));
        T t = ref$ObjectRef.element;
        if (t == null) {
            m.throwUninitializedPropertyAccessException("subscriptionResult");
        }
        return (Subscription) t;
    }

    @MainThread
    public final void bind() {
        Subscription subscription = this.subscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        this.subscription = connectViewRx(this.view, observeState(), new RxViewController$bind$1(this));
    }

    public abstract void configureView(T t);

    public final V getView() {
        return this.view;
    }

    public abstract Observable<T> observeState();
}
