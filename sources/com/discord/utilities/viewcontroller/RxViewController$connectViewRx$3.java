package com.discord.utilities.viewcontroller;

import androidx.exifinterface.media.ExifInterface;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: RxViewController.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\b\u001a\u00020\u0005\"\u0004\b\u0000\u0010\u0000\"\b\b\u0001\u0010\u0002*\u00020\u0001\"\u0004\b\u0002\u0010\u00002\u000e\u0010\u0004\u001a\n \u0003*\u0004\u0018\u00018\u00028\u0002H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {ExifInterface.GPS_DIRECTION_TRUE, "Landroid/view/View;", ExifInterface.GPS_MEASUREMENT_INTERRUPTED, "kotlin.jvm.PlatformType", "emission", "", "invoke", "(Ljava/lang/Object;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class RxViewController$connectViewRx$3 extends o implements Function1<T, Unit> {
    public final /* synthetic */ Function1 $onNext;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public RxViewController$connectViewRx$3(Function1 function1) {
        super(1);
        this.$onNext = function1;
    }

    @Override // kotlin.jvm.functions.Function1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(T t) {
        this.$onNext.invoke(t);
    }
}
