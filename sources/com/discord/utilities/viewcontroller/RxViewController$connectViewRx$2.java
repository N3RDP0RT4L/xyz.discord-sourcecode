package com.discord.utilities.viewcontroller;

import androidx.exifinterface.media.ExifInterface;
import com.discord.utilities.analytics.Traits;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Ref$ObjectRef;
import rx.Subscription;
/* compiled from: RxViewController.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\b\u001a\u00020\u0005\"\u0004\b\u0000\u0010\u0000\"\b\b\u0001\u0010\u0002*\u00020\u0001\"\u0004\b\u0002\u0010\u00002\u0006\u0010\u0004\u001a\u00020\u0003H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {ExifInterface.GPS_DIRECTION_TRUE, "Landroid/view/View;", ExifInterface.GPS_MEASUREMENT_INTERRUPTED, "Lrx/Subscription;", Traits.Payment.Type.SUBSCRIPTION, "", "invoke", "(Lrx/Subscription;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class RxViewController$connectViewRx$2 extends o implements Function1<Subscription, Unit> {
    public final /* synthetic */ Ref$ObjectRef $subscriptionResult;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public RxViewController$connectViewRx$2(Ref$ObjectRef ref$ObjectRef) {
        super(1);
        this.$subscriptionResult = ref$ObjectRef;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Subscription subscription) {
        invoke2(subscription);
        return Unit.a;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Subscription subscription) {
        m.checkNotNullParameter(subscription, Traits.Payment.Type.SUBSCRIPTION);
        this.$subscriptionResult.element = subscription;
    }
}
