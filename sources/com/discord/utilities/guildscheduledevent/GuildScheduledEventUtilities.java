package com.discord.utilities.guildscheduledevent;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.fragment.app.Fragment;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.permission.Permission;
import com.discord.api.role.GuildRole;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreVoiceParticipants;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventUrlUtils;
import com.discord.widgets.voice.model.CallModel;
import d0.t.h0;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: GuildScheduledEventUtilities.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\u0018\u0000 \u00042\u00020\u0001:\u0001\u0004B\u0007¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0005"}, d2 = {"Lcom/discord/utilities/guildscheduledevent/GuildScheduledEventUtilities;", "", HookHelper.constructorName, "()V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildScheduledEventUtilities {
    public static final String ANALYTICS_SOURCE = "Guild Events";
    public static final Companion Companion = new Companion(null);
    public static final String QUERY_PARAM_NAME = "event";

    /* compiled from: GuildScheduledEventUtilities.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0084\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b:\u0010;J5\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\t2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\u000b\u0010\fJ=\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u000e\u001a\u00020\r2\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\u0011\u0010\u0012J-\u0010\u0013\u001a\u00020\u00102\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\u0013\u0010\u0014JQ\u0010\u001b\u001a\u00020\u00102\u000e\u0010\u0016\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00152\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0018\u001a\u00020\u00172\b\b\u0002\u0010\u001a\u001a\u00020\u00192\b\b\u0002\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\u001b\u0010\u001cJ\u001d\u0010\u001d\u001a\u00020\u00102\u000e\u0010\u0016\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0015¢\u0006\u0004\b\u001d\u0010\u001eJ=\u0010\u001f\u001a\u00020\u00102\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u000e\u0010\u0016\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00152\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\u001f\u0010 J%\u0010$\u001a\u00020\u00102\u0006\u0010!\u001a\u00020\n2\u000e\u0010#\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\"¢\u0006\u0004\b$\u0010%J\u001d\u0010'\u001a\u00020\u00102\u000e\u0010&\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\"¢\u0006\u0004\b'\u0010\u001eJ1\u0010,\u001a\u0004\u0018\u00010+2\u0006\u0010)\u001a\u00020(2\u000e\u0010*\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\"2\b\b\u0002\u0010\u0018\u001a\u00020\u0017¢\u0006\u0004\b,\u0010-J?\u00104\u001a\u0002032\u0006\u0010.\u001a\u00020\u00102\u0006\u00100\u001a\u00020/2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\b\u0010!\u001a\u0004\u0018\u00010\n2\n\u00102\u001a\u00060\u0002j\u0002`1¢\u0006\u0004\b4\u00105R\u0016\u00107\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b7\u00108R\u0016\u00109\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b9\u00108¨\u0006<"}, d2 = {"Lcom/discord/utilities/guildscheduledevent/GuildScheduledEventUtilities$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/stores/StoreChannels;", "channelsStore", "Lcom/discord/stores/StorePermissions;", "permissionsStore", "", "Lcom/discord/api/channel/Channel;", "getGuildScheduledEventCreatableChannelsForGuild", "(JLcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;)Ljava/util/List;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lrx/Observable;", "", "observeCanCreateAnyEvent", "(JLcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;)Lrx/Observable;", "canCreateAnyEvent", "(JLcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;)Z", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/stores/StoreGuilds;", "guildsStore", "Lcom/discord/stores/StoreUser;", "usersStore", "canShareEvent", "(Ljava/lang/Long;JLcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StorePermissions;)Z", "isEventViewableByEveryone", "(Ljava/lang/Long;)Z", "canStartEvent", "(JLjava/lang/Long;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;)Z", "channel", "Lcom/discord/api/permission/PermissionBit;", "channelPermissions", "canStartEventInChannel", "(Lcom/discord/api/channel/Channel;Ljava/lang/Long;)Z", "guildPermissions", "canCreateExternalEvent", "Lcom/discord/widgets/voice/model/CallModel;", "callModel", "myPermissions", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "getGuildScheduledEventToEndForCall", "(Lcom/discord/widgets/voice/model/CallModel;Ljava/lang/Long;Lcom/discord/stores/StoreGuilds;)Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "canShare", "Landroidx/fragment/app/Fragment;", "fragment", "Lcom/discord/primitives/GuildScheduledEventId;", "guildEventId", "", "launchInvite", "(ZLandroidx/fragment/app/Fragment;JLcom/discord/api/channel/Channel;J)V", "", "ANALYTICS_SOURCE", "Ljava/lang/String;", "QUERY_PARAM_NAME", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ boolean canCreateAnyEvent$default(Companion companion, long j, StoreChannels storeChannels, StorePermissions storePermissions, int i, Object obj) {
            if ((i & 2) != 0) {
                storeChannels = StoreStream.Companion.getChannels();
            }
            if ((i & 4) != 0) {
                storePermissions = StoreStream.Companion.getPermissions();
            }
            return companion.canCreateAnyEvent(j, storeChannels, storePermissions);
        }

        public static /* synthetic */ boolean canShareEvent$default(Companion companion, Long l, long j, StoreChannels storeChannels, StoreGuilds storeGuilds, StoreUser storeUser, StorePermissions storePermissions, int i, Object obj) {
            return companion.canShareEvent(l, j, (i & 4) != 0 ? StoreStream.Companion.getChannels() : storeChannels, (i & 8) != 0 ? StoreStream.Companion.getGuilds() : storeGuilds, (i & 16) != 0 ? StoreStream.Companion.getUsers() : storeUser, (i & 32) != 0 ? StoreStream.Companion.getPermissions() : storePermissions);
        }

        public static /* synthetic */ boolean canStartEvent$default(Companion companion, long j, Long l, StoreChannels storeChannels, StorePermissions storePermissions, int i, Object obj) {
            if ((i & 4) != 0) {
                storeChannels = StoreStream.Companion.getChannels();
            }
            StoreChannels storeChannels2 = storeChannels;
            if ((i & 8) != 0) {
                storePermissions = StoreStream.Companion.getPermissions();
            }
            return companion.canStartEvent(j, l, storeChannels2, storePermissions);
        }

        private final List<Channel> getGuildScheduledEventCreatableChannelsForGuild(long j, StoreChannels storeChannels, StorePermissions storePermissions) {
            Map<Long, Channel> channelsForGuild = storeChannels.getChannelsForGuild(j);
            Map<Long, Long> permissionsByChannel = storePermissions.getPermissionsByChannel();
            Collection<Channel> values = channelsForGuild.values();
            ArrayList arrayList = new ArrayList();
            for (Object obj : values) {
                Channel channel = (Channel) obj;
                if (GuildScheduledEventUtilities.Companion.canStartEventInChannel(channel, (Long) a.c(channel, permissionsByChannel))) {
                    arrayList.add(obj);
                }
            }
            return arrayList;
        }

        public static /* synthetic */ List getGuildScheduledEventCreatableChannelsForGuild$default(Companion companion, long j, StoreChannels storeChannels, StorePermissions storePermissions, int i, Object obj) {
            if ((i & 2) != 0) {
                storeChannels = StoreStream.Companion.getChannels();
            }
            if ((i & 4) != 0) {
                storePermissions = StoreStream.Companion.getPermissions();
            }
            return companion.getGuildScheduledEventCreatableChannelsForGuild(j, storeChannels, storePermissions);
        }

        public static /* synthetic */ GuildScheduledEvent getGuildScheduledEventToEndForCall$default(Companion companion, CallModel callModel, Long l, StoreGuilds storeGuilds, int i, Object obj) {
            if ((i & 4) != 0) {
                storeGuilds = StoreStream.Companion.getGuilds();
            }
            return companion.getGuildScheduledEventToEndForCall(callModel, l, storeGuilds);
        }

        public static /* synthetic */ Observable observeCanCreateAnyEvent$default(Companion companion, long j, ObservationDeck observationDeck, StoreChannels storeChannels, StorePermissions storePermissions, int i, Object obj) {
            if ((i & 2) != 0) {
                observationDeck = ObservationDeckProvider.get();
            }
            ObservationDeck observationDeck2 = observationDeck;
            if ((i & 4) != 0) {
                storeChannels = StoreStream.Companion.getChannels();
            }
            StoreChannels storeChannels2 = storeChannels;
            if ((i & 8) != 0) {
                storePermissions = StoreStream.Companion.getPermissions();
            }
            return companion.observeCanCreateAnyEvent(j, observationDeck2, storeChannels2, storePermissions);
        }

        public final boolean canCreateAnyEvent(long j, StoreChannels storeChannels, StorePermissions storePermissions) {
            m.checkNotNullParameter(storeChannels, "channelsStore");
            m.checkNotNullParameter(storePermissions, "permissionsStore");
            Long l = storePermissions.getGuildPermissions().get(Long.valueOf(j));
            if (l == null) {
                return false;
            }
            if (PermissionUtils.can(Permission.MANAGE_EVENTS, Long.valueOf(l.longValue()))) {
                return true;
            }
            return !getGuildScheduledEventCreatableChannelsForGuild(j, storeChannels, storePermissions).isEmpty();
        }

        public final boolean canCreateExternalEvent(Long l) {
            return PermissionUtils.can(Permission.MANAGE_EVENTS, l);
        }

        public final boolean canShareEvent(Long l, long j, StoreChannels storeChannels, StoreGuilds storeGuilds, StoreUser storeUser, StorePermissions storePermissions) {
            GuildMember member;
            m.checkNotNullParameter(storeChannels, "channelsStore");
            m.checkNotNullParameter(storeGuilds, "guildsStore");
            m.checkNotNullParameter(storeUser, "usersStore");
            m.checkNotNullParameter(storePermissions, "permissionsStore");
            if (l == null) {
                return PermissionUtils.can(1L, storePermissions.getGuildPermissions().get(Long.valueOf(j)));
            }
            Channel channel = storeChannels.getChannel(l.longValue());
            if (channel == null || !PermissionUtils.can(1L, storePermissions.getPermissionsByChannel().get(l))) {
                return false;
            }
            PermissionUtils permissionUtils = PermissionUtils.INSTANCE;
            Map<Long, GuildRole> map = (Map) a.u0(channel, storeGuilds.getRoles());
            if (map == null) {
                map = h0.emptyMap();
            }
            return permissionUtils.canEveryoneRole(Permission.VIEW_CHANNEL, channel, map) && (member = storeGuilds.getMember(channel.f(), storeUser.getMe().getId())) != null && !member.getPending();
        }

        public final boolean canStartEvent(long j, Long l, StoreChannels storeChannels, StorePermissions storePermissions) {
            m.checkNotNullParameter(storeChannels, "channelsStore");
            m.checkNotNullParameter(storePermissions, "permissionsStore");
            if (l == null) {
                Long l2 = storePermissions.getGuildPermissions().get(Long.valueOf(j));
                if (l2 != null) {
                    return PermissionUtils.can(Permission.MANAGE_EVENTS, Long.valueOf(l2.longValue()));
                }
                return false;
            }
            Channel channel = storeChannels.getChannel(l.longValue());
            Long l3 = storePermissions.getPermissionsByChannel().get(l);
            if (channel == null) {
                return false;
            }
            return canStartEventInChannel(channel, l3);
        }

        public final boolean canStartEventInChannel(Channel channel, Long l) {
            m.checkNotNullParameter(channel, "channel");
            if (!PermissionUtils.can(8589935616L, l)) {
                return false;
            }
            if (ChannelUtils.z(channel)) {
                return PermissionUtils.can(Permission.START_STAGE_EVENT, l);
            }
            if (ChannelUtils.E(channel)) {
                return PermissionUtils.can(Permission.START_VOICE_EVENT, l);
            }
            return false;
        }

        public final GuildScheduledEvent getGuildScheduledEventToEndForCall(CallModel callModel, Long l, StoreGuilds storeGuilds) {
            GuildScheduledEvent liveEvent;
            m.checkNotNullParameter(callModel, "callModel");
            m.checkNotNullParameter(storeGuilds, "guildsStore");
            Channel channel = callModel.getChannel();
            Guild guild = storeGuilds.getGuild(channel.f());
            if (guild == null || (liveEvent = GuildScheduledEventUtilitiesKt.getLiveEvent(callModel.getGuildScheduledEvents())) == null || !canStartEventInChannel(channel, l)) {
                return null;
            }
            Map map = (Map) a.d(guild, storeGuilds.getRoles());
            for (Map.Entry<Long, StoreVoiceParticipants.VoiceUser> entry : callModel.getParticipants().entrySet()) {
                long longValue = entry.getKey().longValue();
                if (!entry.getValue().isMe() && !canStartEventInChannel(channel, Long.valueOf(PermissionUtils.computeNonThreadPermissions(longValue, guild.getId(), guild.getOwnerId(), entry.getValue().getGuildMember(), map, channel.s())))) {
                    return null;
                }
            }
            return liveEvent;
        }

        public final boolean isEventViewableByEveryone(Long l) {
            if (l == null) {
                return true;
            }
            StoreStream.Companion companion = StoreStream.Companion;
            Channel channel = companion.getChannels().getChannel(l.longValue());
            if (channel != null) {
                PermissionUtils permissionUtils = PermissionUtils.INSTANCE;
                Map<Long, GuildRole> map = (Map) a.u0(channel, companion.getGuilds().getRoles());
                if (map == null) {
                    map = h0.emptyMap();
                }
                if (permissionUtils.canEveryoneRole(Permission.VIEW_CHANNEL, channel, map)) {
                    return true;
                }
            }
            return false;
        }

        public final void launchInvite(boolean z2, Fragment fragment, long j, Channel channel, long j2) {
            m.checkNotNullParameter(fragment, "fragment");
            if (z2) {
                Observable<Channel> y2 = StoreStream.Companion.getChannels().observeDefaultChannel(j).y();
                m.checkNotNullExpressionValue(y2, "StoreStream.getChannels(…dId)\n            .first()");
                ObservableExtensionsKt.appSubscribe(y2, fragment.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new GuildScheduledEventUtilities$Companion$launchInvite$1(fragment, channel, j2));
                return;
            }
            Context context = fragment.getContext();
            if (context != null) {
                CharSequence eventDetailsUrl = GuildScheduledEventUrlUtils.INSTANCE.getEventDetailsUrl(j, j2);
                m.checkNotNullExpressionValue(context, "context");
                b.a.d.m.c(context, eventDetailsUrl, 0, 4);
            }
        }

        public final Observable<Boolean> observeCanCreateAnyEvent(long j, ObservationDeck observationDeck, StoreChannels storeChannels, StorePermissions storePermissions) {
            m.checkNotNullParameter(observationDeck, "observationDeck");
            m.checkNotNullParameter(storeChannels, "channelsStore");
            m.checkNotNullParameter(storePermissions, "permissionsStore");
            return ObservationDeck.connectRx$default(observationDeck, new ObservationDeck.UpdateSource[]{storeChannels, storePermissions}, false, null, null, new GuildScheduledEventUtilities$Companion$observeCanCreateAnyEvent$1(j, storeChannels, storePermissions), 14, null);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
