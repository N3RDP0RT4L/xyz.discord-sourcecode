package com.discord.utilities.guildscheduledevent;

import android.content.Context;
import android.text.format.DateUtils;
import b.a.k.b;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.guildscheduledevent.GuildScheduledEventEntityType;
import com.discord.api.guildscheduledevent.GuildScheduledEventStatus;
import com.discord.models.guild.UserGuildMember;
import com.discord.models.user.User;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.time.TimeUtils;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventModel;
import d0.z.d.m;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: GuildScheduledEventUtilities.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000d\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u001a\u0019\u0010\u0002\u001a\u0004\u0018\u00010\u0001*\b\u0012\u0004\u0012\u00020\u00010\u0000¢\u0006\u0004\b\u0002\u0010\u0003\u001a\u0017\u0010\u0005\u001a\u00020\u0004*\b\u0012\u0004\u0012\u00020\u00010\u0000¢\u0006\u0004\b\u0005\u0010\u0006\u001a\u0011\u0010\b\u001a\u00020\u0007*\u00020\u0001¢\u0006\u0004\b\b\u0010\t\u001a\u001d\u0010\b\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\b\u0010\u000e\u001a\u0019\u0010\u0012\u001a\u00020\u0011*\u00020\u00012\u0006\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b\u0012\u0010\u0013\u001a-\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0015\u001a\u00020\u00142\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u0012\u0010\u0016\u001a/\u0010\u0018\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0017\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u0018\u0010\u0019\u001a\u001b\u0010\u001a\u001a\u00020\u0011*\u00020\n2\u0006\u0010\u0010\u001a\u00020\u000fH\u0002¢\u0006\u0004\b\u001a\u0010\u001b\u001a\u0011\u0010\u001d\u001a\u00020\u001c*\u00020\n¢\u0006\u0004\b\u001d\u0010\u001e\u001a\u001b\u0010 \u001a\u00020\u0004*\u00020\n2\u0006\u0010\u001f\u001a\u00020\nH\u0002¢\u0006\u0004\b \u0010!\u001a'\u0010'\u001a\u0004\u0018\u00010&*\u00020\u00012\b\b\u0002\u0010#\u001a\u00020\"2\b\b\u0002\u0010%\u001a\u00020$¢\u0006\u0004\b'\u0010(\u001a'\u0010'\u001a\u0004\u0018\u00010&*\u00020)2\b\b\u0002\u0010#\u001a\u00020\"2\b\b\u0002\u0010%\u001a\u00020$¢\u0006\u0004\b'\u0010*\u001a\u0011\u0010+\u001a\u00020\u0004*\u00020\u0001¢\u0006\u0004\b+\u0010,\"\u0016\u0010-\u001a\u00020\u001c8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b-\u0010.¨\u0006/"}, d2 = {"", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "getLiveEvent", "(Ljava/util/Collection;)Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "", "hasLiveEvent", "(Ljava/util/Collection;)Z", "Lcom/discord/utilities/guildscheduledevent/GuildScheduledEventTiming;", "getEventTiming", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;)Lcom/discord/utilities/guildscheduledevent/GuildScheduledEventTiming;", "", "startTimeMillis", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventStatus;", "status", "(JLcom/discord/api/guildscheduledevent/GuildScheduledEventStatus;)Lcom/discord/utilities/guildscheduledevent/GuildScheduledEventTiming;", "Landroid/content/Context;", "context", "", "getEventStartingTimeString", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;Landroid/content/Context;)Ljava/lang/CharSequence;", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;", "entityType", "(Landroid/content/Context;JLcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;Lcom/discord/api/guildscheduledevent/GuildScheduledEventStatus;)Ljava/lang/CharSequence;", "endTimeMillis", "getEventEndingTimeString", "(Landroid/content/Context;JJLcom/discord/api/guildscheduledevent/GuildScheduledEventStatus;)Ljava/lang/CharSequence;", "getNonRelativeTimeString", "(JLandroid/content/Context;)Ljava/lang/CharSequence;", "", "minutesRelativeToTime", "(J)I", "otherTimeMillis", "isSameDay", "(JJ)Z", "Lcom/discord/stores/StoreGuilds;", "guildsStore", "Lcom/discord/stores/StoreUser;", "usersStore", "Lcom/discord/models/guild/UserGuildMember;", "getCreatorUserGuildMember", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUser;)Lcom/discord/models/guild/UserGuildMember;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;", "(Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventModel;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUser;)Lcom/discord/models/guild/UserGuildMember;", "canRsvp", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;)Z", "DATE_FORMAT_FLAGS", "I", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildScheduledEventUtilitiesKt {
    public static final int DATE_FORMAT_FLAGS = 101139;

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;
        public static final /* synthetic */ int[] $EnumSwitchMapping$2;
        public static final /* synthetic */ int[] $EnumSwitchMapping$3;

        static {
            GuildScheduledEventEntityType.values();
            int[] iArr = new int[5];
            $EnumSwitchMapping$0 = iArr;
            iArr[GuildScheduledEventEntityType.EXTERNAL.ordinal()] = 1;
            GuildScheduledEventTiming.values();
            int[] iArr2 = new int[7];
            $EnumSwitchMapping$1 = iArr2;
            GuildScheduledEventTiming guildScheduledEventTiming = GuildScheduledEventTiming.LIVE;
            iArr2[guildScheduledEventTiming.ordinal()] = 1;
            iArr2[GuildScheduledEventTiming.EXPIRED.ordinal()] = 2;
            iArr2[GuildScheduledEventTiming.NOW.ordinal()] = 3;
            GuildScheduledEventTiming guildScheduledEventTiming2 = GuildScheduledEventTiming.SOON;
            iArr2[guildScheduledEventTiming2.ordinal()] = 4;
            GuildScheduledEventTiming guildScheduledEventTiming3 = GuildScheduledEventTiming.TODAY;
            iArr2[guildScheduledEventTiming3.ordinal()] = 5;
            GuildScheduledEventTiming guildScheduledEventTiming4 = GuildScheduledEventTiming.TOMORROW;
            iArr2[guildScheduledEventTiming4.ordinal()] = 6;
            GuildScheduledEventTiming.values();
            int[] iArr3 = new int[7];
            $EnumSwitchMapping$2 = iArr3;
            iArr3[guildScheduledEventTiming.ordinal()] = 1;
            iArr3[guildScheduledEventTiming2.ordinal()] = 2;
            iArr3[guildScheduledEventTiming3.ordinal()] = 3;
            GuildScheduledEventTiming.values();
            int[] iArr4 = new int[7];
            $EnumSwitchMapping$3 = iArr4;
            iArr4[guildScheduledEventTiming4.ordinal()] = 1;
            iArr4[GuildScheduledEventTiming.LATER.ordinal()] = 2;
        }
    }

    public static final boolean canRsvp(GuildScheduledEvent guildScheduledEvent) {
        m.checkNotNullParameter(guildScheduledEvent, "$this$canRsvp");
        return (getEventTiming(guildScheduledEvent) == GuildScheduledEventTiming.LIVE || guildScheduledEvent.m() == GuildScheduledEventStatus.COMPLETED) ? false : true;
    }

    public static final UserGuildMember getCreatorUserGuildMember(GuildScheduledEvent guildScheduledEvent, StoreGuilds storeGuilds, StoreUser storeUser) {
        m.checkNotNullParameter(guildScheduledEvent, "$this$getCreatorUserGuildMember");
        m.checkNotNullParameter(storeGuilds, "guildsStore");
        m.checkNotNullParameter(storeUser, "usersStore");
        Long c = guildScheduledEvent.c();
        if (c == null) {
            return null;
        }
        long longValue = c.longValue();
        User user = storeUser.getUsers(d0.t.m.listOf(Long.valueOf(longValue)), false).get(Long.valueOf(longValue));
        if (user != null) {
            return new UserGuildMember(user, storeGuilds.getMember(guildScheduledEvent.h(), longValue));
        }
        return null;
    }

    public static /* synthetic */ UserGuildMember getCreatorUserGuildMember$default(GuildScheduledEvent guildScheduledEvent, StoreGuilds storeGuilds, StoreUser storeUser, int i, Object obj) {
        if ((i & 1) != 0) {
            storeGuilds = StoreStream.Companion.getGuilds();
        }
        if ((i & 2) != 0) {
            storeUser = StoreStream.Companion.getUsers();
        }
        return getCreatorUserGuildMember(guildScheduledEvent, storeGuilds, storeUser);
    }

    public static final CharSequence getEventEndingTimeString(Context context, long j, long j2, GuildScheduledEventStatus guildScheduledEventStatus) {
        CharSequence b2;
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(guildScheduledEventStatus, "status");
        if (!isSameDay(j, j2)) {
            return null;
        }
        if (DateUtils.isToday(j2)) {
            int ordinal = getEventTiming(j, guildScheduledEventStatus).ordinal();
            if (ordinal != 2 && ordinal != 3 && ordinal != 5) {
                return null;
            }
            b2 = b.b(context, R.string.ending_on_date, new Object[]{DateUtils.formatDateTime(context, j2, DATE_FORMAT_FLAGS)}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b2;
        }
        int ordinal2 = getEventTiming(j, guildScheduledEventStatus).ordinal();
        if (ordinal2 == 0 || ordinal2 == 1) {
            return DateUtils.formatDateTime(context, j2, DATE_FORMAT_FLAGS);
        }
        return null;
    }

    public static final CharSequence getEventStartingTimeString(GuildScheduledEvent guildScheduledEvent, Context context) {
        m.checkNotNullParameter(guildScheduledEvent, "$this$getEventStartingTimeString");
        m.checkNotNullParameter(context, "context");
        return getEventStartingTimeString(context, guildScheduledEvent.l().g(), guildScheduledEvent.f(), guildScheduledEvent.m());
    }

    public static final GuildScheduledEventTiming getEventTiming(GuildScheduledEvent guildScheduledEvent) {
        m.checkNotNullParameter(guildScheduledEvent, "$this$getEventTiming");
        return getEventTiming(guildScheduledEvent.l().g(), guildScheduledEvent.m());
    }

    public static final GuildScheduledEvent getLiveEvent(Collection<GuildScheduledEvent> collection) {
        Object obj;
        boolean z2;
        m.checkNotNullParameter(collection, "$this$getLiveEvent");
        Iterator<T> it = collection.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (getEventTiming((GuildScheduledEvent) obj) == GuildScheduledEventTiming.LIVE) {
                z2 = true;
                continue;
            } else {
                z2 = false;
                continue;
            }
            if (z2) {
                break;
            }
        }
        return (GuildScheduledEvent) obj;
    }

    private static final CharSequence getNonRelativeTimeString(long j, Context context) {
        String formatDateTime = DateUtils.formatDateTime(context, j, DATE_FORMAT_FLAGS);
        m.checkNotNullExpressionValue(formatDateTime, "DateUtils.formatDateTime… this, DATE_FORMAT_FLAGS)");
        return formatDateTime;
    }

    public static final boolean hasLiveEvent(Collection<GuildScheduledEvent> collection) {
        m.checkNotNullParameter(collection, "$this$hasLiveEvent");
        return getLiveEvent(collection) != null;
    }

    private static final boolean isSameDay(long j, long j2) {
        Calendar calendar = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        m.checkNotNullExpressionValue(calendar, "eventCalendar");
        calendar.setTimeInMillis(j);
        m.checkNotNullExpressionValue(calendar2, "nowCalendar");
        calendar2.setTimeInMillis(j2);
        return calendar.get(1) == calendar2.get(1) && calendar.get(2) == calendar2.get(2) && calendar.get(5) == calendar2.get(5);
    }

    public static final int minutesRelativeToTime(long j) {
        TimeUtils timeUtils = TimeUtils.INSTANCE;
        int minutesFromMillis = timeUtils.getMinutesFromMillis(j - ClockFactory.get().currentTimeMillis());
        return minutesFromMillis != 0 ? minutesFromMillis : timeUtils.getMinutesFromMillis(ClockFactory.get().currentTimeMillis() - j) * (-1);
    }

    public static final CharSequence getEventStartingTimeString(Context context, long j, GuildScheduledEventEntityType guildScheduledEventEntityType, GuildScheduledEventStatus guildScheduledEventStatus) {
        Set set;
        CharSequence b2;
        CharSequence b3;
        CharSequence b4;
        CharSequence b5;
        CharSequence b6;
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(guildScheduledEventEntityType, "entityType");
        m.checkNotNullParameter(guildScheduledEventStatus, "status");
        GuildScheduledEventTiming eventTiming = getEventTiming(j, guildScheduledEventStatus);
        Objects.requireNonNull(GuildScheduledEventStatus.Companion);
        set = GuildScheduledEventStatus.DONE;
        if (set.contains(guildScheduledEventStatus)) {
            return getNonRelativeTimeString(j, context);
        }
        switch (eventTiming.ordinal()) {
            case 1:
            case 2:
                b2 = b.b(context, R.string.guild_event_date_at_time, new Object[]{DateUtils.getRelativeTimeSpanString(j, ClockFactory.get().currentTimeMillis(), 86400000L), DateUtils.formatDateTime(context, j, DATE_FORMAT_FLAGS)}, (r4 & 4) != 0 ? b.C0034b.j : null);
                return b2;
            case 3:
                b3 = b.b(context, R.string.starting_in_minutes, new Object[]{Integer.valueOf(minutesRelativeToTime(j))}, (r4 & 4) != 0 ? b.C0034b.j : null);
                return b3;
            case 4:
            case 6:
                b4 = b.b(context, R.string.starting_soon, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                return b4;
            case 5:
                if (guildScheduledEventEntityType.ordinal() != 3) {
                    b6 = b.b(context, R.string.stage_channel_live_now, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                    return b6;
                }
                b5 = b.b(context, R.string.active_now, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                return b5;
            default:
                return getNonRelativeTimeString(j, context);
        }
    }

    public static final GuildScheduledEventTiming getEventTiming(long j, GuildScheduledEventStatus guildScheduledEventStatus) {
        m.checkNotNullParameter(guildScheduledEventStatus, "status");
        if (guildScheduledEventStatus == GuildScheduledEventStatus.ACTIVE) {
            return GuildScheduledEventTiming.LIVE;
        }
        int minutesRelativeToTime = minutesRelativeToTime(j);
        if (minutesRelativeToTime <= -60) {
            return GuildScheduledEventTiming.EXPIRED;
        }
        if (minutesRelativeToTime <= 0) {
            return GuildScheduledEventTiming.NOW;
        }
        if (minutesRelativeToTime <= 15) {
            return GuildScheduledEventTiming.SOON;
        }
        return DateUtils.isToday(j) ? GuildScheduledEventTiming.TODAY : DateUtils.isToday(j - 86400000) ? GuildScheduledEventTiming.TOMORROW : GuildScheduledEventTiming.LATER;
    }

    public static /* synthetic */ UserGuildMember getCreatorUserGuildMember$default(GuildScheduledEventModel guildScheduledEventModel, StoreGuilds storeGuilds, StoreUser storeUser, int i, Object obj) {
        if ((i & 1) != 0) {
            storeGuilds = StoreStream.Companion.getGuilds();
        }
        if ((i & 2) != 0) {
            storeUser = StoreStream.Companion.getUsers();
        }
        return getCreatorUserGuildMember(guildScheduledEventModel, storeGuilds, storeUser);
    }

    public static final UserGuildMember getCreatorUserGuildMember(GuildScheduledEventModel guildScheduledEventModel, StoreGuilds storeGuilds, StoreUser storeUser) {
        m.checkNotNullParameter(guildScheduledEventModel, "$this$getCreatorUserGuildMember");
        m.checkNotNullParameter(storeGuilds, "guildsStore");
        m.checkNotNullParameter(storeUser, "usersStore");
        Long creatorId = guildScheduledEventModel.getCreatorId();
        if (creatorId == null) {
            return null;
        }
        long longValue = creatorId.longValue();
        User user = storeUser.getUsers(d0.t.m.listOf(Long.valueOf(longValue)), false).get(Long.valueOf(longValue));
        if (user != null) {
            return new UserGuildMember(user, storeGuilds.getMember(guildScheduledEventModel.getGuildId(), longValue));
        }
        return null;
    }
}
