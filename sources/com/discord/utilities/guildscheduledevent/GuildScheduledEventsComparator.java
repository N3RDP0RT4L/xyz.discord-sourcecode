package com.discord.utilities.guildscheduledevent;

import andhook.lib.HookHelper;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import java.util.Comparator;
import kotlin.Metadata;
/* compiled from: GuildScheduledEventUtilities.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u0012\u0012\u0004\u0012\u00020\u00020\u0001j\b\u0012\u0004\u0012\u00020\u0002`\u0003B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ#\u0010\u0007\u001a\u00020\u00062\b\u0010\u0004\u001a\u0004\u0018\u00010\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u0002H\u0016¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/utilities/guildscheduledevent/GuildScheduledEventsComparator;", "Ljava/util/Comparator;", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "Lkotlin/Comparator;", "o1", "o2", "", "compare", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;)I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildScheduledEventsComparator implements Comparator<GuildScheduledEvent> {
    public static final GuildScheduledEventsComparator INSTANCE = new GuildScheduledEventsComparator();

    private GuildScheduledEventsComparator() {
    }

    /* JADX WARN: Code restructure failed: missing block: B:11:0x0017, code lost:
        if ((r7 != null ? r7.m() : null) != r2) goto L12;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public int compare(com.discord.api.guildscheduledevent.GuildScheduledEvent r6, com.discord.api.guildscheduledevent.GuildScheduledEvent r7) {
        /*
            r5 = this;
            r0 = 0
            if (r6 == 0) goto L8
            com.discord.api.guildscheduledevent.GuildScheduledEventStatus r1 = r6.m()
            goto L9
        L8:
            r1 = r0
        L9:
            com.discord.api.guildscheduledevent.GuildScheduledEventStatus r2 = com.discord.api.guildscheduledevent.GuildScheduledEventStatus.ACTIVE
            r3 = 1
            r4 = -1
            if (r1 != r2) goto L1b
            if (r7 == 0) goto L16
            com.discord.api.guildscheduledevent.GuildScheduledEventStatus r1 = r7.m()
            goto L17
        L16:
            r1 = r0
        L17:
            if (r1 == r2) goto L1b
        L19:
            r3 = -1
            goto L46
        L1b:
            if (r6 == 0) goto L22
            com.discord.api.guildscheduledevent.GuildScheduledEventStatus r1 = r6.m()
            goto L23
        L22:
            r1 = r0
        L23:
            if (r1 == r2) goto L2e
            if (r7 == 0) goto L2b
            com.discord.api.guildscheduledevent.GuildScheduledEventStatus r0 = r7.m()
        L2b:
            if (r0 != r2) goto L2e
            goto L46
        L2e:
            if (r6 == 0) goto L3f
            if (r7 == 0) goto L3f
            com.discord.api.utcdatetime.UtcDateTime r6 = r6.l()
            com.discord.api.utcdatetime.UtcDateTime r7 = r7.l()
            int r3 = r6.compareTo(r7)
            goto L46
        L3f:
            if (r6 != 0) goto L42
            goto L46
        L42:
            if (r7 != 0) goto L45
            goto L19
        L45:
            r3 = 0
        L46:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.guildscheduledevent.GuildScheduledEventsComparator.compare(com.discord.api.guildscheduledevent.GuildScheduledEvent, com.discord.api.guildscheduledevent.GuildScheduledEvent):int");
    }
}
