package com.discord.utilities.testing;

import d0.g0.t;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: TestUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class TestUtilsKt$IS_JUNIT_TEST$2 extends o implements Function0<Boolean> {
    public static final TestUtilsKt$IS_JUNIT_TEST$2 INSTANCE = new TestUtilsKt$IS_JUNIT_TEST$2();

    public TestUtilsKt$IS_JUNIT_TEST$2() {
        super(0);
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final Boolean invoke2() {
        Thread currentThread = Thread.currentThread();
        m.checkNotNullExpressionValue(currentThread, "Thread.currentThread()");
        StackTraceElement[] stackTrace = currentThread.getStackTrace();
        m.checkNotNullExpressionValue(stackTrace, "Thread.currentThread().stackTrace");
        for (StackTraceElement stackTraceElement : stackTrace) {
            m.checkNotNullExpressionValue(stackTraceElement, "it");
            String className = stackTraceElement.getClassName();
            m.checkNotNullExpressionValue(className, "it.className");
            if (t.startsWith$default(className, "org.junit", false, 2, null)) {
                return 1;
            }
        }
        return null;
    }
}
