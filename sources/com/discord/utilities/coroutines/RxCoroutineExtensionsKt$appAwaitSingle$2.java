package com.discord.utilities.coroutines;

import android.content.Context;
import androidx.exifinterface.media.ExifInterface;
import com.discord.utilities.error.Error;
import d0.l;
import d0.w.h.c;
import d0.w.i.a.e;
import d0.w.i.a.k;
import d0.z.d.m;
import java.lang.ref.WeakReference;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlinx.coroutines.CoroutineScope;
import rx.Observable;
/* compiled from: RxCoroutineExtensions.kt */
@e(c = "com.discord.utilities.coroutines.RxCoroutineExtensionsKt$appAwaitSingle$2", f = "RxCoroutineExtensions.kt", l = {35, 40}, m = "invokeSuspend")
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00028\u0000\"\u0004\b\u0000\u0010\u0000*\u00020\u0001H\u008a@¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {ExifInterface.GPS_DIRECTION_TRUE, "Lkotlinx/coroutines/CoroutineScope;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class RxCoroutineExtensionsKt$appAwaitSingle$2 extends k implements Function2<CoroutineScope, Continuation<? super T>, Object> {
    public final /* synthetic */ Context $context;
    public final /* synthetic */ Boolean $suppressToast;
    public final /* synthetic */ Observable $this_appAwaitSingle;
    private /* synthetic */ Object L$0;
    public Object L$1;
    public int label;

    /* compiled from: RxCoroutineExtensions.kt */
    @e(c = "com.discord.utilities.coroutines.RxCoroutineExtensionsKt$appAwaitSingle$2$1", f = "RxCoroutineExtensions.kt", l = {}, m = "invokeSuspend")
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u0002\"\u0004\b\u0000\u0010\u0000*\u00020\u0001H\u008a@¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {ExifInterface.GPS_DIRECTION_TRUE, "Lkotlinx/coroutines/CoroutineScope;", "", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.utilities.coroutines.RxCoroutineExtensionsKt$appAwaitSingle$2$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends k implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
        public final /* synthetic */ Throwable $e;
        public final /* synthetic */ Ref$ObjectRef $errorTag;
        public final /* synthetic */ Ref$ObjectRef $weakContext;
        public int label;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Throwable th, Ref$ObjectRef ref$ObjectRef, Ref$ObjectRef ref$ObjectRef2, Continuation continuation) {
            super(2, continuation);
            this.$e = th;
            this.$errorTag = ref$ObjectRef;
            this.$weakContext = ref$ObjectRef2;
        }

        @Override // d0.w.i.a.a
        public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
            m.checkNotNullParameter(continuation, "completion");
            return new AnonymousClass1(this.$e, this.$errorTag, this.$weakContext, continuation);
        }

        @Override // kotlin.jvm.functions.Function2
        public final Object invoke(CoroutineScope coroutineScope, Continuation<? super Unit> continuation) {
            return ((AnonymousClass1) create(coroutineScope, continuation)).invokeSuspend(Unit.a);
        }

        @Override // d0.w.i.a.a
        public final Object invokeSuspend(Object obj) {
            c.getCOROUTINE_SUSPENDED();
            if (this.label == 0) {
                l.throwOnFailure(obj);
                Error.handle(this.$e, (String) this.$errorTag.element, null, (Context) ((WeakReference) this.$weakContext.element).get(), RxCoroutineExtensionsKt$appAwaitSingle$2.this.$suppressToast);
                return Unit.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public RxCoroutineExtensionsKt$appAwaitSingle$2(Observable observable, Context context, Boolean bool, Continuation continuation) {
        super(2, continuation);
        this.$this_appAwaitSingle = observable;
        this.$context = context;
        this.$suppressToast = bool;
    }

    @Override // d0.w.i.a.a
    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        m.checkNotNullParameter(continuation, "completion");
        RxCoroutineExtensionsKt$appAwaitSingle$2 rxCoroutineExtensionsKt$appAwaitSingle$2 = new RxCoroutineExtensionsKt$appAwaitSingle$2(this.$this_appAwaitSingle, this.$context, this.$suppressToast, continuation);
        rxCoroutineExtensionsKt$appAwaitSingle$2.L$0 = obj;
        return rxCoroutineExtensionsKt$appAwaitSingle$2;
    }

    @Override // kotlin.jvm.functions.Function2
    public final Object invoke(CoroutineScope coroutineScope, Object obj) {
        return ((RxCoroutineExtensionsKt$appAwaitSingle$2) create(coroutineScope, (Continuation) obj)).invokeSuspend(Unit.a);
    }

    /* JADX WARN: Code restructure failed: missing block: B:16:0x005c, code lost:
        if (r5 != null) goto L18;
     */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:27:0x009a A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:28:0x009b  */
    /* JADX WARN: Type inference failed for: r4v1, types: [T, java.lang.ref.WeakReference] */
    @Override // d0.w.i.a.a
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.lang.Object invokeSuspend(java.lang.Object r12) {
        /*
            r11 = this;
            java.lang.Object r0 = d0.w.h.c.getCOROUTINE_SUSPENDED()
            int r1 = r11.label
            r2 = 2
            r3 = 1
            if (r1 == 0) goto L30
            if (r1 == r3) goto L1f
            if (r1 == r2) goto L16
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r12.<init>(r0)
            throw r12
        L16:
            java.lang.Object r0 = r11.L$0
            com.discord.utilities.error.Error r0 = (com.discord.utilities.error.Error) r0
            d0.l.throwOnFailure(r12)
            goto L9c
        L1f:
            java.lang.Object r1 = r11.L$1
            kotlin.jvm.internal.Ref$ObjectRef r1 = (kotlin.jvm.internal.Ref$ObjectRef) r1
            java.lang.Object r3 = r11.L$0
            kotlin.jvm.internal.Ref$ObjectRef r3 = (kotlin.jvm.internal.Ref$ObjectRef) r3
            d0.l.throwOnFailure(r12)     // Catch: java.lang.Throwable -> L2b
            goto L78
        L2b:
            r12 = move-exception
            r7 = r12
            r8 = r1
            r9 = r3
            goto L7d
        L30:
            d0.l.throwOnFailure(r12)
            java.lang.Object r12 = r11.L$0
            kotlinx.coroutines.CoroutineScope r12 = (kotlinx.coroutines.CoroutineScope) r12
            kotlin.jvm.internal.Ref$ObjectRef r1 = new kotlin.jvm.internal.Ref$ObjectRef
            r1.<init>()
            java.lang.ref.WeakReference r4 = new java.lang.ref.WeakReference
            android.content.Context r5 = r11.$context
            r4.<init>(r5)
            r1.element = r4
            kotlin.jvm.internal.Ref$ObjectRef r4 = new kotlin.jvm.internal.Ref$ObjectRef
            r4.<init>()
            kotlin.coroutines.CoroutineContext r5 = r12.getCoroutineContext()
            com.discord.utilities.coroutines.AppErrorTag$Key r6 = com.discord.utilities.coroutines.AppErrorTag.Key.INSTANCE
            kotlin.coroutines.CoroutineContext$Element r5 = r5.get(r6)
            com.discord.utilities.coroutines.AppErrorTag r5 = (com.discord.utilities.coroutines.AppErrorTag) r5
            if (r5 == 0) goto L5f
            java.lang.String r5 = r5.getErrorTag()
            if (r5 == 0) goto L5f
            goto L67
        L5f:
            java.lang.Class r12 = r12.getClass()
            java.lang.String r5 = r12.getSimpleName()
        L67:
            r4.element = r5
            rx.Observable r12 = r11.$this_appAwaitSingle     // Catch: java.lang.Throwable -> L79
            r11.L$0 = r1     // Catch: java.lang.Throwable -> L79
            r11.L$1 = r4     // Catch: java.lang.Throwable -> L79
            r11.label = r3     // Catch: java.lang.Throwable -> L79
            java.lang.Object r12 = com.discord.utilities.rx.RxCoroutineUtilsKt.awaitSingle(r12, r11)     // Catch: java.lang.Throwable -> L79
            if (r12 != r0) goto L78
            return r0
        L78:
            return r12
        L79:
            r12 = move-exception
            r7 = r12
            r9 = r1
            r8 = r4
        L7d:
            com.discord.utilities.error.Error r12 = com.discord.utilities.error.Error.create(r7)
            kotlinx.coroutines.CoroutineDispatcher r1 = s.a.k0.a
            s.a.l1 r1 = s.a.a.n.f3802b
            com.discord.utilities.coroutines.RxCoroutineExtensionsKt$appAwaitSingle$2$1 r3 = new com.discord.utilities.coroutines.RxCoroutineExtensionsKt$appAwaitSingle$2$1
            r10 = 0
            r5 = r3
            r6 = r11
            r5.<init>(r7, r8, r9, r10)
            r11.L$0 = r12
            r4 = 0
            r11.L$1 = r4
            r11.label = r2
            java.lang.Object r1 = b.i.a.f.e.o.f.C1(r1, r3, r11)
            if (r1 != r0) goto L9b
            return r0
        L9b:
            r0 = r12
        L9c:
            com.discord.utilities.error.AppCancellationException r12 = new com.discord.utilities.error.AppCancellationException
            java.lang.String r1 = "error"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            java.lang.Throwable r1 = r0.getThrowable()
            java.lang.String r2 = "Unexpected error"
            r12.<init>(r2, r1, r0)
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.coroutines.RxCoroutineExtensionsKt$appAwaitSingle$2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
