package com.discord.utilities;

import androidx.core.app.NotificationCompat;
import com.discord.api.channel.Channel;
import d0.t.n;
import d0.t.o;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
/* compiled from: ShareUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0002*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00000\u00002\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "Lcom/discord/api/channel/Channel;", "kotlin.jvm.PlatformType", "it", "Lcom/discord/utilities/ChannelShortcutInfo;", NotificationCompat.CATEGORY_CALL, "(Ljava/util/List;)Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ShareUtils$updateDirectShareTargets$1<T, R> implements b<List<? extends Channel>, List<? extends ChannelShortcutInfo>> {
    public static final ShareUtils$updateDirectShareTargets$1 INSTANCE = new ShareUtils$updateDirectShareTargets$1();

    @Override // j0.k.b
    public /* bridge */ /* synthetic */ List<? extends ChannelShortcutInfo> call(List<? extends Channel> list) {
        return call2((List<Channel>) list);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final List<ChannelShortcutInfo> call2(List<Channel> list) {
        m.checkNotNullExpressionValue(list, "it");
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
        int i = 0;
        for (T t : list) {
            i++;
            if (i < 0) {
                n.throwIndexOverflow();
            }
            arrayList.add(new ChannelShortcutInfo((Channel) t, i, false));
        }
        return arrayList;
    }
}
