package com.discord.utilities.views;

import andhook.lib.HookHelper;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.google.android.material.appbar.AppBarLayout;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ContentResizingCoordinatorLayout.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0011\b\u0016\u0012\u0006\u0010\u0016\u001a\u00020\u0015¢\u0006\u0004\b\u0017\u0010\u0018B\u001d\b\u0016\u0012\u0006\u0010\u0016\u001a\u00020\u0015\u0012\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u0019¢\u0006\u0004\b\u0017\u0010\u001bB'\b\u0016\u0012\u0006\u0010\u0016\u001a\u00020\u0015\u0012\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u0019\u0012\b\b\u0002\u0010\u001c\u001a\u00020\u0002¢\u0006\u0004\b\u0017\u0010\u001dJ7\u0010\t\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\u000b\u001a\u00020\b2\u0006\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0002H\u0014¢\u0006\u0004\b\u000b\u0010\fR\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0011\u001a\u00020\u00108\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0013\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014¨\u0006\u001e"}, d2 = {"Lcom/discord/utilities/views/ContentResizingCoordinatorLayout;", "Landroidx/coordinatorlayout/widget/CoordinatorLayout;", "", "appBarVerticalOffset", "measuredAppBarHeight", "measuredContainerHeight", "widthMeasureSpec", "heightMeasureSpec", "", "updateContentLayoutParams", "(IIIII)V", "onMeasure", "(II)V", "Landroid/view/View;", "content", "Landroid/view/View;", "Lcom/google/android/material/appbar/AppBarLayout;", "appBarLayout", "Lcom/google/android/material/appbar/AppBarLayout;", "currentVerticalOffset", "I", "Landroid/content/Context;", "context", HookHelper.constructorName, "(Landroid/content/Context;)V", "Landroid/util/AttributeSet;", "attrs", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ContentResizingCoordinatorLayout extends CoordinatorLayout {
    private AppBarLayout appBarLayout;
    private View content;
    private int currentVerticalOffset;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ContentResizingCoordinatorLayout(Context context) {
        super(context);
        m.checkNotNullParameter(context, "context");
    }

    public static final /* synthetic */ AppBarLayout access$getAppBarLayout$p(ContentResizingCoordinatorLayout contentResizingCoordinatorLayout) {
        AppBarLayout appBarLayout = contentResizingCoordinatorLayout.appBarLayout;
        if (appBarLayout == null) {
            m.throwUninitializedPropertyAccessException("appBarLayout");
        }
        return appBarLayout;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateContentLayoutParams(int i, int i2, int i3, int i4, int i5) {
        int i6 = i2 + i;
        View view = this.content;
        if (view == null) {
            m.throwUninitializedPropertyAccessException("content");
        }
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = i3 - i6;
        View view2 = this.content;
        if (view2 == null) {
            m.throwUninitializedPropertyAccessException("content");
        }
        view2.setLayoutParams(layoutParams);
        View view3 = this.content;
        if (view3 == null) {
            m.throwUninitializedPropertyAccessException("content");
        }
        measureChild(view3, i4, i5);
    }

    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View
    public void onMeasure(final int i, final int i2) {
        int size = View.MeasureSpec.getSize(i2);
        if (this.appBarLayout == null && getChildCount() >= 2) {
            View childAt = getChildAt(0);
            Objects.requireNonNull(childAt, "null cannot be cast to non-null type com.google.android.material.appbar.AppBarLayout");
            this.appBarLayout = (AppBarLayout) childAt;
            View childAt2 = getChildAt(1);
            Objects.requireNonNull(childAt2, "null cannot be cast to non-null type android.view.View");
            this.content = childAt2;
            AppBarLayout appBarLayout = this.appBarLayout;
            if (appBarLayout == null) {
                m.throwUninitializedPropertyAccessException("appBarLayout");
            }
            measureChild(appBarLayout, i, i2);
            AppBarLayout appBarLayout2 = this.appBarLayout;
            if (appBarLayout2 == null) {
                m.throwUninitializedPropertyAccessException("appBarLayout");
            }
            appBarLayout2.offsetTopAndBottom(0);
            AppBarLayout appBarLayout3 = this.appBarLayout;
            if (appBarLayout3 == null) {
                m.throwUninitializedPropertyAccessException("appBarLayout");
            }
            updateContentLayoutParams(0, appBarLayout3.getMeasuredHeight(), size, i, i2);
            AppBarLayout appBarLayout4 = this.appBarLayout;
            if (appBarLayout4 == null) {
                m.throwUninitializedPropertyAccessException("appBarLayout");
            }
            appBarLayout4.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() { // from class: com.discord.utilities.views.ContentResizingCoordinatorLayout$onMeasure$2
                @Override // com.google.android.material.appbar.AppBarLayout.OnOffsetChangedListener, com.google.android.material.appbar.AppBarLayout.BaseOnOffsetChangedListener
                public final void onOffsetChanged(AppBarLayout appBarLayout5, int i3) {
                    ContentResizingCoordinatorLayout.this.currentVerticalOffset = i3;
                    ContentResizingCoordinatorLayout contentResizingCoordinatorLayout = ContentResizingCoordinatorLayout.this;
                    contentResizingCoordinatorLayout.updateContentLayoutParams(i3, ContentResizingCoordinatorLayout.access$getAppBarLayout$p(contentResizingCoordinatorLayout).getMeasuredHeight(), ContentResizingCoordinatorLayout.this.getMeasuredHeight(), i, i2);
                }
            });
            AppBarLayout appBarLayout5 = this.appBarLayout;
            if (appBarLayout5 == null) {
                m.throwUninitializedPropertyAccessException("appBarLayout");
            }
            ViewExtensions.addOnHeightChangedListener(appBarLayout5, new ContentResizingCoordinatorLayout$onMeasure$3(this, i, i2));
        }
        super.onMeasure(i, i2);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ContentResizingCoordinatorLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ ContentResizingCoordinatorLayout(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ContentResizingCoordinatorLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ ContentResizingCoordinatorLayout(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }
}
