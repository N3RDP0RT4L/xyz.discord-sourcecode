package com.discord.utilities.views;

import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: ContentResizingCoordinatorLayout.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "<anonymous parameter 0>", "<anonymous parameter 1>", "", "invoke", "(II)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ContentResizingCoordinatorLayout$onMeasure$3 extends o implements Function2<Integer, Integer, Unit> {
    public final /* synthetic */ int $heightMeasureSpec;
    public final /* synthetic */ int $widthMeasureSpec;
    public final /* synthetic */ ContentResizingCoordinatorLayout this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ContentResizingCoordinatorLayout$onMeasure$3(ContentResizingCoordinatorLayout contentResizingCoordinatorLayout, int i, int i2) {
        super(2);
        this.this$0 = contentResizingCoordinatorLayout;
        this.$widthMeasureSpec = i;
        this.$heightMeasureSpec = i2;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(Integer num, Integer num2) {
        invoke(num.intValue(), num2.intValue());
        return Unit.a;
    }

    public final void invoke(int i, int i2) {
        int i3;
        ContentResizingCoordinatorLayout contentResizingCoordinatorLayout = this.this$0;
        i3 = contentResizingCoordinatorLayout.currentVerticalOffset;
        contentResizingCoordinatorLayout.updateContentLayoutParams(i3, ContentResizingCoordinatorLayout.access$getAppBarLayout$p(this.this$0).getMeasuredHeight(), this.this$0.getMeasuredHeight(), this.$widthMeasureSpec, this.$heightMeasureSpec);
    }
}
