package com.discord.utilities.views;

import andhook.lib.HookHelper;
import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AnimationUtils;
import android.widget.TableRow;
import androidx.exifinterface.media.ExifInterface;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: FloatingButtonMenuInitializer.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000W\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010$\n\u0002\u0010\b\n\u0002\b\u0007*\u0001\u0014\u0018\u00002\u00020\u0001:\u0001,B#\u0012\u0006\u0010!\u001a\u00020 \u0012\u0012\u0010'\u001a\u000e\u0012\u0004\u0012\u00020&\u0012\u0004\u0012\u00020\u00180%¢\u0006\u0004\b*\u0010+J\u001f\u0010\u0005\u001a\u00020\u00032\u000e\u0010\u0004\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J'\u0010\u000b\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u00022\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u000b\u0010\fJ!\u0010\u000e\u001a\u0004\u0018\u00010\u00032\u0006\u0010\r\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ7\u0010\u0012\u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\u00072\u001e\u0010\u0011\u001a\u0010\u0012\f\b\u0001\u0012\b\u0012\u0004\u0012\u00020\u00070\u00100\u0002\"\b\u0012\u0004\u0012\u00020\u00070\u0010H\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0015\u001a\u00020\u00142\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u0015\u0010\u0016J!\u0010\u001b\u001a\u00020\u001a2\u0006\u0010\r\u001a\u00020\u00172\b\u0010\u0019\u001a\u0004\u0018\u00010\u0018H\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ%\u0010\u001e\u001a\u00020\u00002\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u001d\u001a\u00020\u0018¢\u0006\u0004\b\u001e\u0010\u001fR\u0016\u0010!\u001a\u00020 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"R\u001c\u0010#\u001a\b\u0012\u0004\u0012\u00020\t0\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$R\"\u0010'\u001a\u000e\u0012\u0004\u0012\u00020&\u0012\u0004\u0012\u00020\u00180%8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R\u001c\u0010)\u001a\b\u0012\u0004\u0012\u00020\t0\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b)\u0010$¨\u0006-"}, d2 = {"Lcom/discord/utilities/views/FloatingButtonMenuInitializer;", "", "", "Landroid/animation/Animator;", "menuRowAnimators", "configureEntranceAnimator", "([Landroid/animation/Animator;)Landroid/animation/Animator;", "Landroid/view/ViewGroup;", "menuContainer", "Landroid/view/View;", "mainFab", "getMenuAnimators", "(Landroid/view/ViewGroup;Landroid/view/View;)[Landroid/animation/Animator;", "row", "createDefaultMenuRowAnimation", "(Landroid/view/ViewGroup;Landroid/view/View;)Landroid/animation/Animator;", "Lcom/discord/utilities/views/FloatingButtonMenuInitializer$AnimatorFactory;", "factories", "createMenuRowAnimation", "(Landroid/view/ViewGroup;[Lcom/discord/utilities/views/FloatingButtonMenuInitializer$AnimatorFactory;)Landroid/animation/Animator;", "com/discord/utilities/views/FloatingButtonMenuInitializer$getAnimatorFactoryScaleUp$1", "getAnimatorFactoryScaleUp", "(Landroid/view/View;)Lcom/discord/utilities/views/FloatingButtonMenuInitializer$getAnimatorFactoryScaleUp$1;", "Landroid/widget/TableRow;", "Landroid/view/View$OnClickListener;", "listener", "", "setFabMenuOnClickListener", "(Landroid/widget/TableRow;Landroid/view/View$OnClickListener;)V", "defaultRowOnClickListener", "initialize", "(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View$OnClickListener;)Lcom/discord/utilities/views/FloatingButtonMenuInitializer;", "Landroid/content/Context;", "context", "Landroid/content/Context;", "animatorFactoryFadeIn", "Lcom/discord/utilities/views/FloatingButtonMenuInitializer$AnimatorFactory;", "", "", "onClickListenerMap", "Ljava/util/Map;", "animatorFactorySlideUp", HookHelper.constructorName, "(Landroid/content/Context;Ljava/util/Map;)V", "AnimatorFactory", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class FloatingButtonMenuInitializer {
    private final AnimatorFactory<View> animatorFactoryFadeIn = new AnimatorFactory<View>() { // from class: com.discord.utilities.views.FloatingButtonMenuInitializer$animatorFactoryFadeIn$1
        @Override // com.discord.utilities.views.FloatingButtonMenuInitializer.AnimatorFactory
        public Animator createAnimator(View view) {
            Context context;
            m.checkNotNullParameter(view, "view");
            context = FloatingButtonMenuInitializer.this.context;
            Animator loadAnimator = AnimatorInflater.loadAnimator(context, 17498112);
            loadAnimator.setTarget(view);
            return loadAnimator;
        }
    };
    private final AnimatorFactory<View> animatorFactorySlideUp = new AnimatorFactory<View>() { // from class: com.discord.utilities.views.FloatingButtonMenuInitializer$animatorFactorySlideUp$1
        @Override // com.discord.utilities.views.FloatingButtonMenuInitializer.AnimatorFactory
        public Animator createAnimator(View view) {
            m.checkNotNullParameter(view, "view");
            return ObjectAnimator.ofFloat(view, View.TRANSLATION_Y, view.getHeight() * 1.0f, 0.0f);
        }
    };
    private final Context context;
    private final Map<Integer, View.OnClickListener> onClickListenerMap;

    /* compiled from: FloatingButtonMenuInitializer.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bf\u0018\u0000*\n\b\u0000\u0010\u0002 \u0000*\u00020\u00012\u00020\u0003J\u0019\u0010\u0006\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u0004\u001a\u00028\u0000H&¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\b"}, d2 = {"Lcom/discord/utilities/views/FloatingButtonMenuInitializer$AnimatorFactory;", "Landroid/view/View;", ExifInterface.GPS_DIRECTION_TRUE, "", "view", "Landroid/animation/Animator;", "createAnimator", "(Landroid/view/View;)Landroid/animation/Animator;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface AnimatorFactory<T extends View> {
        Animator createAnimator(T t);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public FloatingButtonMenuInitializer(Context context, Map<Integer, ? extends View.OnClickListener> map) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(map, "onClickListenerMap");
        this.context = context;
        this.onClickListenerMap = map;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Animator configureEntranceAnimator(Animator[] animatorArr) {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether((Animator[]) Arrays.copyOf(animatorArr, animatorArr.length));
        animatorSet.setInterpolator(AnimationUtils.loadInterpolator(this.context, 17563654));
        AnimatorSet duration = animatorSet.setDuration(this.context.getResources().getInteger(17694720));
        m.checkNotNullExpressionValue(duration, "animatorEntrance.setDura…n(totalDuration.toLong())");
        return duration;
    }

    private final Animator createDefaultMenuRowAnimation(ViewGroup viewGroup, View view) {
        if (viewGroup.getId() == R.id.fab_menu_main) {
            return this.animatorFactoryFadeIn.createAnimator(viewGroup);
        }
        return createMenuRowAnimation(viewGroup, this.animatorFactoryFadeIn, getAnimatorFactoryScaleUp(view), this.animatorFactorySlideUp);
    }

    private final Animator createMenuRowAnimation(ViewGroup viewGroup, AnimatorFactory<? super ViewGroup>... animatorFactoryArr) {
        ArrayList arrayList = new ArrayList(animatorFactoryArr.length);
        for (AnimatorFactory<? super ViewGroup> animatorFactory : animatorFactoryArr) {
            Animator createAnimator = animatorFactory.createAnimator(viewGroup);
            if (createAnimator != null) {
                arrayList.add(createAnimator);
            }
        }
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(arrayList);
        return animatorSet;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [com.discord.utilities.views.FloatingButtonMenuInitializer$getAnimatorFactoryScaleUp$1] */
    private final FloatingButtonMenuInitializer$getAnimatorFactoryScaleUp$1 getAnimatorFactoryScaleUp(final View view) {
        return new AnimatorFactory<ViewGroup>() { // from class: com.discord.utilities.views.FloatingButtonMenuInitializer$getAnimatorFactoryScaleUp$1
            public Animator createAnimator(ViewGroup viewGroup) {
                m.checkNotNullParameter(viewGroup, "view");
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(ObjectAnimator.ofFloat(view, View.SCALE_X, 0.0f, 1.0f), ObjectAnimator.ofFloat(view, View.SCALE_Y, 0.0f, 1.0f));
                return animatorSet;
            }
        };
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Animator[] getMenuAnimators(ViewGroup viewGroup, View view) {
        int childCount = viewGroup.getChildCount();
        Animator[] animatorArr = new Animator[childCount];
        for (int i = 0; i < childCount; i++) {
            View childAt = viewGroup.getChildAt(i);
            Objects.requireNonNull(childAt, "null cannot be cast to non-null type android.view.ViewGroup");
            final ViewGroup viewGroup2 = (ViewGroup) childAt;
            viewGroup2.setVisibility(4);
            Animator createDefaultMenuRowAnimation = createDefaultMenuRowAnimation(viewGroup2, view);
            if (createDefaultMenuRowAnimation != null) {
                createDefaultMenuRowAnimation.addListener(new AnimatorListenerAdapter() { // from class: com.discord.utilities.views.FloatingButtonMenuInitializer$getMenuAnimators$1
                    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                    public void onAnimationStart(Animator animator) {
                        m.checkNotNullParameter(animator, "animation");
                        viewGroup2.setVisibility(0);
                    }
                });
            }
            animatorArr[(childCount - 1) - i] = createDefaultMenuRowAnimation;
        }
        return animatorArr;
    }

    private final void setFabMenuOnClickListener(TableRow tableRow, View.OnClickListener onClickListener) {
        int childCount = tableRow.getChildCount();
        for (int i = 0; i < childCount; i++) {
            tableRow.getChildAt(i).setOnClickListener(onClickListener);
        }
    }

    public final FloatingButtonMenuInitializer initialize(final ViewGroup viewGroup, final View view, View.OnClickListener onClickListener) {
        m.checkNotNullParameter(viewGroup, "menuContainer");
        m.checkNotNullParameter(view, "mainFab");
        m.checkNotNullParameter(onClickListener, "defaultRowOnClickListener");
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = viewGroup.getChildAt(i);
            Objects.requireNonNull(childAt, "null cannot be cast to non-null type android.widget.TableRow");
            TableRow tableRow = (TableRow) childAt;
            tableRow.setOnClickListener(onClickListener);
            setFabMenuOnClickListener(tableRow, this.onClickListenerMap.get(Integer.valueOf(tableRow.getId())));
        }
        viewGroup.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() { // from class: com.discord.utilities.views.FloatingButtonMenuInitializer$initialize$1
            @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
            public void onGlobalLayout() {
                Animator[] menuAnimators;
                Animator configureEntranceAnimator;
                FloatingButtonMenuInitializer floatingButtonMenuInitializer = FloatingButtonMenuInitializer.this;
                menuAnimators = floatingButtonMenuInitializer.getMenuAnimators(viewGroup, view);
                configureEntranceAnimator = floatingButtonMenuInitializer.configureEntranceAnimator(menuAnimators);
                configureEntranceAnimator.start();
                viewGroup.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
        return this;
    }
}
