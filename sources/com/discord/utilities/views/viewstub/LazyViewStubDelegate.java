package com.discord.utilities.views.viewstub;

import andhook.lib.HookHelper;
import android.view.View;
import android.view.ViewStub;
import d0.g;
import d0.i;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: LazyViewStubDelegate.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \u001d2\u00020\u0001:\u0001\u001dB\u0011\b\u0002\u0012\u0006\u0010\u001a\u001a\u00020\u0019¢\u0006\u0004\b\u001b\u0010\u001cJ\u0017\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\b\u001a\u0004\u0018\u00010\u0007¢\u0006\u0004\b\b\u0010\tJ\r\u0010\n\u001a\u00020\u0007¢\u0006\u0004\b\n\u0010\tR$\u0010\r\u001a\u0010\u0012\f\u0012\n \f*\u0004\u0018\u00010\u00070\u00070\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000eR$\u0010\u0011\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u000f8F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0003\u0010\u0015R%\u0010\u0018\u001a\n \f*\u0004\u0018\u00010\u00070\u00078B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0016\u0010\u000e\u001a\u0004\b\u0017\u0010\t¨\u0006\u001e"}, d2 = {"Lcom/discord/utilities/views/viewstub/LazyViewStubDelegate;", "", "Landroid/view/View$OnClickListener;", "listener", "", "setOnClickListener", "(Landroid/view/View$OnClickListener;)V", "Landroid/view/View;", "getMaybeView", "()Landroid/view/View;", "getForceInitializedView", "Lkotlin/Lazy;", "kotlin.jvm.PlatformType", "viewField", "Lkotlin/Lazy;", "", "value", "isVisible", "()Z", "setVisible", "(Z)V", "Landroid/view/View$OnClickListener;", "view$delegate", "getView", "view", "Landroid/view/ViewStub;", "stub", HookHelper.constructorName, "(Landroid/view/ViewStub;)V", "Companion", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class LazyViewStubDelegate {
    public static final Companion Companion = new Companion(null);
    private View.OnClickListener listener;
    private final Lazy view$delegate;
    private final Lazy<View> viewField;

    /* compiled from: LazyViewStubDelegate.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ!\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/utilities/views/viewstub/LazyViewStubDelegate$Companion;", "", "Lkotlin/Function0;", "Landroid/view/ViewStub;", "getViewStub", "Lkotlin/Lazy;", "Lcom/discord/utilities/views/viewstub/LazyViewStubDelegate;", "lazyViewStub", "(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;", HookHelper.constructorName, "()V", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final Lazy<LazyViewStubDelegate> lazyViewStub(Function0<ViewStub> function0) {
            m.checkNotNullParameter(function0, "getViewStub");
            return g.lazy(i.NONE, new LazyViewStubDelegate$Companion$lazyViewStub$1(function0));
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    private LazyViewStubDelegate(ViewStub viewStub) {
        Lazy<View> lazy = g.lazy(i.NONE, new LazyViewStubDelegate$viewField$1(this, viewStub));
        this.viewField = lazy;
        this.view$delegate = lazy;
    }

    private final View getView() {
        return (View) this.view$delegate.getValue();
    }

    public final View getForceInitializedView() {
        View view = getView();
        m.checkNotNullExpressionValue(view, "view");
        return view;
    }

    public final View getMaybeView() {
        if (!this.viewField.isInitialized()) {
            return null;
        }
        return getView();
    }

    public final boolean isVisible() {
        if (this.viewField.isInitialized()) {
            View view = getView();
            m.checkNotNullExpressionValue(view, "view");
            if (view.getVisibility() == 0) {
                return true;
            }
        }
        return false;
    }

    public final void setOnClickListener(View.OnClickListener onClickListener) {
        if (!this.viewField.isInitialized()) {
            this.listener = onClickListener;
        } else {
            getView().setOnClickListener(onClickListener);
        }
    }

    public final void setVisible(boolean z2) {
        if (z2 || this.viewField.isInitialized()) {
            View view = getView();
            m.checkNotNullExpressionValue(view, "view");
            view.setVisibility(z2 ? 0 : 8);
        }
    }

    public /* synthetic */ LazyViewStubDelegate(ViewStub viewStub, DefaultConstructorMarker defaultConstructorMarker) {
        this(viewStub);
    }
}
