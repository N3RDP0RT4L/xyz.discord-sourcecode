package com.discord.utilities.views;

import andhook.lib.HookHelper;
import android.graphics.Canvas;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.solver.widgets.analyzer.BasicMeasure;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: StickyHeaderItemDecoration.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0002'(B\u000f\u0012\u0006\u0010#\u001a\u00020\"¢\u0006\u0004\b%\u0010&J\u0019\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J)\u0010\r\u001a\u00020\f2\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\u00042\b\b\u0002\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\r\u0010\u000eJ!\u0010\u0012\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u000f\u0010\u0014\u001a\u00020\fH\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u0015\u0010\u0016\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b\u0016\u0010\u0017J'\u0010\u001b\u001a\u00020\f2\u0006\u0010\u0018\u001a\u00020\u00072\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u001a\u001a\u00020\u0019H\u0016¢\u0006\u0004\b\u001b\u0010\u001cR\u0018\u0010\u001d\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u0016\u0010\u001f\u001a\u00020\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u0018\u0010!\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b!\u0010\u001eR\u0016\u0010#\u001a\u00020\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$¨\u0006)"}, d2 = {"Lcom/discord/utilities/views/StickyHeaderItemDecoration;", "Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;", "", "itemPosition", "Landroid/view/View;", "getAndBindHeaderViewForItem", "(I)Landroid/view/View;", "Landroid/graphics/Canvas;", "c", "header", "", "offsetY", "", "drawHeader", "(Landroid/graphics/Canvas;Landroid/view/View;F)V", "Landroidx/recyclerview/widget/RecyclerView;", "parent", "contactPoint", "getOverlappingView", "(Landroidx/recyclerview/widget/RecyclerView;I)Landroid/view/View;", "resetActionDownCoordinates", "()V", "blockClicks", "(Landroidx/recyclerview/widget/RecyclerView;)V", "canvas", "Landroidx/recyclerview/widget/RecyclerView$State;", "state", "onDrawOver", "(Landroid/graphics/Canvas;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$State;)V", "actionDownRawY", "Ljava/lang/Float;", "stickyHeaderBottom", "F", "actionDownRawX", "Lcom/discord/utilities/views/StickyHeaderItemDecoration$StickyHeaderAdapter;", "adapter", "Lcom/discord/utilities/views/StickyHeaderItemDecoration$StickyHeaderAdapter;", HookHelper.constructorName, "(Lcom/discord/utilities/views/StickyHeaderItemDecoration$StickyHeaderAdapter;)V", "LayoutManager", "StickyHeaderAdapter", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StickyHeaderItemDecoration extends RecyclerView.ItemDecoration {
    private Float actionDownRawX;
    private Float actionDownRawY;
    private final StickyHeaderAdapter adapter;
    private float stickyHeaderBottom;

    /* compiled from: StickyHeaderItemDecoration.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0007¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/utilities/views/StickyHeaderItemDecoration$LayoutManager;", "", "Landroid/view/ViewGroup;", "parent", "Landroid/view/View;", "view", "", "layoutHeaderView", "(Landroid/view/ViewGroup;Landroid/view/View;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class LayoutManager {
        public static final LayoutManager INSTANCE = new LayoutManager();

        private LayoutManager() {
        }

        public static final void layoutHeaderView(ViewGroup viewGroup, View view) {
            m.checkNotNullParameter(viewGroup, "parent");
            m.checkNotNullParameter(view, "view");
            int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(viewGroup.getWidth(), BasicMeasure.EXACTLY);
            int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(viewGroup.getHeight(), 0);
            view.measure(ViewGroup.getChildMeasureSpec(makeMeasureSpec, viewGroup.getPaddingRight() + viewGroup.getPaddingLeft(), view.getLayoutParams().width), ViewGroup.getChildMeasureSpec(makeMeasureSpec2, viewGroup.getPaddingBottom() + viewGroup.getPaddingTop(), view.getLayoutParams().height));
            view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        }
    }

    /* compiled from: StickyHeaderItemDecoration.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\u0019\u0010\u0004\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0003\u001a\u00020\u0002H&¢\u0006\u0004\b\u0004\u0010\u0005J\u0017\u0010\b\u001a\u00020\u00072\u0006\u0010\u0006\u001a\u00020\u0002H&¢\u0006\u0004\b\b\u0010\tJ\u0019\u0010\u000b\u001a\u0004\u0018\u00010\n2\u0006\u0010\u0006\u001a\u00020\u0002H&¢\u0006\u0004\b\u000b\u0010\f¨\u0006\r"}, d2 = {"Lcom/discord/utilities/views/StickyHeaderItemDecoration$StickyHeaderAdapter;", "", "", "itemPosition", "getHeaderPositionForItem", "(I)Ljava/lang/Integer;", ModelAuditLogEntry.CHANGE_KEY_POSITION, "", "isHeader", "(I)Z", "Landroid/view/View;", "getAndBindHeaderView", "(I)Landroid/view/View;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface StickyHeaderAdapter {
        View getAndBindHeaderView(int i);

        Integer getHeaderPositionForItem(int i);

        boolean isHeader(int i);
    }

    public StickyHeaderItemDecoration(StickyHeaderAdapter stickyHeaderAdapter) {
        m.checkNotNullParameter(stickyHeaderAdapter, "adapter");
        this.adapter = stickyHeaderAdapter;
    }

    private final void drawHeader(Canvas canvas, View view, float f) {
        canvas.save();
        if (f != 0.0f) {
            canvas.translate(0.0f, f);
        }
        this.stickyHeaderBottom = view.getBottom() - f;
        view.draw(canvas);
        canvas.restore();
    }

    public static /* synthetic */ void drawHeader$default(StickyHeaderItemDecoration stickyHeaderItemDecoration, Canvas canvas, View view, float f, int i, Object obj) {
        if ((i & 4) != 0) {
            f = 0.0f;
        }
        stickyHeaderItemDecoration.drawHeader(canvas, view, f);
    }

    private final View getAndBindHeaderViewForItem(int i) {
        Integer headerPositionForItem = this.adapter.getHeaderPositionForItem(i);
        if (headerPositionForItem == null) {
            return null;
        }
        return this.adapter.getAndBindHeaderView(headerPositionForItem.intValue());
    }

    private final View getOverlappingView(RecyclerView recyclerView, int i) {
        int childCount = recyclerView.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = recyclerView.getChildAt(i2);
            m.checkNotNullExpressionValue(childAt, "child");
            int top = childAt.getTop();
            int bottom = childAt.getBottom();
            if (top <= i && bottom > i) {
                return childAt;
            }
        }
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void resetActionDownCoordinates() {
        this.actionDownRawX = null;
        this.actionDownRawY = null;
    }

    public final void blockClicks(RecyclerView recyclerView) {
        m.checkNotNullParameter(recyclerView, "parent");
        recyclerView.addOnItemTouchListener(new RecyclerView.SimpleOnItemTouchListener() { // from class: com.discord.utilities.views.StickyHeaderItemDecoration$blockClicks$1
            /* JADX WARN: Removed duplicated region for block: B:19:0x0067  */
            @Override // androidx.recyclerview.widget.RecyclerView.SimpleOnItemTouchListener, androidx.recyclerview.widget.RecyclerView.OnItemTouchListener
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public boolean onInterceptTouchEvent(androidx.recyclerview.widget.RecyclerView r6, android.view.MotionEvent r7) {
                /*
                    r5 = this;
                    java.lang.String r0 = "recyclerView"
                    d0.z.d.m.checkNotNullParameter(r6, r0)
                    java.lang.String r0 = "event"
                    d0.z.d.m.checkNotNullParameter(r7, r0)
                    int r0 = r7.getAction()
                    r1 = 1
                    r2 = 0
                    if (r0 == 0) goto L76
                    if (r0 == r1) goto L1f
                    r6 = 3
                    if (r0 == r6) goto L19
                    goto L90
                L19:
                    com.discord.utilities.views.StickyHeaderItemDecoration r6 = com.discord.utilities.views.StickyHeaderItemDecoration.this
                    com.discord.utilities.views.StickyHeaderItemDecoration.access$resetActionDownCoordinates(r6)
                    goto L90
                L1f:
                    com.discord.utilities.views.StickyHeaderItemDecoration r0 = com.discord.utilities.views.StickyHeaderItemDecoration.this
                    java.lang.Float r0 = com.discord.utilities.views.StickyHeaderItemDecoration.access$getActionDownRawX$p(r0)
                    com.discord.utilities.views.StickyHeaderItemDecoration r3 = com.discord.utilities.views.StickyHeaderItemDecoration.this
                    java.lang.Float r3 = com.discord.utilities.views.StickyHeaderItemDecoration.access$getActionDownRawY$p(r3)
                    if (r0 == 0) goto L5f
                    if (r3 == 0) goto L5f
                    float r4 = r7.getRawX()
                    float r0 = r0.floatValue()
                    float r4 = r4 - r0
                    float r0 = java.lang.Math.abs(r4)
                    float r4 = r7.getRawY()
                    float r3 = r3.floatValue()
                    float r4 = r4 - r3
                    float r3 = java.lang.Math.abs(r4)
                    android.content.res.Resources r6 = r6.getResources()
                    r4 = 2131165344(0x7f0700a0, float:1.7944902E38)
                    int r6 = r6.getDimensionPixelSize(r4)
                    float r6 = (float) r6
                    int r0 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
                    if (r0 >= 0) goto L5f
                    int r6 = (r3 > r6 ? 1 : (r3 == r6 ? 0 : -1))
                    if (r6 >= 0) goto L5f
                    r6 = 1
                    goto L60
                L5f:
                    r6 = 0
                L60:
                    com.discord.utilities.views.StickyHeaderItemDecoration r0 = com.discord.utilities.views.StickyHeaderItemDecoration.this
                    com.discord.utilities.views.StickyHeaderItemDecoration.access$resetActionDownCoordinates(r0)
                    if (r6 == 0) goto L90
                    float r6 = r7.getY()
                    com.discord.utilities.views.StickyHeaderItemDecoration r7 = com.discord.utilities.views.StickyHeaderItemDecoration.this
                    float r7 = com.discord.utilities.views.StickyHeaderItemDecoration.access$getStickyHeaderBottom$p(r7)
                    int r6 = (r6 > r7 ? 1 : (r6 == r7 ? 0 : -1))
                    if (r6 > 0) goto L90
                    goto L91
                L76:
                    com.discord.utilities.views.StickyHeaderItemDecoration r6 = com.discord.utilities.views.StickyHeaderItemDecoration.this
                    float r0 = r7.getRawX()
                    java.lang.Float r0 = java.lang.Float.valueOf(r0)
                    com.discord.utilities.views.StickyHeaderItemDecoration.access$setActionDownRawX$p(r6, r0)
                    com.discord.utilities.views.StickyHeaderItemDecoration r6 = com.discord.utilities.views.StickyHeaderItemDecoration.this
                    float r7 = r7.getRawY()
                    java.lang.Float r7 = java.lang.Float.valueOf(r7)
                    com.discord.utilities.views.StickyHeaderItemDecoration.access$setActionDownRawY$p(r6, r7)
                L90:
                    r1 = 0
                L91:
                    return r1
                */
                throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.views.StickyHeaderItemDecoration$blockClicks$1.onInterceptTouchEvent(androidx.recyclerview.widget.RecyclerView, android.view.MotionEvent):boolean");
            }
        });
    }

    @Override // androidx.recyclerview.widget.RecyclerView.ItemDecoration
    public void onDrawOver(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
        int childAdapterPosition;
        int childAdapterPosition2;
        m.checkNotNullParameter(canvas, "canvas");
        m.checkNotNullParameter(recyclerView, "parent");
        m.checkNotNullParameter(state, "state");
        super.onDrawOver(canvas, recyclerView, state);
        View childAt = recyclerView.getChildAt(0);
        if (childAt != null && (childAdapterPosition = recyclerView.getChildAdapterPosition(childAt)) != -1) {
            View andBindHeaderViewForItem = getAndBindHeaderViewForItem(childAdapterPosition);
            float f = 0.0f;
            if (andBindHeaderViewForItem == null) {
                this.stickyHeaderBottom = 0.0f;
                return;
            }
            int top = recyclerView.getTop() + andBindHeaderViewForItem.getBottom();
            int top2 = recyclerView.getTop();
            int bottom = recyclerView.getBottom();
            if (top2 <= top && bottom > top) {
                View overlappingView = getOverlappingView(recyclerView, andBindHeaderViewForItem.getBottom());
                if (!(overlappingView == null || (childAdapterPosition2 = recyclerView.getChildAdapterPosition(overlappingView)) == -1 || !this.adapter.isHeader(childAdapterPosition2))) {
                    f = overlappingView.getTop() - andBindHeaderViewForItem.getHeight();
                }
                drawHeader(canvas, andBindHeaderViewForItem, f);
            }
        }
    }
}
