package com.discord.utilities.voice;

import com.discord.overlay.OverlayManager;
import com.discord.overlay.views.OverlayDialog;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: DiscordOverlayService.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/overlay/views/OverlayDialog;", "it", "", "invoke", "(Lcom/discord/overlay/views/OverlayDialog;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class DiscordOverlayService$createVoiceSelector$1 extends o implements Function1<OverlayDialog, Unit> {
    public final /* synthetic */ DiscordOverlayService this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public DiscordOverlayService$createVoiceSelector$1(DiscordOverlayService discordOverlayService) {
        super(1);
        this.this$0 = discordOverlayService;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(OverlayDialog overlayDialog) {
        invoke2(overlayDialog);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(OverlayDialog overlayDialog) {
        OverlayManager overlayManager;
        m.checkNotNullParameter(overlayDialog, "it");
        overlayManager = this.this$0.getOverlayManager();
        overlayManager.d(overlayDialog);
    }
}
