package com.discord.utilities.voice;

import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.widgets.stage.StageChannelNotifications;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: VoiceEngineForegroundService.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Ljava/lang/Void;", "it", "", "invoke", "(Ljava/lang/Void;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class VoiceEngineForegroundService$ackStageInvite$2 extends o implements Function1<Void, Unit> {
    public final /* synthetic */ boolean $accept;
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ VoiceEngineForegroundService this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public VoiceEngineForegroundService$ackStageInvite$2(VoiceEngineForegroundService voiceEngineForegroundService, boolean z2, long j) {
        super(1);
        this.this$0 = voiceEngineForegroundService;
        this.$accept = z2;
        this.$channelId = j;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Void r1) {
        invoke2(r1);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Void r3) {
        if (this.$accept) {
            AnalyticsTracker.INSTANCE.promotedToSpeaker(this.$channelId);
        }
        StageChannelNotifications.Notifications.InvitedToSpeak.INSTANCE.cancel(this.this$0);
    }
}
