package com.discord.utilities.voice;

import android.graphics.Rect;
import android.view.ViewGroup;
import b.a.y.w;
import com.discord.overlay.views.OverlayBubbleWrap;
import com.discord.views.OverlayMenuBubbleDialog;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: DiscordOverlayService.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/overlay/views/OverlayBubbleWrap;", "srcBubble", "invoke", "(Lcom/discord/overlay/views/OverlayBubbleWrap;)Lcom/discord/overlay/views/OverlayBubbleWrap;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class DiscordOverlayService$createVoiceBubble$1 extends o implements Function1<OverlayBubbleWrap, OverlayBubbleWrap> {
    public final /* synthetic */ String $anchorTag;
    public final /* synthetic */ DiscordOverlayService this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public DiscordOverlayService$createVoiceBubble$1(DiscordOverlayService discordOverlayService, String str) {
        super(1);
        this.this$0 = discordOverlayService;
        this.$anchorTag = str;
    }

    public final OverlayBubbleWrap invoke(OverlayBubbleWrap overlayBubbleWrap) {
        OverlayMenuBubbleDialog createMenu;
        Rect rect;
        SimpleDraweeView imageView$app_productionGoogleRelease;
        m.checkNotNullParameter(overlayBubbleWrap, "srcBubble");
        createMenu = this.this$0.createMenu();
        ViewGroup.MarginLayoutParams marginLayoutParams = null;
        w wVar = (w) (!(overlayBubbleWrap instanceof w) ? null : overlayBubbleWrap);
        if (wVar == null || (imageView$app_productionGoogleRelease = wVar.getImageView$app_productionGoogleRelease()) == null) {
            rect = new Rect();
        } else {
            ViewGroup.LayoutParams layoutParams = imageView$app_productionGoogleRelease.getLayoutParams();
            ViewGroup.MarginLayoutParams marginLayoutParams2 = layoutParams instanceof ViewGroup.MarginLayoutParams ? (ViewGroup.MarginLayoutParams) layoutParams : null;
            int i = 0;
            int i2 = marginLayoutParams2 == null ? 0 : marginLayoutParams2.leftMargin;
            ViewGroup.LayoutParams layoutParams2 = imageView$app_productionGoogleRelease.getLayoutParams();
            ViewGroup.MarginLayoutParams marginLayoutParams3 = layoutParams2 instanceof ViewGroup.MarginLayoutParams ? (ViewGroup.MarginLayoutParams) layoutParams2 : null;
            int i3 = marginLayoutParams3 == null ? 0 : marginLayoutParams3.topMargin;
            ViewGroup.LayoutParams layoutParams3 = imageView$app_productionGoogleRelease.getLayoutParams();
            ViewGroup.MarginLayoutParams marginLayoutParams4 = layoutParams3 instanceof ViewGroup.MarginLayoutParams ? (ViewGroup.MarginLayoutParams) layoutParams3 : null;
            int i4 = marginLayoutParams4 == null ? 0 : marginLayoutParams4.rightMargin;
            ViewGroup.LayoutParams layoutParams4 = imageView$app_productionGoogleRelease.getLayoutParams();
            if (layoutParams4 instanceof ViewGroup.MarginLayoutParams) {
                marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams4;
            }
            if (marginLayoutParams != null) {
                i = marginLayoutParams.bottomMargin;
            }
            rect = new Rect(i2, i3, i4, i);
        }
        ViewGroup.LayoutParams layoutParams5 = createMenu.getLinkedAnchorView().getLayoutParams();
        layoutParams5.width = (overlayBubbleWrap.getWidth() - rect.left) - rect.right;
        layoutParams5.height = overlayBubbleWrap.getHeight();
        createMenu.getLinkedAnchorView().requestLayout();
        createMenu.getLinkedAnchorView().setTag(this.$anchorTag);
        return createMenu;
    }
}
