package com.discord.utilities.voice;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.StringRes;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.rtcconnection.RtcConnection;
import com.discord.rtcconnection.audio.DiscordAudioManager;
import com.discord.stores.StoreAudioManagerV2;
import com.discord.stores.StoreMediaSettings;
import com.discord.stores.StoreRtcConnection;
import com.discord.stores.StoreVoiceChannelSelected;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableWithLeadingEdgeThrottle;
import com.discord.utilities.voice.VoiceEngineForegroundService;
import com.discord.utilities.voice.VoiceEngineServiceController;
import com.discord.widgets.voice.model.CallModel;
import d0.g;
import d0.z.d.m;
import j0.l.e.k;
import java.util.concurrent.TimeUnit;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func3;
import xyz.discord.R;
/* compiled from: VoiceEngineServiceController.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 \"2\u00020\u0001:\u0002\"#B'\u0012\u0006\u0010\u001c\u001a\u00020\u001b\u0012\u0006\u0010\u0019\u001a\u00020\u0018\u0012\u0006\u0010\u000e\u001a\u00020\r\u0012\u0006\u0010\u001f\u001a\u00020\u001e¢\u0006\u0004\b \u0010!J\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0015\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\t\u0010\nJ\r\u0010\u000b\u001a\u00020\u0004¢\u0006\u0004\b\u000b\u0010\fR\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR:\u0010\u0013\u001a&\u0012\f\u0012\n \u0012*\u0004\u0018\u00010\u00110\u0011 \u0012*\u0012\u0012\f\u0012\n \u0012*\u0004\u0018\u00010\u00110\u0011\u0018\u00010\u00100\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0016\u001a\u00020\u00158\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0019\u001a\u00020\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001c\u001a\u00020\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001d¨\u0006$"}, d2 = {"Lcom/discord/utilities/voice/VoiceEngineServiceController;", "", "Landroid/content/Context;", "context", "", "init", "(Landroid/content/Context;)V", "Landroid/content/Intent;", "permissionIntent", "startStream", "(Landroid/content/Intent;)V", "stopStream", "()V", "Lcom/discord/stores/StoreVoiceChannelSelected;", "voiceChannelSelectedStore", "Lcom/discord/stores/StoreVoiceChannelSelected;", "Lrx/Observable;", "Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;", "kotlin.jvm.PlatformType", "notificationDataObservable", "Lrx/Observable;", "Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;", "serviceBinding", "Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;", "Lcom/discord/stores/StoreMediaSettings;", "mediaSettingsStore", "Lcom/discord/stores/StoreMediaSettings;", "Lcom/discord/stores/StoreAudioManagerV2;", "audioManagerStore", "Lcom/discord/stores/StoreAudioManagerV2;", "Lcom/discord/stores/StoreRtcConnection;", "rtcConnectionStore", HookHelper.constructorName, "(Lcom/discord/stores/StoreAudioManagerV2;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreRtcConnection;)V", "Companion", "NotificationData", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class VoiceEngineServiceController {
    public static final Companion Companion = new Companion(null);
    private static final Lazy INSTANCE$delegate = g.lazy(VoiceEngineServiceController$Companion$INSTANCE$2.INSTANCE);
    private static final NotificationData NOTIFICATION_DATA_DISCONNECTED = new NotificationData(new RtcConnection.State.d(false), "", false, false, false, false, -1, null, false, false);
    private final StoreAudioManagerV2 audioManagerStore;
    private final StoreMediaSettings mediaSettingsStore;
    private final Observable<NotificationData> notificationDataObservable;
    private VoiceEngineForegroundService.Connection serviceBinding;
    private final StoreVoiceChannelSelected voiceChannelSelectedStore;

    /* compiled from: VoiceEngineServiceController.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fR\u001d\u0010\u0007\u001a\u00020\u00028F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0016\u0010\t\u001a\u00020\b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\t\u0010\n¨\u0006\r"}, d2 = {"Lcom/discord/utilities/voice/VoiceEngineServiceController$Companion;", "", "Lcom/discord/utilities/voice/VoiceEngineServiceController;", "INSTANCE$delegate", "Lkotlin/Lazy;", "getINSTANCE", "()Lcom/discord/utilities/voice/VoiceEngineServiceController;", "INSTANCE", "Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;", "NOTIFICATION_DATA_DISCONNECTED", "Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final VoiceEngineServiceController getINSTANCE() {
            Lazy lazy = VoiceEngineServiceController.INSTANCE$delegate;
            Companion companion = VoiceEngineServiceController.Companion;
            return (VoiceEngineServiceController) lazy.getValue();
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: VoiceEngineServiceController.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0010\b\n\u0002\b\u0016\b\u0082\b\u0018\u00002\u00020\u0001Bc\u0012\u0006\u0010\u0017\u001a\u00020\u0002\u0012\u0006\u0010\u0018\u001a\u00020\u0005\u0012\u0006\u0010\u0019\u001a\u00020\b\u0012\u0006\u0010\u001a\u001a\u00020\b\u0012\u0006\u0010\u001b\u001a\u00020\b\u0012\u0006\u0010\u001c\u001a\u00020\b\u0012\n\u0010\u001d\u001a\u00060\u000ej\u0002`\u000f\u0012\u000e\u0010\u001e\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`\u0012\u0012\u0006\u0010\u001f\u001a\u00020\b\u0012\u0006\u0010 \u001a\u00020\b¢\u0006\u0004\b8\u00109J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u000b\u0010\nJ\u0010\u0010\f\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\f\u0010\nJ\u0010\u0010\r\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\r\u0010\nJ\u0014\u0010\u0010\u001a\u00060\u000ej\u0002`\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0018\u0010\u0013\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u0015\u0010\nJ\u0010\u0010\u0016\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u0016\u0010\nJ\u0080\u0001\u0010!\u001a\u00020\u00002\b\b\u0002\u0010\u0017\u001a\u00020\u00022\b\b\u0002\u0010\u0018\u001a\u00020\u00052\b\b\u0002\u0010\u0019\u001a\u00020\b2\b\b\u0002\u0010\u001a\u001a\u00020\b2\b\b\u0002\u0010\u001b\u001a\u00020\b2\b\b\u0002\u0010\u001c\u001a\u00020\b2\f\b\u0002\u0010\u001d\u001a\u00060\u000ej\u0002`\u000f2\u0010\b\u0002\u0010\u001e\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`\u00122\b\b\u0002\u0010\u001f\u001a\u00020\b2\b\b\u0002\u0010 \u001a\u00020\bHÆ\u0001¢\u0006\u0004\b!\u0010\"J\u0010\u0010#\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b#\u0010\u0007J\u0010\u0010%\u001a\u00020$HÖ\u0001¢\u0006\u0004\b%\u0010&J\u001a\u0010(\u001a\u00020\b2\b\u0010'\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b(\u0010)R\u001c\u0010*\u001a\u00020$8\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b*\u0010+\u001a\u0004\b,\u0010&R\u0019\u0010\u0017\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010-\u001a\u0004\b.\u0010\u0004R\u0019\u0010\u0018\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010/\u001a\u0004\b0\u0010\u0007R\u0019\u0010\u001b\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u00101\u001a\u0004\b\u001b\u0010\nR\u0019\u0010\u001c\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u00101\u001a\u0004\b\u001c\u0010\nR\u0019\u0010\u001f\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00101\u001a\u0004\b2\u0010\nR\u001d\u0010\u001d\u001a\u00060\u000ej\u0002`\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00103\u001a\u0004\b4\u0010\u0011R\u0019\u0010\u001a\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u00101\u001a\u0004\b\u001a\u0010\nR\u0019\u0010 \u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b \u00101\u001a\u0004\b5\u0010\nR\u0019\u0010\u0019\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u00101\u001a\u0004\b\u0019\u0010\nR!\u0010\u001e\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00106\u001a\u0004\b7\u0010\u0014¨\u0006:"}, d2 = {"Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;", "", "Lcom/discord/rtcconnection/RtcConnection$State;", "component1", "()Lcom/discord/rtcconnection/RtcConnection$State;", "", "component2", "()Ljava/lang/String;", "", "component3", "()Z", "component4", "component5", "component6", "", "Lcom/discord/primitives/ChannelId;", "component7", "()J", "Lcom/discord/primitives/GuildId;", "component8", "()Ljava/lang/Long;", "component9", "component10", "rtcConnectionState", "channelName", "isSelfMuted", "isSelfDeafened", "isSelfStreaming", "isVideo", "channelId", "guildId", "proximityLockEnabled", "canSpeak", "copy", "(Lcom/discord/rtcconnection/RtcConnection$State;Ljava/lang/String;ZZZZJLjava/lang/Long;ZZ)Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "stateString", "I", "getStateString", "Lcom/discord/rtcconnection/RtcConnection$State;", "getRtcConnectionState", "Ljava/lang/String;", "getChannelName", "Z", "getProximityLockEnabled", "J", "getChannelId", "getCanSpeak", "Ljava/lang/Long;", "getGuildId", HookHelper.constructorName, "(Lcom/discord/rtcconnection/RtcConnection$State;Ljava/lang/String;ZZZZJLjava/lang/Long;ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class NotificationData {
        private final boolean canSpeak;
        private final long channelId;
        private final String channelName;
        private final Long guildId;
        private final boolean isSelfDeafened;
        private final boolean isSelfMuted;
        private final boolean isSelfStreaming;
        private final boolean isVideo;
        private final boolean proximityLockEnabled;
        private final RtcConnection.State rtcConnectionState;
        @StringRes
        private final int stateString;

        public NotificationData(RtcConnection.State state, String str, boolean z2, boolean z3, boolean z4, boolean z5, long j, Long l, boolean z6, boolean z7) {
            int i;
            m.checkNotNullParameter(state, "rtcConnectionState");
            m.checkNotNullParameter(str, "channelName");
            this.rtcConnectionState = state;
            this.channelName = str;
            this.isSelfMuted = z2;
            this.isSelfDeafened = z3;
            this.isSelfStreaming = z4;
            this.isVideo = z5;
            this.channelId = j;
            this.guildId = l;
            this.proximityLockEnabled = z6;
            this.canSpeak = z7;
            if (state instanceof RtcConnection.State.d) {
                i = R.string.connection_status_disconnected;
            } else if (m.areEqual(state, RtcConnection.State.b.a)) {
                i = R.string.connection_status_awaiting_endpoint;
            } else if (m.areEqual(state, RtcConnection.State.a.a)) {
                i = R.string.connection_status_authenticating;
            } else if (m.areEqual(state, RtcConnection.State.c.a)) {
                i = R.string.connection_status_connecting;
            } else if (m.areEqual(state, RtcConnection.State.h.a)) {
                i = R.string.connection_status_rtc_disconnected;
            } else if (m.areEqual(state, RtcConnection.State.g.a)) {
                i = R.string.connection_status_rtc_connecting;
            } else if (m.areEqual(state, RtcConnection.State.f.a)) {
                i = z4 ? R.string.connection_status_stream_self_connected : z5 ? R.string.connection_status_video_connected : R.string.connection_status_voice_connected;
            } else if (m.areEqual(state, RtcConnection.State.e.a)) {
                i = R.string.connection_status_no_route;
            } else {
                throw new NoWhenBranchMatchedException();
            }
            this.stateString = i;
        }

        public final RtcConnection.State component1() {
            return this.rtcConnectionState;
        }

        public final boolean component10() {
            return this.canSpeak;
        }

        public final String component2() {
            return this.channelName;
        }

        public final boolean component3() {
            return this.isSelfMuted;
        }

        public final boolean component4() {
            return this.isSelfDeafened;
        }

        public final boolean component5() {
            return this.isSelfStreaming;
        }

        public final boolean component6() {
            return this.isVideo;
        }

        public final long component7() {
            return this.channelId;
        }

        public final Long component8() {
            return this.guildId;
        }

        public final boolean component9() {
            return this.proximityLockEnabled;
        }

        public final NotificationData copy(RtcConnection.State state, String str, boolean z2, boolean z3, boolean z4, boolean z5, long j, Long l, boolean z6, boolean z7) {
            m.checkNotNullParameter(state, "rtcConnectionState");
            m.checkNotNullParameter(str, "channelName");
            return new NotificationData(state, str, z2, z3, z4, z5, j, l, z6, z7);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof NotificationData)) {
                return false;
            }
            NotificationData notificationData = (NotificationData) obj;
            return m.areEqual(this.rtcConnectionState, notificationData.rtcConnectionState) && m.areEqual(this.channelName, notificationData.channelName) && this.isSelfMuted == notificationData.isSelfMuted && this.isSelfDeafened == notificationData.isSelfDeafened && this.isSelfStreaming == notificationData.isSelfStreaming && this.isVideo == notificationData.isVideo && this.channelId == notificationData.channelId && m.areEqual(this.guildId, notificationData.guildId) && this.proximityLockEnabled == notificationData.proximityLockEnabled && this.canSpeak == notificationData.canSpeak;
        }

        public final boolean getCanSpeak() {
            return this.canSpeak;
        }

        public final long getChannelId() {
            return this.channelId;
        }

        public final String getChannelName() {
            return this.channelName;
        }

        public final Long getGuildId() {
            return this.guildId;
        }

        public final boolean getProximityLockEnabled() {
            return this.proximityLockEnabled;
        }

        public final RtcConnection.State getRtcConnectionState() {
            return this.rtcConnectionState;
        }

        public final int getStateString() {
            return this.stateString;
        }

        public int hashCode() {
            RtcConnection.State state = this.rtcConnectionState;
            int i = 0;
            int hashCode = (state != null ? state.hashCode() : 0) * 31;
            String str = this.channelName;
            int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
            boolean z2 = this.isSelfMuted;
            int i2 = 1;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            int i5 = (hashCode2 + i3) * 31;
            boolean z3 = this.isSelfDeafened;
            if (z3) {
                z3 = true;
            }
            int i6 = z3 ? 1 : 0;
            int i7 = z3 ? 1 : 0;
            int i8 = (i5 + i6) * 31;
            boolean z4 = this.isSelfStreaming;
            if (z4) {
                z4 = true;
            }
            int i9 = z4 ? 1 : 0;
            int i10 = z4 ? 1 : 0;
            int i11 = (i8 + i9) * 31;
            boolean z5 = this.isVideo;
            if (z5) {
                z5 = true;
            }
            int i12 = z5 ? 1 : 0;
            int i13 = z5 ? 1 : 0;
            int a = (b.a(this.channelId) + ((i11 + i12) * 31)) * 31;
            Long l = this.guildId;
            if (l != null) {
                i = l.hashCode();
            }
            int i14 = (a + i) * 31;
            boolean z6 = this.proximityLockEnabled;
            if (z6) {
                z6 = true;
            }
            int i15 = z6 ? 1 : 0;
            int i16 = z6 ? 1 : 0;
            int i17 = (i14 + i15) * 31;
            boolean z7 = this.canSpeak;
            if (!z7) {
                i2 = z7 ? 1 : 0;
            }
            return i17 + i2;
        }

        public final boolean isSelfDeafened() {
            return this.isSelfDeafened;
        }

        public final boolean isSelfMuted() {
            return this.isSelfMuted;
        }

        public final boolean isSelfStreaming() {
            return this.isSelfStreaming;
        }

        public final boolean isVideo() {
            return this.isVideo;
        }

        public String toString() {
            StringBuilder R = a.R("NotificationData(rtcConnectionState=");
            R.append(this.rtcConnectionState);
            R.append(", channelName=");
            R.append(this.channelName);
            R.append(", isSelfMuted=");
            R.append(this.isSelfMuted);
            R.append(", isSelfDeafened=");
            R.append(this.isSelfDeafened);
            R.append(", isSelfStreaming=");
            R.append(this.isSelfStreaming);
            R.append(", isVideo=");
            R.append(this.isVideo);
            R.append(", channelId=");
            R.append(this.channelId);
            R.append(", guildId=");
            R.append(this.guildId);
            R.append(", proximityLockEnabled=");
            R.append(this.proximityLockEnabled);
            R.append(", canSpeak=");
            return a.M(R, this.canSpeak, ")");
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            StoreMediaSettings.SelfMuteFailure.values();
            int[] iArr = new int[1];
            $EnumSwitchMapping$0 = iArr;
            iArr[StoreMediaSettings.SelfMuteFailure.CANNOT_USE_VAD.ordinal()] = 1;
        }
    }

    public VoiceEngineServiceController(StoreAudioManagerV2 storeAudioManagerV2, StoreMediaSettings storeMediaSettings, StoreVoiceChannelSelected storeVoiceChannelSelected, StoreRtcConnection storeRtcConnection) {
        m.checkNotNullParameter(storeAudioManagerV2, "audioManagerStore");
        m.checkNotNullParameter(storeMediaSettings, "mediaSettingsStore");
        m.checkNotNullParameter(storeVoiceChannelSelected, "voiceChannelSelectedStore");
        m.checkNotNullParameter(storeRtcConnection, "rtcConnectionStore");
        this.audioManagerStore = storeAudioManagerV2;
        this.mediaSettingsStore = storeMediaSettings;
        this.voiceChannelSelectedStore = storeVoiceChannelSelected;
        Observable<R> Y = storeRtcConnection.getConnectionState().Y(new j0.k.b<RtcConnection.StateChange, Observable<? extends NotificationData>>() { // from class: com.discord.utilities.voice.VoiceEngineServiceController$notificationDataObservable$1
            public final Observable<? extends VoiceEngineServiceController.NotificationData> call(RtcConnection.StateChange stateChange) {
                StoreVoiceChannelSelected storeVoiceChannelSelected2;
                VoiceEngineServiceController.NotificationData notificationData;
                final RtcConnection.State state = stateChange.a;
                if (m.areEqual(state, RtcConnection.State.h.a)) {
                    notificationData = VoiceEngineServiceController.NOTIFICATION_DATA_DISCONNECTED;
                    return new k(notificationData);
                }
                storeVoiceChannelSelected2 = VoiceEngineServiceController.this.voiceChannelSelectedStore;
                return (Observable<R>) storeVoiceChannelSelected2.observeSelectedVoiceChannelId().Y(new j0.k.b<Long, Observable<? extends VoiceEngineServiceController.NotificationData>>() { // from class: com.discord.utilities.voice.VoiceEngineServiceController$notificationDataObservable$1.1
                    public final Observable<? extends VoiceEngineServiceController.NotificationData> call(Long l) {
                        StoreMediaSettings storeMediaSettings2;
                        StoreAudioManagerV2 storeAudioManagerV22;
                        storeMediaSettings2 = VoiceEngineServiceController.this.mediaSettingsStore;
                        Observable<StoreMediaSettings.VoiceConfiguration> voiceConfig = storeMediaSettings2.getVoiceConfig();
                        storeAudioManagerV22 = VoiceEngineServiceController.this.audioManagerStore;
                        Observable<StoreAudioManagerV2.State> observeAudioManagerState = storeAudioManagerV22.observeAudioManagerState();
                        CallModel.Companion companion = CallModel.Companion;
                        m.checkNotNullExpressionValue(l, "selectedVoiceChannelId");
                        return ObservableWithLeadingEdgeThrottle.combineLatest(voiceConfig, observeAudioManagerState, companion.get(l.longValue()), new Func3<StoreMediaSettings.VoiceConfiguration, StoreAudioManagerV2.State, CallModel, VoiceEngineServiceController.NotificationData>() { // from class: com.discord.utilities.voice.VoiceEngineServiceController.notificationDataObservable.1.1.1
                            public final VoiceEngineServiceController.NotificationData call(StoreMediaSettings.VoiceConfiguration voiceConfiguration, StoreAudioManagerV2.State state2, CallModel callModel) {
                                Channel channel;
                                Channel channel2;
                                Channel channel3;
                                boolean z2 = false;
                                boolean z3 = callModel != null && callModel.isVideoCall();
                                boolean z4 = !z3 && (state2.getActiveAudioDevice() == DiscordAudioManager.DeviceTypes.EARPIECE);
                                boolean z5 = callModel == null || !callModel.isSuppressed();
                                RtcConnection.State state3 = state;
                                String c = (callModel == null || (channel3 = callModel.getChannel()) == null) ? null : ChannelUtils.c(channel3);
                                if (c == null) {
                                    c = "";
                                }
                                boolean isSelfMuted = voiceConfiguration.isSelfMuted();
                                boolean isSelfDeafened = voiceConfiguration.isSelfDeafened();
                                boolean z6 = callModel != null && callModel.isStreaming();
                                long h = (callModel == null || (channel2 = callModel.getChannel()) == null) ? -1L : channel2.h();
                                Long valueOf = (callModel == null || (channel = callModel.getChannel()) == null) ? null : Long.valueOf(channel.f());
                                if (valueOf != null && valueOf.longValue() > 0) {
                                    z2 = true;
                                }
                                if (!z2) {
                                    valueOf = null;
                                }
                                return new VoiceEngineServiceController.NotificationData(state3, c, isSelfMuted, isSelfDeafened, z6, z3, h, valueOf, z4, z5);
                            }
                        }, 300L, TimeUnit.MILLISECONDS);
                    }
                });
            }
        });
        m.checkNotNullExpressionValue(Y, "rtcConnectionStore\n     …            }\n          }");
        this.notificationDataObservable = ObservableExtensionsKt.computationLatest(Y).q();
    }

    public static final /* synthetic */ VoiceEngineForegroundService.Connection access$getServiceBinding$p(VoiceEngineServiceController voiceEngineServiceController) {
        VoiceEngineForegroundService.Connection connection = voiceEngineServiceController.serviceBinding;
        if (connection == null) {
            m.throwUninitializedPropertyAccessException("serviceBinding");
        }
        return connection;
    }

    public final void init(Context context) {
        m.checkNotNullParameter(context, "context");
        VoiceEngineForegroundService.Companion companion = VoiceEngineForegroundService.Companion;
        companion.setOnDisconnect(new VoiceEngineServiceController$init$1(this));
        companion.setOnToggleSelfDeafen(new VoiceEngineServiceController$init$2(this));
        companion.setOnToggleSelfMute(new VoiceEngineServiceController$init$3(this, context));
        this.serviceBinding = new VoiceEngineForegroundService.Connection(context);
        Observable<NotificationData> observable = this.notificationDataObservable;
        m.checkNotNullExpressionValue(observable, "notificationDataObservable");
        ObservableExtensionsKt.appSubscribe(observable, VoiceEngineServiceController.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new VoiceEngineServiceController$init$4(this, context));
    }

    public final void startStream(Intent intent) {
        m.checkNotNullParameter(intent, "permissionIntent");
        VoiceEngineForegroundService.Companion companion = VoiceEngineForegroundService.Companion;
        VoiceEngineForegroundService.Connection connection = this.serviceBinding;
        if (connection == null) {
            m.throwUninitializedPropertyAccessException("serviceBinding");
        }
        companion.startStream(connection, intent);
    }

    public final void stopStream() {
        VoiceEngineForegroundService.Companion companion = VoiceEngineForegroundService.Companion;
        VoiceEngineForegroundService.Connection connection = this.serviceBinding;
        if (connection == null) {
            m.throwUninitializedPropertyAccessException("serviceBinding");
        }
        companion.stopStream(connection);
    }
}
