package com.discord.utilities.voice;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.app.AppComponent;
import com.discord.models.domain.ModelApplicationStream;
import com.discord.models.user.MeUser;
import com.discord.rtcconnection.RtcConnection;
import com.discord.stores.StoreApplicationStreaming;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreRtcConnection;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreVoiceParticipants;
import com.discord.utilities.media.AppSound;
import com.discord.utilities.media.AppSoundManager;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.t.n;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Subscription;
/* compiled from: CallSoundManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u0000 (2\u00020\u0001:\u0004()*+B#\u0012\u0006\u0010\u000e\u001a\u00020\r\u0012\b\b\u0002\u0010\u001e\u001a\u00020\u001d\u0012\b\b\u0002\u0010\u0015\u001a\u00020\u0014¢\u0006\u0004\b&\u0010'J#\u0010\b\u001a\u00020\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\b\u0010\tJ\u0019\u0010\u000b\u001a\u00020\u00072\n\u0010\n\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u000b\u0010\fR\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR \u0010\u0012\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00110\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0015\u001a\u00020\u00148\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016R\u0018\u0010\u0018\u001a\u0004\u0018\u00010\u00178\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R\u0016\u0010\u001b\u001a\u00020\u001a8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\u0016\u0010\u001e\u001a\u00020\u001d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR\u0018\u0010!\u001a\u0004\u0018\u00010 8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b!\u0010\"R\u001e\u0010#\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b#\u0010$R\u0016\u0010%\u001a\u00020\u001a8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b%\u0010\u001cR\u001e\u0010\n\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\n\u0010$¨\u0006,"}, d2 = {"Lcom/discord/utilities/voice/CallSoundManager;", "", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/utilities/voice/CallSoundManager$StoreState;", "storeState", "", "handleStoreState", "(JLcom/discord/utilities/voice/CallSoundManager$StoreState;)V", "voiceChannelId", "subscribeToStoreState", "(J)V", "Lcom/discord/app/AppComponent;", "appComponent", "Lcom/discord/app/AppComponent;", "", "Lcom/discord/primitives/UserId;", "streamingUserIds", "Ljava/util/List;", "Lcom/discord/utilities/voice/CallSoundManager$IStoreStateGenerator;", "storeStateGenerator", "Lcom/discord/utilities/voice/CallSoundManager$IStoreStateGenerator;", "", "activeStreamKey", "Ljava/lang/String;", "", "numConnectedParticipants", "I", "Lcom/discord/utilities/media/AppSoundManager;", "appSoundManager", "Lcom/discord/utilities/media/AppSoundManager;", "Lrx/Subscription;", "storeStateSubscription", "Lrx/Subscription;", "activeStreamUserId", "Ljava/lang/Long;", "activeStreamViewerCount", HookHelper.constructorName, "(Lcom/discord/app/AppComponent;Lcom/discord/utilities/media/AppSoundManager;Lcom/discord/utilities/voice/CallSoundManager$IStoreStateGenerator;)V", "Companion", "IStoreStateGenerator", "StoreState", "StoreStateGenerator", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class CallSoundManager {
    public static final Companion Companion = new Companion(null);
    private static final int JOIN_LEAVE_USER_LIMIT = 25;
    private String activeStreamKey;
    private Long activeStreamUserId;
    private int activeStreamViewerCount;
    private final AppComponent appComponent;
    private final AppSoundManager appSoundManager;
    private int numConnectedParticipants;
    private final IStoreStateGenerator storeStateGenerator;
    private Subscription storeStateSubscription;
    private List<Long> streamingUserIds;
    private Long voiceChannelId;

    /* compiled from: CallSoundManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/utilities/voice/CallSoundManager$Companion;", "", "", "JOIN_LEAVE_USER_LIMIT", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: CallSoundManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J!\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H&¢\u0006\u0004\b\u0007\u0010\b¨\u0006\t"}, d2 = {"Lcom/discord/utilities/voice/CallSoundManager$IStoreStateGenerator;", "", "", "Lcom/discord/primitives/ChannelId;", "voiceChannelId", "Lrx/Observable;", "Lcom/discord/utilities/voice/CallSoundManager$StoreState;", "observeStoreState", "(J)Lrx/Observable;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface IStoreStateGenerator {
        Observable<StoreState> observeStoreState(long j);
    }

    /* compiled from: CallSoundManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0011\b\u0086\b\u0018\u00002\u00020\u0001B}\u0012\u0016\u0010\u001a\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0002\u0012\u0006\u0010\u001b\u001a\u00020\b\u0012\u0016\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u000b0\u0002\u0012 \u0010\u001d\u001a\u001c\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u000f0\u0002\u0012\b\u0010\u001e\u001a\u0004\u0018\u00010\u0011\u0012\u0006\u0010\u001f\u001a\u00020\u0014\u0012\b\u0010 \u001a\u0004\u0018\u00010\u0017¢\u0006\u0004\b8\u00109J \u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ \u0010\f\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u000b0\u0002HÆ\u0003¢\u0006\u0004\b\f\u0010\u0007J*\u0010\u0010\u001a\u001c\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u000f0\u0002HÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0007J\u0012\u0010\u0012\u001a\u0004\u0018\u00010\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u0012\u0010\u0018\u001a\u0004\u0018\u00010\u0017HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0019J\u0094\u0001\u0010!\u001a\u00020\u00002\u0018\b\u0002\u0010\u001a\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00022\b\b\u0002\u0010\u001b\u001a\u00020\b2\u0018\b\u0002\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u000b0\u00022\"\b\u0002\u0010\u001d\u001a\u001c\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u000f0\u00022\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u00112\b\b\u0002\u0010\u001f\u001a\u00020\u00142\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u0017HÆ\u0001¢\u0006\u0004\b!\u0010\"J\u0010\u0010#\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b#\u0010$J\u0010\u0010&\u001a\u00020%HÖ\u0001¢\u0006\u0004\b&\u0010'J\u001a\u0010*\u001a\u00020)2\b\u0010(\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b*\u0010+R\u0019\u0010\u001b\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010,\u001a\u0004\b-\u0010\nR)\u0010\u001a\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010.\u001a\u0004\b/\u0010\u0007R\u001b\u0010 \u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b \u00100\u001a\u0004\b1\u0010\u0019R)\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u000b0\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010.\u001a\u0004\b2\u0010\u0007R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00103\u001a\u0004\b4\u0010\u0013R3\u0010\u001d\u001a\u001c\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u000f0\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010.\u001a\u0004\b5\u0010\u0007R\u0019\u0010\u001f\u001a\u00020\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00106\u001a\u0004\b7\u0010\u0016¨\u0006:"}, d2 = {"Lcom/discord/utilities/voice/CallSoundManager$StoreState;", "", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "component1", "()Ljava/util/Map;", "Lcom/discord/rtcconnection/RtcConnection$State;", "component2", "()Lcom/discord/rtcconnection/RtcConnection$State;", "Lcom/discord/models/domain/ModelApplicationStream;", "component3", "", "Lcom/discord/primitives/StreamKey;", "", "component4", "Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;", "component5", "()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;", "Lcom/discord/models/user/MeUser;", "component6", "()Lcom/discord/models/user/MeUser;", "Lcom/discord/api/channel/Channel;", "component7", "()Lcom/discord/api/channel/Channel;", "voiceParticipants", "rtcConnectionState", "streamsByUser", "streamSpectators", "activeApplicationStream", "me", "selectedVoiceChannel", "copy", "(Ljava/util/Map;Lcom/discord/rtcconnection/RtcConnection$State;Ljava/util/Map;Ljava/util/Map;Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lcom/discord/models/user/MeUser;Lcom/discord/api/channel/Channel;)Lcom/discord/utilities/voice/CallSoundManager$StoreState;", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/rtcconnection/RtcConnection$State;", "getRtcConnectionState", "Ljava/util/Map;", "getVoiceParticipants", "Lcom/discord/api/channel/Channel;", "getSelectedVoiceChannel", "getStreamsByUser", "Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;", "getActiveApplicationStream", "getStreamSpectators", "Lcom/discord/models/user/MeUser;", "getMe", HookHelper.constructorName, "(Ljava/util/Map;Lcom/discord/rtcconnection/RtcConnection$State;Ljava/util/Map;Ljava/util/Map;Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lcom/discord/models/user/MeUser;Lcom/discord/api/channel/Channel;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final StoreApplicationStreaming.ActiveApplicationStream activeApplicationStream;

        /* renamed from: me  reason: collision with root package name */
        private final MeUser f2795me;
        private final RtcConnection.State rtcConnectionState;
        private final Channel selectedVoiceChannel;
        private final Map<String, List<Long>> streamSpectators;
        private final Map<Long, ModelApplicationStream> streamsByUser;
        private final Map<Long, StoreVoiceParticipants.VoiceUser> voiceParticipants;

        /* JADX WARN: Multi-variable type inference failed */
        public StoreState(Map<Long, StoreVoiceParticipants.VoiceUser> map, RtcConnection.State state, Map<Long, ? extends ModelApplicationStream> map2, Map<String, ? extends List<Long>> map3, StoreApplicationStreaming.ActiveApplicationStream activeApplicationStream, MeUser meUser, Channel channel) {
            m.checkNotNullParameter(map, "voiceParticipants");
            m.checkNotNullParameter(state, "rtcConnectionState");
            m.checkNotNullParameter(map2, "streamsByUser");
            m.checkNotNullParameter(map3, "streamSpectators");
            m.checkNotNullParameter(meUser, "me");
            this.voiceParticipants = map;
            this.rtcConnectionState = state;
            this.streamsByUser = map2;
            this.streamSpectators = map3;
            this.activeApplicationStream = activeApplicationStream;
            this.f2795me = meUser;
            this.selectedVoiceChannel = channel;
        }

        public static /* synthetic */ StoreState copy$default(StoreState storeState, Map map, RtcConnection.State state, Map map2, Map map3, StoreApplicationStreaming.ActiveApplicationStream activeApplicationStream, MeUser meUser, Channel channel, int i, Object obj) {
            Map<Long, StoreVoiceParticipants.VoiceUser> map4 = map;
            if ((i & 1) != 0) {
                map4 = storeState.voiceParticipants;
            }
            if ((i & 2) != 0) {
                state = storeState.rtcConnectionState;
            }
            RtcConnection.State state2 = state;
            Map<Long, ModelApplicationStream> map5 = map2;
            if ((i & 4) != 0) {
                map5 = storeState.streamsByUser;
            }
            Map map6 = map5;
            Map<String, List<Long>> map7 = map3;
            if ((i & 8) != 0) {
                map7 = storeState.streamSpectators;
            }
            Map map8 = map7;
            if ((i & 16) != 0) {
                activeApplicationStream = storeState.activeApplicationStream;
            }
            StoreApplicationStreaming.ActiveApplicationStream activeApplicationStream2 = activeApplicationStream;
            if ((i & 32) != 0) {
                meUser = storeState.f2795me;
            }
            MeUser meUser2 = meUser;
            if ((i & 64) != 0) {
                channel = storeState.selectedVoiceChannel;
            }
            return storeState.copy(map4, state2, map6, map8, activeApplicationStream2, meUser2, channel);
        }

        public final Map<Long, StoreVoiceParticipants.VoiceUser> component1() {
            return this.voiceParticipants;
        }

        public final RtcConnection.State component2() {
            return this.rtcConnectionState;
        }

        public final Map<Long, ModelApplicationStream> component3() {
            return this.streamsByUser;
        }

        public final Map<String, List<Long>> component4() {
            return this.streamSpectators;
        }

        public final StoreApplicationStreaming.ActiveApplicationStream component5() {
            return this.activeApplicationStream;
        }

        public final MeUser component6() {
            return this.f2795me;
        }

        public final Channel component7() {
            return this.selectedVoiceChannel;
        }

        public final StoreState copy(Map<Long, StoreVoiceParticipants.VoiceUser> map, RtcConnection.State state, Map<Long, ? extends ModelApplicationStream> map2, Map<String, ? extends List<Long>> map3, StoreApplicationStreaming.ActiveApplicationStream activeApplicationStream, MeUser meUser, Channel channel) {
            m.checkNotNullParameter(map, "voiceParticipants");
            m.checkNotNullParameter(state, "rtcConnectionState");
            m.checkNotNullParameter(map2, "streamsByUser");
            m.checkNotNullParameter(map3, "streamSpectators");
            m.checkNotNullParameter(meUser, "me");
            return new StoreState(map, state, map2, map3, activeApplicationStream, meUser, channel);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.voiceParticipants, storeState.voiceParticipants) && m.areEqual(this.rtcConnectionState, storeState.rtcConnectionState) && m.areEqual(this.streamsByUser, storeState.streamsByUser) && m.areEqual(this.streamSpectators, storeState.streamSpectators) && m.areEqual(this.activeApplicationStream, storeState.activeApplicationStream) && m.areEqual(this.f2795me, storeState.f2795me) && m.areEqual(this.selectedVoiceChannel, storeState.selectedVoiceChannel);
        }

        public final StoreApplicationStreaming.ActiveApplicationStream getActiveApplicationStream() {
            return this.activeApplicationStream;
        }

        public final MeUser getMe() {
            return this.f2795me;
        }

        public final RtcConnection.State getRtcConnectionState() {
            return this.rtcConnectionState;
        }

        public final Channel getSelectedVoiceChannel() {
            return this.selectedVoiceChannel;
        }

        public final Map<String, List<Long>> getStreamSpectators() {
            return this.streamSpectators;
        }

        public final Map<Long, ModelApplicationStream> getStreamsByUser() {
            return this.streamsByUser;
        }

        public final Map<Long, StoreVoiceParticipants.VoiceUser> getVoiceParticipants() {
            return this.voiceParticipants;
        }

        public int hashCode() {
            Map<Long, StoreVoiceParticipants.VoiceUser> map = this.voiceParticipants;
            int i = 0;
            int hashCode = (map != null ? map.hashCode() : 0) * 31;
            RtcConnection.State state = this.rtcConnectionState;
            int hashCode2 = (hashCode + (state != null ? state.hashCode() : 0)) * 31;
            Map<Long, ModelApplicationStream> map2 = this.streamsByUser;
            int hashCode3 = (hashCode2 + (map2 != null ? map2.hashCode() : 0)) * 31;
            Map<String, List<Long>> map3 = this.streamSpectators;
            int hashCode4 = (hashCode3 + (map3 != null ? map3.hashCode() : 0)) * 31;
            StoreApplicationStreaming.ActiveApplicationStream activeApplicationStream = this.activeApplicationStream;
            int hashCode5 = (hashCode4 + (activeApplicationStream != null ? activeApplicationStream.hashCode() : 0)) * 31;
            MeUser meUser = this.f2795me;
            int hashCode6 = (hashCode5 + (meUser != null ? meUser.hashCode() : 0)) * 31;
            Channel channel = this.selectedVoiceChannel;
            if (channel != null) {
                i = channel.hashCode();
            }
            return hashCode6 + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(voiceParticipants=");
            R.append(this.voiceParticipants);
            R.append(", rtcConnectionState=");
            R.append(this.rtcConnectionState);
            R.append(", streamsByUser=");
            R.append(this.streamsByUser);
            R.append(", streamSpectators=");
            R.append(this.streamSpectators);
            R.append(", activeApplicationStream=");
            R.append(this.activeApplicationStream);
            R.append(", me=");
            R.append(this.f2795me);
            R.append(", selectedVoiceChannel=");
            R.append(this.selectedVoiceChannel);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: CallSoundManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B9\u0012\b\b\u0002\u0010\u0010\u001a\u00020\u000f\u0012\b\b\u0002\u0010\r\u001a\u00020\f\u0012\b\b\u0002\u0010\u0016\u001a\u00020\u0015\u0012\b\b\u0002\u0010\u0013\u001a\u00020\u0012\u0012\b\b\u0002\u0010\n\u001a\u00020\t¢\u0006\u0004\b\u0018\u0010\u0019J!\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0016¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0016\u0010\u0010\u001a\u00020\u000f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0016\u001a\u00020\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017¨\u0006\u001a"}, d2 = {"Lcom/discord/utilities/voice/CallSoundManager$StoreStateGenerator;", "Lcom/discord/utilities/voice/CallSoundManager$IStoreStateGenerator;", "", "Lcom/discord/primitives/ChannelId;", "voiceChannelId", "Lrx/Observable;", "Lcom/discord/utilities/voice/CallSoundManager$StoreState;", "observeStoreState", "(J)Lrx/Observable;", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreChannels;", "Lcom/discord/stores/StoreRtcConnection;", "storeRtcConnection", "Lcom/discord/stores/StoreRtcConnection;", "Lcom/discord/stores/StoreVoiceParticipants;", "storeVoiceParticipants", "Lcom/discord/stores/StoreVoiceParticipants;", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreUser;", "Lcom/discord/stores/StoreApplicationStreaming;", "storeApplicationStreaming", "Lcom/discord/stores/StoreApplicationStreaming;", HookHelper.constructorName, "(Lcom/discord/stores/StoreVoiceParticipants;Lcom/discord/stores/StoreRtcConnection;Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannels;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreStateGenerator implements IStoreStateGenerator {
        private final StoreApplicationStreaming storeApplicationStreaming;
        private final StoreChannels storeChannels;
        private final StoreRtcConnection storeRtcConnection;
        private final StoreUser storeUser;
        private final StoreVoiceParticipants storeVoiceParticipants;

        public StoreStateGenerator() {
            this(null, null, null, null, null, 31, null);
        }

        public StoreStateGenerator(StoreVoiceParticipants storeVoiceParticipants, StoreRtcConnection storeRtcConnection, StoreApplicationStreaming storeApplicationStreaming, StoreUser storeUser, StoreChannels storeChannels) {
            m.checkNotNullParameter(storeVoiceParticipants, "storeVoiceParticipants");
            m.checkNotNullParameter(storeRtcConnection, "storeRtcConnection");
            m.checkNotNullParameter(storeApplicationStreaming, "storeApplicationStreaming");
            m.checkNotNullParameter(storeUser, "storeUser");
            m.checkNotNullParameter(storeChannels, "storeChannels");
            this.storeVoiceParticipants = storeVoiceParticipants;
            this.storeRtcConnection = storeRtcConnection;
            this.storeApplicationStreaming = storeApplicationStreaming;
            this.storeUser = storeUser;
            this.storeChannels = storeChannels;
        }

        @Override // com.discord.utilities.voice.CallSoundManager.IStoreStateGenerator
        public Observable<StoreState> observeStoreState(long j) {
            Observable<StoreState> e = Observable.e(this.storeVoiceParticipants.get(j), this.storeRtcConnection.getConnectionState(), this.storeApplicationStreaming.observeStreamsByUser(), this.storeApplicationStreaming.observeStreamSpectators(), this.storeApplicationStreaming.observeActiveStream(), StoreUser.observeMe$default(this.storeUser, false, 1, null), this.storeChannels.observeChannel(j), CallSoundManager$StoreStateGenerator$observeStoreState$1.INSTANCE);
            m.checkNotNullExpressionValue(e, "Observable.combineLatest…Channel\n        )\n      }");
            return e;
        }

        public /* synthetic */ StoreStateGenerator(StoreVoiceParticipants storeVoiceParticipants, StoreRtcConnection storeRtcConnection, StoreApplicationStreaming storeApplicationStreaming, StoreUser storeUser, StoreChannels storeChannels, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? StoreStream.Companion.getVoiceParticipants() : storeVoiceParticipants, (i & 2) != 0 ? StoreStream.Companion.getRtcConnection() : storeRtcConnection, (i & 4) != 0 ? StoreStream.Companion.getApplicationStreaming() : storeApplicationStreaming, (i & 8) != 0 ? StoreStream.Companion.getUsers() : storeUser, (i & 16) != 0 ? StoreStream.Companion.getChannels() : storeChannels);
        }
    }

    public CallSoundManager(AppComponent appComponent, AppSoundManager appSoundManager, IStoreStateGenerator iStoreStateGenerator) {
        m.checkNotNullParameter(appComponent, "appComponent");
        m.checkNotNullParameter(appSoundManager, "appSoundManager");
        m.checkNotNullParameter(iStoreStateGenerator, "storeStateGenerator");
        this.appComponent = appComponent;
        this.appSoundManager = appSoundManager;
        this.storeStateGenerator = iStoreStateGenerator;
        this.streamingUserIds = n.emptyList();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleStoreState(long j, StoreState storeState) {
        boolean z2;
        boolean z3;
        boolean z4;
        Channel selectedVoiceChannel;
        boolean z5;
        StoreApplicationStreaming.ActiveApplicationStream.State state;
        int i;
        Long l = this.voiceChannelId;
        List<Long> list = this.streamingUserIds;
        int i2 = this.activeStreamViewerCount;
        String str = this.activeStreamKey;
        Long l2 = this.activeStreamUserId;
        int i3 = this.numConnectedParticipants;
        this.voiceChannelId = Long.valueOf(j);
        Collection<ModelApplicationStream> values = storeState.getStreamsByUser().values();
        ArrayList<ModelApplicationStream> arrayList = new ArrayList();
        Iterator<T> it = values.iterator();
        while (true) {
            boolean z6 = true;
            z2 = false;
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            if (((ModelApplicationStream) next).getChannelId() != j) {
                z6 = false;
            }
            if (z6) {
                arrayList.add(next);
            }
        }
        ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(arrayList, 10));
        for (ModelApplicationStream modelApplicationStream : arrayList) {
            arrayList2.add(Long.valueOf(modelApplicationStream.getOwnerId()));
        }
        this.streamingUserIds = arrayList2;
        StoreApplicationStreaming.ActiveApplicationStream activeApplicationStream = storeState.getActiveApplicationStream();
        if (activeApplicationStream == null || (state = activeApplicationStream.getState()) == null || !state.isStreamActive()) {
            this.activeStreamKey = null;
            this.activeStreamUserId = null;
            this.activeStreamViewerCount = 0;
        } else {
            this.activeStreamKey = activeApplicationStream.getStream().getEncodedStreamKey();
            this.activeStreamUserId = Long.valueOf(activeApplicationStream.getStream().getOwnerId());
            List<Long> list2 = storeState.getStreamSpectators().get(this.activeStreamKey);
            if (list2 != null) {
                ArrayList arrayList3 = new ArrayList();
                for (Object obj : list2) {
                    if (((Number) obj).longValue() != storeState.getMe().getId()) {
                        arrayList3.add(obj);
                    }
                }
                i = arrayList3.size();
            } else {
                i = 0;
            }
            this.activeStreamViewerCount = i;
        }
        String str2 = this.activeStreamKey;
        boolean z7 = str2 != null && m.areEqual(str2, str);
        List<Long> list3 = this.streamingUserIds;
        if (!(list3 instanceof Collection) || !list3.isEmpty()) {
            for (Number number : list3) {
                if (!list.contains(Long.valueOf(number.longValue()))) {
                    z3 = true;
                    break;
                }
            }
        }
        z3 = false;
        if (!(list instanceof Collection) || !list.isEmpty()) {
            for (Number number2 : list) {
                long longValue = number2.longValue();
                if (this.streamingUserIds.contains(Long.valueOf(longValue)) || (longValue != storeState.getMe().getId() && (l2 == null || longValue != l2.longValue()))) {
                    z5 = false;
                    continue;
                } else {
                    z5 = true;
                    continue;
                }
                if (z5) {
                    z4 = true;
                    break;
                }
            }
        }
        z4 = false;
        boolean z8 = z7 && i2 <= 25 && this.activeStreamViewerCount > i2;
        if (z7 && i2 <= 25 && this.activeStreamViewerCount < i2) {
            z2 = true;
        }
        if (m.areEqual(this.voiceChannelId, l)) {
            if (z3) {
                this.appSoundManager.play(AppSound.Companion.getSOUND_STREAM_STARTED());
            } else if (z4) {
                this.appSoundManager.play(AppSound.Companion.getSOUND_STREAM_ENDED());
            } else if (z8) {
                this.appSoundManager.play(AppSound.Companion.getSOUND_STREAM_USER_JOINED());
            } else if (z2) {
                this.appSoundManager.play(AppSound.Companion.getSOUND_STREAM_USER_LEFT());
            }
        }
        Map<Long, StoreVoiceParticipants.VoiceUser> voiceParticipants = storeState.getVoiceParticipants();
        boolean areEqual = m.areEqual(storeState.getRtcConnectionState(), RtcConnection.State.f.a);
        Collection<StoreVoiceParticipants.VoiceUser> values2 = voiceParticipants.values();
        ArrayList arrayList4 = new ArrayList();
        for (Object obj2 : values2) {
            if (((StoreVoiceParticipants.VoiceUser) obj2).isConnected()) {
                arrayList4.add(obj2);
            }
        }
        this.numConnectedParticipants = arrayList4.size();
        Collection<StoreVoiceParticipants.VoiceUser> values3 = voiceParticipants.values();
        ArrayList arrayList5 = new ArrayList();
        for (Object obj3 : values3) {
            if (((StoreVoiceParticipants.VoiceUser) obj3).isRinging()) {
                arrayList5.add(obj3);
            }
        }
        int size = arrayList5.size();
        if (areEqual && (selectedVoiceChannel = storeState.getSelectedVoiceChannel()) != null && ChannelUtils.F(selectedVoiceChannel) && i3 <= 25) {
            int i4 = this.numConnectedParticipants;
            if (i4 > i3) {
                this.appSoundManager.play(AppSound.Companion.getSOUND_USER_JOINED());
            } else if (i4 < i3) {
                this.appSoundManager.play(AppSound.Companion.getSOUND_USER_LEFT());
            }
        }
        if (this.numConnectedParticipants < 2 && size > 0) {
            AppSoundManager appSoundManager = this.appSoundManager;
            AppSound.Companion companion = AppSound.Companion;
            if (!appSoundManager.isPlaying(companion.getSOUND_CALL_CALLING()) && areEqual) {
                this.appSoundManager.play(companion.getSOUND_CALL_CALLING());
                return;
            }
        }
        if (!areEqual || size == 0 || this.numConnectedParticipants >= 2) {
            this.appSoundManager.stop(AppSound.Companion.getSOUND_CALL_CALLING());
        }
    }

    public final void subscribeToStoreState(long j) {
        Subscription subscription = this.storeStateSubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        Observable<StoreState> q = this.storeStateGenerator.observeStoreState(j).q();
        m.checkNotNullExpressionValue(q, "storeStateGenerator\n    …  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(q), this.appComponent, null, 2, null), CallSoundManager.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new CallSoundManager$subscribeToStoreState$1(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : new CallSoundManager$subscribeToStoreState$2(this), (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new CallSoundManager$subscribeToStoreState$3(this, j));
    }

    public /* synthetic */ CallSoundManager(AppComponent appComponent, AppSoundManager appSoundManager, IStoreStateGenerator iStoreStateGenerator, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(appComponent, (i & 2) != 0 ? AppSoundManager.Provider.INSTANCE.get() : appSoundManager, (i & 4) != 0 ? new StoreStateGenerator(null, null, null, null, null, 31, null) : iStoreStateGenerator);
    }
}
