package com.discord.utilities.voice;

import androidx.core.app.NotificationCompat;
import d0.z.d.m;
import j0.k.b;
import kotlin.Metadata;
/* compiled from: DiscordOverlayService.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000b\n\u0002\b\u0005\u0010\u0005\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "kotlin.jvm.PlatformType", "it", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Boolean;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class DiscordOverlayService$onStartCommand$1<T, R> implements b<Boolean, Boolean> {
    public static final DiscordOverlayService$onStartCommand$1 INSTANCE = new DiscordOverlayService$onStartCommand$1();

    public final Boolean call(Boolean bool) {
        return Boolean.valueOf(m.areEqual(bool, Boolean.TRUE));
    }
}
