package com.discord.utilities.voice;

import com.discord.stores.StoreStream;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: VoiceEngineServiceController.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/utilities/voice/VoiceEngineServiceController;", "invoke", "()Lcom/discord/utilities/voice/VoiceEngineServiceController;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class VoiceEngineServiceController$Companion$INSTANCE$2 extends o implements Function0<VoiceEngineServiceController> {
    public static final VoiceEngineServiceController$Companion$INSTANCE$2 INSTANCE = new VoiceEngineServiceController$Companion$INSTANCE$2();

    public VoiceEngineServiceController$Companion$INSTANCE$2() {
        super(0);
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final VoiceEngineServiceController invoke() {
        StoreStream.Companion companion = StoreStream.Companion;
        return new VoiceEngineServiceController(companion.getAudioManagerV2(), companion.getMediaSettings(), companion.getVoiceChannelSelected(), companion.getRtcConnection());
    }
}
