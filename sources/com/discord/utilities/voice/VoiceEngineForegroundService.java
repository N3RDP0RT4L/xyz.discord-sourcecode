package com.discord.utilities.voice;

import andhook.lib.HookHelper;
import android.app.Application;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import b.a.d.l;
import b.d.b.a.a;
import com.discord.app.AppComponent;
import com.discord.app.AppLog;
import com.discord.utilities.extensions.PendingIntentExtensionsKt;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.system.SystemServiceExtensionsKt;
import com.discord.utilities.voice.VoiceEngineForegroundService;
import com.discord.widgets.stage.StageChannelAPI;
import com.discord.widgets.stage.StageChannelNotifications;
import com.discord.widgets.voice.fullscreen.WidgetCallFullscreen;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;
/* compiled from: VoiceEngineForegroundService.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 -2\u00020\u00012\u00020\u0002:\u0003-./B\u0007¢\u0006\u0004\b,\u0010\u000bJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\b\u0010\tJ\u000f\u0010\n\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\n\u0010\u000bJ\u000f\u0010\f\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\f\u0010\u000bJ\u0017\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u000e\u0010\u000fJ\u0019\u0010\u0010\u001a\u00020\u00052\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003H\u0016¢\u0006\u0004\b\u0010\u0010\u0011J\u0019\u0010\u0012\u001a\u00020\u00072\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003H\u0014¢\u0006\u0004\b\u0012\u0010\u0013R\u0018\u0010\u0015\u001a\u0004\u0018\u00010\u00148\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016R\u001c\u0010\u0019\u001a\b\u0018\u00010\u0017R\u00020\u00188\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR(\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u001c0\u001b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 R\u0016\u0010\"\u001a\u00020!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010#R\u0016\u0010%\u001a\u00020$8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b%\u0010&R\u001c\u0010)\u001a\b\u0018\u00010'R\u00020(8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b)\u0010*R\u001c\u0010+\u001a\b\u0018\u00010'R\u00020(8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b+\u0010*¨\u00060"}, d2 = {"Lcom/discord/utilities/voice/VoiceEngineForegroundService;", "Landroid/app/IntentService;", "Lcom/discord/app/AppComponent;", "Landroid/content/Intent;", "intent", "", "accept", "", "ackStageInvite", "(Landroid/content/Intent;Z)V", "onCreate", "()V", "onDestroy", "Landroid/os/IBinder;", "onBind", "(Landroid/content/Intent;)Landroid/os/IBinder;", "onUnbind", "(Landroid/content/Intent;)Z", "onHandleIntent", "(Landroid/content/Intent;)V", "Lcom/discord/utilities/voice/ScreenShareManager;", "screenShareManager", "Lcom/discord/utilities/voice/ScreenShareManager;", "Landroid/net/wifi/WifiManager$WifiLock;", "Landroid/net/wifi/WifiManager;", "wakeLockWifi", "Landroid/net/wifi/WifiManager$WifiLock;", "Lrx/subjects/Subject;", "Ljava/lang/Void;", "unsubscribeSignal", "Lrx/subjects/Subject;", "getUnsubscribeSignal", "()Lrx/subjects/Subject;", "Lcom/discord/utilities/voice/VoiceEngineForegroundService$LocalBinder;", "binder", "Lcom/discord/utilities/voice/VoiceEngineForegroundService$LocalBinder;", "Lcom/discord/utilities/voice/CallSoundManager;", "ringManager", "Lcom/discord/utilities/voice/CallSoundManager;", "Landroid/os/PowerManager$WakeLock;", "Landroid/os/PowerManager;", "wakeLockPartial", "Landroid/os/PowerManager$WakeLock;", "wakeLockProximity", HookHelper.constructorName, "Companion", "Connection", "LocalBinder", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class VoiceEngineForegroundService extends IntentService implements AppComponent {
    private static final String LOG_TAG = "DiscordVoiceService";
    private static final int NOTIFICATION_ID = 101;
    private static final long WAKELOCK_TIMEOUT = 7200000;
    private final LocalBinder binder = new LocalBinder(this);
    private final CallSoundManager ringManager = new CallSoundManager(this, null, null, 6, null);
    private ScreenShareManager screenShareManager;
    private final Subject<Void, Void> unsubscribeSignal;
    private PowerManager.WakeLock wakeLockPartial;
    private PowerManager.WakeLock wakeLockProximity;
    private WifiManager.WifiLock wakeLockWifi;
    public static final Companion Companion = new Companion(null);
    private static Function0<Unit> onDisconnect = VoiceEngineForegroundService$Companion$onDisconnect$1.INSTANCE;
    private static Function0<Unit> onToggleSelfDeafen = VoiceEngineForegroundService$Companion$onToggleSelfDeafen$1.INSTANCE;
    private static Function0<Unit> onToggleSelfMute = VoiceEngineForegroundService$Companion$onToggleSelfMute$1.INSTANCE;

    /* compiled from: VoiceEngineForegroundService.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\t\b\u0086\u0003\u0018\u00002\u00020\u0001:\u0002;<B\t\b\u0002¢\u0006\u0004\b9\u0010:J)\u0010\n\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\n\u0010\u000bJ)\u0010\r\u001a\u00020\f2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\r\u0010\u000eJi\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00020\u00072\u0006\u0010\u0015\u001a\u00020\u00072\u0006\u0010\u0016\u001a\u00020\u00072\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u000e\u0010\u0018\u001a\n\u0018\u00010\u0004j\u0004\u0018\u0001`\u00172\u0006\u0010\u0019\u001a\u00020\u00072\u0006\u0010\u001a\u001a\u00020\u0007¢\u0006\u0004\b\u001c\u0010\u001dJ\u0015\u0010\u001e\u001a\u00020\u001b2\u0006\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b\u001e\u0010\u001fJ\u001d\u0010!\u001a\u00020\u001b2\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010 \u001a\u00020\f¢\u0006\u0004\b!\u0010\"J\u0015\u0010#\u001a\u00020\u001b2\u0006\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b#\u0010\u001fR(\u0010%\u001a\b\u0012\u0004\u0012\u00020\u001b0$8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b%\u0010&\u001a\u0004\b'\u0010(\"\u0004\b)\u0010*R(\u0010+\u001a\b\u0012\u0004\u0012\u00020\u001b0$8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b+\u0010&\u001a\u0004\b,\u0010(\"\u0004\b-\u0010*R(\u0010.\u001a\b\u0012\u0004\u0012\u00020\u001b0$8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b.\u0010&\u001a\u0004\b/\u0010(\"\u0004\b0\u0010*R\u0016\u00102\u001a\u0002018\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b2\u00103R\u0016\u00105\u001a\u0002048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b5\u00106R\u0016\u00107\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b7\u00108¨\u0006="}, d2 = {"Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion;", "", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "accept", "Landroid/app/PendingIntent;", "stageInviteAckPendingIntent", "(Landroid/content/Context;JZ)Landroid/app/PendingIntent;", "Landroid/content/Intent;", "stageInviteAckIntent", "(Landroid/content/Context;JZ)Landroid/content/Intent;", "Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;", "connection", "", "title", "subtitle", "selfMute", "selfDeafen", "selfStream", "Lcom/discord/primitives/GuildId;", "guildId", "isProximityLockEnabled", "canSpeak", "", "startForegroundAndBind", "(Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZZJLjava/lang/Long;ZZ)V", "stopForegroundAndUnbind", "(Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;)V", "permissionIntent", "startStream", "(Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;Landroid/content/Intent;)V", "stopStream", "Lkotlin/Function0;", "onDisconnect", "Lkotlin/jvm/functions/Function0;", "getOnDisconnect", "()Lkotlin/jvm/functions/Function0;", "setOnDisconnect", "(Lkotlin/jvm/functions/Function0;)V", "onToggleSelfDeafen", "getOnToggleSelfDeafen", "setOnToggleSelfDeafen", "onToggleSelfMute", "getOnToggleSelfMute", "setOnToggleSelfMute", "", "LOG_TAG", "Ljava/lang/String;", "", "NOTIFICATION_ID", "I", "WAKELOCK_TIMEOUT", "J", HookHelper.constructorName, "()V", "ACTION", "EXTRA", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {

        /* compiled from: VoiceEngineForegroundService.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\bf\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002¨\u0006\u0003"}, d2 = {"Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion$ACTION;", "", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public interface ACTION {
            public static final C0225Companion Companion = C0225Companion.$$INSTANCE;
            public static final String DISCONNECT = "com.discord.utilities.voice.action.disconnect";
            public static final String MAIN_ACTION = "com.discord.utilities.voice.action.main";
            public static final String STAGE_INVITE_ACCEPT = "com.discord.utilities.voice.action.stage_invite_accept";
            public static final String STAGE_INVITE_DECLINE = "com.discord.utilities.voice.action.stage_invite_decline";
            public static final String START_FOREGROUND = "com.discord.utilities.voice.action.start_foreground";
            public static final String START_STREAM = "com.discord.utilities.voice.action.start_stream";
            public static final String STOP_SERVICE = "com.discord.utilities.voice.action.stop";
            public static final String STOP_STREAM = "com.discord.utilities.voice.action.stop_stream";
            public static final String TOGGLE_DEAFENED = "com.discord.utilities.voice.action.toggle_deafened";
            public static final String TOGGLE_MUTED = "com.discord.utilities.voice.action.toggle_muted";

            /* compiled from: VoiceEngineForegroundService.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u000e\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0007\u0010\u0004R\u0016\u0010\b\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\b\u0010\u0004R\u0016\u0010\t\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\t\u0010\u0004R\u0016\u0010\n\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\n\u0010\u0004R\u0016\u0010\u000b\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000b\u0010\u0004R\u0016\u0010\f\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\f\u0010\u0004R\u0016\u0010\r\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\r\u0010\u0004¨\u0006\u0010"}, d2 = {"Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion$ACTION$Companion;", "", "", "TOGGLE_DEAFENED", "Ljava/lang/String;", "STOP_SERVICE", "START_FOREGROUND", "START_STREAM", "STAGE_INVITE_DECLINE", "STOP_STREAM", "STAGE_INVITE_ACCEPT", "MAIN_ACTION", "TOGGLE_MUTED", "DISCONNECT", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* renamed from: com.discord.utilities.voice.VoiceEngineForegroundService$Companion$ACTION$Companion  reason: collision with other inner class name */
            /* loaded from: classes2.dex */
            public static final class C0225Companion {
                public static final /* synthetic */ C0225Companion $$INSTANCE = new C0225Companion();
                public static final String DISCONNECT = "com.discord.utilities.voice.action.disconnect";
                public static final String MAIN_ACTION = "com.discord.utilities.voice.action.main";
                public static final String STAGE_INVITE_ACCEPT = "com.discord.utilities.voice.action.stage_invite_accept";
                public static final String STAGE_INVITE_DECLINE = "com.discord.utilities.voice.action.stage_invite_decline";
                public static final String START_FOREGROUND = "com.discord.utilities.voice.action.start_foreground";
                public static final String START_STREAM = "com.discord.utilities.voice.action.start_stream";
                public static final String STOP_SERVICE = "com.discord.utilities.voice.action.stop";
                public static final String STOP_STREAM = "com.discord.utilities.voice.action.stop_stream";
                public static final String TOGGLE_DEAFENED = "com.discord.utilities.voice.action.toggle_deafened";
                public static final String TOGGLE_MUTED = "com.discord.utilities.voice.action.toggle_muted";

                private C0225Companion() {
                }
            }
        }

        /* compiled from: VoiceEngineForegroundService.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\bb\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002¨\u0006\u0003"}, d2 = {"Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion$EXTRA;", "", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public interface EXTRA {
            public static final String CHANNEL_ID = "com.discord.utilities.voice.extra.channel_id";
            public static final C0226Companion Companion = C0226Companion.$$INSTANCE;
            public static final String GUILD_ID = "com.discord.utilities.voice.extra.guild_id";
            public static final String ITEM_CAN_SPEAK = "com.discord.utilities.voice.extra.item_can_speak";
            public static final String ITEM_DEAFENED = "com.discord.utilities.voice.extra.item_deafened";
            public static final String ITEM_MUTED = "com.discord.utilities.voice.extra.item_muted";
            public static final String ITEM_STREAMING = "com.discord.utilities.voice.extra.item_streaming";
            public static final String PROXIMITY_LOCK_ENABLED = "com.discord.utilities.voice.extra.proximity_lock_enabled";
            public static final String TITLE = "com.discord.utilities.voice.extra.title";
            public static final String TITLE_SUBTEXT = "com.discord.utilities.voice.extra.title_subtext";

            /* compiled from: VoiceEngineForegroundService.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\r\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eR\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0007\u0010\u0004R\u0016\u0010\b\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\b\u0010\u0004R\u0016\u0010\t\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\t\u0010\u0004R\u0016\u0010\n\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\n\u0010\u0004R\u0016\u0010\u000b\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000b\u0010\u0004R\u0016\u0010\f\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\f\u0010\u0004¨\u0006\u000f"}, d2 = {"Lcom/discord/utilities/voice/VoiceEngineForegroundService$Companion$EXTRA$Companion;", "", "", "GUILD_ID", "Ljava/lang/String;", "TITLE", "ITEM_MUTED", "ITEM_CAN_SPEAK", "ITEM_STREAMING", "CHANNEL_ID", "ITEM_DEAFENED", "PROXIMITY_LOCK_ENABLED", "TITLE_SUBTEXT", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* renamed from: com.discord.utilities.voice.VoiceEngineForegroundService$Companion$EXTRA$Companion  reason: collision with other inner class name */
            /* loaded from: classes2.dex */
            public static final class C0226Companion {
                public static final /* synthetic */ C0226Companion $$INSTANCE = new C0226Companion();
                public static final String CHANNEL_ID = "com.discord.utilities.voice.extra.channel_id";
                public static final String GUILD_ID = "com.discord.utilities.voice.extra.guild_id";
                public static final String ITEM_CAN_SPEAK = "com.discord.utilities.voice.extra.item_can_speak";
                public static final String ITEM_DEAFENED = "com.discord.utilities.voice.extra.item_deafened";
                public static final String ITEM_MUTED = "com.discord.utilities.voice.extra.item_muted";
                public static final String ITEM_STREAMING = "com.discord.utilities.voice.extra.item_streaming";
                public static final String PROXIMITY_LOCK_ENABLED = "com.discord.utilities.voice.extra.proximity_lock_enabled";
                public static final String TITLE = "com.discord.utilities.voice.extra.title";
                public static final String TITLE_SUBTEXT = "com.discord.utilities.voice.extra.title_subtext";

                private C0226Companion() {
                }
            }
        }

        private Companion() {
        }

        public final Function0<Unit> getOnDisconnect() {
            return VoiceEngineForegroundService.onDisconnect;
        }

        public final Function0<Unit> getOnToggleSelfDeafen() {
            return VoiceEngineForegroundService.onToggleSelfDeafen;
        }

        public final Function0<Unit> getOnToggleSelfMute() {
            return VoiceEngineForegroundService.onToggleSelfMute;
        }

        public final void setOnDisconnect(Function0<Unit> function0) {
            m.checkNotNullParameter(function0, "<set-?>");
            VoiceEngineForegroundService.onDisconnect = function0;
        }

        public final void setOnToggleSelfDeafen(Function0<Unit> function0) {
            m.checkNotNullParameter(function0, "<set-?>");
            VoiceEngineForegroundService.onToggleSelfDeafen = function0;
        }

        public final void setOnToggleSelfMute(Function0<Unit> function0) {
            m.checkNotNullParameter(function0, "<set-?>");
            VoiceEngineForegroundService.onToggleSelfMute = function0;
        }

        public final Intent stageInviteAckIntent(Context context, long j, boolean z2) {
            m.checkNotNullParameter(context, "context");
            boolean z3 = ContextCompat.checkSelfPermission(context, "android.permission.RECORD_AUDIO") == 0;
            if (!z2 || z3) {
                Intent intent = new Intent(context, VoiceEngineForegroundService.class);
                intent.setAction(z2 ? "com.discord.utilities.voice.action.stage_invite_accept" : "com.discord.utilities.voice.action.stage_invite_decline");
                intent.putExtra("com.discord.utilities.voice.extra.channel_id", j);
                return intent;
            }
            Intent intent2 = new Intent(context, WidgetCallFullscreen.class);
            intent2.putExtra("com.discord.intent.extra.EXTRA_CHANNEL_ID", j);
            return intent2;
        }

        public final PendingIntent stageInviteAckPendingIntent(Context context, long j, boolean z2) {
            m.checkNotNullParameter(context, "context");
            PendingIntent service = PendingIntent.getService(context, 0, stageInviteAckIntent(context, j, z2), PendingIntentExtensionsKt.immutablePendingIntentFlag(1207959552));
            m.checkNotNullExpressionValue(service, "PendingIntent.getService…AG_UPDATE_CURRENT),\n    )");
            return service;
        }

        public final void startForegroundAndBind(Connection connection, CharSequence charSequence, CharSequence charSequence2, boolean z2, boolean z3, boolean z4, long j, Long l, boolean z5, boolean z6) {
            m.checkNotNullParameter(connection, "connection");
            m.checkNotNullParameter(charSequence, "title");
            m.checkNotNullParameter(charSequence2, "subtitle");
            try {
                Logger.v$default(AppLog.g, VoiceEngineForegroundService.LOG_TAG, "Bind service connection.", null, 4, null);
                Context context = connection.getContext();
                Intent intent = new Intent(connection.getContext(), VoiceEngineForegroundService.class);
                intent.setAction("com.discord.utilities.voice.action.start_foreground");
                intent.putExtra("com.discord.utilities.voice.extra.title", charSequence);
                intent.putExtra("com.discord.utilities.voice.extra.title_subtext", charSequence2);
                intent.putExtra("com.discord.utilities.voice.extra.item_muted", z2);
                intent.putExtra("com.discord.utilities.voice.extra.item_deafened", z3);
                intent.putExtra("com.discord.utilities.voice.extra.item_streaming", z4);
                intent.putExtra("com.discord.utilities.voice.extra.item_can_speak", z6);
                intent.putExtra("com.discord.utilities.voice.extra.proximity_lock_enabled", z5);
                intent.putExtra("com.discord.utilities.voice.extra.channel_id", j);
                intent.putExtra("com.discord.utilities.voice.extra.guild_id", l);
                context.startService(intent);
                connection.getContext().bindService(new Intent(connection.getContext(), VoiceEngineForegroundService.class), connection.getConnection(), 1);
            } catch (Exception e) {
                AppLog.g.v(VoiceEngineForegroundService.LOG_TAG, "Unable to bind service connection.", e);
            }
        }

        public final void startStream(Connection connection, Intent intent) {
            m.checkNotNullParameter(connection, "connection");
            m.checkNotNullParameter(intent, "permissionIntent");
            Context context = connection.getContext();
            Intent intent2 = new Intent(context, VoiceEngineForegroundService.class);
            intent2.setAction("com.discord.utilities.voice.action.start_stream");
            intent2.putExtra("android.intent.extra.INTENT", intent);
            context.startService(intent2);
        }

        public final void stopForegroundAndUnbind(Connection connection) {
            m.checkNotNullParameter(connection, "connection");
            try {
                if (connection.getService() != null && !connection.isUnbinding()) {
                    Logger.v$default(AppLog.g, VoiceEngineForegroundService.LOG_TAG, "Unbind service connection.", null, 4, null);
                    connection.setUnbinding(true);
                    Context context = connection.getContext();
                    Intent intent = new Intent(connection.getContext(), VoiceEngineForegroundService.class);
                    intent.setAction("com.discord.utilities.voice.action.stop");
                    context.startService(intent);
                    connection.getContext().unbindService(connection.getConnection());
                }
            } catch (Exception e) {
                AppLog.g.v(VoiceEngineForegroundService.LOG_TAG, "Unable to unbind service connection.", e);
            }
        }

        public final void stopStream(Connection connection) {
            m.checkNotNullParameter(connection, "connection");
            Context context = connection.getContext();
            Intent intent = new Intent(context, VoiceEngineForegroundService.class);
            intent.setAction("com.discord.utilities.voice.action.stop_stream");
            context.startService(intent);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: VoiceEngineForegroundService.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0018\u0010\u0019R\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0019\u0010\b\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR(\u0010\u000e\u001a\u0004\u0018\u00010\f2\b\u0010\r\u001a\u0004\u0018\u00010\f8\u0006@BX\u0086\u000e¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R*\u0010\u0013\u001a\u00020\u00122\u0006\u0010\r\u001a\u00020\u00128F@FX\u0086\u000e¢\u0006\u0012\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0013\u0010\u0015\"\u0004\b\u0016\u0010\u0017¨\u0006\u001a"}, d2 = {"Lcom/discord/utilities/voice/VoiceEngineForegroundService$Connection;", "", "Landroid/content/Context;", "context", "Landroid/content/Context;", "getContext", "()Landroid/content/Context;", "Landroid/content/ServiceConnection;", "connection", "Landroid/content/ServiceConnection;", "getConnection", "()Landroid/content/ServiceConnection;", "Lcom/discord/utilities/voice/VoiceEngineForegroundService;", "<set-?>", NotificationCompat.CATEGORY_SERVICE, "Lcom/discord/utilities/voice/VoiceEngineForegroundService;", "getService", "()Lcom/discord/utilities/voice/VoiceEngineForegroundService;", "", "isUnbinding", "Z", "()Z", "setUnbinding", "(Z)V", HookHelper.constructorName, "(Landroid/content/Context;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Connection {
        private final ServiceConnection connection = new ServiceConnection() { // from class: com.discord.utilities.voice.VoiceEngineForegroundService$Connection$connection$1
            @Override // android.content.ServiceConnection
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                m.checkNotNullParameter(componentName, "className");
                m.checkNotNullParameter(iBinder, "binder");
                VoiceEngineForegroundService.Connection.this.service = ((VoiceEngineForegroundService.LocalBinder) iBinder).getService();
                VoiceEngineForegroundService.Connection.this.setUnbinding(false);
            }

            @Override // android.content.ServiceConnection
            public void onServiceDisconnected(ComponentName componentName) {
                m.checkNotNullParameter(componentName, "className");
                VoiceEngineForegroundService.Connection.this.setUnbinding(false);
                VoiceEngineForegroundService.Connection.this.service = null;
            }
        };
        private final Context context;
        private boolean isUnbinding;
        private VoiceEngineForegroundService service;

        public Connection(Context context) {
            m.checkNotNullParameter(context, "context");
            this.context = context;
        }

        public final ServiceConnection getConnection() {
            return this.connection;
        }

        public final Context getContext() {
            return this.context;
        }

        public final VoiceEngineForegroundService getService() {
            return this.service;
        }

        public final synchronized boolean isUnbinding() {
            return this.isUnbinding;
        }

        public final synchronized void setUnbinding(boolean z2) {
            this.isUnbinding = z2;
        }
    }

    /* compiled from: VoiceEngineForegroundService.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0002\u0018\u00002\u00020\u0001B\u0011\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0007\u0010\bR\u001b\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/utilities/voice/VoiceEngineForegroundService$LocalBinder;", "Landroid/os/Binder;", "Lcom/discord/utilities/voice/VoiceEngineForegroundService;", NotificationCompat.CATEGORY_SERVICE, "Lcom/discord/utilities/voice/VoiceEngineForegroundService;", "getService", "()Lcom/discord/utilities/voice/VoiceEngineForegroundService;", HookHelper.constructorName, "(Lcom/discord/utilities/voice/VoiceEngineForegroundService;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class LocalBinder extends Binder {
        private final VoiceEngineForegroundService service;

        public LocalBinder(VoiceEngineForegroundService voiceEngineForegroundService) {
            this.service = voiceEngineForegroundService;
        }

        public final VoiceEngineForegroundService getService() {
            return this.service;
        }
    }

    public VoiceEngineForegroundService() {
        super("VoiceEngineForegroundService");
        PublishSubject k0 = PublishSubject.k0();
        m.checkNotNullExpressionValue(k0, "PublishSubject.create()");
        this.unsubscribeSignal = k0;
    }

    private final void ackStageInvite(Intent intent, boolean z2) {
        long longExtra = intent.getLongExtra("com.discord.utilities.voice.extra.channel_id", 0L);
        Observable<Void> ackInvitationToSpeak = StageChannelAPI.INSTANCE.ackInvitationToSpeak(longExtra, z2);
        if (ackInvitationToSpeak != null) {
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(ObservableExtensionsKt.restSubscribeOn$default(ackInvitationToSpeak, false, 1, null)), VoiceEngineForegroundService.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new VoiceEngineForegroundService$ackStageInvite$1(longExtra), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new VoiceEngineForegroundService$ackStageInvite$2(this, z2, longExtra));
            return;
        }
        Logger.w$default(AppLog.g, LOG_TAG, a.s("Unable to ack stage invite for unknown channel ", longExtra), null, 4, null);
    }

    @Override // com.discord.app.AppComponent
    public Subject<Void, Void> getUnsubscribeSignal() {
        return this.unsubscribeSignal;
    }

    @Override // android.app.IntentService, android.app.Service
    public IBinder onBind(Intent intent) {
        m.checkNotNullParameter(intent, "intent");
        return this.binder;
    }

    @Override // android.app.IntentService, android.app.Service
    public void onCreate() {
        super.onCreate();
        Logger.v$default(AppLog.g, LOG_TAG, "Service created.", null, 4, null);
        l.c.a(this);
        Application application = getApplication();
        m.checkNotNullExpressionValue(application, "application");
        WifiManager.WifiLock createWifiLock$default = SystemServiceExtensionsKt.createWifiLock$default(application, false, 0, null, 7, null);
        this.wakeLockWifi = createWifiLock$default;
        if (createWifiLock$default != null) {
            createWifiLock$default.acquire();
        }
        Application application2 = getApplication();
        m.checkNotNullExpressionValue(application2, "application");
        PowerManager.WakeLock createPartialWakeLock$default = SystemServiceExtensionsKt.createPartialWakeLock$default(application2, false, null, 3, null);
        this.wakeLockPartial = createPartialWakeLock$default;
        if (createPartialWakeLock$default != null) {
            createPartialWakeLock$default.acquire(WAKELOCK_TIMEOUT);
        }
        Application application3 = getApplication();
        m.checkNotNullExpressionValue(application3, "application");
        PowerManager.WakeLock createProximityScreenWakeLock$default = SystemServiceExtensionsKt.createProximityScreenWakeLock$default(application3, false, null, 3, null);
        this.wakeLockProximity = createProximityScreenWakeLock$default;
        if (createProximityScreenWakeLock$default != null) {
            createProximityScreenWakeLock$default.acquire(WAKELOCK_TIMEOUT);
        }
    }

    @Override // android.app.IntentService, android.app.Service
    public void onDestroy() {
        super.onDestroy();
        Logger.v$default(AppLog.g, LOG_TAG, "Service destroyed.", null, 4, null);
        StageChannelNotifications.Companion.getINSTANCE().onInviteToSpeakRescinded();
        l.c.b(this);
        WifiManager.WifiLock wifiLock = this.wakeLockWifi;
        if (wifiLock != null) {
            wifiLock.release();
        }
        PowerManager.WakeLock wakeLock = this.wakeLockPartial;
        if (wakeLock != null) {
            wakeLock.release();
        }
        PowerManager.WakeLock wakeLock2 = this.wakeLockProximity;
        if (wakeLock2 != null) {
            wakeLock2.release();
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:38:0x00af, code lost:
        if ((!d0.z.d.m.areEqual(r0 != null ? r0.getGuildId() : null, r4)) != false) goto L39;
     */
    /* JADX WARN: Removed duplicated region for block: B:46:0x00ee  */
    /* JADX WARN: Removed duplicated region for block: B:47:0x00f1  */
    /* JADX WARN: Removed duplicated region for block: B:50:0x00f9  */
    /* JADX WARN: Removed duplicated region for block: B:51:0x00fe  */
    @Override // android.app.IntentService
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void onHandleIntent(android.content.Intent r22) {
        /*
            Method dump skipped, instructions count: 468
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.voice.VoiceEngineForegroundService.onHandleIntent(android.content.Intent):void");
    }

    @Override // android.app.Service
    public boolean onUnbind(Intent intent) {
        getUnsubscribeSignal().onNext(null);
        return super.onUnbind(intent);
    }
}
