package com.discord.utilities.voice;

import android.content.Context;
import b.a.d.m;
import com.discord.stores.StoreMediaSettings;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import xyz.discord.R;
/* compiled from: VoiceEngineServiceController.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class VoiceEngineServiceController$init$3 extends o implements Function0<Unit> {
    public final /* synthetic */ Context $context;
    public final /* synthetic */ VoiceEngineServiceController this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public VoiceEngineServiceController$init$3(VoiceEngineServiceController voiceEngineServiceController, Context context) {
        super(0);
        this.this$0 = voiceEngineServiceController;
        this.$context = context;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreMediaSettings storeMediaSettings;
        storeMediaSettings = this.this$0.mediaSettingsStore;
        StoreMediaSettings.SelfMuteFailure selfMuteFailure = storeMediaSettings.toggleSelfMuted();
        if (selfMuteFailure != null && selfMuteFailure.ordinal() == 0) {
            m.g(this.$context, R.string.vad_permission_small, 0, null, 12);
        }
    }
}
