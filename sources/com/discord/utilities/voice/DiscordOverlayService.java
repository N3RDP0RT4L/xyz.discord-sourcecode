package com.discord.utilities.voice;

import andhook.lib.HookHelper;
import android.app.Application;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import androidx.core.app.NotificationCompat;
import b.a.n.c;
import b.a.n.d;
import b.a.y.w;
import b.a.y.z;
import b.d.b.a.a;
import com.discord.app.AppLog;
import com.discord.overlay.OverlayManager;
import com.discord.overlay.OverlayService;
import com.discord.overlay.views.OverlayBubbleWrap;
import com.discord.stores.StoreStream;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.device.DeviceUtils;
import com.discord.utilities.extensions.PendingIntentExtensionsKt;
import com.discord.utilities.fcm.NotificationClient;
import com.discord.utilities.intent.IntentUtils;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.views.OverlayMenuBubbleDialog;
import d0.z.d.m;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import xyz.discord.R;
/* compiled from: DiscordOverlayService.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \u00192\u00020\u0001:\u0001\u0019B\u0007¢\u0006\u0004\b\u0018\u0010\u000bJ\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0004J\u000f\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\n\u001a\u00020\tH\u0016¢\u0006\u0004\b\n\u0010\u000bJ)\u0010\u0011\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\f2\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u000eH\u0016¢\u0006\u0004\b\u0011\u0010\u0012J\u0019\u0010\u0013\u001a\u0004\u0018\u00010\u00022\u0006\u0010\r\u001a\u00020\fH\u0014¢\u0006\u0004\b\u0013\u0010\u0014J\u0019\u0010\u0016\u001a\u00020\u00152\b\u0010\r\u001a\u0004\u0018\u00010\fH\u0014¢\u0006\u0004\b\u0016\u0010\u0017¨\u0006\u001a"}, d2 = {"Lcom/discord/utilities/voice/DiscordOverlayService;", "Lcom/discord/overlay/OverlayService;", "Lcom/discord/overlay/views/OverlayBubbleWrap;", "createVoiceSelector", "()Lcom/discord/overlay/views/OverlayBubbleWrap;", "createVoiceBubble", "Lcom/discord/views/OverlayMenuBubbleDialog;", "createMenu", "()Lcom/discord/views/OverlayMenuBubbleDialog;", "", "onCreate", "()V", "Landroid/content/Intent;", "intent", "", "flags", "startId", "onStartCommand", "(Landroid/content/Intent;II)I", "createOverlayBubble", "(Landroid/content/Intent;)Lcom/discord/overlay/views/OverlayBubbleWrap;", "Landroid/app/Notification;", "createNotification", "(Landroid/content/Intent;)Landroid/app/Notification;", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class DiscordOverlayService extends OverlayService {
    private static final String ACTION_CLOSE = "com.discord.actions.OVERLAY_CLOSE";
    private static final String ACTION_OPEN = "com.discord.actions.OVERLAY_OPEN";
    private static final String ACTION_SELECTOR = "com.discord.actions.OVERLAY_SELECTOR";
    private static final String ACTION_VOICE = "com.discord.actions.OVERLAY_VOICE";
    private static final int CLOSE_INTENT_REQ_CODE = 1010;
    public static final Companion Companion = new Companion(null);
    private static final String LOG_TAG = "OverlayService";

    /* compiled from: DiscordOverlayService.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001c\u0010\u001dJ)\u0010\t\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\f\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\f\u0010\rJ\u0015\u0010\u000e\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u0015\u0010\u0010\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0010\u0010\u000fJ\u0015\u0010\u0011\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0011\u0010\u000fJ\u0015\u0010\u0012\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0012\u0010\u000fR\u0016\u0010\u0013\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0015\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0015\u0010\u0014R\u0016\u0010\u0016\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0016\u0010\u0014R\u0016\u0010\u0017\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0017\u0010\u0014R\u0016\u0010\u0019\u001a\u00020\u00188\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001b\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001b\u0010\u0014¨\u0006\u001e"}, d2 = {"Lcom/discord/utilities/voice/DiscordOverlayService$Companion;", "", "Landroid/content/Context;", "context", "", "action", "", "checkEnabled", "", "tryStartOverlayService", "(Landroid/content/Context;Ljava/lang/String;Z)V", "Landroid/content/Intent;", "createOverlayIntent", "(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;", "launchForConnect", "(Landroid/content/Context;)V", "launchForVoice", "launchForVoiceChannelSelect", "launchForClose", "ACTION_CLOSE", "Ljava/lang/String;", "ACTION_OPEN", "ACTION_SELECTOR", "ACTION_VOICE", "", "CLOSE_INTENT_REQ_CODE", "I", "LOG_TAG", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Intent createOverlayIntent(Context context, String str) {
            return new Intent(str, null, context, DiscordOverlayService.class);
        }

        private final void tryStartOverlayService(Context context, String str, boolean z2) {
            if (StoreStream.Companion.getUserSettings().getIsMobileOverlayEnabled() || !z2) {
                try {
                    context.startService(createOverlayIntent(context, str));
                } catch (Exception e) {
                    AppLog.g.v(DiscordOverlayService.LOG_TAG, "Overlay request failed.", e);
                }
            }
        }

        public static /* synthetic */ void tryStartOverlayService$default(Companion companion, Context context, String str, boolean z2, int i, Object obj) {
            if ((i & 4) != 0) {
                z2 = true;
            }
            companion.tryStartOverlayService(context, str, z2);
        }

        public final void launchForClose(Context context) {
            m.checkNotNullParameter(context, "context");
            tryStartOverlayService(context, DiscordOverlayService.ACTION_CLOSE, false);
        }

        public final void launchForConnect(Context context) {
            m.checkNotNullParameter(context, "context");
            StoreStream.Companion companion = StoreStream.Companion;
            if (companion.getUserSettings().getIsMobileOverlayEnabled()) {
                WeakReference weakReference = new WeakReference(context);
                Observable F = ObservableExtensionsKt.takeSingleUntilTimeout$default(companion.getRtcConnection().getConnectionState(), 1000L, false, 2, null).F(DiscordOverlayService$Companion$launchForConnect$1.INSTANCE);
                m.checkNotNullExpressionValue(F, "StoreStream\n            …          }\n            }");
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(F), DiscordOverlayService.class, (r18 & 2) != 0 ? null : context, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new DiscordOverlayService$Companion$launchForConnect$2(weakReference));
                return;
            }
            b.a.d.m.g(context, R.string.overlay_mobile_required, 1, null, 8);
        }

        public final void launchForVoice(Context context) {
            m.checkNotNullParameter(context, "context");
            tryStartOverlayService$default(this, context, DiscordOverlayService.ACTION_VOICE, false, 4, null);
        }

        public final void launchForVoiceChannelSelect(Context context) {
            m.checkNotNullParameter(context, "context");
            tryStartOverlayService$default(this, context, DiscordOverlayService.ACTION_SELECTOR, false, 4, null);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final OverlayMenuBubbleDialog createMenu() {
        Context applicationContext = getApplicationContext();
        m.checkNotNullExpressionValue(applicationContext, "applicationContext");
        OverlayMenuBubbleDialog overlayMenuBubbleDialog = new OverlayMenuBubbleDialog(applicationContext);
        overlayMenuBubbleDialog.getInsetMargins().top = overlayMenuBubbleDialog.getResources().getDimensionPixelOffset(R.dimen.vertical_safe_margin);
        overlayMenuBubbleDialog.setOnDialogClosed(new DiscordOverlayService$createMenu$1(this));
        return overlayMenuBubbleDialog;
    }

    private final OverlayBubbleWrap createVoiceBubble() {
        int dimensionPixelOffset = getResources().getDimensionPixelOffset(R.dimen.overlay_safe_margin);
        Context applicationContext = getApplicationContext();
        m.checkNotNullExpressionValue(applicationContext, "applicationContext");
        w wVar = new w(applicationContext);
        int i = -dimensionPixelOffset;
        wVar.getInsetMargins().set(i, dimensionPixelOffset, i, dimensionPixelOffset);
        OverlayManager overlayManager = getOverlayManager();
        DiscordOverlayService$createVoiceBubble$1 discordOverlayService$createVoiceBubble$1 = new DiscordOverlayService$createVoiceBubble$1(this, "Active Voice Bubble");
        Objects.requireNonNull(overlayManager);
        m.checkNotNullParameter(wVar, "srcBubble");
        m.checkNotNullParameter("Active Voice Bubble", "anchorViewTag");
        m.checkNotNullParameter(discordOverlayService$createVoiceBubble$1, "menuBubbleProvider");
        wVar.setOnClickListener(new d(overlayManager, discordOverlayService$createVoiceBubble$1, wVar, "Active Voice Bubble"));
        OverlayManager overlayManager2 = getOverlayManager();
        Objects.requireNonNull(overlayManager2);
        m.checkNotNullParameter(wVar, "bubble");
        wVar.setOnTouchListener(overlayManager2.p);
        wVar.setOnMovingStateChanged(new c(overlayManager2, wVar));
        return wVar;
    }

    private final OverlayBubbleWrap createVoiceSelector() {
        Context applicationContext = getApplicationContext();
        m.checkNotNullExpressionValue(applicationContext, "applicationContext");
        z zVar = new z(applicationContext);
        zVar.setOnDialogClosed(new DiscordOverlayService$createVoiceSelector$1(this));
        return zVar;
    }

    @Override // com.discord.overlay.OverlayService
    public Notification createNotification(Intent intent) {
        PendingIntent service = PendingIntent.getService(this, 1010, Companion.createOverlayIntent(this, ACTION_CLOSE), PendingIntentExtensionsKt.immutablePendingIntentFlag(134217728));
        Notification build = new NotificationCompat.Builder(this, NotificationClient.NOTIF_CHANNEL_MEDIA_CONNECTIONS).setCategory(NotificationCompat.CATEGORY_SERVICE).setPriority(-2).setOnlyAlertOnce(true).setLocalOnly(true).setSmallIcon(R.drawable.ic_notification_24dp).setColor(ColorCompat.getColor(this, (int) R.color.status_green_600)).setContentTitle(getString(R.string.overlay)).setContentText(getString(R.string.overlay_mobile_toggle_desc)).setOngoing(true).addAction(R.drawable.ic_close_grey_24dp, getString(R.string.close), service).addAction(R.drawable.ic_settings_grey_a60_24dp, getString(R.string.settings), PendingIntent.getActivity(this, 1010, new Intent("android.intent.action.VIEW", IntentUtils.RouteBuilders.Uris.INSTANCE.getSelectSettingsVoice()).setPackage(getPackageName()), PendingIntentExtensionsKt.immutablePendingIntentFlag(134217728))).build();
        m.checkNotNullExpressionValue(build, "NotificationCompat.Build…       )\n        .build()");
        return build;
    }

    @Override // com.discord.overlay.OverlayService
    public OverlayBubbleWrap createOverlayBubble(Intent intent) {
        Object obj;
        Object obj2;
        m.checkNotNullParameter(intent, "intent");
        String action = intent.getAction();
        if (action == null) {
            return null;
        }
        int hashCode = action.hashCode();
        if (hashCode != -753952221) {
            if (hashCode != -440170727) {
                if (hashCode == 557534510 && action.equals(ACTION_SELECTOR)) {
                    return createVoiceSelector();
                }
                return null;
            } else if (!action.equals(ACTION_OPEN)) {
                return null;
            } else {
                Iterator<T> it = getOverlayManager().k.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj2 = null;
                        break;
                    }
                    obj2 = it.next();
                    if (((OverlayBubbleWrap) obj2) instanceof w) {
                        break;
                    }
                }
                OverlayBubbleWrap overlayBubbleWrap = (OverlayBubbleWrap) obj2;
                if (overlayBubbleWrap == null) {
                    return createVoiceBubble();
                }
                overlayBubbleWrap.performClick();
                return null;
            }
        } else if (!action.equals(ACTION_VOICE)) {
            return null;
        } else {
            Iterator<T> it2 = getOverlayManager().k.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it2.next();
                if (((OverlayBubbleWrap) obj) instanceof w) {
                    break;
                }
            }
            if (((OverlayBubbleWrap) obj) != null) {
                return null;
            }
            return createVoiceBubble();
        }
    }

    @Override // com.discord.overlay.OverlayService, android.app.Service
    public void onCreate() {
        super.onCreate();
        StoreStream.Companion companion = StoreStream.Companion;
        Application application = getApplication();
        m.checkNotNullExpressionValue(application, "application");
        companion.initialize(application);
        OverlayManager overlayManager = getOverlayManager();
        DiscordOverlayService$onCreate$1 discordOverlayService$onCreate$1 = DiscordOverlayService$onCreate$1.INSTANCE;
        Objects.requireNonNull(overlayManager);
        m.checkNotNullParameter(discordOverlayService$onCreate$1, "<set-?>");
        overlayManager.l = discordOverlayService$onCreate$1;
        OverlayManager overlayManager2 = getOverlayManager();
        DiscordOverlayService$onCreate$2 discordOverlayService$onCreate$2 = new DiscordOverlayService$onCreate$2(this);
        Objects.requireNonNull(overlayManager2);
        m.checkNotNullParameter(discordOverlayService$onCreate$2, "<set-?>");
        overlayManager2.m = discordOverlayService$onCreate$2;
    }

    @Override // android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        AppLog appLog = AppLog.g;
        StringBuilder R = a.R("onStartCommand: ");
        String str = null;
        R.append(intent != null ? intent.getAction() : null);
        Logger.v$default(appLog, LOG_TAG, R.toString(), null, 4, null);
        if (intent != null) {
            str = intent.getAction();
        }
        if (m.areEqual(str, ACTION_CLOSE) || !DeviceUtils.INSTANCE.canDrawOverlays(this)) {
            stopForeground(true);
            stopSelf(i2);
            return 2;
        }
        Observable<Boolean> Z = StoreStream.Companion.isInitializedObservable().x(DiscordOverlayService$onStartCommand$1.INSTANCE).Z(1);
        m.checkNotNullExpressionValue(Z, "StoreStream.isInitialize….takeFirst { it == true }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(Z), DiscordOverlayService.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new DiscordOverlayService$onStartCommand$2(this, i2, intent));
        return 3;
    }
}
