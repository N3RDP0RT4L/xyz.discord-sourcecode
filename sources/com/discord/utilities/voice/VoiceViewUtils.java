package com.discord.utilities.voice;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Build;
import androidx.annotation.DrawableRes;
import b.a.k.b;
import com.discord.rtcconnection.RtcConnection;
import com.discord.utilities.streams.StreamContext;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import xyz.discord.R;
/* compiled from: VoiceViewUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\n\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0007¢\u0006\u0004\b\u0005\u0010\u0006J\u001f\u0010\u000b\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00072\b\u0010\n\u001a\u0004\u0018\u00010\t¢\u0006\u0004\b\u000b\u0010\fJ3\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u000e\u001a\u00020\r2\b\u0010\u0010\u001a\u0004\u0018\u00010\u000f2\b\u0010\n\u001a\u0004\u0018\u00010\t2\b\b\u0002\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\u0012\u0010\u0013J+\u0010\u0015\u001a\u00020\u00042\b\u0010\u0014\u001a\u0004\u0018\u00010\u000f2\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u000e\u001a\u00020\rH\u0007¢\u0006\u0004\b\u0015\u0010\u0016J\r\u0010\u0017\u001a\u00020\u0007¢\u0006\u0004\b\u0017\u0010\u0018¨\u0006\u001b"}, d2 = {"Lcom/discord/utilities/voice/VoiceViewUtils;", "", "Lcom/discord/rtcconnection/RtcConnection$Quality;", "connectionQuality", "", "getQualityIndicator", "(Lcom/discord/rtcconnection/RtcConnection$Quality;)I", "", "hasVideo", "Lcom/discord/utilities/streams/StreamContext;", "streamContext", "getCallIndicatorIcon", "(ZLcom/discord/utilities/streams/StreamContext;)I", "Landroid/content/Context;", "context", "Lcom/discord/rtcconnection/RtcConnection$State;", "rtcConnectionState", "", "getConnectedText", "(Landroid/content/Context;Lcom/discord/rtcconnection/RtcConnection$State;Lcom/discord/utilities/streams/StreamContext;Z)Ljava/lang/CharSequence;", "connectionState", "getConnectionStatusColor", "(Lcom/discord/rtcconnection/RtcConnection$State;Lcom/discord/rtcconnection/RtcConnection$Quality;Landroid/content/Context;)I", "getIsSoundshareSupported", "()Z", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class VoiceViewUtils {
    public static final VoiceViewUtils INSTANCE = new VoiceViewUtils();

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            RtcConnection.Quality.values();
            int[] iArr = new int[4];
            $EnumSwitchMapping$0 = iArr;
            RtcConnection.Quality quality = RtcConnection.Quality.BAD;
            iArr[quality.ordinal()] = 1;
            RtcConnection.Quality quality2 = RtcConnection.Quality.AVERAGE;
            iArr[quality2.ordinal()] = 2;
            RtcConnection.Quality quality3 = RtcConnection.Quality.FINE;
            iArr[quality3.ordinal()] = 3;
            RtcConnection.Quality quality4 = RtcConnection.Quality.UNKNOWN;
            iArr[quality4.ordinal()] = 4;
            RtcConnection.Quality.values();
            int[] iArr2 = new int[4];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[quality.ordinal()] = 1;
            iArr2[quality2.ordinal()] = 2;
            iArr2[quality3.ordinal()] = 3;
            iArr2[quality4.ordinal()] = 4;
        }
    }

    private VoiceViewUtils() {
    }

    public static /* synthetic */ CharSequence getConnectedText$default(VoiceViewUtils voiceViewUtils, Context context, RtcConnection.State state, StreamContext streamContext, boolean z2, int i, Object obj) {
        if ((i & 8) != 0) {
            z2 = false;
        }
        return voiceViewUtils.getConnectedText(context, state, streamContext, z2);
    }

    public final int getCallIndicatorIcon(boolean z2, StreamContext streamContext) {
        return (streamContext == null || !streamContext.isCurrentUserParticipating()) ? z2 ? R.drawable.ic_videocam_white_16dp : R.drawable.ic_call_indicator_voice_16dp : streamContext.isSelfStream() ? R.drawable.ic_call_indicator_mobile_screenshare_16dp : R.drawable.ic_call_indicator_streaming_16dp;
    }

    public final CharSequence getConnectedText(Context context, RtcConnection.State state, StreamContext streamContext, boolean z2) {
        int i;
        CharSequence b2;
        CharSequence b3;
        m.checkNotNullParameter(context, "context");
        RtcConnection.State.f fVar = RtcConnection.State.f.a;
        if (state != fVar || streamContext == null || !streamContext.isCurrentUserParticipating()) {
            if (m.areEqual(state, RtcConnection.State.c.a)) {
                i = R.string.connection_status_connecting;
            } else if (m.areEqual(state, RtcConnection.State.e.a)) {
                i = R.string.connection_status_no_route;
            } else if (m.areEqual(state, RtcConnection.State.b.a)) {
                i = R.string.connection_status_awaiting_endpoint;
            } else if (m.areEqual(state, RtcConnection.State.a.a)) {
                i = R.string.connection_status_authenticating;
            } else if (m.areEqual(state, RtcConnection.State.g.a)) {
                i = R.string.connection_status_rtc_connecting;
            } else if (m.areEqual(state, fVar)) {
                i = z2 ? R.string.connection_status_video_connected : R.string.connection_status_voice_connected;
            } else if (!(state instanceof RtcConnection.State.d) && !m.areEqual(state, RtcConnection.State.h.a) && state != null) {
                throw new NoWhenBranchMatchedException();
            } else {
                i = R.string.connection_status_disconnected;
            }
            String string = context.getString(i);
            m.checkNotNullExpressionValue(string, "context.getString(resId)");
            return string;
        } else if (streamContext.isSelfStream()) {
            b3 = b.b(context, R.string.connection_status_stream_self_connected, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            return b3;
        } else {
            Object[] objArr = new Object[1];
            String userNickname = streamContext.getUserNickname();
            if (userNickname == null) {
                userNickname = streamContext.getUser().getUsername();
            }
            objArr[0] = userNickname;
            b2 = b.b(context, R.string.connection_status_stream_connected, objArr, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b2;
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:13:0x0026, code lost:
        if (r5 != 3) goto L14;
     */
    @androidx.annotation.ColorInt
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final int getConnectionStatusColor(com.discord.rtcconnection.RtcConnection.State r4, com.discord.rtcconnection.RtcConnection.Quality r5, android.content.Context r6) {
        /*
            r3 = this;
            java.lang.String r0 = "context"
            d0.z.d.m.checkNotNullParameter(r6, r0)
            com.discord.rtcconnection.RtcConnection$State$f r0 = com.discord.rtcconnection.RtcConnection.State.f.a
            boolean r0 = d0.z.d.m.areEqual(r4, r0)
            r1 = 2131100379(0x7f0602db, float:1.7813138E38)
            r2 = 2131100352(0x7f0602c0, float:1.7813083E38)
            if (r0 == 0) goto L3c
            r4 = 2131100277(0x7f060275, float:1.781293E38)
            if (r5 != 0) goto L19
            goto L28
        L19:
            int r5 = r5.ordinal()
            if (r5 == 0) goto L37
            r0 = 1
            if (r5 == r0) goto L32
            r0 = 2
            if (r5 == r0) goto L2d
            r0 = 3
            if (r5 == r0) goto L37
        L28:
            int r4 = com.discord.utilities.color.ColorCompat.getColor(r6, r4)
            goto L3b
        L2d:
            int r4 = com.discord.utilities.color.ColorCompat.getColor(r6, r1)
            goto L3b
        L32:
            int r4 = com.discord.utilities.color.ColorCompat.getColor(r6, r2)
            goto L3b
        L37:
            int r4 = com.discord.utilities.color.ColorCompat.getColor(r6, r4)
        L3b:
            return r4
        L3c:
            com.discord.rtcconnection.RtcConnection$State$b r5 = com.discord.rtcconnection.RtcConnection.State.b.a
            boolean r5 = d0.z.d.m.areEqual(r4, r5)
            if (r5 == 0) goto L45
            goto L5f
        L45:
            com.discord.rtcconnection.RtcConnection$State$c r5 = com.discord.rtcconnection.RtcConnection.State.c.a
            boolean r5 = d0.z.d.m.areEqual(r4, r5)
            if (r5 == 0) goto L4e
            goto L5f
        L4e:
            com.discord.rtcconnection.RtcConnection$State$a r5 = com.discord.rtcconnection.RtcConnection.State.a.a
            boolean r5 = d0.z.d.m.areEqual(r4, r5)
            if (r5 == 0) goto L57
            goto L5f
        L57:
            com.discord.rtcconnection.RtcConnection$State$g r5 = com.discord.rtcconnection.RtcConnection.State.g.a
            boolean r5 = d0.z.d.m.areEqual(r4, r5)
            if (r5 == 0) goto L64
        L5f:
            int r4 = com.discord.utilities.color.ColorCompat.getColor(r6, r1)
            goto L87
        L64:
            com.discord.rtcconnection.RtcConnection$State$e r5 = com.discord.rtcconnection.RtcConnection.State.e.a
            boolean r5 = d0.z.d.m.areEqual(r4, r5)
            if (r5 == 0) goto L71
            int r4 = com.discord.utilities.color.ColorCompat.getColor(r6, r2)
            goto L87
        L71:
            boolean r5 = r4 instanceof com.discord.rtcconnection.RtcConnection.State.d
            if (r5 == 0) goto L76
            goto L7e
        L76:
            com.discord.rtcconnection.RtcConnection$State$h r5 = com.discord.rtcconnection.RtcConnection.State.h.a
            boolean r4 = d0.z.d.m.areEqual(r4, r5)
            if (r4 == 0) goto L83
        L7e:
            int r4 = com.discord.utilities.color.ColorCompat.getColor(r6, r2)
            goto L87
        L83:
            int r4 = com.discord.utilities.color.ColorCompat.getColor(r6, r2)
        L87:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.voice.VoiceViewUtils.getConnectionStatusColor(com.discord.rtcconnection.RtcConnection$State, com.discord.rtcconnection.RtcConnection$Quality, android.content.Context):int");
    }

    public final boolean getIsSoundshareSupported() {
        return Build.VERSION.SDK_INT >= 29;
    }

    @DrawableRes
    public final int getQualityIndicator(RtcConnection.Quality quality) {
        int ordinal;
        if (quality == null || (ordinal = quality.ordinal()) == 0) {
            return R.drawable.ic_voice_quality_unknown;
        }
        return ordinal != 1 ? ordinal != 2 ? ordinal != 3 ? R.drawable.ic_voice_quality_unknown : R.drawable.ic_voice_quality_fine : R.drawable.ic_voice_quality_average : R.drawable.ic_voice_quality_bad;
    }
}
