package com.discord.utilities.voice;

import andhook.lib.HookHelper;
import android.content.Intent;
import android.graphics.Bitmap;
import androidx.annotation.MainThread;
import b.d.b.a.a;
import com.discord.app.AppComponent;
import com.discord.rtcconnection.RtcConnection;
import com.discord.rtcconnection.mediaengine.ThumbnailEmitter;
import com.discord.stores.StoreApplicationStreaming;
import com.discord.stores.StoreRtcConnection;
import com.discord.stores.StoreStreamRtcConnection;
import com.discord.stores.StoreUser;
import com.discord.tooltips.TooltipManager;
import com.discord.utilities.images.ImageEncoder;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.voice.ScreenShareManager;
import d0.z.d.m;
import j0.k.b;
import j0.l.a.c;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subscriptions.CompositeSubscription;
/* compiled from: ScreenShareManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0090\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 D2\u00020\u0001:\u0003DEFBq\u0012\u0006\u0010/\u001a\u00020.\u0012\n\u0010\u0018\u001a\u00060\u0016j\u0002`\u0017\u0012\u000e\u0010>\u001a\n\u0018\u00010\u0016j\u0004\u0018\u0001`=\u0012\b\b\u0002\u0010&\u001a\u00020%\u0012\b\b\u0002\u0010;\u001a\u00020:\u0012\b\b\u0002\u0010,\u001a\u00020+\u0012\b\b\u0002\u0010 \u001a\u00020\u001f\u0012\b\b\u0002\u0010#\u001a\u00020\"\u0012\b\b\u0002\u0010)\u001a\u00020(\u0012\b\b\u0002\u00108\u001a\u000207¢\u0006\u0004\bB\u0010CJ\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u000b\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\tH\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u000f\u0010\r\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\r\u0010\u0004J\u000f\u0010\u000f\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u0015\u0010\u0011\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\t¢\u0006\u0004\b\u0011\u0010\fJ\r\u0010\u0012\u001a\u00020\u0002¢\u0006\u0004\b\u0012\u0010\u0004J\r\u0010\u0013\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0004R\u0018\u0010\u0014\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u001d\u0010\u0018\u001a\u00060\u0016j\u0002`\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u0016\u0010\u001d\u001a\u00020\u001c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u0016\u0010 \u001a\u00020\u001f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b \u0010!R\u0016\u0010#\u001a\u00020\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$R\u0016\u0010&\u001a\u00020%8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b&\u0010'R\u0016\u0010)\u001a\u00020(8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b)\u0010*R\u0016\u0010,\u001a\u00020+8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b,\u0010-R\u0016\u0010/\u001a\u00020.8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b/\u00100R\u0018\u00101\u001a\u0004\u0018\u00010\t8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b1\u00102R\u001c\u00105\u001a\b\u0012\u0004\u0012\u000204038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b5\u00106R\u0016\u00108\u001a\u0002078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b8\u00109R\u0016\u0010;\u001a\u00020:8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b;\u0010<R!\u0010>\u001a\n\u0018\u00010\u0016j\u0004\u0018\u0001`=8\u0006@\u0006¢\u0006\f\n\u0004\b>\u0010?\u001a\u0004\b@\u0010A¨\u0006G"}, d2 = {"Lcom/discord/utilities/voice/ScreenShareManager;", "", "", "subscribeToStores", "()V", "Lcom/discord/utilities/voice/ScreenShareManager$State;", "state", "handleStateUpdate", "(Lcom/discord/utilities/voice/ScreenShareManager$State;)V", "Landroid/content/Intent;", "intent", "handleStartStream", "(Landroid/content/Intent;)V", "uploadScreenSharePreviews", "Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;", "createThumbnailEmitter", "()Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;", "startStream", "stopStream", "release", "previousState", "Lcom/discord/utilities/voice/ScreenShareManager$State;", "", "Lcom/discord/primitives/ChannelId;", "channelId", "J", "getChannelId", "()J", "Lrx/subscriptions/CompositeSubscription;", "compositeSubscription", "Lrx/subscriptions/CompositeSubscription;", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreUser;", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "Lcom/discord/stores/StoreApplicationStreaming;", "storeApplicationStreaming", "Lcom/discord/stores/StoreApplicationStreaming;", "Lcom/discord/utilities/images/ImageEncoder;", "imageEncoder", "Lcom/discord/utilities/images/ImageEncoder;", "Lcom/discord/stores/StoreStreamRtcConnection;", "storeStreamRtcConnection", "Lcom/discord/stores/StoreStreamRtcConnection;", "Lcom/discord/app/AppComponent;", "appComponent", "Lcom/discord/app/AppComponent;", "screenshareIntent", "Landroid/content/Intent;", "Lrx/subjects/BehaviorSubject;", "Landroid/graphics/Bitmap;", "thumbnailBitmapSubject", "Lrx/subjects/BehaviorSubject;", "Lcom/discord/tooltips/TooltipManager;", "tooltipManager", "Lcom/discord/tooltips/TooltipManager;", "Lcom/discord/stores/StoreRtcConnection;", "storeRtcConnection", "Lcom/discord/stores/StoreRtcConnection;", "Lcom/discord/primitives/GuildId;", "guildId", "Ljava/lang/Long;", "getGuildId", "()Ljava/lang/Long;", HookHelper.constructorName, "(Lcom/discord/app/AppComponent;JLjava/lang/Long;Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreRtcConnection;Lcom/discord/stores/StoreStreamRtcConnection;Lcom/discord/stores/StoreUser;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/utilities/images/ImageEncoder;Lcom/discord/tooltips/TooltipManager;)V", "Companion", "RtcConnectionListener", "State", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ScreenShareManager {
    public static final Companion Companion = new Companion(null);
    public static final int JPEG_QUALITY = 92;
    public static final long PREVIEW_DELAY_MS = 5000;
    public static final long PREVIEW_INTERVAL_MS = 300000;
    public static final int THUMBNAIL_HEIGHT_PX = 288;
    public static final int THUMBNAIL_WIDTH_PX = 512;
    private final AppComponent appComponent;
    private final long channelId;
    private final CompositeSubscription compositeSubscription;
    private final Long guildId;
    private final ImageEncoder imageEncoder;
    private State previousState;
    private final RestAPI restAPI;
    private Intent screenshareIntent;
    private final StoreApplicationStreaming storeApplicationStreaming;
    private final StoreRtcConnection storeRtcConnection;
    private final StoreStreamRtcConnection storeStreamRtcConnection;
    private final StoreUser storeUser;
    private final BehaviorSubject<Bitmap> thumbnailBitmapSubject;
    private final TooltipManager tooltipManager;

    /* compiled from: ScreenShareManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fR\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00058\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007R\u0016\u0010\b\u001a\u00020\u00058\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\b\u0010\u0007R\u0016\u0010\t\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\t\u0010\u0004R\u0016\u0010\n\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\n\u0010\u0004¨\u0006\r"}, d2 = {"Lcom/discord/utilities/voice/ScreenShareManager$Companion;", "", "", "JPEG_QUALITY", "I", "", "PREVIEW_DELAY_MS", "J", "PREVIEW_INTERVAL_MS", "THUMBNAIL_HEIGHT_PX", "THUMBNAIL_WIDTH_PX", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: ScreenShareManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/utilities/voice/ScreenShareManager$RtcConnectionListener;", "Lcom/discord/rtcconnection/RtcConnection$b;", "Lcom/discord/rtcconnection/RtcConnection$StateChange;", "stateChange", "", "onStateChange", "(Lcom/discord/rtcconnection/RtcConnection$StateChange;)V", HookHelper.constructorName, "(Lcom/discord/utilities/voice/ScreenShareManager;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final class RtcConnectionListener extends RtcConnection.b {
        public RtcConnectionListener() {
        }

        @Override // com.discord.rtcconnection.RtcConnection.b, com.discord.rtcconnection.RtcConnection.c
        public void onStateChange(RtcConnection.StateChange stateChange) {
            m.checkNotNullParameter(stateChange, "stateChange");
            RtcConnection.State state = stateChange.a;
            if (m.areEqual(state, RtcConnection.State.f.a)) {
                State state2 = ScreenShareManager.this.previousState;
                RtcConnection rtcConnection = state2 != null ? state2.getRtcConnection() : null;
                if (rtcConnection != null) {
                    rtcConnection.t(ScreenShareManager.this.screenshareIntent, ScreenShareManager.this.createThumbnailEmitter());
                    ScreenShareManager.this.uploadScreenSharePreviews();
                }
            } else if (state instanceof RtcConnection.State.d) {
                ScreenShareManager.this.stopStream();
            }
        }
    }

    /* compiled from: ScreenShareManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0005\u0012\u000e\u0010\u000e\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t¢\u0006\u0004\b!\u0010\"J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0018\u0010\n\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ:\u0010\u000f\u001a\u00020\u00002\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00052\u0010\b\u0002\u0010\u000e\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\tHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u001a\u0010\u0019\u001a\u00020\u00182\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0019\u0010\u001aR\u001b\u0010\f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001b\u001a\u0004\b\u001c\u0010\u0004R\u001b\u0010\r\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001d\u001a\u0004\b\u001e\u0010\u0007R!\u0010\u000e\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001f\u001a\u0004\b \u0010\u000b¨\u0006#"}, d2 = {"Lcom/discord/utilities/voice/ScreenShareManager$State;", "", "Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;", "component1", "()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;", "Lcom/discord/rtcconnection/RtcConnection;", "component2", "()Lcom/discord/rtcconnection/RtcConnection;", "", "Lcom/discord/primitives/UserId;", "component3", "()Ljava/lang/Long;", "activeStream", "rtcConnection", "meId", "copy", "(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lcom/discord/rtcconnection/RtcConnection;Ljava/lang/Long;)Lcom/discord/utilities/voice/ScreenShareManager$State;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;", "getActiveStream", "Lcom/discord/rtcconnection/RtcConnection;", "getRtcConnection", "Ljava/lang/Long;", "getMeId", HookHelper.constructorName, "(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lcom/discord/rtcconnection/RtcConnection;Ljava/lang/Long;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class State {
        private final StoreApplicationStreaming.ActiveApplicationStream activeStream;
        private final Long meId;
        private final RtcConnection rtcConnection;

        public State(StoreApplicationStreaming.ActiveApplicationStream activeApplicationStream, RtcConnection rtcConnection, Long l) {
            this.activeStream = activeApplicationStream;
            this.rtcConnection = rtcConnection;
            this.meId = l;
        }

        public static /* synthetic */ State copy$default(State state, StoreApplicationStreaming.ActiveApplicationStream activeApplicationStream, RtcConnection rtcConnection, Long l, int i, Object obj) {
            if ((i & 1) != 0) {
                activeApplicationStream = state.activeStream;
            }
            if ((i & 2) != 0) {
                rtcConnection = state.rtcConnection;
            }
            if ((i & 4) != 0) {
                l = state.meId;
            }
            return state.copy(activeApplicationStream, rtcConnection, l);
        }

        public final StoreApplicationStreaming.ActiveApplicationStream component1() {
            return this.activeStream;
        }

        public final RtcConnection component2() {
            return this.rtcConnection;
        }

        public final Long component3() {
            return this.meId;
        }

        public final State copy(StoreApplicationStreaming.ActiveApplicationStream activeApplicationStream, RtcConnection rtcConnection, Long l) {
            return new State(activeApplicationStream, rtcConnection, l);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof State)) {
                return false;
            }
            State state = (State) obj;
            return m.areEqual(this.activeStream, state.activeStream) && m.areEqual(this.rtcConnection, state.rtcConnection) && m.areEqual(this.meId, state.meId);
        }

        public final StoreApplicationStreaming.ActiveApplicationStream getActiveStream() {
            return this.activeStream;
        }

        public final Long getMeId() {
            return this.meId;
        }

        public final RtcConnection getRtcConnection() {
            return this.rtcConnection;
        }

        public int hashCode() {
            StoreApplicationStreaming.ActiveApplicationStream activeApplicationStream = this.activeStream;
            int i = 0;
            int hashCode = (activeApplicationStream != null ? activeApplicationStream.hashCode() : 0) * 31;
            RtcConnection rtcConnection = this.rtcConnection;
            int hashCode2 = (hashCode + (rtcConnection != null ? rtcConnection.hashCode() : 0)) * 31;
            Long l = this.meId;
            if (l != null) {
                i = l.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("State(activeStream=");
            R.append(this.activeStream);
            R.append(", rtcConnection=");
            R.append(this.rtcConnection);
            R.append(", meId=");
            return a.F(R, this.meId, ")");
        }
    }

    public ScreenShareManager(AppComponent appComponent, long j, Long l, StoreApplicationStreaming storeApplicationStreaming, StoreRtcConnection storeRtcConnection, StoreStreamRtcConnection storeStreamRtcConnection, StoreUser storeUser, RestAPI restAPI, ImageEncoder imageEncoder, TooltipManager tooltipManager) {
        m.checkNotNullParameter(appComponent, "appComponent");
        m.checkNotNullParameter(storeApplicationStreaming, "storeApplicationStreaming");
        m.checkNotNullParameter(storeRtcConnection, "storeRtcConnection");
        m.checkNotNullParameter(storeStreamRtcConnection, "storeStreamRtcConnection");
        m.checkNotNullParameter(storeUser, "storeUser");
        m.checkNotNullParameter(restAPI, "restAPI");
        m.checkNotNullParameter(imageEncoder, "imageEncoder");
        m.checkNotNullParameter(tooltipManager, "tooltipManager");
        this.appComponent = appComponent;
        this.channelId = j;
        this.guildId = l;
        this.storeApplicationStreaming = storeApplicationStreaming;
        this.storeRtcConnection = storeRtcConnection;
        this.storeStreamRtcConnection = storeStreamRtcConnection;
        this.storeUser = storeUser;
        this.restAPI = restAPI;
        this.imageEncoder = imageEncoder;
        this.tooltipManager = tooltipManager;
        BehaviorSubject<Bitmap> k0 = BehaviorSubject.k0();
        m.checkNotNullExpressionValue(k0, "BehaviorSubject.create()");
        this.thumbnailBitmapSubject = k0;
        this.compositeSubscription = new CompositeSubscription();
        subscribeToStores();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final ThumbnailEmitter createThumbnailEmitter() {
        return new ThumbnailEmitter(512, THUMBNAIL_HEIGHT_PX, 300000L, 5000L, null, new ScreenShareManager$createThumbnailEmitter$1(this), 16);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleStartStream(Intent intent) {
        StoreApplicationStreaming.createStream$default(this.storeApplicationStreaming, this.channelId, this.guildId, null, 4, null);
        this.screenshareIntent = intent;
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Removed duplicated region for block: B:38:0x006c A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:39:0x006d  */
    @androidx.annotation.MainThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void handleStateUpdate(com.discord.utilities.voice.ScreenShareManager.State r7) {
        /*
            r6 = this;
            com.discord.utilities.voice.ScreenShareManager$State r0 = r6.previousState
            r1 = 0
            if (r0 == 0) goto La
            com.discord.stores.StoreApplicationStreaming$ActiveApplicationStream r0 = r0.getActiveStream()
            goto Lb
        La:
            r0 = r1
        Lb:
            r2 = 0
            r3 = 1
            if (r0 == 0) goto L40
            com.discord.stores.StoreApplicationStreaming$ActiveApplicationStream r0 = r7.getActiveStream()
            if (r0 == 0) goto L20
            com.discord.models.domain.ModelApplicationStream r0 = r0.getStream()
            if (r0 == 0) goto L20
            java.lang.String r0 = r0.getEncodedStreamKey()
            goto L21
        L20:
            r0 = r1
        L21:
            com.discord.utilities.voice.ScreenShareManager$State r4 = r6.previousState
            if (r4 == 0) goto L36
            com.discord.stores.StoreApplicationStreaming$ActiveApplicationStream r4 = r4.getActiveStream()
            if (r4 == 0) goto L36
            com.discord.models.domain.ModelApplicationStream r4 = r4.getStream()
            if (r4 == 0) goto L36
            java.lang.String r4 = r4.getEncodedStreamKey()
            goto L37
        L36:
            r4 = r1
        L37:
            boolean r0 = d0.z.d.m.areEqual(r0, r4)
            r0 = r0 ^ r3
            if (r0 == 0) goto L40
            r0 = 1
            goto L41
        L40:
            r0 = 0
        L41:
            if (r0 == 0) goto L4c
            com.discord.rtcconnection.RtcConnection r0 = r7.getRtcConnection()
            if (r0 == 0) goto L4c
            r0.t(r1, r1)
        L4c:
            com.discord.stores.StoreApplicationStreaming$ActiveApplicationStream r0 = r7.getActiveStream()
            if (r0 == 0) goto L61
            com.discord.models.domain.ModelApplicationStream r0 = r0.getStream()
            if (r0 == 0) goto L61
            long r4 = r0.getOwnerId()
            java.lang.Long r0 = java.lang.Long.valueOf(r4)
            goto L62
        L61:
            r0 = r1
        L62:
            java.lang.Long r4 = r7.getMeId()
            boolean r0 = d0.z.d.m.areEqual(r0, r4)
            if (r0 != 0) goto L6d
            return
        L6d:
            com.discord.utilities.voice.ScreenShareManager$State r0 = r6.previousState
            if (r0 == 0) goto L75
            com.discord.rtcconnection.RtcConnection r1 = r0.getRtcConnection()
        L75:
            if (r1 != 0) goto L7e
            com.discord.rtcconnection.RtcConnection r0 = r7.getRtcConnection()
            if (r0 == 0) goto L7e
            r2 = 1
        L7e:
            if (r2 == 0) goto L8e
            com.discord.rtcconnection.RtcConnection r0 = r7.getRtcConnection()
            if (r0 == 0) goto L8e
            com.discord.utilities.voice.ScreenShareManager$RtcConnectionListener r1 = new com.discord.utilities.voice.ScreenShareManager$RtcConnectionListener
            r1.<init>()
            r0.c(r1)
        L8e:
            r6.previousState = r7
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.voice.ScreenShareManager.handleStateUpdate(com.discord.utilities.voice.ScreenShareManager$State):void");
    }

    private final void subscribeToStores() {
        Observable q = Observable.i(this.storeApplicationStreaming.observeActiveStream(), this.storeStreamRtcConnection.observeRtcConnection(), StoreUser.observeMe$default(this.storeUser, false, 1, null), ScreenShareManager$subscribeToStores$1.INSTANCE).q();
        m.checkNotNullExpressionValue(q, "Observable.combineLatest…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(q, this.appComponent, null, 2, null), ScreenShareManager.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new ScreenShareManager$subscribeToStores$2(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new ScreenShareManager$subscribeToStores$3(this));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void uploadScreenSharePreviews() {
        Observable<R> z2 = this.thumbnailBitmapSubject.z(new b<Bitmap, Observable<? extends Void>>() { // from class: com.discord.utilities.voice.ScreenShareManager$uploadScreenSharePreviews$1
            public final Observable<? extends Void> call(Bitmap bitmap) {
                ImageEncoder imageEncoder;
                RestAPI restAPI;
                ScreenShareManager.State state = ScreenShareManager.this.previousState;
                StoreApplicationStreaming.ActiveApplicationStream activeStream = state != null ? state.getActiveStream() : null;
                if (activeStream == null) {
                    return c.k;
                }
                imageEncoder = ScreenShareManager.this.imageEncoder;
                m.checkNotNullExpressionValue(bitmap, "thumbnailBitmap");
                String encodeBitmapAsJpegDataUrl = imageEncoder.encodeBitmapAsJpegDataUrl(bitmap, 92);
                restAPI = ScreenShareManager.this.restAPI;
                return restAPI.postStreamPreview(activeStream.getStream().getEncodedStreamKey(), encodeBitmapAsJpegDataUrl);
            }
        });
        m.checkNotNullExpressionValue(z2, "thumbnailBitmapSubject\n …>()\n          }\n        }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(ObservableExtensionsKt.bindToComponentLifecycle$default(z2, this.appComponent, null, 2, null), false, 1, null), ScreenShareManager.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new ScreenShareManager$uploadScreenSharePreviews$2(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, ScreenShareManager$uploadScreenSharePreviews$3.INSTANCE);
    }

    public final long getChannelId() {
        return this.channelId;
    }

    public final Long getGuildId() {
        return this.guildId;
    }

    public final void release() {
        this.compositeSubscription.unsubscribe();
    }

    public final void startStream(Intent intent) {
        m.checkNotNullParameter(intent, "intent");
        Observable<RtcConnection.Metadata> Z = this.storeRtcConnection.observeRtcConnectionMetadata().x(new b<RtcConnection.Metadata, Boolean>() { // from class: com.discord.utilities.voice.ScreenShareManager$startStream$1
            public final Boolean call(RtcConnection.Metadata metadata) {
                Long l = metadata != null ? metadata.c : null;
                return Boolean.valueOf(l != null && l.longValue() == ScreenShareManager.this.getChannelId() && m.areEqual(metadata.d, ScreenShareManager.this.getGuildId()));
            }
        }).Z(1);
        m.checkNotNullExpressionValue(Z, "storeRtcConnection.obser…       }\n        .take(1)");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(Z, this.appComponent, null, 2, null), ScreenShareManager.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new ScreenShareManager$startStream$2(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new ScreenShareManager$startStream$3(this, intent));
    }

    public final void stopStream() {
        StoreApplicationStreaming.ActiveApplicationStream activeStream;
        State state = this.previousState;
        if (state != null && (activeStream = state.getActiveStream()) != null) {
            this.storeApplicationStreaming.stopStream(activeStream.getStream().getEncodedStreamKey());
            this.screenshareIntent = null;
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public ScreenShareManager(com.discord.app.AppComponent r15, long r16, java.lang.Long r18, com.discord.stores.StoreApplicationStreaming r19, com.discord.stores.StoreRtcConnection r20, com.discord.stores.StoreStreamRtcConnection r21, com.discord.stores.StoreUser r22, com.discord.utilities.rest.RestAPI r23, com.discord.utilities.images.ImageEncoder r24, com.discord.tooltips.TooltipManager r25, int r26, kotlin.jvm.internal.DefaultConstructorMarker r27) {
        /*
            r14 = this;
            r0 = r26
            r1 = r0 & 8
            if (r1 == 0) goto Le
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreApplicationStreaming r1 = r1.getApplicationStreaming()
            r7 = r1
            goto L10
        Le:
            r7 = r19
        L10:
            r1 = r0 & 16
            if (r1 == 0) goto L1c
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreRtcConnection r1 = r1.getRtcConnection()
            r8 = r1
            goto L1e
        L1c:
            r8 = r20
        L1e:
            r1 = r0 & 32
            if (r1 == 0) goto L2a
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreStreamRtcConnection r1 = r1.getStreamRtcConnection()
            r9 = r1
            goto L2c
        L2a:
            r9 = r21
        L2c:
            r1 = r0 & 64
            if (r1 == 0) goto L38
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreUser r1 = r1.getUsers()
            r10 = r1
            goto L3a
        L38:
            r10 = r22
        L3a:
            r1 = r0 & 128(0x80, float:1.794E-43)
            if (r1 == 0) goto L46
            com.discord.utilities.rest.RestAPI$Companion r1 = com.discord.utilities.rest.RestAPI.Companion
            com.discord.utilities.rest.RestAPI r1 = r1.getApi()
            r11 = r1
            goto L48
        L46:
            r11 = r23
        L48:
            r1 = r0 & 256(0x100, float:3.59E-43)
            if (r1 == 0) goto L53
            com.discord.utilities.images.ImageEncoder r1 = new com.discord.utilities.images.ImageEncoder
            r1.<init>()
            r12 = r1
            goto L55
        L53:
            r12 = r24
        L55:
            r0 = r0 & 512(0x200, float:7.175E-43)
            if (r0 == 0) goto Lbc
            com.discord.app.AppLog r0 = com.discord.app.AppLog.g
            java.lang.String r1 = "logger"
            d0.z.d.m.checkNotNullParameter(r0, r1)
            java.lang.ref.WeakReference<b.a.j.a> r1 = b.a.j.a.b.a
            r2 = 0
            if (r1 == 0) goto L6c
            java.lang.Object r1 = r1.get()
            b.a.j.a r1 = (b.a.j.a) r1
            goto L6d
        L6c:
            r1 = r2
        L6d:
            if (r1 != 0) goto L7b
            b.a.j.a r1 = new b.a.j.a
            r1.<init>(r0)
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference
            r0.<init>(r1)
            b.a.j.a.b.a = r0
        L7b:
            com.discord.tooltips.TooltipManager$a r0 = com.discord.tooltips.TooltipManager.a.d
            java.lang.String r0 = "floatingViewManager"
            d0.z.d.m.checkNotNullParameter(r1, r0)
            java.lang.ref.WeakReference<com.discord.tooltips.TooltipManager> r0 = com.discord.tooltips.TooltipManager.a.a
            if (r0 == 0) goto L8d
            java.lang.Object r0 = r0.get()
            r2 = r0
            com.discord.tooltips.TooltipManager r2 = (com.discord.tooltips.TooltipManager) r2
        L8d:
            if (r2 != 0) goto Lba
            com.discord.tooltips.TooltipManager r0 = new com.discord.tooltips.TooltipManager
            kotlin.Lazy r2 = com.discord.tooltips.TooltipManager.a.f2787b
            java.lang.Object r2 = r2.getValue()
            b.a.v.a r2 = (b.a.v.a) r2
            kotlin.Lazy r3 = com.discord.tooltips.TooltipManager.a.c
            java.lang.Object r3 = r3.getValue()
            java.util.Set r3 = (java.util.Set) r3
            r4 = 0
            r5 = 4
            r19 = r0
            r20 = r2
            r21 = r3
            r22 = r4
            r23 = r1
            r24 = r5
            r19.<init>(r20, r21, r22, r23, r24)
            java.lang.ref.WeakReference r1 = new java.lang.ref.WeakReference
            r1.<init>(r0)
            com.discord.tooltips.TooltipManager.a.a = r1
            r2 = r0
        Lba:
            r13 = r2
            goto Lbe
        Lbc:
            r13 = r25
        Lbe:
            r2 = r14
            r3 = r15
            r4 = r16
            r6 = r18
            r2.<init>(r3, r4, r6, r7, r8, r9, r10, r11, r12, r13)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.voice.ScreenShareManager.<init>(com.discord.app.AppComponent, long, java.lang.Long, com.discord.stores.StoreApplicationStreaming, com.discord.stores.StoreRtcConnection, com.discord.stores.StoreStreamRtcConnection, com.discord.stores.StoreUser, com.discord.utilities.rest.RestAPI, com.discord.utilities.images.ImageEncoder, com.discord.tooltips.TooltipManager, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }
}
