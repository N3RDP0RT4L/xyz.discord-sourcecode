package com.discord.utilities.voice;

import android.content.Intent;
import com.discord.app.AppLog;
import com.discord.stores.StoreStream;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.voice.DiscordOverlayService;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: DiscordOverlayService.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Ljava/lang/Boolean;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class DiscordOverlayService$onStartCommand$2 extends o implements Function1<Boolean, Unit> {
    public final /* synthetic */ Intent $intent;
    public final /* synthetic */ int $startId;
    public final /* synthetic */ DiscordOverlayService this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public DiscordOverlayService$onStartCommand$2(DiscordOverlayService discordOverlayService, int i, Intent intent) {
        super(1);
        this.this$0 = discordOverlayService;
        this.$startId = i;
        this.$intent = intent;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Boolean bool) {
        invoke2(bool);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Boolean bool) {
        if (!StoreStream.Companion.getUserSettings().getIsMobileOverlayEnabled()) {
            this.this$0.stopForeground(true);
            this.this$0.stopSelf(this.$startId);
            return;
        }
        try {
            DiscordOverlayService$onStartCommand$2.super.handleStart(this.$intent);
        } catch (Exception e) {
            Logger.e$default(AppLog.g, "OverlayService", "Overlay failed to handle a request.", e, null, 8, null);
            DiscordOverlayService.Companion.tryStartOverlayService$default(DiscordOverlayService.Companion, this.this$0, "com.discord.actions.OVERLAY_CLOSE", false, 4, null);
        }
    }
}
