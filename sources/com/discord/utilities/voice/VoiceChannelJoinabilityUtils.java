package com.discord.utilities.voice;

import andhook.lib.HookHelper;
import b.c.a.a0.d;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guild.GuildMaxVideoChannelUsers;
import com.discord.api.guild.GuildVerificationLevel;
import com.discord.api.permission.Permission;
import com.discord.api.stageinstance.StageInstance;
import com.discord.api.voice.state.VoiceState;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreStageInstances;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreVoiceChannelSelected;
import com.discord.stores.StoreVoiceStates;
import com.discord.utilities.guilds.GuildVerificationLevelUtils;
import com.discord.utilities.permissions.PermissionUtils;
import d0.t.h0;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import kotlin.Metadata;
import rx.Observable;
import rx.functions.Func6;
/* compiled from: VoiceChannelJoinabilityUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b'\u0010(J[\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u00112\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\b\u001a\u00020\u00072\b\b\u0002\u0010\n\u001a\u00020\t2\b\b\u0002\u0010\f\u001a\u00020\u000b2\b\b\u0002\u0010\u000e\u001a\u00020\r2\b\b\u0002\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b\u0013\u0010\u0014J\u0019\u0010\u0015\u001a\u00020\u00122\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0015\u0010\u0016J]\u0010%\u001a\u00020\u00122\u0006\u0010\u0018\u001a\u00020\u00172\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001a0\u00192\u000e\u0010\u001d\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u001c2\u0006\u0010\u001f\u001a\u00020\u001e2\u0006\u0010!\u001a\u00020 2\u000e\u0010\"\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\b\u0010$\u001a\u0004\u0018\u00010#¢\u0006\u0004\b%\u0010&¨\u0006)"}, d2 = {"Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils;", "", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/stores/StoreChannels;", "channelsStore", "Lcom/discord/stores/StoreGuilds;", "guildsStore", "Lcom/discord/stores/StorePermissions;", "permissionsStore", "Lcom/discord/stores/StoreVoiceStates;", "voiceStatesStore", "Lcom/discord/stores/StoreVoiceChannelSelected;", "voiceChannelSelectedStore", "Lcom/discord/stores/StoreStageInstances;", "stageInstancesStore", "Lrx/Observable;", "Lcom/discord/utilities/voice/VoiceChannelJoinability;", "observeJoinability", "(JLcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreVoiceStates;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreStageInstances;)Lrx/Observable;", "getJoinability", "(J)Lcom/discord/utilities/voice/VoiceChannelJoinability;", "Lcom/discord/api/channel/Channel;", "channel", "", "Lcom/discord/api/voice/state/VoiceState;", "channelVoiceStates", "Lcom/discord/api/permission/PermissionBit;", "channelPermissions", "Lcom/discord/api/guild/GuildMaxVideoChannelUsers;", "guildMaxVideoChannelUsers", "Lcom/discord/api/guild/GuildVerificationLevel;", "verificationLevelTriggered", "selectedVoiceChannelId", "Lcom/discord/api/stageinstance/StageInstance;", "stageInstance", "computeJoinability", "(Lcom/discord/api/channel/Channel;Ljava/util/Collection;Ljava/lang/Long;Lcom/discord/api/guild/GuildMaxVideoChannelUsers;Lcom/discord/api/guild/GuildVerificationLevel;Ljava/lang/Long;Lcom/discord/api/stageinstance/StageInstance;)Lcom/discord/utilities/voice/VoiceChannelJoinability;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class VoiceChannelJoinabilityUtils {
    public static final VoiceChannelJoinabilityUtils INSTANCE = new VoiceChannelJoinabilityUtils();

    private VoiceChannelJoinabilityUtils() {
    }

    public final VoiceChannelJoinability computeJoinability(Channel channel, Collection<VoiceState> collection, Long l, GuildMaxVideoChannelUsers guildMaxVideoChannelUsers, GuildVerificationLevel guildVerificationLevel, Long l2, StageInstance stageInstance) {
        boolean z2;
        m.checkNotNullParameter(channel, "channel");
        m.checkNotNullParameter(collection, "channelVoiceStates");
        m.checkNotNullParameter(guildMaxVideoChannelUsers, "guildMaxVideoChannelUsers");
        m.checkNotNullParameter(guildVerificationLevel, "verificationLevelTriggered");
        if (ChannelUtils.x(channel)) {
            return VoiceChannelJoinability.CAN_JOIN;
        }
        long h = channel.h();
        if (l2 != null && h == l2.longValue()) {
            return VoiceChannelJoinability.CAN_JOIN;
        }
        if (!PermissionUtils.can(Permission.CONNECT, l)) {
            return VoiceChannelJoinability.PERMISSIONS_MISSING;
        }
        boolean z3 = true;
        if ((guildVerificationLevel != GuildVerificationLevel.NONE) && (stageInstance == null || !d.W0(stageInstance))) {
            return VoiceChannelJoinability.PERMISSIONS_MISSING;
        }
        ArrayList<VoiceState> arrayList = new ArrayList();
        for (Object obj : collection) {
            Long a = ((VoiceState) obj).a();
            if (a != null && a.longValue() == channel.h()) {
                arrayList.add(obj);
            }
        }
        int size = arrayList.size();
        if (!arrayList.isEmpty()) {
            for (VoiceState voiceState : arrayList) {
                if (voiceState.j()) {
                    z2 = true;
                    break;
                }
            }
        }
        z2 = false;
        boolean can = PermissionUtils.can(Permission.MOVE_MEMBERS, l);
        if (size >= (channel.B() != 0 ? channel.B() : Integer.MAX_VALUE) && !can) {
            return VoiceChannelJoinability.CHANNEL_FULL;
        }
        boolean can2 = PermissionUtils.can(8L, l);
        if (!z2 || !(guildMaxVideoChannelUsers instanceof GuildMaxVideoChannelUsers.Limited) || size < ((GuildMaxVideoChannelUsers.Limited) guildMaxVideoChannelUsers).a()) {
            z3 = false;
        }
        if (!z3 || can2) {
            return VoiceChannelJoinability.CAN_JOIN;
        }
        if (((GuildMaxVideoChannelUsers.Limited) guildMaxVideoChannelUsers).a() != size || !can) {
            return VoiceChannelJoinability.GUILD_VIDEO_AT_CAPACITY;
        }
        return VoiceChannelJoinability.CAN_JOIN;
    }

    public final VoiceChannelJoinability getJoinability(long j) {
        GuildMaxVideoChannelUsers guildMaxVideoChannelUsers;
        StoreStream.Companion companion = StoreStream.Companion;
        Channel channel = companion.getChannels().getChannel(j);
        if (channel == null) {
            return VoiceChannelJoinability.CHANNEL_DOES_NOT_EXIST;
        }
        Map map = (Map) a.u0(channel, companion.getVoiceStates().get());
        if (map == null) {
            map = h0.emptyMap();
        }
        Collection values = map.values();
        Collection<VoiceState> arrayList = new ArrayList<>();
        for (Object obj : values) {
            Long a = ((VoiceState) obj).a();
            if (a != null && a.longValue() == j) {
                arrayList.add(obj);
            }
        }
        StoreStream.Companion companion2 = StoreStream.Companion;
        Long l = companion2.getPermissions().getPermissionsByChannel().get(Long.valueOf(j));
        Guild guild = companion2.getGuilds().getGuild(channel.f());
        GuildVerificationLevel verificationLevelTriggered$default = GuildVerificationLevelUtils.getVerificationLevelTriggered$default(GuildVerificationLevelUtils.INSTANCE, channel.f(), null, null, null, 14, null);
        long selectedVoiceChannelId = companion2.getVoiceChannelSelected().getSelectedVoiceChannelId();
        StageInstance stageInstanceForChannel = companion2.getStageInstances().getStageInstanceForChannel(j);
        if (guild == null || (guildMaxVideoChannelUsers = guild.getMaxVideoChannelUsers()) == null) {
            guildMaxVideoChannelUsers = GuildMaxVideoChannelUsers.Unlimited.INSTANCE;
        }
        return computeJoinability(channel, arrayList, l, guildMaxVideoChannelUsers, verificationLevelTriggered$default, Long.valueOf(selectedVoiceChannelId), stageInstanceForChannel);
    }

    public final Observable<VoiceChannelJoinability> observeJoinability(final long j, StoreChannels storeChannels, final StoreGuilds storeGuilds, final StorePermissions storePermissions, final StoreVoiceStates storeVoiceStates, final StoreVoiceChannelSelected storeVoiceChannelSelected, final StoreStageInstances storeStageInstances) {
        m.checkNotNullParameter(storeChannels, "channelsStore");
        m.checkNotNullParameter(storeGuilds, "guildsStore");
        m.checkNotNullParameter(storePermissions, "permissionsStore");
        m.checkNotNullParameter(storeVoiceStates, "voiceStatesStore");
        m.checkNotNullParameter(storeVoiceChannelSelected, "voiceChannelSelectedStore");
        m.checkNotNullParameter(storeStageInstances, "stageInstancesStore");
        Observable Y = storeChannels.observeChannel(j).Y(new b<Channel, Observable<? extends VoiceChannelJoinability>>() { // from class: com.discord.utilities.voice.VoiceChannelJoinabilityUtils$observeJoinability$1
            public final Observable<? extends VoiceChannelJoinability> call(final Channel channel) {
                if (channel == null) {
                    return new k(VoiceChannelJoinability.CHANNEL_DOES_NOT_EXIST);
                }
                return Observable.f(StoreVoiceStates.this.observe(channel.f(), j), storePermissions.observePermissionsForChannel(j), storeGuilds.observeGuild(channel.f()), GuildVerificationLevelUtils.observeVerificationLevelTriggered$default(GuildVerificationLevelUtils.INSTANCE, channel.f(), null, null, null, 14, null), storeVoiceChannelSelected.observeSelectedVoiceChannelId(), storeStageInstances.observeStageInstanceForChannel(j), new Func6<Map<Long, ? extends VoiceState>, Long, Guild, GuildVerificationLevel, Long, StageInstance, VoiceChannelJoinability>() { // from class: com.discord.utilities.voice.VoiceChannelJoinabilityUtils$observeJoinability$1.1
                    @Override // rx.functions.Func6
                    public /* bridge */ /* synthetic */ VoiceChannelJoinability call(Map<Long, ? extends VoiceState> map, Long l, Guild guild, GuildVerificationLevel guildVerificationLevel, Long l2, StageInstance stageInstance) {
                        return call2((Map<Long, VoiceState>) map, l, guild, guildVerificationLevel, l2, stageInstance);
                    }

                    /* renamed from: call  reason: avoid collision after fix types in other method */
                    public final VoiceChannelJoinability call2(Map<Long, VoiceState> map, Long l, Guild guild, GuildVerificationLevel guildVerificationLevel, Long l2, StageInstance stageInstance) {
                        GuildMaxVideoChannelUsers guildMaxVideoChannelUsers;
                        VoiceChannelJoinabilityUtils voiceChannelJoinabilityUtils = VoiceChannelJoinabilityUtils.INSTANCE;
                        Channel channel2 = Channel.this;
                        Collection<VoiceState> values = map.values();
                        if (guild == null || (guildMaxVideoChannelUsers = guild.getMaxVideoChannelUsers()) == null) {
                            guildMaxVideoChannelUsers = GuildMaxVideoChannelUsers.Unlimited.INSTANCE;
                        }
                        m.checkNotNullExpressionValue(guildVerificationLevel, "verificationLevelTriggered");
                        return voiceChannelJoinabilityUtils.computeJoinability(channel2, values, l, guildMaxVideoChannelUsers, guildVerificationLevel, l2, stageInstance);
                    }
                });
            }
        });
        m.checkNotNullExpressionValue(Y, "channelsStore\n        .o…  }\n          }\n        }");
        return Y;
    }
}
