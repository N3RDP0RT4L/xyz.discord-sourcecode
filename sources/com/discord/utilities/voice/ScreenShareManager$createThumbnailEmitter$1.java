package com.discord.utilities.voice;

import android.graphics.Bitmap;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.subjects.BehaviorSubject;
/* compiled from: ScreenShareManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/graphics/Bitmap;", "thumbnailBitmap", "", "invoke", "(Landroid/graphics/Bitmap;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ScreenShareManager$createThumbnailEmitter$1 extends o implements Function1<Bitmap, Unit> {
    public final /* synthetic */ ScreenShareManager this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ScreenShareManager$createThumbnailEmitter$1(ScreenShareManager screenShareManager) {
        super(1);
        this.this$0 = screenShareManager;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Bitmap bitmap) {
        invoke2(bitmap);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Bitmap bitmap) {
        BehaviorSubject behaviorSubject;
        m.checkNotNullParameter(bitmap, "thumbnailBitmap");
        behaviorSubject = this.this$0.thumbnailBitmapSubject;
        behaviorSubject.onNext(bitmap);
    }
}
