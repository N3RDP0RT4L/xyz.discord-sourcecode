package com.discord.utilities.voice;

import android.content.Context;
import b.a.k.b;
import com.discord.rtcconnection.RtcConnection;
import com.discord.utilities.voice.VoiceEngineServiceController;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: VoiceEngineServiceController.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Lcom/discord/utilities/voice/VoiceEngineServiceController$NotificationData;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class VoiceEngineServiceController$init$4 extends o implements Function1<VoiceEngineServiceController.NotificationData, Unit> {
    public final /* synthetic */ Context $context;
    public final /* synthetic */ VoiceEngineServiceController this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public VoiceEngineServiceController$init$4(VoiceEngineServiceController voiceEngineServiceController, Context context) {
        super(1);
        this.this$0 = voiceEngineServiceController;
        this.$context = context;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(VoiceEngineServiceController.NotificationData notificationData) {
        invoke2(notificationData);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(VoiceEngineServiceController.NotificationData notificationData) {
        CharSequence b2;
        CharSequence b3;
        if (notificationData.getRtcConnectionState() instanceof RtcConnection.State.d) {
            DiscordOverlayService.Companion.launchForClose(this.$context);
            VoiceEngineForegroundService.Companion.stopForegroundAndUnbind(VoiceEngineServiceController.access$getServiceBinding$p(this.this$0));
            return;
        }
        Context context = this.$context;
        b2 = b.b(context, notificationData.getStateString(), new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        b3 = b.b(context, R.string.call_mobile_tap_to_return, new Object[]{b2}, (r4 & 4) != 0 ? b.C0034b.j : null);
        VoiceEngineForegroundService.Companion.startForegroundAndBind(VoiceEngineServiceController.access$getServiceBinding$p(this.this$0), b3, notificationData.getChannelName(), notificationData.isSelfMuted(), notificationData.isSelfDeafened(), notificationData.isSelfStreaming(), notificationData.getChannelId(), notificationData.getGuildId(), notificationData.getProximityLockEnabled(), notificationData.getCanSpeak());
        if (m.areEqual(notificationData.getRtcConnectionState(), RtcConnection.State.f.a)) {
            DiscordOverlayService.Companion.launchForVoice(this.$context);
        }
    }
}
