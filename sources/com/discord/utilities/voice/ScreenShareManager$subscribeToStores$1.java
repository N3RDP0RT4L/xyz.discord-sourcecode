package com.discord.utilities.voice;

import androidx.core.app.NotificationCompat;
import com.discord.models.user.MeUser;
import com.discord.rtcconnection.RtcConnection;
import com.discord.stores.StoreApplicationStreaming;
import com.discord.utilities.voice.ScreenShareManager;
import kotlin.Metadata;
import rx.functions.Func3;
/* compiled from: ScreenShareManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\n\u001a\n \u0005*\u0004\u0018\u00010\u00070\u00072\b\u0010\u0001\u001a\u0004\u0018\u00010\u00002\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u000e\u0010\u0006\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004H\n¢\u0006\u0004\b\b\u0010\t"}, d2 = {"Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;", "activeStream", "Lcom/discord/rtcconnection/RtcConnection;", "rtcConnection", "Lcom/discord/models/user/MeUser;", "kotlin.jvm.PlatformType", "me", "Lcom/discord/utilities/voice/ScreenShareManager$State;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lcom/discord/rtcconnection/RtcConnection;Lcom/discord/models/user/MeUser;)Lcom/discord/utilities/voice/ScreenShareManager$State;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ScreenShareManager$subscribeToStores$1<T1, T2, T3, R> implements Func3<StoreApplicationStreaming.ActiveApplicationStream, RtcConnection, MeUser, ScreenShareManager.State> {
    public static final ScreenShareManager$subscribeToStores$1 INSTANCE = new ScreenShareManager$subscribeToStores$1();

    public final ScreenShareManager.State call(StoreApplicationStreaming.ActiveApplicationStream activeApplicationStream, RtcConnection rtcConnection, MeUser meUser) {
        return new ScreenShareManager.State(activeApplicationStream, rtcConnection, meUser != null ? Long.valueOf(meUser.getId()) : null);
    }
}
