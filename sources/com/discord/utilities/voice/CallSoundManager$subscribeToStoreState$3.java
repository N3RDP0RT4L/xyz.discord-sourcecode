package com.discord.utilities.voice;

import com.discord.utilities.voice.CallSoundManager;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: CallSoundManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/utilities/voice/CallSoundManager$StoreState;", "kotlin.jvm.PlatformType", "storeState", "", "invoke", "(Lcom/discord/utilities/voice/CallSoundManager$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class CallSoundManager$subscribeToStoreState$3 extends o implements Function1<CallSoundManager.StoreState, Unit> {
    public final /* synthetic */ long $voiceChannelId;
    public final /* synthetic */ CallSoundManager this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CallSoundManager$subscribeToStoreState$3(CallSoundManager callSoundManager, long j) {
        super(1);
        this.this$0 = callSoundManager;
        this.$voiceChannelId = j;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(CallSoundManager.StoreState storeState) {
        invoke2(storeState);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(CallSoundManager.StoreState storeState) {
        CallSoundManager callSoundManager = this.this$0;
        long j = this.$voiceChannelId;
        m.checkNotNullExpressionValue(storeState, "storeState");
        callSoundManager.handleStoreState(j, storeState);
    }
}
