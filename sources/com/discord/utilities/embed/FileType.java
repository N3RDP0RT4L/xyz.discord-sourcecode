package com.discord.utilities.embed;

import andhook.lib.HookHelper;
import androidx.annotation.DrawableRes;
import d0.z.d.m;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* JADX WARN: Init of enum ACROBAT can be incorrect */
/* JADX WARN: Init of enum AE can be incorrect */
/* JADX WARN: Init of enum AI can be incorrect */
/* JADX WARN: Init of enum ARCHIVE can be incorrect */
/* JADX WARN: Init of enum CODE can be incorrect */
/* JADX WARN: Init of enum DOCUMENT can be incorrect */
/* JADX WARN: Init of enum SKETCH can be incorrect */
/* JADX WARN: Init of enum SPREADSHEET can be incorrect */
/* JADX WARN: Init of enum VIDEO can be incorrect */
/* JADX WARN: Init of enum WEBCODE can be incorrect */
/* compiled from: FileType.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000f\b\u0086\u0001\u0018\u0000 \u00122\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0012B\u0011\b\u0002\u0012\u0006\u0010\u000f\u001a\u00020\u000e¢\u0006\u0004\b\u0010\u0010\u0011J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\b\u001a\u00020\u0007H\u0007¢\u0006\u0004\b\b\u0010\tR\u001c\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\rj\u0002\b\u0013j\u0002\b\u0014j\u0002\b\u0015j\u0002\b\u0016j\u0002\b\u0017j\u0002\b\u0018j\u0002\b\u0019j\u0002\b\u001aj\u0002\b\u001bj\u0002\b\u001c¨\u0006\u001d"}, d2 = {"Lcom/discord/utilities/embed/FileType;", "", "", "extension", "", "matches", "(Ljava/lang/String;)Z", "", "getFileDrawable", "()I", "Ljava/lang/ThreadLocal;", "Ljava/util/regex/Matcher;", "threadLocalMatcher", "Ljava/lang/ThreadLocal;", "Ljava/util/regex/Pattern;", "pattern", HookHelper.constructorName, "(Ljava/lang/String;ILjava/util/regex/Pattern;)V", "Companion", "VIDEO", "ACROBAT", "AE", "SKETCH", "AI", "ARCHIVE", "CODE", "DOCUMENT", "SPREADSHEET", "WEBCODE", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public enum FileType {
    VIDEO(r2),
    ACROBAT(r2),
    AE(r2),
    SKETCH(r2),
    AI(r2),
    ARCHIVE(r2),
    CODE(r2),
    DOCUMENT(r2),
    SPREADSHEET(r2),
    WEBCODE(r2);
    
    public static final Companion Companion = new Companion(null);
    private final ThreadLocal<Matcher> threadLocalMatcher;

    /* compiled from: FileType.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/utilities/embed/FileType$Companion;", "", "", "extension", "Lcom/discord/utilities/embed/FileType;", "getFromExtension", "(Ljava/lang/String;)Lcom/discord/utilities/embed/FileType;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public final FileType getFromExtension(String str) {
            m.checkNotNullParameter(str, "extension");
            FileType[] values = FileType.values();
            for (int i = 0; i < 10; i++) {
                FileType fileType = values[i];
                if (fileType.matches(str)) {
                    return fileType;
                }
            }
            return null;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            FileType.values();
            int[] iArr = new int[10];
            $EnumSwitchMapping$0 = iArr;
            iArr[FileType.VIDEO.ordinal()] = 1;
            iArr[FileType.ACROBAT.ordinal()] = 2;
            iArr[FileType.AE.ordinal()] = 3;
            iArr[FileType.SKETCH.ordinal()] = 4;
            iArr[FileType.AI.ordinal()] = 5;
            iArr[FileType.ARCHIVE.ordinal()] = 6;
            iArr[FileType.CODE.ordinal()] = 7;
            iArr[FileType.DOCUMENT.ordinal()] = 8;
            iArr[FileType.SPREADSHEET.ordinal()] = 9;
            iArr[FileType.WEBCODE.ordinal()] = 10;
        }
    }

    static {
        m.checkNotNullExpressionValue(Pattern.compile("(?:avi|flv|wmv|mov|mp4)$", 0), "java.util.regex.Pattern.compile(this, flags)");
        m.checkNotNullExpressionValue(Pattern.compile("pdf$", 0), "java.util.regex.Pattern.compile(this, flags)");
        m.checkNotNullExpressionValue(Pattern.compile("ae$", 0), "java.util.regex.Pattern.compile(this, flags)");
        m.checkNotNullExpressionValue(Pattern.compile("sketch$", 0), "java.util.regex.Pattern.compile(this, flags)");
        m.checkNotNullExpressionValue(Pattern.compile("ai$", 0), "java.util.regex.Pattern.compile(this, flags)");
        m.checkNotNullExpressionValue(Pattern.compile("(?:rar|zip|7z|tar|tar\\.gz)$", 0), "java.util.regex.Pattern.compile(this, flags)");
        String str = "(?:c\\+\\+|cpp|cc|c|h|hpp|mm|m|json|js|rb|rake|py|asm|fs|pyc|dtd|cgi|bat|rss|java|graphml|idb|lua|o|gml|prl|sls|conf|cmake|make|sln|vbe|cxx|wbf|vbs|r|wml|php|bash|applescript|fcgi|yaml|ex|exs|sh|ml|actionscript)$";
        m.checkNotNullExpressionValue(str, "StringBuilder()\n      .a…(\")\\$\")\n      .toString()");
        m.checkNotNullExpressionValue(Pattern.compile(str, 0), "java.util.regex.Pattern.compile(this, flags)");
        m.checkNotNullExpressionValue(Pattern.compile("(?:txt|rtf|doc|docx|md|pages|ppt|pptx|pptm|key|log)$", 0), "java.util.regex.Pattern.compile(this, flags)");
        m.checkNotNullExpressionValue(Pattern.compile("(?:xls|xlsx|numbers|csv|xliff)$", 0), "java.util.regex.Pattern.compile(this, flags)");
        m.checkNotNullExpressionValue(Pattern.compile("(?:html|xhtml|htm|js|xml|xls|xsd|css|styl)$", 0), "java.util.regex.Pattern.compile(this, flags)");
    }

    FileType(final Pattern pattern) {
        this.threadLocalMatcher = new ThreadLocal<Matcher>() { // from class: com.discord.utilities.embed.FileType$threadLocalMatcher$1
            @Override // java.lang.ThreadLocal
            public Matcher initialValue() {
                return pattern.matcher("");
            }
        };
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final boolean matches(String str) {
        Matcher matcher = this.threadLocalMatcher.get();
        m.checkNotNull(matcher);
        return matcher.reset(str).find();
    }

    @DrawableRes
    public final int getFileDrawable() {
        switch (ordinal()) {
            case 0:
                return R.drawable.ic_file_video;
            case 1:
                return R.drawable.ic_file_acrobat;
            case 2:
                return R.drawable.ic_file_ae;
            case 3:
                return R.drawable.ic_file_sketch;
            case 4:
                return R.drawable.ic_file_ai;
            case 5:
                return R.drawable.ic_file_archive;
            case 6:
                return R.drawable.ic_file_code;
            case 7:
                return R.drawable.ic_file_document;
            case 8:
                return R.drawable.ic_file_spreadsheet;
            case 9:
                return R.drawable.ic_file_webcode;
            default:
                throw new NoWhenBranchMatchedException();
        }
    }
}
