package com.discord.utilities.embed;

import andhook.lib.HookHelper;
import b.c.a.a0.d;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.stageinstance.RecommendedStageInstance;
import com.discord.api.stageinstance.StageInstance;
import com.discord.models.domain.ModelInvite;
import com.discord.stores.StoreInstantInvites;
import com.discord.stores.StoreRequestedStageChannels;
import com.discord.stores.StoreStream;
import com.discord.utilities.SnowflakeUtils;
import d0.z.d.m;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function4;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func4;
/* compiled from: InviteEmbedModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u0000 *2\u00020\u0001:\u0001*Bc\u0012\u0016\u0010\u0018\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0002\u0012\u0012\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0002\u0012\u0016\u0010\u001a\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u000b0\u0002\u0012\u0016\u0010\u001b\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\r0\u0002¢\u0006\u0004\b(\u0010)J \u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0002HÂ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u001c\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0002HÂ\u0003¢\u0006\u0004\b\n\u0010\u0007J \u0010\f\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u000b0\u0002HÂ\u0003¢\u0006\u0004\b\f\u0010\u0007J \u0010\u000e\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\r0\u0002HÂ\u0003¢\u0006\u0004\b\u000e\u0010\u0007J\u0017\u0010\u0011\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u000f\u001a\u00020\b¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0014\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0013\u001a\u00020\b¢\u0006\u0004\b\u0014\u0010\u0012J\u001f\u0010\u0016\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0013\u001a\u00020\b2\u0006\u0010\u0015\u001a\u00020\b¢\u0006\u0004\b\u0016\u0010\u0017Jt\u0010\u001c\u001a\u00020\u00002\u0018\b\u0002\u0010\u0018\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00022\u0014\b\u0002\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u00022\u0018\b\u0002\u0010\u001a\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u000b0\u00022\u0018\b\u0002\u0010\u001b\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\r0\u0002HÆ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001e\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010!\u001a\u00020 HÖ\u0001¢\u0006\u0004\b!\u0010\"J\u001a\u0010%\u001a\u00020$2\b\u0010#\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b%\u0010&R\"\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010'R&\u0010\u001b\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\r0\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010'R&\u0010\u0018\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010'R&\u0010\u001a\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u000b0\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001a\u0010'¨\u0006+"}, d2 = {"Lcom/discord/utilities/embed/InviteEmbedModel;", "", "", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "component1", "()Ljava/util/Map;", "", "Lcom/discord/stores/StoreInstantInvites$InviteState;", "component2", "Lcom/discord/api/stageinstance/StageInstance;", "component3", "Lcom/discord/stores/StoreRequestedStageChannels$StageInstanceState;", "component4", "inviteKey", "Lcom/discord/models/domain/ModelInvite;", "getResolvedInvite", "(Ljava/lang/String;)Lcom/discord/models/domain/ModelInvite;", "inviteCode", "resolveInviteCodeForPublicStage", "eventId", "resolveInviteForGuildScheduledEvent", "(Ljava/lang/String;Ljava/lang/String;)Lcom/discord/models/domain/ModelInvite;", "channels", "invites", "stageInstances", "requestedInstances", "copy", "(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/utilities/embed/InviteEmbedModel;", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", HookHelper.constructorName, "(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class InviteEmbedModel {
    public static final Companion Companion = new Companion(null);
    private final Map<Long, Channel> channels;
    private final Map<String, StoreInstantInvites.InviteState> invites;
    private final Map<Long, StoreRequestedStageChannels.StageInstanceState> requestedInstances;
    private final Map<Long, StageInstance> stageInstances;

    /* compiled from: InviteEmbedModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u008f\u0001\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00022\u001e\b\u0002\u0010\u0007\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u00060\u00030\u00022\u001a\b\u0002\u0010\n\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u00030\u00022\u001e\b\u0002\u0010\f\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u000b0\u00030\u00022\u001e\b\u0002\u0010\u000e\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\r0\u00030\u0002¢\u0006\u0004\b\u0010\u0010\u0011¨\u0006\u0014"}, d2 = {"Lcom/discord/utilities/embed/InviteEmbedModel$Companion;", "", "Lrx/Observable;", "", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "channelsObservable", "", "Lcom/discord/stores/StoreInstantInvites$InviteState;", "knownInvitesObservable", "Lcom/discord/api/stageinstance/StageInstance;", "stageInstancesObservable", "Lcom/discord/stores/StoreRequestedStageChannels$StageInstanceState;", "requestedStageInstances", "Lcom/discord/utilities/embed/InviteEmbedModel;", "observe", "(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ Observable observe$default(Companion companion, Observable observable, Observable observable2, Observable observable3, Observable observable4, int i, Object obj) {
            Observable<Map<Long, Channel>> observable5 = observable;
            if ((i & 1) != 0) {
                observable5 = StoreStream.Companion.getChannels().observeGuildAndPrivateChannels();
            }
            Observable<Map<String, StoreInstantInvites.InviteState>> observable6 = observable2;
            if ((i & 2) != 0) {
                observable6 = StoreStream.Companion.getInstantInvites().observeKnownInvites();
            }
            Observable<Map<Long, StageInstance>> observable7 = observable3;
            if ((i & 4) != 0) {
                observable7 = StoreStream.Companion.getStageInstances().observeStageInstances();
            }
            Observable<Map<Long, StoreRequestedStageChannels.StageInstanceState>> observable8 = observable4;
            if ((i & 8) != 0) {
                observable8 = StoreStream.Companion.getRequestedStageChannels().observeRequestedStageChannels();
            }
            return companion.observe(observable5, observable6, observable7, observable8);
        }

        public final Observable<InviteEmbedModel> observe(Observable<Map<Long, Channel>> observable, Observable<Map<String, StoreInstantInvites.InviteState>> observable2, Observable<Map<Long, StageInstance>> observable3, Observable<Map<Long, StoreRequestedStageChannels.StageInstanceState>> observable4) {
            m.checkNotNullParameter(observable, "channelsObservable");
            m.checkNotNullParameter(observable2, "knownInvitesObservable");
            m.checkNotNullParameter(observable3, "stageInstancesObservable");
            m.checkNotNullParameter(observable4, "requestedStageInstances");
            final InviteEmbedModel$Companion$observe$1 inviteEmbedModel$Companion$observe$1 = InviteEmbedModel$Companion$observe$1.INSTANCE;
            Object obj = inviteEmbedModel$Companion$observe$1;
            if (inviteEmbedModel$Companion$observe$1 != null) {
                obj = new Func4() { // from class: com.discord.utilities.embed.InviteEmbedModel$sam$rx_functions_Func4$0
                    @Override // rx.functions.Func4
                    public final /* synthetic */ Object call(Object obj2, Object obj3, Object obj4, Object obj5) {
                        return Function4.this.invoke(obj2, obj3, obj4, obj5);
                    }
                };
            }
            Observable<InviteEmbedModel> h = Observable.h(observable, observable2, observable3, observable4, (Func4) obj);
            m.checkNotNullExpressionValue(h, "Observable.combineLatest… ::InviteEmbedModel\n    )");
            return h;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public InviteEmbedModel(Map<Long, Channel> map, Map<String, ? extends StoreInstantInvites.InviteState> map2, Map<Long, StageInstance> map3, Map<Long, StoreRequestedStageChannels.StageInstanceState> map4) {
        m.checkNotNullParameter(map, "channels");
        m.checkNotNullParameter(map2, "invites");
        m.checkNotNullParameter(map3, "stageInstances");
        m.checkNotNullParameter(map4, "requestedInstances");
        this.channels = map;
        this.invites = map2;
        this.stageInstances = map3;
        this.requestedInstances = map4;
    }

    private final Map<Long, Channel> component1() {
        return this.channels;
    }

    private final Map<String, StoreInstantInvites.InviteState> component2() {
        return this.invites;
    }

    private final Map<Long, StageInstance> component3() {
        return this.stageInstances;
    }

    private final Map<Long, StoreRequestedStageChannels.StageInstanceState> component4() {
        return this.requestedInstances;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ InviteEmbedModel copy$default(InviteEmbedModel inviteEmbedModel, Map map, Map map2, Map map3, Map map4, int i, Object obj) {
        if ((i & 1) != 0) {
            map = inviteEmbedModel.channels;
        }
        if ((i & 2) != 0) {
            map2 = inviteEmbedModel.invites;
        }
        if ((i & 4) != 0) {
            map3 = inviteEmbedModel.stageInstances;
        }
        if ((i & 8) != 0) {
            map4 = inviteEmbedModel.requestedInstances;
        }
        return inviteEmbedModel.copy(map, map2, map3, map4);
    }

    public final InviteEmbedModel copy(Map<Long, Channel> map, Map<String, ? extends StoreInstantInvites.InviteState> map2, Map<Long, StageInstance> map3, Map<Long, StoreRequestedStageChannels.StageInstanceState> map4) {
        m.checkNotNullParameter(map, "channels");
        m.checkNotNullParameter(map2, "invites");
        m.checkNotNullParameter(map3, "stageInstances");
        m.checkNotNullParameter(map4, "requestedInstances");
        return new InviteEmbedModel(map, map2, map3, map4);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof InviteEmbedModel)) {
            return false;
        }
        InviteEmbedModel inviteEmbedModel = (InviteEmbedModel) obj;
        return m.areEqual(this.channels, inviteEmbedModel.channels) && m.areEqual(this.invites, inviteEmbedModel.invites) && m.areEqual(this.stageInstances, inviteEmbedModel.stageInstances) && m.areEqual(this.requestedInstances, inviteEmbedModel.requestedInstances);
    }

    public final ModelInvite getResolvedInvite(String str) {
        m.checkNotNullParameter(str, "inviteKey");
        StoreInstantInvites.InviteState inviteState = this.invites.get(str);
        if (!(inviteState instanceof StoreInstantInvites.InviteState.Resolved)) {
            inviteState = null;
        }
        StoreInstantInvites.InviteState.Resolved resolved = (StoreInstantInvites.InviteState.Resolved) inviteState;
        if (resolved != null) {
            return resolved.getInvite();
        }
        return null;
    }

    public int hashCode() {
        Map<Long, Channel> map = this.channels;
        int i = 0;
        int hashCode = (map != null ? map.hashCode() : 0) * 31;
        Map<String, StoreInstantInvites.InviteState> map2 = this.invites;
        int hashCode2 = (hashCode + (map2 != null ? map2.hashCode() : 0)) * 31;
        Map<Long, StageInstance> map3 = this.stageInstances;
        int hashCode3 = (hashCode2 + (map3 != null ? map3.hashCode() : 0)) * 31;
        Map<Long, StoreRequestedStageChannels.StageInstanceState> map4 = this.requestedInstances;
        if (map4 != null) {
            i = map4.hashCode();
        }
        return hashCode3 + i;
    }

    public final ModelInvite resolveInviteCodeForPublicStage(String str) {
        RecommendedStageInstance stageInstance;
        StageInstance a;
        m.checkNotNullParameter(str, "inviteCode");
        ModelInvite resolvedInvite = getResolvedInvite(str);
        if (resolvedInvite != null) {
            Channel channel = resolvedInvite.getChannel();
            if (!(channel != null && ChannelUtils.z(channel))) {
                channel = null;
            }
            if (channel != null) {
                m.checkNotNullExpressionValue(channel, "invite.channel.takeIf { … == true } ?: return null");
                StageInstance stageInstance2 = (StageInstance) a.c(channel, this.stageInstances);
                if (stageInstance2 != null && d.W0(stageInstance2)) {
                    return resolvedInvite;
                }
                if (((Channel) a.c(channel, this.channels)) != null) {
                    return null;
                }
                StoreRequestedStageChannels.StageInstanceState stageInstanceState = (StoreRequestedStageChannels.StageInstanceState) a.c(channel, this.requestedInstances);
                if (stageInstanceState != null && (stageInstance = stageInstanceState.getStageInstance()) != null && (a = stageInstance.a()) != null && d.W0(a)) {
                    return resolvedInvite;
                }
                if (stageInstanceState == null || stageInstanceState.isError()) {
                    StoreStream.Companion.getRequestedStageChannels().enqueueStageChannelFetch(channel.h());
                }
            }
        }
        return null;
    }

    public final ModelInvite resolveInviteForGuildScheduledEvent(String str, String str2) {
        m.checkNotNullParameter(str, "inviteCode");
        m.checkNotNullParameter(str2, "eventId");
        String inviteStoreKey = ModelInvite.getInviteStoreKey(str, SnowflakeUtils.INSTANCE.toSnowflake(str2));
        m.checkNotNullExpressionValue(inviteStoreKey, "ModelInvite.getInviteSto…d.toSnowflake()\n        )");
        ModelInvite resolvedInvite = getResolvedInvite(inviteStoreKey);
        if ((resolvedInvite != null ? resolvedInvite.getGuildScheduledEvent() : null) != null) {
            return resolvedInvite;
        }
        return null;
    }

    public String toString() {
        StringBuilder R = a.R("InviteEmbedModel(channels=");
        R.append(this.channels);
        R.append(", invites=");
        R.append(this.invites);
        R.append(", stageInstances=");
        R.append(this.stageInstances);
        R.append(", requestedInstances=");
        return a.L(R, this.requestedInstances, ")");
    }
}
