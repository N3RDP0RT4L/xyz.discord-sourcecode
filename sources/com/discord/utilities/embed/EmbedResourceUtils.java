package com.discord.utilities.embed;

import andhook.lib.HookHelper;
import andhook.lib.xposed.ClassUtils;
import android.content.Context;
import android.content.res.Resources;
import androidx.annotation.DrawableRes;
import b.d.b.a.a;
import com.discord.api.message.attachment.MessageAttachment;
import com.discord.api.message.attachment.MessageAttachmentType;
import com.discord.api.message.embed.EmbedProvider;
import com.discord.api.message.embed.EmbedType;
import com.discord.api.message.embed.EmbedVideo;
import com.discord.api.message.embed.MessageEmbed;
import com.discord.embed.RenderableEmbedMedia;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.display.DisplayUtils;
import com.discord.utilities.string.StringUtilsKt;
import d0.g0.t;
import d0.g0.w;
import d0.o;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.util.List;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;
import xyz.discord.R;
/* compiled from: EmbedResourceUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0012\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b;\u0010<J\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0015\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\t\u0010\nJ\u0019\u0010\f\u001a\u00020\u000b2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0007¢\u0006\u0004\b\f\u0010\rJK\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000b0\u00152\u0006\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\u000b2\u0006\u0010\u0010\u001a\u00020\u000b2\u0006\u0010\u0011\u001a\u00020\u000b2\u0006\u0010\u0013\u001a\u00020\u00122\b\b\u0002\u0010\u0014\u001a\u00020\u000b¢\u0006\u0004\b\u0016\u0010\u0017J\u0015\u0010\u001a\u001a\u00020\u000b2\u0006\u0010\u0019\u001a\u00020\u0018¢\u0006\u0004\b\u001a\u0010\u001bJ5\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00020!2\u0006\u0010\u001c\u001a\u00020\u00022\u0006\u0010\u001d\u001a\u00020\u000b2\u0006\u0010\u001e\u001a\u00020\u000b2\b\b\u0002\u0010 \u001a\u00020\u001f¢\u0006\u0004\b\"\u0010#J\u0013\u0010%\u001a\u0004\u0018\u00010\u0004*\u00020$¢\u0006\u0004\b%\u0010&J\u0011\u0010'\u001a\u00020\u001f*\u00020$¢\u0006\u0004\b'\u0010(J\u0011\u0010)\u001a\u00020\u001f*\u00020$¢\u0006\u0004\b)\u0010(J\u0011\u0010*\u001a\u00020\u001f*\u00020$¢\u0006\u0004\b*\u0010(J!\u0010*\u001a\u00020\u001f2\b\u0010,\u001a\u0004\u0018\u00010+2\b\u0010-\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b*\u0010.J\u0013\u0010/\u001a\u0004\u0018\u00010\u0002*\u00020$¢\u0006\u0004\b/\u00100J\u0011\u00101\u001a\u00020\u001f*\u00020$¢\u0006\u0004\b1\u0010(R\u0016\u00102\u001a\u00020\u000b8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b2\u00103R\u001c\u00104\u001a\b\u0012\u0004\u0012\u00020+0!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b4\u00105R\u0016\u00106\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b6\u00107R\u0019\u00108\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b8\u00103\u001a\u0004\b9\u0010:¨\u0006="}, d2 = {"Lcom/discord/utilities/embed/EmbedResourceUtils;", "", "", "filename", "Lcom/discord/embed/RenderableEmbedMedia;", "createFileRenderableMedia", "(Ljava/lang/String;)Lcom/discord/embed/RenderableEmbedMedia;", "Lcom/discord/api/message/attachment/MessageAttachment;", "attachment", "createRenderableEmbedMediaFromAttachment", "(Lcom/discord/api/message/attachment/MessageAttachment;)Lcom/discord/embed/RenderableEmbedMedia;", "", "getFileDrawable", "(Ljava/lang/String;)I", "actualWidth", "actualHeight", "maxWidth", "maxHeight", "Landroid/content/res/Resources;", "resources", "minWidth", "Lkotlin/Pair;", "calculateScaledSize", "(IIIILandroid/content/res/Resources;I)Lkotlin/Pair;", "Landroid/content/Context;", "context", "computeMaximumImageWidthPx", "(Landroid/content/Context;)I", "originalUrl", "width", "height", "", "shouldAnimate", "", "getPreviewUrls", "(Ljava/lang/String;IIZ)Ljava/util/List;", "Lcom/discord/api/message/embed/MessageEmbed;", "getPreviewImage", "(Lcom/discord/api/message/embed/MessageEmbed;)Lcom/discord/embed/RenderableEmbedMedia;", "isInlineEmbed", "(Lcom/discord/api/message/embed/MessageEmbed;)Z", "isSimpleEmbed", "isAnimated", "Lcom/discord/api/message/embed/EmbedType;", "embedType", "previewImageUri", "(Lcom/discord/api/message/embed/EmbedType;Ljava/lang/String;)Z", "getExternalOpenUrl", "(Lcom/discord/api/message/embed/MessageEmbed;)Ljava/lang/String;", "isPlayable", "MAX_IMAGE_SIZE", "I", "PLAYABLE_EMBED_TYPES", "Ljava/util/List;", "FILE_SCHEME", "Ljava/lang/String;", "MAX_IMAGE_VIEW_HEIGHT_PX", "getMAX_IMAGE_VIEW_HEIGHT_PX", "()I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class EmbedResourceUtils {
    public static final String FILE_SCHEME = "res:///";
    public static final int MAX_IMAGE_SIZE = 1440;
    public static final EmbedResourceUtils INSTANCE = new EmbedResourceUtils();
    private static final int MAX_IMAGE_VIEW_HEIGHT_PX = DimenUtils.dpToPixels(320);
    private static final List<EmbedType> PLAYABLE_EMBED_TYPES = n.listOf((Object[]) new EmbedType[]{EmbedType.VIDEO, EmbedType.GIFV});

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;
        public static final /* synthetic */ int[] $EnumSwitchMapping$2;
        public static final /* synthetic */ int[] $EnumSwitchMapping$3;
        public static final /* synthetic */ int[] $EnumSwitchMapping$4;
        public static final /* synthetic */ int[] $EnumSwitchMapping$5;

        static {
            MessageAttachmentType.values();
            int[] iArr = new int[3];
            $EnumSwitchMapping$0 = iArr;
            iArr[MessageAttachmentType.VIDEO.ordinal()] = 1;
            iArr[MessageAttachmentType.IMAGE.ordinal()] = 2;
            iArr[MessageAttachmentType.FILE.ordinal()] = 3;
            EmbedType.values();
            int[] iArr2 = new int[11];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[EmbedType.LINK.ordinal()] = 1;
            iArr2[EmbedType.HTML.ordinal()] = 2;
            EmbedType embedType = EmbedType.VIDEO;
            iArr2[embedType.ordinal()] = 3;
            EmbedType embedType2 = EmbedType.GIFV;
            iArr2[embedType2.ordinal()] = 4;
            iArr2[EmbedType.TWEET.ordinal()] = 5;
            EmbedType embedType3 = EmbedType.IMAGE;
            iArr2[embedType3.ordinal()] = 6;
            iArr2[EmbedType.ARTICLE.ordinal()] = 7;
            EmbedType.values();
            int[] iArr3 = new int[11];
            $EnumSwitchMapping$2 = iArr3;
            iArr3[embedType2.ordinal()] = 1;
            iArr3[embedType3.ordinal()] = 2;
            iArr3[embedType.ordinal()] = 3;
            EmbedType.values();
            int[] iArr4 = new int[11];
            $EnumSwitchMapping$3 = iArr4;
            iArr4[embedType3.ordinal()] = 1;
            iArr4[embedType2.ordinal()] = 2;
            EmbedType.values();
            int[] iArr5 = new int[11];
            $EnumSwitchMapping$4 = iArr5;
            iArr5[embedType2.ordinal()] = 1;
            iArr5[embedType3.ordinal()] = 2;
            EmbedType.values();
            int[] iArr6 = new int[11];
            $EnumSwitchMapping$5 = iArr6;
            iArr6[EmbedType.FILE.ordinal()] = 1;
            iArr6[embedType.ordinal()] = 2;
        }
    }

    private EmbedResourceUtils() {
    }

    private final RenderableEmbedMedia createFileRenderableMedia(String str) {
        StringBuilder R = a.R(FILE_SCHEME);
        R.append(getFileDrawable(str));
        return new RenderableEmbedMedia(R.toString(), 30, 40);
    }

    public static /* synthetic */ List getPreviewUrls$default(EmbedResourceUtils embedResourceUtils, String str, int i, int i2, boolean z2, int i3, Object obj) {
        if ((i3 & 8) != 0) {
            z2 = true;
        }
        return embedResourceUtils.getPreviewUrls(str, i, i2, z2);
    }

    public final Pair<Integer, Integer> calculateScaledSize(int i, int i2, int i3, int i4, Resources resources, int i5) {
        m.checkNotNullParameter(resources, "resources");
        float f = resources.getDisplayMetrics().density;
        float f2 = i;
        float f3 = f * f2;
        float f4 = i2;
        float f5 = f * f4;
        boolean z2 = false;
        boolean z3 = f3 > ((float) (i3 / 2)) || f5 > ((float) (i4 / 2));
        boolean z4 = i5 > 0 && i < i5;
        float f6 = i2 > 0 ? f2 / f4 : 0.0f;
        if (z3) {
            if (i > i2) {
                z2 = true;
            }
            float f7 = z2 ? i3 : i4 * f6;
            float f8 = z2 ? i3 / f6 : i4;
            if (z2) {
                float f9 = i4;
                if (f8 > f9) {
                    f7 *= f9 / f8;
                    f8 = f9;
                    return o.to(Integer.valueOf(d0.a0.a.roundToInt(f7)), Integer.valueOf(d0.a0.a.roundToInt(f8)));
                }
            }
            if (!z2) {
                float f10 = i3;
                if (f7 > f10) {
                    f8 *= f10 / f7;
                    f7 = f10;
                }
            }
            return o.to(Integer.valueOf(d0.a0.a.roundToInt(f7)), Integer.valueOf(d0.a0.a.roundToInt(f8)));
        } else if (!z4) {
            return o.to(Integer.valueOf(d0.a0.a.roundToInt(f3)), Integer.valueOf(d0.a0.a.roundToInt(f5)));
        } else {
            float f11 = i5;
            float f12 = (f11 / f2) * f4;
            float f13 = i4;
            if (f12 > f13) {
                z2 = true;
            }
            if (z2) {
                float f14 = f13 / f12;
                f11 *= f14;
                f12 *= f14;
            }
            return o.to(Integer.valueOf(d0.a0.a.roundToInt(f11)), Integer.valueOf(d0.a0.a.roundToInt(f12)));
        }
    }

    public final int computeMaximumImageWidthPx(Context context) {
        m.checkNotNullParameter(context, "context");
        Resources resources = context.getResources();
        return Math.min(1440, DisplayUtils.getScreenSize(context).width() - (resources.getDimensionPixelSize(R.dimen.uikit_guideline_chat) + resources.getDimensionPixelSize(R.dimen.chat_cell_horizontal_spacing_total)));
    }

    public final RenderableEmbedMedia createRenderableEmbedMediaFromAttachment(MessageAttachment messageAttachment) {
        m.checkNotNullParameter(messageAttachment, "attachment");
        int ordinal = messageAttachment.e().ordinal();
        if (ordinal == 0 || ordinal == 1) {
            return new RenderableEmbedMedia(messageAttachment.c(), messageAttachment.g(), messageAttachment.b());
        }
        if (ordinal == 2) {
            return createFileRenderableMedia(messageAttachment.a());
        }
        throw new NoWhenBranchMatchedException();
    }

    public final String getExternalOpenUrl(MessageEmbed messageEmbed) {
        EmbedProvider g;
        m.checkNotNullParameter(messageEmbed, "$this$getExternalOpenUrl");
        EmbedType k = messageEmbed.k();
        if (k == null) {
            return null;
        }
        int ordinal = k.ordinal();
        if (ordinal == 2) {
            EmbedVideo m = messageEmbed.m();
            if (m != null) {
                return m.c();
            }
            return null;
        } else if (ordinal == 6 && (g = messageEmbed.g()) != null) {
            return g.b();
        } else {
            return null;
        }
    }

    @DrawableRes
    public final int getFileDrawable(String str) {
        String substringAfterLast;
        String str2 = "";
        if (!(str == null || (substringAfterLast = w.substringAfterLast(str, ClassUtils.PACKAGE_SEPARATOR_CHAR, str2)) == null)) {
            str2 = substringAfterLast;
        }
        FileType fromExtension = FileType.Companion.getFromExtension(str2);
        return fromExtension != null ? fromExtension.getFileDrawable() : R.drawable.ic_file_unknown;
    }

    public final int getMAX_IMAGE_VIEW_HEIGHT_PX() {
        return MAX_IMAGE_VIEW_HEIGHT_PX;
    }

    /* JADX WARN: Code restructure failed: missing block: B:17:0x0025, code lost:
        if (r0 != 7) goto L18;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final com.discord.embed.RenderableEmbedMedia getPreviewImage(com.discord.api.message.embed.MessageEmbed r5) {
        /*
            r4 = this;
            java.lang.String r0 = "$this$getPreviewImage"
            d0.z.d.m.checkNotNullParameter(r5, r0)
            com.discord.api.message.embed.EmbedType r0 = r5.k()
            java.lang.String r1 = "$this$asRenderableEmbedMedia"
            r2 = 0
            if (r0 != 0) goto Lf
            goto L27
        Lf:
            int r0 = r0.ordinal()
            if (r0 == 0) goto L8d
            r3 = 1
            if (r0 == r3) goto L8d
            r3 = 2
            if (r0 == r3) goto L49
            r3 = 3
            if (r0 == r3) goto L8d
            r3 = 4
            if (r0 == r3) goto Lae
            r3 = 5
            if (r0 == r3) goto Lae
            r3 = 7
            if (r0 == r3) goto L49
        L27:
            com.discord.api.message.embed.EmbedImage r5 = r5.f()
            if (r5 == 0) goto Lae
            d0.z.d.m.checkNotNullParameter(r5, r1)
            com.discord.embed.RenderableEmbedMedia r2 = new com.discord.embed.RenderableEmbedMedia
            java.lang.String r0 = r5.b()
            if (r0 == 0) goto L39
            goto L3d
        L39:
            java.lang.String r0 = r5.c()
        L3d:
            java.lang.Integer r1 = r5.d()
            java.lang.Integer r5 = r5.a()
            r2.<init>(r0, r1, r5)
            goto Lae
        L49:
            com.discord.api.message.embed.EmbedThumbnail r0 = r5.h()
            if (r0 == 0) goto L6b
            d0.z.d.m.checkNotNullParameter(r0, r1)
            com.discord.embed.RenderableEmbedMedia r2 = new com.discord.embed.RenderableEmbedMedia
            java.lang.String r5 = r0.b()
            if (r5 == 0) goto L5b
            goto L5f
        L5b:
            java.lang.String r5 = r0.c()
        L5f:
            java.lang.Integer r1 = r0.d()
            java.lang.Integer r0 = r0.a()
            r2.<init>(r5, r1, r0)
            goto Lae
        L6b:
            com.discord.api.message.embed.EmbedVideo r5 = r5.m()
            if (r5 == 0) goto Lae
            d0.z.d.m.checkNotNullParameter(r5, r1)
            com.discord.embed.RenderableEmbedMedia r2 = new com.discord.embed.RenderableEmbedMedia
            java.lang.String r0 = r5.b()
            if (r0 == 0) goto L7d
            goto L81
        L7d:
            java.lang.String r0 = r5.c()
        L81:
            java.lang.Integer r1 = r5.d()
            java.lang.Integer r5 = r5.a()
            r2.<init>(r0, r1, r5)
            goto Lae
        L8d:
            com.discord.api.message.embed.EmbedThumbnail r5 = r5.h()
            if (r5 == 0) goto Lae
            d0.z.d.m.checkNotNullParameter(r5, r1)
            com.discord.embed.RenderableEmbedMedia r2 = new com.discord.embed.RenderableEmbedMedia
            java.lang.String r0 = r5.b()
            if (r0 == 0) goto L9f
            goto La3
        L9f:
            java.lang.String r0 = r5.c()
        La3:
            java.lang.Integer r1 = r5.d()
            java.lang.Integer r5 = r5.a()
            r2.<init>(r0, r1, r5)
        Lae:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.embed.EmbedResourceUtils.getPreviewImage(com.discord.api.message.embed.MessageEmbed):com.discord.embed.RenderableEmbedMedia");
    }

    public final List<String> getPreviewUrls(String str, int i, int i2, boolean z2) {
        m.checkNotNullParameter(str, "originalUrl");
        String str2 = str + "?width=" + i + "&height=" + i2;
        if (z2 && (t.startsWith$default(str, FILE_SCHEME, false, 2, null) || t.endsWith$default(str, ".gif", false, 2, null))) {
            return d0.t.m.listOf(str2);
        }
        StringBuilder V = a.V(str2, "&format=");
        V.append(StringUtilsKt.getSTATIC_IMAGE_EXTENSION());
        return n.listOf((Object[]) new String[]{V.toString(), str2});
    }

    public final boolean isAnimated(MessageEmbed messageEmbed) {
        m.checkNotNullParameter(messageEmbed, "$this$isAnimated");
        EmbedType k = messageEmbed.k();
        RenderableEmbedMedia previewImage = getPreviewImage(messageEmbed);
        return isAnimated(k, previewImage != null ? previewImage.a : null);
    }

    public final boolean isInlineEmbed(MessageEmbed messageEmbed) {
        EmbedType k;
        m.checkNotNullParameter(messageEmbed, "$this$isInlineEmbed");
        if ((getPreviewImage(messageEmbed) == null && messageEmbed.m() == null) || (k = messageEmbed.k()) == null) {
            return false;
        }
        int ordinal = k.ordinal();
        if (ordinal == 1 || ordinal == 2) {
            if (!(messageEmbed.a() == null && messageEmbed.j() == null)) {
                return false;
            }
        } else if (ordinal != 7) {
            return false;
        }
        return true;
    }

    public final boolean isPlayable(MessageEmbed messageEmbed) {
        m.checkNotNullParameter(messageEmbed, "$this$isPlayable");
        return u.contains(PLAYABLE_EMBED_TYPES, messageEmbed.k());
    }

    public final boolean isSimpleEmbed(MessageEmbed messageEmbed) {
        int ordinal;
        m.checkNotNullParameter(messageEmbed, "$this$isSimpleEmbed");
        EmbedType k = messageEmbed.k();
        if (k != null && ((ordinal = k.ordinal()) == 1 || ordinal == 7)) {
            return isInlineEmbed(messageEmbed);
        }
        return false;
    }

    public final boolean isAnimated(EmbedType embedType, String str) {
        if (embedType == null) {
            return false;
        }
        int ordinal = embedType.ordinal();
        if (ordinal != 1) {
            if (ordinal != 7) {
                return false;
            }
        } else if (str == null) {
            return false;
        } else {
            Locale locale = Locale.ROOT;
            m.checkNotNullExpressionValue(locale, "Locale.ROOT");
            String lowerCase = str.toLowerCase(locale);
            m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            if (lowerCase == null || !w.contains$default((CharSequence) lowerCase, (CharSequence) ".gif", false, 2, (Object) null)) {
                return false;
            }
        }
        return true;
    }
}
