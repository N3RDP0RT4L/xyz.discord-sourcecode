package com.discord.utilities;

import android.content.Context;
import android.content.pm.ShortcutManager;
import android.graphics.Bitmap;
import android.os.Build;
import androidx.core.content.pm.ShortcutInfoCompat;
import androidx.core.content.pm.ShortcutManagerCompat;
import androidx.core.graphics.drawable.IconCompat;
import com.discord.utilities.images.MGImagesBitmap;
import d0.t.n;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: ShareUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\n\u001a\u00020\u00072\u008f\u0001\u0010\u0006\u001a\u008a\u0001\u00120\u0012.\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0000 \u0004*\u0016\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0000\u0018\u00010\u00010\u0001\u0012\f\u0012\n \u0004*\u0004\u0018\u00010\u00050\u0005 \u0004*D\u00120\u0012.\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0000 \u0004*\u0016\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0000\u0018\u00010\u00010\u0001\u0012\f\u0012\n \u0004*\u0004\u0018\u00010\u00050\u0005\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\b\u0010\t"}, d2 = {"Lkotlin/Pair;", "", "Lcom/discord/utilities/ChannelShortcutInfo;", "", "kotlin.jvm.PlatformType", "Lcom/discord/utilities/images/MGImagesBitmap$CloseableBitmaps;", "<name for destructuring parameter 0>", "", "invoke", "(Lkotlin/Pair;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ShareUtils$updateDirectShareTargets$5 extends o implements Function1<Pair<? extends List<? extends Pair<? extends ChannelShortcutInfo, ? extends String>>, ? extends MGImagesBitmap.CloseableBitmaps>, Unit> {
    public final /* synthetic */ Context $context;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ShareUtils$updateDirectShareTargets$5(Context context) {
        super(1);
        this.$context = context;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Pair<? extends List<? extends Pair<? extends ChannelShortcutInfo, ? extends String>>, ? extends MGImagesBitmap.CloseableBitmaps> pair) {
        invoke2((Pair<? extends List<Pair<ChannelShortcutInfo, String>>, MGImagesBitmap.CloseableBitmaps>) pair);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Pair<? extends List<Pair<ChannelShortcutInfo, String>>, MGImagesBitmap.CloseableBitmaps> pair) {
        ShortcutInfoCompat shortcutInfo;
        List<Pair<ChannelShortcutInfo, String>> component1 = pair.component1();
        MGImagesBitmap.CloseableBitmaps component2 = pair.component2();
        try {
            m.checkNotNullExpressionValue(component1, "channelAndIconUriPairs");
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (Object obj : component1) {
                Boolean valueOf = Boolean.valueOf(((ChannelShortcutInfo) ((Pair) obj).component1()).isPinnedOnly());
                Object obj2 = linkedHashMap.get(valueOf);
                if (obj2 == null) {
                    obj2 = new ArrayList();
                    linkedHashMap.put(valueOf, obj2);
                }
                Pair pair2 = (Pair) obj;
                ChannelShortcutInfo channelShortcutInfo = (ChannelShortcutInfo) pair2.component1();
                shortcutInfo = ShareUtils.INSTANCE.toShortcutInfo(channelShortcutInfo.getChannel(), this.$context, IconCompat.createWithAdaptiveBitmap((Bitmap) component2.get((Object) ((String) pair2.component2()))), channelShortcutInfo.getRank());
                ((List) obj2).add(shortcutInfo);
            }
            Object obj3 = linkedHashMap.get(Boolean.FALSE);
            if (obj3 == null) {
                obj3 = n.emptyList();
            }
            List<ShortcutInfoCompat> list = (List) obj3;
            Object obj4 = linkedHashMap.get(Boolean.TRUE);
            if (obj4 == null) {
                obj4 = n.emptyList();
            }
            List list2 = (List) obj4;
            if (Build.VERSION.SDK_INT >= 25) {
                ShortcutManager shortcutManager = (ShortcutManager) this.$context.getSystemService(ShortcutManager.class);
                if (shortcutManager != null) {
                    ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(list, 10));
                    for (ShortcutInfoCompat shortcutInfoCompat : list) {
                        arrayList.add(shortcutInfoCompat.toShortcutInfo());
                    }
                    shortcutManager.setDynamicShortcuts(arrayList);
                }
            } else {
                ShortcutManagerCompat.removeAllDynamicShortcuts(this.$context);
                ShortcutManagerCompat.addDynamicShortcuts(this.$context, list);
            }
            ShortcutManagerCompat.updateShortcuts(this.$context, list2);
            th = null;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }
}
