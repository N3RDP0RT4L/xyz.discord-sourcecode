package com.discord.utilities.bugreports;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.SharedPreferences;
import com.discord.screenshot_detection.ScreenshotDetector;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.cache.SharedPreferencesProvider;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.bugreports.WidgetBugReport;
import d0.g;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: BugReportManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 (2\u00020\u0001:\u0001(B\u0019\u0012\u0006\u0010\u0017\u001a\u00020\u0016\u0012\b\b\u0002\u0010\"\u001a\u00020!¢\u0006\u0004\b&\u0010'J\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\u000b\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\t¢\u0006\u0004\b\u000b\u0010\fJ\u0015\u0010\u000e\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\t¢\u0006\u0004\b\u000e\u0010\fJ\r\u0010\u000f\u001a\u00020\t¢\u0006\u0004\b\u000f\u0010\u0010J\r\u0010\u0011\u001a\u00020\t¢\u0006\u0004\b\u0011\u0010\u0010J\r\u0010\u0012\u001a\u00020\u0006¢\u0006\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0014\u001a\u00020\t8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u0019\u0010\u0017\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u001d\u0010 \u001a\u00020\u001b8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001fR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\n\u0010\u0015R\u0019\u0010\"\u001a\u00020!8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%¨\u0006)"}, d2 = {"Lcom/discord/utilities/bugreports/BugReportManager;", "", "Landroid/content/Context;", "context", "Lcom/discord/screenshot_detection/ScreenshotDetector$Screenshot;", "screenshot", "", "onScreenshot", "(Landroid/content/Context;Lcom/discord/screenshot_detection/ScreenshotDetector$Screenshot;)V", "", "isStaff", "setUserIsStaff", "(Z)V", "enabled", "setBugReportingSettingEnabled", "isEnabled", "()Z", "isBugReportSettingEnabled", "setupSubscriptions", "()V", "settingsEnabled", "Z", "Landroid/content/SharedPreferences;", "cache", "Landroid/content/SharedPreferences;", "getCache", "()Landroid/content/SharedPreferences;", "Lcom/discord/screenshot_detection/ScreenshotDetector;", "screenshotDetector$delegate", "Lkotlin/Lazy;", "getScreenshotDetector", "()Lcom/discord/screenshot_detection/ScreenshotDetector;", "screenshotDetector", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreUser;", "getStoreUser", "()Lcom/discord/stores/StoreUser;", HookHelper.constructorName, "(Landroid/content/SharedPreferences;Lcom/discord/stores/StoreUser;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class BugReportManager {
    public static final Companion Companion = new Companion(null);
    public static final String PREFS_SS_BUG_REPORTING_SETTINGS_ENABLED = "prefs_ss_bug_reporting_enabled";
    private static BugReportManager bugReportManager;
    private final SharedPreferences cache;
    private boolean isStaff;
    private final Lazy screenshotDetector$delegate;
    private boolean settingsEnabled;
    private final StoreUser storeUser;

    /* compiled from: BugReportManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\r\u0010\u0004J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\r\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\t\u001a\u00020\b8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\t\u0010\nR\u0016\u0010\u000b\u001a\u00020\u00058\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u000e"}, d2 = {"Lcom/discord/utilities/bugreports/BugReportManager$Companion;", "", "", "init", "()V", "Lcom/discord/utilities/bugreports/BugReportManager;", "get", "()Lcom/discord/utilities/bugreports/BugReportManager;", "", "PREFS_SS_BUG_REPORTING_SETTINGS_ENABLED", "Ljava/lang/String;", "bugReportManager", "Lcom/discord/utilities/bugreports/BugReportManager;", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public final BugReportManager get() {
            BugReportManager bugReportManager = BugReportManager.bugReportManager;
            if (bugReportManager == null) {
                m.throwUninitializedPropertyAccessException("bugReportManager");
            }
            return bugReportManager;
        }

        public final void init() {
            BugReportManager.bugReportManager = new BugReportManager(SharedPreferencesProvider.INSTANCE.get(), null, 2, null);
            BugReportManager bugReportManager = BugReportManager.bugReportManager;
            if (bugReportManager == null) {
                m.throwUninitializedPropertyAccessException("bugReportManager");
            }
            bugReportManager.setupSubscriptions();
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public BugReportManager(SharedPreferences sharedPreferences, StoreUser storeUser) {
        m.checkNotNullParameter(sharedPreferences, "cache");
        m.checkNotNullParameter(storeUser, "storeUser");
        this.cache = sharedPreferences;
        this.storeUser = storeUser;
        this.settingsEnabled = true;
        this.screenshotDetector$delegate = g.lazy(BugReportManager$screenshotDetector$2.INSTANCE);
        this.settingsEnabled = sharedPreferences.getBoolean(PREFS_SS_BUG_REPORTING_SETTINGS_ENABLED, true);
    }

    private final ScreenshotDetector getScreenshotDetector() {
        return (ScreenshotDetector) this.screenshotDetector$delegate.getValue();
    }

    public final SharedPreferences getCache() {
        return this.cache;
    }

    public final StoreUser getStoreUser() {
        return this.storeUser;
    }

    public final boolean isBugReportSettingEnabled() {
        return this.settingsEnabled;
    }

    public final boolean isEnabled() {
        return this.isStaff && this.settingsEnabled;
    }

    public final void onScreenshot(Context context, ScreenshotDetector.Screenshot screenshot) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(screenshot, "screenshot");
        if (isEnabled()) {
            WidgetBugReport.Companion.launch(context, screenshot);
        }
    }

    public final void setBugReportingSettingEnabled(boolean z2) {
        SharedPreferences.Editor edit = this.cache.edit();
        m.checkNotNullExpressionValue(edit, "editor");
        edit.putBoolean(PREFS_SS_BUG_REPORTING_SETTINGS_ENABLED, z2);
        edit.apply();
        this.settingsEnabled = z2;
        getScreenshotDetector().a(isEnabled());
    }

    public final void setUserIsStaff(boolean z2) {
        this.isStaff = z2;
        getScreenshotDetector().a(isEnabled());
    }

    public final void setupSubscriptions() {
        Observable q = this.storeUser.observeMe(true).F(BugReportManager$setupSubscriptions$1.INSTANCE).q();
        m.checkNotNullExpressionValue(q, "storeUser.observeMe(emit… }.distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(q, BugReportManager.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new BugReportManager$setupSubscriptions$2(this));
    }

    public /* synthetic */ BugReportManager(SharedPreferences sharedPreferences, StoreUser storeUser, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(sharedPreferences, (i & 2) != 0 ? StoreStream.Companion.getUsers() : storeUser);
    }
}
