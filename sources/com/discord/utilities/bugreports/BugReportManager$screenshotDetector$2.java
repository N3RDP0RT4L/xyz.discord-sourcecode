package com.discord.utilities.bugreports;

import com.discord.screenshot_detection.ScreenshotDetector;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: BugReportManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/screenshot_detection/ScreenshotDetector;", "invoke", "()Lcom/discord/screenshot_detection/ScreenshotDetector;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class BugReportManager$screenshotDetector$2 extends o implements Function0<ScreenshotDetector> {
    public static final BugReportManager$screenshotDetector$2 INSTANCE = new BugReportManager$screenshotDetector$2();

    public BugReportManager$screenshotDetector$2() {
        super(0);
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final ScreenshotDetector invoke() {
        ScreenshotDetector screenshotDetector = ScreenshotDetector.a;
        if (screenshotDetector == null) {
            m.throwUninitializedPropertyAccessException("screenshotDetector");
        }
        return screenshotDetector;
    }
}
