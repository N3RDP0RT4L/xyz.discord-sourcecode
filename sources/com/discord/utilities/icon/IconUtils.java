package com.discord.utilities.icon;

import andhook.lib.HookHelper;
import andhook.lib.xposed.ClassUtils;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.widget.ImageView;
import androidx.annotation.DimenRes;
import b.d.b.a.a;
import com.discord.BuildConfig;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.role.GuildRole;
import com.discord.models.commands.Application;
import com.discord.models.commands.ApplicationKt;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.user.CoreUser;
import com.discord.models.user.User;
import com.discord.nullserializable.NullSerializable;
import com.discord.utilities.SnowflakeUtils;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.string.StringUtilsKt;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import d0.g0.t;
import d0.g0.w;
import d0.t.c0;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.ranges.IntRange;
import xyz.discord.R;
/* compiled from: IconUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000¢\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0018\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\b\n\u0002\u0010\u0011\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\u00020\u0001B\u000b\b\u0002¢\u0006\u0006\b\u0083\u0001\u0010\u0084\u0001JM\u0010\u000e\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\b\b\u0003\u0010\u0007\u001a\u00020\u00062\u0016\b\u0002\u0010\u000b\u001a\u0010\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n\u0018\u00010\b2\b\b\u0002\u0010\r\u001a\u00020\fH\u0007¢\u0006\u0004\b\u000e\u0010\u000fJ]\u0010\u000e\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0010\u001a\u00020\u00062\u0006\u0010\u0011\u001a\u00020\u00062\b\b\u0002\u0010\u0013\u001a\u00020\u00122\u0016\b\u0002\u0010\u000b\u001a\u0010\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n\u0018\u00010\b2\b\b\u0002\u0010\r\u001a\u00020\fH\u0007¢\u0006\u0004\b\u000e\u0010\u0014JY\u0010\u000e\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0016\u001a\u0004\u0018\u00010\u00152\b\b\u0003\u0010\u0007\u001a\u00020\u00062\u0016\b\u0002\u0010\u000b\u001a\u0010\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n\u0018\u00010\b2\b\b\u0002\u0010\r\u001a\u00020\f2\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0017H\u0007¢\u0006\u0004\b\u000e\u0010\u0019J}\u0010\u000e\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u001a\u001a\u0004\u0018\u00010\u00042\u000e\u0010\u001d\u001a\n\u0018\u00010\u001bj\u0004\u0018\u0001`\u001c2\b\u0010\u001e\u001a\u0004\u0018\u00010\u00042\b\u0010\u001f\u001a\u0004\u0018\u00010\u00062\b\b\u0003\u0010\u0007\u001a\u00020\u00062\u0016\b\u0002\u0010\u000b\u001a\u0010\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n\u0018\u00010\b2\b\b\u0002\u0010\r\u001a\u00020\f2\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0017H\u0007¢\u0006\u0004\b\u000e\u0010 J5\u0010\u000e\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\"\u001a\u0004\u0018\u00010!2\b\b\u0003\u0010\u0007\u001a\u00020\u00062\b\b\u0002\u0010\r\u001a\u00020\fH\u0007¢\u0006\u0004\b\u000e\u0010#J?\u0010\u000e\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\"\u001a\u0004\u0018\u00010$2\b\b\u0003\u0010\u0007\u001a\u00020\u00062\b\b\u0002\u0010\r\u001a\u00020\f2\b\b\u0002\u0010%\u001a\u00020\u0012H\u0007¢\u0006\u0004\b\u000e\u0010&J5\u0010\u000e\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\"\u001a\u0004\u0018\u00010'2\b\b\u0003\u0010\u0007\u001a\u00020\u00062\b\b\u0002\u0010\r\u001a\u00020\fH\u0007¢\u0006\u0004\b\u000e\u0010(J1\u0010*\u001a\u0004\u0018\u00010\u00042\b\u0010\u0016\u001a\u0004\u0018\u00010\u00152\b\b\u0002\u0010%\u001a\u00020\u00122\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u0006H\u0007¢\u0006\u0004\b*\u0010+J%\u0010,\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\b\u0010)\u001a\u0004\u0018\u00010\u0006H\u0002¢\u0006\u0004\b,\u0010-JM\u0010*\u001a\u0004\u0018\u00010\u00042\u000e\u0010\u001d\u001a\n\u0018\u00010\u001bj\u0004\u0018\u0001`\u001c2\b\u0010\u001e\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010%\u001a\u00020\u00122\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u0006H\u0007¢\u0006\u0004\b*\u0010.J?\u00103\u001a\u0004\u0018\u00010\u00042\u0006\u0010/\u001a\u00020\u001b2\b\u00100\u001a\u0004\u0018\u00010\u00042\u0006\u00101\u001a\u00020\u00062\u0006\u00102\u001a\u00020\u00122\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u0006H\u0007¢\u0006\u0004\b3\u00104J'\u00103\u001a\u0004\u0018\u00010\u00042\b\u00105\u001a\u0004\u0018\u00010!2\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u0006H\u0007¢\u0006\u0004\b3\u00106JG\u00108\u001a\u0004\u0018\u00010\u00042\b\u0010/\u001a\u0004\u0018\u00010\u001b2\b\u00100\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u00107\u001a\u0004\u0018\u00010\u00042\b\b\u0002\u0010%\u001a\u00020\u00122\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u0006H\u0007¢\u0006\u0004\b8\u00109J=\u00108\u001a\u0004\u0018\u00010\u00042\b\u0010:\u001a\u0004\u0018\u00010$2\n\b\u0002\u00107\u001a\u0004\u0018\u00010\u00042\b\b\u0002\u0010%\u001a\u00020\u00122\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u0006H\u0007¢\u0006\u0004\b8\u0010;J-\u0010@\u001a\u00020\u00042\n\u0010=\u001a\u00060\u001bj\u0002`<2\u0006\u0010>\u001a\u00020\u00042\b\b\u0002\u0010?\u001a\u00020\u0006H\u0007¢\u0006\u0004\b@\u0010AJ\u001f\u0010D\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010C\u001a\u00020BH\u0007¢\u0006\u0004\bD\u0010EJ!\u0010H\u001a\u0004\u0018\u00010\u00042\u0006\u0010F\u001a\u00020\u00042\u0006\u0010G\u001a\u00020\u0004H\u0002¢\u0006\u0004\bH\u0010IJ\u0017\u0010J\u001a\u00020\u00062\u0006\u0010)\u001a\u00020\u0006H\u0007¢\u0006\u0004\bJ\u0010KJ9\u0010L\u001a\u0004\u0018\u00010\u00042\b\u0010\u0016\u001a\u0004\u0018\u00010\u00152\b\u0010\u0018\u001a\u0004\u0018\u00010\u00172\n\b\u0002\u0010?\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010%\u001a\u00020\u0012¢\u0006\u0004\bL\u0010MJ-\u0010N\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0018\u001a\u00020\u00172\n\b\u0002\u0010?\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010%\u001a\u00020\u0012¢\u0006\u0004\bN\u0010OJG\u0010N\u001a\u0004\u0018\u00010\u00042\b\u0010P\u001a\u0004\u0018\u00010\u00042\n\u0010R\u001a\u00060\u001bj\u0002`Q2\n\u0010\u001d\u001a\u00060\u001bj\u0002`\u001c2\n\b\u0002\u0010?\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010%\u001a\u00020\u0012¢\u0006\u0004\bN\u0010SJ;\u0010U\u001a\u0004\u0018\u00010\u00042\n\u0010\u001d\u001a\u00060\u001bj\u0002`\u001c2\b\u0010T\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010?\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010%\u001a\u00020\u0012¢\u0006\u0004\bU\u0010VJG\u0010X\u001a\u0004\u0018\u00010\u00042\b\u0010W\u001a\u0004\u0018\u00010\u00042\n\u0010R\u001a\u00060\u001bj\u0002`Q2\n\u0010\u001d\u001a\u00060\u001bj\u0002`\u001c2\n\b\u0002\u0010?\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010%\u001a\u00020\u0012¢\u0006\u0004\bX\u0010SJ\u0019\u0010Z\u001a\u00020\u00042\n\u0010/\u001a\u00060\u001bj\u0002`Y¢\u0006\u0004\bZ\u0010[J/\u0010\\\u001a\u0004\u0018\u00010\u00042\b\u0010:\u001a\u0004\u0018\u00010$2\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010%\u001a\u00020\u0012¢\u0006\u0004\b\\\u0010]J?\u0010\\\u001a\u0004\u0018\u00010\u00042\u000e\u0010R\u001a\n\u0018\u00010\u001bj\u0004\u0018\u0001`Q2\b\u0010^\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010%\u001a\u00020\u0012¢\u0006\u0004\b\\\u0010_J-\u0010`\u001a\u0004\u0018\u00010\u00042\b\u0010=\u001a\u0004\u0018\u00010\u001b2\u0006\u0010>\u001a\u00020\u00042\b\b\u0002\u0010?\u001a\u00020\u0006H\u0007¢\u0006\u0004\b`\u0010aJ-\u0010b\u001a\u0004\u0018\u00010\u00042\b\u0010=\u001a\u0004\u0018\u00010\u001b2\u0006\u0010>\u001a\u00020\u00042\b\b\u0002\u0010)\u001a\u00020\u0006H\u0007¢\u0006\u0004\bb\u0010aJ1\u0010d\u001a\u0004\u0018\u00010\u00042\n\u0010=\u001a\u00060\u001bj\u0002`<2\b\u0010c\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\bd\u0010eJ\u001f\u0010@\u001a\u00020\u00042\u0006\u0010C\u001a\u00020B2\b\b\u0002\u0010?\u001a\u00020\u0006¢\u0006\u0004\b@\u0010fJ-\u0010h\u001a\u00020\u00042\n\u0010R\u001a\u00060\u001bj\u0002`Q2\u0006\u0010g\u001a\u00020\u00042\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\bh\u0010eJ%\u0010h\u001a\u0004\u0018\u00010\u00042\b\u0010:\u001a\u0004\u0018\u00010$2\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\bh\u0010iJ\u0015\u0010k\u001a\u00020\u00122\u0006\u0010j\u001a\u00020\u0004¢\u0006\u0004\bk\u0010lJ\u0015\u0010n\u001a\u00020\u00122\u0006\u0010m\u001a\u00020\u0004¢\u0006\u0004\bn\u0010lJ\u001d\u0010p\u001a\u00020\u00042\u0006\u0010j\u001a\u00020\u00042\u0006\u0010o\u001a\u00020\u0012¢\u0006\u0004\bp\u0010qJ-\u0010u\u001a\u00020\u00042\n\u0010s\u001a\u00060\u001bj\u0002`r2\u0006\u0010t\u001a\u00020\u00042\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\bu\u0010eR\u001c\u0010w\u001a\b\u0012\u0004\u0012\u00020\u00040v8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bw\u0010xR\u0016\u0010y\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\by\u0010zR\u0016\u0010{\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b{\u0010zR\u0016\u0010|\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b|\u0010zR\u0016\u0010}\u001a\u00020\u00068\u0002@\u0003X\u0083T¢\u0006\u0006\n\u0004\b}\u0010~R\u001f\u0010\u0080\u0001\u001a\b\u0012\u0004\u0012\u00020\u00060\u007f8\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0080\u0001\u0010\u0081\u0001R\u0018\u0010\u0082\u0001\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0007\n\u0005\b\u0082\u0001\u0010~¨\u0006\u0085\u0001"}, d2 = {"Lcom/discord/utilities/icon/IconUtils;", "", "Landroid/widget/ImageView;", "imageView", "", "url", "", "sizeDimenRes", "Lkotlin/Function1;", "Lcom/facebook/imagepipeline/request/ImageRequestBuilder;", "", "transform", "Lcom/discord/utilities/images/MGImages$ChangeDetector;", "changeDetector", "setIcon", "(Landroid/widget/ImageView;Ljava/lang/String;ILkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;)V", "widthPixels", "heightPixels", "", "useSmallCache", "(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;)V", "Lcom/discord/models/user/User;", "user", "Lcom/discord/models/member/GuildMember;", "guildMember", "(Landroid/widget/ImageView;Lcom/discord/models/user/User;ILkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;Lcom/discord/models/member/GuildMember;)V", "username", "", "Lcom/discord/primitives/UserId;", "userId", "userAvatar", "discriminator", "(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Integer;ILkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;Lcom/discord/models/member/GuildMember;)V", "Lcom/discord/api/channel/Channel;", "entity", "(Landroid/widget/ImageView;Lcom/discord/api/channel/Channel;ILcom/discord/utilities/images/MGImages$ChangeDetector;)V", "Lcom/discord/models/guild/Guild;", "animated", "(Landroid/widget/ImageView;Lcom/discord/models/guild/Guild;ILcom/discord/utilities/images/MGImages$ChangeDetector;Z)V", "Lcom/discord/api/role/GuildRole;", "(Landroid/widget/ImageView;Lcom/discord/api/role/GuildRole;ILcom/discord/utilities/images/MGImages$ChangeDetector;)V", "size", "getForUser", "(Lcom/discord/models/user/User;ZLjava/lang/Integer;)Ljava/lang/String;", "withSize", "(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/String;", "(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Integer;ZLjava/lang/Integer;)Ljava/lang/String;", ModelAuditLogEntry.CHANGE_KEY_ID, "icon", "type", "returnDefaultAsset", "getForChannel", "(JLjava/lang/String;IZLjava/lang/Integer;)Ljava/lang/String;", "channel", "(Lcom/discord/api/channel/Channel;Ljava/lang/Integer;)Ljava/lang/String;", "defaultIcon", "getForGuild", "(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Integer;)Ljava/lang/String;", "guild", "(Lcom/discord/models/guild/Guild;Ljava/lang/String;ZLjava/lang/Integer;)Ljava/lang/String;", "Lcom/discord/primitives/ApplicationId;", "applicationId", "imageId", "sizePx", "getApplicationIcon", "(JLjava/lang/String;I)Ljava/lang/String;", "Lcom/discord/models/commands/Application;", "application", "setApplicationIcon", "(Landroid/widget/ImageView;Lcom/discord/models/commands/Application;)V", "platform", "data", "getAssetPlatformUrl", "(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;", "getMediaProxySize", "(I)I", "getForGuildMemberOrUser", "(Lcom/discord/models/user/User;Lcom/discord/models/member/GuildMember;Ljava/lang/Integer;Z)Ljava/lang/String;", "getForGuildMember", "(Lcom/discord/models/member/GuildMember;Ljava/lang/Integer;Z)Ljava/lang/String;", "guildMemberAvatar", "Lcom/discord/primitives/GuildId;", "guildId", "(Ljava/lang/String;JJLjava/lang/Integer;Z)Ljava/lang/String;", "bannerHash", "getForUserBanner", "(JLjava/lang/String;Ljava/lang/Integer;Z)Ljava/lang/String;", "guildMemberBanner", "getForGuildMemberBanner", "Lcom/discord/primitives/ChannelId;", "getDefaultForGroupDM", "(J)Ljava/lang/String;", "getBannerForGuild", "(Lcom/discord/models/guild/Guild;Ljava/lang/Integer;Z)Ljava/lang/String;", "banner", "(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Integer;Z)Ljava/lang/String;", "getAssetImage", "(Ljava/lang/Long;Ljava/lang/String;I)Ljava/lang/String;", "getStoreAssetImage", "hash", "getGiftSplashUrl", "(JLjava/lang/String;Ljava/lang/Integer;)Ljava/lang/String;", "(Lcom/discord/models/commands/Application;I)Ljava/lang/String;", "splashHash", "getGuildSplashUrl", "(Lcom/discord/models/guild/Guild;Ljava/lang/Integer;)Ljava/lang/String;", "imageHash", "isImageHashAnimated", "(Ljava/lang/String;)Z", "dataUrl", "isDataUrlForGif", "allowAnimation", "getImageExtension", "(Ljava/lang/String;Z)Ljava/lang/String;", "Lcom/discord/primitives/RoleId;", "roleId", "iconHash", "getRoleIconUrl", "", "GROUP_DM_DEFAULT_ICONS", "Ljava/util/List;", "DEFAULT_ICON_BLURPLE", "Ljava/lang/String;", "DEFAULT_ICON", "ANIMATED_IMAGE_EXTENSION", "UNRESTRICTED", "I", "", "MEDIA_PROXY_SIZES", "[Ljava/lang/Integer;", "IMAGE_SIZE_ASSET_DEFAULT_PX", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class IconUtils {
    public static final String ANIMATED_IMAGE_EXTENSION = "gif";
    public static final String DEFAULT_ICON = "asset://asset/images/default_icon.jpg";
    public static final String DEFAULT_ICON_BLURPLE = "asset://asset/images/default_icon_selected.jpg";
    private static final List<String> GROUP_DM_DEFAULT_ICONS;
    private static final int IMAGE_SIZE_ASSET_DEFAULT_PX = 160;
    public static final IconUtils INSTANCE = new IconUtils();
    private static final Integer[] MEDIA_PROXY_SIZES;
    @DimenRes
    private static final int UNRESTRICTED = 2131165301;

    static {
        IntRange intRange = new IntRange(0, 7);
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(intRange, 10));
        Iterator<Integer> it = intRange.iterator();
        while (it.hasNext()) {
            arrayList.add("asset://asset/images/group_dm_icon_" + ((c0) it).nextInt() + ".png");
        }
        GROUP_DM_DEFAULT_ICONS = arrayList;
        MEDIA_PROXY_SIZES = new Integer[]{16, 20, 22, 24, 28, 32, 40, 44, 48, 56, 60, 64, 80, 96, 100, 128, 160, 240, 256, 300, 320, 480, 512, 600, 640, 1024, 1280, 1536, 2048, 3072, 4096};
    }

    private IconUtils() {
    }

    public static final String getApplicationIcon(long j, String str, int i) {
        m.checkNotNullParameter(str, "imageId");
        int mediaProxySize = getMediaProxySize(i);
        return "https://cdn.discordapp.com/app-icons/" + j + MentionUtilsKt.SLASH_CHAR + str + ClassUtils.PACKAGE_SEPARATOR_CHAR + StringUtilsKt.getSTATIC_IMAGE_EXTENSION() + "?size=" + mediaProxySize;
    }

    public static /* synthetic */ String getApplicationIcon$default(long j, String str, int i, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            i = 160;
        }
        return getApplicationIcon(j, str, i);
    }

    public static /* synthetic */ String getAssetImage$default(IconUtils iconUtils, Long l, String str, int i, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            i = 160;
        }
        return iconUtils.getAssetImage(l, str, i);
    }

    private final String getAssetPlatformUrl(String str, String str2) {
        int hashCode = str.hashCode();
        if (hashCode != -1998723398) {
            if (hashCode == 3491 && str.equals("mp")) {
                return a.v("https://media.discordapp.net/", str2);
            }
        } else if (str.equals("spotify")) {
            return a.v("https://i.scdn.co/image/", str2);
        }
        return null;
    }

    public static /* synthetic */ String getBannerForGuild$default(IconUtils iconUtils, Guild guild, Integer num, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            num = null;
        }
        if ((i & 4) != 0) {
            z2 = false;
        }
        return iconUtils.getBannerForGuild(guild, num, z2);
    }

    public static final String getForChannel(long j, String str, int i, boolean z2, Integer num) {
        boolean z3 = false;
        if (!(str == null || str.length() == 0)) {
            if (BuildConfig.HOST_CDN.length() == 0) {
                z3 = true;
            }
            if (z3) {
                return "https://discord.com/api//channels/" + j + "/icons/" + str + ".jpg";
            }
            IconUtils iconUtils = INSTANCE;
            return iconUtils.withSize("https://cdn.discordapp.com/channel-icons/" + j + MentionUtilsKt.SLASH_CHAR + str + ClassUtils.PACKAGE_SEPARATOR_CHAR + StringUtilsKt.getSTATIC_IMAGE_EXTENSION(), num);
        } else if (i == 3) {
            return INSTANCE.getDefaultForGroupDM(j);
        } else {
            if (z2) {
                return "";
            }
            return null;
        }
    }

    public static /* synthetic */ String getForChannel$default(long j, String str, int i, boolean z2, Integer num, int i2, Object obj) {
        if ((i2 & 16) != 0) {
            num = null;
        }
        return getForChannel(j, str, i, z2, num);
    }

    public static final String getForGuild(Guild guild) {
        return getForGuild$default(guild, null, false, null, 14, null);
    }

    public static final String getForGuild(Guild guild, String str) {
        return getForGuild$default(guild, str, false, null, 12, null);
    }

    public static final String getForGuild(Guild guild, String str, boolean z2) {
        return getForGuild$default(guild, str, z2, null, 8, null);
    }

    public static final String getForGuild(Long l, String str) {
        return getForGuild$default(l, str, null, false, null, 28, null);
    }

    public static final String getForGuild(Long l, String str, String str2) {
        return getForGuild$default(l, str, str2, false, null, 24, null);
    }

    public static final String getForGuild(Long l, String str, String str2, boolean z2) {
        return getForGuild$default(l, str, str2, z2, null, 16, null);
    }

    public static final String getForGuild(Long l, String str, String str2, boolean z2, Integer num) {
        String str3;
        if (TextUtils.isEmpty(str) || l == null) {
            return str2;
        }
        String str4 = null;
        Integer valueOf = num != null ? Integer.valueOf(getMediaProxySize(num.intValue())) : null;
        StringBuilder sb = new StringBuilder();
        if (BuildConfig.HOST_CDN.length() == 0) {
            str3 = "https://discord.com/api//guilds/" + l + "/icons/" + str + ".jpg";
        } else {
            IconUtils iconUtils = INSTANCE;
            m.checkNotNull(str);
            str3 = "https://cdn.discordapp.com/icons/" + l + MentionUtilsKt.SLASH_CHAR + str + ClassUtils.PACKAGE_SEPARATOR_CHAR + iconUtils.getImageExtension(str, z2);
        }
        sb.append(str3);
        if (valueOf != null) {
            str4 = a.p("?size=", valueOf.intValue());
        }
        if (str4 == null) {
            str4 = "";
        }
        sb.append(str4);
        return sb.toString();
    }

    public static /* synthetic */ String getForGuild$default(Long l, String str, String str2, boolean z2, Integer num, int i, Object obj) {
        if ((i & 4) != 0) {
            str2 = null;
        }
        if ((i & 8) != 0) {
            z2 = false;
        }
        if ((i & 16) != 0) {
            num = null;
        }
        return getForGuild(l, str, str2, z2, num);
    }

    public static /* synthetic */ String getForGuildMember$default(IconUtils iconUtils, GuildMember guildMember, Integer num, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            num = null;
        }
        if ((i & 4) != 0) {
            z2 = false;
        }
        return iconUtils.getForGuildMember(guildMember, num, z2);
    }

    public static /* synthetic */ String getForGuildMemberOrUser$default(IconUtils iconUtils, User user, GuildMember guildMember, Integer num, boolean z2, int i, Object obj) {
        if ((i & 4) != 0) {
            num = null;
        }
        if ((i & 8) != 0) {
            z2 = false;
        }
        return iconUtils.getForGuildMemberOrUser(user, guildMember, num, z2);
    }

    public static final String getForUser(User user) {
        return getForUser$default(user, false, null, 6, null);
    }

    public static final String getForUser(User user, boolean z2) {
        return getForUser$default(user, z2, null, 4, null);
    }

    public static final String getForUser(User user, boolean z2, Integer num) {
        String str;
        Integer num2 = null;
        Long valueOf = user != null ? Long.valueOf(user.getId()) : null;
        if (user != null) {
            str = user.getAvatar();
        } else {
            str = null;
        }
        if (user != null) {
            num2 = Integer.valueOf(user.getDiscriminator());
        }
        return getForUser(valueOf, str, num2, z2, num);
    }

    public static final String getForUser(Long l, String str) {
        return getForUser$default(l, str, null, false, null, 28, null);
    }

    public static final String getForUser(Long l, String str, Integer num) {
        return getForUser$default(l, str, num, false, null, 24, null);
    }

    public static final String getForUser(Long l, String str, Integer num, boolean z2) {
        return getForUser$default(l, str, num, z2, null, 16, null);
    }

    public static /* synthetic */ String getForUser$default(User user, boolean z2, Integer num, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        if ((i & 4) != 0) {
            num = null;
        }
        return getForUser(user, z2, num);
    }

    public static /* synthetic */ String getForUserBanner$default(IconUtils iconUtils, long j, String str, Integer num, boolean z2, int i, Object obj) {
        if ((i & 4) != 0) {
            num = null;
        }
        return iconUtils.getForUserBanner(j, str, num, (i & 8) != 0 ? false : z2);
    }

    public static /* synthetic */ String getGiftSplashUrl$default(IconUtils iconUtils, long j, String str, Integer num, int i, Object obj) {
        if ((i & 4) != 0) {
            num = null;
        }
        return iconUtils.getGiftSplashUrl(j, str, num);
    }

    public static /* synthetic */ String getGuildSplashUrl$default(IconUtils iconUtils, long j, String str, Integer num, int i, Object obj) {
        if ((i & 4) != 0) {
            num = null;
        }
        return iconUtils.getGuildSplashUrl(j, str, num);
    }

    public static final int getMediaProxySize(int i) {
        Integer[] numArr;
        for (Integer num : MEDIA_PROXY_SIZES) {
            if (num.intValue() >= i) {
                return num.intValue();
            }
        }
        throw new NoSuchElementException("Array contains no element matching the predicate.");
    }

    public static /* synthetic */ String getRoleIconUrl$default(IconUtils iconUtils, long j, String str, Integer num, int i, Object obj) {
        if ((i & 4) != 0) {
            num = null;
        }
        return iconUtils.getRoleIconUrl(j, str, num);
    }

    public static /* synthetic */ String getStoreAssetImage$default(IconUtils iconUtils, Long l, String str, int i, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            i = 160;
        }
        return iconUtils.getStoreAssetImage(l, str, i);
    }

    public static final void setApplicationIcon(ImageView imageView, Application application) {
        m.checkNotNullParameter(imageView, "imageView");
        m.checkNotNullParameter(application, "application");
        String applicationIcon$default = getApplicationIcon$default(INSTANCE, application, 0, 2, (Object) null);
        if (application.getIconRes() == null || ApplicationKt.hasBotAvatar(application)) {
            MGImages.setImage$default(imageView, applicationIcon$default, 0, 0, false, null, null, 124, null);
            return;
        }
        int themedColor = ColorCompat.getThemedColor(imageView, (int) R.attr.colorTextMuted);
        Context context = imageView.getContext();
        m.checkNotNullExpressionValue(context, "imageView.context");
        Drawable drawable$default = DrawableCompat.getDrawable$default(context, application.getIconRes().intValue(), themedColor, false, 4, null);
        if (drawable$default != null) {
            MGImages.setImage$default(MGImages.INSTANCE, imageView, drawable$default, (MGImages.ChangeDetector) null, 4, (Object) null);
        } else {
            MGImages.setImage$default(MGImages.INSTANCE, imageView, application.getIconRes().intValue(), (MGImages.ChangeDetector) null, 4, (Object) null);
        }
    }

    public static final void setIcon(ImageView imageView, Channel channel) {
        setIcon$default(imageView, channel, 0, (MGImages.ChangeDetector) null, 12, (Object) null);
    }

    public static final void setIcon(ImageView imageView, Channel channel, @DimenRes int i) {
        setIcon$default(imageView, channel, i, (MGImages.ChangeDetector) null, 8, (Object) null);
    }

    public static final void setIcon(ImageView imageView, GuildRole guildRole) {
        setIcon$default(imageView, guildRole, 0, (MGImages.ChangeDetector) null, 12, (Object) null);
    }

    public static final void setIcon(ImageView imageView, GuildRole guildRole, @DimenRes int i) {
        setIcon$default(imageView, guildRole, i, (MGImages.ChangeDetector) null, 8, (Object) null);
    }

    public static final void setIcon(ImageView imageView, Guild guild) {
        setIcon$default(imageView, guild, 0, (MGImages.ChangeDetector) null, false, 28, (Object) null);
    }

    public static final void setIcon(ImageView imageView, Guild guild, @DimenRes int i) {
        setIcon$default(imageView, guild, i, (MGImages.ChangeDetector) null, false, 24, (Object) null);
    }

    public static final void setIcon(ImageView imageView, Guild guild, @DimenRes int i, MGImages.ChangeDetector changeDetector) {
        setIcon$default(imageView, guild, i, changeDetector, false, 16, (Object) null);
    }

    public static final void setIcon(ImageView imageView, User user) {
        setIcon$default(imageView, user, 0, null, null, null, 60, null);
    }

    public static final void setIcon(ImageView imageView, User user, @DimenRes int i) {
        setIcon$default(imageView, user, i, null, null, null, 56, null);
    }

    public static final void setIcon(ImageView imageView, User user, @DimenRes int i, Function1<? super ImageRequestBuilder, Unit> function1) {
        setIcon$default(imageView, user, i, function1, null, null, 48, null);
    }

    public static final void setIcon(ImageView imageView, User user, @DimenRes int i, Function1<? super ImageRequestBuilder, Unit> function1, MGImages.ChangeDetector changeDetector) {
        setIcon$default(imageView, user, i, function1, changeDetector, null, 32, null);
    }

    public static final void setIcon(ImageView imageView, String str) {
        setIcon$default(imageView, str, 0, (Function1) null, (MGImages.ChangeDetector) null, 28, (Object) null);
    }

    public static final void setIcon(ImageView imageView, String str, @DimenRes int i) {
        setIcon$default(imageView, str, i, (Function1) null, (MGImages.ChangeDetector) null, 24, (Object) null);
    }

    public static final void setIcon(ImageView imageView, String str, int i, int i2) {
        setIcon$default(imageView, str, i, i2, false, null, null, 112, null);
    }

    public static final void setIcon(ImageView imageView, String str, int i, int i2, boolean z2) {
        setIcon$default(imageView, str, i, i2, z2, null, null, 96, null);
    }

    public static final void setIcon(ImageView imageView, String str, int i, int i2, boolean z2, Function1<? super ImageRequestBuilder, Unit> function1) {
        setIcon$default(imageView, str, i, i2, z2, function1, null, 64, null);
    }

    public static final void setIcon(ImageView imageView, String str, @DimenRes int i, Function1<? super ImageRequestBuilder, Unit> function1) {
        setIcon$default(imageView, str, i, function1, (MGImages.ChangeDetector) null, 16, (Object) null);
    }

    public static final void setIcon(ImageView imageView, String str, @DimenRes int i, Function1<? super ImageRequestBuilder, Unit> function1, MGImages.ChangeDetector changeDetector) {
        m.checkNotNullParameter(imageView, "imageView");
        m.checkNotNullParameter(changeDetector, "changeDetector");
        setIcon(imageView, str, imageView.getResources().getDimensionPixelSize(i), imageView.getResources().getDimensionPixelSize(i), true, function1, changeDetector);
    }

    public static final void setIcon(ImageView imageView, String str, Long l, String str2, Integer num) {
        setIcon$default(imageView, str, l, str2, num, 0, null, null, null, 480, null);
    }

    public static final void setIcon(ImageView imageView, String str, Long l, String str2, Integer num, @DimenRes int i) {
        setIcon$default(imageView, str, l, str2, num, i, null, null, null, 448, null);
    }

    public static final void setIcon(ImageView imageView, String str, Long l, String str2, Integer num, @DimenRes int i, Function1<? super ImageRequestBuilder, Unit> function1) {
        setIcon$default(imageView, str, l, str2, num, i, function1, null, null, 384, null);
    }

    public static final void setIcon(ImageView imageView, String str, Long l, String str2, Integer num, @DimenRes int i, Function1<? super ImageRequestBuilder, Unit> function1, MGImages.ChangeDetector changeDetector) {
        setIcon$default(imageView, str, l, str2, num, i, function1, changeDetector, null, 256, null);
    }

    public static /* synthetic */ void setIcon$default(ImageView imageView, String str, int i, Function1 function1, MGImages.ChangeDetector changeDetector, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            i = R.dimen.avatar_size_unrestricted;
        }
        if ((i2 & 8) != 0) {
            function1 = null;
        }
        if ((i2 & 16) != 0) {
            changeDetector = MGImages.AlwaysUpdateChangeDetector.INSTANCE;
        }
        setIcon(imageView, str, i, function1, changeDetector);
    }

    private final String withSize(String str, Integer num) {
        if (str == null || num == null || num.intValue() <= 0) {
            return str;
        }
        StringBuilder V = a.V(str, "?size=");
        V.append(getMediaProxySize(num.intValue()));
        return V.toString();
    }

    @SuppressLint({"DefaultLocale"})
    public final String getAssetImage(Long l, String str, int i) {
        m.checkNotNullParameter(str, "imageId");
        int mediaProxySize = getMediaProxySize(i);
        if (w.contains$default((CharSequence) str, (char) MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR, false, 2, (Object) null)) {
            List split$default = w.split$default((CharSequence) str, new char[]{MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR}, false, 2, 2, (Object) null);
            String str2 = (String) split$default.get(0);
            Objects.requireNonNull(str2, "null cannot be cast to non-null type java.lang.String");
            String lowerCase = str2.toLowerCase();
            m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase()");
            return getAssetPlatformUrl(lowerCase, (String) split$default.get(1));
        } else if (l == null) {
            return null;
        } else {
            return "https://cdn.discordapp.com/app-assets/" + l + MentionUtilsKt.SLASH_CHAR + str + ".jpg?size=" + mediaProxySize;
        }
    }

    public final String getBannerForGuild(Guild guild, Integer num, boolean z2) {
        String str = null;
        Long valueOf = guild != null ? Long.valueOf(guild.getId()) : null;
        if (guild != null) {
            str = guild.getBanner();
        }
        return getBannerForGuild(valueOf, str, num, z2);
    }

    public final String getDefaultForGroupDM(long j) {
        long j2 = (j >>> 22) + SnowflakeUtils.DISCORD_EPOCH;
        List<String> list = GROUP_DM_DEFAULT_ICONS;
        return list.get((int) (j2 % list.size()));
    }

    public final String getForGuildMember(GuildMember guildMember, Integer num, boolean z2) {
        m.checkNotNullParameter(guildMember, "guildMember");
        return getForGuildMember(guildMember.getAvatarHash(), guildMember.getGuildId(), guildMember.getUserId(), num, z2);
    }

    public final String getForGuildMemberBanner(String str, long j, long j2, Integer num, boolean z2) {
        if ((str == null || t.isBlank(str)) || j == 0 || j2 == 0) {
            return null;
        }
        String imageExtension = getImageExtension(str, z2);
        return withSize("https://cdn.discordapp.com/guilds/" + j + "/users/" + j2 + "/banners/" + str + ClassUtils.PACKAGE_SEPARATOR_CHAR + imageExtension, num);
    }

    public final String getForGuildMemberOrUser(User user, GuildMember guildMember, Integer num, boolean z2) {
        if (guildMember == null || !guildMember.hasAvatar()) {
            return getForUser(user, z2, num);
        }
        return getForGuildMember(guildMember, num, z2);
    }

    public final String getForUserBanner(long j, String str, Integer num, boolean z2) {
        if (str == null || t.isBlank(str)) {
            return null;
        }
        String imageExtension = getImageExtension(str, z2);
        return withSize("https://cdn.discordapp.com/banners/" + j + MentionUtilsKt.SLASH_CHAR + str + ClassUtils.PACKAGE_SEPARATOR_CHAR + imageExtension, num);
    }

    /* JADX WARN: Code restructure failed: missing block: B:22:0x005d, code lost:
        if (r0 != null) goto L24;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.lang.String getGiftSplashUrl(long r8, java.lang.String r10, java.lang.Integer r11) {
        /*
            r7 = this;
            r0 = 0
            if (r10 != 0) goto L4
            return r0
        L4:
            if (r11 == 0) goto L3c
            r11.intValue()
            java.lang.Integer[] r1 = com.discord.utilities.icon.IconUtils.MEDIA_PROXY_SIZES
            int r2 = r1.length
            r3 = 0
        Ld:
            if (r3 >= r2) goto L25
            r4 = r1[r3]
            int r5 = r4.intValue()
            int r6 = r11.intValue()
            if (r5 <= r6) goto L1d
            r5 = 1
            goto L1e
        L1d:
            r5 = 0
        L1e:
            if (r5 == 0) goto L22
            r0 = r4
            goto L25
        L22:
            int r3 = r3 + 1
            goto Ld
        L25:
            if (r0 == 0) goto L2c
            int r11 = r0.intValue()
            goto L38
        L2c:
            java.lang.Integer[] r11 = com.discord.utilities.icon.IconUtils.MEDIA_PROXY_SIZES
            java.lang.Object r11 = d0.t.k.last(r11)
            java.lang.Number r11 = (java.lang.Number) r11
            int r11 = r11.intValue()
        L38:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r11)
        L3c:
            java.lang.String r11 = "?"
            java.lang.StringBuilder r11 = b.d.b.a.a.R(r11)
            if (r0 == 0) goto L60
            r0.intValue()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "size="
            r1.append(r2)
            r1.append(r0)
            java.lang.String r0 = "&keep_aspect_ratio=true"
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            if (r0 == 0) goto L60
            goto L62
        L60:
            java.lang.String r0 = "keep_aspect_ratio=true"
        L62:
            r11.append(r0)
            java.lang.String r11 = r11.toString()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "https://cdn.discordapp.com/app-icons/"
            r0.append(r1)
            r0.append(r8)
            r8 = 47
            r0.append(r8)
            r0.append(r10)
            r8 = 46
            r0.append(r8)
            java.lang.String r8 = com.discord.utilities.string.StringUtilsKt.getSTATIC_IMAGE_EXTENSION()
            r0.append(r8)
            r0.append(r11)
            java.lang.String r8 = r0.toString()
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.icon.IconUtils.getGiftSplashUrl(long, java.lang.String, java.lang.Integer):java.lang.String");
    }

    public final String getGuildSplashUrl(long j, String str, Integer num) {
        String str2;
        m.checkNotNullParameter(str, "splashHash");
        String str3 = null;
        Integer valueOf = num != null ? Integer.valueOf(getMediaProxySize(num.intValue())) : null;
        if (BuildConfig.HOST_CDN.length() == 0) {
            str2 = "https://discord.com/api//guilds/" + j + "/splashes/" + str + ".jpg";
        } else {
            str2 = "https://cdn.discordapp.com/splashes/" + j + MentionUtilsKt.SLASH_CHAR + str + ClassUtils.PACKAGE_SEPARATOR_CHAR + StringUtilsKt.getSTATIC_IMAGE_EXTENSION();
        }
        StringBuilder R = a.R(str2);
        if (valueOf != null) {
            str3 = a.p("?size=", valueOf.intValue());
        }
        if (str3 == null) {
            str3 = "";
        }
        R.append(str3);
        return R.toString();
    }

    public final String getImageExtension(String str, boolean z2) {
        m.checkNotNullParameter(str, "imageHash");
        return (!z2 || !isImageHashAnimated(str)) ? StringUtilsKt.getSTATIC_IMAGE_EXTENSION() : ANIMATED_IMAGE_EXTENSION;
    }

    public final String getRoleIconUrl(long j, String str, Integer num) {
        String str2;
        m.checkNotNullParameter(str, "iconHash");
        String str3 = null;
        Integer valueOf = num != null ? Integer.valueOf(getMediaProxySize(num.intValue())) : null;
        if (BuildConfig.HOST_CDN.length() == 0) {
            str2 = "https://discord.com/api//roles/" + j + "/icons/" + str + ".png";
        } else {
            str2 = "https://cdn.discordapp.com/role-icons/" + j + MentionUtilsKt.SLASH_CHAR + str + ClassUtils.PACKAGE_SEPARATOR_CHAR + StringUtilsKt.getSTATIC_IMAGE_EXTENSION();
        }
        StringBuilder R = a.R(str2);
        if (valueOf != null) {
            str3 = a.p("?size=", valueOf.intValue());
        }
        if (str3 == null) {
            str3 = "";
        }
        R.append(str3);
        return R.toString();
    }

    @SuppressLint({"DefaultLocale"})
    public final String getStoreAssetImage(Long l, String str, int i) {
        m.checkNotNullParameter(str, "imageId");
        int mediaProxySize = getMediaProxySize(i);
        if (l != null && (!t.isBlank(BuildConfig.HOST_CDN))) {
            return "https://cdn.discordapp.com/app-assets/" + l + "/store/" + str + ".jpg?size=" + mediaProxySize;
        } else if (l == null) {
            return null;
        } else {
            return "https://discord.com/api/store/applications/" + l + "/assets/" + str + ".jpg?size=" + mediaProxySize;
        }
    }

    public final boolean isDataUrlForGif(String str) {
        m.checkNotNullParameter(str, "dataUrl");
        return t.startsWith$default(str, "data:image/gif", false, 2, null);
    }

    public final boolean isImageHashAnimated(String str) {
        m.checkNotNullParameter(str, "imageHash");
        return t.startsWith$default(str, "a_", false, 2, null);
    }

    public static /* synthetic */ String getApplicationIcon$default(IconUtils iconUtils, Application application, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 160;
        }
        return iconUtils.getApplicationIcon(application, i);
    }

    public static /* synthetic */ String getBannerForGuild$default(IconUtils iconUtils, Long l, String str, Integer num, boolean z2, int i, Object obj) {
        if ((i & 4) != 0) {
            num = null;
        }
        if ((i & 8) != 0) {
            z2 = false;
        }
        return iconUtils.getBannerForGuild(l, str, num, z2);
    }

    public static /* synthetic */ String getForChannel$default(Channel channel, Integer num, int i, Object obj) {
        if ((i & 2) != 0) {
            num = null;
        }
        return getForChannel(channel, num);
    }

    public static /* synthetic */ String getForGuild$default(Guild guild, String str, boolean z2, Integer num, int i, Object obj) {
        if ((i & 2) != 0) {
            str = null;
        }
        if ((i & 4) != 0) {
            z2 = false;
        }
        if ((i & 8) != 0) {
            num = null;
        }
        return getForGuild(guild, str, z2, num);
    }

    public static /* synthetic */ String getForUser$default(Long l, String str, Integer num, boolean z2, Integer num2, int i, Object obj) {
        if ((i & 4) != 0) {
            num = null;
        }
        if ((i & 8) != 0) {
            z2 = false;
        }
        if ((i & 16) != 0) {
            num2 = null;
        }
        return getForUser(l, str, num, z2, num2);
    }

    public static /* synthetic */ String getGuildSplashUrl$default(IconUtils iconUtils, Guild guild, Integer num, int i, Object obj) {
        if ((i & 2) != 0) {
            num = null;
        }
        return iconUtils.getGuildSplashUrl(guild, num);
    }

    public static /* synthetic */ void setIcon$default(ImageView imageView, String str, int i, int i2, boolean z2, Function1 function1, MGImages.ChangeDetector changeDetector, int i3, Object obj) {
        boolean z3 = (i3 & 16) != 0 ? true : z2;
        if ((i3 & 32) != 0) {
            function1 = null;
        }
        Function1 function12 = function1;
        if ((i3 & 64) != 0) {
            changeDetector = MGImages.AlwaysUpdateChangeDetector.INSTANCE;
        }
        setIcon(imageView, str, i, i2, z3, function12, changeDetector);
    }

    public final String getBannerForGuild(Long l, String str, Integer num, boolean z2) {
        String str2;
        String str3 = null;
        if (l == null || str == null) {
            return null;
        }
        Integer valueOf = num != null ? Integer.valueOf(getMediaProxySize(num.intValue())) : null;
        StringBuilder sb = new StringBuilder();
        if (BuildConfig.HOST_CDN.length() == 0) {
            str2 = "https://discord.com/api//guilds/" + l + "/banners/" + str + ".jpg";
        } else {
            str2 = "https://cdn.discordapp.com/banners/" + l + MentionUtilsKt.SLASH_CHAR + str + ClassUtils.PACKAGE_SEPARATOR_CHAR + getImageExtension(str, z2);
        }
        sb.append(str2);
        if (valueOf != null) {
            str3 = a.p("?size=", valueOf.intValue());
        }
        if (str3 == null) {
            str3 = "";
        }
        sb.append(str3);
        return sb.toString();
    }

    public static /* synthetic */ void setIcon$default(ImageView imageView, User user, int i, Function1 function1, MGImages.ChangeDetector changeDetector, GuildMember guildMember, int i2, Object obj) {
        int i3 = (i2 & 4) != 0 ? R.dimen.avatar_size_unrestricted : i;
        Function1 function12 = (i2 & 8) != 0 ? null : function1;
        if ((i2 & 16) != 0) {
            changeDetector = MGImages.AlwaysUpdateChangeDetector.INSTANCE;
        }
        setIcon(imageView, user, i3, function12, changeDetector, (i2 & 32) != 0 ? null : guildMember);
    }

    public final String getApplicationIcon(Application application, int i) {
        m.checkNotNullParameter(application, "application");
        com.discord.api.user.User bot = application.getBot();
        String icon = application.getIcon();
        String str = null;
        if (icon == null || t.isBlank(icon)) {
            icon = null;
        }
        CoreUser coreUser = bot != null ? new CoreUser(bot) : null;
        boolean z2 = coreUser != null && (bot.a() instanceof NullSerializable.b);
        int discriminator = (coreUser != null ? coreUser.getDiscriminator() : 0) % 5;
        if (z2) {
            str = getForUser$default(coreUser, false, null, 6, null);
        } else if (icon != null) {
            str = getApplicationIcon(application.getId(), icon, i);
        }
        return str != null ? str : a.q("asset://asset/images/default_avatar_", discriminator, ".png");
    }

    public static final void setIcon(ImageView imageView, String str, int i, int i2, boolean z2, Function1<? super ImageRequestBuilder, Unit> function1, MGImages.ChangeDetector changeDetector) {
        m.checkNotNullParameter(imageView, "imageView");
        m.checkNotNullParameter(changeDetector, "changeDetector");
        MGImages.setImage(imageView, str, i, i2, z2, function1, changeDetector);
    }

    public static final String getForUser(Long l, String str, Integer num, boolean z2, Integer num2) {
        int i = 0;
        if (l != null) {
            if (-1 == l.longValue()) {
                return str;
            }
            if (str != null) {
                if (BuildConfig.HOST_CDN.length() == 0) {
                    i = 1;
                }
                if (i != 0) {
                    return "https://discord.com/api//users/" + l + "/avatars/" + str + ".jpg";
                }
                IconUtils iconUtils = INSTANCE;
                String imageExtension = iconUtils.getImageExtension(str, z2);
                return iconUtils.withSize("https://cdn.discordapp.com/avatars/" + l + MentionUtilsKt.SLASH_CHAR + str + ClassUtils.PACKAGE_SEPARATOR_CHAR + imageExtension, num2);
            } else if (num != null) {
                i = num.intValue() % 5;
            }
        }
        return a.q("asset://asset/images/default_avatar_", i, ".png");
    }

    public static final void setIcon(ImageView imageView, User user, @DimenRes int i, Function1<? super ImageRequestBuilder, Unit> function1, MGImages.ChangeDetector changeDetector, GuildMember guildMember) {
        m.checkNotNullParameter(imageView, "imageView");
        m.checkNotNullParameter(changeDetector, "changeDetector");
        Integer num = null;
        String username = user != null ? user.getUsername() : null;
        Long valueOf = user != null ? Long.valueOf(user.getId()) : null;
        String avatar = user != null ? user.getAvatar() : null;
        if (user != null) {
            num = Integer.valueOf(user.getDiscriminator());
        }
        setIcon(imageView, username, valueOf, avatar, num, i, function1, changeDetector, guildMember);
    }

    public static /* synthetic */ void setIcon$default(ImageView imageView, String str, Long l, String str2, Integer num, int i, Function1 function1, MGImages.ChangeDetector changeDetector, GuildMember guildMember, int i2, Object obj) {
        setIcon(imageView, str, l, str2, num, (i2 & 32) != 0 ? R.dimen.avatar_size_unrestricted : i, (i2 & 64) != 0 ? null : function1, (i2 & 128) != 0 ? MGImages.AlwaysUpdateChangeDetector.INSTANCE : changeDetector, (i2 & 256) != 0 ? null : guildMember);
    }

    public final String getForGuildMember(String str, long j, long j2, Integer num, boolean z2) {
        if ((str == null || t.isBlank(str)) || j == 0 || j2 == 0) {
            return null;
        }
        String imageExtension = getImageExtension(str, z2);
        return withSize("https://cdn.discordapp.com/guilds/" + j + "/users/" + j2 + "/avatars/" + str + ClassUtils.PACKAGE_SEPARATOR_CHAR + imageExtension, num);
    }

    public static final String getForChannel(Channel channel, Integer num) {
        if (channel == null) {
            return null;
        }
        if (channel.A() == 1) {
            return getForUser$default(ChannelUtils.a(channel), false, null, 6, null);
        }
        return getForChannel(channel.h(), channel.g(), channel.A(), false, num);
    }

    public final String getGuildSplashUrl(Guild guild, Integer num) {
        if ((guild != null ? guild.getSplash() : null) == null) {
            return null;
        }
        return getGuildSplashUrl(guild.getId(), guild.getSplash(), num);
    }

    public static /* synthetic */ void setIcon$default(ImageView imageView, Channel channel, int i, MGImages.ChangeDetector changeDetector, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            i = R.dimen.avatar_size_unrestricted;
        }
        if ((i2 & 8) != 0) {
            changeDetector = MGImages.AlwaysUpdateChangeDetector.INSTANCE;
        }
        setIcon(imageView, channel, i, changeDetector);
    }

    public static /* synthetic */ void setIcon$default(ImageView imageView, Guild guild, int i, MGImages.ChangeDetector changeDetector, boolean z2, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            i = R.dimen.avatar_size_unrestricted;
        }
        if ((i2 & 8) != 0) {
            changeDetector = MGImages.AlwaysUpdateChangeDetector.INSTANCE;
        }
        if ((i2 & 16) != 0) {
            z2 = false;
        }
        setIcon(imageView, guild, i, changeDetector, z2);
    }

    public static final String getForGuild(Guild guild, String str, boolean z2, Integer num) {
        String str2 = null;
        Long valueOf = guild != null ? Long.valueOf(guild.getId()) : null;
        if (guild != null) {
            str2 = guild.getIcon();
        }
        return getForGuild(valueOf, str2, str, z2, num);
    }

    public static final void setIcon(ImageView imageView, String str, Long l, String str2, Integer num, @DimenRes int i, Function1<? super ImageRequestBuilder, Unit> function1, MGImages.ChangeDetector changeDetector, GuildMember guildMember) {
        String str3;
        m.checkNotNullParameter(imageView, "imageView");
        m.checkNotNullParameter(changeDetector, "changeDetector");
        Integer valueOf = i != R.dimen.avatar_size_unrestricted ? Integer.valueOf(imageView.getResources().getDimensionPixelSize(i)) : null;
        if (guildMember == null || !guildMember.hasAvatar()) {
            str3 = getForUser$default(l, str2, num, false, valueOf, 8, null);
        } else {
            str3 = getForGuildMember$default(INSTANCE, guildMember, valueOf, false, 4, null);
        }
        setIcon$default(imageView, str3, valueOf != null ? valueOf.intValue() : 0, valueOf != null ? valueOf.intValue() : 0, false, function1, changeDetector, 16, null);
        imageView.setContentDescription(str);
    }

    public static /* synthetic */ void setIcon$default(ImageView imageView, GuildRole guildRole, int i, MGImages.ChangeDetector changeDetector, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            i = R.dimen.avatar_size_unrestricted;
        }
        if ((i2 & 8) != 0) {
            changeDetector = MGImages.AlwaysUpdateChangeDetector.INSTANCE;
        }
        setIcon(imageView, guildRole, i, changeDetector);
    }

    public static final void setIcon(ImageView imageView, Channel channel, @DimenRes int i, MGImages.ChangeDetector changeDetector) {
        m.checkNotNullParameter(imageView, "imageView");
        m.checkNotNullParameter(changeDetector, "changeDetector");
        String str = null;
        setIcon$default(imageView, getForChannel$default(channel, null, 2, null), i, (Function1) null, changeDetector, 8, (Object) null);
        if (channel != null) {
            str = ChannelUtils.c(channel);
        }
        imageView.setContentDescription(str);
    }

    public static final void setIcon(ImageView imageView, Guild guild, @DimenRes int i, MGImages.ChangeDetector changeDetector, boolean z2) {
        m.checkNotNullParameter(imageView, "imageView");
        m.checkNotNullParameter(changeDetector, "changeDetector");
        setIcon$default(imageView, getForGuild$default(guild, null, z2, null, 10, null), i, (Function1) null, changeDetector, 8, (Object) null);
        imageView.setContentDescription(guild != null ? guild.getName() : null);
    }

    public static final void setIcon(ImageView imageView, GuildRole guildRole, @DimenRes int i, MGImages.ChangeDetector changeDetector) {
        String d;
        m.checkNotNullParameter(imageView, "imageView");
        m.checkNotNullParameter(changeDetector, "changeDetector");
        if (guildRole != null && (d = guildRole.d()) != null) {
            setIcon$default(imageView, getRoleIconUrl$default(INSTANCE, guildRole.getId(), d, null, 4, null), i, (Function1) null, changeDetector, 8, (Object) null);
        }
    }
}
