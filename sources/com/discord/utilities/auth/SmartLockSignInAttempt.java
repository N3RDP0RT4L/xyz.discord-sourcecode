package com.discord.utilities.auth;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: GoogleSmartLockRepo.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B\u001b\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0002\u0012\b\b\u0002\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J$\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u00022\b\b\u0002\u0010\u0007\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0011\u001a\u00020\u00022\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0006\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0013\u001a\u0004\b\u0015\u0010\u0004¨\u0006\u0018"}, d2 = {"Lcom/discord/utilities/auth/SmartLockSignInAttempt;", "", "", "component1", "()Z", "component2", "usedAutomaticCredentials", "signedInWithoutError", "copy", "(ZZ)Lcom/discord/utilities/auth/SmartLockSignInAttempt;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getUsedAutomaticCredentials", "getSignedInWithoutError", HookHelper.constructorName, "(ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class SmartLockSignInAttempt {
    private final boolean signedInWithoutError;
    private final boolean usedAutomaticCredentials;

    public SmartLockSignInAttempt() {
        this(false, false, 3, null);
    }

    public SmartLockSignInAttempt(boolean z2, boolean z3) {
        this.usedAutomaticCredentials = z2;
        this.signedInWithoutError = z3;
    }

    public static /* synthetic */ SmartLockSignInAttempt copy$default(SmartLockSignInAttempt smartLockSignInAttempt, boolean z2, boolean z3, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = smartLockSignInAttempt.usedAutomaticCredentials;
        }
        if ((i & 2) != 0) {
            z3 = smartLockSignInAttempt.signedInWithoutError;
        }
        return smartLockSignInAttempt.copy(z2, z3);
    }

    public final boolean component1() {
        return this.usedAutomaticCredentials;
    }

    public final boolean component2() {
        return this.signedInWithoutError;
    }

    public final SmartLockSignInAttempt copy(boolean z2, boolean z3) {
        return new SmartLockSignInAttempt(z2, z3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SmartLockSignInAttempt)) {
            return false;
        }
        SmartLockSignInAttempt smartLockSignInAttempt = (SmartLockSignInAttempt) obj;
        return this.usedAutomaticCredentials == smartLockSignInAttempt.usedAutomaticCredentials && this.signedInWithoutError == smartLockSignInAttempt.signedInWithoutError;
    }

    public final boolean getSignedInWithoutError() {
        return this.signedInWithoutError;
    }

    public final boolean getUsedAutomaticCredentials() {
        return this.usedAutomaticCredentials;
    }

    public int hashCode() {
        boolean z2 = this.usedAutomaticCredentials;
        int i = 1;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        int i4 = i2 * 31;
        boolean z3 = this.signedInWithoutError;
        if (!z3) {
            i = z3 ? 1 : 0;
        }
        return i4 + i;
    }

    public String toString() {
        StringBuilder R = a.R("SmartLockSignInAttempt(usedAutomaticCredentials=");
        R.append(this.usedAutomaticCredentials);
        R.append(", signedInWithoutError=");
        return a.M(R, this.signedInWithoutError, ")");
    }

    public /* synthetic */ SmartLockSignInAttempt(boolean z2, boolean z3, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? false : z2, (i & 2) != 0 ? true : z3);
    }
}
