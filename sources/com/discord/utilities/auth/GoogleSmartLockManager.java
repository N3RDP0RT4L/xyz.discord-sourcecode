package com.discord.utilities.auth;

import andhook.lib.HookHelper;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import androidx.fragment.app.Fragment;
import b.d.b.a.a;
import b.i.a.f.c.a.d.b;
import b.i.a.f.c.a.d.d;
import b.i.a.f.e.k.k;
import b.i.a.f.e.k.s;
import b.i.a.f.e.k.v;
import b.i.a.f.h.c.g;
import b.i.a.f.h.c.h;
import b.i.a.f.h.c.l;
import com.discord.app.AppFragment;
import com.discord.app.AppLog;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.stores.StoreNotices;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.settings.account.WidgetSettingsAccount;
import com.discord.widgets.tabs.WidgetTabsHost;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.CredentialRequest;
import com.google.android.gms.auth.api.credentials.CredentialsClient;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import d0.e0.c;
import d0.t.n;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.objectweb.asm.Opcodes;
import rx.Observable;
/* compiled from: GoogleSmartLockManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 )2\u00020\u0001:\u0002)*B\u0019\u0012\u0006\u0010&\u001a\u00020%\u0012\b\b\u0002\u0010\u001d\u001a\u00020\u001c¢\u0006\u0004\b'\u0010(J'\u0010\t\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\t\u001a\u00020\b2\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\t\u0010\rJ\u001d\u0010\u000f\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u000e2\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\u0013\u001a\u00020\b2\b\u0010\u0011\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\u0012\u001a\u00020\u000b¢\u0006\u0004\b\u0013\u0010\u0014J\u001d\u0010\u0016\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\u0015\u001a\u00020\u000b¢\u0006\u0004\b\u0016\u0010\u0014J\u0015\u0010\u0017\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\u0017\u0010\u0018R\u0016\u0010\u001a\u001a\u00020\u00198\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001a\u0010\u001bR\u0019\u0010\u001d\u001a\u00020\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 R$\u0010#\u001a\u0010\u0012\f\u0012\n\u0012\u0006\b\u0001\u0012\u00020\u000e0\"0!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$¨\u0006+"}, d2 = {"Lcom/discord/utilities/auth/GoogleSmartLockManager;", "", "Landroidx/fragment/app/Fragment;", "fragment", "", "requestCode", "Lcom/google/android/gms/common/api/ResolvableApiException;", "rae", "", "resolveResult", "(Landroidx/fragment/app/Fragment;ILcom/google/android/gms/common/api/ResolvableApiException;)V", "", ModelAuditLogEntry.CHANGE_KEY_ID, "(Lcom/google/android/gms/common/api/ResolvableApiException;Ljava/lang/String;)V", "Lcom/discord/app/AppFragment;", "requestCredentials", "(Lcom/discord/app/AppFragment;I)V", "login", "newPassword", "updateAccountInfo", "(Ljava/lang/String;Ljava/lang/String;)V", "password", "saveCredentials", "deleteCredentials", "(Ljava/lang/String;)V", "Lcom/google/android/gms/auth/api/credentials/CredentialsClient;", "credentialsClient", "Lcom/google/android/gms/auth/api/credentials/CredentialsClient;", "Lcom/discord/utilities/auth/GoogleSmartLockRepo;", "smartLockRepo", "Lcom/discord/utilities/auth/GoogleSmartLockRepo;", "getSmartLockRepo", "()Lcom/discord/utilities/auth/GoogleSmartLockRepo;", "", "Ld0/e0/c;", "resolvableFragments", "Ljava/util/List;", "Landroid/content/Context;", "context", HookHelper.constructorName, "(Landroid/content/Context;Lcom/discord/utilities/auth/GoogleSmartLockRepo;)V", "Companion", "SmartLockCredentials", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GoogleSmartLockManager {
    public static final Companion Companion = new Companion(null);
    public static final String DISCORD_ACCOUNT_IDENTITY = "https://discord.com/";
    public static final boolean ENABLE_SMART_LOCK = false;
    public static final int GOOGLE_SMART_LOCK_REQUEST_CODE_RESOLVE = 4008;
    public static final int GOOGLE_SMART_LOCK_REQUEST_CODE_RESOLVE_FOR_REQUEST = 4009;
    public static final boolean SET_DISCORD_ACCOUNT_DETAILS = true;
    public static final String SMART_LOCK_NOTICE_NAME = "smartlock_resolution_";
    private CredentialsClient credentialsClient;
    private final List<c<? extends AppFragment>> resolvableFragments;
    private final GoogleSmartLockRepo smartLockRepo;

    /* compiled from: GoogleSmartLockManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0016\u0010\u000f\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0011\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0011\u0010\u0010R\u0016\u0010\u0012\u001a\u00020\f8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0012\u0010\u000eR\u0016\u0010\u0013\u001a\u00020\t8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0013\u0010\u000b¨\u0006\u0016"}, d2 = {"Lcom/discord/utilities/auth/GoogleSmartLockManager$Companion;", "", "", "resultCode", "Landroid/content/Intent;", "data", "", "handleResult", "(ILandroid/content/Intent;)V", "", "DISCORD_ACCOUNT_IDENTITY", "Ljava/lang/String;", "", "ENABLE_SMART_LOCK", "Z", "GOOGLE_SMART_LOCK_REQUEST_CODE_RESOLVE", "I", "GOOGLE_SMART_LOCK_REQUEST_CODE_RESOLVE_FOR_REQUEST", "SET_DISCORD_ACCOUNT_DETAILS", "SMART_LOCK_NOTICE_NAME", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void handleResult(int i, Intent intent) {
            if (i == -1 && intent != null) {
                Credential credential = (Credential) intent.getParcelableExtra("com.google.android.gms.credentials.Credential");
                String str = null;
                String str2 = credential != null ? credential.j : null;
                if (credential != null) {
                    str = credential.n;
                }
                if (str2 != null && str != null) {
                    GoogleSmartLockRepo.Companion.getINSTANCE().setSmartLockLogin(str2, str);
                }
            }
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: GoogleSmartLockManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J$\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u00022\b\b\u0002\u0010\u0007\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\n\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\n\u0010\u0004J\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0010\u001a\u00020\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0012\u001a\u0004\b\u0013\u0010\u0004R\u0019\u0010\u0006\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0012\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/utilities/auth/GoogleSmartLockManager$SmartLockCredentials;", "", "", "component1", "()Ljava/lang/String;", "component2", ModelAuditLogEntry.CHANGE_KEY_ID, "password", "copy", "(Ljava/lang/String;Ljava/lang/String;)Lcom/discord/utilities/auth/GoogleSmartLockManager$SmartLockCredentials;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getPassword", "getId", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class SmartLockCredentials {

        /* renamed from: id  reason: collision with root package name */
        private final String f2788id;
        private final String password;

        public SmartLockCredentials(String str, String str2) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_ID);
            m.checkNotNullParameter(str2, "password");
            this.f2788id = str;
            this.password = str2;
        }

        public static /* synthetic */ SmartLockCredentials copy$default(SmartLockCredentials smartLockCredentials, String str, String str2, int i, Object obj) {
            if ((i & 1) != 0) {
                str = smartLockCredentials.f2788id;
            }
            if ((i & 2) != 0) {
                str2 = smartLockCredentials.password;
            }
            return smartLockCredentials.copy(str, str2);
        }

        public final String component1() {
            return this.f2788id;
        }

        public final String component2() {
            return this.password;
        }

        public final SmartLockCredentials copy(String str, String str2) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_ID);
            m.checkNotNullParameter(str2, "password");
            return new SmartLockCredentials(str, str2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof SmartLockCredentials)) {
                return false;
            }
            SmartLockCredentials smartLockCredentials = (SmartLockCredentials) obj;
            return m.areEqual(this.f2788id, smartLockCredentials.f2788id) && m.areEqual(this.password, smartLockCredentials.password);
        }

        public final String getId() {
            return this.f2788id;
        }

        public final String getPassword() {
            return this.password;
        }

        public int hashCode() {
            String str = this.f2788id;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.password;
            if (str2 != null) {
                i = str2.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("SmartLockCredentials(id=");
            R.append(this.f2788id);
            R.append(", password=");
            return a.H(R, this.password, ")");
        }
    }

    public GoogleSmartLockManager(Context context, GoogleSmartLockRepo googleSmartLockRepo) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(googleSmartLockRepo, "smartLockRepo");
        this.smartLockRepo = googleSmartLockRepo;
        this.resolvableFragments = n.listOf((Object[]) new c[]{a0.getOrCreateKotlinClass(WidgetTabsHost.class), a0.getOrCreateKotlinClass(WidgetSettingsAccount.class)});
        d.a aVar = new d.a();
        aVar.f1329b = Boolean.TRUE;
        CredentialsClient credentialsClient = new CredentialsClient(context, new d(aVar, null));
        m.checkNotNullExpressionValue(credentialsClient, "Credentials.getClient(context, options)");
        this.credentialsClient = credentialsClient;
        if (googleSmartLockRepo.getDisableAutoLogin()) {
            CredentialsClient credentialsClient2 = this.credentialsClient;
            Objects.requireNonNull(credentialsClient2);
            b.i.a.f.c.a.d.c cVar = b.i.a.f.c.a.a.g;
            b.i.a.f.e.h.c cVar2 = credentialsClient2.g;
            Objects.requireNonNull((h) cVar);
            b.c.a.a0.d.z(cVar2, "client must not be null");
            k.a(cVar2.b(new b.i.a.f.h.c.k(cVar2)));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void resolveResult(Fragment fragment, int i, ResolvableApiException resolvableApiException) {
        try {
            if ((resolvableApiException.mStatus.p != 4) && !fragment.isDetached() && !fragment.isRemoving()) {
                PendingIntent pendingIntent = resolvableApiException.mStatus.r;
                m.checkNotNullExpressionValue(pendingIntent, "rae.resolution");
                fragment.startIntentSenderForResult(pendingIntent.getIntentSender(), i, null, 0, 0, 0, null);
            }
        } catch (IntentSender.SendIntentException e) {
            AppLog.g.w("Google Smart Lock", "Failed to send resolution.", e);
        } catch (Exception e2) {
            AppLog.g.w("Google Smart Lock", "Failed to resolve", e2);
        }
    }

    public final void deleteCredentials(String str) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_ID);
        CredentialsClient credentialsClient = this.credentialsClient;
        Credential credential = new Credential(str, null, null, null, null, null, null, null);
        Objects.requireNonNull(credentialsClient);
        b.i.a.f.c.a.d.c cVar = b.i.a.f.c.a.a.g;
        b.i.a.f.e.h.c cVar2 = credentialsClient.g;
        Objects.requireNonNull((h) cVar);
        b.c.a.a0.d.z(cVar2, "client must not be null");
        b.c.a.a0.d.z(credential, "credential must not be null");
        k.a(cVar2.b(new l(cVar2, credential)));
    }

    public final GoogleSmartLockRepo getSmartLockRepo() {
        return this.smartLockRepo;
    }

    public final void requestCredentials(final AppFragment appFragment, final int i) {
        m.checkNotNullParameter(appFragment, "fragment");
        CredentialRequest credentialRequest = new CredentialRequest(4, true, new String[]{DISCORD_ACCOUNT_IDENTITY}, null, null, false, null, null, false);
        CredentialsClient credentialsClient = this.credentialsClient;
        Objects.requireNonNull(credentialsClient);
        b.i.a.f.c.a.d.c cVar = b.i.a.f.c.a.a.g;
        b.i.a.f.e.h.c cVar2 = credentialsClient.g;
        Objects.requireNonNull((h) cVar);
        b.c.a.a0.d.z(cVar2, "client must not be null");
        b.c.a.a0.d.z(credentialRequest, "request must not be null");
        b.i.a.f.e.h.j.d a = cVar2.a(new g(cVar2, credentialRequest));
        v vVar = new v(new b.i.a.f.c.a.d.a());
        k.b bVar = k.a;
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        a.c(new s(a, taskCompletionSource, vVar, bVar));
        taskCompletionSource.a.b(new b.i.a.f.n.c<b.i.a.f.c.a.d.a>() { // from class: com.discord.utilities.auth.GoogleSmartLockManager$requestCredentials$1
            @Override // b.i.a.f.n.c
            public final void onComplete(Task<b.i.a.f.c.a.d.a> task) {
                m.checkNotNullExpressionValue(task, "task");
                boolean z2 = true;
                if (task.p()) {
                    b.i.a.f.c.a.d.a l = task.l();
                    m.checkNotNullExpressionValue(l, "task.result");
                    Credential R = ((b) l.a).R();
                    String str = null;
                    String str2 = R != null ? R.j : null;
                    b.i.a.f.c.a.d.a l2 = task.l();
                    m.checkNotNullExpressionValue(l2, "task.result");
                    Credential R2 = ((b) l2.a).R();
                    if (R2 != null) {
                        str = R2.n;
                    }
                    if (str2 != null && str != null) {
                        GoogleSmartLockManager.this.getSmartLockRepo().setAttemptToSignInWithSmartLock(true);
                        GoogleSmartLockManager.this.getSmartLockRepo().setSmartLockLogin(str2, str);
                        return;
                    }
                    return;
                }
                Exception k = task.k();
                if (k instanceof ResolvableApiException) {
                    ResolvableApiException resolvableApiException = (ResolvableApiException) k;
                    if (resolvableApiException.mStatus.p == 4) {
                        z2 = false;
                    }
                    AppLog.g.w("Google Smart Lock", "Resolvable Exception requesting credentials", k);
                    if (z2) {
                        GoogleSmartLockManager.this.resolveResult(appFragment, i, resolvableApiException);
                    }
                } else if (k instanceof ApiException) {
                    AppLog.g.w("Google Smart Lock", "API Exception requesting credentials", k);
                } else {
                    AppLog.g.w("Google Smart Lock", "Exception requesting credentials", k);
                }
            }
        });
    }

    public final void saveCredentials(String str, String str2) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_ID);
        m.checkNotNullParameter(str2, "password");
        Observable Z = StoreUser.observeMe$default(StoreStream.Companion.getUsers(), false, 1, null).Z(1);
        m.checkNotNullExpressionValue(Z, "StoreStream.getUsers().observeMe().take(1)");
        ObservableExtensionsKt.appSubscribe(Z, GoogleSmartLockManager.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new GoogleSmartLockManager$saveCredentials$1(this, str, str2));
    }

    public final void updateAccountInfo(String str, String str2) {
        m.checkNotNullParameter(str2, "newPassword");
        String pendingIdChange = this.smartLockRepo.getPendingIdChange();
        if (pendingIdChange != null) {
            deleteCredentials(pendingIdChange);
            this.smartLockRepo.setPendingIdChange(null);
        }
        if (str == null) {
            str = StoreStream.Companion.getAuthentication().getSavedLogin();
        }
        if (str != null) {
            saveCredentials(str, str2);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void resolveResult(ResolvableApiException resolvableApiException, String str) {
        try {
            if (resolvableApiException.mStatus.p != 4) {
                StoreStream.Companion.getNotices().requestToShow(new StoreNotices.Notice(SMART_LOCK_NOTICE_NAME + str, null, 0L, 0, false, this.resolvableFragments, 0L, false, 0L, new GoogleSmartLockManager$resolveResult$notice$1(this, resolvableApiException), Opcodes.I2F, null));
            }
        } catch (IntentSender.SendIntentException e) {
            AppLog.g.w("Google Smart Lock", "Failed to send resolution.", e);
        } catch (Exception e2) {
            AppLog.g.w("Google Smart Lock", "Failed to resolve", e2);
        }
    }

    public /* synthetic */ GoogleSmartLockManager(Context context, GoogleSmartLockRepo googleSmartLockRepo, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? GoogleSmartLockRepo.Companion.getINSTANCE() : googleSmartLockRepo);
    }
}
