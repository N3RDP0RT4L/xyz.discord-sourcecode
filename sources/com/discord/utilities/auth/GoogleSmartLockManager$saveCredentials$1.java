package com.discord.utilities.auth;

import android.net.Uri;
import b.c.a.a0.d;
import b.i.a.f.c.a.a;
import b.i.a.f.c.a.d.c;
import b.i.a.f.e.k.k;
import b.i.a.f.h.c.h;
import b.i.a.f.h.c.i;
import com.discord.app.AppLog;
import com.discord.models.user.MeUser;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.user.UserUtils;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.CredentialsClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.tasks.Task;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: GoogleSmartLockManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/models/user/MeUser;", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Lcom/discord/models/user/MeUser;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GoogleSmartLockManager$saveCredentials$1 extends o implements Function1<MeUser, Unit> {
    public final /* synthetic */ String $id;
    public final /* synthetic */ String $password;
    public final /* synthetic */ GoogleSmartLockManager this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GoogleSmartLockManager$saveCredentials$1(GoogleSmartLockManager googleSmartLockManager, String str, String str2) {
        super(1);
        this.this$0 = googleSmartLockManager;
        this.$id = str;
        this.$password = str2;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(MeUser meUser) {
        invoke2(meUser);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(MeUser meUser) {
        CredentialsClient credentialsClient;
        String forUser$default = IconUtils.getForUser$default(meUser, false, null, 4, null);
        Credential.a aVar = new Credential.a(this.$id);
        aVar.d = this.$password;
        m.checkNotNullExpressionValue(aVar, "Credential.Builder(id)\n …   .setPassword(password)");
        UserUtils userUtils = UserUtils.INSTANCE;
        m.checkNotNullExpressionValue(meUser, "it");
        aVar.f2967b = UserUtils.getUserNameWithDiscriminator$default(userUtils, meUser, null, null, 3, null).toString();
        aVar.c = Uri.parse(forUser$default);
        Credential a = aVar.a();
        credentialsClient = this.this$0.credentialsClient;
        Objects.requireNonNull(credentialsClient);
        c cVar = a.g;
        b.i.a.f.e.h.c cVar2 = credentialsClient.g;
        Objects.requireNonNull((h) cVar);
        d.z(cVar2, "client must not be null");
        d.z(a, "credential must not be null");
        k.a(cVar2.b(new i(cVar2, a))).b(new b.i.a.f.n.c<Void>() { // from class: com.discord.utilities.auth.GoogleSmartLockManager$saveCredentials$1.1
            @Override // b.i.a.f.n.c
            public final void onComplete(Task<Void> task) {
                m.checkNotNullExpressionValue(task, "task");
                if (task.p()) {
                    Logger.i$default(AppLog.g, "Google Smart Lock", "Credentials Saved", null, 4, null);
                    return;
                }
                Exception k = task.k();
                if (k instanceof ResolvableApiException) {
                    AppLog.g.w("Google Smart Lock", "Resolving Exception saving credentials", k);
                    GoogleSmartLockManager$saveCredentials$1 googleSmartLockManager$saveCredentials$1 = GoogleSmartLockManager$saveCredentials$1.this;
                    googleSmartLockManager$saveCredentials$1.this$0.resolveResult((ResolvableApiException) k, googleSmartLockManager$saveCredentials$1.$id);
                    return;
                }
                AppLog.g.w("Google Smart Lock", "Exception saving credentials", k);
            }
        });
    }
}
