package com.discord.utilities.auth;

import andhook.lib.HookHelper;
import android.annotation.SuppressLint;
import android.util.Patterns;
import androidx.annotation.StringRes;
import androidx.core.app.NotificationCompat;
import b.a.k.b;
import com.discord.utilities.string.StringUtilsKt;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.validators.BasicTextInputValidator;
import com.discord.utilities.view.validators.Input;
import com.discord.utilities.view.validators.InputValidator;
import com.discord.utilities.view.validators.ValidationManager;
import com.google.android.material.textfield.TextInputLayout;
import d0.c0.d;
import d0.g0.s;
import d0.g0.t;
import d0.g0.w;
import d0.t.n;
import d0.z.d.m;
import java.net.URLEncoder;
import java.util.Objects;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: AuthUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000e\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b(\u0010)J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0015\u0010\b\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\b\u0010\u0006J\u000f\u0010\t\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\f\u001a\u00020\u00022\u0006\u0010\u000b\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\f\u0010\rJ\u0017\u0010\u0011\u001a\u00020\u00102\b\b\u0001\u0010\u000f\u001a\u00020\u000e¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0013\u001a\u00020\u00102\b\b\u0001\u0010\u000f\u001a\u00020\u000e¢\u0006\u0004\b\u0013\u0010\u0012J\u0017\u0010\u0014\u001a\u00020\u00102\b\b\u0001\u0010\u000f\u001a\u00020\u000e¢\u0006\u0004\b\u0014\u0010\u0012J'\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00180\u00172\b\b\u0001\u0010\u0015\u001a\u00020\u000e2\b\b\u0001\u0010\u0016\u001a\u00020\u000e¢\u0006\u0004\b\u0019\u0010\u001aJ\u0015\u0010\u001d\u001a\u00020\u001c2\u0006\u0010\u001b\u001a\u00020\u0018¢\u0006\u0004\b\u001d\u0010\u001eR\u0016\u0010\u001f\u001a\u00020\u000e8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u0016\u0010!\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b!\u0010\"R\u0016\u0010#\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b#\u0010\"R\u0016\u0010$\u001a\u00020\u000e8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b$\u0010 R\u0016\u0010%\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b%\u0010\"R\u0016\u0010&\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b&\u0010\"R\u0016\u0010'\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b'\u0010\"¨\u0006*"}, d2 = {"Lcom/discord/utilities/auth/AuthUtils;", "", "", NotificationCompat.CATEGORY_EMAIL, "", "isValidEmail", "(Ljava/lang/String;)Z", "password", "isValidPasswordLength", "generateNewTotpKey", "()Ljava/lang/String;", "secret", "encodeTotpSecret", "(Ljava/lang/String;)Ljava/lang/String;", "", "messageResId", "Lcom/discord/utilities/view/validators/BasicTextInputValidator;", "createPasswordInputValidator", "(I)Lcom/discord/utilities/view/validators/BasicTextInputValidator;", "createEmailInputValidator", "createPhoneInputValidator", "invalidFormatResId", "invalidValueResId", "Lcom/discord/utilities/view/validators/InputValidator;", "Lcom/google/android/material/textfield/TextInputLayout;", "createDiscriminatorInputValidator", "(II)Lcom/discord/utilities/view/validators/InputValidator;", "emailInput", "Lcom/discord/utilities/view/validators/ValidationManager;", "createEmailValidationManager", "(Lcom/google/android/material/textfield/TextInputLayout;)Lcom/discord/utilities/view/validators/ValidationManager;", "MIN_PASSWORD_LENGTH", "I", "URL_PLAY_STORE", "Ljava/lang/String;", "URL_GOOGLE_AUTHENTICATOR", "MAX_PASSWORD_LENGTH", "AUTHY_PACKAGE", "URL_AUTHY", "GOOGLE_AUTHENTICATOR_PACKAGE", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AuthUtils {
    public static final String AUTHY_PACKAGE = "com.authy.authy";
    public static final String GOOGLE_AUTHENTICATOR_PACKAGE = "com.google.android.apps.authenticator2";
    public static final AuthUtils INSTANCE = new AuthUtils();
    private static final int MAX_PASSWORD_LENGTH = 128;
    private static final int MIN_PASSWORD_LENGTH = 6;
    public static final String URL_AUTHY = "https://play.google.com/store/apps/details?id=com.authy.authy";
    public static final String URL_GOOGLE_AUTHENTICATOR = "https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2";
    private static final String URL_PLAY_STORE = "https://play.google.com/store/apps/details";

    private AuthUtils() {
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final boolean isValidEmail(String str) {
        return (str.length() > 0) && Patterns.EMAIL_ADDRESS.matcher(str).matches();
    }

    public final InputValidator<TextInputLayout> createDiscriminatorInputValidator(@StringRes final int i, @StringRes final int i2) {
        return new InputValidator<TextInputLayout>() { // from class: com.discord.utilities.auth.AuthUtils$createDiscriminatorInputValidator$1
            public CharSequence getErrorMessage(TextInputLayout textInputLayout) {
                CharSequence d;
                CharSequence d2;
                m.checkNotNullParameter(textInputLayout, "view");
                String textOrEmpty = ViewExtensions.getTextOrEmpty(textInputLayout);
                Integer intOrNull = s.toIntOrNull(textOrEmpty);
                if (intOrNull == null || textOrEmpty.length() != 4) {
                    d = b.d(textInputLayout, i, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
                    return d;
                } else if (intOrNull.intValue() > 0) {
                    return null;
                } else {
                    d2 = b.d(textInputLayout, i2, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
                    return d2;
                }
            }
        };
    }

    public final BasicTextInputValidator createEmailInputValidator(@StringRes int i) {
        return new BasicTextInputValidator(i, AuthUtils$createEmailInputValidator$1.INSTANCE);
    }

    public final ValidationManager createEmailValidationManager(TextInputLayout textInputLayout) {
        m.checkNotNullParameter(textInputLayout, "emailInput");
        return new ValidationManager(new Input.TextInputLayoutInput(NotificationCompat.CATEGORY_EMAIL, textInputLayout, BasicTextInputValidator.Companion.createRequiredInputValidator(R.string.email_required), createEmailInputValidator(R.string.email_invalid)));
    }

    public final BasicTextInputValidator createPasswordInputValidator(@StringRes int i) {
        return new BasicTextInputValidator(i, AuthUtils$createPasswordInputValidator$1.INSTANCE);
    }

    public final BasicTextInputValidator createPhoneInputValidator(@StringRes int i) {
        return new BasicTextInputValidator(i, AuthUtils$createPhoneInputValidator$1.INSTANCE);
    }

    @SuppressLint({"DefaultLocale"})
    public final String encodeTotpSecret(String str) {
        m.checkNotNullParameter(str, "secret");
        String replace$default = t.replace$default(str, " ", "", false, 4, (Object) null);
        Objects.requireNonNull(replace$default, "null cannot be cast to non-null type java.lang.String");
        String upperCase = replace$default.toUpperCase();
        m.checkNotNullExpressionValue(upperCase, "(this as java.lang.String).toUpperCase()");
        Objects.requireNonNull(upperCase, "null cannot be cast to non-null type kotlin.CharSequence");
        return w.trim(upperCase).toString();
    }

    @SuppressLint({"DefaultLocale"})
    public final String generateNewTotpKey() {
        String encode = URLEncoder.encode(StringUtilsKt.encodeToBase32String(d.Random(ClockFactory.get().currentTimeMillis()).nextBytes(10)), "utf-8");
        m.checkNotNullExpressionValue(encode, "URLEncoder\n        .enco…oBase32String(), \"utf-8\")");
        String replace$default = t.replace$default(encode, "=", "", false, 4, (Object) null);
        Objects.requireNonNull(replace$default, "null cannot be cast to non-null type java.lang.String");
        String lowerCase = replace$default.toLowerCase();
        m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase()");
        Objects.requireNonNull(lowerCase, "null cannot be cast to non-null type kotlin.CharSequence");
        StringBuilder sb = new StringBuilder(w.trim(lowerCase).toString());
        for (Number number : n.listOf((Object[]) new Integer[]{12, 8, 4})) {
            sb.insert(number.intValue(), " ");
        }
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "builder.toString()");
        return sb2;
    }

    public final boolean isValidPasswordLength(String str) {
        m.checkNotNullParameter(str, "password");
        int length = str.length();
        return 6 <= length && 128 >= length;
    }
}
