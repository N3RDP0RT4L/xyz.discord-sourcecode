package com.discord.utilities.uri;

import android.content.Context;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: UriHandler.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class UriHandler$directToPlayStore$2 extends o implements Function0<Unit> {
    public final /* synthetic */ Context $context;
    public final /* synthetic */ UriHandler$directToPlayStore$1 $createFullUriString$1;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public UriHandler$directToPlayStore$2(Context context, UriHandler$directToPlayStore$1 uriHandler$directToPlayStore$1) {
        super(0);
        this.$context = context;
        this.$createFullUriString$1 = uriHandler$directToPlayStore$1;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        UriHandler.handle$default(UriHandler.INSTANCE, this.$context, this.$createFullUriString$1.invoke("https://play.google.com/store/apps/details"), null, 4, null);
    }
}
