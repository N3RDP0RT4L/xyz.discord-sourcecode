package com.discord.utilities.uri;

import andhook.lib.HookHelper;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.browser.customtabs.CustomTabColorSchemeParams;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.app.NotificationCompat;
import b.a.a.g.a;
import com.discord.app.AppTransitionActivity;
import com.discord.databinding.LayoutUnhandledUriBinding;
import com.discord.stores.StoreNotices;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.color.ColorCompat;
import com.discord.widgets.chat.pins.WidgetChannelPinnedMessages;
import com.discord.widgets.home.WidgetHome;
import com.discord.widgets.media.WidgetMedia;
import com.discord.widgets.search.WidgetSearch;
import com.discord.widgets.user.WidgetUserMentions;
import com.google.android.material.button.MaterialButton;
import d0.e0.c;
import d0.t.n;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import org.objectweb.asm.Opcodes;
import xyz.discord.R;
/* compiled from: UriHandler.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000e\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ+\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0004H\u0007¢\u0006\u0004\b\b\u0010\tJ+\u0010\f\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\b\b\u0002\u0010\n\u001a\u00020\u00042\b\b\u0002\u0010\u000b\u001a\u00020\u0004H\u0007¢\u0006\u0004\b\f\u0010\tJ?\u0010\u0011\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u000e\u001a\u0004\u0018\u00010\r2\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0010\b\u0002\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u000fH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u001f\u0010\u0013\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J1\u0010\u0015\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0010\b\u0002\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u000f¢\u0006\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0017\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u0016\u0010\u0019\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0019\u0010\u0018R\u0016\u0010\u001a\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001a\u0010\u0018¨\u0006\u001d"}, d2 = {"Lcom/discord/utilities/uri/UriHandler;", "", "Landroid/content/Context;", "context", "", "url", "mask", "", "handleOrUntrusted", "(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V", "packageName", "source", "directToPlayStore", "Landroid/net/Uri;", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "Lkotlin/Function0;", "onFailure", "openUrlExternally", "(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V", "showUnhandledUrlDialog", "(Landroid/content/Context;Ljava/lang/String;)V", "handle", "(Landroid/content/Context;Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V", "URL_PLAY_STORE_DIRECT", "Ljava/lang/String;", "URL_PLAY_STORE_ALT", "APP_PACKAGE", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class UriHandler {
    private static final String APP_PACKAGE = "com.discord";
    public static final UriHandler INSTANCE = new UriHandler();
    private static final String URL_PLAY_STORE_ALT = "https://play.google.com/store/apps/details";
    private static final String URL_PLAY_STORE_DIRECT = "market://details";

    private UriHandler() {
    }

    public static final void directToPlayStore(Context context) {
        directToPlayStore$default(context, null, null, 6, null);
    }

    public static final void directToPlayStore(Context context, String str) {
        directToPlayStore$default(context, str, null, 4, null);
    }

    public static final void directToPlayStore(Context context, String str, String str2) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(str, "packageName");
        m.checkNotNullParameter(str2, "source");
        UriHandler$directToPlayStore$1 uriHandler$directToPlayStore$1 = new UriHandler$directToPlayStore$1(str, str2);
        INSTANCE.handle(context, uriHandler$directToPlayStore$1.invoke(URL_PLAY_STORE_DIRECT), new UriHandler$directToPlayStore$2(context, uriHandler$directToPlayStore$1));
    }

    public static /* synthetic */ void directToPlayStore$default(Context context, String str, String str2, int i, Object obj) {
        if ((i & 2) != 0) {
            str = "com.discord";
        }
        if ((i & 4) != 0) {
            str2 = "discord";
        }
        directToPlayStore(context, str, str2);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void handle$default(UriHandler uriHandler, Context context, String str, Function0 function0, int i, Object obj) {
        if ((i & 4) != 0) {
            function0 = null;
        }
        uriHandler.handle(context, str, function0);
    }

    public static final void handleOrUntrusted(Context context, String str, String str2) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(str, "url");
        StoreStream.Companion companion = StoreStream.Companion;
        if (companion.getMaskedLinks().isTrustedDomain(str, str2)) {
            handle$default(INSTANCE, context, str, null, 4, null);
            return;
        }
        Objects.requireNonNull(a.k);
        companion.getNotices().requestToShow(new StoreNotices.Notice("WIDGET_SPOOPY_LINKS_DIALOG", null, 0L, 0, false, n.listOf((Object[]) new c[]{a0.getOrCreateKotlinClass(WidgetHome.class), a0.getOrCreateKotlinClass(WidgetUserMentions.class), a0.getOrCreateKotlinClass(WidgetSearch.class), a0.getOrCreateKotlinClass(WidgetChannelPinnedMessages.class), a0.getOrCreateKotlinClass(WidgetMedia.class)}), 0L, false, 0L, new UriHandler$handleOrUntrusted$notice$1(str), Opcodes.I2F, null));
    }

    public static /* synthetic */ void handleOrUntrusted$default(Context context, String str, String str2, int i, Object obj) {
        if ((i & 4) != 0) {
            str2 = null;
        }
        handleOrUntrusted(context, str, str2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void openUrlExternally(Context context, Uri uri, String str, Function0<Unit> function0) {
        try {
            context.startActivity(new Intent("android.intent.action.VIEW", uri));
        } catch (ActivityNotFoundException unused) {
            if ((function0 == null || function0.invoke() == null) && str != null) {
                INSTANCE.showUnhandledUrlDialog(context, str);
            }
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void openUrlExternally$default(UriHandler uriHandler, Context context, Uri uri, String str, Function0 function0, int i, Object obj) {
        if ((i & 4) != 0) {
            str = null;
        }
        if ((i & 8) != 0) {
            function0 = null;
        }
        uriHandler.openUrlExternally(context, uri, str, function0);
    }

    private final void showUnhandledUrlDialog(Context context, final String str) {
        AnalyticsTracker.INSTANCE.unhandledUrl(str);
        View inflate = LayoutInflater.from(context).inflate(R.layout.layout_unhandled_uri, (ViewGroup) null, false);
        int i = R.id.unhandled_uri_display;
        TextView textView = (TextView) inflate.findViewById(R.id.unhandled_uri_display);
        if (textView != null) {
            i = R.id.unhandled_uri_okay;
            MaterialButton materialButton = (MaterialButton) inflate.findViewById(R.id.unhandled_uri_okay);
            if (materialButton != null) {
                LinearLayout linearLayout = (LinearLayout) inflate;
                final LayoutUnhandledUriBinding layoutUnhandledUriBinding = new LayoutUnhandledUriBinding(linearLayout, textView, materialButton);
                m.checkNotNullExpressionValue(layoutUnhandledUriBinding, "LayoutUnhandledUriBindin…utInflater.from(context))");
                final AlertDialog create = new AlertDialog.Builder(context).setView(linearLayout).create();
                m.checkNotNullExpressionValue(textView, "binding.unhandledUriDisplay");
                textView.setText(str);
                textView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.utilities.uri.UriHandler$showUnhandledUrlDialog$$inlined$apply$lambda$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        b.a.d.m.c(b.d.b.a.a.x(view, "v", "v.context"), str, 0, 4);
                    }
                });
                materialButton.setOnClickListener(new View.OnClickListener() { // from class: com.discord.utilities.uri.UriHandler$showUnhandledUrlDialog$1$2
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        AlertDialog.this.dismiss();
                    }
                });
                create.show();
                return;
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    public final void handle(Context context, String str, Function0<Unit> function0) {
        Uri uri;
        m.checkNotNullParameter(context, "context");
        if (str != null) {
            try {
                uri = Uri.parse(str);
            } catch (Exception unused) {
                uri = null;
            }
            if (uri == null) {
                return;
            }
            if (StoreStream.Companion.getUserSettings().getIsChromeCustomTabsEnabled()) {
                AppTransitionActivity.j = true;
                int themedColor = ColorCompat.getThemedColor(context, (int) R.attr.colorPrimary);
                UriHandler$handle$1 uriHandler$handle$1 = new UriHandler$handle$1(context, uri, str, function0);
                m.checkNotNullParameter(context, "context");
                m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
                m.checkNotNullParameter(uriHandler$handle$1, "onFailure");
                if (b.a.h.a.a(context) != null) {
                    CustomTabColorSchemeParams build = new CustomTabColorSchemeParams.Builder().setNavigationBarColor(themedColor).setToolbarColor(themedColor).setSecondaryToolbarColor(themedColor).build();
                    m.checkNotNullExpressionValue(build, "CustomTabColorSchemePara…lor)\n            .build()");
                    CustomTabsIntent build2 = new CustomTabsIntent.Builder().setDefaultColorSchemeParams(build).setShowTitle(false).setStartAnimations(context, R.anim.activity_slide_horizontal_open_in, R.anim.activity_slide_horizontal_open_out).setExitAnimations(context, R.anim.activity_slide_horizontal_close_in, R.anim.activity_slide_horizontal_close_out).build();
                    m.checkNotNullExpressionValue(build2, "CustomTabsIntent.Builder…sId)\n            .build()");
                    try {
                        Intent intent = build2.intent;
                        m.checkNotNullExpressionValue(intent, "customTabsIntent.intent");
                        intent.setData(uri);
                        build2.launchUrl(context, uri);
                    } catch (ActivityNotFoundException unused2) {
                        uriHandler$handle$1.invoke();
                    }
                } else {
                    uriHandler$handle$1.invoke();
                }
            } else {
                openUrlExternally(context, uri, str, function0);
            }
        }
    }
}
