package com.discord.utilities.uri;

import android.net.Uri;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.adjust.sdk.Constants;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: UriHandler.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000e\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "invoke", "(Ljava/lang/String;)Ljava/lang/String;", "createFullUriString"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class UriHandler$directToPlayStore$1 extends o implements Function1<String, String> {
    public final /* synthetic */ String $packageName;
    public final /* synthetic */ String $source;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public UriHandler$directToPlayStore$1(String str, String str2) {
        super(1);
        this.$packageName = str;
        this.$source = str2;
    }

    public final String invoke(String str) {
        m.checkNotNullParameter(str, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        Uri.Builder appendQueryParameter = Uri.parse(str).buildUpon().appendQueryParameter(ModelAuditLogEntry.CHANGE_KEY_ID, this.$packageName);
        StringBuilder R = a.R("utm_source=");
        R.append(this.$source);
        String uri = appendQueryParameter.appendQueryParameter(Constants.REFERRER, R.toString()).build().toString();
        m.checkNotNullExpressionValue(uri, "Uri.parse(uri).buildUpon…ild()\n        .toString()");
        return uri;
    }
}
