package com.discord.utilities.uri;

import android.content.Context;
import android.net.Uri;
import com.discord.app.AppTransitionActivity;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: UriHandler.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class UriHandler$handle$1 extends o implements Function0<Unit> {
    public final /* synthetic */ Context $context;
    public final /* synthetic */ Function0 $onFailure;
    public final /* synthetic */ Uri $uri;
    public final /* synthetic */ String $url;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public UriHandler$handle$1(Context context, Uri uri, String str, Function0 function0) {
        super(0);
        this.$context = context;
        this.$uri = uri;
        this.$url = str;
        this.$onFailure = function0;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        AppTransitionActivity.j = false;
        UriHandler.INSTANCE.openUrlExternally(this.$context, this.$uri, this.$url, this.$onFailure);
    }
}
