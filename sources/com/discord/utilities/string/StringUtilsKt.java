package com.discord.utilities.string;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import b.a.k.b;
import com.discord.utilities.locale.LocaleManager;
import com.discord.utils.R;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.t.n;
import d0.z.d.m;
import java.net.IDN;
import java.net.URL;
import java.text.Normalizer;
import java.text.NumberFormat;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.text.Regex;
/* compiled from: StringUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\r\n\u0002\b\b\n\u0002\u0010\u0012\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a\u001d\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006\u001a#\u0010\f\u001a\u0004\u0018\u00010\u000b*\u00020\u00002\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t¢\u0006\u0004\b\f\u0010\r\u001a\u0019\u0010\u000e\u001a\u00020\t*\u00020\u00072\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u000e\u0010\u000f\u001a\u0011\u0010\u0010\u001a\u00020\t*\u00020\u000b¢\u0006\u0004\b\u0010\u0010\u0011\u001a\u0011\u0010\u0012\u001a\u00020\t*\u00020\t¢\u0006\u0004\b\u0012\u0010\u0013\u001a\u0011\u0010\u0015\u001a\u00020\t*\u00020\u0014¢\u0006\u0004\b\u0015\u0010\u0016\u001a-\u0010\u0019\u001a\u00020\t*\u0004\u0018\u00010\t2\u0018\u0010\u0018\u001a\u0014\u0012\u0006\u0012\u0004\u0018\u00010\t\u0012\u0006\u0012\u0004\u0018\u00010\t\u0018\u00010\u0017¢\u0006\u0004\b\u0019\u0010\u001a\u001a\u0011\u0010\u001b\u001a\u00020\t*\u00020\t¢\u0006\u0004\b\u001b\u0010\u0013\"\u0019\u0010\u001c\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001f\"\u0016\u0010!\u001a\u00020 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"¨\u0006#"}, d2 = {"Landroid/content/Context;", "context", "Landroid/content/res/Resources;", "resources", "", "test", "(Landroid/content/Context;Landroid/content/res/Resources;)V", "", "resId", "", "locale", "", "getStringByLocale", "(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/CharSequence;", "format", "(ILandroid/content/Context;)Ljava/lang/String;", "filenameSanitized", "(Ljava/lang/CharSequence;)Ljava/lang/String;", "stripAccents", "(Ljava/lang/String;)Ljava/lang/String;", "", "encodeToBase32String", "([B)Ljava/lang/String;", "Lkotlin/Function1;", "transform", "transformOrEmpty", "(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Ljava/lang/String;", "toPunyCodeASCIIUrl", "STATIC_IMAGE_EXTENSION", "Ljava/lang/String;", "getSTATIC_IMAGE_EXTENSION", "()Ljava/lang/String;", "Lkotlin/text/Regex;", "STRIP_ACCENTS_REGEX", "Lkotlin/text/Regex;", "utils_release"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StringUtilsKt {
    private static final String STATIC_IMAGE_EXTENSION;
    private static final Regex STRIP_ACCENTS_REGEX = new Regex("[\\p{InCombiningDiacriticalMarks}]");

    static {
        STATIC_IMAGE_EXTENSION = n.listOf((Object[]) new Integer[]{28, 29}).contains(Integer.valueOf(Build.VERSION.SDK_INT)) ? "png" : "webp";
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static final String encodeToBase32String(byte[] bArr) {
        int i;
        int i2;
        m.checkNotNullParameter(bArr, "$this$encodeToBase32String");
        StringBuffer stringBuffer = new StringBuffer(((bArr.length + 7) * 8) / 5);
        int i3 = 0;
        int i4 = 0;
        while (i3 < bArr.length) {
            int i5 = bArr[i3] >= 0 ? bArr[i3] : bArr[i3] + 256;
            if (i4 > 3) {
                i3++;
                if (i3 < bArr.length) {
                    i2 = bArr[i3] >= 0 ? bArr[i3] : bArr[i3] + 256;
                } else {
                    i2 = 0;
                }
                i4 = (i4 + 5) % 8;
                i = ((i5 & (255 >> i4)) << i4) | (i2 >> (8 - i4));
            } else {
                int i6 = i4 + 5;
                i = (i5 >> (8 - i6)) & 31;
                i4 = i6 % 8;
                if (i4 == 0) {
                    i3++;
                }
            }
            stringBuffer.append("ABCDEFGHIJKLMNOPQRSTUVWXYZ234567".charAt(i));
        }
        String stringBuffer2 = stringBuffer.toString();
        m.checkNotNullExpressionValue(stringBuffer2, "Base32.encodeOriginal(this)");
        return stringBuffer2;
    }

    public static final String filenameSanitized(CharSequence charSequence) {
        m.checkNotNullParameter(charSequence, "$this$filenameSanitized");
        return new Regex("[/\\\\]").replace(charSequence, "_");
    }

    public static final String format(int i, Context context) {
        m.checkNotNullParameter(context, "context");
        String format = NumberFormat.getInstance(new LocaleManager().getPrimaryLocale(context)).format(Integer.valueOf(i));
        m.checkNotNullExpressionValue(format, "NumberFormat.getInstance…le(context)).format(this)");
        return format;
    }

    public static final String getSTATIC_IMAGE_EXTENSION() {
        return STATIC_IMAGE_EXTENSION;
    }

    public static final CharSequence getStringByLocale(Context context, int i, String str) {
        CharSequence c;
        m.checkNotNullParameter(context, "$this$getStringByLocale");
        m.checkNotNullParameter(str, "locale");
        try {
            Resources resources = context.getResources();
            m.checkNotNullExpressionValue(resources, "resources");
            Configuration configuration = new Configuration(resources.getConfiguration());
            configuration.setLocale(new Locale(str));
            Context createConfigurationContext = context.createConfigurationContext(configuration);
            m.checkNotNullExpressionValue(createConfigurationContext, "createConfigurationContext(configuration)");
            Resources resources2 = createConfigurationContext.getResources();
            m.checkNotNullExpressionValue(resources2, "createConfigurationConte…(configuration).resources");
            c = b.c(resources2, i, new Object[0], (r4 & 4) != 0 ? b.d.j : null);
            return c;
        } catch (Resources.NotFoundException unused) {
            return null;
        }
    }

    public static final String stripAccents(String str) {
        m.checkNotNullParameter(str, "$this$stripAccents");
        String normalize = Normalizer.normalize(str, Normalizer.Form.NFKD);
        m.checkNotNullExpressionValue(normalize, "normalizedString");
        return STRIP_ACCENTS_REGEX.replace(normalize, "");
    }

    public static final void test(Context context, Resources resources) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(resources, "resources");
        context.getString(R.a.common_google_play_services_unknown_issue, 1);
        resources.getString(R.a.common_google_play_services_enable_button, 1);
    }

    public static final String toPunyCodeASCIIUrl(String str) {
        m.checkNotNullParameter(str, "$this$toPunyCodeASCIIUrl");
        URL url = new URL(str);
        StringBuilder sb = new StringBuilder(new URL(url.getProtocol(), IDN.toASCII(url.getHost(), 1), url.getPort(), url.getFile()).toString());
        if (url.getRef() != null) {
            sb.append(MentionUtilsKt.CHANNELS_CHAR);
            sb.append(url.getRef());
        }
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "safeUrlStringBuilder.toString()");
        return sb2;
    }

    public static final String transformOrEmpty(String str, Function1<? super String, String> function1) {
        String invoke;
        if (!(function1 == null || (invoke = function1.invoke(str)) == null)) {
            str = invoke;
        }
        return str != null ? str : "";
    }
}
