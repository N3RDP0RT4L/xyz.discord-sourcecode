package com.discord.utilities.rx;

import androidx.exifinterface.media.ExifInterface;
import androidx.recyclerview.widget.RecyclerView;
import b.i.a.f.e.o.f;
import d0.l;
import d0.w.d;
import d0.w.h.c;
import d0.w.i.a.e;
import d0.w.i.a.k;
import d0.z.d.m;
import d0.z.d.o;
import j0.g;
import java.util.Objects;
import java.util.concurrent.locks.LockSupport;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineDispatcher;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.channels.ProducerScope;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import s.a.b2.h;
import s.a.i1;
import s.a.k0;
import s.a.q0;
import s.a.v1;
import s.a.w;
import s.a.z0;
/* compiled from: RxCoroutineUtils.kt */
@e(c = "com.discord.utilities.rx.RxCoroutineUtilsKt$toFlow$2", f = "RxCoroutineUtils.kt", l = {47}, m = "invokeSuspend")
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u0002\"\u0004\b\u0000\u0010\u0000*\b\u0012\u0004\u0012\u00028\u00000\u0001H\u008a@¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {ExifInterface.GPS_DIRECTION_TRUE, "Lkotlinx/coroutines/channels/ProducerScope;", "", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class RxCoroutineUtilsKt$toFlow$2 extends k implements Function2<ProducerScope<? super T>, Continuation<? super Unit>, Object> {
    public final /* synthetic */ Observable $this_toFlow;
    private /* synthetic */ Object L$0;
    public int label;

    /* compiled from: RxCoroutineUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {ExifInterface.GPS_DIRECTION_TRUE, "", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.utilities.rx.RxCoroutineUtilsKt$toFlow$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ Subscription $subscription;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Subscription subscription) {
            super(0);
            this.$subscription = subscription;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            this.$subscription.unsubscribe();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public RxCoroutineUtilsKt$toFlow$2(Observable observable, Continuation continuation) {
        super(2, continuation);
        this.$this_toFlow = observable;
    }

    @Override // d0.w.i.a.a
    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        m.checkNotNullParameter(continuation, "completion");
        RxCoroutineUtilsKt$toFlow$2 rxCoroutineUtilsKt$toFlow$2 = new RxCoroutineUtilsKt$toFlow$2(this.$this_toFlow, continuation);
        rxCoroutineUtilsKt$toFlow$2.L$0 = obj;
        return rxCoroutineUtilsKt$toFlow$2;
    }

    @Override // kotlin.jvm.functions.Function2
    public final Object invoke(Object obj, Continuation<? super Unit> continuation) {
        return ((RxCoroutineUtilsKt$toFlow$2) create(obj, continuation)).invokeSuspend(Unit.a);
    }

    @Override // d0.w.i.a.a
    public final Object invokeSuspend(Object obj) {
        Subscription subscription;
        Object coroutine_suspended = c.getCOROUTINE_SUSPENDED();
        int i = this.label;
        if (i == 0) {
            l.throwOnFailure(obj);
            final ProducerScope producerScope = (ProducerScope) this.L$0;
            Observable observable = this.$this_toFlow;
            g<T> rxCoroutineUtilsKt$toFlow$2$subscription$1 = new g<T>() { // from class: com.discord.utilities.rx.RxCoroutineUtilsKt$toFlow$2$subscription$1
                @Override // j0.g
                public void onCompleted() {
                    f.I(producerScope, null, 1, null);
                }

                @Override // j0.g
                public void onError(Throwable th) {
                    f.r(producerScope, f.a("Error in Observable", th));
                }

                @Override // j0.g
                public void onNext(T t) {
                    CoroutineContext coroutineContext;
                    q0 q0Var;
                    ProducerScope producerScope2 = producerScope;
                    if (!producerScope2.offer(t)) {
                        w wVar = null;
                        h hVar = new h(producerScope2, t, null);
                        CoroutineContext coroutineContext2 = d0.w.f.j;
                        Thread currentThread = Thread.currentThread();
                        d.b bVar = d.b.a;
                        d dVar = (d) coroutineContext2.get(bVar);
                        if (dVar == null) {
                            v1 v1Var = v1.f3818b;
                            q0Var = v1.a();
                            coroutineContext = coroutineContext2.plus(coroutineContext2.plus(q0Var));
                            CoroutineDispatcher coroutineDispatcher = k0.a;
                            if (coroutineContext != coroutineDispatcher && coroutineContext.get(bVar) == null) {
                                coroutineContext = coroutineContext.plus(coroutineDispatcher);
                            }
                        } else {
                            if (!(dVar instanceof q0)) {
                                dVar = null;
                            }
                            q0 q0Var2 = (q0) dVar;
                            v1 v1Var2 = v1.f3818b;
                            q0Var = v1.a.get();
                            coroutineContext = coroutineContext2.plus(coroutineContext2);
                            CoroutineDispatcher coroutineDispatcher2 = k0.a;
                            if (coroutineContext != coroutineDispatcher2 && coroutineContext.get(bVar) == null) {
                                coroutineContext = coroutineContext.plus(coroutineDispatcher2);
                            }
                        }
                        s.a.f fVar = new s.a.f(coroutineContext, currentThread, q0Var);
                        fVar.j0(CoroutineStart.DEFAULT, fVar, hVar);
                        q0 q0Var3 = fVar.n;
                        if (q0Var3 != null) {
                            int i2 = q0.j;
                            q0Var3.L(false);
                        }
                        while (!Thread.interrupted()) {
                            q0 q0Var4 = fVar.n;
                            long O = q0Var4 != null ? q0Var4.O() : RecyclerView.FOREVER_NS;
                            if (!(fVar.M() instanceof z0)) {
                                q0 q0Var5 = fVar.n;
                                if (q0Var5 != null) {
                                    int i3 = q0.j;
                                    q0Var5.H(false);
                                }
                                Object a = i1.a(fVar.M());
                                if (a instanceof w) {
                                    wVar = a;
                                }
                                w wVar2 = wVar;
                                if (wVar2 != null) {
                                    throw wVar2.f3819b;
                                }
                                return;
                            }
                            LockSupport.parkNanos(fVar, O);
                        }
                        InterruptedException interruptedException = new InterruptedException();
                        fVar.w(interruptedException);
                        throw interruptedException;
                    }
                }
            };
            Objects.requireNonNull(observable);
            if (rxCoroutineUtilsKt$toFlow$2$subscription$1 instanceof Subscriber) {
                subscription = observable.U((Subscriber) rxCoroutineUtilsKt$toFlow$2$subscription$1);
            } else {
                subscription = observable.U(new j0.l.e.f(rxCoroutineUtilsKt$toFlow$2$subscription$1));
            }
            AnonymousClass1 r3 = new AnonymousClass1(subscription);
            this.label = 1;
            if (s.a.b2.l.a(producerScope, r3, this) == coroutine_suspended) {
                return coroutine_suspended;
            }
        } else if (i == 1) {
            l.throwOnFailure(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return Unit.a;
    }
}
