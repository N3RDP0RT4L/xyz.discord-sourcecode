package com.discord.utilities.rx;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import androidx.exifinterface.media.ExifInterface;
import b.i.a.f.e.o.f;
import com.discord.utilities.rx.OperatorBufferedDelay;
import d0.z.d.m;
import j0.l.a.r;
import j0.p.a;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.observers.SerializedSubscriber;
/* compiled from: OperatorBufferedDelay.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \u001e*\u0004\b\u0000\u0010\u00012\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0003\u0012\u0004\u0012\u00028\u00000\u0002:\u0002\u001e\u001fB)\u0012\u0006\u0010\u000e\u001a\u00020\r\u0012\b\b\u0002\u0010\u0018\u001a\u00020\u0017\u0012\u0006\u0010\t\u001a\u00020\b\u0012\u0006\u0010\u0013\u001a\u00020\u0012¢\u0006\u0004\b\u001c\u0010\u001dJ-\u0010\u0006\u001a\n\u0012\u0006\b\u0000\u0012\u00028\u00000\u00042\u0014\u0010\u0005\u001a\u0010\u0012\f\b\u0000\u0012\b\u0012\u0004\u0012\u00028\u00000\u00030\u0004H\u0016¢\u0006\u0004\b\u0006\u0010\u0007R\u0019\u0010\t\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\fR\u0019\u0010\u000e\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0013\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u0019\u0010\u0018\u001a\u00020\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001b¨\u0006 "}, d2 = {"Lcom/discord/utilities/rx/OperatorBufferedDelay;", ExifInterface.GPS_DIRECTION_TRUE, "Lrx/Observable$b;", "", "Lrx/Subscriber;", "childSubscriber", NotificationCompat.CATEGORY_CALL, "(Lrx/Subscriber;)Lrx/Subscriber;", "", "size", "I", "getSize", "()I", "", "timeSpan", "J", "getTimeSpan", "()J", "Lrx/Scheduler;", "scheduler", "Lrx/Scheduler;", "getScheduler", "()Lrx/Scheduler;", "Ljava/util/concurrent/TimeUnit;", "timeUnit", "Ljava/util/concurrent/TimeUnit;", "getTimeUnit", "()Ljava/util/concurrent/TimeUnit;", HookHelper.constructorName, "(JLjava/util/concurrent/TimeUnit;ILrx/Scheduler;)V", "Companion", "ExactSubscriber", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class OperatorBufferedDelay<T> implements Observable.b<List<? extends T>, T> {
    public static final Companion Companion = new Companion(null);
    private final Scheduler scheduler;
    private final int size;
    private final long timeSpan;
    private final TimeUnit timeUnit;

    /* compiled from: OperatorBufferedDelay.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000f\u0010\u0010JM\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00010\f0\u0003\"\u0004\b\u0001\u0010\u0002*\b\u0012\u0004\u0012\u00028\u00010\u00032\u0006\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\b2\b\b\u0002\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lcom/discord/utilities/rx/OperatorBufferedDelay$Companion;", "", ExifInterface.GPS_DIRECTION_TRUE, "Lrx/Observable;", "", "delay", "Ljava/util/concurrent/TimeUnit;", "timeUnit", "", "size", "Lrx/Scheduler;", "scheduler", "", "bufferedDelay", "(Lrx/Observable;JLjava/util/concurrent/TimeUnit;ILrx/Scheduler;)Lrx/Observable;", HookHelper.constructorName, "()V", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ Observable bufferedDelay$default(Companion companion, Observable observable, long j, TimeUnit timeUnit, int i, Scheduler scheduler, int i2, Object obj) {
            if ((i2 & 2) != 0) {
                timeUnit = TimeUnit.MILLISECONDS;
            }
            TimeUnit timeUnit2 = timeUnit;
            if ((i2 & 8) != 0) {
                scheduler = a.a();
                m.checkNotNullExpressionValue(scheduler, "Schedulers.computation()");
            }
            return companion.bufferedDelay(observable, j, timeUnit2, i, scheduler);
        }

        public final <T> Observable<List<T>> bufferedDelay(Observable<T> observable, long j, TimeUnit timeUnit, int i, Scheduler scheduler) {
            m.checkNotNullParameter(observable, "$this$bufferedDelay");
            m.checkNotNullParameter(timeUnit, "timeUnit");
            m.checkNotNullParameter(scheduler, "scheduler");
            Observable<List<T>> h02 = Observable.h0(new r(observable.j, new OperatorBufferedDelay(j, timeUnit, i, scheduler)));
            m.checkNotNullExpressionValue(h02, "this.lift(OperatorBuffer…meUnit, size, scheduler))");
            return h02;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: OperatorBufferedDelay.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\b\u0005\n\u0002\u0010\u0003\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0082\u0004\u0018\u0000*\u0004\b\u0001\u0010\u00012\b\u0012\u0004\u0012\u00028\u00010\u0002B#\u0012\u0012\u0010\u0018\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00010\r0\u0002\u0012\u0006\u0010\"\u001a\u00020!¢\u0006\u0004\b&\u0010'J\u000f\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J!\u0010\b\u001a\u00020\u00032\u0010\b\u0002\u0010\u0007\u001a\n\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u0006H\u0002¢\u0006\u0004\b\b\u0010\tJ'\u0010\u000b\u001a\n\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00062\u000e\u0010\n\u001a\n\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u0006H\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u001d\u0010\u000f\u001a\u00020\u00032\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00028\u00010\rH\u0002¢\u0006\u0004\b\u000f\u0010\tJ\u0017\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00028\u0001H\u0016¢\u0006\u0004\b\u0011\u0010\u0012J\u0019\u0010\u0015\u001a\u00020\u00032\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013H\u0016¢\u0006\u0004\b\u0015\u0010\u0016J\u000f\u0010\u0017\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0017\u0010\u0005R%\u0010\u0018\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00010\r0\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u0018\u0010\u001d\u001a\u0004\u0018\u00010\u001c8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u001e\u0010\u001f\u001a\n\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00068\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u0019\u0010\"\u001a\u00020!8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%¨\u0006("}, d2 = {"Lcom/discord/utilities/rx/OperatorBufferedDelay$ExactSubscriber;", ExifInterface.GPS_DIRECTION_TRUE, "Lrx/Subscriber;", "", "cancelBufferTimer", "()V", "", "newBufferValue", "tryEmit", "(Ljava/util/List;)V", "newValue", "getAndSetBuffer", "(Ljava/util/List;)Ljava/util/List;", "", "bufferedValues", "publish", "t", "onNext", "(Ljava/lang/Object;)V", "", "e", "onError", "(Ljava/lang/Throwable;)V", "onCompleted", "child", "Lrx/Subscriber;", "getChild", "()Lrx/Subscriber;", "Lrx/Subscription;", "ongoingBufferTimer", "Lrx/Subscription;", "buffer", "Ljava/util/List;", "Lrx/Scheduler$Worker;", "inner", "Lrx/Scheduler$Worker;", "getInner", "()Lrx/Scheduler$Worker;", HookHelper.constructorName, "(Lcom/discord/utilities/rx/OperatorBufferedDelay;Lrx/Subscriber;Lrx/Scheduler$Worker;)V", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final class ExactSubscriber<T> extends Subscriber<T> {
        private List<T> buffer = new ArrayList();
        private final Subscriber<List<T>> child;
        private final Scheduler.Worker inner;
        private Subscription ongoingBufferTimer;
        public final /* synthetic */ OperatorBufferedDelay this$0;

        public ExactSubscriber(OperatorBufferedDelay operatorBufferedDelay, Subscriber<List<T>> subscriber, Scheduler.Worker worker) {
            m.checkNotNullParameter(subscriber, "child");
            m.checkNotNullParameter(worker, "inner");
            this.this$0 = operatorBufferedDelay;
            this.child = subscriber;
            this.inner = worker;
        }

        private final void cancelBufferTimer() {
            Subscription subscription = this.ongoingBufferTimer;
            if (subscription != null) {
                subscription.unsubscribe();
            }
            this.ongoingBufferTimer = null;
        }

        private final List<T> getAndSetBuffer(List<T> list) {
            List<T> list2 = this.buffer;
            this.buffer = list;
            return list2;
        }

        private final void publish(List<? extends T> list) {
            try {
                if (!list.isEmpty()) {
                    this.child.onNext(list);
                }
            } catch (Throwable th) {
                f.o1(th);
                onError(th);
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        private final void tryEmit(List<T> list) {
            synchronized (this) {
                cancelBufferTimer();
                if (this.buffer != null) {
                    List andSetBuffer = getAndSetBuffer(list);
                    if (andSetBuffer != null) {
                        publish(andSetBuffer);
                    }
                }
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ void tryEmit$default(ExactSubscriber exactSubscriber, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                list = new ArrayList();
            }
            exactSubscriber.tryEmit(list);
        }

        public final Subscriber<List<T>> getChild() {
            return this.child;
        }

        public final Scheduler.Worker getInner() {
            return this.inner;
        }

        @Override // j0.g
        public void onCompleted() {
            this.inner.unsubscribe();
            tryEmit(null);
            this.child.onCompleted();
            unsubscribe();
        }

        @Override // j0.g
        public void onError(Throwable th) {
            tryEmit(null);
            this.child.onError(th);
            unsubscribe();
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // j0.g
        public void onNext(final T t) {
            synchronized (this) {
                List<T> list = this.buffer;
                if (list != null) {
                    list.add(t);
                    if (list.size() >= this.this$0.getSize()) {
                        cancelBufferTimer();
                        List andSetBuffer = getAndSetBuffer(new ArrayList());
                        if (andSetBuffer != null) {
                            publish(andSetBuffer);
                        }
                    } else if (this.ongoingBufferTimer == null) {
                        this.ongoingBufferTimer = this.inner.b(new Action0() { // from class: com.discord.utilities.rx.OperatorBufferedDelay$ExactSubscriber$onNext$$inlined$synchronized$lambda$1
                            @Override // rx.functions.Action0
                            public final void call() {
                                OperatorBufferedDelay.ExactSubscriber.tryEmit$default(OperatorBufferedDelay.ExactSubscriber.this, null, 1, null);
                            }
                        }, this.this$0.getTimeSpan(), this.this$0.getTimeUnit());
                    }
                }
            }
        }
    }

    public OperatorBufferedDelay(long j, TimeUnit timeUnit, int i, Scheduler scheduler) {
        m.checkNotNullParameter(timeUnit, "timeUnit");
        m.checkNotNullParameter(scheduler, "scheduler");
        this.timeSpan = j;
        this.timeUnit = timeUnit;
        this.size = i;
        this.scheduler = scheduler;
    }

    @Override // j0.k.b
    public /* bridge */ /* synthetic */ Object call(Object obj) {
        return call((Subscriber) ((Subscriber) obj));
    }

    public final Scheduler getScheduler() {
        return this.scheduler;
    }

    public final int getSize() {
        return this.size;
    }

    public final long getTimeSpan() {
        return this.timeSpan;
    }

    public final TimeUnit getTimeUnit() {
        return this.timeUnit;
    }

    public /* synthetic */ OperatorBufferedDelay(long j, TimeUnit timeUnit, int i, Scheduler scheduler, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(j, (i2 & 2) != 0 ? TimeUnit.MILLISECONDS : timeUnit, i, scheduler);
    }

    public Subscriber<? super T> call(Subscriber<? super List<? extends T>> subscriber) {
        m.checkNotNullParameter(subscriber, "childSubscriber");
        Scheduler.Worker a = this.scheduler.a();
        SerializedSubscriber serializedSubscriber = new SerializedSubscriber(subscriber);
        m.checkNotNullExpressionValue(a, "inner");
        ExactSubscriber exactSubscriber = new ExactSubscriber(this, serializedSubscriber, a);
        exactSubscriber.add(a);
        subscriber.add(exactSubscriber);
        return exactSubscriber;
    }
}
