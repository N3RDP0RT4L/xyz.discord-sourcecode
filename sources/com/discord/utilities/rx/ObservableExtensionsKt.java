package com.discord.utilities.rx;

import android.content.Context;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.discord.app.AppComponent;
import com.discord.restapi.utils.RetryWithDelay;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.dimmer.DimmerView;
import com.discord.utilities.error.Error;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import d0.z.d.m;
import j0.k.b;
import j0.l.a.r;
import j0.p.a;
import java.lang.ref.WeakReference;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Action1;
/* compiled from: ObservableExtensions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000f\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0005\u001a#\u0010\u0002\u001a\b\u0012\u0004\u0012\u00028\u00000\u0001\"\u0004\b\u0000\u0010\u0000*\b\u0012\u0004\u0012\u00028\u00000\u0001¢\u0006\u0004\b\u0002\u0010\u0003\u001a;\u0010\u0002\u001a\b\u0012\u0004\u0012\u00028\u00000\u0001\"\u0004\b\u0000\u0010\u0000*\b\u0012\u0004\u0012\u00028\u00000\u00012\u0006\u0010\u0005\u001a\u00020\u00042\u000e\b\u0002\u0010\u0007\u001a\b\u0012\u0002\b\u0003\u0018\u00010\u0006¢\u0006\u0004\b\u0002\u0010\b\u001a;\u0010\t\u001a\b\u0012\u0004\u0012\u00028\u00000\u0001\"\u0004\b\u0000\u0010\u0000*\b\u0012\u0004\u0012\u00028\u00000\u00012\u0006\u0010\u0005\u001a\u00020\u00042\u000e\b\u0002\u0010\u0007\u001a\b\u0012\u0002\b\u0003\u0018\u00010\u0006¢\u0006\u0004\b\t\u0010\b\u001a#\u0010\n\u001a\b\u0012\u0004\u0012\u00028\u00000\u0001\"\u0004\b\u0000\u0010\u0000*\b\u0012\u0004\u0012\u00028\u00000\u0001¢\u0006\u0004\b\n\u0010\u0003\u001a#\u0010\u000b\u001a\b\u0012\u0004\u0012\u00028\u00000\u0001\"\u0004\b\u0000\u0010\u0000*\b\u0012\u0004\u0012\u00028\u00000\u0001¢\u0006\u0004\b\u000b\u0010\u0003\u001a-\u0010\u000e\u001a\b\u0012\u0004\u0012\u00028\u00000\u0001\"\u0004\b\u0000\u0010\u0000*\b\u0012\u0004\u0012\u00028\u00000\u00012\b\b\u0002\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u000e\u0010\u000f\u001a\u0099\u0001\u0010\u001e\u001a\u00020\u0016\"\u0004\b\u0000\u0010\u0000*\b\u0012\u0004\u0012\u00028\u00000\u00012\n\u0010\u0011\u001a\u0006\u0012\u0002\b\u00030\u00102\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00122\u0016\b\u0002\u0010\u0017\u001a\u0010\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u00142\u0016\b\u0002\u0010\u0019\u001a\u0010\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u00142\u000e\b\u0002\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00160\u001a2\u000e\b\u0002\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00160\u001a2\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00160\u0014¢\u0006\u0004\b\u001e\u0010\u001f\u001a\u0095\u0001\u0010\u001e\u001a\u00020\u0016\"\u0004\b\u0000\u0010\u0000*\b\u0012\u0004\u0012\u00028\u00000\u00012\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00122\u0006\u0010!\u001a\u00020 2\u0016\b\u0002\u0010\u0017\u001a\u0010\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u00142\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00160\u00142\u0016\b\u0002\u0010\u0019\u001a\u0010\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u00142\u000e\b\u0002\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00160\u001a2\u000e\b\u0002\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00160\u001a¢\u0006\u0004\b\u001e\u0010\"\u001a3\u0010'\u001a\b\u0012\u0004\u0012\u00028\u00000\u0001\"\u0004\b\u0000\u0010\u0000*\b\u0012\u0004\u0012\u00028\u00000\u00012\u0006\u0010$\u001a\u00020#2\u0006\u0010&\u001a\u00020%¢\u0006\u0004\b'\u0010(\u001a&\u0010)\u001a\b\u0012\u0004\u0012\u00028\u00000\u0001\"\u0006\b\u0000\u0010\u0000\u0018\u0001*\u0006\u0012\u0002\b\u00030\u0001H\u0086\b¢\u0006\u0004\b)\u0010\u0003\u001a*\u0010*\u001a\b\u0012\u0004\u0012\u00028\u00000\u0001\"\u0006\b\u0000\u0010\u0000\u0018\u0001*\n\u0012\u0006\u0012\u0004\u0018\u00018\u00000\u0001H\u0086\b¢\u0006\u0004\b*\u0010\u0003\u001a?\u0010.\u001a\u0010\u0012\f\u0012\n -*\u0004\u0018\u00018\u00008\u00000\u0001\"\u0004\b\u0000\u0010\u0000*\b\u0012\u0004\u0012\u00028\u00000\u00012\b\b\u0002\u0010+\u001a\u00020#2\b\b\u0002\u0010,\u001a\u00020\f¢\u0006\u0004\b.\u0010/\u001a7\u00103\u001a\b\u0012\u0004\u0012\u00028\u00000\u0001\"\u0004\b\u0000\u0010\u0000*\b\u0012\u0004\u0012\u00028\u00000\u00012\b\u00101\u001a\u0004\u0018\u0001002\b\b\u0002\u00102\u001a\u00020#¢\u0006\u0004\b3\u00104¨\u00065"}, d2 = {ExifInterface.GPS_DIRECTION_TRUE, "Lrx/Observable;", "ui", "(Lrx/Observable;)Lrx/Observable;", "Lcom/discord/app/AppComponent;", "appComponent", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "adapter", "(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;)Lrx/Observable;", "bindToComponentLifecycle", "computationBuffered", "computationLatest", "", "retry", "restSubscribeOn", "(Lrx/Observable;Z)Lrx/Observable;", "Ljava/lang/Class;", "errorClass", "Landroid/content/Context;", "context", "Lkotlin/Function1;", "Lrx/Subscription;", "", "subscriptionHandler", "Lcom/discord/utilities/error/Error;", "errorHandler", "Lkotlin/Function0;", "onCompleted", "onTerminated", "onNext", "appSubscribe", "(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V", "", "errorTag", "(Lrx/Observable;Landroid/content/Context;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V", "", "windowDuration", "Ljava/util/concurrent/TimeUnit;", "timeUnit", "leadingEdgeThrottle", "(Lrx/Observable;JLjava/util/concurrent/TimeUnit;)Lrx/Observable;", "filterIs", "filterNull", "milliseconds", "emitError", "kotlin.jvm.PlatformType", "takeSingleUntilTimeout", "(Lrx/Observable;JZ)Lrx/Observable;", "Lcom/discord/utilities/dimmer/DimmerView;", "dimmer", "delay", "withDimmer", "(Lrx/Observable;Lcom/discord/utilities/dimmer/DimmerView;J)Lrx/Observable;", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ObservableExtensionsKt {
    public static final <T> void appSubscribe(Observable<T> observable, Class<?> cls, Context context, Function1<? super Subscription, Unit> function1, Function1<? super Error, Unit> function12, Function0<Unit> function0, Function0<Unit> function02, Function1<? super T, Unit> function13) {
        m.checkNotNullParameter(observable, "$this$appSubscribe");
        m.checkNotNullParameter(cls, "errorClass");
        m.checkNotNullParameter(function0, "onCompleted");
        m.checkNotNullParameter(function02, "onTerminated");
        m.checkNotNullParameter(function13, "onNext");
        String simpleName = cls.getSimpleName();
        m.checkNotNullExpressionValue(simpleName, "errorClass.simpleName");
        appSubscribe(observable, context, simpleName, function1, function13, function12, function0, function02);
    }

    public static /* synthetic */ void appSubscribe$default(Observable observable, Class cls, Context context, Function1 function1, Function1 function12, Function0 function0, Function0 function02, Function1 function13, int i, Object obj) {
        appSubscribe(observable, cls, (i & 2) != 0 ? null : context, (i & 4) != 0 ? null : function1, (i & 8) != 0 ? null : function12, (i & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : function0, (i & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : function02, function13);
    }

    public static final <T> Observable<T> bindToComponentLifecycle(Observable<T> observable, final AppComponent appComponent, final MGRecyclerAdapterSimple<?> mGRecyclerAdapterSimple) {
        m.checkNotNullParameter(observable, "$this$bindToComponentLifecycle");
        m.checkNotNullParameter(appComponent, "appComponent");
        Observable<T> a02 = observable.a0(appComponent.getUnsubscribeSignal());
        if (appComponent instanceof Fragment) {
            a02 = a02.x((b<T, Boolean>) new b<T, Boolean>() { // from class: com.discord.utilities.rx.ObservableExtensionsKt$bindToComponentLifecycle$$inlined$let$lambda$1
                /* JADX WARN: Can't rename method to resolve collision */
                @Override // j0.k.b
                public final Boolean call(T t) {
                    FragmentActivity activity;
                    return Boolean.valueOf(((Fragment) AppComponent.this).isAdded() && !((Fragment) AppComponent.this).isRemoving() && (activity = ((Fragment) AppComponent.this).e()) != null && !activity.isFinishing());
                }
            });
        }
        Observable<T> v = a02.v(new Action0() { // from class: com.discord.utilities.rx.ObservableExtensionsKt$bindToComponentLifecycle$2
            @Override // rx.functions.Action0
            public final void call() {
                MGRecyclerAdapterSimple mGRecyclerAdapterSimple2 = MGRecyclerAdapterSimple.this;
                if (mGRecyclerAdapterSimple2 != null) {
                    mGRecyclerAdapterSimple2.unsubscribeFromUpdates();
                }
            }
        });
        m.checkNotNullExpressionValue(v, "takeUntil(appComponent.u…beFromUpdates()\n        }");
        return v;
    }

    public static /* synthetic */ Observable bindToComponentLifecycle$default(Observable observable, AppComponent appComponent, MGRecyclerAdapterSimple mGRecyclerAdapterSimple, int i, Object obj) {
        if ((i & 2) != 0) {
            mGRecyclerAdapterSimple = null;
        }
        return bindToComponentLifecycle(observable, appComponent, mGRecyclerAdapterSimple);
    }

    public static final <T> Observable<T> computationBuffered(Observable<T> observable) {
        m.checkNotNullParameter(observable, "$this$computationBuffered");
        Observable<T> I = observable.J().X(a.a()).I(a.a());
        m.checkNotNullExpressionValue(I, "onBackpressureBuffer()\n …Schedulers.computation())");
        return I;
    }

    public static final <T> Observable<T> computationLatest(Observable<T> observable) {
        m.checkNotNullParameter(observable, "$this$computationLatest");
        Observable<T> I = observable.K().X(a.a()).I(a.a());
        m.checkNotNullExpressionValue(I, "onBackpressureLatest()\n …Schedulers.computation())");
        return I;
    }

    public static final /* synthetic */ <T> Observable<T> filterIs(Observable<?> observable) {
        m.checkNotNullParameter(observable, "$this$filterIs");
        m.needClassReification();
        Observable<?> x2 = observable.x(ObservableExtensionsKt$filterIs$1.INSTANCE);
        m.needClassReification();
        Observable<T> observable2 = (Observable<T>) x2.F(ObservableExtensionsKt$filterIs$2.INSTANCE);
        m.checkNotNullExpressionValue(observable2, "filter { it is T }.map { it as T }");
        return observable2;
    }

    public static final /* synthetic */ <T> Observable<T> filterNull(Observable<T> observable) {
        m.checkNotNullParameter(observable, "$this$filterNull");
        Observable<T> observable2 = (Observable<T>) observable.x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
        m.checkNotNullExpressionValue(observable2, "filter { it != null }.map { it!! }");
        return observable2;
    }

    public static final <T> Observable<T> leadingEdgeThrottle(Observable<T> observable, long j, TimeUnit timeUnit) {
        m.checkNotNullParameter(observable, "$this$leadingEdgeThrottle");
        m.checkNotNullParameter(timeUnit, "timeUnit");
        Observable<T> h02 = Observable.h0(new r(observable.j, new LeadingEdgeThrottle(j, timeUnit)));
        m.checkNotNullExpressionValue(h02, "lift(LeadingEdgeThrottle…indowDuration, timeUnit))");
        return h02;
    }

    public static final <T> Observable<T> restSubscribeOn(Observable<T> observable, boolean z2) {
        m.checkNotNullParameter(observable, "$this$restSubscribeOn");
        Observable<T> X = observable.X(a.c());
        if (z2) {
            RetryWithDelay retryWithDelay = RetryWithDelay.INSTANCE;
            m.checkNotNullExpressionValue(X, "observable");
            X = RetryWithDelay.restRetry$default(retryWithDelay, X, 0L, null, null, 7, null);
        }
        m.checkNotNullExpressionValue(X, "subscribeOn(Schedulers.i… observable\n      }\n    }");
        return X;
    }

    public static /* synthetic */ Observable restSubscribeOn$default(Observable observable, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = true;
        }
        return restSubscribeOn(observable, z2);
    }

    public static final <T> Observable<T> takeSingleUntilTimeout(Observable<T> observable, long j, boolean z2) {
        Observable<T> observable2;
        m.checkNotNullParameter(observable, "$this$takeSingleUntilTimeout");
        Observable<T> Z = observable.Z(1);
        if (z2) {
            observable2 = Z.c0(j, TimeUnit.MILLISECONDS);
        } else {
            observable2 = Z.a0(Observable.d0(j, TimeUnit.MILLISECONDS));
        }
        m.checkNotNullExpressionValue(observable2, "take(1).let {\n      if (…LISECONDS))\n      }\n    }");
        return computationLatest(observable2);
    }

    public static /* synthetic */ Observable takeSingleUntilTimeout$default(Observable observable, long j, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            j = 5000;
        }
        if ((i & 2) != 0) {
            z2 = true;
        }
        return takeSingleUntilTimeout(observable, j, z2);
    }

    public static final <T> Observable<T> ui(Observable<T> observable) {
        m.checkNotNullParameter(observable, "$this$ui");
        Observable<T> I = observable.I(j0.j.b.a.a());
        m.checkNotNullExpressionValue(I, "observeOn(AndroidSchedulers.mainThread())");
        return I;
    }

    public static /* synthetic */ Observable ui$default(Observable observable, AppComponent appComponent, MGRecyclerAdapterSimple mGRecyclerAdapterSimple, int i, Object obj) {
        if ((i & 2) != 0) {
            mGRecyclerAdapterSimple = null;
        }
        return ui(observable, appComponent, mGRecyclerAdapterSimple);
    }

    public static final <T> Observable<T> withDimmer(Observable<T> observable, DimmerView dimmerView, long j) {
        m.checkNotNullParameter(observable, "$this$withDimmer");
        Observable<T> h02 = Observable.h0(new r(observable.j, new OnDelayedEmissionHandler(new ObservableExtensionsKt$withDimmer$1(dimmerView), j, TimeUnit.MILLISECONDS, null, 8, null)));
        m.checkNotNullExpressionValue(h02, "lift(\n        OnDelayedE…LISECONDS\n        )\n    )");
        return h02;
    }

    public static /* synthetic */ Observable withDimmer$default(Observable observable, DimmerView dimmerView, long j, int i, Object obj) {
        if ((i & 2) != 0) {
            j = 300;
        }
        return withDimmer(observable, dimmerView, j);
    }

    public static final <T> Observable<T> ui(Observable<T> observable, AppComponent appComponent, MGRecyclerAdapterSimple<?> mGRecyclerAdapterSimple) {
        m.checkNotNullParameter(observable, "$this$ui");
        m.checkNotNullParameter(appComponent, "appComponent");
        return bindToComponentLifecycle(ui(observable), appComponent, mGRecyclerAdapterSimple);
    }

    public static final <T> void appSubscribe(Observable<T> observable, Context context, final String str, Function1<? super Subscription, Unit> function1, Function1<? super T, Unit> function12, final Function1<? super Error, Unit> function13, final Function0<Unit> function0, final Function0<Unit> function02) {
        m.checkNotNullParameter(observable, "$this$appSubscribe");
        m.checkNotNullParameter(str, "errorTag");
        m.checkNotNullParameter(function12, "onNext");
        m.checkNotNullParameter(function0, "onCompleted");
        m.checkNotNullParameter(function02, "onTerminated");
        final WeakReference weakReference = new WeakReference(context);
        ObservableExtensionsKt$sam$rx_functions_Action1$0 observableExtensionsKt$sam$rx_functions_Action1$0 = new ObservableExtensionsKt$sam$rx_functions_Action1$0(function12);
        Action1<Throwable> observableExtensionsKt$appSubscribe$subscription$1 = new Action1<Throwable>() { // from class: com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$subscription$1
            public final void call(Throwable th) {
                String str2 = str;
                Function1 function14 = function13;
                Object obj = function14;
                if (function14 != null) {
                    obj = new ObservableExtensionsKt$sam$rx_functions_Action1$0(function14);
                }
                Error.handle(th, str2, (Action1) obj, (Context) weakReference.get());
                function02.invoke();
            }
        };
        Action0 observableExtensionsKt$appSubscribe$subscription$2 = new Action0() { // from class: com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$subscription$2
            @Override // rx.functions.Action0
            public final void call() {
                Function0.this.invoke();
                function02.invoke();
            }
        };
        Objects.requireNonNull(observable);
        Subscription U = observable.U(new j0.l.e.b(observableExtensionsKt$sam$rx_functions_Action1$0, observableExtensionsKt$appSubscribe$subscription$1, observableExtensionsKt$appSubscribe$subscription$2));
        if (function1 != null) {
            m.checkNotNullExpressionValue(U, Traits.Payment.Type.SUBSCRIPTION);
            function1.invoke(U);
        }
    }

    public static /* synthetic */ void appSubscribe$default(Observable observable, Context context, String str, Function1 function1, Function1 function12, Function1 function13, Function0 function0, Function0 function02, int i, Object obj) {
        appSubscribe(observable, (i & 1) != 0 ? null : context, str, (i & 4) != 0 ? null : function1, function12, (i & 16) != 0 ? null : function13, (i & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : function0, (i & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : function02);
    }
}
