package com.discord.utilities.rx;

import d0.z.d.m;
import j0.l.e.k;
import kotlin.Metadata;
import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
/* compiled from: OnDelayedEmissionHandler.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u0003\n\u0002\b\u0007*\u0001\u0000\b\n\u0018\u00002\b\u0012\u0004\u0012\u00028\u00000\u0001J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0004J\u0017\u0010\b\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\u000b\u001a\u00020\u00022\u0006\u0010\n\u001a\u00028\u0000H\u0016¢\u0006\u0004\b\u000b\u0010\f¨\u0006\r"}, d2 = {"com/discord/utilities/rx/OnDelayedEmissionHandler$call$wrappedSubscriber$1", "Lrx/Subscriber;", "", "tryFinish", "()V", "onCompleted", "", "e", "onError", "(Ljava/lang/Throwable;)V", "t", "onNext", "(Ljava/lang/Object;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class OnDelayedEmissionHandler$call$wrappedSubscriber$1 extends Subscriber<T> {
    public final /* synthetic */ Subscriber $subscriber;
    public final /* synthetic */ OnDelayedEmissionHandler this$0;

    public OnDelayedEmissionHandler$call$wrappedSubscriber$1(OnDelayedEmissionHandler onDelayedEmissionHandler, Subscriber subscriber) {
        this.this$0 = onDelayedEmissionHandler;
        this.$subscriber = subscriber;
    }

    private final void tryFinish() {
        Subscription subscription;
        Scheduler scheduler;
        subscription = this.this$0.delaySubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        k kVar = new k(null);
        scheduler = this.this$0.scheduler;
        Observable<T> I = kVar.I(scheduler);
        m.checkNotNullExpressionValue(I, "Observable\n            .…    .observeOn(scheduler)");
        ObservableExtensionsKt.appSubscribe(I, OnDelayedEmissionHandler$call$wrappedSubscriber$1.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new OnDelayedEmissionHandler$call$wrappedSubscriber$1$tryFinish$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new OnDelayedEmissionHandler$call$wrappedSubscriber$1$tryFinish$1(this));
    }

    @Override // j0.g
    public void onCompleted() {
        if (!this.$subscriber.isUnsubscribed()) {
            tryFinish();
            this.$subscriber.onCompleted();
        }
    }

    @Override // j0.g
    public void onError(Throwable th) {
        m.checkNotNullParameter(th, "e");
        if (!this.$subscriber.isUnsubscribed()) {
            tryFinish();
            this.$subscriber.onError(th);
        }
    }

    @Override // j0.g
    public void onNext(T t) {
        if (!this.$subscriber.isUnsubscribed()) {
            tryFinish();
            this.$subscriber.onNext(t);
        }
    }
}
