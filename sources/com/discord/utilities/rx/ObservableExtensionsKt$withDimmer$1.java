package com.discord.utilities.rx;

import androidx.exifinterface.media.ExifInterface;
import com.discord.utilities.dimmer.DimmerView;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: ObservableExtensions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u0003\"\u0004\b\u0000\u0010\u00002\u0006\u0010\u0002\u001a\u00020\u0001H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {ExifInterface.GPS_DIRECTION_TRUE, "", "it", "", "invoke", "(Z)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ObservableExtensionsKt$withDimmer$1 extends o implements Function1<Boolean, Unit> {
    public final /* synthetic */ DimmerView $dimmer;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ObservableExtensionsKt$withDimmer$1(DimmerView dimmerView) {
        super(1);
        this.$dimmer = dimmerView;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Boolean bool) {
        invoke(bool.booleanValue());
        return Unit.a;
    }

    public final void invoke(boolean z2) {
        DimmerView dimmerView = this.$dimmer;
        if (dimmerView != null) {
            DimmerView.setDimmed$default(dimmerView, z2, false, 2, null);
        }
    }
}
