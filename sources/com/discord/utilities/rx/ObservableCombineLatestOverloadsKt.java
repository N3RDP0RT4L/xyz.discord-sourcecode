package com.discord.utilities.rx;

import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function10;
import kotlin.jvm.functions.Function11;
import kotlin.jvm.functions.Function12;
import kotlin.jvm.functions.Function13;
import kotlin.jvm.functions.Function14;
import kotlin.jvm.functions.Function15;
import kotlin.jvm.functions.Function16;
import kotlin.jvm.functions.Function22;
import rx.Observable;
import rx.functions.Func2;
import rx.functions.Func3;
import rx.functions.Func4;
import rx.functions.Func5;
import rx.functions.Func6;
import rx.functions.Func7;
import rx.functions.Func8;
/* compiled from: ObservableCombineLatestOverloads.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000R\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a«\u0002\u0010\u0018\u001a\b\u0012\u0004\u0012\u00028\n0\u000b\"\u0004\b\u0000\u0010\u0000\"\u0004\b\u0001\u0010\u0001\"\u0004\b\u0002\u0010\u0002\"\u0004\b\u0003\u0010\u0003\"\u0004\b\u0004\u0010\u0004\"\u0004\b\u0005\u0010\u0005\"\u0004\b\u0006\u0010\u0006\"\u0004\b\u0007\u0010\u0007\"\u0004\b\b\u0010\b\"\u0004\b\t\u0010\t\"\u0004\b\n\u0010\n2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00028\u00000\u000b2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00010\u000b2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00028\u00020\u000b2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00030\u000b2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00028\u00040\u000b2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00028\u00050\u000b2\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00028\u00060\u000b2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00028\u00070\u000b2\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00028\b0\u000b2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00028\t0\u000b2H\u0010\u0017\u001aD\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u0003\u0012\u0004\u0012\u00028\u0004\u0012\u0004\u0012\u00028\u0005\u0012\u0004\u0012\u00028\u0006\u0012\u0004\u0012\u00028\u0007\u0012\u0004\u0012\u00028\b\u0012\u0004\u0012\u00028\t\u0012\u0004\u0012\u00028\n0\u0016¢\u0006\u0004\b\u0018\u0010\u0019\u001aÅ\u0002\u0010\u0018\u001a\b\u0012\u0004\u0012\u00028\u000b0\u000b\"\u0004\b\u0000\u0010\u0000\"\u0004\b\u0001\u0010\u0001\"\u0004\b\u0002\u0010\u0002\"\u0004\b\u0003\u0010\u0003\"\u0004\b\u0004\u0010\u0004\"\u0004\b\u0005\u0010\u0005\"\u0004\b\u0006\u0010\u0006\"\u0004\b\u0007\u0010\u0007\"\u0004\b\b\u0010\b\"\u0004\b\t\u0010\t\"\u0004\b\n\u0010\u001a\"\u0004\b\u000b\u0010\n2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00028\u00000\u000b2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00010\u000b2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00028\u00020\u000b2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00030\u000b2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00028\u00040\u000b2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00028\u00050\u000b2\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00028\u00060\u000b2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00028\u00070\u000b2\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00028\b0\u000b2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00028\t0\u000b2\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00028\n0\u000b2N\u0010\u0017\u001aJ\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u0003\u0012\u0004\u0012\u00028\u0004\u0012\u0004\u0012\u00028\u0005\u0012\u0004\u0012\u00028\u0006\u0012\u0004\u0012\u00028\u0007\u0012\u0004\u0012\u00028\b\u0012\u0004\u0012\u00028\t\u0012\u0004\u0012\u00028\n\u0012\u0004\u0012\u00028\u000b0\u001c¢\u0006\u0004\b\u0018\u0010\u001d\u001aß\u0002\u0010\u0018\u001a\b\u0012\u0004\u0012\u00028\f0\u000b\"\u0004\b\u0000\u0010\u0000\"\u0004\b\u0001\u0010\u0001\"\u0004\b\u0002\u0010\u0002\"\u0004\b\u0003\u0010\u0003\"\u0004\b\u0004\u0010\u0004\"\u0004\b\u0005\u0010\u0005\"\u0004\b\u0006\u0010\u0006\"\u0004\b\u0007\u0010\u0007\"\u0004\b\b\u0010\b\"\u0004\b\t\u0010\t\"\u0004\b\n\u0010\u001a\"\u0004\b\u000b\u0010\u001e\"\u0004\b\f\u0010\n2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00028\u00000\u000b2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00010\u000b2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00028\u00020\u000b2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00030\u000b2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00028\u00040\u000b2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00028\u00050\u000b2\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00028\u00060\u000b2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00028\u00070\u000b2\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00028\b0\u000b2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00028\t0\u000b2\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00028\n0\u000b2\f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00028\u000b0\u000b2T\u0010\u0017\u001aP\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u0003\u0012\u0004\u0012\u00028\u0004\u0012\u0004\u0012\u00028\u0005\u0012\u0004\u0012\u00028\u0006\u0012\u0004\u0012\u00028\u0007\u0012\u0004\u0012\u00028\b\u0012\u0004\u0012\u00028\t\u0012\u0004\u0012\u00028\n\u0012\u0004\u0012\u00028\u000b\u0012\u0004\u0012\u00028\f0 ¢\u0006\u0004\b\u0018\u0010!\u001aù\u0002\u0010\u0018\u001a\b\u0012\u0004\u0012\u00028\r0\u000b\"\u0004\b\u0000\u0010\u0000\"\u0004\b\u0001\u0010\u0001\"\u0004\b\u0002\u0010\u0002\"\u0004\b\u0003\u0010\u0003\"\u0004\b\u0004\u0010\u0004\"\u0004\b\u0005\u0010\u0005\"\u0004\b\u0006\u0010\u0006\"\u0004\b\u0007\u0010\u0007\"\u0004\b\b\u0010\b\"\u0004\b\t\u0010\t\"\u0004\b\n\u0010\u001a\"\u0004\b\u000b\u0010\u001e\"\u0004\b\f\u0010\"\"\u0004\b\r\u0010\n2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00028\u00000\u000b2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00010\u000b2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00028\u00020\u000b2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00030\u000b2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00028\u00040\u000b2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00028\u00050\u000b2\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00028\u00060\u000b2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00028\u00070\u000b2\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00028\b0\u000b2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00028\t0\u000b2\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00028\n0\u000b2\f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00028\u000b0\u000b2\f\u0010#\u001a\b\u0012\u0004\u0012\u00028\f0\u000b2Z\u0010\u0017\u001aV\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u0003\u0012\u0004\u0012\u00028\u0004\u0012\u0004\u0012\u00028\u0005\u0012\u0004\u0012\u00028\u0006\u0012\u0004\u0012\u00028\u0007\u0012\u0004\u0012\u00028\b\u0012\u0004\u0012\u00028\t\u0012\u0004\u0012\u00028\n\u0012\u0004\u0012\u00028\u000b\u0012\u0004\u0012\u00028\f\u0012\u0004\u0012\u00028\r0$¢\u0006\u0004\b\u0018\u0010%\u001a\u0093\u0003\u0010\u0018\u001a\b\u0012\u0004\u0012\u00028\u000e0\u000b\"\u0004\b\u0000\u0010\u0000\"\u0004\b\u0001\u0010\u0001\"\u0004\b\u0002\u0010\u0002\"\u0004\b\u0003\u0010\u0003\"\u0004\b\u0004\u0010\u0004\"\u0004\b\u0005\u0010\u0005\"\u0004\b\u0006\u0010\u0006\"\u0004\b\u0007\u0010\u0007\"\u0004\b\b\u0010\b\"\u0004\b\t\u0010\t\"\u0004\b\n\u0010\u001a\"\u0004\b\u000b\u0010\u001e\"\u0004\b\f\u0010\"\"\u0004\b\r\u0010&\"\u0004\b\u000e\u0010\n2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00028\u00000\u000b2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00010\u000b2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00028\u00020\u000b2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00030\u000b2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00028\u00040\u000b2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00028\u00050\u000b2\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00028\u00060\u000b2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00028\u00070\u000b2\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00028\b0\u000b2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00028\t0\u000b2\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00028\n0\u000b2\f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00028\u000b0\u000b2\f\u0010#\u001a\b\u0012\u0004\u0012\u00028\f0\u000b2\f\u0010'\u001a\b\u0012\u0004\u0012\u00028\r0\u000b2`\u0010\u0017\u001a\\\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u0003\u0012\u0004\u0012\u00028\u0004\u0012\u0004\u0012\u00028\u0005\u0012\u0004\u0012\u00028\u0006\u0012\u0004\u0012\u00028\u0007\u0012\u0004\u0012\u00028\b\u0012\u0004\u0012\u00028\t\u0012\u0004\u0012\u00028\n\u0012\u0004\u0012\u00028\u000b\u0012\u0004\u0012\u00028\f\u0012\u0004\u0012\u00028\r\u0012\u0004\u0012\u00028\u000e0(¢\u0006\u0004\b\u0018\u0010)\u001a\u00ad\u0003\u0010\u0018\u001a\b\u0012\u0004\u0012\u00028\u000f0\u000b\"\u0004\b\u0000\u0010\u0000\"\u0004\b\u0001\u0010\u0001\"\u0004\b\u0002\u0010\u0002\"\u0004\b\u0003\u0010\u0003\"\u0004\b\u0004\u0010\u0004\"\u0004\b\u0005\u0010\u0005\"\u0004\b\u0006\u0010\u0006\"\u0004\b\u0007\u0010\u0007\"\u0004\b\b\u0010\b\"\u0004\b\t\u0010\t\"\u0004\b\n\u0010\u001a\"\u0004\b\u000b\u0010\u001e\"\u0004\b\f\u0010\"\"\u0004\b\r\u0010&\"\u0004\b\u000e\u0010*\"\u0004\b\u000f\u0010\n2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00028\u00000\u000b2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00010\u000b2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00028\u00020\u000b2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00030\u000b2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00028\u00040\u000b2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00028\u00050\u000b2\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00028\u00060\u000b2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00028\u00070\u000b2\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00028\b0\u000b2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00028\t0\u000b2\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00028\n0\u000b2\f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00028\u000b0\u000b2\f\u0010#\u001a\b\u0012\u0004\u0012\u00028\f0\u000b2\f\u0010'\u001a\b\u0012\u0004\u0012\u00028\r0\u000b2\f\u0010+\u001a\b\u0012\u0004\u0012\u00028\u000e0\u000b2f\u0010\u0017\u001ab\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u0003\u0012\u0004\u0012\u00028\u0004\u0012\u0004\u0012\u00028\u0005\u0012\u0004\u0012\u00028\u0006\u0012\u0004\u0012\u00028\u0007\u0012\u0004\u0012\u00028\b\u0012\u0004\u0012\u00028\t\u0012\u0004\u0012\u00028\n\u0012\u0004\u0012\u00028\u000b\u0012\u0004\u0012\u00028\f\u0012\u0004\u0012\u00028\r\u0012\u0004\u0012\u00028\u000e\u0012\u0004\u0012\u00028\u000f0,¢\u0006\u0004\b\u0018\u0010-\u001aÇ\u0003\u0010\u0018\u001a\b\u0012\u0004\u0012\u00028\u00100\u000b\"\u0004\b\u0000\u0010\u0000\"\u0004\b\u0001\u0010\u0001\"\u0004\b\u0002\u0010\u0002\"\u0004\b\u0003\u0010\u0003\"\u0004\b\u0004\u0010\u0004\"\u0004\b\u0005\u0010\u0005\"\u0004\b\u0006\u0010\u0006\"\u0004\b\u0007\u0010\u0007\"\u0004\b\b\u0010\b\"\u0004\b\t\u0010\t\"\u0004\b\n\u0010\u001a\"\u0004\b\u000b\u0010\u001e\"\u0004\b\f\u0010\"\"\u0004\b\r\u0010&\"\u0004\b\u000e\u0010*\"\u0004\b\u000f\u0010.\"\u0004\b\u0010\u0010\n2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00028\u00000\u000b2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00010\u000b2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00028\u00020\u000b2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00030\u000b2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00028\u00040\u000b2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00028\u00050\u000b2\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00028\u00060\u000b2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00028\u00070\u000b2\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00028\b0\u000b2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00028\t0\u000b2\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00028\n0\u000b2\f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00028\u000b0\u000b2\f\u0010#\u001a\b\u0012\u0004\u0012\u00028\f0\u000b2\f\u0010'\u001a\b\u0012\u0004\u0012\u00028\r0\u000b2\f\u0010+\u001a\b\u0012\u0004\u0012\u00028\u000e0\u000b2\f\u0010/\u001a\b\u0012\u0004\u0012\u00028\u000f0\u000b2l\u0010\u0017\u001ah\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u0003\u0012\u0004\u0012\u00028\u0004\u0012\u0004\u0012\u00028\u0005\u0012\u0004\u0012\u00028\u0006\u0012\u0004\u0012\u00028\u0007\u0012\u0004\u0012\u00028\b\u0012\u0004\u0012\u00028\t\u0012\u0004\u0012\u00028\n\u0012\u0004\u0012\u00028\u000b\u0012\u0004\u0012\u00028\f\u0012\u0004\u0012\u00028\r\u0012\u0004\u0012\u00028\u000e\u0012\u0004\u0012\u00028\u000f\u0012\u0004\u0012\u00028\u001000¢\u0006\u0004\b\u0018\u00101\u001aå\u0004\u0010\u0018\u001a\b\u0012\u0004\u0012\u00028\u00160\u000b\"\u0004\b\u0000\u0010\u0000\"\u0004\b\u0001\u0010\u0001\"\u0004\b\u0002\u0010\u0002\"\u0004\b\u0003\u0010\u0003\"\u0004\b\u0004\u0010\u0004\"\u0004\b\u0005\u0010\u0005\"\u0004\b\u0006\u0010\u0006\"\u0004\b\u0007\u0010\u0007\"\u0004\b\b\u0010\b\"\u0004\b\t\u0010\t\"\u0004\b\n\u0010\u001a\"\u0004\b\u000b\u0010\u001e\"\u0004\b\f\u0010\"\"\u0004\b\r\u0010&\"\u0004\b\u000e\u0010*\"\u0004\b\u000f\u0010.\"\u0004\b\u0010\u00102\"\u0004\b\u0011\u00103\"\u0004\b\u0012\u00104\"\u0004\b\u0013\u00105\"\u0004\b\u0014\u00106\"\u0004\b\u0015\u00107\"\u0004\b\u0016\u0010\n2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00028\u00000\u000b2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00010\u000b2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00028\u00020\u000b2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00030\u000b2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00028\u00040\u000b2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00028\u00050\u000b2\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00028\u00060\u000b2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00028\u00070\u000b2\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00028\b0\u000b2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00028\t0\u000b2\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00028\n0\u000b2\f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00028\u000b0\u000b2\f\u0010#\u001a\b\u0012\u0004\u0012\u00028\f0\u000b2\f\u0010'\u001a\b\u0012\u0004\u0012\u00028\r0\u000b2\f\u0010+\u001a\b\u0012\u0004\u0012\u00028\u000e0\u000b2\f\u0010/\u001a\b\u0012\u0004\u0012\u00028\u000f0\u000b2\f\u00108\u001a\b\u0012\u0004\u0012\u00028\u00100\u000b2\f\u00109\u001a\b\u0012\u0004\u0012\u00028\u00110\u000b2\f\u0010:\u001a\b\u0012\u0004\u0012\u00028\u00120\u000b2\f\u0010;\u001a\b\u0012\u0004\u0012\u00028\u00130\u000b2\f\u0010<\u001a\b\u0012\u0004\u0012\u00028\u00140\u000b2\f\u0010=\u001a\b\u0012\u0004\u0012\u00028\u00150\u000b2\u0091\u0001\u0010\u0017\u001a\u008c\u0001\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u0003\u0012\u0004\u0012\u00028\u0004\u0012\u0004\u0012\u00028\u0005\u0012\u0004\u0012\u00028\u0006\u0012\u0004\u0012\u00028\u0007\u0012\u0004\u0012\u00028\b\u0012\u0004\u0012\u00028\t\u0012\u0004\u0012\u00028\n\u0012\u0004\u0012\u00028\u000b\u0012\u0004\u0012\u00028\f\u0012\u0004\u0012\u00028\r\u0012\u0004\u0012\u00028\u000e\u0012\u0004\u0012\u00028\u000f\u0012\u0004\u0012\u00028\u0010\u0012\u0004\u0012\u00028\u0011\u0012\u0004\u0012\u00028\u0012\u0012\u0004\u0012\u00028\u0013\u0012\u0004\u0012\u00028\u0014\u0012\u0004\u0012\u00028\u0015\u0012\u0004\u0012\u00028\u00160>¢\u0006\u0004\b\u0018\u0010?\u001aÿ\u0001\u0010A\u001a>\u0012:\u00128\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u0003\u0012\u0004\u0012\u00028\u0004\u0012\u0004\u0012\u00028\u0005\u0012\u0004\u0012\u00028\u0006\u0012\u0004\u0012\u00028\u0007\u0012\u0004\u0012\u00028\b0@0\u000b\"\u0004\b\u0000\u0010\u0000\"\u0004\b\u0001\u0010\u0001\"\u0004\b\u0002\u0010\u0002\"\u0004\b\u0003\u0010\u0003\"\u0004\b\u0004\u0010\u0004\"\u0004\b\u0005\u0010\u0005\"\u0004\b\u0006\u0010\u0006\"\u0004\b\u0007\u0010\u0007\"\u0004\b\b\u0010\b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00028\u00000\u000b2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00010\u000b2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00028\u00020\u000b2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00030\u000b2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00028\u00040\u000b2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00028\u00050\u000b2\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00028\u00060\u000b2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00028\u00070\u000b2\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00028\b0\u000bH\u0002¢\u0006\u0004\bA\u0010B¨\u0006C"}, d2 = {"T1", "T2", "T3", "T4", "T5", "T6", "T7", "T8", "T9", "T10", "R", "Lrx/Observable;", "o1", "o2", "o3", "o4", "o5", "o6", "o7", "o8", "o9", "o10", "Lkotlin/Function10;", "combineFunction", "combineLatest", "(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lkotlin/jvm/functions/Function10;)Lrx/Observable;", "T11", "o11", "Lkotlin/Function11;", "(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lkotlin/jvm/functions/Function11;)Lrx/Observable;", "T12", "o12", "Lkotlin/Function12;", "(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lkotlin/jvm/functions/Function12;)Lrx/Observable;", "T13", "o13", "Lkotlin/Function13;", "(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lkotlin/jvm/functions/Function13;)Lrx/Observable;", "T14", "o14", "Lkotlin/Function14;", "(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lkotlin/jvm/functions/Function14;)Lrx/Observable;", "T15", "o15", "Lkotlin/Function15;", "(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lkotlin/jvm/functions/Function15;)Lrx/Observable;", "T16", "o16", "Lkotlin/Function16;", "(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lkotlin/jvm/functions/Function16;)Lrx/Observable;", "T17", "T18", "T19", "T20", "T21", "T22", "o17", "o18", "o19", "o20", "o21", "o22", "Lkotlin/Function22;", "(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lkotlin/jvm/functions/Function22;)Lrx/Observable;", "Lcom/discord/utilities/rx/Holder;", "createHolderObservable", "(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;)Lrx/Observable;", "utils_release"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ObservableCombineLatestOverloadsKt {
    public static final <T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, R> Observable<R> combineLatest(Observable<T1> observable, Observable<T2> observable2, Observable<T3> observable3, Observable<T4> observable4, Observable<T5> observable5, Observable<T6> observable6, Observable<T7> observable7, Observable<T8> observable8, Observable<T9> observable9, Observable<T10> observable10, final Function10<? super T1, ? super T2, ? super T3, ? super T4, ? super T5, ? super T6, ? super T7, ? super T8, ? super T9, ? super T10, ? extends R> function10) {
        m.checkNotNullParameter(observable, "o1");
        m.checkNotNullParameter(observable2, "o2");
        m.checkNotNullParameter(observable3, "o3");
        m.checkNotNullParameter(observable4, "o4");
        m.checkNotNullParameter(observable5, "o5");
        m.checkNotNullParameter(observable6, "o6");
        m.checkNotNullParameter(observable7, "o7");
        m.checkNotNullParameter(observable8, "o8");
        m.checkNotNullParameter(observable9, "o9");
        m.checkNotNullParameter(observable10, "o10");
        m.checkNotNullParameter(function10, "combineFunction");
        Observable<R> j = Observable.j(createHolderObservable(observable, observable2, observable3, observable4, observable5, observable6, observable7, observable8, observable9), observable10, new Func2<Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9>, T10, R>() { // from class: com.discord.utilities.rx.ObservableCombineLatestOverloadsKt$combineLatest$1
            public final R call(Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9> holder, T10 t10) {
                return (R) Function10.this.invoke(holder.getT1(), holder.getT2(), holder.getT3(), holder.getT4(), holder.getT5(), holder.getT6(), holder.getT7(), holder.getT8(), holder.getT9(), t10);
            }

            @Override // rx.functions.Func2
            public /* bridge */ /* synthetic */ Object call(Object obj, Object obj2) {
                return call((Holder) ((Holder) obj), (Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9>) obj2);
            }
        });
        m.checkNotNullExpressionValue(j, "Observable\n        .comb…t10\n          )\n        }");
        return j;
    }

    private static final <T1, T2, T3, T4, T5, T6, T7, T8, T9> Observable<Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9>> createHolderObservable(Observable<T1> observable, Observable<T2> observable2, Observable<T3> observable3, Observable<T4> observable4, Observable<T5> observable5, Observable<T6> observable6, Observable<T7> observable7, Observable<T8> observable8, Observable<T9> observable9) {
        Observable<Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9>> c = Observable.c(observable, observable2, observable3, observable4, observable5, observable6, observable7, observable8, observable9, ObservableCombineLatestOverloadsKt$createHolderObservable$1.INSTANCE);
        m.checkNotNullExpressionValue(c, "Observable\n    .combineL…t5, t6, t7, t8, t9)\n    }");
        return c;
    }

    public static final <T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, R> Observable<R> combineLatest(Observable<T1> observable, Observable<T2> observable2, Observable<T3> observable3, Observable<T4> observable4, Observable<T5> observable5, Observable<T6> observable6, Observable<T7> observable7, Observable<T8> observable8, Observable<T9> observable9, Observable<T10> observable10, Observable<T11> observable11, final Function11<? super T1, ? super T2, ? super T3, ? super T4, ? super T5, ? super T6, ? super T7, ? super T8, ? super T9, ? super T10, ? super T11, ? extends R> function11) {
        m.checkNotNullParameter(observable, "o1");
        m.checkNotNullParameter(observable2, "o2");
        m.checkNotNullParameter(observable3, "o3");
        m.checkNotNullParameter(observable4, "o4");
        m.checkNotNullParameter(observable5, "o5");
        m.checkNotNullParameter(observable6, "o6");
        m.checkNotNullParameter(observable7, "o7");
        m.checkNotNullParameter(observable8, "o8");
        m.checkNotNullParameter(observable9, "o9");
        m.checkNotNullParameter(observable10, "o10");
        m.checkNotNullParameter(observable11, "o11");
        m.checkNotNullParameter(function11, "combineFunction");
        Observable<R> i = Observable.i(createHolderObservable(observable, observable2, observable3, observable4, observable5, observable6, observable7, observable8, observable9), observable10, observable11, new Func3<Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9>, T10, T11, R>() { // from class: com.discord.utilities.rx.ObservableCombineLatestOverloadsKt$combineLatest$2
            public final R call(Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9> holder, T10 t10, T11 t11) {
                return (R) Function11.this.invoke(holder.getT1(), holder.getT2(), holder.getT3(), holder.getT4(), holder.getT5(), holder.getT6(), holder.getT7(), holder.getT8(), holder.getT9(), t10, t11);
            }

            @Override // rx.functions.Func3
            public /* bridge */ /* synthetic */ Object call(Object obj, Object obj2, Object obj3) {
                return call((Holder) ((Holder) obj), (Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9>) obj2, obj3);
            }
        });
        m.checkNotNullExpressionValue(i, "Observable\n        .comb…t11\n          )\n        }");
        return i;
    }

    public static final <T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, R> Observable<R> combineLatest(Observable<T1> observable, Observable<T2> observable2, Observable<T3> observable3, Observable<T4> observable4, Observable<T5> observable5, Observable<T6> observable6, Observable<T7> observable7, Observable<T8> observable8, Observable<T9> observable9, Observable<T10> observable10, Observable<T11> observable11, Observable<T12> observable12, final Function12<? super T1, ? super T2, ? super T3, ? super T4, ? super T5, ? super T6, ? super T7, ? super T8, ? super T9, ? super T10, ? super T11, ? super T12, ? extends R> function12) {
        m.checkNotNullParameter(observable, "o1");
        m.checkNotNullParameter(observable2, "o2");
        m.checkNotNullParameter(observable3, "o3");
        m.checkNotNullParameter(observable4, "o4");
        m.checkNotNullParameter(observable5, "o5");
        m.checkNotNullParameter(observable6, "o6");
        m.checkNotNullParameter(observable7, "o7");
        m.checkNotNullParameter(observable8, "o8");
        m.checkNotNullParameter(observable9, "o9");
        m.checkNotNullParameter(observable10, "o10");
        m.checkNotNullParameter(observable11, "o11");
        m.checkNotNullParameter(observable12, "o12");
        m.checkNotNullParameter(function12, "combineFunction");
        Observable<R> h = Observable.h(createHolderObservable(observable, observable2, observable3, observable4, observable5, observable6, observable7, observable8, observable9), observable10, observable11, observable12, new Func4<Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9>, T10, T11, T12, R>() { // from class: com.discord.utilities.rx.ObservableCombineLatestOverloadsKt$combineLatest$3
            public final R call(Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9> holder, T10 t10, T11 t11, T12 t12) {
                return (R) Function12.this.invoke(holder.getT1(), holder.getT2(), holder.getT3(), holder.getT4(), holder.getT5(), holder.getT6(), holder.getT7(), holder.getT8(), holder.getT9(), t10, t11, t12);
            }

            @Override // rx.functions.Func4
            public /* bridge */ /* synthetic */ Object call(Object obj, Object obj2, Object obj3, Object obj4) {
                return call((Holder) ((Holder) obj), (Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9>) obj2, obj3, obj4);
            }
        });
        m.checkNotNullExpressionValue(h, "Observable\n        .comb…t12\n          )\n        }");
        return h;
    }

    public static final <T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, R> Observable<R> combineLatest(Observable<T1> observable, Observable<T2> observable2, Observable<T3> observable3, Observable<T4> observable4, Observable<T5> observable5, Observable<T6> observable6, Observable<T7> observable7, Observable<T8> observable8, Observable<T9> observable9, Observable<T10> observable10, Observable<T11> observable11, Observable<T12> observable12, Observable<T13> observable13, final Function13<? super T1, ? super T2, ? super T3, ? super T4, ? super T5, ? super T6, ? super T7, ? super T8, ? super T9, ? super T10, ? super T11, ? super T12, ? super T13, ? extends R> function13) {
        m.checkNotNullParameter(observable, "o1");
        m.checkNotNullParameter(observable2, "o2");
        m.checkNotNullParameter(observable3, "o3");
        m.checkNotNullParameter(observable4, "o4");
        m.checkNotNullParameter(observable5, "o5");
        m.checkNotNullParameter(observable6, "o6");
        m.checkNotNullParameter(observable7, "o7");
        m.checkNotNullParameter(observable8, "o8");
        m.checkNotNullParameter(observable9, "o9");
        m.checkNotNullParameter(observable10, "o10");
        m.checkNotNullParameter(observable11, "o11");
        m.checkNotNullParameter(observable12, "o12");
        m.checkNotNullParameter(observable13, "o13");
        m.checkNotNullParameter(function13, "combineFunction");
        Observable<R> g = Observable.g(createHolderObservable(observable, observable2, observable3, observable4, observable5, observable6, observable7, observable8, observable9), observable10, observable11, observable12, observable13, new Func5<Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9>, T10, T11, T12, T13, R>() { // from class: com.discord.utilities.rx.ObservableCombineLatestOverloadsKt$combineLatest$4
            public final R call(Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9> holder, T10 t10, T11 t11, T12 t12, T13 t13) {
                return (R) Function13.this.invoke(holder.getT1(), holder.getT2(), holder.getT3(), holder.getT4(), holder.getT5(), holder.getT6(), holder.getT7(), holder.getT8(), holder.getT9(), t10, t11, t12, t13);
            }

            @Override // rx.functions.Func5
            public /* bridge */ /* synthetic */ Object call(Object obj, Object obj2, Object obj3, Object obj4, Object obj5) {
                return call((Holder) ((Holder) obj), (Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9>) obj2, obj3, obj4, obj5);
            }
        });
        m.checkNotNullExpressionValue(g, "Observable\n        .comb…t13\n          )\n        }");
        return g;
    }

    public static final <T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, R> Observable<R> combineLatest(Observable<T1> observable, Observable<T2> observable2, Observable<T3> observable3, Observable<T4> observable4, Observable<T5> observable5, Observable<T6> observable6, Observable<T7> observable7, Observable<T8> observable8, Observable<T9> observable9, Observable<T10> observable10, Observable<T11> observable11, Observable<T12> observable12, Observable<T13> observable13, Observable<T14> observable14, final Function14<? super T1, ? super T2, ? super T3, ? super T4, ? super T5, ? super T6, ? super T7, ? super T8, ? super T9, ? super T10, ? super T11, ? super T12, ? super T13, ? super T14, ? extends R> function14) {
        m.checkNotNullParameter(observable, "o1");
        m.checkNotNullParameter(observable2, "o2");
        m.checkNotNullParameter(observable3, "o3");
        m.checkNotNullParameter(observable4, "o4");
        m.checkNotNullParameter(observable5, "o5");
        m.checkNotNullParameter(observable6, "o6");
        m.checkNotNullParameter(observable7, "o7");
        m.checkNotNullParameter(observable8, "o8");
        m.checkNotNullParameter(observable9, "o9");
        m.checkNotNullParameter(observable10, "o10");
        m.checkNotNullParameter(observable11, "o11");
        m.checkNotNullParameter(observable12, "o12");
        m.checkNotNullParameter(observable13, "o13");
        m.checkNotNullParameter(observable14, "o14");
        m.checkNotNullParameter(function14, "combineFunction");
        Observable<R> f = Observable.f(createHolderObservable(observable, observable2, observable3, observable4, observable5, observable6, observable7, observable8, observable9), observable10, observable11, observable12, observable13, observable14, new Func6<Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9>, T10, T11, T12, T13, T14, R>() { // from class: com.discord.utilities.rx.ObservableCombineLatestOverloadsKt$combineLatest$5
            public final R call(Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9> holder, T10 t10, T11 t11, T12 t12, T13 t13, T14 t14) {
                return (R) Function14.this.invoke(holder.getT1(), holder.getT2(), holder.getT3(), holder.getT4(), holder.getT5(), holder.getT6(), holder.getT7(), holder.getT8(), holder.getT9(), t10, t11, t12, t13, t14);
            }

            @Override // rx.functions.Func6
            public /* bridge */ /* synthetic */ Object call(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6) {
                return call((Holder) ((Holder) obj), (Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9>) obj2, obj3, obj4, obj5, obj6);
            }
        });
        m.checkNotNullExpressionValue(f, "Observable\n        .comb…t14\n          )\n        }");
        return f;
    }

    public static final <T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, R> Observable<R> combineLatest(Observable<T1> observable, Observable<T2> observable2, Observable<T3> observable3, Observable<T4> observable4, Observable<T5> observable5, Observable<T6> observable6, Observable<T7> observable7, Observable<T8> observable8, Observable<T9> observable9, Observable<T10> observable10, Observable<T11> observable11, Observable<T12> observable12, Observable<T13> observable13, Observable<T14> observable14, Observable<T15> observable15, final Function15<? super T1, ? super T2, ? super T3, ? super T4, ? super T5, ? super T6, ? super T7, ? super T8, ? super T9, ? super T10, ? super T11, ? super T12, ? super T13, ? super T14, ? super T15, ? extends R> function15) {
        m.checkNotNullParameter(observable, "o1");
        m.checkNotNullParameter(observable2, "o2");
        m.checkNotNullParameter(observable3, "o3");
        m.checkNotNullParameter(observable4, "o4");
        m.checkNotNullParameter(observable5, "o5");
        m.checkNotNullParameter(observable6, "o6");
        m.checkNotNullParameter(observable7, "o7");
        m.checkNotNullParameter(observable8, "o8");
        m.checkNotNullParameter(observable9, "o9");
        m.checkNotNullParameter(observable10, "o10");
        m.checkNotNullParameter(observable11, "o11");
        m.checkNotNullParameter(observable12, "o12");
        m.checkNotNullParameter(observable13, "o13");
        m.checkNotNullParameter(observable14, "o14");
        m.checkNotNullParameter(observable15, "o15");
        m.checkNotNullParameter(function15, "combineFunction");
        Observable<R> e = Observable.e(createHolderObservable(observable, observable2, observable3, observable4, observable5, observable6, observable7, observable8, observable9), observable10, observable11, observable12, observable13, observable14, observable15, new Func7<Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9>, T10, T11, T12, T13, T14, T15, R>() { // from class: com.discord.utilities.rx.ObservableCombineLatestOverloadsKt$combineLatest$6
            public final R call(Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9> holder, T10 t10, T11 t11, T12 t12, T13 t13, T14 t14, T15 t15) {
                return (R) Function15.this.invoke(holder.getT1(), holder.getT2(), holder.getT3(), holder.getT4(), holder.getT5(), holder.getT6(), holder.getT7(), holder.getT8(), holder.getT9(), t10, t11, t12, t13, t14, t15);
            }

            @Override // rx.functions.Func7
            public /* bridge */ /* synthetic */ Object call(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7) {
                return call((Holder) ((Holder) obj), (Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9>) obj2, obj3, obj4, obj5, obj6, obj7);
            }
        });
        m.checkNotNullExpressionValue(e, "Observable\n        .comb…t15\n          )\n        }");
        return e;
    }

    public static final <T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, R> Observable<R> combineLatest(Observable<T1> observable, Observable<T2> observable2, Observable<T3> observable3, Observable<T4> observable4, Observable<T5> observable5, Observable<T6> observable6, Observable<T7> observable7, Observable<T8> observable8, Observable<T9> observable9, Observable<T10> observable10, Observable<T11> observable11, Observable<T12> observable12, Observable<T13> observable13, Observable<T14> observable14, Observable<T15> observable15, Observable<T16> observable16, final Function16<? super T1, ? super T2, ? super T3, ? super T4, ? super T5, ? super T6, ? super T7, ? super T8, ? super T9, ? super T10, ? super T11, ? super T12, ? super T13, ? super T14, ? super T15, ? super T16, ? extends R> function16) {
        m.checkNotNullParameter(observable, "o1");
        m.checkNotNullParameter(observable2, "o2");
        m.checkNotNullParameter(observable3, "o3");
        m.checkNotNullParameter(observable4, "o4");
        m.checkNotNullParameter(observable5, "o5");
        m.checkNotNullParameter(observable6, "o6");
        m.checkNotNullParameter(observable7, "o7");
        m.checkNotNullParameter(observable8, "o8");
        m.checkNotNullParameter(observable9, "o9");
        m.checkNotNullParameter(observable10, "o10");
        m.checkNotNullParameter(observable11, "o11");
        m.checkNotNullParameter(observable12, "o12");
        m.checkNotNullParameter(observable13, "o13");
        m.checkNotNullParameter(observable14, "o14");
        m.checkNotNullParameter(observable15, "o15");
        m.checkNotNullParameter(observable16, "o16");
        m.checkNotNullParameter(function16, "combineFunction");
        Observable<R> d = Observable.d(createHolderObservable(observable, observable2, observable3, observable4, observable5, observable6, observable7, observable8, observable9), observable10, observable11, observable12, observable13, observable14, observable15, observable16, new Func8<Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9>, T10, T11, T12, T13, T14, T15, T16, R>() { // from class: com.discord.utilities.rx.ObservableCombineLatestOverloadsKt$combineLatest$7
            public final R call(Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9> holder, T10 t10, T11 t11, T12 t12, T13 t13, T14 t14, T15 t15, T16 t16) {
                return (R) Function16.this.invoke(holder.getT1(), holder.getT2(), holder.getT3(), holder.getT4(), holder.getT5(), holder.getT6(), holder.getT7(), holder.getT8(), holder.getT9(), t10, t11, t12, t13, t14, t15, t16);
            }

            @Override // rx.functions.Func8
            public /* bridge */ /* synthetic */ Object call(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8) {
                return call((Holder) ((Holder) obj), (Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9>) obj2, obj3, obj4, obj5, obj6, obj7, obj8);
            }
        });
        m.checkNotNullExpressionValue(d, "Observable\n        .comb…t16\n          )\n        }");
        return d;
    }

    public static final <T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, T22, R> Observable<R> combineLatest(Observable<T1> observable, Observable<T2> observable2, Observable<T3> observable3, Observable<T4> observable4, Observable<T5> observable5, Observable<T6> observable6, Observable<T7> observable7, Observable<T8> observable8, Observable<T9> observable9, Observable<T10> observable10, Observable<T11> observable11, Observable<T12> observable12, Observable<T13> observable13, Observable<T14> observable14, Observable<T15> observable15, Observable<T16> observable16, Observable<T17> observable17, Observable<T18> observable18, Observable<T19> observable19, Observable<T20> observable20, Observable<T21> observable21, Observable<T22> observable22, final Function22<? super T1, ? super T2, ? super T3, ? super T4, ? super T5, ? super T6, ? super T7, ? super T8, ? super T9, ? super T10, ? super T11, ? super T12, ? super T13, ? super T14, ? super T15, ? super T16, ? super T17, ? super T18, ? super T19, ? super T20, ? super T21, ? super T22, ? extends R> function22) {
        m.checkNotNullParameter(observable, "o1");
        m.checkNotNullParameter(observable2, "o2");
        m.checkNotNullParameter(observable3, "o3");
        m.checkNotNullParameter(observable4, "o4");
        m.checkNotNullParameter(observable5, "o5");
        m.checkNotNullParameter(observable6, "o6");
        m.checkNotNullParameter(observable7, "o7");
        m.checkNotNullParameter(observable8, "o8");
        m.checkNotNullParameter(observable9, "o9");
        m.checkNotNullParameter(observable10, "o10");
        m.checkNotNullParameter(observable11, "o11");
        m.checkNotNullParameter(observable12, "o12");
        m.checkNotNullParameter(observable13, "o13");
        m.checkNotNullParameter(observable14, "o14");
        m.checkNotNullParameter(observable15, "o15");
        m.checkNotNullParameter(observable16, "o16");
        m.checkNotNullParameter(observable17, "o17");
        m.checkNotNullParameter(observable18, "o18");
        m.checkNotNullParameter(observable19, "o19");
        m.checkNotNullParameter(observable20, "o20");
        m.checkNotNullParameter(observable21, "o21");
        m.checkNotNullParameter(observable22, "o22");
        m.checkNotNullParameter(function22, "combineFunction");
        Observable<R> f = Observable.f(createHolderObservable(observable, observable2, observable3, observable4, observable5, observable6, observable7, observable8, observable9), createHolderObservable(observable10, observable11, observable12, observable13, observable14, observable15, observable16, observable17, observable18), observable19, observable20, observable21, observable22, new Func6<Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9>, Holder<T10, T11, T12, T13, T14, T15, T16, T17, T18>, T19, T20, T21, T22, R>() { // from class: com.discord.utilities.rx.ObservableCombineLatestOverloadsKt$combineLatest$8
            public final R call(Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9> holder, Holder<T10, T11, T12, T13, T14, T15, T16, T17, T18> holder2, T19 t19, T20 t20, T21 t21, T22 t22) {
                return (R) Function22.this.invoke(holder.getT1(), holder.getT2(), holder.getT3(), holder.getT4(), holder.getT5(), holder.getT6(), holder.getT7(), holder.getT8(), holder.getT9(), holder2.getT1(), holder2.getT2(), holder2.getT3(), holder2.getT4(), holder2.getT5(), holder2.getT6(), holder2.getT7(), holder2.getT8(), holder2.getT9(), t19, t20, t21, t22);
            }

            @Override // rx.functions.Func6
            public /* bridge */ /* synthetic */ Object call(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6) {
                return call((Holder) ((Holder) obj), (Holder) ((Holder) obj2), (Holder<T10, T11, T12, T13, T14, T15, T16, T17, T18>) obj3, obj4, obj5, obj6);
            }
        });
        m.checkNotNullExpressionValue(f, "Observable\n        .comb…22,\n          )\n        }");
        return f;
    }
}
