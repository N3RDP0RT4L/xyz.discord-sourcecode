package com.discord.utilities.rx;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import androidx.exifinterface.media.ExifInterface;
import d0.z.d.m;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
/* compiled from: OnDelayedEmissionHandler.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0005\u0018\u0000*\u0004\b\u0000\u0010\u00012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00000\u0002B9\u0012\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\r\u0012\b\b\u0002\u0010\u0019\u001a\u00020\u0018\u0012\b\b\u0002\u0010\b\u001a\u00020\u0007\u0012\b\b\u0002\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\u001b\u0010\u001cJ'\u0010\u0005\u001a\n\u0012\u0006\b\u0000\u0012\u00028\u00000\u00032\u000e\u0010\u0004\u001a\n\u0012\u0006\b\u0000\u0012\u00028\u00000\u0003H\u0016¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\"\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u0018\u0010\u0016\u001a\u0004\u0018\u00010\u00158\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0019\u001a\u00020\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010\u001a¨\u0006\u001d"}, d2 = {"Lcom/discord/utilities/rx/OnDelayedEmissionHandler;", ExifInterface.GPS_DIRECTION_TRUE, "Lrx/Observable$b;", "Lrx/Subscriber;", "subscriber", NotificationCompat.CATEGORY_CALL, "(Lrx/Subscriber;)Lrx/Subscriber;", "Ljava/util/concurrent/TimeUnit;", "unit", "Ljava/util/concurrent/TimeUnit;", "Lrx/Scheduler;", "scheduler", "Lrx/Scheduler;", "Lkotlin/Function1;", "", "", "onDelayCallback", "Lkotlin/jvm/functions/Function1;", "Ljava/util/concurrent/atomic/AtomicBoolean;", "hasFinished", "Ljava/util/concurrent/atomic/AtomicBoolean;", "Lrx/Subscription;", "delaySubscription", "Lrx/Subscription;", "", "timeout", "J", HookHelper.constructorName, "(Lkotlin/jvm/functions/Function1;JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class OnDelayedEmissionHandler<T> implements Observable.b<T, T> {
    private Subscription delaySubscription;
    private final AtomicBoolean hasFinished;
    private final Function1<Boolean, Unit> onDelayCallback;
    private final Scheduler scheduler;
    private final long timeout;
    private final TimeUnit unit;

    /* JADX WARN: Multi-variable type inference failed */
    public OnDelayedEmissionHandler(Function1<? super Boolean, Unit> function1, long j, TimeUnit timeUnit, Scheduler scheduler) {
        m.checkNotNullParameter(function1, "onDelayCallback");
        m.checkNotNullParameter(timeUnit, "unit");
        m.checkNotNullParameter(scheduler, "scheduler");
        this.onDelayCallback = function1;
        this.timeout = j;
        this.unit = timeUnit;
        this.scheduler = scheduler;
        this.hasFinished = new AtomicBoolean(false);
    }

    @Override // j0.k.b
    public /* bridge */ /* synthetic */ Object call(Object obj) {
        return call((Subscriber) ((Subscriber) obj));
    }

    public Subscriber<? super T> call(Subscriber<? super T> subscriber) {
        m.checkNotNullParameter(subscriber, "subscriber");
        OnDelayedEmissionHandler$call$wrappedSubscriber$1 onDelayedEmissionHandler$call$wrappedSubscriber$1 = new OnDelayedEmissionHandler$call$wrappedSubscriber$1(this, subscriber);
        Observable<Long> I = Observable.d0(this.timeout, this.unit).I(this.scheduler);
        m.checkNotNullExpressionValue(I, "Observable\n        .time…    .observeOn(scheduler)");
        ObservableExtensionsKt.appSubscribe(I, OnDelayedEmissionHandler.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new OnDelayedEmissionHandler$call$3(this), (r18 & 8) != 0 ? null : new OnDelayedEmissionHandler$call$2(onDelayedEmissionHandler$call$wrappedSubscriber$1), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new OnDelayedEmissionHandler$call$1(this));
        return onDelayedEmissionHandler$call$wrappedSubscriber$1;
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ OnDelayedEmissionHandler(kotlin.jvm.functions.Function1 r7, long r8, java.util.concurrent.TimeUnit r10, rx.Scheduler r11, int r12, kotlin.jvm.internal.DefaultConstructorMarker r13) {
        /*
            r6 = this;
            r13 = r12 & 2
            if (r13 == 0) goto L6
            r8 = 0
        L6:
            r2 = r8
            r8 = r12 & 4
            if (r8 == 0) goto Ld
            java.util.concurrent.TimeUnit r10 = java.util.concurrent.TimeUnit.MILLISECONDS
        Ld:
            r4 = r10
            r8 = r12 & 8
            if (r8 == 0) goto L1b
            rx.Scheduler r11 = j0.j.b.a.a()
            java.lang.String r8 = "AndroidSchedulers.mainThread()"
            d0.z.d.m.checkNotNullExpressionValue(r11, r8)
        L1b:
            r5 = r11
            r0 = r6
            r1 = r7
            r0.<init>(r1, r2, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.rx.OnDelayedEmissionHandler.<init>(kotlin.jvm.functions.Function1, long, java.util.concurrent.TimeUnit, rx.Scheduler, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }
}
