package com.discord.utilities.rx;

import androidx.core.app.NotificationCompat;
import kotlin.Metadata;
import rx.functions.Func9;
/* compiled from: ObservableCombineLatestOverloads.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\b\u0013\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0016\u001av\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u0003\u0012\u0004\u0012\u00028\u0004\u0012\u0004\u0012\u00028\u0005\u0012\u0004\u0012\u00028\u0006\u0012\u0004\u0012\u00028\u0007\u0012\u0004\u0012\u00028\b \t*:\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u0003\u0012\u0004\u0012\u00028\u0004\u0012\u0004\u0012\u00028\u0005\u0012\u0004\u0012\u00028\u0006\u0012\u0004\u0012\u00028\u0007\u0012\u0004\u0012\u00028\b\u0018\u00010\u00130\u0013\"\u0004\b\u0000\u0010\u0000\"\u0004\b\u0001\u0010\u0001\"\u0004\b\u0002\u0010\u0002\"\u0004\b\u0003\u0010\u0003\"\u0004\b\u0004\u0010\u0004\"\u0004\b\u0005\u0010\u0005\"\u0004\b\u0006\u0010\u0006\"\u0004\b\u0007\u0010\u0007\"\u0004\b\b\u0010\b2\u000e\u0010\n\u001a\n \t*\u0004\u0018\u00018\u00008\u00002\u000e\u0010\u000b\u001a\n \t*\u0004\u0018\u00018\u00018\u00012\u000e\u0010\f\u001a\n \t*\u0004\u0018\u00018\u00028\u00022\u000e\u0010\r\u001a\n \t*\u0004\u0018\u00018\u00038\u00032\u000e\u0010\u000e\u001a\n \t*\u0004\u0018\u00018\u00048\u00042\u000e\u0010\u000f\u001a\n \t*\u0004\u0018\u00018\u00058\u00052\u000e\u0010\u0010\u001a\n \t*\u0004\u0018\u00018\u00068\u00062\u000e\u0010\u0011\u001a\n \t*\u0004\u0018\u00018\u00078\u00072\u000e\u0010\u0012\u001a\n \t*\u0004\u0018\u00018\b8\bH\n¢\u0006\u0004\b\u0014\u0010\u0015"}, d2 = {"T1", "T2", "T3", "T4", "T5", "T6", "T7", "T8", "T9", "kotlin.jvm.PlatformType", "t1", "t2", "t3", "t4", "t5", "t6", "t7", "t8", "t9", "Lcom/discord/utilities/rx/Holder;", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/discord/utilities/rx/Holder;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ObservableCombineLatestOverloadsKt$createHolderObservable$1<T1, T2, T3, T4, T5, T6, T7, T8, T9, R> implements Func9<T1, T2, T3, T4, T5, T6, T7, T8, T9, Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9>> {
    public static final ObservableCombineLatestOverloadsKt$createHolderObservable$1 INSTANCE = new ObservableCombineLatestOverloadsKt$createHolderObservable$1();

    @Override // rx.functions.Func9
    public final Holder<T1, T2, T3, T4, T5, T6, T7, T8, T9> call(T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, T6 t6, T7 t7, T8 t8, T9 t9) {
        return new Holder<>(t1, t2, t3, t4, t5, t6, t7, t8, t9);
    }
}
