package com.discord.utilities.rx;

import androidx.exifinterface.media.ExifInterface;
import d0.k;
import d0.w.h.b;
import d0.w.h.c;
import d0.w.i.a.g;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.coroutines.Continuation;
import kotlinx.coroutines.CancellableContinuation;
import rx.Observable;
import rx.functions.Action1;
import s.a.c2.d;
import s.a.l;
/* compiled from: RxCoroutineUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a#\u0010\u0002\u001a\u00028\u0000\"\u0004\b\u0000\u0010\u0000*\b\u0012\u0004\u0012\u00028\u00000\u0001H\u0086@ø\u0001\u0000¢\u0006\u0004\b\u0002\u0010\u0003\u001a#\u0010\u0004\u001a\u00028\u0000\"\u0004\b\u0000\u0010\u0000*\b\u0012\u0004\u0012\u00028\u00000\u0001H\u0086@ø\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0003\u001a)\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005\"\u0004\b\u0000\u0010\u0000*\b\u0012\u0004\u0012\u00028\u00000\u0001H\u0086@ø\u0001\u0000¢\u0006\u0004\b\u0006\u0010\u0003\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0007"}, d2 = {ExifInterface.GPS_DIRECTION_TRUE, "Lrx/Observable;", "awaitSingle", "(Lrx/Observable;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "awaitFirst", "Ls/a/c2/d;", "toFlow", "utils_release"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class RxCoroutineUtilsKt {
    public static final <T> Object awaitFirst(Observable<T> observable, Continuation<? super T> continuation) {
        Observable<T> y2 = observable.y();
        m.checkNotNullExpressionValue(y2, "first()");
        return awaitSingle(y2, continuation);
    }

    public static final <T> Object awaitSingle(Observable<T> observable, Continuation<? super T> continuation) {
        final l lVar = new l(b.intercepted(continuation), 1);
        lVar.A();
        lVar.f(new RxCoroutineUtilsKt$awaitSingle$2$1(observable.R().W((Action1<T>) new Action1<T>() { // from class: com.discord.utilities.rx.RxCoroutineUtilsKt$awaitSingle$2$subscription$1
            @Override // rx.functions.Action1
            public final void call(T t) {
                CancellableContinuation cancellableContinuation = CancellableContinuation.this;
                k.a aVar = k.j;
                cancellableContinuation.resumeWith(k.m73constructorimpl(t));
            }
        }, new Action1<Throwable>() { // from class: com.discord.utilities.rx.RxCoroutineUtilsKt$awaitSingle$2$subscription$2
            public final void call(Throwable th) {
                CancellableContinuation.this.k(th);
            }
        })));
        Object u = lVar.u();
        if (u == c.getCOROUTINE_SUSPENDED()) {
            g.probeCoroutineSuspended(continuation);
        }
        return u;
    }

    public static final <T> Object toFlow(Observable<T> observable, Continuation<? super d<? extends T>> continuation) {
        return new s.a.c2.b(new RxCoroutineUtilsKt$toFlow$2(observable, null), null, 0, null, 14);
    }
}
