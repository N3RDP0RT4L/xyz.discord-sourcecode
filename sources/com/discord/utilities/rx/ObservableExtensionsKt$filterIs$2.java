package com.discord.utilities.rx;

import androidx.core.app.NotificationCompat;
import androidx.exifinterface.media.ExifInterface;
import d0.z.d.m;
import j0.k.b;
import kotlin.Metadata;
/* compiled from: ObservableExtensions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0005\u0010\u0006\u001a\n \u0002*\u0004\u0018\u00018\u00008\u0000\"\u0006\b\u0000\u0010\u0000\u0018\u00012\u000e\u0010\u0003\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {ExifInterface.GPS_DIRECTION_TRUE, "", "kotlin.jvm.PlatformType", "it", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Object;)Ljava/lang/Object;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ObservableExtensionsKt$filterIs$2<T, R> implements b<Object, T> {
    public static final ObservableExtensionsKt$filterIs$2 INSTANCE = new ObservableExtensionsKt$filterIs$2();

    /* JADX WARN: Multi-variable type inference failed */
    @Override // j0.k.b
    public final T call(Object obj) {
        m.reifiedOperationMarker(1, ExifInterface.GPS_DIRECTION_TRUE);
        return obj;
    }
}
