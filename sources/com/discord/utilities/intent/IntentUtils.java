package com.discord.utilities.intent;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.core.app.NotificationCompat;
import androidx.core.content.pm.ShortcutManagerCompat;
import b.a.d.l0.a;
import b.c.a.a0.d;
import b.i.c.c;
import b.i.c.k.b;
import com.adjust.sdk.Constants;
import com.discord.app.AppLog;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.restapi.RestAPIBuilder;
import com.discord.stores.StoreStream;
import com.discord.utilities.intent.RouteHandlers;
import com.discord.utilities.logging.Logger;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.google.firebase.appindexing.internal.zza;
import com.google.firebase.appindexing.internal.zzc;
import d0.g0.i;
import d0.g0.s;
import d0.g0.t;
import d0.o;
import d0.t.h0;
import d0.z.d.m;
import java.lang.ref.WeakReference;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function3;
import kotlin.text.MatchResult;
import kotlin.text.Regex;
import xyz.discord.R;
/* compiled from: IntentUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001/B\t\b\u0002¢\u0006\u0004\b-\u0010.J)\u0010\t\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0007\u001a\u00020\u0006H\u0007¢\u0006\u0004\b\t\u0010\nJ\u001b\u0010\r\u001a\n \f*\u0004\u0018\u00010\u000b0\u000b*\u00020\u000bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u001d\u0010\u0010\u001a\u00020\u000f*\u00020\u000f2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004H\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u001f\u0010\u0015\u001a\u00020\b2\u0006\u0010\u0012\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\u0015\u0010\u0016J9\u0010\u0019\u001a\u00020\u00132\u0006\u0010\u0012\u001a\u00020\u000f2\u0006\u0010\u0003\u001a\u00020\u00022\u001a\b\u0002\u0010\u0018\u001a\u0014\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\b0\u0017¢\u0006\u0004\b\u0019\u0010\u001aJ\u001d\u0010\u001b\u001a\u00020\u00132\u0006\u0010\u0012\u001a\u00020\u000f2\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ\u0011\u0010\u001d\u001a\u00020\u000f*\u00020\u000f¢\u0006\u0004\b\u001d\u0010\u001eJ\u0013\u0010 \u001a\u0004\u0018\u00010\u001f*\u00020\u000f¢\u0006\u0004\b \u0010!J\u0015\u0010#\u001a\u00020\u00132\u0006\u0010\"\u001a\u00020\u000b¢\u0006\u0004\b#\u0010$J\u0015\u0010%\u001a\u00020\u00132\u0006\u0010\"\u001a\u00020\u000b¢\u0006\u0004\b%\u0010$R<\u0010+\u001a(\u0012\u0004\u0012\u00020'\u0012\u001e\u0012\u001c\u0012\u0004\u0012\u00020\u000b\u0012\u0006\u0012\u0004\u0018\u00010)\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020*0(0&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b+\u0010,¨\u00060"}, d2 = {"Lcom/discord/utilities/intent/IntentUtils;", "", "Landroid/content/Context;", "context", "", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "", "chooserText", "", "performChooserSendIntent", "(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;)V", "Landroid/net/Uri;", "kotlin.jvm.PlatformType", "externalize", "(Landroid/net/Uri;)Landroid/net/Uri;", "Landroid/content/Intent;", "sendText", "(Landroid/content/Intent;Ljava/lang/String;)Landroid/content/Intent;", "intent", "", "isHandledSuccessfully", "notifyFirebaseUserActionStatus", "(Landroid/content/Intent;Z)V", "Lkotlin/Function2;", "callback", "consumeRoutingIntent", "(Landroid/content/Intent;Landroid/content/Context;Lkotlin/jvm/functions/Function2;)Z", "consumeExternalRoutingIntent", "(Landroid/content/Intent;Landroid/content/Context;)Z", "toExternalizedSend", "(Landroid/content/Intent;)Landroid/content/Intent;", "", "getDirectShareId", "(Landroid/content/Intent;)Ljava/lang/Long;", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "isHttpDomainUrl", "(Landroid/net/Uri;)Z", "isDiscordAppUri", "", "Lkotlin/text/Regex;", "Lkotlin/Function3;", "Lkotlin/text/MatchResult;", "Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;", "pathRouterMap", "Ljava/util/Map;", HookHelper.constructorName, "()V", "RouteBuilders", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class IntentUtils {
    public static final IntentUtils INSTANCE = new IntentUtils();
    private static final Map<Regex, Function3<Uri, MatchResult, Context, RouteHandlers.AnalyticsMetadata>> pathRouterMap;

    /* compiled from: IntentUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u000b\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002!\"B\t\b\u0002¢\u0006\u0004\b\u001f\u0010 J;\u0010\n\u001a\u00020\t2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010\u0006\u001a\u00060\u0002j\u0002`\u00052\u0010\b\u0002\u0010\b\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0007H\u0007¢\u0006\u0004\b\n\u0010\u000bJ\u001b\u0010\f\u001a\u00020\t2\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u0005H\u0007¢\u0006\u0004\b\f\u0010\rJ+\u0010\u0010\u001a\u00020\t2\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u00052\u000e\u0010\u000f\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u000eH\u0007¢\u0006\u0004\b\u0010\u0010\u0011J\u001b\u0010\u0014\u001a\u00020\t2\n\u0010\u0013\u001a\u00060\u0002j\u0002`\u0012H\u0007¢\u0006\u0004\b\u0014\u0010\rJ\u0019\u0010\u0016\u001a\u00020\t2\n\u0010\u0015\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0016\u0010\rJ\u0019\u0010\u0017\u001a\u00020\t2\n\u0010\u0013\u001a\u00060\u0002j\u0002`\u0012¢\u0006\u0004\b\u0017\u0010\rJ!\u0010\u001b\u001a\u00020\t2\b\u0010\u0019\u001a\u0004\u0018\u00010\u00182\b\u0010\u001a\u001a\u0004\u0018\u00010\u0018¢\u0006\u0004\b\u001b\u0010\u001cJ!\u0010\u001e\u001a\u00020\t2\b\u0010\u001d\u001a\u0004\u0018\u00010\u00182\b\u0010\u001a\u001a\u0004\u0018\u00010\u0018¢\u0006\u0004\b\u001e\u0010\u001c¨\u0006#"}, d2 = {"Lcom/discord/utilities/intent/IntentUtils$RouteBuilders;", "", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/MessageId;", "messageId", "Landroid/content/Intent;", "selectChannel", "(JJLjava/lang/Long;)Landroid/content/Intent;", "selectGuild", "(J)Landroid/content/Intent;", "Lcom/discord/primitives/GuildScheduledEventId;", "eventId", "selectExternalEvent", "(JLjava/lang/Long;)Landroid/content/Intent;", "Lcom/discord/primitives/UserId;", "userId", "selectUserProfile", "voiceChannelId", "connectVoice", "selectDirectMessage", "", "inviteText", "source", "selectInvite", "(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;", "guildTemplateText", "selectGuildTemplate", HookHelper.constructorName, "()V", "SDK", "Uris", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class RouteBuilders {
        public static final RouteBuilders INSTANCE = new RouteBuilders();

        /* compiled from: IntentUtils.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fJ-\u0010\t\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\b\u0010\u0007\u001a\u0004\u0018\u00010\u0002H\u0007¢\u0006\u0004\b\t\u0010\n¨\u0006\r"}, d2 = {"Lcom/discord/utilities/intent/IntentUtils$RouteBuilders$SDK;", "", "", Constants.DEEPLINK, "", "Lcom/discord/primitives/ApplicationId;", "applicationId", "secret", "Landroid/content/Intent;", "join", "(Ljava/lang/String;JLjava/lang/String;)Landroid/content/Intent;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class SDK {
            public static final SDK INSTANCE = new SDK();

            private SDK() {
            }

            public static final Intent join(String str, long j, String str2) {
                m.checkNotNullParameter(str, Constants.DEEPLINK);
                return new Intent("com.discord.intent.action.SDK", Uri.parse(str).buildUpon().appendPath("join").appendQueryParameter(ModelAuditLogEntry.CHANGE_KEY_APPLICATION_ID, String.valueOf(j)).appendQueryParameter("secret", str2).build());
            }
        }

        /* compiled from: IntentUtils.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\n\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bR\u0013\u0010\u0005\u001a\u00020\u00028F@\u0006¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004R\u0013\u0010\u0007\u001a\u00020\u00028F@\u0006¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0004R\u0013\u0010\t\u001a\u00020\u00028F@\u0006¢\u0006\u0006\u001a\u0004\b\b\u0010\u0004¨\u0006\f"}, d2 = {"Lcom/discord/utilities/intent/IntentUtils$RouteBuilders$Uris;", "", "Landroid/net/Uri;", "getOauth2Authorize", "()Landroid/net/Uri;", "oauth2Authorize", "getSelectSettingsVoice", "selectSettingsVoice", "getApp", "app", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Uris {
            public static final Uris INSTANCE = new Uris();

            private Uris() {
            }

            public final Uri getApp() {
                Uri parse = Uri.parse("discord://app");
                m.checkNotNullExpressionValue(parse, "Uri.parse(\"discord://app\")");
                return parse;
            }

            public final Uri getOauth2Authorize() {
                Uri parse = Uri.parse("discord://action/oauth2/authorize");
                m.checkNotNullExpressionValue(parse, "Uri.parse(\"discord://action/oauth2/authorize\")");
                return parse;
            }

            public final Uri getSelectSettingsVoice() {
                Uri parse = Uri.parse("discord://app/settings/voice");
                m.checkNotNullExpressionValue(parse, "Uri.parse(\"discord://app/settings/voice\")");
                return parse;
            }
        }

        private RouteBuilders() {
        }

        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r6v0, types: [java.lang.StringBuilder] */
        /* JADX WARN: Type inference failed for: r7v0, types: [java.lang.Long] */
        /* JADX WARN: Type inference failed for: r7v1, types: [java.lang.Object] */
        /* JADX WARN: Type inference failed for: r7v2, types: [java.lang.String] */
        public static final Intent selectChannel(long j, long j2, Long l) {
            String valueOf = (j2 == 0 || j2 == -1) ? "@me" : Long.valueOf(j2);
            if (l == 0) {
                l = "";
            }
            return new Intent("android.intent.action.VIEW", Uri.parse("discord://app/channels/" + valueOf + MentionUtilsKt.SLASH_CHAR + j + MentionUtilsKt.SLASH_CHAR + l));
        }

        public static /* synthetic */ Intent selectChannel$default(long j, long j2, Long l, int i, Object obj) {
            if ((i & 2) != 0) {
                j2 = 0;
            }
            if ((i & 4) != 0) {
                l = null;
            }
            return selectChannel(j, j2, l);
        }

        public static final Intent selectExternalEvent(long j, Long l) {
            String valueOf = (j == 0 || j == -1) ? "@me" : Long.valueOf(j);
            return new Intent("android.intent.action.VIEW", Uri.parse("discord://app/events/" + valueOf + MentionUtilsKt.SLASH_CHAR + l));
        }

        public static final Intent selectGuild(long j) {
            String valueOf = (j == 0 || j == -1) ? "@me" : Long.valueOf(j);
            return new Intent("android.intent.action.VIEW", Uri.parse("discord://app/channels/" + valueOf));
        }

        public static final Intent selectUserProfile(long j) {
            return new Intent("android.intent.action.VIEW", Uri.parse("discord://app/users/" + j));
        }

        public final Intent connectVoice(long j) {
            return new Intent("com.discord.intent.action.CONNECT", Uri.parse("discord://app/connect/" + j));
        }

        public final Intent selectDirectMessage(long j) {
            return new Intent("android.intent.action.VIEW", Uri.parse("discord://app/channels/@me/user/" + j));
        }

        public final Intent selectGuildTemplate(String str, String str2) {
            Uri parse = Uri.parse(str);
            m.checkNotNullExpressionValue(parse, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
            if (parse.getScheme() == null) {
                if (str != null) {
                    a aVar = a.F;
                    if (t.startsWith$default(str, a.e, false, 2, null)) {
                        parse = Uri.parse("https://" + str);
                    }
                }
                parse = Uri.parse("discord://app/template/" + str + "?source=" + str2);
            }
            Intent data = new Intent().setData(parse);
            m.checkNotNullExpressionValue(data, "Intent().setData(uriMerged)");
            return data;
        }

        public final Intent selectInvite(String str, String str2) {
            Uri parse = Uri.parse(str);
            m.checkNotNullExpressionValue(parse, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
            if (parse.getScheme() == null) {
                if (str != null) {
                    a aVar = a.F;
                    if (t.startsWith$default(str, a.d, false, 2, null)) {
                        parse = Uri.parse("https://" + str);
                    }
                }
                parse = Uri.parse("discord://app/invite/" + str + "?source=" + str2);
            }
            Intent data = new Intent().setData(parse);
            m.checkNotNullExpressionValue(data, "Intent().setData(uriMerged)");
            return data;
        }
    }

    static {
        a aVar = a.F;
        Regex regex = a.E;
        RouteHandlers routeHandlers = RouteHandlers.INSTANCE;
        pathRouterMap = h0.mapOf(o.to(a.t, IntentUtils$pathRouterMap$1.INSTANCE), o.to(regex, new IntentUtils$pathRouterMap$2(routeHandlers)), o.to(a.u, new IntentUtils$pathRouterMap$3(routeHandlers)), o.to(a.w, new IntentUtils$pathRouterMap$4(routeHandlers)), o.to(a.f62x, new IntentUtils$pathRouterMap$5(routeHandlers)), o.to(a.f63y, new IntentUtils$pathRouterMap$6(routeHandlers)), o.to(a.f64z, new IntentUtils$pathRouterMap$7(routeHandlers)), o.to(a.f61s, new IntentUtils$pathRouterMap$8(routeHandlers)), o.to(a.v, new IntentUtils$pathRouterMap$9(routeHandlers)), o.to(a.B, new IntentUtils$pathRouterMap$10(routeHandlers)), o.to(a.C, new IntentUtils$pathRouterMap$11(routeHandlers)), o.to(a.D, new IntentUtils$pathRouterMap$12(routeHandlers)), o.to(a.A, new IntentUtils$pathRouterMap$13(routeHandlers)), o.to(a.m, new IntentUtils$pathRouterMap$14(routeHandlers)));
    }

    private IntentUtils() {
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ boolean consumeRoutingIntent$default(IntentUtils intentUtils, Intent intent, Context context, Function2 function2, int i, Object obj) {
        if ((i & 4) != 0) {
            function2 = IntentUtils$consumeRoutingIntent$1.INSTANCE;
        }
        return intentUtils.consumeRoutingIntent(intent, context, function2);
    }

    private final Uri externalize(Uri uri) {
        Uri.Builder scheme = uri.buildUpon().scheme(Constants.SCHEME);
        a aVar = a.F;
        return scheme.authority(a.a).build();
    }

    private final void notifyFirebaseUserActionStatus(Intent intent, boolean z2) {
        String stringExtra = intent.getStringExtra("actions.fulfillment.extra.ACTION_TOKEN");
        if (stringExtra != null) {
            m.checkNotNullExpressionValue(stringExtra, "intent.getStringExtra(In…A_VOICE_ACTION) ?: return");
            String str = z2 ? "http://schema.org/CompletedActionStatus" : "http://schema.org/FailedActionStatus";
            b.i.c.k.c.a aVar = new b.i.c.k.c.a();
            aVar.f = stringExtra;
            aVar.e = str;
            d.z(stringExtra, "setActionToken is required before calling build().");
            d.z(new String(aVar.e), "setActionStatus is required before calling build().");
            aVar.a("actionToken", aVar.f);
            b bVar = null;
            if ((aVar.c == null ? null : new String(aVar.c)) == null) {
                aVar.c = "AssistAction";
                aVar.a(ModelAuditLogEntry.CHANGE_KEY_NAME, "AssistAction");
            }
            if ((aVar.d == null ? null : new String(aVar.d)) == null) {
                String valueOf = String.valueOf(aVar.f);
                String concat = valueOf.length() != 0 ? "https://developers.google.com/actions?invocation=".concat(valueOf) : new String("https://developers.google.com/actions?invocation=");
                Objects.requireNonNull(concat, "null reference");
                aVar.d = concat;
                aVar.a("url", concat);
            }
            d.z(aVar.c, "setObject is required before calling build().");
            d.z(aVar.d, "setObject is required before calling build().");
            zza zzaVar = new zza(aVar.f1656b, aVar.c, aVar.d, null, new zzc(true), aVar.e, aVar.a);
            synchronized (b.class) {
                WeakReference<b> weakReference = b.a;
                if (weakReference != null) {
                    bVar = weakReference.get();
                }
                if (bVar == null) {
                    c b2 = c.b();
                    b2.a();
                    bVar = new b.i.c.k.d.b(b2.d);
                    b.a = new WeakReference<>(bVar);
                }
            }
            bVar.a(zzaVar);
        }
    }

    public static final void performChooserSendIntent(Context context, String str) {
        performChooserSendIntent$default(context, str, null, 4, null);
    }

    public static final void performChooserSendIntent(Context context, String str, CharSequence charSequence) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(str, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
        m.checkNotNullParameter(charSequence, "chooserText");
        context.startActivity(Intent.createChooser(INSTANCE.sendText(new Intent("android.intent.action.SEND"), str), charSequence));
    }

    public static /* synthetic */ void performChooserSendIntent$default(Context context, String str, CharSequence charSequence, int i, Object obj) {
        if ((i & 4) != 0) {
            charSequence = context.getString(R.string.share);
            m.checkNotNullExpressionValue(charSequence, "context.getString(R.string.share)");
        }
        performChooserSendIntent(context, str, charSequence);
    }

    private final Intent sendText(Intent intent, String str) {
        intent.setAction("android.intent.action.SEND");
        intent.setType(RestAPIBuilder.CONTENT_TYPE_TEXT);
        if (str == null) {
            str = "";
        }
        intent.putExtra("android.intent.extra.TEXT", str);
        return intent;
    }

    public final boolean consumeExternalRoutingIntent(Intent intent, Context context) {
        m.checkNotNullParameter(intent, "intent");
        m.checkNotNullParameter(context, "context");
        StoreStream.Companion.getDynamicLinkCache().storeLinkIfExists(intent, context);
        return consumeRoutingIntent(intent, context, new IntentUtils$consumeExternalRoutingIntent$1(intent));
    }

    public final boolean consumeRoutingIntent(Intent intent, Context context, Function2<? super Uri, ? super Boolean, Unit> function2) {
        MatchResult matchResult;
        RouteHandlers.AnalyticsMetadata analyticsMetadata;
        m.checkNotNullParameter(intent, "intent");
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(function2, "callback");
        Uri data = intent.getData();
        if (data == null) {
            data = Uri.EMPTY;
        }
        m.checkNotNullExpressionValue(data, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        boolean z2 = isDiscordAppUri(data) || isHttpDomainUrl(data);
        function2.invoke(data, Boolean.valueOf(z2));
        if (z2) {
            AppLog appLog = AppLog.g;
            String simpleName = IntentUtils.class.getSimpleName();
            m.checkNotNullExpressionValue(simpleName, "javaClass.simpleName");
            String uri = data.toString();
            if (uri == null) {
                uri = "<null>";
            }
            m.checkNotNullExpressionValue(uri, "uri?.toString() ?: \"<null>\"");
            appLog.f(simpleName, uri);
            for (Map.Entry<Regex, Function3<Uri, MatchResult, Context, RouteHandlers.AnalyticsMetadata>> entry : pathRouterMap.entrySet()) {
                Regex key = entry.getKey();
                Function3<Uri, MatchResult, Context, RouteHandlers.AnalyticsMetadata> value = entry.getValue();
                String path = data.getPath();
                if (path != null) {
                    m.checkNotNullExpressionValue(path, "it");
                    matchResult = key.matchEntire(path);
                    continue;
                } else {
                    matchResult = null;
                    continue;
                }
                if (matchResult != null) {
                    try {
                        analyticsMetadata = value.invoke(data, matchResult, context);
                    } catch (Exception unused) {
                        analyticsMetadata = RouteHandlers.AnalyticsMetadata.Companion.getUNKNOWN();
                    }
                    intent.setData(Uri.EMPTY);
                    boolean z3 = !m.areEqual(analyticsMetadata, RouteHandlers.AnalyticsMetadata.Companion.getUNKNOWN());
                    Logger.d$default(AppLog.g, "Intent handler activated for " + data + ", consumed: " + z3, null, 2, null);
                    notifyFirebaseUserActionStatus(intent, z3);
                    StoreStream.Companion.getAnalytics().deepLinkReceived(intent, analyticsMetadata);
                    return z3;
                }
            }
        }
        notifyFirebaseUserActionStatus(intent, false);
        return false;
    }

    public final Long getDirectShareId(Intent intent) {
        m.checkNotNullParameter(intent, "$this$getDirectShareId");
        String stringExtra = intent.getStringExtra(ShortcutManagerCompat.EXTRA_SHORTCUT_ID);
        if (stringExtra != null) {
            return s.toLongOrNull(stringExtra);
        }
        return null;
    }

    public final boolean isDiscordAppUri(Uri uri) {
        String str;
        int hashCode;
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        if (t.equals(uri.getScheme(), "discord", true)) {
            String host = uri.getHost();
            if (host != null) {
                Locale locale = Locale.ENGLISH;
                m.checkNotNullExpressionValue(locale, "Locale.ENGLISH");
                str = host.toLowerCase(locale);
                m.checkNotNullExpressionValue(str, "(this as java.lang.String).toLowerCase(locale)");
            } else {
                str = null;
            }
            if (str != null && ((hashCode = str.hashCode()) == -1422950858 ? str.equals("action") : !(hashCode != 96801 || !str.equals("app")))) {
                return true;
            }
        }
        return false;
    }

    public final boolean isHttpDomainUrl(Uri uri) {
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        Regex regex = new Regex("https?", i.IGNORE_CASE);
        String scheme = uri.getScheme();
        if (scheme == null) {
            scheme = "";
        }
        m.checkNotNullExpressionValue(scheme, "uri.scheme ?: \"\"");
        if (regex.matches(scheme)) {
            return a.F.a(uri.getHost());
        }
        return false;
    }

    public final Intent toExternalizedSend(Intent intent) {
        Uri uri;
        m.checkNotNullParameter(intent, "$this$toExternalizedSend");
        Uri data = intent.getData();
        if (data == null || (uri = INSTANCE.externalize(data)) == null) {
            uri = Uri.EMPTY;
        }
        intent.setData(uri);
        IntentUtils intentUtils = INSTANCE;
        Uri data2 = intent.getData();
        intentUtils.sendText(intent, data2 != null ? data2.toString() : null);
        return intent;
    }
}
