package com.discord.utilities.intent;

import andhook.lib.HookHelper;
import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.BuildConfig;
import com.discord.api.channel.Channel;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreAuthentication;
import com.discord.stores.StoreStream;
import com.discord.utilities.channel.ChannelSelector;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$1;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$2;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsBottomSheet;
import com.discord.widgets.media.WidgetQRScanner;
import com.discord.widgets.servers.guildboost.WidgetGuildBoost;
import com.discord.widgets.tabs.NavigationTab;
import com.discord.widgets.user.usersheet.WidgetUserSheet;
import d0.g0.s;
import d0.g0.w;
import d0.o;
import d0.t.h0;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KFunction;
import kotlin.text.MatchResult;
import rx.Observable;
/* compiled from: RouteHandlers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0016\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001\u001dB\t\b\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ)\u0010\t\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\t\u0010\nJ)\u0010\u000b\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\u000b\u0010\nJ)\u0010\f\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\f\u0010\nJ'\u0010\r\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\r\u0010\nJ'\u0010\u000e\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\u000e\u0010\nJ'\u0010\u000f\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\u000f\u0010\nJ'\u0010\u0010\u001a\u00020\b2\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004¢\u0006\u0004\b\u0010\u0010\u0011J'\u0010\u0012\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\u0012\u0010\nJ'\u0010\u0013\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\u0013\u0010\nJ'\u0010\u0014\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\u0014\u0010\nJ'\u0010\u0015\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\u0015\u0010\nJ'\u0010\u0016\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\u0016\u0010\nJ'\u0010\u0017\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\u0017\u0010\nJ'\u0010\u0018\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\u0018\u0010\nJ'\u0010\u0019\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\u0019\u0010\nJ'\u0010\u001a\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\u001a\u0010\n¨\u0006\u001e"}, d2 = {"Lcom/discord/utilities/intent/RouteHandlers;", "", "Landroid/net/Uri;", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "Lkotlin/text/MatchResult;", "matchResult", "Landroid/content/Context;", "context", "Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;", "acceptGift", "(Landroid/net/Uri;Lkotlin/text/MatchResult;Landroid/content/Context;)Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;", "useInvite", "useGuildTemplate", "selectFeature", "handleQuery", "wildCardMatcher", "selectChannel", "(Landroid/content/Context;Landroid/net/Uri;Lkotlin/text/MatchResult;)Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;", "navigateToFriendsScreen", "selectLurk", "voiceConnect", "selectDirectMessage", "selectUserProfile", "openEventDetails", "authorize", "remoteAuth", "selectPremiumGuild", HookHelper.constructorName, "()V", "AnalyticsMetadata", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class RouteHandlers {
    public static final RouteHandlers INSTANCE = new RouteHandlers();

    /* compiled from: RouteHandlers.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 \u00122\u00020\u0001:\u0001\u0012B3\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0010\b\u0002\u0010\u000e\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\r\u0012\u0010\b\u0002\u0010\t\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\b¢\u0006\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R!\u0010\t\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\b8\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\fR!\u0010\u000e\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\n\u001a\u0004\b\u000f\u0010\f¨\u0006\u0013"}, d2 = {"Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;", "", "", "type", "Ljava/lang/String;", "getType", "()Ljava/lang/String;", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Ljava/lang/Long;", "getChannelId", "()Ljava/lang/Long;", "Lcom/discord/primitives/GuildId;", "guildId", "getGuildId", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class AnalyticsMetadata {
        public static final Companion Companion = new Companion(null);
        private static final AnalyticsMetadata UNKNOWN = new AnalyticsMetadata("unknown", null, null, 6, null);
        private final Long channelId;
        private final Long guildId;
        private final String type;

        /* compiled from: RouteHandlers.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata$Companion;", "", "Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;", "UNKNOWN", "Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;", "getUNKNOWN", "()Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final AnalyticsMetadata getUNKNOWN() {
                return AnalyticsMetadata.UNKNOWN;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public AnalyticsMetadata(String str, Long l, Long l2) {
            m.checkNotNullParameter(str, "type");
            this.type = str;
            this.guildId = l;
            this.channelId = l2;
        }

        public final Long getChannelId() {
            return this.channelId;
        }

        public final Long getGuildId() {
            return this.guildId;
        }

        public final String getType() {
            return this.type;
        }

        public /* synthetic */ AnalyticsMetadata(String str, Long l, Long l2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(str, (i & 2) != 0 ? null : l, (i & 4) != 0 ? null : l2);
        }
    }

    private RouteHandlers() {
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Code restructure failed: missing block: B:13:0x0027, code lost:
        if (r11 != null) goto L15;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final com.discord.utilities.intent.RouteHandlers.AnalyticsMetadata acceptGift(android.net.Uri r9, kotlin.text.MatchResult r10, android.content.Context r11) {
        /*
            r8 = this;
            r11 = 0
            if (r10 == 0) goto L10
            java.util.List r10 = r10.getGroupValues()
            if (r10 == 0) goto L10
            java.lang.Object r10 = d0.t.u.last(r10)
            java.lang.String r10 = (java.lang.String) r10
            goto L11
        L10:
            r10 = r11
        L11:
            java.lang.String r0 = "source"
            java.lang.String r9 = r9.getQueryParameter(r0)
            r0 = 1
            if (r9 == 0) goto L2a
            java.lang.String r1 = "it"
            d0.z.d.m.checkNotNullExpressionValue(r9, r1)
            boolean r1 = d0.g0.t.isBlank(r9)
            r1 = r1 ^ r0
            if (r1 == 0) goto L27
            r11 = r9
        L27:
            if (r11 == 0) goto L2a
            goto L2c
        L2a:
            java.lang.String r11 = "Deeplink"
        L2c:
            java.lang.String r9 = "uri.getQueryParameter(\"s…ettings.LOCATION_DEEPLINK"
            d0.z.d.m.checkNotNullExpressionValue(r11, r9)
            if (r10 == 0) goto L41
            boolean r9 = d0.g0.t.isBlank(r10)
            r9 = r9 ^ r0
            if (r9 != r0) goto L41
            b.a.a.z.c$a r9 = b.a.a.z.c.k
            r0 = 0
            r9.a(r10, r11, r0)
        L41:
            com.discord.utilities.intent.RouteHandlers$AnalyticsMetadata r9 = new com.discord.utilities.intent.RouteHandlers$AnalyticsMetadata
            r4 = 0
            r5 = 0
            r6 = 6
            r7 = 0
            java.lang.String r3 = "gift"
            r2 = r9
            r2.<init>(r3, r4, r5, r6, r7)
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.intent.RouteHandlers.acceptGift(android.net.Uri, kotlin.text.MatchResult, android.content.Context):com.discord.utilities.intent.RouteHandlers$AnalyticsMetadata");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final AnalyticsMetadata useGuildTemplate(Uri uri, MatchResult matchResult, Context context) {
        List<String> groupValues;
        String str = (matchResult == null || (groupValues = matchResult.getGroupValues()) == null) ? null : (String) u.last((List<? extends Object>) groupValues);
        if (str != null) {
            StoreStream.Companion.getGuildTemplates().setDynamicLinkGuildTemplateCode(str);
        }
        return new AnalyticsMetadata("guild_template", null, null, 6, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Code restructure failed: missing block: B:16:0x0036, code lost:
        if (r2 != null) goto L18;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final com.discord.utilities.intent.RouteHandlers.AnalyticsMetadata useInvite(android.net.Uri r9, kotlin.text.MatchResult r10, android.content.Context r11) {
        /*
            r8 = this;
            r0 = 0
            if (r10 == 0) goto L10
            java.util.List r10 = r10.getGroupValues()
            if (r10 == 0) goto L10
            java.lang.Object r10 = d0.t.u.last(r10)
            java.lang.String r10 = (java.lang.String) r10
            goto L11
        L10:
            r10 = r0
        L11:
            java.lang.String r1 = "invite"
            boolean r1 = d0.z.d.m.areEqual(r10, r1)
            r1 = r1 ^ 1
            if (r1 == 0) goto L1c
            goto L1d
        L1c:
            r10 = r0
        L1d:
            java.lang.String r1 = "Deeplink"
            java.lang.String r2 = "source"
            java.lang.String r2 = r9.getQueryParameter(r2)
            if (r2 == 0) goto L39
            java.lang.String r3 = "it"
            d0.z.d.m.checkNotNullExpressionValue(r2, r3)
            boolean r3 = d0.g0.t.isBlank(r2)
            r3 = r3 ^ 1
            if (r3 == 0) goto L35
            goto L36
        L35:
            r2 = r0
        L36:
            if (r2 == 0) goto L39
            goto L3a
        L39:
            r2 = r1
        L3a:
            java.lang.String r3 = "uri.getQueryParameter(\"s…        ?: inviteLocation"
            d0.z.d.m.checkNotNullExpressionValue(r2, r3)
            java.lang.String r3 = "event"
            java.lang.String r9 = r9.getQueryParameter(r3)
            if (r9 == 0) goto L4d
            com.discord.utilities.SnowflakeUtils r0 = com.discord.utilities.SnowflakeUtils.INSTANCE
            java.lang.Long r0 = r0.toSnowflake(r9)
        L4d:
            if (r10 == 0) goto L59
            com.discord.stores.StoreStream$Companion r9 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreInviteSettings r9 = r9.getInviteSettings()
            r9.setInviteCode(r10, r2, r0)
            goto L5e
        L59:
            com.discord.widgets.guilds.join.WidgetGuildJoin$Companion r9 = com.discord.widgets.guilds.join.WidgetGuildJoin.Companion
            r9.show(r11, r1)
        L5e:
            com.discord.utilities.intent.RouteHandlers$AnalyticsMetadata r9 = new com.discord.utilities.intent.RouteHandlers$AnalyticsMetadata
            r4 = 0
            r5 = 0
            r6 = 6
            r7 = 0
            java.lang.String r3 = "invite"
            r2 = r9
            r2.<init>(r3, r4, r5, r6, r7)
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.intent.RouteHandlers.useInvite(android.net.Uri, kotlin.text.MatchResult, android.content.Context):com.discord.utilities.intent.RouteHandlers$AnalyticsMetadata");
    }

    public final AnalyticsMetadata authorize(Uri uri, MatchResult matchResult, Context context) {
        boolean z2;
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(context, "context");
        if (!(context instanceof Activity) || Build.VERSION.SDK_INT < 22) {
            z2 = false;
        } else {
            Uri referrer = ((Activity) context).getReferrer();
            z2 = m.areEqual(referrer != null ? referrer.getAuthority() : null, BuildConfig.APPLICATION_ID);
        }
        Uri build = uri.buildUpon().appendQueryParameter("internal_referrer", String.valueOf(z2)).build();
        StoreAuthentication authentication = StoreStream.Companion.getAuthentication();
        m.checkNotNullExpressionValue(build, "oAuthUri");
        authentication.setOAuthUriSubject(build);
        return new AnalyticsMetadata("oauth2", null, null, 6, null);
    }

    public final AnalyticsMetadata handleQuery(Uri uri, MatchResult matchResult, Context context) {
        String str;
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(context, "context");
        String queryParameter = uri.getQueryParameter("type");
        String queryParameter2 = uri.getQueryParameter("channelName");
        String str2 = "";
        String str3 = queryParameter2 != null ? queryParameter2 : str2;
        m.checkNotNullExpressionValue(str3, "uri.getQueryParameter(\"channelName\") ?: \"\"");
        String queryParameter3 = uri.getQueryParameter("serverName");
        if (queryParameter3 == null) {
            queryParameter3 = str2;
        }
        m.checkNotNullExpressionValue(queryParameter3, "uri.getQueryParameter(\"serverName\") ?: \"\"");
        String queryParameter4 = uri.getQueryParameter("userName");
        if (queryParameter4 == null) {
            queryParameter4 = str2;
        }
        m.checkNotNullExpressionValue(queryParameter4, "uri.getQueryParameter(\"userName\") ?: \"\"");
        String queryParameter5 = uri.getQueryParameter("userNameAlt");
        if (queryParameter5 == null) {
            queryParameter5 = str2;
        }
        m.checkNotNullExpressionValue(queryParameter5, "uri.getQueryParameter(\"userNameAlt\") ?: \"\"");
        String queryParameter6 = uri.getQueryParameter("messageText");
        if (queryParameter6 == null) {
            queryParameter6 = str2;
        }
        m.checkNotNullExpressionValue(queryParameter6, "uri.getQueryParameter(\"messageText\") ?: \"\"");
        if (queryParameter != null) {
            int hashCode = queryParameter.hashCode();
            if (hashCode != -905826493) {
                boolean z2 = true;
                if (hashCode != 3599307) {
                    if (hashCode == 738950403 && queryParameter.equals("channel")) {
                        List split$default = w.split$default((CharSequence) str3, new String[]{" in "}, false, 0, 6, (Object) null);
                        String str4 = (String) (n.getLastIndex(split$default) >= 0 ? split$default.get(0) : str2);
                        String str5 = (String) (1 <= n.getLastIndex(split$default) ? split$default.get(1) : str2);
                        if (str5.length() <= 0) {
                            z2 = false;
                        }
                        if (z2) {
                            str2 = ' ' + str5;
                        }
                        str = MentionUtilsKt.CHANNELS_CHAR + str4 + str2;
                        StoreStream.Companion.getNavigation().launchNotice("DEEPLINK_QUERY", new RouteHandlers$handleQuery$1(queryParameter6, str));
                        return new AnalyticsMetadata("query", null, null, 6, null);
                    }
                } else if (queryParameter.equals("user")) {
                    if (!(queryParameter4.length() > 0)) {
                        queryParameter4 = queryParameter5;
                    }
                    if (queryParameter4.length() != 0) {
                        z2 = false;
                    }
                    if (z2) {
                        return AnalyticsMetadata.Companion.getUNKNOWN();
                    }
                    str = MentionUtilsKt.MENTIONS_CHAR + queryParameter4;
                    StoreStream.Companion.getNavigation().launchNotice("DEEPLINK_QUERY", new RouteHandlers$handleQuery$1(queryParameter6, str));
                    return new AnalyticsMetadata("query", null, null, 6, null);
                }
            } else if (queryParameter.equals("server")) {
                str = '*' + queryParameter3;
                StoreStream.Companion.getNavigation().launchNotice("DEEPLINK_QUERY", new RouteHandlers$handleQuery$1(queryParameter6, str));
                return new AnalyticsMetadata("query", null, null, 6, null);
            }
        }
        return AnalyticsMetadata.Companion.getUNKNOWN();
    }

    public final AnalyticsMetadata navigateToFriendsScreen(Uri uri, MatchResult matchResult, Context context) {
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(context, "context");
        StoreStream.Companion.getTabsNavigation().selectTab(NavigationTab.FRIENDS, true);
        return new AnalyticsMetadata("friends", null, null, 6, null);
    }

    public final AnalyticsMetadata openEventDetails(Uri uri, MatchResult matchResult, Context context) {
        List<String> groupValues;
        String str;
        List<String> groupValues2;
        String str2;
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(context, "context");
        long j = 0;
        long parseLong = (matchResult == null || (groupValues2 = matchResult.getGroupValues()) == null || (str2 = groupValues2.get(1)) == null) ? 0L : Long.parseLong(str2);
        if (!(matchResult == null || (groupValues = matchResult.getGroupValues()) == null || (str = groupValues.get(2)) == null)) {
            j = Long.parseLong(str);
        }
        StoreStream.Companion companion = StoreStream.Companion;
        Guild guild = companion.getGuilds().getGuild(parseLong);
        Channel channel = guild == null ? companion.getChannels().getChannel(parseLong) : null;
        if (guild == null && channel == null) {
            WidgetGuildScheduledEventDetailsBottomSheet.Companion.handleInvalidEvent(context);
        } else {
            WidgetGuildScheduledEventDetailsBottomSheet.Companion.enqueue(j);
            if (guild == null) {
                guild = channel != null ? companion.getGuilds().getGuild(channel.f()) : null;
            }
            if (guild != null) {
                companion.getGuildSelected().set(guild.getId());
            }
        }
        return new AnalyticsMetadata("event", null, null, 6, null);
    }

    public final AnalyticsMetadata remoteAuth(Uri uri, MatchResult matchResult, Context context) {
        List<String> groupValues;
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(context, "context");
        if (((matchResult == null || (groupValues = matchResult.getGroupValues()) == null) ? null : (String) u.last((List<? extends Object>) groupValues)) != null) {
            WidgetQRScanner.Companion.launch(context, true);
        }
        return new AnalyticsMetadata("remoteAuth", null, null, 6, null);
    }

    /* JADX WARN: Removed duplicated region for block: B:42:0x0093  */
    /* JADX WARN: Removed duplicated region for block: B:43:0x00a2  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final com.discord.utilities.intent.RouteHandlers.AnalyticsMetadata selectChannel(android.content.Context r11, android.net.Uri r12, kotlin.text.MatchResult r13) {
        /*
            r10 = this;
            java.lang.String r0 = "context"
            d0.z.d.m.checkNotNullParameter(r11, r0)
            java.lang.String r0 = "uri"
            d0.z.d.m.checkNotNullParameter(r12, r0)
            r12 = 1
            r0 = 0
            if (r13 == 0) goto L35
            java.util.List r2 = r13.getGroupValues()
            if (r2 == 0) goto L35
            java.lang.Object r2 = d0.t.u.getOrNull(r2, r12)
            java.lang.String r2 = (java.lang.String) r2
            if (r2 == 0) goto L35
            java.lang.String r3 = "@me"
            boolean r3 = d0.z.d.m.areEqual(r2, r3)
            if (r3 == 0) goto L2a
            java.lang.Long r2 = java.lang.Long.valueOf(r0)
            goto L2e
        L2a:
            java.lang.Long r2 = d0.g0.s.toLongOrNull(r2)
        L2e:
            if (r2 == 0) goto L35
            long r2 = r2.longValue()
            goto L36
        L35:
            r2 = r0
        L36:
            if (r13 == 0) goto L52
            java.util.List r4 = r13.getGroupValues()
            if (r4 == 0) goto L52
            r5 = 2
            java.lang.Object r4 = d0.t.u.getOrNull(r4, r5)
            java.lang.String r4 = (java.lang.String) r4
            if (r4 == 0) goto L52
            java.lang.Long r4 = d0.g0.s.toLongOrNull(r4)
            if (r4 == 0) goto L52
            long r4 = r4.longValue()
            goto L53
        L52:
            r4 = r0
        L53:
            if (r13 == 0) goto L6f
            java.util.List r13 = r13.getGroupValues()
            if (r13 == 0) goto L6f
            r6 = 3
            java.lang.Object r13 = d0.t.u.getOrNull(r13, r6)
            java.lang.String r13 = (java.lang.String) r13
            if (r13 == 0) goto L6f
            java.lang.Long r13 = d0.g0.s.toLongOrNull(r13)
            if (r13 == 0) goto L6f
            long r6 = r13.longValue()
            goto L70
        L6f:
            r6 = r0
        L70:
            com.discord.stores.StoreStream$Companion r13 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreTabsNavigation r8 = r13.getTabsNavigation()
            int r9 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            if (r9 != 0) goto L8f
            int r9 = (r6 > r0 ? 1 : (r6 == r0 ? 0 : -1))
            if (r9 != 0) goto L8f
            int r9 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r9 == 0) goto L8f
            com.discord.stores.StoreGuildSelected r11 = r13.getGuildSelected()
            r11.set(r2)
            com.discord.stores.StoreNavigation$PanelAction r11 = com.discord.stores.StoreNavigation.PanelAction.CLOSE
            r8.selectHomeTab(r11, r12)
            goto La9
        L8f:
            int r9 = (r6 > r0 ? 1 : (r6 == r0 ? 0 : -1))
            if (r9 != 0) goto La2
            com.discord.utilities.channel.ChannelSelector$Companion r13 = com.discord.utilities.channel.ChannelSelector.Companion
            com.discord.utilities.channel.ChannelSelector r13 = r13.getInstance()
            r13.findAndSet(r11, r4)
            com.discord.stores.StoreNavigation$PanelAction r11 = com.discord.stores.StoreNavigation.PanelAction.CLOSE
            r8.selectHomeTab(r11, r12)
            goto La9
        La2:
            com.discord.stores.StoreMessagesLoader r11 = r13.getMessagesLoader()
            r11.jumpToMessage(r4, r6)
        La9:
            com.discord.utilities.intent.RouteHandlers$AnalyticsMetadata r11 = new com.discord.utilities.intent.RouteHandlers$AnalyticsMetadata
            java.lang.Long r12 = java.lang.Long.valueOf(r2)
            java.lang.Long r13 = java.lang.Long.valueOf(r4)
            java.lang.String r0 = "channel"
            r11.<init>(r0, r12, r13)
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.intent.RouteHandlers.selectChannel(android.content.Context, android.net.Uri, kotlin.text.MatchResult):com.discord.utilities.intent.RouteHandlers$AnalyticsMetadata");
    }

    public final AnalyticsMetadata selectDirectMessage(Uri uri, MatchResult matchResult, Context context) {
        List<String> groupValues;
        String str;
        Long longOrNull;
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(context, "context");
        if (!(matchResult == null || (groupValues = matchResult.getGroupValues()) == null || (str = (String) u.last((List<? extends Object>) groupValues)) == null || (longOrNull = s.toLongOrNull(str)) == null)) {
            ChannelSelector.Companion.getInstance().findAndSetDirectMessage(context, longOrNull.longValue());
        }
        return new AnalyticsMetadata("DM", null, null, 6, null);
    }

    public final AnalyticsMetadata selectFeature(Uri uri, MatchResult matchResult, Context context) {
        String str;
        List<String> groupValues;
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(context, "context");
        if (matchResult == null || (groupValues = matchResult.getGroupValues()) == null || (str = (String) u.getOrNull(groupValues, 1)) == null) {
            str = "";
        }
        String str2 = str;
        Map mapOf = h0.mapOf(o.to("/account", RouteHandlers$selectFeature$settingMap$1.INSTANCE), o.to("/nitro", RouteHandlers$selectFeature$settingMap$2.INSTANCE), o.to("/voice", RouteHandlers$selectFeature$settingMap$3.INSTANCE), o.to("/createServer", RouteHandlers$selectFeature$settingMap$4.INSTANCE), o.to("/quickSwitcher", RouteHandlers$selectFeature$settingMap$5.INSTANCE), o.to("/friends", RouteHandlers$selectFeature$settingMap$6.INSTANCE), o.to("/mentions", RouteHandlers$selectFeature$settingMap$7.INSTANCE), o.to("/settings", RouteHandlers$selectFeature$settingMap$8.INSTANCE), o.to("/contactSync", RouteHandlers$selectFeature$settingMap$9.INSTANCE), o.to("/addFriends", RouteHandlers$selectFeature$settingMap$10.INSTANCE), o.to("/editProfile", RouteHandlers$selectFeature$settingMap$11.INSTANCE), o.to("/voiceChannel", new RouteHandlers$selectFeature$settingMap$12(uri)));
        StoreStream.Companion.getNavigation().launchNotice(a.v("ROUTING:", str2), new RouteHandlers$selectFeature$1(mapOf, str2));
        if (mapOf.containsKey(str2)) {
            return new AnalyticsMetadata(str2, null, null, 6, null);
        }
        return AnalyticsMetadata.Companion.getUNKNOWN();
    }

    public final AnalyticsMetadata selectLurk(Uri uri, MatchResult matchResult, Context context) {
        List<String> groupValues;
        String str;
        List<String> groupValues2;
        String str2;
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(context, "context");
        long parseLong = (matchResult == null || (groupValues2 = matchResult.getGroupValues()) == null || (str2 = groupValues2.get(1)) == null) ? 0L : Long.parseLong(str2);
        Long longOrNull = (matchResult == null || (groupValues = matchResult.getGroupValues()) == null || (str = groupValues.get(2)) == null) ? null : s.toLongOrNull(str);
        StoreStream.Companion.getLurking().startLurkingAndNavigate(parseLong, longOrNull, context);
        return new AnalyticsMetadata("lurk", Long.valueOf(parseLong), longOrNull);
    }

    public final AnalyticsMetadata selectPremiumGuild(Uri uri, MatchResult matchResult, Context context) {
        List<String> groupValues;
        String str;
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(context, "context");
        long parseLong = (matchResult == null || (groupValues = matchResult.getGroupValues()) == null || (str = groupValues.get(1)) == null) ? 0L : Long.parseLong(str);
        if (parseLong != 0) {
            WidgetGuildBoost.Companion.create(context, parseLong);
        }
        return new AnalyticsMetadata("guild", Long.valueOf(parseLong), null, 4, null);
    }

    public final AnalyticsMetadata selectUserProfile(Uri uri, MatchResult matchResult, Context context) {
        List<String> groupValues;
        String str;
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(context, "context");
        Long longOrNull = (matchResult == null || (groupValues = matchResult.getGroupValues()) == null || (str = (String) u.last((List<? extends Object>) groupValues)) == null) ? null : s.toLongOrNull(str);
        String queryParameter = uri.getQueryParameter("friend_token");
        if (longOrNull != null) {
            WidgetUserSheet.Companion.enqueueNotice(longOrNull.longValue(), queryParameter);
        }
        return new AnalyticsMetadata("profile", null, null, 6, null);
    }

    public final AnalyticsMetadata voiceConnect(Uri uri, MatchResult matchResult, Context context) {
        List<String> groupValues;
        String str;
        Long longOrNull;
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(context, "context");
        long longValue = (matchResult == null || (groupValues = matchResult.getGroupValues()) == null || (str = groupValues.get(1)) == null || (longOrNull = s.toLongOrNull(str)) == null) ? 0L : longOrNull.longValue();
        boolean booleanQueryParameter = uri.getBooleanQueryParameter("service_denied", false);
        WeakReference weakReference = new WeakReference(context);
        Observable<R> F = StoreStream.Companion.getChannels().observeChannel(longValue).x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
        m.checkNotNullExpressionValue(F, "filter { it != null }.map { it!! }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(ObservableExtensionsKt.takeSingleUntilTimeout$default(F, 0L, false, 3, null)), RouteHandlers.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new RouteHandlers$voiceConnect$1(weakReference, longValue, booleanQueryParameter));
        return new AnalyticsMetadata("voice", null, Long.valueOf(longValue), 2, null);
    }

    public final AnalyticsMetadata wildCardMatcher(Uri uri, MatchResult matchResult, Context context) {
        AnalyticsMetadata analyticsMetadata;
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(context, "context");
        List<String> pathSegments = uri.getPathSegments();
        String str = pathSegments != null ? (String) u.firstOrNull((List<? extends Object>) pathSegments) : null;
        String host = uri.getHost();
        RouteHandlers$wildCardMatcher$1 routeHandlers$wildCardMatcher$1 = RouteHandlers$wildCardMatcher$1.INSTANCE;
        KFunction<AnalyticsMetadata> invoke = routeHandlers$wildCardMatcher$1.invoke2(str);
        if (invoke == null) {
            invoke = routeHandlers$wildCardMatcher$1.invoke2(host);
        }
        return (invoke == null || (analyticsMetadata = (AnalyticsMetadata) ((Function3) invoke).invoke(uri, matchResult, context)) == null) ? AnalyticsMetadata.Companion.getUNKNOWN() : analyticsMetadata;
    }
}
