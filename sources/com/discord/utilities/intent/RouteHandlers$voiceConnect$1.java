package com.discord.utilities.intent;

import android.content.Context;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.stores.StoreStream;
import com.discord.widgets.voice.fullscreen.WidgetCallFullscreen;
import d0.z.d.m;
import d0.z.d.o;
import java.lang.ref.WeakReference;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: RouteHandlers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/api/channel/Channel;", "kotlin.jvm.PlatformType", "channel", "", "invoke", "(Lcom/discord/api/channel/Channel;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class RouteHandlers$voiceConnect$1 extends o implements Function1<Channel, Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ boolean $isServicePermissionDeniedRedirect;
    public final /* synthetic */ WeakReference $weakContext;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public RouteHandlers$voiceConnect$1(WeakReference weakReference, long j, boolean z2) {
        super(1);
        this.$weakContext = weakReference;
        this.$channelId = j;
        this.$isServicePermissionDeniedRedirect = z2;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Channel channel) {
        invoke2(channel);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Channel channel) {
        Context context = (Context) this.$weakContext.get();
        if (context != null) {
            m.checkNotNullExpressionValue(context, "weakContext.get() ?: return@appSubscribe");
            m.checkNotNullExpressionValue(channel, "channel");
            if (!ChannelUtils.x(channel)) {
                StoreStream.Companion.getVoiceChannelSelected().selectVoiceChannel(this.$channelId);
                WidgetCallFullscreen.Companion.launch(context, this.$channelId, (r14 & 4) != 0 ? false : this.$isServicePermissionDeniedRedirect, (r14 & 8) != 0 ? null : null, (r14 & 16) != 0 ? null : null);
            }
        }
    }
}
