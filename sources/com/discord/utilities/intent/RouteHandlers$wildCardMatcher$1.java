package com.discord.utilities.intent;

import android.content.Context;
import android.net.Uri;
import com.discord.utilities.intent.RouteHandlers;
import d0.z.d.k;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function3;
import kotlin.reflect.KFunction;
import kotlin.text.MatchResult;
/* compiled from: RouteHandlers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\t\u001a\u001e\u0012\u0004\u0012\u00020\u0003\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"", "input", "Lkotlin/reflect/KFunction3;", "Landroid/net/Uri;", "Lkotlin/text/MatchResult;", "Landroid/content/Context;", "Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;", "invoke", "(Ljava/lang/String;)Lkotlin/reflect/KFunction;", "getActualHandler"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class RouteHandlers$wildCardMatcher$1 extends o implements Function1<String, KFunction<? extends RouteHandlers.AnalyticsMetadata>> {
    public static final RouteHandlers$wildCardMatcher$1 INSTANCE = new RouteHandlers$wildCardMatcher$1();

    /* compiled from: RouteHandlers.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\t\u001a\u00020\u00062\u0006\u0010\u0001\u001a\u00020\u00002\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"Landroid/net/Uri;", "p1", "Lkotlin/text/MatchResult;", "p2", "Landroid/content/Context;", "p3", "Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;", "invoke", "(Landroid/net/Uri;Lkotlin/text/MatchResult;Landroid/content/Context;)Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.utilities.intent.RouteHandlers$wildCardMatcher$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass1 extends k implements Function3<Uri, MatchResult, Context, RouteHandlers.AnalyticsMetadata> {
        public AnonymousClass1(RouteHandlers routeHandlers) {
            super(3, routeHandlers, RouteHandlers.class, "acceptGift", "acceptGift(Landroid/net/Uri;Lkotlin/text/MatchResult;Landroid/content/Context;)Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;", 0);
        }

        public final RouteHandlers.AnalyticsMetadata invoke(Uri uri, MatchResult matchResult, Context context) {
            RouteHandlers.AnalyticsMetadata acceptGift;
            m.checkNotNullParameter(uri, "p1");
            m.checkNotNullParameter(context, "p3");
            acceptGift = ((RouteHandlers) this.receiver).acceptGift(uri, matchResult, context);
            return acceptGift;
        }
    }

    /* compiled from: RouteHandlers.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\t\u001a\u00020\u00062\u0006\u0010\u0001\u001a\u00020\u00002\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"Landroid/net/Uri;", "p1", "Lkotlin/text/MatchResult;", "p2", "Landroid/content/Context;", "p3", "Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;", "invoke", "(Landroid/net/Uri;Lkotlin/text/MatchResult;Landroid/content/Context;)Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.utilities.intent.RouteHandlers$wildCardMatcher$1$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass2 extends k implements Function3<Uri, MatchResult, Context, RouteHandlers.AnalyticsMetadata> {
        public AnonymousClass2(RouteHandlers routeHandlers) {
            super(3, routeHandlers, RouteHandlers.class, "useInvite", "useInvite(Landroid/net/Uri;Lkotlin/text/MatchResult;Landroid/content/Context;)Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;", 0);
        }

        public final RouteHandlers.AnalyticsMetadata invoke(Uri uri, MatchResult matchResult, Context context) {
            RouteHandlers.AnalyticsMetadata useInvite;
            m.checkNotNullParameter(uri, "p1");
            m.checkNotNullParameter(context, "p3");
            useInvite = ((RouteHandlers) this.receiver).useInvite(uri, matchResult, context);
            return useInvite;
        }
    }

    /* compiled from: RouteHandlers.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\t\u001a\u00020\u00062\u0006\u0010\u0001\u001a\u00020\u00002\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"Landroid/net/Uri;", "p1", "Lkotlin/text/MatchResult;", "p2", "Landroid/content/Context;", "p3", "Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;", "invoke", "(Landroid/net/Uri;Lkotlin/text/MatchResult;Landroid/content/Context;)Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.utilities.intent.RouteHandlers$wildCardMatcher$1$3  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass3 extends k implements Function3<Uri, MatchResult, Context, RouteHandlers.AnalyticsMetadata> {
        public AnonymousClass3(RouteHandlers routeHandlers) {
            super(3, routeHandlers, RouteHandlers.class, "useGuildTemplate", "useGuildTemplate(Landroid/net/Uri;Lkotlin/text/MatchResult;Landroid/content/Context;)Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;", 0);
        }

        public final RouteHandlers.AnalyticsMetadata invoke(Uri uri, MatchResult matchResult, Context context) {
            RouteHandlers.AnalyticsMetadata useGuildTemplate;
            m.checkNotNullParameter(uri, "p1");
            m.checkNotNullParameter(context, "p3");
            useGuildTemplate = ((RouteHandlers) this.receiver).useGuildTemplate(uri, matchResult, context);
            return useGuildTemplate;
        }
    }

    public RouteHandlers$wildCardMatcher$1() {
        super(1);
    }

    /* JADX WARN: Code restructure failed: missing block: B:10:0x0023, code lost:
        if (r2.equals("gift") != false) goto L11;
     */
    /* JADX WARN: Code restructure failed: missing block: B:13:0x0033, code lost:
        if (r2.equals("discord.new") != false) goto L19;
     */
    /* JADX WARN: Code restructure failed: missing block: B:15:0x003c, code lost:
        if (r2.equals("invite") != false) goto L16;
     */
    /* JADX WARN: Code restructure failed: missing block: B:18:0x004c, code lost:
        if (r2.equals("template") != false) goto L19;
     */
    /* JADX WARN: Code restructure failed: missing block: B:21:?, code lost:
        return new com.discord.utilities.intent.RouteHandlers$wildCardMatcher$1.AnonymousClass1(com.discord.utilities.intent.RouteHandlers.INSTANCE);
     */
    /* JADX WARN: Code restructure failed: missing block: B:22:?, code lost:
        return new com.discord.utilities.intent.RouteHandlers$wildCardMatcher$1.AnonymousClass2(com.discord.utilities.intent.RouteHandlers.INSTANCE);
     */
    /* JADX WARN: Code restructure failed: missing block: B:23:?, code lost:
        return new com.discord.utilities.intent.RouteHandlers$wildCardMatcher$1.AnonymousClass3(com.discord.utilities.intent.RouteHandlers.INSTANCE);
     */
    /* JADX WARN: Code restructure failed: missing block: B:6:0x0011, code lost:
        if (r2.equals("discord.gift") != false) goto L11;
     */
    /* JADX WARN: Code restructure failed: missing block: B:8:0x001a, code lost:
        if (r2.equals("discord.gg") != false) goto L16;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final kotlin.reflect.KFunction<com.discord.utilities.intent.RouteHandlers.AnalyticsMetadata> invoke(java.lang.String r2) {
        /*
            r1 = this;
            if (r2 != 0) goto L3
            goto L56
        L3:
            int r0 = r2.hashCode()
            switch(r0) {
                case -1321546630: goto L46;
                case -1183699191: goto L36;
                case -633644578: goto L2d;
                case 3172656: goto L1d;
                case 533748962: goto L14;
                case 1831649458: goto Lb;
                default: goto La;
            }
        La:
            goto L56
        Lb:
            java.lang.String r0 = "discord.gift"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L56
            goto L25
        L14:
            java.lang.String r0 = "discord.gg"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L56
            goto L3e
        L1d:
            java.lang.String r0 = "gift"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L56
        L25:
            com.discord.utilities.intent.RouteHandlers$wildCardMatcher$1$1 r2 = new com.discord.utilities.intent.RouteHandlers$wildCardMatcher$1$1
            com.discord.utilities.intent.RouteHandlers r0 = com.discord.utilities.intent.RouteHandlers.INSTANCE
            r2.<init>(r0)
            goto L57
        L2d:
            java.lang.String r0 = "discord.new"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L56
            goto L4e
        L36:
            java.lang.String r0 = "invite"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L56
        L3e:
            com.discord.utilities.intent.RouteHandlers$wildCardMatcher$1$2 r2 = new com.discord.utilities.intent.RouteHandlers$wildCardMatcher$1$2
            com.discord.utilities.intent.RouteHandlers r0 = com.discord.utilities.intent.RouteHandlers.INSTANCE
            r2.<init>(r0)
            goto L57
        L46:
            java.lang.String r0 = "template"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L56
        L4e:
            com.discord.utilities.intent.RouteHandlers$wildCardMatcher$1$3 r2 = new com.discord.utilities.intent.RouteHandlers$wildCardMatcher$1$3
            com.discord.utilities.intent.RouteHandlers r0 = com.discord.utilities.intent.RouteHandlers.INSTANCE
            r2.<init>(r0)
            goto L57
        L56:
            r2 = 0
        L57:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.intent.RouteHandlers$wildCardMatcher$1.invoke(java.lang.String):kotlin.reflect.KFunction");
    }
}
