package com.discord.utilities.intent;

import android.content.Intent;
import android.net.Uri;
import androidx.core.app.NotificationCompat;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.AppStartAnalyticsTracker;
import com.discord.utilities.fcm.NotificationClient;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: IntentUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Landroid/net/Uri;", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "", "uriRoutable", "", "invoke", "(Landroid/net/Uri;Z)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class IntentUtils$consumeExternalRoutingIntent$1 extends o implements Function2<Uri, Boolean, Unit> {
    public final /* synthetic */ Intent $intent;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public IntentUtils$consumeExternalRoutingIntent$1(Intent intent) {
        super(2);
        this.$intent = intent;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(Uri uri, Boolean bool) {
        invoke(uri, bool.booleanValue());
        return Unit.a;
    }

    public final void invoke(Uri uri, boolean z2) {
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        Map<String, Object> buildTrackingData = NotificationClient.INSTANCE.buildTrackingData(this.$intent);
        for (String str : buildTrackingData.keySet()) {
            this.$intent.removeExtra(str);
        }
        AnalyticsTracker.appNotificationClicked(buildTrackingData);
        AppStartAnalyticsTracker.Companion.getInstance().appOpen(uri, z2, !buildTrackingData.isEmpty());
    }
}
