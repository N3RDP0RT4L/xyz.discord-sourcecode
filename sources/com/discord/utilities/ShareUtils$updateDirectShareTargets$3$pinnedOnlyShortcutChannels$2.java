package com.discord.utilities;

import android.content.pm.ShortcutInfo;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: ShareUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Landroid/content/pm/ShortcutInfo;", "kotlin.jvm.PlatformType", "shortcutInfoCompat", "", "invoke", "(Landroid/content/pm/ShortcutInfo;)J", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ShareUtils$updateDirectShareTargets$3$pinnedOnlyShortcutChannels$2 extends o implements Function1<ShortcutInfo, Long> {
    public static final ShareUtils$updateDirectShareTargets$3$pinnedOnlyShortcutChannels$2 INSTANCE = new ShareUtils$updateDirectShareTargets$3$pinnedOnlyShortcutChannels$2();

    public ShareUtils$updateDirectShareTargets$3$pinnedOnlyShortcutChannels$2() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Long invoke(ShortcutInfo shortcutInfo) {
        return Long.valueOf(invoke2(shortcutInfo));
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final long invoke2(ShortcutInfo shortcutInfo) {
        m.checkNotNullExpressionValue(shortcutInfo, "shortcutInfoCompat");
        String id2 = shortcutInfo.getId();
        m.checkNotNullExpressionValue(id2, "shortcutInfoCompat.id");
        return Long.parseLong(id2);
    }
}
