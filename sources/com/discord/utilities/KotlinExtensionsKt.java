package com.discord.utilities;

import androidx.exifinterface.media.ExifInterface;
import kotlin.Metadata;
/* compiled from: KotlinExtensions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0004\n\u0002\b\u0005\"\u001d\u0010\u0003\u001a\u00028\u0000\"\u0004\b\u0000\u0010\u0000*\u00028\u00008F@\u0006¢\u0006\u0006\u001a\u0004\b\u0001\u0010\u0002¨\u0006\u0004"}, d2 = {ExifInterface.GPS_DIRECTION_TRUE, "getExhaustive", "(Ljava/lang/Object;)Ljava/lang/Object;", "exhaustive", "utils_release"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class KotlinExtensionsKt {
    public static final <T> T getExhaustive(T t) {
        return t;
    }
}
