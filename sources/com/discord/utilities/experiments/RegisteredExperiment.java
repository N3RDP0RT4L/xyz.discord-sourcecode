package com.discord.utilities.experiments;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: ExperimentRegistry.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u0012\b\u0086\b\u0018\u00002\u00020\u0001:\u0001(B5\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\u0006\u0010\u0010\u001a\u00020\u0002\u0012\u0006\u0010\u0011\u001a\u00020\u0006\u0012\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00020\t\u0012\u0006\u0010\u0013\u001a\u00020\f¢\u0006\u0004\b&\u0010'J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0016\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJH\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u000f\u001a\u00020\u00022\b\b\u0002\u0010\u0010\u001a\u00020\u00022\b\b\u0002\u0010\u0011\u001a\u00020\u00062\u000e\b\u0002\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00020\t2\b\b\u0002\u0010\u0013\u001a\u00020\fHÆ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0004J\u0010\u0010\u0018\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u001a\u0010\u001b\u001a\u00020\f2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001b\u0010\u001cR\u0019\u0010\u0010\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001d\u001a\u0004\b\u001e\u0010\u0004R\u001f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u001f\u001a\u0004\b \u0010\u000bR\u0019\u0010\u0013\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010!\u001a\u0004\b\"\u0010\u000eR\u0019\u0010\u0011\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010#\u001a\u0004\b$\u0010\bR\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001d\u001a\u0004\b%\u0010\u0004¨\u0006)"}, d2 = {"Lcom/discord/utilities/experiments/RegisteredExperiment;", "", "", "component1", "()Ljava/lang/String;", "component2", "Lcom/discord/utilities/experiments/RegisteredExperiment$Type;", "component3", "()Lcom/discord/utilities/experiments/RegisteredExperiment$Type;", "", "component4", "()Ljava/util/List;", "", "component5", "()Z", "readableName", ModelAuditLogEntry.CHANGE_KEY_NAME, "type", "buckets", "cacheExperiment", "copy", "(Ljava/lang/String;Ljava/lang/String;Lcom/discord/utilities/experiments/RegisteredExperiment$Type;Ljava/util/List;Z)Lcom/discord/utilities/experiments/RegisteredExperiment;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getName", "Ljava/util/List;", "getBuckets", "Z", "getCacheExperiment", "Lcom/discord/utilities/experiments/RegisteredExperiment$Type;", "getType", "getReadableName", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Lcom/discord/utilities/experiments/RegisteredExperiment$Type;Ljava/util/List;Z)V", "Type", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class RegisteredExperiment {
    private final List<String> buckets;
    private final boolean cacheExperiment;
    private final String name;
    private final String readableName;
    private final Type type;

    /* compiled from: ExperimentRegistry.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/utilities/experiments/RegisteredExperiment$Type;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "GUILD", "USER", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum Type {
        GUILD,
        USER
    }

    public RegisteredExperiment(String str, String str2, Type type, List<String> list, boolean z2) {
        m.checkNotNullParameter(str, "readableName");
        m.checkNotNullParameter(str2, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(type, "type");
        m.checkNotNullParameter(list, "buckets");
        this.readableName = str;
        this.name = str2;
        this.type = type;
        this.buckets = list;
        this.cacheExperiment = z2;
    }

    public static /* synthetic */ RegisteredExperiment copy$default(RegisteredExperiment registeredExperiment, String str, String str2, Type type, List list, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = registeredExperiment.readableName;
        }
        if ((i & 2) != 0) {
            str2 = registeredExperiment.name;
        }
        String str3 = str2;
        if ((i & 4) != 0) {
            type = registeredExperiment.type;
        }
        Type type2 = type;
        List<String> list2 = list;
        if ((i & 8) != 0) {
            list2 = registeredExperiment.buckets;
        }
        List list3 = list2;
        if ((i & 16) != 0) {
            z2 = registeredExperiment.cacheExperiment;
        }
        return registeredExperiment.copy(str, str3, type2, list3, z2);
    }

    public final String component1() {
        return this.readableName;
    }

    public final String component2() {
        return this.name;
    }

    public final Type component3() {
        return this.type;
    }

    public final List<String> component4() {
        return this.buckets;
    }

    public final boolean component5() {
        return this.cacheExperiment;
    }

    public final RegisteredExperiment copy(String str, String str2, Type type, List<String> list, boolean z2) {
        m.checkNotNullParameter(str, "readableName");
        m.checkNotNullParameter(str2, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(type, "type");
        m.checkNotNullParameter(list, "buckets");
        return new RegisteredExperiment(str, str2, type, list, z2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof RegisteredExperiment)) {
            return false;
        }
        RegisteredExperiment registeredExperiment = (RegisteredExperiment) obj;
        return m.areEqual(this.readableName, registeredExperiment.readableName) && m.areEqual(this.name, registeredExperiment.name) && m.areEqual(this.type, registeredExperiment.type) && m.areEqual(this.buckets, registeredExperiment.buckets) && this.cacheExperiment == registeredExperiment.cacheExperiment;
    }

    public final List<String> getBuckets() {
        return this.buckets;
    }

    public final boolean getCacheExperiment() {
        return this.cacheExperiment;
    }

    public final String getName() {
        return this.name;
    }

    public final String getReadableName() {
        return this.readableName;
    }

    public final Type getType() {
        return this.type;
    }

    public int hashCode() {
        String str = this.readableName;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.name;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        Type type = this.type;
        int hashCode3 = (hashCode2 + (type != null ? type.hashCode() : 0)) * 31;
        List<String> list = this.buckets;
        if (list != null) {
            i = list.hashCode();
        }
        int i2 = (hashCode3 + i) * 31;
        boolean z2 = this.cacheExperiment;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        return i2 + i3;
    }

    public String toString() {
        StringBuilder R = a.R("RegisteredExperiment(readableName=");
        R.append(this.readableName);
        R.append(", name=");
        R.append(this.name);
        R.append(", type=");
        R.append(this.type);
        R.append(", buckets=");
        R.append(this.buckets);
        R.append(", cacheExperiment=");
        return a.M(R, this.cacheExperiment, ")");
    }
}
