package com.discord.utilities.experiments;

import andhook.lib.HookHelper;
import andhook.lib.xposed.callbacks.XCallback;
import com.discord.api.guild.GuildFeature;
import com.discord.api.guild.GuildHubType;
import com.discord.models.experiments.domain.ExperimentHash;
import com.discord.models.experiments.dto.GuildExperimentBucketDto;
import com.discord.models.experiments.dto.GuildExperimentDto;
import com.discord.models.experiments.dto.GuildExperimentFilter;
import com.discord.models.experiments.dto.GuildExperimentOverridesDto;
import com.discord.models.experiments.dto.GuildExperimentPopulationDto;
import com.discord.models.guild.Guild;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.d0.f;
import d0.z.d.m;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.ranges.IntRange;
/* compiled from: ExperimentUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0011\u0010\u0012J;\u0010\r\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u00072\b\u0010\n\u001a\u0004\u0018\u00010\t2\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\r\u0010\u000eR\u0016\u0010\u000f\u001a\u00020\u00078\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010¨\u0006\u0013"}, d2 = {"Lcom/discord/utilities/experiments/ExperimentUtils;", "", "", "experimentName", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "guildMemberCount", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/models/experiments/dto/GuildExperimentDto;", "experiment", "computeGuildExperimentBucket", "(Ljava/lang/String;JILcom/discord/models/guild/Guild;Lcom/discord/models/experiments/dto/GuildExperimentDto;)I", "BUCKET_NOT_ELIGIBLE", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ExperimentUtils {
    public static final int BUCKET_NOT_ELIGIBLE = -1;
    public static final ExperimentUtils INSTANCE = new ExperimentUtils();

    private ExperimentUtils() {
    }

    public final int computeGuildExperimentBucket(String str, long j, int i, Guild guild, GuildExperimentDto guildExperimentDto) {
        Object obj;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        boolean z7;
        String str2 = str;
        m.checkNotNullParameter(str2, "experimentName");
        m.checkNotNullParameter(guildExperimentDto, "experiment");
        for (GuildExperimentOverridesDto guildExperimentOverridesDto : guildExperimentDto.getOverrides()) {
            if (guildExperimentOverridesDto.getGuilds().contains(Long.valueOf(j))) {
                return guildExperimentOverridesDto.getBucket();
            }
        }
        StringBuilder sb = new StringBuilder();
        String hashKey = guildExperimentDto.getHashKey();
        if (hashKey != null) {
            str2 = hashKey;
        }
        sb.append(str2);
        sb.append(MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR);
        sb.append(j);
        long from = ExperimentHash.INSTANCE.from(sb.toString()) % ((long) XCallback.PRIORITY_HIGHEST);
        for (GuildExperimentPopulationDto guildExperimentPopulationDto : guildExperimentDto.getPopulations()) {
            Iterator<GuildExperimentFilter> it = guildExperimentPopulationDto.getFilters().iterator();
            boolean z8 = true;
            while (true) {
                obj = null;
                if (it.hasNext()) {
                    GuildExperimentFilter next = it.next();
                    if (next instanceof GuildExperimentFilter.GuildIdsFilter) {
                        if (!((GuildExperimentFilter.GuildIdsFilter) next).getGuildIds().contains(Long.valueOf(j))) {
                            z8 = false;
                        }
                    } else if (next instanceof GuildExperimentFilter.GuildIdRangeFilter) {
                        if (!((GuildExperimentFilter.GuildIdRangeFilter) next).getRange().contains(j)) {
                            z8 = false;
                        }
                    } else if (next instanceof GuildExperimentFilter.GuildMemberCountRangeFilter) {
                        if (!f.longRangeContains(((GuildExperimentFilter.GuildMemberCountRangeFilter) next).getRange(), i)) {
                            z8 = false;
                        }
                    } else if (next instanceof GuildExperimentFilter.GuildHasFeatureFilter) {
                        Set<GuildFeature> features = ((GuildExperimentFilter.GuildHasFeatureFilter) next).getFeatures();
                        if (!(features instanceof Collection) || !features.isEmpty()) {
                            for (GuildFeature guildFeature : features) {
                                if (guild == null || !guild.hasFeature(guildFeature)) {
                                    z5 = false;
                                    continue;
                                } else {
                                    z5 = true;
                                    continue;
                                }
                                if (z5) {
                                    z4 = false;
                                    break;
                                }
                            }
                        }
                        z4 = true;
                        if (z4) {
                            z8 = false;
                        }
                    } else if (next instanceof GuildExperimentFilter.GuildHubTypesFeatureFilter) {
                        Set<GuildHubType> hubTypes = ((GuildExperimentFilter.GuildHubTypesFeatureFilter) next).getHubTypes();
                        if (!(hubTypes instanceof Collection) || !hubTypes.isEmpty()) {
                            for (GuildHubType guildHubType : hubTypes) {
                                if ((guild != null ? guild.getHubType() : null) == guildHubType) {
                                    z7 = true;
                                    continue;
                                } else {
                                    z7 = false;
                                    continue;
                                }
                                if (z7) {
                                    z6 = false;
                                    break;
                                }
                            }
                        }
                        z6 = true;
                        if (z6) {
                            z8 = false;
                        }
                    }
                }
            }
            if (z8) {
                Iterator<T> it2 = guildExperimentPopulationDto.getBuckets().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    Object next2 = it2.next();
                    List<IntRange> positions = ((GuildExperimentBucketDto) next2).getPositions();
                    if (!(positions instanceof Collection) || !positions.isEmpty()) {
                        for (IntRange intRange : positions) {
                            if (from < intRange.getFirst() || from >= intRange.getLast()) {
                                z3 = false;
                                continue;
                            } else {
                                z3 = true;
                                continue;
                            }
                            if (z3) {
                                z2 = true;
                                continue;
                                break;
                            }
                        }
                    }
                    z2 = false;
                    continue;
                    if (z2) {
                        obj = next2;
                        break;
                    }
                }
                GuildExperimentBucketDto guildExperimentBucketDto = (GuildExperimentBucketDto) obj;
                if (guildExperimentBucketDto != null) {
                    return guildExperimentBucketDto.getBucket();
                }
                return 0;
            }
        }
        return -1;
    }
}
