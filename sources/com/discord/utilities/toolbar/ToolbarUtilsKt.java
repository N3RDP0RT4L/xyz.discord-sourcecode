package com.discord.utilities.toolbar;

import android.view.View;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewCompat;
import com.discord.utilities.dimen.DimenUtils;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
/* compiled from: ToolbarUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\u001a\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0001*\u00020\u0000¢\u0006\u0004\b\u0002\u0010\u0003\u001a\u0019\u0010\u0006\u001a\u00020\u0005*\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0001¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\b"}, d2 = {"Landroidx/appcompat/widget/Toolbar;", "Landroid/view/View;", "getNavigationIconView", "(Landroidx/appcompat/widget/Toolbar;)Landroid/view/View;", "view", "", "positionUnreadCountView", "(Landroidx/appcompat/widget/Toolbar;Landroid/view/View;)V", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ToolbarUtilsKt {
    public static final View getNavigationIconView(Toolbar toolbar) {
        m.checkNotNullParameter(toolbar, "$this$getNavigationIconView");
        CharSequence navigationContentDescription = toolbar.getNavigationContentDescription();
        boolean z2 = false;
        if (navigationContentDescription != null) {
            if (navigationContentDescription.length() > 0) {
                z2 = true;
            }
        }
        String navigationContentDescription2 = z2 ? toolbar.getNavigationContentDescription() : "navigationIcon";
        toolbar.setNavigationContentDescription(navigationContentDescription2);
        ArrayList<View> arrayList = new ArrayList<>();
        toolbar.findViewsWithText(arrayList, navigationContentDescription2, 2);
        if (!z2) {
            toolbar.setNavigationContentDescription((CharSequence) null);
        }
        return (View) u.firstOrNull((List<? extends Object>) arrayList);
    }

    public static final void positionUnreadCountView(Toolbar toolbar, final View view) {
        m.checkNotNullParameter(toolbar, "$this$positionUnreadCountView");
        m.checkNotNullParameter(view, "view");
        View navigationIconView = getNavigationIconView(toolbar);
        if (navigationIconView == null) {
            return;
        }
        if (!ViewCompat.isLaidOut(navigationIconView) || navigationIconView.isLayoutRequested()) {
            navigationIconView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() { // from class: com.discord.utilities.toolbar.ToolbarUtilsKt$positionUnreadCountView$$inlined$doOnLayout$1
                @Override // android.view.View.OnLayoutChangeListener
                public void onLayoutChange(View view2, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                    m.checkNotNullParameter(view2, "view");
                    view2.removeOnLayoutChangeListener(this);
                    int dpToPixels = DimenUtils.dpToPixels(2);
                    int right = view2.getRight();
                    int bottom = view2.getBottom();
                    View view3 = view;
                    float f = 2;
                    float f2 = dpToPixels;
                    view3.setX((right / f) + f2);
                    view3.setY((bottom / f) + f2);
                }
            });
            return;
        }
        int dpToPixels = DimenUtils.dpToPixels(2);
        int right = navigationIconView.getRight();
        int bottom = navigationIconView.getBottom();
        float f = 2;
        float f2 = dpToPixels;
        view.setX((right / f) + f2);
        view.setY((bottom / f) + f2);
    }
}
