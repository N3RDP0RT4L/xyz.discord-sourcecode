package com.discord.utilities.fragment;

import androidx.fragment.app.Fragment;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.g;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
/* compiled from: FragmentExtensions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a)\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u00012\b\b\u0002\u0010\u0004\u001a\u00020\u0003¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\b"}, d2 = {"Landroidx/fragment/app/Fragment;", "", ModelAuditLogEntry.CHANGE_KEY_NAME, "", "defaultValue", "Lkotlin/Lazy;", "booleanExtra", "(Landroidx/fragment/app/Fragment;Ljava/lang/String;Z)Lkotlin/Lazy;", "utils_release"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class FragmentExtensionsKt {
    public static final Lazy<Boolean> booleanExtra(Fragment fragment, String str, boolean z2) {
        m.checkNotNullParameter(fragment, "$this$booleanExtra");
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        return g.lazy(new FragmentExtensionsKt$booleanExtra$1(fragment, str, z2));
    }

    public static /* synthetic */ Lazy booleanExtra$default(Fragment fragment, String str, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        return booleanExtra(fragment, str, z2);
    }
}
