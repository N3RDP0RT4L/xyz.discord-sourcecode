package com.discord.utilities.fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: FragmentExtensions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class FragmentExtensionsKt$booleanExtra$1 extends o implements Function0<Boolean> {
    public final /* synthetic */ boolean $defaultValue;
    public final /* synthetic */ String $name;
    public final /* synthetic */ Fragment $this_booleanExtra;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public FragmentExtensionsKt$booleanExtra$1(Fragment fragment, String str, boolean z2) {
        super(0);
        this.$this_booleanExtra = fragment;
        this.$name = str;
        this.$defaultValue = z2;
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [boolean, java.lang.Boolean] */
    /* JADX WARN: Type inference failed for: r0v3, types: [boolean, java.lang.Boolean] */
    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final Boolean invoke2() {
        Bundle arguments = this.$this_booleanExtra.getArguments();
        return arguments != null ? arguments.getBoolean(this.$name, this.$defaultValue) : this.$defaultValue;
    }
}
