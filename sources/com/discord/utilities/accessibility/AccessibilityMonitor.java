package com.discord.utilities.accessibility;

import andhook.lib.HookHelper;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.Application;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.view.accessibility.AccessibilityManager;
import d0.g;
import d0.z.d.m;
import j0.l.e.k;
import java.util.EnumSet;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Action1;
import rx.subjects.BehaviorSubject;
import rx.subjects.SerializedSubject;
/* compiled from: AccessibilityMonitor.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 )2\u00020\u0001:\u0001)B\u0007¢\u0006\u0004\b(\u0010\fJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u0011H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0015\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0006J\u0013\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00110\u0016¢\u0006\u0004\b\u0017\u0010\u0018R\u0016\u0010\u0019\u001a\u00020\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001c\u001a\u00020\u001b8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR\u0016\u0010\u001f\u001a\u00020\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u0016\u0010\"\u001a\u00020!8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\"\u0010#R2\u0010&\u001a\u001e\u0012\f\u0012\n %*\u0004\u0018\u00010\u00110\u0011\u0012\f\u0012\n %*\u0004\u0018\u00010\u00110\u00110$8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b&\u0010'¨\u0006*"}, d2 = {"Lcom/discord/utilities/accessibility/AccessibilityMonitor;", "", "Landroid/content/Context;", "context", "", "handleInitialState", "(Landroid/content/Context;)V", "", "enabled", "handleScreenreaderEnabledUpdate", "(Z)V", "handleReduceMotionUpdated", "()V", "", "uiMode", "handleUIModeUpdate", "(I)V", "Lcom/discord/utilities/accessibility/AccessibilityState;", "newState", "updateAccessibilityState", "(Lcom/discord/utilities/accessibility/AccessibilityState;)V", "bindContext", "Lrx/Observable;", "observeAccessibilityState", "()Lrx/Observable;", "accessibilityState", "Lcom/discord/utilities/accessibility/AccessibilityState;", "Landroid/content/ContentResolver;", "contentResolver", "Landroid/content/ContentResolver;", "Landroid/database/ContentObserver;", "animationScaleObserver", "Landroid/database/ContentObserver;", "Landroid/view/accessibility/AccessibilityManager;", "accessibilityManager", "Landroid/view/accessibility/AccessibilityManager;", "Lrx/subjects/SerializedSubject;", "kotlin.jvm.PlatformType", "accessibilityStateSubject", "Lrx/subjects/SerializedSubject;", HookHelper.constructorName, "Companion", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AccessibilityMonitor {
    public static final Companion Companion = new Companion(null);
    private static final Lazy INSTANCE$delegate = g.lazy(AccessibilityMonitor$Companion$INSTANCE$2.INSTANCE);
    private AccessibilityManager accessibilityManager;
    private AccessibilityState accessibilityState;
    private final SerializedSubject<AccessibilityState, AccessibilityState> accessibilityStateSubject;
    private final ContentObserver animationScaleObserver;
    private ContentResolver contentResolver;

    /* compiled from: AccessibilityMonitor.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u001d\u0010\f\u001a\u00020\u00078F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000b¨\u0006\u000f"}, d2 = {"Lcom/discord/utilities/accessibility/AccessibilityMonitor$Companion;", "", "Landroid/app/Application;", "application", "", "initialize", "(Landroid/app/Application;)V", "Lcom/discord/utilities/accessibility/AccessibilityMonitor;", "INSTANCE$delegate", "Lkotlin/Lazy;", "getINSTANCE", "()Lcom/discord/utilities/accessibility/AccessibilityMonitor;", "INSTANCE", HookHelper.constructorName, "()V", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public final AccessibilityMonitor getINSTANCE() {
            Lazy lazy = AccessibilityMonitor.INSTANCE$delegate;
            Companion companion = AccessibilityMonitor.Companion;
            return (AccessibilityMonitor) lazy.getValue();
        }

        public final void initialize(Application application) {
            m.checkNotNullParameter(application, "application");
            getINSTANCE().bindContext(application);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public AccessibilityMonitor() {
        final Handler handler = new Handler(Looper.getMainLooper());
        this.animationScaleObserver = new ContentObserver(handler) { // from class: com.discord.utilities.accessibility.AccessibilityMonitor$animationScaleObserver$1
            @Override // android.database.ContentObserver
            public void onChange(boolean z2) {
                onChange(z2, null);
            }

            @Override // android.database.ContentObserver
            public void onChange(boolean z2, Uri uri) {
                AccessibilityMonitor.this.handleReduceMotionUpdated();
            }
        };
        AccessibilityState accessibilityState = new AccessibilityState(null, 1, null);
        this.accessibilityState = accessibilityState;
        this.accessibilityStateSubject = new SerializedSubject<>(BehaviorSubject.l0(accessibilityState));
    }

    private final synchronized void handleInitialState(Context context) {
        Object systemService = context.getSystemService("accessibility");
        if (!(systemService instanceof AccessibilityManager)) {
            systemService = null;
        }
        AccessibilityManager accessibilityManager = (AccessibilityManager) systemService;
        if (accessibilityManager != null) {
            this.accessibilityManager = accessibilityManager;
            if (accessibilityManager == null) {
                m.throwUninitializedPropertyAccessException("accessibilityManager");
            }
            accessibilityManager.addAccessibilityStateChangeListener(new AccessibilityManager.AccessibilityStateChangeListener() { // from class: com.discord.utilities.accessibility.AccessibilityMonitor$handleInitialState$1
                @Override // android.view.accessibility.AccessibilityManager.AccessibilityStateChangeListener
                public final void onAccessibilityStateChanged(final boolean z2) {
                    new k(Unit.a).p(200L, TimeUnit.MILLISECONDS).V(new Action1<Unit>() { // from class: com.discord.utilities.accessibility.AccessibilityMonitor$handleInitialState$1.1
                        public final void call(Unit unit) {
                            AccessibilityMonitor.this.handleScreenreaderEnabledUpdate(z2);
                        }
                    });
                }
            });
            AccessibilityManager accessibilityManager2 = this.accessibilityManager;
            if (accessibilityManager2 == null) {
                m.throwUninitializedPropertyAccessException("accessibilityManager");
            }
            handleScreenreaderEnabledUpdate(accessibilityManager2.isEnabled());
            ContentResolver contentResolver = context.getContentResolver();
            m.checkNotNullExpressionValue(contentResolver, "context.contentResolver");
            this.contentResolver = contentResolver;
            Uri uriFor = Settings.Global.getUriFor("transition_animation_scale");
            ContentResolver contentResolver2 = this.contentResolver;
            if (contentResolver2 == null) {
                m.throwUninitializedPropertyAccessException("contentResolver");
            }
            contentResolver2.registerContentObserver(uriFor, false, this.animationScaleObserver);
            Resources resources = context.getResources();
            m.checkNotNullExpressionValue(resources, "context.resources");
            handleUIModeUpdate(resources.getConfiguration().uiMode);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleReduceMotionUpdated() {
        EnumSet<AccessibilityFeatureFlags> copyOf = EnumSet.copyOf((EnumSet) this.accessibilityState.getFeatures());
        ContentResolver contentResolver = this.contentResolver;
        if (contentResolver == null) {
            m.throwUninitializedPropertyAccessException("contentResolver");
        }
        String string = Settings.Global.getString(contentResolver, "transition_animation_scale");
        if (m.areEqual(string, "0.0") || m.areEqual(string, "0")) {
            copyOf.add(AccessibilityFeatureFlags.REDUCED_MOTION);
        } else {
            copyOf.remove(AccessibilityFeatureFlags.REDUCED_MOTION);
        }
        AccessibilityState accessibilityState = this.accessibilityState;
        m.checkNotNullExpressionValue(copyOf, "features");
        updateAccessibilityState(accessibilityState.copy(copyOf));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleScreenreaderEnabledUpdate(boolean z2) {
        EnumSet<AccessibilityFeatureFlags> copyOf = EnumSet.copyOf((EnumSet) this.accessibilityState.getFeatures());
        AccessibilityManager accessibilityManager = this.accessibilityManager;
        if (accessibilityManager == null) {
            m.throwUninitializedPropertyAccessException("accessibilityManager");
        }
        List<AccessibilityServiceInfo> enabledAccessibilityServiceList = accessibilityManager.getEnabledAccessibilityServiceList(1);
        if (z2) {
            m.checkNotNullExpressionValue(enabledAccessibilityServiceList, "services");
            if (!enabledAccessibilityServiceList.isEmpty()) {
                copyOf.add(AccessibilityFeatureFlags.SCREENREADER);
                AccessibilityState accessibilityState = this.accessibilityState;
                m.checkNotNullExpressionValue(copyOf, "features");
                updateAccessibilityState(accessibilityState.copy(copyOf));
            }
        }
        copyOf.remove(AccessibilityFeatureFlags.SCREENREADER);
        AccessibilityState accessibilityState2 = this.accessibilityState;
        m.checkNotNullExpressionValue(copyOf, "features");
        updateAccessibilityState(accessibilityState2.copy(copyOf));
    }

    private final void handleUIModeUpdate(int i) {
        EnumSet<AccessibilityFeatureFlags> copyOf = EnumSet.copyOf((EnumSet) this.accessibilityState.getFeatures());
        int i2 = i & 48;
        if (i2 == 16) {
            copyOf.add(AccessibilityFeatureFlags.PREFERS_COLOR_SCHEME_LIGHT);
            copyOf.remove(AccessibilityFeatureFlags.PREFERS_COLOR_SCHEME_DARK);
        } else if (i2 != 32) {
            copyOf.remove(AccessibilityFeatureFlags.PREFERS_COLOR_SCHEME_LIGHT);
            copyOf.remove(AccessibilityFeatureFlags.PREFERS_COLOR_SCHEME_DARK);
        } else {
            copyOf.add(AccessibilityFeatureFlags.PREFERS_COLOR_SCHEME_DARK);
            copyOf.remove(AccessibilityFeatureFlags.PREFERS_COLOR_SCHEME_LIGHT);
        }
        AccessibilityState accessibilityState = this.accessibilityState;
        m.checkNotNullExpressionValue(copyOf, "features");
        updateAccessibilityState(accessibilityState.copy(copyOf));
    }

    private final void updateAccessibilityState(AccessibilityState accessibilityState) {
        this.accessibilityState = accessibilityState;
        this.accessibilityStateSubject.k.onNext(accessibilityState);
    }

    public final void bindContext(Context context) {
        m.checkNotNullParameter(context, "context");
        handleInitialState(context);
    }

    public final Observable<AccessibilityState> observeAccessibilityState() {
        Observable<AccessibilityState> q = this.accessibilityStateSubject.q();
        m.checkNotNullExpressionValue(q, "accessibilityStateSubject.distinctUntilChanged()");
        return q;
    }
}
