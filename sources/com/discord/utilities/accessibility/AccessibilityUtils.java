package com.discord.utilities.accessibility;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import androidx.annotation.StringRes;
import androidx.core.view.AccessibilityDelegateCompat;
import androidx.core.view.ViewCompat;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import com.discord.stores.StoreStream;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: AccessibilityUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\b\b\u0001\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\bJ\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\t¢\u0006\u0004\b\u0007\u0010\nJ\u0015\u0010\r\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\r\u0010\u000eJ\r\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b\u0010\u0010\u0011¨\u0006\u0014"}, d2 = {"Lcom/discord/utilities/accessibility/AccessibilityUtils;", "", "Landroid/content/Context;", "context", "", "message", "", "sendAnnouncement", "(Landroid/content/Context;I)V", "", "(Landroid/content/Context;Ljava/lang/String;)V", "Landroid/view/View;", "view", "setViewIsHeading", "(Landroid/view/View;)V", "", "isReducedMotionEnabled", "()Z", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AccessibilityUtils {
    public static final AccessibilityUtils INSTANCE = new AccessibilityUtils();

    private AccessibilityUtils() {
    }

    public final boolean isReducedMotionEnabled() {
        return StoreStream.Companion.getAccessibility().isReducedMotionEnabled();
    }

    public final void sendAnnouncement(Context context, @StringRes int i) {
        m.checkNotNullParameter(context, "context");
        String string = context.getString(i);
        m.checkNotNullExpressionValue(string, "context.getString(message)");
        sendAnnouncement(context, string);
    }

    public final void setViewIsHeading(View view) {
        m.checkNotNullParameter(view, "view");
        ViewCompat.setAccessibilityDelegate(view, new AccessibilityDelegateCompat() { // from class: com.discord.utilities.accessibility.AccessibilityUtils$setViewIsHeading$1
            @Override // androidx.core.view.AccessibilityDelegateCompat
            public void onInitializeAccessibilityNodeInfo(View view2, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
                m.checkNotNullParameter(view2, "host");
                m.checkNotNullParameter(accessibilityNodeInfoCompat, "info");
                super.onInitializeAccessibilityNodeInfo(view2, accessibilityNodeInfoCompat);
                accessibilityNodeInfoCompat.setHeading(true);
            }
        });
    }

    public final void sendAnnouncement(Context context, String str) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(str, "message");
        Object systemService = context.getSystemService("accessibility");
        if (!(systemService instanceof AccessibilityManager)) {
            systemService = null;
        }
        AccessibilityManager accessibilityManager = (AccessibilityManager) systemService;
        if (accessibilityManager != null) {
            AccessibilityEvent obtain = AccessibilityEvent.obtain();
            m.checkNotNullExpressionValue(obtain, "event");
            obtain.setEventType(16384);
            obtain.getText().add(str);
            try {
                accessibilityManager.sendAccessibilityEvent(obtain);
            } catch (IllegalStateException unused) {
            }
        }
    }
}
