package com.discord.utilities.accessibility;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import java.util.EnumSet;
import kotlin.Metadata;
/* compiled from: AccessibilityMonitor.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J \u0010\u0007\u001a\u00020\u00002\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u001f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0013\u001a\u0004\b\u0014\u0010\u0005¨\u0006\u0017"}, d2 = {"Lcom/discord/utilities/accessibility/AccessibilityState;", "", "Ljava/util/EnumSet;", "Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;", "component1", "()Ljava/util/EnumSet;", "features", "copy", "(Ljava/util/EnumSet;)Lcom/discord/utilities/accessibility/AccessibilityState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/EnumSet;", "getFeatures", HookHelper.constructorName, "(Ljava/util/EnumSet;)V", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AccessibilityState {
    private final EnumSet<AccessibilityFeatureFlags> features;

    public AccessibilityState() {
        this(null, 1, null);
    }

    public AccessibilityState(EnumSet<AccessibilityFeatureFlags> enumSet) {
        m.checkNotNullParameter(enumSet, "features");
        this.features = enumSet;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ AccessibilityState copy$default(AccessibilityState accessibilityState, EnumSet enumSet, int i, Object obj) {
        if ((i & 1) != 0) {
            enumSet = accessibilityState.features;
        }
        return accessibilityState.copy(enumSet);
    }

    public final EnumSet<AccessibilityFeatureFlags> component1() {
        return this.features;
    }

    public final AccessibilityState copy(EnumSet<AccessibilityFeatureFlags> enumSet) {
        m.checkNotNullParameter(enumSet, "features");
        return new AccessibilityState(enumSet);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof AccessibilityState) && m.areEqual(this.features, ((AccessibilityState) obj).features);
        }
        return true;
    }

    public final EnumSet<AccessibilityFeatureFlags> getFeatures() {
        return this.features;
    }

    public int hashCode() {
        EnumSet<AccessibilityFeatureFlags> enumSet = this.features;
        if (enumSet != null) {
            return enumSet.hashCode();
        }
        return 0;
    }

    public String toString() {
        StringBuilder R = a.R("AccessibilityState(features=");
        R.append(this.features);
        R.append(")");
        return R.toString();
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ AccessibilityState(java.util.EnumSet r1, int r2, kotlin.jvm.internal.DefaultConstructorMarker r3) {
        /*
            r0 = this;
            r2 = r2 & 1
            if (r2 == 0) goto Lf
            com.discord.utilities.accessibility.AccessibilityFeatureFlags r1 = com.discord.utilities.accessibility.AccessibilityFeatureFlags.NONE
            java.util.EnumSet r1 = java.util.EnumSet.of(r1)
            java.lang.String r2 = "EnumSet.of(AccessibilityFeatureFlags.NONE)"
            d0.z.d.m.checkNotNullExpressionValue(r1, r2)
        Lf:
            r0.<init>(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.accessibility.AccessibilityState.<init>(java.util.EnumSet, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }
}
