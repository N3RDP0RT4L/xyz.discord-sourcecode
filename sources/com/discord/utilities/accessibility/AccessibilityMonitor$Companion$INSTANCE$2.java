package com.discord.utilities.accessibility;

import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: AccessibilityMonitor.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/utilities/accessibility/AccessibilityMonitor;", "invoke", "()Lcom/discord/utilities/accessibility/AccessibilityMonitor;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AccessibilityMonitor$Companion$INSTANCE$2 extends o implements Function0<AccessibilityMonitor> {
    public static final AccessibilityMonitor$Companion$INSTANCE$2 INSTANCE = new AccessibilityMonitor$Companion$INSTANCE$2();

    public AccessibilityMonitor$Companion$INSTANCE$2() {
        super(0);
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AccessibilityMonitor invoke() {
        return new AccessibilityMonitor();
    }
}
