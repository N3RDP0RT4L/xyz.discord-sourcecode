package com.discord.utilities.mg_recycler;

import andhook.lib.HookHelper;
import android.view.View;
import androidx.exifinterface.media.ExifInterface;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import d0.z.d.m;
import kotlin.Metadata;
import rx.Subscription;
/* compiled from: MGRecyclerViewHolder.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0016\u0018\u0000*\u000e\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u00028\u00010\u0001*\u0004\b\u0001\u0010\u00032\u00020\u0004B\u0017\u0012\u0006\u0010\u0013\u001a\u00020\u0012\u0012\u0006\u0010\u0010\u001a\u00028\u0000¢\u0006\u0004\b\u0014\u0010\u0015B\u001b\b\u0016\u0012\b\b\u0001\u0010\u0016\u001a\u00020\u0005\u0012\u0006\u0010\u0010\u001a\u00028\u0000¢\u0006\u0004\b\u0014\u0010\u0017J\u0015\u0010\u0007\u001a\u00028\u00002\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\u0007\u0010\bJ\u001f\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\t\u001a\u00028\u0001H\u0014¢\u0006\u0004\b\u000b\u0010\fJ\u0011\u0010\u000e\u001a\u0004\u0018\u00010\rH\u0014¢\u0006\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0010\u001a\u00028\u00008\u0004@\u0005X\u0085\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011¨\u0006\u0018"}, d2 = {"Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;", ExifInterface.GPS_DIRECTION_TRUE, "D", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "onBindViewHolder", "(I)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;", "data", "", "onConfigure", "(ILjava/lang/Object;)V", "Lrx/Subscription;", "getSubscription", "()Lrx/Subscription;", "adapter", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;", "Landroid/view/View;", "itemView", HookHelper.constructorName, "(Landroid/view/View;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)V", "layout", "(ILcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public class MGRecyclerViewHolder<T extends MGRecyclerAdapter<D>, D> extends RecyclerView.ViewHolder {
    public final T adapter;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MGRecyclerViewHolder(View view, T t) {
        super(view);
        m.checkNotNullParameter(view, "itemView");
        m.checkNotNullParameter(t, "adapter");
        this.adapter = t;
    }

    public Subscription getSubscription() {
        return null;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final T onBindViewHolder(int i) {
        T t = this.adapter;
        Subscription subscription = getSubscription();
        if (subscription != null) {
            t.getCellSubscriptions().c(subscription);
        }
        onConfigure(i, t.getItem(i));
        Subscription subscription2 = getSubscription();
        if (subscription2 != null) {
            t.getCellSubscriptions().a(subscription2);
        }
        return t;
    }

    public void onConfigure(int i, D d) {
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public MGRecyclerViewHolder(@androidx.annotation.LayoutRes int r4, T r5) {
        /*
            r3 = this;
            java.lang.String r0 = "adapter"
            d0.z.d.m.checkNotNullParameter(r5, r0)
            androidx.recyclerview.widget.RecyclerView r0 = r5.getRecycler()
            android.content.Context r1 = r0.getContext()
            android.view.LayoutInflater r1 = android.view.LayoutInflater.from(r1)
            r2 = 0
            android.view.View r4 = r1.inflate(r4, r0, r2)
            java.lang.String r0 = "adapter.recycler.let {\n …se) // ktlint-disable\n  }"
            d0.z.d.m.checkNotNullExpressionValue(r4, r0)
            r3.<init>(r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.mg_recycler.MGRecyclerViewHolder.<init>(int, com.discord.utilities.mg_recycler.MGRecyclerAdapter):void");
    }
}
