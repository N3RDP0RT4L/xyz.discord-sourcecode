package com.discord.utilities.mg_recycler;

import andhook.lib.HookHelper;
import android.annotation.SuppressLint;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.app.AppLog;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.recycler.DiffCreator;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.o;
import d0.t.g0;
import d0.t.n;
import d0.z.d.m;
import j0.p.a;
import java.util.List;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Subscription;
/* compiled from: MGRecyclerAdapterSimple.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\b&\u0018\u0000*\b\b\u0000\u0010\u0002*\u00020\u00012\b\u0012\u0004\u0012\u00028\u00000\u0003B\u001b\b\u0007\u0012\u0006\u00102\u001a\u000201\u0012\b\b\u0002\u0010.\u001a\u00020-¢\u0006\u0004\b3\u00104J\u0017\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ5\u0010\u000e\u001a\u00020\u00062\b\u0010\n\u001a\u0004\u0018\u00010\t2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00028\u00000\u000b2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00000\u000bH\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ3\u0010\u0012\u001a\u00020\u00062$\u0010\u0011\u001a \u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u000b\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u000b\u0012\u0004\u0012\u00020\u00060\u0010¢\u0006\u0004\b\u0012\u0010\u0013J\r\u0010\u0014\u001a\u00020\u0006¢\u0006\u0004\b\u0014\u0010\u0015J\u0017\u0010\u0018\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0016H\u0016¢\u0006\u0004\b\u0018\u0010\u0019J\u000f\u0010\u001a\u001a\u00020\u0016H\u0016¢\u0006\u0004\b\u001a\u0010\u001bJ\u0017\u0010\u001c\u001a\u00028\u00002\u0006\u0010\u0017\u001a\u00020\u0016H\u0016¢\u0006\u0004\b\u001c\u0010\u001dJ\u001d\u0010\u001f\u001a\u00020\u00062\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00028\u00000\u000bH\u0016¢\u0006\u0004\b\u001f\u0010 R0\u0010\"\u001a\b\u0012\u0004\u0012\u00028\u00000\u000b2\f\u0010!\u001a\b\u0012\u0004\u0012\u00028\u00000\u000b8\u0004@BX\u0084\u000e¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%R:\u0010(\u001a&\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u000b\u0012\u0016\u0012\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0000\u0012\u0004\u0012\u00028\u00000'0&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u0010)R\u0018\u0010+\u001a\u0004\u0018\u00010*8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b+\u0010,R\u0016\u0010.\u001a\u00020-8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b.\u0010/R6\u0010\u0011\u001a\"\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u000b\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u000b\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0011\u00100¨\u00065"}, d2 = {"Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "D", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;", "", "throwable", "", "handleError", "(Ljava/lang/Throwable;)V", "Landroidx/recyclerview/widget/DiffUtil$DiffResult;", "diffResult", "", "oldData", "newData", "dispatchUpdates", "(Landroidx/recyclerview/widget/DiffUtil$DiffResult;Ljava/util/List;Ljava/util/List;)V", "Lkotlin/Function2;", "onUpdated", "setOnUpdated", "(Lkotlin/jvm/functions/Function2;)V", "unsubscribeFromUpdates", "()V", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "getItemViewType", "(I)I", "getItemCount", "()I", "getItem", "(I)Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "data", "setData", "(Ljava/util/List;)V", "<set-?>", "internalData", "Ljava/util/List;", "getInternalData", "()Ljava/util/List;", "Lcom/discord/utilities/recycler/DiffCreator;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "diffCreator", "Lcom/discord/utilities/recycler/DiffCreator;", "Lrx/Subscription;", "diffingSubscription", "Lrx/Subscription;", "", "deferredDiffs", "Z", "Lkotlin/jvm/functions/Function2;", "Landroidx/recyclerview/widget/RecyclerView;", "recycler", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class MGRecyclerAdapterSimple<D extends MGRecyclerDataPayload> extends MGRecyclerAdapter<D> {
    private final boolean deferredDiffs;
    private final DiffCreator<List<D>, MGRecyclerViewHolder<MGRecyclerAdapterSimple<D>, D>> diffCreator;
    private Subscription diffingSubscription;
    private List<? extends D> internalData;
    private Function2<? super List<? extends D>, ? super List<? extends D>, Unit> onUpdated;

    public MGRecyclerAdapterSimple(RecyclerView recyclerView) {
        this(recyclerView, false, 2, null);
    }

    public /* synthetic */ MGRecyclerAdapterSimple(RecyclerView recyclerView, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(recyclerView, (i & 2) != 0 ? true : z2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @SuppressLint({"NotifyDataSetChanged"})
    public final void dispatchUpdates(DiffUtil.DiffResult diffResult, List<? extends D> list, List<? extends D> list2) {
        this.internalData = list2;
        if (diffResult != null) {
            diffResult.dispatchUpdatesTo(this);
        } else {
            notifyDataSetChanged();
        }
        Function2<? super List<? extends D>, ? super List<? extends D>, Unit> function2 = this.onUpdated;
        if (function2 != null) {
            function2.invoke(list, list2);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleError(Throwable th) {
        AppLog.g.e("Unable to configure recycler.", th, g0.mapOf(o.to("adapterClass", getClass().getSimpleName())));
    }

    public final List<D> getInternalData() {
        return (List<? extends D>) this.internalData;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.internalData.size();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        return getItem(i).getType();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public void setData(final List<? extends D> list) {
        m.checkNotNullParameter(list, "data");
        unsubscribeFromUpdates();
        final List<? extends D> list2 = this.internalData;
        if (list2.isEmpty()) {
            dispatchUpdates(null, list2, list);
        } else if (this.deferredDiffs) {
            Observable X = Observable.C(new Callable<DiffUtil.DiffResult>() { // from class: com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple$setData$1
                /* JADX WARN: Can't rename method to resolve collision */
                @Override // java.util.concurrent.Callable
                public final DiffUtil.DiffResult call() {
                    DiffCreator diffCreator;
                    diffCreator = MGRecyclerAdapterSimple.this.diffCreator;
                    return diffCreator.calculateDiffResult(list2, list);
                }
            }).X(a.a());
            m.checkNotNullExpressionValue(X, "Observable\n            .…Schedulers.computation())");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(X), getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new MGRecyclerAdapterSimple$setData$4(this), (r18 & 8) != 0 ? null : new MGRecyclerAdapterSimple$setData$3(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new MGRecyclerAdapterSimple$setData$2(this, list2, list));
        } else {
            try {
                dispatchUpdates(this.diffCreator.calculateDiffResult(list2, list), list2, list);
            } catch (Throwable th) {
                handleError(th);
            }
        }
    }

    public final void setOnUpdated(Function2<? super List<? extends D>, ? super List<? extends D>, Unit> function2) {
        m.checkNotNullParameter(function2, "onUpdated");
        this.onUpdated = function2;
    }

    public final void unsubscribeFromUpdates() {
        Subscription subscription = this.diffingSubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        this.diffingSubscription = null;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MGRecyclerAdapterSimple(RecyclerView recyclerView, boolean z2) {
        super(recyclerView);
        m.checkNotNullParameter(recyclerView, "recycler");
        this.deferredDiffs = z2;
        this.internalData = n.emptyList();
        this.diffCreator = new DiffCreator<>(null);
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerAdapter
    public D getItem(int i) {
        return this.internalData.get(i);
    }
}
