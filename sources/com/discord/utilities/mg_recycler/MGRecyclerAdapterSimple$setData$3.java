package com.discord.utilities.mg_recycler;

import com.discord.utilities.error.Error;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: MGRecyclerAdapterSimple.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u0004\"\b\b\u0000\u0010\u0001*\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "D", "Lcom/discord/utilities/error/Error;", "error", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MGRecyclerAdapterSimple$setData$3 extends o implements Function1<Error, Unit> {
    public final /* synthetic */ MGRecyclerAdapterSimple this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MGRecyclerAdapterSimple$setData$3(MGRecyclerAdapterSimple mGRecyclerAdapterSimple) {
        super(1);
        this.this$0 = mGRecyclerAdapterSimple;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Error error) {
        invoke2(error);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Error error) {
        m.checkNotNullParameter(error, "error");
        MGRecyclerAdapterSimple mGRecyclerAdapterSimple = this.this$0;
        Throwable throwable = error.getThrowable();
        m.checkNotNullExpressionValue(throwable, "error.throwable");
        mGRecyclerAdapterSimple.handleError(throwable);
    }
}
