package com.discord.utilities.mg_recycler;

import androidx.recyclerview.widget.DiffUtil;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: MGRecyclerAdapterSimple.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u0004\"\b\b\u0000\u0010\u0001*\u00020\u00002\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "D", "Landroidx/recyclerview/widget/DiffUtil$DiffResult;", "diffResult", "", "invoke", "(Landroidx/recyclerview/widget/DiffUtil$DiffResult;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MGRecyclerAdapterSimple$setData$2 extends o implements Function1<DiffUtil.DiffResult, Unit> {
    public final /* synthetic */ List $newData;
    public final /* synthetic */ List $oldData;
    public final /* synthetic */ MGRecyclerAdapterSimple this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MGRecyclerAdapterSimple$setData$2(MGRecyclerAdapterSimple mGRecyclerAdapterSimple, List list, List list2) {
        super(1);
        this.this$0 = mGRecyclerAdapterSimple;
        this.$oldData = list;
        this.$newData = list2;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(DiffUtil.DiffResult diffResult) {
        invoke2(diffResult);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(DiffUtil.DiffResult diffResult) {
        this.this$0.dispatchUpdates(diffResult, this.$oldData, this.$newData);
    }
}
