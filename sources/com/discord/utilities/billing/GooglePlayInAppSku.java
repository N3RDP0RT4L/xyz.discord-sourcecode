package com.discord.utilities.billing;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.android.billingclient.api.SkuDetails;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: GooglePlayInAppSku.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0011\b\u0086\b\u0018\u00002\u00020\u0001B9\u0012\n\u0010\u0010\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\u0011\u001a\u00020\u0006\u0012\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\t\u0012\u0010\b\u0002\u0010\u0013\u001a\n\u0018\u00010\fj\u0004\u0018\u0001`\r¢\u0006\u0004\b*\u0010+J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0018\u0010\u000e\u001a\n\u0018\u00010\fj\u0004\u0018\u0001`\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJF\u0010\u0014\u001a\u00020\u00002\f\b\u0002\u0010\u0010\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u0011\u001a\u00020\u00062\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\t2\u0010\b\u0002\u0010\u0013\u001a\n\u0018\u00010\fj\u0004\u0018\u0001`\rHÆ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0005J\u0010\u0010\u0018\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u001a\u0010\u001c\u001a\u00020\u001b2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR$\u0010\u0012\u001a\u0004\u0018\u00010\t8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0012\u0010\u001e\u001a\u0004\b\u001f\u0010\u000b\"\u0004\b \u0010!R!\u0010\u0013\u001a\n\u0018\u00010\fj\u0004\u0018\u0001`\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\"\u001a\u0004\b#\u0010\u000fR\u001d\u0010\u0010\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010$\u001a\u0004\b%\u0010\u0005R\"\u0010\u0011\u001a\u00020\u00068\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0011\u0010&\u001a\u0004\b'\u0010\b\"\u0004\b(\u0010)¨\u0006,"}, d2 = {"Lcom/discord/utilities/billing/GooglePlayInAppSku;", "", "", "Lcom/discord/primitives/PaymentGatewaySkuId;", "component1", "()Ljava/lang/String;", "Lcom/discord/utilities/billing/InAppSkuType;", "component2", "()Lcom/discord/utilities/billing/InAppSkuType;", "Lcom/android/billingclient/api/SkuDetails;", "component3", "()Lcom/android/billingclient/api/SkuDetails;", "", "Lcom/discord/primitives/SkuId;", "component4", "()Ljava/lang/Long;", "paymentGatewaySkuId", "type", "skuDetails", "skuId", "copy", "(Ljava/lang/String;Lcom/discord/utilities/billing/InAppSkuType;Lcom/android/billingclient/api/SkuDetails;Ljava/lang/Long;)Lcom/discord/utilities/billing/GooglePlayInAppSku;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/android/billingclient/api/SkuDetails;", "getSkuDetails", "setSkuDetails", "(Lcom/android/billingclient/api/SkuDetails;)V", "Ljava/lang/Long;", "getSkuId", "Ljava/lang/String;", "getPaymentGatewaySkuId", "Lcom/discord/utilities/billing/InAppSkuType;", "getType", "setType", "(Lcom/discord/utilities/billing/InAppSkuType;)V", HookHelper.constructorName, "(Ljava/lang/String;Lcom/discord/utilities/billing/InAppSkuType;Lcom/android/billingclient/api/SkuDetails;Ljava/lang/Long;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GooglePlayInAppSku {
    private final String paymentGatewaySkuId;
    private SkuDetails skuDetails;
    private final Long skuId;
    private InAppSkuType type;

    public GooglePlayInAppSku(String str, InAppSkuType inAppSkuType, SkuDetails skuDetails, Long l) {
        m.checkNotNullParameter(str, "paymentGatewaySkuId");
        m.checkNotNullParameter(inAppSkuType, "type");
        this.paymentGatewaySkuId = str;
        this.type = inAppSkuType;
        this.skuDetails = skuDetails;
        this.skuId = l;
    }

    public static /* synthetic */ GooglePlayInAppSku copy$default(GooglePlayInAppSku googlePlayInAppSku, String str, InAppSkuType inAppSkuType, SkuDetails skuDetails, Long l, int i, Object obj) {
        if ((i & 1) != 0) {
            str = googlePlayInAppSku.paymentGatewaySkuId;
        }
        if ((i & 2) != 0) {
            inAppSkuType = googlePlayInAppSku.type;
        }
        if ((i & 4) != 0) {
            skuDetails = googlePlayInAppSku.skuDetails;
        }
        if ((i & 8) != 0) {
            l = googlePlayInAppSku.skuId;
        }
        return googlePlayInAppSku.copy(str, inAppSkuType, skuDetails, l);
    }

    public final String component1() {
        return this.paymentGatewaySkuId;
    }

    public final InAppSkuType component2() {
        return this.type;
    }

    public final SkuDetails component3() {
        return this.skuDetails;
    }

    public final Long component4() {
        return this.skuId;
    }

    public final GooglePlayInAppSku copy(String str, InAppSkuType inAppSkuType, SkuDetails skuDetails, Long l) {
        m.checkNotNullParameter(str, "paymentGatewaySkuId");
        m.checkNotNullParameter(inAppSkuType, "type");
        return new GooglePlayInAppSku(str, inAppSkuType, skuDetails, l);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GooglePlayInAppSku)) {
            return false;
        }
        GooglePlayInAppSku googlePlayInAppSku = (GooglePlayInAppSku) obj;
        return m.areEqual(this.paymentGatewaySkuId, googlePlayInAppSku.paymentGatewaySkuId) && m.areEqual(this.type, googlePlayInAppSku.type) && m.areEqual(this.skuDetails, googlePlayInAppSku.skuDetails) && m.areEqual(this.skuId, googlePlayInAppSku.skuId);
    }

    public final String getPaymentGatewaySkuId() {
        return this.paymentGatewaySkuId;
    }

    public final SkuDetails getSkuDetails() {
        return this.skuDetails;
    }

    public final Long getSkuId() {
        return this.skuId;
    }

    public final InAppSkuType getType() {
        return this.type;
    }

    public int hashCode() {
        String str = this.paymentGatewaySkuId;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        InAppSkuType inAppSkuType = this.type;
        int hashCode2 = (hashCode + (inAppSkuType != null ? inAppSkuType.hashCode() : 0)) * 31;
        SkuDetails skuDetails = this.skuDetails;
        int hashCode3 = (hashCode2 + (skuDetails != null ? skuDetails.hashCode() : 0)) * 31;
        Long l = this.skuId;
        if (l != null) {
            i = l.hashCode();
        }
        return hashCode3 + i;
    }

    public final void setSkuDetails(SkuDetails skuDetails) {
        this.skuDetails = skuDetails;
    }

    public final void setType(InAppSkuType inAppSkuType) {
        m.checkNotNullParameter(inAppSkuType, "<set-?>");
        this.type = inAppSkuType;
    }

    public String toString() {
        StringBuilder R = a.R("GooglePlayInAppSku(paymentGatewaySkuId=");
        R.append(this.paymentGatewaySkuId);
        R.append(", type=");
        R.append(this.type);
        R.append(", skuDetails=");
        R.append(this.skuDetails);
        R.append(", skuId=");
        return a.F(R, this.skuId, ")");
    }

    public /* synthetic */ GooglePlayInAppSku(String str, InAppSkuType inAppSkuType, SkuDetails skuDetails, Long l, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(str, inAppSkuType, (i & 4) != 0 ? null : skuDetails, (i & 8) != 0 ? null : l);
    }
}
