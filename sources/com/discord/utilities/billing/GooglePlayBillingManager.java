package com.discord.utilities.billing;

import andhook.lib.HookHelper;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import androidx.appcompat.widget.ActivityChooserModel;
import b.a.d.j;
import b.d.a.a.b;
import b.d.a.a.d;
import b.d.a.a.e;
import b.d.a.a.g;
import b.d.a.a.h;
import b.d.a.a.i;
import b.d.b.a.a;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.SkuDetails;
import com.discord.app.AppActivity;
import com.discord.app.AppLog;
import com.discord.stores.StoreGooglePlaySkuDetails;
import com.discord.stores.StoreStream;
import com.discord.utilities.KotlinExtensionsKt;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.rx.ActivityLifecycleCallbacks;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.t.n;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import rx.Observable;
/* compiled from: GooglePlayBillingManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0094\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u0002JKB\t\b\u0002¢\u0006\u0004\bI\u0010\u0007J\u000f\u0010\u0006\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u001f\u0010\f\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\f\u0010\rJ\u0017\u0010\u0010\u001a\u00020\u00052\u0006\u0010\u000f\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u0017\u0010\u0012\u001a\u00020\u00052\u0006\u0010\u000f\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0012\u0010\u0011J\u0017\u0010\u0013\u001a\u00020\u00052\u0006\u0010\u000f\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0013\u0010\u0011J\u0015\u0010\u0016\u001a\u00020\u00052\u0006\u0010\u0015\u001a\u00020\u0014¢\u0006\u0004\b\u0016\u0010\u0017J\r\u0010\u0018\u001a\u00020\u0005¢\u0006\u0004\b\u0018\u0010\u0007J\r\u0010\u0019\u001a\u00020\u0005¢\u0006\u0004\b\u0019\u0010\u0007J\u0017\u0010\u001c\u001a\u00020\u00052\u0006\u0010\u001b\u001a\u00020\u001aH\u0016¢\u0006\u0004\b\u001c\u0010\u001dJ\u000f\u0010\u001e\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u001e\u0010\u0007J\u001d\u0010$\u001a\u00020#2\u0006\u0010 \u001a\u00020\u001f2\u0006\u0010\"\u001a\u00020!¢\u0006\u0004\b$\u0010%J\r\u0010&\u001a\u00020\u0005¢\u0006\u0004\b&\u0010\u0007J'\u0010*\u001a\u00020\u00052\u0006\u0010\u001b\u001a\u00020\u001a2\u000e\u0010)\u001a\n\u0012\u0004\u0012\u00020(\u0018\u00010'H\u0016¢\u0006\u0004\b*\u0010+J\r\u0010,\u001a\u00020\u0005¢\u0006\u0004\b,\u0010\u0007J'\u0010.\u001a\u00020\u00052\u0006\u0010\u001b\u001a\u00020\u001a2\u000e\u0010-\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010'H\u0016¢\u0006\u0004\b.\u0010+J-\u00104\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\b2\u0006\u00100\u001a\u00020/2\u000e\u00103\u001a\n\u0018\u000101j\u0004\u0018\u0001`2¢\u0006\u0004\b4\u00105J\u001f\u00106\u001a\u00020\u00052\u0006\u0010\u001b\u001a\u00020\u001a2\u0006\u0010\u000f\u001a\u00020\u000eH\u0016¢\u0006\u0004\b6\u00107R\u0016\u00109\u001a\u0002088\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b9\u0010:R\u0016\u0010<\u001a\u00020;8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b<\u0010=R\u0016\u0010>\u001a\u0002018\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b>\u0010?R\u0016\u0010@\u001a\u00020\u000e8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b@\u0010AR\u0016\u0010B\u001a\u00020\u000e8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bB\u0010AR\u0016\u0010D\u001a\u00020C8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bD\u0010ER$\u0010G\u001a\u0010\u0012\u0004\u0012\u00020\u000e\u0012\u0006\u0012\u0004\u0018\u00010\n0F8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bG\u0010H¨\u0006L"}, d2 = {"Lcom/discord/utilities/billing/GooglePlayBillingManager;", "Lb/d/a/a/g;", "Lb/d/a/a/b;", "Lb/d/a/a/i;", "Lb/d/a/a/e;", "", "queryInAppSkuDetails", "()V", "Lcom/android/billingclient/api/Purchase;", "purchase", "Lcom/discord/utilities/billing/GooglePlayBillingManager$InAppSkuToConsume;", "inAppSkuToConsume", "handleConsumeStart", "(Lcom/android/billingclient/api/Purchase;Lcom/discord/utilities/billing/GooglePlayBillingManager$InAppSkuToConsume;)V", "", "purchaseToken", "handleConsumeSuccess", "(Ljava/lang/String;)V", "handleConsumeFailure", "handleConsumeEnd", "Landroid/app/Application;", "application", "init", "(Landroid/app/Application;)V", "onActivityCreated", "onActivityDestroyed", "Lcom/android/billingclient/api/BillingResult;", "billingResult", "onBillingSetupFinished", "(Lcom/android/billingclient/api/BillingResult;)V", "onBillingServiceDisconnected", "Landroid/app/Activity;", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "Lcom/android/billingclient/api/BillingFlowParams;", "params", "", "launchBillingFlow", "(Landroid/app/Activity;Lcom/android/billingclient/api/BillingFlowParams;)I", "querySkuDetails", "", "Lcom/android/billingclient/api/SkuDetails;", "skuDetails", "onSkuDetailsResponse", "(Lcom/android/billingclient/api/BillingResult;Ljava/util/List;)V", "queryPurchases", "purchases", "onPurchasesUpdated", "Lcom/discord/utilities/billing/InAppSkuType;", "inAppSkuType", "", "Lcom/discord/primitives/SkuId;", "skuId", "consumePurchase", "(Lcom/android/billingclient/api/Purchase;Lcom/discord/utilities/billing/InAppSkuType;Ljava/lang/Long;)V", "onConsumeResponse", "(Lcom/android/billingclient/api/BillingResult;Ljava/lang/String;)V", "", "isAuthenticated", "Z", "Lcom/android/billingclient/api/BillingClient;", "billingClient", "Lcom/android/billingclient/api/BillingClient;", "DEFAULT_BACKOFF_TIME_MS", "J", "PLAY_STORE_SUBSCRIPTION_URL", "Ljava/lang/String;", "PLAY_STORE_SUBSCRIPTION_DEEPLINK_URL", "Ljava/util/concurrent/atomic/AtomicLong;", "backoffTimeMs", "Ljava/util/concurrent/atomic/AtomicLong;", "", "inAppSkusToConsume", "Ljava/util/Map;", HookHelper.constructorName, "GooglePlayBillingManagerLifecycleListener", "InAppSkuToConsume", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GooglePlayBillingManager implements g, b, i, e {
    private static final long DEFAULT_BACKOFF_TIME_MS = 1000;
    public static final String PLAY_STORE_SUBSCRIPTION_DEEPLINK_URL = "https://play.google.com/store/account/subscriptions?sku=%s&package=%s";
    public static final String PLAY_STORE_SUBSCRIPTION_URL = "https://play.google.com/store/account/subscriptions";
    private static BillingClient billingClient;
    private static boolean isAuthenticated;
    public static final GooglePlayBillingManager INSTANCE = new GooglePlayBillingManager();
    private static AtomicLong backoffTimeMs = new AtomicLong(1000);
    private static Map<String, InAppSkuToConsume> inAppSkusToConsume = new HashMap();

    /* compiled from: GooglePlayBillingManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\b\b\u0002\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\f\u0010\rJ!\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\t\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\u000b\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u000b\u0010\n¨\u0006\u000e"}, d2 = {"Lcom/discord/utilities/billing/GooglePlayBillingManager$GooglePlayBillingManagerLifecycleListener;", "Lcom/discord/utilities/rx/ActivityLifecycleCallbacks;", "Lcom/discord/app/AppActivity;", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "Landroid/os/Bundle;", "savedInstanceState", "", "onActivityCreated", "(Lcom/discord/app/AppActivity;Landroid/os/Bundle;)V", "onActivityResumed", "(Lcom/discord/app/AppActivity;)V", "onActivityDestroyed", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class GooglePlayBillingManagerLifecycleListener extends ActivityLifecycleCallbacks {
        @Override // com.discord.utilities.rx.ActivityLifecycleCallbacks
        public void onActivityCreated(AppActivity appActivity, Bundle bundle) {
            m.checkNotNullParameter(appActivity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
            super.onActivityCreated(appActivity, bundle);
            j jVar = j.g;
            if (appActivity.g(j.f)) {
                GooglePlayBillingManager googlePlayBillingManager = GooglePlayBillingManager.INSTANCE;
                if (GooglePlayBillingManager.isAuthenticated) {
                    googlePlayBillingManager.onActivityCreated();
                }
            }
        }

        @Override // com.discord.utilities.rx.ActivityLifecycleCallbacks
        public void onActivityDestroyed(AppActivity appActivity) {
            m.checkNotNullParameter(appActivity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
            super.onActivityDestroyed(appActivity);
            j jVar = j.g;
            if (appActivity.g(j.f)) {
                GooglePlayBillingManager.INSTANCE.onActivityDestroyed();
            }
        }

        @Override // com.discord.utilities.rx.ActivityLifecycleCallbacks
        public void onActivityResumed(AppActivity appActivity) {
            m.checkNotNullParameter(appActivity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
            super.onActivityResumed(appActivity);
            j jVar = j.g;
            if (appActivity.g(j.f)) {
                GooglePlayBillingManager googlePlayBillingManager = GooglePlayBillingManager.INSTANCE;
                if (GooglePlayBillingManager.isAuthenticated) {
                    googlePlayBillingManager.queryPurchases();
                }
            }
        }
    }

    /* compiled from: GooglePlayBillingManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\r\u001a\u00020\u0002\u0012\u000e\u0010\u000e\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006\u0012\n\u0010\u000f\u001a\u00060\tj\u0002`\n¢\u0006\u0004\b \u0010!J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0018\u0010\u0007\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0014\u0010\u000b\u001a\u00060\tj\u0002`\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ:\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\r\u001a\u00020\u00022\u0010\b\u0002\u0010\u000e\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00062\f\b\u0002\u0010\u000f\u001a\u00060\tj\u0002`\nHÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\u0012\u0010\fJ\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0018\u001a\u00020\u00172\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u0019\u0010\r\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001a\u001a\u0004\b\u001b\u0010\u0004R!\u0010\u000e\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001c\u001a\u0004\b\u001d\u0010\bR\u001d\u0010\u000f\u001a\u00060\tj\u0002`\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001e\u001a\u0004\b\u001f\u0010\f¨\u0006\""}, d2 = {"Lcom/discord/utilities/billing/GooglePlayBillingManager$InAppSkuToConsume;", "", "Lcom/discord/utilities/billing/InAppSkuType;", "component1", "()Lcom/discord/utilities/billing/InAppSkuType;", "", "Lcom/discord/primitives/SkuId;", "component2", "()Ljava/lang/Long;", "", "Lcom/discord/primitives/PaymentGatewaySkuId;", "component3", "()Ljava/lang/String;", "type", "skuId", "paymentGatewaySkuId", "copy", "(Lcom/discord/utilities/billing/InAppSkuType;Ljava/lang/Long;Ljava/lang/String;)Lcom/discord/utilities/billing/GooglePlayBillingManager$InAppSkuToConsume;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/utilities/billing/InAppSkuType;", "getType", "Ljava/lang/Long;", "getSkuId", "Ljava/lang/String;", "getPaymentGatewaySkuId", HookHelper.constructorName, "(Lcom/discord/utilities/billing/InAppSkuType;Ljava/lang/Long;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class InAppSkuToConsume {
        private final String paymentGatewaySkuId;
        private final Long skuId;
        private final InAppSkuType type;

        public InAppSkuToConsume(InAppSkuType inAppSkuType, Long l, String str) {
            m.checkNotNullParameter(inAppSkuType, "type");
            m.checkNotNullParameter(str, "paymentGatewaySkuId");
            this.type = inAppSkuType;
            this.skuId = l;
            this.paymentGatewaySkuId = str;
        }

        public static /* synthetic */ InAppSkuToConsume copy$default(InAppSkuToConsume inAppSkuToConsume, InAppSkuType inAppSkuType, Long l, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                inAppSkuType = inAppSkuToConsume.type;
            }
            if ((i & 2) != 0) {
                l = inAppSkuToConsume.skuId;
            }
            if ((i & 4) != 0) {
                str = inAppSkuToConsume.paymentGatewaySkuId;
            }
            return inAppSkuToConsume.copy(inAppSkuType, l, str);
        }

        public final InAppSkuType component1() {
            return this.type;
        }

        public final Long component2() {
            return this.skuId;
        }

        public final String component3() {
            return this.paymentGatewaySkuId;
        }

        public final InAppSkuToConsume copy(InAppSkuType inAppSkuType, Long l, String str) {
            m.checkNotNullParameter(inAppSkuType, "type");
            m.checkNotNullParameter(str, "paymentGatewaySkuId");
            return new InAppSkuToConsume(inAppSkuType, l, str);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof InAppSkuToConsume)) {
                return false;
            }
            InAppSkuToConsume inAppSkuToConsume = (InAppSkuToConsume) obj;
            return m.areEqual(this.type, inAppSkuToConsume.type) && m.areEqual(this.skuId, inAppSkuToConsume.skuId) && m.areEqual(this.paymentGatewaySkuId, inAppSkuToConsume.paymentGatewaySkuId);
        }

        public final String getPaymentGatewaySkuId() {
            return this.paymentGatewaySkuId;
        }

        public final Long getSkuId() {
            return this.skuId;
        }

        public final InAppSkuType getType() {
            return this.type;
        }

        public int hashCode() {
            InAppSkuType inAppSkuType = this.type;
            int i = 0;
            int hashCode = (inAppSkuType != null ? inAppSkuType.hashCode() : 0) * 31;
            Long l = this.skuId;
            int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
            String str = this.paymentGatewaySkuId;
            if (str != null) {
                i = str.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("InAppSkuToConsume(type=");
            R.append(this.type);
            R.append(", skuId=");
            R.append(this.skuId);
            R.append(", paymentGatewaySkuId=");
            return a.H(R, this.paymentGatewaySkuId, ")");
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;
        public static final /* synthetic */ int[] $EnumSwitchMapping$2 = new int[1];

        static {
            InAppSkuType.values();
            int[] iArr = new int[1];
            $EnumSwitchMapping$0 = iArr;
            InAppSkuType inAppSkuType = InAppSkuType.PREMIUM_GIFT;
            iArr[inAppSkuType.ordinal()] = 1;
            InAppSkuType.values();
            int[] iArr2 = new int[1];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[inAppSkuType.ordinal()] = 1;
            InAppSkuType.values();
        }
    }

    private GooglePlayBillingManager() {
    }

    public static final /* synthetic */ BillingClient access$getBillingClient$p(GooglePlayBillingManager googlePlayBillingManager) {
        BillingClient billingClient2 = billingClient;
        if (billingClient2 == null) {
            m.throwUninitializedPropertyAccessException("billingClient");
        }
        return billingClient2;
    }

    private final void handleConsumeEnd(String str) {
        InAppSkuToConsume inAppSkuToConsume = inAppSkusToConsume.get(str);
        if (inAppSkuToConsume != null) {
            inAppSkuToConsume.getType();
        }
    }

    private final void handleConsumeFailure(String str) {
        InAppSkuToConsume inAppSkuToConsume = inAppSkusToConsume.get(str);
        if (inAppSkuToConsume != null) {
            StoreStream.Companion.getGooglePlayPurchases().trackPaymentFlowFailed(inAppSkuToConsume.getPaymentGatewaySkuId());
        }
    }

    private final void handleConsumeStart(Purchase purchase, InAppSkuToConsume inAppSkuToConsume) {
        Map<String, InAppSkuToConsume> map = inAppSkusToConsume;
        String a = purchase.a();
        m.checkNotNullExpressionValue(a, "purchase.purchaseToken");
        map.put(a, inAppSkuToConsume);
        if (inAppSkuToConsume.getType().ordinal() == 0) {
            KotlinExtensionsKt.getExhaustive(Unit.a);
            return;
        }
        throw new NoWhenBranchMatchedException();
    }

    private final void handleConsumeSuccess(String str) {
        InAppSkuToConsume inAppSkuToConsume = inAppSkusToConsume.get(str);
        if (inAppSkuToConsume != null) {
            StoreStream.Companion.getGooglePlayPurchases().trackPaymentFlowCompleted(inAppSkuToConsume.getPaymentGatewaySkuId());
            if (inAppSkuToConsume.getType().ordinal() == 0) {
                KotlinExtensionsKt.getExhaustive(Unit.a);
                return;
            }
            throw new NoWhenBranchMatchedException();
        }
    }

    private final void queryInAppSkuDetails() {
        BillingClient billingClient2 = billingClient;
        if (billingClient2 == null) {
            m.throwUninitializedPropertyAccessException("billingClient");
        }
        if (billingClient2.d()) {
            List<GooglePlayInAppSku> skus = GooglePlayInAppSkus.INSTANCE.getSkus();
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(skus, 10));
            for (GooglePlayInAppSku googlePlayInAppSku : skus) {
                arrayList.add(googlePlayInAppSku.getPaymentGatewaySkuId());
            }
            ArrayList arrayList2 = new ArrayList(arrayList);
            h hVar = new h();
            hVar.a = "inapp";
            hVar.f446b = arrayList2;
            m.checkNotNullExpressionValue(hVar, "SkuDetailsParams.newBuil…kuNames)\n        .build()");
            BillingClient billingClient3 = billingClient;
            if (billingClient3 == null) {
                m.throwUninitializedPropertyAccessException("billingClient");
            }
            billingClient3.g(hVar, this);
        }
    }

    public final void consumePurchase(Purchase purchase, InAppSkuType inAppSkuType, Long l) {
        m.checkNotNullParameter(purchase, "purchase");
        m.checkNotNullParameter(inAppSkuType, "inAppSkuType");
        BillingClient billingClient2 = billingClient;
        if (billingClient2 == null) {
            m.throwUninitializedPropertyAccessException("billingClient");
        }
        if (billingClient2.d()) {
            String a = purchase.a();
            if (a != null) {
                d dVar = new d();
                dVar.a = a;
                m.checkNotNullExpressionValue(dVar, "ConsumeParams.newBuilder…se.purchaseToken).build()");
                BillingClient billingClient3 = billingClient;
                if (billingClient3 == null) {
                    m.throwUninitializedPropertyAccessException("billingClient");
                }
                billingClient3.a(dVar, this);
                ArrayList<String> b2 = purchase.b();
                m.checkNotNullExpressionValue(b2, "purchase.skus");
                for (String str : b2) {
                    GooglePlayBillingManager googlePlayBillingManager = INSTANCE;
                    m.checkNotNullExpressionValue(str, "sku");
                    googlePlayBillingManager.handleConsumeStart(purchase, new InAppSkuToConsume(inAppSkuType, l, str));
                }
                return;
            }
            throw new IllegalArgumentException("Purchase token must be set");
        }
    }

    public final void init(Application application) {
        m.checkNotNullParameter(application, "application");
        Context applicationContext = application.getApplicationContext();
        if (applicationContext != null) {
            b.d.a.a.a aVar = new b.d.a.a.a(null, true, applicationContext, this);
            m.checkNotNullExpressionValue(aVar, "BillingClient.newBuilder…chases()\n        .build()");
            billingClient = aVar;
            application.registerActivityLifecycleCallbacks(new GooglePlayBillingManagerLifecycleListener());
            ObservableExtensionsKt.appSubscribe(StoreStream.Companion.getAuthentication().observeIsAuthed$app_productionGoogleRelease(), GooglePlayBillingManager.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, GooglePlayBillingManager$init$1.INSTANCE);
            return;
        }
        throw new IllegalArgumentException("Please provide a valid Context.");
    }

    public final int launchBillingFlow(Activity activity, BillingFlowParams billingFlowParams) {
        m.checkNotNullParameter(activity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
        m.checkNotNullParameter(billingFlowParams, "params");
        BillingClient billingClient2 = billingClient;
        if (billingClient2 == null) {
            m.throwUninitializedPropertyAccessException("billingClient");
        }
        if (!billingClient2.d()) {
            return -1;
        }
        BillingClient billingClient3 = billingClient;
        if (billingClient3 == null) {
            m.throwUninitializedPropertyAccessException("billingClient");
        }
        BillingResult e = billingClient3.e(activity, billingFlowParams);
        m.checkNotNullExpressionValue(e, "billingClient.launchBillingFlow(activity, params)");
        return e.a;
    }

    public final void onActivityCreated() {
        BillingClient billingClient2 = billingClient;
        if (billingClient2 == null) {
            m.throwUninitializedPropertyAccessException("billingClient");
        }
        if (!billingClient2.d()) {
            BillingClient billingClient3 = billingClient;
            if (billingClient3 == null) {
                m.throwUninitializedPropertyAccessException("billingClient");
            }
            billingClient3.h(this);
        }
    }

    public final void onActivityDestroyed() {
        BillingClient billingClient2 = billingClient;
        if (billingClient2 == null) {
            m.throwUninitializedPropertyAccessException("billingClient");
        }
        if (billingClient2.d()) {
            BillingClient billingClient3 = billingClient;
            if (billingClient3 == null) {
                m.throwUninitializedPropertyAccessException("billingClient");
            }
            billingClient3.b();
        }
    }

    @Override // b.d.a.a.b
    public void onBillingServiceDisconnected() {
        BillingClient billingClient2 = billingClient;
        if (billingClient2 == null) {
            m.throwUninitializedPropertyAccessException("billingClient");
        }
        if (billingClient2.c() != 0) {
            BillingClient billingClient3 = billingClient;
            if (billingClient3 == null) {
                m.throwUninitializedPropertyAccessException("billingClient");
            }
            if (billingClient3.c() != 3) {
                return;
            }
        }
        long j = backoffTimeMs.get();
        Observable<Long> d02 = Observable.d0(j, TimeUnit.MILLISECONDS);
        m.checkNotNullExpressionValue(d02, "Observable\n          .ti…s, TimeUnit.MILLISECONDS)");
        ObservableExtensionsKt.appSubscribe(d02, GooglePlayBillingManager.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new GooglePlayBillingManager$onBillingServiceDisconnected$1(j));
    }

    @Override // b.d.a.a.b
    public void onBillingSetupFinished(BillingResult billingResult) {
        m.checkNotNullParameter(billingResult, "billingResult");
        if (billingResult.a == 0) {
            querySkuDetails();
            queryInAppSkuDetails();
            queryPurchases();
            backoffTimeMs.set(1000L);
        }
    }

    @Override // b.d.a.a.e
    public void onConsumeResponse(BillingResult billingResult, String str) {
        m.checkNotNullParameter(billingResult, "billingResult");
        m.checkNotNullParameter(str, "purchaseToken");
        if (billingResult.a == 0) {
            handleConsumeSuccess(str);
        } else {
            handleConsumeFailure(str);
            AppLog appLog = AppLog.g;
            StringBuilder V = a.V("Failed to consume purchase. ", "Billing Response Code: ");
            V.append(billingResult.a);
            V.append(", ");
            V.append("Purchase Token: ");
            V.append(str);
            Logger.e$default(appLog, V.toString(), null, null, 6, null);
        }
        handleConsumeEnd(str);
    }

    @Override // b.d.a.a.g
    public void onPurchasesUpdated(BillingResult billingResult, List<? extends Purchase> list) {
        boolean z2;
        m.checkNotNullParameter(billingResult, "billingResult");
        if (billingResult.a != 0) {
            StoreStream.Companion.getGooglePlayPurchases().updatePendingDowngrade(null);
        }
        int i = billingResult.a;
        if (i == 0) {
            if (list == null || list.isEmpty()) {
                StoreStream.Companion.getGooglePlayPurchases().downgradePurchase();
            }
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            if (list != null) {
                for (Purchase purchase : list) {
                    ArrayList<String> b2 = purchase.b();
                    m.checkNotNullExpressionValue(b2, "purchase.skus");
                    if (!b2.isEmpty()) {
                        for (String str : b2) {
                            GooglePlayInAppSkus googlePlayInAppSkus = GooglePlayInAppSkus.INSTANCE;
                            m.checkNotNullExpressionValue(str, "sku");
                            if (googlePlayInAppSkus.isInAppSku(str)) {
                                z2 = true;
                                break;
                            }
                        }
                    }
                    z2 = false;
                    if (z2) {
                        arrayList2.add(purchase);
                    } else {
                        arrayList.add(purchase);
                    }
                }
            }
            if (!arrayList2.isEmpty()) {
                StoreStream.Companion.getGooglePlayPurchases().processPurchases(arrayList2, "inapp");
            }
            if (!arrayList.isEmpty()) {
                StoreStream.Companion.getGooglePlayPurchases().processPurchases(arrayList, "subs");
            }
        } else if (i == 1) {
            AppLog.i("onPurchasesUpdated: User canceled the purchase");
        } else if (i == 5) {
            Logger.e$default(AppLog.g, "onPurchasesUpdated: Google Play doesn't recognize this app config. Verify the SKU product ID and the signed APK you are using.", null, null, 6, null);
        } else if (i == 7) {
            AppLog.i("onPurchasesUpdated: The user already owns this item");
        }
    }

    @Override // b.d.a.a.i
    public void onSkuDetailsResponse(BillingResult billingResult, List<? extends SkuDetails> list) {
        m.checkNotNullParameter(billingResult, "billingResult");
        int i = billingResult.a;
        String str = billingResult.f1999b;
        m.checkNotNullExpressionValue(str, "billingResult.debugMessage");
        switch (i) {
            case -2:
            case 1:
            case 7:
            case 8:
                StoreStream.Companion.getGooglePlaySkuDetails().handleError();
                AppLog appLog = AppLog.g;
                Logger.e$default(appLog, "onSkuDetailsResponse: " + i + ' ' + str, null, null, 6, null);
                return;
            case -1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                StoreStream.Companion.getGooglePlaySkuDetails().handleError();
                return;
            case 0:
                StoreGooglePlaySkuDetails googlePlaySkuDetails = StoreStream.Companion.getGooglePlaySkuDetails();
                if (list == null) {
                    list = n.emptyList();
                }
                googlePlaySkuDetails.updateSkuDetails(list);
                return;
            default:
                return;
        }
    }

    public final void queryPurchases() {
        BillingClient billingClient2 = billingClient;
        if (billingClient2 == null) {
            m.throwUninitializedPropertyAccessException("billingClient");
        }
        if (billingClient2.d()) {
            BillingClient billingClient3 = billingClient;
            if (billingClient3 == null) {
                m.throwUninitializedPropertyAccessException("billingClient");
            }
            billingClient3.f("subs", GooglePlayBillingManager$queryPurchases$1.INSTANCE);
            BillingClient billingClient4 = billingClient;
            if (billingClient4 == null) {
                m.throwUninitializedPropertyAccessException("billingClient");
            }
            billingClient4.f("inapp", GooglePlayBillingManager$queryPurchases$2.INSTANCE);
        }
    }

    public final void querySkuDetails() {
        BillingClient billingClient2 = billingClient;
        if (billingClient2 == null) {
            m.throwUninitializedPropertyAccessException("billingClient");
        }
        if (billingClient2.d()) {
            ArrayList arrayList = new ArrayList(GooglePlaySku.Companion.getALL_SKU_NAMES());
            h hVar = new h();
            hVar.a = "subs";
            hVar.f446b = arrayList;
            m.checkNotNullExpressionValue(hVar, "SkuDetailsParams.newBuil…U_NAMES)\n        .build()");
            BillingClient billingClient3 = billingClient;
            if (billingClient3 == null) {
                m.throwUninitializedPropertyAccessException("billingClient");
            }
            billingClient3.g(hVar, this);
        }
    }
}
