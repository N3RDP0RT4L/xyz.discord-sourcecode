package com.discord.utilities.billing;

import andhook.lib.HookHelper;
import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;
import com.discord.api.premium.SubscriptionInterval;
import d0.d0.f;
import d0.t.g0;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* JADX WARN: Init of enum PREMIUM_GUILD_1_MONTHLY can be incorrect */
/* JADX WARN: Init of enum PREMIUM_GUILD_2_MONTHLY can be incorrect */
/* JADX WARN: Init of enum PREMIUM_TIER_1_MONTHLY can be incorrect */
/* JADX WARN: Init of enum PREMIUM_TIER_1_PREMIUM_GUILD_1_MONTHLY can be incorrect */
/* JADX WARN: Init of enum PREMIUM_TIER_1_PREMIUM_GUILD_1_YEARLY can be incorrect */
/* JADX WARN: Init of enum PREMIUM_TIER_1_YEARLY can be incorrect */
/* JADX WARN: Init of enum PREMIUM_TIER_2_MONTHLY can be incorrect */
/* JADX WARN: Init of enum PREMIUM_TIER_2_PREMIUM_GUILD_10_MONTHLY can be incorrect */
/* JADX WARN: Init of enum PREMIUM_TIER_2_PREMIUM_GUILD_13_MONTHLY can be incorrect */
/* JADX WARN: Init of enum PREMIUM_TIER_2_PREMIUM_GUILD_1_MONTHLY can be incorrect */
/* JADX WARN: Init of enum PREMIUM_TIER_2_PREMIUM_GUILD_1_YEARLY can be incorrect */
/* JADX WARN: Init of enum PREMIUM_TIER_2_PREMIUM_GUILD_28_MONTHLY can be incorrect */
/* JADX WARN: Init of enum PREMIUM_TIER_2_PREMIUM_GUILD_2_MONTHLY can be incorrect */
/* JADX WARN: Init of enum PREMIUM_TIER_2_PREMIUM_GUILD_2_YEARLY can be incorrect */
/* JADX WARN: Init of enum PREMIUM_TIER_2_PREMIUM_GUILD_3_MONTHLY can be incorrect */
/* JADX WARN: Init of enum PREMIUM_TIER_2_PREMIUM_GUILD_3_YEARLY can be incorrect */
/* JADX WARN: Init of enum PREMIUM_TIER_2_PREMIUM_GUILD_5_MONTHLY can be incorrect */
/* JADX WARN: Init of enum PREMIUM_TIER_2_PREMIUM_GUILD_5_YEARLY can be incorrect */
/* JADX WARN: Init of enum PREMIUM_TIER_2_YEARLY can be incorrect */
/* compiled from: GooglePlaySku.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u001d\b\u0086\u0001\u0018\u0000 \u001e2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0003\u001e\u001f B?\b\u0002\u0012\u0006\u0010\u0013\u001a\u00020\u0012\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u0000\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\u0018\u001a\u00020\u0017¢\u0006\u0004\b\u001c\u0010\u001dR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0019\u0010\b\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0004\u001a\u0004\b\r\u0010\u0006R\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u00008\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0013\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u0019\u0010\u0018\u001a\u00020\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bj\u0002\b!j\u0002\b\"j\u0002\b#j\u0002\b$j\u0002\b%j\u0002\b&j\u0002\b'j\u0002\b(j\u0002\b)j\u0002\b*j\u0002\b+j\u0002\b,j\u0002\b-j\u0002\b.j\u0002\b/j\u0002\b0j\u0002\b1j\u0002\b2j\u0002\b3¨\u00064"}, d2 = {"Lcom/discord/utilities/billing/GooglePlaySku;", "", "", "iconDrawableResId", "I", "getIconDrawableResId", "()I", "Lcom/discord/utilities/billing/GooglePlaySku$Type;", "type", "Lcom/discord/utilities/billing/GooglePlaySku$Type;", "getType", "()Lcom/discord/utilities/billing/GooglePlaySku$Type;", "premiumSubscriptionCount", "getPremiumSubscriptionCount", "upgrade", "Lcom/discord/utilities/billing/GooglePlaySku;", "getUpgrade", "()Lcom/discord/utilities/billing/GooglePlaySku;", "", "skuName", "Ljava/lang/String;", "getSkuName", "()Ljava/lang/String;", "Lcom/discord/api/premium/SubscriptionInterval;", "interval", "Lcom/discord/api/premium/SubscriptionInterval;", "getInterval", "()Lcom/discord/api/premium/SubscriptionInterval;", HookHelper.constructorName, "(Ljava/lang/String;ILjava/lang/String;ILcom/discord/utilities/billing/GooglePlaySku$Type;Lcom/discord/utilities/billing/GooglePlaySku;ILcom/discord/api/premium/SubscriptionInterval;)V", "Companion", "Section", "Type", "PREMIUM_TIER_2_YEARLY", "PREMIUM_TIER_2_MONTHLY", "PREMIUM_TIER_1_YEARLY", "PREMIUM_TIER_1_MONTHLY", "PREMIUM_TIER_2_PREMIUM_GUILD_1_YEARLY", "PREMIUM_TIER_2_PREMIUM_GUILD_1_MONTHLY", "PREMIUM_TIER_2_PREMIUM_GUILD_2_YEARLY", "PREMIUM_TIER_2_PREMIUM_GUILD_2_MONTHLY", "PREMIUM_TIER_2_PREMIUM_GUILD_3_YEARLY", "PREMIUM_TIER_2_PREMIUM_GUILD_3_MONTHLY", "PREMIUM_TIER_2_PREMIUM_GUILD_5_YEARLY", "PREMIUM_TIER_2_PREMIUM_GUILD_5_MONTHLY", "PREMIUM_TIER_2_PREMIUM_GUILD_10_MONTHLY", "PREMIUM_TIER_2_PREMIUM_GUILD_13_MONTHLY", "PREMIUM_TIER_2_PREMIUM_GUILD_28_MONTHLY", "PREMIUM_TIER_1_PREMIUM_GUILD_1_YEARLY", "PREMIUM_TIER_1_PREMIUM_GUILD_1_MONTHLY", "PREMIUM_GUILD_1_MONTHLY", "PREMIUM_GUILD_2_MONTHLY", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public enum GooglePlaySku {
    PREMIUM_TIER_2_YEARLY("premium_tier_2_yearly", R.drawable.ic_plan_premium_tier_2, r14, null, 2, r15, 8, null),
    PREMIUM_TIER_2_MONTHLY("premium_tier_2_monthly", R.drawable.ic_plan_premium_tier_2, r14, r13, 2, r18),
    PREMIUM_TIER_1_YEARLY("premium_tier_1_yearly", R.drawable.ic_plan_premium_tier_1, r14, null, 0, r15, 8, null),
    PREMIUM_TIER_1_MONTHLY("premium_tier_1_monthly", R.drawable.ic_plan_premium_tier_1, r14, r13, 0, r18),
    PREMIUM_TIER_2_PREMIUM_GUILD_1_YEARLY("premium_tier_2_premium_guild_1_yearly", R.drawable.ic_plan_premium_and_premium_guild_1, r14, null, 3, r15, 8, null),
    PREMIUM_TIER_2_PREMIUM_GUILD_1_MONTHLY("premium_tier_2_premium_guild_1_monthly", R.drawable.ic_plan_premium_and_premium_guild_1, r14, r13, 3, r18),
    PREMIUM_TIER_2_PREMIUM_GUILD_2_YEARLY("premium_tier_2_premium_guild_2_yearly", R.drawable.ic_plan_premium_and_premium_guild_2, r14, null, 4, r15, 8, null),
    PREMIUM_TIER_2_PREMIUM_GUILD_2_MONTHLY("premium_tier_2_premium_guild_2_monthly", R.drawable.ic_plan_premium_and_premium_guild_2, r14, r13, 4, r18),
    PREMIUM_TIER_2_PREMIUM_GUILD_3_YEARLY("premium_tier_2_premium_guild_3_yearly", R.drawable.ic_plan_premium_and_premium_guild_3, r14, null, 5, r15, 8, null),
    PREMIUM_TIER_2_PREMIUM_GUILD_3_MONTHLY("premium_tier_2_premium_guild_3_monthly", R.drawable.ic_plan_premium_and_premium_guild_3, r14, r13, 5, r18),
    PREMIUM_TIER_2_PREMIUM_GUILD_5_YEARLY("premium_tier_2_premium_guild_5_yearly", R.drawable.ic_plan_premium_and_premium_guild_5, r14, null, 7, r15, 8, null),
    PREMIUM_TIER_2_PREMIUM_GUILD_5_MONTHLY("premium_tier_2_premium_guild_5_monthly", R.drawable.ic_plan_premium_and_premium_guild_5, r14, r13, 7, r18),
    PREMIUM_TIER_2_PREMIUM_GUILD_10_MONTHLY("premium_tier_2_premium_guild_10_monthly", R.drawable.ic_plan_premium_and_premium_guild_10, r14, null, 12, r18, 8, null),
    PREMIUM_TIER_2_PREMIUM_GUILD_13_MONTHLY("premium_tier_2_premium_guild_13_monthly", R.drawable.ic_plan_premium_and_premium_guild_13, r14, null, 15, r18, 8, null),
    PREMIUM_TIER_2_PREMIUM_GUILD_28_MONTHLY("premium_tier_2_premium_guild_28_monthly", R.drawable.ic_plan_premium_and_premium_guild_28, r14, null, 30, r18, 8, null),
    PREMIUM_TIER_1_PREMIUM_GUILD_1_YEARLY("premium_tier_1_premium_guild_1_yearly", R.drawable.ic_plan_premium_tier_1_and_premium_guild_1, r14, null, 1, r15, 8, null),
    PREMIUM_TIER_1_PREMIUM_GUILD_1_MONTHLY("premium_tier_1_premium_guild_1_monthly", R.drawable.ic_plan_premium_tier_1_and_premium_guild_1, r14, r13, 1, r18),
    PREMIUM_GUILD_1_MONTHLY("premium_guild_1_monthly", R.drawable.ic_plan_premium_guild_1, r13, r17, 1, r18),
    PREMIUM_GUILD_2_MONTHLY("premium_guild_2_monthly", R.drawable.ic_plan_premium_guild_2, r13, r17, 2, r18);
    
    private static final List<String> ALL_SKU_NAMES;
    public static final Companion Companion = new Companion(null);
    private static final Map<String, GooglePlaySku> skusBySkuName;
    private final int iconDrawableResId;
    private final SubscriptionInterval interval;
    private final int premiumSubscriptionCount;
    private final String skuName;
    private final Type type;
    private final GooglePlaySku upgrade;

    /* compiled from: GooglePlaySku.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0004\n\u0002\u0010$\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0017\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\u0007\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\u0006J\u0017\u0010\u000b\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\bH\u0007¢\u0006\u0004\b\u000b\u0010\fR\u001f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\"\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u00128\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014¨\u0006\u0017"}, d2 = {"Lcom/discord/utilities/billing/GooglePlaySku$Companion;", "", "", "skuName", "Lcom/discord/utilities/billing/GooglePlaySku;", "fromSkuName", "(Ljava/lang/String;)Lcom/discord/utilities/billing/GooglePlaySku;", "getDowngrade", "Lcom/discord/utilities/billing/GooglePlaySku$Type;", "skuType", "", "getBorderResource", "(Lcom/discord/utilities/billing/GooglePlaySku$Type;)I", "", "ALL_SKU_NAMES", "Ljava/util/List;", "getALL_SKU_NAMES", "()Ljava/util/List;", "", "skusBySkuName", "Ljava/util/Map;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {

        @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public final /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                Type.values();
                int[] iArr = new int[5];
                $EnumSwitchMapping$0 = iArr;
                iArr[Type.PREMIUM_TIER_2.ordinal()] = 1;
                iArr[Type.PREMIUM_TIER_1.ordinal()] = 2;
                iArr[Type.PREMIUM_GUILD.ordinal()] = 3;
                iArr[Type.PREMIUM_TIER_2_AND_PREMIUM_GUILD.ordinal()] = 4;
                iArr[Type.PREMIUM_TIER_1_AND_PREMIUM_GUILD.ordinal()] = 5;
            }
        }

        private Companion() {
        }

        public final GooglePlaySku fromSkuName(String str) {
            m.checkNotNullParameter(str, "skuName");
            return (GooglePlaySku) GooglePlaySku.skusBySkuName.get(str);
        }

        public final List<String> getALL_SKU_NAMES() {
            return GooglePlaySku.ALL_SKU_NAMES;
        }

        @DrawableRes
        public final int getBorderResource(Type type) {
            m.checkNotNullParameter(type, "skuType");
            int ordinal = type.ordinal();
            if (ordinal == 0) {
                return R.drawable.drawable_bg_nitro_gradient_diagonal;
            }
            if (ordinal == 1) {
                return R.drawable.drawable_bg_nitro_classic_gradient_diagonal;
            }
            if (ordinal == 2) {
                return R.drawable.drawable_bg_premium_tier_2_and_premium_guild_gradient;
            }
            if (ordinal == 3) {
                return R.drawable.drawable_bg_premium_tier_1_and_premium_guild_gradient;
            }
            if (ordinal == 4) {
                return R.drawable.drawable_bg_premium_guild_gradient;
            }
            throw new NoWhenBranchMatchedException();
        }

        public final GooglePlaySku getDowngrade(String str) {
            m.checkNotNullParameter(str, "skuName");
            GooglePlaySku[] values = GooglePlaySku.values();
            int i = 0;
            while (true) {
                String str2 = null;
                if (i >= 19) {
                    return null;
                }
                GooglePlaySku googlePlaySku = values[i];
                GooglePlaySku upgrade = googlePlaySku.getUpgrade();
                if (upgrade != null) {
                    str2 = upgrade.getSkuName();
                }
                if (m.areEqual(str2, str)) {
                    return googlePlaySku;
                }
                i++;
            }
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: GooglePlaySku.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0086\u0001\u0018\u0000 \u00042\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0004B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/discord/utilities/billing/GooglePlaySku$Section;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "Companion", "PREMIUM", "PREMIUM_AND_PREMIUM_GUILD", "PREMIUM_GUILD", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum Section {
        PREMIUM,
        PREMIUM_AND_PREMIUM_GUILD,
        PREMIUM_GUILD;
        
        public static final Companion Companion = new Companion(null);

        /* compiled from: GooglePlaySku.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/utilities/billing/GooglePlaySku$Section$Companion;", "", "Lcom/discord/utilities/billing/GooglePlaySku$Section;", "section", "", "getHeaderResource", "(Lcom/discord/utilities/billing/GooglePlaySku$Section;)I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Companion {

            @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
            /* loaded from: classes.dex */
            public final /* synthetic */ class WhenMappings {
                public static final /* synthetic */ int[] $EnumSwitchMapping$0;

                static {
                    Section.values();
                    int[] iArr = new int[3];
                    $EnumSwitchMapping$0 = iArr;
                    iArr[Section.PREMIUM.ordinal()] = 1;
                    iArr[Section.PREMIUM_AND_PREMIUM_GUILD.ordinal()] = 2;
                    iArr[Section.PREMIUM_GUILD.ordinal()] = 3;
                }
            }

            private Companion() {
            }

            @StringRes
            public final int getHeaderResource(Section section) {
                m.checkNotNullParameter(section, "section");
                int ordinal = section.ordinal();
                if (ordinal == 0) {
                    return R.string.billing_premium_plans;
                }
                if (ordinal == 1) {
                    return R.string.billing_premium_and_premium_guild_plans;
                }
                if (ordinal == 2) {
                    return R.string.billing_premium_guild_plans;
                }
                throw new NoWhenBranchMatchedException();
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    /* compiled from: GooglePlaySku.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\b\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\t"}, d2 = {"Lcom/discord/utilities/billing/GooglePlaySku$Type;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "PREMIUM_TIER_2", "PREMIUM_TIER_1", "PREMIUM_TIER_2_AND_PREMIUM_GUILD", "PREMIUM_TIER_1_AND_PREMIUM_GUILD", "PREMIUM_GUILD", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum Type {
        PREMIUM_TIER_2,
        PREMIUM_TIER_1,
        PREMIUM_TIER_2_AND_PREMIUM_GUILD,
        PREMIUM_TIER_1_AND_PREMIUM_GUILD,
        PREMIUM_GUILD
    }

    static {
        Type type = Type.PREMIUM_TIER_2;
        SubscriptionInterval subscriptionInterval = SubscriptionInterval.YEARLY;
        GooglePlaySku googlePlaySku = PREMIUM_TIER_2_YEARLY;
        SubscriptionInterval subscriptionInterval2 = SubscriptionInterval.MONTHLY;
        GooglePlaySku googlePlaySku2 = PREMIUM_TIER_2_MONTHLY;
        Type type2 = Type.PREMIUM_TIER_1;
        GooglePlaySku googlePlaySku3 = PREMIUM_TIER_1_YEARLY;
        Type type3 = Type.PREMIUM_TIER_2_AND_PREMIUM_GUILD;
        GooglePlaySku googlePlaySku4 = PREMIUM_TIER_2_PREMIUM_GUILD_1_YEARLY;
        GooglePlaySku googlePlaySku5 = PREMIUM_TIER_2_PREMIUM_GUILD_2_YEARLY;
        GooglePlaySku googlePlaySku6 = PREMIUM_TIER_2_PREMIUM_GUILD_3_YEARLY;
        GooglePlaySku googlePlaySku7 = PREMIUM_TIER_2_PREMIUM_GUILD_5_YEARLY;
        Type type4 = Type.PREMIUM_TIER_1_AND_PREMIUM_GUILD;
        GooglePlaySku googlePlaySku8 = PREMIUM_TIER_1_PREMIUM_GUILD_1_YEARLY;
        Type type5 = Type.PREMIUM_GUILD;
        GooglePlaySku[] values = values();
        ArrayList arrayList = new ArrayList(19);
        for (int i = 0; i < 19; i++) {
            arrayList.add(values[i].skuName);
        }
        ALL_SKU_NAMES = arrayList;
        GooglePlaySku[] values2 = values();
        LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(19), 16));
        for (int i2 = 0; i2 < 19; i2++) {
            GooglePlaySku googlePlaySku9 = values2[i2];
            linkedHashMap.put(googlePlaySku9.skuName, googlePlaySku9);
        }
        skusBySkuName = linkedHashMap;
    }

    GooglePlaySku(String str, @DrawableRes int i, Type type, GooglePlaySku googlePlaySku, int i2, SubscriptionInterval subscriptionInterval) {
        this.skuName = str;
        this.iconDrawableResId = i;
        this.type = type;
        this.upgrade = googlePlaySku;
        this.premiumSubscriptionCount = i2;
        this.interval = subscriptionInterval;
    }

    public final int getIconDrawableResId() {
        return this.iconDrawableResId;
    }

    public final SubscriptionInterval getInterval() {
        return this.interval;
    }

    public final int getPremiumSubscriptionCount() {
        return this.premiumSubscriptionCount;
    }

    public final String getSkuName() {
        return this.skuName;
    }

    public final Type getType() {
        return this.type;
    }

    public final GooglePlaySku getUpgrade() {
        return this.upgrade;
    }

    /* synthetic */ GooglePlaySku(String str, int i, Type type, GooglePlaySku googlePlaySku, int i2, SubscriptionInterval subscriptionInterval, int i3, DefaultConstructorMarker defaultConstructorMarker) {
        this(str, i, type, (i3 & 8) != 0 ? null : googlePlaySku, i2, subscriptionInterval);
    }
}
