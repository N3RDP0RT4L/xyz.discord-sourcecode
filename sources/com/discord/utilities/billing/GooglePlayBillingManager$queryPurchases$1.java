package com.discord.utilities.billing;

import b.d.a.a.f;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.discord.stores.StoreStream;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: GooglePlayBillingManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\n\u001a\u00020\u00072\u0006\u0010\u0001\u001a\u00020\u00002(\u0010\u0006\u001a$\u0012\f\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003 \u0004*\u0010\u0012\f\u0012\n \u0004*\u0004\u0018\u00010\u00030\u00030\u00050\u0002H\n¢\u0006\u0004\b\b\u0010\t"}, d2 = {"Lcom/android/billingclient/api/BillingResult;", "<anonymous parameter 0>", "", "Lcom/android/billingclient/api/Purchase;", "kotlin.jvm.PlatformType", "", "purchasesList", "", "onQueryPurchasesResponse", "(Lcom/android/billingclient/api/BillingResult;Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GooglePlayBillingManager$queryPurchases$1 implements f {
    public static final GooglePlayBillingManager$queryPurchases$1 INSTANCE = new GooglePlayBillingManager$queryPurchases$1();

    @Override // b.d.a.a.f
    public final void onQueryPurchasesResponse(BillingResult billingResult, List<Purchase> list) {
        m.checkNotNullParameter(billingResult, "<anonymous parameter 0>");
        m.checkNotNullParameter(list, "purchasesList");
        StoreStream.Companion.getGooglePlayPurchases().processPurchases(list, "subs");
    }
}
