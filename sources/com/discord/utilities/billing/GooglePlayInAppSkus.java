package com.discord.utilities.billing;

import andhook.lib.HookHelper;
import com.android.billingclient.api.SkuDetails;
import com.discord.models.domain.ModelSku;
import d0.d0.f;
import d0.t.g0;
import d0.t.o;
import d0.z.d.m;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
/* compiled from: GooglePlayInAppSku.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\b\u0007\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001c\u0010\u001dJ\u0019\u0010\u0006\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0006\u0010\u0007J%\u0010\f\u001a\u00020\u000b2\u0016\u0010\n\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\t0\b¢\u0006\u0004\b\f\u0010\rJ\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u0012\u001a\u00020\u0011¢\u0006\u0004\b\u000f\u0010\u0013R&\u0010\u0016\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u000e0\b8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0015R\u001f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001b¨\u0006\u001e"}, d2 = {"Lcom/discord/utilities/billing/GooglePlayInAppSkus;", "", "", "Lcom/discord/primitives/PaymentGatewaySkuId;", "paymentGatewaySkuId", "", "isInAppSku", "(Ljava/lang/String;)Z", "", "Lcom/android/billingclient/api/SkuDetails;", "skuDetails", "", "populateSkuDetails", "(Ljava/util/Map;)V", "Lcom/discord/utilities/billing/GooglePlayInAppSku;", "getInAppSku", "(Ljava/lang/String;)Lcom/discord/utilities/billing/GooglePlayInAppSku;", "Lcom/discord/models/domain/ModelSku;", "sku", "(Lcom/discord/models/domain/ModelSku;)Lcom/discord/utilities/billing/GooglePlayInAppSku;", "getSkusById", "()Ljava/util/Map;", "skusById", "", "skus", "Ljava/util/List;", "getSkus", "()Ljava/util/List;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GooglePlayInAppSkus {
    public static final GooglePlayInAppSkus INSTANCE = new GooglePlayInAppSkus();
    private static final List<GooglePlayInAppSku> skus = GooglePlayInAppSkuKt.getPremiumGifts();

    private GooglePlayInAppSkus() {
    }

    private final Map<String, GooglePlayInAppSku> getSkusById() {
        List<GooglePlayInAppSku> list = skus;
        LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(o.collectionSizeOrDefault(list, 10)), 16));
        for (Object obj : list) {
            linkedHashMap.put(((GooglePlayInAppSku) obj).getPaymentGatewaySkuId(), obj);
        }
        return linkedHashMap;
    }

    public final GooglePlayInAppSku getInAppSku(String str) {
        m.checkNotNullParameter(str, "paymentGatewaySkuId");
        return getSkusById().get(str);
    }

    public final List<GooglePlayInAppSku> getSkus() {
        return skus;
    }

    public final boolean isInAppSku(String str) {
        m.checkNotNullParameter(str, "paymentGatewaySkuId");
        return getSkusById().containsKey(str);
    }

    public final void populateSkuDetails(Map<String, ? extends SkuDetails> map) {
        m.checkNotNullParameter(map, "skuDetails");
        for (Map.Entry<String, ? extends SkuDetails> entry : map.entrySet()) {
            GooglePlayInAppSku googlePlayInAppSku = INSTANCE.getSkusById().get(entry.getKey());
            if (googlePlayInAppSku != null) {
                googlePlayInAppSku.setSkuDetails(entry.getValue());
            }
        }
    }

    public final GooglePlayInAppSku getInAppSku(ModelSku modelSku) {
        Object obj;
        m.checkNotNullParameter(modelSku, "sku");
        Iterator<T> it = GooglePlayInAppSkuKt.getPremiumGifts().iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (m.areEqual(((GooglePlayInAppSku) obj).getPaymentGatewaySkuId(), modelSku.getName())) {
                break;
            }
        }
        return (GooglePlayInAppSku) obj;
    }
}
