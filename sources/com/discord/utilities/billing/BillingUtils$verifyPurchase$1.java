package com.discord.utilities.billing;

import com.android.billingclient.api.Purchase;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreGooglePlayPurchases;
import com.discord.stores.StoreStream;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: BillingUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/restapi/RestAPIParams$VerifyPurchaseResponse;", "response", "", "invoke", "(Lcom/discord/restapi/RestAPIParams$VerifyPurchaseResponse;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class BillingUtils$verifyPurchase$1 extends o implements Function1<RestAPIParams.VerifyPurchaseResponse, Unit> {
    public final /* synthetic */ Purchase $purchase;
    public final /* synthetic */ String $sku;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public BillingUtils$verifyPurchase$1(Purchase purchase, String str) {
        super(1);
        this.$purchase = purchase;
        this.$sku = str;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(RestAPIParams.VerifyPurchaseResponse verifyPurchaseResponse) {
        invoke2(verifyPurchaseResponse);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(RestAPIParams.VerifyPurchaseResponse verifyPurchaseResponse) {
        StoreStream.Companion.getGooglePlayPurchases().onVerificationSuccess(new StoreGooglePlayPurchases.VerificationResult(this.$purchase, verifyPurchaseResponse));
        GooglePlayInAppSku inAppSku = GooglePlayInAppSkus.INSTANCE.getInAppSku(this.$sku);
        if (inAppSku != null) {
            GooglePlayBillingManager.INSTANCE.consumePurchase(this.$purchase, inAppSku.getType(), verifyPurchaseResponse != null ? Long.valueOf(verifyPurchaseResponse.getVerifiedSkuId()) : null);
        }
    }
}
