package com.discord.utilities.billing;

import android.content.Context;
import com.discord.models.domain.premium.SubscriptionPlanType;
import com.discord.utilities.locale.LocaleManager;
import d0.o;
import d0.t.h0;
import d0.t.m0;
import d0.t.n0;
import d0.t.o0;
import d0.z.d.m;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
/* compiled from: PremiumUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\n\u001a\u0017\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\u0007¢\u0006\u0004\b\u0003\u0010\u0004\u001a\u001d\u0010\t\u001a\u00020\b2\u0006\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\t\u0010\n\"\u0016\u0010\u000b\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000b\u0010\f\"\u001c\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010\"-\u0010\u0012\u001a\u0016\u0012\u0006\u0012\u0004\u0018\u00010\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015\"\u0016\u0010\u0016\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0016\u0010\f\"\u001c\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u000e0\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0010\"\u0019\u0010\u0019\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001c\"\u001c\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u000e0\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u0010\"\u0019\u0010\u001e\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u001a\u001a\u0004\b\u001f\u0010\u001c\"\u001c\u0010 \u001a\b\u0012\u0004\u0012\u00020\u000e0\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b \u0010\u0010\"\u0016\u0010!\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b!\u0010\f¨\u0006\""}, d2 = {"Lcom/discord/models/domain/ModelPaymentSource;", "paymentSource", "", "getPaymentSourceIcon", "(Lcom/discord/models/domain/ModelPaymentSource;)I", "priceUsCents", "Landroid/content/Context;", "context", "", "getFormattedPriceUsd", "(ILandroid/content/Context;)Ljava/lang/CharSequence;", "MAX_ACCOUNT_HOLD_DAYS", "I", "", "Lcom/discord/models/domain/premium/SubscriptionPlanType;", "TIER_2_PLANS", "Ljava/util/Set;", "", "UPGRADE_ELIGIBILITY", "Ljava/util/Map;", "getUPGRADE_ELIGIBILITY", "()Ljava/util/Map;", "GRACE_PERIOD_LONG", "TIER_1_PLANS", "Ljava/util/Date;", "GRANDFATHERED_MONTHLY_END_DATE", "Ljava/util/Date;", "getGRANDFATHERED_MONTHLY_END_DATE", "()Ljava/util/Date;", "ALL_PAID_PLANS", "GRANDFATHERED_YEARLY_END_DATE", "getGRANDFATHERED_YEARLY_END_DATE", "NONE_PLANS", "GRACE_PERIOD_SHORT", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class PremiumUtilsKt {
    private static final Set<SubscriptionPlanType> ALL_PAID_PLANS;
    public static final int GRACE_PERIOD_LONG = 7;
    public static final int GRACE_PERIOD_SHORT = 3;
    private static final Date GRANDFATHERED_MONTHLY_END_DATE;
    private static final Date GRANDFATHERED_YEARLY_END_DATE;
    public static final int MAX_ACCOUNT_HOLD_DAYS = 30;
    private static final Set<SubscriptionPlanType> NONE_PLANS;
    private static final Set<SubscriptionPlanType> TIER_1_PLANS;
    private static final Set<SubscriptionPlanType> TIER_2_PLANS;
    private static final Map<SubscriptionPlanType, Set<SubscriptionPlanType>> UPGRADE_ELIGIBILITY;

    static {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2020, 2, 1);
        m.checkNotNullExpressionValue(calendar, "Calendar.getInstance().apply { set(2020, 2, 1) }");
        Date time = calendar.getTime();
        m.checkNotNullExpressionValue(time, "Calendar.getInstance().a… { set(2020, 2, 1) }.time");
        GRANDFATHERED_MONTHLY_END_DATE = time;
        Calendar calendar2 = Calendar.getInstance();
        calendar2.set(2021, 0, 1);
        m.checkNotNullExpressionValue(calendar2, "Calendar.getInstance().apply { set(2021, 0, 1) }");
        Date time2 = calendar2.getTime();
        m.checkNotNullExpressionValue(time2, "Calendar.getInstance().a… { set(2021, 0, 1) }.time");
        GRANDFATHERED_YEARLY_END_DATE = time2;
        SubscriptionPlanType subscriptionPlanType = SubscriptionPlanType.NONE_MONTH;
        SubscriptionPlanType subscriptionPlanType2 = SubscriptionPlanType.NONE_YEAR;
        Set<SubscriptionPlanType> of = n0.setOf((Object[]) new SubscriptionPlanType[]{subscriptionPlanType, subscriptionPlanType2});
        NONE_PLANS = of;
        SubscriptionPlanType subscriptionPlanType3 = SubscriptionPlanType.PREMIUM_MONTH_TIER_1;
        SubscriptionPlanType subscriptionPlanType4 = SubscriptionPlanType.PREMIUM_YEAR_TIER_1;
        Set<SubscriptionPlanType> of2 = n0.setOf((Object[]) new SubscriptionPlanType[]{subscriptionPlanType3, subscriptionPlanType4});
        TIER_1_PLANS = of2;
        SubscriptionPlanType subscriptionPlanType5 = SubscriptionPlanType.PREMIUM_MONTH_TIER_2;
        SubscriptionPlanType subscriptionPlanType6 = SubscriptionPlanType.PREMIUM_YEAR_TIER_2;
        Set<SubscriptionPlanType> of3 = n0.setOf((Object[]) new SubscriptionPlanType[]{subscriptionPlanType5, subscriptionPlanType6});
        TIER_2_PLANS = of3;
        Set<SubscriptionPlanType> plus = o0.plus((Set) of2, (Iterable) of3);
        ALL_PAID_PLANS = plus;
        UPGRADE_ELIGIBILITY = h0.mapOf(o.to(null, o0.plus((Set) of, (Iterable) plus)), o.to(subscriptionPlanType, o0.plus(m0.setOf(subscriptionPlanType2), (Iterable) plus)), o.to(subscriptionPlanType2, plus), o.to(SubscriptionPlanType.PREMIUM_MONTH_LEGACY, o0.plus(m0.setOf(subscriptionPlanType4), (Iterable) of3)), o.to(SubscriptionPlanType.PREMIUM_YEAR_LEGACY, m0.setOf(subscriptionPlanType6)), o.to(subscriptionPlanType3, o0.plus(m0.setOf(subscriptionPlanType4), (Iterable) of3)), o.to(subscriptionPlanType4, m0.setOf(subscriptionPlanType6)), o.to(subscriptionPlanType5, m0.setOf(subscriptionPlanType6)), o.to(subscriptionPlanType6, n0.emptySet()));
    }

    public static final CharSequence getFormattedPriceUsd(int i, Context context) {
        m.checkNotNullParameter(context, "context");
        NumberFormat currencyInstance = NumberFormat.getCurrencyInstance(new LocaleManager().getPrimaryLocale(context));
        currencyInstance.setCurrency(Currency.getInstance("USD"));
        String format = currencyInstance.format(Float.valueOf(i / 100));
        m.checkNotNullExpressionValue(format, "numberFormat.format(priceUsdDollars)");
        return format;
    }

    public static final Date getGRANDFATHERED_MONTHLY_END_DATE() {
        return GRANDFATHERED_MONTHLY_END_DATE;
    }

    public static final Date getGRANDFATHERED_YEARLY_END_DATE() {
        return GRANDFATHERED_YEARLY_END_DATE;
    }

    /* JADX WARN: Removed duplicated region for block: B:21:0x0066 A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:24:0x0073 A[ORIG_RETURN, RETURN] */
    @androidx.annotation.DrawableRes
    @android.annotation.SuppressLint({"DefaultLocale"})
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final int getPaymentSourceIcon(com.discord.models.domain.ModelPaymentSource r2) {
        /*
            java.lang.String r0 = "paymentSource"
            d0.z.d.m.checkNotNullParameter(r2, r0)
            boolean r0 = r2 instanceof com.discord.models.domain.ModelPaymentSource.ModelPaymentSourcePaypal
            r1 = 2131231641(0x7f080399, float:1.8079369E38)
            if (r0 == 0) goto L11
            r1 = 2131231643(0x7f08039b, float:1.8079373E38)
            goto L76
        L11:
            boolean r0 = r2 instanceof com.discord.models.domain.ModelPaymentSource.ModelPaymentSourceCard
            if (r0 == 0) goto L76
            com.discord.models.domain.ModelPaymentSource$ModelPaymentSourceCard r2 = (com.discord.models.domain.ModelPaymentSource.ModelPaymentSourceCard) r2
            java.lang.String r2 = r2.getBrand()
            java.lang.String r0 = "null cannot be cast to non-null type java.lang.String"
            java.util.Objects.requireNonNull(r2, r0)
            java.lang.String r2 = r2.toLowerCase()
            java.lang.String r0 = "(this as java.lang.String).toLowerCase()"
            d0.z.d.m.checkNotNullExpressionValue(r2, r0)
            int r0 = r2.hashCode()
            switch(r0) {
                case -2038717326: goto L6a;
                case 2997727: goto L5e;
                case 3619905: goto L51;
                case 61060803: goto L48;
                case 273184745: goto L3c;
                case 1174445979: goto L32;
                default: goto L31;
            }
        L31:
            goto L76
        L32:
            java.lang.String r0 = "master-card"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L76
            goto L73
        L3c:
            java.lang.String r0 = "discover"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L76
            r1 = 2131231640(0x7f080398, float:1.8079367E38)
            goto L76
        L48:
            java.lang.String r0 = "american-express"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L76
            goto L66
        L51:
            java.lang.String r0 = "visa"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L76
            r1 = 2131231644(0x7f08039c, float:1.8079375E38)
            goto L76
        L5e:
            java.lang.String r0 = "amex"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L76
        L66:
            r1 = 2131231639(0x7f080397, float:1.8079365E38)
            goto L76
        L6a:
            java.lang.String r0 = "mastercard"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L76
        L73:
            r1 = 2131231642(0x7f08039a, float:1.807937E38)
        L76:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.billing.PremiumUtilsKt.getPaymentSourceIcon(com.discord.models.domain.ModelPaymentSource):int");
    }

    public static final Map<SubscriptionPlanType, Set<SubscriptionPlanType>> getUPGRADE_ELIGIBILITY() {
        return UPGRADE_ELIGIBILITY;
    }
}
