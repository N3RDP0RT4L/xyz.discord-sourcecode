package com.discord.utilities.billing;

import andhook.lib.HookHelper;
import com.android.billingclient.api.Purchase;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreStream;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: BillingUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0004\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u001d\u0010\u000b\u001a\u00020\u00062\u000e\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\t¢\u0006\u0004\b\u000b\u0010\fJ9\u0010\u0013\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u000e2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00060\u00102\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00060\u0010¢\u0006\u0004\b\u0013\u0010\u0014J\u0015\u0010\u0016\u001a\u00020\u000e2\u0006\u0010\u0015\u001a\u00020\u000e¢\u0006\u0004\b\u0016\u0010\u0017¨\u0006\u001a"}, d2 = {"Lcom/discord/utilities/billing/BillingUtils;", "", "Lcom/android/billingclient/api/Purchase;", "purchase", "", "sku", "", "verifyPurchase", "(Lcom/android/billingclient/api/Purchase;Ljava/lang/String;)V", "", "purchases", "verifyPurchases", "(Ljava/util/List;)V", "paymentGatewaySkuId", "", "skuId", "Lkotlin/Function0;", "onSuccess", "onFailure", "createPendingPurchaseMetadata", "(Ljava/lang/String;JLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V", "amount", "microToMinor", "(J)J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class BillingUtils {
    public static final BillingUtils INSTANCE = new BillingUtils();

    private BillingUtils() {
    }

    private final void verifyPurchase(Purchase purchase, String str) {
        String str2;
        String str3;
        StoreStream.Companion companion = StoreStream.Companion;
        long id2 = companion.getUsers().getMe().getId();
        if (GooglePlayInAppSkus.INSTANCE.isInAppSku(str)) {
            str2 = str;
            str3 = null;
        } else {
            str3 = str;
            str2 = null;
        }
        String a = purchase.a();
        m.checkNotNullExpressionValue(a, "purchase.purchaseToken");
        String optString = purchase.c.optString("packageName");
        m.checkNotNullExpressionValue(optString, "purchase.packageName");
        RestAPIParams.VerifyPurchaseTokenBody verifyPurchaseTokenBody = new RestAPIParams.VerifyPurchaseTokenBody(a, id2, optString, str3, str2);
        companion.getGooglePlayPurchases().onVerificationStart();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().verifyPurchaseToken(verifyPurchaseTokenBody), false, 1, null), BillingUtils.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new BillingUtils$verifyPurchase$2(purchase, str), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new BillingUtils$verifyPurchase$1(purchase, str));
    }

    public final void createPendingPurchaseMetadata(String str, long j, Function0<Unit> function0, Function0<Unit> function02) {
        m.checkNotNullParameter(str, "paymentGatewaySkuId");
        m.checkNotNullParameter(function0, "onSuccess");
        m.checkNotNullParameter(function02, "onFailure");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().createPurchaseMetadata(new RestAPIParams.PurchaseMetadataBody(j, str)), false, 1, null), BillingUtils.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new BillingUtils$createPendingPurchaseMetadata$2(function02), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new BillingUtils$createPendingPurchaseMetadata$1(function0));
    }

    public final long microToMinor(long j) {
        return j / 10000;
    }

    public final void verifyPurchases(List<? extends Purchase> list) {
        if (list != null) {
            for (Purchase purchase : list) {
                if ((purchase.c.optInt("purchaseState", 1) != 4 ? (char) 1 : (char) 2) == 1 && !purchase.c()) {
                    ArrayList<String> b2 = purchase.b();
                    m.checkNotNullExpressionValue(b2, "purchase.skus");
                    for (String str : b2) {
                        BillingUtils billingUtils = INSTANCE;
                        m.checkNotNullExpressionValue(str, "sku");
                        billingUtils.verifyPurchase(purchase, str);
                    }
                }
            }
        }
    }
}
