package com.discord.utilities.user;

import andhook.lib.HookHelper;
import com.discord.api.user.User;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import java.util.Collection;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.Subscription;
/* compiled from: UserRequestManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u001e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u001b\u0012\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00070\u0015¢\u0006\u0004\b\u0019\u0010\u001aJ#\u0010\b\u001a\u00020\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\b\u0010\tJ\u001b\u0010\n\u001a\u00020\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u001f\u0010\u000e\u001a\u00020\u00072\u0010\u0010\r\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f¢\u0006\u0004\b\u000e\u0010\u000fJ\u0019\u0010\u0010\u001a\u00020\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0010\u0010\u000bR:\u0010\u0013\u001a&\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00050\u0011j\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u0005`\u00128\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\"\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00070\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018¨\u0006\u001b"}, d2 = {"Lcom/discord/utilities/user/UserRequestManager;", "", "", "Lcom/discord/primitives/UserId;", "userId", "Lrx/Subscription;", Traits.Payment.Type.SUBSCRIPTION, "", "onRequestStarted", "(JLrx/Subscription;)V", "onRequestEnded", "(J)V", "", "userIds", "requestUsers", "(Ljava/util/Collection;)V", "requestUser", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "userRequests", "Ljava/util/HashMap;", "Lkotlin/Function1;", "Lcom/discord/api/user/User;", "onFlush", "Lkotlin/jvm/functions/Function1;", HookHelper.constructorName, "(Lkotlin/jvm/functions/Function1;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class UserRequestManager {
    private final Function1<User, Unit> onFlush;
    private final HashMap<Long, Subscription> userRequests = new HashMap<>();

    /* JADX WARN: Multi-variable type inference failed */
    public UserRequestManager(Function1<? super User, Unit> function1) {
        m.checkNotNullParameter(function1, "onFlush");
        this.onFlush = function1;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void onRequestEnded(long j) {
        Subscription remove = this.userRequests.remove(Long.valueOf(j));
        if (remove != null) {
            remove.unsubscribe();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void onRequestStarted(long j, Subscription subscription) {
        this.userRequests.put(Long.valueOf(j), subscription);
    }

    public final synchronized void requestUser(long j) {
        if (!this.userRequests.containsKey(Long.valueOf(j))) {
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().userGet(j), false, 1, null), getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new UserRequestManager$requestUser$3(this, j), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : new UserRequestManager$requestUser$2(this, j), new UserRequestManager$requestUser$1(this));
        }
    }

    public final synchronized void requestUsers(Collection<Long> collection) {
        m.checkNotNullParameter(collection, "userIds");
        for (Number number : collection) {
            requestUser(number.longValue());
        }
    }
}
