package com.discord.utilities.user;

import android.content.Context;
import com.discord.api.user.UserProfile;
import com.discord.utilities.time.TimeUtils;
import d0.z.d.m;
import f0.e0.c;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Metadata;
/* compiled from: UserProfileUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0004\u001a\u001b\u0010\u0004\u001a\u0004\u0018\u00010\u0003*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0001¢\u0006\u0004\b\u0004\u0010\u0005\u001a\u001b\u0010\u0006\u001a\u0004\u0018\u00010\u0003*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0001¢\u0006\u0004\b\u0006\u0010\u0005\"'\u0010\r\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`\t\u0012\u0004\u0012\u00020\n0\u0007*\u00020\u00008F@\u0006¢\u0006\u0006\u001a\u0004\b\u000b\u0010\f\"\u0019\u0010\u0011\u001a\u0004\u0018\u00010\u000e*\u00020\u00008F@\u0006¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010\"\u0017\u0010\u0013\u001a\u00020\u0012*\u00020\u00008F@\u0006¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014\"\u0017\u0010\u0015\u001a\u00020\u0012*\u00020\u00008F@\u0006¢\u0006\u0006\u001a\u0004\b\u0015\u0010\u0014¨\u0006\u0016"}, d2 = {"Lcom/discord/api/user/UserProfile;", "Landroid/content/Context;", "context", "", "getPremiumSince", "(Lcom/discord/api/user/UserProfile;Landroid/content/Context;)Ljava/lang/String;", "getBoostingSince", "", "", "Lcom/discord/primitives/GuildId;", "Lcom/discord/api/user/UserProfile$GuildReference;", "getMutualGuildsById", "(Lcom/discord/api/user/UserProfile;)Ljava/util/Map;", "mutualGuildsById", "", "getGuildBoostMonthsSubscribed", "(Lcom/discord/api/user/UserProfile;)Ljava/lang/Integer;", "guildBoostMonthsSubscribed", "", "isGuildBooster", "(Lcom/discord/api/user/UserProfile;)Z", "isPremium", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class UserProfileUtilsKt {
    public static final String getBoostingSince(UserProfile userProfile, Context context) {
        m.checkNotNullParameter(userProfile, "$this$getBoostingSince");
        m.checkNotNullParameter(context, "context");
        String e = userProfile.e();
        if (e != null) {
            return TimeUtils.getReadableTimeString(context, e);
        }
        return null;
    }

    public static final Integer getGuildBoostMonthsSubscribed(UserProfile userProfile) {
        m.checkNotNullParameter(userProfile, "$this$guildBoostMonthsSubscribed");
        String e = userProfile.e();
        if (e != null) {
            return Integer.valueOf(TimeUtils.getMonthsBetweenDates(new Date(TimeUtils.parseUTCDate(e)), new Date()));
        }
        return null;
    }

    public static final Map<Long, UserProfile.GuildReference> getMutualGuildsById(UserProfile userProfile) {
        m.checkNotNullParameter(userProfile, "$this$mutualGuildsById");
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Object obj : userProfile.d()) {
            linkedHashMap.put(Long.valueOf(((UserProfile.GuildReference) obj).a()), obj);
        }
        return c.A(linkedHashMap);
    }

    public static final String getPremiumSince(UserProfile userProfile, Context context) {
        m.checkNotNullParameter(userProfile, "$this$getPremiumSince");
        m.checkNotNullParameter(context, "context");
        String f = userProfile.f();
        if (f != null) {
            return TimeUtils.getReadableTimeString(context, f);
        }
        return null;
    }

    public static final boolean isGuildBooster(UserProfile userProfile) {
        m.checkNotNullParameter(userProfile, "$this$isGuildBooster");
        String e = userProfile.e();
        return !(e == null || e.length() == 0);
    }

    public static final boolean isPremium(UserProfile userProfile) {
        m.checkNotNullParameter(userProfile, "$this$isPremium");
        String f = userProfile.f();
        return !(f == null || f.length() == 0);
    }
}
