package com.discord.utilities.system;

import andhook.lib.HookHelper;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Process;
import android.util.Log;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.logging.LoggingProvider;
import com.discord.utilities.system.RemoteIntentService;
import com.discord.utilities.time.NtpClock;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: RemoteIntentService.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0007\b&\u0018\u0000 \u00122\u00020\u0001:\u0002\u0012\u0013B\u000f\u0012\u0006\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b\u0010\u0010\u0011J\u0019\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0003\u001a\u00020\u0002H$¢\u0006\u0004\b\u0005\u0010\u0006J\r\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\b\u0010\tJ\u0019\u0010\n\u001a\u00020\u00072\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0004¢\u0006\u0004\b\n\u0010\u000bJ\r\u0010\f\u001a\u00020\u0007¢\u0006\u0004\b\f\u0010\tR\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000f¨\u0006\u0014"}, d2 = {"Lcom/discord/utilities/system/RemoteIntentService;", "Landroid/app/IntentService;", "Landroid/content/Intent;", "intent", "Landroid/os/Bundle;", "doWork", "(Landroid/content/Intent;)Landroid/os/Bundle;", "", "onCreate", "()V", "onHandleIntent", "(Landroid/content/Intent;)V", "onDestroy", "", ModelAuditLogEntry.CHANGE_KEY_NAME, "Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "Companion", "IpcCallback", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class RemoteIntentService extends IntentService {
    public static final Companion Companion = new Companion(null);
    private static final String MESSENGER_KEY = "com.discord.utilities.analytics.RemoteIntentService.MESSENGER_KEY";
    private static final String TAG = "RemoteIntentService";
    private static final int WHAT_CALLBACK_RESULT = 1;
    private final String name;

    /* compiled from: RemoteIntentService.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J)\u0010\u000e\u001a\u00020\r2\u0006\u0010\b\u001a\u00020\u00072\n\u0010\n\u001a\u0006\u0012\u0002\b\u00030\t2\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0010\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0012\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0012\u0010\u0011R\u0016\u0010\u0014\u001a\u00020\u00138\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015¨\u0006\u0018"}, d2 = {"Lcom/discord/utilities/system/RemoteIntentService$Companion;", "", "Landroid/os/Message;", NotificationCompat.CATEGORY_MESSAGE, "", "messageToString", "(Landroid/os/Message;)Ljava/lang/String;", "Landroid/content/Context;", "context", "Ljava/lang/Class;", "serviceClass", "Lcom/discord/utilities/system/RemoteIntentService$IpcCallback;", "callback", "", "startRemoteServiceWithCallback", "(Landroid/content/Context;Ljava/lang/Class;Lcom/discord/utilities/system/RemoteIntentService$IpcCallback;)V", "MESSENGER_KEY", "Ljava/lang/String;", "TAG", "", "WHAT_CALLBACK_RESULT", "I", HookHelper.constructorName, "()V", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final String messageToString(Message message) {
            StringBuilder R = a.R("Message(what=");
            R.append(message.what);
            R.append(", arg1=");
            R.append(message.arg1);
            R.append(", arg2=");
            R.append(message.arg2);
            R.append(", obj=");
            R.append(message.obj);
            R.append(", replyTo=");
            R.append(message.replyTo);
            R.append(')');
            return R.toString();
        }

        public final void startRemoteServiceWithCallback(Context context, Class<?> cls, IpcCallback ipcCallback) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(cls, "serviceClass");
            m.checkNotNullParameter(ipcCallback, "callback");
            Logger logger = LoggingProvider.INSTANCE.get();
            StringBuilder R = a.R("Starting service in remote process: ");
            R.append(cls.getSimpleName());
            R.append(", app pid=");
            R.append(Process.myPid());
            Logger.d$default(logger, RemoteIntentService.TAG, R.toString(), null, 4, null);
            Intent intent = new Intent(context, cls);
            Bundle bundle = new Bundle();
            bundle.putParcelable(RemoteIntentService.MESSENGER_KEY, ipcCallback.getMessenger());
            intent.putExtras(bundle);
            context.startService(intent);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: RemoteIntentService.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b&\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0013\u001a\u00020\u0012\u0012\u0006\u0010\u0016\u001a\u00020\u0015¢\u0006\u0004\b\u0017\u0010\u0018J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H&¢\u0006\u0004\b\u0005\u0010\u0006J\u0019\u0010\t\u001a\u00020\u00042\b\u0010\b\u001a\u0004\u0018\u00010\u0007H&¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\f\u001a\u00020\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\rR\u0013\u0010\u0011\u001a\u00020\u000e8F@\u0006¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014¨\u0006\u0019"}, d2 = {"Lcom/discord/utilities/system/RemoteIntentService$IpcCallback;", "", "Landroid/os/Bundle;", "bundle", "", "onSuccess", "(Landroid/os/Bundle;)V", "", "throwable", "onFailure", "(Ljava/lang/Throwable;)V", "Landroid/os/Handler;", "handler", "Landroid/os/Handler;", "Landroid/os/Messenger;", "getMessenger", "()Landroid/os/Messenger;", "messenger", "", ModelAuditLogEntry.CHANGE_KEY_NAME, "Ljava/lang/String;", "Landroid/os/Looper;", "callbackLooper", HookHelper.constructorName, "(Ljava/lang/String;Landroid/os/Looper;)V", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class IpcCallback {
        private final Handler handler;
        private final String name;

        public IpcCallback(String str, Looper looper) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkNotNullParameter(looper, "callbackLooper");
            this.name = str;
            this.handler = new Handler(looper, new Handler.Callback() { // from class: com.discord.utilities.system.RemoteIntentService$IpcCallback$handler$1
                @Override // android.os.Handler.Callback
                public final boolean handleMessage(Message message) {
                    String str2;
                    String messageToString;
                    String str3;
                    String str4;
                    String str5;
                    m.checkNotNullParameter(message, NotificationCompat.CATEGORY_MESSAGE);
                    Logger logger = LoggingProvider.INSTANCE.get();
                    if (message.what == 1) {
                        StringBuilder O = a.O('[');
                        str3 = RemoteIntentService.IpcCallback.this.name;
                        O.append(str3);
                        O.append("] in IpcCallback: app pid=");
                        O.append(Process.myPid());
                        Logger.d$default(logger, "RemoteIntentService", O.toString(), null, 4, null);
                        Object obj = message.obj;
                        if (obj instanceof Bundle) {
                            StringBuilder O2 = a.O('[');
                            str5 = RemoteIntentService.IpcCallback.this.name;
                            O2.append(str5);
                            O2.append("] success after (");
                            Logger.d$default(logger, "RemoteIntentService", a.A(O2, message.arg1, " ms)"), null, 4, null);
                            RemoteIntentService.IpcCallback ipcCallback = RemoteIntentService.IpcCallback.this;
                            Object obj2 = message.obj;
                            Objects.requireNonNull(obj2, "null cannot be cast to non-null type android.os.Bundle");
                            ipcCallback.onSuccess((Bundle) obj2);
                            return true;
                        } else if ((obj instanceof Throwable) || obj == null) {
                            StringBuilder O3 = a.O('[');
                            str4 = RemoteIntentService.IpcCallback.this.name;
                            O3.append(str4);
                            O3.append("] failure after (");
                            Logger.d$default(logger, "RemoteIntentService", a.A(O3, message.arg1, " ms)"), null, 4, null);
                            RemoteIntentService.IpcCallback ipcCallback2 = RemoteIntentService.IpcCallback.this;
                            Object obj3 = message.obj;
                            if (!(obj3 instanceof Throwable)) {
                                obj3 = null;
                            }
                            ipcCallback2.onFailure((Throwable) obj3);
                            return true;
                        }
                    }
                    StringBuilder O4 = a.O('[');
                    str2 = RemoteIntentService.IpcCallback.this.name;
                    O4.append(str2);
                    O4.append("] unexpected message in handler: ");
                    messageToString = RemoteIntentService.Companion.messageToString(message);
                    O4.append(messageToString);
                    Logger.w$default(logger, "RemoteIntentService", O4.toString(), null, 4, null);
                    return false;
                }
            });
        }

        public final Messenger getMessenger() {
            return new Messenger(this.handler);
        }

        public abstract void onFailure(Throwable th);

        public abstract void onSuccess(Bundle bundle);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public RemoteIntentService(String str) {
        super(str);
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        this.name = str;
    }

    public abstract Bundle doWork(Intent intent);

    @Override // android.app.IntentService, android.app.Service
    public final void onCreate() {
        super.onCreate();
        StringBuilder O = a.O('[');
        O.append(this.name);
        O.append("] created, remote pid=");
        O.append(Process.myPid());
        Log.d(TAG, O.toString());
    }

    @Override // android.app.IntentService, android.app.Service
    public final void onDestroy() {
        StringBuilder O = a.O('[');
        O.append(this.name);
        O.append("] destroyed");
        Log.d(TAG, O.toString());
        super.onDestroy();
    }

    @Override // android.app.IntentService
    public final void onHandleIntent(Intent intent) {
        Bundle th;
        StringBuilder O = a.O('[');
        O.append(this.name);
        O.append("] onHandleIntent() start, pid=");
        O.append(Process.myPid());
        Log.d(TAG, O.toString());
        if (intent == null) {
            StringBuilder O2 = a.O('[');
            O2.append(this.name);
            O2.append("] null intent");
            Log.d(TAG, O2.toString());
            return;
        }
        NtpClock ntpClock = new NtpClock(b.m.a.a.a(this, null, null, 0L, 0L, 0L, 62));
        long currentTimeMillis = ntpClock.currentTimeMillis();
        try {
            th = doWork(intent);
        } catch (Throwable th2) {
            th = th2;
            StringBuilder O3 = a.O('[');
            O3.append(this.name);
            O3.append("] doWork returned error: ");
            O3.append(th);
            Log.e(TAG, O3.toString());
        }
        long currentTimeMillis2 = ntpClock.currentTimeMillis() - currentTimeMillis;
        StringBuilder O4 = a.O('[');
        O4.append(this.name);
        O4.append("] doWork finished: ");
        O4.append(currentTimeMillis2);
        O4.append(" ms");
        Log.d(TAG, O4.toString());
        Message obtain = Message.obtain();
        obtain.what = 1;
        obtain.arg1 = (int) currentTimeMillis2;
        obtain.obj = th;
        Messenger messenger = (Messenger) intent.getParcelableExtra(MESSENGER_KEY);
        if (messenger != null) {
            messenger.send(obtain);
            return;
        }
        StringBuilder O5 = a.O('[');
        O5.append(this.name);
        O5.append("] reply-to Messenger not set by caller");
        Log.e(TAG, O5.toString());
    }
}
