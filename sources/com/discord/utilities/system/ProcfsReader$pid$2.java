package com.discord.utilities.system;

import android.os.Process;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: ProcfsReader.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\b\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()I", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ProcfsReader$pid$2 extends o implements Function0<Integer> {
    public static final ProcfsReader$pid$2 INSTANCE = new ProcfsReader$pid$2();

    public ProcfsReader$pid$2() {
        super(0);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [int, java.lang.Integer] */
    @Override // kotlin.jvm.functions.Function0
    public final Integer invoke() {
        return Process.myPid();
    }
}
