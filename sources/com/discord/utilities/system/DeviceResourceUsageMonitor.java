package com.discord.utilities.system;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.system.Os;
import android.system.OsConstants;
import androidx.annotation.AnyThread;
import b.d.b.a.a;
import com.discord.api.permission.Permission;
import com.discord.utilities.system.ProcfsReader;
import com.discord.utilities.time.Clock;
import com.discord.utilities.time.TimeSpan;
import d0.k;
import d0.l;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: DeviceResourceUsageMonitor.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \u001c2\u00020\u0001:\u0002\u001c\u001dB+\u0012\u0006\u0010\u0015\u001a\u00020\u0014\u0012\u0006\u0010\u0018\u001a\u00020\u0017\u0012\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u00020\u000b¢\u0006\u0004\b\u001a\u0010\u001bJ\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0005\u0010\u0004J\u000f\u0010\u0006\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0006\u0010\u0004J\u000f\u0010\u0007\u001a\u00020\u0002H\u0005¢\u0006\u0004\b\u0007\u0010\u0004R\u0018\u0010\t\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\t\u0010\nR\"\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u00020\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0016\u0010\u000f\u001a\u00020\u00018\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010R\u0018\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0015\u001a\u00020\u00148\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0018\u001a\u00020\u00178\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019¨\u0006\u001e"}, d2 = {"Lcom/discord/utilities/system/DeviceResourceUsageMonitor;", "", "", "monitor", "()V", "start", "stop", "finalize", "", "activeThreadId", "Ljava/lang/Long;", "Lkotlin/Function1;", "Lcom/discord/utilities/system/DeviceResourceUsageMonitor$ResourceUsage;", "callback", "Lkotlin/jvm/functions/Function1;", "threadSync", "Ljava/lang/Object;", "Ljava/lang/Thread;", "thread", "Ljava/lang/Thread;", "Lcom/discord/utilities/time/TimeSpan;", "timeSpan", "Lcom/discord/utilities/time/TimeSpan;", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", HookHelper.constructorName, "(Lcom/discord/utilities/time/TimeSpan;Lcom/discord/utilities/time/Clock;Lkotlin/jvm/functions/Function1;)V", "Companion", "ResourceUsage", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class DeviceResourceUsageMonitor {
    public static final Companion Companion = new Companion(null);
    private static final long SC_CLK_TCK;
    private static final long SC_PAGE_SIZE;
    private static final int cpuCoreCount;
    private Long activeThreadId;
    private final Function1<ResourceUsage, Unit> callback;
    private final Clock clock;
    private Thread thread;
    private final Object threadSync = new Object();
    private final TimeSpan timeSpan;

    /* compiled from: DeviceResourceUsageMonitor.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00068\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/utilities/system/DeviceResourceUsageMonitor$Companion;", "", "", "SC_CLK_TCK", "J", "SC_PAGE_SIZE", "", "cpuCoreCount", "I", HookHelper.constructorName, "()V", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: DeviceResourceUsageMonitor.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\u0006\u0010\n\u001a\u00020\u0005\u0012\u0006\u0010\u000b\u001a\u00020\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\b\u0010\u0004J.\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\t\u001a\u00020\u00022\b\b\u0002\u0010\n\u001a\u00020\u00052\b\b\u0002\u0010\u000b\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0004J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\n\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0016\u001a\u0004\b\u0017\u0010\u0007R\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0018\u001a\u0004\b\u0019\u0010\u0004R\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0018\u001a\u0004\b\u001a\u0010\u0004¨\u0006\u001d"}, d2 = {"Lcom/discord/utilities/system/DeviceResourceUsageMonitor$ResourceUsage;", "", "", "component1", "()I", "", "component2", "()J", "component3", "cpuUsagePercent", "memoryRssBytes", "cpuCoreCount", "copy", "(IJI)Lcom/discord/utilities/system/DeviceResourceUsageMonitor$ResourceUsage;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getMemoryRssBytes", "I", "getCpuUsagePercent", "getCpuCoreCount", HookHelper.constructorName, "(IJI)V", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ResourceUsage {
        private final int cpuCoreCount;
        private final int cpuUsagePercent;
        private final long memoryRssBytes;

        public ResourceUsage(int i, long j, int i2) {
            this.cpuUsagePercent = i;
            this.memoryRssBytes = j;
            this.cpuCoreCount = i2;
        }

        public static /* synthetic */ ResourceUsage copy$default(ResourceUsage resourceUsage, int i, long j, int i2, int i3, Object obj) {
            if ((i3 & 1) != 0) {
                i = resourceUsage.cpuUsagePercent;
            }
            if ((i3 & 2) != 0) {
                j = resourceUsage.memoryRssBytes;
            }
            if ((i3 & 4) != 0) {
                i2 = resourceUsage.cpuCoreCount;
            }
            return resourceUsage.copy(i, j, i2);
        }

        public final int component1() {
            return this.cpuUsagePercent;
        }

        public final long component2() {
            return this.memoryRssBytes;
        }

        public final int component3() {
            return this.cpuCoreCount;
        }

        public final ResourceUsage copy(int i, long j, int i2) {
            return new ResourceUsage(i, j, i2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ResourceUsage)) {
                return false;
            }
            ResourceUsage resourceUsage = (ResourceUsage) obj;
            return this.cpuUsagePercent == resourceUsage.cpuUsagePercent && this.memoryRssBytes == resourceUsage.memoryRssBytes && this.cpuCoreCount == resourceUsage.cpuCoreCount;
        }

        public final int getCpuCoreCount() {
            return this.cpuCoreCount;
        }

        public final int getCpuUsagePercent() {
            return this.cpuUsagePercent;
        }

        public final long getMemoryRssBytes() {
            return this.memoryRssBytes;
        }

        public int hashCode() {
            return ((b.a(this.memoryRssBytes) + (this.cpuUsagePercent * 31)) * 31) + this.cpuCoreCount;
        }

        public String toString() {
            StringBuilder R = a.R("ResourceUsage(cpuUsagePercent=");
            R.append(this.cpuUsagePercent);
            R.append(", memoryRssBytes=");
            R.append(this.memoryRssBytes);
            R.append(", cpuCoreCount=");
            return a.A(R, this.cpuCoreCount, ")");
        }
    }

    static {
        long j;
        Object obj;
        int i;
        try {
            k.a aVar = k.j;
            j = k.m73constructorimpl(Long.valueOf(Os.sysconf(OsConstants._SC_CLK_TCK)));
        } catch (Throwable th) {
            k.a aVar2 = k.j;
            j = k.m73constructorimpl(l.createFailure(th));
        }
        j = 100L;
        if (k.m77isFailureimpl(j)) {
        }
        SC_CLK_TCK = ((Number) j).longValue();
        try {
            k.a aVar3 = k.j;
            obj = k.m73constructorimpl(Long.valueOf(Os.sysconf(OsConstants._SC_PAGE_SIZE)));
        } catch (Throwable th2) {
            k.a aVar4 = k.j;
            obj = k.m73constructorimpl(l.createFailure(th2));
        }
        obj = Long.valueOf((long) Permission.SEND_TTS_MESSAGES);
        if (k.m77isFailureimpl(obj)) {
        }
        SC_PAGE_SIZE = ((Number) obj).longValue();
        try {
            k.a aVar5 = k.j;
            i = k.m73constructorimpl(Integer.valueOf(Runtime.getRuntime().availableProcessors()));
        } catch (Throwable th3) {
            k.a aVar6 = k.j;
            i = k.m73constructorimpl(l.createFailure(th3));
        }
        i = 1;
        if (k.m77isFailureimpl(i)) {
        }
        cpuCoreCount = ((Number) i).intValue();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public DeviceResourceUsageMonitor(TimeSpan timeSpan, Clock clock, Function1<? super ResourceUsage, Unit> function1) {
        m.checkNotNullParameter(timeSpan, "timeSpan");
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(function1, "callback");
        this.timeSpan = timeSpan;
        this.clock = clock;
        this.callback = function1;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void monitor() {
        ProcfsReader.Stat stat = null;
        Long l = null;
        while (true) {
            synchronized (this.threadSync) {
                Long l2 = this.activeThreadId;
                Thread currentThread = Thread.currentThread();
                m.checkNotNullExpressionValue(currentThread, "Thread.currentThread()");
                long id2 = currentThread.getId();
                if (l2 == null || l2.longValue() != id2) {
                    break;
                } else if (!Thread.interrupted()) {
                    ProcfsReader.Stat readStatFile = ProcfsReader.INSTANCE.readStatFile();
                    long currentTimeMillis = this.clock.currentTimeMillis();
                    if (!(stat == null || l == null)) {
                        double totalTime = ((readStatFile.getTotalTime() - stat.getTotalTime()) * 100) / (SC_CLK_TCK * ((currentTimeMillis - l.longValue()) / 1000.0d));
                        int i = cpuCoreCount;
                        if (i == 0) {
                            i = 1;
                        }
                        this.callback.invoke(new ResourceUsage((int) (totalTime / i), readStatFile.getRssPages() * SC_PAGE_SIZE, i));
                    }
                    l = Long.valueOf(currentTimeMillis);
                    Thread.sleep(this.timeSpan.toMillis());
                    stat = readStatFile;
                } else {
                    return;
                }
            }
        }
    }

    @AnyThread
    public final void finalize() {
        stop();
    }

    @AnyThread
    public final void start() {
        Thread thread;
        Thread thread2;
        synchronized (this.threadSync) {
            thread = this.thread;
            thread2 = d0.v.a.thread((r12 & 1) != 0 ? true : true, (r12 & 2) != 0 ? false : true, (r12 & 4) != 0 ? null : null, (r12 & 8) != 0 ? null : "DeviceResourceUsageMonitor", (r12 & 16) != 0 ? -1 : 2, new DeviceResourceUsageMonitor$start$$inlined$synchronized$lambda$1(this));
            this.activeThreadId = Long.valueOf(thread2.getId());
            this.thread = thread2;
        }
        if (thread != null) {
            thread.interrupt();
        }
    }

    @AnyThread
    public final void stop() {
        Thread thread;
        synchronized (this.threadSync) {
            thread = this.thread;
            this.thread = null;
            this.activeThreadId = null;
        }
        if (thread != null) {
            thread.interrupt();
        }
    }
}
