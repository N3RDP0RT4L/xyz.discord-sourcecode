package com.discord.utilities;

import androidx.exifinterface.media.ExifInterface;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: Quad.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0003\u001a5\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0002\"\u0004\b\u0000\u0010\u0000*\u001a\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00000\u0001¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {ExifInterface.GPS_DIRECTION_TRUE, "Lcom/discord/utilities/Quad;", "", "toList", "(Lcom/discord/utilities/Quad;)Ljava/util/List;", "utils_release"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class QuadKt {
    public static final <T> List<T> toList(Quad<? extends T, ? extends T, ? extends T, ? extends T> quad) {
        m.checkNotNullParameter(quad, "$this$toList");
        return n.listOf(quad.getFirst(), quad.getSecond(), quad.getThird(), quad.getFourth());
    }
}
