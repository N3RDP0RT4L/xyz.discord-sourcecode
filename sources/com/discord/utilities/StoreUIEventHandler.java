package com.discord.utilities;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.annotation.MainThread;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.rtcconnection.KrispOveruseDetector;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreMediaEngine;
import com.discord.stores.StoreStageChannels;
import com.discord.stores.StoreUserGuildSettings;
import com.discord.stores.StoreVoiceChannelSelected;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.stage.StageRoles;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import xyz.discord.R;
/* compiled from: StoreUIEventHandler.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B7\u0012\u0006\u0010\u000e\u001a\u00020\r\u0012\u0006\u0010\u001d\u001a\u00020\u001c\u0012\u0006\u0010\u0017\u001a\u00020\u0016\u0012\u0006\u0010\u0014\u001a\u00020\u0013\u0012\u0006\u0010\u0011\u001a\u00020\u0010\u0012\u0006\u0010\u001a\u001a\u00020\u0019¢\u0006\u0004\b\u001f\u0010 J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u000b\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\tH\u0003¢\u0006\u0004\b\u000b\u0010\fR\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0011\u001a\u00020\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0014\u001a\u00020\u00138\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u0016\u0010\u0017\u001a\u00020\u00168\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u0016\u0010\u001a\u001a\u00020\u00198\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001a\u0010\u001bR\u0016\u0010\u001d\u001a\u00020\u001c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u001e¨\u0006!"}, d2 = {"Lcom/discord/utilities/StoreUIEventHandler;", "", "", "subscribeToStoreEvents", "()V", "Lcom/discord/rtcconnection/KrispOveruseDetector$Status;", "krispStatusEvent", "handleKrispStatusEvent", "(Lcom/discord/rtcconnection/KrispOveruseDetector$Status;)V", "Lcom/discord/stores/StoreUserGuildSettings$Event;", "userGuildSettingsEvent", "handleUserGuildSettingsEvent", "(Lcom/discord/stores/StoreUserGuildSettings$Event;)V", "Landroid/content/Context;", "context", "Landroid/content/Context;", "Lcom/discord/stores/StoreStageChannels;", "stageChannelsStore", "Lcom/discord/stores/StoreStageChannels;", "Lcom/discord/stores/StoreVoiceChannelSelected;", "voiceChannelSelectedStore", "Lcom/discord/stores/StoreVoiceChannelSelected;", "Lcom/discord/stores/StoreChannels;", "channelsStore", "Lcom/discord/stores/StoreChannels;", "Lcom/discord/stores/StoreUserGuildSettings;", "storeUserGuildSettings", "Lcom/discord/stores/StoreUserGuildSettings;", "Lcom/discord/stores/StoreMediaEngine;", "mediaEngineStore", "Lcom/discord/stores/StoreMediaEngine;", HookHelper.constructorName, "(Landroid/content/Context;Lcom/discord/stores/StoreMediaEngine;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreStageChannels;Lcom/discord/stores/StoreUserGuildSettings;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUIEventHandler {
    private final StoreChannels channelsStore;
    private final Context context;
    private final StoreMediaEngine mediaEngineStore;
    private final StoreStageChannels stageChannelsStore;
    private final StoreUserGuildSettings storeUserGuildSettings;
    private final StoreVoiceChannelSelected voiceChannelSelectedStore;

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            KrispOveruseDetector.Status.values();
            int[] iArr = new int[3];
            $EnumSwitchMapping$0 = iArr;
            iArr[KrispOveruseDetector.Status.CPU_OVERUSE.ordinal()] = 1;
            iArr[KrispOveruseDetector.Status.VAD_CPU_OVERUSE.ordinal()] = 2;
            iArr[KrispOveruseDetector.Status.FAILED.ordinal()] = 3;
            StoreUserGuildSettings.SettingsUpdateType.values();
            int[] iArr2 = new int[4];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[StoreUserGuildSettings.SettingsUpdateType.GUILD.ordinal()] = 1;
            iArr2[StoreUserGuildSettings.SettingsUpdateType.CHANNEL.ordinal()] = 2;
            iArr2[StoreUserGuildSettings.SettingsUpdateType.THREAD.ordinal()] = 3;
            iArr2[StoreUserGuildSettings.SettingsUpdateType.CATEGORY.ordinal()] = 4;
        }
    }

    public StoreUIEventHandler(Context context, StoreMediaEngine storeMediaEngine, StoreChannels storeChannels, StoreVoiceChannelSelected storeVoiceChannelSelected, StoreStageChannels storeStageChannels, StoreUserGuildSettings storeUserGuildSettings) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(storeMediaEngine, "mediaEngineStore");
        m.checkNotNullParameter(storeChannels, "channelsStore");
        m.checkNotNullParameter(storeVoiceChannelSelected, "voiceChannelSelectedStore");
        m.checkNotNullParameter(storeStageChannels, "stageChannelsStore");
        m.checkNotNullParameter(storeUserGuildSettings, "storeUserGuildSettings");
        this.context = context;
        this.mediaEngineStore = storeMediaEngine;
        this.channelsStore = storeChannels;
        this.voiceChannelSelectedStore = storeVoiceChannelSelected;
        this.stageChannelsStore = storeStageChannels;
        this.storeUserGuildSettings = storeUserGuildSettings;
        subscribeToStoreEvents();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleKrispStatusEvent(KrispOveruseDetector.Status status) {
        int i;
        Channel channel;
        StageRoles stageRoles;
        if (status != KrispOveruseDetector.Status.FAILED || (channel = this.channelsStore.getChannel(this.voiceChannelSelectedStore.getSelectedVoiceChannelId())) == null || !ChannelUtils.z(channel) || (stageRoles = this.stageChannelsStore.m10getMyRolesvisDeB4(channel.h())) == null || !StageRoles.m24isAudienceimpl(stageRoles.m29unboximpl())) {
            int ordinal = status.ordinal();
            if (ordinal == 0) {
                i = R.string.mobile_noise_cancellation_cpu_overuse;
            } else if (ordinal == 1) {
                i = R.string.mobile_noise_cancellation_failed;
            } else if (ordinal == 2) {
                i = R.string.mobile_advanced_voice_activity_cpu_overuse;
            } else {
                throw new NoWhenBranchMatchedException();
            }
            b.a.d.m.g(this.context, i, 0, null, 12);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleUserGuildSettingsEvent(StoreUserGuildSettings.Event event) {
        int i;
        if (event instanceof StoreUserGuildSettings.Event.SettingsUpdated) {
            int ordinal = ((StoreUserGuildSettings.Event.SettingsUpdated) event).getType().ordinal();
            if (ordinal == 0) {
                i = R.string.server_settings_updated;
            } else if (ordinal == 1) {
                i = R.string.channel_settings_have_been_updated;
            } else if (ordinal == 2) {
                i = R.string.thread_settings_updated;
            } else if (ordinal == 3) {
                i = R.string.category_settings_have_been_updated;
            } else {
                throw new NoWhenBranchMatchedException();
            }
            b.a.d.m.g(this.context, i, 0, null, 12);
            KotlinExtensionsKt.getExhaustive(Unit.a);
            return;
        }
        throw new NoWhenBranchMatchedException();
    }

    private final void subscribeToStoreEvents() {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(this.mediaEngineStore.onKrispStatusEvent()), StoreUIEventHandler.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreUIEventHandler$subscribeToStoreEvents$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(this.storeUserGuildSettings.observeEvents()), StoreUIEventHandler.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreUIEventHandler$subscribeToStoreEvents$2(this));
    }
}
