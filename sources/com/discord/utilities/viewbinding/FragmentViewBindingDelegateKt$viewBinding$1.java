package com.discord.utilities.viewbinding;

import androidx.exifinterface.media.ExifInterface;
import androidx.viewbinding.ViewBinding;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: FragmentViewBindingDelegate.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u0003\"\b\b\u0000\u0010\u0001*\u00020\u00002\u0006\u0010\u0002\u001a\u00028\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Landroidx/viewbinding/ViewBinding;", ExifInterface.GPS_DIRECTION_TRUE, "it", "", "invoke", "(Landroidx/viewbinding/ViewBinding;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class FragmentViewBindingDelegateKt$viewBinding$1 extends o implements Function1<T, Unit> {
    public static final FragmentViewBindingDelegateKt$viewBinding$1 INSTANCE = new FragmentViewBindingDelegateKt$viewBinding$1();

    public FragmentViewBindingDelegateKt$viewBinding$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Object obj) {
        invoke((ViewBinding) obj);
        return Unit.a;
    }

    /* JADX WARN: Incorrect types in method signature: (TT;)V */
    public final void invoke(ViewBinding viewBinding) {
        m.checkNotNullParameter(viewBinding, "it");
    }
}
