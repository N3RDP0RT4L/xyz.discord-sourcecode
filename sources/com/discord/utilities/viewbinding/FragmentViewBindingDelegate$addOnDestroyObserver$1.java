package com.discord.utilities.viewbinding;

import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.viewbinding.ViewBinding;
import d0.z.d.m;
import kotlin.Metadata;
import y.c.a;
/* compiled from: FragmentViewBindingDelegate.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"com/discord/utilities/viewbinding/FragmentViewBindingDelegate$addOnDestroyObserver$1", "Landroidx/lifecycle/DefaultLifecycleObserver;", "Landroidx/lifecycle/LifecycleOwner;", "owner", "", "onCreate", "(Landroidx/lifecycle/LifecycleOwner;)V", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class FragmentViewBindingDelegate$addOnDestroyObserver$1 implements DefaultLifecycleObserver {
    public final /* synthetic */ FragmentViewBindingDelegate this$0;

    public FragmentViewBindingDelegate$addOnDestroyObserver$1(FragmentViewBindingDelegate fragmentViewBindingDelegate) {
        this.this$0 = fragmentViewBindingDelegate;
    }

    @Override // androidx.lifecycle.DefaultLifecycleObserver, androidx.lifecycle.FullLifecycleObserver
    public void onCreate(LifecycleOwner lifecycleOwner) {
        m.checkNotNullParameter(lifecycleOwner, "owner");
        this.this$0.getFragment().getViewLifecycleOwnerLiveData().observe(this.this$0.getFragment(), new Observer<LifecycleOwner>() { // from class: com.discord.utilities.viewbinding.FragmentViewBindingDelegate$addOnDestroyObserver$1$onCreate$1
            public final void onChanged(LifecycleOwner lifecycleOwner2) {
                m.checkNotNullExpressionValue(lifecycleOwner2, "viewLifecycleOwner");
                lifecycleOwner2.getLifecycle().addObserver(new DefaultLifecycleObserver() { // from class: com.discord.utilities.viewbinding.FragmentViewBindingDelegate$addOnDestroyObserver$1$onCreate$1.1
                    @Override // androidx.lifecycle.DefaultLifecycleObserver, androidx.lifecycle.FullLifecycleObserver
                    public /* synthetic */ void onCreate(LifecycleOwner lifecycleOwner3) {
                        a.a(this, lifecycleOwner3);
                    }

                    @Override // androidx.lifecycle.DefaultLifecycleObserver, androidx.lifecycle.FullLifecycleObserver
                    public void onDestroy(LifecycleOwner lifecycleOwner3) {
                        ViewBinding viewBinding;
                        m.checkNotNullParameter(lifecycleOwner3, "owner");
                        viewBinding = FragmentViewBindingDelegate$addOnDestroyObserver$1.this.this$0.binding;
                        if (viewBinding != null) {
                            FragmentViewBindingDelegate$addOnDestroyObserver$1.this.this$0.getOnViewBindingDestroy().invoke(viewBinding);
                        }
                        FragmentViewBindingDelegate$addOnDestroyObserver$1.this.this$0.binding = null;
                    }

                    @Override // androidx.lifecycle.DefaultLifecycleObserver, androidx.lifecycle.FullLifecycleObserver
                    public /* synthetic */ void onPause(LifecycleOwner lifecycleOwner3) {
                        a.c(this, lifecycleOwner3);
                    }

                    @Override // androidx.lifecycle.DefaultLifecycleObserver, androidx.lifecycle.FullLifecycleObserver
                    public /* synthetic */ void onResume(LifecycleOwner lifecycleOwner3) {
                        a.d(this, lifecycleOwner3);
                    }

                    @Override // androidx.lifecycle.DefaultLifecycleObserver, androidx.lifecycle.FullLifecycleObserver
                    public /* synthetic */ void onStart(LifecycleOwner lifecycleOwner3) {
                        a.e(this, lifecycleOwner3);
                    }

                    @Override // androidx.lifecycle.DefaultLifecycleObserver, androidx.lifecycle.FullLifecycleObserver
                    public /* synthetic */ void onStop(LifecycleOwner lifecycleOwner3) {
                        a.f(this, lifecycleOwner3);
                    }
                });
            }
        });
    }

    @Override // androidx.lifecycle.DefaultLifecycleObserver, androidx.lifecycle.FullLifecycleObserver
    public /* synthetic */ void onDestroy(LifecycleOwner lifecycleOwner) {
        a.b(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.DefaultLifecycleObserver, androidx.lifecycle.FullLifecycleObserver
    public /* synthetic */ void onPause(LifecycleOwner lifecycleOwner) {
        a.c(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.DefaultLifecycleObserver, androidx.lifecycle.FullLifecycleObserver
    public /* synthetic */ void onResume(LifecycleOwner lifecycleOwner) {
        a.d(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.DefaultLifecycleObserver, androidx.lifecycle.FullLifecycleObserver
    public /* synthetic */ void onStart(LifecycleOwner lifecycleOwner) {
        a.e(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.DefaultLifecycleObserver, androidx.lifecycle.FullLifecycleObserver
    public /* synthetic */ void onStop(LifecycleOwner lifecycleOwner) {
        a.f(this, lifecycleOwner);
    }
}
