package com.discord.utilities.cache;

import androidx.exifinterface.media.ExifInterface;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: SharedPreferenceExtensions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u0003\"\u0004\b\u0000\u0010\u0000\"\u0004\b\u0001\u0010\u00012\u0006\u0010\u0002\u001a\u00028\u0001H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"K", ExifInterface.GPS_MEASUREMENT_INTERRUPTED, "entryValue", "", "invoke", "(Ljava/lang/Object;)Ljava/lang/String;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class SharedPreferenceExtensionsKt$putStringEntrySetAsMap$1 extends o implements Function1<V, String> {
    public static final SharedPreferenceExtensionsKt$putStringEntrySetAsMap$1 INSTANCE = new SharedPreferenceExtensionsKt$putStringEntrySetAsMap$1();

    public SharedPreferenceExtensionsKt$putStringEntrySetAsMap$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final String invoke2(V v) {
        return String.valueOf(v);
    }
}
