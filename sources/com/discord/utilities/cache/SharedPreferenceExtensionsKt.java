package com.discord.utilities.cache;

import android.content.SharedPreferences;
import androidx.exifinterface.media.ExifInterface;
import d0.g0.w;
import d0.t.h0;
import d0.t.n0;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
/* compiled from: SharedPreferenceExtensions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\"\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u001a\u001b\u0010\u0003\u001a\u0004\u0018\u00010\u0001*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0001¢\u0006\u0004\b\u0003\u0010\u0004\u001a!\u0010\u0006\u001a\u00020\u0001*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u0001¢\u0006\u0004\b\u0006\u0010\u0007\u001a/\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00010\b*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u00012\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00010\b¢\u0006\u0004\b\n\u0010\u000b\u001aa\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0013\"\u0004\b\u0000\u0010\f\"\u0004\b\u0001\u0010\r*\u00020\u00002\u0006\u0010\u000e\u001a\u00020\u00012\b\b\u0002\u0010\u000f\u001a\u00020\u00012$\u0010\u0012\u001a \u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u0001\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00110\u0010¢\u0006\u0004\b\u0014\u0010\u0015\u001aY\u0010\u001b\u001a\u00020\u001a\"\u0004\b\u0000\u0010\f\"\u0004\b\u0001\u0010\r*\u00020\u00162\u0006\u0010\u000e\u001a\u00020\u00012\u0012\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00172\b\b\u0002\u0010\u000f\u001a\u00020\u00012\u0014\b\u0002\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00020\u00010\u0019¢\u0006\u0004\b\u001b\u0010\u001c¨\u0006\u001d"}, d2 = {"Landroid/content/SharedPreferences;", "", "key", "getString", "(Landroid/content/SharedPreferences;Ljava/lang/String;)Ljava/lang/String;", "defValue", "getStringNonNull", "(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;", "", "defValues", "getStringSetNonNull", "(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;", "K", ExifInterface.GPS_MEASUREMENT_INTERRUPTED, "cacheKey", "delimiter", "Lkotlin/Function2;", "Lkotlin/Pair;", "transformer", "", "getStringEntrySetAsMap", "(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function2;)Ljava/util/Map;", "Landroid/content/SharedPreferences$Editor;", "", "value", "Lkotlin/Function1;", "", "putStringEntrySetAsMap", "(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V", "utils_release"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class SharedPreferenceExtensionsKt {
    public static final String getString(SharedPreferences sharedPreferences, String str) {
        m.checkNotNullParameter(sharedPreferences, "$this$getString");
        m.checkNotNullParameter(str, "key");
        return sharedPreferences.getString(str, null);
    }

    public static final <K, V> Map<K, V> getStringEntrySetAsMap(SharedPreferences sharedPreferences, String str, String str2, Function2<? super String, ? super String, ? extends Pair<? extends K, ? extends V>> function2) {
        m.checkNotNullParameter(sharedPreferences, "$this$getStringEntrySetAsMap");
        m.checkNotNullParameter(str, "cacheKey");
        m.checkNotNullParameter(str2, "delimiter");
        m.checkNotNullParameter(function2, "transformer");
        Set<String> stringSetNonNull$default = getStringSetNonNull$default(sharedPreferences, str, null, 2, null);
        HashMap hashMap = new HashMap(stringSetNonNull$default.size());
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(stringSetNonNull$default, 10));
        for (String str3 : stringSetNonNull$default) {
            List split$default = w.split$default((CharSequence) str3, new String[]{str2}, false, 2, 2, (Object) null);
            arrayList.add(function2.invoke(split$default.get(0), split$default.get(1)));
        }
        return h0.toMap(arrayList, hashMap);
    }

    public static /* synthetic */ Map getStringEntrySetAsMap$default(SharedPreferences sharedPreferences, String str, String str2, Function2 function2, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = ",";
        }
        return getStringEntrySetAsMap(sharedPreferences, str, str2, function2);
    }

    public static final String getStringNonNull(SharedPreferences sharedPreferences, String str, String str2) {
        m.checkNotNullParameter(sharedPreferences, "$this$getStringNonNull");
        m.checkNotNullParameter(str, "key");
        m.checkNotNullParameter(str2, "defValue");
        String string = sharedPreferences.getString(str, str2);
        return string != null ? string : str2;
    }

    public static final Set<String> getStringSetNonNull(SharedPreferences sharedPreferences, String str, Set<String> set) {
        m.checkNotNullParameter(sharedPreferences, "$this$getStringSetNonNull");
        m.checkNotNullParameter(str, "key");
        m.checkNotNullParameter(set, "defValues");
        Set<String> stringSet = sharedPreferences.getStringSet(str, set);
        return stringSet != null ? stringSet : n0.emptySet();
    }

    public static /* synthetic */ Set getStringSetNonNull$default(SharedPreferences sharedPreferences, String str, Set set, int i, Object obj) {
        if ((i & 2) != 0) {
            set = n0.emptySet();
        }
        return getStringSetNonNull(sharedPreferences, str, set);
    }

    public static final <K, V> void putStringEntrySetAsMap(SharedPreferences.Editor editor, String str, Map<K, ? extends V> map, String str2, Function1<? super V, String> function1) {
        m.checkNotNullParameter(editor, "$this$putStringEntrySetAsMap");
        m.checkNotNullParameter(str, "cacheKey");
        m.checkNotNullParameter(map, "value");
        m.checkNotNullParameter(str2, "delimiter");
        m.checkNotNullParameter(function1, "transformer");
        ArrayList arrayList = new ArrayList(map.size());
        for (Map.Entry<K, ? extends V> entry : map.entrySet()) {
            arrayList.add(entry.getKey() + str2 + function1.invoke((V) entry.getValue()));
        }
        editor.putStringSet(str, u.toSet(arrayList));
    }

    public static /* synthetic */ void putStringEntrySetAsMap$default(SharedPreferences.Editor editor, String str, Map map, String str2, Function1 function1, int i, Object obj) {
        if ((i & 4) != 0) {
            str2 = ",";
        }
        if ((i & 8) != 0) {
            function1 = SharedPreferenceExtensionsKt$putStringEntrySetAsMap$1.INSTANCE;
        }
        putStringEntrySetAsMap(editor, str, map, str2, function1);
    }
}
