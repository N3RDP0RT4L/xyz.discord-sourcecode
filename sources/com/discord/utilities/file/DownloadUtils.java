package com.discord.utilities.file;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.annotation.RequiresPermission;
import com.discord.utilities.auth.GoogleSmartLockManager;
import com.discord.utilities.file.DownloadUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import d0.z.d.m;
import d0.z.d.o;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import okhttp3.ResponseBody;
import rx.Emitter;
import rx.Observable;
import rx.functions.Action1;
/* compiled from: DownloadUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001\u000fB\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eJ7\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\t2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00042\b\b\u0002\u0010\b\u001a\u00020\u0007H\u0007¢\u0006\u0004\b\u000b\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/utilities/file/DownloadUtils;", "", "Landroid/content/Context;", "context", "", "fileUrl", "fileName", "Ljava/io/File;", "downloadDirectory", "Lrx/Observable;", "Lcom/discord/utilities/file/DownloadUtils$DownloadState;", "downloadFile", "(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Lrx/Observable;", HookHelper.constructorName, "()V", "DownloadState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class DownloadUtils {
    public static final DownloadUtils INSTANCE = new DownloadUtils();

    /* compiled from: DownloadUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/utilities/file/DownloadUtils$DownloadState;", "", HookHelper.constructorName, "()V", "Completed", "Failure", "InProgress", "Lcom/discord/utilities/file/DownloadUtils$DownloadState$InProgress;", "Lcom/discord/utilities/file/DownloadUtils$DownloadState$Completed;", "Lcom/discord/utilities/file/DownloadUtils$DownloadState$Failure;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class DownloadState {

        /* compiled from: DownloadUtils.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/utilities/file/DownloadUtils$DownloadState$Completed;", "Lcom/discord/utilities/file/DownloadUtils$DownloadState;", "Ljava/io/File;", "file", "Ljava/io/File;", "getFile", "()Ljava/io/File;", HookHelper.constructorName, "(Ljava/io/File;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Completed extends DownloadState {
            private final File file;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Completed(File file) {
                super(null);
                m.checkNotNullParameter(file, "file");
                this.file = file;
            }

            public final File getFile() {
                return this.file;
            }
        }

        /* compiled from: DownloadUtils.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0013\u0012\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\b\u0010\tR\u001d\u0010\u0004\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/utilities/file/DownloadUtils$DownloadState$Failure;", "Lcom/discord/utilities/file/DownloadUtils$DownloadState;", "Ljava/lang/Exception;", "Lkotlin/Exception;", "exception", "Ljava/lang/Exception;", "getException", "()Ljava/lang/Exception;", HookHelper.constructorName, "(Ljava/lang/Exception;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Failure extends DownloadState {
            private final Exception exception;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Failure(Exception exc) {
                super(null);
                m.checkNotNullParameter(exc, "exception");
                this.exception = exc;
            }

            public final Exception getException() {
                return this.exception;
            }
        }

        /* compiled from: DownloadUtils.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0007\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/utilities/file/DownloadUtils$DownloadState$InProgress;", "Lcom/discord/utilities/file/DownloadUtils$DownloadState;", "", "progress", "F", "getProgress", "()F", HookHelper.constructorName, "(F)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class InProgress extends DownloadState {
            private final float progress;

            public InProgress(float f) {
                super(null);
                this.progress = f;
            }

            public final float getProgress() {
                return this.progress;
            }
        }

        private DownloadState() {
        }

        public /* synthetic */ DownloadState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    private DownloadUtils() {
    }

    @RequiresPermission(conditional = GoogleSmartLockManager.SET_DISCORD_ACCOUNT_DETAILS, value = "android.permission.WRITE_EXTERNAL_STORAGE")
    public static final Observable<DownloadState> downloadFile(Context context, final String str, final String str2, final File file) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(str, "fileUrl");
        m.checkNotNullParameter(str2, "fileName");
        m.checkNotNullParameter(file, "downloadDirectory");
        Observable<DownloadState> n = Observable.n(new Action1<Emitter<DownloadState>>() { // from class: com.discord.utilities.file.DownloadUtils$downloadFile$1

            /* compiled from: DownloadUtils.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lokhttp3/ResponseBody;", "responseBody", "", "invoke", "(Lokhttp3/ResponseBody;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.utilities.file.DownloadUtils$downloadFile$1$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends o implements Function1<ResponseBody, Unit> {
                public final /* synthetic */ Emitter $emitter;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public AnonymousClass1(Emitter emitter) {
                    super(1);
                    this.$emitter = emitter;
                }

                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(ResponseBody responseBody) {
                    invoke2(responseBody);
                    return Unit.a;
                }

                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2(ResponseBody responseBody) {
                    m.checkNotNullParameter(responseBody, "responseBody");
                    try {
                        try {
                            DownloadUtils$downloadFile$1 downloadUtils$downloadFile$1 = DownloadUtils$downloadFile$1.this;
                            File file = new File(file, str2);
                            byte[] bArr = new byte[8192];
                            FileOutputStream fileOutputStream = new FileOutputStream(file);
                            float a = (float) responseBody.a();
                            int i = 0;
                            while (true) {
                                int read = responseBody.c().u0().read(bArr);
                                if (read == -1) {
                                    break;
                                }
                                i += read;
                                this.$emitter.onNext(new DownloadUtils.DownloadState.InProgress(i / a));
                                fileOutputStream.write(bArr, 0, read);
                            }
                            this.$emitter.onNext(new DownloadUtils.DownloadState.Completed(file));
                            this.$emitter.onCompleted();
                        } catch (IOException e) {
                            e.printStackTrace();
                            this.$emitter.onNext(new DownloadUtils.DownloadState.Failure(e));
                        }
                    } finally {
                        responseBody.close();
                    }
                }
            }

            public final void call(Emitter<DownloadUtils.DownloadState> emitter) {
                m.checkNotNullParameter(emitter, "emitter");
                emitter.onNext(new DownloadUtils.DownloadState.InProgress(0.0f));
                ObservableExtensionsKt.appSubscribe$default(RestAPI.Companion.getApiFiles().getFile(str), DownloadUtils.INSTANCE.getClass(), (Context) null, (Function1) null, (Function1) null, (Function0) null, (Function0) null, new AnonymousClass1(emitter), 62, (Object) null);
            }
        }, Emitter.BackpressureMode.BUFFER);
        m.checkNotNullExpressionValue(n, "Observable.create({ emit….BackpressureMode.BUFFER)");
        return n;
    }

    public static /* synthetic */ Observable downloadFile$default(Context context, String str, String str2, File file, int i, Object obj) {
        if ((i & 8) != 0) {
            file = context.getCacheDir();
            m.checkNotNullExpressionValue(file, "context.cacheDir");
        }
        return downloadFile(context, str, str2, file);
    }
}
