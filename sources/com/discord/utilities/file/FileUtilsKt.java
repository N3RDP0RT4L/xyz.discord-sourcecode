package com.discord.utilities.file;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import androidx.annotation.DrawableRes;
import androidx.core.app.NotificationCompat;
import androidx.core.net.UriKt;
import b.d.b.a.a;
import com.discord.utilities.drawable.DrawableCompat;
import d0.g0.t;
import d0.z.d.m;
import kotlin.Metadata;
import org.webrtc.MediaStreamTrack;
import xyz.discord.R;
/* compiled from: FileUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\u001a\u001f\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006\u001a\u0017\u0010\t\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0007\u001a\u00020\u0004¢\u0006\u0004\b\t\u0010\n\u001a\u001f\u0010\r\u001a\u00020\f2\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u000b\u001a\u00020\bH\u0007¢\u0006\u0004\b\r\u0010\u000e\u001a\u0017\u0010\u000f\u001a\u00020\b2\u0006\u0010\u0007\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000f\u0010\n¨\u0006\u0010"}, d2 = {"Landroid/content/Context;", "context", "Landroid/net/Uri;", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "", "getFileSizeBytes", "(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/Long;", "bytes", "", "getSizeSubtitle", "(J)Ljava/lang/String;", "mimeType", "", "getIconForFiletype", "(Landroid/content/Context;Ljava/lang/String;)I", "getHumanReadableByteCount", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class FileUtilsKt {
    public static final Long getFileSizeBytes(Context context, Uri uri) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        Long l = null;
        if (Build.VERSION.SDK_INT >= 26) {
            Cursor query = context.getContentResolver().query(uri, null, null, null);
            if (query != null) {
                query.moveToFirst();
            }
            if (query != null) {
                l = Long.valueOf(query.getLong(query.getColumnIndex("_size")));
            }
            if (query == null) {
                return l;
            }
            query.close();
            return l;
        }
        try {
            return Long.valueOf(UriKt.toFile(uri).length());
        } catch (IllegalArgumentException unused) {
            return null;
        }
    }

    private static final String getHumanReadableByteCount(long j) {
        if (j < 1024) {
            return j + " B";
        }
        double d = j;
        double d2 = 1024;
        int log = (int) (Math.log(d) / Math.log(d2));
        return a.N(new Object[]{Double.valueOf(d / Math.pow(d2, log)), Character.valueOf("KMGTPE".charAt(log - 1))}, 2, "%.1f %sB", "java.lang.String.format(format, *args)");
    }

    @DrawableRes
    public static final int getIconForFiletype(Context context, String str) {
        int i;
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(str, "mimeType");
        if (t.startsWith$default(str, "image", false, 2, null)) {
            i = R.attr.ic_uploads_image;
        } else {
            i = t.startsWith$default(str, MediaStreamTrack.VIDEO_TRACK_KIND, false, 2, null) ? R.attr.ic_uploads_video : R.attr.ic_uploads_file;
        }
        return DrawableCompat.getThemedDrawableRes$default(context, i, 0, 2, (Object) null);
    }

    public static final String getSizeSubtitle(long j) {
        if (j != -1) {
            return getHumanReadableByteCount(j);
        }
        return null;
    }
}
