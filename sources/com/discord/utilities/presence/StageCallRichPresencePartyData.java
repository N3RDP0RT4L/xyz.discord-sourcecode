package com.discord.utilities.presence;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import kotlin.Metadata;
/* compiled from: ActivityUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0012\b\u0086\b\u0018\u00002\u00020\u0001BS\u0012\n\u0010\u0011\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010\u0012\u001a\u00060\u0002j\u0002`\u0006\u0012\u0006\u0010\u0013\u001a\u00020\b\u0012\n\u0010\u0014\u001a\u00060\u0002j\u0002`\u000b\u0012\u0006\u0010\u0015\u001a\u00020\b\u0012\u0006\u0010\u0016\u001a\u00020\b\u0012\u0006\u0010\u0017\u001a\u00020\u0002\u0012\u0006\u0010\u0018\u001a\u00020\u0002¢\u0006\u0004\b.\u0010/J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\u0007\u001a\u00060\u0002j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0005J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0014\u0010\f\u001a\u00060\u0002j\u0002`\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\u0005J\u0010\u0010\r\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\r\u0010\nJ\u0010\u0010\u000e\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u000e\u0010\nJ\u0010\u0010\u000f\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0005J\u0010\u0010\u0010\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0005Jl\u0010\u0019\u001a\u00020\u00002\f\b\u0002\u0010\u0011\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010\u0012\u001a\u00060\u0002j\u0002`\u00062\b\b\u0002\u0010\u0013\u001a\u00020\b2\f\b\u0002\u0010\u0014\u001a\u00060\u0002j\u0002`\u000b2\b\b\u0002\u0010\u0015\u001a\u00020\b2\b\b\u0002\u0010\u0016\u001a\u00020\b2\b\b\u0002\u0010\u0017\u001a\u00020\u00022\b\b\u0002\u0010\u0018\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u001c\u001a\u00020\u001bHÖ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001f\u001a\u00020\u001eHÖ\u0001¢\u0006\u0004\b\u001f\u0010 J\u001a\u0010\"\u001a\u00020\b2\b\u0010!\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\"\u0010#R\u001d\u0010\u0014\u001a\u00060\u0002j\u0002`\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010$\u001a\u0004\b%\u0010\u0005R\u0019\u0010\u0015\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010&\u001a\u0004\b'\u0010\nR\u001d\u0010\u0011\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010$\u001a\u0004\b(\u0010\u0005R\u001d\u0010\u0012\u001a\u00060\u0002j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010$\u001a\u0004\b)\u0010\u0005R\u0019\u0010\u0013\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010&\u001a\u0004\b*\u0010\nR\u0019\u0010\u0017\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010$\u001a\u0004\b+\u0010\u0005R\u0019\u0010\u0016\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010&\u001a\u0004\b,\u0010\nR\u0019\u0010\u0018\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010$\u001a\u0004\b-\u0010\u0005¨\u00060"}, d2 = {"Lcom/discord/utilities/presence/StageCallRichPresencePartyData;", "", "", "Lcom/discord/primitives/StageInstanceId;", "component1", "()J", "Lcom/discord/primitives/ChannelId;", "component2", "", "component3", "()Z", "Lcom/discord/primitives/GuildId;", "component4", "component5", "component6", "component7", "component8", "stageInstanceId", "channelId", "userIsSpeaker", "guildId", "guildIsPartnered", "guildIsVerified", "speakerCount", "audienceSize", "copy", "(JJZJZZJJ)Lcom/discord/utilities/presence/StageCallRichPresencePartyData;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "J", "getGuildId", "Z", "getGuildIsPartnered", "getStageInstanceId", "getChannelId", "getUserIsSpeaker", "getSpeakerCount", "getGuildIsVerified", "getAudienceSize", HookHelper.constructorName, "(JJZJZZJJ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StageCallRichPresencePartyData {
    private final long audienceSize;
    private final long channelId;
    private final long guildId;
    private final boolean guildIsPartnered;
    private final boolean guildIsVerified;
    private final long speakerCount;
    private final long stageInstanceId;
    private final boolean userIsSpeaker;

    public StageCallRichPresencePartyData(long j, long j2, boolean z2, long j3, boolean z3, boolean z4, long j4, long j5) {
        this.stageInstanceId = j;
        this.channelId = j2;
        this.userIsSpeaker = z2;
        this.guildId = j3;
        this.guildIsPartnered = z3;
        this.guildIsVerified = z4;
        this.speakerCount = j4;
        this.audienceSize = j5;
    }

    public final long component1() {
        return this.stageInstanceId;
    }

    public final long component2() {
        return this.channelId;
    }

    public final boolean component3() {
        return this.userIsSpeaker;
    }

    public final long component4() {
        return this.guildId;
    }

    public final boolean component5() {
        return this.guildIsPartnered;
    }

    public final boolean component6() {
        return this.guildIsVerified;
    }

    public final long component7() {
        return this.speakerCount;
    }

    public final long component8() {
        return this.audienceSize;
    }

    public final StageCallRichPresencePartyData copy(long j, long j2, boolean z2, long j3, boolean z3, boolean z4, long j4, long j5) {
        return new StageCallRichPresencePartyData(j, j2, z2, j3, z3, z4, j4, j5);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StageCallRichPresencePartyData)) {
            return false;
        }
        StageCallRichPresencePartyData stageCallRichPresencePartyData = (StageCallRichPresencePartyData) obj;
        return this.stageInstanceId == stageCallRichPresencePartyData.stageInstanceId && this.channelId == stageCallRichPresencePartyData.channelId && this.userIsSpeaker == stageCallRichPresencePartyData.userIsSpeaker && this.guildId == stageCallRichPresencePartyData.guildId && this.guildIsPartnered == stageCallRichPresencePartyData.guildIsPartnered && this.guildIsVerified == stageCallRichPresencePartyData.guildIsVerified && this.speakerCount == stageCallRichPresencePartyData.speakerCount && this.audienceSize == stageCallRichPresencePartyData.audienceSize;
    }

    public final long getAudienceSize() {
        return this.audienceSize;
    }

    public final long getChannelId() {
        return this.channelId;
    }

    public final long getGuildId() {
        return this.guildId;
    }

    public final boolean getGuildIsPartnered() {
        return this.guildIsPartnered;
    }

    public final boolean getGuildIsVerified() {
        return this.guildIsVerified;
    }

    public final long getSpeakerCount() {
        return this.speakerCount;
    }

    public final long getStageInstanceId() {
        return this.stageInstanceId;
    }

    public final boolean getUserIsSpeaker() {
        return this.userIsSpeaker;
    }

    public int hashCode() {
        int a = (b.a(this.channelId) + (b.a(this.stageInstanceId) * 31)) * 31;
        boolean z2 = this.userIsSpeaker;
        int i = 1;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        int a2 = (b.a(this.guildId) + ((a + i2) * 31)) * 31;
        boolean z3 = this.guildIsPartnered;
        if (z3) {
            z3 = true;
        }
        int i4 = z3 ? 1 : 0;
        int i5 = z3 ? 1 : 0;
        int i6 = (a2 + i4) * 31;
        boolean z4 = this.guildIsVerified;
        if (!z4) {
            i = z4 ? 1 : 0;
        }
        int a3 = b.a(this.speakerCount);
        return b.a(this.audienceSize) + ((a3 + ((i6 + i) * 31)) * 31);
    }

    public String toString() {
        StringBuilder R = a.R("StageCallRichPresencePartyData(stageInstanceId=");
        R.append(this.stageInstanceId);
        R.append(", channelId=");
        R.append(this.channelId);
        R.append(", userIsSpeaker=");
        R.append(this.userIsSpeaker);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", guildIsPartnered=");
        R.append(this.guildIsPartnered);
        R.append(", guildIsVerified=");
        R.append(this.guildIsVerified);
        R.append(", speakerCount=");
        R.append(this.speakerCount);
        R.append(", audienceSize=");
        return a.B(R, this.audienceSize, ")");
    }
}
