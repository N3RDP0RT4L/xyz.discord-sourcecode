package com.discord.utilities.presence;

import com.discord.api.activity.Activity;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: PresenceUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000f\n\u0002\b\u0003\u0010\u0005\u001a\b\u0012\u0002\b\u0003\u0018\u00010\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/activity/Activity;", "it", "", "invoke", "(Lcom/discord/api/activity/Activity;)Ljava/lang/Comparable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class PresenceUtils$ACTIVITY_COMPARATOR$1 extends o implements Function1<Activity, Comparable<?>> {
    public static final PresenceUtils$ACTIVITY_COMPARATOR$1 INSTANCE = new PresenceUtils$ACTIVITY_COMPARATOR$1();

    public PresenceUtils$ACTIVITY_COMPARATOR$1() {
        super(1);
    }

    public final Comparable<?> invoke(Activity activity) {
        m.checkNotNullParameter(activity, "it");
        return activity.p();
    }
}
