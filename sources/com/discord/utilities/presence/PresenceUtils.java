package com.discord.utilities.presence;

import andhook.lib.HookHelper;
import android.content.Context;
import android.text.SpannableStringBuilder;
import androidx.annotation.StringRes;
import androidx.appcompat.widget.ActivityChooserModel;
import b.a.k.b;
import com.discord.api.activity.Activity;
import com.discord.api.activity.ActivityEmoji;
import com.discord.api.activity.ActivityParty;
import com.discord.api.activity.ActivityPlatform;
import com.discord.api.activity.ActivityType;
import com.discord.api.presence.ClientStatus;
import com.discord.api.presence.ClientStatuses;
import com.discord.models.presence.Presence;
import com.discord.utilities.platform.Platform;
import com.discord.utilities.textprocessing.node.EmojiNode;
import com.discord.utilities.view.text.SimpleDraweeSpanTextView;
import com.facebook.drawee.span.DraweeSpanStringBuilder;
import d0.t.u;
import d0.u.a;
import d0.z.d.m;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: PresenceUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u000b\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\bH\u0010IJ=\u0010\u000b\u001a\u00020\n2\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\b\b\u0002\u0010\b\u001a\u00020\u00042\b\b\u0002\u0010\t\u001a\u00020\u0004H\u0007¢\u0006\u0004\b\u000b\u0010\fJ\u001f\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u000fH\u0007¢\u0006\u0004\b\u0012\u0010\u0013J!\u0010\u0014\u001a\u00020\u00112\u0006\u0010\u000e\u001a\u00020\r2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J#\u0010\u0017\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u000e\u001a\u00020\r2\b\u0010\u0016\u001a\u0004\u0018\u00010\u000fH\u0002¢\u0006\u0004\b\u0017\u0010\u0013J5\u0010\u0018\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u000e\u001a\u00020\r2\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\b\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0018\u0010\u0019JE\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u000e\u001a\u00020\r2\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\u00042\b\b\u0002\u0010\b\u001a\u00020\u00042\b\b\u0002\u0010\u001a\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u001c\u0010\u001dJ\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u000f*\b\u0012\u0004\u0012\u00020\u000f0\u001eH\u0002¢\u0006\u0004\b\u001f\u0010 J\u001b\u0010!\u001a\u0004\u0018\u00010\u000f*\b\u0012\u0004\u0012\u00020\u000f0\u001eH\u0002¢\u0006\u0004\b!\u0010 J\u0015\u0010\u0018\u001a\u00020\"*\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0018\u0010#J\u0017\u0010$\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b$\u0010%J\u0017\u0010&\u001a\u00020\"2\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b&\u0010#J!\u0010)\u001a\u0004\u0018\u00010\u000f*\b\u0012\u0004\u0012\u00020\u000f0\u001e2\u0006\u0010(\u001a\u00020'¢\u0006\u0004\b)\u0010*R,\u0010-\u001a\u0012\u0012\u0004\u0012\u00020\u000f0+j\b\u0012\u0004\u0012\u00020\u000f`,8\u0000@\u0000X\u0080\u0004¢\u0006\f\n\u0004\b-\u0010.\u001a\u0004\b/\u00100R\u0017\u00105\u001a\u000202*\u0002018F@\u0006¢\u0006\u0006\u001a\u0004\b3\u00104R\u0019\u00108\u001a\u0004\u0018\u00010\u000f*\u00020\u00028F@\u0006¢\u0006\u0006\u001a\u0004\b6\u00107R\u0019\u00109\u001a\u0004\u0018\u00010\u000f*\u00020\u00028F@\u0006¢\u0006\u0006\u001a\u0004\b!\u00107R\u0019\u0010;\u001a\u0004\u0018\u00010\u000f*\u00020\u00028F@\u0006¢\u0006\u0006\u001a\u0004\b:\u00107R\u0019\u0010<\u001a\u0004\u0018\u00010\u000f*\u00020\u00028F@\u0006¢\u0006\u0006\u001a\u0004\b\u001f\u00107R\u001f\u0010;\u001a\u0004\u0018\u00010\u000f*\b\u0012\u0004\u0012\u00020\u000f0\u001e8F@\u0006¢\u0006\u0006\u001a\u0004\b:\u0010 R\u0019\u0010>\u001a\u0004\u0018\u00010\u000f*\u00020\u00028F@\u0006¢\u0006\u0006\u001a\u0004\b=\u00107R\u0017\u0010@\u001a\u00020\u0004*\u00020?8F@\u0006¢\u0006\u0006\u001a\u0004\b@\u0010AR\u0017\u0010C\u001a\u000202*\u0002018F@\u0006¢\u0006\u0006\u001a\u0004\bB\u00104R\u0017\u0010E\u001a\u000202*\u0002018F@\u0006¢\u0006\u0006\u001a\u0004\bD\u00104R\u0019\u0010G\u001a\u0004\u0018\u00010\u000f*\u00020\u00028F@\u0006¢\u0006\u0006\u001a\u0004\bF\u00107¨\u0006J"}, d2 = {"Lcom/discord/utilities/presence/PresenceUtils;", "", "Lcom/discord/models/presence/Presence;", "presence", "", "isStreamingApplication", "Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;", "textView", "showFallbackStatusText", "hideEmoji", "", "setPresenceText", "(Lcom/discord/models/presence/Presence;ZLcom/discord/utilities/view/text/SimpleDraweeSpanTextView;ZZ)V", "Landroid/content/Context;", "context", "Lcom/discord/api/activity/Activity;", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "", "getActivityHeader", "(Landroid/content/Context;Lcom/discord/api/activity/Activity;)Ljava/lang/CharSequence;", "getApplicationStreamingString", "(Landroid/content/Context;Lcom/discord/models/presence/Presence;)Ljava/lang/CharSequence;", "activityModel", "getActivityString", "getStatusText", "(Landroid/content/Context;Lcom/discord/models/presence/Presence;ZZ)Ljava/lang/CharSequence;", "animateCustomStatusEmoji", "Lcom/facebook/drawee/span/DraweeSpanStringBuilder;", "getStatusDraweeSpanStringBuilder", "(Landroid/content/Context;Lcom/discord/models/presence/Presence;ZZZZ)Lcom/facebook/drawee/span/DraweeSpanStringBuilder;", "", "getStageChannelActivity", "(Ljava/util/List;)Lcom/discord/api/activity/Activity;", "getSpotifyListeningActivity", "", "(Lcom/discord/models/presence/Presence;)I", "shouldShowRichPresenceIcon", "(Lcom/discord/models/presence/Presence;)Z", "getStatusStringResForPresence", "Lcom/discord/api/activity/ActivityType;", "type", "getActivityByType", "(Ljava/util/List;Lcom/discord/api/activity/ActivityType;)Lcom/discord/api/activity/Activity;", "Ljava/util/Comparator;", "Lkotlin/Comparator;", "ACTIVITY_COMPARATOR", "Ljava/util/Comparator;", "getACTIVITY_COMPARATOR$app_productionGoogleRelease", "()Ljava/util/Comparator;", "Lcom/discord/api/activity/ActivityParty;", "", "getNumOpenSlots", "(Lcom/discord/api/activity/ActivityParty;)J", "numOpenSlots", "getStreamingActivity", "(Lcom/discord/models/presence/Presence;)Lcom/discord/api/activity/Activity;", "streamingActivity", "spotifyListeningActivity", "getPrimaryActivity", "primaryActivity", "stageChannelActivity", "getCustomStatusActivity", "customStatusActivity", "Lcom/discord/api/presence/ClientStatuses;", "isMobile", "(Lcom/discord/api/presence/ClientStatuses;)Z", "getMaxSize", "maxSize", "getCurrentSize", "currentSize", "getPlayingActivity", "playingActivity", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class PresenceUtils {
    public static final PresenceUtils INSTANCE = new PresenceUtils();
    private static final Comparator<Activity> ACTIVITY_COMPARATOR = a.compareBy(PresenceUtils$ACTIVITY_COMPARATOR$1.INSTANCE, PresenceUtils$ACTIVITY_COMPARATOR$2.INSTANCE, PresenceUtils$ACTIVITY_COMPARATOR$3.INSTANCE);

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;
        public static final /* synthetic */ int[] $EnumSwitchMapping$2;

        static {
            ActivityType.values();
            int[] iArr = new int[7];
            $EnumSwitchMapping$0 = iArr;
            ActivityType activityType = ActivityType.STREAMING;
            iArr[activityType.ordinal()] = 1;
            ActivityType activityType2 = ActivityType.LISTENING;
            iArr[activityType2.ordinal()] = 2;
            ActivityType activityType3 = ActivityType.WATCHING;
            iArr[activityType3.ordinal()] = 3;
            ActivityType activityType4 = ActivityType.PLAYING;
            iArr[activityType4.ordinal()] = 4;
            ActivityType activityType5 = ActivityType.COMPETING;
            iArr[activityType5.ordinal()] = 5;
            ActivityType.values();
            int[] iArr2 = new int[7];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[activityType4.ordinal()] = 1;
            iArr2[activityType2.ordinal()] = 2;
            iArr2[activityType.ordinal()] = 3;
            iArr2[activityType3.ordinal()] = 4;
            iArr2[activityType5.ordinal()] = 5;
            ClientStatus.values();
            int[] iArr3 = new int[5];
            $EnumSwitchMapping$2 = iArr3;
            iArr3[ClientStatus.ONLINE.ordinal()] = 1;
            iArr3[ClientStatus.IDLE.ordinal()] = 2;
            iArr3[ClientStatus.DND.ordinal()] = 3;
        }
    }

    private PresenceUtils() {
    }

    public static final CharSequence getActivityHeader(Context context, Activity activity) {
        CharSequence b2;
        Object obj;
        CharSequence b3;
        CharSequence b4;
        CharSequence b5;
        CharSequence b6;
        CharSequence b7;
        CharSequence b8;
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(activity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
        int ordinal = activity.p().ordinal();
        if (ordinal == 0) {
            ActivityPlatform j = activity.j();
            if (j != null) {
                Platform from = Platform.Companion.from(j);
                if (j == ActivityPlatform.PS4) {
                    obj = "PS4";
                } else if (j == ActivityPlatform.PS5) {
                    obj = "PS5";
                } else {
                    obj = j;
                    if (from != Platform.NONE) {
                        obj = from.getProperName();
                    }
                }
                if (obj != null) {
                    b3 = b.b(context, R.string.user_activity_header_playing_on_platform, new Object[]{obj}, (r4 & 4) != 0 ? b.C0034b.j : null);
                    return b3;
                }
            }
            b2 = b.b(context, R.string.user_activity_header_playing, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            return b2;
        } else if (ordinal == 1) {
            b4 = b.b(context, R.string.user_activity_header_live_on_platform, new Object[]{activity.h()}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b4;
        } else if (ordinal == 2) {
            b5 = b.b(context, R.string.user_activity_header_listening, new Object[]{activity.h()}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b5;
        } else if (ordinal == 3) {
            b6 = b.b(context, R.string.user_activity_header_watching, new Object[]{activity.h()}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b6;
        } else if (ordinal != 5) {
            b8 = b.b(context, R.string.user_activity_header_playing, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            return b8;
        } else {
            b7 = b.b(context, R.string.user_activity_header_competing, new Object[]{activity.h()}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b7;
        }
    }

    private final CharSequence getActivityString(Context context, Activity activity) {
        CharSequence b2;
        CharSequence b3;
        CharSequence b4;
        CharSequence b5;
        CharSequence b6;
        ActivityType p = activity != null ? activity.p() : null;
        if (p == null) {
            return null;
        }
        int ordinal = p.ordinal();
        if (ordinal == 0) {
            b2 = b.b(context, R.string.playing_game, new Object[]{activity.h()}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b2;
        } else if (ordinal == 1) {
            Object[] objArr = new Object[1];
            String e = activity.e();
            if (e == null) {
                e = activity.h();
            }
            objArr[0] = e;
            b3 = b.b(context, R.string.streaming, objArr, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b3;
        } else if (ordinal == 2) {
            b4 = b.b(context, R.string.listening_to, new Object[]{activity.h()}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b4;
        } else if (ordinal == 3) {
            Object[] objArr2 = new Object[1];
            String e2 = activity.e();
            if (e2 == null) {
                e2 = activity.h();
            }
            objArr2[0] = e2;
            b5 = b.b(context, R.string.watching, objArr2, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b5;
        } else if (ordinal != 5) {
            return null;
        } else {
            b6 = b.b(context, R.string.competing, new Object[]{activity.h()}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b6;
        }
    }

    private final CharSequence getApplicationStreamingString(Context context, Presence presence) {
        CharSequence b2;
        Activity playingActivity;
        CharSequence b3;
        if (presence == null || (playingActivity = getPlayingActivity(presence)) == null) {
            b2 = b.b(context, R.string.streaming_a_game, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            return b2;
        }
        b3 = b.b(context, R.string.streaming, new Object[]{playingActivity.h()}, (r4 & 4) != 0 ? b.C0034b.j : null);
        return b3;
    }

    private final DraweeSpanStringBuilder getStatusDraweeSpanStringBuilder(final Context context, Presence presence, boolean z2, boolean z3, boolean z4, final boolean z5) {
        Activity customStatusActivity;
        ActivityEmoji f;
        DraweeSpanStringBuilder draweeSpanStringBuilder = new DraweeSpanStringBuilder();
        EmojiNode emojiNode = null;
        if (!(z3 || presence == null || (customStatusActivity = getCustomStatusActivity(presence)) == null || (f = customStatusActivity.f()) == null)) {
            EmojiNode.Companion companion = EmojiNode.Companion;
            emojiNode = EmojiNode.Companion.from$default(companion, 0, companion.generateEmojiIdAndType(f), 1, (Object) null);
        }
        if (emojiNode != null) {
            emojiNode.render((SpannableStringBuilder) draweeSpanStringBuilder, (DraweeSpanStringBuilder) new EmojiNode.RenderContext(context, z5) { // from class: com.discord.utilities.presence.PresenceUtils$getStatusDraweeSpanStringBuilder$1
                public final /* synthetic */ boolean $animateCustomStatusEmoji;
                public final /* synthetic */ Context $context;
                private final Context context;
                private final boolean isAnimationEnabled;

                {
                    this.$context = context;
                    this.$animateCustomStatusEmoji = z5;
                    this.context = context;
                    this.isAnimationEnabled = z5;
                }

                @Override // com.discord.utilities.textprocessing.node.EmojiNode.RenderContext
                public Context getContext() {
                    return this.context;
                }

                @Override // com.discord.utilities.textprocessing.node.EmojiNode.RenderContext
                public boolean isAnimationEnabled() {
                    return this.isAnimationEnabled;
                }

                @Override // com.discord.utilities.textprocessing.node.EmojiNode.RenderContext
                public void onEmojiClicked(EmojiNode.EmojiIdAndType emojiIdAndType) {
                    m.checkNotNullParameter(emojiIdAndType, "emojiIdAndType");
                    EmojiNode.RenderContext.DefaultImpls.onEmojiClicked(this, emojiIdAndType);
                }
            });
        }
        CharSequence statusText = getStatusText(context, presence, z2, z4);
        if (statusText != null) {
            if (emojiNode != null) {
                draweeSpanStringBuilder.append((char) 8194);
            }
            draweeSpanStringBuilder.append(statusText);
        }
        return draweeSpanStringBuilder;
    }

    public static /* synthetic */ DraweeSpanStringBuilder getStatusDraweeSpanStringBuilder$default(PresenceUtils presenceUtils, Context context, Presence presence, boolean z2, boolean z3, boolean z4, boolean z5, int i, Object obj) {
        return presenceUtils.getStatusDraweeSpanStringBuilder(context, presence, z2, z3, (i & 16) != 0 ? false : z4, (i & 32) != 0 ? false : z5);
    }

    private final CharSequence getStatusText(Context context, Presence presence, boolean z2, boolean z3) {
        Activity activity;
        CharSequence b2;
        Activity customStatusActivity;
        String l = (presence == null || (customStatusActivity = getCustomStatusActivity(presence)) == null) ? null : customStatusActivity.l();
        if (l != null) {
            return l;
        }
        if (z2) {
            return getApplicationStreamingString(context, presence);
        }
        if (presence != null) {
            activity = getPrimaryActivity(presence);
        } else {
            activity = null;
        }
        CharSequence activityString = getActivityString(context, activity);
        if (activityString != null) {
            return activityString;
        }
        if (!z3) {
            return null;
        }
        b2 = b.b(context, getStatusText(presence), new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        return b2;
    }

    public static /* synthetic */ CharSequence getStatusText$default(PresenceUtils presenceUtils, Context context, Presence presence, boolean z2, boolean z3, int i, Object obj) {
        if ((i & 8) != 0) {
            z3 = false;
        }
        return presenceUtils.getStatusText(context, presence, z2, z3);
    }

    public static final void setPresenceText(Presence presence, boolean z2, SimpleDraweeSpanTextView simpleDraweeSpanTextView, boolean z3, boolean z4) {
        m.checkNotNullParameter(simpleDraweeSpanTextView, "textView");
        PresenceUtils presenceUtils = INSTANCE;
        Context context = simpleDraweeSpanTextView.getContext();
        m.checkNotNullExpressionValue(context, "textView.context");
        DraweeSpanStringBuilder statusDraweeSpanStringBuilder$default = getStatusDraweeSpanStringBuilder$default(presenceUtils, context, presence, z2, z4, z3, false, 32, null);
        simpleDraweeSpanTextView.setDraweeSpanStringBuilder(statusDraweeSpanStringBuilder$default);
        int i = 0;
        if (!(statusDraweeSpanStringBuilder$default.length() > 0)) {
            i = 8;
        }
        simpleDraweeSpanTextView.setVisibility(i);
    }

    public static /* synthetic */ void setPresenceText$default(Presence presence, boolean z2, SimpleDraweeSpanTextView simpleDraweeSpanTextView, boolean z3, boolean z4, int i, Object obj) {
        if ((i & 8) != 0) {
            z3 = false;
        }
        if ((i & 16) != 0) {
            z4 = false;
        }
        setPresenceText(presence, z2, simpleDraweeSpanTextView, z3, z4);
    }

    public final Comparator<Activity> getACTIVITY_COMPARATOR$app_productionGoogleRelease() {
        return ACTIVITY_COMPARATOR;
    }

    public final Activity getActivityByType(List<Activity> list, ActivityType activityType) {
        Object obj;
        boolean z2;
        m.checkNotNullParameter(list, "$this$getActivityByType");
        m.checkNotNullParameter(activityType, "type");
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (((Activity) obj).p() == activityType) {
                z2 = true;
                continue;
            } else {
                z2 = false;
                continue;
            }
            if (z2) {
                break;
            }
        }
        return (Activity) obj;
    }

    public final long getCurrentSize(ActivityParty activityParty) {
        Long l;
        m.checkNotNullParameter(activityParty, "$this$currentSize");
        List<Long> b2 = activityParty.b();
        if (b2 == null || (l = (Long) u.first((List<? extends Object>) b2)) == null) {
            return 0L;
        }
        return l.longValue();
    }

    public final Activity getCustomStatusActivity(Presence presence) {
        m.checkNotNullParameter(presence, "$this$customStatusActivity");
        List<Activity> activities = presence.getActivities();
        if (activities != null) {
            return getActivityByType(activities, ActivityType.CUSTOM_STATUS);
        }
        return null;
    }

    public final long getMaxSize(ActivityParty activityParty) {
        Long l;
        m.checkNotNullParameter(activityParty, "$this$maxSize");
        List<Long> b2 = activityParty.b();
        if (b2 == null || (l = (Long) u.last((List<? extends Object>) b2)) == null) {
            return 0L;
        }
        return l.longValue();
    }

    public final long getNumOpenSlots(ActivityParty activityParty) {
        m.checkNotNullParameter(activityParty, "$this$numOpenSlots");
        return getMaxSize(activityParty) - getCurrentSize(activityParty);
    }

    public final Activity getPlayingActivity(Presence presence) {
        m.checkNotNullParameter(presence, "$this$playingActivity");
        List<Activity> activities = presence.getActivities();
        if (activities != null) {
            return getActivityByType(activities, ActivityType.PLAYING);
        }
        return null;
    }

    public final Activity getPrimaryActivity(Presence presence) {
        m.checkNotNullParameter(presence, "$this$primaryActivity");
        List<Activity> activities = presence.getActivities();
        if (activities != null) {
            return getPrimaryActivity(activities);
        }
        return null;
    }

    public final Activity getSpotifyListeningActivity(Presence presence) {
        m.checkNotNullParameter(presence, "$this$spotifyListeningActivity");
        List<Activity> activities = presence.getActivities();
        if (activities != null) {
            return getSpotifyListeningActivity(activities);
        }
        return null;
    }

    public final Activity getStageChannelActivity(Presence presence) {
        m.checkNotNullParameter(presence, "$this$stageChannelActivity");
        List<Activity> activities = presence.getActivities();
        if (activities != null) {
            return getStageChannelActivity(activities);
        }
        return null;
    }

    @StringRes
    public final int getStatusStringResForPresence(Presence presence) {
        m.checkNotNullParameter(presence, "presence");
        return getStatusText(presence);
    }

    public final Activity getStreamingActivity(Presence presence) {
        m.checkNotNullParameter(presence, "$this$streamingActivity");
        List<Activity> activities = presence.getActivities();
        if (activities != null) {
            return getActivityByType(activities, ActivityType.STREAMING);
        }
        return null;
    }

    public final boolean isMobile(ClientStatuses clientStatuses) {
        m.checkNotNullParameter(clientStatuses, "$this$isMobile");
        ClientStatus b2 = clientStatuses.b();
        ClientStatus clientStatus = ClientStatus.ONLINE;
        return (b2 != clientStatus || clientStatuses.c() == clientStatus || clientStatuses.a() == clientStatus) ? false : true;
    }

    public final boolean shouldShowRichPresenceIcon(Presence presence) {
        List<Activity> activities;
        boolean z2;
        if (!(presence == null || (activities = presence.getActivities()) == null)) {
            if (!activities.isEmpty()) {
                for (Activity activity : activities) {
                    if (ActivityUtilsKt.isRichPresence(activity)) {
                        z2 = true;
                        break;
                    }
                }
            }
            z2 = false;
            if (z2) {
                return true;
            }
        }
        return false;
    }

    private final Activity getSpotifyListeningActivity(List<Activity> list) {
        Object obj;
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (ActivityUtilsKt.isSpotifyActivity((Activity) obj)) {
                break;
            }
        }
        return (Activity) obj;
    }

    private final Activity getStageChannelActivity(List<Activity> list) {
        Object obj;
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (ActivityUtilsKt.isStageChannelActivity((Activity) obj)) {
                break;
            }
        }
        return (Activity) obj;
    }

    public final Activity getPrimaryActivity(List<Activity> list) {
        Object obj;
        boolean z2;
        m.checkNotNullParameter(list, "$this$primaryActivity");
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (((Activity) obj).p() != ActivityType.CUSTOM_STATUS) {
                z2 = true;
                continue;
            } else {
                z2 = false;
                continue;
            }
            if (z2) {
                break;
            }
        }
        return (Activity) obj;
    }

    private final int getStatusText(Presence presence) {
        ClientStatus status = presence != null ? presence.getStatus() : null;
        if (status != null) {
            int ordinal = status.ordinal();
            if (ordinal == 0) {
                return R.string.status_online;
            }
            if (ordinal == 1) {
                return R.string.status_idle;
            }
            if (ordinal == 2) {
                return R.string.status_dnd;
            }
        }
        return R.string.status_offline;
    }
}
