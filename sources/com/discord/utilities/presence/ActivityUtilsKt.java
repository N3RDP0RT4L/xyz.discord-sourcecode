package com.discord.utilities.presence;

import androidx.appcompat.widget.ActivityChooserModel;
import com.discord.api.activity.Activity;
import com.discord.api.activity.ActivityAssets;
import com.discord.api.activity.ActivityEmoji;
import com.discord.api.activity.ActivityParty;
import com.discord.api.activity.ActivityPlatform;
import com.discord.api.activity.ActivityTimestamps;
import com.discord.api.activity.ActivityType;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guild.GuildFeature;
import com.discord.api.stageinstance.StageInstance;
import com.discord.api.stageinstance.StageInstancePrivacyLevel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreThread;
import com.discord.utilities.platform.Platform;
import com.discord.utilities.time.ClockFactory;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.discord.widgets.stage.StageRoles;
import d0.g0.a;
import d0.g0.t;
import d0.t.n;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
/* compiled from: ActivityUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0018\u001a\u0019\u0010\u0004\u001a\u00020\u0003*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0001¢\u0006\u0004\b\u0004\u0010\u0005\u001a!\u0010\n\u001a\u0004\u0018\u00010\u00002\b\u0010\u0007\u001a\u0004\u0018\u00010\u00062\u0006\u0010\t\u001a\u00020\b¢\u0006\u0004\b\n\u0010\u000b\u001am\u0010\u0015\u001a\u0004\u0018\u00010\u00002\u0006\u0010\t\u001a\u00020\b2\b\u0010\f\u001a\u0004\u0018\u00010\u00062\b\u0010\r\u001a\u0004\u0018\u00010\u00062\b\u0010\u000e\u001a\u0004\u0018\u00010\u00062\b\u0010\u000f\u001a\u0004\u0018\u00010\u00062\b\u0010\u0010\u001a\u0004\u0018\u00010\u00062\b\u0010\u0011\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0012\u001a\u00020\b2\u0006\u0010\u0013\u001a\u00020\b2\b\u0010\u0014\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\b\u0015\u0010\u0016\u001a)\u0010\u001a\u001a\u00020\u00002\b\u0010\u0017\u001a\u0004\u0018\u00010\u00062\b\u0010\u0019\u001a\u0004\u0018\u00010\u00182\u0006\u0010\t\u001a\u00020\b¢\u0006\u0004\b\u001a\u0010\u001b\u001a\u0011\u0010\u001c\u001a\u0004\u0018\u00010\u0000H\u0007¢\u0006\u0004\b\u001c\u0010\u001d\u001a7\u0010(\u001a\u00020\u00062\u0006\u0010\u001f\u001a\u00020\u001e2\u0006\u0010!\u001a\u00020 2\b\u0010#\u001a\u0004\u0018\u00010\"2\u0006\u0010%\u001a\u00020$H\u0002ø\u0001\u0000ø\u0001\u0001¢\u0006\u0004\b&\u0010'\u001a\u001b\u0010+\u001a\u0004\u0018\u00010*2\b\u0010)\u001a\u0004\u0018\u00010\u0000H\u0002¢\u0006\u0004\b+\u0010,\"\u0017\u0010-\u001a\u00020\u0003*\u00020\u00008F@\u0006¢\u0006\u0006\u001a\u0004\b-\u0010.\"\u0019\u00102\u001a\u0004\u0018\u00010/*\u00020\u00008F@\u0006¢\u0006\u0006\u001a\u0004\b0\u00101\"\u0016\u00103\u001a\u00020\b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b3\u00104\"\u0017\u00105\u001a\u00020\u0003*\u00020\u00008F@\u0006¢\u0006\u0006\u001a\u0004\b5\u0010.\"\u0017\u00106\u001a\u00020\u0003*\u00020\u00008F@\u0006¢\u0006\u0006\u001a\u0004\b6\u0010.\"\u0019\u00108\u001a\u0004\u0018\u00010**\u00020\u00008F@\u0006¢\u0006\u0006\u001a\u0004\b7\u0010,\"\u0016\u00109\u001a\u00020\b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b9\u00104\"\u0016\u0010:\u001a\u00020\b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b:\u00104\"\u0017\u0010;\u001a\u00020\u0003*\u00020\u00008F@\u0006¢\u0006\u0006\u001a\u0004\b;\u0010.\"\u0017\u0010<\u001a\u00020\u0003*\u00020\u00008F@\u0006¢\u0006\u0006\u001a\u0004\b<\u0010.\"\u0016\u0010=\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b=\u0010>\"\u0017\u0010?\u001a\u00020\u0003*\u00020\u00008F@\u0006¢\u0006\u0006\u001a\u0004\b?\u0010.\"\u0016\u0010@\u001a\u00020\b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b@\u00104\"\u0017\u0010A\u001a\u00020\u0003*\u00020\u00008F@\u0006¢\u0006\u0006\u001a\u0004\bA\u0010.\"\u0017\u0010B\u001a\u00020\u0003*\u00020\u00008F@\u0006¢\u0006\u0006\u001a\u0004\bB\u0010.\"\u0017\u0010C\u001a\u00020\u0003*\u00020\u00008F@\u0006¢\u0006\u0006\u001a\u0004\bC\u0010.\"\u001c\u0010D\u001a\u00020\b8\u0002@\u0003X\u0083T¢\u0006\f\n\u0004\bD\u00104\u0012\u0004\bE\u0010F\u0082\u0002\u000b\n\u0002\b\u0019\n\u0005\b¡\u001e0\u0001¨\u0006G"}, d2 = {"Lcom/discord/api/activity/Activity;", "", "expected", "", "hasFlag", "(Lcom/discord/api/activity/Activity;I)Z", "", ModelAuditLogEntry.CHANGE_KEY_NAME, "", "createdAt", "createPlayingActivity", "(Ljava/lang/String;J)Lcom/discord/api/activity/Activity;", "musicSource", "trackName", "trackId", "album", "albumArtUrl", "artists", "start", "end", "partyId", "createSpotifyListeningActivity", "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;)Lcom/discord/api/activity/Activity;", "state", "Lcom/discord/api/activity/ActivityEmoji;", "emoji", "createCustomStatusActivity", "(Ljava/lang/String;Lcom/discord/api/activity/ActivityEmoji;J)Lcom/discord/api/activity/Activity;", "createStageChannelListeningActivity", "()Lcom/discord/api/activity/Activity;", "Lcom/discord/api/channel/Channel;", "channel", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/widgets/stage/StageRoles;", "myStageRoles", "Lcom/discord/api/stageinstance/StageInstance;", "stageInstance", "packStageChannelPartyId-hLOU_GE", "(Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;Lcom/discord/widgets/stage/StageRoles;Lcom/discord/api/stageinstance/StageInstance;)Ljava/lang/String;", "packStageChannelPartyId", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "Lcom/discord/utilities/presence/StageCallRichPresencePartyData;", "unpackStageChannelParty", "(Lcom/discord/api/activity/Activity;)Lcom/discord/utilities/presence/StageCallRichPresencePartyData;", "isStageChannelActivity", "(Lcom/discord/api/activity/Activity;)Z", "Lcom/discord/api/activity/ActivityPlatform;", "getGamePlatform", "(Lcom/discord/api/activity/Activity;)Lcom/discord/api/activity/ActivityPlatform;", "gamePlatform", "STAGE_PRESENCE_FLAG_GUILD_IS_VERIFIED", "J", "isStreaming", "isGamePlatform", "getStageChannelRichPresencePartyData", "stageChannelRichPresencePartyData", "STAGE_PRESENCE_FLAG_GUILD_IS_PARTNERED", "STAGE_PRESENCE_FLAG_USER_IS_SPEAKER", "isCustomStatus", "isGameActivity", "STAGE_PRESENCE_PARTY_PREFIX", "Ljava/lang/String;", "isSpotifyActivity", "STAGE_PRESENCE_APPLICATION_ID", "isXboxActivity", "isCurrentPlatform", "isRichPresence", "XBOX_APPLICATION_ID", "getXBOX_APPLICATION_ID$annotations", "()V", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ActivityUtilsKt {
    private static final long STAGE_PRESENCE_APPLICATION_ID = 834488117758001152L;
    private static final long STAGE_PRESENCE_FLAG_GUILD_IS_PARTNERED = 2;
    private static final long STAGE_PRESENCE_FLAG_GUILD_IS_VERIFIED = 4;
    private static final long STAGE_PRESENCE_FLAG_USER_IS_SPEAKER = 1;
    private static final String STAGE_PRESENCE_PARTY_PREFIX = "stage:";
    private static final long XBOX_APPLICATION_ID = 438122941302046720L;

    public static final Activity createCustomStatusActivity(String str, ActivityEmoji activityEmoji, long j) {
        return new Activity("Custom Status", ActivityType.CUSTOM_STATUS, null, j, null, null, null, str, activityEmoji, null, null, null, null, null, null, null, null, null);
    }

    public static final Activity createPlayingActivity(String str, long j) {
        if (str == null) {
            return null;
        }
        return new Activity(str, ActivityType.PLAYING, null, j, null, null, null, null, null, null, null, null, null, null, null, ActivityPlatform.ANDROID, null, null);
    }

    public static final Activity createSpotifyListeningActivity(long j, String str, String str2, String str3, String str4, String str5, String str6, long j2, long j3, String str7) {
        if (str == null) {
            return null;
        }
        ActivityType activityType = ActivityType.LISTENING;
        ActivityAssets activityAssets = new ActivityAssets(str5, str4, null, null);
        return new Activity(str, activityType, null, j, new ActivityTimestamps(String.valueOf(j2), String.valueOf(j3)), null, str2, str6, null, new ActivityParty(str7, null), activityAssets, 48, str3, null, null, null, null, null);
    }

    @StoreThread
    public static final Activity createStageChannelListeningActivity() {
        Guild guild;
        int i;
        String str;
        ActivityTimestamps o;
        Collection<StageRoles> values;
        StoreStream.Companion companion = StoreStream.Companion;
        long selectedVoiceChannelId = companion.getVoiceChannelSelected().getSelectedVoiceChannelId();
        Channel channel = companion.getChannels().getChannel(selectedVoiceChannelId);
        if (channel == null || !ChannelUtils.z(channel)) {
            return null;
        }
        Map<Long, StageRoles> channelRolesInternal = companion.getStageChannels().getChannelRolesInternal(selectedVoiceChannelId);
        StageRoles stageRoles = companion.getStageChannels().m11getMyRolesInternalvisDeB4(selectedVoiceChannelId);
        StageInstance stageInstanceForChannelInternal = companion.getStageInstances().getStageInstanceForChannelInternal(channel.h());
        if (!(stageInstanceForChannelInternal == null || (guild = companion.getGuilds().getGuild(channel.f())) == null)) {
            String str2 = m15packStageChannelPartyIdhLOU_GE(channel, guild, stageRoles, stageInstanceForChannelInternal);
            if (channelRolesInternal == null || (values = channelRolesInternal.values()) == null) {
                i = 0;
            } else {
                ArrayList arrayList = new ArrayList();
                for (Object obj : values) {
                    if (StageRoles.m27isSpeakerimpl(((StageRoles) obj).m29unboximpl())) {
                        arrayList.add(obj);
                    }
                }
                i = arrayList.size();
            }
            int size = channelRolesInternal != null ? channelRolesInternal.size() : 0;
            StoreStream.Companion companion2 = StoreStream.Companion;
            Activity applicationActivity = companion2.getPresences().getApplicationActivity(companion2.getUsers().getMe().getId(), STAGE_PRESENCE_APPLICATION_ID);
            StageCallRichPresencePartyData unpackStageChannelParty = unpackStageChannelParty(applicationActivity);
            if (!(unpackStageChannelParty != null && unpackStageChannelParty.getGuildId() == guild.getId() && unpackStageChannelParty.getChannelId() == channel.h())) {
                applicationActivity = null;
            }
            if (applicationActivity == null || (o = applicationActivity.o()) == null || (str = String.valueOf(o.c())) == null) {
                str = String.valueOf(ClockFactory.get().currentTimeMillis());
            }
            if (stageInstanceForChannelInternal.e() != StageInstancePrivacyLevel.PUBLIC) {
                return null;
            }
            return new Activity(stageInstanceForChannelInternal.f(), ActivityType.LISTENING, null, ClockFactory.get().currentTimeMillis(), new ActivityTimestamps(str, null), Long.valueOf((long) STAGE_PRESENCE_APPLICATION_ID), null, null, null, new ActivityParty(str2, n.listOf((Object[]) new Long[]{Long.valueOf(i), Long.valueOf(size)})), new ActivityAssets(null, null, guild.getIcon(), guild.getName()), null, null, null, null, null, null, null);
        }
        return null;
    }

    public static final ActivityPlatform getGamePlatform(Activity activity) {
        m.checkNotNullParameter(activity, "$this$gamePlatform");
        if (!isGameActivity(activity)) {
            return null;
        }
        if (isXboxActivity(activity)) {
            return ActivityPlatform.XBOX;
        }
        ActivityPlatform j = activity.j();
        return j != null ? j : ActivityPlatform.DESKTOP;
    }

    public static final StageCallRichPresencePartyData getStageChannelRichPresencePartyData(Activity activity) {
        m.checkNotNullParameter(activity, "$this$stageChannelRichPresencePartyData");
        return unpackStageChannelParty(activity);
    }

    private static /* synthetic */ void getXBOX_APPLICATION_ID$annotations() {
    }

    public static final boolean hasFlag(Activity activity, int i) {
        m.checkNotNullParameter(activity, "$this$hasFlag");
        Integer g = activity.g();
        return ((g != null ? g.intValue() : 0) & i) == i;
    }

    public static final boolean isCurrentPlatform(Activity activity) {
        m.checkNotNullParameter(activity, "$this$isCurrentPlatform");
        ActivityPlatform j = activity.j();
        ActivityPlatform activityPlatform = ActivityPlatform.ANDROID;
        if (j == activityPlatform) {
            return true;
        }
        List<ActivityPlatform> m = activity.m();
        return m != null && m.contains(activityPlatform);
    }

    public static final boolean isCustomStatus(Activity activity) {
        m.checkNotNullParameter(activity, "$this$isCustomStatus");
        return activity.p() == ActivityType.CUSTOM_STATUS;
    }

    public static final boolean isGameActivity(Activity activity) {
        m.checkNotNullParameter(activity, "$this$isGameActivity");
        return activity.p() == ActivityType.PLAYING;
    }

    public static final boolean isGamePlatform(Activity activity) {
        m.checkNotNullParameter(activity, "$this$isGamePlatform");
        return isXboxActivity(activity) || activity.j() == ActivityPlatform.SAMSUNG || activity.j() == ActivityPlatform.PS4 || activity.j() == ActivityPlatform.PS5;
    }

    public static final boolean isRichPresence(Activity activity) {
        m.checkNotNullParameter(activity, "$this$isRichPresence");
        if (!isCustomStatus(activity)) {
            if (activity.b() != null || activity.i() != null) {
                return true;
            }
            String l = activity.l();
            if (!(l == null || l.length() == 0)) {
                return true;
            }
            String e = activity.e();
            if (!(e == null || e.length() == 0)) {
                return true;
            }
        }
        return false;
    }

    public static final boolean isSpotifyActivity(Activity activity) {
        m.checkNotNullParameter(activity, "$this$isSpotifyActivity");
        return t.equals(activity.h(), Platform.SPOTIFY.getProperName(), true);
    }

    public static final boolean isStageChannelActivity(Activity activity) {
        Long a;
        m.checkNotNullParameter(activity, "$this$isStageChannelActivity");
        return activity.p() == ActivityType.LISTENING && (a = activity.a()) != null && a.longValue() == STAGE_PRESENCE_APPLICATION_ID;
    }

    public static final boolean isStreaming(Activity activity) {
        m.checkNotNullParameter(activity, "$this$isStreaming");
        return activity.p() == ActivityType.STREAMING;
    }

    public static final boolean isXboxActivity(Activity activity) {
        Long a;
        m.checkNotNullParameter(activity, "$this$isXboxActivity");
        return activity.j() == ActivityPlatform.XBOX || ((a = activity.a()) != null && a.longValue() == XBOX_APPLICATION_ID);
    }

    /* renamed from: packStageChannelPartyId-hLOU_GE  reason: not valid java name */
    private static final String m15packStageChannelPartyIdhLOU_GE(Channel channel, Guild guild, StageRoles stageRoles, StageInstance stageInstance) {
        long j;
        if (stageRoles != null && StageRoles.m27isSpeakerimpl(stageRoles.m29unboximpl())) {
            j = 1;
        } else if (guild.hasFeature(GuildFeature.PARTNERED)) {
            j = 2;
        } else {
            j = guild.hasFeature(GuildFeature.VERIFIED) ? 4L : 0L;
        }
        String l = Long.toString(j, a.checkRadix(16));
        m.checkNotNullExpressionValue(l, "java.lang.Long.toString(this, checkRadix(radix))");
        StringBuilder R = b.d.b.a.a.R(STAGE_PRESENCE_PARTY_PREFIX);
        R.append(channel.f());
        R.append(MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR);
        R.append(channel.h());
        R.append(MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR);
        R.append(l);
        R.append(MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR);
        R.append(stageInstance.c());
        return R.toString();
    }

    /* JADX WARN: Removed duplicated region for block: B:24:0x0037 A[Catch: Exception -> 0x00e6, TryCatch #0 {Exception -> 0x00e6, blocks: (B:11:0x001a, B:13:0x0020, B:17:0x0029, B:24:0x0037, B:25:0x0044, B:27:0x004a, B:28:0x0057, B:29:0x0063, B:33:0x00c9, B:37:0x00d5, B:41:0x00e1), top: B:45:0x001a }] */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0044 A[Catch: Exception -> 0x00e6, TryCatch #0 {Exception -> 0x00e6, blocks: (B:11:0x001a, B:13:0x0020, B:17:0x0029, B:24:0x0037, B:25:0x0044, B:27:0x004a, B:28:0x0057, B:29:0x0063, B:33:0x00c9, B:37:0x00d5, B:41:0x00e1), top: B:45:0x001a }] */
    /* JADX WARN: Removed duplicated region for block: B:31:0x00c4  */
    /* JADX WARN: Removed duplicated region for block: B:32:0x00c7  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x00d0  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x00d3  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x00dc  */
    /* JADX WARN: Removed duplicated region for block: B:40:0x00df  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private static final com.discord.utilities.presence.StageCallRichPresencePartyData unpackStageChannelParty(com.discord.api.activity.Activity r27) {
        /*
            r0 = 0
            if (r27 == 0) goto Le6
            com.discord.api.activity.ActivityParty r1 = r27.i()
            if (r1 == 0) goto Le6
            java.lang.String r2 = r1.a()
            if (r2 == 0) goto Le6
            java.lang.String r1 = "stage:"
            r8 = 0
            r9 = 2
            boolean r1 = d0.g0.t.startsWith$default(r2, r1, r8, r9, r0)
            if (r1 != 0) goto L1a
            return r0
        L1a:
            com.discord.api.activity.ActivityParty r1 = r27.i()     // Catch: java.lang.Exception -> Le6
            if (r1 == 0) goto L25
            java.util.List r1 = r1.b()     // Catch: java.lang.Exception -> Le6
            goto L26
        L25:
            r1 = r0
        L26:
            r10 = 1
            if (r1 == 0) goto L32
            boolean r3 = r1.isEmpty()     // Catch: java.lang.Exception -> Le6
            if (r3 == 0) goto L30
            goto L32
        L30:
            r3 = 0
            goto L33
        L32:
            r3 = 1
        L33:
            r11 = 0
            if (r3 == 0) goto L44
            java.lang.Long r1 = java.lang.Long.valueOf(r11)     // Catch: java.lang.Exception -> Le6
            java.lang.Long r3 = java.lang.Long.valueOf(r11)     // Catch: java.lang.Exception -> Le6
            kotlin.Pair r1 = d0.o.to(r1, r3)     // Catch: java.lang.Exception -> Le6
            goto L63
        L44:
            int r3 = r1.size()     // Catch: java.lang.Exception -> Le6
            if (r3 == r9) goto L57
            java.lang.Long r1 = java.lang.Long.valueOf(r11)     // Catch: java.lang.Exception -> Le6
            java.lang.Long r3 = java.lang.Long.valueOf(r11)     // Catch: java.lang.Exception -> Le6
            kotlin.Pair r1 = d0.o.to(r1, r3)     // Catch: java.lang.Exception -> Le6
            goto L63
        L57:
            java.lang.Object r3 = r1.get(r8)     // Catch: java.lang.Exception -> Le6
            java.lang.Object r1 = r1.get(r10)     // Catch: java.lang.Exception -> Le6
            kotlin.Pair r1 = d0.o.to(r3, r1)     // Catch: java.lang.Exception -> Le6
        L63:
            java.lang.Object r3 = r1.component1()     // Catch: java.lang.Exception -> Le6
            java.lang.Number r3 = (java.lang.Number) r3     // Catch: java.lang.Exception -> Le6
            long r23 = r3.longValue()     // Catch: java.lang.Exception -> Le6
            java.lang.Object r1 = r1.component2()     // Catch: java.lang.Exception -> Le6
            java.lang.Number r1 = (java.lang.Number) r1     // Catch: java.lang.Exception -> Le6
            long r3 = r1.longValue()     // Catch: java.lang.Exception -> Le6
            long r3 = r3 - r23
            long r25 = d0.d0.f.coerceAtLeast(r11, r3)     // Catch: java.lang.Exception -> Le6
            char[] r3 = new char[r10]     // Catch: java.lang.Exception -> Le6
            r1 = 58
            r3[r8] = r1     // Catch: java.lang.Exception -> Le6
            r4 = 0
            r5 = 0
            r6 = 6
            r7 = 0
            java.util.List r1 = d0.g0.w.split$default(r2, r3, r4, r5, r6, r7)     // Catch: java.lang.Exception -> Le6
            java.lang.Object r2 = r1.get(r10)     // Catch: java.lang.Exception -> Le6
            java.lang.String r2 = (java.lang.String) r2     // Catch: java.lang.Exception -> Le6
            java.lang.Object r3 = r1.get(r9)     // Catch: java.lang.Exception -> Le6
            java.lang.String r3 = (java.lang.String) r3     // Catch: java.lang.Exception -> Le6
            r4 = 3
            java.lang.Object r4 = r1.get(r4)     // Catch: java.lang.Exception -> Le6
            java.lang.String r4 = (java.lang.String) r4     // Catch: java.lang.Exception -> Le6
            r5 = 4
            java.lang.Object r1 = r1.get(r5)     // Catch: java.lang.Exception -> Le6
            java.lang.String r1 = (java.lang.String) r1     // Catch: java.lang.Exception -> Le6
            r5 = 16
            int r5 = d0.g0.a.checkRadix(r5)     // Catch: java.lang.Exception -> Le6
            long r4 = java.lang.Long.parseLong(r4, r5)     // Catch: java.lang.Exception -> Le6
            com.discord.utilities.presence.StageCallRichPresencePartyData r6 = new com.discord.utilities.presence.StageCallRichPresencePartyData     // Catch: java.lang.Exception -> Le6
            long r14 = java.lang.Long.parseLong(r1)     // Catch: java.lang.Exception -> Le6
            long r19 = java.lang.Long.parseLong(r2)     // Catch: java.lang.Exception -> Le6
            long r16 = java.lang.Long.parseLong(r3)     // Catch: java.lang.Exception -> Le6
            r1 = 1
            long r1 = r1 & r4
            int r3 = (r1 > r11 ? 1 : (r1 == r11 ? 0 : -1))
            if (r3 == 0) goto Lc7
            r18 = 1
            goto Lc9
        Lc7:
            r18 = 0
        Lc9:
            r1 = 2
            long r1 = r1 & r4
            int r3 = (r1 > r11 ? 1 : (r1 == r11 ? 0 : -1))
            if (r3 == 0) goto Ld3
            r21 = 1
            goto Ld5
        Ld3:
            r21 = 0
        Ld5:
            r1 = 4
            long r1 = r1 & r4
            int r3 = (r1 > r11 ? 1 : (r1 == r11 ? 0 : -1))
            if (r3 == 0) goto Ldf
            r22 = 1
            goto Le1
        Ldf:
            r22 = 0
        Le1:
            r13 = r6
            r13.<init>(r14, r16, r18, r19, r21, r22, r23, r25)     // Catch: java.lang.Exception -> Le6
            return r6
        Le6:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.presence.ActivityUtilsKt.unpackStageChannelParty(com.discord.api.activity.Activity):com.discord.utilities.presence.StageCallRichPresencePartyData");
    }
}
