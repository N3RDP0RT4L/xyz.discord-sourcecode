package com.discord.utilities.surveys;

import androidx.core.app.NotificationCompat;
import com.discord.api.user.UserSurvey;
import com.discord.utilities.surveys.SurveyUtils;
import j0.k.b;
import kotlin.Metadata;
/* compiled from: SurveyUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0006\u001a\n \u0003*\u0004\u0018\u00010\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/api/user/UserSurvey;", "it", "Lcom/discord/utilities/surveys/SurveyUtils$Survey;", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/api/user/UserSurvey;)Lcom/discord/utilities/surveys/SurveyUtils$Survey;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SurveyUtils$getSurveyToShow$1<T, R> implements b<UserSurvey, SurveyUtils.Survey> {
    public static final SurveyUtils$getSurveyToShow$1 INSTANCE = new SurveyUtils$getSurveyToShow$1();

    public final SurveyUtils.Survey call(UserSurvey userSurvey) {
        if (userSurvey != null) {
            return new SurveyUtils.Survey(userSurvey.e(), userSurvey.g(), userSurvey.e(), userSurvey.f(), userSurvey.a());
        }
        return null;
    }
}
