package com.discord.utilities.images;

import android.graphics.Bitmap;
import androidx.core.app.NotificationCompat;
import com.discord.utilities.images.MGImagesBitmap;
import d0.o;
import j0.k.b;
import kotlin.Metadata;
import kotlin.Pair;
import rx.Observable;
/* compiled from: MGImagesBitmap.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\t\u001az\u00126\b\u0001\u00122\u0012\u0004\u0012\u00020\u0005\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00060\u0006 \u0001*\u0018\u0012\u0004\u0012\u00020\u0005\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00060\u0006\u0018\u00010\u00040\u0004 \u0001*<\u00126\b\u0001\u00122\u0012\u0004\u0012\u00020\u0005\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00060\u0006 \u0001*\u0018\u0012\u0004\u0012\u00020\u0005\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00060\u0006\u0018\u00010\u00040\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"Lcom/discord/utilities/images/MGImagesBitmap$ImageRequest;", "kotlin.jvm.PlatformType", "imageRequest", "Lrx/Observable;", "Lkotlin/Pair;", "", "Landroid/graphics/Bitmap;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/utilities/images/MGImagesBitmap$ImageRequest;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MGImagesBitmap$getBitmaps$1<T, R> implements b<MGImagesBitmap.ImageRequest, Observable<? extends Pair<? extends String, ? extends Bitmap>>> {
    public static final MGImagesBitmap$getBitmaps$1 INSTANCE = new MGImagesBitmap$getBitmaps$1();

    public final Observable<? extends Pair<String, Bitmap>> call(final MGImagesBitmap.ImageRequest imageRequest) {
        return (Observable<R>) MGImagesBitmap.INSTANCE.getBitmap(imageRequest.getImageUri(), imageRequest.getRoundAsCircle()).F(new b<Bitmap, Pair<? extends String, ? extends Bitmap>>() { // from class: com.discord.utilities.images.MGImagesBitmap$getBitmaps$1.1
            public final Pair<String, Bitmap> call(Bitmap bitmap) {
                return o.to(MGImagesBitmap.ImageRequest.this.getImageUri(), bitmap);
            }
        });
    }
}
