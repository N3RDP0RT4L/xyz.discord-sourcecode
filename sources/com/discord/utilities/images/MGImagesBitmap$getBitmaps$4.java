package com.discord.utilities.images;

import android.graphics.Bitmap;
import androidx.core.app.NotificationCompat;
import com.discord.utilities.images.MGImagesBitmap;
import d0.z.d.m;
import j0.k.b;
import java.util.Map;
import kotlin.Metadata;
/* compiled from: MGImagesBitmap.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\t\u001a\n \u0002*\u0004\u0018\u00010\u00060\u00062F\u0010\u0005\u001aB\u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00010\u0001\u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00030\u0003 \u0002* \u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00010\u0001\u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00030\u0003\u0018\u00010\u00040\u0000H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"", "", "kotlin.jvm.PlatformType", "Landroid/graphics/Bitmap;", "", "it", "Lcom/discord/utilities/images/MGImagesBitmap$CloseableBitmaps;", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Map;)Lcom/discord/utilities/images/MGImagesBitmap$CloseableBitmaps;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MGImagesBitmap$getBitmaps$4<T, R> implements b<Map<String, Bitmap>, MGImagesBitmap.CloseableBitmaps> {
    public static final MGImagesBitmap$getBitmaps$4 INSTANCE = new MGImagesBitmap$getBitmaps$4();

    public final MGImagesBitmap.CloseableBitmaps call(Map<String, Bitmap> map) {
        m.checkNotNullExpressionValue(map, "it");
        return new MGImagesBitmap.CloseableBitmaps(map, false, 2, null);
    }
}
