package com.discord.utilities.images;

import android.graphics.Bitmap;
import androidx.core.app.NotificationCompat;
import j0.k.b;
import kotlin.Metadata;
import kotlin.Pair;
/* compiled from: MGImagesBitmap.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0005\u0010\u0007\u001a\n \u0003*\u0004\u0018\u00010\u00020\u000226\u0010\u0004\u001a2\u0012\u0004\u0012\u00020\u0001\u0012\f\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0018\u0012\u0004\u0012\u00020\u0001\u0012\f\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lkotlin/Pair;", "", "Landroid/graphics/Bitmap;", "kotlin.jvm.PlatformType", "it", NotificationCompat.CATEGORY_CALL, "(Lkotlin/Pair;)Landroid/graphics/Bitmap;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MGImagesBitmap$getBitmaps$3<T, R> implements b<Pair<? extends String, ? extends Bitmap>, Bitmap> {
    public static final MGImagesBitmap$getBitmaps$3 INSTANCE = new MGImagesBitmap$getBitmaps$3();

    @Override // j0.k.b
    public /* bridge */ /* synthetic */ Bitmap call(Pair<? extends String, ? extends Bitmap> pair) {
        return call2((Pair<String, Bitmap>) pair);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final Bitmap call2(Pair<String, Bitmap> pair) {
        return pair.getSecond();
    }
}
