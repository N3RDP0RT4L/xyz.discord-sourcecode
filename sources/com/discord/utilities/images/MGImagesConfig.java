package com.discord.utilities.images;

import andhook.lib.HookHelper;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import androidx.appcompat.widget.ActivityChooserModel;
import b.c.a.a0.d;
import b.f.d.d.k;
import b.f.d.e.a;
import b.f.g.a.a.e;
import b.f.j.e.g;
import b.f.j.e.h;
import b.f.j.e.j;
import b.f.j.e.l;
import b.f.j.e.n;
import b.f.j.r.b;
import b.f.m.n.c;
import com.facebook.cache.disk.DiskCacheConfig;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.cache.DefaultBitmapMemoryCacheParamsSupplier;
import com.facebook.imagepipeline.cache.MemoryCacheParams;
import d0.z.d.m;
import java.lang.reflect.InvocationTargetException;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
/* compiled from: MGImagesConfig.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\t\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001c\u0010\u001dJ\u001b\u0010\u0006\u001a\u00020\u0005*\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0013\u0010\t\u001a\u00020\b*\u00020\u0002H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0015\u0010\u000e\u001a\u00020\r2\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\u000e\u0010\u000fJ\u0015\u0010\u0012\u001a\u00020\r2\u0006\u0010\u0011\u001a\u00020\u0010¢\u0006\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0014\u001a\u00020\u00038\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u0016\u0010\u0016\u001a\u00020\u00038\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0016\u0010\u0015R\u0016\u0010\u0017\u001a\u00020\u00108\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u0016\u0010\u001a\u001a\u00020\u00198\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001a\u0010\u001b¨\u0006\u001e"}, d2 = {"Lcom/discord/utilities/images/MGImagesConfig;", "", "Landroid/content/Context;", "", "directoryName", "Lcom/facebook/cache/disk/DiskCacheConfig;", "newDiskCacheConfig", "(Landroid/content/Context;Ljava/lang/String;)Lcom/facebook/cache/disk/DiskCacheConfig;", "Lcom/facebook/imagepipeline/cache/DefaultBitmapMemoryCacheParamsSupplier;", "getAppBitmapMemoryCacheParamsSupplier", "(Landroid/content/Context;)Lcom/facebook/imagepipeline/cache/DefaultBitmapMemoryCacheParamsSupplier;", "Landroid/app/Application;", "context", "", "init", "(Landroid/app/Application;)V", "", "level", "onTrimMemory", "(I)V", "CACHE_DIR", "Ljava/lang/String;", "CACHE_DIR_SMALL", "MAX_BITMAP_MEM_CACHE_SIZE_RATIO", "I", "", "MAX_DISK_CACHE_SIZE", "J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MGImagesConfig {
    private static final String CACHE_DIR = "app_images_cache";
    private static final String CACHE_DIR_SMALL = "app_images_cache_small";
    public static final MGImagesConfig INSTANCE = new MGImagesConfig();
    private static final int MAX_BITMAP_MEM_CACHE_SIZE_RATIO = 3;
    private static final long MAX_DISK_CACHE_SIZE = 41943040;

    private MGImagesConfig() {
    }

    private final DefaultBitmapMemoryCacheParamsSupplier getAppBitmapMemoryCacheParamsSupplier(Context context) {
        Object systemService = context.getSystemService(ActivityChooserModel.ATTRIBUTE_ACTIVITY);
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.app.ActivityManager");
        final ActivityManager activityManager = (ActivityManager) systemService;
        return new DefaultBitmapMemoryCacheParamsSupplier(activityManager) { // from class: com.discord.utilities.images.MGImagesConfig$getAppBitmapMemoryCacheParamsSupplier$1
            /* JADX WARN: Can't rename method to resolve collision */
            @Override // com.facebook.imagepipeline.cache.DefaultBitmapMemoryCacheParamsSupplier, com.facebook.common.internal.Supplier
            public MemoryCacheParams get() {
                MemoryCacheParams memoryCacheParams = super.get();
                int i = memoryCacheParams.a;
                return new MemoryCacheParams(i, memoryCacheParams.f2867b, memoryCacheParams.c, memoryCacheParams.d, i / 3, TimeUnit.MINUTES.toMillis(5L));
            }
        };
    }

    private final DiskCacheConfig newDiskCacheConfig(Context context, String str) {
        DiskCacheConfig.b bVar = new DiskCacheConfig.b(context, null);
        bVar.f2855b = new k(context.getCacheDir());
        bVar.a = str;
        bVar.c = MAX_DISK_CACHE_SIZE;
        DiskCacheConfig diskCacheConfig = new DiskCacheConfig(bVar);
        m.checkNotNullExpressionValue(diskCacheConfig, "DiskCacheConfig\n        …HE_SIZE)\n        .build()");
        return diskCacheConfig;
    }

    public final void init(Application application) {
        m.checkNotNullParameter(application, "context");
        j.a aVar = new j.a(application, null);
        aVar.c = true;
        aVar.d = newDiskCacheConfig(application, CACHE_DIR);
        aVar.e = newDiskCacheConfig(application, CACHE_DIR_SMALL);
        DefaultBitmapMemoryCacheParamsSupplier appBitmapMemoryCacheParamsSupplier = getAppBitmapMemoryCacheParamsSupplier(application);
        Objects.requireNonNull(appBitmapMemoryCacheParamsSupplier);
        aVar.a = appBitmapMemoryCacheParamsSupplier;
        l.b bVar = aVar.f;
        bVar.f572b = true;
        j.a aVar2 = bVar.a;
        Objects.requireNonNull(aVar2);
        j jVar = new j(aVar2, null);
        b.b();
        if (b.f.g.a.a.b.f473b) {
            a.k(b.f.g.a.a.b.class, "Fresco has already been initialized! `Fresco.initialize(...)` should only be called 1 single time to avoid memory leaks!");
        } else {
            b.f.g.a.a.b.f473b = true;
        }
        n.a = true;
        if (!b.f.m.n.a.b()) {
            b.b();
            try {
                try {
                    try {
                        Class.forName("com.facebook.imagepipeline.nativecode.NativeCodeInitializer").getMethod("init", Context.class).invoke(null, application);
                    } catch (ClassNotFoundException unused) {
                        b.f.m.n.a.a(new c());
                    } catch (IllegalAccessException unused2) {
                        b.f.m.n.a.a(new c());
                    }
                } catch (NoSuchMethodException unused3) {
                    b.f.m.n.a.a(new c());
                } catch (InvocationTargetException unused4) {
                    b.f.m.n.a.a(new c());
                }
                b.b();
            } finally {
                b.b();
            }
        }
        Context applicationContext = application.getApplicationContext();
        b.f.j.e.m.j(jVar);
        b.b();
        e eVar = new e(applicationContext);
        b.f.g.a.a.b.a = eVar;
        SimpleDraweeView.initialize(eVar);
        b.b();
    }

    public final void onTrimMemory(int i) {
        if (i == 5 || i == 10 || i == 15 || i == 40 || i == 60 || i == 80) {
            b.f.j.e.m mVar = b.f.j.e.m.a;
            d.y(mVar, "ImagePipelineFactory was not initialized!");
            if (mVar.l == null) {
                mVar.l = mVar.a();
            }
            h hVar = mVar.l;
            g gVar = new g(hVar);
            hVar.e.d(gVar);
            hVar.f.d(gVar);
        }
    }
}
