package com.discord.utilities.images;

import andhook.lib.HookHelper;
import andhook.lib.xposed.ClassUtils;
import android.graphics.Bitmap;
import android.net.Uri;
import androidx.core.app.NotificationCompat;
import b.c.a.a0.d;
import b.f.j.e.h;
import b.f.j.j.c;
import com.discord.utilities.images.MGImagesBitmap;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import d0.g0.t;
import d0.g0.w;
import d0.z.d.g0.a;
import d0.z.d.m;
import j0.l.a.q;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.function.BiFunction;
import java.util.function.Function;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Subscriber;
/* compiled from: MGImagesBitmap.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\bÆ\u0002\u0018\u00002\u00020\u0001:\u0005\u0014\u0015\u0016\u0017\u0018B\t\b\u0002¢\u0006\u0004\b\u0012\u0010\u0013J#\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0007¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\f\u001a\u00020\u000b2\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\f\u0010\rJ#\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00052\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u000e\u001a\u00020\u000b¢\u0006\u0004\b\u0010\u0010\u0011¨\u0006\u0019"}, d2 = {"Lcom/discord/utilities/images/MGImagesBitmap;", "", "", "Lcom/discord/utilities/images/MGImagesBitmap$ImageRequest;", "imageRequests", "Lrx/Observable;", "Lcom/discord/utilities/images/MGImagesBitmap$CloseableBitmaps;", "getBitmaps", "(Ljava/util/Set;)Lrx/Observable;", "", "imageUri", "", "isValidUri", "(Ljava/lang/String;)Z", "imageIsCircle", "Landroid/graphics/Bitmap;", "getBitmap", "(Ljava/lang/String;Z)Lrx/Observable;", HookHelper.constructorName, "()V", "CloseableBitmaps", "DecodeException", "ImageNotFoundException", "ImageRequest", "MissingBitmapException", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MGImagesBitmap {
    public static final MGImagesBitmap INSTANCE = new MGImagesBitmap();

    /* compiled from: MGImagesBitmap.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\r\n\u0002\u0010\"\n\u0002\b\u0003\n\u0002\u0010&\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u00020\u0004B%\u0012\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001\u0012\b\b\u0002\u0010\u0015\u001a\u00020\t¢\u0006\u0004\b&\u0010'J\u000f\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0006\u0010\u0007J\u0018\u0010\n\u001a\u00020\t2\u0006\u0010\b\u001a\u00020\u0002H\u0096\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0018\u0010\r\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u0003H\u0096\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u000f\u001a\u0004\u0018\u00010\u00032\u0006\u0010\b\u001a\u00020\u0002H\u0096\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\tH\u0096\u0001¢\u0006\u0004\b\u0011\u0010\u0012R\"\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00018\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0015\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016R\u001c\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00020\u00178\u0016@\u0016X\u0096\u0005¢\u0006\u0006\u001a\u0004\b\u0018\u0010\u0019R(\u0010\u001d\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u001b0\u00178\u0016@\u0016X\u0096\u0005¢\u0006\u0006\u001a\u0004\b\u001c\u0010\u0019R\u001c\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00030\u001e8\u0016@\u0016X\u0096\u0005¢\u0006\u0006\u001a\u0004\b\u001f\u0010 R\u0016\u0010%\u001a\u00020\"8\u0016@\u0016X\u0096\u0005¢\u0006\u0006\u001a\u0004\b#\u0010$¨\u0006("}, d2 = {"Lcom/discord/utilities/images/MGImagesBitmap$CloseableBitmaps;", "", "", "Landroid/graphics/Bitmap;", "Ljava/io/Closeable;", "", "close", "()V", "key", "", "containsKey", "(Ljava/lang/String;)Z", "value", "containsValue", "(Landroid/graphics/Bitmap;)Z", "get", "(Ljava/lang/String;)Landroid/graphics/Bitmap;", "isEmpty", "()Z", "underlyingMap", "Ljava/util/Map;", "recycleBitmaps", "Z", "", "getKeys", "()Ljava/util/Set;", "keys", "", "getEntries", "entries", "", "getValues", "()Ljava/util/Collection;", "values", "", "getSize", "()I", "size", HookHelper.constructorName, "(Ljava/util/Map;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class CloseableBitmaps implements Map<String, Bitmap>, Closeable, a {
        private final boolean recycleBitmaps;
        private final Map<String, Bitmap> underlyingMap;

        public CloseableBitmaps(Map<String, Bitmap> map, boolean z2) {
            m.checkNotNullParameter(map, "underlyingMap");
            this.underlyingMap = map;
            this.recycleBitmaps = z2;
        }

        @Override // java.util.Map
        public void clear() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            if (this.recycleBitmaps) {
                for (Map.Entry<String, Bitmap> entry : this.underlyingMap.entrySet()) {
                    entry.getValue().recycle();
                }
            }
        }

        /* renamed from: compute  reason: avoid collision after fix types in other method */
        public Bitmap compute2(String str, BiFunction<? super String, ? super Bitmap, ? extends Bitmap> biFunction) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.Map
        public /* synthetic */ Bitmap compute(String str, BiFunction<? super String, ? super Bitmap, ? extends Bitmap> biFunction) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        /* renamed from: computeIfAbsent  reason: avoid collision after fix types in other method */
        public Bitmap computeIfAbsent2(String str, Function<? super String, ? extends Bitmap> function) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.Map
        public /* synthetic */ Bitmap computeIfAbsent(String str, Function<? super String, ? extends Bitmap> function) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        /* renamed from: computeIfPresent  reason: avoid collision after fix types in other method */
        public Bitmap computeIfPresent2(String str, BiFunction<? super String, ? super Bitmap, ? extends Bitmap> biFunction) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.Map
        public /* synthetic */ Bitmap computeIfPresent(String str, BiFunction<? super String, ? super Bitmap, ? extends Bitmap> biFunction) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.Map
        public final /* bridge */ boolean containsKey(Object obj) {
            if (obj instanceof String) {
                return containsKey((String) obj);
            }
            return false;
        }

        public boolean containsKey(String str) {
            m.checkNotNullParameter(str, "key");
            return this.underlyingMap.containsKey(str);
        }

        public boolean containsValue(Bitmap bitmap) {
            m.checkNotNullParameter(bitmap, "value");
            return this.underlyingMap.containsValue(bitmap);
        }

        @Override // java.util.Map
        public final /* bridge */ boolean containsValue(Object obj) {
            if (obj instanceof Bitmap) {
                return containsValue((Bitmap) obj);
            }
            return false;
        }

        @Override // java.util.Map
        public final /* bridge */ Set<Map.Entry<String, Bitmap>> entrySet() {
            return getEntries();
        }

        public Bitmap get(String str) {
            m.checkNotNullParameter(str, "key");
            return this.underlyingMap.get(str);
        }

        @Override // java.util.Map
        public final /* bridge */ Bitmap get(Object obj) {
            if (obj instanceof String) {
                return get((String) obj);
            }
            return null;
        }

        public Set<Map.Entry<String, Bitmap>> getEntries() {
            return this.underlyingMap.entrySet();
        }

        public Set<String> getKeys() {
            return this.underlyingMap.keySet();
        }

        public int getSize() {
            return this.underlyingMap.size();
        }

        public Collection<Bitmap> getValues() {
            return this.underlyingMap.values();
        }

        @Override // java.util.Map
        public boolean isEmpty() {
            return this.underlyingMap.isEmpty();
        }

        @Override // java.util.Map
        public final /* bridge */ Set<String> keySet() {
            return getKeys();
        }

        /* renamed from: merge  reason: avoid collision after fix types in other method */
        public Bitmap merge2(String str, Bitmap bitmap, BiFunction<? super Bitmap, ? super Bitmap, ? extends Bitmap> biFunction) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.Map
        public /* synthetic */ Bitmap merge(String str, Bitmap bitmap, BiFunction<? super Bitmap, ? super Bitmap, ? extends Bitmap> biFunction) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        /* renamed from: put  reason: avoid collision after fix types in other method */
        public Bitmap put2(String str, Bitmap bitmap) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.Map
        public /* synthetic */ Bitmap put(String str, Bitmap bitmap) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.Map
        public void putAll(Map<? extends String, ? extends Bitmap> map) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        /* renamed from: putIfAbsent  reason: avoid collision after fix types in other method */
        public Bitmap putIfAbsent2(String str, Bitmap bitmap) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.Map
        public /* synthetic */ Bitmap putIfAbsent(String str, Bitmap bitmap) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.Map
        public Bitmap remove(Object obj) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.Map
        public boolean remove(Object obj, Object obj2) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        /* renamed from: replace  reason: avoid collision after fix types in other method */
        public Bitmap replace2(String str, Bitmap bitmap) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.Map
        public /* synthetic */ Bitmap replace(String str, Bitmap bitmap) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.Map
        public /* synthetic */ boolean replace(String str, Bitmap bitmap, Bitmap bitmap2) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        /* renamed from: replace  reason: avoid collision after fix types in other method */
        public boolean replace2(String str, Bitmap bitmap, Bitmap bitmap2) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.Map
        public void replaceAll(BiFunction<? super String, ? super Bitmap, ? extends Bitmap> biFunction) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.Map
        public final /* bridge */ int size() {
            return getSize();
        }

        @Override // java.util.Map
        public final /* bridge */ Collection<Bitmap> values() {
            return getValues();
        }

        public /* synthetic */ CloseableBitmaps(Map map, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(map, (i & 2) != 0 ? true : z2);
        }
    }

    /* compiled from: MGImagesBitmap.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00060\u0001j\u0002`\u0002B\u000f\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/utilities/images/MGImagesBitmap$DecodeException;", "Ljava/lang/Exception;", "Lkotlin/Exception;", "", "imageUri", HookHelper.constructorName, "(Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class DecodeException extends Exception {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public DecodeException(String str) {
            super("Unable to decode image: " + str + ClassUtils.PACKAGE_SEPARATOR_CHAR);
            m.checkNotNullParameter(str, "imageUri");
        }
    }

    /* compiled from: MGImagesBitmap.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00060\u0001j\u0002`\u0002B\u000f\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/utilities/images/MGImagesBitmap$ImageNotFoundException;", "Ljava/lang/Exception;", "Lkotlin/Exception;", "", "imageUri", HookHelper.constructorName, "(Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ImageNotFoundException extends Exception {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ImageNotFoundException(String str) {
            super("404 image not found: " + str);
            m.checkNotNullParameter(str, "imageUri");
        }
    }

    /* compiled from: MGImagesBitmap.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\f\u0010\u0004J\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0011\u001a\u00020\u00052\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0013\u001a\u0004\b\u0014\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004¨\u0006\u0019"}, d2 = {"Lcom/discord/utilities/images/MGImagesBitmap$ImageRequest;", "", "", "component1", "()Ljava/lang/String;", "", "component2", "()Z", "imageUri", "roundAsCircle", "copy", "(Ljava/lang/String;Z)Lcom/discord/utilities/images/MGImagesBitmap$ImageRequest;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getRoundAsCircle", "Ljava/lang/String;", "getImageUri", HookHelper.constructorName, "(Ljava/lang/String;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ImageRequest {
        private final String imageUri;
        private final boolean roundAsCircle;

        public ImageRequest(String str, boolean z2) {
            m.checkNotNullParameter(str, "imageUri");
            this.imageUri = str;
            this.roundAsCircle = z2;
        }

        public static /* synthetic */ ImageRequest copy$default(ImageRequest imageRequest, String str, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                str = imageRequest.imageUri;
            }
            if ((i & 2) != 0) {
                z2 = imageRequest.roundAsCircle;
            }
            return imageRequest.copy(str, z2);
        }

        public final String component1() {
            return this.imageUri;
        }

        public final boolean component2() {
            return this.roundAsCircle;
        }

        public final ImageRequest copy(String str, boolean z2) {
            m.checkNotNullParameter(str, "imageUri");
            return new ImageRequest(str, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ImageRequest)) {
                return false;
            }
            ImageRequest imageRequest = (ImageRequest) obj;
            return m.areEqual(this.imageUri, imageRequest.imageUri) && this.roundAsCircle == imageRequest.roundAsCircle;
        }

        public final String getImageUri() {
            return this.imageUri;
        }

        public final boolean getRoundAsCircle() {
            return this.roundAsCircle;
        }

        public int hashCode() {
            String str = this.imageUri;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            boolean z2 = this.roundAsCircle;
            if (z2) {
                z2 = true;
            }
            int i = z2 ? 1 : 0;
            int i2 = z2 ? 1 : 0;
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("ImageRequest(imageUri=");
            R.append(this.imageUri);
            R.append(", roundAsCircle=");
            return b.d.b.a.a.M(R, this.roundAsCircle, ")");
        }
    }

    /* compiled from: MGImagesBitmap.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00060\u0001j\u0002`\u0002B\u000f\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/utilities/images/MGImagesBitmap$MissingBitmapException;", "Ljava/lang/Exception;", "Lkotlin/Exception;", "", "imageUri", HookHelper.constructorName, "(Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class MissingBitmapException extends Exception {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public MissingBitmapException(String str) {
            super("Unable to decode image as bitmap: " + str);
            m.checkNotNullParameter(str, "imageUri");
        }
    }

    private MGImagesBitmap() {
    }

    public static final Observable<CloseableBitmaps> getBitmaps(Set<ImageRequest> set) {
        m.checkNotNullParameter(set, "imageRequests");
        ArrayList arrayList = new ArrayList();
        for (Object obj : set) {
            if (!t.isBlank(((ImageRequest) obj).getImageUri())) {
                arrayList.add(obj);
            }
        }
        Observable<CloseableBitmaps> X = Observable.h0(new q(arrayList)).z(MGImagesBitmap$getBitmaps$1.INSTANCE).g0(MGImagesBitmap$getBitmaps$2.INSTANCE, MGImagesBitmap$getBitmaps$3.INSTANCE).F(MGImagesBitmap$getBitmaps$4.INSTANCE).X(j0.p.a.a());
        m.checkNotNullExpressionValue(X, "Observable\n        .from…Schedulers.computation())");
        return X;
    }

    private final boolean isValidUri(String str) {
        Uri parse = Uri.parse(str);
        m.checkNotNullExpressionValue(parse, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        String scheme = parse.getScheme();
        if (scheme == null || t.isBlank(scheme)) {
            return false;
        }
        String host = parse.getHost();
        if (host == null || t.isBlank(host)) {
            return false;
        }
        String path = parse.getPath();
        return !(path == null || t.isBlank(path));
    }

    public final Observable<Bitmap> getBitmap(final String str, boolean z2) {
        m.checkNotNullParameter(str, "imageUri");
        if (!isValidUri(str)) {
            Observable<Bitmap> w = Observable.w(new IllegalArgumentException("invalid uri"));
            m.checkNotNullExpressionValue(w, "Observable.error(Illegal…Exception(\"invalid uri\"))");
            return w;
        }
        b.f.j.e.m mVar = b.f.j.e.m.a;
        d.y(mVar, "ImagePipelineFactory was not initialized!");
        if (mVar.l == null) {
            mVar.l = mVar.a();
        }
        h hVar = mVar.l;
        ImageRequestBuilder imageRequest = MGImages.getImageRequest(str, 0, 0, false);
        if (z2) {
            imageRequest.l = new RoundAsCirclePostprocessor(str);
        }
        final DataSource<CloseableReference<c>> a = hVar.a(imageRequest.a(), null, ImageRequest.c.FULL_FETCH, null, null);
        Observable<Bitmap> h02 = Observable.h0(new Observable.a<Bitmap>() { // from class: com.discord.utilities.images.MGImagesBitmap$getBitmap$1

            /* compiled from: MGImagesBitmap.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Ljava/lang/Runnable;", "kotlin.jvm.PlatformType", "runnable", "", "execute", "(Ljava/lang/Runnable;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.utilities.images.MGImagesBitmap$getBitmap$1$2  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass2 implements Executor {
                public static final AnonymousClass2 INSTANCE = new AnonymousClass2();

                @Override // java.util.concurrent.Executor
                public final void execute(Runnable runnable) {
                    runnable.run();
                }
            }

            public final void call(final Subscriber<? super Bitmap> subscriber) {
                DataSource.this.f(new b.f.j.f.c() { // from class: com.discord.utilities.images.MGImagesBitmap$getBitmap$1.1
                    @Override // b.f.e.d
                    public void onFailureImpl(DataSource<CloseableReference<c>> dataSource) {
                        String message;
                        m.checkNotNullParameter(dataSource, "dataSource");
                        Throwable d = dataSource.d();
                        if (d == null || (message = d.getMessage()) == null || !w.contains$default((CharSequence) message, (CharSequence) "404", false, 2, (Object) null)) {
                            Subscriber subscriber2 = subscriber;
                            Throwable d2 = dataSource.d();
                            if (d2 == null) {
                                d2 = new MGImagesBitmap.DecodeException(str);
                            }
                            subscriber2.onError(d2);
                            return;
                        }
                        subscriber.onError(new MGImagesBitmap.ImageNotFoundException(str));
                    }

                    @Override // b.f.j.f.c
                    public void onNewResultImpl(Bitmap bitmap) {
                        if (bitmap != null) {
                            subscriber.onNext(Bitmap.createBitmap(bitmap));
                            subscriber.onCompleted();
                            return;
                        }
                        subscriber.onError(new MGImagesBitmap.MissingBitmapException(str));
                    }
                }, AnonymousClass2.INSTANCE);
            }
        });
        m.checkNotNullExpressionValue(h02, "Observable.unsafeCreate …y emits the bitmap.\n    }");
        return h02;
    }
}
