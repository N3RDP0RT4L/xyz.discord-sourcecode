package com.discord.utilities.images;

import andhook.lib.HookHelper;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.widget.ImageView;
import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.FragmentManager;
import b.a.k.b;
import b.c.a.a0.d;
import b.f.g.f.a;
import b.f.g.f.c;
import b.f.j.d.e;
import com.discord.dialogs.ImageUploadDialog;
import com.discord.media_picker.MediaPicker;
import com.discord.media_picker.RequestType;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.icon.IconUtils;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.drawable.ScalingUtils$ScaleType;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.view.DraweeView;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.yalantis.ucrop.UCropActivity;
import d0.g0.s;
import d0.g0.t;
import d0.g0.w;
import d0.t.n;
import d0.t.o;
import d0.y.b;
import d0.z.d.m;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import org.objectweb.asm.Opcodes;
import rx.functions.Action1;
import xyz.discord.R;
/* compiled from: MGImages.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000°\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\b\bÆ\u0002\u0018\u00002\u00020\u0001:\u0003\\]^B\t\b\u0002¢\u0006\u0004\bZ\u0010[JG\u0010\u000e\u001a\u00020\r2\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\b\b\u0002\u0010\t\u001a\u00020\b2\b\b\u0002\u0010\n\u001a\u00020\b2\b\b\u0002\u0010\f\u001a\u00020\u000bH\u0007¢\u0006\u0004\b\u000e\u0010\u000fJ9\u0010\u0015\u001a\u00020\r2\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0010\u001a\u00020\u00062\u0006\u0010\u0012\u001a\u00020\u00112\u000e\u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\u0011\u0018\u00010\u0013H\u0007¢\u0006\u0004\b\u0015\u0010\u0016J\u001f\u0010\u001b\u001a\u00020\r2\u0006\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u001a\u001a\u00020\u0019H\u0007¢\u0006\u0004\b\u001b\u0010\u001cJK\u0010#\u001a\u00020\r2\u0006\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u001d\u001a\u00020\b2\u0006\u0010\u001f\u001a\u00020\u001e2\n\b\u0003\u0010 \u001a\u0004\u0018\u00010\u000b2\n\b\u0003\u0010!\u001a\u0004\u0018\u00010\u000b2\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\bH\u0007¢\u0006\u0004\b#\u0010$Ja\u0010.\u001a\u00020\r2\u0006\u0010\u0018\u001a\u00020\u00172\b\u0010%\u001a\u0004\u0018\u00010\u00112\b\b\u0002\u0010&\u001a\u00020\u000b2\b\b\u0002\u0010'\u001a\u00020\u000b2\b\b\u0002\u0010(\u001a\u00020\u001e2\u0016\b\u0002\u0010+\u001a\u0010\u0012\u0004\u0012\u00020*\u0012\u0004\u0012\u00020\r\u0018\u00010)2\b\b\u0002\u0010-\u001a\u00020,H\u0007¢\u0006\u0004\b.\u0010/Jw\u0010.\u001a\u00020\r2\u0006\u0010\u0018\u001a\u00020\u00172\f\u00101\u001a\b\u0012\u0004\u0012\u00020\u0011002\b\b\u0002\u0010&\u001a\u00020\u000b2\b\b\u0002\u0010'\u001a\u00020\u000b2\b\b\u0002\u0010(\u001a\u00020\u001e2\u0016\b\u0002\u0010+\u001a\u0010\u0012\u0004\u0012\u00020*\u0012\u0004\u0012\u00020\r\u0018\u00010)2\b\b\u0002\u0010-\u001a\u00020,2\u0010\b\u0002\u00104\u001a\n\u0012\u0004\u0012\u000203\u0018\u000102H\u0007¢\u0006\u0004\b.\u00105J/\u00106\u001a\u00020*2\u0006\u0010%\u001a\u00020\u00112\u0006\u0010&\u001a\u00020\u000b2\u0006\u0010'\u001a\u00020\u000b2\u0006\u0010(\u001a\u00020\u001eH\u0007¢\u0006\u0004\b6\u00107JG\u0010=\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u00062\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u00109\u001a\u0002082\u0006\u0010\u0005\u001a\u00020\u00042\u000e\u0010:\u001a\n\u0012\u0004\u0012\u00020\u0011\u0018\u00010\u00132\u0006\u0010<\u001a\u00020;H\u0007¢\u0006\u0004\b=\u0010>J\u001b\u0010A\u001a\u0006\u0012\u0002\b\u00030@2\u0006\u0010?\u001a\u00020\u0017H\u0002¢\u0006\u0004\bA\u0010BJ\u0017\u0010D\u001a\u00020C2\u0006\u0010?\u001a\u00020\u0017H\u0002¢\u0006\u0004\bD\u0010EJ'\u0010.\u001a\u00020\r2\u0006\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0010\u001a\u00020\u00062\b\b\u0002\u0010-\u001a\u00020,¢\u0006\u0004\b.\u0010FJ)\u0010.\u001a\u00020\r2\u0006\u0010\u0018\u001a\u00020\u00172\b\b\u0001\u0010G\u001a\u00020\u000b2\b\b\u0002\u0010-\u001a\u00020,¢\u0006\u0004\b.\u0010HJ1\u0010.\u001a\u00020\r2\u0006\u0010\u0018\u001a\u00020\u00172\b\b\u0001\u0010G\u001a\u00020\u000b2\u0006\u0010\u001a\u001a\u00020\u00192\b\b\u0002\u0010-\u001a\u00020,¢\u0006\u0004\b.\u0010IJ'\u0010.\u001a\u00020\r2\u0006\u0010\u0018\u001a\u00020\u00172\u0006\u0010K\u001a\u00020J2\b\b\u0002\u0010-\u001a\u00020,¢\u0006\u0004\b.\u0010LJ\u0015\u0010M\u001a\u00020\r2\u0006\u0010\u0018\u001a\u00020\u0017¢\u0006\u0004\bM\u0010NJ5\u0010U\u001a\u00020O2\u0006\u0010P\u001a\u00020O2\u0006\u0010Q\u001a\u00020\u000b2\u0006\u0010R\u001a\u00020\u000b2\u0006\u0010S\u001a\u00020\u000b2\u0006\u0010T\u001a\u00020\u000b¢\u0006\u0004\bU\u0010VR\u0016\u0010X\u001a\u00020W8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bX\u0010Y¨\u0006_"}, d2 = {"Lcom/discord/utilities/images/MGImages;", "", "Landroid/content/Context;", "context", "Lcom/discord/media_picker/MediaPicker$Provider;", "provider", "Landroid/net/Uri;", "inputUri", "", "aspectRatioX", "aspectRatioY", "", "maxOutputDimensionPx", "", "requestImageCrop", "(Landroid/content/Context;Lcom/discord/media_picker/MediaPicker$Provider;Landroid/net/Uri;FFI)V", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "", "mimeType", "Lrx/functions/Action1;", "resultCallback", "requestDataUrl", "(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Lrx/functions/Action1;)V", "Landroid/widget/ImageView;", "view", "Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;", "scaleType", "setScaleType", "(Landroid/widget/ImageView;Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;)V", "cornerRadius", "", "circle", "overlayColor", "borderColor", "borderWidth", "setRoundingParams", "(Landroid/widget/ImageView;FZLjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Float;)V", "url", "width", "height", "useSmallCache", "Lkotlin/Function1;", "Lcom/facebook/imagepipeline/request/ImageRequestBuilder;", "transform", "Lcom/discord/utilities/images/MGImages$ChangeDetector;", "changeDetector", "setImage", "(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;)V", "", "urls", "Lcom/facebook/drawee/controller/ControllerListener;", "Lcom/facebook/imagepipeline/image/ImageInfo;", "controllerListener", "(Landroid/widget/ImageView;Ljava/util/List;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;Lcom/facebook/drawee/controller/ControllerListener;)V", "getImageRequest", "(Ljava/lang/String;IIZ)Lcom/facebook/imagepipeline/request/ImageRequestBuilder;", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "cropResultCallback", "Lcom/discord/dialogs/ImageUploadDialog$PreviewType;", "previewType", "prepareImageUpload", "(Landroid/net/Uri;Ljava/lang/String;Landroidx/fragment/app/FragmentManager;Lcom/discord/media_picker/MediaPicker$Provider;Lrx/functions/Action1;Lcom/discord/dialogs/ImageUploadDialog$PreviewType;)V", "imageView", "Lcom/facebook/drawee/view/DraweeView;", "getDrawee", "(Landroid/widget/ImageView;)Lcom/facebook/drawee/view/DraweeView;", "Lcom/facebook/drawee/generic/GenericDraweeHierarchy;", "getHierarchy", "(Landroid/widget/ImageView;)Lcom/facebook/drawee/generic/GenericDraweeHierarchy;", "(Landroid/widget/ImageView;Landroid/net/Uri;Lcom/discord/utilities/images/MGImages$ChangeDetector;)V", "resourceId", "(Landroid/widget/ImageView;ILcom/discord/utilities/images/MGImages$ChangeDetector;)V", "(Landroid/widget/ImageView;ILcom/facebook/drawee/drawable/ScalingUtils$ScaleType;Lcom/discord/utilities/images/MGImages$ChangeDetector;)V", "Landroid/graphics/drawable/Drawable;", "drawable", "(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;Lcom/discord/utilities/images/MGImages$ChangeDetector;)V", "cancelImageRequests", "(Landroid/widget/ImageView;)V", "Landroid/graphics/Bitmap;", "src", "innerHeight", "innerWidth", "outerHeight", "outerWidth", "centerBitmapInTransparentBitmap", "(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;", "Lcom/discord/utilities/images/ImageEncoder;", "imageEncoder", "Lcom/discord/utilities/images/ImageEncoder;", HookHelper.constructorName, "()V", "AlwaysUpdateChangeDetector", "ChangeDetector", "DistinctChangeDetector", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MGImages {
    public static final MGImages INSTANCE = new MGImages();
    private static final ImageEncoder imageEncoder = new ImageEncoder();

    /* compiled from: MGImages.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tJ!\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0004\u001a\u0004\u0018\u00010\u0002H\u0016¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/utilities/images/MGImages$AlwaysUpdateChangeDetector;", "Lcom/discord/utilities/images/MGImages$ChangeDetector;", "", "key", "value", "", "track", "(Ljava/lang/Object;Ljava/lang/Object;)Z", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class AlwaysUpdateChangeDetector implements ChangeDetector {
        public static final AlwaysUpdateChangeDetector INSTANCE = new AlwaysUpdateChangeDetector();

        private AlwaysUpdateChangeDetector() {
        }

        @Override // com.discord.utilities.images.MGImages.ChangeDetector
        public boolean track(Object obj, Object obj2) {
            m.checkNotNullParameter(obj, "key");
            return true;
        }
    }

    /* compiled from: MGImages.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J!\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0002\u001a\u00020\u00012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0001H&¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/utilities/images/MGImages$ChangeDetector;", "", "key", "value", "", "track", "(Ljava/lang/Object;Ljava/lang/Object;)Z", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface ChangeDetector {
        boolean track(Object obj, Object obj2);
    }

    /* compiled from: MGImages.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\f\u0010\rJ!\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0004\u001a\u0004\u0018\u00010\u0002H\u0016¢\u0006\u0004\b\u0006\u0010\u0007R6\u0010\n\u001a\"\u0012\u0004\u0012\u00020\u0002\u0012\u0006\u0012\u0004\u0018\u00010\u00020\bj\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0006\u0012\u0004\u0018\u00010\u0002`\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;", "Lcom/discord/utilities/images/MGImages$ChangeDetector;", "", "key", "value", "", "track", "(Ljava/lang/Object;Ljava/lang/Object;)Z", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "dataMap", "Ljava/util/HashMap;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class DistinctChangeDetector implements ChangeDetector {
        private final HashMap<Object, Object> dataMap = new HashMap<>();

        @Override // com.discord.utilities.images.MGImages.ChangeDetector
        public boolean track(Object obj, Object obj2) {
            m.checkNotNullParameter(obj, "key");
            if (this.dataMap.containsKey(obj) && m.areEqual(this.dataMap.get(obj), obj2)) {
                return false;
            }
            this.dataMap.put(obj, obj2);
            return true;
        }
    }

    private MGImages() {
    }

    private final DraweeView<?> getDrawee(ImageView imageView) {
        Objects.requireNonNull(imageView, "null cannot be cast to non-null type com.facebook.drawee.view.DraweeView<*>");
        return (DraweeView) imageView;
    }

    private final GenericDraweeHierarchy getHierarchy(ImageView imageView) {
        DraweeView<?> drawee = getDrawee(imageView);
        if (!drawee.hasHierarchy()) {
            drawee.setHierarchy(new a(imageView.getResources()).a());
        }
        Object hierarchy = drawee.getHierarchy();
        Objects.requireNonNull(hierarchy, "null cannot be cast to non-null type com.facebook.drawee.generic.GenericDraweeHierarchy");
        return (GenericDraweeHierarchy) hierarchy;
    }

    public static final ImageRequestBuilder getImageRequest(String str, int i, int i2, boolean z2) {
        ImageRequest.b bVar;
        m.checkNotNullParameter(str, "url");
        ImageRequestBuilder b2 = ImageRequestBuilder.b(Uri.parse(str));
        b2.f2876b = ImageRequest.c.FULL_FETCH;
        m.checkNotNullExpressionValue(b2, "requestBuilder");
        boolean z3 = false;
        if (z2 || !w.contains$default((CharSequence) str, (CharSequence) IconUtils.ANIMATED_IMAGE_EXTENSION, false, 2, (Object) null)) {
            bVar = ImageRequest.b.SMALL;
        } else {
            bVar = ImageRequest.b.DEFAULT;
        }
        b2.g = bVar;
        if (i > 0 && i2 > 0) {
            z3 = true;
        }
        if (z3) {
            b2.d = new e(i, i2);
        }
        return b2;
    }

    public static final void prepareImageUpload(Uri uri, String str, FragmentManager fragmentManager, MediaPicker.Provider provider, Action1<String> action1, ImageUploadDialog.PreviewType previewType) {
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(str, "mimeType");
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        m.checkNotNullParameter(provider, "provider");
        m.checkNotNullParameter(previewType, "previewType");
        Objects.requireNonNull(ImageUploadDialog.k);
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(provider, "provider");
        m.checkNotNullParameter(str, "mimeType");
        m.checkNotNullParameter(previewType, "previewType");
        ImageUploadDialog imageUploadDialog = new ImageUploadDialog();
        m.checkNotNullParameter(uri, "<set-?>");
        imageUploadDialog.m = uri;
        m.checkNotNullParameter(provider, "<set-?>");
        imageUploadDialog.n = provider;
        imageUploadDialog.o = str;
        imageUploadDialog.p = action1;
        imageUploadDialog.q = previewType;
        imageUploadDialog.show(fragmentManager, ImageUploadDialog.class.getName());
    }

    public static final void requestDataUrl(Context context, Uri uri, String str, Action1<String> action1) {
        CharSequence b2;
        CharSequence b3;
        CharSequence b4;
        InputStream openInputStream;
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(str, "mimeType");
        if (context != null) {
            try {
                ContentResolver contentResolver = context.getContentResolver();
                if (contentResolver != null && (openInputStream = contentResolver.openInputStream(uri)) != null) {
                    if (action1 != null) {
                        ImageEncoder imageEncoder2 = imageEncoder;
                        m.checkNotNullExpressionValue(openInputStream, "it");
                        action1.call(imageEncoder2.getDataUrl(str, openInputStream));
                    }
                    b.closeFinally(openInputStream, null);
                }
            } catch (IOException e) {
                b2 = b.a.k.b.b(context, R.string.avatar_convert_failure_mobile, new Object[]{e.getMessage()}, (r4 & 4) != 0 ? b.C0034b.j : null);
                b.a.d.m.h(context, b2, 0, null, 12);
            } catch (IllegalStateException e2) {
                b4 = b.a.k.b.b(context, R.string.avatar_convert_failure_mobile, new Object[]{e2.getMessage()}, (r4 & 4) != 0 ? b.C0034b.j : null);
                b.a.d.m.h(context, b4, 0, null, 12);
            } catch (NullPointerException e3) {
                b3 = b.a.k.b.b(context, R.string.avatar_convert_failure_mobile, new Object[]{e3.getMessage()}, (r4 & 4) != 0 ? b.C0034b.j : null);
                b.a.d.m.h(context, b3, 0, null, 12);
            }
        }
    }

    public static final void requestImageCrop(Context context, MediaPicker.Provider provider, Uri uri, float f, float f2, int i) {
        m.checkNotNullParameter(provider, "provider");
        m.checkNotNullParameter(uri, "inputUri");
        MGImages$requestImageCrop$1 mGImages$requestImageCrop$1 = new MGImages$requestImageCrop$1(context);
        int themedColor = ColorCompat.getThemedColor(context, (int) R.attr.color_brand_500);
        int color = ColorCompat.getColor(context, (int) R.color.white);
        int themedColor2 = ColorCompat.getThemedColor(context, (int) R.attr.color_brand_630);
        int themedColor3 = ColorCompat.getThemedColor(context, (int) R.attr.color_brand_500);
        m.checkNotNullParameter(provider, "provider");
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(mGImages$requestImageCrop$1, "onError");
        try {
            Bundle bundle = new Bundle();
            bundle.putInt("com.yalantis.ucrop.ToolbarColor", themedColor);
            bundle.putInt("com.yalantis.ucrop.StatusBarColor", themedColor2);
            bundle.putInt("com.yalantis.ucrop.UcropToolbarWidgetColor", color);
            bundle.putInt("com.yalantis.ucrop.UcropColorControlsWidgetActive", themedColor3);
            bundle.putInt("com.yalantis.ucrop.CropFrameColor", themedColor3);
            bundle.putInt("com.yalantis.ucrop.CropGridColor", themedColor3);
            if (bundle.getString("com.yalantis.ucrop.CompressionFormatName") == null) {
                bundle.putString("com.yalantis.ucrop.CompressionFormatName", Bitmap.CompressFormat.PNG.name());
            }
            Context requireContext = provider.requireContext();
            Uri fromFile = Uri.fromFile(provider.getImageFile());
            m.checkNotNullExpressionValue(fromFile, "Uri.fromFile(provider.getImageFile())");
            b.q.a.a aVar = new b.q.a.a(uri, fromFile);
            m.checkNotNullExpressionValue(aVar, "UCrop.of(uri, destUri)");
            aVar.f1973b.putAll(bundle);
            int i2 = i < 10 ? 10 : i;
            if (i < 10) {
                i = 10;
            }
            aVar.f1973b.putInt("com.yalantis.ucrop.MaxSizeX", i2);
            aVar.f1973b.putInt("com.yalantis.ucrop.MaxSizeY", i);
            aVar.f1973b.putFloat("com.yalantis.ucrop.AspectRatioX", f);
            aVar.f1973b.putFloat("com.yalantis.ucrop.AspectRatioY", f2);
            aVar.a.setClass(requireContext, UCropActivity.class);
            aVar.a.putExtras(aVar.f1973b);
            Intent intent = aVar.a;
            m.checkNotNullExpressionValue(intent, "uCrop.getIntent(context)");
            List<ResolveInfo> queryIntentActivities = requireContext.getPackageManager().queryIntentActivities(intent, 65536);
            m.checkNotNullExpressionValue(queryIntentActivities, "context\n        .package…nager.MATCH_DEFAULT_ONLY)");
            for (ResolveInfo resolveInfo : queryIntentActivities) {
                requireContext.grantUriPermission(resolveInfo.activityInfo.packageName, fromFile, 3);
            }
            try {
                provider.startActivityForResult(intent, RequestType.CROP.getCode$media_picker_release());
            } catch (ActivityNotFoundException unused) {
                throw new IOException("No application available for media picker.");
            }
        } catch (IOException e) {
            mGImages$requestImageCrop$1.invoke((MGImages$requestImageCrop$1) e);
        }
    }

    public static final void setImage(ImageView imageView, String str) {
        setImage$default(imageView, str, 0, 0, false, null, null, 124, null);
    }

    public static final void setImage(ImageView imageView, String str, int i) {
        setImage$default(imageView, str, i, 0, false, null, null, 120, null);
    }

    public static final void setImage(ImageView imageView, String str, int i, int i2) {
        setImage$default(imageView, str, i, i2, false, null, null, 112, null);
    }

    public static final void setImage(ImageView imageView, String str, int i, int i2, boolean z2) {
        setImage$default(imageView, str, i, i2, z2, null, null, 96, null);
    }

    public static final void setImage(ImageView imageView, String str, int i, int i2, boolean z2, Function1<? super ImageRequestBuilder, Unit> function1) {
        setImage$default(imageView, str, i, i2, z2, function1, null, 64, null);
    }

    public static final void setImage(ImageView imageView, List<String> list) {
        setImage$default(imageView, list, 0, 0, false, null, null, null, 252, null);
    }

    public static final void setImage(ImageView imageView, List<String> list, int i) {
        setImage$default(imageView, list, i, 0, false, null, null, null, 248, null);
    }

    public static final void setImage(ImageView imageView, List<String> list, int i, int i2) {
        setImage$default(imageView, list, i, i2, false, null, null, null, 240, null);
    }

    public static final void setImage(ImageView imageView, List<String> list, int i, int i2, boolean z2) {
        setImage$default(imageView, list, i, i2, z2, null, null, null, 224, null);
    }

    public static final void setImage(ImageView imageView, List<String> list, int i, int i2, boolean z2, Function1<? super ImageRequestBuilder, Unit> function1) {
        setImage$default(imageView, list, i, i2, z2, function1, null, null, Opcodes.CHECKCAST, null);
    }

    public static final void setImage(ImageView imageView, List<String> list, int i, int i2, boolean z2, Function1<? super ImageRequestBuilder, Unit> function1, ChangeDetector changeDetector) {
        setImage$default(imageView, list, i, i2, z2, function1, changeDetector, null, 128, null);
    }

    public static /* synthetic */ void setImage$default(MGImages mGImages, ImageView imageView, Uri uri, ChangeDetector changeDetector, int i, Object obj) {
        if ((i & 4) != 0) {
            changeDetector = AlwaysUpdateChangeDetector.INSTANCE;
        }
        mGImages.setImage(imageView, uri, changeDetector);
    }

    public static final void setRoundingParams(ImageView imageView, float f, boolean z2, @ColorInt Integer num, @ColorInt Integer num2, Float f2) {
        c cVar;
        m.checkNotNullParameter(imageView, "view");
        boolean z3 = true;
        if (z2) {
            cVar = new c();
            cVar.f519b = true;
            cVar.a = 1;
        } else {
            cVar = c.a(f);
        }
        if (Build.VERSION.SDK_INT == 28) {
            m.checkNotNullExpressionValue(cVar, "roundingParams");
            cVar.h = true;
        }
        if (num != null) {
            num.intValue();
            m.checkNotNullExpressionValue(cVar, "roundingParams");
            cVar.b(num.intValue());
        }
        if (num2 != null) {
            num2.intValue();
            m.checkNotNullExpressionValue(cVar, "roundingParams");
            cVar.f = num2.intValue();
        }
        if (f2 != null) {
            f2.floatValue();
            m.checkNotNullExpressionValue(cVar, "roundingParams");
            float floatValue = f2.floatValue();
            if (floatValue < 0.0f) {
                z3 = false;
            }
            d.k(z3, "the border width cannot be < 0");
            cVar.e = floatValue;
        }
        INSTANCE.getHierarchy(imageView).s(cVar);
    }

    public static final void setScaleType(ImageView imageView, ScalingUtils$ScaleType scalingUtils$ScaleType) {
        m.checkNotNullParameter(imageView, "view");
        m.checkNotNullParameter(scalingUtils$ScaleType, "scaleType");
        INSTANCE.getHierarchy(imageView).n(scalingUtils$ScaleType);
    }

    public final void cancelImageRequests(ImageView imageView) {
        m.checkNotNullParameter(imageView, "view");
        getDrawee(imageView).setController(null);
    }

    public final Bitmap centerBitmapInTransparentBitmap(Bitmap bitmap, int i, int i2, int i3, int i4) {
        m.checkNotNullParameter(bitmap, "src");
        if (i4 <= i2 || i3 <= i) {
            StringBuilder U = b.d.b.a.a.U("Cannot fit bitmap of size ", i2, " x ", i, " inside ");
            U.append("bitmap of size ");
            U.append(i4);
            U.append(" x ");
            U.append(i3);
            throw new IllegalArgumentException(U.toString());
        }
        Bitmap createBitmap = Bitmap.createBitmap(i4, i3, bitmap.getConfig());
        float f = (i4 - i2) / 2.0f;
        float f2 = (i3 - i) / 2.0f;
        new Canvas(createBitmap).drawBitmap(bitmap, (Rect) null, new RectF(f, f2, i2 + f, i + f2), (Paint) null);
        m.checkNotNullExpressionValue(createBitmap, "dest");
        return createBitmap;
    }

    public final void setImage(ImageView imageView, Uri uri, ChangeDetector changeDetector) {
        Integer intOrNull;
        m.checkNotNullParameter(imageView, "view");
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(changeDetector, "changeDetector");
        if (m.areEqual(uri.getScheme(), "android.resource")) {
            String host = uri.getHost();
            Context context = imageView.getContext();
            m.checkNotNullExpressionValue(context, "view.context");
            if (m.areEqual(host, context.getPackageName())) {
                String lastPathSegment = uri.getLastPathSegment();
                setImage(imageView, (lastPathSegment == null || (intOrNull = s.toIntOrNull(lastPathSegment)) == null) ? 0 : intOrNull.intValue(), changeDetector);
                return;
            }
        }
        setImage$default(imageView, uri.toString(), 0, 0, false, null, changeDetector, 60, null);
    }

    public static /* synthetic */ void setImage$default(MGImages mGImages, ImageView imageView, int i, ChangeDetector changeDetector, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            changeDetector = AlwaysUpdateChangeDetector.INSTANCE;
        }
        mGImages.setImage(imageView, i, changeDetector);
    }

    public static /* synthetic */ void setImage$default(MGImages mGImages, ImageView imageView, int i, ScalingUtils$ScaleType scalingUtils$ScaleType, ChangeDetector changeDetector, int i2, Object obj) {
        if ((i2 & 8) != 0) {
            changeDetector = AlwaysUpdateChangeDetector.INSTANCE;
        }
        mGImages.setImage(imageView, i, scalingUtils$ScaleType, changeDetector);
    }

    public static /* synthetic */ void setImage$default(MGImages mGImages, ImageView imageView, Drawable drawable, ChangeDetector changeDetector, int i, Object obj) {
        if ((i & 4) != 0) {
            changeDetector = AlwaysUpdateChangeDetector.INSTANCE;
        }
        mGImages.setImage(imageView, drawable, changeDetector);
    }

    public static /* synthetic */ void setImage$default(ImageView imageView, String str, int i, int i2, boolean z2, Function1 function1, ChangeDetector changeDetector, int i3, Object obj) {
        boolean z3 = false;
        int i4 = (i3 & 4) != 0 ? 0 : i;
        int i5 = (i3 & 8) != 0 ? 0 : i2;
        if ((i3 & 16) == 0) {
            z3 = z2;
        }
        setImage(imageView, str, i4, i5, z3, (i3 & 32) != 0 ? null : function1, (i3 & 64) != 0 ? AlwaysUpdateChangeDetector.INSTANCE : changeDetector);
    }

    public final void setImage(ImageView imageView, @DrawableRes int i, ChangeDetector changeDetector) {
        m.checkNotNullParameter(imageView, "view");
        m.checkNotNullParameter(changeDetector, "changeDetector");
        if (changeDetector.track(imageView, Integer.valueOf(i))) {
            GenericDraweeHierarchy hierarchy = getHierarchy(imageView);
            hierarchy.o(1, hierarchy.f2862b.getDrawable(i));
        }
    }

    public static /* synthetic */ void setImage$default(ImageView imageView, List list, int i, int i2, boolean z2, Function1 function1, ChangeDetector changeDetector, ControllerListener controllerListener, int i3, Object obj) {
        boolean z3 = false;
        int i4 = (i3 & 4) != 0 ? 0 : i;
        int i5 = (i3 & 8) != 0 ? 0 : i2;
        if ((i3 & 16) == 0) {
            z3 = z2;
        }
        ControllerListener controllerListener2 = null;
        Function1 function12 = (i3 & 32) != 0 ? null : function1;
        ChangeDetector changeDetector2 = (i3 & 64) != 0 ? AlwaysUpdateChangeDetector.INSTANCE : changeDetector;
        if ((i3 & 128) == 0) {
            controllerListener2 = controllerListener;
        }
        setImage(imageView, list, i4, i5, z3, function12, changeDetector2, controllerListener2);
    }

    public final void setImage(ImageView imageView, @DrawableRes int i, ScalingUtils$ScaleType scalingUtils$ScaleType, ChangeDetector changeDetector) {
        m.checkNotNullParameter(imageView, "view");
        m.checkNotNullParameter(scalingUtils$ScaleType, "scaleType");
        m.checkNotNullParameter(changeDetector, "changeDetector");
        if (changeDetector.track(imageView, Integer.valueOf(i))) {
            GenericDraweeHierarchy hierarchy = getHierarchy(imageView);
            hierarchy.q(hierarchy.f2862b.getDrawable(i), scalingUtils$ScaleType);
        }
    }

    public final void setImage(ImageView imageView, Drawable drawable, ChangeDetector changeDetector) {
        m.checkNotNullParameter(imageView, "view");
        m.checkNotNullParameter(drawable, "drawable");
        m.checkNotNullParameter(changeDetector, "changeDetector");
        if (changeDetector.track(imageView, drawable)) {
            getHierarchy(imageView).o(1, drawable);
        }
    }

    public static final void setImage(ImageView imageView, String str, int i, int i2, boolean z2, Function1<? super ImageRequestBuilder, Unit> function1, ChangeDetector changeDetector) {
        m.checkNotNullParameter(imageView, "view");
        m.checkNotNullParameter(changeDetector, "changeDetector");
        setImage$default(imageView, (str == null || t.isBlank(str)) ? n.emptyList() : d0.t.m.listOf(str), i, i2, z2, function1, changeDetector, null, 128, null);
    }

    /* JADX WARN: Type inference failed for: r4v3, types: [REQUEST[], com.facebook.imagepipeline.request.ImageRequest[]] */
    public static final void setImage(ImageView imageView, List<String> list, int i, int i2, boolean z2, Function1<? super ImageRequestBuilder, Unit> function1, ChangeDetector changeDetector, ControllerListener<ImageInfo> controllerListener) {
        m.checkNotNullParameter(imageView, "view");
        m.checkNotNullParameter(list, "urls");
        m.checkNotNullParameter(changeDetector, "changeDetector");
        if (changeDetector.track(imageView, list)) {
            if (list.isEmpty()) {
                INSTANCE.getDrawee(imageView).setController(null);
                return;
            }
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
            for (String str : list) {
                ImageRequestBuilder imageRequest = getImageRequest(str, i, i2, z2);
                if (function1 != null) {
                    function1.invoke(imageRequest);
                }
                arrayList.add(imageRequest.a());
            }
            boolean z3 = false;
            Object[] array = arrayList.toArray(new ImageRequest[0]);
            Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
            ?? r4 = (ImageRequest[]) array;
            b.f.g.a.a.d a = b.f.g.a.a.b.a();
            MGImages mGImages = INSTANCE;
            a.n = mGImages.getDrawee(imageView).getController();
            a.k = controllerListener;
            a.m = true;
            if (r4.length > 0) {
                z3 = true;
            }
            d.k(z3, "No requests specified!");
            a.i = r4;
            a.j = true;
            mGImages.getDrawee(imageView).setController(a.a());
        }
    }
}
