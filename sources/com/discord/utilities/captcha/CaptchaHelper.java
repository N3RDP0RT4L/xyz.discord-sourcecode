package com.discord.utilities.captcha;

import andhook.lib.HookHelper;
import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.appcompat.widget.ActivityChooserModel;
import androidx.fragment.app.FragmentManager;
import b.d.b.a.a;
import b.i.a.f.e.h.a;
import b.i.a.f.e.h.c;
import b.i.a.f.e.k.k;
import b.i.a.f.e.k.s;
import b.i.a.f.e.k.v;
import b.i.a.f.h.o.i;
import b.i.a.f.h.o.j;
import b.i.a.f.h.o.l;
import b.i.a.f.n.c0;
import b.i.a.f.n.d;
import b.i.a.f.n.e;
import b.i.a.f.n.g;
import com.discord.app.AppActivity;
import com.discord.stores.StoreStream;
import com.discord.utilities.captcha.CaptchaHelper;
import com.discord.widgets.notice.WidgetNoticeDialog;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.safetynet.SafetyNetApi;
import com.google.android.gms.safetynet.SafetyNetClient;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.hcaptcha.sdk.HCaptchaConfig;
import com.hcaptcha.sdk.HCaptchaDialogListener;
import com.hcaptcha.sdk.HCaptchaError;
import com.hcaptcha.sdk.HCaptchaException;
import com.hcaptcha.sdk.HCaptchaSize;
import com.hcaptcha.sdk.HCaptchaTheme;
import com.hcaptcha.sdk.HCaptchaTokenResponse;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.Executor;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Emitter;
import rx.Observable;
import rx.functions.Action1;
import xyz.discord.R;
/* compiled from: CaptchaHelper.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0013\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002()B\t\b\u0002¢\u0006\u0004\b&\u0010'J;\u0010\t\u001a\u00020\u0005*\u00020\u00022\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00032\u0012\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00050\u0003H\u0002¢\u0006\u0004\b\t\u0010\nJ=\u0010\u0010\u001a\u00020\u0005*\u00020\u000b2\u0006\u0010\r\u001a\u00020\f2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00050\u000e2\u0012\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00050\u0003H\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u001d\u0010\u0015\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u00142\u0006\u0010\u0013\u001a\u00020\u0012¢\u0006\u0004\b\u0015\u0010\u0016J)\u0010\u0019\u001a\u00020\u00052\b\u0010\r\u001a\u0004\u0018\u00010\u00172\u0010\b\u0002\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u000e¢\u0006\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001b\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\u0016\u0010\u001d\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001d\u0010\u001cR$\u0010\u001e\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u001e\u0010\u001c\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R\u0016\u0010#\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b#\u0010\u001cR\u0016\u0010$\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b$\u0010\u001cR\u0016\u0010%\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b%\u0010\u001c¨\u0006*"}, d2 = {"Lcom/discord/utilities/captcha/CaptchaHelper;", "", "Lcom/google/android/gms/safetynet/SafetyNetClient;", "Lkotlin/Function1;", "", "", "onSuccess", "Lcom/discord/utilities/captcha/CaptchaHelper$Failure;", "errorHandler", "showCaptcha", "(Lcom/google/android/gms/safetynet/SafetyNetClient;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "Lcom/google/android/gms/common/GoogleApiAvailability;", "Landroid/app/Activity;", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "Lkotlin/Function0;", "onReady", "ensurePlayServicesAvailable", "(Lcom/google/android/gms/common/GoogleApiAvailability;Landroid/app/Activity;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V", "Lcom/discord/utilities/captcha/CaptchaHelper$CaptchaRequest;", "captchaRequest", "Lrx/Observable;", "tryShowCaptcha", "(Lcom/discord/utilities/captcha/CaptchaHelper$CaptchaRequest;)Lrx/Observable;", "Lcom/discord/app/AppActivity;", "onOpenBrowser", "showCaptchaHelpDialog", "(Lcom/discord/app/AppActivity;Lkotlin/jvm/functions/Function0;)V", "FAILED_CAPTCHA_EXPIRED", "Ljava/lang/String;", "RECAPTCHA_SITE_KEY", "captchaToken", "getCaptchaToken", "()Ljava/lang/String;", "setCaptchaToken", "(Ljava/lang/String;)V", "CAPTCHA_KEY", "FAILED_MISSING_DEPS", "FAILED_DEVICE_UNSUPPORTED", HookHelper.constructorName, "()V", "CaptchaRequest", "Failure", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class CaptchaHelper {
    public static final String CAPTCHA_KEY = "captcha_key";
    private static final String FAILED_CAPTCHA_EXPIRED = "expired";
    private static final String FAILED_DEVICE_UNSUPPORTED = "unsupported_device";
    private static final String FAILED_MISSING_DEPS = "missing_dependencies";
    public static final CaptchaHelper INSTANCE = new CaptchaHelper();
    private static final String RECAPTCHA_SITE_KEY = "6Lff5jIUAAAAAImNXvYYPv2VW2En3Dexy4oX2o4s";
    private static String captchaToken;

    /* compiled from: CaptchaHelper.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/utilities/captcha/CaptchaHelper$CaptchaRequest;", "", HookHelper.constructorName, "()V", "HCaptcha", "ReCaptcha", "Lcom/discord/utilities/captcha/CaptchaHelper$CaptchaRequest$HCaptcha;", "Lcom/discord/utilities/captcha/CaptchaHelper$CaptchaRequest$ReCaptcha;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class CaptchaRequest {

        /* compiled from: CaptchaHelper.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\f\u0010\u0004J\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0007¨\u0006\u001b"}, d2 = {"Lcom/discord/utilities/captcha/CaptchaHelper$CaptchaRequest$HCaptcha;", "Lcom/discord/utilities/captcha/CaptchaHelper$CaptchaRequest;", "", "component1", "()Ljava/lang/String;", "Landroid/app/Activity;", "component2", "()Landroid/app/Activity;", "siteKey", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "copy", "(Ljava/lang/String;Landroid/app/Activity;)Lcom/discord/utilities/captcha/CaptchaHelper$CaptchaRequest$HCaptcha;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getSiteKey", "Landroid/app/Activity;", "getActivity", HookHelper.constructorName, "(Ljava/lang/String;Landroid/app/Activity;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class HCaptcha extends CaptchaRequest {
            private final Activity activity;
            private final String siteKey;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public HCaptcha(String str, Activity activity) {
                super(null);
                m.checkNotNullParameter(str, "siteKey");
                m.checkNotNullParameter(activity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
                this.siteKey = str;
                this.activity = activity;
            }

            public static /* synthetic */ HCaptcha copy$default(HCaptcha hCaptcha, String str, Activity activity, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = hCaptcha.siteKey;
                }
                if ((i & 2) != 0) {
                    activity = hCaptcha.activity;
                }
                return hCaptcha.copy(str, activity);
            }

            public final String component1() {
                return this.siteKey;
            }

            public final Activity component2() {
                return this.activity;
            }

            public final HCaptcha copy(String str, Activity activity) {
                m.checkNotNullParameter(str, "siteKey");
                m.checkNotNullParameter(activity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
                return new HCaptcha(str, activity);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof HCaptcha)) {
                    return false;
                }
                HCaptcha hCaptcha = (HCaptcha) obj;
                return m.areEqual(this.siteKey, hCaptcha.siteKey) && m.areEqual(this.activity, hCaptcha.activity);
            }

            public final Activity getActivity() {
                return this.activity;
            }

            public final String getSiteKey() {
                return this.siteKey;
            }

            public int hashCode() {
                String str = this.siteKey;
                int i = 0;
                int hashCode = (str != null ? str.hashCode() : 0) * 31;
                Activity activity = this.activity;
                if (activity != null) {
                    i = activity.hashCode();
                }
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("HCaptcha(siteKey=");
                R.append(this.siteKey);
                R.append(", activity=");
                R.append(this.activity);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: CaptchaHelper.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/utilities/captcha/CaptchaHelper$CaptchaRequest$ReCaptcha;", "Lcom/discord/utilities/captcha/CaptchaHelper$CaptchaRequest;", "Landroid/app/Activity;", "component1", "()Landroid/app/Activity;", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "copy", "(Landroid/app/Activity;)Lcom/discord/utilities/captcha/CaptchaHelper$CaptchaRequest$ReCaptcha;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Landroid/app/Activity;", "getActivity", HookHelper.constructorName, "(Landroid/app/Activity;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class ReCaptcha extends CaptchaRequest {
            private final Activity activity;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public ReCaptcha(Activity activity) {
                super(null);
                m.checkNotNullParameter(activity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
                this.activity = activity;
            }

            public static /* synthetic */ ReCaptcha copy$default(ReCaptcha reCaptcha, Activity activity, int i, Object obj) {
                if ((i & 1) != 0) {
                    activity = reCaptcha.activity;
                }
                return reCaptcha.copy(activity);
            }

            public final Activity component1() {
                return this.activity;
            }

            public final ReCaptcha copy(Activity activity) {
                m.checkNotNullParameter(activity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
                return new ReCaptcha(activity);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof ReCaptcha) && m.areEqual(this.activity, ((ReCaptcha) obj).activity);
                }
                return true;
            }

            public final Activity getActivity() {
                return this.activity;
            }

            public int hashCode() {
                Activity activity = this.activity;
                if (activity != null) {
                    return activity.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("ReCaptcha(activity=");
                R.append(this.activity);
                R.append(")");
                return R.toString();
            }
        }

        private CaptchaRequest() {
        }

        public /* synthetic */ CaptchaRequest(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: CaptchaHelper.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\f\u0010\u0007J\u0010\u0010\r\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\r\u0010\u0004J\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0013\u001a\u0004\b\u0014\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004¨\u0006\u0019"}, d2 = {"Lcom/discord/utilities/captcha/CaptchaHelper$Failure;", "", "", "component1", "()I", "", "component2", "()Ljava/lang/String;", "errorStringId", "reasonCode", "copy", "(ILjava/lang/String;)Lcom/discord/utilities/captcha/CaptchaHelper$Failure;", "toString", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getReasonCode", "I", "getErrorStringId", HookHelper.constructorName, "(ILjava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Failure extends Throwable {
        private final int errorStringId;
        private final String reasonCode;

        public Failure(int i, String str) {
            m.checkNotNullParameter(str, "reasonCode");
            this.errorStringId = i;
            this.reasonCode = str;
        }

        public static /* synthetic */ Failure copy$default(Failure failure, int i, String str, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                i = failure.errorStringId;
            }
            if ((i2 & 2) != 0) {
                str = failure.reasonCode;
            }
            return failure.copy(i, str);
        }

        public final int component1() {
            return this.errorStringId;
        }

        public final String component2() {
            return this.reasonCode;
        }

        public final Failure copy(int i, String str) {
            m.checkNotNullParameter(str, "reasonCode");
            return new Failure(i, str);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Failure)) {
                return false;
            }
            Failure failure = (Failure) obj;
            return this.errorStringId == failure.errorStringId && m.areEqual(this.reasonCode, failure.reasonCode);
        }

        public final int getErrorStringId() {
            return this.errorStringId;
        }

        public final String getReasonCode() {
            return this.reasonCode;
        }

        public int hashCode() {
            int i = this.errorStringId * 31;
            String str = this.reasonCode;
            return i + (str != null ? str.hashCode() : 0);
        }

        @Override // java.lang.Throwable
        public String toString() {
            StringBuilder R = a.R("Failure(errorStringId=");
            R.append(this.errorStringId);
            R.append(", reasonCode=");
            return a.H(R, this.reasonCode, ")");
        }
    }

    private CaptchaHelper() {
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void ensurePlayServicesAvailable(GoogleApiAvailability googleApiAvailability, Activity activity, final Function0<Unit> function0, final Function1<? super Failure, Unit> function1) {
        int c = googleApiAvailability.c(activity);
        if (c != 0 && googleApiAvailability.d(c)) {
            Task<Void> e = googleApiAvailability.e(activity);
            e<Void> captchaHelper$ensurePlayServicesAvailable$1 = new e<Void>() { // from class: com.discord.utilities.captcha.CaptchaHelper$ensurePlayServicesAvailable$1
                public final void onSuccess(Void r1) {
                    Function0.this.invoke();
                }
            };
            c0 c0Var = (c0) e;
            Objects.requireNonNull(c0Var);
            Executor executor = g.a;
            c0Var.g(executor, captchaHelper$ensurePlayServicesAvailable$1);
            c0Var.e(executor, new d() { // from class: com.discord.utilities.captcha.CaptchaHelper$ensurePlayServicesAvailable$2
                @Override // b.i.a.f.n.d
                public final void onFailure(Exception exc) {
                    Function1.this.invoke(new CaptchaHelper.Failure(R.string.captcha_failed_play_services, "missing_dependencies"));
                }
            });
            m.checkNotNullExpressionValue(c0Var, "makeGooglePlayServicesAv…ISSING_DEPS))\n          }");
        } else if (c == 0) {
            function0.invoke();
        } else {
            function1.invoke(new Failure(R.string.captcha_failed_unsupported, FAILED_DEVICE_UNSUPPORTED));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void showCaptcha(SafetyNetClient safetyNetClient, final Function1<? super String, Unit> function1, final Function1<? super Failure, Unit> function12) {
        Objects.requireNonNull(safetyNetClient);
        SafetyNetApi safetyNetApi = b.i.a.f.k.a.d;
        c cVar = safetyNetClient.g;
        Objects.requireNonNull((i) safetyNetApi);
        if (!TextUtils.isEmpty(RECAPTCHA_SITE_KEY)) {
            b.i.a.f.e.h.j.d a = cVar.a(new j(cVar, RECAPTCHA_SITE_KEY));
            v vVar = new v(new SafetyNetApi.RecaptchaTokenResponse());
            k.b bVar = k.a;
            TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
            a.c(new s(a, taskCompletionSource, vVar, bVar));
            Task task = taskCompletionSource.a;
            e<SafetyNetApi.RecaptchaTokenResponse> captchaHelper$showCaptcha$1 = new e<SafetyNetApi.RecaptchaTokenResponse>() { // from class: com.discord.utilities.captcha.CaptchaHelper$showCaptcha$1
                public final void onSuccess(SafetyNetApi.RecaptchaTokenResponse recaptchaTokenResponse) {
                    m.checkNotNullExpressionValue(recaptchaTokenResponse, "it");
                    String C = ((SafetyNetApi.a) recaptchaTokenResponse.a).C();
                    m.checkNotNullExpressionValue(C, "userResponseToken");
                    if (!(C.length() == 0)) {
                        Function1 function13 = Function1.this;
                        function13.invoke("android:" + C);
                    }
                }
            };
            Objects.requireNonNull(task);
            Executor executor = g.a;
            task.g(executor, captchaHelper$showCaptcha$1);
            task.e(executor, new d() { // from class: com.discord.utilities.captcha.CaptchaHelper$showCaptcha$2
                @Override // b.i.a.f.n.d
                public final void onFailure(Exception exc) {
                    Function1.this.invoke(new CaptchaHelper.Failure(R.string.captcha_failed, "expired"));
                }
            });
            return;
        }
        throw new IllegalArgumentException("Null or empty site key in verifyWithRecaptcha");
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void showCaptchaHelpDialog$default(CaptchaHelper captchaHelper, AppActivity appActivity, Function0 function0, int i, Object obj) {
        if ((i & 2) != 0) {
            function0 = null;
        }
        captchaHelper.showCaptchaHelpDialog(appActivity, function0);
    }

    public final String getCaptchaToken() {
        return captchaToken;
    }

    public final void setCaptchaToken(String str) {
        captchaToken = str;
    }

    public final void showCaptchaHelpDialog(AppActivity appActivity, Function0<Unit> function0) {
        FragmentManager supportFragmentManager;
        if (appActivity != null && (supportFragmentManager = appActivity.getSupportFragmentManager()) != null) {
            WidgetNoticeDialog.Builder negativeButton$default = WidgetNoticeDialog.Builder.setNegativeButton$default(new WidgetNoticeDialog.Builder(appActivity).setTitle(R.string.captcha_problems).setMessage(R.string.captcha_problems_info).setPositiveButton(R.string.captcha_open_browser, new CaptchaHelper$showCaptchaHelpDialog$$inlined$let$lambda$1(appActivity, function0)), (int) R.string.cancel, (Function1) null, 2, (Object) null);
            m.checkNotNullExpressionValue(supportFragmentManager, "it");
            negativeButton$default.show(supportFragmentManager);
        }
    }

    public final Observable<String> tryShowCaptcha(final CaptchaRequest captchaRequest) {
        m.checkNotNullParameter(captchaRequest, "captchaRequest");
        Observable<String> n = Observable.n(new Action1<Emitter<String>>() { // from class: com.discord.utilities.captcha.CaptchaHelper$tryShowCaptcha$1

            /* compiled from: CaptchaHelper.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.utilities.captcha.CaptchaHelper$tryShowCaptcha$1$3  reason: invalid class name */
            /* loaded from: classes.dex */
            public static final class AnonymousClass3 extends o implements Function0<Unit> {
                public final /* synthetic */ Activity $activity;
                public final /* synthetic */ Emitter $emitter;
                public final /* synthetic */ Function1 $errorHandler;

                /* compiled from: CaptchaHelper.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "token", "", "invoke", "(Ljava/lang/String;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.utilities.captcha.CaptchaHelper$tryShowCaptcha$1$3$1  reason: invalid class name */
                /* loaded from: classes.dex */
                public static final class AnonymousClass1 extends o implements Function1<String, Unit> {
                    public AnonymousClass1() {
                        super(1);
                    }

                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(String str) {
                        invoke2(str);
                        return Unit.a;
                    }

                    /* renamed from: invoke  reason: avoid collision after fix types in other method */
                    public final void invoke2(String str) {
                        m.checkNotNullParameter(str, "token");
                        AnonymousClass3.this.$emitter.onNext(str);
                        AnonymousClass3.this.$emitter.onCompleted();
                    }
                }

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public AnonymousClass3(Activity activity, Emitter emitter, Function1 function1) {
                    super(0);
                    this.$activity = activity;
                    this.$emitter = emitter;
                    this.$errorHandler = function1;
                }

                @Override // kotlin.jvm.functions.Function0
                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2() {
                    CaptchaHelper captchaHelper = CaptchaHelper.INSTANCE;
                    Activity activity = this.$activity;
                    a.g<l> gVar = b.i.a.f.k.a.a;
                    SafetyNetClient safetyNetClient = new SafetyNetClient(activity);
                    m.checkNotNullExpressionValue(safetyNetClient, "SafetyNet\n              …     .getClient(activity)");
                    captchaHelper.showCaptcha(safetyNetClient, new AnonymousClass1(), this.$errorHandler);
                }
            }

            public final void call(final Emitter<String> emitter) {
                final CaptchaHelper$tryShowCaptcha$1$errorHandler$1 captchaHelper$tryShowCaptcha$1$errorHandler$1 = new CaptchaHelper$tryShowCaptcha$1$errorHandler$1(emitter);
                CaptchaHelper.CaptchaRequest captchaRequest2 = CaptchaHelper.CaptchaRequest.this;
                if (captchaRequest2 instanceof CaptchaHelper.CaptchaRequest.HCaptcha) {
                    Activity activity = ((CaptchaHelper.CaptchaRequest.HCaptcha) captchaRequest2).getActivity();
                    HCaptchaConfig.a builder = HCaptchaConfig.builder();
                    String siteKey = ((CaptchaHelper.CaptchaRequest.HCaptcha) CaptchaHelper.CaptchaRequest.this).getSiteKey();
                    Objects.requireNonNull(builder);
                    Objects.requireNonNull(siteKey, "siteKey is marked non-null but is null");
                    builder.a = siteKey;
                    builder.n = StoreStream.Companion.getUserSettingsSystem().getLocale();
                    builder.m = true;
                    Boolean bool = builder.c;
                    if (!builder.f3113b) {
                        bool = Boolean.TRUE;
                    }
                    Boolean bool2 = bool;
                    Boolean bool3 = builder.e;
                    if (!builder.d) {
                        bool3 = Boolean.TRUE;
                    }
                    Boolean bool4 = bool3;
                    String str = builder.h;
                    if (!builder.g) {
                        str = HCaptchaConfig.$default$apiEndpoint();
                    }
                    String str2 = str;
                    String str3 = builder.n;
                    if (!builder.m) {
                        str3 = HCaptchaConfig.$default$locale();
                    }
                    String str4 = str3;
                    HCaptchaSize hCaptchaSize = builder.p;
                    if (!builder.o) {
                        hCaptchaSize = HCaptchaSize.INVISIBLE;
                    }
                    HCaptchaSize hCaptchaSize2 = hCaptchaSize;
                    HCaptchaTheme hCaptchaTheme = builder.r;
                    if (!builder.q) {
                        hCaptchaTheme = HCaptchaTheme.LIGHT;
                    }
                    HCaptchaConfig hCaptchaConfig = new HCaptchaConfig(builder.a, bool2, bool4, builder.f, str2, builder.i, builder.j, builder.k, builder.l, str4, hCaptchaSize2, hCaptchaTheme);
                    final b.j.a.a aVar = new b.j.a.a(activity);
                    HCaptchaDialogListener hCaptcha$1 = new HCaptchaDialogListener() { // from class: com.hcaptcha.sdk.HCaptcha$1
                        @Override // com.hcaptcha.sdk.HCaptchaDialogListener
                        public void a(HCaptchaException hCaptchaException) {
                            b.j.a.a aVar2 = b.j.a.a.this;
                            aVar2.f1893b = hCaptchaException;
                            aVar2.a();
                        }

                        /* JADX WARN: Multi-variable type inference failed */
                        @Override // com.hcaptcha.sdk.HCaptchaDialogListener
                        public void b(HCaptchaTokenResponse hCaptchaTokenResponse) {
                            b.j.a.a aVar2 = b.j.a.a.this;
                            aVar2.a = hCaptchaTokenResponse;
                            aVar2.a();
                        }
                    };
                    String str5 = b.j.a.c.j;
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("hCaptchaConfig", hCaptchaConfig);
                    bundle.putParcelable("hCaptchaDialogListener", hCaptcha$1);
                    b.j.a.c cVar = new b.j.a.c();
                    cVar.setArguments(bundle);
                    cVar.show(aVar.e, b.j.a.c.j);
                    aVar.c.add(new b.j.a.f.c<HCaptchaTokenResponse>() { // from class: com.discord.utilities.captcha.CaptchaHelper$tryShowCaptcha$1.1
                        public final void onSuccess(HCaptchaTokenResponse hCaptchaTokenResponse) {
                            Emitter emitter2 = Emitter.this;
                            m.checkNotNullExpressionValue(hCaptchaTokenResponse, "hCaptchaTokenResponse");
                            emitter2.onNext(hCaptchaTokenResponse.a);
                            Emitter.this.onCompleted();
                        }
                    });
                    aVar.a();
                    aVar.d.add(new b.j.a.f.a() { // from class: com.discord.utilities.captcha.CaptchaHelper$tryShowCaptcha$1.2
                        @Override // b.j.a.f.a
                        public final void onFailure(HCaptchaException hCaptchaException) {
                            m.checkNotNullExpressionValue(hCaptchaException, "hCaptchaException");
                            if (hCaptchaException.a() == HCaptchaError.CHALLENGE_CLOSED) {
                                Emitter.this.onCompleted();
                                return;
                            }
                            String name = hCaptchaException.a().name();
                            Locale locale = Locale.ROOT;
                            m.checkNotNullExpressionValue(locale, "Locale.ROOT");
                            Objects.requireNonNull(name, "null cannot be cast to non-null type java.lang.String");
                            String lowerCase = name.toLowerCase(locale);
                            m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                            captchaHelper$tryShowCaptcha$1$errorHandler$1.invoke(new CaptchaHelper.Failure(R.string.captcha_failed, lowerCase));
                        }
                    });
                    aVar.a();
                } else if (captchaRequest2 instanceof CaptchaHelper.CaptchaRequest.ReCaptcha) {
                    Activity activity2 = ((CaptchaHelper.CaptchaRequest.ReCaptcha) captchaRequest2).getActivity();
                    CaptchaHelper captchaHelper = CaptchaHelper.INSTANCE;
                    Object obj = GoogleApiAvailability.c;
                    GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.d;
                    m.checkNotNullExpressionValue(googleApiAvailability, "GoogleApiAvailability\n              .getInstance()");
                    captchaHelper.ensurePlayServicesAvailable(googleApiAvailability, activity2, new AnonymousClass3(activity2, emitter, captchaHelper$tryShowCaptcha$1$errorHandler$1), captchaHelper$tryShowCaptcha$1$errorHandler$1);
                }
            }
        }, Emitter.BackpressureMode.BUFFER);
        m.checkNotNullExpressionValue(n, "Observable.create({ emit….BackpressureMode.BUFFER)");
        return n;
    }
}
