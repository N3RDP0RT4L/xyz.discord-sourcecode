package com.discord.utilities.captcha;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import b.i.a.f.e.o.f;
import b.i.d.c;
import b.i.d.e;
import com.discord.app.AppLog;
import com.discord.utilities.error.Error;
import com.discord.utilities.logging.Logger;
import d0.z.d.m;
import java.io.Serializable;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: CaptchaErrorBody.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u0000 !2\u00020\u0001:\u0001!B+\u0012\u000e\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0002\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\r\u001a\u0004\u0018\u00010\b¢\u0006\u0004\b\u001f\u0010 J\u0018\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0003HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ:\u0010\u000e\u001a\u00020\u00002\u0010\b\u0002\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00022\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0007J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u001a\u0010\u0017\u001a\u00020\u00162\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R!\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0019\u001a\u0004\b\u001a\u0010\u0005R\u001b\u0010\f\u001a\u0004\u0018\u00010\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001b\u001a\u0004\b\u001c\u0010\u0007R\u001b\u0010\r\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001d\u001a\u0004\b\u001e\u0010\n¨\u0006\""}, d2 = {"Lcom/discord/utilities/captcha/CaptchaErrorBody;", "Ljava/io/Serializable;", "", "", "component1", "()Ljava/util/List;", "component2", "()Ljava/lang/String;", "Lcom/discord/utilities/captcha/CaptchaService;", "component3", "()Lcom/discord/utilities/captcha/CaptchaService;", "captchaKey", "captchaSitekey", "captchaService", "copy", "(Ljava/util/List;Ljava/lang/String;Lcom/discord/utilities/captcha/CaptchaService;)Lcom/discord/utilities/captcha/CaptchaErrorBody;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getCaptchaKey", "Ljava/lang/String;", "getCaptchaSitekey", "Lcom/discord/utilities/captcha/CaptchaService;", "getCaptchaService", HookHelper.constructorName, "(Ljava/util/List;Ljava/lang/String;Lcom/discord/utilities/captcha/CaptchaService;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class CaptchaErrorBody implements Serializable {
    public static final Companion Companion = new Companion(null);
    private final List<String> captchaKey;
    private final CaptchaService captchaService;
    private final String captchaSitekey;

    /* compiled from: CaptchaErrorBody.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u0004\u0018\u00010\u00042\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\t\u0010\n¨\u0006\r"}, d2 = {"Lcom/discord/utilities/captcha/CaptchaErrorBody$Companion;", "", "Lcom/discord/utilities/error/Error;", "error", "Lcom/discord/utilities/captcha/CaptchaErrorBody;", "createFromError", "(Lcom/discord/utilities/error/Error;)Lcom/discord/utilities/captcha/CaptchaErrorBody;", "", "errorString", "createFromString", "(Ljava/lang/String;)Lcom/discord/utilities/captcha/CaptchaErrorBody;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public final CaptchaErrorBody createFromError(Error error) {
            m.checkNotNullParameter(error, "error");
            String bodyText = error.getBodyText();
            if (bodyText == null) {
                return null;
            }
            e eVar = new e();
            eVar.c = c.m;
            try {
                return (CaptchaErrorBody) f.E1(CaptchaErrorBody.class).cast(eVar.a().g(bodyText, CaptchaErrorBody.class));
            } catch (Exception e) {
                Logger.e$default(AppLog.g, "failed to parse captcha error body", e, null, 4, null);
                return null;
            }
        }

        public final CaptchaErrorBody createFromString(String str) {
            m.checkNotNullParameter(str, "errorString");
            e eVar = new e();
            eVar.c = c.m;
            try {
                return (CaptchaErrorBody) f.E1(CaptchaErrorBody.class).cast(eVar.a().g(str, CaptchaErrorBody.class));
            } catch (Exception e) {
                Logger.e$default(AppLog.g, "failed to parse captcha error body", e, null, 4, null);
                return null;
            }
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public CaptchaErrorBody(List<String> list, String str, CaptchaService captchaService) {
        this.captchaKey = list;
        this.captchaSitekey = str;
        this.captchaService = captchaService;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ CaptchaErrorBody copy$default(CaptchaErrorBody captchaErrorBody, List list, String str, CaptchaService captchaService, int i, Object obj) {
        if ((i & 1) != 0) {
            list = captchaErrorBody.captchaKey;
        }
        if ((i & 2) != 0) {
            str = captchaErrorBody.captchaSitekey;
        }
        if ((i & 4) != 0) {
            captchaService = captchaErrorBody.captchaService;
        }
        return captchaErrorBody.copy(list, str, captchaService);
    }

    public final List<String> component1() {
        return this.captchaKey;
    }

    public final String component2() {
        return this.captchaSitekey;
    }

    public final CaptchaService component3() {
        return this.captchaService;
    }

    public final CaptchaErrorBody copy(List<String> list, String str, CaptchaService captchaService) {
        return new CaptchaErrorBody(list, str, captchaService);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CaptchaErrorBody)) {
            return false;
        }
        CaptchaErrorBody captchaErrorBody = (CaptchaErrorBody) obj;
        return m.areEqual(this.captchaKey, captchaErrorBody.captchaKey) && m.areEqual(this.captchaSitekey, captchaErrorBody.captchaSitekey) && m.areEqual(this.captchaService, captchaErrorBody.captchaService);
    }

    public final List<String> getCaptchaKey() {
        return this.captchaKey;
    }

    public final CaptchaService getCaptchaService() {
        return this.captchaService;
    }

    public final String getCaptchaSitekey() {
        return this.captchaSitekey;
    }

    public int hashCode() {
        List<String> list = this.captchaKey;
        int i = 0;
        int hashCode = (list != null ? list.hashCode() : 0) * 31;
        String str = this.captchaSitekey;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        CaptchaService captchaService = this.captchaService;
        if (captchaService != null) {
            i = captchaService.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("CaptchaErrorBody(captchaKey=");
        R.append(this.captchaKey);
        R.append(", captchaSitekey=");
        R.append(this.captchaSitekey);
        R.append(", captchaService=");
        R.append(this.captchaService);
        R.append(")");
        return R.toString();
    }
}
