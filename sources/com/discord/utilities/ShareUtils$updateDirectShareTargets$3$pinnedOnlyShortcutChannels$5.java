package com.discord.utilities;

import com.discord.api.channel.Channel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: ShareUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/channel/Channel;", "channel", "Lcom/discord/utilities/ChannelShortcutInfo;", "invoke", "(Lcom/discord/api/channel/Channel;)Lcom/discord/utilities/ChannelShortcutInfo;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ShareUtils$updateDirectShareTargets$3$pinnedOnlyShortcutChannels$5 extends o implements Function1<Channel, ChannelShortcutInfo> {
    public static final ShareUtils$updateDirectShareTargets$3$pinnedOnlyShortcutChannels$5 INSTANCE = new ShareUtils$updateDirectShareTargets$3$pinnedOnlyShortcutChannels$5();

    public ShareUtils$updateDirectShareTargets$3$pinnedOnlyShortcutChannels$5() {
        super(1);
    }

    public final ChannelShortcutInfo invoke(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        return new ChannelShortcutInfo(channel, 20, true);
    }
}
