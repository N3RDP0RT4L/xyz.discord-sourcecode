package com.discord.utilities.apng;

import android.widget.ImageView;
import b.i.a.f.e.o.f;
import b.l.a.a;
import d0.l;
import d0.w.h.c;
import d0.w.i.a.e;
import d0.w.i.a.k;
import d0.y.b;
import d0.z.d.m;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.lang.ref.WeakReference;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlinx.coroutines.CoroutineDispatcher;
import kotlinx.coroutines.CoroutineScope;
import s.a.a.n;
import s.a.k0;
import s.a.l1;
/* compiled from: ApngUtils.kt */
@e(c = "com.discord.utilities.apng.ApngUtils$renderApngFromFile$3", f = "ApngUtils.kt", l = {31}, m = "invokeSuspend")
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\u008a@¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lkotlinx/coroutines/CoroutineScope;", "", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ApngUtils$renderApngFromFile$3 extends k implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
    public final /* synthetic */ boolean $autoPlay;
    public final /* synthetic */ File $file;
    public final /* synthetic */ Ref$ObjectRef $imageViewRef;
    public final /* synthetic */ Integer $maxHeight;
    public final /* synthetic */ Integer $maxWidth;
    public int label;

    /* compiled from: ApngUtils.kt */
    @e(c = "com.discord.utilities.apng.ApngUtils$renderApngFromFile$3$1", f = "ApngUtils.kt", l = {}, m = "invokeSuspend")
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\u008a@¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lkotlinx/coroutines/CoroutineScope;", "", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.utilities.apng.ApngUtils$renderApngFromFile$3$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends k implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
        public final /* synthetic */ Ref$ObjectRef $drawable;
        public int label;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Ref$ObjectRef ref$ObjectRef, Continuation continuation) {
            super(2, continuation);
            this.$drawable = ref$ObjectRef;
        }

        @Override // d0.w.i.a.a
        public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
            m.checkNotNullParameter(continuation, "completion");
            return new AnonymousClass1(this.$drawable, continuation);
        }

        @Override // kotlin.jvm.functions.Function2
        public final Object invoke(CoroutineScope coroutineScope, Continuation<? super Unit> continuation) {
            return ((AnonymousClass1) create(coroutineScope, continuation)).invokeSuspend(Unit.a);
        }

        @Override // d0.w.i.a.a
        public final Object invokeSuspend(Object obj) {
            c.getCOROUTINE_SUSPENDED();
            if (this.label == 0) {
                l.throwOnFailure(obj);
                ImageView imageView = (ImageView) ((WeakReference) ApngUtils$renderApngFromFile$3.this.$imageViewRef.element).get();
                if (imageView == null) {
                    return Unit.a;
                }
                m.checkNotNullExpressionValue(imageView, "imageViewRef.get() ?: return@withContext");
                imageView.setImageDrawable((a) this.$drawable.element);
                if (ApngUtils$renderApngFromFile$3.this.$autoPlay) {
                    ApngUtils.INSTANCE.playApngAnimation((a) this.$drawable.element);
                }
                return Unit.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ApngUtils$renderApngFromFile$3(File file, Integer num, Integer num2, Ref$ObjectRef ref$ObjectRef, boolean z2, Continuation continuation) {
        super(2, continuation);
        this.$file = file;
        this.$maxHeight = num;
        this.$maxWidth = num2;
        this.$imageViewRef = ref$ObjectRef;
        this.$autoPlay = z2;
    }

    @Override // d0.w.i.a.a
    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        m.checkNotNullParameter(continuation, "completion");
        return new ApngUtils$renderApngFromFile$3(this.$file, this.$maxHeight, this.$maxWidth, this.$imageViewRef, this.$autoPlay, continuation);
    }

    @Override // kotlin.jvm.functions.Function2
    public final Object invoke(CoroutineScope coroutineScope, Continuation<? super Unit> continuation) {
        return ((ApngUtils$renderApngFromFile$3) create(coroutineScope, continuation)).invokeSuspend(Unit.a);
    }

    /* JADX WARN: Type inference failed for: r1v3, types: [T, b.l.a.a] */
    @Override // d0.w.i.a.a
    public final Object invokeSuspend(Object obj) {
        Object coroutine_suspended = c.getCOROUTINE_SUSPENDED();
        int i = this.label;
        try {
            if (i == 0) {
                l.throwOnFailure(obj);
                Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
                File file = this.$file;
                Integer num = this.$maxHeight;
                Integer num2 = this.$maxWidth;
                m.checkNotNullParameter(file, "file");
                FileInputStream fileInputStream = new FileInputStream(file);
                BufferedInputStream bufferedInputStream = fileInputStream instanceof BufferedInputStream ? (BufferedInputStream) fileInputStream : new BufferedInputStream(fileInputStream, 8192);
                ?? a = a.a(bufferedInputStream, num2, num);
                b.closeFinally(bufferedInputStream, null);
                ref$ObjectRef.element = a;
                CoroutineDispatcher coroutineDispatcher = k0.a;
                l1 l1Var = n.f3802b;
                AnonymousClass1 r4 = new AnonymousClass1(ref$ObjectRef, null);
                this.label = 1;
                if (f.C1(l1Var, r4, this) == coroutine_suspended) {
                    return coroutine_suspended;
                }
            } else if (i == 1) {
                l.throwOnFailure(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Unit.a;
    }
}
