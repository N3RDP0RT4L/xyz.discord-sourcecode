package com.discord.utilities.apng;

import andhook.lib.HookHelper;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import b.i.a.f.e.o.f;
import b.l.a.a;
import d0.z.d.m;
import java.io.File;
import java.lang.ref.WeakReference;
import kotlin.Metadata;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlinx.coroutines.Job;
import s.a.k0;
import s.a.x0;
/* compiled from: ApngUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0014\u0010\u0015J?\u0010\f\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010\n\u001a\u00020\t¢\u0006\u0004\b\f\u0010\rJ\u0017\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000e¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0013\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000e¢\u0006\u0004\b\u0013\u0010\u0012¨\u0006\u0016"}, d2 = {"Lcom/discord/utilities/apng/ApngUtils;", "", "Ljava/io/File;", "file", "Landroid/widget/ImageView;", "imageView", "", "maxWidth", "maxHeight", "", "autoPlay", "Lkotlinx/coroutines/Job;", "renderApngFromFile", "(Ljava/io/File;Landroid/widget/ImageView;Ljava/lang/Integer;Ljava/lang/Integer;Z)Lkotlinx/coroutines/Job;", "Landroid/graphics/drawable/Drawable;", "drawable", "", "playApngAnimation", "(Landroid/graphics/drawable/Drawable;)V", "pauseApngAnimation", HookHelper.constructorName, "()V", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ApngUtils {
    public static final ApngUtils INSTANCE = new ApngUtils();

    private ApngUtils() {
    }

    public final void pauseApngAnimation(Drawable drawable) {
        if (drawable instanceof a) {
            ((a) drawable).stop();
        }
    }

    public final void playApngAnimation(Drawable drawable) {
        if (drawable instanceof a) {
            ((a) drawable).start();
        }
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [T, java.lang.ref.WeakReference] */
    public final Job renderApngFromFile(File file, ImageView imageView, Integer num, Integer num2, boolean z2) {
        m.checkNotNullParameter(file, "file");
        m.checkNotNullParameter(imageView, "imageView");
        Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        ref$ObjectRef.element = new WeakReference(imageView);
        if (num2 != null) {
            int intValue = num2.intValue();
            ImageView imageView2 = (ImageView) ((WeakReference) ref$ObjectRef.element).get();
            if (imageView2 != null) {
                imageView2.setMaxHeight(intValue);
            }
        }
        if (num != null) {
            int intValue2 = num.intValue();
            ImageView imageView3 = (ImageView) ((WeakReference) ref$ObjectRef.element).get();
            if (imageView3 != null) {
                imageView3.setMaxWidth(intValue2);
            }
        }
        return f.H0(x0.j, k0.f3814b, null, new ApngUtils$renderApngFromFile$3(file, num2, num, ref$ObjectRef, z2, null), 2, null);
    }
}
