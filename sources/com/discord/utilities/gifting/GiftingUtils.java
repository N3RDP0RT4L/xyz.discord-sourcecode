package com.discord.utilities.gifting;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.appcompat.widget.ActivityChooserModel;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.SkuDetails;
import com.discord.api.premium.PremiumTier;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelSkuKt;
import com.discord.stores.StoreGooglePlayPurchases;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.billing.GooglePlayInAppSku;
import com.discord.utilities.billing.GooglePlayInAppSkuKt;
import com.discord.utilities.premium.PremiumUtils;
import com.discord.utilities.resources.DurationUnit;
import com.discord.utilities.resources.DurationUtilsKt;
import com.discord.utilities.time.TimeUtils;
import com.discord.utilities.user.UserUtils;
import com.discord.widgets.notice.WidgetNoticeDialog;
import d0.o;
import d0.t.h0;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import xyz.discord.R;
/* compiled from: GiftingUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\t\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001$B\t\b\u0002¢\u0006\u0004\b\"\u0010#J\u0015\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u001d\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\b¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u0010\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b\u0010\u0010\u0011J\u001f\u0010\u0015\u001a\u0004\u0018\u00010\u00142\u000e\u0010\u0013\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0012¢\u0006\u0004\b\u0015\u0010\u0016J7\u0010\u001e\u001a\u00020\u001c2\u0006\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u001a\u001a\u00020\u00192\u0010\b\u0002\u0010\u001d\u001a\n\u0012\u0004\u0012\u00020\u001c\u0018\u00010\u001b¢\u0006\u0004\b\u001e\u0010\u001fR\u0016\u0010 \u001a\u00020\u00068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b \u0010!¨\u0006%"}, d2 = {"Lcom/discord/utilities/gifting/GiftingUtils;", "", "", "giftCode", "generateGiftUrl", "(Ljava/lang/String;)Ljava/lang/String;", "", "millis", "Landroid/content/Context;", "context", "", "getTimeString", "(JLandroid/content/Context;)Ljava/lang/CharSequence;", "Lcom/discord/utilities/billing/GooglePlayInAppSku;", "inAppSku", "", "getIconForSku", "(Lcom/discord/utilities/billing/GooglePlayInAppSku;)Ljava/lang/Integer;", "Lcom/discord/primitives/SkuId;", "skuId", "Lcom/discord/api/premium/PremiumTier;", "getTierForSku", "(Ljava/lang/Long;)Lcom/discord/api/premium/PremiumTier;", "Landroidx/fragment/app/FragmentActivity;", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "Lcom/discord/utilities/analytics/Traits$Location;", ModelAuditLogEntry.CHANGE_KEY_LOCATION, "Lkotlin/Function0;", "", "dismissWarningCallback", "buyGift", "(Landroidx/fragment/app/FragmentActivity;Lcom/discord/utilities/billing/GooglePlayInAppSku;Lcom/discord/utilities/analytics/Traits$Location;Lkotlin/jvm/functions/Function0;)V", "PREMIUM_SUBSCRIPTION_APPLICATION_ID", "J", HookHelper.constructorName, "()V", "SkuTypes", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GiftingUtils {
    public static final GiftingUtils INSTANCE = new GiftingUtils();
    public static final long PREMIUM_SUBSCRIPTION_APPLICATION_ID = 521842831262875670L;

    /* compiled from: GiftingUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\b\n\u0002\b\f\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\r¨\u0006\u000e"}, d2 = {"Lcom/discord/utilities/gifting/GiftingUtils$SkuTypes;", "", "", ModelAuditLogEntry.CHANGE_KEY_ID, "I", "getId", "()I", HookHelper.constructorName, "(Ljava/lang/String;II)V", "DURABLE_PRIMARY", "DURABLE", "CONSUMABLE", "BUNDLE", "SUBSCRIPTION", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum SkuTypes {
        DURABLE_PRIMARY(1),
        DURABLE(2),
        CONSUMABLE(3),
        BUNDLE(4),
        SUBSCRIPTION(5);
        

        /* renamed from: id  reason: collision with root package name */
        private final int f2791id;

        SkuTypes(int i) {
            this.f2791id = i;
        }

        public final int getId() {
            return this.f2791id;
        }
    }

    private GiftingUtils() {
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void buyGift$default(GiftingUtils giftingUtils, FragmentActivity fragmentActivity, GooglePlayInAppSku googlePlayInAppSku, Traits.Location location, Function0 function0, int i, Object obj) throws Exception {
        if ((i & 8) != 0) {
            function0 = null;
        }
        giftingUtils.buyGift(fragmentActivity, googlePlayInAppSku, location, function0);
    }

    public final void buyGift(FragmentActivity fragmentActivity, GooglePlayInAppSku googlePlayInAppSku, Traits.Location location, Function0<Unit> function0) throws Exception {
        long j;
        m.checkNotNullParameter(fragmentActivity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
        m.checkNotNullParameter(googlePlayInAppSku, "inAppSku");
        m.checkNotNullParameter(location, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        SkuDetails skuDetails = googlePlayInAppSku.getSkuDetails();
        if (skuDetails != null) {
            UserUtils userUtils = UserUtils.INSTANCE;
            StoreStream.Companion companion = StoreStream.Companion;
            String obfuscatedUserId = userUtils.getObfuscatedUserId(Long.valueOf(companion.getUsers().getMe().getId()));
            if (obfuscatedUserId != null) {
                BillingFlowParams.a aVar = new BillingFlowParams.a();
                ArrayList<SkuDetails> arrayList = new ArrayList<>();
                arrayList.add(skuDetails);
                aVar.d = arrayList;
                aVar.a = obfuscatedUserId;
                BillingFlowParams a = aVar.a();
                m.checkNotNullExpressionValue(a, "BillingFlowParams\n      …dUserId)\n        .build()");
                Long skuId = googlePlayInAppSku.getSkuId();
                if (skuId != null) {
                    long longValue = skuId.longValue();
                    StoreGooglePlayPurchases googlePlayPurchases = companion.getGooglePlayPurchases();
                    String paymentGatewaySkuId = googlePlayInAppSku.getPaymentGatewaySkuId();
                    int id2 = SkuTypes.SUBSCRIPTION.getId();
                    String a2 = skuDetails.a();
                    m.checkNotNullExpressionValue(a2, "skuDetails.description");
                    Traits.StoreSku storeSku = new Traits.StoreSku(longValue, id2, 521842831262875670L, a2);
                    PremiumUtils premiumUtils = PremiumUtils.INSTANCE;
                    int microAmountToMinor = premiumUtils.microAmountToMinor(skuDetails.c());
                    if (skuDetails.f2002b.has("original_price_micros")) {
                        j = skuDetails.f2002b.optLong("original_price_micros");
                    } else {
                        j = skuDetails.c();
                    }
                    int microAmountToMinor2 = premiumUtils.microAmountToMinor(j);
                    String optString = skuDetails.f2002b.optString("price_currency_code");
                    m.checkNotNullExpressionValue(optString, "skuDetails.priceCurrencyCode");
                    Locale locale = Locale.ROOT;
                    m.checkNotNullExpressionValue(locale, "Locale.ROOT");
                    Objects.requireNonNull(optString, "null cannot be cast to non-null type java.lang.String");
                    String lowerCase = optString.toLowerCase(locale);
                    m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                    googlePlayPurchases.trackPaymentFlowStarted(paymentGatewaySkuId, longValue, location, storeSku, new Traits.Payment(Traits.Payment.Type.SUBSCRIPTION, microAmountToMinor, microAmountToMinor2, lowerCase, true));
                    GiftingUtils$buyGift$1 giftingUtils$buyGift$1 = new GiftingUtils$buyGift$1(fragmentActivity, a);
                    if (companion.getGooglePlayPurchases().hasSeenGiftingWarning()) {
                        giftingUtils$buyGift$1.invoke2();
                        return;
                    }
                    WidgetNoticeDialog.Companion companion2 = WidgetNoticeDialog.Companion;
                    FragmentManager supportFragmentManager = fragmentActivity.getSupportFragmentManager();
                    m.checkNotNullExpressionValue(supportFragmentManager, "activity.supportFragmentManager");
                    String string = fragmentActivity.getString(R.string.dialog_just_so_you_know);
                    String string2 = fragmentActivity.getString(R.string.gift_purchase_google_play_notice_extended);
                    m.checkNotNullExpressionValue(string2, "activity.getString(R.str…gle_play_notice_extended)");
                    WidgetNoticeDialog.Companion.show$default(companion2, supportFragmentManager, string, string2, fragmentActivity.getString(R.string.application_store_buy_gift), fragmentActivity.getString(R.string.nevermind), h0.mapOf(o.to(Integer.valueOf((int) R.id.OK_BUTTON), new GiftingUtils$buyGift$2(googlePlayInAppSku, giftingUtils$buyGift$1)), o.to(Integer.valueOf((int) R.id.CANCEL_BUTTON), new GiftingUtils$buyGift$3(function0))), null, null, null, null, null, null, 0, new GiftingUtils$buyGift$4(function0), 8128, null);
                    companion.getGooglePlayPurchases().markViewedGiftingWarning();
                    return;
                }
                throw new Exception("No skuId for " + googlePlayInAppSku);
            }
            throw new Exception("No current user ID");
        }
        throw new Exception("No skuDetails for " + googlePlayInAppSku);
    }

    public final String generateGiftUrl(String str) {
        m.checkNotNullParameter(str, "giftCode");
        return "https://discord.gift/" + str;
    }

    public final Integer getIconForSku(GooglePlayInAppSku googlePlayInAppSku) {
        m.checkNotNullParameter(googlePlayInAppSku, "inAppSku");
        if (m.areEqual(googlePlayInAppSku, GooglePlayInAppSkuKt.getPremiumTier1Month())) {
            return Integer.valueOf((int) R.drawable.ic_plan_premium_tier_1);
        }
        if (m.areEqual(googlePlayInAppSku, GooglePlayInAppSkuKt.getPremiumTier1Year())) {
            return Integer.valueOf((int) R.drawable.ic_plan_premium_tier_1_year);
        }
        if (m.areEqual(googlePlayInAppSku, GooglePlayInAppSkuKt.getPremiumTier2Month())) {
            return Integer.valueOf((int) R.drawable.ic_plan_premium_tier_2);
        }
        if (m.areEqual(googlePlayInAppSku, GooglePlayInAppSkuKt.getPremiumTier2Year())) {
            return Integer.valueOf((int) R.drawable.ic_plan_premium_tier_2_year);
        }
        return null;
    }

    public final PremiumTier getTierForSku(Long l) {
        if (l != null && l.longValue() == ModelSkuKt.PREMIUM_TIER_1_SKU_ID) {
            return PremiumTier.TIER_1;
        }
        if (l != null && l.longValue() == ModelSkuKt.PREMIUM_TIER_2_SKU_ID) {
            return PremiumTier.TIER_2;
        }
        return null;
    }

    public final CharSequence getTimeString(long j, Context context) {
        m.checkNotNullParameter(context, "context");
        TimeUtils timeUtils = TimeUtils.INSTANCE;
        int hoursFromMillis = timeUtils.getHoursFromMillis(j);
        if (hoursFromMillis > 0) {
            return DurationUtilsKt.formatDuration(context, DurationUnit.HOURS, hoursFromMillis);
        }
        return DurationUtilsKt.formatDuration(context, DurationUnit.MINS, timeUtils.getMinutesFromMillis(j));
    }
}
