package com.discord.utilities.gifting;

import androidx.fragment.app.FragmentActivity;
import com.android.billingclient.api.BillingFlowParams;
import com.discord.utilities.billing.GooglePlayBillingManager;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: GiftingUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "launchBillingFlow"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GiftingUtils$buyGift$1 extends o implements Function0<Unit> {
    public final /* synthetic */ FragmentActivity $activity;
    public final /* synthetic */ BillingFlowParams $billingParams;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GiftingUtils$buyGift$1(FragmentActivity fragmentActivity, BillingFlowParams billingFlowParams) {
        super(0);
        this.$activity = fragmentActivity;
        this.$billingParams = billingFlowParams;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        GooglePlayBillingManager googlePlayBillingManager = GooglePlayBillingManager.INSTANCE;
        if (googlePlayBillingManager.launchBillingFlow(this.$activity, this.$billingParams) == 7) {
            googlePlayBillingManager.queryPurchases();
        }
    }
}
