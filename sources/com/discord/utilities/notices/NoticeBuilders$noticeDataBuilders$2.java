package com.discord.utilities.notices;

import android.content.Context;
import com.discord.stores.StoreNotices;
import com.discord.utilities.notices.NoticeBuilders;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function2;
/* compiled from: NoticeBuilders.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Landroid/content/Context;", "p1", "Lcom/discord/stores/StoreNotices$Dialog;", "p2", "Lcom/discord/utilities/notices/NoticeBuilders$DialogData;", "invoke", "(Landroid/content/Context;Lcom/discord/stores/StoreNotices$Dialog;)Lcom/discord/utilities/notices/NoticeBuilders$DialogData;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class NoticeBuilders$noticeDataBuilders$2 extends k implements Function2<Context, StoreNotices.Dialog, NoticeBuilders.DialogData> {
    public NoticeBuilders$noticeDataBuilders$2(NoticeBuilders noticeBuilders) {
        super(2, noticeBuilders, NoticeBuilders.class, "deleteConnectionModalBuilder", "deleteConnectionModalBuilder(Landroid/content/Context;Lcom/discord/stores/StoreNotices$Dialog;)Lcom/discord/utilities/notices/NoticeBuilders$DialogData;", 0);
    }

    public final NoticeBuilders.DialogData invoke(Context context, StoreNotices.Dialog dialog) {
        NoticeBuilders.DialogData deleteConnectionModalBuilder;
        m.checkNotNullParameter(context, "p1");
        m.checkNotNullParameter(dialog, "p2");
        deleteConnectionModalBuilder = ((NoticeBuilders) this.receiver).deleteConnectionModalBuilder(context, dialog);
        return deleteConnectionModalBuilder;
    }
}
