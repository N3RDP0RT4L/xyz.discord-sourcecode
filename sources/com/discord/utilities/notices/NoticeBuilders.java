package com.discord.utilities.notices;

import andhook.lib.HookHelper;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import androidx.fragment.app.FragmentManager;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.stores.StoreNotices;
import com.discord.widgets.notice.WidgetNoticeDialog;
import com.discord.widgets.settings.connections.WidgetSettingsUserConnections;
import d0.o;
import d0.t.g0;
import d0.t.h0;
import d0.z.d.m;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: NoticeBuilders.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001\u0016B\t\b\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u001f\u0010\t\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0003¢\u0006\u0004\b\t\u0010\bJ%\u0010\r\u001a\u00020\f2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\r\u0010\u000eR6\u0010\u0012\u001a\"\u0012\u0004\u0012\u00020\u0010\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00110\u000f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013¨\u0006\u0017"}, d2 = {"Lcom/discord/utilities/notices/NoticeBuilders;", "", "Landroid/content/Context;", "context", "Lcom/discord/stores/StoreNotices$Dialog;", "notice", "Lcom/discord/utilities/notices/NoticeBuilders$DialogData;", "requestRatingModalBuilder", "(Landroid/content/Context;Lcom/discord/stores/StoreNotices$Dialog;)Lcom/discord/utilities/notices/NoticeBuilders$DialogData;", "deleteConnectionModalBuilder", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "showNotice", "(Landroid/content/Context;Landroidx/fragment/app/FragmentManager;Lcom/discord/stores/StoreNotices$Dialog;)V", "", "Lcom/discord/stores/StoreNotices$Dialog$Type;", "Lkotlin/Function2;", "noticeDataBuilders", "Ljava/util/Map;", HookHelper.constructorName, "()V", "DialogData", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class NoticeBuilders {
    public static final NoticeBuilders INSTANCE;
    private static final Map<StoreNotices.Dialog.Type, Function2<Context, StoreNotices.Dialog, DialogData>> noticeDataBuilders;

    /* compiled from: NoticeBuilders.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\r\n\u0002\b\u0005\n\u0002\u0010$\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\f\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u000e\b\u0082\b\u0018\u00002\u00020\u0001B_\u0012\u0006\u0010\u0011\u001a\u00020\u0002\u0012\u0006\u0010\u0012\u001a\u00020\u0002\u0012\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0002\u0012\"\b\u0002\u0010\u0015\u001a\u001c\u0012\u0004\u0012\u00020\t\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\f0\n\u0018\u00010\b\u0012\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\t¢\u0006\u0004\b+\u0010,J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0004J*\u0010\r\u001a\u001c\u0012\u0004\u0012\u00020\t\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\f0\n\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0012\u0010\u000f\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010Jl\u0010\u0017\u001a\u00020\u00002\b\b\u0002\u0010\u0011\u001a\u00020\u00022\b\b\u0002\u0010\u0012\u001a\u00020\u00022\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00022\"\b\u0002\u0010\u0015\u001a\u001c\u0012\u0004\u0012\u00020\t\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\f0\n\u0018\u00010\b2\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\tHÆ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u001a\u001a\u00020\u0019HÖ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u001c\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u001a\u0010 \u001a\u00020\u001f2\b\u0010\u001e\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b \u0010!R3\u0010\u0015\u001a\u001c\u0012\u0004\u0012\u00020\t\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\f0\n\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\"\u001a\u0004\b#\u0010\u000eR\u0019\u0010\u0012\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010$\u001a\u0004\b%\u0010\u0004R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010$\u001a\u0004\b&\u0010\u0004R\u0019\u0010\u0011\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010$\u001a\u0004\b'\u0010\u0004R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010(\u001a\u0004\b)\u0010\u0010R\u001b\u0010\u0014\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010$\u001a\u0004\b*\u0010\u0004¨\u0006-"}, d2 = {"Lcom/discord/utilities/notices/NoticeBuilders$DialogData;", "", "", "component1", "()Ljava/lang/CharSequence;", "component2", "component3", "component4", "", "", "Lkotlin/Function1;", "Landroid/view/View;", "", "component5", "()Ljava/util/Map;", "component6", "()Ljava/lang/Integer;", "headerText", "bodyText", "okText", "cancelText", "listenerMap", "extraLayoutId", "copy", "(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/util/Map;Ljava/lang/Integer;)Lcom/discord/utilities/notices/NoticeBuilders$DialogData;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getListenerMap", "Ljava/lang/CharSequence;", "getBodyText", "getOkText", "getHeaderText", "Ljava/lang/Integer;", "getExtraLayoutId", "getCancelText", HookHelper.constructorName, "(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/util/Map;Ljava/lang/Integer;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class DialogData {
        private final CharSequence bodyText;
        private final CharSequence cancelText;
        private final Integer extraLayoutId;
        private final CharSequence headerText;
        private final Map<Integer, Function1<View, Unit>> listenerMap;
        private final CharSequence okText;

        /* JADX WARN: Multi-variable type inference failed */
        public DialogData(CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, CharSequence charSequence4, Map<Integer, ? extends Function1<? super View, Unit>> map, Integer num) {
            m.checkNotNullParameter(charSequence, "headerText");
            m.checkNotNullParameter(charSequence2, "bodyText");
            this.headerText = charSequence;
            this.bodyText = charSequence2;
            this.okText = charSequence3;
            this.cancelText = charSequence4;
            this.listenerMap = map;
            this.extraLayoutId = num;
        }

        public static /* synthetic */ DialogData copy$default(DialogData dialogData, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, CharSequence charSequence4, Map map, Integer num, int i, Object obj) {
            if ((i & 1) != 0) {
                charSequence = dialogData.headerText;
            }
            if ((i & 2) != 0) {
                charSequence2 = dialogData.bodyText;
            }
            CharSequence charSequence5 = charSequence2;
            if ((i & 4) != 0) {
                charSequence3 = dialogData.okText;
            }
            CharSequence charSequence6 = charSequence3;
            if ((i & 8) != 0) {
                charSequence4 = dialogData.cancelText;
            }
            CharSequence charSequence7 = charSequence4;
            Map<Integer, Function1<View, Unit>> map2 = map;
            if ((i & 16) != 0) {
                map2 = dialogData.listenerMap;
            }
            Map map3 = map2;
            if ((i & 32) != 0) {
                num = dialogData.extraLayoutId;
            }
            return dialogData.copy(charSequence, charSequence5, charSequence6, charSequence7, map3, num);
        }

        public final CharSequence component1() {
            return this.headerText;
        }

        public final CharSequence component2() {
            return this.bodyText;
        }

        public final CharSequence component3() {
            return this.okText;
        }

        public final CharSequence component4() {
            return this.cancelText;
        }

        public final Map<Integer, Function1<View, Unit>> component5() {
            return this.listenerMap;
        }

        public final Integer component6() {
            return this.extraLayoutId;
        }

        public final DialogData copy(CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, CharSequence charSequence4, Map<Integer, ? extends Function1<? super View, Unit>> map, Integer num) {
            m.checkNotNullParameter(charSequence, "headerText");
            m.checkNotNullParameter(charSequence2, "bodyText");
            return new DialogData(charSequence, charSequence2, charSequence3, charSequence4, map, num);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof DialogData)) {
                return false;
            }
            DialogData dialogData = (DialogData) obj;
            return m.areEqual(this.headerText, dialogData.headerText) && m.areEqual(this.bodyText, dialogData.bodyText) && m.areEqual(this.okText, dialogData.okText) && m.areEqual(this.cancelText, dialogData.cancelText) && m.areEqual(this.listenerMap, dialogData.listenerMap) && m.areEqual(this.extraLayoutId, dialogData.extraLayoutId);
        }

        public final CharSequence getBodyText() {
            return this.bodyText;
        }

        public final CharSequence getCancelText() {
            return this.cancelText;
        }

        public final Integer getExtraLayoutId() {
            return this.extraLayoutId;
        }

        public final CharSequence getHeaderText() {
            return this.headerText;
        }

        public final Map<Integer, Function1<View, Unit>> getListenerMap() {
            return this.listenerMap;
        }

        public final CharSequence getOkText() {
            return this.okText;
        }

        public int hashCode() {
            CharSequence charSequence = this.headerText;
            int i = 0;
            int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
            CharSequence charSequence2 = this.bodyText;
            int hashCode2 = (hashCode + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
            CharSequence charSequence3 = this.okText;
            int hashCode3 = (hashCode2 + (charSequence3 != null ? charSequence3.hashCode() : 0)) * 31;
            CharSequence charSequence4 = this.cancelText;
            int hashCode4 = (hashCode3 + (charSequence4 != null ? charSequence4.hashCode() : 0)) * 31;
            Map<Integer, Function1<View, Unit>> map = this.listenerMap;
            int hashCode5 = (hashCode4 + (map != null ? map.hashCode() : 0)) * 31;
            Integer num = this.extraLayoutId;
            if (num != null) {
                i = num.hashCode();
            }
            return hashCode5 + i;
        }

        public String toString() {
            StringBuilder R = a.R("DialogData(headerText=");
            R.append(this.headerText);
            R.append(", bodyText=");
            R.append(this.bodyText);
            R.append(", okText=");
            R.append(this.okText);
            R.append(", cancelText=");
            R.append(this.cancelText);
            R.append(", listenerMap=");
            R.append(this.listenerMap);
            R.append(", extraLayoutId=");
            return a.E(R, this.extraLayoutId, ")");
        }

        public /* synthetic */ DialogData(CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, CharSequence charSequence4, Map map, Integer num, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(charSequence, charSequence2, (i & 4) != 0 ? null : charSequence3, (i & 8) != 0 ? null : charSequence4, (i & 16) != 0 ? null : map, (i & 32) != 0 ? null : num);
        }
    }

    static {
        NoticeBuilders noticeBuilders = new NoticeBuilders();
        INSTANCE = noticeBuilders;
        noticeDataBuilders = h0.mapOf(o.to(StoreNotices.Dialog.Type.REQUEST_RATING_MODAL, new NoticeBuilders$noticeDataBuilders$1(noticeBuilders)), o.to(StoreNotices.Dialog.Type.DELETE_CONNECTION_MODAL, new NoticeBuilders$noticeDataBuilders$2(noticeBuilders)));
    }

    private NoticeBuilders() {
    }

    /* JADX INFO: Access modifiers changed from: private */
    @SuppressLint({"DefaultLocale"})
    public final DialogData deleteConnectionModalBuilder(Context context, StoreNotices.Dialog dialog) {
        CharSequence b2;
        CharSequence b3;
        CharSequence b4;
        Object[] objArr = new Object[1];
        Map<String, Object> metadata = dialog.getMetadata();
        objArr[0] = metadata != null ? metadata.get(WidgetSettingsUserConnections.PLATFORM_TITLE) : null;
        CharSequence b5 = b.b(context, R.string.disconnect_account_title, objArr, NoticeBuilders$deleteConnectionModalBuilder$1.INSTANCE);
        b2 = b.b(context, R.string.disconnect_account_body, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        Map mapOf = g0.mapOf(o.to(Integer.valueOf((int) R.id.OK_BUTTON), new NoticeBuilders$deleteConnectionModalBuilder$2(dialog)));
        b3 = b.b(context, R.string.cancel, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        b4 = b.b(context, R.string.service_connections_disconnect, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        return new DialogData(b5, b2, b4, b3, mapOf, null, 32, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final DialogData requestRatingModalBuilder(Context context, StoreNotices.Dialog dialog) {
        CharSequence b2;
        CharSequence b3;
        CharSequence b4;
        CharSequence b5;
        b2 = b.b(context, R.string.rating_request_title, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        b3 = b.b(context, R.string.rating_request_body_android, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        Integer valueOf = Integer.valueOf((int) R.layout.view_review_request_modal_image);
        Map mapOf = h0.mapOf(o.to(0, NoticeBuilders$requestRatingModalBuilder$1.INSTANCE), o.to(Integer.valueOf((int) R.id.OK_BUTTON), NoticeBuilders$requestRatingModalBuilder$2.INSTANCE), o.to(Integer.valueOf((int) R.id.CANCEL_BUTTON), NoticeBuilders$requestRatingModalBuilder$3.INSTANCE));
        b4 = b.b(context, R.string.okay, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        b5 = b.b(context, R.string.no_thanks, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        return new DialogData(b2, b3, b4, b5, mapOf, valueOf);
    }

    public final void showNotice(Context context, FragmentManager fragmentManager, StoreNotices.Dialog dialog) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        m.checkNotNullParameter(dialog, "notice");
        Function2<Context, StoreNotices.Dialog, DialogData> function2 = noticeDataBuilders.get(dialog.getType());
        DialogData invoke = function2 != null ? function2.invoke(context, dialog) : null;
        if (invoke != null) {
            WidgetNoticeDialog.Companion.show$default(WidgetNoticeDialog.Companion, fragmentManager, invoke.getHeaderText(), invoke.getBodyText(), invoke.getOkText(), invoke.getCancelText(), invoke.getListenerMap(), dialog.getType(), invoke.getExtraLayoutId(), null, null, null, null, 0, null, 16128, null);
        }
    }
}
