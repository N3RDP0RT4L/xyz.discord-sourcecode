package com.discord.utilities.collections;

import androidx.exifinterface.media.ExifInterface;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: ShallowPartitionMap.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u0003\"\u0004\b\u0000\u0010\u0000\"\u0004\b\u0001\u0010\u0000\"\u0004\b\u0002\u0010\u00012\u0006\u0010\u0002\u001a\u00028\u0001H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"K", ExifInterface.GPS_MEASUREMENT_INTERRUPTED, "key", "", "invoke", "(Ljava/lang/Object;)I", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ShallowPartitionMap$Companion$getHashCodePartitionStrategy$1 extends o implements Function1<K, Integer> {
    public final /* synthetic */ int $numPartitions;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ShallowPartitionMap$Companion$getHashCodePartitionStrategy$1(int i) {
        super(1);
        this.$numPartitions = i;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    /* JADX WARN: Type inference failed for: r2v4, types: [int, java.lang.Integer] */
    @Override // kotlin.jvm.functions.Function1
    public final Integer invoke(K k) {
        int hashCode = k != 0 ? k.hashCode() : 0;
        return Math.abs(hashCode ^ (hashCode >>> 16)) % this.$numPartitions;
    }
}
