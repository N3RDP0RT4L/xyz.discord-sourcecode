package com.discord.utilities.collections;

import a0.a.a.b;
import andhook.lib.HookHelper;
import androidx.annotation.VisibleForTesting;
import b.d.b.a.a;
import com.google.android.material.shadow.ShadowDrawableWrapper;
import d0.t.n;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import kotlin.Metadata;
import kotlin.collections.ArrayDeque;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: Histogram.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u00002\u00020\u0001:\u0001\u001dB\u0019\u0012\u0006\u0010\u000e\u001a\u00020\r\u0012\b\b\u0002\u0010\u0017\u001a\u00020\r¢\u0006\u0004\b\u001b\u0010\u001cJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0002H\u0001¢\u0006\u0004\b\u0007\u0010\bJ\r\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\u000b\u0010\fR\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0010\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0012\u001a\u00020\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u000fR\"\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00020\u00138\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u0016\u0010\u0016\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0016\u0010\u0011R\u0016\u0010\u0017\u001a\u00020\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u000fR\u0018\u0010\u0018\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R\u0018\u0010\u001a\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001a\u0010\u0019¨\u0006\u001e"}, d2 = {"Lcom/discord/utilities/collections/Histogram;", "", "", "value", "", "addSample", "(J)V", "roundValue$utils_release", "(J)J", "roundValue", "Lcom/discord/utilities/collections/Histogram$Report;", "getReport", "()Lcom/discord/utilities/collections/Histogram$Report;", "", "precision", "I", "total", "J", "halfPrecision", "Ljava/util/TreeMap;", "histogram", "Ljava/util/TreeMap;", "count", "bucketLimit", "max", "Ljava/lang/Long;", "min", HookHelper.constructorName, "(II)V", "Report", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class Histogram {
    private final int bucketLimit;
    private long count;
    private final int halfPrecision;
    private final TreeMap<Long, Long> histogram;
    private Long max;
    private Long min;
    private final int precision;
    private long total;

    /* compiled from: Histogram.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u0006\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0013\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0012\b\u0086\b\u0018\u00002\u00020\u0001BW\u0012\u0006\u0010\u0012\u001a\u00020\u0002\u0012\u0006\u0010\u0013\u001a\u00020\u0002\u0012\u0006\u0010\u0014\u001a\u00020\u0006\u0012\u0006\u0010\u0015\u001a\u00020\u0002\u0012\u0006\u0010\u0016\u001a\u00020\n\u0012\u0006\u0010\u0017\u001a\u00020\u0002\u0012\u0006\u0010\u0018\u001a\u00020\u0002\u0012\u0006\u0010\u0019\u001a\u00020\u0002\u0012\u0006\u0010\u001a\u001a\u00020\u0002\u0012\u0006\u0010\u001b\u001a\u00020\u0002¢\u0006\u0004\b3\u00104J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\t\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\u0004J\u0010\u0010\u000b\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\r\u0010\u0004J\u0010\u0010\u000e\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u000e\u0010\u0004J\u0010\u0010\u000f\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0004J\u0010\u0010\u0010\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0004J\u0010\u0010\u0011\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0004Jt\u0010\u001c\u001a\u00020\u00002\b\b\u0002\u0010\u0012\u001a\u00020\u00022\b\b\u0002\u0010\u0013\u001a\u00020\u00022\b\b\u0002\u0010\u0014\u001a\u00020\u00062\b\b\u0002\u0010\u0015\u001a\u00020\u00022\b\b\u0002\u0010\u0016\u001a\u00020\n2\b\b\u0002\u0010\u0017\u001a\u00020\u00022\b\b\u0002\u0010\u0018\u001a\u00020\u00022\b\b\u0002\u0010\u0019\u001a\u00020\u00022\b\b\u0002\u0010\u001a\u001a\u00020\u00022\b\b\u0002\u0010\u001b\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001f\u001a\u00020\u001eHÖ\u0001¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010!\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b!\u0010\fJ\u001a\u0010$\u001a\u00020#2\b\u0010\"\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b$\u0010%R\u0019\u0010\u0018\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010&\u001a\u0004\b'\u0010\u0004R\u0019\u0010\u001b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010&\u001a\u0004\b(\u0010\u0004R\u0019\u0010\u0019\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010&\u001a\u0004\b)\u0010\u0004R\u0019\u0010\u0016\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010*\u001a\u0004\b+\u0010\fR\u0019\u0010\u0013\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010&\u001a\u0004\b,\u0010\u0004R\u0019\u0010\u001a\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010&\u001a\u0004\b-\u0010\u0004R\u0019\u0010\u0015\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010&\u001a\u0004\b.\u0010\u0004R\u0019\u0010\u0017\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010&\u001a\u0004\b/\u0010\u0004R\u0019\u0010\u0012\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010&\u001a\u0004\b0\u0010\u0004R\u0019\u0010\u0014\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u00101\u001a\u0004\b2\u0010\b¨\u00065"}, d2 = {"Lcom/discord/utilities/collections/Histogram$Report;", "", "", "component1", "()J", "component2", "", "component3", "()D", "component4", "", "component5", "()I", "component6", "component7", "component8", "component9", "component10", "min", "max", "average", "count", "bucketCount", "percentile25", "percentile50", "percentile75", "percentile90", "percentile95", "copy", "(JJDJIJJJJJ)Lcom/discord/utilities/collections/Histogram$Report;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getPercentile50", "getPercentile95", "getPercentile75", "I", "getBucketCount", "getMax", "getPercentile90", "getCount", "getPercentile25", "getMin", "D", "getAverage", HookHelper.constructorName, "(JJDJIJJJJJ)V", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Report {
        private final double average;
        private final int bucketCount;
        private final long count;
        private final long max;
        private final long min;
        private final long percentile25;
        private final long percentile50;
        private final long percentile75;
        private final long percentile90;
        private final long percentile95;

        public Report(long j, long j2, double d, long j3, int i, long j4, long j5, long j6, long j7, long j8) {
            this.min = j;
            this.max = j2;
            this.average = d;
            this.count = j3;
            this.bucketCount = i;
            this.percentile25 = j4;
            this.percentile50 = j5;
            this.percentile75 = j6;
            this.percentile90 = j7;
            this.percentile95 = j8;
        }

        public final long component1() {
            return this.min;
        }

        public final long component10() {
            return this.percentile95;
        }

        public final long component2() {
            return this.max;
        }

        public final double component3() {
            return this.average;
        }

        public final long component4() {
            return this.count;
        }

        public final int component5() {
            return this.bucketCount;
        }

        public final long component6() {
            return this.percentile25;
        }

        public final long component7() {
            return this.percentile50;
        }

        public final long component8() {
            return this.percentile75;
        }

        public final long component9() {
            return this.percentile90;
        }

        public final Report copy(long j, long j2, double d, long j3, int i, long j4, long j5, long j6, long j7, long j8) {
            return new Report(j, j2, d, j3, i, j4, j5, j6, j7, j8);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Report)) {
                return false;
            }
            Report report = (Report) obj;
            return this.min == report.min && this.max == report.max && Double.compare(this.average, report.average) == 0 && this.count == report.count && this.bucketCount == report.bucketCount && this.percentile25 == report.percentile25 && this.percentile50 == report.percentile50 && this.percentile75 == report.percentile75 && this.percentile90 == report.percentile90 && this.percentile95 == report.percentile95;
        }

        public final double getAverage() {
            return this.average;
        }

        public final int getBucketCount() {
            return this.bucketCount;
        }

        public final long getCount() {
            return this.count;
        }

        public final long getMax() {
            return this.max;
        }

        public final long getMin() {
            return this.min;
        }

        public final long getPercentile25() {
            return this.percentile25;
        }

        public final long getPercentile50() {
            return this.percentile50;
        }

        public final long getPercentile75() {
            return this.percentile75;
        }

        public final long getPercentile90() {
            return this.percentile90;
        }

        public final long getPercentile95() {
            return this.percentile95;
        }

        public int hashCode() {
            int a = b.a(this.max);
            int doubleToLongBits = Double.doubleToLongBits(this.average);
            int a2 = b.a(this.count);
            int a3 = b.a(this.percentile25);
            int a4 = b.a(this.percentile50);
            int a5 = b.a(this.percentile75);
            int a6 = b.a(this.percentile90);
            return b.a(this.percentile95) + ((a6 + ((a5 + ((a4 + ((a3 + ((((a2 + ((doubleToLongBits + ((a + (b.a(this.min) * 31)) * 31)) * 31)) * 31) + this.bucketCount) * 31)) * 31)) * 31)) * 31)) * 31);
        }

        public String toString() {
            StringBuilder R = a.R("Report(min=");
            R.append(this.min);
            R.append(", max=");
            R.append(this.max);
            R.append(", average=");
            R.append(this.average);
            R.append(", count=");
            R.append(this.count);
            R.append(", bucketCount=");
            R.append(this.bucketCount);
            R.append(", percentile25=");
            R.append(this.percentile25);
            R.append(", percentile50=");
            R.append(this.percentile50);
            R.append(", percentile75=");
            R.append(this.percentile75);
            R.append(", percentile90=");
            R.append(this.percentile90);
            R.append(", percentile95=");
            return a.B(R, this.percentile95, ")");
        }
    }

    public Histogram(int i, int i2) {
        this.precision = i;
        this.bucketLimit = i2;
        this.histogram = new TreeMap<>();
        this.halfPrecision = i / 2;
    }

    public final void addSample(long j) {
        this.count++;
        this.total += j;
        Long l = this.min;
        Long l2 = this.max;
        if (l == null || l.longValue() > j) {
            this.min = Long.valueOf(j);
        }
        if (l2 == null || l2.longValue() < j) {
            this.max = Long.valueOf(j);
        }
        long roundValue$utils_release = roundValue$utils_release(j);
        Long l3 = this.histogram.get(Long.valueOf(roundValue$utils_release));
        if (l3 != null || this.histogram.size() <= this.bucketLimit) {
            this.histogram.put(Long.valueOf(roundValue$utils_release), Long.valueOf((l3 != null ? l3.longValue() : 0L) + 1));
        }
    }

    public final Report getReport() {
        long j;
        Integer num;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        Integer num2 = 50;
        ArrayDeque arrayDeque = new ArrayDeque(n.listOf((Object[]) new Integer[]{25, num2, 75, 90, 95}));
        long j2 = 0;
        for (Map.Entry<Long, Long> entry : this.histogram.entrySet()) {
            long longValue = entry.getKey().longValue();
            long longValue2 = entry.getValue().longValue();
            if (arrayDeque.isEmpty()) {
                break;
            }
            j2 += longValue2;
            num2 = num2;
            double d = (j2 / this.count) * 100;
            while ((!arrayDeque.isEmpty()) && ((Number) arrayDeque.get(0)).doubleValue() <= d) {
                linkedHashMap.put(Integer.valueOf(((Number) arrayDeque.removeFirst()).intValue()), Long.valueOf(longValue));
            }
        }
        Integer num3 = num2;
        Long l = this.min;
        long longValue3 = l != null ? l.longValue() : 0L;
        Long l2 = this.max;
        long longValue4 = l2 != null ? l2.longValue() : 0L;
        long j3 = this.count;
        double d2 = j3 == 0 ? ShadowDrawableWrapper.COS_45 : this.total / j3;
        int size = this.histogram.size();
        Long l3 = (Long) linkedHashMap.get(25);
        if (l3 != null) {
            j = l3.longValue();
            num = num3;
        } else {
            num = num3;
            j = 0;
        }
        Long l4 = (Long) linkedHashMap.get(num);
        long longValue5 = l4 != null ? l4.longValue() : 0L;
        Long l5 = (Long) linkedHashMap.get(75);
        long longValue6 = l5 != null ? l5.longValue() : 0L;
        Long l6 = (Long) linkedHashMap.get(90);
        long longValue7 = l6 != null ? l6.longValue() : 0L;
        Long l7 = (Long) linkedHashMap.get(95);
        return new Report(longValue3, longValue4, d2, j3, size, j, longValue5, longValue6, longValue7, l7 != null ? l7.longValue() : 0L);
    }

    @VisibleForTesting
    public final long roundValue$utils_release(long j) {
        int i = this.precision;
        long j2 = j % i;
        long j3 = j - j2;
        return j2 <= ((long) this.halfPrecision) ? j3 : j3 + i;
    }

    public /* synthetic */ Histogram(int i, int i2, int i3, DefaultConstructorMarker defaultConstructorMarker) {
        this(i, (i3 & 2) != 0 ? 1000 : i2);
    }
}
