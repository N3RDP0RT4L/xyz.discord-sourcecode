package com.discord.utilities.collections;

import andhook.lib.HookHelper;
import androidx.exifinterface.media.ExifInterface;
import com.discord.utilities.collections.ShallowPartitionImmutableCollection;
import d0.a0.a;
import d0.t.c0;
import d0.t.n;
import d0.t.o;
import d0.t.r;
import d0.t.u;
import d0.z.d.g0.d;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.ranges.IntRange;
/* compiled from: ShallowPartitionMap.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010$\n\u0002\b\u0004\n\u0002\u0010#\n\u0002\u0010'\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010 \n\u0002\b\t\n\u0002\u0010\u001f\n\u0002\b\u000b\b\u0016\u0018\u0000 =*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u00022\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0003:\u0002=>B5\u0012\u0018\u0010+\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00030*\u0012\u0012\u0010\"\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020!0 ¢\u0006\u0004\b:\u0010;B'\b\u0016\u0012\b\b\u0002\u0010&\u001a\u00020!\u0012\u0012\u0010\"\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020!0 ¢\u0006\u0004\b:\u0010<J#\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00032\u0006\u0010\u0004\u001a\u00028\u0000H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J#\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00032\u0006\u0010\u0004\u001a\u00028\u0000H\u0016¢\u0006\u0004\b\u0007\u0010\u0006J\u0017\u0010\t\u001a\u00020\b2\u0006\u0010\u0004\u001a\u00028\u0000H\u0016¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\f\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00028\u0001H\u0016¢\u0006\u0004\b\f\u0010\nJ\u001a\u0010\r\u001a\u0004\u0018\u00018\u00012\u0006\u0010\u0004\u001a\u00028\u0000H\u0096\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u000f\u001a\u00020\bH\u0016¢\u0006\u0004\b\u000f\u0010\u0010J\u000f\u0010\u0012\u001a\u00020\u0011H\u0016¢\u0006\u0004\b\u0012\u0010\u0013J!\u0010\u0014\u001a\u0004\u0018\u00018\u00012\u0006\u0010\u0004\u001a\u00028\u00002\u0006\u0010\u000b\u001a\u00028\u0001H\u0016¢\u0006\u0004\b\u0014\u0010\u0015J%\u0010\u0018\u001a\u00020\u00112\u0014\u0010\u0017\u001a\u0010\u0012\u0006\b\u0001\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0016H\u0016¢\u0006\u0004\b\u0018\u0010\u0019J\u0019\u0010\u001a\u001a\u0004\u0018\u00018\u00012\u0006\u0010\u0004\u001a\u00028\u0000H\u0016¢\u0006\u0004\b\u001a\u0010\u000eR(\u0010\u001f\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u001c0\u001b8V@\u0016X\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001d\u0010\u001eR(\u0010\"\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020!0 8\u0004@\u0004X\u0084\u0004¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%R\u0019\u0010&\u001a\u00020!8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010'\u001a\u0004\b(\u0010)R.\u0010+\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00030*8\u0004@\u0004X\u0084\u0004¢\u0006\f\n\u0004\b+\u0010,\u001a\u0004\b-\u0010.R*\u00100\u001a\u00020!2\u0006\u0010/\u001a\u00020!8\u0016@TX\u0096\u000e¢\u0006\u0012\n\u0004\b0\u0010'\u001a\u0004\b1\u0010)\"\u0004\b2\u00103R\u001c\u00107\u001a\b\u0012\u0004\u0012\u00028\u0001048V@\u0016X\u0096\u0004¢\u0006\u0006\u001a\u0004\b5\u00106R\u001c\u00109\u001a\b\u0012\u0004\u0012\u00028\u00000\u001b8V@\u0016X\u0096\u0004¢\u0006\u0006\u001a\u0004\b8\u0010\u001e¨\u0006?"}, d2 = {"Lcom/discord/utilities/collections/ShallowPartitionMap;", "K", ExifInterface.GPS_MEASUREMENT_INTERRUPTED, "", "key", "getPartition", "(Ljava/lang/Object;)Ljava/util/Map;", "getPartitionForWrite", "", "containsKey", "(Ljava/lang/Object;)Z", "value", "containsValue", "get", "(Ljava/lang/Object;)Ljava/lang/Object;", "isEmpty", "()Z", "", "clear", "()V", "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "", "from", "putAll", "(Ljava/util/Map;)V", "remove", "", "", "getEntries", "()Ljava/util/Set;", "entries", "Lkotlin/Function1;", "", "partitionStrategy", "Lkotlin/jvm/functions/Function1;", "getPartitionStrategy", "()Lkotlin/jvm/functions/Function1;", "numPartitions", "I", "getNumPartitions", "()I", "", "partitions", "Ljava/util/List;", "getPartitions", "()Ljava/util/List;", "<set-?>", "size", "getSize", "setSize", "(I)V", "", "getValues", "()Ljava/util/Collection;", "values", "getKeys", "keys", HookHelper.constructorName, "(Ljava/util/List;Lkotlin/jvm/functions/Function1;)V", "(ILkotlin/jvm/functions/Function1;)V", "Companion", "CopiablePartitionMap", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public class ShallowPartitionMap<K, V> implements Map<K, V>, d {
    public static final Companion Companion = new Companion(null);
    private final int numPartitions;
    private final Function1<K, Integer> partitionStrategy;
    private final List<Map<K, V>> partitions;
    private int size;

    /* compiled from: ShallowPartitionMap.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0010\u0010\u0011J)\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00020\u00030\u0005\"\u0004\b\u0002\u0010\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007JW\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\r\"\u0004\b\u0002\u0010\u0002\"\u0004\b\u0003\u0010\b2\u0006\u0010\t\u001a\u00020\u00032\b\b\u0002\u0010\n\u001a\u00020\u00032\b\b\u0002\u0010\u000b\u001a\u00020\u00032\u0014\b\u0002\u0010\f\u001a\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00020\u00030\u0005¢\u0006\u0004\b\u000e\u0010\u000f¨\u0006\u0012"}, d2 = {"Lcom/discord/utilities/collections/ShallowPartitionMap$Companion;", "", "K", "", "numPartitions", "Lkotlin/Function1;", "getHashCodePartitionStrategy", "(I)Lkotlin/jvm/functions/Function1;", ExifInterface.GPS_MEASUREMENT_INTERRUPTED, "mapSize", "partitionSize", "partitionCount", "partitionStrategy", "Lcom/discord/utilities/collections/ShallowPartitionMap;", "create", "(IIILkotlin/jvm/functions/Function1;)Lcom/discord/utilities/collections/ShallowPartitionMap;", HookHelper.constructorName, "()V", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ ShallowPartitionMap create$default(Companion companion, int i, int i2, int i3, Function1 function1, int i4, Object obj) {
            if ((i4 & 2) != 0) {
                i2 = 100;
            }
            if ((i4 & 4) != 0) {
                i3 = a.roundToInt((float) Math.ceil((i > 0 ? i : 1) / i2));
            }
            if ((i4 & 8) != 0) {
                function1 = companion.getHashCodePartitionStrategy(i3);
            }
            return companion.create(i, i2, i3, function1);
        }

        private final <K> Function1<K, Integer> getHashCodePartitionStrategy(int i) {
            return new ShallowPartitionMap$Companion$getHashCodePartitionStrategy$1(i);
        }

        public final <K, V> ShallowPartitionMap<K, V> create(int i, int i2, int i3, Function1<? super K, Integer> function1) {
            m.checkNotNullParameter(function1, "partitionStrategy");
            IntRange intRange = new IntRange(0, i3);
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(intRange, 10));
            Iterator<Integer> it = intRange.iterator();
            while (it.hasNext()) {
                ((c0) it).nextInt();
                arrayList.add(new HashMap(i2));
            }
            return new ShallowPartitionMap<>(arrayList, function1);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: ShallowPartitionMap.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0016\u0018\u0000*\u0004\b\u0002\u0010\u0001*\u0004\b\u0003\u0010\u00022\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u0003B#\u0012\u0006\u0010\u001d\u001a\u00020\u0012\u0012\u0012\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00020\u00120\u001e¢\u0006\u0004\b \u0010!J#\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u00052\u0006\u0010\u0004\u001a\u00028\u0002H\u0016¢\u0006\u0004\b\u0006\u0010\u0007J\u000f\u0010\t\u001a\u00020\bH\u0016¢\u0006\u0004\b\t\u0010\nJ7\u0010\f\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u00050\u000b*\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u00050\u000bH\u0004¢\u0006\u0004\b\f\u0010\rJ\u001b\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u000eH\u0016¢\u0006\u0004\b\u000f\u0010\u0010R&\u0010\u0014\u001a\u0012\u0012\u0004\u0012\u00020\u00120\u0011j\b\u0012\u0004\u0012\u00020\u0012`\u00138\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015RN\u0010\u0017\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u00050\u000b2\u0018\u0010\u0016\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u00050\u000b8\u0004@DX\u0084\u000e¢\u0006\u0012\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001c¨\u0006\""}, d2 = {"Lcom/discord/utilities/collections/ShallowPartitionMap$CopiablePartitionMap;", "K", ExifInterface.GPS_MEASUREMENT_INTERRUPTED, "Lcom/discord/utilities/collections/ShallowPartitionMap;", "key", "", "getPartitionForWrite", "(Ljava/lang/Object;)Ljava/util/Map;", "", "clear", "()V", "", "defensiveCopy", "(Ljava/util/List;)Ljava/util/List;", "", "fastCopy", "()Ljava/util/Map;", "Ljava/util/HashSet;", "", "Lkotlin/collections/HashSet;", "dirtyPartitionIndices", "Ljava/util/HashSet;", "value", "defensiveCopyPartitions", "Ljava/util/List;", "getDefensiveCopyPartitions", "()Ljava/util/List;", "setDefensiveCopyPartitions", "(Ljava/util/List;)V", "numPartitions", "Lkotlin/Function1;", "partitionStrategy", HookHelper.constructorName, "(ILkotlin/jvm/functions/Function1;)V", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static class CopiablePartitionMap<K, V> extends ShallowPartitionMap<K, V> {
        private final HashSet<Integer> dirtyPartitionIndices = u.toHashSet(n.getIndices(getPartitions()));
        private List<? extends Map<K, V>> defensiveCopyPartitions = defensiveCopy(getPartitions());

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public CopiablePartitionMap(int i, Function1<? super K, Integer> function1) {
            super(i, function1);
            m.checkNotNullParameter(function1, "partitionStrategy");
        }

        @Override // com.discord.utilities.collections.ShallowPartitionMap, java.util.Map
        public void clear() {
            r.addAll(this.dirtyPartitionIndices, n.getIndices(getPartitions()));
            ShallowPartitionMap.super.clear();
        }

        public final List<Map<K, V>> defensiveCopy(List<? extends Map<K, V>> list) {
            m.checkNotNullParameter(list, "$this$defensiveCopy");
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
            int i = 0;
            for (Object obj : list) {
                i++;
                if (i < 0) {
                    n.throwIndexOverflow();
                }
                Object obj2 = (Map) obj;
                if (this.dirtyPartitionIndices.contains(Integer.valueOf(i))) {
                    obj2 = new HashMap(getPartitions().get(i));
                }
                arrayList.add(obj2);
            }
            return arrayList;
        }

        public Map<K, V> fastCopy() {
            setDefensiveCopyPartitions(defensiveCopy(this.defensiveCopyPartitions));
            return new ShallowPartitionMap(this.defensiveCopyPartitions, getPartitionStrategy());
        }

        public final List<Map<K, V>> getDefensiveCopyPartitions() {
            return (List<? extends Map<K, V>>) this.defensiveCopyPartitions;
        }

        @Override // com.discord.utilities.collections.ShallowPartitionMap
        public Map<K, V> getPartitionForWrite(K k) {
            int intValue = getPartitionStrategy().invoke(k).intValue();
            this.dirtyPartitionIndices.add(Integer.valueOf(intValue));
            return getPartitions().get(intValue);
        }

        public final void setDefensiveCopyPartitions(List<? extends Map<K, V>> list) {
            m.checkNotNullParameter(list, "value");
            this.defensiveCopyPartitions = list;
            this.dirtyPartitionIndices.clear();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public ShallowPartitionMap(List<? extends Map<K, V>> list, Function1<? super K, Integer> function1) {
        m.checkNotNullParameter(list, "partitions");
        m.checkNotNullParameter(function1, "partitionStrategy");
        this.partitions = list;
        this.partitionStrategy = function1;
        this.numPartitions = list.size();
        Iterator it = list.iterator();
        int i = 0;
        while (it.hasNext()) {
            i += ((Map) it.next()).size();
        }
        this.size = i;
    }

    private final Map<K, V> getPartition(K k) {
        return this.partitions.get(this.partitionStrategy.invoke(k).intValue());
    }

    @Override // java.util.Map
    public void clear() {
        Iterator<T> it = this.partitions.iterator();
        while (it.hasNext()) {
            ((Map) it.next()).clear();
        }
        setSize(0);
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.Map
    public boolean containsKey(Object obj) {
        return getPartition(obj).containsKey(obj);
    }

    @Override // java.util.Map
    public boolean containsValue(Object obj) {
        List<Map<K, V>> list = this.partitions;
        if ((list instanceof Collection) && list.isEmpty()) {
            return false;
        }
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            if (((Map) it.next()).containsValue(obj)) {
                return true;
            }
        }
        return false;
    }

    @Override // java.util.Map
    public final /* bridge */ Set<Map.Entry<K, V>> entrySet() {
        return getEntries();
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.Map
    public V get(Object obj) {
        return getPartition(obj).get(obj);
    }

    public Set<Map.Entry<K, V>> getEntries() {
        List<Map<K, V>> list = this.partitions;
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(((Map) it.next()).entrySet());
        }
        return new ShallowPartitionImmutableCollection.Set(arrayList, new ShallowPartitionMap$entries$2(this));
    }

    public Set<K> getKeys() {
        List<Map<K, V>> list = this.partitions;
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(((Map) it.next()).keySet());
        }
        return new ShallowPartitionImmutableCollection.Set(arrayList, this.partitionStrategy);
    }

    public final int getNumPartitions() {
        return this.numPartitions;
    }

    public Map<K, V> getPartitionForWrite(K k) {
        return this.partitions.get(this.partitionStrategy.invoke(k).intValue());
    }

    public final Function1<K, Integer> getPartitionStrategy() {
        return this.partitionStrategy;
    }

    public final List<Map<K, V>> getPartitions() {
        return this.partitions;
    }

    public int getSize() {
        return this.size;
    }

    public Collection<V> getValues() {
        List<Map<K, V>> list = this.partitions;
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(((Map) it.next()).values());
        }
        return new ShallowPartitionImmutableCollection(arrayList, null, 2, null);
    }

    @Override // java.util.Map
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override // java.util.Map
    public final /* bridge */ Set<K> keySet() {
        return getKeys();
    }

    @Override // java.util.Map
    public V put(K k, V v) {
        V put = getPartitionForWrite(k).put(k, v);
        if (put == null) {
            setSize(size() + 1);
        }
        return put;
    }

    @Override // java.util.Map
    public void putAll(Map<? extends K, ? extends V> map) {
        m.checkNotNullParameter(map, "from");
        for (Map.Entry<? extends K, ? extends V> entry : map.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.Map
    public V remove(Object obj) {
        V remove = getPartitionForWrite(obj).remove(obj);
        if (remove == null) {
            return null;
        }
        setSize(size() - 1);
        return remove;
    }

    public void setSize(int i) {
        this.size = i;
    }

    @Override // java.util.Map
    public final /* bridge */ int size() {
        return getSize();
    }

    @Override // java.util.Map
    public final /* bridge */ Collection<V> values() {
        return getValues();
    }

    public /* synthetic */ ShallowPartitionMap(int i, Function1 function1, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this((i2 & 1) != 0 ? 200 : i, function1);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public ShallowPartitionMap(int r3, kotlin.jvm.functions.Function1<? super K, java.lang.Integer> r4) {
        /*
            r2 = this;
            java.lang.String r0 = "partitionStrategy"
            d0.z.d.m.checkNotNullParameter(r4, r0)
            r0 = 0
            kotlin.ranges.IntRange r3 = d0.d0.f.until(r0, r3)
            java.util.ArrayList r0 = new java.util.ArrayList
            r1 = 10
            int r1 = d0.t.o.collectionSizeOrDefault(r3, r1)
            r0.<init>(r1)
            java.util.Iterator r3 = r3.iterator()
        L1a:
            boolean r1 = r3.hasNext()
            if (r1 == 0) goto L2f
            r1 = r3
            d0.t.c0 r1 = (d0.t.c0) r1
            r1.nextInt()
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            r0.add(r1)
            goto L1a
        L2f:
            r2.<init>(r0, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.collections.ShallowPartitionMap.<init>(int, kotlin.jvm.functions.Function1):void");
    }
}
