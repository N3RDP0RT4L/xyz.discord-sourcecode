package com.discord.utilities;

import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.permission.Permission;
import com.discord.api.permission.PermissionOverwrite;
import d0.o;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Pair;
/* compiled from: PermissionOverwriteUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u001a\u0019\u0010\u0004\u001a\u00020\u0003*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0001¢\u0006\u0004\b\u0004\u0010\u0005\u001a\u0019\u0010\u0006\u001a\u00020\u0003*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0001¢\u0006\u0004\b\u0006\u0010\u0005\u001a;\u0010\u000b\u001a\u0016\u0012\b\u0012\u00060\u0001j\u0002`\u0007\u0012\b\u0012\u00060\u0001j\u0002`\u00070\n*\u0004\u0018\u00010\u00002\n\u0010\b\u001a\u00060\u0001j\u0002`\u00072\u0006\u0010\t\u001a\u00020\u0003¢\u0006\u0004\b\u000b\u0010\f\u001a\u0019\u0010\u000f\u001a\u00020\u0003*\u00020\u00002\u0006\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b\u000f\u0010\u0010\u001a\u0019\u0010\u0011\u001a\u00020\u0003*\u00020\u00002\u0006\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b\u0011\u0010\u0010¨\u0006\u0012"}, d2 = {"Lcom/discord/api/permission/PermissionOverwrite;", "", "permission", "", "allows", "(Lcom/discord/api/permission/PermissionOverwrite;J)Z", "denies", "Lcom/discord/api/permission/PermissionBit;", "bit", "isAllowed", "Lkotlin/Pair;", "computeAllowDenyUpdateBits", "(Lcom/discord/api/permission/PermissionOverwrite;JZ)Lkotlin/Pair;", "Lcom/discord/api/channel/Channel;", "channel", "grantsAccessTo", "(Lcom/discord/api/permission/PermissionOverwrite;Lcom/discord/api/channel/Channel;)Z", "deniesAccessTo", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class PermissionOverwriteUtilsKt {
    public static final boolean allows(PermissionOverwrite permissionOverwrite, long j) {
        m.checkNotNullParameter(permissionOverwrite, "$this$allows");
        return (j & permissionOverwrite.c()) != 0;
    }

    public static final Pair<Long, Long> computeAllowDenyUpdateBits(PermissionOverwrite permissionOverwrite, long j, boolean z2) {
        long j2 = 0;
        long c = permissionOverwrite != null ? permissionOverwrite.c() : 0L;
        if (permissionOverwrite != null) {
            j2 = permissionOverwrite.d();
        }
        if (z2) {
            return o.to(Long.valueOf(c | j), Long.valueOf((~j) & j2));
        }
        return o.to(Long.valueOf(c & (~j)), Long.valueOf(j | j2));
    }

    public static final boolean denies(PermissionOverwrite permissionOverwrite, long j) {
        m.checkNotNullParameter(permissionOverwrite, "$this$denies");
        return (j & permissionOverwrite.d()) != 0;
    }

    public static final boolean deniesAccessTo(PermissionOverwrite permissionOverwrite, Channel channel) {
        m.checkNotNullParameter(permissionOverwrite, "$this$deniesAccessTo");
        m.checkNotNullParameter(channel, "channel");
        return denies(permissionOverwrite, Permission.VIEW_CHANNEL) || (ChannelUtils.t(channel) && denies(permissionOverwrite, Permission.CONNECT));
    }

    public static final boolean grantsAccessTo(PermissionOverwrite permissionOverwrite, Channel channel) {
        m.checkNotNullParameter(permissionOverwrite, "$this$grantsAccessTo");
        m.checkNotNullParameter(channel, "channel");
        return !deniesAccessTo(permissionOverwrite, channel) && ((ChannelUtils.r(channel) && allows(permissionOverwrite, Permission.VIEW_CHANNEL)) || (ChannelUtils.t(channel) && allows(permissionOverwrite, Permission.CONNECT)));
    }
}
