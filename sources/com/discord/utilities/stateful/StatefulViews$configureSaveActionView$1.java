package com.discord.utilities.stateful;

import android.view.View;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StatefulViews.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Landroid/view/View;", "", "invoke", "(Landroid/view/View;)V", "smartHide"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StatefulViews$configureSaveActionView$1 extends o implements Function1<View, Unit> {
    public static final StatefulViews$configureSaveActionView$1 INSTANCE = new StatefulViews$configureSaveActionView$1();

    public StatefulViews$configureSaveActionView$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(View view) {
        invoke2(view);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(View view) {
        m.checkNotNullParameter(view, "$this$smartHide");
        if (view instanceof FloatingActionButton) {
            ((FloatingActionButton) view).hide();
        } else {
            view.setVisibility(8);
        }
    }
}
