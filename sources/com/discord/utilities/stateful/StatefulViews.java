package com.discord.utilities.stateful;

import andhook.lib.HookHelper;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.ActivityChooserModel;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.FragmentActivity;
import com.discord.app.AppFragment;
import com.discord.databinding.ViewDialogConfirmationBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.stateful.StatefulViews;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.text.TextWatcherKt;
import com.discord.views.CheckedSetting;
import com.google.android.material.textfield.TextInputLayout;
import d0.t.k;
import d0.z.d.m;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import rx.functions.Action1;
import rx.functions.Func0;
import xyz.discord.R;
/* compiled from: StatefulViews.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0015\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0005\n\u0002\u0010\u0015\n\u0000\n\u0002\u0010\u001e\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u00016B\u0015\u0012\f\u0010-\u001a\b\u0012\u0004\u0012\u00020\u00030,¢\u0006\u0004\b0\u00101B\u0015\b\u0016\u0012\n\u0010-\u001a\u000202\"\u00020\u0003¢\u0006\u0004\b0\u00103B\u0017\b\u0016\u0012\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\r04¢\u0006\u0004\b0\u00105J\u001d\u0010\u0005\u001a\u00028\u0000\"\u0004\b\u0000\u0010\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\b\u0010\tJ\u000f\u0010\n\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\n\u0010\tJ3\u0010\u0012\u001a\u00020\u00112\u0006\u0010\f\u001a\u00020\u000b2\b\u0010\u000e\u001a\u0004\u0018\u00010\r2\u0012\u0010\u0010\u001a\n\u0012\u0006\b\u0001\u0012\u00020\r0\u000f\"\u00020\r¢\u0006\u0004\b\u0012\u0010\u0013J!\u0010\u0014\u001a\u00020\u00112\u0012\u0010\u0010\u001a\n\u0012\u0006\b\u0001\u0012\u00020\r0\u000f\"\u00020\r¢\u0006\u0004\b\u0014\u0010\u0015J\u0015\u0010\u0016\u001a\u00020\u00112\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\u0016\u0010\u0017J#\u0010\u0005\u001a\u00028\u0000\"\u0004\b\u0000\u0010\u00022\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0018\u001a\u00028\u0000¢\u0006\u0004\b\u0005\u0010\u0019J\u001d\u0010\u001a\u001a\u0004\u0018\u00018\u0000\"\u0004\b\u0000\u0010\u00022\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0004\b\u001a\u0010\u0006J#\u0010\u001c\u001a\u00020\u0011\"\u0004\b\u0000\u0010\u00022\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u001b\u001a\u00028\u0000¢\u0006\u0004\b\u001c\u0010\u001dJ\u0019\u0010\u001f\u001a\u00020\u00112\b\b\u0002\u0010\u001e\u001a\u00020\u0007H\u0007¢\u0006\u0004\b\u001f\u0010 J\u0017\u0010\"\u001a\u00020\u00112\b\u0010!\u001a\u0004\u0018\u00010\r¢\u0006\u0004\b\"\u0010#J\u0015\u0010%\u001a\u00020\u00072\u0006\u0010$\u001a\u00020\u0003¢\u0006\u0004\b%\u0010&R2\u0010)\u001a\u001e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00070'j\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0007`(8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b)\u0010*R6\u0010+\u001a\"\u0012\u0004\u0012\u00020\u0003\u0012\u0006\u0012\u0004\u0018\u00010\u00010'j\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0006\u0012\u0004\u0018\u00010\u0001`(8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b+\u0010*R\u001c\u0010-\u001a\b\u0012\u0004\u0012\u00020\u00030,8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b-\u0010.R6\u0010/\u001a\"\u0012\u0004\u0012\u00020\u0003\u0012\u0006\u0012\u0004\u0018\u00010\u00010'j\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0006\u0012\u0004\u0018\u00010\u0001`(8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b/\u0010*¨\u00067"}, d2 = {"Lcom/discord/utilities/stateful/StatefulViews;", "", ExifInterface.GPS_DIRECTION_TRUE, "", ModelAuditLogEntry.CHANGE_KEY_ID, "get", "(I)Ljava/lang/Object;", "", "isAnyRequiredFieldBlank", "()Z", "hasAnythingChanged", "Lcom/discord/app/AppFragment;", "fragment", "Landroid/view/View;", "saveAction", "", "views", "", "setupTextWatcherWithSaveAction", "(Lcom/discord/app/AppFragment;Landroid/view/View;[Landroid/view/View;)V", "addOptionalFields", "([Landroid/view/View;)V", "setupUnsavedChangesConfirmation", "(Lcom/discord/app/AppFragment;)V", "defaultValue", "(ILjava/lang/Object;)Ljava/lang/Object;", "getIfChanged", "value", "put", "(ILjava/lang/Object;)V", "clearDefaults", "clear", "(Z)V", "saveActionView", "configureSaveActionView", "(Landroid/view/View;)V", "viewId", "hasChanged", "(I)Z", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "requiredFieldIds", "Ljava/util/HashMap;", "viewValues", "", "ids", "Ljava/util/List;", "viewValuesEdited", HookHelper.constructorName, "(Ljava/util/List;)V", "", "([I)V", "", "(Ljava/util/Collection;)V", "FragmentOnBackPressedHandler", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StatefulViews {
    private final List<Integer> ids;
    private final HashMap<Integer, Boolean> requiredFieldIds;
    private final HashMap<Integer, Object> viewValues;
    private final HashMap<Integer, Object> viewValuesEdited;

    /* compiled from: StatefulViews.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0002\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u000e\u001a\u00020\r\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00020\u0005¢\u0006\u0004\b\u0012\u0010\u0013J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004R\u001f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0007\u001a\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u0019\u0010\u000e\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011¨\u0006\u0014"}, d2 = {"Lcom/discord/utilities/stateful/StatefulViews$FragmentOnBackPressedHandler;", "", "", "onBackPressed", "()Z", "Lkotlin/Function0;", "hasAnythingChanged", "Lkotlin/jvm/functions/Function0;", "getHasAnythingChanged", "()Lkotlin/jvm/functions/Function0;", "Ljava/util/concurrent/atomic/AtomicBoolean;", "discardConfirmed", "Ljava/util/concurrent/atomic/AtomicBoolean;", "Landroid/app/Activity;", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "Landroid/app/Activity;", "getActivity", "()Landroid/app/Activity;", HookHelper.constructorName, "(Landroid/app/Activity;Lkotlin/jvm/functions/Function0;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class FragmentOnBackPressedHandler {
        private final Activity activity;
        private final AtomicBoolean discardConfirmed = new AtomicBoolean(false);
        private final Function0<Boolean> hasAnythingChanged;

        public FragmentOnBackPressedHandler(Activity activity, Function0<Boolean> function0) {
            m.checkNotNullParameter(activity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
            m.checkNotNullParameter(function0, "hasAnythingChanged");
            this.activity = activity;
            this.hasAnythingChanged = function0;
        }

        public final Activity getActivity() {
            return this.activity;
        }

        public final Function0<Boolean> getHasAnythingChanged() {
            return this.hasAnythingChanged;
        }

        public final boolean onBackPressed() {
            if (!this.hasAnythingChanged.invoke().booleanValue() || this.discardConfirmed.get()) {
                return false;
            }
            ViewDialogConfirmationBinding b2 = ViewDialogConfirmationBinding.b(LayoutInflater.from(this.activity));
            m.checkNotNullExpressionValue(b2, "ViewDialogConfirmationBi…tInflater.from(activity))");
            final AlertDialog create = new AlertDialog.Builder(this.activity).setView(b2.a).create();
            m.checkNotNullExpressionValue(create, "AlertDialog.Builder(acti…logBinding.root).create()");
            b2.d.setText(R.string.discard_changes);
            b2.e.setText(R.string.discard_changes_description);
            b2.f2167b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.utilities.stateful.StatefulViews$FragmentOnBackPressedHandler$onBackPressed$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    AlertDialog.this.dismiss();
                }
            });
            b2.c.setText(R.string.okay);
            b2.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.utilities.stateful.StatefulViews$FragmentOnBackPressedHandler$onBackPressed$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    AtomicBoolean atomicBoolean;
                    atomicBoolean = StatefulViews.FragmentOnBackPressedHandler.this.discardConfirmed;
                    atomicBoolean.set(true);
                    create.dismiss();
                    StatefulViews.FragmentOnBackPressedHandler.this.getActivity().onBackPressed();
                }
            });
            create.show();
            return true;
        }
    }

    public StatefulViews(List<Integer> list) {
        m.checkNotNullParameter(list, "ids");
        this.ids = list;
        this.viewValues = new HashMap<>();
        this.viewValuesEdited = new HashMap<>();
        this.requiredFieldIds = new HashMap<>();
    }

    public static /* synthetic */ void clear$default(StatefulViews statefulViews, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = false;
        }
        statefulViews.clear(z2);
    }

    private final <T> T get(int i) {
        return (T) (this.viewValuesEdited.containsKey(Integer.valueOf(i)) ? this.viewValuesEdited : this.viewValues).get(Integer.valueOf(i));
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Removed duplicated region for block: B:24:0x0075 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final boolean hasAnythingChanged() {
        /*
            r7 = this;
            java.util.List<java.lang.Integer> r0 = r7.ids
            boolean r1 = r0 instanceof java.util.Collection
            r2 = 1
            r3 = 0
            if (r1 == 0) goto L10
            boolean r1 = r0.isEmpty()
            if (r1 == 0) goto L10
        Le:
            r2 = 0
            goto L75
        L10:
            java.util.Iterator r0 = r0.iterator()
        L14:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto Le
            java.lang.Object r1 = r0.next()
            java.lang.Number r1 = (java.lang.Number) r1
            int r1 = r1.intValue()
            java.util.HashMap<java.lang.Integer, java.lang.Object> r4 = r7.viewValuesEdited
            java.lang.Integer r5 = java.lang.Integer.valueOf(r1)
            boolean r4 = r4.containsKey(r5)
            if (r4 == 0) goto L72
            java.util.HashMap<java.lang.Integer, java.lang.Object> r4 = r7.viewValuesEdited
            java.lang.Integer r5 = java.lang.Integer.valueOf(r1)
            java.lang.Object r4 = r4.get(r5)
            boolean r5 = r4 instanceof android.text.SpannableStringBuilder
            if (r5 == 0) goto L42
            java.lang.String r4 = r4.toString()
        L42:
            java.util.HashMap<java.lang.Integer, java.lang.Object> r5 = r7.viewValues
            java.lang.Integer r6 = java.lang.Integer.valueOf(r1)
            java.lang.Object r5 = r5.get(r6)
            boolean r5 = r5 instanceof android.text.SpannableStringBuilder
            if (r5 == 0) goto L5f
            java.util.HashMap<java.lang.Integer, java.lang.Object> r5 = r7.viewValues
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            java.lang.Object r1 = r5.get(r1)
            java.lang.String r1 = java.lang.String.valueOf(r1)
            goto L69
        L5f:
            java.util.HashMap<java.lang.Integer, java.lang.Object> r5 = r7.viewValues
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            java.lang.Object r1 = r5.get(r1)
        L69:
            boolean r1 = d0.z.d.m.areEqual(r4, r1)
            r1 = r1 ^ r2
            if (r1 == 0) goto L72
            r1 = 1
            goto L73
        L72:
            r1 = 0
        L73:
            if (r1 == 0) goto L14
        L75:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.stateful.StatefulViews.hasAnythingChanged():boolean");
    }

    /* JADX WARN: Removed duplicated region for block: B:20:0x004e A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final boolean isAnyRequiredFieldBlank() {
        /*
            r5 = this;
            java.util.HashMap<java.lang.Integer, java.lang.Boolean> r0 = r5.requiredFieldIds
            boolean r1 = r0.isEmpty()
            r2 = 1
            r3 = 0
            if (r1 == 0) goto Lc
        La:
            r2 = 0
            goto L4e
        Lc:
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r0 = r0.iterator()
        L14:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto La
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            java.lang.Object r4 = r1.getKey()
            java.lang.Number r4 = (java.lang.Number) r4
            int r4 = r4.intValue()
            java.lang.Object r1 = r1.getValue()
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L4b
            java.lang.Object r1 = r5.get(r4)
            boolean r4 = r1 instanceof java.lang.String
            if (r4 != 0) goto L3f
            r1 = 0
        L3f:
            java.lang.String r1 = (java.lang.String) r1
            if (r1 == 0) goto L4b
            boolean r1 = d0.g0.t.isBlank(r1)
            if (r1 != r2) goto L4b
            r1 = 1
            goto L4c
        L4b:
            r1 = 0
        L4c:
            if (r1 == 0) goto L14
        L4e:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.stateful.StatefulViews.isAnyRequiredFieldBlank():boolean");
    }

    public final void addOptionalFields(View... viewArr) {
        m.checkNotNullParameter(viewArr, "views");
        for (View view : viewArr) {
            this.requiredFieldIds.put(Integer.valueOf(view.getId()), Boolean.FALSE);
        }
    }

    public final void clear() {
        clear$default(this, false, 1, null);
    }

    public final void clear(boolean z2) {
        this.viewValuesEdited.clear();
        if (z2) {
            this.viewValues.clear();
        }
    }

    public final void configureSaveActionView(View view) {
        StatefulViews$configureSaveActionView$1 statefulViews$configureSaveActionView$1 = StatefulViews$configureSaveActionView$1.INSTANCE;
        StatefulViews$configureSaveActionView$2 statefulViews$configureSaveActionView$2 = StatefulViews$configureSaveActionView$2.INSTANCE;
        if (view == null) {
            return;
        }
        if (!hasAnythingChanged() || isAnyRequiredFieldBlank()) {
            statefulViews$configureSaveActionView$1.invoke2(view);
        } else {
            statefulViews$configureSaveActionView$2.invoke2(view);
        }
    }

    public final <T> T getIfChanged(int i) {
        if (hasChanged(i)) {
            return (T) this.viewValuesEdited.get(Integer.valueOf(i));
        }
        return null;
    }

    public final boolean hasChanged(int i) {
        if (this.viewValuesEdited.containsKey(Integer.valueOf(i))) {
            return !m.areEqual(this.viewValuesEdited.get(Integer.valueOf(i)), this.viewValues.get(Integer.valueOf(i)));
        }
        return false;
    }

    public final <T> void put(int i, T t) {
        this.viewValuesEdited.put(Integer.valueOf(i), t);
    }

    public final void setupTextWatcherWithSaveAction(AppFragment appFragment, View view, View... viewArr) {
        m.checkNotNullParameter(appFragment, "fragment");
        m.checkNotNullParameter(viewArr, "views");
        for (View view2 : viewArr) {
            HashMap<Integer, Boolean> hashMap = this.requiredFieldIds;
            Integer valueOf = Integer.valueOf(view2.getId());
            Boolean bool = this.requiredFieldIds.get(Integer.valueOf(view2.getId()));
            if (bool == null) {
                bool = Boolean.TRUE;
            }
            m.checkNotNullExpressionValue(bool, "requiredFieldIds[view.id] ?: true");
            hashMap.put(valueOf, bool);
            StatefulViews$setupTextWatcherWithSaveAction$$inlined$forEach$lambda$1 statefulViews$setupTextWatcherWithSaveAction$$inlined$forEach$lambda$1 = new StatefulViews$setupTextWatcherWithSaveAction$$inlined$forEach$lambda$1(view2, this, view, appFragment);
            final StatefulViews$setupTextWatcherWithSaveAction$$inlined$forEach$lambda$2 statefulViews$setupTextWatcherWithSaveAction$$inlined$forEach$lambda$2 = new StatefulViews$setupTextWatcherWithSaveAction$$inlined$forEach$lambda$2(view2, this, view, appFragment);
            if (view2 instanceof TextView) {
                TextWatcherKt.addBindedTextWatcher((TextView) view2, appFragment, statefulViews$setupTextWatcherWithSaveAction$$inlined$forEach$lambda$1);
            } else if (view2 instanceof TextInputLayout) {
                ViewExtensions.addBindedTextWatcher((TextInputLayout) view2, appFragment, statefulViews$setupTextWatcherWithSaveAction$$inlined$forEach$lambda$1);
            } else if (view2 instanceof CheckedSetting) {
                ((CheckedSetting) view2).setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.utilities.stateful.StatefulViews$setupTextWatcherWithSaveAction$$inlined$forEach$lambda$3
                    public final void call(Boolean bool2) {
                        StatefulViews$setupTextWatcherWithSaveAction$$inlined$forEach$lambda$2 statefulViews$setupTextWatcherWithSaveAction$$inlined$forEach$lambda$22 = StatefulViews$setupTextWatcherWithSaveAction$$inlined$forEach$lambda$2.this;
                        m.checkNotNullExpressionValue(bool2, "it");
                        statefulViews$setupTextWatcherWithSaveAction$$inlined$forEach$lambda$22.invoke(bool2.booleanValue());
                    }
                });
            } else {
                throw new UnsupportedOperationException(view2 + " must support using `TextWatcher`.");
            }
        }
    }

    public final void setupUnsavedChangesConfirmation(AppFragment appFragment) {
        final FragmentOnBackPressedHandler fragmentOnBackPressedHandler;
        m.checkNotNullParameter(appFragment, "fragment");
        FragmentActivity activity = appFragment.e();
        if (activity != null) {
            m.checkNotNullExpressionValue(activity, "it");
            fragmentOnBackPressedHandler = new FragmentOnBackPressedHandler(activity, new StatefulViews$setupUnsavedChangesConfirmation$$inlined$let$lambda$1(this));
        } else {
            fragmentOnBackPressedHandler = null;
        }
        AppFragment.setOnBackPressed$default(appFragment, new Func0<Boolean>() { // from class: com.discord.utilities.stateful.StatefulViews$setupUnsavedChangesConfirmation$1
            @Override // rx.functions.Func0, java.util.concurrent.Callable
            public final Boolean call() {
                StatefulViews.FragmentOnBackPressedHandler fragmentOnBackPressedHandler2 = StatefulViews.FragmentOnBackPressedHandler.this;
                return fragmentOnBackPressedHandler2 != null ? Boolean.valueOf(fragmentOnBackPressedHandler2.onBackPressed()) : Boolean.FALSE;
            }
        }, 0, 2, null);
    }

    public final <T> T get(int i, T t) {
        this.viewValues.put(Integer.valueOf(i), t);
        T t2 = (T) get(i);
        return (!m.areEqual(this.requiredFieldIds.get(Integer.valueOf(i)), Boolean.FALSE) && t2 == null) ? t : t2;
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public StatefulViews(int... iArr) {
        this(k.toList(iArr));
        m.checkNotNullParameter(iArr, "ids");
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public StatefulViews(java.util.Collection<? extends android.view.View> r3) {
        /*
            r2 = this;
            java.lang.String r0 = "views"
            d0.z.d.m.checkNotNullParameter(r3, r0)
            java.util.ArrayList r0 = new java.util.ArrayList
            r1 = 10
            int r1 = d0.t.o.collectionSizeOrDefault(r3, r1)
            r0.<init>(r1)
            java.util.Iterator r3 = r3.iterator()
        L14:
            boolean r1 = r3.hasNext()
            if (r1 == 0) goto L2c
            java.lang.Object r1 = r3.next()
            android.view.View r1 = (android.view.View) r1
            int r1 = r1.getId()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r0.add(r1)
            goto L14
        L2c:
            r2.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.stateful.StatefulViews.<init>(java.util.Collection):void");
    }
}
