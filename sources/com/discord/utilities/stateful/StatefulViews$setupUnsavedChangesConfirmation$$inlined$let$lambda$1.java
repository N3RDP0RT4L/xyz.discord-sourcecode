package com.discord.utilities.stateful;

import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StatefulViews.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000b\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002¨\u0006\u0003"}, d2 = {"", "invoke", "()Z", "com/discord/utilities/stateful/StatefulViews$setupUnsavedChangesConfirmation$onBackPressedHandler$1$1", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StatefulViews$setupUnsavedChangesConfirmation$$inlined$let$lambda$1 extends o implements Function0<Boolean> {
    public final /* synthetic */ StatefulViews this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StatefulViews$setupUnsavedChangesConfirmation$$inlined$let$lambda$1(StatefulViews statefulViews) {
        super(0);
        this.this$0 = statefulViews;
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [boolean, java.lang.Boolean] */
    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final Boolean invoke2() {
        ?? hasAnythingChanged;
        hasAnythingChanged = this.this$0.hasAnythingChanged();
        return hasAnythingChanged;
    }
}
