package com.discord.utilities.messagesend;

import androidx.core.app.NotificationCompat;
import com.discord.restapi.PayloadJSON;
import com.discord.restapi.RestAPIParams;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rest.SendUtils;
import d0.t.o;
import j0.k.b;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import okhttp3.MultipartBody;
import rx.Observable;
/* compiled from: MessageQueue.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u001e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0004 \u0001*\u000e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/utilities/rest/SendUtils$SendPayload$ReadyToSendCommand;", "kotlin.jvm.PlatformType", "sendPayload", "Lrx/Observable;", "Ljava/lang/Void;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/utilities/rest/SendUtils$SendPayload$ReadyToSendCommand;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MessageQueue$doSendApplicationCommand$2<T, R> implements b<SendUtils.SendPayload.ReadyToSendCommand, Observable<? extends Void>> {
    public static final MessageQueue$doSendApplicationCommand$2 INSTANCE = new MessageQueue$doSendApplicationCommand$2();

    public final Observable<? extends Void> call(SendUtils.SendPayload.ReadyToSendCommand readyToSendCommand) {
        List<SendUtils.FileUpload> uploads = readyToSendCommand.getUploads();
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(uploads, 10));
        for (SendUtils.FileUpload fileUpload : uploads) {
            arrayList.add(fileUpload.getPart());
        }
        if (!(!arrayList.isEmpty())) {
            return RestAPI.Companion.getApi().sendApplicationCommand(readyToSendCommand.getCommand().toRestApiParam());
        }
        RestAPI api = RestAPI.Companion.getApi();
        PayloadJSON<RestAPIParams.ApplicationCommand> payloadJSON = new PayloadJSON<>(readyToSendCommand.getCommand().toRestApiParam());
        Object[] array = arrayList.toArray(new MultipartBody.Part[0]);
        Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
        return api.sendApplicationCommand(payloadJSON, (MultipartBody.Part[]) array);
    }
}
