package com.discord.utilities.messagesend;

import andhook.lib.HookHelper;
import androidx.appcompat.widget.ActivityChooserModel;
import com.discord.api.activity.Activity;
import com.discord.api.message.allowedmentions.MessageAllowedMentions;
import com.discord.models.commands.ApplicationCommandLocalSendData;
import com.discord.models.message.Message;
import com.discord.utilities.messagesend.MessageResult;
import com.discord.utilities.rest.SendUtils;
import com.lytefast.flexinput.model.Attachment;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: MessageQueue.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0010\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0016\u0017\u0018B3\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0018\u0010\u0010\u001a\u0014\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\f\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0019\u0010\b\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR+\u0010\u0010\u001a\u0014\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013\u0082\u0001\u0003\u0019\u001a\u001b¨\u0006\u001c"}, d2 = {"Lcom/discord/utilities/messagesend/MessageRequest;", "", "", "requestId", "Ljava/lang/String;", "getRequestId", "()Ljava/lang/String;", "", "attemptTimestamp", "J", "getAttemptTimestamp", "()J", "Lkotlin/Function2;", "Lcom/discord/utilities/messagesend/MessageResult;", "", "", "onCompleted", "Lkotlin/jvm/functions/Function2;", "getOnCompleted", "()Lkotlin/jvm/functions/Function2;", HookHelper.constructorName, "(Ljava/lang/String;Lkotlin/jvm/functions/Function2;J)V", "Edit", "Send", "SendApplicationCommand", "Lcom/discord/utilities/messagesend/MessageRequest$Send;", "Lcom/discord/utilities/messagesend/MessageRequest$Edit;", "Lcom/discord/utilities/messagesend/MessageRequest$SendApplicationCommand;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class MessageRequest {
    private final long attemptTimestamp;
    private final Function2<MessageResult, Boolean, Unit> onCompleted;
    private final String requestId;

    /* compiled from: MessageQueue.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B9\u0012\n\u0010\u0013\u001a\u00060\u0007j\u0002`\u0012\u0012\u0006\u0010\u000e\u001a\u00020\r\u0012\n\u0010\t\u001a\u00060\u0007j\u0002`\b\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\u0015\u001a\u00020\u0007¢\u0006\u0004\b\u0016\u0010\u0017R\u001b\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u001d\u0010\t\u001a\u00060\u0007j\u0002`\b8\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\fR\u0019\u0010\u000e\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u001d\u0010\u0013\u001a\u00060\u0007j\u0002`\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\n\u001a\u0004\b\u0014\u0010\f¨\u0006\u0018"}, d2 = {"Lcom/discord/utilities/messagesend/MessageRequest$Edit;", "Lcom/discord/utilities/messagesend/MessageRequest;", "Lcom/discord/api/message/allowedmentions/MessageAllowedMentions;", "allowedMentions", "Lcom/discord/api/message/allowedmentions/MessageAllowedMentions;", "getAllowedMentions", "()Lcom/discord/api/message/allowedmentions/MessageAllowedMentions;", "", "Lcom/discord/primitives/MessageId;", "messageId", "J", "getMessageId", "()J", "", "content", "Ljava/lang/String;", "getContent", "()Ljava/lang/String;", "Lcom/discord/primitives/ChannelId;", "channelId", "getChannelId", "attemptTimestamp", HookHelper.constructorName, "(JLjava/lang/String;JLcom/discord/api/message/allowedmentions/MessageAllowedMentions;J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Edit extends MessageRequest {
        private final MessageAllowedMentions allowedMentions;
        private final long channelId;
        private final String content;
        private final long messageId;

        /* compiled from: MessageQueue.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/utilities/messagesend/MessageResult;", "<anonymous parameter 0>", "", "<anonymous parameter 1>", "", "invoke", "(Lcom/discord/utilities/messagesend/MessageResult;Z)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.utilities.messagesend.MessageRequest$Edit$1  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass1 extends o implements Function2<MessageResult, Boolean, Unit> {
            public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

            public AnonymousClass1() {
                super(2);
            }

            @Override // kotlin.jvm.functions.Function2
            public /* bridge */ /* synthetic */ Unit invoke(MessageResult messageResult, Boolean bool) {
                invoke(messageResult, bool.booleanValue());
                return Unit.a;
            }

            public final void invoke(MessageResult messageResult, boolean z2) {
                m.checkNotNullParameter(messageResult, "<anonymous parameter 0>");
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public Edit(long j, String str, long j2, MessageAllowedMentions messageAllowedMentions, long j3) {
            super(String.valueOf(j2), AnonymousClass1.INSTANCE, j3, null);
            m.checkNotNullParameter(str, "content");
            this.channelId = j;
            this.content = str;
            this.messageId = j2;
            this.allowedMentions = messageAllowedMentions;
        }

        public final MessageAllowedMentions getAllowedMentions() {
            return this.allowedMentions;
        }

        public final long getChannelId() {
            return this.channelId;
        }

        public final String getContent() {
            return this.content;
        }

        public final long getMessageId() {
            return this.messageId;
        }
    }

    /* compiled from: MessageQueue.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0002\b\u0004\u0018\u00002\u00020\u0001B}\u0012\u0006\u0010\u001b\u001a\u00020\u001a\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\r\u0012\u0012\u0010\u0013\u001a\u000e\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u0012\u0018\u00010\u0006\u0012\u0018\u0010\"\u001a\u0014\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020!\u0012\u0004\u0012\u00020\b0\u001f\u0012\u0012\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\b0\u0005\u0012\u0018\u0010\t\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0004\u0012\u00020\b0\u0005\u0012\u0006\u0010$\u001a\u00020#¢\u0006\u0004\b%\u0010&J\u000f\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0003\u0010\u0004R+\u0010\t\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0004\u0012\u00020\b0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\fR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R%\u0010\u0013\u001a\u000e\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u0012\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R%\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\b0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\n\u001a\u0004\b\u0019\u0010\fR\u0019\u0010\u001b\u001a\u00020\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001e¨\u0006'"}, d2 = {"Lcom/discord/utilities/messagesend/MessageRequest$Send;", "Lcom/discord/utilities/messagesend/MessageRequest;", "Lcom/discord/utilities/messagesend/MessageResult$ValidationError;", "validateMessage", "()Lcom/discord/utilities/messagesend/MessageResult$ValidationError;", "Lkotlin/Function1;", "", "Lcom/discord/utilities/rest/SendUtils$FileUpload;", "", "onProgress", "Lkotlin/jvm/functions/Function1;", "getOnProgress", "()Lkotlin/jvm/functions/Function1;", "Lcom/discord/api/activity/Activity;", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "Lcom/discord/api/activity/Activity;", "getActivity", "()Lcom/discord/api/activity/Activity;", "Lcom/lytefast/flexinput/model/Attachment;", "attachments", "Ljava/util/List;", "getAttachments", "()Ljava/util/List;", "Lcom/discord/utilities/rest/SendUtils$SendPayload$Preprocessing;", "onPreprocessing", "getOnPreprocessing", "Lcom/discord/models/message/Message;", "message", "Lcom/discord/models/message/Message;", "getMessage", "()Lcom/discord/models/message/Message;", "Lkotlin/Function2;", "Lcom/discord/utilities/messagesend/MessageResult;", "", "onCompleted", "", "attemptTimestamp", HookHelper.constructorName, "(Lcom/discord/models/message/Message;Lcom/discord/api/activity/Activity;Ljava/util/List;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Send extends MessageRequest {
        private final Activity activity;
        private final List<Attachment<?>> attachments;
        private final Message message;
        private final Function1<SendUtils.SendPayload.Preprocessing, Unit> onPreprocessing;
        private final Function1<List<SendUtils.FileUpload>, Unit> onProgress;

        /* JADX WARN: Illegal instructions before constructor call */
        /* JADX WARN: Multi-variable type inference failed */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public Send(com.discord.models.message.Message r8, com.discord.api.activity.Activity r9, java.util.List<? extends com.lytefast.flexinput.model.Attachment<?>> r10, kotlin.jvm.functions.Function2<? super com.discord.utilities.messagesend.MessageResult, ? super java.lang.Boolean, kotlin.Unit> r11, kotlin.jvm.functions.Function1<? super com.discord.utilities.rest.SendUtils.SendPayload.Preprocessing, kotlin.Unit> r12, kotlin.jvm.functions.Function1<? super java.util.List<com.discord.utilities.rest.SendUtils.FileUpload>, kotlin.Unit> r13, long r14) {
            /*
                r7 = this;
                java.lang.String r0 = "message"
                d0.z.d.m.checkNotNullParameter(r8, r0)
                java.lang.String r0 = "onCompleted"
                d0.z.d.m.checkNotNullParameter(r11, r0)
                java.lang.String r0 = "onPreprocessing"
                d0.z.d.m.checkNotNullParameter(r12, r0)
                java.lang.String r0 = "onProgress"
                d0.z.d.m.checkNotNullParameter(r13, r0)
                java.lang.String r2 = r8.getNonce()
                d0.z.d.m.checkNotNull(r2)
                r6 = 0
                r1 = r7
                r3 = r11
                r4 = r14
                r1.<init>(r2, r3, r4, r6)
                r7.message = r8
                r7.activity = r9
                r7.attachments = r10
                r7.onPreprocessing = r12
                r7.onProgress = r13
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.messagesend.MessageRequest.Send.<init>(com.discord.models.message.Message, com.discord.api.activity.Activity, java.util.List, kotlin.jvm.functions.Function2, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function1, long):void");
        }

        public final Activity getActivity() {
            return this.activity;
        }

        public final List<Attachment<?>> getAttachments() {
            return this.attachments;
        }

        public final Message getMessage() {
            return this.message;
        }

        public final Function1<SendUtils.SendPayload.Preprocessing, Unit> getOnPreprocessing() {
            return this.onPreprocessing;
        }

        public final Function1<List<SendUtils.FileUpload>, Unit> getOnProgress() {
            return this.onProgress;
        }

        public final MessageResult.ValidationError validateMessage() {
            if (this.message.getActivity() != null) {
                List<Attachment<?>> list = this.attachments;
                if (!(list == null || list.isEmpty())) {
                    return new MessageResult.ValidationError("Cannot send attachments with activity action");
                }
            }
            return null;
        }
    }

    /* compiled from: MessageQueue.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0002\b\u0004\u0018\u00002\u00020\u0001B{\u0012\u0006\u0010\u0010\u001a\u00020\u000f\u0012\u0006\u0010\u0018\u001a\u00020\u0017\u0012\u0012\u0010\u0004\u001a\u000e\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u0003\u0018\u00010\u0002\u0012\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\b\u0012\u0018\u0010\u0015\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00140\u0002\u0012\u0004\u0012\u00020\n0\b\u0012\u0018\u0010\u001f\u001a\u0014\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\n0\u001c\u0012\u0006\u0010!\u001a\u00020 ¢\u0006\u0004\b\"\u0010#R%\u0010\u0004\u001a\u000e\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u0003\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007R%\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000eR\u0019\u0010\u0010\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R+\u0010\u0015\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00140\u0002\u0012\u0004\u0012\u00020\n0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\f\u001a\u0004\b\u0016\u0010\u000eR\u0019\u0010\u0018\u001a\u00020\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001b¨\u0006$"}, d2 = {"Lcom/discord/utilities/messagesend/MessageRequest$SendApplicationCommand;", "Lcom/discord/utilities/messagesend/MessageRequest;", "", "Lcom/lytefast/flexinput/model/Attachment;", "attachments", "Ljava/util/List;", "getAttachments", "()Ljava/util/List;", "Lkotlin/Function1;", "Lcom/discord/utilities/rest/SendUtils$SendPayload$Preprocessing;", "", "onPreprocessing", "Lkotlin/jvm/functions/Function1;", "getOnPreprocessing", "()Lkotlin/jvm/functions/Function1;", "Lcom/discord/api/message/Message;", "message", "Lcom/discord/api/message/Message;", "getMessage", "()Lcom/discord/api/message/Message;", "Lcom/discord/utilities/rest/SendUtils$FileUpload;", "onProgress", "getOnProgress", "Lcom/discord/models/commands/ApplicationCommandLocalSendData;", "applicationCommandSendData", "Lcom/discord/models/commands/ApplicationCommandLocalSendData;", "getApplicationCommandSendData", "()Lcom/discord/models/commands/ApplicationCommandLocalSendData;", "Lkotlin/Function2;", "Lcom/discord/utilities/messagesend/MessageResult;", "", "onCompleted", "", "attemptTimestamp", HookHelper.constructorName, "(Lcom/discord/api/message/Message;Lcom/discord/models/commands/ApplicationCommandLocalSendData;Ljava/util/List;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class SendApplicationCommand extends MessageRequest {
        private final ApplicationCommandLocalSendData applicationCommandSendData;
        private final List<Attachment<?>> attachments;
        private final com.discord.api.message.Message message;
        private final Function1<SendUtils.SendPayload.Preprocessing, Unit> onPreprocessing;
        private final Function1<List<SendUtils.FileUpload>, Unit> onProgress;

        /* JADX WARN: Illegal instructions before constructor call */
        /* JADX WARN: Multi-variable type inference failed */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public SendApplicationCommand(com.discord.api.message.Message r8, com.discord.models.commands.ApplicationCommandLocalSendData r9, java.util.List<? extends com.lytefast.flexinput.model.Attachment<?>> r10, kotlin.jvm.functions.Function1<? super com.discord.utilities.rest.SendUtils.SendPayload.Preprocessing, kotlin.Unit> r11, kotlin.jvm.functions.Function1<? super java.util.List<com.discord.utilities.rest.SendUtils.FileUpload>, kotlin.Unit> r12, kotlin.jvm.functions.Function2<? super com.discord.utilities.messagesend.MessageResult, ? super java.lang.Boolean, kotlin.Unit> r13, long r14) {
            /*
                r7 = this;
                java.lang.String r0 = "message"
                d0.z.d.m.checkNotNullParameter(r8, r0)
                java.lang.String r0 = "applicationCommandSendData"
                d0.z.d.m.checkNotNullParameter(r9, r0)
                java.lang.String r0 = "onPreprocessing"
                d0.z.d.m.checkNotNullParameter(r11, r0)
                java.lang.String r0 = "onProgress"
                d0.z.d.m.checkNotNullParameter(r12, r0)
                java.lang.String r0 = "onCompleted"
                d0.z.d.m.checkNotNullParameter(r13, r0)
                java.lang.String r0 = r8.v()
                if (r0 == 0) goto L20
                goto L28
            L20:
                long r0 = r9.getNonce()
                java.lang.String r0 = java.lang.String.valueOf(r0)
            L28:
                r2 = r0
                r6 = 0
                r1 = r7
                r3 = r13
                r4 = r14
                r1.<init>(r2, r3, r4, r6)
                r7.message = r8
                r7.applicationCommandSendData = r9
                r7.attachments = r10
                r7.onPreprocessing = r11
                r7.onProgress = r12
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.messagesend.MessageRequest.SendApplicationCommand.<init>(com.discord.api.message.Message, com.discord.models.commands.ApplicationCommandLocalSendData, java.util.List, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function2, long):void");
        }

        public final ApplicationCommandLocalSendData getApplicationCommandSendData() {
            return this.applicationCommandSendData;
        }

        public final List<Attachment<?>> getAttachments() {
            return this.attachments;
        }

        public final com.discord.api.message.Message getMessage() {
            return this.message;
        }

        public final Function1<SendUtils.SendPayload.Preprocessing, Unit> getOnPreprocessing() {
            return this.onPreprocessing;
        }

        public final Function1<List<SendUtils.FileUpload>, Unit> getOnProgress() {
            return this.onProgress;
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    private MessageRequest(String str, Function2<? super MessageResult, ? super Boolean, Unit> function2, long j) {
        this.requestId = str;
        this.onCompleted = function2;
        this.attemptTimestamp = j;
    }

    public final long getAttemptTimestamp() {
        return this.attemptTimestamp;
    }

    public final Function2<MessageResult, Boolean, Unit> getOnCompleted() {
        return this.onCompleted;
    }

    public final String getRequestId() {
        return this.requestId;
    }

    public /* synthetic */ MessageRequest(String str, Function2 function2, long j, DefaultConstructorMarker defaultConstructorMarker) {
        this(str, function2, j);
    }
}
