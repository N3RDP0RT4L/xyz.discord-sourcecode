package com.discord.utilities.messagesend;

import andhook.lib.HookHelper;
import com.discord.api.message.Message;
import com.discord.utilities.error.Error;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: MessageQueue.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\n\u0004\u0005\u0006\u0007\b\t\n\u000b\f\rB\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\n\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017¨\u0006\u0018"}, d2 = {"Lcom/discord/utilities/messagesend/MessageResult;", "", HookHelper.constructorName, "()V", "CaptchaRequired", "NetworkFailure", "NoValidContent", "RateLimited", "Slowmode", "Success", "Timeout", "UnknownFailure", "UserCancelled", "ValidationError", "Lcom/discord/utilities/messagesend/MessageResult$Success;", "Lcom/discord/utilities/messagesend/MessageResult$Slowmode;", "Lcom/discord/utilities/messagesend/MessageResult$RateLimited;", "Lcom/discord/utilities/messagesend/MessageResult$UserCancelled;", "Lcom/discord/utilities/messagesend/MessageResult$NetworkFailure;", "Lcom/discord/utilities/messagesend/MessageResult$CaptchaRequired;", "Lcom/discord/utilities/messagesend/MessageResult$UnknownFailure;", "Lcom/discord/utilities/messagesend/MessageResult$ValidationError;", "Lcom/discord/utilities/messagesend/MessageResult$NoValidContent;", "Lcom/discord/utilities/messagesend/MessageResult$Timeout;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class MessageResult {

    /* compiled from: MessageQueue.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0007\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\t\u001a\u00020\b\u0012\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\r¢\u0006\u0004\b\u0012\u0010\u0013R\u001d\u0010\u0004\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007R\u0019\u0010\t\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\fR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011¨\u0006\u0014"}, d2 = {"Lcom/discord/utilities/messagesend/MessageResult$CaptchaRequired;", "Lcom/discord/utilities/messagesend/MessageResult;", "", "Lcom/discord/primitives/ChannelId;", "channelId", "J", "getChannelId", "()J", "Lcom/discord/utilities/error/Error;", "error", "Lcom/discord/utilities/error/Error;", "getError", "()Lcom/discord/utilities/error/Error;", "", "nonce", "Ljava/lang/String;", "getNonce", "()Ljava/lang/String;", HookHelper.constructorName, "(Lcom/discord/utilities/error/Error;JLjava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class CaptchaRequired extends MessageResult {
        private final long channelId;
        private final Error error;
        private final String nonce;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public CaptchaRequired(Error error, long j, String str) {
            super(null);
            m.checkNotNullParameter(error, "error");
            this.error = error;
            this.channelId = j;
            this.nonce = str;
        }

        public final long getChannelId() {
            return this.channelId;
        }

        public final Error getError() {
            return this.error;
        }

        public final String getNonce() {
            return this.nonce;
        }
    }

    /* compiled from: MessageQueue.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/utilities/messagesend/MessageResult$NetworkFailure;", "Lcom/discord/utilities/messagesend/MessageResult;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class NetworkFailure extends MessageResult {
        public static final NetworkFailure INSTANCE = new NetworkFailure();

        private NetworkFailure() {
            super(null);
        }
    }

    /* compiled from: MessageQueue.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/utilities/messagesend/MessageResult$NoValidContent;", "Lcom/discord/utilities/messagesend/MessageResult;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class NoValidContent extends MessageResult {
        public static final NoValidContent INSTANCE = new NoValidContent();

        private NoValidContent() {
            super(null);
        }
    }

    /* compiled from: MessageQueue.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/utilities/messagesend/MessageResult$RateLimited;", "Lcom/discord/utilities/messagesend/MessageResult;", "", "retryAfterMs", "J", "getRetryAfterMs", "()J", HookHelper.constructorName, "(J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class RateLimited extends MessageResult {
        private final long retryAfterMs;

        public RateLimited(long j) {
            super(null);
            this.retryAfterMs = j;
        }

        public final long getRetryAfterMs() {
            return this.retryAfterMs;
        }
    }

    /* compiled from: MessageQueue.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/utilities/messagesend/MessageResult$Slowmode;", "Lcom/discord/utilities/messagesend/MessageResult;", "", "cooldownMs", "J", "getCooldownMs", "()J", HookHelper.constructorName, "(J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Slowmode extends MessageResult {
        private final long cooldownMs;

        public Slowmode(long j) {
            super(null);
            this.cooldownMs = j;
        }

        public final long getCooldownMs() {
            return this.cooldownMs;
        }
    }

    /* compiled from: MessageQueue.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/utilities/messagesend/MessageResult$Success;", "Lcom/discord/utilities/messagesend/MessageResult;", "Lcom/discord/api/message/Message;", "message", "Lcom/discord/api/message/Message;", "getMessage", "()Lcom/discord/api/message/Message;", HookHelper.constructorName, "(Lcom/discord/api/message/Message;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Success extends MessageResult {
        private final Message message;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public Success(Message message) {
            super(null);
            m.checkNotNullParameter(message, "message");
            this.message = message;
        }

        public final Message getMessage() {
            return this.message;
        }
    }

    /* compiled from: MessageQueue.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/utilities/messagesend/MessageResult$Timeout;", "Lcom/discord/utilities/messagesend/MessageResult;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Timeout extends MessageResult {
        public static final Timeout INSTANCE = new Timeout();

        private Timeout() {
            super(null);
        }
    }

    /* compiled from: MessageQueue.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/utilities/messagesend/MessageResult$UnknownFailure;", "Lcom/discord/utilities/messagesend/MessageResult;", "Lcom/discord/utilities/error/Error;", "error", "Lcom/discord/utilities/error/Error;", "getError", "()Lcom/discord/utilities/error/Error;", HookHelper.constructorName, "(Lcom/discord/utilities/error/Error;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class UnknownFailure extends MessageResult {
        private final Error error;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public UnknownFailure(Error error) {
            super(null);
            m.checkNotNullParameter(error, "error");
            this.error = error;
        }

        public final Error getError() {
            return this.error;
        }
    }

    /* compiled from: MessageQueue.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/utilities/messagesend/MessageResult$UserCancelled;", "Lcom/discord/utilities/messagesend/MessageResult;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class UserCancelled extends MessageResult {
        public static final UserCancelled INSTANCE = new UserCancelled();

        private UserCancelled() {
            super(null);
        }
    }

    /* compiled from: MessageQueue.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/utilities/messagesend/MessageResult$ValidationError;", "Lcom/discord/utilities/messagesend/MessageResult;", "", "message", "Ljava/lang/String;", "getMessage", "()Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ValidationError extends MessageResult {
        private final String message;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ValidationError(String str) {
            super(null);
            m.checkNotNullParameter(str, "message");
            this.message = str;
        }

        public final String getMessage() {
            return this.message;
        }
    }

    private MessageResult() {
    }

    public /* synthetic */ MessageResult(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }
}
