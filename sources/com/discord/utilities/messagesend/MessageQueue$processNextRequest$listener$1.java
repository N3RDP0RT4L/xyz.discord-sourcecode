package com.discord.utilities.messagesend;

import com.discord.utilities.messagesend.MessageResult;
import com.discord.utilities.networking.Backoff;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import rx.Observable;
import rx.Subscription;
/* compiled from: MessageQueue.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/messagesend/MessageResult;", "result", "", "invoke", "(Lcom/discord/utilities/messagesend/MessageResult;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MessageQueue$processNextRequest$listener$1 extends o implements Function1<MessageResult, Unit> {
    public final /* synthetic */ MessageRequest $request;
    public final /* synthetic */ MessageQueue this$0;

    /* compiled from: MessageQueue.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Ljava/lang/Long;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.utilities.messagesend.MessageQueue$processNextRequest$listener$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<Long, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Long l) {
            invoke2(l);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Long l) {
            ExecutorService executorService;
            executorService = MessageQueue$processNextRequest$listener$1.this.this$0.executorService;
            executorService.submit(new Runnable() { // from class: com.discord.utilities.messagesend.MessageQueue.processNextRequest.listener.1.1.1
                @Override // java.lang.Runnable
                public final void run() {
                    ArrayDeque arrayDeque;
                    arrayDeque = MessageQueue$processNextRequest$listener$1.this.this$0.queue;
                    arrayDeque.addFirst(MessageQueue$processNextRequest$listener$1.this.$request);
                    MessageQueue$processNextRequest$listener$1.this.this$0.retrySubscription = null;
                    MessageQueue$processNextRequest$listener$1.this.this$0.onDrainingCompleted();
                    MessageQueue$processNextRequest$listener$1.this.this$0.processNextRequest();
                }
            });
        }
    }

    /* compiled from: MessageQueue.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lrx/Subscription;", "it", "", "invoke", "(Lrx/Subscription;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.utilities.messagesend.MessageQueue$processNextRequest$listener$1$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function1<Subscription, Unit> {
        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Subscription subscription) {
            invoke2(subscription);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Subscription subscription) {
            m.checkNotNullParameter(subscription, "it");
            MessageQueue$processNextRequest$listener$1.this.this$0.retrySubscription = subscription;
        }
    }

    /* compiled from: MessageQueue.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "run", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.utilities.messagesend.MessageQueue$processNextRequest$listener$1$3  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass3 implements Runnable {

        /* compiled from: MessageQueue.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.utilities.messagesend.MessageQueue$processNextRequest$listener$1$3$1  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass1 extends o implements Function0<Unit> {
            public AnonymousClass1() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                ExecutorService executorService;
                executorService = MessageQueue$processNextRequest$listener$1.this.this$0.executorService;
                executorService.submit(new Runnable() { // from class: com.discord.utilities.messagesend.MessageQueue.processNextRequest.listener.1.3.1.1
                    @Override // java.lang.Runnable
                    public final void run() {
                        MessageQueue$processNextRequest$listener$1.this.this$0.processNextRequest();
                    }
                });
            }
        }

        public AnonymousClass3() {
        }

        @Override // java.lang.Runnable
        public final void run() {
            ArrayDeque arrayDeque;
            Backoff backoff;
            MessageQueue$processNextRequest$listener$1.this.this$0.onDrainingCompleted();
            arrayDeque = MessageQueue$processNextRequest$listener$1.this.this$0.queue;
            arrayDeque.addFirst(MessageQueue$processNextRequest$listener$1.this.$request);
            backoff = MessageQueue$processNextRequest$listener$1.this.this$0.networkBackoff;
            backoff.fail(new AnonymousClass1());
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MessageQueue$processNextRequest$listener$1(MessageQueue messageQueue, MessageRequest messageRequest) {
        super(1);
        this.this$0 = messageQueue;
        this.$request = messageRequest;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(MessageResult messageResult) {
        invoke2(messageResult);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(final MessageResult messageResult) {
        ExecutorService executorService;
        ExecutorService executorService2;
        m.checkNotNullParameter(messageResult, "result");
        if (messageResult instanceof MessageResult.RateLimited) {
            Observable<Long> d02 = Observable.d0(((MessageResult.RateLimited) messageResult).getRetryAfterMs(), TimeUnit.MILLISECONDS);
            m.checkNotNullExpressionValue(d02, "Observable\n             …s, TimeUnit.MILLISECONDS)");
            ObservableExtensionsKt.appSubscribe(d02, this.this$0.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new AnonymousClass2(), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
        } else if (messageResult instanceof MessageResult.NetworkFailure) {
            executorService2 = this.this$0.executorService;
            executorService2.submit(new AnonymousClass3());
        } else {
            executorService = this.this$0.executorService;
            executorService.submit(new Runnable() { // from class: com.discord.utilities.messagesend.MessageQueue$processNextRequest$listener$1.4
                @Override // java.lang.Runnable
                public final void run() {
                    ArrayDeque arrayDeque;
                    Backoff backoff;
                    MessageQueue$processNextRequest$listener$1.this.this$0.onDrainingCompleted();
                    Function2<MessageResult, Boolean, Unit> onCompleted = MessageQueue$processNextRequest$listener$1.this.$request.getOnCompleted();
                    MessageResult messageResult2 = messageResult;
                    arrayDeque = MessageQueue$processNextRequest$listener$1.this.this$0.queue;
                    onCompleted.invoke(messageResult2, Boolean.valueOf(arrayDeque.isEmpty()));
                    backoff = MessageQueue$processNextRequest$listener$1.this.this$0.networkBackoff;
                    backoff.succeed();
                    MessageQueue$processNextRequest$listener$1.this.this$0.processNextRequest();
                }
            });
        }
    }
}
