package com.discord.utilities.messagesend;

import andhook.lib.HookHelper;
import android.content.ContentResolver;
import com.discord.api.activity.Activity;
import com.discord.api.application.Application;
import com.discord.api.message.MessageReference;
import com.discord.api.message.activity.MessageActivity;
import com.discord.api.message.allowedmentions.MessageAllowedMentions;
import com.discord.api.sticker.Sticker;
import com.discord.api.sticker.StickerPartial;
import com.discord.models.message.Message;
import com.discord.restapi.PayloadJSON;
import com.discord.restapi.RestAPIParams;
import com.discord.utilities.captcha.CaptchaHelper;
import com.discord.utilities.error.Error;
import com.discord.utilities.messagesend.MessageQueue;
import com.discord.utilities.messagesend.MessageRequest;
import com.discord.utilities.messagesend.MessageResult;
import com.discord.utilities.networking.Backoff;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rest.SendUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.Clock;
import d0.t.o;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import okhttp3.MultipartBody;
import org.objectweb.asm.Opcodes;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
/* compiled from: MessageQueue.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0096\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 A2\u00020\u0001:\u0003ABCB\u001f\u0012\u0006\u0010+\u001a\u00020*\u0012\u0006\u00104\u001a\u000203\u0012\u0006\u0010:\u001a\u000209¢\u0006\u0004\b?\u0010@J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0004J\u001f\u0010\n\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u001f\u0010\u000e\u001a\u00020\u00022\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\u0012\u001a\u00020\u00022\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u001f\u0010\u0016\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u00142\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\u0016\u0010\u0017J+\u0010\u001d\u001a\u00020\u00022\u0006\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\b2\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u001bH\u0002¢\u0006\u0004\b\u001d\u0010\u001eJ\u0015\u0010 \u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u001f¢\u0006\u0004\b \u0010!J\u0015\u0010$\u001a\u00020\u00022\u0006\u0010#\u001a\u00020\"¢\u0006\u0004\b$\u0010%J\r\u0010&\u001a\u00020\u0002¢\u0006\u0004\b&\u0010\u0004R\u001c\u0010(\u001a\b\u0012\u0004\u0012\u00020\u001f0'8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u0010)R\u0016\u0010+\u001a\u00020*8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b+\u0010,R\u0018\u0010.\u001a\u0004\u0018\u00010-8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b.\u0010/R\u0016\u00101\u001a\u0002008\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b1\u00102R\u0016\u00104\u001a\u0002038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b4\u00105R\u0016\u00107\u001a\u0002068\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b7\u00108R\u0016\u0010:\u001a\u0002098\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b:\u0010;R\u0018\u0010=\u001a\u0004\u0018\u00010<8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b=\u0010>¨\u0006D"}, d2 = {"Lcom/discord/utilities/messagesend/MessageQueue;", "", "", "processNextRequest", "()V", "onDrainingCompleted", "Lcom/discord/utilities/messagesend/MessageRequest$Send;", "request", "Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;", "drainListener", "doSend", "(Lcom/discord/utilities/messagesend/MessageRequest$Send;Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;)V", "Lcom/discord/utilities/messagesend/MessageRequest$Edit;", "editRequest", "doEdit", "(Lcom/discord/utilities/messagesend/MessageRequest$Edit;Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;)V", "Lcom/discord/utilities/messagesend/MessageRequest$SendApplicationCommand;", "sendApplicationCommandRequest", "doSendApplicationCommand", "(Lcom/discord/utilities/messagesend/MessageRequest$SendApplicationCommand;Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;)V", "Lcom/discord/api/message/Message;", "message", "handleSuccess", "(Lcom/discord/api/message/Message;Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;)V", "Lcom/discord/utilities/error/Error;", "error", "onDrainListener", "Lcom/discord/models/message/Message;", "clientMessage", "handleError", "(Lcom/discord/utilities/error/Error;Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;Lcom/discord/models/message/Message;)V", "Lcom/discord/utilities/messagesend/MessageRequest;", "enqueue", "(Lcom/discord/utilities/messagesend/MessageRequest;)V", "", "requestId", "cancel", "(Ljava/lang/String;)V", "handleConnected", "Ljava/util/ArrayDeque;", "queue", "Ljava/util/ArrayDeque;", "Landroid/content/ContentResolver;", "contentResolver", "Landroid/content/ContentResolver;", "Lrx/Subscription;", "retrySubscription", "Lrx/Subscription;", "", "isDraining", "Z", "Ljava/util/concurrent/ExecutorService;", "executorService", "Ljava/util/concurrent/ExecutorService;", "Lcom/discord/utilities/networking/Backoff;", "networkBackoff", "Lcom/discord/utilities/networking/Backoff;", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "Lcom/discord/utilities/messagesend/MessageQueue$InflightRequest;", "inFlightRequest", "Lcom/discord/utilities/messagesend/MessageQueue$InflightRequest;", HookHelper.constructorName, "(Landroid/content/ContentResolver;Ljava/util/concurrent/ExecutorService;Lcom/discord/utilities/time/Clock;)V", "Companion", "DrainListener", "InflightRequest", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MessageQueue {
    private static final Companion Companion = new Companion(null);
    @Deprecated
    private static final long DEFAULT_MESSAGE_TIMEOUT_MS = 3600000;
    @Deprecated
    private static final long DEFAULT_NETWORK_INITIAL_FAILURE_RETRY_MS = 5000;
    @Deprecated
    private static final long DEFAULT_RETRY_MS = 100;
    private final Clock clock;
    private final ContentResolver contentResolver;
    private final ExecutorService executorService;
    private InflightRequest inFlightRequest;
    private boolean isDraining;
    private Subscription retrySubscription;
    private final ArrayDeque<MessageRequest> queue = new ArrayDeque<>();
    private final Backoff networkBackoff = new Backoff(5000, DEFAULT_MESSAGE_TIMEOUT_MS, 0, false, null, 28, null);

    /* compiled from: MessageQueue.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0007\b\u0082\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004¨\u0006\t"}, d2 = {"Lcom/discord/utilities/messagesend/MessageQueue$Companion;", "", "", "DEFAULT_MESSAGE_TIMEOUT_MS", "J", "DEFAULT_NETWORK_INITIAL_FAILURE_RETRY_MS", "DEFAULT_RETRY_MS", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: MessageQueue.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\u00020\u0001B\u001b\u0012\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\n¢\u0006\u0004\b\r\u0010\u000eJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\b\u0010\tR\"\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u000f"}, d2 = {"Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;", "", "Lcom/discord/utilities/messagesend/MessageResult;", "result", "", "complete", "(Lcom/discord/utilities/messagesend/MessageResult;)V", "Ljava/util/concurrent/atomic/AtomicBoolean;", "isCompleted", "Ljava/util/concurrent/atomic/AtomicBoolean;", "Lkotlin/Function1;", "onCompleted", "Lkotlin/jvm/functions/Function1;", HookHelper.constructorName, "(Lkotlin/jvm/functions/Function1;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class DrainListener {
        private AtomicBoolean isCompleted = new AtomicBoolean(false);
        private final Function1<MessageResult, Unit> onCompleted;

        /* JADX WARN: Multi-variable type inference failed */
        public DrainListener(Function1<? super MessageResult, Unit> function1) {
            m.checkNotNullParameter(function1, "onCompleted");
            this.onCompleted = function1;
        }

        public final synchronized void complete(MessageResult messageResult) {
            m.checkNotNullParameter(messageResult, "result");
            if (!this.isCompleted.getAndSet(true)) {
                this.onCompleted.invoke(messageResult);
            }
        }
    }

    /* compiled from: MessageQueue.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0002\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0019\u0010\b\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR\u0019\u0010\r\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u0013"}, d2 = {"Lcom/discord/utilities/messagesend/MessageQueue$InflightRequest;", "", "Lcom/discord/utilities/messagesend/MessageRequest;", "baseRequest", "Lcom/discord/utilities/messagesend/MessageRequest;", "getBaseRequest", "()Lcom/discord/utilities/messagesend/MessageRequest;", "Lrx/Subscription;", "networkSubscription", "Lrx/Subscription;", "getNetworkSubscription", "()Lrx/Subscription;", "Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;", "drainListener", "Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;", "getDrainListener", "()Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;", HookHelper.constructorName, "(Lcom/discord/utilities/messagesend/MessageRequest;Lrx/Subscription;Lcom/discord/utilities/messagesend/MessageQueue$DrainListener;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class InflightRequest {
        private final MessageRequest baseRequest;
        private final DrainListener drainListener;
        private final Subscription networkSubscription;

        public InflightRequest(MessageRequest messageRequest, Subscription subscription, DrainListener drainListener) {
            m.checkNotNullParameter(messageRequest, "baseRequest");
            m.checkNotNullParameter(subscription, "networkSubscription");
            m.checkNotNullParameter(drainListener, "drainListener");
            this.baseRequest = messageRequest;
            this.networkSubscription = subscription;
            this.drainListener = drainListener;
        }

        public final MessageRequest getBaseRequest() {
            return this.baseRequest;
        }

        public final DrainListener getDrainListener() {
            return this.drainListener;
        }

        public final Subscription getNetworkSubscription() {
            return this.networkSubscription;
        }
    }

    public MessageQueue(ContentResolver contentResolver, ExecutorService executorService, Clock clock) {
        m.checkNotNullParameter(contentResolver, "contentResolver");
        m.checkNotNullParameter(executorService, "executorService");
        m.checkNotNullParameter(clock, "clock");
        this.contentResolver = contentResolver;
        this.executorService = executorService;
        this.clock = clock;
    }

    private final void doEdit(MessageRequest.Edit edit, DrainListener drainListener) {
        RestAPI api = RestAPI.Companion.getApi();
        long channelId = edit.getChannelId();
        long messageId = edit.getMessageId();
        String content = edit.getContent();
        MessageAllowedMentions allowedMentions = edit.getAllowedMentions();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn(api.editMessage(channelId, messageId, new RestAPIParams.Message(content, null, null, null, null, null, allowedMentions != null ? RestAPIParams.Message.AllowedMentions.Companion.create(allowedMentions) : null, null, Opcodes.NEWARRAY, null)), false), MessageQueue.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new MessageQueue$doEdit$4(this, edit, drainListener), (r18 & 8) != 0 ? null : new MessageQueue$doEdit$3(this, drainListener), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new MessageQueue$doEdit$2(this, drainListener));
    }

    private final void doSend(final MessageRequest.Send send, DrainListener drainListener) {
        RestAPIParams.Message.Activity activity;
        RestAPIParams.Message.MessageReference messageReference;
        String k;
        MessageResult.ValidationError validateMessage = send.validateMessage();
        if (validateMessage != null) {
            drainListener.complete(validateMessage);
            return;
        }
        final Message message = send.getMessage();
        boolean z2 = message.getNonce() != null;
        MessageActivity activity2 = message.getActivity();
        ArrayList arrayList = null;
        if (activity2 != null) {
            Activity activity3 = send.getActivity();
            activity = (activity3 == null || (k = activity3.k()) == null) ? null : new RestAPIParams.Message.Activity(activity2.b(), activity2.a(), k);
        } else {
            activity = null;
        }
        MessageReference messageReference2 = message.getMessageReference();
        if (messageReference2 != null) {
            Long b2 = messageReference2.b();
            Long a = messageReference2.a();
            m.checkNotNull(a);
            messageReference = new RestAPIParams.Message.MessageReference(b2, a.longValue(), messageReference2.c());
        } else {
            messageReference = null;
        }
        MessageAllowedMentions allowedMentions = message.getAllowedMentions();
        RestAPIParams.Message.AllowedMentions create = allowedMentions != null ? RestAPIParams.Message.AllowedMentions.Companion.create(allowedMentions) : null;
        String content = message.getContent();
        String nonce = message.getNonce();
        Application application = message.getApplication();
        Long valueOf = application != null ? Long.valueOf(application.g()) : null;
        List<StickerPartial> stickerItems = message.getStickerItems();
        if (stickerItems != null) {
            arrayList = new ArrayList(o.collectionSizeOrDefault(stickerItems, 10));
            for (StickerPartial stickerPartial : stickerItems) {
                arrayList.add(Long.valueOf(stickerPartial.getId()));
            }
        } else {
            List<Sticker> stickers = message.getStickers();
            if (stickers != null) {
                arrayList = new ArrayList(o.collectionSizeOrDefault(stickers, 10));
                for (Sticker sticker : stickers) {
                    arrayList.add(Long.valueOf(sticker.getId()));
                }
            }
        }
        Observable<SendUtils.SendPayload> t = SendUtils.INSTANCE.getSendPayload(this.contentResolver, new RestAPIParams.Message(content, nonce, valueOf, activity, arrayList, messageReference, create, message.getCaptchaKey()), send.getAttachments()).t(new Action1<SendUtils.SendPayload>() { // from class: com.discord.utilities.messagesend.MessageQueue$doSend$1
            public final void call(SendUtils.SendPayload sendPayload) {
                if (sendPayload instanceof SendUtils.SendPayload.Preprocessing) {
                    MessageRequest.Send.this.getOnPreprocessing().invoke(sendPayload);
                } else if (sendPayload instanceof SendUtils.SendPayload.ReadyToSend) {
                    SendUtils.SendPayload.ReadyToSend readyToSend = (SendUtils.SendPayload.ReadyToSend) sendPayload;
                    if (!readyToSend.getUploads().isEmpty()) {
                        MessageRequest.Send.this.getOnProgress().invoke(readyToSend.getUploads());
                    }
                }
            }
        });
        m.checkNotNullExpressionValue(t, "SendUtils\n        .getSe…  }\n          }\n        }");
        Observable<R> F = t.x(MessageQueue$doSend$$inlined$filterIs$1.INSTANCE).F(MessageQueue$doSend$$inlined$filterIs$2.INSTANCE);
        m.checkNotNullExpressionValue(F, "filter { it is T }.map { it as T }");
        Observable z3 = F.Z(1).z(new b<SendUtils.SendPayload.ReadyToSend, Observable<? extends com.discord.api.message.Message>>() { // from class: com.discord.utilities.messagesend.MessageQueue$doSend$2
            public final Observable<? extends com.discord.api.message.Message> call(SendUtils.SendPayload.ReadyToSend readyToSend) {
                List<SendUtils.FileUpload> uploads = readyToSend.getUploads();
                ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(uploads, 10));
                for (SendUtils.FileUpload fileUpload : uploads) {
                    arrayList2.add(fileUpload.getPart());
                }
                if (!(!arrayList2.isEmpty())) {
                    return RestAPI.Companion.getApi().sendMessage(Message.this.getChannelId(), readyToSend.getMessage());
                }
                RestAPI api = RestAPI.Companion.getApi();
                long channelId = Message.this.getChannelId();
                PayloadJSON<RestAPIParams.Message> payloadJSON = new PayloadJSON<>(readyToSend.getMessage());
                Object[] array = arrayList2.toArray(new MultipartBody.Part[0]);
                Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
                return api.sendMessage(channelId, payloadJSON, (MultipartBody.Part[]) array);
            }
        });
        m.checkNotNullExpressionValue(z3, "SendUtils\n        .getSe…ge)\n          }\n        }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn(z3, z2), MessageQueue.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new MessageQueue$doSend$5(this, send, drainListener), (r18 & 8) != 0 ? null : new MessageQueue$doSend$4(this, drainListener, message), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new MessageQueue$doSend$3(this, drainListener));
    }

    private final void doSendApplicationCommand(final MessageRequest.SendApplicationCommand sendApplicationCommand, DrainListener drainListener) {
        Observable<SendUtils.SendPayload> t = SendUtils.INSTANCE.getSendCommandPayload(this.contentResolver, sendApplicationCommand.getApplicationCommandSendData(), sendApplicationCommand.getAttachments()).t(new Action1<SendUtils.SendPayload>() { // from class: com.discord.utilities.messagesend.MessageQueue$doSendApplicationCommand$1
            public final void call(SendUtils.SendPayload sendPayload) {
                if (sendPayload instanceof SendUtils.SendPayload.Preprocessing) {
                    MessageRequest.SendApplicationCommand.this.getOnPreprocessing().invoke(sendPayload);
                } else if (sendPayload instanceof SendUtils.SendPayload.ReadyToSendCommand) {
                    SendUtils.SendPayload.ReadyToSendCommand readyToSendCommand = (SendUtils.SendPayload.ReadyToSendCommand) sendPayload;
                    if (!readyToSendCommand.getUploads().isEmpty()) {
                        MessageRequest.SendApplicationCommand.this.getOnProgress().invoke(readyToSendCommand.getUploads());
                    }
                }
            }
        });
        m.checkNotNullExpressionValue(t, "SendUtils\n        .getSe…  }\n          }\n        }");
        Observable<R> F = t.x(MessageQueue$doSendApplicationCommand$$inlined$filterIs$1.INSTANCE).F(MessageQueue$doSendApplicationCommand$$inlined$filterIs$2.INSTANCE);
        m.checkNotNullExpressionValue(F, "filter { it is T }.map { it as T }");
        Observable z2 = F.y().z(MessageQueue$doSendApplicationCommand$2.INSTANCE);
        m.checkNotNullExpressionValue(z2, "SendUtils\n        .getSe…())\n          }\n        }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn(z2, false), MessageQueue.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new MessageQueue$doSendApplicationCommand$5(this, sendApplicationCommand, drainListener), (r18 & 8) != 0 ? null : new MessageQueue$doSendApplicationCommand$4(this, drainListener), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new MessageQueue$doSendApplicationCommand$3(this, sendApplicationCommand, drainListener));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleError(Error error, DrainListener drainListener, Message message) {
        MessageResult messageResult;
        Error.Response response = error.getResponse();
        m.checkNotNullExpressionValue(response, "error.response");
        Long l = 100L;
        if (response.getCode() == 20016) {
            Error.Response response2 = error.getResponse();
            m.checkNotNullExpressionValue(response2, "error.response");
            Long retryAfterMs = response2.getRetryAfterMs();
            if (retryAfterMs != null) {
                l = retryAfterMs;
            }
            m.checkNotNullExpressionValue(l, "error.response.retryAfterMs ?: DEFAULT_RETRY_MS");
            messageResult = new MessageResult.Slowmode(l.longValue());
        } else if (error.getType() == Error.Type.RATE_LIMITED) {
            Error.Response response3 = error.getResponse();
            m.checkNotNullExpressionValue(response3, "error.response");
            Long retryAfterMs2 = response3.getRetryAfterMs();
            if (retryAfterMs2 != null) {
                l = retryAfterMs2;
            }
            m.checkNotNullExpressionValue(l, "error.response.retryAfterMs ?: DEFAULT_RETRY_MS");
            messageResult = new MessageResult.RateLimited(l.longValue());
        } else if (error.getType() == Error.Type.NETWORK) {
            messageResult = MessageResult.NetworkFailure.INSTANCE;
        } else {
            if (error.getType() == Error.Type.DISCORD_BAD_REQUEST) {
                Error.Response response4 = error.getResponse();
                m.checkNotNullExpressionValue(response4, "error.response");
                if (response4.getMessages().containsKey(CaptchaHelper.CAPTCHA_KEY) && message != null) {
                    messageResult = new MessageResult.CaptchaRequired(error, message.getChannelId(), message.getNonce());
                }
            }
            messageResult = new MessageResult.UnknownFailure(error);
        }
        drainListener.complete(messageResult);
    }

    public static /* synthetic */ void handleError$default(MessageQueue messageQueue, Error error, DrainListener drainListener, Message message, int i, Object obj) {
        if ((i & 4) != 0) {
            message = null;
        }
        messageQueue.handleError(error, drainListener, message);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleSuccess(com.discord.api.message.Message message, DrainListener drainListener) {
        drainListener.complete(new MessageResult.Success(message));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onDrainingCompleted() {
        this.isDraining = false;
        this.inFlightRequest = null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void processNextRequest() {
        MessageRequest remove;
        if (this.queue.isEmpty() || this.retrySubscription != null || this.networkBackoff.isPending() || this.isDraining || (remove = this.queue.remove()) == null) {
            return;
        }
        if (this.clock.currentTimeMillis() - remove.getAttemptTimestamp() > DEFAULT_MESSAGE_TIMEOUT_MS) {
            remove.getOnCompleted().invoke(MessageResult.Timeout.INSTANCE, Boolean.valueOf(this.queue.isEmpty()));
            this.networkBackoff.succeed();
            processNextRequest();
            return;
        }
        this.isDraining = true;
        DrainListener drainListener = new DrainListener(new MessageQueue$processNextRequest$listener$1(this, remove));
        if (remove instanceof MessageRequest.Send) {
            doSend((MessageRequest.Send) remove, drainListener);
        } else if (remove instanceof MessageRequest.Edit) {
            doEdit((MessageRequest.Edit) remove, drainListener);
        } else if (remove instanceof MessageRequest.SendApplicationCommand) {
            doSendApplicationCommand((MessageRequest.SendApplicationCommand) remove, drainListener);
        }
    }

    public final void cancel(final String str) {
        m.checkNotNullParameter(str, "requestId");
        this.executorService.submit(new Runnable() { // from class: com.discord.utilities.messagesend.MessageQueue$cancel$1
            @Override // java.lang.Runnable
            public final void run() {
                MessageQueue.InflightRequest inflightRequest;
                ArrayDeque arrayDeque;
                Object obj;
                ArrayDeque arrayDeque2;
                ArrayDeque arrayDeque3;
                inflightRequest = MessageQueue.this.inFlightRequest;
                if (inflightRequest == null || !m.areEqual(inflightRequest.getBaseRequest().getRequestId(), str)) {
                    arrayDeque = MessageQueue.this.queue;
                    Iterator it = arrayDeque.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            obj = null;
                            break;
                        }
                        obj = it.next();
                        if (m.areEqual(((MessageRequest) obj).getRequestId(), str)) {
                            break;
                        }
                    }
                    MessageRequest messageRequest = (MessageRequest) obj;
                    if (messageRequest != null) {
                        arrayDeque2 = MessageQueue.this.queue;
                        arrayDeque2.remove(messageRequest);
                        Function2<MessageResult, Boolean, Unit> onCompleted = messageRequest.getOnCompleted();
                        MessageResult.UserCancelled userCancelled = MessageResult.UserCancelled.INSTANCE;
                        arrayDeque3 = MessageQueue.this.queue;
                        onCompleted.invoke(userCancelled, Boolean.valueOf(arrayDeque3.isEmpty()));
                        return;
                    }
                    return;
                }
                inflightRequest.getNetworkSubscription().unsubscribe();
                inflightRequest.getDrainListener().complete(MessageResult.UserCancelled.INSTANCE);
            }
        });
    }

    public final void enqueue(final MessageRequest messageRequest) {
        m.checkNotNullParameter(messageRequest, "request");
        this.executorService.submit(new Runnable() { // from class: com.discord.utilities.messagesend.MessageQueue$enqueue$1
            @Override // java.lang.Runnable
            public final void run() {
                ArrayDeque arrayDeque;
                arrayDeque = MessageQueue.this.queue;
                arrayDeque.add(messageRequest);
                MessageQueue.this.processNextRequest();
            }
        });
    }

    public final void handleConnected() {
        this.executorService.submit(new Runnable() { // from class: com.discord.utilities.messagesend.MessageQueue$handleConnected$1
            @Override // java.lang.Runnable
            public final void run() {
                Backoff backoff;
                Backoff backoff2;
                backoff = MessageQueue.this.networkBackoff;
                if (backoff.isPending()) {
                    backoff2 = MessageQueue.this.networkBackoff;
                    backoff2.cancel();
                    MessageQueue.this.processNextRequest();
                }
            }
        });
    }
}
