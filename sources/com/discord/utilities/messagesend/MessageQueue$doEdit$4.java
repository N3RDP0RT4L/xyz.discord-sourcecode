package com.discord.utilities.messagesend;

import com.discord.utilities.analytics.Traits;
import com.discord.utilities.messagesend.MessageQueue;
import com.discord.utilities.messagesend.MessageRequest;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.Subscription;
/* compiled from: MessageQueue.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lrx/Subscription;", Traits.Payment.Type.SUBSCRIPTION, "", "invoke", "(Lrx/Subscription;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MessageQueue$doEdit$4 extends o implements Function1<Subscription, Unit> {
    public final /* synthetic */ MessageQueue.DrainListener $drainListener;
    public final /* synthetic */ MessageRequest.Edit $editRequest;
    public final /* synthetic */ MessageQueue this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MessageQueue$doEdit$4(MessageQueue messageQueue, MessageRequest.Edit edit, MessageQueue.DrainListener drainListener) {
        super(1);
        this.this$0 = messageQueue;
        this.$editRequest = edit;
        this.$drainListener = drainListener;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Subscription subscription) {
        invoke2(subscription);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Subscription subscription) {
        m.checkNotNullParameter(subscription, Traits.Payment.Type.SUBSCRIPTION);
        this.this$0.inFlightRequest = new MessageQueue.InflightRequest(this.$editRequest, subscription, this.$drainListener);
    }
}
