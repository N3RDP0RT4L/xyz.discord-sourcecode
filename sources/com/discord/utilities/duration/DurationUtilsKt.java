package com.discord.utilities.duration;

import android.content.Context;
import b.a.k.b;
import com.discord.utilities.resources.StringResourceUtilsKt;
import d0.a0.a;
import d0.d0.f;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: DurationUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0003\u001a\u001d\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006\u001a\u001d\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\u0006\u001a\u001d\u0010\b\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\b\u0010\u0006\"\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\f"}, d2 = {"Landroid/content/Context;", "context", "", "durationMillis", "", "humanizeDuration", "(Landroid/content/Context;J)Ljava/lang/CharSequence;", "humanizeDurationRounded", "humanizeCountdownDuration", "", "FEW_SECONDS_BOUNDARY", "I", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class DurationUtilsKt {
    private static final int FEW_SECONDS_BOUNDARY = 30;

    public static final CharSequence humanizeCountdownDuration(Context context, long j) {
        CharSequence b2;
        CharSequence b3;
        CharSequence b4;
        CharSequence b5;
        CharSequence b6;
        m.checkNotNullParameter(context, "context");
        float f = (float) j;
        int i = (int) (f / ((float) 86400000));
        int i2 = ((int) (f / ((float) 3600000))) % 24;
        int coerceAtLeast = f.coerceAtLeast(((int) (f / ((float) 1000))) % 60, 1);
        StringBuilder sb = new StringBuilder();
        sb.append(((int) (f / ((float) 60000))) % 60);
        b2 = b.b(context, R.string.countdown_units_minutes, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        sb.append(b2);
        sb.append(' ');
        sb.append(coerceAtLeast);
        b3 = b.b(context, R.string.countdown_units_seconds, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        sb.append(b3);
        String sb2 = sb.toString();
        if (i > 0) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append(i);
            b5 = b.b(context, R.string.countdown_units_days, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            sb3.append(b5);
            sb3.append(' ');
            sb3.append(i2);
            b6 = b.b(context, R.string.countdown_units_hours, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            sb3.append(b6);
            sb3.append(' ');
            sb3.append(sb2);
            return sb3.toString();
        } else if (i2 <= 0) {
            return sb2;
        } else {
            StringBuilder sb4 = new StringBuilder();
            sb4.append(i2);
            b4 = b.b(context, R.string.countdown_units_hours, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            sb4.append(b4);
            sb4.append(' ');
            sb4.append(sb2);
            return sb4.toString();
        }
    }

    public static final CharSequence humanizeDuration(Context context, long j) {
        CharSequence b2;
        CharSequence b3;
        CharSequence b4;
        CharSequence b5;
        m.checkNotNullParameter(context, "context");
        if (j <= 30000) {
            String string = context.getString(R.string.humanize_duration_a_few_seconds);
            m.checkNotNullExpressionValue(string, "context.getString(\n     …ation_a_few_seconds\n    )");
            return string;
        } else if (j <= 60000) {
            int i = (int) (j / 1000);
            b5 = b.b(context, R.string.humanize_duration_seconds, new Object[]{StringResourceUtilsKt.getI18nPluralString(context, R.plurals.humanize_duration_seconds_time, i, Integer.valueOf(i))}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b5;
        } else if (j <= 3600000) {
            int i2 = (int) (j / 60000);
            b4 = b.b(context, R.string.humanize_duration_minutes, new Object[]{StringResourceUtilsKt.getI18nPluralString(context, R.plurals.humanize_duration_minutes_time, i2, Integer.valueOf(i2))}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b4;
        } else if (j <= 86400000) {
            int i3 = (int) (j / 3600000);
            b3 = b.b(context, R.string.humanize_duration_hours, new Object[]{StringResourceUtilsKt.getI18nPluralString(context, R.plurals.humanize_duration_hours_time, i3, Integer.valueOf(i3))}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b3;
        } else {
            int i4 = (int) (j / 86400000);
            b2 = b.b(context, R.string.humanize_duration_days, new Object[]{StringResourceUtilsKt.getI18nPluralString(context, R.plurals.humanize_duration_days_time, i4, Integer.valueOf(i4))}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b2;
        }
    }

    public static final CharSequence humanizeDurationRounded(Context context, long j) {
        CharSequence b2;
        CharSequence b3;
        CharSequence b4;
        CharSequence b5;
        m.checkNotNullParameter(context, "context");
        if (j <= 30000) {
            String string = context.getString(R.string.humanize_duration_a_few_seconds);
            m.checkNotNullExpressionValue(string, "context.getString(\n     …ation_a_few_seconds\n    )");
            return string;
        }
        double d = j;
        if (d <= 54000.0d) {
            float f = ((float) j) / ((float) 1000);
            b5 = b.b(context, R.string.humanize_duration_seconds, new Object[]{StringResourceUtilsKt.getI18nPluralString(context, R.plurals.humanize_duration_seconds_time, a.roundToInt(f), Integer.valueOf(a.roundToInt(f)))}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b5;
        } else if (d <= 3240000.0d) {
            float f2 = ((float) j) / ((float) 60000);
            b4 = b.b(context, R.string.humanize_duration_minutes, new Object[]{StringResourceUtilsKt.getI18nPluralString(context, R.plurals.humanize_duration_minutes_time, a.roundToInt(f2), Integer.valueOf(a.roundToInt(f2)))}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b4;
        } else if (d <= 7.776E7d) {
            float f3 = ((float) j) / ((float) 3600000);
            b3 = b.b(context, R.string.humanize_duration_hours, new Object[]{StringResourceUtilsKt.getI18nPluralString(context, R.plurals.humanize_duration_hours_time, a.roundToInt(f3), Integer.valueOf(a.roundToInt(f3)))}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b3;
        } else {
            float f4 = ((float) j) / ((float) 86400000);
            b2 = b.b(context, R.string.humanize_duration_days, new Object[]{StringResourceUtilsKt.getI18nPluralString(context, R.plurals.humanize_duration_days_time, a.roundToInt(f4), Integer.valueOf(a.roundToInt(f4)))}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b2;
        }
    }
}
