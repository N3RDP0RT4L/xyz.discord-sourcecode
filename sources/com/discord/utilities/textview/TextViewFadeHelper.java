package com.discord.utilities.textview;

import andhook.lib.HookHelper;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.text.Editable;
import android.text.Layout;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.style.CharacterStyle;
import android.text.style.UpdateAppearance;
import android.view.View;
import android.widget.TextView;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: TextViewFadeHelper.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0007\n\u0002\b\n\u0018\u00002\u00020\u0001:\u0001\u0019B\u000f\u0012\u0006\u0010\b\u001a\u00020\u0005¢\u0006\u0004\b\u0017\u0010\u0018J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\r\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\u0006\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u0007R\u001d\u0010\f\u001a\u00060\u000bR\u00020\u00008\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000fR$\u0010\u0011\u001a\u0004\u0018\u00010\u00108\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016¨\u0006\u001a"}, d2 = {"Lcom/discord/utilities/textview/TextViewFadeHelper;", "", "", "updateFade", "()V", "Landroid/widget/TextView;", "registerFadeHelper", "()Landroid/widget/TextView;", "textView", "Landroid/widget/TextView;", "getTextView", "Lcom/discord/utilities/textview/TextViewFadeHelper$LinearGradientSpan;", "span", "Lcom/discord/utilities/textview/TextViewFadeHelper$LinearGradientSpan;", "getSpan", "()Lcom/discord/utilities/textview/TextViewFadeHelper$LinearGradientSpan;", "", "lineWidth", "Ljava/lang/Float;", "getLineWidth", "()Ljava/lang/Float;", "setLineWidth", "(Ljava/lang/Float;)V", HookHelper.constructorName, "(Landroid/widget/TextView;)V", "LinearGradientSpan", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class TextViewFadeHelper {
    private Float lineWidth;
    private final LinearGradientSpan span = new LinearGradientSpan();
    private final TextView textView;

    /* compiled from: TextViewFadeHelper.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\u0004\u0018\u00002\u00020\u00012\u00020\u0002B\u0007¢\u0006\u0004\b\b\u0010\tJ\u0019\u0010\u0006\u001a\u00020\u00052\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003H\u0016¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/utilities/textview/TextViewFadeHelper$LinearGradientSpan;", "Landroid/text/style/CharacterStyle;", "Landroid/text/style/UpdateAppearance;", "Landroid/text/TextPaint;", "tp", "", "updateDrawState", "(Landroid/text/TextPaint;)V", HookHelper.constructorName, "(Lcom/discord/utilities/textview/TextViewFadeHelper;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final class LinearGradientSpan extends CharacterStyle implements UpdateAppearance {
        public LinearGradientSpan() {
        }

        @Override // android.text.style.CharacterStyle
        public void updateDrawState(TextPaint textPaint) {
            Float lineWidth;
            if (textPaint != null && (lineWidth = TextViewFadeHelper.this.getLineWidth()) != null) {
                float floatValue = lineWidth.floatValue();
                textPaint.bgColor = 0;
                textPaint.setShader(new LinearGradient(0.0f, 0.0f, floatValue, 0.0f, TextViewFadeHelper.this.getTextView().getCurrentTextColor(), Color.parseColor("#00000000"), Shader.TileMode.CLAMP));
            }
        }
    }

    public TextViewFadeHelper(TextView textView) {
        m.checkNotNullParameter(textView, "textView");
        this.textView = textView;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateFade() {
        Layout layout = this.textView.getLayout();
        if (layout != null) {
            CharSequence text = this.textView.getText();
            if (!(text instanceof Spannable)) {
                text = null;
            }
            Spannable spannable = (Spannable) text;
            if (spannable != null) {
                int lineCount = this.textView.getLineCount();
                int maxLines = this.textView.getMaxLines();
                int spanStart = spannable.getSpanStart(this.span);
                int spanEnd = spannable.getSpanEnd(this.span);
                boolean z2 = (spanStart == -1 || spanEnd == -1) ? false : true;
                if (lineCount <= maxLines && z2) {
                    spannable.removeSpan(this.span);
                } else if (lineCount > maxLines) {
                    int i = maxLines - 1;
                    int lineStart = layout.getLineStart(i);
                    int lineEnd = layout.getLineEnd(i);
                    float lineWidth = layout.getLineWidth(i);
                    if (z2) {
                        if (spanStart != lineStart || spanEnd != lineEnd || !m.areEqual(this.lineWidth, lineWidth)) {
                            spannable.removeSpan(this.span);
                        } else {
                            return;
                        }
                    }
                    this.lineWidth = Float.valueOf(lineWidth);
                    spannable.setSpan(this.span, lineStart, lineEnd, 0);
                } else {
                    return;
                }
                this.textView.setText(spannable);
            }
        }
    }

    public final Float getLineWidth() {
        return this.lineWidth;
    }

    public final LinearGradientSpan getSpan() {
        return this.span;
    }

    public final TextView getTextView() {
        return this.textView;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [com.discord.utilities.textview.TextViewFadeHelper$registerFadeHelper$$inlined$apply$lambda$2] */
    public final TextView registerFadeHelper() {
        final TextView textView = this.textView;
        final View.OnLayoutChangeListener textViewFadeHelper$registerFadeHelper$$inlined$apply$lambda$1 = new View.OnLayoutChangeListener() { // from class: com.discord.utilities.textview.TextViewFadeHelper$registerFadeHelper$$inlined$apply$lambda$1
            @Override // android.view.View.OnLayoutChangeListener
            public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                TextViewFadeHelper.this.updateFade();
            }
        };
        final ?? textViewFadeHelper$registerFadeHelper$$inlined$apply$lambda$2 = new TextWatcher() { // from class: com.discord.utilities.textview.TextViewFadeHelper$registerFadeHelper$$inlined$apply$lambda$2
            @Override // android.text.TextWatcher
            public void afterTextChanged(Editable editable) {
                TextViewFadeHelper.this.updateFade();
            }

            @Override // android.text.TextWatcher
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override // android.text.TextWatcher
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }
        };
        textView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() { // from class: com.discord.utilities.textview.TextViewFadeHelper$registerFadeHelper$$inlined$apply$lambda$3
            @Override // android.view.View.OnAttachStateChangeListener
            public void onViewAttachedToWindow(View view) {
                textView.addOnLayoutChangeListener(textViewFadeHelper$registerFadeHelper$$inlined$apply$lambda$1);
                textView.addTextChangedListener(textViewFadeHelper$registerFadeHelper$$inlined$apply$lambda$2);
            }

            @Override // android.view.View.OnAttachStateChangeListener
            public void onViewDetachedFromWindow(View view) {
                textView.removeOnLayoutChangeListener(textViewFadeHelper$registerFadeHelper$$inlined$apply$lambda$1);
                textView.removeTextChangedListener(textViewFadeHelper$registerFadeHelper$$inlined$apply$lambda$2);
            }
        });
        return textView;
    }

    public final void setLineWidth(Float f) {
        this.lineWidth = f;
    }
}
