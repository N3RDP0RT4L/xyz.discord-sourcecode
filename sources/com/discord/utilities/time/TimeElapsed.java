package com.discord.utilities.time;

import andhook.lib.HookHelper;
import d0.g;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: TimeElapsed.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0007\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\u0010\u001a\u00020\u000f\u0012\b\b\u0002\u0010\r\u001a\u00020\b¢\u0006\u0004\b\u0011\u0010\u0012R\u001d\u0010\u0007\u001a\u00020\u00028F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u001d\u0010\f\u001a\u00020\b8F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\t\u0010\u0004\u001a\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000e¨\u0006\u0013"}, d2 = {"Lcom/discord/utilities/time/TimeElapsed;", "", "", "seconds$delegate", "Lkotlin/Lazy;", "getSeconds", "()F", "seconds", "", "milliseconds$delegate", "getMilliseconds", "()J", "milliseconds", "startTime", "J", "Lcom/discord/utilities/time/Clock;", "clock", HookHelper.constructorName, "(Lcom/discord/utilities/time/Clock;J)V", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class TimeElapsed {
    private final Lazy milliseconds$delegate;
    private final Lazy seconds$delegate;
    private final long startTime;

    public TimeElapsed(Clock clock, long j) {
        m.checkNotNullParameter(clock, "clock");
        this.startTime = j;
        this.milliseconds$delegate = g.lazy(new TimeElapsed$milliseconds$2(this, clock));
        this.seconds$delegate = g.lazy(new TimeElapsed$seconds$2(this));
    }

    public final long getMilliseconds() {
        return ((Number) this.milliseconds$delegate.getValue()).longValue();
    }

    public final float getSeconds() {
        return ((Number) this.seconds$delegate.getValue()).floatValue();
    }

    public /* synthetic */ TimeElapsed(Clock clock, long j, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(clock, (i & 2) != 0 ? clock.currentTimeMillis() : j);
    }
}
