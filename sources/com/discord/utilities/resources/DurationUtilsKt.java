package com.discord.utilities.resources;

import android.content.Context;
import android.widget.TextView;
import b.a.k.b;
import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: DurationUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\r\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u001a!\u0010\u0006\u001a\u00020\u0005*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0004\b\u0006\u0010\u0007\u001a\u0019\u0010\t\u001a\u00020\u0005*\u00020\u00002\u0006\u0010\b\u001a\u00020\u0003¢\u0006\u0004\b\t\u0010\n\u001a!\u0010\r\u001a\u00020\f*\u00020\u000b2\u0006\u0010\u0002\u001a\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0004\b\r\u0010\u000e¨\u0006\u000f"}, d2 = {"Landroid/content/Context;", "Lcom/discord/utilities/resources/DurationUnit;", "unit", "", "quantity", "", "formatDuration", "(Landroid/content/Context;Lcom/discord/utilities/resources/DurationUnit;I)Ljava/lang/CharSequence;", "inviteExpirationDurationSeconds", "formatInviteExpireAfterString", "(Landroid/content/Context;I)Ljava/lang/CharSequence;", "Landroid/widget/TextView;", "", "setDurationText", "(Landroid/widget/TextView;Lcom/discord/utilities/resources/DurationUnit;I)V", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class DurationUtilsKt {
    public static final CharSequence formatDuration(Context context, DurationUnit durationUnit, int i) {
        CharSequence b2;
        m.checkNotNullParameter(context, "$this$formatDuration");
        m.checkNotNullParameter(durationUnit, "unit");
        b2 = b.b(context, durationUnit.getStringRes$app_productionGoogleRelease(), new Object[]{StringResourceUtilsKt.getI18nPluralString(context, durationUnit.getQuantityPluralRes$app_productionGoogleRelease(), i, Integer.valueOf(i))}, (r4 & 4) != 0 ? b.C0034b.j : null);
        return b2;
    }

    public static final CharSequence formatInviteExpireAfterString(Context context, int i) {
        CharSequence b2;
        m.checkNotNullParameter(context, "$this$formatInviteExpireAfterString");
        if (i == 0) {
            b2 = b.b(context, R.string.no_user_limit, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            return b2;
        } else if (i == 1800) {
            return formatDuration(context, DurationUnit.MINS, 30);
        } else {
            if (i == 3600) {
                return formatDuration(context, DurationUnit.HOURS, 1);
            }
            if (i == 21600) {
                return formatDuration(context, DurationUnit.HOURS, 6);
            }
            if (i == 43200) {
                return formatDuration(context, DurationUnit.HOURS, 12);
            }
            if (i == 86400) {
                return formatDuration(context, DurationUnit.DAYS, 1);
            }
            if (i == 604800) {
                return formatDuration(context, DurationUnit.DAYS, 7);
            }
            throw new IllegalArgumentException(a.p("Invalid expiration duration ", i));
        }
    }

    public static final void setDurationText(TextView textView, DurationUnit durationUnit, int i) {
        m.checkNotNullParameter(textView, "$this$setDurationText");
        m.checkNotNullParameter(durationUnit, "unit");
        Context context = textView.getContext();
        m.checkNotNullExpressionValue(context, "context");
        textView.setText(formatDuration(context, durationUnit, i));
    }
}
