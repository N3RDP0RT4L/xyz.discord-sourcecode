package com.discord.utilities.media;

import andhook.lib.HookHelper;
import android.app.Application;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioAttributes;
import android.media.MediaPlayer;
import com.discord.app.AppLog;
import com.discord.utilities.logging.Logger;
import d0.g;
import d0.k;
import d0.l;
import d0.z.d.m;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: AppSoundManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0002\u0017\u0018B\u000f\u0012\u0006\u0010\u0014\u001a\u00020\u0013¢\u0006\u0004\b\u0015\u0010\u0016J\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0015\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\u0006J\u0015\u0010\t\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\f\u001a\u00020\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\rR\"\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u000e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012¨\u0006\u0019"}, d2 = {"Lcom/discord/utilities/media/AppSoundManager;", "", "Lcom/discord/utilities/media/AppSound;", "sound", "", "play", "(Lcom/discord/utilities/media/AppSound;)V", "stop", "", "isPlaying", "(Lcom/discord/utilities/media/AppSound;)Z", "Landroid/content/Context;", "context", "Landroid/content/Context;", "", "", "Lcom/discord/utilities/media/AppSoundManager$SoundPlayer;", "soundPlayers", "Ljava/util/Map;", "Landroid/app/Application;", "application", HookHelper.constructorName, "(Landroid/app/Application;)V", "Provider", "SoundPlayer", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class AppSoundManager {
    private final Context context;
    private Map<Integer, SoundPlayer> soundPlayers = new LinkedHashMap();

    /* compiled from: AppSoundManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\t\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004R\u001d\u0010\b\u001a\u00020\u00028B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0005\u0010\u0006\u001a\u0004\b\u0007\u0010\u0004¨\u0006\u000b"}, d2 = {"Lcom/discord/utilities/media/AppSoundManager$Provider;", "", "Lcom/discord/utilities/media/AppSoundManager;", "get", "()Lcom/discord/utilities/media/AppSoundManager;", "INSTANCE$delegate", "Lkotlin/Lazy;", "getINSTANCE", "INSTANCE", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Provider {
        public static final Provider INSTANCE = new Provider();
        private static final Lazy INSTANCE$delegate = g.lazy(AppSoundManager$Provider$INSTANCE$2.INSTANCE);

        private Provider() {
        }

        private final AppSoundManager getINSTANCE() {
            return (AppSoundManager) INSTANCE$delegate.getValue();
        }

        public final AppSoundManager get() {
            return getINSTANCE();
        }
    }

    /* compiled from: AppSoundManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0002\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u000f\u001a\u00020\u000e\u0012\u0006\u0010\u0011\u001a\u00020\u0010\u0012\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00020\u0012¢\u0006\u0004\b\u0014\u0010\u0015J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\r\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0004J\u000f\u0010\u0006\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\r\u0010\t\u001a\u00020\b¢\u0006\u0004\b\t\u0010\nR\u0018\u0010\f\u001a\u0004\u0018\u00010\u000b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\f\u0010\r¨\u0006\u0016"}, d2 = {"Lcom/discord/utilities/media/AppSoundManager$SoundPlayer;", "", "", "start", "()V", "stop", "release", "()Lkotlin/Unit;", "", "isPlaying", "()Z", "Landroid/media/MediaPlayer;", "mediaPlayer", "Landroid/media/MediaPlayer;", "Landroid/content/Context;", "context", "Lcom/discord/utilities/media/AppSound;", "sound", "Lkotlin/Function0;", "onCompletion", HookHelper.constructorName, "(Landroid/content/Context;Lcom/discord/utilities/media/AppSound;Lkotlin/jvm/functions/Function0;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class SoundPlayer {
        private MediaPlayer mediaPlayer;

        public SoundPlayer(final Context context, final AppSound appSound, final Function0<Unit> function0) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(appSound, "sound");
            m.checkNotNullParameter(function0, "onCompletion");
            MediaPlayer mediaPlayer = new MediaPlayer();
            this.mediaPlayer = mediaPlayer;
            if (mediaPlayer != null) {
                mediaPlayer.setAudioAttributes(new AudioAttributes.Builder().setContentType(appSound.getContentType()).setUsage(appSound.getUsage()).build());
                AssetFileDescriptor openRawResourceFd = context.getResources().openRawResourceFd(appSound.getResId());
                m.checkNotNullExpressionValue(openRawResourceFd, "assetFileDescriptor");
                mediaPlayer.setDataSource(openRawResourceFd.getFileDescriptor(), openRawResourceFd.getStartOffset(), openRawResourceFd.getLength());
                mediaPlayer.setLooping(appSound.getShouldLoop());
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() { // from class: com.discord.utilities.media.AppSoundManager$SoundPlayer$$special$$inlined$apply$lambda$1
                    @Override // android.media.MediaPlayer.OnCompletionListener
                    public final void onCompletion(MediaPlayer mediaPlayer2) {
                        function0.invoke();
                    }
                });
                try {
                    mediaPlayer.prepare();
                } catch (IOException unused) {
                    this.mediaPlayer = null;
                }
            }
        }

        public final boolean isPlaying() {
            Boolean bool;
            try {
                k.a aVar = k.j;
                MediaPlayer mediaPlayer = this.mediaPlayer;
                boolean z2 = true;
                if (mediaPlayer == null || !mediaPlayer.isPlaying()) {
                    z2 = false;
                }
                bool = k.m73constructorimpl(Boolean.valueOf(z2));
            } catch (Throwable th) {
                k.a aVar2 = k.j;
                bool = k.m73constructorimpl(l.createFailure(th));
            }
            Throwable th2 = k.m75exceptionOrNullimpl(bool);
            if (th2 != null) {
                AppLog.g.w("Error checking if MediaPlayer is playing", th2);
            }
            Boolean bool2 = Boolean.FALSE;
            if (k.m77isFailureimpl(bool)) {
                bool = bool2;
            }
            return ((Boolean) bool).booleanValue();
        }

        public final Unit release() {
            MediaPlayer mediaPlayer = this.mediaPlayer;
            if (mediaPlayer == null) {
                return null;
            }
            mediaPlayer.release();
            return Unit.a;
        }

        public final void start() {
            Object obj;
            Unit unit;
            try {
                k.a aVar = k.j;
                MediaPlayer mediaPlayer = this.mediaPlayer;
                if (mediaPlayer != null) {
                    mediaPlayer.start();
                    unit = Unit.a;
                } else {
                    unit = null;
                }
                obj = k.m73constructorimpl(unit);
            } catch (Throwable th) {
                k.a aVar2 = k.j;
                obj = k.m73constructorimpl(l.createFailure(th));
            }
            Throwable th2 = k.m75exceptionOrNullimpl(obj);
            if (th2 != null) {
                Logger.e$default(AppLog.g, "Error starting MediaPlayer in invalid state", th2, null, 4, null);
            }
        }

        public final void stop() {
            Object obj;
            Unit unit;
            try {
                k.a aVar = k.j;
                MediaPlayer mediaPlayer = this.mediaPlayer;
                if (mediaPlayer != null) {
                    mediaPlayer.stop();
                    unit = Unit.a;
                } else {
                    unit = null;
                }
                obj = k.m73constructorimpl(unit);
            } catch (Throwable th) {
                k.a aVar2 = k.j;
                obj = k.m73constructorimpl(l.createFailure(th));
            }
            Throwable th2 = k.m75exceptionOrNullimpl(obj);
            if (th2 != null) {
                AppLog.g.w("Called stop on uninitialized MediaPlayer", th2);
            }
        }
    }

    public AppSoundManager(Application application) {
        m.checkNotNullParameter(application, "application");
        this.context = application;
    }

    public final boolean isPlaying(AppSound appSound) {
        m.checkNotNullParameter(appSound, "sound");
        return this.soundPlayers.containsKey(Integer.valueOf(appSound.getResId()));
    }

    public final void play(AppSound appSound) {
        m.checkNotNullParameter(appSound, "sound");
        if (isPlaying(appSound)) {
            SoundPlayer soundPlayer = this.soundPlayers.get(Integer.valueOf(appSound.getResId()));
            if (soundPlayer != null) {
                soundPlayer.stop();
            }
            SoundPlayer soundPlayer2 = this.soundPlayers.get(Integer.valueOf(appSound.getResId()));
            if (soundPlayer2 != null) {
                soundPlayer2.release();
            }
        }
        this.soundPlayers.put(Integer.valueOf(appSound.getResId()), new SoundPlayer(this.context, appSound, new AppSoundManager$play$1(this, appSound)));
        SoundPlayer soundPlayer3 = this.soundPlayers.get(Integer.valueOf(appSound.getResId()));
        if (soundPlayer3 != null) {
            soundPlayer3.start();
        }
    }

    public final void stop(AppSound appSound) {
        SoundPlayer soundPlayer;
        m.checkNotNullParameter(appSound, "sound");
        SoundPlayer soundPlayer2 = this.soundPlayers.get(Integer.valueOf(appSound.getResId()));
        if (!(soundPlayer2 == null || !soundPlayer2.isPlaying() || (soundPlayer = this.soundPlayers.get(Integer.valueOf(appSound.getResId()))) == null)) {
            soundPlayer.stop();
        }
        SoundPlayer soundPlayer3 = this.soundPlayers.get(Integer.valueOf(appSound.getResId()));
        if (soundPlayer3 != null) {
            soundPlayer3.release();
        }
        this.soundPlayers.remove(Integer.valueOf(appSound.getResId()));
    }
}
