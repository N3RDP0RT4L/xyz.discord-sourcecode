package com.discord.utilities.device;

import android.app.Application;
import co.discord.media_engine.CameraEnumeratorProvider;
import com.discord.models.experiments.domain.Experiment;
import com.discord.utilities.lifecycle.ApplicationProvider;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: RtcCameraConfig.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/experiments/domain/Experiment;", "it", "", "invoke", "(Lcom/discord/models/experiments/domain/Experiment;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class RtcCameraConfig$init$1 extends o implements Function1<Experiment, Unit> {
    public static final RtcCameraConfig$init$1 INSTANCE = new RtcCameraConfig$init$1();

    public RtcCameraConfig$init$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Experiment experiment) {
        invoke2(experiment);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Experiment experiment) {
        m.checkNotNullParameter(experiment, "it");
        CameraEnumeratorProvider cameraEnumeratorProvider = CameraEnumeratorProvider.INSTANCE;
        Application application = ApplicationProvider.INSTANCE.get();
        boolean z2 = true;
        if (experiment.getBucket() != 1) {
            z2 = false;
        }
        cameraEnumeratorProvider.init(application, z2);
    }
}
