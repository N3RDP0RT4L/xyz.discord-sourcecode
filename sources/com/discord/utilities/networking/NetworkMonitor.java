package com.discord.utilities.networking;

import andhook.lib.HookHelper;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.os.Build;
import com.discord.utilities.io.NetworkUtils;
import com.discord.utilities.logging.Logger;
import d0.z.d.m;
import kotlin.Metadata;
import rx.Observable;
import rx.subjects.BehaviorSubject;
/* compiled from: NetworkMonitor.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001\u001eB\u0017\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\u0016\u001a\u00020\u0015¢\u0006\u0004\b\u001c\u0010\u001dJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0007\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\b\u0010\u0006J#\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\tH\u0002¢\u0006\u0004\b\u000b\u0010\fJ#\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0003\u001a\u00020\u00022\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\tH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u0013\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\r0\u0010¢\u0006\u0004\b\u0011\u0010\u0012J\u0013\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00130\u0010¢\u0006\u0004\b\u0014\u0010\u0012R\u0016\u0010\u0016\u001a\u00020\u00158\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R:\u0010\u001a\u001a&\u0012\f\u0012\n \u0019*\u0004\u0018\u00010\r0\r \u0019*\u0012\u0012\f\u0012\n \u0019*\u0004\u0018\u00010\r0\r\u0018\u00010\u00180\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001a\u0010\u001b¨\u0006\u001f"}, d2 = {"Lcom/discord/utilities/networking/NetworkMonitor;", "", "Landroid/content/Context;", "context", "", "registerConnectivityNetworkMonitor", "(Landroid/content/Context;)V", "registerBroadcastConnectivityNetworkMonitor", "registerBroadcastAirplaneMode", "Landroid/content/Intent;", "intent", "updateNetworkState", "(Landroid/content/Context;Landroid/content/Intent;)V", "Lcom/discord/utilities/networking/NetworkMonitor$State;", "getState", "(Landroid/content/Context;Landroid/content/Intent;)Lcom/discord/utilities/networking/NetworkMonitor$State;", "Lrx/Observable;", "observeState", "()Lrx/Observable;", "", "observeIsConnected", "Lcom/discord/utilities/logging/Logger;", "logger", "Lcom/discord/utilities/logging/Logger;", "Lrx/subjects/BehaviorSubject;", "kotlin.jvm.PlatformType", "networkStateSubject", "Lrx/subjects/BehaviorSubject;", HookHelper.constructorName, "(Landroid/content/Context;Lcom/discord/utilities/logging/Logger;)V", "State", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class NetworkMonitor {
    private Logger logger;
    private final BehaviorSubject<State> networkStateSubject;

    /* compiled from: NetworkMonitor.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/utilities/networking/NetworkMonitor$State;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "ONLINE", "OFFLINE", "OFFLINE_AIRPLANE_MODE", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum State {
        ONLINE,
        OFFLINE,
        OFFLINE_AIRPLANE_MODE
    }

    public NetworkMonitor(Context context, Logger logger) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(logger, "logger");
        this.logger = logger;
        this.networkStateSubject = BehaviorSubject.l0(getState$default(this, context, null, 2, null));
        registerConnectivityNetworkMonitor(context);
    }

    private final State getState(Context context, Intent intent) {
        State state;
        NetworkUtils networkUtils = NetworkUtils.INSTANCE;
        if (networkUtils.isDeviceConnected(context, intent, this.logger)) {
            state = State.ONLINE;
        } else if (networkUtils.isAirplaneModeOn(context)) {
            state = State.OFFLINE_AIRPLANE_MODE;
        } else {
            state = State.OFFLINE;
        }
        Logger logger = this.logger;
        Logger.i$default(logger, "[NetworkMonitor]", "Updating network state to " + state + ", API: " + Build.VERSION.SDK_INT, null, 4, null);
        return state;
    }

    public static /* synthetic */ State getState$default(NetworkMonitor networkMonitor, Context context, Intent intent, int i, Object obj) {
        if ((i & 2) != 0) {
            intent = null;
        }
        return networkMonitor.getState(context, intent);
    }

    private final void registerBroadcastAirplaneMode(Context context) {
        context.registerReceiver(new BroadcastReceiver() { // from class: com.discord.utilities.networking.NetworkMonitor$registerBroadcastAirplaneMode$1
            @Override // android.content.BroadcastReceiver
            public void onReceive(Context context2, Intent intent) {
                Logger logger;
                m.checkNotNullParameter(context2, "context");
                m.checkNotNullParameter(intent, "intent");
                logger = NetworkMonitor.this.logger;
                Logger.i$default(logger, "[NetworkMonitor]", "Got airplane mode broadcast intent.", null, 4, null);
                NetworkMonitor.updateNetworkState$default(NetworkMonitor.this, context2, null, 2, null);
            }
        }, new IntentFilter("android.intent.action.AIRPLANE_MODE"));
    }

    private final void registerBroadcastConnectivityNetworkMonitor(Context context) {
        context.registerReceiver(new BroadcastReceiver() { // from class: com.discord.utilities.networking.NetworkMonitor$registerBroadcastConnectivityNetworkMonitor$1
            @Override // android.content.BroadcastReceiver
            public void onReceive(Context context2, Intent intent) {
                Logger logger;
                m.checkNotNullParameter(context2, "context");
                m.checkNotNullParameter(intent, "intent");
                logger = NetworkMonitor.this.logger;
                Logger.i$default(logger, "[NetworkMonitor]", "Got connectivity action broadcast intent.", null, 4, null);
                NetworkMonitor.this.updateNetworkState(context2, intent);
            }
        }, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v1, types: [android.net.ConnectivityManager$NetworkCallback, com.discord.utilities.networking.NetworkMonitor$registerConnectivityNetworkMonitor$networkCallback$1] */
    private final void registerConnectivityNetworkMonitor(final Context context) {
        Object systemService = context.getSystemService("connectivity");
        if (!(systemService instanceof ConnectivityManager)) {
            systemService = null;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) systemService;
        if (connectivityManager != 0) {
            ?? networkMonitor$registerConnectivityNetworkMonitor$networkCallback$1 = new ConnectivityManager.NetworkCallback() { // from class: com.discord.utilities.networking.NetworkMonitor$registerConnectivityNetworkMonitor$networkCallback$1
                @Override // android.net.ConnectivityManager.NetworkCallback
                public void onAvailable(Network network) {
                    Logger logger;
                    m.checkNotNullParameter(network, "network");
                    logger = NetworkMonitor.this.logger;
                    Logger.i$default(logger, "[NetworkMonitor]", "Network callback, onAvailable fired: " + network, null, 4, null);
                    NetworkMonitor.updateNetworkState$default(NetworkMonitor.this, context, null, 2, null);
                }

                @Override // android.net.ConnectivityManager.NetworkCallback
                public void onCapabilitiesChanged(Network network, NetworkCapabilities networkCapabilities) {
                    Logger logger;
                    m.checkNotNullParameter(network, "network");
                    m.checkNotNullParameter(networkCapabilities, "networkCapabilities");
                    logger = NetworkMonitor.this.logger;
                    Logger.i$default(logger, "[NetworkMonitor]", "Network callback, onCapabilitiesChanged fired: " + network, null, 4, null);
                    NetworkMonitor.updateNetworkState$default(NetworkMonitor.this, context, null, 2, null);
                }

                @Override // android.net.ConnectivityManager.NetworkCallback
                public void onLost(Network network) {
                    Logger logger;
                    m.checkNotNullParameter(network, "network");
                    logger = NetworkMonitor.this.logger;
                    Logger.i$default(logger, "[NetworkMonitor]", "Network callback, onLost fired: " + network, null, 4, null);
                    NetworkMonitor.updateNetworkState$default(NetworkMonitor.this, context, null, 2, null);
                }

                @Override // android.net.ConnectivityManager.NetworkCallback
                public void onUnavailable() {
                    Logger logger;
                    logger = NetworkMonitor.this.logger;
                    Logger.i$default(logger, "[NetworkMonitor]", "Network callback, onUnavailable fired.", null, 4, null);
                    NetworkMonitor.updateNetworkState$default(NetworkMonitor.this, context, null, 2, null);
                }
            };
            NetworkMonitor$registerConnectivityNetworkMonitor$1 networkMonitor$registerConnectivityNetworkMonitor$1 = new NetworkMonitor$registerConnectivityNetworkMonitor$1(this, connectivityManager, networkMonitor$registerConnectivityNetworkMonitor$networkCallback$1);
            if (Build.VERSION.SDK_INT >= 24) {
                try {
                    connectivityManager.registerDefaultNetworkCallback(networkMonitor$registerConnectivityNetworkMonitor$networkCallback$1);
                } catch (Exception unused) {
                    networkMonitor$registerConnectivityNetworkMonitor$1.invoke2();
                }
            } else {
                networkMonitor$registerConnectivityNetworkMonitor$1.invoke2();
            }
            registerBroadcastConnectivityNetworkMonitor(context);
            registerBroadcastAirplaneMode(context);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateNetworkState(Context context, Intent intent) {
        this.networkStateSubject.onNext(getState(context, intent));
    }

    public static /* synthetic */ void updateNetworkState$default(NetworkMonitor networkMonitor, Context context, Intent intent, int i, Object obj) {
        if ((i & 2) != 0) {
            intent = null;
        }
        networkMonitor.updateNetworkState(context, intent);
    }

    public final Observable<Boolean> observeIsConnected() {
        Observable<Boolean> q = observeState().F(NetworkMonitor$observeIsConnected$1.INSTANCE).q();
        m.checkNotNullExpressionValue(q, "observeState().map { sta… }.distinctUntilChanged()");
        return q;
    }

    public final Observable<State> observeState() {
        Observable<State> q = this.networkStateSubject.J().q();
        m.checkNotNullExpressionValue(q, "networkStateSubject.onBa…().distinctUntilChanged()");
        return q;
    }
}
