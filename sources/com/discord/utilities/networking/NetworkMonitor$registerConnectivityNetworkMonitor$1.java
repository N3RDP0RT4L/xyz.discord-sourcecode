package com.discord.utilities.networking;

import android.net.ConnectivityManager;
import android.net.NetworkRequest;
import com.discord.utilities.logging.Logger;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: NetworkMonitor.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "registerDeprecatedNetworkCallback"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class NetworkMonitor$registerConnectivityNetworkMonitor$1 extends o implements Function0<Unit> {
    public final /* synthetic */ ConnectivityManager $connectivityManager;
    public final /* synthetic */ NetworkMonitor$registerConnectivityNetworkMonitor$networkCallback$1 $networkCallback;
    public final /* synthetic */ NetworkMonitor this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public NetworkMonitor$registerConnectivityNetworkMonitor$1(NetworkMonitor networkMonitor, ConnectivityManager connectivityManager, NetworkMonitor$registerConnectivityNetworkMonitor$networkCallback$1 networkMonitor$registerConnectivityNetworkMonitor$networkCallback$1) {
        super(0);
        this.this$0 = networkMonitor;
        this.$connectivityManager = connectivityManager;
        this.$networkCallback = networkMonitor$registerConnectivityNetworkMonitor$networkCallback$1;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        Logger logger;
        try {
            this.$connectivityManager.registerNetworkCallback(new NetworkRequest.Builder().build(), this.$networkCallback);
        } catch (Exception e) {
            logger = this.this$0.logger;
            logger.i("[NetworkMonitor]", "Unable to register network callback.", e);
        }
    }
}
