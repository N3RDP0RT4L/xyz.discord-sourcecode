package com.discord.utilities.networking;

import andhook.lib.HookHelper;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.logging.LoggingProvider;
import com.discord.utilities.networking.Backoff;
import d0.z.d.m;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: Backoff.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001:\u0002!\"B9\u0012\b\b\u0002\u0010\u0015\u001a\u00020\f\u0012\b\b\u0002\u0010\u0013\u001a\u00020\f\u0012\b\b\u0002\u0010\u0011\u001a\u00020\u0010\u0012\b\b\u0002\u0010\u001e\u001a\u00020\u0007\u0012\b\b\u0002\u0010\u001c\u001a\u00020\u001b¢\u0006\u0004\b\u001f\u0010 J\u001d\u0010\u0005\u001a\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\r\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\b\u0010\tJ\r\u0010\n\u001a\u00020\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u001f\u0010\r\u001a\u00020\f2\u0010\b\u0002\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0002¢\u0006\u0004\b\r\u0010\u000eJ\r\u0010\u000f\u001a\u00020\u0003¢\u0006\u0004\b\u000f\u0010\u000bR\u0016\u0010\u0011\u001a\u00020\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0013\u001a\u00020\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0015\u001a\u00020\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0015\u0010\u0014R\u0016\u0010\u0016\u001a\u00020\f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0016\u0010\u0014R\u0016\u0010\u0017\u001a\u00020\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0017\u0010\u0012R$\u0010\u0019\u001a\u00020\u00072\u0006\u0010\u0018\u001a\u00020\u00078\u0006@BX\u0086\u000e¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u0019\u0010\tR\u0016\u0010\u001c\u001a\u00020\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR\u0016\u0010\u001e\u001a\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001e\u0010\u001a¨\u0006#"}, d2 = {"Lcom/discord/utilities/networking/Backoff;", "", "Lkotlin/Function0;", "", "callback", "executeFailureCallback", "(Lkotlin/jvm/functions/Function0;)V", "", "hasReachedFailureThreshold", "()Z", "succeed", "()V", "", "fail", "(Lkotlin/jvm/functions/Function0;)J", "cancel", "", "failureThreshold", "I", "maxBackoffMs", "J", "minBackoffMs", "current", "fails", "<set-?>", "isPending", "Z", "Lcom/discord/utilities/networking/Backoff$Scheduler;", "scheduler", "Lcom/discord/utilities/networking/Backoff$Scheduler;", "jitter", HookHelper.constructorName, "(JJIZLcom/discord/utilities/networking/Backoff$Scheduler;)V", "Scheduler", "TimerScheduler", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class Backoff {
    private long current;
    private int fails;
    private final int failureThreshold;
    private boolean isPending;
    private final boolean jitter;
    private final long maxBackoffMs;
    private final long minBackoffMs;
    private final Scheduler scheduler;

    /* compiled from: Backoff.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0006\bf\u0018\u00002\u00020\u0001J%\u0010\u0007\u001a\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\u0006\u0010\u0006\u001a\u00020\u0005H&¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\t\u001a\u00020\u0003H&¢\u0006\u0004\b\t\u0010\n¨\u0006\u000b"}, d2 = {"Lcom/discord/utilities/networking/Backoff$Scheduler;", "", "Lkotlin/Function0;", "", "action", "", "delayMs", "schedule", "(Lkotlin/jvm/functions/Function0;J)V", "cancel", "()V", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface Scheduler {
        void cancel();

        void schedule(Function0<Unit> function0, long j);
    }

    /* compiled from: Backoff.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u00012\u00020\u0002B\u001d\u0012\b\b\u0002\u0010\u0013\u001a\u00020\u0012\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u000f¢\u0006\u0004\b\u0015\u0010\u0016J%\u0010\b\u001a\u00020\u00042\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\b\u0010\tJ\u000f\u0010\n\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\n\u0010\u000bR\u0018\u0010\r\u001a\u0004\u0018\u00010\f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0018\u0010\u0010\u001a\u0004\u0018\u00010\u000f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014¨\u0006\u0017"}, d2 = {"Lcom/discord/utilities/networking/Backoff$TimerScheduler;", "Lcom/discord/utilities/networking/Backoff$Scheduler;", "Ljava/util/Timer;", "Lkotlin/Function0;", "", "action", "", "delayMs", "schedule", "(Lkotlin/jvm/functions/Function0;J)V", "cancel", "()V", "Ljava/util/TimerTask;", "timeoutTimerTask", "Ljava/util/TimerTask;", "Ljava/util/concurrent/ExecutorService;", "delegateExecutor", "Ljava/util/concurrent/ExecutorService;", "", "tag", "Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;Ljava/util/concurrent/ExecutorService;)V", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class TimerScheduler extends Timer implements Scheduler {
        private final ExecutorService delegateExecutor;
        private final String tag;
        private TimerTask timeoutTimerTask;

        public TimerScheduler() {
            this(null, null, 3, null);
        }

        public /* synthetic */ TimerScheduler(String str, ExecutorService executorService, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? "TimerScheduler" : str, (i & 2) != 0 ? null : executorService);
        }

        @Override // java.util.Timer, com.discord.utilities.networking.Backoff.Scheduler
        public synchronized void cancel() {
            TimerTask timerTask = this.timeoutTimerTask;
            if (timerTask != null) {
                timerTask.cancel();
            }
        }

        @Override // com.discord.utilities.networking.Backoff.Scheduler
        public synchronized void schedule(final Function0<Unit> function0, long j) {
            m.checkNotNullParameter(function0, "action");
            TimerTask timerTask = this.timeoutTimerTask;
            if (timerTask != null) {
                timerTask.cancel();
            }
            TimerTask backoff$TimerScheduler$schedule$1 = new TimerTask() { // from class: com.discord.utilities.networking.Backoff$TimerScheduler$schedule$1
                /* JADX WARN: Multi-variable type inference failed */
                @Override // java.util.TimerTask, java.lang.Runnable
                public void run() {
                    ExecutorService executorService;
                    ExecutorService executorService2;
                    String str;
                    ExecutorService executorService3;
                    String str2;
                    executorService = Backoff.TimerScheduler.this.delegateExecutor;
                    if (executorService == null) {
                        function0.invoke();
                        return;
                    }
                    Logger logger = LoggingProvider.INSTANCE.get();
                    executorService2 = Backoff.TimerScheduler.this.delegateExecutor;
                    if (executorService2.isShutdown()) {
                        str2 = Backoff.TimerScheduler.this.tag;
                        Logger.i$default(logger, str2, "skipping delayed task. executor is not running", null, 4, null);
                        return;
                    }
                    str = Backoff.TimerScheduler.this.tag;
                    Logger.v$default(logger, str, "TimerScheduler posting to delegate scheduler", null, 4, null);
                    executorService3 = Backoff.TimerScheduler.this.delegateExecutor;
                    final Function0 function02 = function0;
                    Runnable runnable = function02;
                    if (function02 != 0) {
                        runnable = new Runnable() { // from class: com.discord.utilities.networking.Backoff$sam$java_lang_Runnable$0
                            @Override // java.lang.Runnable
                            public final /* synthetic */ void run() {
                                m.checkNotNullExpressionValue(Function0.this.invoke(), "invoke(...)");
                            }
                        };
                    }
                    executorService3.execute(runnable);
                }
            };
            this.timeoutTimerTask = backoff$TimerScheduler$schedule$1;
            schedule(backoff$TimerScheduler$schedule$1, j);
        }

        public TimerScheduler(String str, ExecutorService executorService) {
            m.checkNotNullParameter(str, "tag");
            this.tag = str;
            this.delegateExecutor = executorService;
        }
    }

    public Backoff() {
        this(0L, 0L, 0, false, null, 31, null);
    }

    public Backoff(long j, long j2, int i, boolean z2, Scheduler scheduler) {
        m.checkNotNullParameter(scheduler, "scheduler");
        this.minBackoffMs = j;
        this.maxBackoffMs = j2;
        this.failureThreshold = i;
        this.jitter = z2;
        this.scheduler = scheduler;
        this.current = j;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void executeFailureCallback(Function0<Unit> function0) {
        this.isPending = false;
        function0.invoke();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ long fail$default(Backoff backoff, Function0 function0, int i, Object obj) {
        if ((i & 1) != 0) {
            function0 = null;
        }
        return backoff.fail(function0);
    }

    public final synchronized void cancel() {
        this.scheduler.cancel();
        this.isPending = false;
    }

    public final synchronized long fail(Function0<Unit> function0) {
        long j;
        this.fails++;
        double random = this.jitter ? Math.random() : 1.0d;
        this.current = Math.min(this.current + ((long) (2 * j * random)), this.maxBackoffMs);
        if (function0 != null && !this.isPending) {
            this.isPending = true;
            this.scheduler.schedule(new Backoff$fail$1(this, function0), this.current);
        }
        return this.current;
    }

    public final boolean hasReachedFailureThreshold() {
        return this.fails > this.failureThreshold;
    }

    public final boolean isPending() {
        return this.isPending;
    }

    public final synchronized void succeed() {
        cancel();
        this.fails = 0;
        this.current = this.minBackoffMs;
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ Backoff(long r10, long r12, int r14, boolean r15, com.discord.utilities.networking.Backoff.Scheduler r16, int r17, kotlin.jvm.internal.DefaultConstructorMarker r18) {
        /*
            r9 = this;
            r0 = r17 & 1
            if (r0 == 0) goto L7
            r0 = 500(0x1f4, double:2.47E-321)
            goto L8
        L7:
            r0 = r10
        L8:
            r2 = r17 & 2
            if (r2 == 0) goto L12
            r2 = 10
            long r2 = (long) r2
            long r2 = r2 * r0
            goto L13
        L12:
            r2 = r12
        L13:
            r4 = r17 & 4
            if (r4 == 0) goto L1b
            r4 = 2147483647(0x7fffffff, float:NaN)
            goto L1c
        L1b:
            r4 = r14
        L1c:
            r5 = r17 & 8
            if (r5 == 0) goto L22
            r5 = 1
            goto L23
        L22:
            r5 = r15
        L23:
            r6 = r17 & 16
            if (r6 == 0) goto L2f
            com.discord.utilities.networking.Backoff$TimerScheduler r6 = new com.discord.utilities.networking.Backoff$TimerScheduler
            r7 = 3
            r8 = 0
            r6.<init>(r8, r8, r7, r8)
            goto L31
        L2f:
            r6 = r16
        L31:
            r10 = r9
            r11 = r0
            r13 = r2
            r15 = r4
            r16 = r5
            r17 = r6
            r10.<init>(r11, r13, r15, r16, r17)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.networking.Backoff.<init>(long, long, int, boolean, com.discord.utilities.networking.Backoff$Scheduler, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }
}
