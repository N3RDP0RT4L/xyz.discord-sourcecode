package com.discord.utilities.message;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.LocaleList;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelRecipientNick;
import com.discord.api.interaction.Interaction;
import com.discord.api.sticker.StickerFormatType;
import com.discord.api.sticker.StickerPartial;
import com.discord.api.user.User;
import com.discord.models.member.GuildMember;
import com.discord.models.message.Message;
import com.discord.models.user.CoreUser;
import com.discord.utilities.time.TimeUtils;
import d0.z.d.m;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: MessageUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000t\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0015\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0004\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b0\u0010)J[\u0010\u000f\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020\u000e0\u00062\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0016\u0010\n\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020\t0\u00062\u0010\b\u0002\u0010\r\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u000bH\u0007¢\u0006\u0004\b\u000f\u0010\u0010J#\u0010\u0014\u001a\u00020\u00132\b\u0010\u0011\u001a\u0004\u0018\u00010\u00072\b\u0010\u0012\u001a\u0004\u0018\u00010\u0007H\u0007¢\u0006\u0004\b\u0014\u0010\u0015J#\u0010\u0017\u001a\u00020\u00162\b\u0010\u0011\u001a\u0004\u0018\u00010\u00072\b\u0010\u0012\u001a\u0004\u0018\u00010\u0007H\u0007¢\u0006\u0004\b\u0017\u0010\u0018J\u001d\u0010\u001c\u001a\u00020\u00132\u0006\u0010\u001a\u001a\u00020\u00192\u0006\u0010\u001b\u001a\u00020\u0007¢\u0006\u0004\b\u001c\u0010\u001dJ!\u0010 \u001a\u00020\u001f2\n\u0010\u001e\u001a\u00060\u0007j\u0002`\b2\u0006\u0010\u001b\u001a\u00020\u0007¢\u0006\u0004\b \u0010!R2\u0010$\u001a\u0012\u0012\u0004\u0012\u00020\u00070\"j\b\u0012\u0004\u0012\u00020\u0007`#8\u0006@\u0007X\u0087\u0004¢\u0006\u0012\n\u0004\b$\u0010%\u0012\u0004\b(\u0010)\u001a\u0004\b&\u0010'R\u0016\u0010+\u001a\u00020*8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b+\u0010,R\u001c\u0010.\u001a\b\u0012\u0004\u0012\u00020\u001f0-8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b.\u0010/¨\u00061"}, d2 = {"Lcom/discord/utilities/message/MessageUtils;", "", "Lcom/discord/models/message/Message;", "message", "Lcom/discord/api/channel/Channel;", "channel", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/member/GuildMember;", "guildMembers", "", "Lcom/discord/api/channel/ChannelRecipientNick;", "nicks", "", "getNickOrUsernames", "(Lcom/discord/models/message/Message;Lcom/discord/api/channel/Channel;Ljava/util/Map;Ljava/util/List;)Ljava/util/Map;", "messageId1", "messageId2", "", "compareMessages", "(Ljava/lang/Long;Ljava/lang/Long;)I", "", "isNewer", "(Ljava/lang/Long;Ljava/lang/Long;)Z", "Landroid/content/Context;", "context", "messageId", "getSystemMessageUserJoin", "(Landroid/content/Context;J)I", "meUserId", "Lcom/discord/api/sticker/StickerPartial;", "getWelcomeSticker", "(JJ)Lcom/discord/api/sticker/StickerPartial;", "Ljava/util/Comparator;", "Lkotlin/Comparator;", "SORT_BY_IDS_COMPARATOR", "Ljava/util/Comparator;", "getSORT_BY_IDS_COMPARATOR", "()Ljava/util/Comparator;", "getSORT_BY_IDS_COMPARATOR$annotations", "()V", "", "WELCOME_MESSAGES", "[I", "", "WELCOME_STICKERS", "[Lcom/discord/api/sticker/StickerPartial;", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MessageUtils {
    private static final StickerPartial[] WELCOME_STICKERS;
    public static final MessageUtils INSTANCE = new MessageUtils();
    private static final int[] WELCOME_MESSAGES = {R.string.system_message_guild_member_join_001, R.string.system_message_guild_member_join_002, R.string.system_message_guild_member_join_003, R.string.system_message_guild_member_join_004, R.string.system_message_guild_member_join_005, R.string.system_message_guild_member_join_006, R.string.system_message_guild_member_join_007, R.string.system_message_guild_member_join_008, R.string.system_message_guild_member_join_009, R.string.system_message_guild_member_join_010, R.string.system_message_guild_member_join_011, R.string.system_message_guild_member_join_012, R.string.system_message_guild_member_join_013};
    private static final Comparator<Long> SORT_BY_IDS_COMPARATOR = MessageUtils$SORT_BY_IDS_COMPARATOR$1.INSTANCE;

    static {
        StickerFormatType stickerFormatType = StickerFormatType.LOTTIE;
        WELCOME_STICKERS = new StickerPartial[]{new StickerPartial(749054660769218631L, stickerFormatType, "Wumpus waves hello"), new StickerPartial(751606379340365864L, stickerFormatType, "Nelly peeks around a wall and waves hello"), new StickerPartial(754108890559283200L, stickerFormatType, "Clyde cheerfully waving"), new StickerPartial(781291131828699156L, StickerFormatType.APNG, "Choco waves in greeting"), new StickerPartial(816087792291282944L, stickerFormatType, "Doggo spins and greets you"), new StickerPartial(819128604311027752L, stickerFormatType, "Peach raising her arms in distress")};
    }

    private MessageUtils() {
    }

    public static final int compareMessages(Long l, Long l2) {
        long j = 0;
        long longValue = l != null ? l.longValue() : 0L;
        if (l2 != null) {
            j = l2.longValue();
        }
        int i = (longValue > j ? 1 : (longValue == j ? 0 : -1));
        if (i == 0) {
            return 0;
        }
        return i < 0 ? -1 : 1;
    }

    public static final Map<Long, String> getNickOrUsernames(Message message, Channel channel, Map<Long, GuildMember> map, List<ChannelRecipientNick> list) {
        m.checkNotNullParameter(message, "message");
        m.checkNotNullParameter(map, "guildMembers");
        List<User> mentions = message.getMentions();
        HashMap hashMap = new HashMap((mentions != null ? mentions.size() : 0) + 1);
        List<User> mentions2 = message.getMentions();
        if (mentions2 != null) {
            for (User user : mentions2) {
                hashMap.put(Long.valueOf(user.i()), GuildMember.Companion.getNickOrUsername(new CoreUser(user), map.get(Long.valueOf(user.i())), channel, list));
            }
        }
        User author = message.getAuthor();
        if (author != null) {
            hashMap.put(Long.valueOf(author.i()), GuildMember.Companion.getNickOrUsername(new CoreUser(author), map.get(Long.valueOf(author.i())), channel, list));
        }
        Interaction interaction = message.getInteraction();
        User c = interaction != null ? interaction.c() : null;
        if (c != null) {
            hashMap.put(Long.valueOf(c.i()), GuildMember.Companion.getNickOrUsername(new CoreUser(c), map.get(Long.valueOf(c.i())), channel, list));
        }
        return hashMap;
    }

    public static /* synthetic */ Map getNickOrUsernames$default(Message message, Channel channel, Map map, List list, int i, Object obj) {
        if ((i & 8) != 0) {
            list = null;
        }
        return getNickOrUsernames(message, channel, map, list);
    }

    public static final Comparator<Long> getSORT_BY_IDS_COMPARATOR() {
        return SORT_BY_IDS_COMPARATOR;
    }

    public static /* synthetic */ void getSORT_BY_IDS_COMPARATOR$annotations() {
    }

    public static final boolean isNewer(Long l, Long l2) {
        return compareMessages(l, l2) < 0;
    }

    public final int getSystemMessageUserJoin(Context context, long j) {
        Locale locale;
        Configuration configuration;
        Configuration configuration2;
        LocaleList locales;
        m.checkNotNullParameter(context, "context");
        String str = null;
        if (Build.VERSION.SDK_INT >= 24) {
            Resources resources = context.getResources();
            if (!(resources == null || (configuration2 = resources.getConfiguration()) == null || (locales = configuration2.getLocales()) == null)) {
                locale = locales.get(0);
            }
            locale = null;
        } else {
            Resources resources2 = context.getResources();
            if (!(resources2 == null || (configuration = resources2.getConfiguration()) == null)) {
                locale = configuration.locale;
            }
            locale = null;
        }
        Locale locale2 = new Locale("en");
        if (locale != null) {
            str = locale.getLanguage();
        }
        return WELCOME_MESSAGES[(int) (m.areEqual(str, locale2.getLanguage()) ^ true ? 0L : TimeUtils.parseSnowflake(Long.valueOf(j)) % WELCOME_MESSAGES.length)];
    }

    public final StickerPartial getWelcomeSticker(long j, long j2) {
        long parseSnowflake = TimeUtils.parseSnowflake(Long.valueOf(j2)) + TimeUtils.parseSnowflake(Long.valueOf(j));
        StickerPartial[] stickerPartialArr = WELCOME_STICKERS;
        return stickerPartialArr[(int) (parseSnowflake % stickerPartialArr.length)];
    }
}
