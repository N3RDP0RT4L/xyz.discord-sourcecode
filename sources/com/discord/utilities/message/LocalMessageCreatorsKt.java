package com.discord.utilities.message;

import androidx.appcompat.widget.ActivityChooserModel;
import com.discord.api.application.Application;
import com.discord.api.interaction.Interaction;
import com.discord.api.message.LocalAttachment;
import com.discord.api.message.MessageReference;
import com.discord.api.message.activity.MessageActivity;
import com.discord.api.message.allowedmentions.MessageAllowedMentions;
import com.discord.api.message.reaction.MessageReaction;
import com.discord.api.sticker.BaseSticker;
import com.discord.api.user.User;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.models.domain.NonceGenerator;
import com.discord.models.message.Message;
import com.discord.utilities.time.Clock;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import kotlin.Metadata;
/* compiled from: LocalMessageCreators.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000l\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u000f\u001a¿\u0001\u0010\u001f\u001a\u00020\u001e2\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u000e\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00062\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\b2\b\u0010\f\u001a\u0004\u0018\u00010\u000b2\b\u0010\u000e\u001a\u0004\u0018\u00010\r2\u0006\u0010\u0010\u001a\u00020\u000f2\u000e\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u0011\u0018\u00010\u00062\b\u0010\u0013\u001a\u0004\u0018\u00010\u00022\b\u0010\u0014\u001a\u0004\u0018\u00010\u00022\b\u0010\u0016\u001a\u0004\u0018\u00010\u00152\u000e\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\u0017\u0018\u00010\u00062\b\u0010\u001a\u001a\u0004\u0018\u00010\u00192\b\u0010\u001c\u001a\u0004\u0018\u00010\u001b2\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u0000¢\u0006\u0004\b\u001f\u0010 \u001a3\u0010!\u001a\u00020\u001e2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u000f2\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00110\u0006¢\u0006\u0004\b!\u0010\"\u001a=\u0010(\u001a\u00020\u001e2\u0006\u0010#\u001a\u00020\u001e2&\u0010'\u001a\"\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020%\u0018\u00010$j\u0010\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020%\u0018\u0001`&¢\u0006\u0004\b(\u0010)\u001a[\u00100\u001a\u00020\u001e2\u0006\u0010*\u001a\u00020\u00022\b\u0010+\u001a\u0004\u0018\u00010\u00002\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010,\u001a\u0004\u0018\u00010\u00042\u0006\u0010\f\u001a\u00020\u00042\u0006\u0010-\u001a\u00020\b2\u0006\u0010.\u001a\u00020\b2\b\u0010/\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b0\u00101\u001a7\u00100\u001a\u00020\u001e2\u0006\u0010#\u001a\u00020\u001e2\b\u0010/\u001a\u0004\u0018\u00010\u00022\u0006\u0010-\u001a\u00020\b2\u0006\u0010.\u001a\u00020\b2\u0006\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b0\u00102\u001a%\u00103\u001a\u00020\u001e2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b3\u00104¨\u00065"}, d2 = {"", "content", "", "channelId", "Lcom/discord/api/user/User;", "author", "", "mentions", "", "unsent", "hasLocalUploads", "Lcom/discord/api/application/Application;", "application", "Lcom/discord/api/message/activity/MessageActivity;", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/api/message/LocalAttachment;", "localAttachments", "lastManualAttemptTimestamp", "initialAttemptTimestamp", "", "numRetries", "Lcom/discord/api/sticker/BaseSticker;", "stickers", "Lcom/discord/api/message/MessageReference;", "messageReference", "Lcom/discord/api/message/allowedmentions/MessageAllowedMentions;", "allowedMentions", "captchaKey", "Lcom/discord/models/message/Message;", "createLocalMessage", "(Ljava/lang/String;JLcom/discord/api/user/User;Ljava/util/List;ZZLcom/discord/api/application/Application;Lcom/discord/api/message/activity/MessageActivity;Lcom/discord/utilities/time/Clock;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/util/List;Lcom/discord/api/message/MessageReference;Lcom/discord/api/message/allowedmentions/MessageAllowedMentions;Ljava/lang/String;)Lcom/discord/models/message/Message;", "createInvalidAttachmentsMessage", "(JLcom/discord/api/user/User;Lcom/discord/utilities/time/Clock;Ljava/util/List;)Lcom/discord/models/message/Message;", "other", "Ljava/util/LinkedHashMap;", "Lcom/discord/api/message/reaction/MessageReaction;", "Lkotlin/collections/LinkedHashMap;", "reactions", "createWithReactions", "(Lcom/discord/models/message/Message;Ljava/util/LinkedHashMap;)Lcom/discord/models/message/Message;", "nonce", "commandName", "interactionAuthor", "isFailed", "isLoading", "interactionId", "createLocalApplicationCommandMessage", "(JLjava/lang/String;JLcom/discord/api/user/User;Lcom/discord/api/user/User;ZZLjava/lang/Long;Lcom/discord/utilities/time/Clock;)Lcom/discord/models/message/Message;", "(Lcom/discord/models/message/Message;Ljava/lang/Long;ZZLcom/discord/utilities/time/Clock;)Lcom/discord/models/message/Message;", "createThreadStarterMessageNotFoundMessage", "(JLcom/discord/api/user/User;Lcom/discord/utilities/time/Clock;)Lcom/discord/models/message/Message;", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class LocalMessageCreatorsKt {
    public static final Message createInvalidAttachmentsMessage(long j, User user, Clock clock, List<LocalAttachment> list) {
        m.checkNotNullParameter(user, "author");
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(list, "localAttachments");
        long computeNonce = NonceGenerator.Companion.computeNonce(clock);
        UtcDateTime utcDateTime = new UtcDateTime(clock.currentTimeMillis());
        Boolean bool = Boolean.FALSE;
        return new Message(computeNonce, j, null, user, "", utcDateTime, null, bool, bool, n.emptyList(), n.emptyList(), n.emptyList(), n.emptyList(), n.emptyList(), String.valueOf(computeNonce), bool, null, -3, null, null, null, null, null, null, null, null, null, null, null, null, null, true, null, null, null, null, list, null, 4, 47, null);
    }

    public static final Message createLocalApplicationCommandMessage(long j, String str, long j2, User user, User user2, boolean z2, boolean z3, Long l, Clock clock) {
        m.checkNotNullParameter(user2, "application");
        m.checkNotNullParameter(clock, "clock");
        int i = z2 ? -4 : -5;
        Long l2 = z3 ? 128L : null;
        UtcDateTime utcDateTime = new UtcDateTime(clock.currentTimeMillis());
        Interaction interaction = new Interaction(l, 2, str, user, null);
        return new Message(j, j2, null, user2, "", utcDateTime, null, null, null, null, null, null, null, null, String.valueOf(j), null, null, Integer.valueOf(i), null, null, Long.valueOf(user2.i()), null, l2, null, null, null, interaction, null, null, null, null, false, null, null, null, null, null, null, -72499260, 63, null);
    }

    public static final Message createLocalMessage(String str, long j, User user, List<User> list, boolean z2, boolean z3, Application application, MessageActivity messageActivity, Clock clock, List<LocalAttachment> list2, Long l, Long l2, Integer num, List<? extends BaseSticker> list3, MessageReference messageReference, MessageAllowedMentions messageAllowedMentions, String str2) {
        ArrayList arrayList;
        m.checkNotNullParameter(str, "content");
        m.checkNotNullParameter(user, "author");
        m.checkNotNullParameter(clock, "clock");
        long computeNonce = NonceGenerator.Companion.computeNonce(clock);
        int i = z2 ? -2 : -1;
        UtcDateTime utcDateTime = new UtcDateTime(clock.currentTimeMillis());
        Boolean bool = Boolean.FALSE;
        List emptyList = n.emptyList();
        List emptyList2 = n.emptyList();
        List emptyList3 = n.emptyList();
        List emptyList4 = n.emptyList();
        String valueOf = String.valueOf(computeNonce);
        Integer valueOf2 = Integer.valueOf(i);
        if (list3 != null) {
            arrayList = new ArrayList(o.collectionSizeOrDefault(list3, 10));
            for (BaseSticker baseSticker : list3) {
                arrayList.add(baseSticker.c());
            }
        } else {
            arrayList = null;
        }
        return new Message(computeNonce, j, null, user, str, utcDateTime, null, bool, bool, list, emptyList, emptyList2, emptyList3, emptyList4, valueOf, bool, null, valueOf2, messageActivity, application, null, messageReference, null, null, arrayList, null, null, null, null, null, Boolean.FALSE, z3, messageAllowedMentions, num, l, l2, list2, str2, 4, 0, null);
    }

    public static final Message createThreadStarterMessageNotFoundMessage(long j, User user, Clock clock) {
        m.checkNotNullParameter(user, "author");
        m.checkNotNullParameter(clock, "clock");
        long computeNonce = NonceGenerator.Companion.computeNonce(clock);
        return new Message(computeNonce, j, null, user, "", new UtcDateTime(clock.currentTimeMillis()), null, null, null, null, null, null, null, null, String.valueOf(computeNonce), null, null, 24, null, null, null, null, null, null, null, null, null, null, null, null, null, false, null, null, null, null, null, null, -147516, 63, null);
    }

    public static final Message createWithReactions(Message message, LinkedHashMap<String, MessageReaction> linkedHashMap) {
        List list;
        Collection<MessageReaction> values;
        m.checkNotNullParameter(message, "other");
        if (linkedHashMap == null || (values = linkedHashMap.values()) == null || (list = u.toList(values)) == null) {
            list = n.emptyList();
        }
        return Message.copy$default(message, 0L, 0L, null, null, null, null, null, null, null, null, null, null, null, list, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, false, null, null, null, null, null, null, -8193, 63, null);
    }

    public static final Message createLocalApplicationCommandMessage(Message message, Long l, boolean z2, boolean z3, Clock clock) {
        m.checkNotNullParameter(message, "other");
        m.checkNotNullParameter(clock, "clock");
        Interaction interaction = message.getInteraction();
        String b2 = interaction != null ? interaction.b() : null;
        Interaction interaction2 = message.getInteraction();
        User c = interaction2 != null ? interaction2.c() : null;
        long id2 = message.getId();
        long channelId = message.getChannelId();
        User author = message.getAuthor();
        m.checkNotNull(author);
        return createLocalApplicationCommandMessage(id2, b2, channelId, c, author, z2, z3, l, clock);
    }
}
