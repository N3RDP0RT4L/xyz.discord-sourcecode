package com.discord.utilities;

import kotlin.Metadata;
/* compiled from: ShareUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\b\n\u0002\b\u0003\"\u0016\u0010\u0001\u001a\u00020\u00008\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0001\u0010\u0002¨\u0006\u0003"}, d2 = {"", "NON_DIRECT_SHARE_TARGET_SHORTCUT_RANK", "I", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ShareUtilsKt {
    public static final int NON_DIRECT_SHARE_TARGET_SHORTCUT_RANK = 20;
}
