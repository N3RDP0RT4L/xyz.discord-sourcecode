package com.discord.utilities.animations;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.ViewPropertyAnimator;
import d0.k;
import d0.w.h.b;
import d0.w.h.c;
import d0.w.i.a.g;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlinx.coroutines.CancellableContinuation;
import s.a.l;
/* compiled from: AnimationCoroutineUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u001a\u0017\u0010\u0002\u001a\u00020\u0001*\u00020\u0000H\u0086@ø\u0001\u0000¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0004"}, d2 = {"Landroid/view/ViewPropertyAnimator;", "", "await", "(Landroid/view/ViewPropertyAnimator;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "utils_release"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AnimationCoroutineUtilsKt {
    public static final Object await(final ViewPropertyAnimator viewPropertyAnimator, Continuation<? super Unit> continuation) {
        final l lVar = new l(b.intercepted(continuation), 1);
        lVar.A();
        lVar.f(new AnimationCoroutineUtilsKt$await$$inlined$suspendCancellableCoroutine$lambda$1(viewPropertyAnimator));
        viewPropertyAnimator.setListener(new AnimatorListenerAdapter() { // from class: com.discord.utilities.animations.AnimationCoroutineUtilsKt$await$$inlined$suspendCancellableCoroutine$lambda$2
            @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
            public void onAnimationCancel(Animator animator) {
                viewPropertyAnimator.setListener(null);
                if (CancellableContinuation.this.a()) {
                    CancellableContinuation.this.k(null);
                }
            }

            @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
            public void onAnimationEnd(Animator animator) {
                viewPropertyAnimator.setListener(null);
                if (CancellableContinuation.this.a()) {
                    CancellableContinuation cancellableContinuation = CancellableContinuation.this;
                    Unit unit = Unit.a;
                    k.a aVar = k.j;
                    cancellableContinuation.resumeWith(k.m73constructorimpl(unit));
                }
            }
        });
        viewPropertyAnimator.start();
        Object u = lVar.u();
        if (u == c.getCOROUTINE_SUSPENDED()) {
            g.probeCoroutineSuspended(continuation);
        }
        return u;
    }
}
