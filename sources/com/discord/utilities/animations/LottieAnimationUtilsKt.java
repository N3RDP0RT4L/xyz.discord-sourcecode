package com.discord.utilities.animations;

import com.airbnb.lottie.LottieAnimationView;
import com.discord.stores.StoreStream;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.ranges.IntRange;
/* compiled from: LottieAnimationUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u001a+\u0010\b\u001a\u00020\u0007*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\b\u0010\t¨\u0006\n"}, d2 = {"Lcom/airbnb/lottie/LottieAnimationView;", "", "triggerFrame", "Lkotlin/ranges/IntRange;", "loopFrames", "", "disabledAnimation", "", "loopFrom", "(Lcom/airbnb/lottie/LottieAnimationView;ILkotlin/ranges/IntRange;Z)V", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class LottieAnimationUtilsKt {
    public static final void loopFrom(LottieAnimationView lottieAnimationView, int i, IntRange intRange, boolean z2) {
        m.checkNotNullParameter(lottieAnimationView, "$this$loopFrom");
        m.checkNotNullParameter(intRange, "loopFrames");
        if (z2) {
            lottieAnimationView.setFrame(i);
            lottieAnimationView.c();
            return;
        }
        lottieAnimationView.p.l.j.add(new LoopAnimationListener(lottieAnimationView, i, intRange));
    }

    public static /* synthetic */ void loopFrom$default(LottieAnimationView lottieAnimationView, int i, IntRange intRange, boolean z2, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            z2 = StoreStream.Companion.getAccessibility().isReducedMotionEnabled();
        }
        loopFrom(lottieAnimationView, i, intRange, z2);
    }
}
