package com.discord.utilities.auditlogs;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: AuditLogChangeUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "it", "", "invoke", "(J)Ljava/lang/Integer;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AuditLogChangeUtils$renderPermissionList$1 extends o implements Function1<Long, Integer> {
    public final /* synthetic */ ModelAuditLogEntry $auditLogEntry;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AuditLogChangeUtils$renderPermissionList$1(ModelAuditLogEntry modelAuditLogEntry) {
        super(1);
        this.$auditLogEntry = modelAuditLogEntry;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Integer invoke(Long l) {
        return invoke(l.longValue());
    }

    public final Integer invoke(long j) {
        int stringForPermission;
        stringForPermission = AuditLogChangeUtils.INSTANCE.getStringForPermission(j, this.$auditLogEntry);
        return Integer.valueOf(stringForPermission);
    }
}
