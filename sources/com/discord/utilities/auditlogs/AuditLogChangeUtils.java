package com.discord.utilities.auditlogs;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.Resources;
import androidx.annotation.StringRes;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.guild.GuildExplicitContentFilter;
import com.discord.api.guild.GuildVerificationLevel;
import com.discord.api.guildscheduledevent.GuildScheduledEventEntityType;
import com.discord.api.guildscheduledevent.GuildScheduledEventStatus;
import com.discord.api.permission.Permission;
import com.discord.api.stageinstance.StageInstancePrivacyLevel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.rtcconnection.MediaSinkWantsManager;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.resources.StringResourceUtilsKt;
import d0.f0.q;
import d0.g0.t;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;
import xyz.discord.R;
/* compiled from: AuditLogChangeUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000p\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u001a\n\u0002\u0010 \n\u0002\b\t\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\\\u0010]J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0007\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\b\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\t\u0010\u0006J\u0017\u0010\n\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\n\u0010\u0006J\u0017\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u000b\u0010\u0006J\u0017\u0010\f\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\f\u0010\u0006J\u0017\u0010\r\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\r\u0010\u0006J\u0017\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u000e\u0010\u0006J\u0017\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u000f\u0010\u0006J\u0017\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0010\u0010\u0006J\u0017\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0011\u0010\u0006J'\u0010\u0014\u001a\u00020\u0004*\u00020\u00022\b\b\u0001\u0010\u0012\u001a\u00020\u00042\b\b\u0001\u0010\u0013\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J'\u0010\u0018\u001a\u00020\u0004*\u00020\u00022\b\b\u0001\u0010\u0016\u001a\u00020\u00042\b\b\u0001\u0010\u0017\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0018\u0010\u0015JC\u0010\u001b\u001a\u00020\u0004*\u00020\u00022\n\b\u0003\u0010\u0019\u001a\u0004\u0018\u00010\u00042\n\b\u0003\u0010\u0012\u001a\u0004\u0018\u00010\u00042\n\b\u0003\u0010\u0016\u001a\u0004\u0018\u00010\u00042\n\b\u0003\u0010\u001a\u001a\u0004\u0018\u00010\u0004H\u0002¢\u0006\u0004\b\u001b\u0010\u001cJC\u0010!\u001a.\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u001f0\u001ej\b\u0012\u0004\u0012\u00020\u001f` \u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u001f0\u001ej\b\u0012\u0004\u0012\u00020\u001f` 0\u001d2\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b!\u0010\"JM\u0010-\u001a\u00020,2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010$\u001a\u00020#2\u0006\u0010&\u001a\u00020%2$\u0010+\u001a \u0012\u0004\u0012\u00020(\u0012\u0016\u0012\u0014\u0012\b\u0012\u00060\u001fj\u0002`)\u0012\u0006\u0012\u0004\u0018\u00010*0'0'H\u0002¢\u0006\u0004\b-\u0010.J'\u0010/\u001a\u00020,2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010$\u001a\u00020#2\u0006\u0010&\u001a\u00020%H\u0002¢\u0006\u0004\b/\u00100J#\u00104\u001a\u00020\u00042\n\u00102\u001a\u00060\u001fj\u0002`12\u0006\u00103\u001a\u00020#H\u0003¢\u0006\u0004\b4\u00105J\u001f\u00108\u001a\u00020*2\u0006\u00107\u001a\u0002062\u0006\u0010&\u001a\u00020%H\u0002¢\u0006\u0004\b8\u00109J\u001f\u0010;\u001a\u00020:2\u0006\u0010$\u001a\u00020#2\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b;\u0010<J\u0017\u0010>\u001a\u00020,2\u0006\u0010=\u001a\u00020\u0004H\u0002¢\u0006\u0004\b>\u0010?J\u001f\u0010@\u001a\u00020,2\u0006\u0010&\u001a\u00020%2\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b@\u0010AJ\u001f\u0010B\u001a\u00020\u00042\u0006\u0010&\u001a\u00020%2\u0006\u0010$\u001a\u00020#H\u0002¢\u0006\u0004\bB\u0010CJM\u0010E\u001a\u00020,2\u0006\u0010&\u001a\u00020%2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010D\u001a\u00020#2$\u0010+\u001a \u0012\u0004\u0012\u00020(\u0012\u0016\u0012\u0014\u0012\b\u0012\u00060\u001fj\u0002`)\u0012\u0006\u0012\u0004\u0018\u00010*0'0'H\u0002¢\u0006\u0004\bE\u0010FJ\u001f\u0010G\u001a\u00020*2\u0006\u0010&\u001a\u00020%2\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\bG\u0010HJW\u0010J\u001a\u00020*2\u0006\u0010&\u001a\u00020%2\u0006\u0010$\u001a\u00020#2\u0006\u0010\u0003\u001a\u00020\u00022\b\b\u0001\u0010I\u001a\u00020\u00042$\u0010+\u001a \u0012\u0004\u0012\u00020(\u0012\u0016\u0012\u0014\u0012\b\u0012\u00060\u001fj\u0002`)\u0012\u0006\u0012\u0004\u0018\u00010*0'0'H\u0002¢\u0006\u0004\bJ\u0010KJ\u001b\u0010\u0017\u001a\u00020:*\u00020\u00022\u0006\u0010L\u001a\u00020,H\u0002¢\u0006\u0004\b\u0017\u0010MJ\u0019\u0010O\u001a\u00020\u00042\b\u0010N\u001a\u0004\u0018\u00010\u0004H\u0003¢\u0006\u0004\bO\u0010PJC\u0010Q\u001a\u00020*2\u0006\u0010&\u001a\u00020%2\u0006\u0010$\u001a\u00020#2$\u0010+\u001a \u0012\u0004\u0012\u00020(\u0012\u0016\u0012\u0014\u0012\b\u0012\u00060\u001fj\u0002`)\u0012\u0006\u0012\u0004\u0018\u00010*0'0'¢\u0006\u0004\bQ\u0010RJ\u0015\u0010S\u001a\u00020:2\u0006\u00103\u001a\u00020#¢\u0006\u0004\bS\u0010TJ\u001b\u0010V\u001a\b\u0012\u0004\u0012\u00020\u00020U2\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\bV\u0010WJ\u001b\u0010X\u001a\b\u0012\u0004\u0012\u00020\u00020U2\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\bX\u0010WR\u001c\u0010Y\u001a\b\u0012\u0004\u0012\u00020\u00040U8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bY\u0010ZR\u001c\u0010[\u001a\b\u0012\u0004\u0012\u00020,0U8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b[\u0010Z¨\u0006^"}, d2 = {"Lcom/discord/utilities/auditlogs/AuditLogChangeUtils;", "", "Lcom/discord/models/domain/ModelAuditLogEntry$Change;", "change", "", "getGuildChangeString", "(Lcom/discord/models/domain/ModelAuditLogEntry$Change;)I", "getChannelChangeString", "getUserChangeString", "getRoleChangeString", "getInviteChangeString", "getWebhookChangeString", "getEmojiChangeString", "getStickerChangeString", "getIntegrationChangeString", "getStageInstanceChangeString", "getGuildScheduledEventChangeString", "getThreadChangeString", "hasNoOldValue", "hasOldValue", "getNullableOldValueString", "(Lcom/discord/models/domain/ModelAuditLogEntry$Change;II)I", "hasNoNewValue", "hasNewValue", "getNullableNewValueString", "hasBoth", "hasNeither", "getNullableNewOrOldValueString", "(Lcom/discord/models/domain/ModelAuditLogEntry$Change;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)I", "Lkotlin/Pair;", "Ljava/util/HashSet;", "", "Lkotlin/collections/HashSet;", "calculatePermissionChange", "(Lcom/discord/models/domain/ModelAuditLogEntry$Change;)Lkotlin/Pair;", "Lcom/discord/models/domain/ModelAuditLogEntry;", "auditLogEntry", "Landroid/content/Context;", "context", "", "Lcom/discord/models/domain/ModelAuditLogEntry$TargetType;", "Lcom/discord/stores/TargetId;", "", "targets", "", "renderPermissions", "(Lcom/discord/models/domain/ModelAuditLogEntry$Change;Lcom/discord/models/domain/ModelAuditLogEntry;Landroid/content/Context;Ljava/util/Map;)Ljava/lang/String;", "renderPermissionList", "(Lcom/discord/models/domain/ModelAuditLogEntry$Change;Lcom/discord/models/domain/ModelAuditLogEntry;Landroid/content/Context;)Ljava/lang/String;", "Lcom/discord/api/permission/PermissionBit;", "permission", "log", "getStringForPermission", "(JLcom/discord/models/domain/ModelAuditLogEntry;)I", "Lcom/discord/rtcconnection/MediaSinkWantsManager$VideoQualityMode;", "mode", "getStringForVideoQualityMode", "(Lcom/discord/rtcconnection/MediaSinkWantsManager$VideoQualityMode;Landroid/content/Context;)Ljava/lang/CharSequence;", "", "shouldNotRenderChange", "(Lcom/discord/models/domain/ModelAuditLogEntry;Lcom/discord/models/domain/ModelAuditLogEntry$Change;)Z", "number", "getChangeNumberString", "(I)Ljava/lang/String;", "renderRoles", "(Landroid/content/Context;Lcom/discord/models/domain/ModelAuditLogEntry$Change;)Ljava/lang/String;", "getTextColor", "(Landroid/content/Context;Lcom/discord/models/domain/ModelAuditLogEntry;)I", "entry", "getOverridesPluralString", "(Landroid/content/Context;Lcom/discord/models/domain/ModelAuditLogEntry$Change;Lcom/discord/models/domain/ModelAuditLogEntry;Ljava/util/Map;)Ljava/lang/String;", "getPluralString", "(Landroid/content/Context;Lcom/discord/models/domain/ModelAuditLogEntry$Change;)Ljava/lang/CharSequence;", "textId", "getChangeTextWithParams", "(Landroid/content/Context;Lcom/discord/models/domain/ModelAuditLogEntry;Lcom/discord/models/domain/ModelAuditLogEntry$Change;ILjava/util/Map;)Ljava/lang/CharSequence;", "key", "(Lcom/discord/models/domain/ModelAuditLogEntry$Change;Ljava/lang/String;)Z", "value", "getChannelTypeStringResIdFromValue", "(Ljava/lang/Integer;)I", "getChangeSummary", "(Landroid/content/Context;Lcom/discord/models/domain/ModelAuditLogEntry;Ljava/util/Map;)Ljava/lang/CharSequence;", "hasChangesToRender", "(Lcom/discord/models/domain/ModelAuditLogEntry;)Z", "", "transformPermissionOverride", "(Lcom/discord/models/domain/ModelAuditLogEntry$Change;)Ljava/util/List;", "transformPermissionChange", "RENDERABLE_DELETE_ACTION_TYPES", "Ljava/util/List;", "CHANGE_KEYS_REFERENCING_CHANNEL", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AuditLogChangeUtils {
    public static final AuditLogChangeUtils INSTANCE = new AuditLogChangeUtils();
    private static final List<Integer> RENDERABLE_DELETE_ACTION_TYPES = n.listOf((Object[]) new Integer[]{22, 20, 21});
    private static final List<String> CHANGE_KEYS_REFERENCING_CHANNEL = n.listOf((Object[]) new String[]{ModelAuditLogEntry.CHANGE_KEY_CHANNEL_ID, ModelAuditLogEntry.CHANGE_KEY_AFK_CHANNEL_ID, ModelAuditLogEntry.CHANGE_KEY_SYSTEM_CHANNEL_ID, ModelAuditLogEntry.CHANGE_KEY_RULES_CHANNEL_ID, ModelAuditLogEntry.CHANGE_KEY_UPDATES_CHANNEL_ID});

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;
        public static final /* synthetic */ int[] $EnumSwitchMapping$2;

        static {
            ModelAuditLogEntry.TargetType.values();
            int[] iArr = new int[15];
            $EnumSwitchMapping$0 = iArr;
            iArr[ModelAuditLogEntry.TargetType.ALL.ordinal()] = 1;
            iArr[ModelAuditLogEntry.TargetType.GUILD.ordinal()] = 2;
            iArr[ModelAuditLogEntry.TargetType.CHANNEL.ordinal()] = 3;
            iArr[ModelAuditLogEntry.TargetType.CHANNEL_OVERWRITE.ordinal()] = 4;
            iArr[ModelAuditLogEntry.TargetType.USER.ordinal()] = 5;
            iArr[ModelAuditLogEntry.TargetType.ROLE.ordinal()] = 6;
            iArr[ModelAuditLogEntry.TargetType.INVITE.ordinal()] = 7;
            iArr[ModelAuditLogEntry.TargetType.WEBHOOK.ordinal()] = 8;
            iArr[ModelAuditLogEntry.TargetType.EMOJI.ordinal()] = 9;
            iArr[ModelAuditLogEntry.TargetType.STICKER.ordinal()] = 10;
            iArr[ModelAuditLogEntry.TargetType.INTEGRATION.ordinal()] = 11;
            iArr[ModelAuditLogEntry.TargetType.STAGE_INSTANCE.ordinal()] = 12;
            iArr[ModelAuditLogEntry.TargetType.GUILD_SCHEDULED_EVENT.ordinal()] = 13;
            iArr[ModelAuditLogEntry.TargetType.THREAD.ordinal()] = 14;
            MediaSinkWantsManager.VideoQualityMode.values();
            int[] iArr2 = new int[2];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[MediaSinkWantsManager.VideoQualityMode.AUTO.ordinal()] = 1;
            iArr2[MediaSinkWantsManager.VideoQualityMode.FULL.ordinal()] = 2;
            ModelAuditLogEntry.ActionType.values();
            int[] iArr3 = new int[4];
            $EnumSwitchMapping$2 = iArr3;
            iArr3[ModelAuditLogEntry.ActionType.CREATE.ordinal()] = 1;
            iArr3[ModelAuditLogEntry.ActionType.UPDATE.ordinal()] = 2;
            iArr3[ModelAuditLogEntry.ActionType.DELETE.ordinal()] = 3;
        }
    }

    private AuditLogChangeUtils() {
    }

    private final Pair<HashSet<Long>, HashSet<Long>> calculatePermissionChange(ModelAuditLogEntry.Change change) {
        Object oldValue = change.getOldValue();
        String str = null;
        if (!(oldValue instanceof String)) {
            oldValue = null;
        }
        String str2 = (String) oldValue;
        long j = 0;
        long parseLong = str2 != null ? Long.parseLong(str2) : 0L;
        Object newValue = change.getNewValue();
        if (newValue instanceof String) {
            str = newValue;
        }
        String str3 = str;
        if (str3 != null) {
            j = Long.parseLong(str3);
        }
        long j2 = (~parseLong) & j;
        long j3 = parseLong & (~j);
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        for (int i = 0; i <= 63; i++) {
            long j4 = 1 << i;
            if ((j2 & j4) == j4) {
                hashSet.add(Long.valueOf(j4));
            }
            if ((j3 & j4) == j4) {
                hashSet2.add(Long.valueOf(j4));
            }
        }
        return new Pair<>(hashSet, hashSet2);
    }

    private final String getChangeNumberString(int i) {
        if (i < 0 || 9 < i) {
            return String.valueOf(i);
        }
        StringBuilder sb = new StringBuilder();
        sb.append('0');
        sb.append(i);
        return sb.toString();
    }

    /* JADX WARN: Removed duplicated region for block: B:20:0x004c A[Catch: ClassCastException -> 0x0378, MissingFormatArgumentException -> 0x03ce, TryCatch #2 {ClassCastException -> 0x0378, MissingFormatArgumentException -> 0x03ce, blocks: (B:3:0x0004, B:6:0x0013, B:8:0x001b, B:10:0x0021, B:12:0x0027, B:14:0x0038, B:15:0x003f, B:16:0x0044, B:18:0x0046, B:20:0x004c, B:22:0x0052, B:24:0x0063, B:25:0x006a, B:26:0x006f, B:30:0x0075, B:31:0x007f, B:32:0x008b, B:34:0x0093, B:36:0x00a2, B:37:0x00c6, B:38:0x00cb, B:39:0x00cc, B:41:0x00d5, B:43:0x00dd, B:44:0x00f1, B:45:0x00f6, B:46:0x00f7, B:49:0x0101, B:51:0x0109, B:53:0x0113, B:55:0x011d, B:57:0x012a, B:58:0x0130, B:60:0x0136, B:62:0x0140, B:64:0x014a, B:65:0x0175, B:67:0x017b, B:69:0x0187, B:71:0x0193, B:72:0x01a2, B:74:0x01a8, B:76:0x01b4, B:78:0x01c0, B:80:0x01d8, B:81:0x01e1, B:83:0x01e9, B:85:0x01f1, B:86:0x0208, B:87:0x020d, B:88:0x020e, B:90:0x0217, B:92:0x023b, B:94:0x0241, B:95:0x024a, B:98:0x0255, B:100:0x0267, B:102:0x0276, B:103:0x0290, B:104:0x0295, B:105:0x0296, B:106:0x029b, B:107:0x029c, B:109:0x02a4, B:111:0x02ac, B:112:0x02c3, B:113:0x02c8, B:114:0x02c9, B:116:0x02d2, B:119:0x02e7, B:121:0x02f5, B:122:0x0305, B:123:0x030a, B:124:0x030b, B:125:0x0310, B:126:0x0311, B:128:0x031a, B:130:0x0324, B:131:0x033f, B:132:0x0344, B:133:0x0345, B:135:0x034d, B:137:0x0355, B:138:0x0363, B:139:0x036b, B:140:0x036c), top: B:146:0x0004 }] */
    /* JADX WARN: Removed duplicated region for block: B:29:0x0073 A[ADDED_TO_REGION] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final java.lang.CharSequence getChangeTextWithParams(android.content.Context r11, com.discord.models.domain.ModelAuditLogEntry r12, com.discord.models.domain.ModelAuditLogEntry.Change r13, @androidx.annotation.StringRes int r14, java.util.Map<com.discord.models.domain.ModelAuditLogEntry.TargetType, ? extends java.util.Map<java.lang.Long, ? extends java.lang.CharSequence>> r15) {
        /*
            Method dump skipped, instructions count: 1033
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.auditlogs.AuditLogChangeUtils.getChangeTextWithParams(android.content.Context, com.discord.models.domain.ModelAuditLogEntry, com.discord.models.domain.ModelAuditLogEntry$Change, int, java.util.Map):java.lang.CharSequence");
    }

    @StringRes
    private final int getChannelChangeString(ModelAuditLogEntry.Change change) {
        String key = change.getKey();
        if (key != null) {
            switch (key.hashCode()) {
                case -1920783726:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_REGION_OVERRIDE)) {
                        return getNullableNewOrOldValueString$default(this, change, Integer.valueOf((int) R.string.guild_settings_audit_log_channel_rtc_region_override_change), Integer.valueOf((int) R.string.guild_settings_audit_log_channel_rtc_region_override_create), Integer.valueOf((int) R.string.guild_settings_audit_log_channel_rtc_region_override_delete), null, 8, null);
                    }
                    break;
                case -1451708889:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_VIDEO_QUALITY_MODE)) {
                        return getNullableOldValueString(change, R.string.guild_settings_audit_log_channel_video_quality_mode_create, R.string.guild_settings_audit_log_channel_video_quality_mode_change);
                    }
                    break;
                case -934964668:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_REASON)) {
                        return R.string.guild_settings_audit_log_common_reason;
                    }
                    break;
                case -102270099:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_BITRATE)) {
                        return getNullableOldValueString(change, R.string.guild_settings_audit_log_channel_bitrate_create, R.string.guild_settings_audit_log_channel_bitrate_change);
                    }
                    break;
                case 3079692:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_DENIED)) {
                        return R.string.guild_settings_audit_log_channel_permission_overrides_denied;
                    }
                    break;
                case 3373707:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_NAME)) {
                        return getNullableOldValueString(change, R.string.guild_settings_audit_log_channel_name_create, R.string.guild_settings_audit_log_channel_name_change);
                    }
                    break;
                case 3390806:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_NSFW)) {
                        Object value = change.getValue();
                        Objects.requireNonNull(value, "null cannot be cast to non-null type kotlin.Boolean");
                        boolean booleanValue = ((Boolean) value).booleanValue();
                        if (!booleanValue) {
                            return R.string.guild_settings_audit_log_channel_nsfw_disabled;
                        }
                        if (booleanValue) {
                            return R.string.guild_settings_audit_log_channel_nsfw_enabled;
                        }
                        throw new NoWhenBranchMatchedException();
                    }
                    break;
                case 3575610:
                    if (key.equals("type")) {
                        return getNullableOldValueString(change, R.string.guild_settings_audit_log_channel_type_create, R.string.guild_settings_audit_log_channel_type_change);
                    }
                    break;
                case 92906313:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_GRANTED)) {
                        return R.string.guild_settings_audit_log_channel_permission_overrides_granted;
                    }
                    break;
                case 108404047:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_RESET)) {
                        return R.string.guild_settings_audit_log_channel_permission_overrides_reset;
                    }
                    break;
                case 110546223:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_TOPIC)) {
                        return getNullableNewOrOldValueString$default(this, change, Integer.valueOf((int) R.string.guild_settings_audit_log_channel_topic_change), Integer.valueOf((int) R.string.guild_settings_audit_log_channel_topic_create), Integer.valueOf((int) R.string.guild_settings_audit_log_channel_topic_clear), null, 8, null);
                    }
                    break;
                case 747804969:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_POSITION)) {
                        return getNullableOldValueString(change, R.string.guild_settings_audit_log_channel_position_create, R.string.guild_settings_audit_log_channel_position_change);
                    }
                    break;
                case 987155184:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_RATE_LIMIT_PER_USER)) {
                        return getNullableOldValueString(change, R.string.guild_settings_audit_log_channel_rate_limit_per_user_create, R.string.guild_settings_audit_log_channel_rate_limit_per_user_change);
                    }
                    break;
                case 1702269315:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_DEFAULT_AUTO_ARCHIVE_DURATION)) {
                        return getNullableOldValueString(change, R.string.guild_settings_audit_log_channel_default_auto_archive_duration_create, R.string.guild_settings_audit_log_channel_default_auto_archive_duration_change);
                    }
                    break;
            }
        }
        return 0;
    }

    @StringRes
    private final int getChannelTypeStringResIdFromValue(Integer num) {
        if (num != null && num.intValue() == 1) {
            return R.string.dm;
        }
        if (num != null && num.intValue() == 3) {
            return R.string.group_dm;
        }
        if (num != null && num.intValue() == 0) {
            return R.string.text_channel;
        }
        if (num != null && num.intValue() == 2) {
            return R.string.voice_channel;
        }
        if (num != null && num.intValue() == 5) {
            return R.string.news_channel;
        }
        if (num != null && num.intValue() == 6) {
            return R.string.store_channel;
        }
        if (num != null && num.intValue() == 13) {
            return R.string.stage_channel;
        }
        if ((num != null && num.intValue() == 10) || ((num != null && num.intValue() == 11) || (num != null && num.intValue() == 12))) {
            return R.string.thread;
        }
        if (num != null && num.intValue() == 4) {
            return R.string.category;
        }
        return 0;
    }

    @StringRes
    private final int getEmojiChangeString(ModelAuditLogEntry.Change change) {
        String key = change.getKey();
        if (key != null) {
            int hashCode = key.hashCode();
            if (hashCode != -934964668) {
                if (hashCode == 3373707 && key.equals(ModelAuditLogEntry.CHANGE_KEY_NAME)) {
                    return getNullableOldValueString(change, R.string.guild_settings_audit_log_emoji_name_create, R.string.guild_settings_audit_log_emoji_name_change);
                }
            } else if (key.equals(ModelAuditLogEntry.CHANGE_KEY_REASON)) {
                return R.string.guild_settings_audit_log_common_reason;
            }
        }
        return 0;
    }

    @StringRes
    private final int getGuildChangeString(ModelAuditLogEntry.Change change) {
        String key = change.getKey();
        if (key == null) {
            return 0;
        }
        switch (key.hashCode()) {
            case -1907190207:
                if (key.equals(ModelAuditLogEntry.CHANGE_KEY_BANNER_HASH)) {
                    return R.string.guild_settings_audit_log_guild_banner_hash_change;
                }
                return 0;
            case -1724546052:
                if (key.equals(ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION)) {
                    return getNullableNewValueString(change, R.string.guild_settings_audit_log_guild_description_clear, R.string.guild_settings_audit_log_guild_description_change);
                }
                return 0;
            case -1705139351:
                if (!key.equals(ModelAuditLogEntry.CHANGE_KEY_EXPLICIT_CONTENT_FILTER)) {
                    return 0;
                }
                Object value = change.getValue();
                Objects.requireNonNull(value, "null cannot be cast to non-null type kotlin.Long");
                int longValue = (int) ((Long) value).longValue();
                if (longValue == GuildExplicitContentFilter.NONE.getApiValue()) {
                    return R.string.guild_settings_audit_log_guild_explicit_content_filter_disable;
                }
                if (longValue == GuildExplicitContentFilter.SOME.getApiValue()) {
                    return R.string.guild_settings_audit_log_guild_explicit_content_filter_members_without_roles;
                }
                if (longValue == GuildExplicitContentFilter.ALL.getApiValue()) {
                    return R.string.guild_settings_audit_log_guild_explicit_content_filter_all_members;
                }
                return 0;
            case -1572429104:
                if (key.equals(ModelAuditLogEntry.CHANGE_KEY_AFK_CHANNEL_ID)) {
                    return getNullableNewValueString(change, R.string.guild_settings_audit_log_guild_afk_channel_id_clear, R.string.guild_settings_audit_log_guild_afk_channel_id_change);
                }
                return 0;
            case -1390796524:
                if (key.equals(ModelAuditLogEntry.CHANGE_KEY_ICON_HASH)) {
                    return R.string.guild_settings_audit_log_guild_icon_hash_change;
                }
                return 0;
            case -1100074521:
                if (key.equals(ModelAuditLogEntry.CHANGE_KEY_SYSTEM_CHANNEL_ID)) {
                    return getNullableNewValueString(change, R.string.guild_settings_audit_log_guild_system_channel_id_disable, R.string.guild_settings_audit_log_guild_system_channel_id_change);
                }
                return 0;
            case -934964668:
                if (key.equals(ModelAuditLogEntry.CHANGE_KEY_REASON)) {
                    return R.string.guild_settings_audit_log_common_reason;
                }
                return 0;
            case -934795532:
                if (key.equals(ModelAuditLogEntry.CHANGE_KEY_REGION)) {
                    return R.string.guild_settings_audit_log_guild_region_change;
                }
                return 0;
            case -645093141:
                if (!key.equals(ModelAuditLogEntry.CHANGE_KEY_PREMIUM_PROGRESS_BAR_ENABLED)) {
                    return 0;
                }
                Object value2 = change.getValue();
                Objects.requireNonNull(value2, "null cannot be cast to non-null type kotlin.Boolean");
                boolean booleanValue = ((Boolean) value2).booleanValue();
                if (booleanValue) {
                    return R.string.guild_settings_audit_log_guild_premium_progress_bar_enabled_enabled;
                }
                if (!booleanValue) {
                    return R.string.guild_settings_audit_log_guild_premium_progress_bar_enabled_disabled;
                }
                throw new NoWhenBranchMatchedException();
            case -506227616:
                if (!key.equals(ModelAuditLogEntry.CHANGE_KEY_VERIFICATION_LEVEL)) {
                    return 0;
                }
                Object value3 = change.getValue();
                Objects.requireNonNull(value3, "null cannot be cast to non-null type kotlin.Long");
                int longValue2 = (int) ((Long) value3).longValue();
                if (longValue2 == GuildVerificationLevel.NONE.getApiValue()) {
                    return R.string.guild_settings_audit_log_guild_verification_level_change_none;
                }
                if (longValue2 == GuildVerificationLevel.LOW.getApiValue()) {
                    return R.string.guild_settings_audit_log_guild_verification_level_change_low;
                }
                if (longValue2 == GuildVerificationLevel.MEDIUM.getApiValue()) {
                    return R.string.guild_settings_audit_log_guild_verification_level_change_medium;
                }
                if (longValue2 == GuildVerificationLevel.HIGH.getApiValue()) {
                    return R.string.guild_settings_audit_log_guild_verification_level_change_high;
                }
                if (longValue2 == GuildVerificationLevel.HIGHEST.getApiValue()) {
                    return R.string.guild_settings_audit_log_guild_verification_level_change_very_high;
                }
                return 0;
            case -154917112:
                if (key.equals(ModelAuditLogEntry.CHANGE_KEY_AFK_TIMEOUT)) {
                    return R.string.guild_settings_audit_log_guild_afk_timeout_change;
                }
                return 0;
            case 3373707:
                if (key.equals(ModelAuditLogEntry.CHANGE_KEY_NAME)) {
                    return R.string.guild_settings_audit_log_guild_name_change;
                }
                return 0;
            case 226923479:
                if (key.equals(ModelAuditLogEntry.CHANGE_KEY_DISCOVERY_SPLASH_HASH)) {
                    return R.string.guild_settings_audit_log_guild_discovery_splash_hash_change;
                }
                return 0;
            case 643741670:
                if (key.equals(ModelAuditLogEntry.CHANGE_KEY_SPLASH_HASH)) {
                    return R.string.guild_settings_audit_log_guild_splash_hash_change;
                }
                return 0;
            case 706006559:
                if (key.equals(ModelAuditLogEntry.CHANGE_KEY_RULES_CHANNEL_ID)) {
                    return getNullableNewValueString(change, R.string.guild_settings_audit_log_guild_rules_channel_id_clear, R.string.guild_settings_audit_log_guild_rules_channel_id_change);
                }
                return 0;
            case 945133165:
                if (!key.equals(ModelAuditLogEntry.CHANGE_KEY_MFA_LEVEL)) {
                    return 0;
                }
                Object value4 = change.getValue();
                Objects.requireNonNull(value4, "null cannot be cast to non-null type kotlin.Long");
                int longValue3 = (int) ((Long) value4).longValue();
                if (longValue3 == 0) {
                    return R.string.guild_settings_audit_log_guild_mfa_level_disabled;
                }
                if (longValue3 != 1) {
                    return 0;
                }
                return R.string.guild_settings_audit_log_guild_mfa_level_enabled;
            case 950953474:
                if (key.equals(ModelAuditLogEntry.CHANGE_KEY_UPDATES_CHANNEL_ID)) {
                    return getNullableNewValueString(change, R.string.guild_settings_audit_log_guild_updates_channel_id_clear, R.string.guild_settings_audit_log_guild_updates_channel_id_change);
                }
                return 0;
            case 1207357234:
                if (!key.equals(ModelAuditLogEntry.CHANGE_KEY_DEFAULT_MESSAGE_NOTIFICATIONS)) {
                    return 0;
                }
                Object value5 = change.getValue();
                Objects.requireNonNull(value5, "null cannot be cast to non-null type kotlin.Long");
                int longValue4 = (int) ((Long) value5).longValue();
                if (longValue4 == 0) {
                    return R.string.guild_settings_audit_log_guild_default_message_notifications_change_all_messages;
                }
                if (longValue4 != 1) {
                    return 0;
                }
                return R.string.guild_settings_audit_log_guild_default_message_notifications_change_only_mentions;
            case 1580684753:
                if (key.equals(ModelAuditLogEntry.CHANGE_KEY_VANITY_URL_CODE)) {
                    return getNullableNewValueString(change, R.string.guild_settings_audit_log_guild_vanity_url_code_delete, R.string.guild_settings_audit_log_guild_vanity_url_code_change);
                }
                return 0;
            case 1639242418:
                if (key.equals(ModelAuditLogEntry.CHANGE_KEY_WIDGET_CHANNEL_ID)) {
                    return getNullableNewValueString(change, R.string.guild_settings_audit_log_guild_widget_channel_id_delete, R.string.guild_settings_audit_log_guild_widget_channel_id_change);
                }
                return 0;
            case 1663147559:
                if (key.equals(ModelAuditLogEntry.CHANGE_KEY_OWNER_ID)) {
                    return R.string.guild_settings_audit_log_guild_owner_id_change;
                }
                return 0;
            case 1792613336:
                if (key.equals(ModelAuditLogEntry.CHANGE_KEY_PREFERRED_LOCALE)) {
                    return R.string.guild_settings_audit_log_guild_preferred_locale_change;
                }
                return 0;
            case 2010777670:
                if (!key.equals(ModelAuditLogEntry.CHANGE_KEY_WIDGET_ENABLED)) {
                    return 0;
                }
                Object value6 = change.getValue();
                Objects.requireNonNull(value6, "null cannot be cast to non-null type kotlin.Boolean");
                boolean booleanValue2 = ((Boolean) value6).booleanValue();
                if (booleanValue2) {
                    return R.string.guild_settings_audit_log_guild_widget_enabled;
                }
                if (!booleanValue2) {
                    return R.string.guild_settings_audit_log_guild_widget_disabled;
                }
                throw new NoWhenBranchMatchedException();
            default:
                return 0;
        }
    }

    @StringRes
    private final int getGuildScheduledEventChangeString(ModelAuditLogEntry.Change change) {
        String key = change.getKey();
        if (key == null) {
            return 0;
        }
        switch (key.hashCode()) {
            case -1930808873:
                if (key.equals(ModelAuditLogEntry.CHANGE_KEY_CHANNEL_ID)) {
                    return getNullableNewValueString(change, R.string.guild_settings_audit_log_scheduled_event_channel_clear, R.string.guild_settings_audit_log_scheduled_event_channel);
                }
                return 0;
            case -1724546052:
                if (key.equals(ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION)) {
                    return R.string.guild_settings_audit_log_scheduled_event_description_create;
                }
                return 0;
            case -892481550:
                if (!key.equals("status")) {
                    return 0;
                }
                Object value = change.getValue();
                Objects.requireNonNull(value, "null cannot be cast to non-null type kotlin.Long");
                int longValue = (int) ((Long) value).longValue();
                if (longValue == GuildScheduledEventStatus.SCHEDULED.getApiValue()) {
                    return R.string.guild_settings_audit_log_scheduled_event_status_scheduled;
                }
                if (longValue == GuildScheduledEventStatus.ACTIVE.getApiValue()) {
                    return R.string.guild_settings_audit_log_scheduled_event_status_active;
                }
                if (longValue == GuildScheduledEventStatus.COMPLETED.getApiValue()) {
                    return R.string.guild_settings_audit_log_scheduled_event_status_completed;
                }
                if (longValue == GuildScheduledEventStatus.CANCELED.getApiValue()) {
                    return R.string.guild_settings_audit_log_scheduled_event_status_canceled;
                }
                return 0;
            case 3373707:
                if (key.equals(ModelAuditLogEntry.CHANGE_KEY_NAME)) {
                    return R.string.guild_settings_audit_log_scheduled_event_name_create;
                }
                return 0;
            case 1281710614:
                if (!key.equals(ModelAuditLogEntry.CHANGE_KEY_ENTITY_TYPE)) {
                    return 0;
                }
                Object value2 = change.getValue();
                Objects.requireNonNull(value2, "null cannot be cast to non-null type kotlin.Long");
                int longValue2 = (int) ((Long) value2).longValue();
                if (longValue2 == GuildScheduledEventEntityType.NONE.getApiValue()) {
                    return R.string.guild_settings_audit_log_scheduled_event_entity_type_none;
                }
                if (longValue2 == GuildScheduledEventEntityType.STAGE_INSTANCE.getApiValue()) {
                    return R.string.guild_settings_audit_log_scheduled_event_entity_type_stage_instance;
                }
                if (longValue2 == GuildScheduledEventEntityType.VOICE.getApiValue()) {
                    return R.string.guild_settings_audit_log_scheduled_event_entity_type_voice;
                }
                if (longValue2 == GuildScheduledEventEntityType.EXTERNAL.getApiValue()) {
                    return R.string.guild_settings_audit_log_scheduled_event_entity_type_external;
                }
                return 0;
            case 1901043637:
                if (key.equals(ModelAuditLogEntry.CHANGE_KEY_LOCATION)) {
                    return getNullableNewValueString(change, R.string.guild_settings_audit_log_scheduled_event_location_clear, R.string.guild_settings_audit_log_scheduled_event_location);
                }
                return 0;
            case 1965579277:
                if (!key.equals(ModelAuditLogEntry.CHANGE_KEY_PRIVACY_LEVEL)) {
                    return 0;
                }
                Object value3 = change.getValue();
                Objects.requireNonNull(value3, "null cannot be cast to non-null type kotlin.Long");
                int longValue3 = (int) ((Long) value3).longValue();
                if (longValue3 == StageInstancePrivacyLevel.GUILD_ONLY.getApiValue()) {
                    return R.string.guild_settings_audit_log_stage_instance_privacy_level_guild_only;
                }
                if (longValue3 == StageInstancePrivacyLevel.PUBLIC.getApiValue()) {
                    return R.string.guild_settings_audit_log_stage_instance_privacy_level_public;
                }
                return 0;
            default:
                return 0;
        }
    }

    @StringRes
    private final int getIntegrationChangeString(ModelAuditLogEntry.Change change) {
        String key = change.getKey();
        if (key == null) {
            return 0;
        }
        int hashCode = key.hashCode();
        if (hashCode != -1743820047) {
            if (hashCode != -486786702) {
                if (hashCode == 1767574344 && key.equals(ModelAuditLogEntry.CHANGE_KEY_EXPIRE_GRACE_PERIOD)) {
                    return R.string.guild_settings_audit_log_integration_expire_grace_period;
                }
                return 0;
            } else if (!key.equals(ModelAuditLogEntry.CHANGE_KEY_EXPIRE_BEHAVIOR)) {
                return 0;
            } else {
                Object value = change.getValue();
                Objects.requireNonNull(value, "null cannot be cast to non-null type kotlin.Long");
                int longValue = (int) ((Long) value).longValue();
                if (longValue == 0) {
                    return R.string.guild_settings_audit_log_integration_expire_behavior_remove_synced_role;
                }
                if (longValue != 1) {
                    return 0;
                }
                return R.string.guild_settings_audit_log_integration_expire_behavior_kick_from_server;
            }
        } else if (!key.equals(ModelAuditLogEntry.CHANGE_KEY_ENABLE_EMOTICONS)) {
            return 0;
        } else {
            Object value2 = change.getValue();
            Objects.requireNonNull(value2, "null cannot be cast to non-null type kotlin.Boolean");
            boolean booleanValue = ((Boolean) value2).booleanValue();
            if (booleanValue) {
                return R.string.guild_settings_audit_log_integration_enable_emoticons_on;
            }
            if (!booleanValue) {
                return R.string.guild_settings_audit_log_integration_enable_emoticons_off;
            }
            throw new NoWhenBranchMatchedException();
        }
    }

    @StringRes
    private final int getInviteChangeString(ModelAuditLogEntry.Change change) {
        String key = change.getKey();
        if (key != null) {
            switch (key.hashCode()) {
                case -1930808873:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_CHANNEL_ID)) {
                        return R.string.guild_settings_audit_log_invite_channel_create;
                    }
                    break;
                case -934964668:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_REASON)) {
                        return R.string.guild_settings_audit_log_common_reason;
                    }
                    break;
                case 3059181:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_CODE)) {
                        return R.string.guild_settings_audit_log_invite_code_create;
                    }
                    break;
                case 408141255:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_MAX_USES)) {
                        Object value = change.getValue();
                        Objects.requireNonNull(value, "null cannot be cast to non-null type kotlin.Long");
                        return ((int) ((Long) value).longValue()) != 0 ? R.string.guild_settings_audit_log_invite_max_uses_create : R.string.guild_settings_audit_log_invite_max_uses_create_infinite;
                    }
                    break;
                case 844430244:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_MAX_AGE)) {
                        Object value2 = change.getValue();
                        Objects.requireNonNull(value2, "null cannot be cast to non-null type kotlin.Long");
                        return ((int) ((Long) value2).longValue()) != 0 ? R.string.guild_settings_audit_log_invite_max_age_create : R.string.guild_settings_audit_log_invite_max_age_create_infinite;
                    }
                    break;
                case 1984986705:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_TEMPORARY)) {
                        Object value3 = change.getValue();
                        Objects.requireNonNull(value3, "null cannot be cast to non-null type kotlin.Boolean");
                        boolean booleanValue = ((Boolean) value3).booleanValue();
                        if (booleanValue) {
                            return R.string.guild_settings_audit_log_invite_temporary_on;
                        }
                        if (!booleanValue) {
                            return R.string.guild_settings_audit_log_invite_temporary_off;
                        }
                        throw new NoWhenBranchMatchedException();
                    }
                    break;
            }
        }
        return 0;
    }

    private final int getNullableNewOrOldValueString(ModelAuditLogEntry.Change change, @StringRes Integer num, @StringRes Integer num2, @StringRes Integer num3, @StringRes Integer num4) {
        if (change.getNewValue() == null || change.getOldValue() == null) {
            if (change.getNewValue() != null) {
                num = num2;
            } else {
                num = change.getOldValue() != null ? num3 : num4;
            }
        }
        if (num != null) {
            return num.intValue();
        }
        return 0;
    }

    public static /* synthetic */ int getNullableNewOrOldValueString$default(AuditLogChangeUtils auditLogChangeUtils, ModelAuditLogEntry.Change change, Integer num, Integer num2, Integer num3, Integer num4, int i, Object obj) {
        return auditLogChangeUtils.getNullableNewOrOldValueString(change, (i & 1) != 0 ? null : num, (i & 2) != 0 ? null : num2, (i & 4) != 0 ? null : num3, (i & 8) != 0 ? null : num4);
    }

    private final int getNullableNewValueString(ModelAuditLogEntry.Change change, @StringRes int i, @StringRes int i2) {
        return change.getNewValue() == null ? i : i2;
    }

    private final int getNullableOldValueString(ModelAuditLogEntry.Change change, @StringRes int i, @StringRes int i2) {
        return change.getOldValue() == null ? i : i2;
    }

    private final String getOverridesPluralString(Context context, ModelAuditLogEntry.Change change, ModelAuditLogEntry modelAuditLogEntry, Map<ModelAuditLogEntry.TargetType, ? extends Map<Long, ? extends CharSequence>> map) {
        CharSequence charSequence;
        Object value = change.getValue();
        Objects.requireNonNull(value, "null cannot be cast to non-null type kotlin.collections.Collection<*>");
        Collection collection = (Collection) value;
        ModelAuditLogEntry.Options options = modelAuditLogEntry.getOptions();
        Integer valueOf = options != null ? Integer.valueOf(options.getType()) : null;
        Object obj = "";
        if (valueOf != null && valueOf.intValue() == 1) {
            Map<Long, ? extends CharSequence> map2 = map.get(ModelAuditLogEntry.TargetType.USER);
            if (map2 != null) {
                ModelAuditLogEntry.Options options2 = modelAuditLogEntry.getOptions();
                charSequence = map2.get(options2 != null ? Long.valueOf(options2.getId()) : null);
            }
            charSequence = null;
        } else if (valueOf != null && valueOf.intValue() == 0) {
            ModelAuditLogEntry.Options options3 = modelAuditLogEntry.getOptions();
            if (options3 != null) {
                charSequence = options3.getRoleName();
            }
            charSequence = null;
        } else {
            charSequence = obj;
        }
        String key = change.getKey();
        if (key != null) {
            int hashCode = key.hashCode();
            if (hashCode != 3079692) {
                if (hashCode != 92906313) {
                    if (hashCode == 108404047 && key.equals(ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_RESET)) {
                        Resources resources = context.getResources();
                        m.checkNotNullExpressionValue(resources, "context.resources");
                        obj = b.b(context, R.string.guild_settings_audit_log_channel_permission_overrides_reset, new Object[]{StringResourceUtilsKt.getQuantityString(resources, context, (int) R.plurals.guild_settings_audit_log_channel_permission_overrides_reset_count, collection.size(), Integer.valueOf(collection.size())), charSequence}, (r4 & 4) != 0 ? b.C0034b.j : null);
                    }
                } else if (key.equals(ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_GRANTED)) {
                    Resources resources2 = context.getResources();
                    m.checkNotNullExpressionValue(resources2, "context.resources");
                    obj = b.b(context, R.string.guild_settings_audit_log_channel_permission_overrides_granted, new Object[]{StringResourceUtilsKt.getQuantityString(resources2, context, (int) R.plurals.guild_settings_audit_log_channel_permission_overrides_granted_count, collection.size(), Integer.valueOf(collection.size())), charSequence}, (r4 & 4) != 0 ? b.C0034b.j : null);
                }
            } else if (key.equals(ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_DENIED)) {
                Resources resources3 = context.getResources();
                m.checkNotNullExpressionValue(resources3, "context.resources");
                obj = b.b(context, R.string.guild_settings_audit_log_channel_permission_overrides_denied, new Object[]{StringResourceUtilsKt.getQuantityString(resources3, context, (int) R.plurals.guild_settings_audit_log_channel_permission_overrides_denied_count, collection.size(), Integer.valueOf(collection.size())), charSequence}, (r4 & 4) != 0 ? b.C0034b.j : null);
            }
        }
        return obj.toString();
    }

    private final CharSequence getPluralString(Context context, ModelAuditLogEntry.Change change) {
        Object value = change.getValue();
        Objects.requireNonNull(value, "null cannot be cast to non-null type kotlin.collections.Collection<*>");
        Collection collection = (Collection) value;
        String key = change.getKey();
        if (key != null) {
            switch (key.hashCode()) {
                case 1168893:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_ROLES_ADD)) {
                        Resources resources = context.getResources();
                        m.checkNotNullExpressionValue(resources, "context.resources");
                        return StringResourceUtilsKt.getQuantityString(resources, context, (int) R.plurals.guild_settings_audit_log_member_roles_add_count, collection.size(), Integer.valueOf(collection.size()));
                    }
                    break;
                case 3079692:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_DENIED)) {
                        Resources resources2 = context.getResources();
                        m.checkNotNullExpressionValue(resources2, "context.resources");
                        return StringResourceUtilsKt.getQuantityString(resources2, context, (int) R.plurals.guild_settings_audit_log_channel_permission_overrides_denied_count, collection.size(), Integer.valueOf(collection.size()));
                    }
                    break;
                case 92906313:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_GRANTED)) {
                        Resources resources3 = context.getResources();
                        m.checkNotNullExpressionValue(resources3, "context.resources");
                        return StringResourceUtilsKt.getQuantityString(resources3, context, (int) R.plurals.guild_settings_audit_log_channel_permission_overrides_granted_count, collection.size(), Integer.valueOf(collection.size()));
                    }
                    break;
                case 950750632:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_ROLES_REMOVE)) {
                        Resources resources4 = context.getResources();
                        m.checkNotNullExpressionValue(resources4, "context.resources");
                        return StringResourceUtilsKt.getQuantityString(resources4, context, (int) R.plurals.guild_settings_audit_log_member_roles_remove_count, collection.size(), Integer.valueOf(collection.size()));
                    }
                    break;
            }
        }
        return "";
    }

    @StringRes
    private final int getRoleChangeString(ModelAuditLogEntry.Change change) {
        String key = change.getKey();
        if (key != null) {
            switch (key.hashCode()) {
                case -1390796524:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_ICON_HASH)) {
                        return R.string.guild_settings_audit_log_role_icon_hash_change;
                    }
                    break;
                case -934964668:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_REASON)) {
                        return R.string.guild_settings_audit_log_common_reason;
                    }
                    break;
                case 3079692:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_DENIED)) {
                        return R.string.guild_settings_audit_log_role_permissions_denied;
                    }
                    break;
                case 3373707:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_NAME)) {
                        return getNullableOldValueString(change, R.string.guild_settings_audit_log_role_name_create, R.string.guild_settings_audit_log_role_name_change);
                    }
                    break;
                case 64859716:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_MENTIONABLE)) {
                        Object value = change.getValue();
                        Objects.requireNonNull(value, "null cannot be cast to non-null type kotlin.Boolean");
                        boolean booleanValue = ((Boolean) value).booleanValue();
                        if (booleanValue) {
                            return R.string.guild_settings_audit_log_role_mentionable_on;
                        }
                        if (!booleanValue) {
                            return R.string.guild_settings_audit_log_role_mentionable_off;
                        }
                        throw new NoWhenBranchMatchedException();
                    }
                    break;
                case 92906313:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_GRANTED)) {
                        return R.string.guild_settings_audit_log_role_permissions_granted;
                    }
                    break;
                case 94842723:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_COLOR)) {
                        Object value2 = change.getValue();
                        Objects.requireNonNull(value2, "null cannot be cast to non-null type kotlin.Long");
                        return ((Long) value2).longValue() == 0 ? R.string.guild_settings_audit_log_role_color_none : R.string.guild_settings_audit_log_role_color;
                    }
                    break;
                case 99457571:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_HOIST)) {
                        Object value3 = change.getValue();
                        Objects.requireNonNull(value3, "null cannot be cast to non-null type kotlin.Boolean");
                        boolean booleanValue2 = ((Boolean) value3).booleanValue();
                        if (booleanValue2) {
                            return R.string.guild_settings_audit_log_role_hoist_on;
                        }
                        if (!booleanValue2) {
                            return R.string.guild_settings_audit_log_role_hoist_off;
                        }
                        throw new NoWhenBranchMatchedException();
                    }
                    break;
                case 1052836964:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_UNICODE_EMOJI)) {
                        return R.string.guild_settings_audit_log_role_unicode_emoji_change;
                    }
                    break;
            }
        }
        return 0;
    }

    @StringRes
    private final int getStageInstanceChangeString(ModelAuditLogEntry.Change change) {
        String key = change.getKey();
        if (key == null) {
            return 0;
        }
        int hashCode = key.hashCode();
        if (hashCode != 110546223) {
            if (hashCode != 1965579277 || !key.equals(ModelAuditLogEntry.CHANGE_KEY_PRIVACY_LEVEL)) {
                return 0;
            }
            Object value = change.getValue();
            Objects.requireNonNull(value, "null cannot be cast to non-null type kotlin.Long");
            int longValue = (int) ((Long) value).longValue();
            if (longValue == StageInstancePrivacyLevel.GUILD_ONLY.getApiValue()) {
                return R.string.guild_settings_audit_log_stage_instance_privacy_level_guild_only;
            }
            if (longValue == StageInstancePrivacyLevel.PUBLIC.getApiValue()) {
                return R.string.guild_settings_audit_log_stage_instance_privacy_level_public;
            }
            return 0;
        } else if (key.equals(ModelAuditLogEntry.CHANGE_KEY_TOPIC)) {
            return getNullableOldValueString(change, R.string.guild_settings_audit_log_channel_topic_create, R.string.guild_settings_audit_log_channel_topic_change);
        } else {
            return 0;
        }
    }

    @StringRes
    private final int getStickerChangeString(ModelAuditLogEntry.Change change) {
        String key = change.getKey();
        if (key != null) {
            int hashCode = key.hashCode();
            if (hashCode != -1724546052) {
                if (hashCode != 3373707) {
                    if (hashCode == 3552281 && key.equals(ModelAuditLogEntry.CHANGE_KEY_TAGS)) {
                        return getNullableOldValueString(change, R.string.guild_settings_audit_log_tags_create, R.string.guild_settings_audit_log_tags_change);
                    }
                } else if (key.equals(ModelAuditLogEntry.CHANGE_KEY_NAME)) {
                    return getNullableOldValueString(change, R.string.guild_settings_audit_log_name_create, R.string.guild_settings_audit_log_name_change);
                }
            } else if (key.equals(ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION)) {
                return getNullableOldValueString(change, R.string.guild_settings_audit_log_description_create, R.string.guild_settings_audit_log_description_change);
            }
        }
        return 0;
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StringRes
    public final int getStringForPermission(long j, ModelAuditLogEntry modelAuditLogEntry) {
        if (j == 1) {
            return R.string.create_instant_invite;
        }
        if (j == 2) {
            return R.string.kick_members;
        }
        if (j == 4) {
            return R.string.ban_members;
        }
        if (j == 8) {
            return R.string.administrator;
        }
        if (j == 16) {
            return (modelAuditLogEntry.getTargetType() == ModelAuditLogEntry.TargetType.CHANNEL || modelAuditLogEntry.getTargetType() == ModelAuditLogEntry.TargetType.CHANNEL_OVERWRITE) ? R.string.manage_channel : R.string.manage_channels;
        }
        if (j == 32) {
            return R.string.manage_server;
        }
        if (j == Permission.VIEW_GUILD_ANALYTICS) {
            return R.string.view_guild_analytics;
        }
        if (j == Permission.CHANGE_NICKNAME) {
            return R.string.change_nickname;
        }
        if (j == Permission.MANAGE_NICKNAMES) {
            return R.string.manage_nicknames;
        }
        if (j == Permission.MANAGE_ROLES) {
            return R.string.manage_roles;
        }
        if (j == Permission.MANAGE_WEBHOOKS) {
            return R.string.manage_webhooks;
        }
        if (j == Permission.MANAGE_EMOJIS_AND_STICKERS) {
            return R.string.manage_emojis_and_stickers;
        }
        if (j == Permission.MANAGE_EVENTS) {
            return R.string.manage_events;
        }
        if (j == 128) {
            return R.string.view_audit_log;
        }
        if (j == Permission.VIEW_CHANNEL) {
            return R.string.read_messages;
        }
        if (j == Permission.SEND_MESSAGES) {
            return R.string.send_messages;
        }
        if (j == Permission.SEND_TTS_MESSAGES) {
            return R.string.send_tts_messages;
        }
        if (j == Permission.MANAGE_MESSAGES) {
            return R.string.manage_messages;
        }
        if (j == Permission.EMBED_LINKS) {
            return R.string.embed_links;
        }
        if (j == Permission.ATTACH_FILES) {
            return R.string.attach_files;
        }
        if (j == Permission.READ_MESSAGE_HISTORY) {
            return R.string.read_message_history;
        }
        if (j == Permission.MENTION_EVERYONE) {
            return R.string.mention_everyone_android;
        }
        if (j == Permission.USE_EXTERNAL_EMOJIS) {
            return R.string.use_external_emojis;
        }
        if (j == Permission.USE_EXTERNAL_STICKERS) {
            return R.string.role_permissions_use_external_stickers;
        }
        if (j == 64) {
            return R.string.add_reactions;
        }
        if (j == Permission.CONNECT) {
            return R.string.connect;
        }
        if (j == Permission.SPEAK) {
            return R.string.speak;
        }
        if (j == Permission.MUTE_MEMBERS) {
            return R.string.mute_members;
        }
        if (j == Permission.DEAFEN_MEMBERS) {
            return R.string.deafen_members;
        }
        if (j == Permission.MOVE_MEMBERS) {
            return R.string.move_members;
        }
        if (j == Permission.USE_VAD) {
            return R.string.use_vad;
        }
        if (j == 256) {
            return R.string.priority_speaker;
        }
        if (j == 512) {
            return R.string.video;
        }
        if (j == Permission.USE_APPLICATION_COMMANDS) {
            return R.string.use_application_commands;
        }
        if (j == Permission.REQUEST_TO_SPEAK) {
            return R.string.request_to_speak;
        }
        if (j == Permission.SEND_MESSAGES_IN_THREADS) {
            return R.string.send_messages_in_threads;
        }
        if (j == Permission.CREATE_PUBLIC_THREADS) {
            return R.string.create_public_threads;
        }
        if (j == Permission.CREATE_PRIVATE_THREADS) {
            return R.string.create_private_threads;
        }
        if (j == Permission.MANAGE_THREADS) {
            return R.string.manage_threads;
        }
        if (j == Permission.MODERATE_MEMBERS) {
            return R.string.moderate_member;
        }
        return 0;
    }

    private final CharSequence getStringForVideoQualityMode(MediaSinkWantsManager.VideoQualityMode videoQualityMode, Context context) {
        CharSequence b2;
        CharSequence b3;
        int ordinal = videoQualityMode.ordinal();
        if (ordinal == 0) {
            b2 = b.b(context, R.string.video_quality_mode_auto, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            return b2;
        } else if (ordinal == 1) {
            b3 = b.b(context, R.string.video_quality_mode_full, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            return b3;
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    private final int getTextColor(Context context, ModelAuditLogEntry modelAuditLogEntry) {
        ModelAuditLogEntry.ActionType actionType = modelAuditLogEntry.getActionType();
        if (actionType != null) {
            int ordinal = actionType.ordinal();
            if (ordinal == 1) {
                return ColorCompat.getColor(context, (int) R.color.status_green_500);
            }
            if (ordinal == 2) {
                return ColorCompat.getColor(context, (int) R.color.status_yellow_400);
            }
            if (ordinal == 3) {
                return ColorCompat.getColor(context, (int) R.color.status_red_400);
            }
        }
        return ColorCompat.getColor(context, (int) R.color.primary_300);
    }

    @StringRes
    private final int getThreadChangeString(ModelAuditLogEntry.Change change) {
        String key = change.getKey();
        if (key != null) {
            switch (key.hashCode()) {
                case -1716307998:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_ARCHIVED)) {
                        Object value = change.getValue();
                        Objects.requireNonNull(value, "null cannot be cast to non-null type kotlin.Boolean");
                        boolean booleanValue = ((Boolean) value).booleanValue();
                        if (booleanValue) {
                            return R.string.guild_settings_audit_log_thread_archived;
                        }
                        if (!booleanValue) {
                            return R.string.guild_settings_audit_log_thread_unarchived;
                        }
                        throw new NoWhenBranchMatchedException();
                    }
                    break;
                case -1097452790:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_LOCKED)) {
                        Object value2 = change.getValue();
                        Objects.requireNonNull(value2, "null cannot be cast to non-null type kotlin.Boolean");
                        boolean booleanValue2 = ((Boolean) value2).booleanValue();
                        if (booleanValue2) {
                            return R.string.guild_settings_audit_log_thread_locked;
                        }
                        if (!booleanValue2) {
                            return R.string.guild_settings_audit_log_thread_unlocked;
                        }
                        throw new NoWhenBranchMatchedException();
                    }
                    break;
                case 3373707:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_NAME)) {
                        return getNullableOldValueString(change, R.string.guild_settings_audit_log_thread_name_create, R.string.guild_settings_audit_log_thread_name_change);
                    }
                    break;
                case 987155184:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_RATE_LIMIT_PER_USER)) {
                        return getNullableOldValueString(change, R.string.guild_settings_audit_log_channel_rate_limit_per_user_create, R.string.guild_settings_audit_log_channel_rate_limit_per_user_change);
                    }
                    break;
                case 1901007105:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_AUTO_ARCHIVE_DURATION)) {
                        return getNullableOldValueString(change, R.string.guild_settings_audit_log_thread_auto_archive_duration_create, R.string.guild_settings_audit_log_thread_auto_archive_duration_change);
                    }
                    break;
            }
        }
        return 0;
    }

    @StringRes
    private final int getUserChangeString(ModelAuditLogEntry.Change change) {
        String key = change.getKey();
        if (key != null) {
            switch (key.hashCode()) {
                case -1919744682:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_PRUNE_DELETE_DAYS)) {
                        return R.string.guild_settings_audit_log_member_prune_delete_days;
                    }
                    break;
                case -934964668:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_REASON)) {
                        return R.string.guild_settings_audit_log_common_reason;
                    }
                    break;
                case 1168893:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_ROLES_ADD)) {
                        return R.string.guild_settings_audit_log_member_roles_add;
                    }
                    break;
                case 3079270:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_DEAF)) {
                        Object value = change.getValue();
                        Objects.requireNonNull(value, "null cannot be cast to non-null type kotlin.Boolean");
                        boolean booleanValue = ((Boolean) value).booleanValue();
                        if (booleanValue) {
                            return R.string.guild_settings_audit_log_member_deaf_on;
                        }
                        if (!booleanValue) {
                            return R.string.guild_settings_audit_log_member_deaf_off;
                        }
                        throw new NoWhenBranchMatchedException();
                    }
                    break;
                case 3363353:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_MUTE)) {
                        Object value2 = change.getValue();
                        Objects.requireNonNull(value2, "null cannot be cast to non-null type kotlin.Boolean");
                        boolean booleanValue2 = ((Boolean) value2).booleanValue();
                        if (booleanValue2) {
                            return R.string.guild_settings_audit_log_member_mute_on;
                        }
                        if (!booleanValue2) {
                            return R.string.guild_settings_audit_log_member_mute_off;
                        }
                        throw new NoWhenBranchMatchedException();
                    }
                    break;
                case 3381091:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_NICK)) {
                        return getNullableNewOrOldValueString$default(this, change, Integer.valueOf((int) R.string.guild_settings_audit_log_member_nick_change), Integer.valueOf((int) R.string.guild_settings_audit_log_member_nick_create), Integer.valueOf((int) R.string.guild_settings_audit_log_member_nick_delete), null, 8, null);
                    }
                    break;
                case 950750632:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_ROLES_REMOVE)) {
                        return R.string.guild_settings_audit_log_member_roles_remove;
                    }
                    break;
                case 1217496932:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_GUILD_COMMUNICATION_DISABLED)) {
                        return getNullableNewValueString(change, R.string.guild_settings_audit_log_communication_disabled_until_removed, R.string.guild_settings_audit_log_communication_disabled_until);
                    }
                    break;
            }
        }
        return 0;
    }

    @StringRes
    private final int getWebhookChangeString(ModelAuditLogEntry.Change change) {
        String key = change.getKey();
        if (key != null) {
            switch (key.hashCode()) {
                case -1930808873:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_CHANNEL_ID)) {
                        return getNullableOldValueString(change, R.string.guild_settings_audit_log_webhook_channel_create, R.string.guild_settings_audit_log_webhook_channel_change);
                    }
                    break;
                case -934964668:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_REASON)) {
                        return R.string.guild_settings_audit_log_common_reason;
                    }
                    break;
                case 3373707:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_NAME)) {
                        return getNullableOldValueString(change, R.string.guild_settings_audit_log_webhook_name_create, R.string.guild_settings_audit_log_webhook_name_change);
                    }
                    break;
                case 396929076:
                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_AVATAR_HASH)) {
                        return R.string.guild_settings_audit_log_webhook_avatar;
                    }
                    break;
            }
        }
        return 0;
    }

    private final boolean hasNewValue(ModelAuditLogEntry.Change change, String str) {
        return change.getNewValue() != null && m.areEqual(change.getKey(), str);
    }

    private final String renderPermissionList(ModelAuditLogEntry.Change change, ModelAuditLogEntry modelAuditLogEntry, Context context) {
        Object value = change.getValue();
        if (!(value instanceof Set)) {
            value = null;
        }
        Set set = (Set) value;
        return set != null ? q.joinToString$default(q.filterNot(q.map(q.filter(q.mapNotNull(q.sorted(u.asSequence(set)), new AuditLogChangeUtils$renderPermissionList$1(modelAuditLogEntry)), AuditLogChangeUtils$renderPermissionList$2.INSTANCE), new AuditLogChangeUtils$renderPermissionList$3(context)), AuditLogChangeUtils$renderPermissionList$4.INSTANCE), null, null, null, 0, null, null, 63, null) : "";
    }

    private final String renderPermissions(ModelAuditLogEntry.Change change, ModelAuditLogEntry modelAuditLogEntry, Context context, Map<ModelAuditLogEntry.TargetType, ? extends Map<Long, ? extends CharSequence>> map) {
        return getOverridesPluralString(context, change, modelAuditLogEntry, map) + "\n" + renderPermissionList(change, modelAuditLogEntry, context);
    }

    private final String renderRoles(Context context, ModelAuditLogEntry.Change change) {
        String str = getPluralString(context, change).toString() + "\n";
        Object newValue = change.getNewValue();
        Objects.requireNonNull(newValue, "null cannot be cast to non-null type kotlin.collections.List<com.discord.models.domain.ModelAuditLogEntry.ChangeNameId>");
        boolean z2 = false;
        for (ModelAuditLogEntry.ChangeNameId changeNameId : (List) newValue) {
            if (z2) {
                str = a.v(str, ", ");
            } else {
                z2 = true;
            }
            StringBuilder R = a.R(str);
            R.append(changeNameId.getName());
            str = R.toString();
        }
        return str;
    }

    private final boolean shouldNotRenderChange(ModelAuditLogEntry modelAuditLogEntry, ModelAuditLogEntry.Change change) {
        String key;
        if (modelAuditLogEntry.getActionType() != ModelAuditLogEntry.ActionType.DELETE || RENDERABLE_DELETE_ACTION_TYPES.contains(Integer.valueOf(modelAuditLogEntry.getActionTypeId()))) {
            if (modelAuditLogEntry.getTargetType() == ModelAuditLogEntry.TargetType.CHANNEL) {
                String key2 = change.getKey();
                if (key2 == null) {
                    return false;
                }
                int hashCode = key2.hashCode();
                if (hashCode != 3355) {
                    if (hashCode != 852040376 || !key2.equals(ModelAuditLogEntry.CHANGE_KEY_PERMISSION_OVERWRITES)) {
                        return false;
                    }
                } else if (!key2.equals(ModelAuditLogEntry.CHANGE_KEY_ID)) {
                    return false;
                }
            } else if (modelAuditLogEntry.getTargetType() == ModelAuditLogEntry.TargetType.CHANNEL_OVERWRITE) {
                String key3 = change.getKey();
                if (key3 == null) {
                    return false;
                }
                int hashCode2 = key3.hashCode();
                if (hashCode2 != 3355) {
                    if (hashCode2 != 3575610) {
                        if (hashCode2 != 852040376 || !key3.equals(ModelAuditLogEntry.CHANGE_KEY_PERMISSION_OVERWRITES)) {
                            return false;
                        }
                    } else if (!key3.equals("type")) {
                        return false;
                    }
                } else if (!key3.equals(ModelAuditLogEntry.CHANGE_KEY_ID)) {
                    return false;
                }
            } else if (modelAuditLogEntry.getTargetType() == ModelAuditLogEntry.TargetType.INVITE) {
                String key4 = change.getKey();
                if (key4 == null) {
                    return false;
                }
                int hashCode3 = key4.hashCode();
                if (hashCode3 != 3599308) {
                    if (hashCode3 != 1198966417 || !key4.equals(ModelAuditLogEntry.CHANGE_KEY_INVITER_ID)) {
                        return false;
                    }
                } else if (!key4.equals(ModelAuditLogEntry.CHANGE_KEY_USES)) {
                    return false;
                }
            } else if (modelAuditLogEntry.getTargetType() == ModelAuditLogEntry.TargetType.WEBHOOK) {
                String key5 = change.getKey();
                if (key5 == null) {
                    return false;
                }
                int hashCode4 = key5.hashCode();
                if (hashCode4 != -1287148950) {
                    if (hashCode4 != 3575610 || !key5.equals("type")) {
                        return false;
                    }
                } else if (!key5.equals(ModelAuditLogEntry.CHANGE_KEY_APPLICATION_ID)) {
                    return false;
                }
            } else if (modelAuditLogEntry.getTargetType() == ModelAuditLogEntry.TargetType.INTEGRATION) {
                String key6 = change.getKey();
                if (key6 == null || key6.hashCode() != 3575610 || !key6.equals("type")) {
                    return false;
                }
            } else if (modelAuditLogEntry.getTargetType() != ModelAuditLogEntry.TargetType.STICKER || (key = change.getKey()) == null) {
                return false;
            } else {
                switch (key.hashCode()) {
                    case -1724546052:
                        if (key.equals(ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION)) {
                            return t.isBlank(change.getValue().toString());
                        }
                        return false;
                    case -1306538777:
                        if (!key.equals(ModelAuditLogEntry.CHANGE_KEY_GUILD_ID)) {
                            return false;
                        }
                        break;
                    case -733902135:
                        if (!key.equals(ModelAuditLogEntry.CHANGE_KEY_AVAILABLE)) {
                            return false;
                        }
                        break;
                    case 3355:
                        if (!key.equals(ModelAuditLogEntry.CHANGE_KEY_ID)) {
                            return false;
                        }
                        break;
                    case 3575610:
                        if (!key.equals("type")) {
                            return false;
                        }
                        break;
                    case 93121264:
                        if (!key.equals(ModelAuditLogEntry.CHANGE_KEY_ASSET)) {
                            return false;
                        }
                        break;
                    case 1458614914:
                        if (!key.equals(ModelAuditLogEntry.CHANGE_KEY_FORMAT_TYPE)) {
                            return false;
                        }
                        break;
                    default:
                        return false;
                }
            }
        }
        return true;
    }

    /* JADX WARN: Removed duplicated region for block: B:49:0x017c A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:50:0x00a0 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.lang.CharSequence getChangeSummary(android.content.Context r23, com.discord.models.domain.ModelAuditLogEntry r24, java.util.Map<com.discord.models.domain.ModelAuditLogEntry.TargetType, ? extends java.util.Map<java.lang.Long, ? extends java.lang.CharSequence>> r25) {
        /*
            Method dump skipped, instructions count: 456
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.auditlogs.AuditLogChangeUtils.getChangeSummary(android.content.Context, com.discord.models.domain.ModelAuditLogEntry, java.util.Map):java.lang.CharSequence");
    }

    public final boolean hasChangesToRender(ModelAuditLogEntry modelAuditLogEntry) {
        boolean z2;
        m.checkNotNullParameter(modelAuditLogEntry, "log");
        List<ModelAuditLogEntry.Change> changes = modelAuditLogEntry.getChanges();
        if (changes == null) {
            return false;
        }
        if (!changes.isEmpty()) {
            for (ModelAuditLogEntry.Change change : changes) {
                AuditLogChangeUtils auditLogChangeUtils = INSTANCE;
                m.checkNotNullExpressionValue(change, "it");
                if (!auditLogChangeUtils.shouldNotRenderChange(modelAuditLogEntry, change)) {
                    z2 = true;
                    break;
                }
            }
        }
        z2 = false;
        return z2;
    }

    public final List<ModelAuditLogEntry.Change> transformPermissionChange(ModelAuditLogEntry.Change change) {
        m.checkNotNullParameter(change, "change");
        Pair<HashSet<Long>, HashSet<Long>> calculatePermissionChange = calculatePermissionChange(change);
        ArrayList arrayList = new ArrayList();
        if (!calculatePermissionChange.getFirst().isEmpty()) {
            arrayList.add(new ModelAuditLogEntry.Change(ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_GRANTED, null, calculatePermissionChange.getFirst()));
        }
        if (!calculatePermissionChange.getSecond().isEmpty()) {
            arrayList.add(new ModelAuditLogEntry.Change(ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_DENIED, null, calculatePermissionChange.getSecond()));
        }
        return arrayList;
    }

    public final List<ModelAuditLogEntry.Change> transformPermissionOverride(ModelAuditLogEntry.Change change) {
        m.checkNotNullParameter(change, "change");
        Pair<HashSet<Long>, HashSet<Long>> calculatePermissionChange = calculatePermissionChange(change);
        ArrayList arrayList = new ArrayList();
        if (!calculatePermissionChange.getFirst().isEmpty()) {
            arrayList.add(new ModelAuditLogEntry.Change(change.getKey(), null, calculatePermissionChange.getFirst()));
        }
        if (!calculatePermissionChange.getSecond().isEmpty()) {
            arrayList.add(new ModelAuditLogEntry.Change(ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_RESET, null, calculatePermissionChange.getSecond()));
        }
        return arrayList;
    }
}
