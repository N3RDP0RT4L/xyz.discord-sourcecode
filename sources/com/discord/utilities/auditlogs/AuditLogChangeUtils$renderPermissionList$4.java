package com.discord.utilities.auditlogs;

import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: AuditLogChangeUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "it", "", "invoke", "(Ljava/lang/CharSequence;)Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AuditLogChangeUtils$renderPermissionList$4 extends o implements Function1<CharSequence, Boolean> {
    public static final AuditLogChangeUtils$renderPermissionList$4 INSTANCE = new AuditLogChangeUtils$renderPermissionList$4();

    public AuditLogChangeUtils$renderPermissionList$4() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Boolean invoke(CharSequence charSequence) {
        return Boolean.valueOf(invoke2(charSequence));
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final boolean invoke2(CharSequence charSequence) {
        m.checkNotNullParameter(charSequence, "it");
        return charSequence.length() == 0;
    }
}
