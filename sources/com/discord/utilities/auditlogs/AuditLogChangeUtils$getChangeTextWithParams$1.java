package com.discord.utilities.auditlogs;

import com.discord.i18n.RenderContext;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: AuditLogChangeUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/i18n/RenderContext;", "", "invoke", "(Lcom/discord/i18n/RenderContext;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AuditLogChangeUtils$getChangeTextWithParams$1 extends o implements Function1<RenderContext, Unit> {
    public final /* synthetic */ ModelAuditLogEntry.Change $change;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AuditLogChangeUtils$getChangeTextWithParams$1(ModelAuditLogEntry.Change change) {
        super(1);
        this.$change = change;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(RenderContext renderContext) {
        String str;
        String obj;
        m.checkNotNullParameter(renderContext, "$receiver");
        Map<String, String> map = renderContext.a;
        Object oldValue = this.$change.getOldValue();
        String str2 = "";
        if (oldValue == null || (str = oldValue.toString()) == null) {
            str = str2;
        }
        map.put("oldValue", str);
        Map<String, String> map2 = renderContext.a;
        Object newValue = this.$change.getNewValue();
        if (!(newValue == null || (obj = newValue.toString()) == null)) {
            str2 = obj;
        }
        map2.put("newValue", str2);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(RenderContext renderContext) {
        invoke2(renderContext);
        return Unit.a;
    }
}
