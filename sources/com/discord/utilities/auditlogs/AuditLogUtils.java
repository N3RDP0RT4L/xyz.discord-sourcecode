package com.discord.utilities.auditlogs;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.Resources;
import android.text.format.DateUtils;
import android.view.View;
import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.sticker.Sticker;
import com.discord.app.AppLog;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.stores.StoreStream;
import com.discord.utilities.SnowflakeUtils;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.time.Clock;
import com.discord.utilities.time.ClockFactory;
import d0.g;
import d0.z.d.m;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import kotlin.Lazy;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: AuditLogUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0010%\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010 \n\u0002\b\b\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b1\u00102J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0005\u0010\u0006JG\u0010\u0010\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u00022$\u0010\r\u001a \u0012\u0004\u0012\u00020\b\u0012\u0016\u0012\u0014\u0012\b\u0012\u00060\nj\u0002`\u000b\u0012\u0006\u0012\u0004\u0018\u00010\f0\t0\u00072\b\b\u0002\u0010\u000f\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u001d\u0010\u0013\u001a\u0004\u0018\u00010\u0001*\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0013\u0010\u0014J#\u0010\u0019\u001a\u00020\f2\n\u0010\u0016\u001a\u00060\nj\u0002`\u00152\u0006\u0010\u0018\u001a\u00020\u0017H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJU\u0010\u001d\u001a\u00020\f2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u001b\u001a\u00020\f2\u0006\u0010\u0018\u001a\u00020\u00172$\u0010\r\u001a \u0012\u0004\u0012\u00020\b\u0012\u0016\u0012\u0014\u0012\b\u0012\u00060\nj\u0002`\u000b\u0012\u0006\u0012\u0004\u0018\u00010\f0\t0\u00072\b\b\u0002\u0010\u001c\u001a\u00020\u000e¢\u0006\u0004\b\u001d\u0010\u001eJ\u0017\u0010 \u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\u0004H\u0007¢\u0006\u0004\b \u0010!J\u001f\u0010$\u001a\u00020\u00042\u0006\u0010#\u001a\u00020\"2\u0006\u0010\u001f\u001a\u00020\u0004H\u0007¢\u0006\u0004\b$\u0010%J\u0017\u0010&\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\u0004H\u0007¢\u0006\u0004\b&\u0010!J\u0015\u0010'\u001a\u00060\nj\u0002`\u0015*\u00020\u0002¢\u0006\u0004\b'\u0010(J\u001d\u0010)\u001a\u00020\f2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u0017¢\u0006\u0004\b)\u0010*R#\u00100\u001a\b\u0012\u0004\u0012\u00020\u00040+8F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b,\u0010-\u001a\u0004\b.\u0010/¨\u00063"}, d2 = {"Lcom/discord/utilities/auditlogs/AuditLogUtils;", "", "Lcom/discord/models/domain/ModelAuditLogEntry;", "auditLogEntry", "", "getEntryTitle", "(Lcom/discord/models/domain/ModelAuditLogEntry;)I", "", "Lcom/discord/models/domain/ModelAuditLogEntry$TargetType;", "", "", "Lcom/discord/stores/TargetId;", "", "targets", "", "channelPrefix", "getTargetText", "(Lcom/discord/models/domain/ModelAuditLogEntry;Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;", "keyForValue", "getTargetValue", "(Lcom/discord/models/domain/ModelAuditLogEntry;Ljava/lang/String;)Ljava/lang/Object;", "Lcom/discord/primitives/Timestamp;", "timestamp", "Landroid/content/Context;", "context", "getTimestampText", "(JLandroid/content/Context;)Ljava/lang/CharSequence;", "username", "prefix", "getHeaderString", "(Lcom/discord/models/domain/ModelAuditLogEntry;Ljava/lang/CharSequence;Landroid/content/Context;Ljava/util/Map;Ljava/lang/String;)Ljava/lang/CharSequence;", "actionTypeId", "getActionName", "(I)I", "Landroid/view/View;", "view", "getTargetTypeImage", "(Landroid/view/View;I)I", "getActionTypeImage", "getTimestampStart", "(Lcom/discord/models/domain/ModelAuditLogEntry;)J", "getTimestampString", "(Lcom/discord/models/domain/ModelAuditLogEntry;Landroid/content/Context;)Ljava/lang/CharSequence;", "", "ALL_ACTION_TYPES$delegate", "Lkotlin/Lazy;", "getALL_ACTION_TYPES", "()Ljava/util/List;", "ALL_ACTION_TYPES", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AuditLogUtils {
    public static final AuditLogUtils INSTANCE = new AuditLogUtils();
    private static final Lazy ALL_ACTION_TYPES$delegate = g.lazy(AuditLogUtils$ALL_ACTION_TYPES$2.INSTANCE);

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;
        public static final /* synthetic */ int[] $EnumSwitchMapping$2;

        static {
            ModelAuditLogEntry.TargetType.values();
            int[] iArr = new int[15];
            $EnumSwitchMapping$0 = iArr;
            iArr[ModelAuditLogEntry.TargetType.ALL.ordinal()] = 1;
            iArr[ModelAuditLogEntry.TargetType.GUILD.ordinal()] = 2;
            ModelAuditLogEntry.TargetType targetType = ModelAuditLogEntry.TargetType.CHANNEL;
            iArr[targetType.ordinal()] = 3;
            iArr[ModelAuditLogEntry.TargetType.CHANNEL_OVERWRITE.ordinal()] = 4;
            iArr[ModelAuditLogEntry.TargetType.USER.ordinal()] = 5;
            ModelAuditLogEntry.TargetType targetType2 = ModelAuditLogEntry.TargetType.ROLE;
            iArr[targetType2.ordinal()] = 6;
            ModelAuditLogEntry.TargetType targetType3 = ModelAuditLogEntry.TargetType.INVITE;
            iArr[targetType3.ordinal()] = 7;
            ModelAuditLogEntry.TargetType targetType4 = ModelAuditLogEntry.TargetType.WEBHOOK;
            iArr[targetType4.ordinal()] = 8;
            iArr[ModelAuditLogEntry.TargetType.EMOJI.ordinal()] = 9;
            ModelAuditLogEntry.TargetType targetType5 = ModelAuditLogEntry.TargetType.INTEGRATION;
            iArr[targetType5.ordinal()] = 10;
            iArr[ModelAuditLogEntry.TargetType.STAGE_INSTANCE.ordinal()] = 11;
            iArr[ModelAuditLogEntry.TargetType.GUILD_SCHEDULED_EVENT.ordinal()] = 12;
            iArr[ModelAuditLogEntry.TargetType.STICKER.ordinal()] = 13;
            iArr[ModelAuditLogEntry.TargetType.THREAD.ordinal()] = 14;
            ModelAuditLogEntry.ActionType.values();
            int[] iArr2 = new int[4];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[ModelAuditLogEntry.ActionType.CREATE.ordinal()] = 1;
            iArr2[ModelAuditLogEntry.ActionType.UPDATE.ordinal()] = 2;
            iArr2[ModelAuditLogEntry.ActionType.DELETE.ordinal()] = 3;
            ModelAuditLogEntry.TargetType.values();
            int[] iArr3 = new int[15];
            $EnumSwitchMapping$2 = iArr3;
            iArr3[targetType.ordinal()] = 1;
            iArr3[targetType2.ordinal()] = 2;
            iArr3[targetType3.ordinal()] = 3;
            iArr3[targetType4.ordinal()] = 4;
            iArr3[targetType5.ordinal()] = 5;
        }
    }

    private AuditLogUtils() {
    }

    /* JADX WARN: Multi-variable type inference failed */
    @StringRes
    private final int getEntryTitle(ModelAuditLogEntry modelAuditLogEntry) {
        int actionTypeId = modelAuditLogEntry.getActionTypeId();
        ModelAuditLogEntry.Change change = null;
        switch (actionTypeId) {
            case 1:
                return R.string.guild_settings_audit_log_guild_update;
            case 20:
                return R.string.guild_settings_audit_log_member_kick;
            case 21:
                return R.string.guild_settings_audit_log_member_prune;
            case 22:
                return R.string.guild_settings_audit_log_member_ban_add;
            case 23:
                return R.string.guild_settings_audit_log_member_ban_remove;
            case 24:
                return R.string.guild_settings_audit_log_member_update;
            case 25:
                return R.string.guild_settings_audit_log_member_role_update;
            case 26:
                return R.string.guild_settings_audit_log_member_move;
            case 27:
                return R.string.guild_settings_audit_log_member_disconnect;
            case 28:
                return R.string.guild_settings_audit_log_bot_add;
            case 110:
                List<ModelAuditLogEntry.Change> changes = modelAuditLogEntry.getChanges();
                if (changes != null) {
                    Iterator<T> it = changes.iterator();
                    while (true) {
                        if (it.hasNext()) {
                            Object next = it.next();
                            ModelAuditLogEntry.Change change2 = (ModelAuditLogEntry.Change) next;
                            m.checkNotNullExpressionValue(change2, "it");
                            if (m.areEqual(change2.getKey(), "type")) {
                                change = next;
                            }
                        }
                    }
                    change = change;
                }
                if (change == null) {
                    return 0;
                }
                Object newValue = change.getNewValue();
                Objects.requireNonNull(newValue, "null cannot be cast to non-null type kotlin.Long");
                int longValue = (int) ((Long) newValue).longValue();
                return longValue != 10 ? longValue != 12 ? R.string.guild_settings_audit_log_thread_create : R.string.guild_settings_audit_log_private_thread_create : R.string.guild_settings_audit_log_announcement_thread_create;
            case 111:
                return R.string.guild_settings_audit_log_thread_update;
            case 112:
                return R.string.guild_settings_audit_log_thread_delete;
            default:
                switch (actionTypeId) {
                    case 10:
                        List<ModelAuditLogEntry.Change> changes2 = modelAuditLogEntry.getChanges();
                        if (changes2 != null) {
                            Iterator<T> it2 = changes2.iterator();
                            while (true) {
                                if (it2.hasNext()) {
                                    Object next2 = it2.next();
                                    ModelAuditLogEntry.Change change3 = (ModelAuditLogEntry.Change) next2;
                                    m.checkNotNullExpressionValue(change3, "it");
                                    if (m.areEqual(change3.getKey(), "type")) {
                                        change = next2;
                                    }
                                }
                            }
                            change = change;
                        }
                        if (change == null) {
                            return 0;
                        }
                        Object newValue2 = change.getNewValue();
                        Objects.requireNonNull(newValue2, "null cannot be cast to non-null type kotlin.Long");
                        int longValue2 = (int) ((Long) newValue2).longValue();
                        return longValue2 != 2 ? longValue2 != 4 ? longValue2 != 13 ? R.string.guild_settings_audit_log_channel_text_create : R.string.guild_settings_audit_log_channel_stage_create : R.string.guild_settings_audit_log_channel_category_create : R.string.guild_settings_audit_log_channel_voice_create;
                    case 11:
                        return R.string.guild_settings_audit_log_channel_update;
                    case 12:
                        return R.string.guild_settings_audit_log_channel_delete;
                    case 13:
                        return R.string.guild_settings_audit_log_channel_overwrite_create;
                    case 14:
                        return R.string.guild_settings_audit_log_channel_overwrite_update;
                    case 15:
                        return R.string.guild_settings_audit_log_channel_overwrite_delete;
                    default:
                        switch (actionTypeId) {
                            case 30:
                                return R.string.guild_settings_audit_log_role_create;
                            case 31:
                                return R.string.guild_settings_audit_log_role_update;
                            case 32:
                                return R.string.guild_settings_audit_log_role_delete;
                            default:
                                switch (actionTypeId) {
                                    case 40:
                                        return R.string.guild_settings_audit_log_invite_create;
                                    case 41:
                                        return R.string.guild_settings_audit_log_invite_update;
                                    case 42:
                                        return R.string.guild_settings_audit_log_invite_delete;
                                    default:
                                        switch (actionTypeId) {
                                            case 50:
                                                return R.string.guild_settings_audit_log_webhook_create;
                                            case 51:
                                                return R.string.guild_settings_audit_log_webhook_update;
                                            case 52:
                                                return R.string.guild_settings_audit_log_webhook_delete;
                                            default:
                                                switch (actionTypeId) {
                                                    case 60:
                                                        return R.string.guild_settings_audit_log_emoji_create;
                                                    case 61:
                                                        return R.string.guild_settings_audit_log_emoji_update;
                                                    case 62:
                                                        return R.string.guild_settings_audit_log_emoji_delete;
                                                    default:
                                                        switch (actionTypeId) {
                                                            case 72:
                                                                return R.string.guild_settings_audit_log_message_delete;
                                                            case 73:
                                                                return R.string.guild_settings_audit_log_message_bulk_delete;
                                                            case 74:
                                                                return R.string.guild_settings_audit_log_message_pin;
                                                            case 75:
                                                                return R.string.guild_settings_audit_log_message_unpin;
                                                            default:
                                                                switch (actionTypeId) {
                                                                    case 80:
                                                                        return R.string.guild_settings_audit_log_integration_create;
                                                                    case 81:
                                                                        return R.string.guild_settings_audit_log_integration_update;
                                                                    case 82:
                                                                        return R.string.guild_settings_audit_log_integration_delete;
                                                                    case 83:
                                                                        return R.string.guild_settings_audit_log_stage_instance_create;
                                                                    case 84:
                                                                        return R.string.guild_settings_audit_log_stage_instance_update;
                                                                    case 85:
                                                                        return modelAuditLogEntry.getUserId() == 0 ? R.string.guild_settings_audit_log_stage_instance_delete_no_user : R.string.guild_settings_audit_log_stage_instance_delete;
                                                                    default:
                                                                        switch (actionTypeId) {
                                                                            case 90:
                                                                                return R.string.guild_settings_audit_log_sticker_create;
                                                                            case 91:
                                                                                return R.string.guild_settings_audit_log_sticker_update;
                                                                            case 92:
                                                                                return R.string.guild_settings_audit_log_sticker_delete;
                                                                            default:
                                                                                switch (actionTypeId) {
                                                                                    case 100:
                                                                                        return R.string.guild_settings_audit_log_scheduled_event_create;
                                                                                    case 101:
                                                                                        return R.string.guild_settings_audit_log_scheduled_event_update;
                                                                                    case 102:
                                                                                        return R.string.guild_settings_audit_log_scheduled_event_delete;
                                                                                    default:
                                                                                        AppLog appLog = AppLog.g;
                                                                                        StringBuilder R = a.R("Unknown audit log action type: ");
                                                                                        R.append(modelAuditLogEntry.getActionTypeId());
                                                                                        Logger.e$default(appLog, R.toString(), null, null, 6, null);
                                                                                        return R.string.guild_settings_audit_log_unknown_action;
                                                                                }
                                                                        }
                                                                }
                                                        }
                                                }
                                        }
                                }
                        }
                }
        }
    }

    public static /* synthetic */ CharSequence getHeaderString$default(AuditLogUtils auditLogUtils, ModelAuditLogEntry modelAuditLogEntry, CharSequence charSequence, Context context, Map map, String str, int i, Object obj) {
        if ((i & 16) != 0) {
            str = "";
        }
        return auditLogUtils.getHeaderString(modelAuditLogEntry, charSequence, context, map, str);
    }

    private final String getTargetText(ModelAuditLogEntry modelAuditLogEntry, Map<ModelAuditLogEntry.TargetType, ? extends Map<Long, CharSequence>> map, String str) {
        CharSequence charSequence;
        ModelAuditLogEntry.TargetType targetType = modelAuditLogEntry.getTargetType();
        if (targetType == ModelAuditLogEntry.TargetType.CHANNEL_OVERWRITE) {
            targetType = ModelAuditLogEntry.TargetType.CHANNEL;
        }
        Map<Long, CharSequence> map2 = map.get(targetType);
        if (map2 == null || (charSequence = map2.get(Long.valueOf(modelAuditLogEntry.getTargetId()))) == null) {
            if (targetType != null) {
                int ordinal = targetType.ordinal();
                if (ordinal == 3) {
                    StringBuilder R = a.R(str);
                    R.append(getTargetValue(modelAuditLogEntry, ModelAuditLogEntry.CHANGE_KEY_NAME));
                    charSequence = R.toString();
                } else if (ordinal == 10) {
                    charSequence = getTargetValue(modelAuditLogEntry, "type");
                } else if (ordinal == 6) {
                    ModelAuditLogEntry.Options options = modelAuditLogEntry.getOptions();
                    if (options == null || (charSequence = options.getRoleName()) == null) {
                        charSequence = getTargetValue(modelAuditLogEntry, ModelAuditLogEntry.CHANGE_KEY_NAME);
                    }
                } else if (ordinal == 7) {
                    charSequence = getTargetValue(modelAuditLogEntry, ModelAuditLogEntry.CHANGE_KEY_CODE);
                } else if (ordinal == 8) {
                    charSequence = getTargetValue(modelAuditLogEntry, ModelAuditLogEntry.CHANGE_KEY_NAME);
                }
            }
            charSequence = null;
        }
        if (charSequence == null) {
            charSequence = Long.valueOf(modelAuditLogEntry.getTargetId());
        }
        return charSequence.toString();
    }

    public static /* synthetic */ String getTargetText$default(AuditLogUtils auditLogUtils, ModelAuditLogEntry modelAuditLogEntry, Map map, String str, int i, Object obj) {
        if ((i & 4) != 0) {
            str = "";
        }
        return auditLogUtils.getTargetText(modelAuditLogEntry, map, str);
    }

    private final Object getTargetValue(ModelAuditLogEntry modelAuditLogEntry, String str) {
        Object obj;
        List<ModelAuditLogEntry.Change> changes = modelAuditLogEntry.getChanges();
        if (changes == null) {
            return null;
        }
        Iterator<T> it = changes.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            ModelAuditLogEntry.Change change = (ModelAuditLogEntry.Change) obj;
            m.checkNotNullExpressionValue(change, "it");
            if (m.areEqual(change.getKey(), str)) {
                break;
            }
        }
        ModelAuditLogEntry.Change change2 = (ModelAuditLogEntry.Change) obj;
        if (change2 != null) {
            return change2.getValue();
        }
        return null;
    }

    private final CharSequence getTimestampText(long j, Context context) {
        CharSequence b2;
        Clock clock = ClockFactory.get();
        CharSequence relativeTimeSpanString = DateUtils.getRelativeTimeSpanString(j, clock.currentTimeMillis(), 86400000L, 131092);
        if (TimeUnit.MILLISECONDS.toDays(clock.currentTimeMillis() - j) < 7) {
            b2 = b.b(context, R.string.guild_settings_audit_log_time_at_android, new Object[]{relativeTimeSpanString, DateUtils.formatDateTime(context, j, 1)}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b2;
        }
        m.checkNotNullExpressionValue(relativeTimeSpanString, "timeString");
        return relativeTimeSpanString;
    }

    public final List<Integer> getALL_ACTION_TYPES() {
        return (List) ALL_ACTION_TYPES$delegate.getValue();
    }

    @StringRes
    public final int getActionName(int i) {
        if (i == 0) {
            return R.string.guild_settings_filter_all_actions;
        }
        if (i == 1) {
            return R.string.guild_settings_action_filter_guild_update;
        }
        switch (i) {
            case 10:
                return R.string.guild_settings_action_filter_channel_create;
            case 11:
                return R.string.guild_settings_action_filter_channel_update;
            case 12:
                return R.string.guild_settings_action_filter_channel_delete;
            case 13:
                return R.string.guild_settings_action_filter_channel_overwrite_create;
            case 14:
                return R.string.guild_settings_action_filter_channel_overwrite_update;
            case 15:
                return R.string.guild_settings_action_filter_channel_overwrite_delete;
            default:
                switch (i) {
                    case 20:
                        return R.string.guild_settings_action_filter_member_kick;
                    case 21:
                        return R.string.guild_settings_action_filter_member_prune;
                    case 22:
                        return R.string.guild_settings_action_filter_member_ban_add;
                    case 23:
                        return R.string.guild_settings_action_filter_member_ban_remove;
                    case 24:
                        return R.string.guild_settings_action_filter_member_update;
                    case 25:
                        return R.string.guild_settings_action_filter_member_role_update;
                    case 26:
                        return R.string.guild_settings_action_filter_member_move;
                    case 27:
                        return R.string.guild_settings_action_filter_member_disconnect;
                    case 28:
                        return R.string.guild_settings_action_filter_bot_add;
                    case 100:
                        return R.string.guild_settings_action_filter_guild_scheduled_event_create;
                    case 101:
                        return R.string.guild_settings_action_filter_guild_scheduled_event_update;
                    case 102:
                        return R.string.guild_settings_action_filter_guild_scheduled_event_delete;
                    case 110:
                        return R.string.guild_settings_action_filter_thread_create;
                    case 111:
                        return R.string.guild_settings_action_filter_thread_update;
                    case 112:
                        return R.string.guild_settings_action_filter_thread_delete;
                    default:
                        switch (i) {
                            case 30:
                                return R.string.guild_settings_action_filter_role_create;
                            case 31:
                                return R.string.guild_settings_action_filter_role_update;
                            case 32:
                                return R.string.guild_settings_action_filter_role_delete;
                            default:
                                switch (i) {
                                    case 40:
                                        return R.string.guild_settings_action_filter_invite_create;
                                    case 41:
                                        return R.string.guild_settings_action_filter_invite_update;
                                    case 42:
                                        return R.string.guild_settings_action_filter_invite_delete;
                                    default:
                                        switch (i) {
                                            case 50:
                                                return R.string.guild_settings_action_filter_webhook_create;
                                            case 51:
                                                return R.string.guild_settings_action_filter_webhook_update;
                                            case 52:
                                                return R.string.guild_settings_action_filter_webhook_delete;
                                            default:
                                                switch (i) {
                                                    case 60:
                                                        return R.string.guild_settings_action_filter_emoji_create;
                                                    case 61:
                                                        return R.string.guild_settings_action_filter_emoji_update;
                                                    case 62:
                                                        return R.string.guild_settings_action_filter_emoji_delete;
                                                    default:
                                                        switch (i) {
                                                            case 72:
                                                                return R.string.guild_settings_action_filter_message_delete;
                                                            case 73:
                                                                return R.string.guild_settings_action_filter_message_bulk_delete;
                                                            case 74:
                                                                return R.string.guild_settings_action_filter_message_pin;
                                                            case 75:
                                                                return R.string.guild_settings_action_filter_message_unpin;
                                                            default:
                                                                switch (i) {
                                                                    case 80:
                                                                        return R.string.guild_settings_action_filter_integration_create;
                                                                    case 81:
                                                                        return R.string.guild_settings_action_filter_integration_update;
                                                                    case 82:
                                                                        return R.string.guild_settings_action_filter_integration_delete;
                                                                    case 83:
                                                                        return R.string.guild_settings_action_filter_stage_instance_create;
                                                                    case 84:
                                                                        return R.string.guild_settings_action_filter_stage_instance_update;
                                                                    case 85:
                                                                        return R.string.guild_settings_action_filter_stage_instance_delete;
                                                                    default:
                                                                        switch (i) {
                                                                            case 90:
                                                                                return R.string.guild_settings_action_filter_sticker_create;
                                                                            case 91:
                                                                                return R.string.guild_settings_action_filter_sticker_update;
                                                                            case 92:
                                                                                return R.string.guild_settings_action_filter_sticker_delete;
                                                                            default:
                                                                                return 0;
                                                                        }
                                                                }
                                                        }
                                                }
                                        }
                                }
                        }
                }
        }
    }

    @DrawableRes
    public final int getActionTypeImage(int i) {
        ModelAuditLogEntry.ActionType actionType = ModelAuditLogEntry.getActionType(i);
        if (actionType != null) {
            int ordinal = actionType.ordinal();
            if (ordinal == 1) {
                return R.drawable.ic_audit_audit_create_24dp;
            }
            if (ordinal == 2) {
                return R.drawable.ic_audit_audit_update_24dp;
            }
            if (ordinal == 3) {
                return R.drawable.ic_audit_audit_delete_24dp;
            }
        }
        return 0;
    }

    public final CharSequence getHeaderString(ModelAuditLogEntry modelAuditLogEntry, CharSequence charSequence, Context context, Map<ModelAuditLogEntry.TargetType, ? extends Map<Long, CharSequence>> map, String str) {
        CharSequence b2;
        Object obj;
        CharSequence b3;
        CharSequence charSequence2;
        CharSequence b4;
        CharSequence b5;
        Object obj2;
        CharSequence b6;
        CharSequence charSequence3;
        CharSequence b7;
        CharSequence b8;
        CharSequence charSequence4;
        CharSequence b9;
        CharSequence charSequence5;
        CharSequence b10;
        CharSequence b11;
        String str2;
        CharSequence b12;
        Object obj3;
        Object obj4;
        CharSequence b13;
        m.checkNotNullParameter(modelAuditLogEntry, "auditLogEntry");
        m.checkNotNullParameter(charSequence, "username");
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(map, "targets");
        m.checkNotNullParameter(str, "prefix");
        int actionTypeId = modelAuditLogEntry.getActionTypeId();
        if (actionTypeId == 21) {
            int entryTitle = getEntryTitle(modelAuditLogEntry);
            Object[] objArr = new Object[2];
            objArr[0] = charSequence;
            Resources resources = context.getResources();
            m.checkNotNullExpressionValue(resources, "context.resources");
            ModelAuditLogEntry.Options options = modelAuditLogEntry.getOptions();
            int membersRemoved = options != null ? options.getMembersRemoved() : 0;
            Object[] objArr2 = new Object[1];
            ModelAuditLogEntry.Options options2 = modelAuditLogEntry.getOptions();
            objArr2[0] = Integer.valueOf(options2 != null ? options2.getMembersRemoved() : 0);
            objArr[1] = StringResourceUtilsKt.getQuantityString(resources, context, (int) R.plurals.guild_settings_audit_log_member_prune_count, membersRemoved, objArr2);
            b2 = b.b(context, entryTitle, objArr, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b2;
        } else if (actionTypeId == 42) {
            int entryTitle2 = getEntryTitle(modelAuditLogEntry);
            Object[] objArr3 = new Object[2];
            objArr3[0] = charSequence;
            List<ModelAuditLogEntry.Change> changes = modelAuditLogEntry.getChanges();
            if (changes != null) {
                for (ModelAuditLogEntry.Change change : changes) {
                    m.checkNotNullExpressionValue(change, "it");
                    if (m.areEqual(change.getKey(), ModelAuditLogEntry.CHANGE_KEY_CODE)) {
                        if (change != null) {
                            obj = change.getOldValue();
                            objArr3[1] = String.valueOf(obj);
                            b3 = b.b(context, entryTitle2, objArr3, (r4 & 4) != 0 ? b.C0034b.j : null);
                            return b3;
                        }
                    }
                }
                throw new NoSuchElementException("Collection contains no element matching the predicate.");
            }
            obj = null;
            objArr3[1] = String.valueOf(obj);
            b3 = b.b(context, entryTitle2, objArr3, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b3;
        } else if (actionTypeId == 26) {
            ModelAuditLogEntry.Options options3 = modelAuditLogEntry.getOptions();
            Long valueOf = options3 != null ? Long.valueOf(options3.getChannelId()) : null;
            int entryTitle3 = getEntryTitle(modelAuditLogEntry);
            Object[] objArr4 = new Object[3];
            objArr4[0] = charSequence;
            ModelAuditLogEntry.Options options4 = modelAuditLogEntry.getOptions();
            int count = options4 != null ? options4.getCount() : 0;
            Object[] objArr5 = new Object[1];
            ModelAuditLogEntry.Options options5 = modelAuditLogEntry.getOptions();
            objArr5[0] = Integer.valueOf(options5 != null ? options5.getCount() : 0);
            objArr4[1] = StringResourceUtilsKt.getI18nPluralString(context, R.plurals.guild_settings_audit_log_member_move_count, count, objArr5);
            Map<Long, CharSequence> map2 = map.get(ModelAuditLogEntry.TargetType.CHANNEL);
            if (map2 == null || (charSequence2 = map2.get(valueOf)) == null) {
                charSequence2 = valueOf != null ? String.valueOf(valueOf.longValue()) : null;
            }
            objArr4[2] = charSequence2;
            b4 = b.b(context, entryTitle3, objArr4, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b4;
        } else if (actionTypeId != 27) {
            switch (actionTypeId) {
                case 60:
                case 61:
                case 62:
                    int entryTitle4 = getEntryTitle(modelAuditLogEntry);
                    Object[] objArr6 = new Object[2];
                    objArr6[0] = charSequence;
                    List<ModelAuditLogEntry.Change> changes2 = modelAuditLogEntry.getChanges();
                    if (changes2 != null) {
                        for (ModelAuditLogEntry.Change change2 : changes2) {
                            m.checkNotNullExpressionValue(change2, "it");
                            if (m.areEqual(change2.getKey(), ModelAuditLogEntry.CHANGE_KEY_NAME)) {
                                if (change2 != null) {
                                    obj2 = change2.getValue();
                                    objArr6[1] = String.valueOf(obj2);
                                    b6 = b.b(context, entryTitle4, objArr6, (r4 & 4) != 0 ? b.C0034b.j : null);
                                    return b6;
                                }
                            }
                        }
                        throw new NoSuchElementException("Collection contains no element matching the predicate.");
                    }
                    obj2 = null;
                    objArr6[1] = String.valueOf(obj2);
                    b6 = b.b(context, entryTitle4, objArr6, (r4 & 4) != 0 ? b.C0034b.j : null);
                    return b6;
                default:
                    switch (actionTypeId) {
                        case 72:
                            ModelAuditLogEntry.Options options6 = modelAuditLogEntry.getOptions();
                            Long valueOf2 = options6 != null ? Long.valueOf(options6.getChannelId()) : null;
                            int entryTitle5 = getEntryTitle(modelAuditLogEntry);
                            Object[] objArr7 = new Object[4];
                            objArr7[0] = charSequence;
                            ModelAuditLogEntry.Options options7 = modelAuditLogEntry.getOptions();
                            int count2 = options7 != null ? options7.getCount() : 0;
                            Object[] objArr8 = new Object[1];
                            ModelAuditLogEntry.Options options8 = modelAuditLogEntry.getOptions();
                            objArr8[0] = Integer.valueOf(options8 != null ? options8.getCount() : 0);
                            objArr7[1] = StringResourceUtilsKt.getI18nPluralString(context, R.plurals.guild_settings_audit_log_message_delete_count, count2, objArr8);
                            objArr7[2] = getTargetText(modelAuditLogEntry, map, str);
                            Map<Long, CharSequence> map3 = map.get(ModelAuditLogEntry.TargetType.CHANNEL);
                            if (map3 == null || (charSequence3 = map3.get(valueOf2)) == null) {
                                charSequence3 = valueOf2 != null ? String.valueOf(valueOf2.longValue()) : null;
                            }
                            objArr7[3] = charSequence3;
                            b7 = b.b(context, entryTitle5, objArr7, (r4 & 4) != 0 ? b.C0034b.j : null);
                            return b7;
                        case 73:
                            int entryTitle6 = getEntryTitle(modelAuditLogEntry);
                            Object[] objArr9 = new Object[3];
                            objArr9[0] = charSequence;
                            ModelAuditLogEntry.Options options9 = modelAuditLogEntry.getOptions();
                            int count3 = options9 != null ? options9.getCount() : 0;
                            Object[] objArr10 = new Object[1];
                            ModelAuditLogEntry.Options options10 = modelAuditLogEntry.getOptions();
                            objArr10[0] = Integer.valueOf(options10 != null ? options10.getCount() : 0);
                            objArr9[1] = StringResourceUtilsKt.getI18nPluralString(context, R.plurals.guild_settings_audit_log_message_bulk_delete_count, count3, objArr10);
                            objArr9[2] = getTargetText(modelAuditLogEntry, map, str);
                            b8 = b.b(context, entryTitle6, objArr9, (r4 & 4) != 0 ? b.C0034b.j : null);
                            return b8;
                        case 74:
                        case 75:
                            ModelAuditLogEntry.Options options11 = modelAuditLogEntry.getOptions();
                            Long valueOf3 = options11 != null ? Long.valueOf(options11.getChannelId()) : null;
                            int entryTitle7 = getEntryTitle(modelAuditLogEntry);
                            Object[] objArr11 = new Object[3];
                            objArr11[0] = charSequence;
                            objArr11[1] = getTargetText(modelAuditLogEntry, map, str);
                            Map<Long, CharSequence> map4 = map.get(ModelAuditLogEntry.TargetType.CHANNEL);
                            if (map4 == null || (charSequence4 = map4.get(valueOf3)) == null) {
                                charSequence4 = valueOf3 != null ? String.valueOf(valueOf3.longValue()) : null;
                            }
                            objArr11[2] = charSequence4;
                            b9 = b.b(context, entryTitle7, objArr11, (r4 & 4) != 0 ? b.C0034b.j : null);
                            return b9;
                        default:
                            switch (actionTypeId) {
                                case 83:
                                case 84:
                                case 85:
                                    ModelAuditLogEntry.Options options12 = modelAuditLogEntry.getOptions();
                                    Long valueOf4 = options12 != null ? Long.valueOf(options12.getChannelId()) : null;
                                    Map<Long, CharSequence> map5 = map.get(ModelAuditLogEntry.TargetType.CHANNEL);
                                    if (map5 == null || (charSequence5 = map5.get(valueOf4)) == null) {
                                        charSequence5 = valueOf4 != null ? String.valueOf(valueOf4.longValue()) : null;
                                    }
                                    if (modelAuditLogEntry.getUserId() == 0) {
                                        b11 = b.b(context, getEntryTitle(modelAuditLogEntry), new Object[]{charSequence5}, (r4 & 4) != 0 ? b.C0034b.j : null);
                                        return b11;
                                    }
                                    b10 = b.b(context, getEntryTitle(modelAuditLogEntry), new Object[]{charSequence, charSequence5}, (r4 & 4) != 0 ? b.C0034b.j : null);
                                    return b10;
                                default:
                                    switch (actionTypeId) {
                                        case 90:
                                        case 91:
                                        case 92:
                                            int entryTitle8 = getEntryTitle(modelAuditLogEntry);
                                            Object[] objArr12 = new Object[2];
                                            objArr12[0] = charSequence;
                                            Sticker guildSticker = StoreStream.Companion.getGuildStickers().getGuildSticker(modelAuditLogEntry.getTargetId());
                                            if (guildSticker == null || (str2 = guildSticker.h()) == null) {
                                                List<ModelAuditLogEntry.Change> changes3 = modelAuditLogEntry.getChanges();
                                                if (changes3 != null) {
                                                    Iterator<T> it = changes3.iterator();
                                                    while (true) {
                                                        if (it.hasNext()) {
                                                            obj4 = it.next();
                                                            ModelAuditLogEntry.Change change3 = (ModelAuditLogEntry.Change) obj4;
                                                            m.checkNotNullExpressionValue(change3, "it");
                                                            if (m.areEqual(change3.getKey(), ModelAuditLogEntry.CHANGE_KEY_NAME)) {
                                                            }
                                                        } else {
                                                            obj4 = null;
                                                        }
                                                    }
                                                    ModelAuditLogEntry.Change change4 = (ModelAuditLogEntry.Change) obj4;
                                                    if (change4 != null) {
                                                        obj3 = change4.getValue();
                                                        str2 = String.valueOf(obj3);
                                                    }
                                                }
                                                obj3 = null;
                                                str2 = String.valueOf(obj3);
                                            }
                                            objArr12[1] = str2;
                                            b12 = b.b(context, entryTitle8, objArr12, (r4 & 4) != 0 ? b.C0034b.j : null);
                                            return b12;
                                        default:
                                            b13 = b.b(context, getEntryTitle(modelAuditLogEntry), new Object[]{charSequence, getTargetText(modelAuditLogEntry, map, str)}, (r4 & 4) != 0 ? b.C0034b.j : null);
                                            return b13;
                                    }
                            }
                    }
            }
        } else {
            int entryTitle9 = getEntryTitle(modelAuditLogEntry);
            Object[] objArr13 = new Object[2];
            objArr13[0] = charSequence;
            ModelAuditLogEntry.Options options13 = modelAuditLogEntry.getOptions();
            int count4 = options13 != null ? options13.getCount() : 0;
            Object[] objArr14 = new Object[1];
            ModelAuditLogEntry.Options options14 = modelAuditLogEntry.getOptions();
            objArr14[0] = Integer.valueOf(options14 != null ? options14.getCount() : 0);
            objArr13[1] = StringResourceUtilsKt.getI18nPluralString(context, R.plurals.guild_settings_audit_log_member_disconnect_count, count4, objArr14);
            b5 = b.b(context, entryTitle9, objArr13, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b5;
        }
    }

    @DrawableRes
    public final int getTargetTypeImage(View view, int i) {
        m.checkNotNullParameter(view, "view");
        ModelAuditLogEntry.TargetType targetType = ModelAuditLogEntry.getTargetType(i);
        m.checkNotNullExpressionValue(targetType, "ModelAuditLogEntry.getTargetType(actionTypeId)");
        if (i == 72) {
            return DrawableCompat.getThemedDrawableRes(view, (int) R.attr.ic_audit_message, 0);
        }
        int ordinal = targetType.ordinal();
        if (ordinal == 0) {
            return DrawableCompat.getThemedDrawableRes(view, (int) R.attr.ic_audit_all, 0);
        }
        switch (ordinal) {
            case 2:
                return DrawableCompat.getThemedDrawableRes(view, (int) R.attr.ic_audit_server, 0);
            case 3:
            case 4:
                return DrawableCompat.getThemedDrawableRes(view, (int) R.attr.ic_audit_channel, 0);
            case 5:
                return DrawableCompat.getThemedDrawableRes(view, (int) R.attr.ic_audit_member, 0);
            case 6:
                return DrawableCompat.getThemedDrawableRes(view, (int) R.attr.ic_audit_role, 0);
            case 7:
                return DrawableCompat.getThemedDrawableRes(view, (int) R.attr.ic_audit_invite, 0);
            case 8:
                return DrawableCompat.getThemedDrawableRes(view, (int) R.attr.ic_audit_webhook, 0);
            case 9:
                return DrawableCompat.getThemedDrawableRes(view, (int) R.attr.ic_audit_emoji, 0);
            case 10:
                return DrawableCompat.getThemedDrawableRes(view, (int) R.attr.ic_audit_integration, 0);
            case 11:
                return DrawableCompat.getThemedDrawableRes(view, (int) R.attr.ic_audit_stage_instance, 0);
            case 12:
                return DrawableCompat.getThemedDrawableRes(view, (int) R.attr.ic_audit_calendar, 0);
            case 13:
                return DrawableCompat.getThemedDrawableRes(view, (int) R.attr.ic_audit_sticker, 0);
            case 14:
                return DrawableCompat.getThemedDrawableRes(view, (int) R.attr.ic_audit_thread, 0);
            default:
                return 0;
        }
    }

    public final long getTimestampStart(ModelAuditLogEntry modelAuditLogEntry) {
        m.checkNotNullParameter(modelAuditLogEntry, "$this$getTimestampStart");
        return (modelAuditLogEntry.getId() >>> 22) + SnowflakeUtils.DISCORD_EPOCH;
    }

    public final CharSequence getTimestampString(ModelAuditLogEntry modelAuditLogEntry, Context context) {
        m.checkNotNullParameter(modelAuditLogEntry, "auditLogEntry");
        m.checkNotNullParameter(context, "context");
        if (modelAuditLogEntry.getTimestampEnd() == null) {
            return getTimestampText(getTimestampStart(modelAuditLogEntry), context);
        }
        CharSequence timestampText = getTimestampText(getTimestampStart(modelAuditLogEntry), context);
        Long timestampEnd = modelAuditLogEntry.getTimestampEnd();
        if (timestampEnd == null) {
            timestampEnd = 0L;
        }
        m.checkNotNullExpressionValue(timestampEnd, "auditLogEntry.timestampEnd ?: 0L");
        CharSequence timestampText2 = getTimestampText(timestampEnd.longValue(), context);
        return timestampText + " - " + timestampText2;
    }
}
