package com.discord.utilities.auditlogs;

import android.content.Context;
import b.a.k.b;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: AuditLogChangeUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\r\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "it", "", "invoke", "(I)Ljava/lang/CharSequence;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AuditLogChangeUtils$renderPermissionList$3 extends o implements Function1<Integer, CharSequence> {
    public final /* synthetic */ Context $context;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AuditLogChangeUtils$renderPermissionList$3(Context context) {
        super(1);
        this.$context = context;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ CharSequence invoke(Integer num) {
        return invoke(num.intValue());
    }

    public final CharSequence invoke(int i) {
        CharSequence b2;
        b2 = b.b(this.$context, i, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        return b2;
    }
}
