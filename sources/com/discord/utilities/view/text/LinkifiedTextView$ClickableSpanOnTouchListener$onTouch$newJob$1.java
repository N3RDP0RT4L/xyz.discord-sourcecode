package com.discord.utilities.view.text;

import android.view.View;
import b.i.a.f.e.o.f;
import com.discord.utilities.view.text.LinkifiedTextView;
import d0.l;
import d0.w.h.c;
import d0.w.i.a.e;
import d0.w.i.a.k;
import d0.z.d.m;
import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicBoolean;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlinx.coroutines.CoroutineDispatcher;
import kotlinx.coroutines.CoroutineScope;
import org.objectweb.asm.Opcodes;
import s.a.a.n;
import s.a.k0;
/* compiled from: LinkifiedTextView.kt */
@e(c = "com.discord.utilities.view.text.LinkifiedTextView$ClickableSpanOnTouchListener$onTouch$newJob$1", f = "LinkifiedTextView.kt", l = {Opcodes.D2I}, m = "invokeSuspend")
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\u008a@¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lkotlinx/coroutines/CoroutineScope;", "", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class LinkifiedTextView$ClickableSpanOnTouchListener$onTouch$newJob$1 extends k implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
    public final /* synthetic */ Ref$ObjectRef $clickableSpan;
    public final /* synthetic */ WeakReference $weakView;
    private /* synthetic */ Object L$0;
    public int label;
    public final /* synthetic */ LinkifiedTextView.ClickableSpanOnTouchListener this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public LinkifiedTextView$ClickableSpanOnTouchListener$onTouch$newJob$1(LinkifiedTextView.ClickableSpanOnTouchListener clickableSpanOnTouchListener, WeakReference weakReference, Ref$ObjectRef ref$ObjectRef, Continuation continuation) {
        super(2, continuation);
        this.this$0 = clickableSpanOnTouchListener;
        this.$weakView = weakReference;
        this.$clickableSpan = ref$ObjectRef;
    }

    @Override // d0.w.i.a.a
    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        m.checkNotNullParameter(continuation, "completion");
        LinkifiedTextView$ClickableSpanOnTouchListener$onTouch$newJob$1 linkifiedTextView$ClickableSpanOnTouchListener$onTouch$newJob$1 = new LinkifiedTextView$ClickableSpanOnTouchListener$onTouch$newJob$1(this.this$0, this.$weakView, this.$clickableSpan, continuation);
        linkifiedTextView$ClickableSpanOnTouchListener$onTouch$newJob$1.L$0 = obj;
        return linkifiedTextView$ClickableSpanOnTouchListener$onTouch$newJob$1;
    }

    @Override // kotlin.jvm.functions.Function2
    public final Object invoke(CoroutineScope coroutineScope, Continuation<? super Unit> continuation) {
        return ((LinkifiedTextView$ClickableSpanOnTouchListener$onTouch$newJob$1) create(coroutineScope, continuation)).invokeSuspend(Unit.a);
    }

    @Override // d0.w.i.a.a
    public final Object invokeSuspend(Object obj) {
        CoroutineScope coroutineScope;
        AtomicBoolean atomicBoolean;
        View view;
        long j;
        Object coroutine_suspended = c.getCOROUTINE_SUSPENDED();
        int i = this.label;
        if (i == 0) {
            l.throwOnFailure(obj);
            CoroutineScope coroutineScope2 = (CoroutineScope) this.L$0;
            j = this.this$0.longPressDelayInMs;
            this.L$0 = coroutineScope2;
            this.label = 1;
            if (f.P(j, this) == coroutine_suspended) {
                return coroutine_suspended;
            }
            coroutineScope = coroutineScope2;
        } else if (i == 1) {
            coroutineScope = (CoroutineScope) this.L$0;
            l.throwOnFailure(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        atomicBoolean = this.this$0.isClickHandled;
        if (!atomicBoolean.getAndSet(true) && f.y0(coroutineScope) && (view = (View) this.$weakView.get()) != null) {
            CoroutineDispatcher coroutineDispatcher = k0.a;
            f.H0(coroutineScope, n.f3802b, null, new LinkifiedTextView$ClickableSpanOnTouchListener$onTouch$newJob$1$invokeSuspend$$inlined$also$lambda$1(view, null, this, coroutineScope), 2, null);
        }
        return Unit.a;
    }
}
