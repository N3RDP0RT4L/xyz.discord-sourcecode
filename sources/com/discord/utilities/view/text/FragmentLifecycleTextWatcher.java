package com.discord.utilities.view.text;

import andhook.lib.HookHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.OnLifecycleEvent;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function4;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: TextWatcher.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u00012\u00020\u0002B\u0095\u0001\u0012\u0006\u0010\u001f\u001a\u00020\u001e\u0012\u0006\u0010\u001b\u001a\u00020\u001a\u0012.\b\u0002\u0010\f\u001a(\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0013j\u0004\u0018\u0001`\u001d\u0012.\b\u0002\u0010\u0015\u001a(\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0013j\u0004\u0018\u0001`\u0014\u0012\u001c\b\u0002\u0010\u0011\u001a\u0016\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0017j\u0004\u0018\u0001`\u0018¢\u0006\u0004\b \u0010!J\u000f\u0010\u0004\u001a\u00020\u0003H\u0007¢\u0006\u0004\b\u0004\u0010\u0005J/\u0010\f\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\bH\u0016¢\u0006\u0004\b\f\u0010\rJ/\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000e\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\bH\u0016¢\u0006\u0004\b\u000f\u0010\rJ\u0017\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0010H\u0016¢\u0006\u0004\b\u0011\u0010\u0012R<\u0010\u0015\u001a(\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0013j\u0004\u0018\u0001`\u00148\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016R*\u0010\u0011\u001a\u0016\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0017j\u0004\u0018\u0001`\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0019R\u0016\u0010\u001b\u001a\u00020\u001a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR<\u0010\f\u001a(\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0013j\u0004\u0018\u0001`\u001d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\u0016¨\u0006\""}, d2 = {"Lcom/discord/utilities/view/text/FragmentLifecycleTextWatcher;", "Landroid/text/TextWatcher;", "Landroidx/lifecycle/LifecycleObserver;", "", "onDestroyView", "()V", "", "s", "", "start", "count", "after", "beforeTextChanged", "(Ljava/lang/CharSequence;III)V", "before", "onTextChanged", "Landroid/text/Editable;", "afterTextChanged", "(Landroid/text/Editable;)V", "Lkotlin/Function4;", "Lcom/discord/utilities/view/text/OnTextChanged;", "textChanged", "Lkotlin/jvm/functions/Function4;", "Lkotlin/Function1;", "Lcom/discord/utilities/view/text/OnAfterTextChanged;", "Lkotlin/jvm/functions/Function1;", "Landroid/widget/TextView;", "textView", "Landroid/widget/TextView;", "Lcom/discord/utilities/view/text/OnBeforeTextChanged;", "Landroidx/fragment/app/Fragment;", "fragment", HookHelper.constructorName, "(Landroidx/fragment/app/Fragment;Landroid/widget/TextView;Lkotlin/jvm/functions/Function4;Lkotlin/jvm/functions/Function4;Lkotlin/jvm/functions/Function1;)V", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class FragmentLifecycleTextWatcher implements TextWatcher, LifecycleObserver {
    private final Function1<Editable, Unit> afterTextChanged;
    private final Function4<CharSequence, Integer, Integer, Integer, Unit> beforeTextChanged;
    private final Function4<CharSequence, Integer, Integer, Integer, Unit> textChanged;
    private final TextView textView;

    /* JADX WARN: Multi-variable type inference failed */
    public FragmentLifecycleTextWatcher(Fragment fragment, TextView textView, Function4<? super CharSequence, ? super Integer, ? super Integer, ? super Integer, Unit> function4, Function4<? super CharSequence, ? super Integer, ? super Integer, ? super Integer, Unit> function42, Function1<? super Editable, Unit> function1) {
        m.checkNotNullParameter(fragment, "fragment");
        m.checkNotNullParameter(textView, "textView");
        this.textView = textView;
        this.beforeTextChanged = function4;
        this.textChanged = function42;
        this.afterTextChanged = function1;
        LifecycleOwner viewLifecycleOwner = fragment.getViewLifecycleOwner();
        m.checkNotNullExpressionValue(viewLifecycleOwner, "fragment.viewLifecycleOwner");
        viewLifecycleOwner.getLifecycle().addObserver(this);
    }

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        m.checkNotNullParameter(editable, "s");
        Function1<Editable, Unit> function1 = this.afterTextChanged;
        if (function1 != null) {
            function1.invoke(editable);
        }
    }

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        m.checkNotNullParameter(charSequence, "s");
        Function4<CharSequence, Integer, Integer, Integer, Unit> function4 = this.beforeTextChanged;
        if (function4 != null) {
            function4.invoke(charSequence, Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3));
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public final void onDestroyView() {
        this.textView.removeTextChangedListener(this);
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        m.checkNotNullParameter(charSequence, "s");
        Function4<CharSequence, Integer, Integer, Integer, Unit> function4 = this.textChanged;
        if (function4 != null) {
            function4.invoke(charSequence, Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3));
        }
    }

    public /* synthetic */ FragmentLifecycleTextWatcher(Fragment fragment, TextView textView, Function4 function4, Function4 function42, Function1 function1, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(fragment, textView, (i & 4) != 0 ? null : function4, (i & 8) != 0 ? null : function42, (i & 16) != 0 ? null : function1);
    }
}
