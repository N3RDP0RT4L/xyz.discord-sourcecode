package com.discord.utilities.view.grid;

import android.view.View;
import com.discord.utilities.view.grid.FrameGridLayout;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: FrameGridLayout.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Landroid/view/View;", "view", "Lcom/discord/utilities/view/grid/FrameGridLayout$PositionSpec;", "childBounds", "", "invoke", "(Landroid/view/View;Lcom/discord/utilities/view/grid/FrameGridLayout$PositionSpec;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class FrameGridLayout$onMeasure$1 extends o implements Function2<View, FrameGridLayout.PositionSpec, Unit> {
    public final /* synthetic */ FrameGridLayout this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public FrameGridLayout$onMeasure$1(FrameGridLayout frameGridLayout) {
        super(2);
        this.this$0 = frameGridLayout;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(View view, FrameGridLayout.PositionSpec positionSpec) {
        invoke2(view, positionSpec);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(View view, FrameGridLayout.PositionSpec positionSpec) {
        m.checkNotNullParameter(view, "view");
        m.checkNotNullParameter(positionSpec, "childBounds");
        this.this$0.measure(view, positionSpec);
    }
}
