package com.discord.utilities.view.grid;

import android.view.View;
import com.discord.utilities.view.grid.FrameGridLayout;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: FrameGridLayout.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u0003*\u00020\u00002\b\u0010\u0002\u001a\u0004\u0018\u00010\u0001H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/utilities/view/grid/FrameGridLayout$Data;", "Landroid/view/View;", "view", "", "invoke", "(Lcom/discord/utilities/view/grid/FrameGridLayout$Data;Landroid/view/View;)V", "bindView"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class FrameGridLayout$bindViews$1 extends o implements Function2<FrameGridLayout.Data, View, Unit> {
    public final /* synthetic */ FrameGridLayout this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public FrameGridLayout$bindViews$1(FrameGridLayout frameGridLayout) {
        super(2);
        this.this$0 = frameGridLayout;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(FrameGridLayout.Data data, View view) {
        invoke2(data, view);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(FrameGridLayout.Data data, View view) {
        Function2 function2;
        m.checkNotNullParameter(data, "$this$bindView");
        if (!(view instanceof FrameGridLayout.DataView)) {
            view = null;
        }
        FrameGridLayout.DataView dataView = (FrameGridLayout.DataView) view;
        if (dataView != null) {
            dataView.onBind(data);
            function2 = this.this$0.onBindView;
            if (function2 != null) {
                Unit unit = (Unit) function2.invoke((View) dataView, data);
            }
        }
    }
}
