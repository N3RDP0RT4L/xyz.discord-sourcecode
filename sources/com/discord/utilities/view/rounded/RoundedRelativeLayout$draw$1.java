package com.discord.utilities.view.rounded;

import android.graphics.Canvas;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: RoundedRelativeLayout.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/graphics/Canvas;", "it", "", "invoke", "(Landroid/graphics/Canvas;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class RoundedRelativeLayout$draw$1 extends o implements Function1<Canvas, Unit> {
    public final /* synthetic */ RoundedRelativeLayout this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public RoundedRelativeLayout$draw$1(RoundedRelativeLayout roundedRelativeLayout) {
        super(1);
        this.this$0 = roundedRelativeLayout;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Canvas canvas) {
        invoke2(canvas);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Canvas canvas) {
        m.checkNotNullParameter(canvas, "it");
        RoundedRelativeLayout$draw$1.super.draw(canvas);
    }
}
