package com.discord.utilities.view.validators;

import andhook.lib.HookHelper;
import androidx.annotation.StringRes;
import b.a.k.b;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: BasicTextInputValidator.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u000b\n\u0002\b\u0006\u0018\u0000 \u00132\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0013B%\u0012\b\b\u0001\u0010\b\u001a\u00020\u0007\u0012\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\f¢\u0006\u0004\b\u0011\u0010\u0012J\u0019\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006R\u0019\u0010\b\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR\"\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010¨\u0006\u0014"}, d2 = {"Lcom/discord/utilities/view/validators/BasicTextInputValidator;", "Lcom/discord/utilities/view/validators/InputValidator;", "Lcom/google/android/material/textfield/TextInputLayout;", "view", "", "getErrorMessage", "(Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/CharSequence;", "", "messageResId", "I", "getMessageResId", "()I", "Lkotlin/Function1;", "", "", "inputPredicate", "Lkotlin/jvm/functions/Function1;", HookHelper.constructorName, "(ILkotlin/jvm/functions/Function1;)V", "Companion", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class BasicTextInputValidator implements InputValidator<TextInputLayout> {
    public static final Companion Companion = new Companion(null);
    private final Function1<String, Boolean> inputPredicate;
    private final int messageResId;

    /* compiled from: BasicTextInputValidator.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\b\b\u0001\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/utilities/view/validators/BasicTextInputValidator$Companion;", "", "", "messageResId", "Lcom/discord/utilities/view/validators/BasicTextInputValidator;", "createRequiredInputValidator", "(I)Lcom/discord/utilities/view/validators/BasicTextInputValidator;", HookHelper.constructorName, "()V", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final BasicTextInputValidator createRequiredInputValidator(@StringRes int i) {
            return new BasicTextInputValidator(i, BasicTextInputValidator$Companion$createRequiredInputValidator$1.INSTANCE);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public BasicTextInputValidator(@StringRes int i, Function1<? super String, Boolean> function1) {
        m.checkNotNullParameter(function1, "inputPredicate");
        this.messageResId = i;
        this.inputPredicate = function1;
    }

    public final int getMessageResId() {
        return this.messageResId;
    }

    public CharSequence getErrorMessage(TextInputLayout textInputLayout) {
        CharSequence d;
        m.checkNotNullParameter(textInputLayout, "view");
        if (this.inputPredicate.invoke(ViewExtensions.getTextOrEmpty(textInputLayout)).booleanValue()) {
            return null;
        }
        d = b.d(textInputLayout, this.messageResId, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
        return d;
    }
}
