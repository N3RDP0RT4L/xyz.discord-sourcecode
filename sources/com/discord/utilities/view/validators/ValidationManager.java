package com.discord.utilities.view.validators;

import andhook.lib.HookHelper;
import android.view.View;
import d0.t.k;
import d0.t.o0;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
/* compiled from: ValidationManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0010\u001e\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B+\u0012\"\u0010\u0011\u001a\u0012\u0012\u000e\b\u0001\u0012\n\u0012\u0006\b\u0001\u0012\u00020\u00100\u000f0\u000e\"\n\u0012\u0006\b\u0001\u0012\u00020\u00100\u000f¢\u0006\u0004\b\u0013\u0010\u0014J\u0017\u0010\u0004\u001a\u00020\u00022\b\b\u0002\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0004\u0010\u0005J5\u0010\f\u001a\f\u0012\b\u0012\u00060\u0007j\u0002`\b0\u000b2\u001c\u0010\n\u001a\u0018\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\t0\u0006¢\u0006\u0004\b\f\u0010\rR&\u0010\u0011\u001a\u0012\u0012\u000e\b\u0001\u0012\n\u0012\u0006\b\u0001\u0012\u00020\u00100\u000f0\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012¨\u0006\u0015"}, d2 = {"Lcom/discord/utilities/view/validators/ValidationManager;", "", "", "showErrors", "validate", "(Z)Z", "", "", "Lcom/discord/utilities/view/validators/FieldName;", "", "errorMap", "", "setErrors", "(Ljava/util/Map;)Ljava/util/Collection;", "", "Lcom/discord/utilities/view/validators/Input;", "Landroid/view/View;", "inputs", "[Lcom/discord/utilities/view/validators/Input;", HookHelper.constructorName, "([Lcom/discord/utilities/view/validators/Input;)V", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ValidationManager {
    private final Input<? extends View>[] inputs;

    public ValidationManager(Input<? extends View>... inputArr) {
        m.checkNotNullParameter(inputArr, "inputs");
        this.inputs = inputArr;
    }

    public static /* synthetic */ boolean validate$default(ValidationManager validationManager, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = true;
        }
        return validationManager.validate(z2);
    }

    public final Collection<String> setErrors(Map<String, ? extends List<String>> map) {
        m.checkNotNullParameter(map, "errorMap");
        ArrayList arrayList = new ArrayList();
        for (Input input : k.reversed(this.inputs)) {
            List<String> list = map.get(input.getName());
            if (input.setErrorMessage(list != null ? (String) u.firstOrNull((List<? extends Object>) list) : null)) {
                arrayList.add(input.getName());
            }
        }
        return o0.minus((Set) map.keySet(), (Iterable) arrayList);
    }

    public final boolean validate(boolean z2) {
        boolean z3 = true;
        for (Input<? extends View> input : this.inputs) {
            z3 = input.validate(z2) && z3;
        }
        return z3;
    }
}
