package com.discord.utilities.view.validators;

import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: BasicTextInputValidator.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "it", "", "invoke", "(Ljava/lang/String;)Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class BasicTextInputValidator$Companion$createRequiredInputValidator$1 extends o implements Function1<String, Boolean> {
    public static final BasicTextInputValidator$Companion$createRequiredInputValidator$1 INSTANCE = new BasicTextInputValidator$Companion$createRequiredInputValidator$1();

    public BasicTextInputValidator$Companion$createRequiredInputValidator$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Boolean invoke(String str) {
        return Boolean.valueOf(invoke2(str));
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final boolean invoke2(String str) {
        m.checkNotNullParameter(str, "it");
        return str.length() > 0;
    }
}
