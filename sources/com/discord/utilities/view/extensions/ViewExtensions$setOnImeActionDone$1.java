package com.discord.utilities.view.extensions;

import android.view.KeyEvent;
import android.widget.TextView;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function3;
/* compiled from: ViewExtensions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\t\u001a\u00020\u00062\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"Landroid/widget/TextView;", "textView", "", "actionId", "Landroid/view/KeyEvent;", "<anonymous parameter 2>", "", "invoke", "(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ViewExtensions$setOnImeActionDone$1 extends o implements Function3<TextView, Integer, KeyEvent, Boolean> {
    public final /* synthetic */ boolean $isEventConsumed;
    public final /* synthetic */ Function1 $onImeActionDone;
    public final /* synthetic */ TextInputLayout $this_setOnImeActionDone;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ViewExtensions$setOnImeActionDone$1(TextInputLayout textInputLayout, Function1 function1, boolean z2) {
        super(3);
        this.$this_setOnImeActionDone = textInputLayout;
        this.$onImeActionDone = function1;
        this.$isEventConsumed = z2;
    }

    @Override // kotlin.jvm.functions.Function3
    public /* bridge */ /* synthetic */ Boolean invoke(TextView textView, Integer num, KeyEvent keyEvent) {
        return Boolean.valueOf(invoke(textView, num.intValue(), keyEvent));
    }

    public final boolean invoke(TextView textView, int i, KeyEvent keyEvent) {
        m.checkNotNullParameter(textView, "textView");
        if (i == 6) {
            if (ViewExtensions.getTextOrEmpty(this.$this_setOnImeActionDone).length() > 0) {
                this.$onImeActionDone.invoke(textView);
            }
        }
        return this.$isEventConsumed;
    }
}
