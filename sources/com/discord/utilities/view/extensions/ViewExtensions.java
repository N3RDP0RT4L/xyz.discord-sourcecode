package com.discord.utilities.view.extensions;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.DrawableRes;
import androidx.annotation.MainThread;
import androidx.annotation.PluralsRes;
import androidx.annotation.StringRes;
import androidx.annotation.StyleableRes;
import androidx.core.app.NotificationCompat;
import androidx.core.view.ViewCompat;
import androidx.core.widget.NestedScrollView;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import b.a.k.b;
import com.discord.i18n.RenderContext;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.view.extensions.FadeAnimation;
import com.discord.utilities.view.text.TextWatcherKt;
import com.google.android.material.textfield.TextInputLayout;
import d0.g0.t;
import d0.z.d.c0;
import d0.z.d.m;
import java.util.Arrays;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.functions.Function4;
/* compiled from: ViewExtensions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000ê\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u0007\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0015\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u001a\"\u0010\u0003\u001a\u0004\u0018\u00018\u0000\"\n\b\u0000\u0010\u0001\u0018\u0001*\u00020\u0000*\u00020\u0002H\u0086\b¢\u0006\u0004\b\u0003\u0010\u0004\u001a%\u0010\b\u001a\u00020\u0006*\u00020\u00022\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0004\b\b\u0010\t\u001a\u001b\u0010\f\u001a\u00020\u0006*\u00020\u00022\b\b\u0002\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\f\u0010\r\u001a[\u0010\u0014\u001a\u00020\u0006*\u0004\u0018\u00010\u00022\b\b\u0002\u0010\u000e\u001a\u00020\n2\u0014\b\u0002\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00060\u00052\u0014\b\u0002\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00060\u00052\u000e\b\u0002\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00060\u0012H\u0007¢\u0006\u0004\b\u0014\u0010\u0015\u001aE\u0010\u0016\u001a\u00020\u0006*\u0004\u0018\u00010\u00022\b\b\u0002\u0010\u000e\u001a\u00020\n2\u0014\b\u0002\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00060\u00052\u000e\b\u0002\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00060\u0012H\u0007¢\u0006\u0004\b\u0016\u0010\u0017\u001a\u0011\u0010\u0018\u001a\u00020\u0006*\u00020\u0002¢\u0006\u0004\b\u0018\u0010\u0019\u001a+\u0010\u001d\u001a\u00020\u0006*\u00020\u00022\u0018\u0010\u001c\u001a\u0014\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\u00060\u001a¢\u0006\u0004\b\u001d\u0010\u001e\u001a+\u0010 \u001a\u00020\u0006*\u00020\u00022\u0018\u0010\u001f\u001a\u0014\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\u00060\u001a¢\u0006\u0004\b \u0010\u001e\u001a7\u0010#\u001a\u00020\u0006*\u00020\u00022$\u0010\"\u001a \u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\u00060!¢\u0006\u0004\b#\u0010$\u001a\u0011\u0010'\u001a\u00020&*\u00020%¢\u0006\u0004\b'\u0010(\u001a\u0013\u0010)\u001a\u0004\u0018\u00010\u0006*\u00020%¢\u0006\u0004\b)\u0010*\u001a\u0019\u0010,\u001a\u00020\u0006*\u00020%2\u0006\u0010+\u001a\u00020\u001b¢\u0006\u0004\b,\u0010-\u001a\u0019\u0010.\u001a\u00020\u0006*\u00020%2\u0006\u0010+\u001a\u00020\u001b¢\u0006\u0004\b.\u0010-\u001a\u0019\u0010.\u001a\u00020\u0006*\u00020%2\u0006\u00100\u001a\u00020/¢\u0006\u0004\b.\u00101\u001aM\u00109\u001a\u00020\u0006*\u00020%2\b\b\u0001\u00102\u001a\u00020\u001b2\u0016\u00105\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010403\"\u0004\u0018\u0001042\u0018\b\u0002\u00108\u001a\u0012\u0012\u0004\u0012\u000206\u0012\u0004\u0012\u00020\u00060\u0005j\u0002`7¢\u0006\u0004\b9\u0010:\u001a\u001b\u0010;\u001a\u0004\u0018\u00010\u0006*\u00020%2\u0006\u0010+\u001a\u00020\u001b¢\u0006\u0004\b;\u0010<\u001a\u001d\u0010;\u001a\u0004\u0018\u00010\u0006*\u00020%2\b\u0010=\u001a\u0004\u0018\u00010/¢\u0006\u0004\b;\u0010>\u001a\u001b\u0010?\u001a\u00020\u0006*\u00020%2\b\u0010=\u001a\u0004\u0018\u00010/¢\u0006\u0004\b?\u00101\u001a\u001b\u0010A\u001a\u0004\u0018\u00010\u0006*\u00020%2\u0006\u0010@\u001a\u00020\u001b¢\u0006\u0004\bA\u0010<\u001a\u0013\u0010B\u001a\u0004\u0018\u00010\u0006*\u00020%¢\u0006\u0004\bB\u0010*\u001a/\u0010G\u001a\u0004\u0018\u00010\u0006*\u00020%2\u0006\u0010D\u001a\u00020C2\u0012\u0010F\u001a\u000e\u0012\u0004\u0012\u00020E\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0004\bG\u0010H\u001a5\u0010N\u001a\u0004\u0018\u00010\u0006*\u00020%2 \u0010M\u001a\u001c\u0012\u0004\u0012\u00020J\u0012\u0004\u0012\u00020\u001b\u0012\u0006\u0012\u0004\u0018\u00010K\u0012\u0004\u0012\u00020L0I¢\u0006\u0004\bN\u0010O\u001a1\u0010R\u001a\u0004\u0018\u00010\u0006*\u00020%2\b\b\u0002\u0010P\u001a\u00020L2\u0012\u0010Q\u001a\u000e\u0012\u0004\u0012\u00020J\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0004\bR\u0010S\u001a'\u0010T\u001a\u0004\u0018\u00010\u0006*\u00020%2\u0012\u0010M\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0004\bT\u0010U\u001a\u0019\u0010W\u001a\u00020\u0006*\u00020%2\u0006\u0010M\u001a\u00020V¢\u0006\u0004\bW\u0010X\u001a\u0011\u0010Y\u001a\u00020\u0006*\u00020%¢\u0006\u0004\bY\u0010Z\u001a\u0011\u0010[\u001a\u00020\u0006*\u00020%¢\u0006\u0004\b[\u0010Z\u001a'\u0010]\u001a\u00020\u0006*\u0004\u0018\u00010\u00022\u0006\u0010\\\u001a\u00020L2\b\b\u0002\u0010\u000e\u001a\u00020\nH\u0007¢\u0006\u0004\b]\u0010^\u001a\u001b\u0010a\u001a\u00020\u0006*\u00020\u00022\b\u0010`\u001a\u0004\u0018\u00010_¢\u0006\u0004\ba\u0010b\u001a\u001b\u0010c\u001a\u00020\u0006*\u00020J2\b\u0010=\u001a\u0004\u0018\u00010/¢\u0006\u0004\bc\u0010d\u001a7\u0010g\u001a\u00020\u0006*\u00020J2\b\b\u0001\u0010e\u001a\u00020\u001b2\u0006\u0010f\u001a\u00020\u001b2\u0012\u00105\u001a\n\u0012\u0006\b\u0001\u0012\u00020403\"\u000204¢\u0006\u0004\bg\u0010h\u001a#\u0010l\u001a\u00020\u0006*\u00020\u00022\u0006\u0010i\u001a\u00020L2\b\b\u0002\u0010k\u001a\u00020j¢\u0006\u0004\bl\u0010m\u001a#\u0010n\u001a\u00020\u0006*\u00020\u00022\u0006\u0010i\u001a\u00020L2\b\b\u0002\u0010k\u001a\u00020j¢\u0006\u0004\bn\u0010m\u001a9\u0010s\u001a\u00020\u0006*\u00020J2\b\b\u0003\u0010o\u001a\u00020\u001b2\b\b\u0003\u0010p\u001a\u00020\u001b2\b\b\u0003\u0010q\u001a\u00020\u001b2\b\b\u0003\u0010r\u001a\u00020\u001b¢\u0006\u0004\bs\u0010t\u001a\u0011\u0010v\u001a\u00020\u0006*\u00020u¢\u0006\u0004\bv\u0010w\u001a\u0011\u0010y\u001a\u00020\u0002*\u00020x¢\u0006\u0004\by\u0010z\u001a\u0011\u0010{\u001a\u00020\u0006*\u00020\u0002¢\u0006\u0004\b{\u0010\u0019\u001a\u0011\u0010|\u001a\u00020\u0006*\u00020\u0002¢\u0006\u0004\b|\u0010\u0019\u001aE\u0010\u0083\u0001\u001a\u00020\u0006*\u00020\u00022\b\u0010~\u001a\u0004\u0018\u00010}2\t\b\u0001\u0010\u0080\u0001\u001a\u00020\u007f2\u0014\u0010\u0082\u0001\u001a\u000f\u0012\u0005\u0012\u00030\u0081\u0001\u0012\u0004\u0012\u00020\u00060\u0005H\u0086\bø\u0001\u0000¢\u0006\u0006\b\u0083\u0001\u0010\u0084\u0001\u001a(\u0010\u0089\u0001\u001a\u00020\u0006*\u00030\u0085\u00012\u0007\u0010\u0086\u0001\u001a\u00020L2\b\u0010\u0088\u0001\u001a\u00030\u0087\u0001¢\u0006\u0006\b\u0089\u0001\u0010\u008a\u0001\"\u0019\u0010\u008b\u0001\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\b\n\u0006\b\u008b\u0001\u0010\u008c\u0001\"\u0019\u0010\u008d\u0001\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\b\n\u0006\b\u008d\u0001\u0010\u008c\u0001\"9\u0010\u0091\u0001\u001a\"\u0012\u0004\u0012\u00020\u0002\u0012\u0005\u0012\u00030\u008f\u00010\u008e\u0001j\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0005\u0012\u00030\u008f\u0001`\u0090\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0091\u0001\u0010\u0092\u0001*%\b\u0002\u0010\u0093\u0001\"\u000e\u0012\u0004\u0012\u000206\u0012\u0004\u0012\u00020\u00060\u00052\u000e\u0012\u0004\u0012\u000206\u0012\u0004\u0012\u00020\u00060\u0005\u0082\u0002\u0007\n\u0005\b\u009920\u0001¨\u0006\u0094\u0001"}, d2 = {"Landroid/view/ViewParent;", ExifInterface.GPS_DIRECTION_TRUE, "Landroid/view/View;", "findParent", "(Landroid/view/View;)Landroid/view/ViewParent;", "Lkotlin/Function1;", "", "onLongClick", "setOnLongClickListenerConsumeClick", "(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V", "", "delayMillis", "hintWithRipple", "(Landroid/view/View;J)V", "fadeMillis", "setup", "Landroid/view/ViewPropertyAnimator;", "additionalAnimation", "Lkotlin/Function0;", "onAnimationEnd", "fadeIn", "(Landroid/view/View;JLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V", "fadeOut", "(Landroid/view/View;JLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V", "cancelFadeAnimations", "(Landroid/view/View;)V", "Lkotlin/Function2;", "", "onHeightChanged", "addOnHeightChangedListener", "(Landroid/view/View;Lkotlin/jvm/functions/Function2;)V", "onWidthChanged", "addOnWidthChangedListener", "Lkotlin/Function4;", "onSizeChanged", "addOnSizeChangedListener", "(Landroid/view/View;Lkotlin/jvm/functions/Function4;)V", "Lcom/google/android/material/textfield/TextInputLayout;", "", "getTextOrEmpty", "(Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;", "clear", "(Lcom/google/android/material/textfield/TextInputLayout;)Lkotlin/Unit;", "resId", "setHint", "(Lcom/google/android/material/textfield/TextInputLayout;I)V", "setSingleLineHint", "", "charSequence", "(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)V", "stringResId", "", "", "formatArgs", "Lcom/discord/i18n/RenderContext;", "Lcom/discord/utilities/view/extensions/Initializer;", "initializer", "i18nSetText", "(Lcom/google/android/material/textfield/TextInputLayout;I[Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)V", "setText", "(Lcom/google/android/material/textfield/TextInputLayout;I)Lkotlin/Unit;", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)Lkotlin/Unit;", "setTextIfDifferent", "index", "setSelection", "setSelectionEnd", "Landroidx/fragment/app/Fragment;", "fragment", "Landroid/text/Editable;", "onAfterTextChanged", "addBindedTextWatcher", "(Lcom/google/android/material/textfield/TextInputLayout;Landroidx/fragment/app/Fragment;Lkotlin/jvm/functions/Function1;)Lkotlin/Unit;", "Lkotlin/Function3;", "Landroid/widget/TextView;", "Landroid/view/KeyEvent;", "", "l", "setOnEditorActionListener", "(Lcom/google/android/material/textfield/TextInputLayout;Lkotlin/jvm/functions/Function3;)Lkotlin/Unit;", "isEventConsumed", "onImeActionDone", "setOnImeActionDone", "(Lcom/google/android/material/textfield/TextInputLayout;ZLkotlin/jvm/functions/Function1;)Lkotlin/Unit;", "setOnEditTextClickListener", "(Lcom/google/android/material/textfield/TextInputLayout;Lkotlin/jvm/functions/Function1;)Lkotlin/Unit;", "Landroid/view/View$OnFocusChangeListener;", "setOnEditTextFocusChangeListener", "(Lcom/google/android/material/textfield/TextInputLayout;Landroid/view/View$OnFocusChangeListener;)V", "interceptScrollWhenInsideScrollable", "(Lcom/google/android/material/textfield/TextInputLayout;)V", "moveCursorToEnd", "isVisible", "fadeBy", "(Landroid/view/View;ZJ)V", "Landroid/graphics/drawable/Drawable;", "backgroundDrawable", "setBackgroundAndKeepPadding", "(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V", "setTextAndVisibilityBy", "(Landroid/widget/TextView;Ljava/lang/CharSequence;)V", "pluralResId", "quantity", "setPluralText", "(Landroid/widget/TextView;II[Ljava/lang/Object;)V", "enabled", "", "disabledAlpha", "setEnabledAndAlpha", "(Landroid/view/View;ZF)V", "setEnabledAlpha", "start", "top", "end", "bottom", "setCompoundDrawableWithIntrinsicBounds", "(Landroid/widget/TextView;IIII)V", "Landroid/view/ViewGroup;", "setForwardingWindowInsetsListener", "(Landroid/view/ViewGroup;)V", "Landroidx/core/widget/NestedScrollView;", "getContentView", "(Landroidx/core/widget/NestedScrollView;)Landroid/view/View;", "enable", "disable", "Landroid/util/AttributeSet;", "attrs", "", "styleable", "Landroid/content/res/TypedArray;", "block", "useAttrs", "(Landroid/view/View;Landroid/util/AttributeSet;[ILkotlin/jvm/functions/Function1;)V", "Landroid/widget/CompoundButton;", "isChecked", "Landroid/widget/CompoundButton$OnCheckedChangeListener;", "listener", "setProgrammaticChecked", "(Landroid/widget/CompoundButton;ZLandroid/widget/CompoundButton$OnCheckedChangeListener;)V", "DEFAULT_FADE_MILLIS_RES", "J", "HINT_DELAY_MILLIS", "Ljava/util/HashMap;", "Lcom/discord/utilities/view/extensions/FadeAnimation;", "Lkotlin/collections/HashMap;", "fadeAnimations", "Ljava/util/HashMap;", "Initializer", "utils_release"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ViewExtensions {
    private static final long DEFAULT_FADE_MILLIS_RES = 350;
    private static final long HINT_DELAY_MILLIS = 250;
    private static final HashMap<View, FadeAnimation> fadeAnimations = new HashMap<>();

    public static final Unit addBindedTextWatcher(TextInputLayout textInputLayout, Fragment fragment, Function1<? super Editable, Unit> function1) {
        m.checkNotNullParameter(textInputLayout, "$this$addBindedTextWatcher");
        m.checkNotNullParameter(fragment, "fragment");
        m.checkNotNullParameter(function1, "onAfterTextChanged");
        EditText editText = textInputLayout.getEditText();
        if (editText == null) {
            return null;
        }
        TextWatcherKt.addBindedTextWatcher(editText, fragment, function1);
        return Unit.a;
    }

    public static final void addOnHeightChangedListener(View view, final Function2<? super Integer, ? super Integer, Unit> function2) {
        m.checkNotNullParameter(view, "$this$addOnHeightChangedListener");
        m.checkNotNullParameter(function2, "onHeightChanged");
        view.addOnLayoutChangeListener(new View.OnLayoutChangeListener() { // from class: com.discord.utilities.view.extensions.ViewExtensions$addOnHeightChangedListener$1
            @Override // android.view.View.OnLayoutChangeListener
            public final void onLayoutChange(View view2, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                int i9 = i4 - i2;
                int i10 = i8 - i6;
                if (i9 != i10) {
                    Function2.this.invoke(Integer.valueOf(i9), Integer.valueOf(i10));
                }
            }
        });
    }

    public static final void addOnSizeChangedListener(View view, final Function4<? super Integer, ? super Integer, ? super Integer, ? super Integer, Unit> function4) {
        m.checkNotNullParameter(view, "$this$addOnSizeChangedListener");
        m.checkNotNullParameter(function4, "onSizeChanged");
        view.addOnLayoutChangeListener(new View.OnLayoutChangeListener() { // from class: com.discord.utilities.view.extensions.ViewExtensions$addOnSizeChangedListener$1
            @Override // android.view.View.OnLayoutChangeListener
            public final void onLayoutChange(View view2, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                int i9 = i3 - i;
                int i10 = i4 - i2;
                int i11 = i7 - i5;
                int i12 = i8 - i6;
                if (i9 != i11 || i10 != i12) {
                    Function4.this.invoke(Integer.valueOf(i9), Integer.valueOf(i11), Integer.valueOf(i10), Integer.valueOf(i12));
                }
            }
        });
    }

    public static final void addOnWidthChangedListener(View view, final Function2<? super Integer, ? super Integer, Unit> function2) {
        m.checkNotNullParameter(view, "$this$addOnWidthChangedListener");
        m.checkNotNullParameter(function2, "onWidthChanged");
        view.addOnLayoutChangeListener(new View.OnLayoutChangeListener() { // from class: com.discord.utilities.view.extensions.ViewExtensions$addOnWidthChangedListener$1
            @Override // android.view.View.OnLayoutChangeListener
            public final void onLayoutChange(View view2, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                int i9 = i3 - i;
                int i10 = i7 - i5;
                if (i9 != i10) {
                    Function2.this.invoke(Integer.valueOf(i9), Integer.valueOf(i10));
                }
            }
        });
    }

    public static final void cancelFadeAnimations(View view) {
        ViewPropertyAnimator viewPropertyAnimator;
        m.checkNotNullParameter(view, "$this$cancelFadeAnimations");
        FadeAnimation remove = fadeAnimations.remove(view);
        if (remove != null && (viewPropertyAnimator = remove.getViewPropertyAnimator()) != null) {
            viewPropertyAnimator.cancel();
        }
    }

    public static final Unit clear(TextInputLayout textInputLayout) {
        Editable text;
        m.checkNotNullParameter(textInputLayout, "$this$clear");
        EditText editText = textInputLayout.getEditText();
        if (editText == null || (text = editText.getText()) == null) {
            return null;
        }
        text.clear();
        return Unit.a;
    }

    public static final void disable(View view) {
        m.checkNotNullParameter(view, "$this$disable");
        view.setEnabled(false);
    }

    public static final void enable(View view) {
        m.checkNotNullParameter(view, "$this$enable");
        view.setEnabled(true);
    }

    public static final void fadeBy(View view, boolean z2) {
        fadeBy$default(view, z2, 0L, 2, null);
    }

    public static final void fadeBy(View view, boolean z2, long j) {
        if (z2) {
            fadeIn$default(view, j, null, null, null, 14, null);
        } else {
            fadeOut$default(view, j, null, null, 6, null);
        }
    }

    public static /* synthetic */ void fadeBy$default(View view, boolean z2, long j, int i, Object obj) {
        if ((i & 2) != 0) {
            j = DEFAULT_FADE_MILLIS_RES;
        }
        fadeBy(view, z2, j);
    }

    @MainThread
    public static final void fadeIn(View view) {
        fadeIn$default(view, 0L, null, null, null, 15, null);
    }

    @MainThread
    public static final void fadeIn(View view, long j) {
        fadeIn$default(view, j, null, null, null, 14, null);
    }

    @MainThread
    public static final void fadeIn(View view, long j, Function1<? super View, Unit> function1) {
        fadeIn$default(view, j, function1, null, null, 12, null);
    }

    @MainThread
    public static final void fadeIn(View view, long j, Function1<? super View, Unit> function1, Function1<? super ViewPropertyAnimator, Unit> function12) {
        fadeIn$default(view, j, function1, function12, null, 8, null);
    }

    @MainThread
    public static final void fadeIn(View view, long j, Function1<? super View, Unit> function1, Function1<? super ViewPropertyAnimator, Unit> function12, final Function0<Unit> function0) {
        m.checkNotNullParameter(function1, "setup");
        m.checkNotNullParameter(function12, "additionalAnimation");
        m.checkNotNullParameter(function0, "onAnimationEnd");
        if (view != null) {
            HashMap<View, FadeAnimation> hashMap = fadeAnimations;
            FadeAnimation fadeAnimation = hashMap.get(view);
            FadeAnimation.Type type = fadeAnimation != null ? fadeAnimation.getType() : null;
            FadeAnimation.Type type2 = FadeAnimation.Type.FADE_IN;
            if (type != type2) {
                if (fadeAnimation != null) {
                    hashMap.remove(view);
                    fadeAnimation.getViewPropertyAnimator().cancel();
                }
                if (view.getVisibility() != 0) {
                    view.setAlpha(0.0f);
                    function1.invoke(view);
                    view.setVisibility(0);
                    ViewPropertyAnimator alpha = view.animate().alpha(1.0f);
                    function12.invoke(alpha);
                    ViewPropertyAnimator listener = alpha.setDuration(j).setInterpolator(new AccelerateDecelerateInterpolator()).withEndAction(new Runnable() { // from class: com.discord.utilities.view.extensions.ViewExtensions$fadeIn$viewPropertyAnimator$1
                        @Override // java.lang.Runnable
                        public final void run() {
                            Function0.this.invoke();
                        }
                    }).setListener(new CleanupViewAnimationListener(view));
                    m.checkNotNullExpressionValue(listener, "viewPropertyAnimator");
                    hashMap.put(view, new FadeAnimation(listener, type2));
                    listener.start();
                    return;
                }
                view.setAlpha(1.0f);
            }
        }
    }

    public static /* synthetic */ void fadeIn$default(View view, long j, Function1 function1, Function1 function12, Function0 function0, int i, Object obj) {
        if ((i & 1) != 0) {
            j = DEFAULT_FADE_MILLIS_RES;
        }
        long j2 = j;
        if ((i & 2) != 0) {
            function1 = ViewExtensions$fadeIn$1.INSTANCE;
        }
        Function1 function13 = function1;
        if ((i & 4) != 0) {
            function12 = ViewExtensions$fadeIn$2.INSTANCE;
        }
        Function1 function14 = function12;
        if ((i & 8) != 0) {
            function0 = ViewExtensions$fadeIn$3.INSTANCE;
        }
        fadeIn(view, j2, function13, function14, function0);
    }

    @MainThread
    public static final void fadeOut(View view) {
        fadeOut$default(view, 0L, null, null, 7, null);
    }

    @MainThread
    public static final void fadeOut(View view, long j) {
        fadeOut$default(view, j, null, null, 6, null);
    }

    @MainThread
    public static final void fadeOut(View view, long j, Function1<? super ViewPropertyAnimator, Unit> function1) {
        fadeOut$default(view, j, function1, null, 4, null);
    }

    @MainThread
    public static final void fadeOut(final View view, long j, Function1<? super ViewPropertyAnimator, Unit> function1, final Function0<Unit> function0) {
        m.checkNotNullParameter(function1, "additionalAnimation");
        m.checkNotNullParameter(function0, "onAnimationEnd");
        if (view != null) {
            HashMap<View, FadeAnimation> hashMap = fadeAnimations;
            FadeAnimation fadeAnimation = hashMap.get(view);
            FadeAnimation.Type type = fadeAnimation != null ? fadeAnimation.getType() : null;
            FadeAnimation.Type type2 = FadeAnimation.Type.FADE_OUT;
            if (type != type2) {
                if (fadeAnimation != null) {
                    hashMap.remove(view);
                    fadeAnimation.getViewPropertyAnimator().cancel();
                }
                if (view.getVisibility() == 0) {
                    ViewPropertyAnimator alpha = view.animate().alpha(0.0f);
                    function1.invoke(alpha);
                    ViewPropertyAnimator listener = alpha.setDuration(j).setInterpolator(new AccelerateDecelerateInterpolator()).withEndAction(new Runnable() { // from class: com.discord.utilities.view.extensions.ViewExtensions$fadeOut$viewPropertyAnimator$1
                        @Override // java.lang.Runnable
                        public final void run() {
                            view.setVisibility(8);
                            function0.invoke();
                        }
                    }).setListener(new CleanupViewAnimationListener(view));
                    m.checkNotNullExpressionValue(listener, "viewPropertyAnimator");
                    hashMap.put(view, new FadeAnimation(listener, type2));
                    listener.start();
                }
            }
        }
    }

    public static /* synthetic */ void fadeOut$default(View view, long j, Function1 function1, Function0 function0, int i, Object obj) {
        if ((i & 1) != 0) {
            j = DEFAULT_FADE_MILLIS_RES;
        }
        if ((i & 2) != 0) {
            function1 = ViewExtensions$fadeOut$1.INSTANCE;
        }
        if ((i & 4) != 0) {
            function0 = ViewExtensions$fadeOut$2.INSTANCE;
        }
        fadeOut(view, j, function1, function0);
    }

    public static final /* synthetic */ <T extends ViewParent> T findParent(View view) {
        m.checkNotNullParameter(view, "$this$findParent");
        T t = (T) view.getParent();
        if (t == null) {
            return null;
        }
        m.reifiedOperationMarker(3, ExifInterface.GPS_DIRECTION_TRUE);
        return t;
    }

    public static final View getContentView(NestedScrollView nestedScrollView) {
        m.checkNotNullParameter(nestedScrollView, "$this$getContentView");
        View childAt = nestedScrollView.getChildAt(0);
        m.checkNotNullExpressionValue(childAt, "getChildAt(0)");
        return childAt;
    }

    public static final String getTextOrEmpty(TextInputLayout textInputLayout) {
        Editable text;
        m.checkNotNullParameter(textInputLayout, "$this$getTextOrEmpty");
        EditText editText = textInputLayout.getEditText();
        String obj = (editText == null || (text = editText.getText()) == null) ? null : text.toString();
        return obj != null ? obj : "";
    }

    public static final void hintWithRipple(final View view, long j) {
        m.checkNotNullParameter(view, "$this$hintWithRipple");
        view.setPressed(true);
        view.setPressed(false);
        view.postDelayed(new Runnable() { // from class: com.discord.utilities.view.extensions.ViewExtensions$hintWithRipple$1
            @Override // java.lang.Runnable
            public final void run() {
                view.setPressed(true);
                view.setPressed(false);
            }
        }, j);
    }

    public static /* synthetic */ void hintWithRipple$default(View view, long j, int i, Object obj) {
        if ((i & 1) != 0) {
            j = HINT_DELAY_MILLIS;
        }
        hintWithRipple(view, j);
    }

    public static final void i18nSetText(TextInputLayout textInputLayout, @StringRes int i, Object[] objArr, Function1<? super RenderContext, Unit> function1) {
        m.checkNotNullParameter(textInputLayout, "$this$i18nSetText");
        m.checkNotNullParameter(objArr, "formatArgs");
        m.checkNotNullParameter(function1, "initializer");
        EditText editText = textInputLayout.getEditText();
        if (editText != null) {
            b.m(editText, i, Arrays.copyOf(objArr, objArr.length), function1);
        }
    }

    public static /* synthetic */ void i18nSetText$default(TextInputLayout textInputLayout, int i, Object[] objArr, Function1 function1, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            function1 = ViewExtensions$i18nSetText$1.INSTANCE;
        }
        i18nSetText(textInputLayout, i, objArr, function1);
    }

    public static final void interceptScrollWhenInsideScrollable(TextInputLayout textInputLayout) {
        m.checkNotNullParameter(textInputLayout, "$this$interceptScrollWhenInsideScrollable");
        EditText editText = textInputLayout.getEditText();
        if (editText != null) {
            editText.setOnTouchListener(ViewExtensions$interceptScrollWhenInsideScrollable$1.INSTANCE);
        }
    }

    public static final void moveCursorToEnd(TextInputLayout textInputLayout) {
        m.checkNotNullParameter(textInputLayout, "$this$moveCursorToEnd");
        EditText editText = textInputLayout.getEditText();
        if (editText != null) {
            editText.setSelection(editText.getText().toString().length());
        }
    }

    public static final void setBackgroundAndKeepPadding(View view, Drawable drawable) {
        m.checkNotNullParameter(view, "$this$setBackgroundAndKeepPadding");
        view.setBackground(drawable);
    }

    public static final void setCompoundDrawableWithIntrinsicBounds(TextView textView, @DrawableRes int i, @DrawableRes int i2, @DrawableRes int i3, @DrawableRes int i4) {
        m.checkNotNullParameter(textView, "$this$setCompoundDrawableWithIntrinsicBounds");
        textView.setCompoundDrawablesRelativeWithIntrinsicBounds(i, i2, i3, i4);
    }

    public static /* synthetic */ void setCompoundDrawableWithIntrinsicBounds$default(TextView textView, int i, int i2, int i3, int i4, int i5, Object obj) {
        if ((i5 & 1) != 0) {
            i = 0;
        }
        if ((i5 & 2) != 0) {
            i2 = 0;
        }
        if ((i5 & 4) != 0) {
            i3 = 0;
        }
        if ((i5 & 8) != 0) {
            i4 = 0;
        }
        setCompoundDrawableWithIntrinsicBounds(textView, i, i2, i3, i4);
    }

    public static final void setEnabledAlpha(View view, boolean z2, float f) {
        m.checkNotNullParameter(view, "$this$setEnabledAlpha");
        if (z2) {
            f = 1.0f;
        }
        view.setAlpha(f);
    }

    public static /* synthetic */ void setEnabledAlpha$default(View view, boolean z2, float f, int i, Object obj) {
        if ((i & 2) != 0) {
            f = 0.3f;
        }
        setEnabledAlpha(view, z2, f);
    }

    public static final void setEnabledAndAlpha(View view, boolean z2, float f) {
        m.checkNotNullParameter(view, "$this$setEnabledAndAlpha");
        view.setEnabled(z2);
        setEnabledAlpha(view, z2, f);
    }

    public static /* synthetic */ void setEnabledAndAlpha$default(View view, boolean z2, float f, int i, Object obj) {
        if ((i & 2) != 0) {
            f = 0.3f;
        }
        setEnabledAndAlpha(view, z2, f);
    }

    public static final void setForwardingWindowInsetsListener(ViewGroup viewGroup) {
        m.checkNotNullParameter(viewGroup, "$this$setForwardingWindowInsetsListener");
        ViewCompat.setOnApplyWindowInsetsListener(viewGroup, ViewExtensions$setForwardingWindowInsetsListener$1.INSTANCE);
    }

    public static final void setHint(TextInputLayout textInputLayout, int i) {
        m.checkNotNullParameter(textInputLayout, "$this$setHint");
        textInputLayout.setHint(textInputLayout.getContext().getString(i));
    }

    public static final Unit setOnEditTextClickListener(TextInputLayout textInputLayout, final Function1<? super View, Unit> function1) {
        m.checkNotNullParameter(textInputLayout, "$this$setOnEditTextClickListener");
        m.checkNotNullParameter(function1, "l");
        EditText editText = textInputLayout.getEditText();
        if (editText == null) {
            return null;
        }
        editText.setOnClickListener(new View.OnClickListener() { // from class: com.discord.utilities.view.extensions.ViewExtensions$sam$android_view_View_OnClickListener$0
            @Override // android.view.View.OnClickListener
            public final /* synthetic */ void onClick(View view) {
                m.checkNotNullExpressionValue(Function1.this.invoke(view), "invoke(...)");
            }
        });
        return Unit.a;
    }

    public static final void setOnEditTextFocusChangeListener(TextInputLayout textInputLayout, View.OnFocusChangeListener onFocusChangeListener) {
        m.checkNotNullParameter(textInputLayout, "$this$setOnEditTextFocusChangeListener");
        m.checkNotNullParameter(onFocusChangeListener, "l");
        EditText editText = textInputLayout.getEditText();
        if (editText != null) {
            editText.setOnFocusChangeListener(onFocusChangeListener);
        }
    }

    public static final Unit setOnEditorActionListener(TextInputLayout textInputLayout, final Function3<? super TextView, ? super Integer, ? super KeyEvent, Boolean> function3) {
        m.checkNotNullParameter(textInputLayout, "$this$setOnEditorActionListener");
        m.checkNotNullParameter(function3, "l");
        EditText editText = textInputLayout.getEditText();
        if (editText == null) {
            return null;
        }
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: com.discord.utilities.view.extensions.ViewExtensions$sam$android_widget_TextView_OnEditorActionListener$0
            @Override // android.widget.TextView.OnEditorActionListener
            public final /* synthetic */ boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                Object invoke = Function3.this.invoke(textView, Integer.valueOf(i), keyEvent);
                m.checkNotNullExpressionValue(invoke, "invoke(...)");
                return ((Boolean) invoke).booleanValue();
            }
        });
        return Unit.a;
    }

    public static final Unit setOnImeActionDone(TextInputLayout textInputLayout, boolean z2, Function1<? super TextView, Unit> function1) {
        m.checkNotNullParameter(textInputLayout, "$this$setOnImeActionDone");
        m.checkNotNullParameter(function1, "onImeActionDone");
        return setOnEditorActionListener(textInputLayout, new ViewExtensions$setOnImeActionDone$1(textInputLayout, function1, z2));
    }

    public static /* synthetic */ Unit setOnImeActionDone$default(TextInputLayout textInputLayout, boolean z2, Function1 function1, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = false;
        }
        return setOnImeActionDone(textInputLayout, z2, function1);
    }

    public static final void setOnLongClickListenerConsumeClick(View view, final Function1<? super View, Unit> function1) {
        m.checkNotNullParameter(view, "$this$setOnLongClickListenerConsumeClick");
        m.checkNotNullParameter(function1, "onLongClick");
        view.setOnLongClickListener(new View.OnLongClickListener() { // from class: com.discord.utilities.view.extensions.ViewExtensions$setOnLongClickListenerConsumeClick$1
            @Override // android.view.View.OnLongClickListener
            public final boolean onLongClick(View view2) {
                Function1 function12 = Function1.this;
                m.checkNotNullExpressionValue(view2, "view");
                function12.invoke(view2);
                RecyclerView parent = view2.getParent();
                while (true) {
                    if (parent == null) {
                        parent = null;
                        break;
                    } else if (parent instanceof RecyclerView) {
                        break;
                    } else {
                        parent = parent.getParent();
                    }
                }
                RecyclerView recyclerView = parent;
                if (recyclerView == null) {
                    return true;
                }
                RecyclerViewExtensionsKt.ignoreCurrentTouch(recyclerView);
                return true;
            }
        });
    }

    public static final void setPluralText(TextView textView, @PluralsRes int i, int i2, Object... objArr) {
        m.checkNotNullParameter(textView, "$this$setPluralText");
        m.checkNotNullParameter(objArr, "formatArgs");
        Resources resources = textView.getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        Context context = textView.getContext();
        m.checkNotNullExpressionValue(context, "context");
        c0 c0Var = new c0(2);
        c0Var.add(Integer.valueOf(i2));
        c0Var.addSpread(objArr);
        textView.setText(StringResourceUtilsKt.getQuantityString(resources, context, i, i2, c0Var.toArray(new Object[c0Var.size()])));
    }

    public static final void setProgrammaticChecked(CompoundButton compoundButton, boolean z2, CompoundButton.OnCheckedChangeListener onCheckedChangeListener) {
        m.checkNotNullParameter(compoundButton, "$this$setProgrammaticChecked");
        m.checkNotNullParameter(onCheckedChangeListener, "listener");
        compoundButton.setOnCheckedChangeListener(null);
        compoundButton.setChecked(z2);
        compoundButton.setOnCheckedChangeListener(onCheckedChangeListener);
    }

    public static final Unit setSelection(TextInputLayout textInputLayout, int i) {
        m.checkNotNullParameter(textInputLayout, "$this$setSelection");
        EditText editText = textInputLayout.getEditText();
        if (editText == null) {
            return null;
        }
        editText.setSelection(i);
        return Unit.a;
    }

    public static final Unit setSelectionEnd(TextInputLayout textInputLayout) {
        m.checkNotNullParameter(textInputLayout, "$this$setSelectionEnd");
        EditText editText = textInputLayout.getEditText();
        return setSelection(textInputLayout, editText != null ? editText.length() : 0);
    }

    public static final void setSingleLineHint(TextInputLayout textInputLayout, int i) {
        m.checkNotNullParameter(textInputLayout, "$this$setSingleLineHint");
        String string = textInputLayout.getContext().getString(i);
        m.checkNotNullExpressionValue(string, "context.getString(resId)");
        setSingleLineHint(textInputLayout, string);
    }

    public static final Unit setText(TextInputLayout textInputLayout, int i) {
        m.checkNotNullParameter(textInputLayout, "$this$setText");
        EditText editText = textInputLayout.getEditText();
        if (editText == null) {
            return null;
        }
        editText.setText(i);
        return Unit.a;
    }

    public static final void setTextAndVisibilityBy(TextView textView, CharSequence charSequence) {
        m.checkNotNullParameter(textView, "$this$setTextAndVisibilityBy");
        textView.setText(charSequence);
        int i = 0;
        if (!(!(charSequence == null || t.isBlank(charSequence)))) {
            i = 8;
        }
        textView.setVisibility(i);
    }

    public static final void setTextIfDifferent(TextInputLayout textInputLayout, CharSequence charSequence) {
        EditText editText;
        Editable text;
        m.checkNotNullParameter(textInputLayout, "$this$setTextIfDifferent");
        EditText editText2 = textInputLayout.getEditText();
        if ((!m.areEqual((editText2 == null || (text = editText2.getText()) == null) ? null : text.toString(), charSequence)) && (editText = textInputLayout.getEditText()) != null) {
            editText.setText(charSequence);
        }
    }

    public static final void useAttrs(View view, AttributeSet attributeSet, @StyleableRes int[] iArr, Function1<? super TypedArray, Unit> function1) {
        m.checkNotNullParameter(view, "$this$useAttrs");
        m.checkNotNullParameter(iArr, "styleable");
        m.checkNotNullParameter(function1, "block");
        Context context = view.getContext();
        m.checkNotNullExpressionValue(context, "context");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, iArr);
        m.checkNotNullExpressionValue(obtainStyledAttributes, "obtainStyledAttributes(attrs, styleable)");
        function1.invoke(obtainStyledAttributes);
        obtainStyledAttributes.recycle();
    }

    public static final void setSingleLineHint(TextInputLayout textInputLayout, CharSequence charSequence) {
        m.checkNotNullParameter(textInputLayout, "$this$setSingleLineHint");
        m.checkNotNullParameter(charSequence, "charSequence");
        textInputLayout.setHintEnabled(false);
        EditText editText = textInputLayout.getEditText();
        if (editText != null) {
            editText.setHint(charSequence);
        }
    }

    public static final Unit setText(TextInputLayout textInputLayout, CharSequence charSequence) {
        m.checkNotNullParameter(textInputLayout, "$this$setText");
        EditText editText = textInputLayout.getEditText();
        if (editText == null) {
            return null;
        }
        editText.setText(charSequence);
        return Unit.a;
    }
}
