package com.discord.utilities.view.recycler;

import com.google.android.material.tabs.TabLayout;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: ViewPager2Extensions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/google/android/material/tabs/TabLayout$Tab;", "<anonymous parameter 0>", "", "<anonymous parameter 1>", "", "invoke", "(Lcom/google/android/material/tabs/TabLayout$Tab;I)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ViewPager2ExtensionsKt$setUpWithViewPager2$1 extends o implements Function2<TabLayout.Tab, Integer, Unit> {
    public static final ViewPager2ExtensionsKt$setUpWithViewPager2$1 INSTANCE = new ViewPager2ExtensionsKt$setUpWithViewPager2$1();

    public ViewPager2ExtensionsKt$setUpWithViewPager2$1() {
        super(2);
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(TabLayout.Tab tab, Integer num) {
        invoke(tab, num.intValue());
        return Unit.a;
    }

    public final void invoke(TabLayout.Tab tab, int i) {
        m.checkNotNullParameter(tab, "<anonymous parameter 0>");
    }
}
