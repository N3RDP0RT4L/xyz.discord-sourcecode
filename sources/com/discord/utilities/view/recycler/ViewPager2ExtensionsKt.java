package com.discord.utilities.view.recycler;

import androidx.annotation.NonNull;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: ViewPager2Extensions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0010\u0002\n\u0002\b\u0004\u001a5\u0010\b\u001a\u00020\u0006*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u00012\u001a\b\u0002\u0010\u0007\u001a\u0014\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0003¢\u0006\u0004\b\b\u0010\t¨\u0006\n"}, d2 = {"Lcom/google/android/material/tabs/TabLayout;", "Landroidx/viewpager2/widget/ViewPager2;", "viewPager2", "Lkotlin/Function2;", "Lcom/google/android/material/tabs/TabLayout$Tab;", "", "", "configureTab", "setUpWithViewPager2", "(Lcom/google/android/material/tabs/TabLayout;Landroidx/viewpager2/widget/ViewPager2;Lkotlin/jvm/functions/Function2;)V", "utils_release"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ViewPager2ExtensionsKt {
    public static final void setUpWithViewPager2(TabLayout tabLayout, ViewPager2 viewPager2, final Function2<? super TabLayout.Tab, ? super Integer, Unit> function2) {
        m.checkNotNullParameter(tabLayout, "$this$setUpWithViewPager2");
        m.checkNotNullParameter(viewPager2, "viewPager2");
        m.checkNotNullParameter(function2, "configureTab");
        new TabLayoutMediator(tabLayout, viewPager2, new TabLayoutMediator.TabConfigurationStrategy() { // from class: com.discord.utilities.view.recycler.ViewPager2ExtensionsKt$sam$com_google_android_material_tabs_TabLayoutMediator_TabConfigurationStrategy$0
            @Override // com.google.android.material.tabs.TabLayoutMediator.TabConfigurationStrategy
            public final /* synthetic */ void onConfigureTab(@NonNull TabLayout.Tab tab, int i) {
                m.checkNotNullParameter(tab, "p0");
                m.checkNotNullExpressionValue(Function2.this.invoke(tab, Integer.valueOf(i)), "invoke(...)");
            }
        }).attach();
    }

    public static /* synthetic */ void setUpWithViewPager2$default(TabLayout tabLayout, ViewPager2 viewPager2, Function2 function2, int i, Object obj) {
        if ((i & 2) != 0) {
            function2 = ViewPager2ExtensionsKt$setUpWithViewPager2$1.INSTANCE;
        }
        setUpWithViewPager2(tabLayout, viewPager2, function2);
    }
}
