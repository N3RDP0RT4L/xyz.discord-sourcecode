package com.discord.utilities.view.recycler;

import andhook.lib.HookHelper;
import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import d0.j;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: PaddedItemDecorator.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0007\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\u0012\u001a\u00020\r\u0012\u0006\u0010\u0014\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\r\u0012\b\b\u0002\u0010\u0017\u001a\u00020\u0016¢\u0006\u0004\b\u001b\u0010\u001cJ/\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\bH\u0016¢\u0006\u0004\b\u000b\u0010\fR\u0019\u0010\u000e\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0012\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u000f\u001a\u0004\b\u0013\u0010\u0011R\u0019\u0010\u0014\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u000f\u001a\u0004\b\u0015\u0010\u0011R\u0019\u0010\u0017\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001a¨\u0006\u001d"}, d2 = {"Lcom/discord/utilities/view/recycler/PaddedItemDecorator;", "Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;", "Landroid/graphics/Rect;", "outRect", "Landroid/view/View;", "view", "Landroidx/recyclerview/widget/RecyclerView;", "parent", "Landroidx/recyclerview/widget/RecyclerView$State;", "state", "", "getItemOffsets", "(Landroid/graphics/Rect;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$State;)V", "", "spaceVert", "I", "getSpaceVert", "()I", "orientation", "getOrientation", "spaceHorz", "getSpaceHorz", "", "spaceBetweenItemsOnly", "Z", "getSpaceBetweenItemsOnly", "()Z", HookHelper.constructorName, "(IIIZ)V", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class PaddedItemDecorator extends RecyclerView.ItemDecoration {
    private final int orientation;
    private final boolean spaceBetweenItemsOnly;
    private final int spaceHorz;
    private final int spaceVert;

    public /* synthetic */ PaddedItemDecorator(int i, int i2, int i3, boolean z2, int i4, DefaultConstructorMarker defaultConstructorMarker) {
        this(i, i2, i3, (i4 & 8) != 0 ? false : z2);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.ItemDecoration
    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
        m.checkNotNullParameter(rect, "outRect");
        m.checkNotNullParameter(view, "view");
        m.checkNotNullParameter(recyclerView, "parent");
        m.checkNotNullParameter(state, "state");
        super.getItemOffsets(rect, view, recyclerView, state);
        int childAdapterPosition = recyclerView.getChildAdapterPosition(view);
        int i = 0;
        boolean z2 = childAdapterPosition == 0;
        boolean z3 = childAdapterPosition == state.getItemCount() - 1;
        int i2 = this.orientation;
        if (i2 == 0) {
            int i3 = this.spaceVert;
            rect.top = i3;
            rect.bottom = i3;
            rect.left = (!z2 || this.spaceBetweenItemsOnly) ? 0 : this.spaceHorz;
            if (!z3 || !this.spaceBetweenItemsOnly) {
                i = this.spaceHorz;
            }
            rect.right = i;
        } else if (i2 == 1) {
            rect.top = (!z2 || this.spaceBetweenItemsOnly) ? 0 : this.spaceVert;
            if (!z3 || !this.spaceBetweenItemsOnly) {
                i = this.spaceVert;
            }
            rect.bottom = i;
            int i4 = this.spaceHorz;
            rect.left = i4;
            rect.right = i4;
        } else {
            throw new j(a.v("An operation is not implemented: ", a.A(a.R("Other("), this.orientation, ") orientation padding for items not supported yet.")));
        }
    }

    public final int getOrientation() {
        return this.orientation;
    }

    public final boolean getSpaceBetweenItemsOnly() {
        return this.spaceBetweenItemsOnly;
    }

    public final int getSpaceHorz() {
        return this.spaceHorz;
    }

    public final int getSpaceVert() {
        return this.spaceVert;
    }

    public PaddedItemDecorator(int i, int i2, int i3, boolean z2) {
        this.orientation = i;
        this.spaceHorz = i2;
        this.spaceVert = i3;
        this.spaceBetweenItemsOnly = z2;
    }
}
