package com.discord.utilities.frecency;

import andhook.lib.HookHelper;
import androidx.exifinterface.media.ExifInterface;
import com.discord.utilities.time.ClockFactory;
import d0.t.n;
import d0.t.u;
import d0.u.a;
import d0.z.d.m;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
/* compiled from: FrecencyTracker.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u001e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0007\b&\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002B\u0017\u0012\u0006\u0010\"\u001a\u00020\t\u0012\u0006\u0010\u0015\u001a\u00020\t¢\u0006\u0004\b$\u0010%J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J!\u0010\n\u001a\u00020\t*\b\u0012\u0004\u0012\u00020\u00030\b2\u0006\u0010\u0004\u001a\u00020\u0003H$¢\u0006\u0004\b\n\u0010\u000bJ\u001f\u0010\u000e\u001a\u00020\u00052\u0006\u0010\f\u001a\u00028\u00002\b\b\u0002\u0010\r\u001a\u00020\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u001d\u0010\u0011\u001a\b\u0012\u0004\u0012\u00028\u00000\u00102\b\b\u0002\u0010\r\u001a\u00020\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0015\u0010\u0013\u001a\u00020\u00052\u0006\u0010\f\u001a\u00028\u0000¢\u0006\u0004\b\u0013\u0010\u0014R\u0019\u0010\u0015\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018R>\u0010\u001b\u001a*\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\b0\u0019j\u0014\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\b`\u001a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\u001c\u0010\u001d\u001a\b\u0012\u0004\u0012\u00028\u00000\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u0016\u0010 \u001a\u00020\u001f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b \u0010!R\u0019\u0010\"\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u0016\u001a\u0004\b#\u0010\u0018¨\u0006&"}, d2 = {"Lcom/discord/utilities/frecency/FrecencyTracker;", ExifInterface.GPS_DIRECTION_TRUE, "", "", "currentTime", "", "computeScores", "(J)V", "", "", "computeScore", "(Ljava/util/List;J)I", "key", "now", "track", "(Ljava/lang/Object;J)V", "", "getSortedKeys", "(J)Ljava/util/Collection;", "removeEntry", "(Ljava/lang/Object;)V", "maxSamples", "I", "getMaxSamples", "()I", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "history", "Ljava/util/HashMap;", "sortedKeys", "Ljava/util/List;", "", "dirty", "Z", "minScoreThreshold", "getMinScoreThreshold", HookHelper.constructorName, "(II)V", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class FrecencyTracker<T> {
    private final int maxSamples;
    private final int minScoreThreshold;
    private final HashMap<T, List<Long>> history = new HashMap<>();
    private transient boolean dirty = true;
    private transient List<? extends T> sortedKeys = n.emptyList();

    public FrecencyTracker(int i, int i2) {
        this.minScoreThreshold = i;
        this.maxSamples = i2;
    }

    private final void computeScores(long j) {
        final HashMap hashMap = new HashMap(this.history.size());
        Iterator<Map.Entry<T, List<Long>>> it = this.history.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<T, List<Long>> next = it.next();
            T key = next.getKey();
            int computeScore = computeScore(next.getValue(), j);
            if (computeScore > this.minScoreThreshold) {
                hashMap.put(key, Integer.valueOf(computeScore));
            } else {
                it.remove();
            }
        }
        Set keySet = hashMap.keySet();
        m.checkNotNullExpressionValue(keySet, "scores.keys");
        this.sortedKeys = u.sortedWith(keySet, new Comparator<T>() { // from class: com.discord.utilities.frecency.FrecencyTracker$computeScores$1
            @Override // java.util.Comparator
            public final int compare(T t, T t2) {
                int i;
                HashMap hashMap2;
                HashMap hashMap3;
                Integer num = (Integer) hashMap.get(t);
                Integer num2 = (Integer) hashMap.get(t2);
                if (!m.areEqual(num, num2)) {
                    i = a.compareValues(num, num2);
                } else {
                    hashMap2 = FrecencyTracker.this.history;
                    List list = (List) hashMap2.get(t);
                    Long l = null;
                    Long l2 = list != null ? (Long) u.last((List<? extends Object>) list) : null;
                    hashMap3 = FrecencyTracker.this.history;
                    List list2 = (List) hashMap3.get(t2);
                    if (list2 != null) {
                        l = (Long) u.last((List<? extends Object>) list2);
                    }
                    i = a.compareValues(l2, l);
                }
                return i * (-1);
            }
        });
        this.dirty = false;
    }

    public static /* synthetic */ Collection getSortedKeys$default(FrecencyTracker frecencyTracker, long j, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                j = ClockFactory.get().currentTimeMillis();
            }
            return frecencyTracker.getSortedKeys(j);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getSortedKeys");
    }

    public static /* synthetic */ void track$default(FrecencyTracker frecencyTracker, Object obj, long j, int i, Object obj2) {
        if (obj2 == null) {
            if ((i & 2) != 0) {
                j = ClockFactory.get().currentTimeMillis();
            }
            frecencyTracker.track(obj, j);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: track");
    }

    public abstract int computeScore(List<Long> list, long j);

    public final int getMaxSamples() {
        return this.maxSamples;
    }

    public final int getMinScoreThreshold() {
        return this.minScoreThreshold;
    }

    public final synchronized Collection<T> getSortedKeys(long j) {
        if (this.dirty) {
            computeScores(j);
        }
        return this.sortedKeys;
    }

    public final synchronized void removeEntry(T t) {
        this.history.remove(t);
        this.dirty = true;
    }

    public final synchronized void track(T t, long j) {
        List<Long> list = this.history.get(t);
        if (list == null) {
            list = n.emptyList();
        }
        this.history.put(t, u.takeLast(u.plus((Collection<? extends Long>) list, Long.valueOf(j)), this.maxSamples));
        this.dirty = true;
    }
}
