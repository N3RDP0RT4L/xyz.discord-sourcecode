package com.discord.utilities.attachments;

import android.webkit.MimeTypeMap;
import d0.z.d.m;
import d0.z.d.o;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: AttachmentUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000e\n\u0002\b\u0004\u0010\u0004\u001a\u0004\u0018\u00010\u00002\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"", "file", "invoke", "(Ljava/lang/String;)Ljava/lang/String;", "getMimeTypeFromExtension"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AttachmentUtilsKt$getMimeType$1 extends o implements Function1<String, String> {
    public static final AttachmentUtilsKt$getMimeType$1 INSTANCE = new AttachmentUtilsKt$getMimeType$1();

    public AttachmentUtilsKt$getMimeType$1() {
        super(1);
    }

    public final String invoke(String str) {
        Pattern regex_file_name_pattern;
        m.checkNotNullParameter(str, "file");
        regex_file_name_pattern = AttachmentUtilsKt.getREGEX_FILE_NAME_PATTERN();
        Matcher matcher = regex_file_name_pattern.matcher(str);
        if (!matcher.matches()) {
            return null;
        }
        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(matcher.group(2));
    }
}
