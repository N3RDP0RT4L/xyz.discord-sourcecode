package com.discord.utilities.attachments;

import androidx.core.view.inputmethod.InputContentInfoCompat;
import com.lytefast.flexinput.model.Attachment;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: AttachmentUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\n\u0010\u0001\u001a\u0006\u0012\u0002\b\u00030\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/lytefast/flexinput/model/Attachment;", "it", "", "invoke", "(Lcom/lytefast/flexinput/model/Attachment;)Ljava/lang/CharSequence;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AttachmentUtilsKt$appendLinks$1 extends o implements Function1<Attachment<?>, CharSequence> {
    public static final AttachmentUtilsKt$appendLinks$1 INSTANCE = new AttachmentUtilsKt$appendLinks$1();

    public AttachmentUtilsKt$appendLinks$1() {
        super(1);
    }

    public final CharSequence invoke(Attachment<?> attachment) {
        m.checkNotNullParameter(attachment, "it");
        Object data = attachment.getData();
        Objects.requireNonNull(data, "null cannot be cast to non-null type androidx.core.view.inputmethod.InputContentInfoCompat");
        InputContentInfoCompat inputContentInfoCompat = (InputContentInfoCompat) data;
        String valueOf = String.valueOf(inputContentInfoCompat.getLinkUri());
        inputContentInfoCompat.releasePermission();
        return valueOf;
    }
}
