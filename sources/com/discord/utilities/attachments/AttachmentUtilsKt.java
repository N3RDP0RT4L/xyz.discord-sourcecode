package com.discord.utilities.attachments;

import andhook.lib.xposed.ClassUtils;
import android.content.ClipDescription;
import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.net.Uri;
import androidx.core.app.NotificationCompat;
import androidx.core.view.inputmethod.InputContentInfoCompat;
import com.discord.api.message.LocalAttachment;
import com.discord.utilities.string.StringUtilsKt;
import com.lytefast.flexinput.model.Attachment;
import d0.g;
import d0.g0.t;
import d0.t.u;
import d0.z.d.m;
import g0.e;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.ranges.IntRange;
import org.webrtc.MediaStreamTrack;
/* compiled from: AttachmentUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0001\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a+\u0010\u0006\u001a\u00020\u00042\b\u0010\u0001\u001a\u0004\u0018\u00010\u00002\u0006\u0010\u0003\u001a\u00020\u00022\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0004¢\u0006\u0004\b\u0006\u0010\u0007\u001a\u001f\u0010\u0006\u001a\u00020\u0004*\u0006\u0012\u0002\b\u00030\b2\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000¢\u0006\u0004\b\u0006\u0010\t\u001a+\u0010\u000b\u001a\u00020\n2\b\u0010\u0001\u001a\u0004\u0018\u00010\u00002\u0006\u0010\u0003\u001a\u00020\u00022\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0004¢\u0006\u0004\b\u000b\u0010\f\u001a\u001f\u0010\u000b\u001a\u00020\n*\u0006\u0012\u0002\b\u00030\b2\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000¢\u0006\u0004\b\u000b\u0010\r\u001a+\u0010\u000e\u001a\u00020\n2\b\u0010\u0001\u001a\u0004\u0018\u00010\u00002\u0006\u0010\u0003\u001a\u00020\u00022\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0004¢\u0006\u0004\b\u000e\u0010\f\u001a\u001f\u0010\u000e\u001a\u00020\n*\u0006\u0012\u0002\b\u00030\b2\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000¢\u0006\u0004\b\u000e\u0010\r\u001a\u001f\u0010\u000f\u001a\u00020\n*\u0006\u0012\u0002\b\u00030\b2\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000¢\u0006\u0004\b\u000f\u0010\r\u001a\u001f\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u00042\b\u0010\u0012\u001a\u0004\u0018\u00010\u0011¢\u0006\u0004\b\u0013\u0010\u0014\u001a-\u0010\u0016\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\b0\u0015*\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\b0\u00152\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0016\u0010\u0017\u001a'\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u0018\u001a\u00020\u00042\u0010\u0010\u0019\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\b0\u0015¢\u0006\u0004\b\u001a\u0010\u001b\u001a\u0015\u0010\u001d\u001a\u00020\u001c*\u0006\u0012\u0002\b\u00030\b¢\u0006\u0004\b\u001d\u0010\u001e\u001a\u0017\u0010 \u001a\b\u0012\u0004\u0012\u00020\u001f0\b*\u00020\u001c¢\u0006\u0004\b \u0010!\u001a\u0019\u0010\"\u001a\u00020\n*\u00020\u001c2\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\"\u0010#\u001a\u0019\u0010$\u001a\u00020\n*\u00020\u001c2\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b$\u0010#\u001a\u0013\u0010%\u001a\u00020\u0004*\u00020\u0011H\u0002¢\u0006\u0004\b%\u0010&\u001a\u0013\u0010'\u001a\u00020\u0004*\u00020\u0004H\u0002¢\u0006\u0004\b'\u0010(\"\u0016\u0010*\u001a\u00020)8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b*\u0010+\"\u0016\u0010,\u001a\u00020)8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b,\u0010+\"\u001d\u00102\u001a\u00020-8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b.\u0010/\u001a\u0004\b0\u00101\"\u0016\u00104\u001a\u0002038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b4\u00105¨\u00066"}, d2 = {"Landroid/content/ContentResolver;", "contentResolver", "Landroid/net/Uri;", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "", "displayName", "getMimeType", "(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;", "Lcom/lytefast/flexinput/model/Attachment;", "(Lcom/lytefast/flexinput/model/Attachment;Landroid/content/ContentResolver;)Ljava/lang/String;", "", "isImage", "(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;)Z", "(Lcom/lytefast/flexinput/model/Attachment;Landroid/content/ContentResolver;)Z", "isVideo", "isGif", "fileName", "Landroid/graphics/Bitmap$CompressFormat;", "compressFormat", "getSanitizedFileName", "(Ljava/lang/String;Landroid/graphics/Bitmap$CompressFormat;)Ljava/lang/String;", "", "extractLinks", "(Ljava/util/List;Landroid/content/ContentResolver;)Ljava/util/List;", "content", "links", "appendLinks", "(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;", "Lcom/discord/api/message/LocalAttachment;", "toLocalAttachment", "(Lcom/lytefast/flexinput/model/Attachment;)Lcom/discord/api/message/LocalAttachment;", "", "toAttachment", "(Lcom/discord/api/message/LocalAttachment;)Lcom/lytefast/flexinput/model/Attachment;", "isImageAttachment", "(Lcom/discord/api/message/LocalAttachment;Landroid/content/ContentResolver;)Z", "isVideoAttachment", "getExtension", "(Landroid/graphics/Bitmap$CompressFormat;)Ljava/lang/String;", "toHumanReadableAscii", "(Ljava/lang/String;)Ljava/lang/String;", "", "UTF_8_RANGE_START_EXCLUSIVE", "I", "UTF_8_RANGE_END_EXCLUSIVE", "Ljava/util/regex/Pattern;", "REGEX_FILE_NAME_PATTERN$delegate", "Lkotlin/Lazy;", "getREGEX_FILE_NAME_PATTERN", "()Ljava/util/regex/Pattern;", "REGEX_FILE_NAME_PATTERN", "Lkotlin/ranges/IntRange;", "UTF_8_RANGE_EXCLUSIVE", "Lkotlin/ranges/IntRange;", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AttachmentUtilsKt {
    private static final int UTF_8_RANGE_END_EXCLUSIVE = 126;
    private static final int UTF_8_RANGE_START_EXCLUSIVE = 32;
    private static final Lazy REGEX_FILE_NAME_PATTERN$delegate = g.lazy(AttachmentUtilsKt$REGEX_FILE_NAME_PATTERN$2.INSTANCE);
    private static final IntRange UTF_8_RANGE_EXCLUSIVE = new IntRange(32, 126);

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0 = new int[Bitmap.CompressFormat.values().length];
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            int[] iArr = new int[Bitmap.CompressFormat.values().length];
            $EnumSwitchMapping$1 = iArr;
            iArr[Bitmap.CompressFormat.PNG.ordinal()] = 1;
            iArr[Bitmap.CompressFormat.WEBP.ordinal()] = 2;
            iArr[Bitmap.CompressFormat.JPEG.ordinal()] = 3;
        }
    }

    public static final String appendLinks(String str, List<? extends Attachment<?>> list) {
        m.checkNotNullParameter(str, "content");
        m.checkNotNullParameter(list, "links");
        return str + "\n" + u.joinToString$default(list, "\n", null, null, 0, null, AttachmentUtilsKt$appendLinks$1.INSTANCE, 30, null);
    }

    public static final List<Attachment<?>> extractLinks(List<? extends Attachment<?>> list, ContentResolver contentResolver) {
        m.checkNotNullParameter(list, "$this$extractLinks");
        m.checkNotNullParameter(contentResolver, "contentResolver");
        ArrayList arrayList = new ArrayList();
        for (Object obj : list) {
            Attachment attachment = (Attachment) obj;
            Object data = attachment.getData();
            if (!(data instanceof InputContentInfoCompat)) {
                data = null;
            }
            InputContentInfoCompat inputContentInfoCompat = (InputContentInfoCompat) data;
            boolean z2 = true;
            if (inputContentInfoCompat == null || inputContentInfoCompat.getLinkUri() == null || !m.areEqual(getMimeType(attachment, contentResolver), "image/gif")) {
                z2 = false;
            }
            if (z2) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }

    private static final String getExtension(Bitmap.CompressFormat compressFormat) {
        int i = WhenMappings.$EnumSwitchMapping$1[compressFormat.ordinal()];
        return i != 1 ? i != 2 ? "jpg" : "webp" : "png";
    }

    public static final String getMimeType(ContentResolver contentResolver, Uri uri, String str) {
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        String str2 = null;
        String type = contentResolver != null ? contentResolver.getType(uri) : null;
        if (type != null) {
            return type;
        }
        AttachmentUtilsKt$getMimeType$1 attachmentUtilsKt$getMimeType$1 = AttachmentUtilsKt$getMimeType$1.INSTANCE;
        try {
            String invoke = attachmentUtilsKt$getMimeType$1.invoke2(String.valueOf(uri));
            str2 = invoke != null ? invoke : attachmentUtilsKt$getMimeType$1.invoke2(String.valueOf(str));
        } catch (Exception unused) {
        }
        return str2 != null ? str2 : "application/octet-stream";
    }

    public static /* synthetic */ String getMimeType$default(ContentResolver contentResolver, Uri uri, String str, int i, Object obj) {
        if ((i & 4) != 0) {
            str = null;
        }
        return getMimeType(contentResolver, uri, str);
    }

    public static final Pattern getREGEX_FILE_NAME_PATTERN() {
        return (Pattern) REGEX_FILE_NAME_PATTERN$delegate.getValue();
    }

    public static final String getSanitizedFileName(String str, Bitmap.CompressFormat compressFormat) {
        m.checkNotNullParameter(str, "fileName");
        if (compressFormat != null) {
            Matcher matcher = getREGEX_FILE_NAME_PATTERN().matcher(str);
            if (matcher.matches()) {
                str = matcher.group(1);
            }
            str = str + ClassUtils.PACKAGE_SEPARATOR_CHAR + getExtension(compressFormat);
        }
        return toHumanReadableAscii(StringUtilsKt.stripAccents(str));
    }

    public static final boolean isGif(Attachment<?> attachment, ContentResolver contentResolver) {
        m.checkNotNullParameter(attachment, "$this$isGif");
        return m.areEqual(getMimeType(attachment, contentResolver), "image/gif");
    }

    public static final boolean isImage(ContentResolver contentResolver, Uri uri, String str) {
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        return t.startsWith$default(getMimeType(contentResolver, uri, str), "image", false, 2, null);
    }

    public static /* synthetic */ boolean isImage$default(ContentResolver contentResolver, Uri uri, String str, int i, Object obj) {
        if ((i & 4) != 0) {
            str = null;
        }
        return isImage(contentResolver, uri, str);
    }

    public static final boolean isImageAttachment(LocalAttachment localAttachment, ContentResolver contentResolver) {
        m.checkNotNullParameter(localAttachment, "$this$isImageAttachment");
        m.checkNotNullParameter(contentResolver, "contentResolver");
        Uri parse = Uri.parse(localAttachment.c());
        m.checkNotNullExpressionValue(parse, "Uri.parse(uriString)");
        return isImage(contentResolver, parse, localAttachment.a());
    }

    public static final boolean isVideo(ContentResolver contentResolver, Uri uri, String str) {
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        return t.startsWith$default(getMimeType(contentResolver, uri, str), MediaStreamTrack.VIDEO_TRACK_KIND, false, 2, null);
    }

    public static /* synthetic */ boolean isVideo$default(ContentResolver contentResolver, Uri uri, String str, int i, Object obj) {
        if ((i & 4) != 0) {
            str = null;
        }
        return isVideo(contentResolver, uri, str);
    }

    public static final boolean isVideoAttachment(LocalAttachment localAttachment, ContentResolver contentResolver) {
        m.checkNotNullParameter(localAttachment, "$this$isVideoAttachment");
        m.checkNotNullParameter(contentResolver, "contentResolver");
        Uri parse = Uri.parse(localAttachment.c());
        m.checkNotNullExpressionValue(parse, "Uri.parse(uriString)");
        return isVideo(contentResolver, parse, localAttachment.a());
    }

    public static final Attachment toAttachment(LocalAttachment localAttachment) {
        m.checkNotNullParameter(localAttachment, "$this$toAttachment");
        long b2 = localAttachment.b();
        Uri parse = Uri.parse(localAttachment.c());
        m.checkNotNullExpressionValue(parse, "Uri.parse(uriString)");
        return new Attachment(b2, parse, localAttachment.a(), null, false, 16, null);
    }

    private static final String toHumanReadableAscii(String str) {
        int i = 0;
        while (i < str.length()) {
            int codePointAt = str.codePointAt(i);
            if (UTF_8_RANGE_EXCLUSIVE.contains(codePointAt)) {
                i += Character.charCount(codePointAt);
            } else {
                e eVar = new e();
                eVar.c0(str, 0, i);
                while (i < str.length()) {
                    int codePointAt2 = str.codePointAt(i);
                    eVar.d0(UTF_8_RANGE_EXCLUSIVE.contains(codePointAt2) ? codePointAt2 : 63);
                    i += Character.charCount(codePointAt2);
                }
                return eVar.D();
            }
        }
        return str;
    }

    public static final LocalAttachment toLocalAttachment(Attachment<?> attachment) {
        m.checkNotNullParameter(attachment, "$this$toLocalAttachment");
        long id2 = attachment.getId();
        String uri = attachment.getUri().toString();
        m.checkNotNullExpressionValue(uri, "uri.toString()");
        return new LocalAttachment(id2, uri, attachment.getDisplayName());
    }

    public static final boolean isImage(Attachment<?> attachment, ContentResolver contentResolver) {
        m.checkNotNullParameter(attachment, "$this$isImage");
        return t.startsWith$default(getMimeType(attachment, contentResolver), "image", false, 2, null);
    }

    public static final boolean isVideo(Attachment<?> attachment, ContentResolver contentResolver) {
        m.checkNotNullParameter(attachment, "$this$isVideo");
        return t.startsWith$default(getMimeType(attachment, contentResolver), MediaStreamTrack.VIDEO_TRACK_KIND, false, 2, null);
    }

    public static final String getMimeType(Attachment<?> attachment, ContentResolver contentResolver) {
        ClipDescription description;
        m.checkNotNullParameter(attachment, "$this$getMimeType");
        Object data = attachment.getData();
        String str = null;
        if (!(data instanceof InputContentInfoCompat)) {
            data = null;
        }
        InputContentInfoCompat inputContentInfoCompat = (InputContentInfoCompat) data;
        if (!(inputContentInfoCompat == null || (description = inputContentInfoCompat.getDescription()) == null)) {
            str = description.getMimeType(0);
        }
        return str != null ? str : getMimeType(contentResolver, attachment.getUri(), attachment.getDisplayName());
    }
}
