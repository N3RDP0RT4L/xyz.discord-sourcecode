package com.discord.utilities.attachments;

import d0.z.d.o;
import java.util.regex.Pattern;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: AttachmentUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0004\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Ljava/util/regex/Pattern;", "kotlin.jvm.PlatformType", "invoke", "()Ljava/util/regex/Pattern;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AttachmentUtilsKt$REGEX_FILE_NAME_PATTERN$2 extends o implements Function0<Pattern> {
    public static final AttachmentUtilsKt$REGEX_FILE_NAME_PATTERN$2 INSTANCE = new AttachmentUtilsKt$REGEX_FILE_NAME_PATTERN$2();

    public AttachmentUtilsKt$REGEX_FILE_NAME_PATTERN$2() {
        super(0);
    }

    @Override // kotlin.jvm.functions.Function0
    public final Pattern invoke() {
        return Pattern.compile("(.*)\\.(\\w+)$");
    }
}
