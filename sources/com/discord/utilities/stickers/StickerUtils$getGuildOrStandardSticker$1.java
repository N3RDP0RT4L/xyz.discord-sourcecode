package com.discord.utilities.stickers;

import androidx.core.app.NotificationCompat;
import com.discord.api.sticker.Sticker;
import com.discord.stores.StoreStream;
import kotlin.Metadata;
import rx.functions.Action1;
/* compiled from: StickerUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/sticker/Sticker;", "fetchedSticker", "", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/api/sticker/Sticker;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StickerUtils$getGuildOrStandardSticker$1<T> implements Action1<Sticker> {
    public static final StickerUtils$getGuildOrStandardSticker$1 INSTANCE = new StickerUtils$getGuildOrStandardSticker$1();

    public final void call(Sticker sticker) {
        StoreStream.Companion companion = StoreStream.Companion;
        companion.getStickers().handleFetchedSticker(sticker);
        companion.getGuildStickers().handleFetchedSticker(sticker);
    }
}
