package com.discord.utilities.stickers;

import com.discord.utilities.dimen.DimenUtils;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import org.objectweb.asm.Opcodes;
/* compiled from: StickerUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\b\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()I", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StickerUtils$DEFAULT_STICKER_SIZE_PX$2 extends o implements Function0<Integer> {
    public static final StickerUtils$DEFAULT_STICKER_SIZE_PX$2 INSTANCE = new StickerUtils$DEFAULT_STICKER_SIZE_PX$2();

    public StickerUtils$DEFAULT_STICKER_SIZE_PX$2() {
        super(0);
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [int, java.lang.Integer] */
    @Override // kotlin.jvm.functions.Function0
    public final Integer invoke() {
        return DimenUtils.dpToPixels((int) Opcodes.IF_ICMPNE);
    }
}
