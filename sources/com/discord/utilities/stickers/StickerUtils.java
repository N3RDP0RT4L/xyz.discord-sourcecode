package com.discord.utilities.stickers;

import andhook.lib.HookHelper;
import andhook.lib.xposed.ClassUtils;
import android.content.Context;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.permission.Permission;
import com.discord.api.premium.PremiumTier;
import com.discord.api.sticker.BaseSticker;
import com.discord.api.sticker.Sticker;
import com.discord.api.sticker.StickerFormatType;
import com.discord.api.sticker.StickerType;
import com.discord.app.AppLog;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelSku;
import com.discord.models.sticker.dto.ModelStickerPack;
import com.discord.models.sticker.dto.ModelStickerPackStoreListing;
import com.discord.models.user.User;
import com.discord.stores.StoreGuildSelected;
import com.discord.stores.StoreGuildStickers;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreStickers;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.billing.PremiumUtilsKt;
import com.discord.utilities.file.DownloadUtils;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.premium.PremiumUtils;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.search.SearchUtils;
import com.discord.utilities.string.StringUtilsKt;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.time.TimeUtils;
import com.discord.utilities.user.UserUtils;
import d0.g;
import d0.g0.t;
import d0.g0.w;
import d0.t.m0;
import d0.t.n;
import d0.t.n0;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import j0.l.e.k;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import kotlin.Lazy;
import kotlin.Metadata;
import org.json.JSONException;
import org.json.JSONObject;
import org.objectweb.asm.Opcodes;
import rx.Observable;
import xyz.discord.R;
/* compiled from: StickerUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000°\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\t\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0011\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001[B\t\b\u0002¢\u0006\u0004\bY\u0010ZJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J!\u0010\t\u001a\u0004\u0018\u00010\u00042\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\t\u0010\nJ#\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b\u0011\u0010\u0012J+\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u000e\u001a\u00020\r2\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00042\b\b\u0002\u0010\u0015\u001a\u00020\u0014¢\u0006\u0004\b\u0017\u0010\u0018J!\u0010\u0019\u001a\u00020\u00162\u0006\u0010\b\u001a\u00020\u00072\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0004¢\u0006\u0004\b\u0019\u0010\u001aJ\u0019\u0010\u001d\u001a\u0004\u0018\u00010\u001c2\b\u0010\u001b\u001a\u0004\u0018\u00010\u0016¢\u0006\u0004\b\u001d\u0010\u001eJ\r\u0010\u001f\u001a\u00020\u0004¢\u0006\u0004\b\u001f\u0010 J\u001f\u0010$\u001a\u00020#2\u0006\u0010\f\u001a\u00020\u000b2\b\u0010\"\u001a\u0004\u0018\u00010!¢\u0006\u0004\b$\u0010%J\u001d\u0010&\u001a\u00020\u00142\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b&\u0010'J-\u0010*\u001a\u00020#2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010(\u001a\u00020\u00022\u0006\u0010)\u001a\u00020\u0014¢\u0006\u0004\b*\u0010+J3\u00101\u001a\b\u0012\u0004\u0012\u00020\u001c002\u0006\u0010,\u001a\u00020\u00162\f\u0010.\u001a\b\u0012\u0004\u0012\u00020\u001c0-2\b\b\u0002\u0010/\u001a\u00020\u0014¢\u0006\u0004\b1\u00102J+\u00107\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u001c0\u000f2\n\u00105\u001a\u000603j\u0002`42\b\b\u0002\u00106\u001a\u00020\u0014¢\u0006\u0004\b7\u00108JE\u0010C\u001a\b\u0012\u0004\u0012\u00020\u001c0-2\b\b\u0002\u0010:\u001a\u0002092\b\b\u0002\u0010<\u001a\u00020;2\b\b\u0002\u0010>\u001a\u00020=2\b\b\u0002\u0010@\u001a\u00020?2\b\b\u0002\u0010B\u001a\u00020A¢\u0006\u0004\bC\u0010DJ;\u0010L\u001a\u00020K2\u0006\u0010\u000e\u001a\u00020\u001c2\u0006\u0010F\u001a\u00020E2\n\b\u0002\u0010H\u001a\u0004\u0018\u00010G2\u0010\b\u0002\u0010J\u001a\n\u0018\u000103j\u0004\u0018\u0001`I¢\u0006\u0004\bL\u0010MR\u0016\u0010N\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bN\u0010OR\u001d\u0010S\u001a\u00020\u00048F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\bP\u0010Q\u001a\u0004\bR\u0010 R\u0016\u0010T\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bT\u0010OR\u0016\u0010U\u001a\u0002038\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\bU\u0010VR\u0016\u0010W\u001a\u00020\u00048\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bW\u0010OR\u0016\u0010X\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\bX\u0010O¨\u0006\\"}, d2 = {"Lcom/discord/utilities/stickers/StickerUtils;", "", "Lcom/discord/api/premium/PremiumTier;", "premiumTier", "", "getStickerPackPrice", "(Lcom/discord/api/premium/PremiumTier;)I", "Lcom/discord/models/sticker/dto/ModelStickerPack;", "stickerPack", "getStickerPackPriceForPremiumTier", "(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/api/premium/PremiumTier;)Ljava/lang/Integer;", "Landroid/content/Context;", "context", "Lcom/discord/api/sticker/BaseSticker;", "sticker", "Lrx/Observable;", "Lcom/discord/utilities/file/DownloadUtils$DownloadState;", "fetchSticker", "(Landroid/content/Context;Lcom/discord/api/sticker/BaseSticker;)Lrx/Observable;", "size", "", "passthrough", "", "getCDNAssetUrl", "(Lcom/discord/api/sticker/BaseSticker;Ljava/lang/Integer;Z)Ljava/lang/String;", "getBannerCDNAssetUrl", "(Lcom/discord/models/sticker/dto/ModelStickerPack;Ljava/lang/Integer;)Ljava/lang/String;", "data", "Lcom/discord/api/sticker/Sticker;", "parseFromMessageNotificationJson", "(Ljava/lang/String;)Lcom/discord/api/sticker/Sticker;", "calculatePremiumStickerPackDiscount", "()I", "Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;", "stickerPackStoreListing", "", "getLimitedTimeLeftString", "(Landroid/content/Context;Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;)Ljava/lang/CharSequence;", "isStickerPackFreeForPremiumTier", "(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/api/premium/PremiumTier;)Z", "currentPremiumTier", "isPackEnabled", "getStickerPackPremiumPriceLabel", "(Landroid/content/Context;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/api/premium/PremiumTier;Z)Ljava/lang/CharSequence;", "searchText", "", "stickers", "allowPartialMatches", "", "findStickerMatches", "(Ljava/lang/String;Ljava/util/List;Z)Ljava/util/Set;", "", "Lcom/discord/primitives/StickerId;", "stickerId", "fetchIfMissing", "getGuildOrStandardSticker", "(JZ)Lrx/Observable;", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreStickers;", "storeStickers", "Lcom/discord/stores/StoreGuildSelected;", "storeGuildSelected", "Lcom/discord/stores/StoreGuildStickers;", "storeGuildStickers", "getStickersForAutocomplete", "(Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreStickers;Lcom/discord/stores/StoreGuildSelected;Lcom/discord/stores/StoreGuildStickers;)Ljava/util/List;", "Lcom/discord/models/user/User;", "meUser", "Lcom/discord/api/channel/Channel;", "currentChannel", "Lcom/discord/api/permission/PermissionBit;", "currentChannelPermissions", "Lcom/discord/utilities/stickers/StickerUtils$StickerSendability;", "getStickerSendability", "(Lcom/discord/api/sticker/Sticker;Lcom/discord/models/user/User;Lcom/discord/api/channel/Channel;Ljava/lang/Long;)Lcom/discord/utilities/stickers/StickerUtils$StickerSendability;", "MINIMUM_LENGTH_STICKER_TEXT_SUGGESTIONS", "I", "DEFAULT_STICKER_SIZE_PX$delegate", "Lkotlin/Lazy;", "getDEFAULT_STICKER_SIZE_PX", "DEFAULT_STICKER_SIZE_PX", "NUM_STICKERS_AUTO_SUGGEST", "STICKER_APPLICATION_ID", "J", "MAXIMUM_LENGTH_STICKER_TEXT_SUGGESTIONS", "MAXIMUM_WORD_COUNT_STICKER_TEXT_SUGGESTIONS", HookHelper.constructorName, "()V", "StickerSendability", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StickerUtils {
    public static final int MAXIMUM_LENGTH_STICKER_TEXT_SUGGESTIONS = 50;
    private static final int MAXIMUM_WORD_COUNT_STICKER_TEXT_SUGGESTIONS = 5;
    public static final int MINIMUM_LENGTH_STICKER_TEXT_SUGGESTIONS = 3;
    public static final int NUM_STICKERS_AUTO_SUGGEST = 4;
    private static final long STICKER_APPLICATION_ID = 710982414301790216L;
    public static final StickerUtils INSTANCE = new StickerUtils();
    private static final Lazy DEFAULT_STICKER_SIZE_PX$delegate = g.lazy(StickerUtils$DEFAULT_STICKER_SIZE_PX$2.INSTANCE);

    /* compiled from: StickerUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/discord/utilities/stickers/StickerUtils$StickerSendability;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "SENDABLE", "SENDABLE_WITH_PREMIUM", "SENDABLE_WITH_PREMIUM_GUILD", "NONSENDABLE", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum StickerSendability {
        SENDABLE,
        SENDABLE_WITH_PREMIUM,
        SENDABLE_WITH_PREMIUM_GUILD,
        NONSENDABLE
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            StickerFormatType.values();
            int[] iArr = new int[4];
            $EnumSwitchMapping$0 = iArr;
            iArr[StickerFormatType.LOTTIE.ordinal()] = 1;
            iArr[StickerFormatType.APNG.ordinal()] = 2;
            iArr[StickerFormatType.PNG.ordinal()] = 3;
            PremiumTier.values();
            int[] iArr2 = new int[4];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[PremiumTier.TIER_2.ordinal()] = 1;
        }
    }

    private StickerUtils() {
    }

    public static /* synthetic */ Set findStickerMatches$default(StickerUtils stickerUtils, String str, List list, boolean z2, int i, Object obj) {
        if ((i & 4) != 0) {
            z2 = false;
        }
        return stickerUtils.findStickerMatches(str, list, z2);
    }

    public static /* synthetic */ String getBannerCDNAssetUrl$default(StickerUtils stickerUtils, ModelStickerPack modelStickerPack, Integer num, int i, Object obj) {
        if ((i & 2) != 0) {
            num = null;
        }
        return stickerUtils.getBannerCDNAssetUrl(modelStickerPack, num);
    }

    public static /* synthetic */ String getCDNAssetUrl$default(StickerUtils stickerUtils, BaseSticker baseSticker, Integer num, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            num = Integer.valueOf(stickerUtils.getDEFAULT_STICKER_SIZE_PX());
        }
        if ((i & 4) != 0) {
            z2 = true;
        }
        return stickerUtils.getCDNAssetUrl(baseSticker, num, z2);
    }

    public static /* synthetic */ Observable getGuildOrStandardSticker$default(StickerUtils stickerUtils, long j, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        return stickerUtils.getGuildOrStandardSticker(j, z2);
    }

    private final int getStickerPackPrice(PremiumTier premiumTier) {
        if (premiumTier.ordinal() != 3) {
            return 299;
        }
        return Opcodes.IFNONNULL;
    }

    private final Integer getStickerPackPriceForPremiumTier(ModelStickerPack modelStickerPack, PremiumTier premiumTier) {
        ModelSku sku;
        ModelSku.Price price;
        ModelSku.ModelPremiumSkuPrice modelPremiumSkuPrice;
        ModelStickerPackStoreListing storeListing = modelStickerPack.getStoreListing();
        if (storeListing == null || (sku = storeListing.getSku()) == null || (price = sku.getPrice()) == null) {
            return null;
        }
        Map<PremiumTier, ModelSku.ModelPremiumSkuPrice> premium = price.getPremium();
        return Integer.valueOf((premium == null || (modelPremiumSkuPrice = premium.get(premiumTier)) == null) ? price.getAmount() : modelPremiumSkuPrice.getAmount());
    }

    public static /* synthetic */ StickerSendability getStickerSendability$default(StickerUtils stickerUtils, Sticker sticker, User user, Channel channel, Long l, int i, Object obj) {
        if ((i & 4) != 0) {
            channel = null;
        }
        if ((i & 8) != 0) {
            l = null;
        }
        return stickerUtils.getStickerSendability(sticker, user, channel, l);
    }

    public static /* synthetic */ List getStickersForAutocomplete$default(StickerUtils stickerUtils, StoreUser storeUser, StoreGuilds storeGuilds, StoreStickers storeStickers, StoreGuildSelected storeGuildSelected, StoreGuildStickers storeGuildStickers, int i, Object obj) {
        if ((i & 1) != 0) {
            storeUser = StoreStream.Companion.getUsers();
        }
        if ((i & 2) != 0) {
            storeGuilds = StoreStream.Companion.getGuilds();
        }
        StoreGuilds storeGuilds2 = storeGuilds;
        if ((i & 4) != 0) {
            storeStickers = StoreStream.Companion.getStickers();
        }
        StoreStickers storeStickers2 = storeStickers;
        if ((i & 8) != 0) {
            storeGuildSelected = StoreStream.Companion.getGuildSelected();
        }
        StoreGuildSelected storeGuildSelected2 = storeGuildSelected;
        if ((i & 16) != 0) {
            storeGuildStickers = StoreStream.Companion.getGuildStickers();
        }
        return stickerUtils.getStickersForAutocomplete(storeUser, storeGuilds2, storeStickers2, storeGuildSelected2, storeGuildStickers);
    }

    public final int calculatePremiumStickerPackDiscount() {
        int stickerPackPrice = getStickerPackPrice(PremiumTier.PREMIUM_GUILD_SUBSCRIPTION_ONLY);
        return (int) (((stickerPackPrice - getStickerPackPrice(PremiumTier.TIER_2)) * 100.0f) / stickerPackPrice);
    }

    public final Observable<DownloadUtils.DownloadState> fetchSticker(Context context, BaseSticker baseSticker) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(baseSticker, "sticker");
        File file = new File(context.getCacheDir(), "stickers");
        if (!file.exists()) {
            file.mkdir();
        }
        String str = baseSticker.d() + baseSticker.b();
        File file2 = new File(file, str);
        if (!file2.exists()) {
            return DownloadUtils.downloadFile(context, getCDNAssetUrl$default(this, baseSticker, null, false, 6, null), str, file);
        }
        k kVar = new k(new DownloadUtils.DownloadState.Completed(file2));
        m.checkNotNullExpressionValue(kVar, "Observable.just(Download…oadState.Completed(file))");
        return kVar;
    }

    public final Set<Sticker> findStickerMatches(String str, List<Sticker> list, boolean z2) {
        boolean z3;
        boolean z4;
        m.checkNotNullParameter(str, "searchText");
        m.checkNotNullParameter(list, "stickers");
        if (str.length() == 0) {
            return n0.emptySet();
        }
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        LinkedHashSet linkedHashSet2 = new LinkedHashSet();
        LinkedHashSet linkedHashSet3 = new LinkedHashSet();
        LinkedHashSet linkedHashSet4 = new LinkedHashSet();
        LinkedHashSet linkedHashSet5 = new LinkedHashSet();
        Set<String> queriesFromSearchText = SearchUtils.INSTANCE.getQueriesFromSearchText(str);
        if (queriesFromSearchText.size() > 5) {
            return n0.emptySet();
        }
        for (String str2 : queriesFromSearchText) {
            for (Sticker sticker : list) {
                if (t.equals(sticker.h(), str2, true)) {
                    linkedHashSet2.add(sticker);
                } else if (z2 && t.startsWith(sticker.h(), str2, true)) {
                    linkedHashSet3.add(sticker);
                } else if (sticker.k() == StickerType.GUILD) {
                    List<String> split$default = w.split$default((CharSequence) sticker.h(), new String[]{" "}, false, 0, 6, (Object) null);
                    if (!(split$default instanceof Collection) || !split$default.isEmpty()) {
                        for (String str3 : split$default) {
                            if (t.equals(str3, str2, true)) {
                                z4 = true;
                                break;
                            }
                        }
                    }
                    z4 = false;
                    if (z4) {
                        linkedHashSet3.add(sticker);
                    }
                }
                List<String> split$default2 = w.split$default((CharSequence) sticker.j(), new String[]{","}, false, 0, 6, (Object) null);
                ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(split$default2, 10));
                for (String str4 : split$default2) {
                    Objects.requireNonNull(str4, "null cannot be cast to non-null type kotlin.CharSequence");
                    arrayList.add(w.trim(str4).toString());
                }
                ArrayList<String> arrayList2 = new ArrayList();
                for (Object obj : arrayList) {
                    if (!t.isBlank((String) obj)) {
                        arrayList2.add(obj);
                    }
                }
                String replace$default = t.replace$default(str2, ":", "", false, 4, (Object) null);
                for (String str5 : arrayList2) {
                    if (t.equals(str5, replace$default, true)) {
                        linkedHashSet4.add(sticker);
                    } else if (z2 && t.startsWith(str5, replace$default, true)) {
                        linkedHashSet5.add(sticker);
                    } else if (sticker.k() == StickerType.GUILD) {
                        List<String> split$default3 = w.split$default((CharSequence) sticker.j(), new String[]{"_"}, false, 0, 6, (Object) null);
                        if (!(split$default3 instanceof Collection) || !split$default3.isEmpty()) {
                            for (String str6 : split$default3) {
                                if (t.equals(str6, str2, true)) {
                                    z3 = true;
                                    break;
                                }
                            }
                        }
                        z3 = false;
                        if (z3) {
                            linkedHashSet5.add(sticker);
                        }
                    }
                }
            }
        }
        linkedHashSet.addAll(linkedHashSet2);
        linkedHashSet.addAll(linkedHashSet3);
        linkedHashSet.addAll(linkedHashSet4);
        linkedHashSet.addAll(linkedHashSet5);
        return linkedHashSet;
    }

    public final String getBannerCDNAssetUrl(ModelStickerPack modelStickerPack, Integer num) {
        String str;
        m.checkNotNullParameter(modelStickerPack, "stickerPack");
        StringBuilder sb = new StringBuilder();
        sb.append("https://cdn.discordapp.com/app-assets/710982414301790216/store/");
        sb.append(modelStickerPack.getBannerAssetId());
        sb.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
        sb.append(StringUtilsKt.getSTATIC_IMAGE_EXTENSION());
        if (num != null) {
            StringBuilder R = a.R("?size=");
            R.append(IconUtils.getMediaProxySize(num.intValue()));
            str = R.toString();
        } else {
            str = "";
        }
        sb.append(str);
        return sb.toString();
    }

    public final String getCDNAssetUrl(BaseSticker baseSticker, Integer num, boolean z2) {
        m.checkNotNullParameter(baseSticker, "sticker");
        int ordinal = baseSticker.a().ordinal();
        String str = "";
        if (ordinal == 1 || ordinal == 2) {
            StringBuilder R = a.R("https://media.discordapp.net/stickers/");
            R.append(baseSticker.d());
            R.append(baseSticker.b());
            R.append("?passthrough=");
            R.append(z2);
            if (num != null) {
                StringBuilder R2 = a.R("&size=");
                R2.append(IconUtils.getMediaProxySize(num.intValue()));
                str = R2.toString();
            }
            R.append(str);
            return R.toString();
        } else if (ordinal != 3) {
            return str;
        } else {
            StringBuilder R3 = a.R("https://discord.com/stickers/");
            R3.append(baseSticker.d());
            R3.append(baseSticker.b());
            return R3.toString();
        }
    }

    public final int getDEFAULT_STICKER_SIZE_PX() {
        return ((Number) DEFAULT_STICKER_SIZE_PX$delegate.getValue()).intValue();
    }

    public final Observable<Sticker> getGuildOrStandardSticker(long j, boolean z2) {
        StoreStream.Companion companion = StoreStream.Companion;
        Sticker sticker = companion.getStickers().getStickers().get(Long.valueOf(j));
        if (sticker != null) {
            k kVar = new k(sticker);
            m.checkNotNullExpressionValue(kVar, "Observable.just(fullStandardSticker)");
            return kVar;
        }
        Sticker guildSticker = companion.getGuildStickers().getGuildSticker(j);
        if (guildSticker != null) {
            k kVar2 = new k(guildSticker);
            m.checkNotNullExpressionValue(kVar2, "Observable.just(fullGuildSticker)");
            return kVar2;
        } else if (!z2) {
            k kVar3 = new k(null);
            m.checkNotNullExpressionValue(kVar3, "Observable.just(null)");
            return kVar3;
        } else {
            Observable<Sticker> t = ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().getSticker(j), false, 1, null).t(StickerUtils$getGuildOrStandardSticker$1.INSTANCE);
            m.checkNotNullExpressionValue(t, "RestAPI\n        .api\n   …fetchedSticker)\n        }");
            return t;
        }
    }

    public final CharSequence getLimitedTimeLeftString(Context context, ModelStickerPackStoreListing modelStickerPackStoreListing) {
        CharSequence b2;
        CharSequence b3;
        m.checkNotNullParameter(context, "context");
        if ((modelStickerPackStoreListing != null ? modelStickerPackStoreListing.getUnpublishedAt() : null) == null) {
            return "";
        }
        long unpublishedAtDate = modelStickerPackStoreListing.getUnpublishedAtDate() - ClockFactory.get().currentTimeMillis();
        TimeUtils timeUtils = TimeUtils.INSTANCE;
        int daysFromMillis = timeUtils.getDaysFromMillis(unpublishedAtDate);
        if (daysFromMillis > 0) {
            return StringResourceUtilsKt.getI18nPluralString(context, R.plurals.duration_days_days, daysFromMillis, Integer.valueOf(daysFromMillis));
        }
        int hoursFromMillis = timeUtils.getHoursFromMillis(unpublishedAtDate);
        long j = unpublishedAtDate - (hoursFromMillis * 3600000);
        int minutesFromMillis = timeUtils.getMinutesFromMillis(j);
        int secondsFromMillis = timeUtils.getSecondsFromMillis(j - (minutesFromMillis * 60000));
        if (hoursFromMillis < 0 || minutesFromMillis < 0 || secondsFromMillis < 0) {
            b2 = b.b(context, R.string.sticker_picker_pack_expiring_soon, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            return b2;
        }
        b3 = b.b(context, R.string.duration_hours_minutes_seconds, new Object[]{a.N(new Object[]{Integer.valueOf(hoursFromMillis)}, 1, "%02d", "java.lang.String.format(format, *args)"), a.N(new Object[]{Integer.valueOf(minutesFromMillis)}, 1, "%02d", "java.lang.String.format(format, *args)"), a.N(new Object[]{Integer.valueOf(secondsFromMillis)}, 1, "%02d", "java.lang.String.format(format, *args)")}, (r4 & 4) != 0 ? b.C0034b.j : null);
        return b3;
    }

    public final CharSequence getStickerPackPremiumPriceLabel(Context context, ModelStickerPack modelStickerPack, PremiumTier premiumTier, boolean z2) {
        CharSequence b2;
        CharSequence b3;
        CharSequence b4;
        CharSequence b5;
        CharSequence b6;
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(modelStickerPack, "stickerPack");
        m.checkNotNullParameter(premiumTier, "currentPremiumTier");
        if (!z2 || premiumTier == PremiumTier.TIER_2) {
            PremiumTier premiumTier2 = PremiumTier.PREMIUM_GUILD_SUBSCRIPTION_ONLY;
            if (premiumTier != premiumTier2) {
                Integer stickerPackPriceForPremiumTier = getStickerPackPriceForPremiumTier(modelStickerPack, premiumTier2);
                Integer stickerPackPriceForPremiumTier2 = getStickerPackPriceForPremiumTier(modelStickerPack, premiumTier);
                if (!(stickerPackPriceForPremiumTier == null || stickerPackPriceForPremiumTier2 == null || !(!m.areEqual(stickerPackPriceForPremiumTier, stickerPackPriceForPremiumTier2)))) {
                    CharSequence formattedPriceUsd = PremiumUtilsKt.getFormattedPriceUsd(stickerPackPriceForPremiumTier.intValue(), context);
                    if (stickerPackPriceForPremiumTier2.intValue() == 0) {
                        return b.b(context, R.string.sticker_picker_discounted_free_android, new Object[]{formattedPriceUsd}, new StickerUtils$getStickerPackPremiumPriceLabel$1(context));
                    }
                    return b.b(context, R.string.sticker_picker_discounted_price_android, new Object[]{PremiumUtilsKt.getFormattedPriceUsd(stickerPackPriceForPremiumTier2.intValue(), context), formattedPriceUsd}, new StickerUtils$getStickerPackPremiumPriceLabel$2(context));
                }
            }
            if (isStickerPackFreeForPremiumTier(modelStickerPack, premiumTier)) {
                b5 = b.b(context, R.string.sticker_pack_price_free, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                return b5;
            }
            PremiumTier premiumTier3 = PremiumTier.TIER_1;
            if (!isStickerPackFreeForPremiumTier(modelStickerPack, premiumTier3) || PremiumUtils.INSTANCE.isPremiumTierAtLeast(premiumTier, premiumTier3)) {
                PremiumTier premiumTier4 = PremiumTier.TIER_2;
                if (!isStickerPackFreeForPremiumTier(modelStickerPack, premiumTier4) || PremiumUtils.INSTANCE.isPremiumTierAtLeast(premiumTier, premiumTier4)) {
                    CharSequence formattedPriceUsd2 = PremiumUtilsKt.getFormattedPriceUsd(getStickerPackPrice(premiumTier4), context);
                    if (premiumTier == premiumTier4) {
                        return formattedPriceUsd2;
                    }
                    b2 = b.b(context, R.string.sticker_picker_price_with_premium_tier_2, new Object[]{formattedPriceUsd2}, (r4 & 4) != 0 ? b.C0034b.j : null);
                    return b2;
                }
                b3 = b.b(context, R.string.sticker_pack_price_free_with_premium_tier_2, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                return b3;
            }
            b4 = b.b(context, R.string.sticker_pack_price_free_with_premium_tier_1, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            return b4;
        }
        b6 = b.b(context, R.string.sticker_pack_premium_cta, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        return b6;
    }

    public final StickerSendability getStickerSendability(Sticker sticker, User user, Channel channel, Long l) {
        m.checkNotNullParameter(sticker, "sticker");
        m.checkNotNullParameter(user, "meUser");
        boolean z2 = user.getPremiumTier() == PremiumTier.TIER_2;
        if (sticker.k() == StickerType.STANDARD) {
            if (z2) {
                return StickerSendability.SENDABLE;
            }
            return StickerSendability.SENDABLE_WITH_PREMIUM;
        } else if (sticker.k() != StickerType.GUILD) {
            return StickerSendability.NONSENDABLE;
        } else {
            if (m.areEqual(sticker.e(), Boolean.FALSE)) {
                return StickerSendability.SENDABLE_WITH_PREMIUM_GUILD;
            }
            if (m.areEqual(sticker.g(), channel != null ? Long.valueOf(channel.f()) : null)) {
                return StickerSendability.SENDABLE;
            }
            if (channel != null && !ChannelUtils.x(channel) && !PermissionUtils.can(Permission.USE_EXTERNAL_STICKERS, l)) {
                return StickerSendability.NONSENDABLE;
            }
            if (z2) {
                return StickerSendability.SENDABLE;
            }
            return StickerSendability.SENDABLE_WITH_PREMIUM;
        }
    }

    public final List<Sticker> getStickersForAutocomplete(StoreUser storeUser, StoreGuilds storeGuilds, StoreStickers storeStickers, StoreGuildSelected storeGuildSelected, StoreGuildStickers storeGuildStickers) {
        List<Sticker> list;
        Set<Long> set;
        m.checkNotNullParameter(storeUser, "storeUser");
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        m.checkNotNullParameter(storeStickers, "storeStickers");
        m.checkNotNullParameter(storeGuildSelected, "storeGuildSelected");
        m.checkNotNullParameter(storeGuildStickers, "storeGuildStickers");
        boolean isPremiumTier2 = UserUtils.INSTANCE.isPremiumTier2(storeUser.getMe());
        if (isPremiumTier2) {
            list = storeStickers.getEnabledStickers();
        } else {
            list = n.emptyList();
        }
        if (isPremiumTier2) {
            set = storeGuilds.getGuilds().keySet();
        } else {
            set = m0.setOf(Long.valueOf(storeGuildSelected.getSelectedGuildId()));
        }
        List<Sticker> allGuildStickersFlattened = storeGuildStickers.getAllGuildStickersFlattened();
        ArrayList arrayList = new ArrayList();
        for (Object obj : allGuildStickersFlattened) {
            if (u.contains(set, ((Sticker) obj).g())) {
                arrayList.add(obj);
            }
        }
        return u.plus((Collection) list, (Iterable) arrayList);
    }

    public final boolean isStickerPackFreeForPremiumTier(ModelStickerPack modelStickerPack, PremiumTier premiumTier) {
        m.checkNotNullParameter(modelStickerPack, "stickerPack");
        m.checkNotNullParameter(premiumTier, "premiumTier");
        boolean z2 = premiumTier == PremiumTier.TIER_2 && modelStickerPack.isPremiumPack();
        Integer stickerPackPriceForPremiumTier = getStickerPackPriceForPremiumTier(modelStickerPack, premiumTier);
        return z2 || (stickerPackPriceForPremiumTier != null && stickerPackPriceForPremiumTier.intValue() == 0);
    }

    public final Sticker parseFromMessageNotificationJson(String str) {
        if (str == null) {
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject(str).getJSONArray("stickers").getJSONObject(0);
            m.checkNotNullExpressionValue(jSONObject, "JSONObject(data).getJSON…ickers\").getJSONObject(0)");
            try {
                long parseLong = Long.parseLong(jSONObject.get(ModelAuditLogEntry.CHANGE_KEY_ID).toString());
                Long valueOf = Long.valueOf(Long.parseLong(jSONObject.get("pack_id").toString()));
                String string = jSONObject.getString(ModelAuditLogEntry.CHANGE_KEY_NAME);
                m.checkNotNullExpressionValue(string, "jsonSticker.getString(\"name\")");
                String string2 = jSONObject.getString(ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION);
                m.checkNotNullExpressionValue(string2, "jsonSticker.getString(\"description\")");
                return new Sticker(parseLong, valueOf, Long.valueOf(Long.parseLong(jSONObject.get(ModelAuditLogEntry.CHANGE_KEY_GUILD_ID).toString())), string, string2, StickerFormatType.Companion.a(Integer.parseInt(jSONObject.get(ModelAuditLogEntry.CHANGE_KEY_FORMAT_TYPE).toString())), jSONObject.has(ModelAuditLogEntry.CHANGE_KEY_TAGS) ? jSONObject.get(ModelAuditLogEntry.CHANGE_KEY_TAGS).toString() : "", StickerType.Companion.a(Integer.parseInt(jSONObject.get("type").toString())), null, 256);
            } catch (JSONException e) {
                Logger.e$default(AppLog.g, "Error parsing sticker from notification", e, null, 4, null);
                return null;
            }
        } catch (JSONException unused) {
            return null;
        }
    }
}
