package com.discord.utilities.streams;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import com.discord.api.channel.Channel;
import com.discord.api.permission.Permission;
import com.discord.api.voice.state.VoiceState;
import com.discord.models.domain.ModelApplicationStream;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.user.MeUser;
import com.discord.models.user.User;
import com.discord.stores.StoreApplicationStreamPreviews;
import com.discord.stores.StoreApplicationStreaming;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreVoiceChannelSelected;
import com.discord.stores.StoreVoiceStates;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rx.ObservableCombineLatestOverloadsKt;
import com.discord.utilities.streams.StreamContext;
import d0.d0.f;
import d0.t.g0;
import d0.t.h0;
import d0.t.o;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.functions.Function10;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.FuncN;
/* compiled from: StreamContextService.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001BW\u0012\b\b\u0002\u0010\u001a\u001a\u00020\u0019\u0012\b\b\u0002\u0010\u0017\u001a\u00020\u0016\u0012\b\b\u0002\u0010#\u001a\u00020\"\u0012\b\b\u0002\u0010\u001d\u001a\u00020\u001c\u0012\b\b\u0002\u0010 \u001a\u00020\u001f\u0012\b\b\u0002\u0010*\u001a\u00020)\u0012\b\b\u0002\u0010'\u001a\u00020&\u0012\b\b\u0002\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b,\u0010-J1\u0010\n\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010\t0\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0015\u0010\r\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\f0\b¢\u0006\u0004\b\r\u0010\u000eJ#\u0010\u0012\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0010j\u0002`\u0011\u0012\u0004\u0012\u00020\f0\u000f0\b¢\u0006\u0004\b\u0012\u0010\u000eJ)\u0010\u0014\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\f0\b2\n\u0010\u0013\u001a\u00060\u0010j\u0002`\u00112\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0014\u0010\u0015R\u0016\u0010\u0017\u001a\u00020\u00168\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u0016\u0010\u001a\u001a\u00020\u00198\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001a\u0010\u001bR\u0016\u0010\u001d\u001a\u00020\u001c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u0016\u0010 \u001a\u00020\u001f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b \u0010!R\u0016\u0010#\u001a\u00020\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$R\u0016\u0010\u0007\u001a\u00020\u00068\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0007\u0010%R\u0016\u0010'\u001a\u00020&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R\u0016\u0010*\u001a\u00020)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010+¨\u0006."}, d2 = {"Lcom/discord/utilities/streams/StreamContextService;", "", "Lcom/discord/models/domain/ModelApplicationStream;", "stream", "", "includePreview", "Lcom/discord/stores/StoreApplicationStreamPreviews;", "applicationStreamPreviewStore", "Lrx/Observable;", "Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;", "getPreviewObservable", "(Lcom/discord/models/domain/ModelApplicationStream;ZLcom/discord/stores/StoreApplicationStreamPreviews;)Lrx/Observable;", "Lcom/discord/utilities/streams/StreamContext;", "getForActiveStream", "()Lrx/Observable;", "", "", "Lcom/discord/primitives/UserId;", "getForAllStreamingUsers", "userId", "getForUser", "(JZ)Lrx/Observable;", "Lcom/discord/stores/StoreGuilds;", "guildStore", "Lcom/discord/stores/StoreGuilds;", "Lcom/discord/stores/StoreApplicationStreaming;", "applicationStreamingStore", "Lcom/discord/stores/StoreApplicationStreaming;", "Lcom/discord/stores/StoreUser;", "userStore", "Lcom/discord/stores/StoreUser;", "Lcom/discord/stores/StoreVoiceChannelSelected;", "voiceChannelSelectedStore", "Lcom/discord/stores/StoreVoiceChannelSelected;", "Lcom/discord/stores/StorePermissions;", "permissionsStore", "Lcom/discord/stores/StorePermissions;", "Lcom/discord/stores/StoreApplicationStreamPreviews;", "Lcom/discord/stores/StoreChannels;", "channelStore", "Lcom/discord/stores/StoreChannels;", "Lcom/discord/stores/StoreVoiceStates;", "voiceStateStore", "Lcom/discord/stores/StoreVoiceStates;", HookHelper.constructorName, "(Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreVoiceStates;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreApplicationStreamPreviews;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StreamContextService {
    private final StoreApplicationStreamPreviews applicationStreamPreviewStore;
    private final StoreApplicationStreaming applicationStreamingStore;
    private final StoreChannels channelStore;
    private final StoreGuilds guildStore;
    private final StorePermissions permissionsStore;
    private final StoreUser userStore;
    private final StoreVoiceChannelSelected voiceChannelSelectedStore;
    private final StoreVoiceStates voiceStateStore;

    public StreamContextService() {
        this(null, null, null, null, null, null, null, null, 255, null);
    }

    public StreamContextService(StoreApplicationStreaming storeApplicationStreaming, StoreGuilds storeGuilds, StorePermissions storePermissions, StoreUser storeUser, StoreVoiceChannelSelected storeVoiceChannelSelected, StoreVoiceStates storeVoiceStates, StoreChannels storeChannels, StoreApplicationStreamPreviews storeApplicationStreamPreviews) {
        m.checkNotNullParameter(storeApplicationStreaming, "applicationStreamingStore");
        m.checkNotNullParameter(storeGuilds, "guildStore");
        m.checkNotNullParameter(storePermissions, "permissionsStore");
        m.checkNotNullParameter(storeUser, "userStore");
        m.checkNotNullParameter(storeVoiceChannelSelected, "voiceChannelSelectedStore");
        m.checkNotNullParameter(storeVoiceStates, "voiceStateStore");
        m.checkNotNullParameter(storeChannels, "channelStore");
        m.checkNotNullParameter(storeApplicationStreamPreviews, "applicationStreamPreviewStore");
        this.applicationStreamingStore = storeApplicationStreaming;
        this.guildStore = storeGuilds;
        this.permissionsStore = storePermissions;
        this.userStore = storeUser;
        this.voiceChannelSelectedStore = storeVoiceChannelSelected;
        this.voiceStateStore = storeVoiceStates;
        this.channelStore = storeChannels;
        this.applicationStreamPreviewStore = storeApplicationStreamPreviews;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Observable<? extends StoreApplicationStreamPreviews.StreamPreview> getPreviewObservable(ModelApplicationStream modelApplicationStream, boolean z2, StoreApplicationStreamPreviews storeApplicationStreamPreviews) {
        if (z2) {
            return storeApplicationStreamPreviews.observeStreamPreview(modelApplicationStream);
        }
        k kVar = new k(null);
        m.checkNotNullExpressionValue(kVar, "Observable.just(null)");
        return kVar;
    }

    public final Observable<StreamContext> getForActiveStream() {
        Observable Y = this.applicationStreamingStore.observeActiveStream().Y(new b<StoreApplicationStreaming.ActiveApplicationStream, Observable<? extends StreamContext>>() { // from class: com.discord.utilities.streams.StreamContextService$getForActiveStream$1
            public final Observable<? extends StreamContext> call(StoreApplicationStreaming.ActiveApplicationStream activeApplicationStream) {
                if (activeApplicationStream == null) {
                    return new k(null);
                }
                return StreamContextService.this.getForUser(activeApplicationStream.getStream().getOwnerId(), false);
            }
        });
        m.checkNotNullExpressionValue(Y, "applicationStreamingStor…  )\n          }\n        }");
        return Y;
    }

    public final Observable<Map<Long, StreamContext>> getForAllStreamingUsers() {
        Observable<Map<Long, StreamContext>> Y = this.applicationStreamingStore.observeStreamsByUser().F(StreamContextService$getForAllStreamingUsers$1.INSTANCE).Y(new b<Collection<? extends ModelApplicationStream>, Observable<? extends Map<Long, ? extends StreamContext>>>() { // from class: com.discord.utilities.streams.StreamContextService$getForAllStreamingUsers$2

            /* compiled from: StreamContextService.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0010\u0011\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\n\u001a*\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u0007 \u0002*\u0014\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u00040\u00042,\u0010\u0003\u001a(\u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00010\u0001 \u0002*\u0014\u0012\u000e\b\u0001\u0012\n \u0002*\u0004\u0018\u00010\u00010\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\b\u0010\t"}, d2 = {"", "", "kotlin.jvm.PlatformType", "allUserStreamContexts", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/utilities/streams/StreamContext;", NotificationCompat.CATEGORY_CALL, "([Ljava/lang/Object;)Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.utilities.streams.StreamContextService$getForAllStreamingUsers$2$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1<R> implements FuncN<Map<Long, ? extends StreamContext>> {
                public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                @Override // rx.functions.FuncN
                public final Map<Long, ? extends StreamContext> call(Object[] objArr) {
                    m.checkNotNullExpressionValue(objArr, "allUserStreamContexts");
                    ArrayList arrayList = new ArrayList();
                    for (Object obj : objArr) {
                        if (!(obj instanceof StreamContext)) {
                            obj = null;
                        }
                        StreamContext streamContext = (StreamContext) obj;
                        if (streamContext != null) {
                            arrayList.add(streamContext);
                        }
                    }
                    LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(o.collectionSizeOrDefault(arrayList, 10)), 16));
                    for (T t : arrayList) {
                        linkedHashMap.put(Long.valueOf(((StreamContext) t).getUser().getId()), t);
                    }
                    return linkedHashMap;
                }
            }

            public final Observable<? extends Map<Long, StreamContext>> call(Collection<? extends ModelApplicationStream> collection) {
                if (collection.isEmpty()) {
                    return new k(h0.emptyMap());
                }
                m.checkNotNullExpressionValue(collection, "allUserStreams");
                ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(collection, 10));
                for (ModelApplicationStream modelApplicationStream : collection) {
                    arrayList.add(StreamContextService.this.getForUser(modelApplicationStream.getOwnerId(), false));
                }
                return Observable.b(arrayList, AnonymousClass1.INSTANCE);
            }
        });
        m.checkNotNullExpressionValue(Y, "applicationStreamingStor…  }\n          }\n        }");
        return Y;
    }

    public final Observable<StreamContext> getForUser(final long j, final boolean z2) {
        Observable Y = this.applicationStreamingStore.observeStreamsForUser(j).Y(new b<ModelApplicationStream, Observable<? extends StreamContext>>() { // from class: com.discord.utilities.streams.StreamContextService$getForUser$1

            /* compiled from: StreamContextService.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\u0010\u0006\u001a\u0004\u0018\u00010\u00032\u000e\u0010\u0002\u001a\n\u0018\u00010\u0000j\u0004\u0018\u0001`\u0001H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/models/member/GuildMember;", "Lcom/discord/stores/ClientGuildMember;", "it", "", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/member/GuildMember;)Ljava/lang/String;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.utilities.streams.StreamContextService$getForUser$1$2  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass2<T, R> implements b<GuildMember, String> {
                public static final AnonymousClass2 INSTANCE = new AnonymousClass2();

                public final String call(GuildMember guildMember) {
                    if (guildMember != null) {
                        return guildMember.getNick();
                    }
                    return null;
                }
            }

            /* compiled from: StreamContextService.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u001a\u001a\u0004\u0018\u00010\u00172\b\u0010\u0001\u001a\u0004\u0018\u00010\u00002\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u000e\u0010\u0006\u001a\n\u0018\u00010\u0004j\u0004\u0018\u0001`\u00052\b\u0010\b\u001a\u0004\u0018\u00010\u00072\u0006\u0010\n\u001a\u00020\t2\b\u0010\f\u001a\u0004\u0018\u00010\u000b2\u0016\u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u000e\u0012\u0004\u0012\u00020\u000f0\r2\b\u0010\u0012\u001a\u0004\u0018\u00010\u00112\n\u0010\u0014\u001a\u00060\u0004j\u0002`\u00132\b\u0010\u0016\u001a\u0004\u0018\u00010\u0015H\n¢\u0006\u0004\b\u0018\u0010\u0019"}, d2 = {"Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;", "preview", "", "Lcom/discord/api/permission/PermissionBit;", "channelPermissions", "Lcom/discord/models/user/User;", "user", "Lcom/discord/models/user/MeUser;", "me", "", "userNickname", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/api/voice/state/VoiceState;", "voiceStates", "Lcom/discord/api/channel/Channel;", "channel", "Lcom/discord/primitives/ChannelId;", "selectedVoiceChannelId", "Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;", "activeStream", "Lcom/discord/utilities/streams/StreamContext;", "invoke", "(Lcom/discord/models/guild/Guild;Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;Ljava/lang/Long;Lcom/discord/models/user/User;Lcom/discord/models/user/MeUser;Ljava/lang/String;Ljava/util/Map;Lcom/discord/api/channel/Channel;JLcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;)Lcom/discord/utilities/streams/StreamContext;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.utilities.streams.StreamContextService$getForUser$1$3  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass3 extends d0.z.d.o implements Function10<Guild, StoreApplicationStreamPreviews.StreamPreview, Long, User, MeUser, String, Map<Long, ? extends VoiceState>, Channel, Long, StoreApplicationStreaming.ActiveApplicationStream, StreamContext> {
                public final /* synthetic */ ModelApplicationStream $stream;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public AnonymousClass3(ModelApplicationStream modelApplicationStream) {
                    super(10);
                    this.$stream = modelApplicationStream;
                }

                @Override // kotlin.jvm.functions.Function10
                public /* bridge */ /* synthetic */ StreamContext invoke(Guild guild, StoreApplicationStreamPreviews.StreamPreview streamPreview, Long l, User user, MeUser meUser, String str, Map<Long, ? extends VoiceState> map, Channel channel, Long l2, StoreApplicationStreaming.ActiveApplicationStream activeApplicationStream) {
                    return invoke(guild, streamPreview, l, user, meUser, str, (Map<Long, VoiceState>) map, channel, l2.longValue(), activeApplicationStream);
                }

                public final StreamContext invoke(Guild guild, StoreApplicationStreamPreviews.StreamPreview streamPreview, Long l, User user, MeUser meUser, String str, Map<Long, VoiceState> map, Channel channel, long j, StoreApplicationStreaming.ActiveApplicationStream activeApplicationStream) {
                    StreamContext.Joinability joinability;
                    m.checkNotNullParameter(meUser, "me");
                    m.checkNotNullParameter(map, "voiceStates");
                    if (((this.$stream instanceof ModelApplicationStream.GuildStream) && guild == null) || user == null) {
                        return null;
                    }
                    int B = channel != null ? channel.B() : 0;
                    boolean z2 = B > 0 && map.size() >= B;
                    boolean z3 = channel != null && channel.h() == j;
                    boolean can = PermissionUtils.can(Permission.CONNECT, l);
                    boolean can2 = PermissionUtils.can(16L, l);
                    if ((this.$stream instanceof ModelApplicationStream.GuildStream) && !can && !z3) {
                        joinability = StreamContext.Joinability.MISSING_PERMISSIONS;
                    } else if (!z2 || can2) {
                        joinability = StreamContext.Joinability.CAN_CONNECT;
                    } else {
                        joinability = StreamContext.Joinability.VOICE_CHANNEL_FULL;
                    }
                    return new StreamContext(this.$stream, guild, streamPreview, joinability, user, str, activeApplicationStream != null && activeApplicationStream.getState().isStreamActive() && m.areEqual(activeApplicationStream.getStream(), this.$stream), user.getId() == meUser.getId());
                }
            }

            public final Observable<? extends StreamContext> call(ModelApplicationStream modelApplicationStream) {
                long j2;
                StoreGuilds storeGuilds;
                StoreApplicationStreamPreviews storeApplicationStreamPreviews;
                Observable previewObservable;
                StorePermissions storePermissions;
                StoreUser storeUser;
                StoreUser storeUser2;
                StoreGuilds storeGuilds2;
                StoreVoiceStates storeVoiceStates;
                StoreChannels storeChannels;
                StoreVoiceChannelSelected storeVoiceChannelSelected;
                StoreApplicationStreaming storeApplicationStreaming;
                if (modelApplicationStream == null) {
                    return new k(null);
                }
                if (modelApplicationStream instanceof ModelApplicationStream.GuildStream) {
                    j2 = ((ModelApplicationStream.GuildStream) modelApplicationStream).getGuildId();
                } else if (modelApplicationStream instanceof ModelApplicationStream.CallStream) {
                    j2 = 0;
                } else {
                    throw new NoWhenBranchMatchedException();
                }
                storeGuilds = StreamContextService.this.guildStore;
                Observable<Guild> observeGuild = storeGuilds.observeGuild(j2);
                StreamContextService streamContextService = StreamContextService.this;
                boolean z3 = z2;
                storeApplicationStreamPreviews = streamContextService.applicationStreamPreviewStore;
                previewObservable = streamContextService.getPreviewObservable(modelApplicationStream, z3, storeApplicationStreamPreviews);
                storePermissions = StreamContextService.this.permissionsStore;
                Observable<Long> observePermissionsForChannel = storePermissions.observePermissionsForChannel(modelApplicationStream.getChannelId());
                storeUser = StreamContextService.this.userStore;
                Observable<User> observeUser = storeUser.observeUser(j);
                storeUser2 = StreamContextService.this.userStore;
                Observable observeMe$default = StoreUser.observeMe$default(storeUser2, false, 1, null);
                storeGuilds2 = StreamContextService.this.guildStore;
                Observable<R> q = storeGuilds2.observeComputed(j2).F(new b<Map<Long, ? extends GuildMember>, GuildMember>() { // from class: com.discord.utilities.streams.StreamContextService$getForUser$1.1
                    @Override // j0.k.b
                    public /* bridge */ /* synthetic */ GuildMember call(Map<Long, ? extends GuildMember> map) {
                        return call2((Map<Long, GuildMember>) map);
                    }

                    /* renamed from: call  reason: avoid collision after fix types in other method */
                    public final GuildMember call2(Map<Long, GuildMember> map) {
                        return map.get(Long.valueOf(j));
                    }
                }).F(AnonymousClass2.INSTANCE).q();
                m.checkNotNullExpressionValue(q, "guildStore\n             …  .distinctUntilChanged()");
                storeVoiceStates = StreamContextService.this.voiceStateStore;
                Observable<Map<Long, VoiceState>> observe = storeVoiceStates.observe(j2, modelApplicationStream.getChannelId());
                storeChannels = StreamContextService.this.channelStore;
                Observable<Channel> observeChannel = storeChannels.observeChannel(modelApplicationStream.getChannelId());
                storeVoiceChannelSelected = StreamContextService.this.voiceChannelSelectedStore;
                Observable<Long> observeSelectedVoiceChannelId = storeVoiceChannelSelected.observeSelectedVoiceChannelId();
                storeApplicationStreaming = StreamContextService.this.applicationStreamingStore;
                return ObservableCombineLatestOverloadsKt.combineLatest(observeGuild, previewObservable, observePermissionsForChannel, observeUser, observeMe$default, q, observe, observeChannel, observeSelectedVoiceChannelId, storeApplicationStreaming.observeActiveStream(), new AnonymousClass3(modelApplicationStream));
            }
        });
        m.checkNotNullExpressionValue(Y, "applicationStreamingStor…  }\n          }\n        }");
        return Y;
    }

    public /* synthetic */ StreamContextService(StoreApplicationStreaming storeApplicationStreaming, StoreGuilds storeGuilds, StorePermissions storePermissions, StoreUser storeUser, StoreVoiceChannelSelected storeVoiceChannelSelected, StoreVoiceStates storeVoiceStates, StoreChannels storeChannels, StoreApplicationStreamPreviews storeApplicationStreamPreviews, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? StoreStream.Companion.getApplicationStreaming() : storeApplicationStreaming, (i & 2) != 0 ? StoreStream.Companion.getGuilds() : storeGuilds, (i & 4) != 0 ? StoreStream.Companion.getPermissions() : storePermissions, (i & 8) != 0 ? StoreStream.Companion.getUsers() : storeUser, (i & 16) != 0 ? StoreStream.Companion.getVoiceChannelSelected() : storeVoiceChannelSelected, (i & 32) != 0 ? StoreStream.Companion.getVoiceStates() : storeVoiceStates, (i & 64) != 0 ? StoreStream.Companion.getChannels() : storeChannels, (i & 128) != 0 ? StoreStream.Companion.getApplicationStreamPreviews() : storeApplicationStreamPreviews);
    }
}
