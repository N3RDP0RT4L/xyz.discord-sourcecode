package com.discord.utilities.streams;

import androidx.core.app.NotificationCompat;
import com.discord.models.domain.ModelApplicationStream;
import j0.k.b;
import java.util.Collection;
import java.util.Map;
import kotlin.Metadata;
/* compiled from: StreamContextService.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0002\b\u0003\u0010\t\u001a\u0016\u0012\u0004\u0012\u00020\u0003 \u0004*\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00060\u00062.\u0010\u0005\u001a*\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003 \u0004*\u0014\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/domain/ModelApplicationStream;", "kotlin.jvm.PlatformType", "allUserStreamsMap", "", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Map;)Ljava/util/Collection;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StreamContextService$getForAllStreamingUsers$1<T, R> implements b<Map<Long, ? extends ModelApplicationStream>, Collection<? extends ModelApplicationStream>> {
    public static final StreamContextService$getForAllStreamingUsers$1 INSTANCE = new StreamContextService$getForAllStreamingUsers$1();

    public final Collection<ModelApplicationStream> call(Map<Long, ? extends ModelApplicationStream> map) {
        return map.values();
    }
}
