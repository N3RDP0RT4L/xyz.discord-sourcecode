package com.discord.utilities.spans;

import andhook.lib.HookHelper;
import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.text.Layout;
import android.text.Spanned;
import android.text.style.LeadingMarginSpan;
import androidx.core.app.NotificationCompat;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: BulletSpan.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0010\u0018\u0000 %2\u00020\u0001:\u0001%B\u001d\b\u0016\u0012\b\b\u0002\u0010\u001f\u001a\u00020\u0004\u0012\b\b\u0002\u0010 \u001a\u00020\u0004¢\u0006\u0004\b!\u0010\"B#\b\u0016\u0012\u0006\u0010\u001f\u001a\u00020\u0004\u0012\u0006\u0010#\u001a\u00020\u0004\u0012\b\b\u0002\u0010 \u001a\u00020\u0004¢\u0006\u0004\b!\u0010$J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006Jo\u0010\u0017\u001a\u00020\u00162\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u0014H\u0017¢\u0006\u0004\b\u0017\u0010\u0018R\u0016\u0010\u0019\u001a\u00020\u00048\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001b\u001a\u00020\u00048\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001aR\u0016\u0010\u001c\u001a\u00020\u00048\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001aR\u0016\u0010\u001d\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u001e¨\u0006&"}, d2 = {"Lcom/discord/utilities/spans/BulletSpan;", "Landroid/text/style/LeadingMarginSpan;", "", "first", "", "getLeadingMargin", "(Z)I", "Landroid/graphics/Canvas;", "c", "Landroid/graphics/Paint;", "p", "x", "dir", "top", "baseline", "bottom", "", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "start", "end", "Landroid/text/Layout;", "l", "", "drawLeadingMargin", "(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIILjava/lang/CharSequence;IIZLandroid/text/Layout;)V", "mColor", "I", "mBulletRadius", "mGapWidth", "mWantColor", "Z", "gapWidth", "bulletRadius", HookHelper.constructorName, "(II)V", ModelAuditLogEntry.CHANGE_KEY_COLOR, "(III)V", "Companion", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class BulletSpan implements LeadingMarginSpan {
    private static Path sBulletPath;
    private final int mBulletRadius;
    private final int mColor;
    private final int mGapWidth;
    private final boolean mWantColor;
    public static final Companion Companion = new Companion(null);
    private static final int STANDARD_GAP_WIDTH = 2;
    private static final int STANDARD_BULLET_RADIUS = 4;

    /* compiled from: BulletSpan.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rR\u001c\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086D¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u001c\u0010\u0007\u001a\u00020\u00028\u0006@\u0006X\u0086D¢\u0006\f\n\u0004\b\u0007\u0010\u0004\u001a\u0004\b\b\u0010\u0006R\u0018\u0010\n\u001a\u0004\u0018\u00010\t8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/utilities/spans/BulletSpan$Companion;", "", "", "STANDARD_BULLET_RADIUS", "I", "getSTANDARD_BULLET_RADIUS", "()I", "STANDARD_GAP_WIDTH", "getSTANDARD_GAP_WIDTH", "Landroid/graphics/Path;", "sBulletPath", "Landroid/graphics/Path;", HookHelper.constructorName, "()V", "utils_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final int getSTANDARD_BULLET_RADIUS() {
            return BulletSpan.STANDARD_BULLET_RADIUS;
        }

        public final int getSTANDARD_GAP_WIDTH() {
            return BulletSpan.STANDARD_GAP_WIDTH;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ BulletSpan(int i, int i2, int i3, DefaultConstructorMarker defaultConstructorMarker) {
        this((i3 & 1) != 0 ? STANDARD_GAP_WIDTH : i, (i3 & 2) != 0 ? STANDARD_BULLET_RADIUS : i2);
    }

    @Override // android.text.style.LeadingMarginSpan
    @SuppressLint({"NewApi"})
    public void drawLeadingMargin(Canvas canvas, Paint paint, int i, int i2, int i3, int i4, int i5, CharSequence charSequence, int i6, int i7, boolean z2, Layout layout) {
        m.checkNotNullParameter(canvas, "c");
        m.checkNotNullParameter(paint, "p");
        m.checkNotNullParameter(charSequence, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
        m.checkNotNullParameter(layout, "l");
        if (((Spanned) charSequence).getSpanStart(this) == i6) {
            Paint.Style style = paint.getStyle();
            int i8 = 0;
            if (this.mWantColor) {
                i8 = paint.getColor();
                paint.setColor(this.mColor);
            }
            paint.setStyle(Paint.Style.FILL);
            if (canvas.isHardwareAccelerated()) {
                if (sBulletPath == null) {
                    Path path = new Path();
                    sBulletPath = path;
                    m.checkNotNull(path);
                    path.addCircle(0.0f, 0.0f, this.mBulletRadius * 1.2f, Path.Direction.CW);
                }
                canvas.save();
                canvas.translate((i2 * 1.2f * this.mBulletRadius) + i, (i3 + i5) / 2.0f);
                Path path2 = sBulletPath;
                m.checkNotNull(path2);
                canvas.drawPath(path2, paint);
                canvas.restore();
            } else {
                int i9 = this.mBulletRadius;
                canvas.drawCircle((i2 * i9) + i, (i3 + i5) / 2.0f, i9, paint);
            }
            if (this.mWantColor) {
                paint.setColor(i8);
            }
            paint.setStyle(style);
        }
    }

    @Override // android.text.style.LeadingMarginSpan
    public int getLeadingMargin(boolean z2) {
        return (this.mBulletRadius * 2) + this.mGapWidth;
    }

    public BulletSpan(int i, int i2) {
        this.mGapWidth = i;
        this.mBulletRadius = i2;
        this.mWantColor = false;
        this.mColor = 0;
    }

    public /* synthetic */ BulletSpan(int i, int i2, int i3, int i4, DefaultConstructorMarker defaultConstructorMarker) {
        this(i, i2, (i4 & 4) != 0 ? STANDARD_BULLET_RADIUS : i3);
    }

    public BulletSpan(int i, int i2, int i3) {
        this.mGapWidth = i;
        this.mBulletRadius = i3;
        this.mWantColor = true;
        this.mColor = i2;
    }
}
