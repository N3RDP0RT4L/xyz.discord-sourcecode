package com.discord.utilities.spans;

import andhook.lib.HookHelper;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.text.Layout;
import android.text.Spanned;
import android.text.style.LeadingMarginSpan;
import androidx.annotation.ColorInt;
import androidx.annotation.IntRange;
import androidx.annotation.Px;
import androidx.core.app.NotificationCompat;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: QuoteSpan.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001B%\u0012\b\b\u0001\u0010%\u001a\u00020\u000b\u0012\b\b\u0001\u0010'\u001a\u00020\u000b\u0012\b\b\u0001\u0010\u001b\u001a\u00020\u000b¢\u0006\u0004\b)\u0010*J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\f\u001a\u00020\u000b2\u0006\u0010\n\u001a\u00020\tH\u0016¢\u0006\u0004\b\f\u0010\rJo\u0010\u0019\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\u000b2\u0006\u0010\u0010\u001a\u00020\u000b2\u0006\u0010\u0011\u001a\u00020\u000b2\u0006\u0010\u0012\u001a\u00020\u000b2\u0006\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u000b2\u0006\u0010\u0016\u001a\u00020\u000b2\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u0018\u001a\u00020\u0017H\u0016¢\u0006\u0004\b\u0019\u0010\u001aR\u0019\u0010\u001b\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u0016\u0010 \u001a\u00020\u001f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b \u0010!R\u0016\u0010#\u001a\u00020\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$R\u0019\u0010%\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u001c\u001a\u0004\b&\u0010\u001eR\u0019\u0010'\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010\u001c\u001a\u0004\b(\u0010\u001e¨\u0006+"}, d2 = {"Lcom/discord/utilities/spans/QuoteSpan;", "Landroid/text/style/LeadingMarginSpan;", "Landroid/graphics/Canvas;", "c", "Landroid/graphics/Paint;", "p", "", "draw", "(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V", "", "first", "", "getLeadingMargin", "(Z)I", "x", "dir", "top", "baseline", "bottom", "", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "start", "end", "Landroid/text/Layout;", "layout", "drawLeadingMargin", "(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIILjava/lang/CharSequence;IIZLandroid/text/Layout;)V", "gapWidth", "I", "getGapWidth", "()I", "", "radius", "F", "Landroid/graphics/RectF;", "rect", "Landroid/graphics/RectF;", "stripeColor", "getStripeColor", "stripeWidth", "getStripeWidth", HookHelper.constructorName, "(III)V", "utils_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class QuoteSpan implements LeadingMarginSpan {
    private final int gapWidth;
    private final float radius;
    private final RectF rect = new RectF();
    private final int stripeColor;
    private final int stripeWidth;

    public QuoteSpan(@ColorInt int i, @IntRange(from = 0) @Px int i2, @IntRange(from = 0) @Px int i3) {
        this.stripeColor = i;
        this.stripeWidth = i2;
        this.gapWidth = i3;
        this.radius = i2;
    }

    private final void draw(Canvas canvas, Paint paint) {
        Paint.Style style = paint.getStyle();
        int color = paint.getColor();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(this.stripeColor);
        RectF rectF = this.rect;
        float f = this.radius;
        canvas.drawRoundRect(rectF, f, f, paint);
        paint.setStyle(style);
        paint.setColor(color);
    }

    @Override // android.text.style.LeadingMarginSpan
    public void drawLeadingMargin(Canvas canvas, Paint paint, int i, int i2, int i3, int i4, int i5, CharSequence charSequence, int i6, int i7, boolean z2, Layout layout) {
        m.checkNotNullParameter(canvas, "c");
        m.checkNotNullParameter(paint, "p");
        m.checkNotNullParameter(charSequence, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
        m.checkNotNullParameter(layout, "layout");
        if (charSequence instanceof Spanned) {
            Spanned spanned = (Spanned) charSequence;
            if (spanned.getSpanStart(this) == i6) {
                RectF rectF = this.rect;
                rectF.left = i;
                rectF.right = (i2 * this.stripeWidth) + i;
                rectF.top = i3;
            }
            int spanEnd = spanned.getSpanEnd(this);
            if (spanEnd == i7 || (spanEnd - 1 == i7 && charSequence.charAt(i7) == '\n')) {
                this.rect.bottom = i5;
                draw(canvas, paint);
            }
        }
    }

    public final int getGapWidth() {
        return this.gapWidth;
    }

    @Override // android.text.style.LeadingMarginSpan
    public int getLeadingMargin(boolean z2) {
        return this.stripeWidth + this.gapWidth;
    }

    public final int getStripeColor() {
        return this.stripeColor;
    }

    public final int getStripeWidth() {
        return this.stripeWidth;
    }
}
