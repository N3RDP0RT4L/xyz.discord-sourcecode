package com.discord.utilities.color;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.ColorInt;
import androidx.core.graphics.drawable.DrawableCompat;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.t.k;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ColorCompat.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a\u0019\u0010\u0004\u001a\u00020\u0003*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0001¢\u0006\u0004\b\u0004\u0010\u0005\u001a\u0019\u0010\u0007\u001a\u00020\u0003*\u00020\u00002\u0006\u0010\u0006\u001a\u00020\u0001¢\u0006\u0004\b\u0007\u0010\u0005\u001a%\u0010\f\u001a\u00020\u0003*\u00020\b2\b\b\u0001\u0010\t\u001a\u00020\u00012\b\b\u0002\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\f\u0010\r\u001a\u001b\u0010\u000f\u001a\u00020\u0003*\u00020\u000e2\b\b\u0001\u0010\u0002\u001a\u00020\u0001¢\u0006\u0004\b\u000f\u0010\u0010¨\u0006\u0011"}, d2 = {"Landroid/widget/ImageView;", "", ModelAuditLogEntry.CHANGE_KEY_COLOR, "", "tintWithColor", "(Landroid/widget/ImageView;I)V", "colorResId", "tintWithColorResource", "Landroid/graphics/drawable/Drawable;", "tint", "", "isGlobalTint", "setTint", "(Landroid/graphics/drawable/Drawable;IZ)V", "Landroid/widget/TextView;", "setDrawableColor", "(Landroid/widget/TextView;I)V", "utils_release"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ColorCompatKt {
    public static final void setDrawableColor(TextView textView, @ColorInt int i) {
        m.checkNotNullParameter(textView, "$this$setDrawableColor");
        Drawable[] compoundDrawables = textView.getCompoundDrawables();
        m.checkNotNullExpressionValue(compoundDrawables, "compoundDrawables");
        for (Drawable drawable : k.filterNotNull(compoundDrawables)) {
            drawable.setColorFilter(new PorterDuffColorFilter(i, PorterDuff.Mode.SRC_IN));
        }
    }

    public static final void setTint(Drawable drawable, @ColorInt int i, boolean z2) {
        m.checkNotNullParameter(drawable, "$this$setTint");
        if (!z2) {
            drawable.mutate();
        }
        DrawableCompat.setTint(drawable, i);
    }

    public static /* synthetic */ void setTint$default(Drawable drawable, int i, boolean z2, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z2 = false;
        }
        setTint(drawable, i, z2);
    }

    public static final void tintWithColor(ImageView imageView, int i) {
        m.checkNotNullParameter(imageView, "$this$tintWithColor");
        imageView.setImageTintList(ColorStateList.valueOf(i));
    }

    public static final void tintWithColorResource(ImageView imageView, int i) {
        m.checkNotNullParameter(imageView, "$this$tintWithColorResource");
        tintWithColor(imageView, ColorCompat.getColor(imageView, i));
    }
}
