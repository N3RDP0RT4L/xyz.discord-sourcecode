package com.discord.utilities.lazy.memberlist;

import a0.a.a.b;
import andhook.lib.HookHelper;
import androidx.annotation.ColorInt;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.presence.Presence;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: MemberListRow.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\t\n\u000bB\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u001c\u0010\u0003\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006\u0082\u0001\u0003\f\r\u000e¨\u0006\u000f"}, d2 = {"Lcom/discord/utilities/lazy/memberlist/MemberListRow;", "", "", "rowId", "Ljava/lang/String;", "getRowId", "()Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "Member", "RoleHeader", "StatusHeader", "Lcom/discord/utilities/lazy/memberlist/MemberListRow$RoleHeader;", "Lcom/discord/utilities/lazy/memberlist/MemberListRow$StatusHeader;", "Lcom/discord/utilities/lazy/memberlist/MemberListRow$Member;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class MemberListRow {
    private final String rowId;

    /* compiled from: MemberListRow.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0019\n\u0002\u0010\u0000\n\u0002\b\u0016\b\u0086\b\u0018\u00002\u00020\u0001Bw\u0012\n\u0010\u001a\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\u001b\u001a\u00020\u0006\u0012\u0006\u0010\u001c\u001a\u00020\t\u0012\b\u0010\u001d\u001a\u0004\u0018\u00010\f\u0012\u0006\u0010\u001e\u001a\u00020\t\u0012\b\u0010\u001f\u001a\u0004\u0018\u00010\u0010\u0012\n\b\u0001\u0010 \u001a\u0004\u0018\u00010\f\u0012\b\u0010!\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\"\u001a\u00020\t\u0012\b\u0010#\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010$\u001a\u00020\t\u0012\u0006\u0010%\u001a\u00020\f¢\u0006\u0004\b>\u0010?J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0012\u0010\r\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u000bJ\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0012\u0010\u0013\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\u0013\u0010\u000eJ\u0012\u0010\u0014\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0014\u0010\bJ\u0010\u0010\u0015\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\u0015\u0010\u000bJ\u0012\u0010\u0016\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0016\u0010\bJ\u0010\u0010\u0017\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\u0017\u0010\u000bJ\u0010\u0010\u0018\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0019J\u0096\u0001\u0010&\u001a\u00020\u00002\f\b\u0002\u0010\u001a\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u001b\u001a\u00020\u00062\b\b\u0002\u0010\u001c\u001a\u00020\t2\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\f2\b\b\u0002\u0010\u001e\u001a\u00020\t2\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u00102\n\b\u0003\u0010 \u001a\u0004\u0018\u00010\f2\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010\"\u001a\u00020\t2\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010$\u001a\u00020\t2\b\b\u0002\u0010%\u001a\u00020\fHÆ\u0001¢\u0006\u0004\b&\u0010'J\u0010\u0010(\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b(\u0010\bJ\u0010\u0010)\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b)\u0010\u0019J\u001a\u0010,\u001a\u00020\t2\b\u0010+\u001a\u0004\u0018\u00010*HÖ\u0003¢\u0006\u0004\b,\u0010-R\u0019\u0010%\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010.\u001a\u0004\b/\u0010\u0019R\u001d\u0010\u001a\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u00100\u001a\u0004\b1\u0010\u0005R\u001b\u0010!\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b!\u00102\u001a\u0004\b3\u0010\bR\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00104\u001a\u0004\b5\u0010\u0012R\u0019\u0010\u001c\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u00106\u001a\u0004\b\u001c\u0010\u000bR\u001b\u0010#\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b#\u00102\u001a\u0004\b7\u0010\bR\u0019\u0010\u001b\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u00102\u001a\u0004\b8\u0010\bR\u001b\u0010 \u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b \u00109\u001a\u0004\b:\u0010\u000eR\u0019\u0010\"\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00106\u001a\u0004\b;\u0010\u000bR\u0019\u0010\u001e\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00106\u001a\u0004\b<\u0010\u000bR\u001b\u0010\u001d\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00109\u001a\u0004\b=\u0010\u000eR\u0019\u0010$\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b$\u00106\u001a\u0004\b$\u0010\u000b¨\u0006@"}, d2 = {"Lcom/discord/utilities/lazy/memberlist/MemberListRow$Member;", "Lcom/discord/utilities/lazy/memberlist/MemberListRow;", "", "Lcom/discord/primitives/UserId;", "component1", "()J", "", "component2", "()Ljava/lang/String;", "", "component3", "()Z", "", "component4", "()Ljava/lang/Integer;", "component5", "Lcom/discord/models/presence/Presence;", "component6", "()Lcom/discord/models/presence/Presence;", "component7", "component8", "component9", "component10", "component11", "component12", "()I", "userId", ModelAuditLogEntry.CHANGE_KEY_NAME, "isBot", "tagText", "tagVerified", "presence", ModelAuditLogEntry.CHANGE_KEY_COLOR, "avatarUrl", "showOwnerIndicator", "premiumSince", "isApplicationStreaming", "userFlags", "copy", "(JLjava/lang/String;ZLjava/lang/Integer;ZLcom/discord/models/presence/Presence;Ljava/lang/Integer;Ljava/lang/String;ZLjava/lang/String;ZI)Lcom/discord/utilities/lazy/memberlist/MemberListRow$Member;", "toString", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "I", "getUserFlags", "J", "getUserId", "Ljava/lang/String;", "getAvatarUrl", "Lcom/discord/models/presence/Presence;", "getPresence", "Z", "getPremiumSince", "getName", "Ljava/lang/Integer;", "getColor", "getShowOwnerIndicator", "getTagVerified", "getTagText", HookHelper.constructorName, "(JLjava/lang/String;ZLjava/lang/Integer;ZLcom/discord/models/presence/Presence;Ljava/lang/Integer;Ljava/lang/String;ZLjava/lang/String;ZI)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Member extends MemberListRow {
        private final String avatarUrl;
        private final Integer color;
        private final boolean isApplicationStreaming;
        private final boolean isBot;
        private final String name;
        private final String premiumSince;
        private final Presence presence;
        private final boolean showOwnerIndicator;
        private final Integer tagText;
        private final boolean tagVerified;
        private final int userFlags;
        private final long userId;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public Member(long j, String str, boolean z2, Integer num, boolean z3, Presence presence, @ColorInt Integer num2, String str2, boolean z4, String str3, boolean z5, int i) {
            super(String.valueOf(j), null);
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            this.userId = j;
            this.name = str;
            this.isBot = z2;
            this.tagText = num;
            this.tagVerified = z3;
            this.presence = presence;
            this.color = num2;
            this.avatarUrl = str2;
            this.showOwnerIndicator = z4;
            this.premiumSince = str3;
            this.isApplicationStreaming = z5;
            this.userFlags = i;
        }

        public final long component1() {
            return this.userId;
        }

        public final String component10() {
            return this.premiumSince;
        }

        public final boolean component11() {
            return this.isApplicationStreaming;
        }

        public final int component12() {
            return this.userFlags;
        }

        public final String component2() {
            return this.name;
        }

        public final boolean component3() {
            return this.isBot;
        }

        public final Integer component4() {
            return this.tagText;
        }

        public final boolean component5() {
            return this.tagVerified;
        }

        public final Presence component6() {
            return this.presence;
        }

        public final Integer component7() {
            return this.color;
        }

        public final String component8() {
            return this.avatarUrl;
        }

        public final boolean component9() {
            return this.showOwnerIndicator;
        }

        public final Member copy(long j, String str, boolean z2, Integer num, boolean z3, Presence presence, @ColorInt Integer num2, String str2, boolean z4, String str3, boolean z5, int i) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            return new Member(j, str, z2, num, z3, presence, num2, str2, z4, str3, z5, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Member)) {
                return false;
            }
            Member member = (Member) obj;
            return this.userId == member.userId && m.areEqual(this.name, member.name) && this.isBot == member.isBot && m.areEqual(this.tagText, member.tagText) && this.tagVerified == member.tagVerified && m.areEqual(this.presence, member.presence) && m.areEqual(this.color, member.color) && m.areEqual(this.avatarUrl, member.avatarUrl) && this.showOwnerIndicator == member.showOwnerIndicator && m.areEqual(this.premiumSince, member.premiumSince) && this.isApplicationStreaming == member.isApplicationStreaming && this.userFlags == member.userFlags;
        }

        public final String getAvatarUrl() {
            return this.avatarUrl;
        }

        public final Integer getColor() {
            return this.color;
        }

        public final String getName() {
            return this.name;
        }

        public final String getPremiumSince() {
            return this.premiumSince;
        }

        public final Presence getPresence() {
            return this.presence;
        }

        public final boolean getShowOwnerIndicator() {
            return this.showOwnerIndicator;
        }

        public final Integer getTagText() {
            return this.tagText;
        }

        public final boolean getTagVerified() {
            return this.tagVerified;
        }

        public final int getUserFlags() {
            return this.userFlags;
        }

        public final long getUserId() {
            return this.userId;
        }

        public int hashCode() {
            int a = b.a(this.userId) * 31;
            String str = this.name;
            int i = 0;
            int hashCode = (a + (str != null ? str.hashCode() : 0)) * 31;
            boolean z2 = this.isBot;
            int i2 = 1;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            int i5 = (hashCode + i3) * 31;
            Integer num = this.tagText;
            int hashCode2 = (i5 + (num != null ? num.hashCode() : 0)) * 31;
            boolean z3 = this.tagVerified;
            if (z3) {
                z3 = true;
            }
            int i6 = z3 ? 1 : 0;
            int i7 = z3 ? 1 : 0;
            int i8 = (hashCode2 + i6) * 31;
            Presence presence = this.presence;
            int hashCode3 = (i8 + (presence != null ? presence.hashCode() : 0)) * 31;
            Integer num2 = this.color;
            int hashCode4 = (hashCode3 + (num2 != null ? num2.hashCode() : 0)) * 31;
            String str2 = this.avatarUrl;
            int hashCode5 = (hashCode4 + (str2 != null ? str2.hashCode() : 0)) * 31;
            boolean z4 = this.showOwnerIndicator;
            if (z4) {
                z4 = true;
            }
            int i9 = z4 ? 1 : 0;
            int i10 = z4 ? 1 : 0;
            int i11 = (hashCode5 + i9) * 31;
            String str3 = this.premiumSince;
            if (str3 != null) {
                i = str3.hashCode();
            }
            int i12 = (i11 + i) * 31;
            boolean z5 = this.isApplicationStreaming;
            if (!z5) {
                i2 = z5 ? 1 : 0;
            }
            return ((i12 + i2) * 31) + this.userFlags;
        }

        public final boolean isApplicationStreaming() {
            return this.isApplicationStreaming;
        }

        public final boolean isBot() {
            return this.isBot;
        }

        public String toString() {
            StringBuilder R = a.R("Member(userId=");
            R.append(this.userId);
            R.append(", name=");
            R.append(this.name);
            R.append(", isBot=");
            R.append(this.isBot);
            R.append(", tagText=");
            R.append(this.tagText);
            R.append(", tagVerified=");
            R.append(this.tagVerified);
            R.append(", presence=");
            R.append(this.presence);
            R.append(", color=");
            R.append(this.color);
            R.append(", avatarUrl=");
            R.append(this.avatarUrl);
            R.append(", showOwnerIndicator=");
            R.append(this.showOwnerIndicator);
            R.append(", premiumSince=");
            R.append(this.premiumSince);
            R.append(", isApplicationStreaming=");
            R.append(this.isApplicationStreaming);
            R.append(", userFlags=");
            return a.A(R, this.userFlags, ")");
        }
    }

    /* compiled from: MemberListRow.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B#\u0012\n\u0010\f\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\r\u001a\u00020\u0006\u0012\u0006\u0010\u000e\u001a\u00020\t¢\u0006\u0004\b\u001e\u0010\u001fJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ2\u0010\u000f\u001a\u00020\u00002\f\b\u0002\u0010\f\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\r\u001a\u00020\u00062\b\b\u0002\u0010\u000e\u001a\u00020\tHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0011\u0010\bJ\u0010\u0010\u0012\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\u0012\u0010\u000bJ\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u0019\u0010\u000e\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u0018\u001a\u0004\b\u0019\u0010\u000bR\u0019\u0010\r\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001a\u001a\u0004\b\u001b\u0010\bR\u001d\u0010\f\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001c\u001a\u0004\b\u001d\u0010\u0005¨\u0006 "}, d2 = {"Lcom/discord/utilities/lazy/memberlist/MemberListRow$RoleHeader;", "Lcom/discord/utilities/lazy/memberlist/MemberListRow;", "", "Lcom/discord/primitives/RoleId;", "component1", "()J", "", "component2", "()Ljava/lang/String;", "", "component3", "()I", "roleId", "roleName", "memberCount", "copy", "(JLjava/lang/String;I)Lcom/discord/utilities/lazy/memberlist/MemberListRow$RoleHeader;", "toString", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getMemberCount", "Ljava/lang/String;", "getRoleName", "J", "getRoleId", HookHelper.constructorName, "(JLjava/lang/String;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class RoleHeader extends MemberListRow {
        private final int memberCount;
        private final long roleId;
        private final String roleName;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public RoleHeader(long j, String str, int i) {
            super(String.valueOf(j), null);
            m.checkNotNullParameter(str, "roleName");
            this.roleId = j;
            this.roleName = str;
            this.memberCount = i;
        }

        public static /* synthetic */ RoleHeader copy$default(RoleHeader roleHeader, long j, String str, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                j = roleHeader.roleId;
            }
            if ((i2 & 2) != 0) {
                str = roleHeader.roleName;
            }
            if ((i2 & 4) != 0) {
                i = roleHeader.memberCount;
            }
            return roleHeader.copy(j, str, i);
        }

        public final long component1() {
            return this.roleId;
        }

        public final String component2() {
            return this.roleName;
        }

        public final int component3() {
            return this.memberCount;
        }

        public final RoleHeader copy(long j, String str, int i) {
            m.checkNotNullParameter(str, "roleName");
            return new RoleHeader(j, str, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof RoleHeader)) {
                return false;
            }
            RoleHeader roleHeader = (RoleHeader) obj;
            return this.roleId == roleHeader.roleId && m.areEqual(this.roleName, roleHeader.roleName) && this.memberCount == roleHeader.memberCount;
        }

        public final int getMemberCount() {
            return this.memberCount;
        }

        public final long getRoleId() {
            return this.roleId;
        }

        public final String getRoleName() {
            return this.roleName;
        }

        public int hashCode() {
            int a = b.a(this.roleId) * 31;
            String str = this.roleName;
            return ((a + (str != null ? str.hashCode() : 0)) * 31) + this.memberCount;
        }

        public String toString() {
            StringBuilder R = a.R("RoleHeader(roleId=");
            R.append(this.roleId);
            R.append(", roleName=");
            R.append(this.roleName);
            R.append(", memberCount=");
            return a.A(R, this.memberCount, ")");
        }
    }

    /* compiled from: MemberListRow.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u001fB\u001f\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\b¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ.\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\u00052\b\b\u0002\u0010\r\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0004J\u0010\u0010\u0011\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u0011\u0010\nJ\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u0019\u0010\r\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u0017\u001a\u0004\b\u0018\u0010\nR\u0019\u0010\f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0019\u001a\u0004\b\u001a\u0010\u0007R\u001c\u0010\u000b\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u000b\u0010\u001b\u001a\u0004\b\u001c\u0010\u0004¨\u0006 "}, d2 = {"Lcom/discord/utilities/lazy/memberlist/MemberListRow$StatusHeader;", "Lcom/discord/utilities/lazy/memberlist/MemberListRow;", "", "component1", "()Ljava/lang/String;", "Lcom/discord/utilities/lazy/memberlist/MemberListRow$StatusHeader$Type;", "component2", "()Lcom/discord/utilities/lazy/memberlist/MemberListRow$StatusHeader$Type;", "", "component3", "()I", "rowId", "type", "memberCount", "copy", "(Ljava/lang/String;Lcom/discord/utilities/lazy/memberlist/MemberListRow$StatusHeader$Type;I)Lcom/discord/utilities/lazy/memberlist/MemberListRow$StatusHeader;", "toString", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getMemberCount", "Lcom/discord/utilities/lazy/memberlist/MemberListRow$StatusHeader$Type;", "getType", "Ljava/lang/String;", "getRowId", HookHelper.constructorName, "(Ljava/lang/String;Lcom/discord/utilities/lazy/memberlist/MemberListRow$StatusHeader$Type;I)V", "Type", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StatusHeader extends MemberListRow {
        private final int memberCount;
        private final String rowId;
        private final Type type;

        /* compiled from: MemberListRow.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\t\n\u0002\b\t\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\tj\u0002\b\n¨\u0006\u000b"}, d2 = {"Lcom/discord/utilities/lazy/memberlist/MemberListRow$StatusHeader$Type;", "", "", ModelAuditLogEntry.CHANGE_KEY_ID, "J", "getId", "()J", HookHelper.constructorName, "(Ljava/lang/String;IJ)V", "ONLINE", "OFFLINE", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public enum Type {
            ONLINE(0),
            OFFLINE(1);
            

            /* renamed from: id  reason: collision with root package name */
            private final long f2792id;

            Type(long j) {
                this.f2792id = j;
            }

            public final long getId() {
                return this.f2792id;
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public StatusHeader(String str, Type type, int i) {
            super(str, null);
            m.checkNotNullParameter(str, "rowId");
            m.checkNotNullParameter(type, "type");
            this.rowId = str;
            this.type = type;
            this.memberCount = i;
        }

        public static /* synthetic */ StatusHeader copy$default(StatusHeader statusHeader, String str, Type type, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                str = statusHeader.getRowId();
            }
            if ((i2 & 2) != 0) {
                type = statusHeader.type;
            }
            if ((i2 & 4) != 0) {
                i = statusHeader.memberCount;
            }
            return statusHeader.copy(str, type, i);
        }

        public final String component1() {
            return getRowId();
        }

        public final Type component2() {
            return this.type;
        }

        public final int component3() {
            return this.memberCount;
        }

        public final StatusHeader copy(String str, Type type, int i) {
            m.checkNotNullParameter(str, "rowId");
            m.checkNotNullParameter(type, "type");
            return new StatusHeader(str, type, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StatusHeader)) {
                return false;
            }
            StatusHeader statusHeader = (StatusHeader) obj;
            return m.areEqual(getRowId(), statusHeader.getRowId()) && m.areEqual(this.type, statusHeader.type) && this.memberCount == statusHeader.memberCount;
        }

        public final int getMemberCount() {
            return this.memberCount;
        }

        @Override // com.discord.utilities.lazy.memberlist.MemberListRow
        public String getRowId() {
            return this.rowId;
        }

        public final Type getType() {
            return this.type;
        }

        public int hashCode() {
            String rowId = getRowId();
            int i = 0;
            int hashCode = (rowId != null ? rowId.hashCode() : 0) * 31;
            Type type = this.type;
            if (type != null) {
                i = type.hashCode();
            }
            return ((hashCode + i) * 31) + this.memberCount;
        }

        public String toString() {
            StringBuilder R = a.R("StatusHeader(rowId=");
            R.append(getRowId());
            R.append(", type=");
            R.append(this.type);
            R.append(", memberCount=");
            return a.A(R, this.memberCount, ")");
        }
    }

    private MemberListRow(String str) {
        this.rowId = str;
    }

    public String getRowId() {
        return this.rowId;
    }

    public /* synthetic */ MemberListRow(String str, DefaultConstructorMarker defaultConstructorMarker) {
        this(str);
    }
}
