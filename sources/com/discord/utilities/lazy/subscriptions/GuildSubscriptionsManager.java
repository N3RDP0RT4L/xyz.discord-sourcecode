package com.discord.utilities.lazy.subscriptions;

import andhook.lib.HookHelper;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function2;
import kotlin.ranges.IntRange;
/* compiled from: GuildSubscriptionsManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0086\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0018\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B%\u0012\u001c\u0010<\u001a\u0018\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\n0;¢\u0006\u0004\bK\u0010LJ9\u0010\u000b\u001a\u00020\n2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u001c\u0010\t\u001a\u0018\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0005H\u0002¢\u0006\u0004\b\u000b\u0010\fJ5\u0010\u0012\u001a\u00020\n2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0010\u0010\u000f\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u000e0\r2\u0006\u0010\u0011\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u0012\u0010\u0013J-\u0010\u0015\u001a\u00020\n2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0010\u0010\u0014\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00060\u0007H\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u001d\u0010\u0019\u001a\u00020\n2\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\n0\u0017H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u0019\u0010\u001c\u001a\u00020\u001b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u001c\u0010\u001dJ%\u0010\u001f\u001a\u00020\n2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u001e\u001a\u00060\u0002j\u0002`\u000e¢\u0006\u0004\b\u001f\u0010 J%\u0010!\u001a\u00020\n2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u001e\u001a\u00060\u0002j\u0002`\u000e¢\u0006\u0004\b!\u0010 J3\u0010$\u001a\u00020\n2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\"\u001a\u00060\u0002j\u0002`\u00062\f\u0010#\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\u0004\b$\u0010%J%\u0010'\u001a\u00020\n2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010&\u001a\u00060\u0002j\u0002`\u0006¢\u0006\u0004\b'\u0010 J\u0019\u0010(\u001a\u00020\n2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b(\u0010)J\u0019\u0010*\u001a\u00020\n2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b*\u0010)J\u0019\u0010+\u001a\u00020\n2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b+\u0010)J\u0019\u0010,\u001a\u00020\n2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b,\u0010)J\r\u0010-\u001a\u00020\n¢\u0006\u0004\b-\u0010.J\u001f\u00100\u001a\u00020\n2\u0010\u0010/\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\u0007¢\u0006\u0004\b0\u00101J\r\u00102\u001a\u00020\n¢\u0006\u0004\b2\u0010.J\r\u00103\u001a\u00020\n¢\u0006\u0004\b3\u0010.R\u0016\u00105\u001a\u0002048\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b5\u00106R:\u00109\u001a&\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u001b07j\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u001b`88\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b9\u0010:R,\u0010<\u001a\u0018\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\n0;8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b<\u0010=R.\u0010@\u001a\u001a\u0012\b\u0012\u00060\u0002j\u0002`\u00030>j\f\u0012\b\u0012\u00060\u0002j\u0002`\u0003`?8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b@\u0010AR.\u0010B\u001a\u001a\u0012\b\u0012\u00060\u0002j\u0002`\u00030>j\f\u0012\b\u0012\u00060\u0002j\u0002`\u0003`?8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bB\u0010AR\u0016\u0010D\u001a\u00020C8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bD\u0010ER\u0016\u0010G\u001a\u00020F8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bG\u0010HR.\u0010I\u001a\u001a\u0012\b\u0012\u00060\u0002j\u0002`\u00030>j\f\u0012\b\u0012\u00060\u0002j\u0002`\u0003`?8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bI\u0010AR.\u0010J\u001a\u001a\u0012\b\u0012\u00060\u0002j\u0002`\u00030>j\f\u0012\b\u0012\u00060\u0002j\u0002`\u0003`?8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bJ\u0010A¨\u0006M"}, d2 = {"Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "Lcom/discord/primitives/ChannelId;", "", "Lkotlin/ranges/IntRange;", "channelSubscriptions", "", "handleChannelSubscriptionsChange", "(JLjava/util/Map;)V", "", "Lcom/discord/primitives/UserId;", "users", "", "forceUpdate", "handleMemberSubscriptionsChange", "(JLjava/util/Set;Z)V", "threadSubscriptions", "handleThreadSubscriptionsChange", "(JLjava/util/List;)V", "Lkotlin/Function0;", "flushUnsubscriptions", "requestFlushUnsubscriptions", "(Lkotlin/jvm/functions/Function0;)V", "Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;", "get", "(J)Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;", "userId", "subscribeMember", "(JJ)V", "unsubscribeMember", "channelId", "ranges", "subscribeChannel", "(JJLjava/util/List;)V", "threadId", "subscribeThread", "subscribeTyping", "(J)V", "subscribeActivities", "subscribeThreads", "remove", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_RESET, "()V", "guildIds", "retainAll", "(Ljava/util/List;)V", "flush", "queueExistingSubscriptions", "Lcom/discord/utilities/lazy/subscriptions/GuildMemberSubscriptionsManager;", "guildMemberSubscriptionsManager", "Lcom/discord/utilities/lazy/subscriptions/GuildMemberSubscriptionsManager;", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "pendingEmissions", "Ljava/util/HashMap;", "Lkotlin/Function2;", "onChange", "Lkotlin/jvm/functions/Function2;", "Ljava/util/HashSet;", "Lkotlin/collections/HashSet;", "subscribedGuilds", "Ljava/util/HashSet;", "activityGuilds", "Lcom/discord/utilities/lazy/subscriptions/GuildThreadSubscriptionsManager;", "guildThreadSubscriptionManager", "Lcom/discord/utilities/lazy/subscriptions/GuildThreadSubscriptionsManager;", "Lcom/discord/utilities/lazy/subscriptions/GuildChannelSubscriptionsManager;", "guildChannelSubscriptionsManager", "Lcom/discord/utilities/lazy/subscriptions/GuildChannelSubscriptionsManager;", "typingGuilds", "threadGuilds", HookHelper.constructorName, "(Lkotlin/jvm/functions/Function2;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildSubscriptionsManager {
    private final Function2<Long, GuildSubscriptions, Unit> onChange;
    private final HashSet<Long> typingGuilds = new HashSet<>();
    private final HashSet<Long> activityGuilds = new HashSet<>();
    private final HashSet<Long> threadGuilds = new HashSet<>();
    private final GuildChannelSubscriptionsManager guildChannelSubscriptionsManager = new GuildChannelSubscriptionsManager(new GuildSubscriptionsManager$guildChannelSubscriptionsManager$1(this));
    private final GuildMemberSubscriptionsManager guildMemberSubscriptionsManager = new GuildMemberSubscriptionsManager(new GuildSubscriptionsManager$guildMemberSubscriptionsManager$1(this), new GuildSubscriptionsManager$guildMemberSubscriptionsManager$2(this), null, 4, null);
    private final GuildThreadSubscriptionsManager guildThreadSubscriptionManager = new GuildThreadSubscriptionsManager(new GuildSubscriptionsManager$guildThreadSubscriptionManager$1(this));
    private final HashMap<Long, GuildSubscriptions> pendingEmissions = new HashMap<>();
    private final HashSet<Long> subscribedGuilds = new HashSet<>();

    /* JADX WARN: Multi-variable type inference failed */
    public GuildSubscriptionsManager(Function2<? super Long, ? super GuildSubscriptions, Unit> function2) {
        m.checkNotNullParameter(function2, "onChange");
        this.onChange = function2;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void handleChannelSubscriptionsChange(long j, Map<Long, ? extends List<IntRange>> map) {
        GuildSubscriptions guildSubscriptions;
        HashMap<Long, GuildSubscriptions> hashMap = this.pendingEmissions;
        Long valueOf = Long.valueOf(j);
        GuildSubscriptions guildSubscriptions2 = this.pendingEmissions.get(Long.valueOf(j));
        if (guildSubscriptions2 == null || (guildSubscriptions = GuildSubscriptions.copy$default(guildSubscriptions2, map, null, null, null, null, null, 62, null)) == null) {
            guildSubscriptions = new GuildSubscriptions(map, null, null, null, null, null, 62, null);
        }
        hashMap.put(valueOf, guildSubscriptions);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void handleMemberSubscriptionsChange(long j, Set<Long> set, boolean z2) {
        GuildSubscriptions guildSubscriptions;
        HashMap<Long, GuildSubscriptions> hashMap = this.pendingEmissions;
        Long valueOf = Long.valueOf(j);
        GuildSubscriptions guildSubscriptions2 = this.pendingEmissions.get(Long.valueOf(j));
        if (guildSubscriptions2 == null || (guildSubscriptions = GuildSubscriptions.copy$default(guildSubscriptions2, null, null, null, set, null, null, 55, null)) == null) {
            guildSubscriptions = new GuildSubscriptions(null, null, null, set, null, null, 55, null);
        }
        hashMap.put(valueOf, guildSubscriptions);
        if (z2) {
            flush();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void handleThreadSubscriptionsChange(long j, List<Long> list) {
        GuildSubscriptions guildSubscriptions;
        HashMap<Long, GuildSubscriptions> hashMap = this.pendingEmissions;
        Long valueOf = Long.valueOf(j);
        GuildSubscriptions guildSubscriptions2 = this.pendingEmissions.get(Long.valueOf(j));
        if (guildSubscriptions2 == null || (guildSubscriptions = GuildSubscriptions.copy$default(guildSubscriptions2, null, null, null, null, null, list, 31, null)) == null) {
            guildSubscriptions = new GuildSubscriptions(null, null, null, null, null, list, 31, null);
        }
        hashMap.put(valueOf, guildSubscriptions);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void requestFlushUnsubscriptions(Function0<Unit> function0) {
        function0.invoke();
    }

    public final synchronized void flush() {
        for (Map.Entry<Long, GuildSubscriptions> entry : this.pendingEmissions.entrySet()) {
            long longValue = entry.getKey().longValue();
            this.subscribedGuilds.add(Long.valueOf(longValue));
            this.onChange.invoke(Long.valueOf(longValue), entry.getValue());
        }
        this.pendingEmissions.clear();
    }

    public final synchronized GuildSubscriptions get(long j) {
        return new GuildSubscriptions(this.guildChannelSubscriptionsManager.get(j), Boolean.valueOf(this.typingGuilds.contains(Long.valueOf(j))), Boolean.valueOf(this.activityGuilds.contains(Long.valueOf(j))), this.guildMemberSubscriptionsManager.get(j), Boolean.valueOf(this.threadGuilds.contains(Long.valueOf(j))), this.guildThreadSubscriptionManager.get(j));
    }

    public final synchronized void queueExistingSubscriptions() {
        for (Number number : this.subscribedGuilds) {
            long longValue = number.longValue();
            this.pendingEmissions.put(Long.valueOf(longValue), get(longValue));
        }
    }

    public final synchronized void remove(long j) {
        this.subscribedGuilds.remove(Long.valueOf(j));
        this.pendingEmissions.remove(Long.valueOf(j));
        this.typingGuilds.remove(Long.valueOf(j));
        this.activityGuilds.remove(Long.valueOf(j));
        this.threadGuilds.remove(Long.valueOf(j));
        this.guildMemberSubscriptionsManager.remove(j);
        this.guildChannelSubscriptionsManager.remove(j);
        this.guildThreadSubscriptionManager.remove(j);
    }

    public final synchronized void reset() {
        this.subscribedGuilds.clear();
        this.pendingEmissions.clear();
        this.typingGuilds.clear();
        this.activityGuilds.clear();
        this.threadGuilds.clear();
        this.guildChannelSubscriptionsManager.reset();
        this.guildMemberSubscriptionsManager.reset();
        this.guildThreadSubscriptionManager.reset();
    }

    public final synchronized void retainAll(List<Long> list) {
        m.checkNotNullParameter(list, "guildIds");
        this.subscribedGuilds.retainAll(list);
        Iterator<Map.Entry<Long, GuildSubscriptions>> it = this.pendingEmissions.entrySet().iterator();
        while (it.hasNext()) {
            if (!list.contains(Long.valueOf(it.next().getKey().longValue()))) {
                it.remove();
            }
        }
        this.typingGuilds.retainAll(list);
        this.activityGuilds.retainAll(list);
        this.threadGuilds.retainAll(list);
        this.guildChannelSubscriptionsManager.retainAll(list);
        this.guildMemberSubscriptionsManager.retainAll(list);
        this.guildThreadSubscriptionManager.retainAll(list);
    }

    public final synchronized void subscribeActivities(long j) {
        GuildSubscriptions guildSubscriptions;
        if (!this.activityGuilds.contains(Long.valueOf(j))) {
            this.activityGuilds.add(Long.valueOf(j));
            HashMap<Long, GuildSubscriptions> hashMap = this.pendingEmissions;
            Long valueOf = Long.valueOf(j);
            GuildSubscriptions guildSubscriptions2 = this.pendingEmissions.get(Long.valueOf(j));
            if (guildSubscriptions2 == null || (guildSubscriptions = GuildSubscriptions.copy$default(guildSubscriptions2, null, null, Boolean.TRUE, null, null, null, 59, null)) == null) {
                guildSubscriptions = new GuildSubscriptions(null, null, Boolean.TRUE, null, null, null, 59, null);
            }
            hashMap.put(valueOf, guildSubscriptions);
        }
    }

    public final synchronized void subscribeChannel(long j, long j2, List<IntRange> list) {
        m.checkNotNullParameter(list, "ranges");
        this.guildChannelSubscriptionsManager.subscribe(j, j2, list);
    }

    public final synchronized void subscribeMember(long j, long j2) {
        this.guildMemberSubscriptionsManager.subscribe(j, j2);
    }

    public final synchronized void subscribeThread(long j, long j2) {
        this.guildThreadSubscriptionManager.subscribe(j, j2);
    }

    public final synchronized void subscribeThreads(long j) {
        GuildSubscriptions guildSubscriptions;
        if (!this.threadGuilds.contains(Long.valueOf(j))) {
            this.threadGuilds.add(Long.valueOf(j));
            HashMap<Long, GuildSubscriptions> hashMap = this.pendingEmissions;
            Long valueOf = Long.valueOf(j);
            GuildSubscriptions guildSubscriptions2 = this.pendingEmissions.get(Long.valueOf(j));
            if (guildSubscriptions2 == null || (guildSubscriptions = GuildSubscriptions.copy$default(guildSubscriptions2, null, null, null, null, Boolean.TRUE, null, 47, null)) == null) {
                guildSubscriptions = new GuildSubscriptions(null, null, null, null, Boolean.TRUE, null, 47, null);
            }
            hashMap.put(valueOf, guildSubscriptions);
        }
    }

    public final synchronized void subscribeTyping(long j) {
        GuildSubscriptions guildSubscriptions;
        if (!this.typingGuilds.contains(Long.valueOf(j))) {
            this.typingGuilds.add(Long.valueOf(j));
            HashMap<Long, GuildSubscriptions> hashMap = this.pendingEmissions;
            Long valueOf = Long.valueOf(j);
            GuildSubscriptions guildSubscriptions2 = this.pendingEmissions.get(Long.valueOf(j));
            if (guildSubscriptions2 == null || (guildSubscriptions = GuildSubscriptions.copy$default(guildSubscriptions2, null, Boolean.TRUE, null, null, null, null, 61, null)) == null) {
                guildSubscriptions = new GuildSubscriptions(null, Boolean.TRUE, null, null, null, null, 61, null);
            }
            hashMap.put(valueOf, guildSubscriptions);
        }
    }

    public final synchronized void unsubscribeMember(long j, long j2) {
        this.guildMemberSubscriptionsManager.unsubscribe(j, j2);
    }
}
