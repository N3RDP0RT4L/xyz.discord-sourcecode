package com.discord.utilities.lazy.subscriptions;

import andhook.lib.HookHelper;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.t.n0;
import d0.z.d.m;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function3;
import rx.Observable;
import rx.Scheduler;
import rx.Subscription;
/* compiled from: GuildMemberSubscriptionsManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010%\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001BY\u0012,\u0010$\u001a(\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0006j\u0002`\n0\t\u0012\u0004\u0012\u00020#\u0012\u0004\u0012\u00020\u00020\"\u0012\u0018\u0010\u001d\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00020\u001c\u0012\u0004\u0012\u00020\u00020\u001b\u0012\b\b\u0002\u0010\u0019\u001a\u00020\u0018¢\u0006\u0004\b-\u0010.J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0004J#\u0010\u000b\u001a\f\u0012\b\u0012\u00060\u0006j\u0002`\n0\t2\n\u0010\b\u001a\u00060\u0006j\u0002`\u0007¢\u0006\u0004\b\u000b\u0010\fJ%\u0010\u000e\u001a\u00020\u00022\n\u0010\b\u001a\u00060\u0006j\u0002`\u00072\n\u0010\r\u001a\u00060\u0006j\u0002`\n¢\u0006\u0004\b\u000e\u0010\u000fJ%\u0010\u0010\u001a\u00020\u00022\n\u0010\b\u001a\u00060\u0006j\u0002`\u00072\n\u0010\r\u001a\u00060\u0006j\u0002`\n¢\u0006\u0004\b\u0010\u0010\u000fJ\u0019\u0010\u0011\u001a\u00020\u00022\n\u0010\b\u001a\u00060\u0006j\u0002`\u0007¢\u0006\u0004\b\u0011\u0010\u0012J\r\u0010\u0013\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0004J\u001f\u0010\u0016\u001a\u00020\u00022\u0010\u0010\u0015\u001a\f\u0012\b\u0012\u00060\u0006j\u0002`\u00070\u0014¢\u0006\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0019\u001a\u00020\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR(\u0010\u001d\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00020\u001c\u0012\u0004\u0012\u00020\u00020\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u0018\u0010 \u001a\u0004\u0018\u00010\u001f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b \u0010!R<\u0010$\u001a(\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0006j\u0002`\n0\t\u0012\u0004\u0012\u00020#\u0012\u0004\u0012\u00020\u00020\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%RZ\u0010*\u001aF\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0006j\u0002`\n\u0012\u0004\u0012\u00020(0'0&j\"\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0006j\u0002`\n\u0012\u0004\u0012\u00020(0'`)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010+RZ\u0010,\u001aF\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0006j\u0002`\n\u0012\u0004\u0012\u00020(0'0&j\"\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0006j\u0002`\n\u0012\u0004\u0012\u00020(0'`)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b,\u0010+¨\u0006/"}, d2 = {"Lcom/discord/utilities/lazy/subscriptions/GuildMemberSubscriptionsManager;", "", "", "triggerUnsubscribe", "()V", "flushUnsubscriptions", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "Lcom/discord/primitives/UserId;", "get", "(J)Ljava/util/Set;", "userId", "subscribe", "(JJ)V", "unsubscribe", "remove", "(J)V", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_RESET, "", "guildIds", "retainAll", "(Ljava/util/List;)V", "Lrx/Scheduler;", "delayScheduler", "Lrx/Scheduler;", "Lkotlin/Function1;", "Lkotlin/Function0;", "requestFlushUnsubscriptions", "Lkotlin/jvm/functions/Function1;", "Lrx/Subscription;", "delayedFlushSubscription", "Lrx/Subscription;", "Lkotlin/Function3;", "", "onChange", "Lkotlin/jvm/functions/Function3;", "Ljava/util/HashMap;", "", "", "Lkotlin/collections/HashMap;", "subscriptions", "Ljava/util/HashMap;", "pendingUnsubscriptions", HookHelper.constructorName, "(Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;Lrx/Scheduler;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildMemberSubscriptionsManager {
    private final Scheduler delayScheduler;
    private Subscription delayedFlushSubscription;
    private final Function3<Long, Set<Long>, Boolean, Unit> onChange;
    private final HashMap<Long, Map<Long, Integer>> pendingUnsubscriptions;
    private final Function1<Function0<Unit>, Unit> requestFlushUnsubscriptions;
    private final HashMap<Long, Map<Long, Integer>> subscriptions;

    /* JADX WARN: Multi-variable type inference failed */
    public GuildMemberSubscriptionsManager(Function3<? super Long, ? super Set<Long>, ? super Boolean, Unit> function3, Function1<? super Function0<Unit>, Unit> function1, Scheduler scheduler) {
        m.checkNotNullParameter(function3, "onChange");
        m.checkNotNullParameter(function1, "requestFlushUnsubscriptions");
        m.checkNotNullParameter(scheduler, "delayScheduler");
        this.onChange = function3;
        this.requestFlushUnsubscriptions = function1;
        this.delayScheduler = scheduler;
        this.subscriptions = new HashMap<>();
        this.pendingUnsubscriptions = new HashMap<>();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void flushUnsubscriptions() {
        if (!this.pendingUnsubscriptions.isEmpty()) {
            for (Map.Entry<Long, Map<Long, Integer>> entry : this.pendingUnsubscriptions.entrySet()) {
                long longValue = entry.getKey().longValue();
                Map<Long, Integer> value = entry.getValue();
                Map<Long, Integer> map = this.subscriptions.get(Long.valueOf(longValue));
                if (map != null) {
                    m.checkNotNullExpressionValue(map, "subscriptions[guildId] ?: continue");
                    boolean z2 = false;
                    for (Map.Entry<Long, Integer> entry2 : value.entrySet()) {
                        long longValue2 = entry2.getKey().longValue();
                        int intValue = entry2.getValue().intValue();
                        Integer num = map.get(Long.valueOf(longValue2));
                        if (num != null) {
                            int intValue2 = num.intValue() - intValue;
                            if (intValue2 > 0) {
                                map.put(Long.valueOf(longValue2), Integer.valueOf(intValue2));
                            } else {
                                map.remove(Long.valueOf(longValue2));
                                z2 = true;
                            }
                        }
                    }
                    if (z2) {
                        if (map.isEmpty()) {
                            this.subscriptions.remove(Long.valueOf(longValue));
                        }
                        this.onChange.invoke(Long.valueOf(longValue), get(longValue), Boolean.TRUE);
                    }
                }
            }
            this.delayedFlushSubscription = null;
            this.pendingUnsubscriptions.clear();
        }
    }

    private final void triggerUnsubscribe() {
        if (this.delayedFlushSubscription == null) {
            Observable<Long> e02 = Observable.e0(20L, TimeUnit.SECONDS, this.delayScheduler);
            m.checkNotNullExpressionValue(e02, "Observable\n          .ti….SECONDS, delayScheduler)");
            ObservableExtensionsKt.appSubscribe(e02, GuildMemberSubscriptionsManager.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new GuildMemberSubscriptionsManager$triggerUnsubscribe$1(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new GuildMemberSubscriptionsManager$triggerUnsubscribe$2(this));
        }
    }

    public final Set<Long> get(long j) {
        Set<Long> keySet;
        Map<Long, Integer> map = this.subscriptions.get(Long.valueOf(j));
        return (map == null || (keySet = map.keySet()) == null) ? n0.emptySet() : keySet;
    }

    public final void remove(long j) {
        this.subscriptions.remove(Long.valueOf(j));
        this.pendingUnsubscriptions.remove(Long.valueOf(j));
    }

    public final void reset() {
        this.subscriptions.clear();
        this.pendingUnsubscriptions.clear();
        Subscription subscription = this.delayedFlushSubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        this.delayedFlushSubscription = null;
    }

    public final void retainAll(List<Long> list) {
        m.checkNotNullParameter(list, "guildIds");
        Iterator<Map.Entry<Long, Map<Long, Integer>>> it = this.subscriptions.entrySet().iterator();
        while (it.hasNext()) {
            if (!list.contains(Long.valueOf(it.next().getKey().longValue()))) {
                it.remove();
            }
        }
    }

    public final void subscribe(long j, long j2) {
        Map<Long, Integer> map = this.subscriptions.get(Long.valueOf(j));
        if (map == null) {
            map = new HashMap<>();
        }
        Long valueOf = Long.valueOf(j2);
        Integer num = map.get(Long.valueOf(j2));
        map.put(valueOf, Integer.valueOf(num != null ? num.intValue() + 1 : 1));
        this.subscriptions.put(Long.valueOf(j), map);
        Integer num2 = map.get(Long.valueOf(j2));
        if (num2 != null && num2.intValue() == 1) {
            this.onChange.invoke(Long.valueOf(j), map.keySet(), Boolean.FALSE);
        }
    }

    public final void unsubscribe(long j, long j2) {
        Map<Long, Integer> map = this.subscriptions.get(Long.valueOf(j));
        if ((map != null ? map.get(Long.valueOf(j2)) : null) != null) {
            Map<Long, Integer> map2 = this.pendingUnsubscriptions.get(Long.valueOf(j));
            if (map2 == null) {
                map2 = new HashMap<>();
            }
            Long valueOf = Long.valueOf(j2);
            Integer num = map2.get(Long.valueOf(j2));
            map2.put(valueOf, Integer.valueOf(num != null ? num.intValue() + 1 : 1));
            this.pendingUnsubscriptions.put(Long.valueOf(j), map2);
            Integer num2 = map2.get(Long.valueOf(j2));
            if (num2 != null && num2.intValue() == 1) {
                triggerUnsubscribe();
            }
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ GuildMemberSubscriptionsManager(kotlin.jvm.functions.Function3 r1, kotlin.jvm.functions.Function1 r2, rx.Scheduler r3, int r4, kotlin.jvm.internal.DefaultConstructorMarker r5) {
        /*
            r0 = this;
            r4 = r4 & 4
            if (r4 == 0) goto Ld
            rx.Scheduler r3 = j0.p.a.a()
            java.lang.String r4 = "Schedulers.computation()"
            d0.z.d.m.checkNotNullExpressionValue(r3, r4)
        Ld:
            r0.<init>(r1, r2, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.utilities.lazy.subscriptions.GuildMemberSubscriptionsManager.<init>(kotlin.jvm.functions.Function3, kotlin.jvm.functions.Function1, rx.Scheduler, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }
}
