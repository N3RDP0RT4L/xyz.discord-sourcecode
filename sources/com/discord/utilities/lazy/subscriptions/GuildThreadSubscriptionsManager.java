package com.discord.utilities.lazy.subscriptions;

import andhook.lib.HookHelper;
import android.util.LruCache;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: GuildThreadSubscriptionsManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B/\u0012&\u0010\u0015\u001a\"\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00050\n\u0012\u0004\u0012\u00020\u00070\u0014¢\u0006\u0004\b\u001c\u0010\u001dJ%\u0010\b\u001a\u00020\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u0005¢\u0006\u0004\b\b\u0010\tJ#\u0010\u000b\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00050\n2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u000b\u0010\fJ\r\u0010\r\u001a\u00020\u0007¢\u0006\u0004\b\r\u0010\u000eJ\u0019\u0010\u000f\u001a\u00020\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\u0012\u001a\u00020\u00072\u0010\u0010\u0011\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\n¢\u0006\u0004\b\u0012\u0010\u0013R6\u0010\u0015\u001a\"\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00050\n\u0012\u0004\u0012\u00020\u00070\u00148\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016Rb\u0010\u001a\u001aN\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0002j\u0002`\u0005\u0012\b\u0012\u00060\u0002j\u0002`\u00050\u00180\u0017j&\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0002j\u0002`\u0005\u0012\b\u0012\u00060\u0002j\u0002`\u00050\u0018`\u00198\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001a\u0010\u001b¨\u0006\u001e"}, d2 = {"Lcom/discord/utilities/lazy/subscriptions/GuildThreadSubscriptionsManager;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/ChannelId;", "channelId", "", "subscribe", "(JJ)V", "", "get", "(J)Ljava/util/List;", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_RESET, "()V", "remove", "(J)V", "guildIds", "retainAll", "(Ljava/util/List;)V", "Lkotlin/Function2;", "changeHandler", "Lkotlin/jvm/functions/Function2;", "Ljava/util/HashMap;", "Landroid/util/LruCache;", "Lkotlin/collections/HashMap;", "subscriptions", "Ljava/util/HashMap;", HookHelper.constructorName, "(Lkotlin/jvm/functions/Function2;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildThreadSubscriptionsManager {
    private final Function2<Long, List<Long>, Unit> changeHandler;
    private final HashMap<Long, LruCache<Long, Long>> subscriptions = new HashMap<>();

    /* JADX WARN: Multi-variable type inference failed */
    public GuildThreadSubscriptionsManager(Function2<? super Long, ? super List<Long>, Unit> function2) {
        m.checkNotNullParameter(function2, "changeHandler");
        this.changeHandler = function2;
    }

    public final List<Long> get(long j) {
        Map<Long, Long> snapshot;
        Set<Long> keySet;
        List<Long> list;
        LruCache<Long, Long> lruCache = this.subscriptions.get(Long.valueOf(j));
        return (lruCache == null || (snapshot = lruCache.snapshot()) == null || (keySet = snapshot.keySet()) == null || (list = u.toList(keySet)) == null) ? n.emptyList() : list;
    }

    public final void remove(long j) {
        this.subscriptions.remove(Long.valueOf(j));
    }

    public final void reset() {
        this.subscriptions.clear();
    }

    public final void retainAll(List<Long> list) {
        m.checkNotNullParameter(list, "guildIds");
        Iterator<Map.Entry<Long, LruCache<Long, Long>>> it = this.subscriptions.entrySet().iterator();
        while (it.hasNext()) {
            if (!list.contains(Long.valueOf(it.next().getKey().longValue()))) {
                it.remove();
            }
        }
    }

    public final void subscribe(long j, long j2) {
        LruCache<Long, Long> lruCache = this.subscriptions.get(Long.valueOf(j));
        if (lruCache == null) {
            lruCache = new LruCache<>(3);
        }
        if (lruCache.get(Long.valueOf(j2)) == null) {
            lruCache.put(Long.valueOf(j2), Long.valueOf(j2));
            this.subscriptions.put(Long.valueOf(j), lruCache);
            this.changeHandler.invoke(Long.valueOf(j), u.toList(lruCache.snapshot().keySet()));
        }
    }
}
