package com.discord.utilities.lazy.subscriptions;

import andhook.lib.HookHelper;
import android.util.LruCache;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.t.h0;
import d0.z.d.m;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.ranges.IntRange;
/* compiled from: GuildChannelSubscriptionsManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B;\u00122\u0010\u0018\u001a.\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u001a\u0012\u0018\u0012\b\u0012\u00060\u0002j\u0002`\u0005\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\r\u0012\u0004\u0012\u00020\n0\u0017¢\u0006\u0004\b\u001f\u0010 J3\u0010\u000b\u001a\u00020\n2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u00052\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\u0004\b\u000b\u0010\fJ/\u0010\u000e\u001a\u0018\u0012\b\u0012\u00060\u0002j\u0002`\u0005\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\r2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\r\u0010\u0010\u001a\u00020\n¢\u0006\u0004\b\u0010\u0010\u0011J\u0019\u0010\u0012\u001a\u00020\n2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u001f\u0010\u0015\u001a\u00020\n2\u0010\u0010\u0014\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\u0007¢\u0006\u0004\b\u0015\u0010\u0016RB\u0010\u0018\u001a.\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u001a\u0012\u0018\u0012\b\u0012\u00060\u0002j\u0002`\u0005\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\r\u0012\u0004\u0012\u00020\n0\u00178\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019Rf\u0010\u001d\u001aR\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u001a\u0012\u0018\u0012\b\u0012\u00060\u0002j\u0002`\u0005\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u001b0\u001aj(\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u001a\u0012\u0018\u0012\b\u0012\u00060\u0002j\u0002`\u0005\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u001b`\u001c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u001e¨\u0006!"}, d2 = {"Lcom/discord/utilities/lazy/subscriptions/GuildChannelSubscriptionsManager;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/ChannelId;", "channelId", "", "Lkotlin/ranges/IntRange;", "ranges", "", "subscribe", "(JJLjava/util/List;)V", "", "get", "(J)Ljava/util/Map;", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_RESET, "()V", "remove", "(J)V", "guildIds", "retainAll", "(Ljava/util/List;)V", "Lkotlin/Function2;", "changeHandler", "Lkotlin/jvm/functions/Function2;", "Ljava/util/HashMap;", "Landroid/util/LruCache;", "Lkotlin/collections/HashMap;", "subscriptions", "Ljava/util/HashMap;", HookHelper.constructorName, "(Lkotlin/jvm/functions/Function2;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildChannelSubscriptionsManager {
    private final Function2<Long, Map<Long, ? extends List<IntRange>>, Unit> changeHandler;
    private final HashMap<Long, LruCache<Long, List<IntRange>>> subscriptions = new HashMap<>();

    /* JADX WARN: Multi-variable type inference failed */
    public GuildChannelSubscriptionsManager(Function2<? super Long, ? super Map<Long, ? extends List<IntRange>>, Unit> function2) {
        m.checkNotNullParameter(function2, "changeHandler");
        this.changeHandler = function2;
    }

    public final Map<Long, List<IntRange>> get(long j) {
        Map<Long, List<IntRange>> snapshot;
        LruCache<Long, List<IntRange>> lruCache = this.subscriptions.get(Long.valueOf(j));
        return (lruCache == null || (snapshot = lruCache.snapshot()) == null) ? h0.emptyMap() : snapshot;
    }

    public final void remove(long j) {
        this.subscriptions.remove(Long.valueOf(j));
    }

    public final void reset() {
        this.subscriptions.clear();
    }

    public final void retainAll(List<Long> list) {
        m.checkNotNullParameter(list, "guildIds");
        Iterator<Map.Entry<Long, LruCache<Long, List<IntRange>>>> it = this.subscriptions.entrySet().iterator();
        while (it.hasNext()) {
            if (!list.contains(Long.valueOf(it.next().getKey().longValue()))) {
                it.remove();
            }
        }
    }

    public final void subscribe(long j, long j2, List<IntRange> list) {
        m.checkNotNullParameter(list, "ranges");
        LruCache<Long, List<IntRange>> lruCache = this.subscriptions.get(Long.valueOf(j));
        if (lruCache == null) {
            lruCache = new LruCache<>(5);
        }
        if (!m.areEqual(lruCache.get(Long.valueOf(j2)), list)) {
            lruCache.put(Long.valueOf(j2), list);
            this.subscriptions.put(Long.valueOf(j), lruCache);
            Function2<Long, Map<Long, ? extends List<IntRange>>, Unit> function2 = this.changeHandler;
            Long valueOf = Long.valueOf(j);
            Map<Long, List<IntRange>> snapshot = lruCache.snapshot();
            m.checkNotNullExpressionValue(snapshot, "guildSubscriptions.snapshot()");
            function2.invoke(valueOf, snapshot);
        }
    }
}
