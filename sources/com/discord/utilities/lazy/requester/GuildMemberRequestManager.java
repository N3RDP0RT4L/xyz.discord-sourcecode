package com.discord.utilities.lazy.requester;

import andhook.lib.HookHelper;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
/* compiled from: GuildMemberRequestManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0001\u001eBQ\u0012 \u0010\u0017\u001a\u001c\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\b\u0012\u00060\u0002j\u0002`\b\u0012\u0004\u0012\u00020\u00160\u0012\u0012&\u0010\u0014\u001a\"\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\b0\u0013\u0012\u0004\u0012\u00020\n0\u0012¢\u0006\u0004\b\u001c\u0010\u001dJ\u001f\u0010\u0006\u001a\u00060\u0005R\u00020\u00002\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J%\u0010\u000b\u001a\u00020\n2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\t\u001a\u00060\u0002j\u0002`\b¢\u0006\u0004\b\u000b\u0010\fJ\r\u0010\r\u001a\u00020\n¢\u0006\u0004\b\r\u0010\u000eJ%\u0010\u000f\u001a\u00020\n2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\t\u001a\u00060\u0002j\u0002`\b¢\u0006\u0004\b\u000f\u0010\fJ\r\u0010\u0010\u001a\u00020\n¢\u0006\u0004\b\u0010\u0010\u000eJ\r\u0010\u0011\u001a\u00020\n¢\u0006\u0004\b\u0011\u0010\u000eR6\u0010\u0014\u001a\"\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\b0\u0013\u0012\u0004\u0012\u00020\n0\u00128\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R0\u0010\u0017\u001a\u001c\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\b\u0012\u00060\u0002j\u0002`\b\u0012\u0004\u0012\u00020\u00160\u00128\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0015RB\u0010\u001a\u001a.\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\b\u0012\u00060\u0005R\u00020\u00000\u0018j\u0016\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\b\u0012\u00060\u0005R\u00020\u0000`\u00198\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001a\u0010\u001b¨\u0006\u001f"}, d2 = {"Lcom/discord/utilities/lazy/requester/GuildMemberRequestManager;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/utilities/lazy/requester/GuildMemberRequestManager$GuildMemberRequestState;", "getGuildRequestState", "(J)Lcom/discord/utilities/lazy/requester/GuildMemberRequestManager$GuildMemberRequestState;", "Lcom/discord/primitives/UserId;", "userId", "", "queueRequest", "(JJ)V", "requestUnacknowledged", "()V", "acknowledge", "flush", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_RESET, "Lkotlin/Function2;", "", "onFlush", "Lkotlin/jvm/functions/Function2;", "", "guildMemberExists", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "guildRequestStates", "Ljava/util/HashMap;", HookHelper.constructorName, "(Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;)V", "GuildMemberRequestState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildMemberRequestManager {
    private final Function2<Long, Long, Boolean> guildMemberExists;
    private final HashMap<Long, GuildMemberRequestState> guildRequestStates = new HashMap<>();
    private final Function2<Long, List<Long>, Unit> onFlush;

    /* compiled from: GuildMemberRequestManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0082\u0004\u0018\u00002\u00020\u0001B+\u0012\n\u0010\u001b\u001a\u00060\u0002j\u0002`\u001a\u0012\u0016\u0010\u0018\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00050\u0017¢\u0006\u0004\b\u001f\u0010 J\u0019\u0010\u0006\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\r\u0010\b\u001a\u00020\u0005¢\u0006\u0004\b\b\u0010\tJ\u0019\u0010\u000b\u001a\u00020\n2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u000b\u0010\fJ\r\u0010\r\u001a\u00020\n¢\u0006\u0004\b\r\u0010\u000eR1\u0010\u0011\u001a\u001a\u0012\b\u0012\u00060\u0002j\u0002`\u00030\u000fj\f\u0012\b\u0012\u00060\u0002j\u0002`\u0003`\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R1\u0010\u0015\u001a\u001a\u0012\b\u0012\u00060\u0002j\u0002`\u00030\u000fj\f\u0012\b\u0012\u00060\u0002j\u0002`\u0003`\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0012\u001a\u0004\b\u0016\u0010\u0014R&\u0010\u0018\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00050\u00178\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R\u001a\u0010\u001b\u001a\u00060\u0002j\u0002`\u001a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR1\u0010\u001d\u001a\u001a\u0012\b\u0012\u00060\u0002j\u0002`\u00030\u000fj\f\u0012\b\u0012\u00060\u0002j\u0002`\u0003`\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u0012\u001a\u0004\b\u001e\u0010\u0014¨\u0006!"}, d2 = {"Lcom/discord/utilities/lazy/requester/GuildMemberRequestManager$GuildMemberRequestState;", "", "", "Lcom/discord/primitives/UserId;", "userId", "", "request", "(J)Z", "requestUnacknowledged", "()Z", "", "acknowledge", "(J)V", "flushRequests", "()V", "Ljava/util/HashSet;", "Lkotlin/collections/HashSet;", "pendingRequests", "Ljava/util/HashSet;", "getPendingRequests", "()Ljava/util/HashSet;", "sentRequests", "getSentRequests", "Lkotlin/Function1;", "guildMemberExists", "Lkotlin/jvm/functions/Function1;", "Lcom/discord/primitives/GuildId;", "guildId", "J", "unacknowledgedRequests", "getUnacknowledgedRequests", HookHelper.constructorName, "(Lcom/discord/utilities/lazy/requester/GuildMemberRequestManager;JLkotlin/jvm/functions/Function1;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final class GuildMemberRequestState {
        private final long guildId;
        private final Function1<Long, Boolean> guildMemberExists;
        public final /* synthetic */ GuildMemberRequestManager this$0;
        private final HashSet<Long> pendingRequests = new HashSet<>();
        private final HashSet<Long> sentRequests = new HashSet<>();
        private final HashSet<Long> unacknowledgedRequests = new HashSet<>();

        /* JADX WARN: Multi-variable type inference failed */
        public GuildMemberRequestState(GuildMemberRequestManager guildMemberRequestManager, long j, Function1<? super Long, Boolean> function1) {
            m.checkNotNullParameter(function1, "guildMemberExists");
            this.this$0 = guildMemberRequestManager;
            this.guildId = j;
            this.guildMemberExists = function1;
        }

        public final void acknowledge(long j) {
            this.unacknowledgedRequests.remove(Long.valueOf(j));
            this.pendingRequests.remove(Long.valueOf(j));
        }

        public final void flushRequests() {
            HashSet hashSet = new HashSet();
            Iterator<Long> it = this.pendingRequests.iterator();
            while (it.hasNext()) {
                Long next = it.next();
                Function1<Long, Boolean> function1 = this.guildMemberExists;
                m.checkNotNullExpressionValue(next, "userId");
                if (!function1.invoke(next).booleanValue()) {
                    this.unacknowledgedRequests.add(next);
                    this.sentRequests.add(next);
                    hashSet.add(next);
                }
            }
            if (!hashSet.isEmpty()) {
                this.this$0.onFlush.invoke(Long.valueOf(this.guildId), u.toList(hashSet));
            }
            this.pendingRequests.clear();
        }

        public final HashSet<Long> getPendingRequests() {
            return this.pendingRequests;
        }

        public final HashSet<Long> getSentRequests() {
            return this.sentRequests;
        }

        public final HashSet<Long> getUnacknowledgedRequests() {
            return this.unacknowledgedRequests;
        }

        public final boolean request(long j) {
            if (this.sentRequests.contains(Long.valueOf(j)) || this.pendingRequests.contains(Long.valueOf(j)) || this.guildMemberExists.invoke(Long.valueOf(j)).booleanValue()) {
                return false;
            }
            this.pendingRequests.add(Long.valueOf(j));
            return true;
        }

        public final boolean requestUnacknowledged() {
            if (this.unacknowledgedRequests.isEmpty()) {
                return false;
            }
            Iterator<Long> it = this.unacknowledgedRequests.iterator();
            m.checkNotNullExpressionValue(it, "unacknowledgedRequests.iterator()");
            while (it.hasNext()) {
                Long next = it.next();
                m.checkNotNullExpressionValue(next, "iter.next()");
                long longValue = next.longValue();
                if (this.guildMemberExists.invoke(Long.valueOf(longValue)).booleanValue()) {
                    it.remove();
                } else {
                    this.pendingRequests.add(Long.valueOf(longValue));
                }
            }
            return !this.pendingRequests.isEmpty();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public GuildMemberRequestManager(Function2<? super Long, ? super Long, Boolean> function2, Function2<? super Long, ? super List<Long>, Unit> function22) {
        m.checkNotNullParameter(function2, "guildMemberExists");
        m.checkNotNullParameter(function22, "onFlush");
        this.guildMemberExists = function2;
        this.onFlush = function22;
    }

    private final GuildMemberRequestState getGuildRequestState(long j) {
        GuildMemberRequestState guildMemberRequestState = this.guildRequestStates.get(Long.valueOf(j));
        if (guildMemberRequestState != null) {
            return guildMemberRequestState;
        }
        GuildMemberRequestState guildMemberRequestState2 = new GuildMemberRequestState(this, j, new GuildMemberRequestManager$getGuildRequestState$1(this, j));
        this.guildRequestStates.put(Long.valueOf(j), guildMemberRequestState2);
        return guildMemberRequestState2;
    }

    public final void acknowledge(long j, long j2) {
        getGuildRequestState(j).acknowledge(j2);
    }

    public final void flush() {
        for (GuildMemberRequestState guildMemberRequestState : this.guildRequestStates.values()) {
            guildMemberRequestState.flushRequests();
        }
    }

    public final void queueRequest(long j, long j2) {
        getGuildRequestState(j).request(j2);
    }

    public final void requestUnacknowledged() {
        Collection<GuildMemberRequestState> values = this.guildRequestStates.values();
        m.checkNotNullExpressionValue(values, "guildRequestStates.values");
        int i = 0;
        if (!(values instanceof Collection) || !values.isEmpty()) {
            for (GuildMemberRequestState guildMemberRequestState : values) {
                if (guildMemberRequestState.requestUnacknowledged() && (i = i + 1) < 0) {
                    n.throwCountOverflow();
                }
            }
        }
        if (i > 0) {
            flush();
        }
    }

    public final void reset() {
        this.guildRequestStates.clear();
    }
}
