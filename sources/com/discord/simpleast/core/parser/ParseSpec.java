package com.discord.simpleast.core.parser;

import andhook.lib.HookHelper;
import androidx.exifinterface.media.ExifInterface;
import com.discord.simpleast.core.node.Node;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ParseSpec.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\b\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u00022\u00020\u0003B/\b\u0016\u0012\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00028\u00000\u0015\u0012\u0006\u0010\u0010\u001a\u00028\u0001\u0012\u0006\u0010\u0014\u001a\u00020\u0004\u0012\u0006\u0010\u000b\u001a\u00020\u0004¢\u0006\u0004\b \u0010!B\u001f\b\u0016\u0012\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00028\u00000\u0015\u0012\u0006\u0010\u0010\u001a\u00028\u0001¢\u0006\u0004\b \u0010\"R\"\u0010\u000b\u001a\u00020\u00048\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0005\u0010\u0006\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u0019\u0010\u0010\u001a\u00028\u00018\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000fR\"\u0010\u0014\u001a\u00020\u00048\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0011\u0010\u0006\u001a\u0004\b\u0012\u0010\b\"\u0004\b\u0013\u0010\nR\u001f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00028\u00000\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u0019\u0010\u001e\u001a\u00020\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001f¨\u0006#"}, d2 = {"Lcom/discord/simpleast/core/parser/ParseSpec;", "R", ExifInterface.LATITUDE_SOUTH, "", "", "e", "I", "getEndIndex", "()I", "setEndIndex", "(I)V", "endIndex", "c", "Ljava/lang/Object;", "getState", "()Ljava/lang/Object;", "state", "d", "getStartIndex", "setStartIndex", "startIndex", "Lcom/discord/simpleast/core/node/Node;", "a", "Lcom/discord/simpleast/core/node/Node;", "getRoot", "()Lcom/discord/simpleast/core/node/Node;", "root", "", "b", "Z", "isTerminal", "()Z", HookHelper.constructorName, "(Lcom/discord/simpleast/core/node/Node;Ljava/lang/Object;II)V", "(Lcom/discord/simpleast/core/node/Node;Ljava/lang/Object;)V", "simpleast-core_release"}, k = 1, mv = {1, 4, 0})
/* loaded from: classes.dex */
public final class ParseSpec<R, S> {
    public final Node<R> a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f2780b = true;
    public final S c;
    public int d;
    public int e;

    public ParseSpec(Node<R> node, S s2, int i, int i2) {
        m.checkNotNullParameter(node, "root");
        this.a = node;
        this.c = s2;
        this.d = i;
        this.e = i2;
    }

    public ParseSpec(Node<R> node, S s2) {
        m.checkNotNullParameter(node, "root");
        this.a = node;
        this.c = s2;
    }
}
