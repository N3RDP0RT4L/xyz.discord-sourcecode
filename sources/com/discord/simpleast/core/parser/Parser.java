package com.discord.simpleast.core.parser;

import andhook.lib.HookHelper;
import android.util.Log;
import androidx.exifinterface.media.ExifInterface;
import b.d.b.a.a;
import com.discord.simpleast.core.node.Node;
import d0.o;
import d0.t.j;
import d0.t.u;
import d0.z.d.e0;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;
import java.util.regex.Matcher;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: Parser.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\u0011\n\u0002\b\u0003\n\u0002\u0010\u001e\n\u0002\b\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0010!\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0016\u0018\u0000 #*\u0004\b\u0000\u0010\u0001*\u000e\b\u0001\u0010\u0003*\b\u0012\u0004\u0012\u00028\u00000\u0002*\u0004\b\u0002\u0010\u00042\u00020\u0005:\u0002#$B\u0013\b\u0007\u0012\b\b\u0002\u0010\u001d\u001a\u00020\u001c¢\u0006\u0004\b!\u0010\"JM\u0010\u000b\u001a\u00020\n\"\u0004\b\u0003\u0010\u0001\"\u000e\b\u0004\u0010\u0003*\b\u0012\u0004\u0012\u00028\u00030\u0002\"\u0004\b\u0005\u0010\u00042\u0018\u0010\u0007\u001a\u0014\u0012\u0004\u0012\u00028\u0003\u0012\u0004\u0012\u00028\u0004\u0012\u0004\u0012\u00028\u00050\u00062\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\u000b\u0010\fJM\u0010\r\u001a\u00020\n\"\u0004\b\u0003\u0010\u0001\"\u000e\b\u0004\u0010\u0003*\b\u0012\u0004\u0012\u00028\u00030\u0002\"\u0004\b\u0005\u0010\u00042\u0018\u0010\u0007\u001a\u0014\u0012\u0004\u0012\u00028\u0003\u0012\u0004\u0012\u00028\u0004\u0012\u0004\u0012\u00028\u00050\u00062\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\r\u0010\fJ;\u0010\u000e\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00002\u001a\u0010\u0007\u001a\u0016\u0012\u0004\u0012\u00028\u0000\u0012\u0006\b\u0001\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0006¢\u0006\u0004\b\u000e\u0010\u000fJ[\u0010\u0012\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00002:\u0010\u0011\u001a\u001e\u0012\u001a\b\u0001\u0012\u0016\u0012\u0004\u0012\u00028\u0000\u0012\u0006\b\u0001\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00060\u0010\"\u0016\u0012\u0004\u0012\u00028\u0000\u0012\u0006\b\u0001\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0006¢\u0006\u0004\b\u0012\u0010\u0013JA\u0010\u0012\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00002 \u0010\u0011\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00028\u0000\u0012\u0006\b\u0001\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00060\u0014¢\u0006\u0004\b\u0012\u0010\u0015JI\u0010\u001a\u001a\b\u0012\u0004\u0012\u00028\u00010\u00192\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u0016\u001a\u00028\u00022\"\b\u0002\u0010\u0018\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00028\u0000\u0012\u0006\b\u0001\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00060\u0017H\u0007¢\u0006\u0004\b\u001a\u0010\u001bR\u0016\u0010\u001d\u001a\u00020\u001c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR0\u0010\u0018\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00028\u0000\u0012\u0006\b\u0001\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00060\u001f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010 ¨\u0006%"}, d2 = {"Lcom/discord/simpleast/core/parser/Parser;", "R", "Lcom/discord/simpleast/core/node/Node;", ExifInterface.GPS_DIRECTION_TRUE, ExifInterface.LATITUDE_SOUTH, "", "Lcom/discord/simpleast/core/parser/Rule;", "rule", "", "source", "", "logMatch", "(Lcom/discord/simpleast/core/parser/Rule;Ljava/lang/CharSequence;)V", "logMiss", "addRule", "(Lcom/discord/simpleast/core/parser/Rule;)Lcom/discord/simpleast/core/parser/Parser;", "", "newRules", "addRules", "([Lcom/discord/simpleast/core/parser/Rule;)Lcom/discord/simpleast/core/parser/Parser;", "", "(Ljava/util/Collection;)Lcom/discord/simpleast/core/parser/Parser;", "initialState", "", "rules", "", "parse", "(Ljava/lang/CharSequence;Ljava/lang/Object;Ljava/util/List;)Ljava/util/List;", "", "enableDebugging", "Z", "Ljava/util/ArrayList;", "Ljava/util/ArrayList;", HookHelper.constructorName, "(Z)V", "Companion", "ParseException", "simpleast-core_release"}, k = 1, mv = {1, 4, 0})
/* loaded from: classes.dex */
public class Parser<R, T extends Node<R>, S> {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = "Parser";
    private final boolean enableDebugging;
    private final ArrayList<Rule<R, ? extends T, S>> rules;

    /* compiled from: Parser.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/simpleast/core/parser/Parser$Companion;", "", "", "TAG", "Ljava/lang/String;", HookHelper.constructorName, "()V", "simpleast-core_release"}, k = 1, mv = {1, 4, 0})
    /* loaded from: classes.dex */
    public static final class Companion {
        public Companion() {
        }

        public Companion(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: Parser.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0004\u0018\u00002\u00060\u0001j\u0002`\u0002B%\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0007¢\u0006\u0004\b\t\u0010\n¨\u0006\u000b"}, d2 = {"Lcom/discord/simpleast/core/parser/Parser$ParseException;", "Ljava/lang/RuntimeException;", "Lkotlin/RuntimeException;", "", "message", "", "source", "", "cause", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/Throwable;)V", "simpleast-core_release"}, k = 1, mv = {1, 4, 0})
    /* loaded from: classes.dex */
    public static final class ParseException extends RuntimeException {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ParseException(String str, CharSequence charSequence, Throwable th) {
            super("Error while parsing: " + str + " \n Source: " + charSequence, th);
            m.checkNotNullParameter(str, "message");
        }
    }

    public Parser() {
        this(false, 1, null);
    }

    public Parser(boolean z2) {
        this.enableDebugging = z2;
        this.rules = new ArrayList<>();
    }

    private final <R, T extends Node<R>, S> void logMatch(Rule<R, T, S> rule, CharSequence charSequence) {
        if (this.enableDebugging) {
            StringBuilder R = a.R("MATCH: with rule with pattern: ");
            R.append(rule.getMatcher().pattern().toString());
            R.append(" to source: ");
            R.append(charSequence);
            Log.i(TAG, R.toString());
        }
    }

    private final <R, T extends Node<R>, S> void logMiss(Rule<R, T, S> rule, CharSequence charSequence) {
        if (this.enableDebugging) {
            StringBuilder R = a.R("MISS: with rule with pattern: ");
            R.append(rule.getMatcher().pattern().toString());
            R.append(" to source: ");
            R.append(charSequence);
            Log.i(TAG, R.toString());
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ List parse$default(Parser parser, CharSequence charSequence, Object obj, List list, int i, Object obj2) {
        if (obj2 == null) {
            if ((i & 4) != 0) {
                list = parser.rules;
            }
            return parser.parse(charSequence, obj, list);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: parse");
    }

    public final Parser<R, T, S> addRule(Rule<R, ? extends T, S> rule) {
        m.checkNotNullParameter(rule, "rule");
        this.rules.add(rule);
        return this;
    }

    public final Parser<R, T, S> addRules(Rule<R, ? extends T, S>... ruleArr) {
        m.checkNotNullParameter(ruleArr, "newRules");
        return addRules(j.asList(ruleArr));
    }

    public final List<T> parse(CharSequence charSequence, S s2) {
        return parse$default(this, charSequence, s2, null, 4, null);
    }

    public final List<T> parse(CharSequence charSequence, S s2, List<? extends Rule<R, ? extends T, S>> list) {
        ParseSpec parseSpec;
        int i;
        int i2;
        Pair pair;
        m.checkNotNullParameter(charSequence, "source");
        m.checkNotNullParameter(list, "rules");
        Stack stack = new Stack();
        List<T> list2 = null;
        boolean z2 = true;
        Node node = new Node(null, 1, null);
        if (charSequence.length() <= 0) {
            z2 = false;
        }
        if (z2) {
            stack.add(new ParseSpec(node, s2, 0, charSequence.length()));
        }
        String str = null;
        while (!stack.isEmpty() && (i = (parseSpec = (ParseSpec) stack.pop()).d) < (i2 = parseSpec.e)) {
            CharSequence subSequence = charSequence.subSequence(i, i2);
            int i3 = parseSpec.d;
            Iterator<? extends Rule<R, ? extends T, S>> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    pair = null;
                    break;
                }
                Rule<R, T, S> rule = (Rule) ((Rule<R, ? extends T, S>) it.next());
                Matcher match = rule.match(subSequence, str, parseSpec.c);
                if (match == null) {
                    logMiss(rule, subSequence);
                    pair = null;
                    continue;
                } else {
                    logMatch(rule, subSequence);
                    pair = o.to(rule, match);
                    continue;
                }
                if (pair != null) {
                    break;
                }
            }
            if (pair != null) {
                Matcher matcher = (Matcher) pair.component2();
                int end = matcher.end() + i3;
                ParseSpec<R, S> parse = ((Rule) pair.component1()).parse(matcher, this, parseSpec.c);
                Node<R> node2 = parseSpec.a;
                node2.addChild(parse.a);
                int i4 = parseSpec.e;
                if (end != i4) {
                    S s3 = parseSpec.c;
                    m.checkNotNullParameter(node2, "node");
                    stack.push(new ParseSpec(node2, s3, end, i4));
                }
                if (!parse.f2780b) {
                    parse.d += i3;
                    parse.e += i3;
                    stack.push(parse);
                }
                try {
                    str = matcher.group(0);
                } catch (Throwable th) {
                    throw new ParseException("matcher found no matches", charSequence, th);
                }
            } else {
                throw new ParseException("failed to find rule to match source", charSequence, null);
            }
        }
        Collection<Node<R>> children = node.getChildren();
        List<T> mutableList = children != null ? u.toMutableList((Collection) children) : null;
        if (e0.isMutableList(mutableList)) {
            list2 = mutableList;
        }
        return list2 != null ? list2 : new ArrayList();
    }

    public final Parser<R, T, S> addRules(Collection<? extends Rule<R, ? extends T, S>> collection) {
        m.checkNotNullParameter(collection, "newRules");
        this.rules.addAll(collection);
        return this;
    }

    public /* synthetic */ Parser(boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? false : z2);
    }
}
