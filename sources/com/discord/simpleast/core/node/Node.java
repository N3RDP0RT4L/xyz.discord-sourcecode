package com.discord.simpleast.core.node;

import andhook.lib.HookHelper;
import android.text.SpannableStringBuilder;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: Node.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0010\u001e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u001f\n\u0002\b\u0006\b\u0016\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002:\u0001\u0017B\u001f\u0012\u0016\b\u0002\u0010\u0013\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0000\u0018\u00010\u0012¢\u0006\u0004\b\u0015\u0010\u0016J\u001b\u0010\u0004\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0000\u0018\u00010\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\r\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\u0007\u0010\bJ\u001b\u0010\u000b\u001a\u00020\n2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00028\u00000\u0000¢\u0006\u0004\b\u000b\u0010\fJ\u001f\u0010\u0010\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00028\u0000H\u0016¢\u0006\u0004\b\u0010\u0010\u0011R$\u0010\u0013\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0000\u0018\u00010\u00128\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014¨\u0006\u0018"}, d2 = {"Lcom/discord/simpleast/core/node/Node;", "R", "", "", "getChildren", "()Ljava/util/Collection;", "", "hasChildren", "()Z", "child", "", "addChild", "(Lcom/discord/simpleast/core/node/Node;)V", "Landroid/text/SpannableStringBuilder;", "builder", "renderContext", "render", "(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;)V", "", "children", "Ljava/util/Collection;", HookHelper.constructorName, "(Ljava/util/Collection;)V", "a", "simpleast-core_release"}, k = 1, mv = {1, 4, 0})
/* loaded from: classes.dex */
public class Node<R> {
    private Collection<Node<R>> children;

    /* compiled from: Node.kt */
    /* loaded from: classes.dex */
    public static class a<R> extends Node<R> {

        /* compiled from: Node.kt */
        /* renamed from: com.discord.simpleast.core.node.Node$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C0192a extends o implements Function1<Node<R>, CharSequence> {
            public static final C0192a j = new C0192a();

            public C0192a() {
                super(1);
            }

            @Override // kotlin.jvm.functions.Function1
            public CharSequence invoke(Object obj) {
                Node node = (Node) obj;
                m.checkNotNullParameter(node, "it");
                return node.toString();
            }
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public a(com.discord.simpleast.core.node.Node<R>... r5) {
            /*
                r4 = this;
                java.lang.String r0 = "children"
                d0.z.d.m.checkNotNullParameter(r5, r0)
                java.util.ArrayList r0 = new java.util.ArrayList
                r0.<init>()
                int r1 = r5.length
                r2 = 0
            Lc:
                if (r2 >= r1) goto L18
                r3 = r5[r2]
                if (r3 == 0) goto L15
                r0.add(r3)
            L15:
                int r2 = r2 + 1
                goto Lc
            L18:
                java.util.List r5 = d0.t.u.toMutableList(r0)
                r4.<init>(r5)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.simpleast.core.node.Node.a.<init>(com.discord.simpleast.core.node.Node[]):void");
        }

        @Override // com.discord.simpleast.core.node.Node
        public void render(SpannableStringBuilder spannableStringBuilder, R r) {
            m.checkNotNullParameter(spannableStringBuilder, "builder");
            Collection<Node<R>> children = getChildren();
            if (children != null) {
                Iterator<T> it = children.iterator();
                while (it.hasNext()) {
                    ((Node) it.next()).render(spannableStringBuilder, r);
                }
            }
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(getClass().getSimpleName());
            sb.append(" >\n");
            Collection<Node<R>> children = getChildren();
            sb.append(children != null ? u.joinToString$default(children, "\n->", ">>", "\n>|", 0, null, C0192a.j, 24, null) : null);
            return sb.toString();
        }
    }

    public Node() {
        this(null, 1, null);
    }

    public Node(Collection<Node<R>> collection) {
        this.children = collection;
    }

    public final void addChild(Node<R> node) {
        m.checkNotNullParameter(node, "child");
        Collection<Node<R>> collection = this.children;
        if (collection == null) {
            collection = new ArrayList<>();
        }
        collection.add(node);
        this.children = collection;
    }

    public final Collection<Node<R>> getChildren() {
        return this.children;
    }

    public final boolean hasChildren() {
        Collection<Node<R>> collection = this.children;
        return collection != null && (collection.isEmpty() ^ true);
    }

    public void render(SpannableStringBuilder spannableStringBuilder, R r) {
        m.checkNotNullParameter(spannableStringBuilder, "builder");
    }

    public /* synthetic */ Node(Collection collection, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? null : collection);
    }
}
