package com.discord.api.role;

import b.d.b.a.a;
import com.discord.api.guildhash.GuildHashes;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: GuildRoleUpdate.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\r\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u0019\u0010\u0016\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u000e\u001a\u0004\b\u0017\u0010\u0010R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u00188\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001c¨\u0006\u001d"}, d2 = {"Lcom/discord/api/role/GuildRoleUpdate;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "roleId", "J", "getRoleId", "()J", "Lcom/discord/api/role/GuildRole;", "role", "Lcom/discord/api/role/GuildRole;", "c", "()Lcom/discord/api/role/GuildRole;", "guildId", "b", "Lcom/discord/api/guildhash/GuildHashes;", "guildHashes", "Lcom/discord/api/guildhash/GuildHashes;", "a", "()Lcom/discord/api/guildhash/GuildHashes;", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildRoleUpdate {
    private final GuildHashes guildHashes;
    private final long guildId;
    private final GuildRole role;
    private final long roleId;

    public final GuildHashes a() {
        return this.guildHashes;
    }

    public final long b() {
        return this.guildId;
    }

    public final GuildRole c() {
        return this.role;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildRoleUpdate)) {
            return false;
        }
        GuildRoleUpdate guildRoleUpdate = (GuildRoleUpdate) obj;
        return this.guildId == guildRoleUpdate.guildId && this.roleId == guildRoleUpdate.roleId && m.areEqual(this.role, guildRoleUpdate.role) && m.areEqual(this.guildHashes, guildRoleUpdate.guildHashes);
    }

    public int hashCode() {
        long j = this.guildId;
        long j2 = this.roleId;
        int i = ((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31;
        GuildRole guildRole = this.role;
        int i2 = 0;
        int hashCode = (i + (guildRole != null ? guildRole.hashCode() : 0)) * 31;
        GuildHashes guildHashes = this.guildHashes;
        if (guildHashes != null) {
            i2 = guildHashes.hashCode();
        }
        return hashCode + i2;
    }

    public String toString() {
        StringBuilder R = a.R("GuildRoleUpdate(guildId=");
        R.append(this.guildId);
        R.append(", roleId=");
        R.append(this.roleId);
        R.append(", role=");
        R.append(this.role);
        R.append(", guildHashes=");
        R.append(this.guildHashes);
        R.append(")");
        return R.toString();
    }
}
