package com.discord.api.role;

import b.d.b.a.a;
import com.discord.nullserializable.NullSerializable;
import d0.z.d.m;
import java.io.Serializable;
import kotlin.Metadata;
/* compiled from: GuildRoleTags.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\u000b\u001a\u00020\n2\b\u0010\t\u001a\u0004\u0018\u00010\bHÖ\u0003¢\u0006\u0004\b\u000b\u0010\fR\u0013\u0010\u000f\u001a\u00020\n8F@\u0006¢\u0006\u0006\u001a\u0004\b\r\u0010\u000eR\u001e\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u001e\u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0012R\u001b\u0010\u0014\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0015\u001a\u0004\b\u0018\u0010\u0004R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u0015\u001a\u0004\b\u001a\u0010\u0004R\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u0015\u001a\u0004\b\u001c\u0010\u0004¨\u0006\u001d"}, d2 = {"Lcom/discord/api/role/GuildRoleTags;", "Ljava/io/Serializable;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "b", "()Z", "isPurchasableOrHasSubscribers", "Lcom/discord/nullserializable/NullSerializable;", "purchasableOrHasSubscribers", "Lcom/discord/nullserializable/NullSerializable;", "premiumSubscriber", "integrationId", "Ljava/lang/String;", "getIntegrationId", "skuId", "getSkuId", "botId", "getBotId", "subscriptionListingId", "a", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildRoleTags implements Serializable {
    private final String botId;
    private final String integrationId;
    private final NullSerializable<Object> premiumSubscriber;
    private final NullSerializable<Object> purchasableOrHasSubscribers;
    private final String skuId;
    private final String subscriptionListingId;

    public final String a() {
        return this.subscriptionListingId;
    }

    public final boolean b() {
        return this.purchasableOrHasSubscribers != null;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildRoleTags)) {
            return false;
        }
        GuildRoleTags guildRoleTags = (GuildRoleTags) obj;
        return m.areEqual(this.premiumSubscriber, guildRoleTags.premiumSubscriber) && m.areEqual(this.botId, guildRoleTags.botId) && m.areEqual(this.integrationId, guildRoleTags.integrationId) && m.areEqual(this.skuId, guildRoleTags.skuId) && m.areEqual(this.subscriptionListingId, guildRoleTags.subscriptionListingId) && m.areEqual(this.purchasableOrHasSubscribers, guildRoleTags.purchasableOrHasSubscribers);
    }

    public int hashCode() {
        NullSerializable<Object> nullSerializable = this.premiumSubscriber;
        int i = 0;
        int hashCode = (nullSerializable != null ? nullSerializable.hashCode() : 0) * 31;
        String str = this.botId;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.integrationId;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.skuId;
        int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.subscriptionListingId;
        int hashCode5 = (hashCode4 + (str4 != null ? str4.hashCode() : 0)) * 31;
        NullSerializable<Object> nullSerializable2 = this.purchasableOrHasSubscribers;
        if (nullSerializable2 != null) {
            i = nullSerializable2.hashCode();
        }
        return hashCode5 + i;
    }

    public String toString() {
        StringBuilder R = a.R("GuildRoleTags(premiumSubscriber=");
        R.append(this.premiumSubscriber);
        R.append(", botId=");
        R.append(this.botId);
        R.append(", integrationId=");
        R.append(this.integrationId);
        R.append(", skuId=");
        R.append(this.skuId);
        R.append(", subscriptionListingId=");
        R.append(this.subscriptionListingId);
        R.append(", purchasableOrHasSubscribers=");
        R.append(this.purchasableOrHasSubscribers);
        R.append(")");
        return R.toString();
    }
}
