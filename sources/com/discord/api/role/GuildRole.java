package com.discord.api.role;

import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.io.Serializable;
import kotlin.Metadata;
/* compiled from: GuildRole.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0019\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u001a\u0010\u000e\u001a\u00020\r2\b\u0010\f\u001a\u0004\u0018\u00010\u000bHÖ\u0003¢\u0006\u0004\b\u000e\u0010\u000fR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0007R\u0019\u0010\u0013\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0011\u001a\u0004\b\u0014\u0010\u0007R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u0019\u0010\u001a\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u0004R\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u0011\u001a\u0004\b\u001e\u0010\u0007R\u0019\u0010\u001f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u001b\u001a\u0004\b \u0010\u0004R\u0019\u0010!\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010\nR\u0019\u0010$\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'R\u0019\u0010(\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010%\u001a\u0004\b)\u0010'R\u0019\u0010*\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010%\u001a\u0004\b+\u0010'R\u0019\u0010,\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010\"\u001a\u0004\b-\u0010\n¨\u0006."}, d2 = {"Lcom/discord/api/role/GuildRole;", "Ljava/io/Serializable;", "", "a", "()J", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "unicodeEmoji", "Ljava/lang/String;", "k", ModelAuditLogEntry.CHANGE_KEY_NAME, "g", "Lcom/discord/api/role/GuildRoleTags;", ModelAuditLogEntry.CHANGE_KEY_TAGS, "Lcom/discord/api/role/GuildRoleTags;", "j", "()Lcom/discord/api/role/GuildRoleTags;", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS, "J", "h", "icon", "d", ModelAuditLogEntry.CHANGE_KEY_ID, "getId", ModelAuditLogEntry.CHANGE_KEY_COLOR, "I", "b", ModelAuditLogEntry.CHANGE_KEY_HOIST, "Z", "c", "()Z", "managed", "e", ModelAuditLogEntry.CHANGE_KEY_MENTIONABLE, "f", ModelAuditLogEntry.CHANGE_KEY_POSITION, "i", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildRole implements Serializable {
    private final int color;
    private final boolean hoist;
    private final String icon;

    /* renamed from: id  reason: collision with root package name */
    private final long f2051id;
    private final boolean managed;
    private final boolean mentionable;
    private final String name;
    private final long permissions;
    private final int position;
    private final GuildRoleTags tags;
    private final String unicodeEmoji;

    public final long a() {
        return this.f2051id;
    }

    public final int b() {
        return this.color;
    }

    public final boolean c() {
        return this.hoist;
    }

    public final String d() {
        return this.icon;
    }

    public final boolean e() {
        return this.managed;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildRole)) {
            return false;
        }
        GuildRole guildRole = (GuildRole) obj;
        return this.position == guildRole.position && m.areEqual(this.name, guildRole.name) && this.f2051id == guildRole.f2051id && this.color == guildRole.color && this.hoist == guildRole.hoist && this.permissions == guildRole.permissions && this.mentionable == guildRole.mentionable && this.managed == guildRole.managed && m.areEqual(this.icon, guildRole.icon) && m.areEqual(this.tags, guildRole.tags) && m.areEqual(this.unicodeEmoji, guildRole.unicodeEmoji);
    }

    public final boolean f() {
        return this.mentionable;
    }

    public final String g() {
        return this.name;
    }

    public final long getId() {
        return this.f2051id;
    }

    public final long h() {
        return this.permissions;
    }

    public int hashCode() {
        int i = this.position * 31;
        String str = this.name;
        int i2 = 0;
        int hashCode = str != null ? str.hashCode() : 0;
        long j = this.f2051id;
        int i3 = (((((i + hashCode) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + this.color) * 31;
        boolean z2 = this.hoist;
        int i4 = 1;
        if (z2) {
            z2 = true;
        }
        int i5 = z2 ? 1 : 0;
        int i6 = z2 ? 1 : 0;
        long j2 = this.permissions;
        int i7 = (((i3 + i5) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31;
        boolean z3 = this.mentionable;
        if (z3) {
            z3 = true;
        }
        int i8 = z3 ? 1 : 0;
        int i9 = z3 ? 1 : 0;
        int i10 = (i7 + i8) * 31;
        boolean z4 = this.managed;
        if (!z4) {
            i4 = z4 ? 1 : 0;
        }
        int i11 = (i10 + i4) * 31;
        String str2 = this.icon;
        int hashCode2 = (i11 + (str2 != null ? str2.hashCode() : 0)) * 31;
        GuildRoleTags guildRoleTags = this.tags;
        int hashCode3 = (hashCode2 + (guildRoleTags != null ? guildRoleTags.hashCode() : 0)) * 31;
        String str3 = this.unicodeEmoji;
        if (str3 != null) {
            i2 = str3.hashCode();
        }
        return hashCode3 + i2;
    }

    public final int i() {
        return this.position;
    }

    public final GuildRoleTags j() {
        return this.tags;
    }

    public final String k() {
        return this.unicodeEmoji;
    }

    public String toString() {
        StringBuilder R = a.R("GuildRole(position=");
        R.append(this.position);
        R.append(", name=");
        R.append(this.name);
        R.append(", id=");
        R.append(this.f2051id);
        R.append(", color=");
        R.append(this.color);
        R.append(", hoist=");
        R.append(this.hoist);
        R.append(", permissions=");
        R.append(this.permissions);
        R.append(", mentionable=");
        R.append(this.mentionable);
        R.append(", managed=");
        R.append(this.managed);
        R.append(", icon=");
        R.append(this.icon);
        R.append(", tags=");
        R.append(this.tags);
        R.append(", unicodeEmoji=");
        return a.H(R, this.unicodeEmoji, ")");
    }
}
