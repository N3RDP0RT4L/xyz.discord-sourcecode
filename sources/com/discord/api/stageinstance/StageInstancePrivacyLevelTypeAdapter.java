package com.discord.api.stageinstance;

import andhook.lib.HookHelper;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: StageInstancePrivacyLevel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/api/stageinstance/StageInstancePrivacyLevelTypeAdapter;", "Lcom/google/gson/TypeAdapter;", "Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StageInstancePrivacyLevelTypeAdapter extends TypeAdapter<StageInstancePrivacyLevel> {
    @Override // com.google.gson.TypeAdapter
    public StageInstancePrivacyLevel read(JsonReader jsonReader) {
        StageInstancePrivacyLevel stageInstancePrivacyLevel;
        m.checkNotNullParameter(jsonReader, "in");
        int y2 = jsonReader.y();
        StageInstancePrivacyLevel[] values = StageInstancePrivacyLevel.values();
        int i = 0;
        while (true) {
            if (i >= 3) {
                stageInstancePrivacyLevel = null;
                break;
            }
            stageInstancePrivacyLevel = values[i];
            if (stageInstancePrivacyLevel.getApiValue() == y2) {
                break;
            }
            i++;
        }
        return stageInstancePrivacyLevel != null ? stageInstancePrivacyLevel : StageInstancePrivacyLevel.INVALID;
    }

    @Override // com.google.gson.TypeAdapter
    public void write(JsonWriter jsonWriter, StageInstancePrivacyLevel stageInstancePrivacyLevel) {
        StageInstancePrivacyLevel stageInstancePrivacyLevel2 = stageInstancePrivacyLevel;
        m.checkNotNullParameter(jsonWriter, "out");
        if (stageInstancePrivacyLevel2 != null) {
            jsonWriter.D(Integer.valueOf(stageInstancePrivacyLevel2.getApiValue()));
        } else {
            jsonWriter.s();
        }
    }
}
