package com.discord.api.stageinstance;

import b.d.b.a.a;
import com.discord.api.guild.GuildFeature;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.Set;
import kotlin.Metadata;
/* compiled from: RecommendedStageInstanceGuild.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u0004R\u0019\u0010\u0010\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R\u0019\u0010\u0014\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\r\u001a\u0004\b\u0015\u0010\u0004R\u001f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00170\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001b¨\u0006\u001c"}, d2 = {"Lcom/discord/api/stageinstance/RecommendedStageInstanceGuild;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "icon", "Ljava/lang/String;", "getIcon", "", ModelAuditLogEntry.CHANGE_KEY_ID, "J", "getId", "()J", ModelAuditLogEntry.CHANGE_KEY_NAME, "getName", "", "Lcom/discord/api/guild/GuildFeature;", "features", "Ljava/util/Set;", "getFeatures", "()Ljava/util/Set;", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class RecommendedStageInstanceGuild {
    private final Set<GuildFeature> features;
    private final String icon;

    /* renamed from: id  reason: collision with root package name */
    private final long f2052id;
    private final String name;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof RecommendedStageInstanceGuild)) {
            return false;
        }
        RecommendedStageInstanceGuild recommendedStageInstanceGuild = (RecommendedStageInstanceGuild) obj;
        return this.f2052id == recommendedStageInstanceGuild.f2052id && m.areEqual(this.name, recommendedStageInstanceGuild.name) && m.areEqual(this.icon, recommendedStageInstanceGuild.icon) && m.areEqual(this.features, recommendedStageInstanceGuild.features);
    }

    public int hashCode() {
        long j = this.f2052id;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        String str = this.name;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.icon;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        Set<GuildFeature> set = this.features;
        if (set != null) {
            i2 = set.hashCode();
        }
        return hashCode2 + i2;
    }

    public String toString() {
        StringBuilder R = a.R("RecommendedStageInstanceGuild(id=");
        R.append(this.f2052id);
        R.append(", name=");
        R.append(this.name);
        R.append(", icon=");
        R.append(this.icon);
        R.append(", features=");
        R.append(this.features);
        R.append(")");
        return R.toString();
    }
}
