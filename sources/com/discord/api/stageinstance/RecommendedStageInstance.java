package com.discord.api.stageinstance;

import b.d.b.a.a;
import com.discord.api.guildmember.GuildMember;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: RecommendedStageInstance.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\b\u0007\n\u0002\u0010\u0006\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\r\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u0019\u0010\u0017\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u0007R\u0019\u0010\u001b\u001a\u00020\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u0019\u0010\u001f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u0018\u001a\u0004\b \u0010\u0007R\u001f\u0010\"\u001a\b\u0012\u0004\u0012\u00020!0\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u0014\u001a\u0004\b#\u0010\u0016R\u0019\u0010%\u001a\u00020$8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010&\u001a\u0004\b'\u0010(¨\u0006)"}, d2 = {"Lcom/discord/api/stageinstance/RecommendedStageInstance;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/stageinstance/RecommendedStageInstanceGuild;", "guild", "Lcom/discord/api/stageinstance/RecommendedStageInstanceGuild;", "getGuild", "()Lcom/discord/api/stageinstance/RecommendedStageInstanceGuild;", "", "", "speakers", "Ljava/util/List;", "d", "()Ljava/util/List;", "participantCount", "I", "b", "", "score", "D", "getScore", "()D", "source", "getSource", "Lcom/discord/api/guildmember/GuildMember;", "sampleSpeakerMembers", "c", "Lcom/discord/api/stageinstance/StageInstance;", "instance", "Lcom/discord/api/stageinstance/StageInstance;", "a", "()Lcom/discord/api/stageinstance/StageInstance;", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class RecommendedStageInstance {
    private final RecommendedStageInstanceGuild guild;
    private final StageInstance instance;
    private final int participantCount;
    private final List<GuildMember> sampleSpeakerMembers;
    private final double score;
    private final int source;
    private final List<Long> speakers;

    public final StageInstance a() {
        return this.instance;
    }

    public final int b() {
        return this.participantCount;
    }

    public final List<GuildMember> c() {
        return this.sampleSpeakerMembers;
    }

    public final List<Long> d() {
        return this.speakers;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof RecommendedStageInstance)) {
            return false;
        }
        RecommendedStageInstance recommendedStageInstance = (RecommendedStageInstance) obj;
        return m.areEqual(this.guild, recommendedStageInstance.guild) && m.areEqual(this.instance, recommendedStageInstance.instance) && m.areEqual(this.speakers, recommendedStageInstance.speakers) && m.areEqual(this.sampleSpeakerMembers, recommendedStageInstance.sampleSpeakerMembers) && this.participantCount == recommendedStageInstance.participantCount && this.source == recommendedStageInstance.source && Double.compare(this.score, recommendedStageInstance.score) == 0;
    }

    public int hashCode() {
        RecommendedStageInstanceGuild recommendedStageInstanceGuild = this.guild;
        int i = 0;
        int hashCode = (recommendedStageInstanceGuild != null ? recommendedStageInstanceGuild.hashCode() : 0) * 31;
        StageInstance stageInstance = this.instance;
        int hashCode2 = (hashCode + (stageInstance != null ? stageInstance.hashCode() : 0)) * 31;
        List<Long> list = this.speakers;
        int hashCode3 = (hashCode2 + (list != null ? list.hashCode() : 0)) * 31;
        List<GuildMember> list2 = this.sampleSpeakerMembers;
        if (list2 != null) {
            i = list2.hashCode();
        }
        long doubleToLongBits = Double.doubleToLongBits(this.score);
        return ((((((hashCode3 + i) * 31) + this.participantCount) * 31) + this.source) * 31) + ((int) (doubleToLongBits ^ (doubleToLongBits >>> 32)));
    }

    public String toString() {
        StringBuilder R = a.R("RecommendedStageInstance(guild=");
        R.append(this.guild);
        R.append(", instance=");
        R.append(this.instance);
        R.append(", speakers=");
        R.append(this.speakers);
        R.append(", sampleSpeakerMembers=");
        R.append(this.sampleSpeakerMembers);
        R.append(", participantCount=");
        R.append(this.participantCount);
        R.append(", source=");
        R.append(this.source);
        R.append(", score=");
        R.append(this.score);
        R.append(")");
        return R.toString();
    }
}
