package com.discord.api.stageinstance;

import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: StageInstance.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\r\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0012\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0004R\u0019\u0010\u0019\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u0013\u001a\u0004\b\u001a\u0010\u0015R\u0019\u0010\u001b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u0017\u001a\u0004\b\u001c\u0010\u0004R\u0019\u0010\u001d\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u0013\u001a\u0004\b\u001e\u0010\u0015¨\u0006\u001f"}, d2 = {"Lcom/discord/api/stageinstance/StageInstance;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;", "privacyLevel", "Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;", "e", "()Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;", "", "guildId", "J", "b", "()J", "inviteCode", "Ljava/lang/String;", "d", ModelAuditLogEntry.CHANGE_KEY_ID, "c", ModelAuditLogEntry.CHANGE_KEY_TOPIC, "f", "channelId", "a", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StageInstance {
    private final long channelId;
    private final long guildId;

    /* renamed from: id  reason: collision with root package name */
    private final long f2053id;
    private final String inviteCode;
    private final StageInstancePrivacyLevel privacyLevel;
    private final String topic;

    public final long a() {
        return this.channelId;
    }

    public final long b() {
        return this.guildId;
    }

    public final long c() {
        return this.f2053id;
    }

    public final String d() {
        return this.inviteCode;
    }

    public final StageInstancePrivacyLevel e() {
        return this.privacyLevel;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StageInstance)) {
            return false;
        }
        StageInstance stageInstance = (StageInstance) obj;
        return this.f2053id == stageInstance.f2053id && this.guildId == stageInstance.guildId && this.channelId == stageInstance.channelId && m.areEqual(this.topic, stageInstance.topic) && m.areEqual(this.privacyLevel, stageInstance.privacyLevel) && m.areEqual(this.inviteCode, stageInstance.inviteCode);
    }

    public final String f() {
        return this.topic;
    }

    public int hashCode() {
        long j = this.f2053id;
        long j2 = this.guildId;
        long j3 = this.channelId;
        int i = ((((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31;
        String str = this.topic;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        StageInstancePrivacyLevel stageInstancePrivacyLevel = this.privacyLevel;
        int hashCode2 = (hashCode + (stageInstancePrivacyLevel != null ? stageInstancePrivacyLevel.hashCode() : 0)) * 31;
        String str2 = this.inviteCode;
        if (str2 != null) {
            i2 = str2.hashCode();
        }
        return hashCode2 + i2;
    }

    public String toString() {
        StringBuilder R = a.R("StageInstance(id=");
        R.append(this.f2053id);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", channelId=");
        R.append(this.channelId);
        R.append(", topic=");
        R.append(this.topic);
        R.append(", privacyLevel=");
        R.append(this.privacyLevel);
        R.append(", inviteCode=");
        return a.H(R, this.inviteCode, ")");
    }
}
