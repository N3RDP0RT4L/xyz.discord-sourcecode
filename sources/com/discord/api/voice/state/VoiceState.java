package com.discord.api.voice.state;

import b.d.b.a.a;
import com.discord.api.guildmember.GuildMember;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: VoiceState.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0011\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\f\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000fR\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u0019\u0010\u0018\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\r\u001a\u0004\b\u0019\u0010\u000fR\u0019\u0010\u001a\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\r\u001a\u0004\b\u001b\u0010\u000fR\u0019\u0010\u001c\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\r\u001a\u0004\b\u001d\u0010\u000fR\u0019\u0010\u001f\u001a\u00020\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"R\u001b\u0010$\u001a\u0004\u0018\u00010#8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'R\u0019\u0010(\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010\r\u001a\u0004\b)\u0010\u000fR\u0019\u0010*\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010\r\u001a\u0004\b+\u0010\u000fR\u0019\u0010,\u001a\u00020\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010 \u001a\u0004\b-\u0010\"R\u001b\u0010.\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010/\u001a\u0004\b0\u00101R\u0019\u00102\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010\r\u001a\u0004\b3\u0010\u000f¨\u00064"}, d2 = {"Lcom/discord/api/voice/state/VoiceState;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "selfStream", "Z", "i", "()Z", "Lcom/discord/api/guildmember/GuildMember;", "member", "Lcom/discord/api/guildmember/GuildMember;", "d", "()Lcom/discord/api/guildmember/GuildMember;", "sessionId", "Ljava/lang/String;", "k", "selfMute", "h", ModelAuditLogEntry.CHANGE_KEY_DEAF, "b", ModelAuditLogEntry.CHANGE_KEY_MUTE, "e", "", "userId", "J", "m", "()J", "Lcom/discord/api/utcdatetime/UtcDateTime;", "requestToSpeakTimestamp", "Lcom/discord/api/utcdatetime/UtcDateTime;", "f", "()Lcom/discord/api/utcdatetime/UtcDateTime;", "selfDeaf", "g", "suppress", "l", "guildId", "c", "channelId", "Ljava/lang/Long;", "a", "()Ljava/lang/Long;", "selfVideo", "j", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class VoiceState {
    private final Long channelId;
    private final boolean deaf;
    private final long guildId;
    private final GuildMember member;
    private final boolean mute;
    private final UtcDateTime requestToSpeakTimestamp;
    private final boolean selfDeaf;
    private final boolean selfMute;
    private final boolean selfStream;
    private final boolean selfVideo;
    private final String sessionId;
    private final boolean suppress;
    private final long userId;

    public final Long a() {
        return this.channelId;
    }

    public final boolean b() {
        return this.deaf;
    }

    public final long c() {
        return this.guildId;
    }

    public final GuildMember d() {
        return this.member;
    }

    public final boolean e() {
        return this.mute;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof VoiceState)) {
            return false;
        }
        VoiceState voiceState = (VoiceState) obj;
        return this.userId == voiceState.userId && this.suppress == voiceState.suppress && m.areEqual(this.sessionId, voiceState.sessionId) && this.selfMute == voiceState.selfMute && this.selfDeaf == voiceState.selfDeaf && this.selfVideo == voiceState.selfVideo && this.selfStream == voiceState.selfStream && this.mute == voiceState.mute && this.guildId == voiceState.guildId && this.deaf == voiceState.deaf && m.areEqual(this.member, voiceState.member) && m.areEqual(this.requestToSpeakTimestamp, voiceState.requestToSpeakTimestamp) && m.areEqual(this.channelId, voiceState.channelId);
    }

    public final UtcDateTime f() {
        return this.requestToSpeakTimestamp;
    }

    public final boolean g() {
        return this.selfDeaf;
    }

    public final boolean h() {
        return this.selfMute;
    }

    public int hashCode() {
        long j = this.userId;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        boolean z2 = this.suppress;
        int i2 = 1;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        int i5 = (i + i3) * 31;
        String str = this.sessionId;
        int i6 = 0;
        int hashCode = (i5 + (str != null ? str.hashCode() : 0)) * 31;
        boolean z3 = this.selfMute;
        if (z3) {
            z3 = true;
        }
        int i7 = z3 ? 1 : 0;
        int i8 = z3 ? 1 : 0;
        int i9 = (hashCode + i7) * 31;
        boolean z4 = this.selfDeaf;
        if (z4) {
            z4 = true;
        }
        int i10 = z4 ? 1 : 0;
        int i11 = z4 ? 1 : 0;
        int i12 = (i9 + i10) * 31;
        boolean z5 = this.selfVideo;
        if (z5) {
            z5 = true;
        }
        int i13 = z5 ? 1 : 0;
        int i14 = z5 ? 1 : 0;
        int i15 = (i12 + i13) * 31;
        boolean z6 = this.selfStream;
        if (z6) {
            z6 = true;
        }
        int i16 = z6 ? 1 : 0;
        int i17 = z6 ? 1 : 0;
        int i18 = (i15 + i16) * 31;
        boolean z7 = this.mute;
        if (z7) {
            z7 = true;
        }
        int i19 = z7 ? 1 : 0;
        int i20 = z7 ? 1 : 0;
        long j2 = this.guildId;
        int i21 = (((i18 + i19) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31;
        boolean z8 = this.deaf;
        if (!z8) {
            i2 = z8 ? 1 : 0;
        }
        int i22 = (i21 + i2) * 31;
        GuildMember guildMember = this.member;
        int hashCode2 = (i22 + (guildMember != null ? guildMember.hashCode() : 0)) * 31;
        UtcDateTime utcDateTime = this.requestToSpeakTimestamp;
        int hashCode3 = (hashCode2 + (utcDateTime != null ? utcDateTime.hashCode() : 0)) * 31;
        Long l = this.channelId;
        if (l != null) {
            i6 = l.hashCode();
        }
        return hashCode3 + i6;
    }

    public final boolean i() {
        return this.selfStream;
    }

    public final boolean j() {
        return this.selfVideo;
    }

    public final String k() {
        return this.sessionId;
    }

    public final boolean l() {
        return this.suppress;
    }

    public final long m() {
        return this.userId;
    }

    public String toString() {
        StringBuilder R = a.R("VoiceState(userId=");
        R.append(this.userId);
        R.append(", suppress=");
        R.append(this.suppress);
        R.append(", sessionId=");
        R.append(this.sessionId);
        R.append(", selfMute=");
        R.append(this.selfMute);
        R.append(", selfDeaf=");
        R.append(this.selfDeaf);
        R.append(", selfVideo=");
        R.append(this.selfVideo);
        R.append(", selfStream=");
        R.append(this.selfStream);
        R.append(", mute=");
        R.append(this.mute);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", deaf=");
        R.append(this.deaf);
        R.append(", member=");
        R.append(this.member);
        R.append(", requestToSpeakTimestamp=");
        R.append(this.requestToSpeakTimestamp);
        R.append(", channelId=");
        return a.F(R, this.channelId, ")");
    }
}
