package com.discord.api.interaction;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.user.User;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: Interaction.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\b\u0086\b\u0018\u0000 $2\u00020\u0001:\u0001$BC\u0012\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0013\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u0018\u0012\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u001d¢\u0006\u0004\b\"\u0010#J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\f\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000fR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004R\u001b\u0010\u0014\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u00188\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001cR\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010!¨\u0006%"}, d2 = {"Lcom/discord/api/interaction/Interaction;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "type", "Ljava/lang/Integer;", "getType", "()Ljava/lang/Integer;", ModelAuditLogEntry.CHANGE_KEY_NAME, "Ljava/lang/String;", "b", "", ModelAuditLogEntry.CHANGE_KEY_ID, "Ljava/lang/Long;", "a", "()Ljava/lang/Long;", "Lcom/discord/api/user/User;", "user", "Lcom/discord/api/user/User;", "c", "()Lcom/discord/api/user/User;", "Lcom/discord/api/interaction/Member;", "member", "Lcom/discord/api/interaction/Member;", "getMember", "()Lcom/discord/api/interaction/Member;", HookHelper.constructorName, "(Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/String;Lcom/discord/api/user/User;Lcom/discord/api/interaction/Member;)V", "Companion", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class Interaction {
    public static final Companion Companion = new Companion(null);
    public static final int TYPE_INTERACTION_APPLICATION_COMMAND = 2;
    public static final int TYPE_INTERACTION_PING = 1;

    /* renamed from: id  reason: collision with root package name */
    private final Long f2039id;
    private final Member member;
    private final String name;
    private final Integer type;
    private final User user;

    /* compiled from: Interaction.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/api/interaction/Interaction$Companion;", "", "", "TYPE_INTERACTION_APPLICATION_COMMAND", "I", "TYPE_INTERACTION_PING", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        public Companion() {
        }

        public Companion(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    public Interaction() {
        this.f2039id = null;
        this.type = null;
        this.name = null;
        this.user = null;
        this.member = null;
    }

    public final Long a() {
        return this.f2039id;
    }

    public final String b() {
        return this.name;
    }

    public final User c() {
        return this.user;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Interaction)) {
            return false;
        }
        Interaction interaction = (Interaction) obj;
        return m.areEqual(this.f2039id, interaction.f2039id) && m.areEqual(this.type, interaction.type) && m.areEqual(this.name, interaction.name) && m.areEqual(this.user, interaction.user) && m.areEqual(this.member, interaction.member);
    }

    public int hashCode() {
        Long l = this.f2039id;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Integer num = this.type;
        int hashCode2 = (hashCode + (num != null ? num.hashCode() : 0)) * 31;
        String str = this.name;
        int hashCode3 = (hashCode2 + (str != null ? str.hashCode() : 0)) * 31;
        User user = this.user;
        int hashCode4 = (hashCode3 + (user != null ? user.hashCode() : 0)) * 31;
        Member member = this.member;
        if (member != null) {
            i = member.hashCode();
        }
        return hashCode4 + i;
    }

    public String toString() {
        StringBuilder R = a.R("Interaction(id=");
        R.append(this.f2039id);
        R.append(", type=");
        R.append(this.type);
        R.append(", name=");
        R.append(this.name);
        R.append(", user=");
        R.append(this.user);
        R.append(", member=");
        R.append(this.member);
        R.append(")");
        return R.toString();
    }

    public Interaction(Long l, Integer num, String str, User user, Member member) {
        this.f2039id = l;
        this.type = num;
        this.name = str;
        this.user = user;
        this.member = null;
    }
}
