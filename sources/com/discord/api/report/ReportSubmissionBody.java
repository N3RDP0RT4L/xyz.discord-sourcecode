package com.discord.api.report;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.t.h0;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: InAppReportsMenu.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\f\n\u0002\u0010$\n\u0002\u0010 \n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\b\u0018\u0000 12\u00020\u0001:\u00011J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u0004R\u0019\u0010\u0010\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R\u001b\u0010\u0014\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017R\u0019\u0010\u0018\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\r\u001a\u0004\b\u0019\u0010\u0004R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0015\u001a\u0004\b\u001b\u0010\u0017R+\u0010\u001e\u001a\u0014\u0012\u0004\u0012\u00020\u0002\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00020\u001d0\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010!R\u001b\u0010\"\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u0015\u001a\u0004\b#\u0010\u0017R\u0019\u0010$\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\r\u001a\u0004\b%\u0010\u0004R\u001b\u0010&\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010\u0015\u001a\u0004\b'\u0010\u0017R\u0019\u0010(\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010\r\u001a\u0004\b)\u0010\u0004R\u001b\u0010*\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010\u0015\u001a\u0004\b+\u0010\u0017R#\u0010-\u001a\f\u0012\b\u0012\u00060\u0005j\u0002`,0\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010.\u001a\u0004\b/\u00100¨\u00062"}, d2 = {"Lcom/discord/api/report/ReportSubmissionBody;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "language", "Ljava/lang/String;", "getLanguage", "", ModelAuditLogEntry.CHANGE_KEY_ID, "J", "getId", "()J", "channelId", "Ljava/lang/Long;", "getChannelId", "()Ljava/lang/Long;", "variant", "getVariant", "hubId", "getHubId", "", "", "elements", "Ljava/util/Map;", "getElements", "()Ljava/util/Map;", "guildScheduledEventId", "getGuildScheduledEventId", ModelAuditLogEntry.CHANGE_KEY_NAME, "getName", "messageId", "getMessageId", "version", "getVersion", "guildId", "getGuildId", "Lcom/discord/api/report/ReportNodeRef;", "breadcrumbs", "Ljava/util/List;", "getBreadcrumbs", "()Ljava/util/List;", "Companion", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ReportSubmissionBody {
    public static final Companion Companion = new Companion(null);
    private final List<Integer> breadcrumbs;
    private final Long channelId;
    private final Map<String, List<String>> elements;
    private final Long guildId;
    private final Long guildScheduledEventId;
    private final Long hubId;

    /* renamed from: id  reason: collision with root package name */
    private final long f2050id;
    private final String language;
    private final Long messageId;
    private final String name;
    private final String variant;
    private final String version;

    /* compiled from: InAppReportsMenu.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rJC\u0010\n\u001a*\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0006j\u0002`\u00070\u0002\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00020\t\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\u00020\b0\u00052\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/api/report/ReportSubmissionBody$Companion;", "", "", "Lcom/discord/api/report/NodeResult;", "results", "Lkotlin/Pair;", "", "Lcom/discord/api/report/ReportNodeRef;", "", "", "a", "(Ljava/util/List;)Lkotlin/Pair;", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        public Companion() {
        }

        public final Pair<List<Integer>, Map<String, List<String>>> a(List<NodeResult> list) {
            Pair pair;
            m.checkNotNullParameter(list, "results");
            ArrayList arrayList = new ArrayList();
            Iterator<T> it = list.iterator();
            while (true) {
                Integer num = null;
                if (!it.hasNext()) {
                    break;
                }
                ReportNodeChild a = ((NodeResult) it.next()).a();
                if (a != null) {
                    num = Integer.valueOf(a.b());
                }
                if (num != null) {
                    arrayList.add(num);
                }
            }
            ArrayList arrayList2 = new ArrayList();
            for (NodeResult nodeResult : list) {
                NodeElementResult b2 = nodeResult.b();
                if (b2 != null) {
                    String b3 = b2.b();
                    List<ReportNodeElementData> a2 = b2.a();
                    ArrayList arrayList3 = new ArrayList(o.collectionSizeOrDefault(a2, 10));
                    for (ReportNodeElementData reportNodeElementData : a2) {
                        arrayList3.add(reportNodeElementData.a());
                    }
                    pair = d0.o.to(b3, arrayList3);
                } else {
                    pair = null;
                }
                if (pair != null) {
                    arrayList2.add(pair);
                }
            }
            return d0.o.to(arrayList, h0.toMap(arrayList2));
        }

        public Companion(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    public ReportSubmissionBody(long j, Long l, Long l2, Long l3, Long l4, Long l5, String str, String str2, String str3, String str4, List list, Map map, int i) {
        Long l6 = null;
        Long l7 = (i & 2) != 0 ? null : l;
        Long l8 = (i & 4) != 0 ? null : l2;
        Long l9 = (i & 8) != 0 ? null : l3;
        Long l10 = (i & 16) != 0 ? null : l4;
        l6 = (i & 32) == 0 ? l5 : l6;
        m.checkNotNullParameter(str, "language");
        m.checkNotNullParameter(str2, "variant");
        m.checkNotNullParameter(str3, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(str4, "version");
        m.checkNotNullParameter(list, "breadcrumbs");
        m.checkNotNullParameter(map, "elements");
        this.f2050id = j;
        this.messageId = l7;
        this.channelId = l8;
        this.guildId = l9;
        this.hubId = l10;
        this.guildScheduledEventId = l6;
        this.language = str;
        this.variant = str2;
        this.name = str3;
        this.version = str4;
        this.breadcrumbs = list;
        this.elements = map;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ReportSubmissionBody)) {
            return false;
        }
        ReportSubmissionBody reportSubmissionBody = (ReportSubmissionBody) obj;
        return this.f2050id == reportSubmissionBody.f2050id && m.areEqual(this.messageId, reportSubmissionBody.messageId) && m.areEqual(this.channelId, reportSubmissionBody.channelId) && m.areEqual(this.guildId, reportSubmissionBody.guildId) && m.areEqual(this.hubId, reportSubmissionBody.hubId) && m.areEqual(this.guildScheduledEventId, reportSubmissionBody.guildScheduledEventId) && m.areEqual(this.language, reportSubmissionBody.language) && m.areEqual(this.variant, reportSubmissionBody.variant) && m.areEqual(this.name, reportSubmissionBody.name) && m.areEqual(this.version, reportSubmissionBody.version) && m.areEqual(this.breadcrumbs, reportSubmissionBody.breadcrumbs) && m.areEqual(this.elements, reportSubmissionBody.elements);
    }

    public int hashCode() {
        long j = this.f2050id;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        Long l = this.messageId;
        int i2 = 0;
        int hashCode = (i + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.channelId;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.guildId;
        int hashCode3 = (hashCode2 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.hubId;
        int hashCode4 = (hashCode3 + (l4 != null ? l4.hashCode() : 0)) * 31;
        Long l5 = this.guildScheduledEventId;
        int hashCode5 = (hashCode4 + (l5 != null ? l5.hashCode() : 0)) * 31;
        String str = this.language;
        int hashCode6 = (hashCode5 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.variant;
        int hashCode7 = (hashCode6 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.name;
        int hashCode8 = (hashCode7 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.version;
        int hashCode9 = (hashCode8 + (str4 != null ? str4.hashCode() : 0)) * 31;
        List<Integer> list = this.breadcrumbs;
        int hashCode10 = (hashCode9 + (list != null ? list.hashCode() : 0)) * 31;
        Map<String, List<String>> map = this.elements;
        if (map != null) {
            i2 = map.hashCode();
        }
        return hashCode10 + i2;
    }

    public String toString() {
        StringBuilder R = a.R("ReportSubmissionBody(id=");
        R.append(this.f2050id);
        R.append(", messageId=");
        R.append(this.messageId);
        R.append(", channelId=");
        R.append(this.channelId);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", hubId=");
        R.append(this.hubId);
        R.append(", guildScheduledEventId=");
        R.append(this.guildScheduledEventId);
        R.append(", language=");
        R.append(this.language);
        R.append(", variant=");
        R.append(this.variant);
        R.append(", name=");
        R.append(this.name);
        R.append(", version=");
        R.append(this.version);
        R.append(", breadcrumbs=");
        R.append(this.breadcrumbs);
        R.append(", elements=");
        return a.L(R, this.elements, ")");
    }
}
