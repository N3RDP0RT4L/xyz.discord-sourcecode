package com.discord.api.report;

import andhook.lib.HookHelper;
import com.discord.api.report.ReportNodeBottomButton;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import d0.y.b;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
/* compiled from: InAppReportsMenu.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/api/report/ReportNodeBottomButtonTypeAdapter;", "Lcom/google/gson/TypeAdapter;", "Lcom/discord/api/report/ReportNodeBottomButton;", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ReportNodeBottomButtonTypeAdapter extends TypeAdapter<ReportNodeBottomButton> {

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            JsonToken.values();
            int[] iArr = new int[10];
            $EnumSwitchMapping$0 = iArr;
            iArr[6] = 1;
            iArr[5] = 2;
        }
    }

    @Override // com.google.gson.TypeAdapter
    public ReportNodeBottomButton read(JsonReader jsonReader) {
        m.checkNotNullParameter(jsonReader, "reader");
        if (jsonReader.N() == JsonToken.NULL) {
            jsonReader.U();
            return null;
        }
        jsonReader.b();
        String str = "";
        Object obj = str;
        while (jsonReader.q()) {
            String C = jsonReader.C();
            if (C != null) {
                int hashCode = C.hashCode();
                if (hashCode != -880905839) {
                    if (hashCode == 3575610 && C.equals("type")) {
                        str = jsonReader.J();
                        m.checkNotNullExpressionValue(str, "reader.nextString()");
                    }
                } else if (C.equals("target")) {
                    JsonToken N = jsonReader.N();
                    if (N != null) {
                        int ordinal = N.ordinal();
                        if (ordinal == 5) {
                            obj = jsonReader.J();
                        } else if (ordinal == 6) {
                            obj = Integer.valueOf(jsonReader.y());
                        }
                        m.checkNotNullExpressionValue(obj, "when (reader.peek()) {\n …der.skipValue()\n        }");
                    }
                    jsonReader.U();
                    obj = Unit.a;
                    m.checkNotNullExpressionValue(obj, "when (reader.peek()) {\n …der.skipValue()\n        }");
                }
            }
            jsonReader.U();
        }
        jsonReader.f();
        switch (str.hashCode()) {
            case -1367724422:
                if (str.equals("cancel")) {
                    return ReportNodeBottomButton.Cancel.INSTANCE;
                }
                return null;
            case -891535336:
                if (str.equals("submit")) {
                    return ReportNodeBottomButton.Submit.INSTANCE;
                }
                return null;
            case 3089282:
                if (str.equals("done")) {
                    return ReportNodeBottomButton.Done.INSTANCE;
                }
                return null;
            case 3377907:
                if (!str.equals("next")) {
                    return null;
                }
                Objects.requireNonNull(obj, "null cannot be cast to non-null type kotlin.Int");
                return new ReportNodeBottomButton.Next(((Integer) obj).intValue());
            default:
                return null;
        }
    }

    /* JADX WARN: Finally extract failed */
    @Override // com.google.gson.TypeAdapter
    public void write(JsonWriter jsonWriter, ReportNodeBottomButton reportNodeBottomButton) {
        ReportNodeBottomButton reportNodeBottomButton2 = reportNodeBottomButton;
        m.checkNotNullParameter(jsonWriter, "out");
        if (reportNodeBottomButton2 == null) {
            jsonWriter.s();
            return;
        }
        JsonWriter n = jsonWriter.c().n("type").H(reportNodeBottomButton2.a()).n("target");
        try {
            JsonWriter D = reportNodeBottomButton2 instanceof ReportNodeBottomButton.Next ? n.D(Integer.valueOf(((ReportNodeBottomButton.Next) reportNodeBottomButton2).b())) : n.s();
            b.closeFinally(n, null);
            D.f();
        } catch (Throwable th) {
            try {
                throw th;
            } catch (Throwable th2) {
                b.closeFinally(n, th);
                throw th2;
            }
        }
    }
}
