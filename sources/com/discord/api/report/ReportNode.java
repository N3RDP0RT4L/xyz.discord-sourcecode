package com.discord.api.report;

import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: InAppReportsMenu.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u0004R\u001f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\r\u001a\u0004\b\u0016\u0010\u0004R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u001d\u0010\u001d\u001a\u00060\u0005j\u0002`\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010\u0007R\u001f\u0010!\u001a\b\u0012\u0004\u0012\u00020 0\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u0012\u001a\u0004\b\"\u0010\u0014R\u001b\u0010#\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\r\u001a\u0004\b$\u0010\u0004R\u001b\u0010%\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\r\u001a\u0004\b&\u0010\u0004R\u0019\u0010'\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010\r\u001a\u0004\b(\u0010\u0004¨\u0006)"}, d2 = {"Lcom/discord/api/report/ReportNode;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "Ljava/lang/String;", "getDescription", "", "Lcom/discord/api/report/ReportNodeElement;", "elements", "Ljava/util/List;", "c", "()Ljava/util/List;", "subheader", "g", "Lcom/discord/api/report/ReportNodeBottomButton;", "button", "Lcom/discord/api/report/ReportNodeBottomButton;", "a", "()Lcom/discord/api/report/ReportNodeBottomButton;", "Lcom/discord/api/report/ReportNodeRef;", ModelAuditLogEntry.CHANGE_KEY_ID, "I", "e", "Lcom/discord/api/report/ReportNodeChild;", "children", "b", "menu_name", "getMenu_name", "info", "f", "header", "d", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ReportNode {
    private final ReportNodeBottomButton button;
    private final List<ReportNodeChild> children;
    private final String description;
    private final List<ReportNodeElement> elements;
    private final String header;

    /* renamed from: id  reason: collision with root package name */
    private final int f2049id;
    private final String info;
    private final String menu_name;
    private final String subheader;

    public final ReportNodeBottomButton a() {
        return this.button;
    }

    public final List<ReportNodeChild> b() {
        return this.children;
    }

    public final List<ReportNodeElement> c() {
        return this.elements;
    }

    public final String d() {
        return this.header;
    }

    public final int e() {
        return this.f2049id;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ReportNode)) {
            return false;
        }
        ReportNode reportNode = (ReportNode) obj;
        return this.f2049id == reportNode.f2049id && m.areEqual(this.menu_name, reportNode.menu_name) && m.areEqual(this.header, reportNode.header) && m.areEqual(this.subheader, reportNode.subheader) && m.areEqual(this.info, reportNode.info) && m.areEqual(this.description, reportNode.description) && m.areEqual(this.children, reportNode.children) && m.areEqual(this.elements, reportNode.elements) && m.areEqual(this.button, reportNode.button);
    }

    public final String f() {
        return this.info;
    }

    public final String g() {
        return this.subheader;
    }

    public int hashCode() {
        int i = this.f2049id * 31;
        String str = this.menu_name;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.header;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.subheader;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.info;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.description;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        List<ReportNodeChild> list = this.children;
        int hashCode6 = (hashCode5 + (list != null ? list.hashCode() : 0)) * 31;
        List<ReportNodeElement> list2 = this.elements;
        int hashCode7 = (hashCode6 + (list2 != null ? list2.hashCode() : 0)) * 31;
        ReportNodeBottomButton reportNodeBottomButton = this.button;
        if (reportNodeBottomButton != null) {
            i2 = reportNodeBottomButton.hashCode();
        }
        return hashCode7 + i2;
    }

    public String toString() {
        StringBuilder R = a.R("ReportNode(id=");
        R.append(this.f2049id);
        R.append(", menu_name=");
        R.append(this.menu_name);
        R.append(", header=");
        R.append(this.header);
        R.append(", subheader=");
        R.append(this.subheader);
        R.append(", info=");
        R.append(this.info);
        R.append(", description=");
        R.append(this.description);
        R.append(", children=");
        R.append(this.children);
        R.append(", elements=");
        R.append(this.elements);
        R.append(", button=");
        R.append(this.button);
        R.append(")");
        return R.toString();
    }
}
