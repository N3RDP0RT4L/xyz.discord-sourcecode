package com.discord.api.report;

import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.Map;
import kotlin.Metadata;
/* compiled from: InAppReportsMenu.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u0004R\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\r\u001a\u0004\b\u0010\u0010\u0004R\u0019\u0010\u0011\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\r\u001a\u0004\b\u0012\u0010\u0004R\u0019\u0010\u0013\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\r\u001a\u0004\b\u0014\u0010\u0004R)\u0010\u0018\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0016\u0012\u0004\u0012\u00020\u00170\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u0019\u0010\u001c\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\r\u001a\u0004\b\u001d\u0010\u0004R\u001d\u0010\u001e\u001a\u00060\u0005j\u0002`\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010\u0007R\u001d\u0010!\u001a\u00060\u0005j\u0002`\u00168\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u001f\u001a\u0004\b\"\u0010\u0007¨\u0006#"}, d2 = {"Lcom/discord/api/report/MenuAPIResponse;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "language", "Ljava/lang/String;", "a", "version", "g", "variant", "f", "postback_url", "getPostback_url", "", "Lcom/discord/api/report/ReportNodeRef;", "Lcom/discord/api/report/ReportNode;", "nodes", "Ljava/util/Map;", "c", "()Ljava/util/Map;", ModelAuditLogEntry.CHANGE_KEY_NAME, "b", "success_node_id", "I", "e", "root_node_id", "d", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class MenuAPIResponse {
    private final String language;
    private final String name;
    private final Map<Integer, ReportNode> nodes;
    private final String postback_url;
    private final int root_node_id;
    private final int success_node_id;
    private final String variant;
    private final String version;

    public final String a() {
        return this.language;
    }

    public final String b() {
        return this.name;
    }

    public final Map<Integer, ReportNode> c() {
        return this.nodes;
    }

    public final int d() {
        return this.root_node_id;
    }

    public final int e() {
        return this.success_node_id;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MenuAPIResponse)) {
            return false;
        }
        MenuAPIResponse menuAPIResponse = (MenuAPIResponse) obj;
        return m.areEqual(this.name, menuAPIResponse.name) && m.areEqual(this.variant, menuAPIResponse.variant) && m.areEqual(this.version, menuAPIResponse.version) && m.areEqual(this.language, menuAPIResponse.language) && this.success_node_id == menuAPIResponse.success_node_id && this.root_node_id == menuAPIResponse.root_node_id && m.areEqual(this.postback_url, menuAPIResponse.postback_url) && m.areEqual(this.nodes, menuAPIResponse.nodes);
    }

    public final String f() {
        return this.variant;
    }

    public final String g() {
        return this.version;
    }

    public int hashCode() {
        String str = this.name;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.variant;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.version;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.language;
        int hashCode4 = (((((hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31) + this.success_node_id) * 31) + this.root_node_id) * 31;
        String str5 = this.postback_url;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        Map<Integer, ReportNode> map = this.nodes;
        if (map != null) {
            i = map.hashCode();
        }
        return hashCode5 + i;
    }

    public String toString() {
        StringBuilder R = a.R("MenuAPIResponse(name=");
        R.append(this.name);
        R.append(", variant=");
        R.append(this.variant);
        R.append(", version=");
        R.append(this.version);
        R.append(", language=");
        R.append(this.language);
        R.append(", success_node_id=");
        R.append(this.success_node_id);
        R.append(", root_node_id=");
        R.append(this.root_node_id);
        R.append(", postback_url=");
        R.append(this.postback_url);
        R.append(", nodes=");
        return a.L(R, this.nodes, ")");
    }
}
