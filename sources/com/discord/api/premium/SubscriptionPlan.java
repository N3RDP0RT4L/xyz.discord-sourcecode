package com.discord.api.premium;

import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: SubscriptionPlan.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u0007R\u0016\u0010\u000f\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0012\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u0019\u0010\u0016\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\r\u001a\u0004\b\u0017\u0010\u0007R\u0019\u0010\u0019\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001c¨\u0006\u001d"}, d2 = {"Lcom/discord/api/premium/SubscriptionPlan;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "price", "I", "d", ModelAuditLogEntry.CHANGE_KEY_NAME, "Ljava/lang/String;", "", ModelAuditLogEntry.CHANGE_KEY_ID, "J", "a", "()J", "intervalCount", "c", "Lcom/discord/api/premium/SubscriptionInterval;", "interval", "Lcom/discord/api/premium/SubscriptionInterval;", "b", "()Lcom/discord/api/premium/SubscriptionInterval;", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class SubscriptionPlan {

    /* renamed from: id  reason: collision with root package name */
    private final long f2048id;
    private final SubscriptionInterval interval;
    private final int intervalCount;
    private final String name;
    private final int price;

    public final long a() {
        return this.f2048id;
    }

    public final SubscriptionInterval b() {
        return this.interval;
    }

    public final int c() {
        return this.intervalCount;
    }

    public final int d() {
        return this.price;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SubscriptionPlan)) {
            return false;
        }
        SubscriptionPlan subscriptionPlan = (SubscriptionPlan) obj;
        return this.f2048id == subscriptionPlan.f2048id && this.intervalCount == subscriptionPlan.intervalCount && m.areEqual(this.interval, subscriptionPlan.interval) && m.areEqual(this.name, subscriptionPlan.name) && this.price == subscriptionPlan.price;
    }

    public int hashCode() {
        long j = this.f2048id;
        int i = ((((int) (j ^ (j >>> 32))) * 31) + this.intervalCount) * 31;
        SubscriptionInterval subscriptionInterval = this.interval;
        int i2 = 0;
        int hashCode = (i + (subscriptionInterval != null ? subscriptionInterval.hashCode() : 0)) * 31;
        String str = this.name;
        if (str != null) {
            i2 = str.hashCode();
        }
        return ((hashCode + i2) * 31) + this.price;
    }

    public String toString() {
        StringBuilder R = a.R("SubscriptionPlan(id=");
        R.append(this.f2048id);
        R.append(", intervalCount=");
        R.append(this.intervalCount);
        R.append(", interval=");
        R.append(this.interval);
        R.append(", name=");
        R.append(this.name);
        R.append(", price=");
        return a.A(R, this.price, ")");
    }
}
