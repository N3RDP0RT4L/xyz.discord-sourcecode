package com.discord.api.premium;

import b.d.b.a.a;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
/* compiled from: ClaimedOutboundPromotion.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0013\u0010\u000f\u001a\u00020\f8F@\u0006¢\u0006\u0006\u001a\u0004\b\r\u0010\u000eR\u0019\u0010\u0010\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u000eR\u001d\u0010\u0015\u001a\u00060\u0013j\u0002`\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018R\u0019\u0010\u001a\u001a\u00020\u00198\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u001dR\u0013\u0010\u001f\u001a\u00020\u00028F@\u0006¢\u0006\u0006\u001a\u0004\b\u001e\u0010\u0004R\u0019\u0010 \u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010\u0004¨\u0006#"}, d2 = {"Lcom/discord/api/premium/ClaimedOutboundPromotion;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/utcdatetime/UtcDateTime;", "c", "()Lcom/discord/api/utcdatetime/UtcDateTime;", "redeemByDate", "claimedAt", "Lcom/discord/api/utcdatetime/UtcDateTime;", "getClaimedAt", "", "Lcom/discord/primitives/UserId;", "userId", "J", "getUserId", "()J", "Lcom/discord/api/premium/OutboundPromotion;", "promotion", "Lcom/discord/api/premium/OutboundPromotion;", "b", "()Lcom/discord/api/premium/OutboundPromotion;", "d", "redemptionLink", ModelAuditLogEntry.CHANGE_KEY_CODE, "Ljava/lang/String;", "a", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ClaimedOutboundPromotion {
    private final UtcDateTime claimedAt;
    private final String code;
    private final OutboundPromotion promotion;
    private final long userId;

    public final String a() {
        return this.code;
    }

    public final OutboundPromotion b() {
        return this.promotion;
    }

    public final UtcDateTime c() {
        return new UtcDateTime(TimeUnit.DAYS.toMillis(30L) + this.promotion.a().g());
    }

    /* JADX WARN: Code restructure failed: missing block: B:5:0x001f, code lost:
        if (r0 != null) goto L7;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.lang.String d() {
        /*
            r7 = this;
            com.discord.api.premium.OutboundPromotion r0 = r7.promotion
            java.lang.String r1 = r0.e()
            if (r1 == 0) goto L22
            java.lang.String r0 = r7.code
            java.lang.String r2 = "UTF-8"
            java.lang.String r3 = java.net.URLEncoder.encode(r0, r2)
            java.lang.String r0 = "URLEncoder.encode(code, \"UTF-8\")"
            d0.z.d.m.checkNotNullExpressionValue(r3, r0)
            r4 = 0
            r5 = 4
            r6 = 0
            java.lang.String r2 = "{code}"
            java.lang.String r0 = d0.g0.t.replace$default(r1, r2, r3, r4, r5, r6)
            if (r0 == 0) goto L22
            goto L28
        L22:
            com.discord.api.premium.OutboundPromotion r0 = r7.promotion
            java.lang.String r0 = r0.d()
        L28:
            if (r0 == 0) goto L2b
            goto L2d
        L2b:
            java.lang.String r0 = ""
        L2d:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.api.premium.ClaimedOutboundPromotion.d():java.lang.String");
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ClaimedOutboundPromotion)) {
            return false;
        }
        ClaimedOutboundPromotion claimedOutboundPromotion = (ClaimedOutboundPromotion) obj;
        return m.areEqual(this.code, claimedOutboundPromotion.code) && this.userId == claimedOutboundPromotion.userId && m.areEqual(this.claimedAt, claimedOutboundPromotion.claimedAt) && m.areEqual(this.promotion, claimedOutboundPromotion.promotion);
    }

    public int hashCode() {
        String str = this.code;
        int i = 0;
        int hashCode = str != null ? str.hashCode() : 0;
        long j = this.userId;
        int i2 = ((hashCode * 31) + ((int) (j ^ (j >>> 32)))) * 31;
        UtcDateTime utcDateTime = this.claimedAt;
        int hashCode2 = (i2 + (utcDateTime != null ? utcDateTime.hashCode() : 0)) * 31;
        OutboundPromotion outboundPromotion = this.promotion;
        if (outboundPromotion != null) {
            i = outboundPromotion.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("ClaimedOutboundPromotion(code=");
        R.append(this.code);
        R.append(", userId=");
        R.append(this.userId);
        R.append(", claimedAt=");
        R.append(this.claimedAt);
        R.append(", promotion=");
        R.append(this.promotion);
        R.append(")");
        return R.toString();
    }
}
