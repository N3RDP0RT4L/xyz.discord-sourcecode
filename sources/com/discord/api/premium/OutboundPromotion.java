package com.discord.api.premium;

import b.d.b.a.a;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: OutboundPromotion.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\r\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0011\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0004R\u0019\u0010\u0014\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0012\u001a\u0004\b\u0015\u0010\u0004R\u001d\u0010\u0018\u001a\u00060\u0016j\u0002`\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u0019\u0010\u001c\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u000e\u001a\u0004\b\u001d\u0010\u0010R\u0019\u0010\u001e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u0012\u001a\u0004\b\u001f\u0010\u0004R\u001b\u0010 \u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0012\u001a\u0004\b!\u0010\u0004R\u001b\u0010\"\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u0012\u001a\u0004\b#\u0010\u0004¨\u0006$"}, d2 = {"Lcom/discord/api/premium/OutboundPromotion;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/utcdatetime/UtcDateTime;", "startDate", "Lcom/discord/api/utcdatetime/UtcDateTime;", "h", "()Lcom/discord/api/utcdatetime/UtcDateTime;", "outboundRedemptionModalBody", "Ljava/lang/String;", "c", "outboundTitle", "g", "", "Lcom/discord/primitives/PromoId;", ModelAuditLogEntry.CHANGE_KEY_ID, "J", "b", "()J", "endDate", "a", "outboundTermsAndConditions", "f", "outboundRedemptionPageLink", "d", "outboundRedemptionUrlFormat", "e", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class OutboundPromotion {
    private final UtcDateTime endDate;

    /* renamed from: id  reason: collision with root package name */
    private final long f2047id;
    private final String outboundRedemptionModalBody;
    private final String outboundRedemptionPageLink;
    private final String outboundRedemptionUrlFormat;
    private final String outboundTermsAndConditions;
    private final String outboundTitle;
    private final UtcDateTime startDate;

    public final UtcDateTime a() {
        return this.endDate;
    }

    public final long b() {
        return this.f2047id;
    }

    public final String c() {
        return this.outboundRedemptionModalBody;
    }

    public final String d() {
        return this.outboundRedemptionPageLink;
    }

    public final String e() {
        return this.outboundRedemptionUrlFormat;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof OutboundPromotion)) {
            return false;
        }
        OutboundPromotion outboundPromotion = (OutboundPromotion) obj;
        return this.f2047id == outboundPromotion.f2047id && m.areEqual(this.startDate, outboundPromotion.startDate) && m.areEqual(this.endDate, outboundPromotion.endDate) && m.areEqual(this.outboundTitle, outboundPromotion.outboundTitle) && m.areEqual(this.outboundRedemptionModalBody, outboundPromotion.outboundRedemptionModalBody) && m.areEqual(this.outboundRedemptionPageLink, outboundPromotion.outboundRedemptionPageLink) && m.areEqual(this.outboundRedemptionUrlFormat, outboundPromotion.outboundRedemptionUrlFormat) && m.areEqual(this.outboundTermsAndConditions, outboundPromotion.outboundTermsAndConditions);
    }

    public final String f() {
        return this.outboundTermsAndConditions;
    }

    public final String g() {
        return this.outboundTitle;
    }

    public final UtcDateTime h() {
        return this.startDate;
    }

    public int hashCode() {
        long j = this.f2047id;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        UtcDateTime utcDateTime = this.startDate;
        int i2 = 0;
        int hashCode = (i + (utcDateTime != null ? utcDateTime.hashCode() : 0)) * 31;
        UtcDateTime utcDateTime2 = this.endDate;
        int hashCode2 = (hashCode + (utcDateTime2 != null ? utcDateTime2.hashCode() : 0)) * 31;
        String str = this.outboundTitle;
        int hashCode3 = (hashCode2 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.outboundRedemptionModalBody;
        int hashCode4 = (hashCode3 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.outboundRedemptionPageLink;
        int hashCode5 = (hashCode4 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.outboundRedemptionUrlFormat;
        int hashCode6 = (hashCode5 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.outboundTermsAndConditions;
        if (str5 != null) {
            i2 = str5.hashCode();
        }
        return hashCode6 + i2;
    }

    public String toString() {
        StringBuilder R = a.R("OutboundPromotion(id=");
        R.append(this.f2047id);
        R.append(", startDate=");
        R.append(this.startDate);
        R.append(", endDate=");
        R.append(this.endDate);
        R.append(", outboundTitle=");
        R.append(this.outboundTitle);
        R.append(", outboundRedemptionModalBody=");
        R.append(this.outboundRedemptionModalBody);
        R.append(", outboundRedemptionPageLink=");
        R.append(this.outboundRedemptionPageLink);
        R.append(", outboundRedemptionUrlFormat=");
        R.append(this.outboundRedemptionUrlFormat);
        R.append(", outboundTermsAndConditions=");
        return a.H(R, this.outboundTermsAndConditions, ")");
    }
}
