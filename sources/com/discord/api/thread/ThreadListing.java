package com.discord.api.thread;

import b.d.b.a.a;
import com.discord.api.channel.Channel;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: ThreadListing.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\f\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000fR\u001f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00110\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00160\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0013\u001a\u0004\b\u0018\u0010\u0015¨\u0006\u0019"}, d2 = {"Lcom/discord/api/thread/ThreadListing;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "hasMore", "Z", "a", "()Z", "", "Lcom/discord/api/channel/Channel;", "threads", "Ljava/util/List;", "b", "()Ljava/util/List;", "Lcom/discord/api/thread/ThreadMember;", "members", "getMembers", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ThreadListing {
    private final boolean hasMore;
    private final List<ThreadMember> members;
    private final List<Channel> threads;

    public final boolean a() {
        return this.hasMore;
    }

    public final List<Channel> b() {
        return this.threads;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ThreadListing)) {
            return false;
        }
        ThreadListing threadListing = (ThreadListing) obj;
        return m.areEqual(this.threads, threadListing.threads) && this.hasMore == threadListing.hasMore && m.areEqual(this.members, threadListing.members);
    }

    public int hashCode() {
        List<Channel> list = this.threads;
        int i = 0;
        int hashCode = (list != null ? list.hashCode() : 0) * 31;
        boolean z2 = this.hasMore;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        int i4 = (hashCode + i2) * 31;
        List<ThreadMember> list2 = this.members;
        if (list2 != null) {
            i = list2.hashCode();
        }
        return i4 + i;
    }

    public String toString() {
        StringBuilder R = a.R("ThreadListing(threads=");
        R.append(this.threads);
        R.append(", hasMore=");
        R.append(this.hasMore);
        R.append(", members=");
        return a.K(R, this.members, ")");
    }
}
