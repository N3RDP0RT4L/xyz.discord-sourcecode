package com.discord.api.thread;

import andhook.lib.HookHelper;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ThreadMember.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\b\u0016\u0018\u00002\u00020\u0001B9\u0012\u0006\u0010\u001b\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\u0006\u0010\r\u001a\u00020\f\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\u0012\u001a\u00020\u0011\u0012\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016¢\u0006\u0004\b\u001d\u0010\u001eR\u001c\u0010\u0003\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u001c\u0010\b\u001a\u00020\u00078\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR\u001c\u0010\r\u001a\u00020\f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0012\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u001c\u0010\u001b\u001a\u00020\u00078\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001b\u0010\t\u001a\u0004\b\u001c\u0010\u000b¨\u0006\u001f"}, d2 = {"Lcom/discord/api/thread/ThreadMember;", "", "Lcom/discord/api/utcdatetime/UtcDateTime;", "joinTimestamp", "Lcom/discord/api/utcdatetime/UtcDateTime;", "c", "()Lcom/discord/api/utcdatetime/UtcDateTime;", "", "userId", "J", "f", "()J", "", "flags", "I", "a", "()I", "", "muted", "Z", "e", "()Z", "Lcom/discord/api/thread/MuteConfig;", "muteConfig", "Lcom/discord/api/thread/MuteConfig;", "d", "()Lcom/discord/api/thread/MuteConfig;", ModelAuditLogEntry.CHANGE_KEY_ID, "b", HookHelper.constructorName, "(JJILcom/discord/api/utcdatetime/UtcDateTime;ZLcom/discord/api/thread/MuteConfig;)V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public class ThreadMember {
    private final int flags;

    /* renamed from: id  reason: collision with root package name */
    private final long f2057id;
    private final UtcDateTime joinTimestamp;
    private final MuteConfig muteConfig;
    private final boolean muted;
    private final long userId;

    public ThreadMember(long j, long j2, int i, UtcDateTime utcDateTime, boolean z2, MuteConfig muteConfig) {
        m.checkNotNullParameter(utcDateTime, "joinTimestamp");
        this.f2057id = j;
        this.userId = j2;
        this.flags = i;
        this.joinTimestamp = utcDateTime;
        this.muted = z2;
        this.muteConfig = muteConfig;
    }

    public int a() {
        return this.flags;
    }

    public long b() {
        return this.f2057id;
    }

    public UtcDateTime c() {
        return this.joinTimestamp;
    }

    public final MuteConfig d() {
        return this.muteConfig;
    }

    public final boolean e() {
        return this.muted;
    }

    public long f() {
        return this.userId;
    }
}
