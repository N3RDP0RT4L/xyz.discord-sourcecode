package com.discord.api.thread;

import b.d.b.a.a;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: ThreadMemberListUpdate.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\r\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0011\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u000e\u001a\u0004\b\u0012\u0010\u0010R!\u0010\u0015\u001a\n\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018¨\u0006\u0019"}, d2 = {"Lcom/discord/api/thread/ThreadMemberListUpdate;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "guildId", "J", "a", "()J", "threadId", "c", "", "Lcom/discord/api/thread/ThreadListMember;", "members", "Ljava/util/List;", "b", "()Ljava/util/List;", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ThreadMemberListUpdate {
    private final long guildId;
    private final List<ThreadListMember> members;
    private final long threadId;

    public final long a() {
        return this.guildId;
    }

    public final List<ThreadListMember> b() {
        return this.members;
    }

    public final long c() {
        return this.threadId;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ThreadMemberListUpdate)) {
            return false;
        }
        ThreadMemberListUpdate threadMemberListUpdate = (ThreadMemberListUpdate) obj;
        return this.guildId == threadMemberListUpdate.guildId && this.threadId == threadMemberListUpdate.threadId && m.areEqual(this.members, threadMemberListUpdate.members);
    }

    public int hashCode() {
        long j = this.guildId;
        long j2 = this.threadId;
        int i = ((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31;
        List<ThreadListMember> list = this.members;
        return i + (list != null ? list.hashCode() : 0);
    }

    public String toString() {
        StringBuilder R = a.R("ThreadMemberListUpdate(guildId=");
        R.append(this.guildId);
        R.append(", threadId=");
        R.append(this.threadId);
        R.append(", members=");
        return a.K(R, this.members, ")");
    }
}
