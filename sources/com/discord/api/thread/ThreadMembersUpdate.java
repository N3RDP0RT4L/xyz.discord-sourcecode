package com.discord.api.thread;

import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: ThreadMembersUpdate.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR!\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0012\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R!\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u000f\u001a\u0004\b\u0017\u0010\u0011R\u0019\u0010\u0018\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u0007R\u0019\u0010\u001b\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u0013\u001a\u0004\b\u001c\u0010\u0015R!\u0010\u001e\u001a\n\u0012\u0004\u0012\u00020\u001d\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u000f\u001a\u0004\b\u001f\u0010\u0011¨\u0006 "}, d2 = {"Lcom/discord/api/thread/ThreadMembersUpdate;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "", "memberIdsPreview", "Ljava/util/List;", "getMemberIdsPreview", "()Ljava/util/List;", ModelAuditLogEntry.CHANGE_KEY_ID, "J", "c", "()J", "removedMemberIds", "d", "memberCount", "I", "getMemberCount", "guildId", "b", "Lcom/discord/api/thread/AugmentedThreadMember;", "addedMembers", "a", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ThreadMembersUpdate {
    private final List<AugmentedThreadMember> addedMembers;
    private final long guildId;

    /* renamed from: id  reason: collision with root package name */
    private final long f2059id;
    private final int memberCount;
    private final List<Long> memberIdsPreview;
    private final List<Long> removedMemberIds;

    public final List<AugmentedThreadMember> a() {
        return this.addedMembers;
    }

    public final long b() {
        return this.guildId;
    }

    public final long c() {
        return this.f2059id;
    }

    public final List<Long> d() {
        return this.removedMemberIds;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ThreadMembersUpdate)) {
            return false;
        }
        ThreadMembersUpdate threadMembersUpdate = (ThreadMembersUpdate) obj;
        return this.f2059id == threadMembersUpdate.f2059id && this.guildId == threadMembersUpdate.guildId && this.memberCount == threadMembersUpdate.memberCount && m.areEqual(this.removedMemberIds, threadMembersUpdate.removedMemberIds) && m.areEqual(this.memberIdsPreview, threadMembersUpdate.memberIdsPreview) && m.areEqual(this.addedMembers, threadMembersUpdate.addedMembers);
    }

    public int hashCode() {
        long j = this.f2059id;
        long j2 = this.guildId;
        int i = ((((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + this.memberCount) * 31;
        List<Long> list = this.removedMemberIds;
        int i2 = 0;
        int hashCode = (i + (list != null ? list.hashCode() : 0)) * 31;
        List<Long> list2 = this.memberIdsPreview;
        int hashCode2 = (hashCode + (list2 != null ? list2.hashCode() : 0)) * 31;
        List<AugmentedThreadMember> list3 = this.addedMembers;
        if (list3 != null) {
            i2 = list3.hashCode();
        }
        return hashCode2 + i2;
    }

    public String toString() {
        StringBuilder R = a.R("ThreadMembersUpdate(id=");
        R.append(this.f2059id);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", memberCount=");
        R.append(this.memberCount);
        R.append(", removedMemberIds=");
        R.append(this.removedMemberIds);
        R.append(", memberIdsPreview=");
        R.append(this.memberIdsPreview);
        R.append(", addedMembers=");
        return a.K(R, this.addedMembers, ")");
    }
}
