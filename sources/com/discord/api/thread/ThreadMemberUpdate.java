package com.discord.api.thread;

import b.d.b.a.a;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ThreadMemberUpdate.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\t\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\r\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0011\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R\u0019\u0010\u0016\u001a\u00020\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u0019\u0010\u001a\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u0007R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010!R\u0019\u0010\"\u001a\u00020\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u0017\u001a\u0004\b#\u0010\u0019R\u0019\u0010$\u001a\u00020\u00158\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u0017\u001a\u0004\b%\u0010\u0019¨\u0006&"}, d2 = {"Lcom/discord/api/thread/ThreadMemberUpdate;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/utcdatetime/UtcDateTime;", "joinTimestamp", "Lcom/discord/api/utcdatetime/UtcDateTime;", "d", "()Lcom/discord/api/utcdatetime/UtcDateTime;", "muted", "Z", "f", "()Z", "", "guildId", "J", "b", "()J", "flags", "I", "a", "Lcom/discord/api/thread/MuteConfig;", "muteConfig", "Lcom/discord/api/thread/MuteConfig;", "e", "()Lcom/discord/api/thread/MuteConfig;", "userId", "g", ModelAuditLogEntry.CHANGE_KEY_ID, "c", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ThreadMemberUpdate {
    private final int flags;
    private final long guildId;

    /* renamed from: id  reason: collision with root package name */
    private final long f2058id;
    private final UtcDateTime joinTimestamp;
    private final MuteConfig muteConfig;
    private final boolean muted;
    private final long userId;

    public final int a() {
        return this.flags;
    }

    public final long b() {
        return this.guildId;
    }

    public final long c() {
        return this.f2058id;
    }

    public final UtcDateTime d() {
        return this.joinTimestamp;
    }

    public final MuteConfig e() {
        return this.muteConfig;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ThreadMemberUpdate)) {
            return false;
        }
        ThreadMemberUpdate threadMemberUpdate = (ThreadMemberUpdate) obj;
        return this.f2058id == threadMemberUpdate.f2058id && this.guildId == threadMemberUpdate.guildId && this.userId == threadMemberUpdate.userId && this.flags == threadMemberUpdate.flags && m.areEqual(this.joinTimestamp, threadMemberUpdate.joinTimestamp) && this.muted == threadMemberUpdate.muted && m.areEqual(this.muteConfig, threadMemberUpdate.muteConfig);
    }

    public final boolean f() {
        return this.muted;
    }

    public final long g() {
        return this.userId;
    }

    public int hashCode() {
        long j = this.f2058id;
        long j2 = this.guildId;
        long j3 = this.userId;
        int i = ((((((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + this.flags) * 31;
        UtcDateTime utcDateTime = this.joinTimestamp;
        int i2 = 0;
        int hashCode = (i + (utcDateTime != null ? utcDateTime.hashCode() : 0)) * 31;
        boolean z2 = this.muted;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        int i5 = (hashCode + i3) * 31;
        MuteConfig muteConfig = this.muteConfig;
        if (muteConfig != null) {
            i2 = muteConfig.hashCode();
        }
        return i5 + i2;
    }

    public String toString() {
        StringBuilder R = a.R("ThreadMemberUpdate(id=");
        R.append(this.f2058id);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", userId=");
        R.append(this.userId);
        R.append(", flags=");
        R.append(this.flags);
        R.append(", joinTimestamp=");
        R.append(this.joinTimestamp);
        R.append(", muted=");
        R.append(this.muted);
        R.append(", muteConfig=");
        R.append(this.muteConfig);
        R.append(")");
        return R.toString();
    }
}
