package com.discord.api.thread;

import com.discord.api.guildmember.GuildMember;
import com.discord.api.presence.Presence;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.models.domain.ModelAuditLogEntry;
import kotlin.Metadata;
/* compiled from: AugmentedThreadMember.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0007\u0018\u00002\u00020\u0001R\u001b\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u001b\u0010\b\u001a\u0004\u0018\u00010\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR\u0019\u0010\r\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u0019\u0010\u0017\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u0019\u0010\u001c\u001a\u00020\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001fR\u0019\u0010!\u001a\u00020 8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010$R\u0019\u0010%\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u000e\u001a\u0004\b&\u0010\u0010¨\u0006'"}, d2 = {"Lcom/discord/api/thread/AugmentedThreadMember;", "", "Lcom/discord/api/presence/Presence;", "presence", "Lcom/discord/api/presence/Presence;", "f", "()Lcom/discord/api/presence/Presence;", "Lcom/discord/api/guildmember/GuildMember;", "member", "Lcom/discord/api/guildmember/GuildMember;", "c", "()Lcom/discord/api/guildmember/GuildMember;", "", "userId", "J", "g", "()J", "Lcom/discord/api/thread/MuteConfig;", "muteConfig", "Lcom/discord/api/thread/MuteConfig;", "d", "()Lcom/discord/api/thread/MuteConfig;", "", "flags", "I", "a", "()I", "Lcom/discord/api/utcdatetime/UtcDateTime;", "joinTimestamp", "Lcom/discord/api/utcdatetime/UtcDateTime;", "b", "()Lcom/discord/api/utcdatetime/UtcDateTime;", "", "muted", "Z", "e", "()Z", ModelAuditLogEntry.CHANGE_KEY_ID, "getId", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AugmentedThreadMember {
    private final int flags;

    /* renamed from: id  reason: collision with root package name */
    private final long f2056id;
    private final UtcDateTime joinTimestamp;
    private final GuildMember member;
    private final MuteConfig muteConfig;
    private final boolean muted;
    private final Presence presence;
    private final long userId;

    public final int a() {
        return this.flags;
    }

    public final UtcDateTime b() {
        return this.joinTimestamp;
    }

    public final GuildMember c() {
        return this.member;
    }

    public final MuteConfig d() {
        return this.muteConfig;
    }

    public final boolean e() {
        return this.muted;
    }

    public final Presence f() {
        return this.presence;
    }

    public final long g() {
        return this.userId;
    }
}
