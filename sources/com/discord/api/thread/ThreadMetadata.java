package com.discord.api.thread;

import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ThreadMetadata.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\f\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000fR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004R\u0019\u0010\u0013\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\r\u001a\u0004\b\u0014\u0010\u000fR\u0019\u0010\u0015\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0007¨\u0006\u0018"}, d2 = {"Lcom/discord/api/thread/ThreadMetadata;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", ModelAuditLogEntry.CHANGE_KEY_ARCHIVED, "Z", "b", "()Z", "archiveTimestamp", "Ljava/lang/String;", "a", ModelAuditLogEntry.CHANGE_KEY_LOCKED, "d", "autoArchiveDuration", "I", "c", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ThreadMetadata {
    private final String archiveTimestamp;
    private final boolean archived;
    private final int autoArchiveDuration;
    private final boolean locked;

    public final String a() {
        return this.archiveTimestamp;
    }

    public final boolean b() {
        return this.archived;
    }

    public final int c() {
        return this.autoArchiveDuration;
    }

    public final boolean d() {
        return this.locked;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ThreadMetadata)) {
            return false;
        }
        ThreadMetadata threadMetadata = (ThreadMetadata) obj;
        return this.archived == threadMetadata.archived && m.areEqual(this.archiveTimestamp, threadMetadata.archiveTimestamp) && this.autoArchiveDuration == threadMetadata.autoArchiveDuration && this.locked == threadMetadata.locked;
    }

    public int hashCode() {
        boolean z2 = this.archived;
        int i = 1;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        int i4 = i2 * 31;
        String str = this.archiveTimestamp;
        int hashCode = (((i4 + (str != null ? str.hashCode() : 0)) * 31) + this.autoArchiveDuration) * 31;
        boolean z3 = this.locked;
        if (!z3) {
            i = z3 ? 1 : 0;
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder R = a.R("ThreadMetadata(archived=");
        R.append(this.archived);
        R.append(", archiveTimestamp=");
        R.append(this.archiveTimestamp);
        R.append(", autoArchiveDuration=");
        R.append(this.autoArchiveDuration);
        R.append(", locked=");
        return a.M(R, this.locked, ")");
    }
}
