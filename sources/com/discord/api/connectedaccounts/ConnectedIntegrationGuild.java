package com.discord.api.connectedaccounts;

import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ConnectedIntegrationGuild.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001d\u0010\u000e\u001a\u00060\fj\u0002`\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0013\u001a\u0004\b\u0016\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/api/connectedaccounts/ConnectedIntegrationGuild;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "Lcom/discord/primitives/GuildId;", ModelAuditLogEntry.CHANGE_KEY_ID, "J", "b", "()J", ModelAuditLogEntry.CHANGE_KEY_NAME, "Ljava/lang/String;", "c", "icon", "a", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ConnectedIntegrationGuild {
    private final String icon;

    /* renamed from: id  reason: collision with root package name */
    private final long f2028id;
    private final String name;

    public final String a() {
        return this.icon;
    }

    public final long b() {
        return this.f2028id;
    }

    public final String c() {
        return this.name;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ConnectedIntegrationGuild)) {
            return false;
        }
        ConnectedIntegrationGuild connectedIntegrationGuild = (ConnectedIntegrationGuild) obj;
        return this.f2028id == connectedIntegrationGuild.f2028id && m.areEqual(this.icon, connectedIntegrationGuild.icon) && m.areEqual(this.name, connectedIntegrationGuild.name);
    }

    public int hashCode() {
        long j = this.f2028id;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        String str = this.icon;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.name;
        if (str2 != null) {
            i2 = str2.hashCode();
        }
        return hashCode + i2;
    }

    public String toString() {
        StringBuilder R = a.R("ConnectedIntegrationGuild(id=");
        R.append(this.f2028id);
        R.append(", icon=");
        R.append(this.icon);
        R.append(", name=");
        return a.H(R, this.name, ")");
    }
}
