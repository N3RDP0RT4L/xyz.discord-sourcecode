package com.discord.api.connectedaccounts;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ConnectedAccount.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0012\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\n\b\u0086\b\u0018\u0000 &2\u00020\u0001:\u0001&J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u0007R\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0004R\u0019\u0010\u0012\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u0019\u0010\u0016\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0010\u001a\u0004\b\u0017\u0010\u0004R\u0019\u0010\u0018\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0010\u001a\u0004\b\u0019\u0010\u0004R\u0019\u0010\u001a\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0013\u001a\u0004\b\u001b\u0010\u0015R!\u0010\u001e\u001a\n\u0012\u0004\u0012\u00020\u001d\u0018\u00010\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010!R\u0019\u0010\"\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u0013\u001a\u0004\b#\u0010\u0015R\u0019\u0010$\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u0013\u001a\u0004\b%\u0010\u0015¨\u0006'"}, d2 = {"Lcom/discord/api/connectedaccounts/ConnectedAccount;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "visibility", "I", "i", ModelAuditLogEntry.CHANGE_KEY_NAME, "Ljava/lang/String;", "d", "friendSync", "Z", "a", "()Z", ModelAuditLogEntry.CHANGE_KEY_ID, "b", "type", "g", "showActivity", "f", "", "Lcom/discord/api/connectedaccounts/ConnectedAccountIntegration;", "integrations", "Ljava/util/List;", "c", "()Ljava/util/List;", "verified", "h", "revoked", "e", "Companion", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ConnectedAccount {
    public static final Companion Companion = new Companion(null);
    public static final int HIDDEN = 0;
    public static final int VISIBLE = 1;

    /* renamed from: id  reason: collision with root package name */
    private final String f2025id = "";
    private final String name = "";
    private final int visibility = 0;
    private final boolean friendSync = false;
    private final boolean showActivity = false;
    private final boolean revoked = false;
    private final boolean verified = false;
    private final List<ConnectedAccountIntegration> integrations = null;
    private final String type = "";

    /* compiled from: ConnectedAccount.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/api/connectedaccounts/ConnectedAccount$Companion;", "", "", "HIDDEN", "I", "VISIBLE", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        public Companion() {
        }

        public Companion(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    public ConnectedAccount() {
        a.n0("", ModelAuditLogEntry.CHANGE_KEY_ID, "", ModelAuditLogEntry.CHANGE_KEY_NAME, "", "type");
    }

    public final boolean a() {
        return this.friendSync;
    }

    public final String b() {
        return this.f2025id;
    }

    public final List<ConnectedAccountIntegration> c() {
        return this.integrations;
    }

    public final String d() {
        return this.name;
    }

    public final boolean e() {
        return this.revoked;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ConnectedAccount)) {
            return false;
        }
        ConnectedAccount connectedAccount = (ConnectedAccount) obj;
        return m.areEqual(this.f2025id, connectedAccount.f2025id) && m.areEqual(this.name, connectedAccount.name) && this.visibility == connectedAccount.visibility && this.friendSync == connectedAccount.friendSync && this.showActivity == connectedAccount.showActivity && this.revoked == connectedAccount.revoked && this.verified == connectedAccount.verified && m.areEqual(this.integrations, connectedAccount.integrations) && m.areEqual(this.type, connectedAccount.type);
    }

    public final boolean f() {
        return this.showActivity;
    }

    public final String g() {
        return this.type;
    }

    public final boolean h() {
        return this.verified;
    }

    public int hashCode() {
        String str = this.f2025id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.name;
        int hashCode2 = (((hashCode + (str2 != null ? str2.hashCode() : 0)) * 31) + this.visibility) * 31;
        boolean z2 = this.friendSync;
        int i2 = 1;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        int i5 = (hashCode2 + i3) * 31;
        boolean z3 = this.showActivity;
        if (z3) {
            z3 = true;
        }
        int i6 = z3 ? 1 : 0;
        int i7 = z3 ? 1 : 0;
        int i8 = (i5 + i6) * 31;
        boolean z4 = this.revoked;
        if (z4) {
            z4 = true;
        }
        int i9 = z4 ? 1 : 0;
        int i10 = z4 ? 1 : 0;
        int i11 = (i8 + i9) * 31;
        boolean z5 = this.verified;
        if (!z5) {
            i2 = z5 ? 1 : 0;
        }
        int i12 = (i11 + i2) * 31;
        List<ConnectedAccountIntegration> list = this.integrations;
        int hashCode3 = (i12 + (list != null ? list.hashCode() : 0)) * 31;
        String str3 = this.type;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode3 + i;
    }

    public final int i() {
        return this.visibility;
    }

    public String toString() {
        StringBuilder R = a.R("ConnectedAccount(id=");
        R.append(this.f2025id);
        R.append(", name=");
        R.append(this.name);
        R.append(", visibility=");
        R.append(this.visibility);
        R.append(", friendSync=");
        R.append(this.friendSync);
        R.append(", showActivity=");
        R.append(this.showActivity);
        R.append(", revoked=");
        R.append(this.revoked);
        R.append(", verified=");
        R.append(this.verified);
        R.append(", integrations=");
        R.append(this.integrations);
        R.append(", type=");
        return a.H(R, this.type, ")");
    }
}
