package com.discord.api.science;

import andhook.lib.HookHelper;
import b.i.d.c;
import b.i.d.e;
import b.i.d.l;
import b.i.d.m;
import b.i.d.q.x.b;
import com.discord.api.science.Science;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import d0.t.h0;
import d0.t.r;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
/* compiled from: AnalyticsSchemaTypeAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007¢\u0006\u0004\b\u000e\u0010\u000fJ/\u0010\b\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00070\u0006*\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003¢\u0006\u0004\b\b\u0010\tR\u001e\u0010\f\u001a\n \u000b*\u0004\u0018\u00010\n0\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\r¨\u0006\u0010"}, d2 = {"Lcom/discord/api/science/AnalyticsSchemaTypeAdapter;", "Lb/i/d/m;", "Lcom/discord/api/science/Science$Event$SchemaObject;", "", "", "", "", "Lkotlin/Pair;", "a", "(Ljava/util/Map;)Ljava/util/List;", "Lcom/google/gson/Gson;", "kotlin.jvm.PlatformType", "gson", "Lcom/google/gson/Gson;", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AnalyticsSchemaTypeAdapter implements m<Science.Event.SchemaObject> {
    private final Gson gson;

    public AnalyticsSchemaTypeAdapter() {
        e eVar = new e();
        eVar.c = c.m;
        this.gson = eVar.a();
    }

    public final List<Pair<String, Object>> a(Map<String, ? extends Object> map) {
        List<Pair<String, Object>> list;
        d0.z.d.m.checkNotNullParameter(map, "$this$flatMapProperties");
        Set<Map.Entry<String, ? extends Object>> entrySet = map.entrySet();
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = entrySet.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            Object value = entry.getValue();
            if (!(value instanceof Map)) {
                value = null;
            }
            Map<String, ? extends Object> map2 = (Map) value;
            if (map2 == null || (list = a(map2)) == null) {
                list = d0.t.m.listOf(new Pair(entry.getKey(), entry.getValue()));
            }
            r.addAll(arrayList, list);
        }
        return arrayList;
    }

    @Override // b.i.d.m
    public JsonElement serialize(Science.Event.SchemaObject schemaObject, Type type, l lVar) {
        Science.Event.SchemaObject schemaObject2 = schemaObject;
        d0.z.d.m.checkNotNullParameter(schemaObject2, "src");
        d0.z.d.m.checkNotNullParameter(type, "typeOfSrc");
        d0.z.d.m.checkNotNullParameter(lVar, "context");
        Gson gson = this.gson;
        String a = schemaObject2.a();
        AnalyticsSchema b2 = schemaObject2.b();
        d0.z.d.m.checkNotNullParameter(b2, "$this$serializeToMap");
        Object g = this.gson.g(this.gson.m(b2), new TypeToken<Map<String, ? extends Object>>() { // from class: com.discord.api.science.AnalyticsSchemaTypeAdapter$serializeToMap$1
        }.getType());
        d0.z.d.m.checkNotNullExpressionValue(g, "gson.fromJson(json, obje…<String, Any>>() {}.type)");
        Science.Event.MapObject mapObject = new Science.Event.MapObject(a, h0.toMap(a((Map) g)));
        Objects.requireNonNull(gson);
        b bVar = new b();
        gson.o(mapObject, Science.Event.MapObject.class, bVar);
        JsonElement L = bVar.L();
        d0.z.d.m.checkNotNullExpressionValue(L, "gson.toJsonTree(\n       …).toMap()\n        )\n    )");
        return L;
    }
}
