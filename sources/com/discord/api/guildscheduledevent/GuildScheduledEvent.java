package com.discord.api.guildscheduledevent;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.guild.Guild;
import com.discord.api.stageinstance.StageInstancePrivacyLevel;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: GuildScheduledEvent.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0082\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010 \n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001BÅ\u0001\u0012\n\u0010-\u001a\u00060\u0002j\u0002`,\u0012\n\u0010;\u001a\u00060\u0002j\u0002`:\u0012\u000e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003\u0012\u000e\u0010G\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`F\u0012\u0006\u0010\u0011\u001a\u00020\b\u0012\b\u00101\u001a\u0004\u0018\u00010\b\u0012\b\u00103\u001a\u0004\u0018\u00010\b\u0012\u0006\u0010#\u001a\u00020\"\u0012\b\u0010S\u001a\u0004\u0018\u00010\"\u0012\u0006\u00106\u001a\u000205\u0012\u0006\u0010>\u001a\u00020=\u0012\u0006\u0010O\u001a\u00020N\u0012\b\u0010B\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010(\u001a\u0004\u0018\u00010'\u0012\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00020\u0014\u0012\b\u0010\u001e\u001a\u0004\u0018\u00010\u000b\u0012\b\u0010J\u001a\u0004\u0018\u00010I\u0012\b\u0010\u001a\u001a\u0004\u0018\u00010\u0019¢\u0006\u0004\bV\u0010WJ\u0019\u0010\u0006\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u000f\u001a\u00020\u00052\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0011\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\nR\u001f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00020\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00198\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u001dR\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010!R\u0019\u0010#\u001a\u00020\"8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010$\u001a\u0004\b%\u0010&R\u001b\u0010(\u001a\u0004\u0018\u00010'8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010)\u001a\u0004\b*\u0010+R\u001d\u0010-\u001a\u00060\u0002j\u0002`,8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010.\u001a\u0004\b/\u00100R\u001b\u00101\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010\u0012\u001a\u0004\b2\u0010\nR\u001b\u00103\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010\u0012\u001a\u0004\b4\u0010\nR\u0019\u00106\u001a\u0002058\u0006@\u0006¢\u0006\f\n\u0004\b6\u00107\u001a\u0004\b8\u00109R\u001d\u0010;\u001a\u00060\u0002j\u0002`:8\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010.\u001a\u0004\b<\u00100R\u0019\u0010>\u001a\u00020=8\u0006@\u0006¢\u0006\f\n\u0004\b>\u0010?\u001a\u0004\b@\u0010AR\u001b\u0010B\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010C\u001a\u0004\bD\u0010ER!\u0010G\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`F8\u0006@\u0006¢\u0006\f\n\u0004\bG\u0010C\u001a\u0004\bH\u0010ER\u001b\u0010J\u001a\u0004\u0018\u00010I8\u0006@\u0006¢\u0006\f\n\u0004\bJ\u0010K\u001a\u0004\bL\u0010MR\u0019\u0010O\u001a\u00020N8\u0006@\u0006¢\u0006\f\n\u0004\bO\u0010P\u001a\u0004\bQ\u0010RR\u001b\u0010S\u001a\u0004\u0018\u00010\"8\u0006@\u0006¢\u0006\f\n\u0004\bS\u0010$\u001a\u0004\bT\u0010&R!\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010C\u001a\u0004\bU\u0010E¨\u0006X"}, d2 = {"Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "p", "(J)Z", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", ModelAuditLogEntry.CHANGE_KEY_NAME, "Ljava/lang/String;", "j", "", "skuIds", "Ljava/util/List;", "getSkuIds", "()Ljava/util/List;", "Lcom/discord/api/guild/Guild;", "guild", "Lcom/discord/api/guild/Guild;", "g", "()Lcom/discord/api/guild/Guild;", "userCount", "Ljava/lang/Integer;", "n", "()Ljava/lang/Integer;", "Lcom/discord/api/utcdatetime/UtcDateTime;", "scheduledStartTime", "Lcom/discord/api/utcdatetime/UtcDateTime;", "l", "()Lcom/discord/api/utcdatetime/UtcDateTime;", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityMetadata;", "entityMetadata", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityMetadata;", "e", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityMetadata;", "Lcom/discord/primitives/GuildScheduledEventId;", ModelAuditLogEntry.CHANGE_KEY_ID, "J", "i", "()J", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "d", "image", "getImage", "Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;", "privacyLevel", "Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;", "getPrivacyLevel", "()Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;", "Lcom/discord/primitives/GuildId;", "guildId", "h", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventStatus;", "status", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventStatus;", "m", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEventStatus;", "entityId", "Ljava/lang/Long;", "getEntityId", "()Ljava/lang/Long;", "Lcom/discord/primitives/UserId;", "creatorId", "c", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventMeUser;", "userRsvp", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventMeUser;", "o", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEventMeUser;", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;", "entityType", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;", "f", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;", "scheduledEndTime", "k", "b", HookHelper.constructorName, "(JJLjava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/discord/api/utcdatetime/UtcDateTime;Lcom/discord/api/utcdatetime/UtcDateTime;Lcom/discord/api/stageinstance/StageInstancePrivacyLevel;Lcom/discord/api/guildscheduledevent/GuildScheduledEventStatus;Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityType;Ljava/lang/Long;Lcom/discord/api/guildscheduledevent/GuildScheduledEventEntityMetadata;Ljava/util/List;Ljava/lang/Integer;Lcom/discord/api/guildscheduledevent/GuildScheduledEventMeUser;Lcom/discord/api/guild/Guild;)V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildScheduledEvent {
    private final Long channelId;
    private final Long creatorId;
    private final String description;
    private final Long entityId;
    private final GuildScheduledEventEntityMetadata entityMetadata;
    private final GuildScheduledEventEntityType entityType;
    private final Guild guild;
    private final long guildId;

    /* renamed from: id  reason: collision with root package name */
    private final long f2037id;
    private final String image;
    private final String name;
    private final StageInstancePrivacyLevel privacyLevel;
    private final UtcDateTime scheduledEndTime;
    private final UtcDateTime scheduledStartTime;
    private final List<Long> skuIds;
    private final GuildScheduledEventStatus status;
    private final Integer userCount;
    private final GuildScheduledEventMeUser userRsvp;

    public GuildScheduledEvent(long j, long j2, Long l, Long l2, String str, String str2, String str3, UtcDateTime utcDateTime, UtcDateTime utcDateTime2, StageInstancePrivacyLevel stageInstancePrivacyLevel, GuildScheduledEventStatus guildScheduledEventStatus, GuildScheduledEventEntityType guildScheduledEventEntityType, Long l3, GuildScheduledEventEntityMetadata guildScheduledEventEntityMetadata, List<Long> list, Integer num, GuildScheduledEventMeUser guildScheduledEventMeUser, Guild guild) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(utcDateTime, "scheduledStartTime");
        m.checkNotNullParameter(stageInstancePrivacyLevel, "privacyLevel");
        m.checkNotNullParameter(guildScheduledEventStatus, "status");
        m.checkNotNullParameter(guildScheduledEventEntityType, "entityType");
        m.checkNotNullParameter(list, "skuIds");
        this.f2037id = j;
        this.guildId = j2;
        this.channelId = l;
        this.creatorId = l2;
        this.name = str;
        this.description = str2;
        this.image = str3;
        this.scheduledStartTime = utcDateTime;
        this.scheduledEndTime = utcDateTime2;
        this.privacyLevel = stageInstancePrivacyLevel;
        this.status = guildScheduledEventStatus;
        this.entityType = guildScheduledEventEntityType;
        this.entityId = l3;
        this.entityMetadata = guildScheduledEventEntityMetadata;
        this.skuIds = list;
        this.userCount = num;
        this.userRsvp = guildScheduledEventMeUser;
        this.guild = guild;
    }

    public static GuildScheduledEvent a(GuildScheduledEvent guildScheduledEvent, long j, long j2, Long l, Long l2, String str, String str2, String str3, UtcDateTime utcDateTime, UtcDateTime utcDateTime2, StageInstancePrivacyLevel stageInstancePrivacyLevel, GuildScheduledEventStatus guildScheduledEventStatus, GuildScheduledEventEntityType guildScheduledEventEntityType, Long l3, GuildScheduledEventEntityMetadata guildScheduledEventEntityMetadata, List list, Integer num, GuildScheduledEventMeUser guildScheduledEventMeUser, Guild guild, int i) {
        long j3 = (i & 1) != 0 ? guildScheduledEvent.f2037id : j;
        long j4 = (i & 2) != 0 ? guildScheduledEvent.guildId : j2;
        Long l4 = (i & 4) != 0 ? guildScheduledEvent.channelId : null;
        Long l5 = (i & 8) != 0 ? guildScheduledEvent.creatorId : null;
        String str4 = (i & 16) != 0 ? guildScheduledEvent.name : null;
        String str5 = (i & 32) != 0 ? guildScheduledEvent.description : null;
        String str6 = (i & 64) != 0 ? guildScheduledEvent.image : null;
        UtcDateTime utcDateTime3 = (i & 128) != 0 ? guildScheduledEvent.scheduledStartTime : null;
        UtcDateTime utcDateTime4 = (i & 256) != 0 ? guildScheduledEvent.scheduledEndTime : null;
        StageInstancePrivacyLevel stageInstancePrivacyLevel2 = (i & 512) != 0 ? guildScheduledEvent.privacyLevel : null;
        GuildScheduledEventStatus guildScheduledEventStatus2 = (i & 1024) != 0 ? guildScheduledEvent.status : null;
        GuildScheduledEventEntityType guildScheduledEventEntityType2 = (i & 2048) != 0 ? guildScheduledEvent.entityType : null;
        UtcDateTime utcDateTime5 = utcDateTime4;
        Long l6 = (i & 4096) != 0 ? guildScheduledEvent.entityId : null;
        GuildScheduledEventEntityMetadata guildScheduledEventEntityMetadata2 = (i & 8192) != 0 ? guildScheduledEvent.entityMetadata : null;
        List<Long> list2 = (i & 16384) != 0 ? guildScheduledEvent.skuIds : null;
        String str7 = str6;
        Integer num2 = (i & 32768) != 0 ? guildScheduledEvent.userCount : num;
        GuildScheduledEventMeUser guildScheduledEventMeUser2 = (i & 65536) != 0 ? guildScheduledEvent.userRsvp : null;
        Guild guild2 = (i & 131072) != 0 ? guildScheduledEvent.guild : null;
        m.checkNotNullParameter(str4, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(utcDateTime3, "scheduledStartTime");
        m.checkNotNullParameter(stageInstancePrivacyLevel2, "privacyLevel");
        m.checkNotNullParameter(guildScheduledEventStatus2, "status");
        m.checkNotNullParameter(guildScheduledEventEntityType2, "entityType");
        m.checkNotNullParameter(list2, "skuIds");
        return new GuildScheduledEvent(j3, j4, l4, l5, str4, str5, str7, utcDateTime3, utcDateTime5, stageInstancePrivacyLevel2, guildScheduledEventStatus2, guildScheduledEventEntityType2, l6, guildScheduledEventEntityMetadata2, list2, num2, guildScheduledEventMeUser2, guild2);
    }

    public final Long b() {
        return this.channelId;
    }

    public final Long c() {
        return this.creatorId;
    }

    public final String d() {
        return this.description;
    }

    public final GuildScheduledEventEntityMetadata e() {
        return this.entityMetadata;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildScheduledEvent)) {
            return false;
        }
        GuildScheduledEvent guildScheduledEvent = (GuildScheduledEvent) obj;
        return this.f2037id == guildScheduledEvent.f2037id && this.guildId == guildScheduledEvent.guildId && m.areEqual(this.channelId, guildScheduledEvent.channelId) && m.areEqual(this.creatorId, guildScheduledEvent.creatorId) && m.areEqual(this.name, guildScheduledEvent.name) && m.areEqual(this.description, guildScheduledEvent.description) && m.areEqual(this.image, guildScheduledEvent.image) && m.areEqual(this.scheduledStartTime, guildScheduledEvent.scheduledStartTime) && m.areEqual(this.scheduledEndTime, guildScheduledEvent.scheduledEndTime) && m.areEqual(this.privacyLevel, guildScheduledEvent.privacyLevel) && m.areEqual(this.status, guildScheduledEvent.status) && m.areEqual(this.entityType, guildScheduledEvent.entityType) && m.areEqual(this.entityId, guildScheduledEvent.entityId) && m.areEqual(this.entityMetadata, guildScheduledEvent.entityMetadata) && m.areEqual(this.skuIds, guildScheduledEvent.skuIds) && m.areEqual(this.userCount, guildScheduledEvent.userCount) && m.areEqual(this.userRsvp, guildScheduledEvent.userRsvp) && m.areEqual(this.guild, guildScheduledEvent.guild);
    }

    public final GuildScheduledEventEntityType f() {
        return this.entityType;
    }

    public final Guild g() {
        return this.guild;
    }

    public final long h() {
        return this.guildId;
    }

    public int hashCode() {
        long j = this.f2037id;
        long j2 = this.guildId;
        int i = ((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31;
        Long l = this.channelId;
        int i2 = 0;
        int hashCode = (i + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.creatorId;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        String str = this.name;
        int hashCode3 = (hashCode2 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.description;
        int hashCode4 = (hashCode3 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.image;
        int hashCode5 = (hashCode4 + (str3 != null ? str3.hashCode() : 0)) * 31;
        UtcDateTime utcDateTime = this.scheduledStartTime;
        int hashCode6 = (hashCode5 + (utcDateTime != null ? utcDateTime.hashCode() : 0)) * 31;
        UtcDateTime utcDateTime2 = this.scheduledEndTime;
        int hashCode7 = (hashCode6 + (utcDateTime2 != null ? utcDateTime2.hashCode() : 0)) * 31;
        StageInstancePrivacyLevel stageInstancePrivacyLevel = this.privacyLevel;
        int hashCode8 = (hashCode7 + (stageInstancePrivacyLevel != null ? stageInstancePrivacyLevel.hashCode() : 0)) * 31;
        GuildScheduledEventStatus guildScheduledEventStatus = this.status;
        int hashCode9 = (hashCode8 + (guildScheduledEventStatus != null ? guildScheduledEventStatus.hashCode() : 0)) * 31;
        GuildScheduledEventEntityType guildScheduledEventEntityType = this.entityType;
        int hashCode10 = (hashCode9 + (guildScheduledEventEntityType != null ? guildScheduledEventEntityType.hashCode() : 0)) * 31;
        Long l3 = this.entityId;
        int hashCode11 = (hashCode10 + (l3 != null ? l3.hashCode() : 0)) * 31;
        GuildScheduledEventEntityMetadata guildScheduledEventEntityMetadata = this.entityMetadata;
        int hashCode12 = (hashCode11 + (guildScheduledEventEntityMetadata != null ? guildScheduledEventEntityMetadata.hashCode() : 0)) * 31;
        List<Long> list = this.skuIds;
        int hashCode13 = (hashCode12 + (list != null ? list.hashCode() : 0)) * 31;
        Integer num = this.userCount;
        int hashCode14 = (hashCode13 + (num != null ? num.hashCode() : 0)) * 31;
        GuildScheduledEventMeUser guildScheduledEventMeUser = this.userRsvp;
        int hashCode15 = (hashCode14 + (guildScheduledEventMeUser != null ? guildScheduledEventMeUser.hashCode() : 0)) * 31;
        Guild guild = this.guild;
        if (guild != null) {
            i2 = guild.hashCode();
        }
        return hashCode15 + i2;
    }

    public final long i() {
        return this.f2037id;
    }

    public final String j() {
        return this.name;
    }

    public final UtcDateTime k() {
        return this.scheduledEndTime;
    }

    public final UtcDateTime l() {
        return this.scheduledStartTime;
    }

    public final GuildScheduledEventStatus m() {
        return this.status;
    }

    public final Integer n() {
        return this.userCount;
    }

    public final GuildScheduledEventMeUser o() {
        return this.userRsvp;
    }

    public final boolean p(long j) {
        Long l = this.channelId;
        if (l == null) {
            return true;
        }
        return l != null && l.longValue() == j;
    }

    public String toString() {
        StringBuilder R = a.R("GuildScheduledEvent(id=");
        R.append(this.f2037id);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", channelId=");
        R.append(this.channelId);
        R.append(", creatorId=");
        R.append(this.creatorId);
        R.append(", name=");
        R.append(this.name);
        R.append(", description=");
        R.append(this.description);
        R.append(", image=");
        R.append(this.image);
        R.append(", scheduledStartTime=");
        R.append(this.scheduledStartTime);
        R.append(", scheduledEndTime=");
        R.append(this.scheduledEndTime);
        R.append(", privacyLevel=");
        R.append(this.privacyLevel);
        R.append(", status=");
        R.append(this.status);
        R.append(", entityType=");
        R.append(this.entityType);
        R.append(", entityId=");
        R.append(this.entityId);
        R.append(", entityMetadata=");
        R.append(this.entityMetadata);
        R.append(", skuIds=");
        R.append(this.skuIds);
        R.append(", userCount=");
        R.append(this.userCount);
        R.append(", userRsvp=");
        R.append(this.userRsvp);
        R.append(", guild=");
        R.append(this.guild);
        R.append(")");
        return R.toString();
    }
}
