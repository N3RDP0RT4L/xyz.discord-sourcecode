package com.discord.api.guildscheduledevent;

import b.d.b.a.a;
import com.discord.api.guildmember.GuildMember;
import com.discord.api.user.User;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ApiGuildScheduledEventUser.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\b\u0018\u00002\u00020\u0001J\u001b\u0010\u0006\u001a\u0004\u0018\u00010\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0010\u001a\u00020\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0010\u0010\u0011R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u001d\u0010\u001d\u001a\u00060\u0002j\u0002`\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 ¨\u0006!"}, d2 = {"Lcom/discord/api/guildscheduledevent/ApiGuildScheduledEventUser;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/api/guildmember/GuildMember;", "a", "(J)Lcom/discord/api/guildmember/GuildMember;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/user/User;", "user", "Lcom/discord/api/user/User;", "c", "()Lcom/discord/api/user/User;", "Lcom/discord/api/guildscheduledevent/ApiGuildScheduledEventUserGuildMember;", "member", "Lcom/discord/api/guildscheduledevent/ApiGuildScheduledEventUserGuildMember;", "getMember", "()Lcom/discord/api/guildscheduledevent/ApiGuildScheduledEventUserGuildMember;", "Lcom/discord/primitives/GuildScheduledEventId;", "guildScheduledEventId", "J", "b", "()J", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ApiGuildScheduledEventUser {
    private final long guildScheduledEventId;
    private final ApiGuildScheduledEventUserGuildMember member;
    private final User user;

    public final GuildMember a(long j) {
        ApiGuildScheduledEventUserGuildMember apiGuildScheduledEventUserGuildMember;
        User user = this.user;
        if (user == null || (apiGuildScheduledEventUserGuildMember = this.member) == null) {
            return null;
        }
        return apiGuildScheduledEventUserGuildMember.a(user, j);
    }

    public final long b() {
        return this.guildScheduledEventId;
    }

    public final User c() {
        return this.user;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ApiGuildScheduledEventUser)) {
            return false;
        }
        ApiGuildScheduledEventUser apiGuildScheduledEventUser = (ApiGuildScheduledEventUser) obj;
        return m.areEqual(this.user, apiGuildScheduledEventUser.user) && m.areEqual(this.member, apiGuildScheduledEventUser.member) && this.guildScheduledEventId == apiGuildScheduledEventUser.guildScheduledEventId;
    }

    public int hashCode() {
        User user = this.user;
        int i = 0;
        int hashCode = (user != null ? user.hashCode() : 0) * 31;
        ApiGuildScheduledEventUserGuildMember apiGuildScheduledEventUserGuildMember = this.member;
        if (apiGuildScheduledEventUserGuildMember != null) {
            i = apiGuildScheduledEventUserGuildMember.hashCode();
        }
        long j = this.guildScheduledEventId;
        return ((hashCode + i) * 31) + ((int) (j ^ (j >>> 32)));
    }

    public String toString() {
        StringBuilder R = a.R("ApiGuildScheduledEventUser(user=");
        R.append(this.user);
        R.append(", member=");
        R.append(this.member);
        R.append(", guildScheduledEventId=");
        return a.B(R, this.guildScheduledEventId, ")");
    }
}
