package com.discord.api.guildscheduledevent;

import b.d.b.a.a;
import com.discord.api.user.User;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: GuildScheduledEventMeUser.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\r\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u001d\u0010\u001c\u001a\u00060\fj\u0002`\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u000e\u001a\u0004\b\u001d\u0010\u0010¨\u0006\u001e"}, d2 = {"Lcom/discord/api/guildscheduledevent/GuildScheduledEventMeUser;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "userId", "J", "getUserId", "()J", "Lcom/discord/api/user/User;", "user", "Lcom/discord/api/user/User;", "getUser", "()Lcom/discord/api/user/User;", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "guildScheduledEvent", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "getGuildScheduledEvent", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "Lcom/discord/primitives/GuildScheduledEventId;", "guildScheduledEventId", "a", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildScheduledEventMeUser {
    private final GuildScheduledEvent guildScheduledEvent;
    private final long guildScheduledEventId;
    private final User user;
    private final long userId;

    public final long a() {
        return this.guildScheduledEventId;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildScheduledEventMeUser)) {
            return false;
        }
        GuildScheduledEventMeUser guildScheduledEventMeUser = (GuildScheduledEventMeUser) obj;
        return this.userId == guildScheduledEventMeUser.userId && this.guildScheduledEventId == guildScheduledEventMeUser.guildScheduledEventId && m.areEqual(this.user, guildScheduledEventMeUser.user) && m.areEqual(this.guildScheduledEvent, guildScheduledEventMeUser.guildScheduledEvent);
    }

    public int hashCode() {
        long j = this.userId;
        long j2 = this.guildScheduledEventId;
        int i = ((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31;
        User user = this.user;
        int i2 = 0;
        int hashCode = (i + (user != null ? user.hashCode() : 0)) * 31;
        GuildScheduledEvent guildScheduledEvent = this.guildScheduledEvent;
        if (guildScheduledEvent != null) {
            i2 = guildScheduledEvent.hashCode();
        }
        return hashCode + i2;
    }

    public String toString() {
        StringBuilder R = a.R("GuildScheduledEventMeUser(userId=");
        R.append(this.userId);
        R.append(", guildScheduledEventId=");
        R.append(this.guildScheduledEventId);
        R.append(", user=");
        R.append(this.user);
        R.append(", guildScheduledEvent=");
        R.append(this.guildScheduledEvent);
        R.append(")");
        return R.toString();
    }
}
