package com.discord.api.guildscheduledevent;

import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: GuildScheduledEventBroadcast.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0005\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u0007R\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013¨\u0006\u0014"}, d2 = {"Lcom/discord/api/guildscheduledevent/GuildScheduledEventBroadcastBody;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "type", "I", "getType", "", "entityId", "Ljava/lang/Long;", "getEntityId", "()Ljava/lang/Long;", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildScheduledEventBroadcastBody {
    private final Long entityId;
    private final int type;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildScheduledEventBroadcastBody)) {
            return false;
        }
        GuildScheduledEventBroadcastBody guildScheduledEventBroadcastBody = (GuildScheduledEventBroadcastBody) obj;
        return this.type == guildScheduledEventBroadcastBody.type && m.areEqual(this.entityId, guildScheduledEventBroadcastBody.entityId);
    }

    public int hashCode() {
        int i = this.type * 31;
        Long l = this.entityId;
        return i + (l != null ? l.hashCode() : 0);
    }

    public String toString() {
        StringBuilder R = a.R("GuildScheduledEventBroadcastBody(type=");
        R.append(this.type);
        R.append(", entityId=");
        return a.F(R, this.entityId, ")");
    }
}
