package com.discord.api.guildscheduledevent;

import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: GuildScheduledEventBroadcast.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\f\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000fR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013¨\u0006\u0014"}, d2 = {"Lcom/discord/api/guildscheduledevent/GuildScheduledEventBroadcast;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "canBroadcast", "Z", "a", "()Z", "hasBroadcast", "Ljava/lang/Boolean;", "b", "()Ljava/lang/Boolean;", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildScheduledEventBroadcast {
    private final boolean canBroadcast;
    private final Boolean hasBroadcast;

    public final boolean a() {
        return this.canBroadcast;
    }

    public final Boolean b() {
        return this.hasBroadcast;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildScheduledEventBroadcast)) {
            return false;
        }
        GuildScheduledEventBroadcast guildScheduledEventBroadcast = (GuildScheduledEventBroadcast) obj;
        return this.canBroadcast == guildScheduledEventBroadcast.canBroadcast && m.areEqual(this.hasBroadcast, guildScheduledEventBroadcast.hasBroadcast);
    }

    public int hashCode() {
        boolean z2 = this.canBroadcast;
        if (z2) {
            z2 = true;
        }
        int i = z2 ? 1 : 0;
        int i2 = z2 ? 1 : 0;
        int i3 = i * 31;
        Boolean bool = this.hasBroadcast;
        return i3 + (bool != null ? bool.hashCode() : 0);
    }

    public String toString() {
        StringBuilder R = a.R("GuildScheduledEventBroadcast(canBroadcast=");
        R.append(this.canBroadcast);
        R.append(", hasBroadcast=");
        return a.C(R, this.hasBroadcast, ")");
    }
}
