package com.discord.api.guildscheduledevent;

import b.d.b.a.a;
import com.discord.api.guildmember.GuildMember;
import com.discord.api.user.User;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: ApiGuildScheduledEventUser.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010 \n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001J!\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u0005¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\fR\u001b\u0010\u001c\u001a\u0004\u0018\u00010\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u001a\u001a\u0004\b\u001d\u0010\fR\u0019\u0010\u001e\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010!R\u001f\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00040\"8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010$\u001a\u0004\b%\u0010&R\u001b\u0010'\u001a\u0004\u0018\u00010\n8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010\u001a\u001a\u0004\b(\u0010\f¨\u0006)"}, d2 = {"Lcom/discord/api/guildscheduledevent/ApiGuildScheduledEventUserGuildMember;", "", "Lcom/discord/api/user/User;", "user", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/api/guildmember/GuildMember;", "a", "(Lcom/discord/api/user/User;J)Lcom/discord/api/guildmember/GuildMember;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/utcdatetime/UtcDateTime;", "joinedAt", "Lcom/discord/api/utcdatetime/UtcDateTime;", "getJoinedAt", "()Lcom/discord/api/utcdatetime/UtcDateTime;", "avatar", "Ljava/lang/String;", "getAvatar", ModelAuditLogEntry.CHANGE_KEY_NICK, "getNick", "pending", "Z", "getPending", "()Z", "", "roles", "Ljava/util/List;", "getRoles", "()Ljava/util/List;", "premiumSince", "getPremiumSince", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ApiGuildScheduledEventUserGuildMember {
    private final String avatar;
    private final UtcDateTime joinedAt;
    private final String nick;
    private final boolean pending;
    private final String premiumSince;
    private final List<Long> roles;

    public final GuildMember a(User user, long j) {
        m.checkNotNullParameter(user, "user");
        return new GuildMember(j, user, this.roles, this.nick, this.premiumSince, this.joinedAt, this.pending, null, Long.valueOf(user.i()), this.avatar, null, null, null, 4096);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ApiGuildScheduledEventUserGuildMember)) {
            return false;
        }
        ApiGuildScheduledEventUserGuildMember apiGuildScheduledEventUserGuildMember = (ApiGuildScheduledEventUserGuildMember) obj;
        return m.areEqual(this.roles, apiGuildScheduledEventUserGuildMember.roles) && m.areEqual(this.nick, apiGuildScheduledEventUserGuildMember.nick) && m.areEqual(this.premiumSince, apiGuildScheduledEventUserGuildMember.premiumSince) && m.areEqual(this.joinedAt, apiGuildScheduledEventUserGuildMember.joinedAt) && this.pending == apiGuildScheduledEventUserGuildMember.pending && m.areEqual(this.avatar, apiGuildScheduledEventUserGuildMember.avatar);
    }

    public int hashCode() {
        List<Long> list = this.roles;
        int i = 0;
        int hashCode = (list != null ? list.hashCode() : 0) * 31;
        String str = this.nick;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.premiumSince;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        UtcDateTime utcDateTime = this.joinedAt;
        int hashCode4 = (hashCode3 + (utcDateTime != null ? utcDateTime.hashCode() : 0)) * 31;
        boolean z2 = this.pending;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        int i4 = (hashCode4 + i2) * 31;
        String str3 = this.avatar;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return i4 + i;
    }

    public String toString() {
        StringBuilder R = a.R("ApiGuildScheduledEventUserGuildMember(roles=");
        R.append(this.roles);
        R.append(", nick=");
        R.append(this.nick);
        R.append(", premiumSince=");
        R.append(this.premiumSince);
        R.append(", joinedAt=");
        R.append(this.joinedAt);
        R.append(", pending=");
        R.append(this.pending);
        R.append(", avatar=");
        return a.H(R, this.avatar, ")");
    }
}
