package com.discord.api.guildscheduledevent;

import andhook.lib.HookHelper;
import com.discord.api.guildscheduledevent.GuildScheduledEventStatus;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
/* compiled from: GuildScheduledEventStatus.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/api/guildscheduledevent/GuildScheduledEventStatusTypeAdapter;", "Lcom/google/gson/TypeAdapter;", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventStatus;", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildScheduledEventStatusTypeAdapter extends TypeAdapter<GuildScheduledEventStatus> {
    @Override // com.google.gson.TypeAdapter
    public GuildScheduledEventStatus read(JsonReader jsonReader) {
        m.checkNotNullParameter(jsonReader, "in");
        GuildScheduledEventStatus.Companion companion = GuildScheduledEventStatus.Companion;
        int y2 = jsonReader.y();
        Objects.requireNonNull(companion);
        if (y2 == 1) {
            return GuildScheduledEventStatus.SCHEDULED;
        }
        if (y2 == 2) {
            return GuildScheduledEventStatus.ACTIVE;
        }
        if (y2 == 3) {
            return GuildScheduledEventStatus.COMPLETED;
        }
        if (y2 != 4) {
            return GuildScheduledEventStatus.UNKNOWN;
        }
        return GuildScheduledEventStatus.CANCELED;
    }

    @Override // com.google.gson.TypeAdapter
    public void write(JsonWriter jsonWriter, GuildScheduledEventStatus guildScheduledEventStatus) {
        GuildScheduledEventStatus guildScheduledEventStatus2 = guildScheduledEventStatus;
        m.checkNotNullParameter(jsonWriter, "out");
        if (guildScheduledEventStatus2 != null) {
            jsonWriter.D(Integer.valueOf(guildScheduledEventStatus2.getApiValue()));
        } else {
            jsonWriter.s();
        }
    }
}
