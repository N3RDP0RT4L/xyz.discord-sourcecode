package com.discord.api.sticker;

import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.io.Serializable;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
/* compiled from: Sticker.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0015\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002J\u000f\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0004\u0010\u0005J\u000f\u0010\u0007\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\n\u001a\u00020\tH\u0016¢\u0006\u0004\b\n\u0010\u000bJ\u000f\u0010\r\u001a\u00020\fH\u0016¢\u0006\u0004\b\r\u0010\u000eJ\r\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0012\u0010\bJ\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0018\u001a\u00020\u000f2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u0019\u0010\u001a\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\bR\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 R\u0019\u0010\"\u001a\u00020!8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%R\u0019\u0010&\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010\u001b\u001a\u0004\b'\u0010\bR\u0019\u0010(\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010)\u001a\u0004\b*\u0010\u000bR\u0019\u0010+\u001a\u00020\u00038\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010,\u001a\u0004\b-\u0010\u0005R\u001b\u0010.\u001a\u0004\u0018\u00010\u00038\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010\u001e\u001a\u0004\b/\u0010 R\u0019\u00100\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010\u001b\u001a\u0004\b1\u0010\bR\u001b\u00102\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b2\u00103\u001a\u0004\b4\u00105¨\u00066"}, d2 = {"Lcom/discord/api/sticker/Sticker;", "Ljava/io/Serializable;", "Lcom/discord/api/sticker/BaseSticker;", "", "d", "()J", "", "b", "()Ljava/lang/String;", "Lcom/discord/api/sticker/StickerFormatType;", "a", "()Lcom/discord/api/sticker/StickerFormatType;", "Lcom/discord/api/sticker/StickerPartial;", "c", "()Lcom/discord/api/sticker/StickerPartial;", "", "l", "()Z", "toString", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", ModelAuditLogEntry.CHANGE_KEY_NAME, "Ljava/lang/String;", "h", "packId", "Ljava/lang/Long;", "i", "()Ljava/lang/Long;", "Lcom/discord/api/sticker/StickerType;", "type", "Lcom/discord/api/sticker/StickerType;", "k", "()Lcom/discord/api/sticker/StickerType;", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "f", "formatType", "Lcom/discord/api/sticker/StickerFormatType;", "getFormatType", ModelAuditLogEntry.CHANGE_KEY_ID, "J", "getId", "guildId", "g", ModelAuditLogEntry.CHANGE_KEY_TAGS, "j", ModelAuditLogEntry.CHANGE_KEY_AVAILABLE, "Ljava/lang/Boolean;", "e", "()Ljava/lang/Boolean;", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class Sticker implements Serializable, BaseSticker {
    private final Boolean available = null;
    private final String description;
    private final StickerFormatType formatType;
    private final Long guildId;

    /* renamed from: id  reason: collision with root package name */
    private final long f2054id;
    private final String name;
    private final Long packId;
    private final String tags;
    private final StickerType type;

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            StickerFormatType.values();
            int[] iArr = new int[4];
            $EnumSwitchMapping$0 = iArr;
            iArr[StickerFormatType.UNKNOWN.ordinal()] = 1;
            iArr[StickerFormatType.PNG.ordinal()] = 2;
            iArr[StickerFormatType.APNG.ordinal()] = 3;
            iArr[StickerFormatType.LOTTIE.ordinal()] = 4;
        }
    }

    public Sticker(long j, Long l, Long l2, String str, String str2, StickerFormatType stickerFormatType, String str3, StickerType stickerType, Boolean bool, int i) {
        int i2 = i & 256;
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(str2, ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION);
        m.checkNotNullParameter(stickerFormatType, "formatType");
        m.checkNotNullParameter(str3, ModelAuditLogEntry.CHANGE_KEY_TAGS);
        m.checkNotNullParameter(stickerType, "type");
        this.f2054id = j;
        this.packId = l;
        this.guildId = l2;
        this.name = str;
        this.description = str2;
        this.formatType = stickerFormatType;
        this.tags = str3;
        this.type = stickerType;
    }

    @Override // com.discord.api.sticker.BaseSticker
    public StickerFormatType a() {
        return this.formatType;
    }

    @Override // com.discord.api.sticker.BaseSticker
    public String b() {
        int ordinal = this.formatType.ordinal();
        if (ordinal == 0) {
            return "";
        }
        if (ordinal == 1 || ordinal == 2) {
            return ".png";
        }
        if (ordinal == 3) {
            return ".json";
        }
        throw new NoWhenBranchMatchedException();
    }

    @Override // com.discord.api.sticker.BaseSticker
    public StickerPartial c() {
        return new StickerPartial(this.f2054id, this.formatType, this.name);
    }

    @Override // com.discord.api.sticker.BaseSticker
    public long d() {
        return this.f2054id;
    }

    public final Boolean e() {
        return this.available;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Sticker)) {
            return false;
        }
        Sticker sticker = (Sticker) obj;
        return this.f2054id == sticker.f2054id && m.areEqual(this.packId, sticker.packId) && m.areEqual(this.guildId, sticker.guildId) && m.areEqual(this.name, sticker.name) && m.areEqual(this.description, sticker.description) && m.areEqual(this.formatType, sticker.formatType) && m.areEqual(this.tags, sticker.tags) && m.areEqual(this.type, sticker.type) && m.areEqual(this.available, sticker.available);
    }

    public final String f() {
        return this.description;
    }

    public final Long g() {
        return this.guildId;
    }

    public final long getId() {
        return this.f2054id;
    }

    public final String h() {
        return this.name;
    }

    public int hashCode() {
        long j = this.f2054id;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        Long l = this.packId;
        int i2 = 0;
        int hashCode = (i + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.guildId;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        String str = this.name;
        int hashCode3 = (hashCode2 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.description;
        int hashCode4 = (hashCode3 + (str2 != null ? str2.hashCode() : 0)) * 31;
        StickerFormatType stickerFormatType = this.formatType;
        int hashCode5 = (hashCode4 + (stickerFormatType != null ? stickerFormatType.hashCode() : 0)) * 31;
        String str3 = this.tags;
        int hashCode6 = (hashCode5 + (str3 != null ? str3.hashCode() : 0)) * 31;
        StickerType stickerType = this.type;
        int hashCode7 = (hashCode6 + (stickerType != null ? stickerType.hashCode() : 0)) * 31;
        Boolean bool = this.available;
        if (bool != null) {
            i2 = bool.hashCode();
        }
        return hashCode7 + i2;
    }

    public final Long i() {
        return this.packId;
    }

    public final String j() {
        return this.tags;
    }

    public final StickerType k() {
        return this.type;
    }

    public final boolean l() {
        StickerFormatType stickerFormatType = this.formatType;
        return stickerFormatType == StickerFormatType.APNG || stickerFormatType == StickerFormatType.LOTTIE;
    }

    public String toString() {
        StringBuilder R = a.R("Sticker(id=");
        R.append(this.f2054id);
        R.append(", packId=");
        R.append(this.packId);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", name=");
        R.append(this.name);
        R.append(", description=");
        R.append(this.description);
        R.append(", formatType=");
        R.append(this.formatType);
        R.append(", tags=");
        R.append(this.tags);
        R.append(", type=");
        R.append(this.type);
        R.append(", available=");
        return a.C(R, this.available, ")");
    }
}
