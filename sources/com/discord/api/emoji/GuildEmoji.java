package com.discord.api.emoji;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: GuildEmoji.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\f\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001BI\u0012\u0006\u0010!\u001a\u00020\u0017\u0012\u0006\u0010\u001e\u001a\u00020\u0002\u0012\u000e\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\u0017\u0018\u00010\u0016\u0012\u0006\u0010\u001c\u001a\u00020\t\u0012\u0006\u0010\u0010\u001a\u00020\t\u0012\u0006\u0010\f\u001a\u00020\t\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\t¢\u0006\u0004\b%\u0010&J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\f\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000fR\u0019\u0010\u0010\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\r\u001a\u0004\b\u0011\u0010\u000fR\u001b\u0010\u0012\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R!\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\u0017\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u0019\u0010\u001c\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\r\u001a\u0004\b\u001d\u0010\u000fR\u0019\u0010\u001e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010\u0004R\u0019\u0010!\u001a\u00020\u00178\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010$¨\u0006'"}, d2 = {"Lcom/discord/api/emoji/GuildEmoji;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "animated", "Z", "a", "()Z", "managed", "d", ModelAuditLogEntry.CHANGE_KEY_AVAILABLE, "Ljava/lang/Boolean;", "b", "()Ljava/lang/Boolean;", "", "", "roles", "Ljava/util/List;", "g", "()Ljava/util/List;", "requireColons", "f", ModelAuditLogEntry.CHANGE_KEY_NAME, "Ljava/lang/String;", "e", ModelAuditLogEntry.CHANGE_KEY_ID, "J", "c", "()J", HookHelper.constructorName, "(JLjava/lang/String;Ljava/util/List;ZZZLjava/lang/Boolean;)V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildEmoji {
    private final boolean animated;
    private final Boolean available;

    /* renamed from: id  reason: collision with root package name */
    private final long f2029id;
    private final boolean managed;
    private final String name;
    private final boolean requireColons;
    private final List<Long> roles;

    public GuildEmoji(long j, String str, List<Long> list, boolean z2, boolean z3, boolean z4, Boolean bool) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        this.f2029id = j;
        this.name = str;
        this.roles = list;
        this.requireColons = z2;
        this.managed = z3;
        this.animated = z4;
        this.available = bool;
    }

    public final boolean a() {
        return this.animated;
    }

    public final Boolean b() {
        return this.available;
    }

    public final long c() {
        return this.f2029id;
    }

    public final boolean d() {
        return this.managed;
    }

    public final String e() {
        return this.name;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildEmoji)) {
            return false;
        }
        GuildEmoji guildEmoji = (GuildEmoji) obj;
        return this.f2029id == guildEmoji.f2029id && m.areEqual(this.name, guildEmoji.name) && m.areEqual(this.roles, guildEmoji.roles) && this.requireColons == guildEmoji.requireColons && this.managed == guildEmoji.managed && this.animated == guildEmoji.animated && m.areEqual(this.available, guildEmoji.available);
    }

    public final boolean f() {
        return this.requireColons;
    }

    public final List<Long> g() {
        return this.roles;
    }

    public int hashCode() {
        long j = this.f2029id;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        String str = this.name;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        List<Long> list = this.roles;
        int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
        boolean z2 = this.requireColons;
        int i3 = 1;
        if (z2) {
            z2 = true;
        }
        int i4 = z2 ? 1 : 0;
        int i5 = z2 ? 1 : 0;
        int i6 = (hashCode2 + i4) * 31;
        boolean z3 = this.managed;
        if (z3) {
            z3 = true;
        }
        int i7 = z3 ? 1 : 0;
        int i8 = z3 ? 1 : 0;
        int i9 = (i6 + i7) * 31;
        boolean z4 = this.animated;
        if (!z4) {
            i3 = z4 ? 1 : 0;
        }
        int i10 = (i9 + i3) * 31;
        Boolean bool = this.available;
        if (bool != null) {
            i2 = bool.hashCode();
        }
        return i10 + i2;
    }

    public String toString() {
        StringBuilder R = a.R("GuildEmoji(id=");
        R.append(this.f2029id);
        R.append(", name=");
        R.append(this.name);
        R.append(", roles=");
        R.append(this.roles);
        R.append(", requireColons=");
        R.append(this.requireColons);
        R.append(", managed=");
        R.append(this.managed);
        R.append(", animated=");
        R.append(this.animated);
        R.append(", available=");
        return a.C(R, this.available, ")");
    }
}
