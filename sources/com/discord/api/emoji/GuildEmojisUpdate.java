package com.discord.api.emoji;

import b.d.b.a.a;
import com.discord.api.guildhash.GuildHashes;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: GuildEmojisUpdate.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\r\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u0019\u0010\u0018\u001a\u00020\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001b¨\u0006\u001c"}, d2 = {"Lcom/discord/api/emoji/GuildEmojisUpdate;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "guildId", "J", "c", "()J", "", "Lcom/discord/api/emoji/GuildEmoji;", "emojis", "Ljava/util/List;", "a", "()Ljava/util/List;", "Lcom/discord/api/guildhash/GuildHashes;", "guildHashes", "Lcom/discord/api/guildhash/GuildHashes;", "b", "()Lcom/discord/api/guildhash/GuildHashes;", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildEmojisUpdate {
    private final List<GuildEmoji> emojis;
    private final GuildHashes guildHashes;
    private final long guildId;

    public final List<GuildEmoji> a() {
        return this.emojis;
    }

    public final GuildHashes b() {
        return this.guildHashes;
    }

    public final long c() {
        return this.guildId;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildEmojisUpdate)) {
            return false;
        }
        GuildEmojisUpdate guildEmojisUpdate = (GuildEmojisUpdate) obj;
        return this.guildId == guildEmojisUpdate.guildId && m.areEqual(this.emojis, guildEmojisUpdate.emojis) && m.areEqual(this.guildHashes, guildEmojisUpdate.guildHashes);
    }

    public int hashCode() {
        long j = this.guildId;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        List<GuildEmoji> list = this.emojis;
        int i2 = 0;
        int hashCode = (i + (list != null ? list.hashCode() : 0)) * 31;
        GuildHashes guildHashes = this.guildHashes;
        if (guildHashes != null) {
            i2 = guildHashes.hashCode();
        }
        return hashCode + i2;
    }

    public String toString() {
        StringBuilder R = a.R("GuildEmojisUpdate(guildId=");
        R.append(this.guildId);
        R.append(", emojis=");
        R.append(this.emojis);
        R.append(", guildHashes=");
        R.append(this.guildHashes);
        R.append(")");
        return R.toString();
    }
}
