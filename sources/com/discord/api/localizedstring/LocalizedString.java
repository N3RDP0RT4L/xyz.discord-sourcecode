package com.discord.api.localizedstring;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import java.io.Serializable;
import java.util.Map;
import kotlin.Metadata;
/* compiled from: LocalizedString.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010$\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\r\u001a\u00020\u0002\u0012\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00020\u0010¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\u000b\u001a\u00020\n2\b\u0010\t\u001a\u0004\u0018\u00010\bHÖ\u0003¢\u0006\u0004\b\u000b\u0010\fR\u0019\u0010\r\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0004R%\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00020\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014¨\u0006\u0017"}, d2 = {"Lcom/discord/api/localizedstring/LocalizedString;", "Ljava/io/Serializable;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "default", "Ljava/lang/String;", "a", "", "localizations", "Ljava/util/Map;", "b", "()Ljava/util/Map;", HookHelper.constructorName, "(Ljava/lang/String;Ljava/util/Map;)V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class LocalizedString implements Serializable {

    /* renamed from: default  reason: not valid java name */
    private final String f4default;
    private final Map<String, String> localizations;

    public LocalizedString(String str, Map<String, String> map) {
        m.checkNotNullParameter(str, "default");
        m.checkNotNullParameter(map, "localizations");
        this.f4default = str;
        this.localizations = map;
    }

    public final String a() {
        return this.f4default;
    }

    public final Map<String, String> b() {
        return this.localizations;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LocalizedString)) {
            return false;
        }
        LocalizedString localizedString = (LocalizedString) obj;
        return m.areEqual(this.f4default, localizedString.f4default) && m.areEqual(this.localizations, localizedString.localizations);
    }

    public int hashCode() {
        String str = this.f4default;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Map<String, String> map = this.localizations;
        if (map != null) {
            i = map.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder R = a.R("LocalizedString(default=");
        R.append(this.f4default);
        R.append(", localizations=");
        return a.L(R, this.localizations, ")");
    }
}
