package com.discord.api.localizedstring;

import andhook.lib.HookHelper;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import d0.t.h0;
import d0.z.d.m;
import java.util.HashMap;
import java.util.Map;
import kotlin.Metadata;
/* compiled from: LocalizedString.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/api/localizedstring/LocalizedStringTypeAdapter;", "Lcom/google/gson/TypeAdapter;", "Lcom/discord/api/localizedstring/LocalizedString;", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class LocalizedStringTypeAdapter extends TypeAdapter<LocalizedString> {

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            JsonToken.values();
            int[] iArr = new int[10];
            $EnumSwitchMapping$0 = iArr;
            iArr[2] = 1;
            iArr[5] = 2;
        }
    }

    @Override // com.google.gson.TypeAdapter
    public LocalizedString read(JsonReader jsonReader) {
        m.checkNotNullParameter(jsonReader, "in");
        JsonToken N = jsonReader.N();
        if (N != null) {
            int ordinal = N.ordinal();
            if (ordinal == 2) {
                jsonReader.b();
                HashMap hashMap = new HashMap();
                String str = "";
                while (jsonReader.q()) {
                    String C = jsonReader.C();
                    if (C != null) {
                        int hashCode = C.hashCode();
                        if (hashCode != 597626106) {
                            if (hashCode == 1544803905 && C.equals("default")) {
                                str = jsonReader.J();
                                m.checkNotNullExpressionValue(str, "`in`.nextString()");
                            }
                        } else if (C.equals("localizations")) {
                            jsonReader.b();
                            while (jsonReader.q()) {
                                String C2 = jsonReader.C();
                                String J = jsonReader.J();
                                m.checkNotNullExpressionValue(C2, "locale");
                                m.checkNotNullExpressionValue(J, "localization");
                                hashMap.put(C2, J);
                            }
                            jsonReader.f();
                        }
                    }
                }
                jsonReader.f();
                return new LocalizedString(str, hashMap);
            } else if (ordinal == 5) {
                String J2 = jsonReader.J();
                m.checkNotNullExpressionValue(J2, "default");
                return new LocalizedString(J2, h0.emptyMap());
            }
        }
        throw new IllegalArgumentException("could not parse localized string. token was: " + N);
    }

    @Override // com.google.gson.TypeAdapter
    public void write(JsonWriter jsonWriter, LocalizedString localizedString) {
        LocalizedString localizedString2 = localizedString;
        m.checkNotNullParameter(jsonWriter, "out");
        if (localizedString2 != null) {
            jsonWriter.c();
            jsonWriter.n("default");
            jsonWriter.H(localizedString2.a());
            jsonWriter.n("localizations");
            jsonWriter.c();
            for (Map.Entry<String, String> entry : localizedString2.b().entrySet()) {
                jsonWriter.n(entry.getKey());
                jsonWriter.H(entry.getValue());
            }
            jsonWriter.f();
            jsonWriter.f();
            return;
        }
        jsonWriter.s();
    }
}
