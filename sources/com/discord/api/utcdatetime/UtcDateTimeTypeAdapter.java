package com.discord.api.utcdatetime;

import andhook.lib.HookHelper;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import d0.z.d.m;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: UtcDateTime.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 \u00052\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u0001:\u0001\u0005B\u0007¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0006"}, d2 = {"Lcom/discord/api/utcdatetime/UtcDateTimeTypeAdapter;", "Lcom/google/gson/TypeAdapter;", "Lcom/discord/api/utcdatetime/UtcDateTime;", HookHelper.constructorName, "()V", "Companion", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class UtcDateTimeTypeAdapter extends TypeAdapter<UtcDateTime> {
    public static final Companion Companion = new Companion(null);
    private static final String FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    /* compiled from: UtcDateTime.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/api/utcdatetime/UtcDateTimeTypeAdapter$Companion;", "", "", "FORMAT", "Ljava/lang/String;", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        public Companion() {
        }

        public Companion(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    @Override // com.google.gson.TypeAdapter
    public UtcDateTime read(JsonReader jsonReader) {
        m.checkNotNullParameter(jsonReader, "in");
        if (jsonReader.N() == JsonToken.NULL) {
            jsonReader.H();
            return null;
        }
        String J = jsonReader.J();
        long j = 0;
        if (J != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ROOT);
                simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date parse = simpleDateFormat.parse(J);
                if (parse != null) {
                    j = parse.getTime();
                }
            } catch (ArrayIndexOutOfBoundsException | NumberFormatException | ParseException unused) {
            }
        }
        return new UtcDateTime(j);
    }

    @Override // com.google.gson.TypeAdapter
    public void write(JsonWriter jsonWriter, UtcDateTime utcDateTime) {
        UtcDateTime utcDateTime2 = utcDateTime;
        m.checkNotNullParameter(jsonWriter, "out");
        if (utcDateTime2 != null) {
            long g = utcDateTime2.g();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ROOT);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            jsonWriter.H(simpleDateFormat.format(new Date(g)));
            return;
        }
        jsonWriter.s();
    }
}
