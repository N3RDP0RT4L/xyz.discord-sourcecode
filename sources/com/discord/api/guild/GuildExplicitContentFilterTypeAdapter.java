package com.discord.api.guild;

import andhook.lib.HookHelper;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: GuildExplicitContentFilter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/api/guild/GuildExplicitContentFilterTypeAdapter;", "Lcom/google/gson/TypeAdapter;", "Lcom/discord/api/guild/GuildExplicitContentFilter;", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildExplicitContentFilterTypeAdapter extends TypeAdapter<GuildExplicitContentFilter> {
    @Override // com.google.gson.TypeAdapter
    public GuildExplicitContentFilter read(JsonReader jsonReader) {
        GuildExplicitContentFilter guildExplicitContentFilter;
        m.checkNotNullParameter(jsonReader, "in");
        int y2 = jsonReader.y();
        GuildExplicitContentFilter[] values = GuildExplicitContentFilter.values();
        int i = 0;
        while (true) {
            if (i >= 3) {
                guildExplicitContentFilter = null;
                break;
            }
            guildExplicitContentFilter = values[i];
            if (guildExplicitContentFilter.getApiValue() == y2) {
                break;
            }
            i++;
        }
        return guildExplicitContentFilter != null ? guildExplicitContentFilter : GuildExplicitContentFilter.NONE;
    }

    @Override // com.google.gson.TypeAdapter
    public void write(JsonWriter jsonWriter, GuildExplicitContentFilter guildExplicitContentFilter) {
        GuildExplicitContentFilter guildExplicitContentFilter2 = guildExplicitContentFilter;
        m.checkNotNullParameter(jsonWriter, "out");
        if (guildExplicitContentFilter2 != null) {
            jsonWriter.D(Integer.valueOf(guildExplicitContentFilter2.getApiValue()));
        } else {
            jsonWriter.s();
        }
    }
}
