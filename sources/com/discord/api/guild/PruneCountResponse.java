package com.discord.api.guild;

import b.d.b.a.a;
import kotlin.Metadata;
/* compiled from: PruneCountResponse.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u0007¨\u0006\u000f"}, d2 = {"Lcom/discord/api/guild/PruneCountResponse;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "pruned", "I", "a", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class PruneCountResponse {
    private final int pruned;

    public final int a() {
        return this.pruned;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof PruneCountResponse) && this.pruned == ((PruneCountResponse) obj).pruned;
        }
        return true;
    }

    public int hashCode() {
        return this.pruned;
    }

    public String toString() {
        return a.A(a.R("PruneCountResponse(pruned="), this.pruned, ")");
    }
}
