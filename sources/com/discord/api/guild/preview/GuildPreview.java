package com.discord.api.guild.preview;

import b.d.b.a.a;
import com.discord.api.emoji.GuildEmoji;
import com.discord.api.guild.GuildFeature;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: GuildPreview.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u0004R\u0019\u0010\u0010\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R\u001f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00150\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u001dR\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\r\u001a\u0004\b\u001f\u0010\u0004R\u001b\u0010 \u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#R\u001b\u0010$\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\r\u001a\u0004\b%\u0010\u0004R\u001b\u0010&\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010\r\u001a\u0004\b'\u0010\u0004R\u001b\u0010(\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010!\u001a\u0004\b)\u0010#R\u001f\u0010+\u001a\b\u0012\u0004\u0012\u00020*0\u00148\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010\u0017\u001a\u0004\b,\u0010\u0019R\u001b\u0010-\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\r\u001a\u0004\b.\u0010\u0004¨\u0006/"}, d2 = {"Lcom/discord/api/guild/preview/GuildPreview;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", ModelAuditLogEntry.CHANGE_KEY_NAME, "Ljava/lang/String;", "i", "", ModelAuditLogEntry.CHANGE_KEY_ID, "J", "h", "()J", "", "Lcom/discord/api/guild/GuildFeature;", "features", "Ljava/util/List;", "f", "()Ljava/util/List;", "featurableInDirectory", "Ljava/lang/Boolean;", "e", "()Ljava/lang/Boolean;", "splash", "j", "approximateMemberCount", "Ljava/lang/Integer;", "a", "()Ljava/lang/Integer;", "icon", "g", "banner", "getBanner", "approximatePresenceCount", "b", "Lcom/discord/api/emoji/GuildEmoji;", "emojis", "d", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "c", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildPreview {
    private final Integer approximateMemberCount;
    private final Integer approximatePresenceCount;
    private final String banner;
    private final String description;
    private final List<GuildEmoji> emojis;
    private final Boolean featurableInDirectory;
    private final List<GuildFeature> features;
    private final String icon;

    /* renamed from: id  reason: collision with root package name */
    private final long f2031id;
    private final String name;
    private final String splash;

    public final Integer a() {
        return this.approximateMemberCount;
    }

    public final Integer b() {
        return this.approximatePresenceCount;
    }

    public final String c() {
        return this.description;
    }

    public final List<GuildEmoji> d() {
        return this.emojis;
    }

    public final Boolean e() {
        return this.featurableInDirectory;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildPreview)) {
            return false;
        }
        GuildPreview guildPreview = (GuildPreview) obj;
        return this.f2031id == guildPreview.f2031id && m.areEqual(this.name, guildPreview.name) && m.areEqual(this.description, guildPreview.description) && m.areEqual(this.splash, guildPreview.splash) && m.areEqual(this.banner, guildPreview.banner) && m.areEqual(this.icon, guildPreview.icon) && m.areEqual(this.approximatePresenceCount, guildPreview.approximatePresenceCount) && m.areEqual(this.approximateMemberCount, guildPreview.approximateMemberCount) && m.areEqual(this.emojis, guildPreview.emojis) && m.areEqual(this.features, guildPreview.features) && m.areEqual(this.featurableInDirectory, guildPreview.featurableInDirectory);
    }

    public final List<GuildFeature> f() {
        return this.features;
    }

    public final String g() {
        return this.icon;
    }

    public final long h() {
        return this.f2031id;
    }

    public int hashCode() {
        long j = this.f2031id;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        String str = this.name;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.description;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.splash;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.banner;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.icon;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        Integer num = this.approximatePresenceCount;
        int hashCode6 = (hashCode5 + (num != null ? num.hashCode() : 0)) * 31;
        Integer num2 = this.approximateMemberCount;
        int hashCode7 = (hashCode6 + (num2 != null ? num2.hashCode() : 0)) * 31;
        List<GuildEmoji> list = this.emojis;
        int hashCode8 = (hashCode7 + (list != null ? list.hashCode() : 0)) * 31;
        List<GuildFeature> list2 = this.features;
        int hashCode9 = (hashCode8 + (list2 != null ? list2.hashCode() : 0)) * 31;
        Boolean bool = this.featurableInDirectory;
        if (bool != null) {
            i2 = bool.hashCode();
        }
        return hashCode9 + i2;
    }

    public final String i() {
        return this.name;
    }

    public final String j() {
        return this.splash;
    }

    public String toString() {
        StringBuilder R = a.R("GuildPreview(id=");
        R.append(this.f2031id);
        R.append(", name=");
        R.append(this.name);
        R.append(", description=");
        R.append(this.description);
        R.append(", splash=");
        R.append(this.splash);
        R.append(", banner=");
        R.append(this.banner);
        R.append(", icon=");
        R.append(this.icon);
        R.append(", approximatePresenceCount=");
        R.append(this.approximatePresenceCount);
        R.append(", approximateMemberCount=");
        R.append(this.approximateMemberCount);
        R.append(", emojis=");
        R.append(this.emojis);
        R.append(", features=");
        R.append(this.features);
        R.append(", featurableInDirectory=");
        return a.C(R, this.featurableInDirectory, ")");
    }
}
