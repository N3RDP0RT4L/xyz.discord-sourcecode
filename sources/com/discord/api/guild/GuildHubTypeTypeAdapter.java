package com.discord.api.guild;

import andhook.lib.HookHelper;
import b.c.a.a0.d;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: GuildHubType.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/api/guild/GuildHubTypeTypeAdapter;", "Lcom/google/gson/TypeAdapter;", "Lcom/discord/api/guild/GuildHubType;", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildHubTypeTypeAdapter extends TypeAdapter<GuildHubType> {
    @Override // com.google.gson.TypeAdapter
    public GuildHubType read(JsonReader jsonReader) {
        m.checkNotNullParameter(jsonReader, "in");
        Integer n1 = d.n1(jsonReader);
        GuildHubType[] values = GuildHubType.values();
        for (int i = 0; i < 3; i++) {
            GuildHubType guildHubType = values[i];
            if (n1 != null && guildHubType.getApiValue() == n1.intValue()) {
                return guildHubType;
            }
        }
        return null;
    }

    @Override // com.google.gson.TypeAdapter
    public void write(JsonWriter jsonWriter, GuildHubType guildHubType) {
        GuildHubType guildHubType2 = guildHubType;
        m.checkNotNullParameter(jsonWriter, "out");
        if (guildHubType2 == null) {
            jsonWriter.s();
        } else {
            jsonWriter.D(Integer.valueOf(guildHubType2.getApiValue()));
        }
    }
}
