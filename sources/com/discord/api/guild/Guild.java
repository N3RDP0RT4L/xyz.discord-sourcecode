package com.discord.api.guild;

import andhook.lib.HookHelper;
import androidx.constraintlayout.solver.widgets.analyzer.BasicMeasure;
import androidx.exifinterface.media.ExifInterface;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.embeddedactivities.EmbeddedActivityInGuild;
import com.discord.api.emoji.GuildEmoji;
import com.discord.api.guild.welcome.GuildWelcomeScreen;
import com.discord.api.guildhash.GuildHashes;
import com.discord.api.guildmember.GuildMember;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.presence.Presence;
import com.discord.api.role.GuildRole;
import com.discord.api.stageinstance.StageInstance;
import com.discord.api.sticker.Sticker;
import com.discord.api.voice.state.VoiceState;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
/* compiled from: Guild.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000²\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0018\u0002\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u008f\u0004\u0012\u000e\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\u0015\u0018\u00010\u000f\u0012\u000e\u0010G\u001a\n\u0012\u0004\u0012\u00020F\u0018\u00010\u000f\u0012\u000e\u0010L\u001a\n\u0012\u0004\u0012\u00020K\u0018\u00010\u000f\u0012\u0007\u0010\u0088\u0001\u001a\u00020\u0002\u0012\b\u0010j\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010~\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u00109\u001a\u00020\u0018\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\u001d\u001a\u00020\u0018\u0012\b\u0010N\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010m\u001a\u0004\u0018\u00010l\u0012\b\u0010f\u001a\u0004\u0018\u00010e\u0012\u000e\u0010r\u001a\n\u0012\u0004\u0012\u00020q\u0018\u00010\u000f\u0012\u000e\u0010c\u001a\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000f\u0012\u000e\u0010U\u001a\n\u0012\u0004\u0012\u00020T\u0018\u00010\u000f\u0012\u000e\u0010a\u001a\n\u0012\u0004\u0012\u00020`\u0018\u00010\u000f\u0012\u0006\u0010;\u001a\u00020\t\u0012\u0006\u0010P\u001a\u00020\u0005\u0012\u0007\u0010\u008f\u0001\u001a\u00020\u0005\u0012\b\u0010/\u001a\u0004\u0018\u00010\u0018\u0012\b\u0010\u0019\u001a\u0004\u0018\u00010\u0018\u0012\u000e\u0010\u008b\u0001\u001a\t\u0012\u0005\u0012\u00030\u008a\u00010\u000f\u0012\u0006\u0010I\u001a\u00020\u0005\u0012\b\u0010&\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010t\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u00101\u001a\u00020\u0005\u0012\u0007\u0010\u008d\u0001\u001a\u00020\u0005\u0012\u0006\u0010R\u001a\u00020\u0005\u0012\t\u0010\u0082\u0001\u001a\u0004\u0018\u00010\u0002\u0012\t\u0010\u0084\u0001\u001a\u0004\u0018\u00010\u0018\u0012\b\u0010-\u001a\u0004\u0018\u00010\u0018\u0012\b\u0010D\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010v\u001a\u0004\u0018\u00010u\u0012\b\u0010X\u001a\u0004\u0018\u00010W\u0012\b\u0010$\u001a\u0004\u0018\u00010\u0002\u0012\u0007\u0010\u0086\u0001\u001a\u00020\u0005\u0012\u0006\u0010z\u001a\u00020\u0005\u0012\b\u0010\\\u001a\u0004\u0018\u00010\u0018\u0012\b\u0010)\u001a\u0004\u0018\u00010(\u0012\u000e\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000f\u0012\u000e\u0010|\u001a\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000f\u0012\u0006\u0010^\u001a\u00020\t\u0012\u000e\u00105\u001a\n\u0012\u0004\u0012\u000204\u0018\u00010\u000f\u0012\u000e\u00107\u001a\n\u0012\u0004\u0012\u000206\u0018\u00010\u000f\u0012\u000e\u0010\"\u001a\n\u0012\u0004\u0012\u00020!\u0018\u00010\u000f\u0012\b\u0010@\u001a\u0004\u0018\u00010?¢\u0006\u0006\b\u0091\u0001\u0010\u0092\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u0004R!\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R!\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\u0015\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0012\u001a\u0004\b\u0017\u0010\u0014R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u00188\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001cR\u0019\u0010\u001d\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 R!\u0010\"\u001a\n\u0012\u0004\u0012\u00020!\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u0012\u001a\u0004\b#\u0010\u0014R\u001b\u0010$\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\r\u001a\u0004\b%\u0010\u0004R\u001b\u0010&\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010\r\u001a\u0004\b'\u0010\u0004R\u001b\u0010)\u001a\u0004\u0018\u00010(8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010*\u001a\u0004\b+\u0010,R\u001b\u0010-\u001a\u0004\u0018\u00010\u00188\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u001a\u001a\u0004\b.\u0010\u001cR\u001b\u0010/\u001a\u0004\u0018\u00010\u00188\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u001a\u001a\u0004\b0\u0010\u001cR\u0019\u00101\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b1\u00102\u001a\u0004\b3\u0010\u0007R!\u00105\u001a\n\u0012\u0004\u0012\u000204\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010\u0012\u001a\u0004\b\u001e\u0010\u0014R!\u00107\u001a\n\u0012\u0004\u0012\u000206\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010\u0012\u001a\u0004\b8\u0010\u0014R\u0019\u00109\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010\u001e\u001a\u0004\b:\u0010 R\u0019\u0010;\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010<\u001a\u0004\b=\u0010>R\u001b\u0010@\u001a\u0004\u0018\u00010?8\u0006@\u0006¢\u0006\f\n\u0004\b@\u0010A\u001a\u0004\bB\u0010CR\u001b\u0010D\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\bD\u0010\r\u001a\u0004\bE\u0010\u0004R!\u0010G\u001a\n\u0012\u0004\u0012\u00020F\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bG\u0010\u0012\u001a\u0004\bH\u0010\u0014R\u0019\u0010I\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\bI\u00102\u001a\u0004\bJ\u0010\u0007R!\u0010L\u001a\n\u0012\u0004\u0012\u00020K\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bL\u0010\u0012\u001a\u0004\bM\u0010\u0014R\u001b\u0010N\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\bN\u0010\r\u001a\u0004\bO\u0010\u0004R\u0019\u0010P\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\bP\u00102\u001a\u0004\bQ\u0010\u0007R\u0019\u0010R\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\bR\u00102\u001a\u0004\bS\u0010\u0007R!\u0010U\u001a\n\u0012\u0004\u0012\u00020T\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bU\u0010\u0012\u001a\u0004\bV\u0010\u0014R\u001b\u0010X\u001a\u0004\u0018\u00010W8\u0006@\u0006¢\u0006\f\n\u0004\bX\u0010Y\u001a\u0004\bZ\u0010[R\u001b\u0010\\\u001a\u0004\u0018\u00010\u00188\u0006@\u0006¢\u0006\f\n\u0004\b\\\u0010\u001a\u001a\u0004\b]\u0010\u001cR\u0019\u0010^\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b^\u0010<\u001a\u0004\b_\u0010>R!\u0010a\u001a\n\u0012\u0004\u0012\u00020`\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\ba\u0010\u0012\u001a\u0004\bb\u0010\u0014R!\u0010c\u001a\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\bc\u0010\u0012\u001a\u0004\bd\u0010\u0014R\u001b\u0010f\u001a\u0004\u0018\u00010e8\u0006@\u0006¢\u0006\f\n\u0004\bf\u0010g\u001a\u0004\bh\u0010iR\u001b\u0010j\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\bj\u0010\r\u001a\u0004\bk\u0010\u0004R\u001b\u0010m\u001a\u0004\u0018\u00010l8\u0006@\u0006¢\u0006\f\n\u0004\bm\u0010n\u001a\u0004\bo\u0010pR!\u0010r\u001a\n\u0012\u0004\u0012\u00020q\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\br\u0010\u0012\u001a\u0004\bs\u0010\u0014R\u001b\u0010t\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\bt\u0010\r\u001a\u0004\b2\u0010\u0004R\u001b\u0010v\u001a\u0004\u0018\u00010u8\u0006@\u0006¢\u0006\f\n\u0004\bv\u0010w\u001a\u0004\bx\u0010yR\u0019\u0010z\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\bz\u00102\u001a\u0004\b{\u0010\u0007R!\u0010|\u001a\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b|\u0010\u0012\u001a\u0004\b}\u0010\u0014R\u001d\u0010~\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\u000e\n\u0004\b~\u0010\u007f\u001a\u0006\b\u0080\u0001\u0010\u0081\u0001R\u001e\u0010\u0082\u0001\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\u000e\n\u0005\b\u0082\u0001\u0010\r\u001a\u0005\b\u0083\u0001\u0010\u0004R\u001e\u0010\u0084\u0001\u001a\u0004\u0018\u00010\u00188\u0006@\u0006¢\u0006\u000e\n\u0005\b\u0084\u0001\u0010\u001a\u001a\u0005\b\u0085\u0001\u0010\u001cR\u001c\u0010\u0086\u0001\u001a\u00020\u00058\u0006@\u0006¢\u0006\u000e\n\u0005\b\u0086\u0001\u00102\u001a\u0005\b\u0087\u0001\u0010\u0007R\u001c\u0010\u0088\u0001\u001a\u00020\u00028\u0006@\u0006¢\u0006\u000e\n\u0005\b\u0088\u0001\u0010\r\u001a\u0005\b\u0089\u0001\u0010\u0004R#\u0010\u008b\u0001\u001a\t\u0012\u0005\u0012\u00030\u008a\u00010\u000f8\u0006@\u0006¢\u0006\u000e\n\u0005\b\u008b\u0001\u0010\u0012\u001a\u0005\b\u008c\u0001\u0010\u0014R\u001c\u0010\u008d\u0001\u001a\u00020\u00058\u0006@\u0006¢\u0006\u000e\n\u0005\b\u008d\u0001\u00102\u001a\u0005\b\u008e\u0001\u0010\u0007R\u001c\u0010\u008f\u0001\u001a\u00020\u00058\u0006@\u0006¢\u0006\u000e\n\u0005\b\u008f\u0001\u00102\u001a\u0005\b\u0090\u0001\u0010\u0007¨\u0006\u0093\u0001"}, d2 = {"Lcom/discord/api/guild/Guild;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", ModelAuditLogEntry.CHANGE_KEY_REGION, "Ljava/lang/String;", "F", "", "Lcom/discord/api/channel/Channel;", "channelUpdates", "Ljava/util/List;", "f", "()Ljava/util/List;", "Lcom/discord/api/role/GuildRole;", "roles", "G", "", "systemChannelId", "Ljava/lang/Long;", "M", "()Ljava/lang/Long;", "ownerId", "J", "z", "()J", "Lcom/discord/api/embeddedactivities/EmbeddedActivityInGuild;", "embeddedActivities", "j", "vanityUrlCode", "P", "banner", "e", "Lcom/discord/api/guildhash/GuildHashes;", "guildHashes", "Lcom/discord/api/guildhash/GuildHashes;", "n", "()Lcom/discord/api/guildhash/GuildHashes;", "publicUpdatesChannelId", ExifInterface.LONGITUDE_EAST, "afkChannelId", "b", "premiumTier", "I", "C", "Lcom/discord/api/stageinstance/StageInstance;", "stageInstances", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "guildScheduledEvents", "o", ModelAuditLogEntry.CHANGE_KEY_ID, "r", "unavailable", "Z", "O", "()Z", "Lcom/discord/api/guild/GuildHubType;", "hubType", "Lcom/discord/api/guild/GuildHubType;", "p", "()Lcom/discord/api/guild/GuildHubType;", "preferredLocale", ExifInterface.GPS_MEASUREMENT_IN_PROGRESS, "Lcom/discord/api/emoji/GuildEmoji;", "emojis", "k", "memberCount", "u", "Lcom/discord/api/sticker/Sticker;", "stickers", "K", "icon", "q", "mfaLevel", "w", "systemChannelFlags", "L", "Lcom/discord/api/guildmember/GuildMember;", "members", "v", "Lcom/discord/api/guild/GuildMaxVideoChannelUsers;", "maxVideoChannelUsers", "Lcom/discord/api/guild/GuildMaxVideoChannelUsers;", "t", "()Lcom/discord/api/guild/GuildMaxVideoChannelUsers;", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS, "getPermissions", ModelAuditLogEntry.CHANGE_KEY_NSFW, "y", "Lcom/discord/api/voice/state/VoiceState;", "voiceStates", "R", "channels", "g", "Lcom/discord/api/guild/GuildExplicitContentFilter;", "explicitContentFilter", "Lcom/discord/api/guild/GuildExplicitContentFilter;", "l", "()Lcom/discord/api/guild/GuildExplicitContentFilter;", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "i", "Lcom/discord/api/guild/GuildVerificationLevel;", "verificationLevel", "Lcom/discord/api/guild/GuildVerificationLevel;", "Q", "()Lcom/discord/api/guild/GuildVerificationLevel;", "Lcom/discord/api/presence/Presence;", "presences", "D", "splash", "Lcom/discord/api/guild/welcome/GuildWelcomeScreen;", "welcomeScreen", "Lcom/discord/api/guild/welcome/GuildWelcomeScreen;", ExifInterface.LATITUDE_SOUTH, "()Lcom/discord/api/guild/welcome/GuildWelcomeScreen;", "approximatePresenceCount", "d", "threads", "N", "defaultMessageNotifications", "Ljava/lang/Integer;", "h", "()Ljava/lang/Integer;", "joinedAt", "s", "rulesChannelId", "H", "approximateMemberCount", "getApproximateMemberCount", ModelAuditLogEntry.CHANGE_KEY_NAME, "x", "Lcom/discord/api/guild/GuildFeature;", "features", "m", "premiumSubscriptionCount", "B", "afkTimeout", "c", HookHelper.constructorName, "(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;JLjava/lang/String;JLjava/lang/String;Lcom/discord/api/guild/GuildVerificationLevel;Lcom/discord/api/guild/GuildExplicitContentFilter;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;ZIILjava/lang/Long;Ljava/lang/Long;Ljava/util/List;ILjava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Lcom/discord/api/guild/welcome/GuildWelcomeScreen;Lcom/discord/api/guild/GuildMaxVideoChannelUsers;Ljava/lang/String;IILjava/lang/Long;Lcom/discord/api/guildhash/GuildHashes;Ljava/util/List;Ljava/util/List;ZLjava/util/List;Ljava/util/List;Ljava/util/List;Lcom/discord/api/guild/GuildHubType;)V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class Guild {
    private final Long afkChannelId;
    private final int afkTimeout;
    private final int approximateMemberCount;
    private final int approximatePresenceCount;
    private final String banner;
    private final List<Channel> channelUpdates;
    private final List<Channel> channels;
    private final Integer defaultMessageNotifications;
    private final String description;
    private final List<EmbeddedActivityInGuild> embeddedActivities;
    private final List<GuildEmoji> emojis;
    private final GuildExplicitContentFilter explicitContentFilter;
    private final List<GuildFeature> features;
    private final GuildHashes guildHashes;
    private final List<GuildScheduledEvent> guildScheduledEvents;
    private final GuildHubType hubType;
    private final String icon;

    /* renamed from: id  reason: collision with root package name */
    private final long f2030id;
    private final String joinedAt;
    private final GuildMaxVideoChannelUsers maxVideoChannelUsers;
    private final int memberCount;
    private final List<GuildMember> members;
    private final int mfaLevel;
    private final String name;
    private final boolean nsfw;
    private final long ownerId;
    private final Long permissions;
    private final String preferredLocale;
    private final int premiumSubscriptionCount;
    private final int premiumTier;
    private final List<Presence> presences;
    private final Long publicUpdatesChannelId;
    private final String region;
    private final List<GuildRole> roles;
    private final Long rulesChannelId;
    private final String splash;
    private final List<StageInstance> stageInstances;
    private final List<Sticker> stickers;
    private final int systemChannelFlags;
    private final Long systemChannelId;
    private final List<Channel> threads;
    private final boolean unavailable;
    private final String vanityUrlCode;
    private final GuildVerificationLevel verificationLevel;
    private final List<VoiceState> voiceStates;
    private final GuildWelcomeScreen welcomeScreen;

    /* JADX WARN: Multi-variable type inference failed */
    public Guild(List<GuildRole> list, List<GuildEmoji> list2, List<Sticker> list3, String str, String str2, Integer num, long j, String str3, long j2, String str4, GuildVerificationLevel guildVerificationLevel, GuildExplicitContentFilter guildExplicitContentFilter, List<Presence> list4, List<Channel> list5, List<GuildMember> list6, List<VoiceState> list7, boolean z2, int i, int i2, Long l, Long l2, List<? extends GuildFeature> list8, int i3, String str5, String str6, int i4, int i5, int i6, String str7, Long l3, Long l4, String str8, GuildWelcomeScreen guildWelcomeScreen, GuildMaxVideoChannelUsers guildMaxVideoChannelUsers, String str9, int i7, int i8, Long l5, GuildHashes guildHashes, List<Channel> list9, List<Channel> list10, boolean z3, List<StageInstance> list11, List<GuildScheduledEvent> list12, List<EmbeddedActivityInGuild> list13, GuildHubType guildHubType) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(list8, "features");
        this.roles = list;
        this.emojis = list2;
        this.stickers = list3;
        this.name = str;
        this.description = str2;
        this.defaultMessageNotifications = num;
        this.f2030id = j;
        this.region = str3;
        this.ownerId = j2;
        this.icon = str4;
        this.verificationLevel = guildVerificationLevel;
        this.explicitContentFilter = guildExplicitContentFilter;
        this.presences = list4;
        this.channels = list5;
        this.members = list6;
        this.voiceStates = list7;
        this.unavailable = z2;
        this.mfaLevel = i;
        this.afkTimeout = i2;
        this.afkChannelId = l;
        this.systemChannelId = l2;
        this.features = list8;
        this.memberCount = i3;
        this.banner = str5;
        this.splash = str6;
        this.premiumTier = i4;
        this.premiumSubscriptionCount = i5;
        this.systemChannelFlags = i6;
        this.joinedAt = str7;
        this.rulesChannelId = l3;
        this.publicUpdatesChannelId = l4;
        this.preferredLocale = str8;
        this.welcomeScreen = guildWelcomeScreen;
        this.maxVideoChannelUsers = guildMaxVideoChannelUsers;
        this.vanityUrlCode = str9;
        this.approximateMemberCount = i7;
        this.approximatePresenceCount = i8;
        this.permissions = l5;
        this.guildHashes = guildHashes;
        this.channelUpdates = list9;
        this.threads = list10;
        this.nsfw = z3;
        this.stageInstances = list11;
        this.guildScheduledEvents = list12;
        this.embeddedActivities = list13;
        this.hubType = guildHubType;
    }

    public static Guild a(Guild guild, List list, List list2, List list3, String str, String str2, Integer num, long j, String str3, long j2, String str4, GuildVerificationLevel guildVerificationLevel, GuildExplicitContentFilter guildExplicitContentFilter, List list4, List list5, List list6, List list7, boolean z2, int i, int i2, Long l, Long l2, List list8, int i3, String str5, String str6, int i4, int i5, int i6, String str7, Long l3, Long l4, String str8, GuildWelcomeScreen guildWelcomeScreen, GuildMaxVideoChannelUsers guildMaxVideoChannelUsers, String str9, int i7, int i8, Long l5, GuildHashes guildHashes, List list9, List list10, boolean z3, List list11, List list12, List list13, GuildHubType guildHubType, int i9, int i10) {
        List list14 = (i9 & 1) != 0 ? guild.roles : list;
        List list15 = (i9 & 2) != 0 ? guild.emojis : list2;
        List list16 = (i9 & 4) != 0 ? guild.stickers : list3;
        String str10 = (i9 & 8) != 0 ? guild.name : str;
        String str11 = (i9 & 16) != 0 ? guild.description : str2;
        Integer num2 = (i9 & 32) != 0 ? guild.defaultMessageNotifications : num;
        long j3 = (i9 & 64) != 0 ? guild.f2030id : j;
        String str12 = (i9 & 128) != 0 ? guild.region : str3;
        long j4 = (i9 & 256) != 0 ? guild.ownerId : j2;
        String str13 = (i9 & 512) != 0 ? guild.icon : str4;
        GuildVerificationLevel guildVerificationLevel2 = (i9 & 1024) != 0 ? guild.verificationLevel : guildVerificationLevel;
        GuildExplicitContentFilter guildExplicitContentFilter2 = (i9 & 2048) != 0 ? guild.explicitContentFilter : guildExplicitContentFilter;
        List list17 = (i9 & 4096) != 0 ? guild.presences : list4;
        List list18 = (i9 & 8192) != 0 ? guild.channels : list5;
        GuildHubType guildHubType2 = null;
        List list19 = (i9 & 16384) != 0 ? guild.members : list6;
        List<VoiceState> list20 = (i9 & 32768) != 0 ? guild.voiceStates : null;
        boolean z4 = (i9 & 65536) != 0 ? guild.unavailable : z2;
        int i11 = (i9 & 131072) != 0 ? guild.mfaLevel : i;
        int i12 = (i9 & 262144) != 0 ? guild.afkTimeout : i2;
        Long l6 = (i9 & 524288) != 0 ? guild.afkChannelId : l;
        Long l7 = (i9 & 1048576) != 0 ? guild.systemChannelId : l2;
        List list21 = (i9 & 2097152) != 0 ? guild.features : list8;
        String str14 = str13;
        int i13 = (i9 & 4194304) != 0 ? guild.memberCount : i3;
        String str15 = (i9 & 8388608) != 0 ? guild.banner : str5;
        String str16 = (i9 & 16777216) != 0 ? guild.splash : str6;
        int i14 = (i9 & 33554432) != 0 ? guild.premiumTier : i4;
        int i15 = (i9 & 67108864) != 0 ? guild.premiumSubscriptionCount : i5;
        int i16 = (i9 & 134217728) != 0 ? guild.systemChannelFlags : i6;
        String str17 = (i9 & 268435456) != 0 ? guild.joinedAt : null;
        Long l8 = (i9 & 536870912) != 0 ? guild.rulesChannelId : l3;
        Long l9 = (i9 & BasicMeasure.EXACTLY) != 0 ? guild.publicUpdatesChannelId : l4;
        String str18 = (i9 & Integer.MIN_VALUE) != 0 ? guild.preferredLocale : str8;
        GuildWelcomeScreen guildWelcomeScreen2 = (i10 & 1) != 0 ? guild.welcomeScreen : null;
        GuildMaxVideoChannelUsers guildMaxVideoChannelUsers2 = (i10 & 2) != 0 ? guild.maxVideoChannelUsers : guildMaxVideoChannelUsers;
        String str19 = (i10 & 4) != 0 ? guild.vanityUrlCode : str9;
        int i17 = (i10 & 8) != 0 ? guild.approximateMemberCount : i7;
        int i18 = (i10 & 16) != 0 ? guild.approximatePresenceCount : i8;
        Long l10 = (i10 & 32) != 0 ? guild.permissions : null;
        GuildHashes guildHashes2 = (i10 & 64) != 0 ? guild.guildHashes : null;
        List<Channel> list22 = (i10 & 128) != 0 ? guild.channelUpdates : null;
        List<Channel> list23 = (i10 & 256) != 0 ? guild.threads : null;
        boolean z5 = (i10 & 512) != 0 ? guild.nsfw : z3;
        List<StageInstance> list24 = (i10 & 1024) != 0 ? guild.stageInstances : null;
        List<GuildScheduledEvent> list25 = (i10 & 2048) != 0 ? guild.guildScheduledEvents : null;
        List<EmbeddedActivityInGuild> list26 = (i10 & 4096) != 0 ? guild.embeddedActivities : null;
        if ((i10 & 8192) != 0) {
            guildHubType2 = guild.hubType;
        }
        Objects.requireNonNull(guild);
        m.checkNotNullParameter(str10, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(list21, "features");
        return new Guild(list14, list15, list16, str10, str11, num2, j3, str12, j4, str14, guildVerificationLevel2, guildExplicitContentFilter2, list17, list18, list19, list20, z4, i11, i12, l6, l7, list21, i13, str15, str16, i14, i15, i16, str17, l8, l9, str18, guildWelcomeScreen2, guildMaxVideoChannelUsers2, str19, i17, i18, l10, guildHashes2, list22, list23, z5, list24, list25, list26, guildHubType2);
    }

    public final String A() {
        return this.preferredLocale;
    }

    public final int B() {
        return this.premiumSubscriptionCount;
    }

    public final int C() {
        return this.premiumTier;
    }

    public final List<Presence> D() {
        return this.presences;
    }

    public final Long E() {
        return this.publicUpdatesChannelId;
    }

    public final String F() {
        return this.region;
    }

    public final List<GuildRole> G() {
        return this.roles;
    }

    public final Long H() {
        return this.rulesChannelId;
    }

    public final String I() {
        return this.splash;
    }

    public final List<StageInstance> J() {
        return this.stageInstances;
    }

    public final List<Sticker> K() {
        return this.stickers;
    }

    public final int L() {
        return this.systemChannelFlags;
    }

    public final Long M() {
        return this.systemChannelId;
    }

    public final List<Channel> N() {
        return this.threads;
    }

    public final boolean O() {
        return this.unavailable;
    }

    public final String P() {
        return this.vanityUrlCode;
    }

    public final GuildVerificationLevel Q() {
        return this.verificationLevel;
    }

    public final List<VoiceState> R() {
        return this.voiceStates;
    }

    public final GuildWelcomeScreen S() {
        return this.welcomeScreen;
    }

    public final Long b() {
        return this.afkChannelId;
    }

    public final int c() {
        return this.afkTimeout;
    }

    public final int d() {
        return this.approximatePresenceCount;
    }

    public final String e() {
        return this.banner;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Guild)) {
            return false;
        }
        Guild guild = (Guild) obj;
        return m.areEqual(this.roles, guild.roles) && m.areEqual(this.emojis, guild.emojis) && m.areEqual(this.stickers, guild.stickers) && m.areEqual(this.name, guild.name) && m.areEqual(this.description, guild.description) && m.areEqual(this.defaultMessageNotifications, guild.defaultMessageNotifications) && this.f2030id == guild.f2030id && m.areEqual(this.region, guild.region) && this.ownerId == guild.ownerId && m.areEqual(this.icon, guild.icon) && m.areEqual(this.verificationLevel, guild.verificationLevel) && m.areEqual(this.explicitContentFilter, guild.explicitContentFilter) && m.areEqual(this.presences, guild.presences) && m.areEqual(this.channels, guild.channels) && m.areEqual(this.members, guild.members) && m.areEqual(this.voiceStates, guild.voiceStates) && this.unavailable == guild.unavailable && this.mfaLevel == guild.mfaLevel && this.afkTimeout == guild.afkTimeout && m.areEqual(this.afkChannelId, guild.afkChannelId) && m.areEqual(this.systemChannelId, guild.systemChannelId) && m.areEqual(this.features, guild.features) && this.memberCount == guild.memberCount && m.areEqual(this.banner, guild.banner) && m.areEqual(this.splash, guild.splash) && this.premiumTier == guild.premiumTier && this.premiumSubscriptionCount == guild.premiumSubscriptionCount && this.systemChannelFlags == guild.systemChannelFlags && m.areEqual(this.joinedAt, guild.joinedAt) && m.areEqual(this.rulesChannelId, guild.rulesChannelId) && m.areEqual(this.publicUpdatesChannelId, guild.publicUpdatesChannelId) && m.areEqual(this.preferredLocale, guild.preferredLocale) && m.areEqual(this.welcomeScreen, guild.welcomeScreen) && m.areEqual(this.maxVideoChannelUsers, guild.maxVideoChannelUsers) && m.areEqual(this.vanityUrlCode, guild.vanityUrlCode) && this.approximateMemberCount == guild.approximateMemberCount && this.approximatePresenceCount == guild.approximatePresenceCount && m.areEqual(this.permissions, guild.permissions) && m.areEqual(this.guildHashes, guild.guildHashes) && m.areEqual(this.channelUpdates, guild.channelUpdates) && m.areEqual(this.threads, guild.threads) && this.nsfw == guild.nsfw && m.areEqual(this.stageInstances, guild.stageInstances) && m.areEqual(this.guildScheduledEvents, guild.guildScheduledEvents) && m.areEqual(this.embeddedActivities, guild.embeddedActivities) && m.areEqual(this.hubType, guild.hubType);
    }

    public final List<Channel> f() {
        return this.channelUpdates;
    }

    public final List<Channel> g() {
        return this.channels;
    }

    public final Integer h() {
        return this.defaultMessageNotifications;
    }

    public int hashCode() {
        List<GuildRole> list = this.roles;
        int i = 0;
        int hashCode = (list != null ? list.hashCode() : 0) * 31;
        List<GuildEmoji> list2 = this.emojis;
        int hashCode2 = (hashCode + (list2 != null ? list2.hashCode() : 0)) * 31;
        List<Sticker> list3 = this.stickers;
        int hashCode3 = (hashCode2 + (list3 != null ? list3.hashCode() : 0)) * 31;
        String str = this.name;
        int hashCode4 = (hashCode3 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.description;
        int hashCode5 = (hashCode4 + (str2 != null ? str2.hashCode() : 0)) * 31;
        Integer num = this.defaultMessageNotifications;
        int hashCode6 = num != null ? num.hashCode() : 0;
        long j = this.f2030id;
        int i2 = (((hashCode5 + hashCode6) * 31) + ((int) (j ^ (j >>> 32)))) * 31;
        String str3 = this.region;
        int hashCode7 = str3 != null ? str3.hashCode() : 0;
        long j2 = this.ownerId;
        int i3 = (((i2 + hashCode7) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31;
        String str4 = this.icon;
        int hashCode8 = (i3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        GuildVerificationLevel guildVerificationLevel = this.verificationLevel;
        int hashCode9 = (hashCode8 + (guildVerificationLevel != null ? guildVerificationLevel.hashCode() : 0)) * 31;
        GuildExplicitContentFilter guildExplicitContentFilter = this.explicitContentFilter;
        int hashCode10 = (hashCode9 + (guildExplicitContentFilter != null ? guildExplicitContentFilter.hashCode() : 0)) * 31;
        List<Presence> list4 = this.presences;
        int hashCode11 = (hashCode10 + (list4 != null ? list4.hashCode() : 0)) * 31;
        List<Channel> list5 = this.channels;
        int hashCode12 = (hashCode11 + (list5 != null ? list5.hashCode() : 0)) * 31;
        List<GuildMember> list6 = this.members;
        int hashCode13 = (hashCode12 + (list6 != null ? list6.hashCode() : 0)) * 31;
        List<VoiceState> list7 = this.voiceStates;
        int hashCode14 = (hashCode13 + (list7 != null ? list7.hashCode() : 0)) * 31;
        boolean z2 = this.unavailable;
        int i4 = 1;
        if (z2) {
            z2 = true;
        }
        int i5 = z2 ? 1 : 0;
        int i6 = z2 ? 1 : 0;
        int i7 = (((((hashCode14 + i5) * 31) + this.mfaLevel) * 31) + this.afkTimeout) * 31;
        Long l = this.afkChannelId;
        int hashCode15 = (i7 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.systemChannelId;
        int hashCode16 = (hashCode15 + (l2 != null ? l2.hashCode() : 0)) * 31;
        List<GuildFeature> list8 = this.features;
        int hashCode17 = (((hashCode16 + (list8 != null ? list8.hashCode() : 0)) * 31) + this.memberCount) * 31;
        String str5 = this.banner;
        int hashCode18 = (hashCode17 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.splash;
        int hashCode19 = (((((((hashCode18 + (str6 != null ? str6.hashCode() : 0)) * 31) + this.premiumTier) * 31) + this.premiumSubscriptionCount) * 31) + this.systemChannelFlags) * 31;
        String str7 = this.joinedAt;
        int hashCode20 = (hashCode19 + (str7 != null ? str7.hashCode() : 0)) * 31;
        Long l3 = this.rulesChannelId;
        int hashCode21 = (hashCode20 + (l3 != null ? l3.hashCode() : 0)) * 31;
        Long l4 = this.publicUpdatesChannelId;
        int hashCode22 = (hashCode21 + (l4 != null ? l4.hashCode() : 0)) * 31;
        String str8 = this.preferredLocale;
        int hashCode23 = (hashCode22 + (str8 != null ? str8.hashCode() : 0)) * 31;
        GuildWelcomeScreen guildWelcomeScreen = this.welcomeScreen;
        int hashCode24 = (hashCode23 + (guildWelcomeScreen != null ? guildWelcomeScreen.hashCode() : 0)) * 31;
        GuildMaxVideoChannelUsers guildMaxVideoChannelUsers = this.maxVideoChannelUsers;
        int hashCode25 = (hashCode24 + (guildMaxVideoChannelUsers != null ? guildMaxVideoChannelUsers.hashCode() : 0)) * 31;
        String str9 = this.vanityUrlCode;
        int hashCode26 = (((((hashCode25 + (str9 != null ? str9.hashCode() : 0)) * 31) + this.approximateMemberCount) * 31) + this.approximatePresenceCount) * 31;
        Long l5 = this.permissions;
        int hashCode27 = (hashCode26 + (l5 != null ? l5.hashCode() : 0)) * 31;
        GuildHashes guildHashes = this.guildHashes;
        int hashCode28 = (hashCode27 + (guildHashes != null ? guildHashes.hashCode() : 0)) * 31;
        List<Channel> list9 = this.channelUpdates;
        int hashCode29 = (hashCode28 + (list9 != null ? list9.hashCode() : 0)) * 31;
        List<Channel> list10 = this.threads;
        int hashCode30 = (hashCode29 + (list10 != null ? list10.hashCode() : 0)) * 31;
        boolean z3 = this.nsfw;
        if (!z3) {
            i4 = z3 ? 1 : 0;
        }
        int i8 = (hashCode30 + i4) * 31;
        List<StageInstance> list11 = this.stageInstances;
        int hashCode31 = (i8 + (list11 != null ? list11.hashCode() : 0)) * 31;
        List<GuildScheduledEvent> list12 = this.guildScheduledEvents;
        int hashCode32 = (hashCode31 + (list12 != null ? list12.hashCode() : 0)) * 31;
        List<EmbeddedActivityInGuild> list13 = this.embeddedActivities;
        int hashCode33 = (hashCode32 + (list13 != null ? list13.hashCode() : 0)) * 31;
        GuildHubType guildHubType = this.hubType;
        if (guildHubType != null) {
            i = guildHubType.hashCode();
        }
        return hashCode33 + i;
    }

    public final String i() {
        return this.description;
    }

    public final List<EmbeddedActivityInGuild> j() {
        return this.embeddedActivities;
    }

    public final List<GuildEmoji> k() {
        return this.emojis;
    }

    public final GuildExplicitContentFilter l() {
        return this.explicitContentFilter;
    }

    public final List<GuildFeature> m() {
        return this.features;
    }

    public final GuildHashes n() {
        return this.guildHashes;
    }

    public final List<GuildScheduledEvent> o() {
        return this.guildScheduledEvents;
    }

    public final GuildHubType p() {
        return this.hubType;
    }

    public final String q() {
        return this.icon;
    }

    public final long r() {
        return this.f2030id;
    }

    public final String s() {
        return this.joinedAt;
    }

    public final GuildMaxVideoChannelUsers t() {
        return this.maxVideoChannelUsers;
    }

    public String toString() {
        StringBuilder R = a.R("Guild(roles=");
        R.append(this.roles);
        R.append(", emojis=");
        R.append(this.emojis);
        R.append(", stickers=");
        R.append(this.stickers);
        R.append(", name=");
        R.append(this.name);
        R.append(", description=");
        R.append(this.description);
        R.append(", defaultMessageNotifications=");
        R.append(this.defaultMessageNotifications);
        R.append(", id=");
        R.append(this.f2030id);
        R.append(", region=");
        R.append(this.region);
        R.append(", ownerId=");
        R.append(this.ownerId);
        R.append(", icon=");
        R.append(this.icon);
        R.append(", verificationLevel=");
        R.append(this.verificationLevel);
        R.append(", explicitContentFilter=");
        R.append(this.explicitContentFilter);
        R.append(", presences=");
        R.append(this.presences);
        R.append(", channels=");
        R.append(this.channels);
        R.append(", members=");
        R.append(this.members);
        R.append(", voiceStates=");
        R.append(this.voiceStates);
        R.append(", unavailable=");
        R.append(this.unavailable);
        R.append(", mfaLevel=");
        R.append(this.mfaLevel);
        R.append(", afkTimeout=");
        R.append(this.afkTimeout);
        R.append(", afkChannelId=");
        R.append(this.afkChannelId);
        R.append(", systemChannelId=");
        R.append(this.systemChannelId);
        R.append(", features=");
        R.append(this.features);
        R.append(", memberCount=");
        R.append(this.memberCount);
        R.append(", banner=");
        R.append(this.banner);
        R.append(", splash=");
        R.append(this.splash);
        R.append(", premiumTier=");
        R.append(this.premiumTier);
        R.append(", premiumSubscriptionCount=");
        R.append(this.premiumSubscriptionCount);
        R.append(", systemChannelFlags=");
        R.append(this.systemChannelFlags);
        R.append(", joinedAt=");
        R.append(this.joinedAt);
        R.append(", rulesChannelId=");
        R.append(this.rulesChannelId);
        R.append(", publicUpdatesChannelId=");
        R.append(this.publicUpdatesChannelId);
        R.append(", preferredLocale=");
        R.append(this.preferredLocale);
        R.append(", welcomeScreen=");
        R.append(this.welcomeScreen);
        R.append(", maxVideoChannelUsers=");
        R.append(this.maxVideoChannelUsers);
        R.append(", vanityUrlCode=");
        R.append(this.vanityUrlCode);
        R.append(", approximateMemberCount=");
        R.append(this.approximateMemberCount);
        R.append(", approximatePresenceCount=");
        R.append(this.approximatePresenceCount);
        R.append(", permissions=");
        R.append(this.permissions);
        R.append(", guildHashes=");
        R.append(this.guildHashes);
        R.append(", channelUpdates=");
        R.append(this.channelUpdates);
        R.append(", threads=");
        R.append(this.threads);
        R.append(", nsfw=");
        R.append(this.nsfw);
        R.append(", stageInstances=");
        R.append(this.stageInstances);
        R.append(", guildScheduledEvents=");
        R.append(this.guildScheduledEvents);
        R.append(", embeddedActivities=");
        R.append(this.embeddedActivities);
        R.append(", hubType=");
        R.append(this.hubType);
        R.append(")");
        return R.toString();
    }

    public final int u() {
        return this.memberCount;
    }

    public final List<GuildMember> v() {
        return this.members;
    }

    public final int w() {
        return this.mfaLevel;
    }

    public final String x() {
        return this.name;
    }

    public final boolean y() {
        return this.nsfw;
    }

    public final long z() {
        return this.ownerId;
    }
}
