package com.discord.api.guild.welcome;

import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: GuildWelcomeChannel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\r\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0011\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R\u0019\u0010\u0015\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0016\u001a\u0004\b\u0019\u0010\u0004¨\u0006\u001a"}, d2 = {"Lcom/discord/api/guild/welcome/GuildWelcomeChannel;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "channelId", "J", "a", "()J", "emojiId", "Ljava/lang/Long;", "c", "()Ljava/lang/Long;", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "Ljava/lang/String;", "b", "emojiName", "d", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildWelcomeChannel {
    private final long channelId;
    private final String description;
    private final Long emojiId;
    private final String emojiName;

    public final long a() {
        return this.channelId;
    }

    public final String b() {
        return this.description;
    }

    public final Long c() {
        return this.emojiId;
    }

    public final String d() {
        return this.emojiName;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildWelcomeChannel)) {
            return false;
        }
        GuildWelcomeChannel guildWelcomeChannel = (GuildWelcomeChannel) obj;
        return this.channelId == guildWelcomeChannel.channelId && m.areEqual(this.description, guildWelcomeChannel.description) && m.areEqual(this.emojiId, guildWelcomeChannel.emojiId) && m.areEqual(this.emojiName, guildWelcomeChannel.emojiName);
    }

    public int hashCode() {
        long j = this.channelId;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        String str = this.description;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        Long l = this.emojiId;
        int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
        String str2 = this.emojiName;
        if (str2 != null) {
            i2 = str2.hashCode();
        }
        return hashCode2 + i2;
    }

    public String toString() {
        StringBuilder R = a.R("GuildWelcomeChannel(channelId=");
        R.append(this.channelId);
        R.append(", description=");
        R.append(this.description);
        R.append(", emojiId=");
        R.append(this.emojiId);
        R.append(", emojiName=");
        return a.H(R, this.emojiName, ")");
    }
}
