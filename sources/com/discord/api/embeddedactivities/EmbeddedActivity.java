package com.discord.api.embeddedactivities;

import b.d.b.a.a;
import com.discord.api.activity.ActivityAssets;
import com.discord.api.activity.ActivitySecrets;
import com.discord.api.activity.ActivityTimestamps;
import com.discord.api.activity.ActivityType;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: EmbeddedActivity.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u0004R\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\r\u001a\u0004\b\u0010\u0010\u0004R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\r\u001a\u0004\b\u0017\u0010\u0004R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u00188\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001cR\u001d\u0010\u001f\u001a\u00060\u001dj\u0002`\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"R\u001b\u0010$\u001a\u0004\u0018\u00010#8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'R\u001b\u0010)\u001a\u0004\u0018\u00010(8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010*\u001a\u0004\b+\u0010,R\u001b\u0010-\u001a\u0004\u0018\u00010\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010.\u001a\u0004\b/\u00100¨\u00061"}, d2 = {"Lcom/discord/api/embeddedactivities/EmbeddedActivity;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "details", "Ljava/lang/String;", "d", ModelAuditLogEntry.CHANGE_KEY_NAME, "e", "Lcom/discord/api/activity/ActivityType;", "type", "Lcom/discord/api/activity/ActivityType;", "i", "()Lcom/discord/api/activity/ActivityType;", "state", "g", "Lcom/discord/api/activity/ActivitySecrets;", "secrets", "Lcom/discord/api/activity/ActivitySecrets;", "f", "()Lcom/discord/api/activity/ActivitySecrets;", "", "Lcom/discord/primitives/ApplicationId;", "applicationId", "J", "a", "()J", "Lcom/discord/api/activity/ActivityAssets;", "assets", "Lcom/discord/api/activity/ActivityAssets;", "b", "()Lcom/discord/api/activity/ActivityAssets;", "Lcom/discord/api/activity/ActivityTimestamps;", "timestamps", "Lcom/discord/api/activity/ActivityTimestamps;", "h", "()Lcom/discord/api/activity/ActivityTimestamps;", "createdAt", "Ljava/lang/Long;", "c", "()Ljava/lang/Long;", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class EmbeddedActivity {
    private final long applicationId;
    private final ActivityAssets assets;
    private final Long createdAt;
    private final String details;
    private final String name;
    private final ActivitySecrets secrets;
    private final String state;
    private final ActivityTimestamps timestamps;
    private final ActivityType type;

    public final long a() {
        return this.applicationId;
    }

    public final ActivityAssets b() {
        return this.assets;
    }

    public final Long c() {
        return this.createdAt;
    }

    public final String d() {
        return this.details;
    }

    public final String e() {
        return this.name;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof EmbeddedActivity)) {
            return false;
        }
        EmbeddedActivity embeddedActivity = (EmbeddedActivity) obj;
        return this.applicationId == embeddedActivity.applicationId && m.areEqual(this.assets, embeddedActivity.assets) && m.areEqual(this.createdAt, embeddedActivity.createdAt) && m.areEqual(this.details, embeddedActivity.details) && m.areEqual(this.name, embeddedActivity.name) && m.areEqual(this.secrets, embeddedActivity.secrets) && m.areEqual(this.state, embeddedActivity.state) && m.areEqual(this.timestamps, embeddedActivity.timestamps) && m.areEqual(this.type, embeddedActivity.type);
    }

    public final ActivitySecrets f() {
        return this.secrets;
    }

    public final String g() {
        return this.state;
    }

    public final ActivityTimestamps h() {
        return this.timestamps;
    }

    public int hashCode() {
        long j = this.applicationId;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        ActivityAssets activityAssets = this.assets;
        int i2 = 0;
        int hashCode = (i + (activityAssets != null ? activityAssets.hashCode() : 0)) * 31;
        Long l = this.createdAt;
        int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
        String str = this.details;
        int hashCode3 = (hashCode2 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.name;
        int hashCode4 = (hashCode3 + (str2 != null ? str2.hashCode() : 0)) * 31;
        ActivitySecrets activitySecrets = this.secrets;
        int hashCode5 = (hashCode4 + (activitySecrets != null ? activitySecrets.hashCode() : 0)) * 31;
        String str3 = this.state;
        int hashCode6 = (hashCode5 + (str3 != null ? str3.hashCode() : 0)) * 31;
        ActivityTimestamps activityTimestamps = this.timestamps;
        int hashCode7 = (hashCode6 + (activityTimestamps != null ? activityTimestamps.hashCode() : 0)) * 31;
        ActivityType activityType = this.type;
        if (activityType != null) {
            i2 = activityType.hashCode();
        }
        return hashCode7 + i2;
    }

    public final ActivityType i() {
        return this.type;
    }

    public String toString() {
        StringBuilder R = a.R("EmbeddedActivity(applicationId=");
        R.append(this.applicationId);
        R.append(", assets=");
        R.append(this.assets);
        R.append(", createdAt=");
        R.append(this.createdAt);
        R.append(", details=");
        R.append(this.details);
        R.append(", name=");
        R.append(this.name);
        R.append(", secrets=");
        R.append(this.secrets);
        R.append(", state=");
        R.append(this.state);
        R.append(", timestamps=");
        R.append(this.timestamps);
        R.append(", type=");
        R.append(this.type);
        R.append(")");
        return R.toString();
    }
}
