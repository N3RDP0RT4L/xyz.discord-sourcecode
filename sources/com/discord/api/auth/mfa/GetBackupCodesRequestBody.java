package com.discord.api.auth.mfa;

import andhook.lib.HookHelper;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: GetBackupCodesRequestBody.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/api/auth/mfa/GetBackupCodesRequestBody;", "", "", "password", "Ljava/lang/String;", "", "regenerate", "Z", HookHelper.constructorName, "(Ljava/lang/String;Z)V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GetBackupCodesRequestBody {
    private final String password;
    private final boolean regenerate;

    public GetBackupCodesRequestBody(String str, boolean z2) {
        m.checkNotNullParameter(str, "password");
        this.password = str;
        this.regenerate = z2;
    }
}
