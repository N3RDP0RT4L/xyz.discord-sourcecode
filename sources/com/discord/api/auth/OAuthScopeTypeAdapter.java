package com.discord.api.auth;

import andhook.lib.HookHelper;
import com.discord.api.auth.OAuthScope;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import d0.e0.c;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.HashMap;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: OAuthScopeTypeAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 \u00052\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0005B\u0007¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0006"}, d2 = {"Lcom/discord/api/auth/OAuthScopeTypeAdapter;", "Lcom/google/gson/TypeAdapter;", "Lcom/discord/api/auth/OAuthScope;", HookHelper.constructorName, "()V", "Companion", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class OAuthScopeTypeAdapter extends TypeAdapter<OAuthScope> {
    public static final Companion Companion = new Companion(null);
    private static final HashMap<String, OAuthScope> nameToScopeMap;

    /* compiled from: OAuthScopeTypeAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/api/auth/OAuthScopeTypeAdapter$Companion;", "", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        public Companion() {
        }

        public Companion(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    static {
        HashMap<String, OAuthScope> hashMap = new HashMap<>();
        for (c cVar : a0.getOrCreateKotlinClass(OAuthScope.class).getSealedSubclasses()) {
            if (cVar.getObjectInstance() != null) {
                Object objectInstance = cVar.getObjectInstance();
                Objects.requireNonNull(objectInstance, "null cannot be cast to non-null type com.discord.api.auth.OAuthScope");
                OAuthScope oAuthScope = (OAuthScope) objectInstance;
                hashMap.put(oAuthScope.a(), oAuthScope);
            }
        }
        nameToScopeMap = hashMap;
    }

    @Override // com.google.gson.TypeAdapter
    public OAuthScope read(JsonReader jsonReader) {
        m.checkNotNullParameter(jsonReader, "in");
        if (jsonReader.N() == JsonToken.NULL) {
            jsonReader.H();
            return null;
        }
        String J = jsonReader.J();
        OAuthScope oAuthScope = nameToScopeMap.get(J);
        if (oAuthScope == null) {
            m.checkNotNullExpressionValue(J, "scopeName");
            oAuthScope = new OAuthScope.Invalid(J);
        }
        return oAuthScope;
    }

    @Override // com.google.gson.TypeAdapter
    public void write(JsonWriter jsonWriter, OAuthScope oAuthScope) {
        OAuthScope oAuthScope2 = oAuthScope;
        m.checkNotNullParameter(jsonWriter, "out");
        if (oAuthScope2 != null) {
            jsonWriter.H(oAuthScope2.a());
        } else {
            jsonWriter.s();
        }
    }
}
