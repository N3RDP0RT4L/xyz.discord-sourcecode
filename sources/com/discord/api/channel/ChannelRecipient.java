package com.discord.api.channel;

import b.d.b.a.a;
import com.discord.api.user.User;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ChannelRecipient.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u0004R\u0019\u0010\u0010\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R\u0019\u0010\u0015\u001a\u00020\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018¨\u0006\u0019"}, d2 = {"Lcom/discord/api/channel/ChannelRecipient;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", ModelAuditLogEntry.CHANGE_KEY_NICK, "Ljava/lang/String;", "b", "", "channelId", "J", "a", "()J", "Lcom/discord/api/user/User;", "user", "Lcom/discord/api/user/User;", "c", "()Lcom/discord/api/user/User;", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ChannelRecipient {
    private final long channelId;
    private final String nick;
    private final User user;

    public final long a() {
        return this.channelId;
    }

    public final String b() {
        return this.nick;
    }

    public final User c() {
        return this.user;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ChannelRecipient)) {
            return false;
        }
        ChannelRecipient channelRecipient = (ChannelRecipient) obj;
        return m.areEqual(this.nick, channelRecipient.nick) && this.channelId == channelRecipient.channelId && m.areEqual(this.user, channelRecipient.user);
    }

    public int hashCode() {
        String str = this.nick;
        int i = 0;
        int hashCode = str != null ? str.hashCode() : 0;
        long j = this.channelId;
        int i2 = ((hashCode * 31) + ((int) (j ^ (j >>> 32)))) * 31;
        User user = this.user;
        if (user != null) {
            i = user.hashCode();
        }
        return i2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("ChannelRecipient(nick=");
        R.append(this.nick);
        R.append(", channelId=");
        R.append(this.channelId);
        R.append(", user=");
        R.append(this.user);
        R.append(")");
        return R.toString();
    }
}
