package com.discord.api.channel;

import com.discord.api.channel.Channel;
import java.util.Comparator;
import kotlin.Metadata;
/* compiled from: ChannelUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ChannelUtils$getSortByNameAndType$1<T> implements Comparator<Channel> {
    public final /* synthetic */ Channel.Companion $this_getSortByNameAndType;

    public ChannelUtils$getSortByNameAndType$1(Channel.Companion companion) {
        this.$this_getSortByNameAndType = companion;
    }

    @Override // java.util.Comparator
    public int compare(Channel channel, Channel channel2) {
        long j;
        long j2;
        int i;
        int i2;
        Channel channel3 = channel;
        Channel channel4 = channel2;
        if (channel3 == null) {
            return channel4 == null ? 0 : -1;
        }
        if (channel4 != null) {
            if (channel3.A() == 4 && channel4.A() == 4) {
                if (channel3.t() != channel4.t()) {
                    i2 = channel3.t();
                    i = channel4.t();
                    return i2 - i;
                }
                j2 = channel3.h();
                j = channel4.h();
                return (j2 > j ? 1 : (j2 == j ? 0 : -1));
            } else if (channel3.A() == 4) {
                return -1;
            } else {
                if (channel4.A() != 4) {
                    if (channel3.A() == 0 && ChannelUtils.t(channel4)) {
                        return -1;
                    }
                    if (!ChannelUtils.t(channel3) || channel4.A() != 0) {
                        if (channel3.t() != channel4.t()) {
                            i2 = channel3.t();
                            i = channel4.t();
                            return i2 - i;
                        }
                        j2 = channel3.h();
                        j = channel4.h();
                        return (j2 > j ? 1 : (j2 == j ? 0 : -1));
                    }
                }
            }
        }
        return 1;
    }
}
