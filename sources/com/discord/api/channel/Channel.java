package com.discord.api.channel;

import andhook.lib.HookHelper;
import androidx.exifinterface.media.ExifInterface;
import b.d.b.a.a;
import com.discord.api.guildhash.GuildHashes;
import com.discord.api.permission.PermissionOverwrite;
import com.discord.api.thread.ThreadMember;
import com.discord.api.thread.ThreadMetadata;
import com.discord.api.user.User;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: Channel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0007\n\u0002\u0010 \n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0018\b\u0086\b\u0018\u0000 d2\u00020\u0001:\u0001dB\u00ad\u0002\u0012\b\u0010`\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010,\u001a\u00020\u0005\u0012\u0006\u0010\u0019\u001a\u00020\f\u0012\b\u0010>\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010 \u001a\u00020\f\u0012\u0006\u0010B\u001a\u00020\f\u0012\u0006\u0010D\u001a\u00020\f\u0012\u000e\u00105\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u0014\u0012\u000e\u0010N\u001a\n\u0012\u0004\u0012\u00020M\u0018\u00010\u0014\u0012\u0006\u0010P\u001a\u00020\u0005\u0012\u000e\u00108\u001a\n\u0012\u0004\u0012\u000207\u0018\u00010\u0014\u0012\u0006\u0010F\u001a\u00020\u0005\u0012\u0006\u0010@\u001a\u00020\u0005\u0012\b\u0010R\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\r\u001a\u00020\f\u0012\u0006\u00103\u001a\u00020\f\u0012\u000e\u0010#\u001a\n\u0012\u0004\u0012\u00020\"\u0018\u00010\u0014\u0012\u0006\u0010T\u001a\u00020\t\u0012\u0006\u00101\u001a\u00020\f\u0012\b\u0010^\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010/\u001a\u00020\u0005\u0012\b\u0010X\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010&\u001a\u0004\u0018\u00010%\u0012\b\u0010\u001c\u001a\u0004\u0018\u00010\u001b\u0012\b\u0010Z\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010:\u001a\u0004\u0018\u00010\u0005\u0012\u000e\u0010\u0015\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u0014\u0012\b\u0010I\u001a\u0004\u0018\u00010H¢\u0006\u0004\bb\u0010cJ\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\r\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0004R!\u0010\u0015\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018R\u0019\u0010\u0019\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u000e\u001a\u0004\b\u001a\u0010\u0010R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001fR\u0019\u0010 \u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u000e\u001a\u0004\b!\u0010\u0010R!\u0010#\u001a\n\u0012\u0004\u0012\u00020\"\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u0016\u001a\u0004\b$\u0010\u0018R$\u0010&\u001a\u0004\u0018\u00010%8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b&\u0010'\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R\u0019\u0010,\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010-\u001a\u0004\b.\u0010\u0007R\u0019\u0010/\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010-\u001a\u0004\b0\u0010\u0007R\u0019\u00101\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010\u000e\u001a\u0004\b2\u0010\u0010R\u0019\u00103\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010\u000e\u001a\u0004\b4\u0010\u0010R!\u00105\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010\u0016\u001a\u0004\b6\u0010\u0018R!\u00108\u001a\n\u0012\u0004\u0012\u000207\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010\u0016\u001a\u0004\b9\u0010\u0018R\u001b\u0010:\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010;\u001a\u0004\b<\u0010=R\u001b\u0010>\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b>\u0010\u0012\u001a\u0004\b?\u0010\u0004R\u0019\u0010@\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b@\u0010-\u001a\u0004\bA\u0010\u0007R\u0019\u0010B\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010\u000e\u001a\u0004\bC\u0010\u0010R\u0019\u0010D\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\bD\u0010\u000e\u001a\u0004\bE\u0010\u0010R\u0019\u0010F\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\bF\u0010-\u001a\u0004\bG\u0010\u0007R\u001b\u0010I\u001a\u0004\u0018\u00010H8\u0006@\u0006¢\u0006\f\n\u0004\bI\u0010J\u001a\u0004\bK\u0010LR!\u0010N\u001a\n\u0012\u0004\u0012\u00020M\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\bN\u0010\u0016\u001a\u0004\bO\u0010\u0018R\u0019\u0010P\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\bP\u0010-\u001a\u0004\bQ\u0010\u0007R\u001b\u0010R\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\bR\u0010\u0012\u001a\u0004\bS\u0010\u0004R\u0019\u0010T\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\bT\u0010U\u001a\u0004\bV\u0010WR\u001b\u0010X\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\bX\u0010;\u001a\u0004\bY\u0010=R$\u0010Z\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\bZ\u0010;\u001a\u0004\b[\u0010=\"\u0004\b\\\u0010]R\u001b\u0010^\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b^\u0010\u0012\u001a\u0004\b_\u0010\u0004R\u001b\u0010`\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b`\u0010\u0012\u001a\u0004\ba\u0010\u0004¨\u0006e"}, d2 = {"Lcom/discord/api/channel/Channel;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "originChannelId", "J", "p", "()J", "rtcRegion", "Ljava/lang/String;", "x", "", "memberIdsPreview", "Ljava/util/List;", "getMemberIdsPreview", "()Ljava/util/List;", "guildId", "f", "Lcom/discord/api/thread/ThreadMetadata;", "threadMetadata", "Lcom/discord/api/thread/ThreadMetadata;", "y", "()Lcom/discord/api/thread/ThreadMetadata;", "lastMessageId", "i", "Lcom/discord/api/channel/ChannelRecipientNick;", "nicks", "n", "Lcom/discord/api/guildhash/GuildHashes;", "guildHashes", "Lcom/discord/api/guildhash/GuildHashes;", "e", "()Lcom/discord/api/guildhash/GuildHashes;", "setGuildHashes", "(Lcom/discord/api/guildhash/GuildHashes;)V", "type", "I", ExifInterface.GPS_MEASUREMENT_IN_PROGRESS, "rateLimitPerUser", "u", "parentId", "r", "applicationId", "b", "recipientIds", "v", "Lcom/discord/api/permission/PermissionOverwrite;", "permissionOverwrites", "s", "memberCount", "Ljava/lang/Integer;", "getMemberCount", "()Ljava/lang/Integer;", ModelAuditLogEntry.CHANGE_KEY_NAME, "m", ModelAuditLogEntry.CHANGE_KEY_BITRATE, "c", ModelAuditLogEntry.CHANGE_KEY_ID, "h", "ownerId", "q", "userLimit", "B", "Lcom/discord/api/thread/ThreadMember;", "member", "Lcom/discord/api/thread/ThreadMember;", "j", "()Lcom/discord/api/thread/ThreadMember;", "Lcom/discord/api/user/User;", "recipients", "w", ModelAuditLogEntry.CHANGE_KEY_POSITION, "t", "icon", "g", ModelAuditLogEntry.CHANGE_KEY_NSFW, "Z", "o", "()Z", "defaultAutoArchiveDuration", "d", "messageCount", "l", "setMessageCount", "(Ljava/lang/Integer;)V", "memberListId", "k", ModelAuditLogEntry.CHANGE_KEY_TOPIC, "z", HookHelper.constructorName, "(Ljava/lang/String;IJLjava/lang/String;JJJLjava/util/List;Ljava/util/List;ILjava/util/List;IILjava/lang/String;JJLjava/util/List;ZJLjava/lang/String;ILjava/lang/Integer;Ljava/lang/String;Lcom/discord/api/guildhash/GuildHashes;Lcom/discord/api/thread/ThreadMetadata;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/List;Lcom/discord/api/thread/ThreadMember;)V", "Companion", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class Channel {
    public static final int ANNOUNCEMENT_THREAD = 10;
    public static final int CATEGORY = 4;
    public static final Companion Companion = new Companion(null);
    public static final int DIRECTORY = 14;
    public static final int DM = 1;
    public static final int GROUP_DM = 3;
    public static final int GUILD_ANNOUNCEMENT = 5;
    public static final int GUILD_FORUM = 15;
    public static final int GUILD_STAGE_VOICE = 13;
    public static final int GUILD_STORE = 6;
    public static final int GUILD_TEXT = 0;
    public static final int GUILD_VOICE = 2;
    public static final int PRIVATE_THREAD = 12;
    public static final int PUBLIC_THREAD = 11;
    private final long applicationId;
    private final int bitrate;
    private final Integer defaultAutoArchiveDuration;
    private transient GuildHashes guildHashes;
    private final long guildId;
    private final String icon;

    /* renamed from: id  reason: collision with root package name */
    private final long f2019id;
    private final long lastMessageId;
    private final transient ThreadMember member;
    private final transient Integer memberCount;
    private final transient List<Long> memberIdsPreview;
    private final String memberListId;
    private transient Integer messageCount;
    private final String name;
    private final List<ChannelRecipientNick> nicks;
    private final boolean nsfw;
    private final long originChannelId;
    private final long ownerId;
    private final long parentId;
    private final List<PermissionOverwrite> permissionOverwrites;
    private final int position;
    private final int rateLimitPerUser;
    private final List<Long> recipientIds;
    private final List<User> recipients;
    private final String rtcRegion;
    private final ThreadMetadata threadMetadata;
    private final String topic;
    private final int type;
    private final int userLimit;

    /* compiled from: Channel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0011\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0007\u0010\u0004R\u0016\u0010\b\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\b\u0010\u0004R\u0016\u0010\t\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\t\u0010\u0004R\u0016\u0010\n\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\n\u0010\u0004R\u0016\u0010\u000b\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000b\u0010\u0004R\u0016\u0010\f\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\f\u0010\u0004R\u0016\u0010\r\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\r\u0010\u0004R\u0016\u0010\u000e\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000e\u0010\u0004R\u0016\u0010\u000f\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000f\u0010\u0004R\u0016\u0010\u0010\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0010\u0010\u0004¨\u0006\u0013"}, d2 = {"Lcom/discord/api/channel/Channel$Companion;", "", "", "ANNOUNCEMENT_THREAD", "I", "CATEGORY", "DIRECTORY", "DM", "GROUP_DM", "GUILD_ANNOUNCEMENT", "GUILD_FORUM", "GUILD_STAGE_VOICE", "GUILD_STORE", "GUILD_TEXT", "GUILD_VOICE", "PRIVATE_THREAD", "PUBLIC_THREAD", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        public Companion() {
        }

        public Companion(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    public Channel(String str, int i, long j, String str2, long j2, long j3, long j4, List<Long> list, List<User> list2, int i2, List<PermissionOverwrite> list3, int i3, int i4, String str3, long j5, long j6, List<ChannelRecipientNick> list4, boolean z2, long j7, String str4, int i5, Integer num, String str5, GuildHashes guildHashes, ThreadMetadata threadMetadata, Integer num2, Integer num3, List<Long> list5, ThreadMember threadMember) {
        this.topic = str;
        this.type = i;
        this.guildId = j;
        this.name = str2;
        this.lastMessageId = j2;
        this.f2019id = j3;
        this.ownerId = j4;
        this.recipientIds = list;
        this.recipients = list2;
        this.position = i2;
        this.permissionOverwrites = list3;
        this.userLimit = i3;
        this.bitrate = i4;
        this.icon = str3;
        this.originChannelId = j5;
        this.applicationId = j6;
        this.nicks = list4;
        this.nsfw = z2;
        this.parentId = j7;
        this.memberListId = str4;
        this.rateLimitPerUser = i5;
        this.defaultAutoArchiveDuration = num;
        this.rtcRegion = str5;
        this.guildHashes = guildHashes;
        this.threadMetadata = threadMetadata;
        this.messageCount = num2;
        this.memberCount = num3;
        this.memberIdsPreview = list5;
        this.member = threadMember;
    }

    public static Channel a(Channel channel, String str, int i, long j, String str2, long j2, long j3, long j4, List list, List list2, int i2, List list3, int i3, int i4, String str3, long j5, long j6, List list4, boolean z2, long j7, String str4, int i5, Integer num, String str5, GuildHashes guildHashes, ThreadMetadata threadMetadata, Integer num2, Integer num3, List list5, ThreadMember threadMember, int i6) {
        String str6 = (i6 & 1) != 0 ? channel.topic : null;
        int i7 = (i6 & 2) != 0 ? channel.type : i;
        long j8 = (i6 & 4) != 0 ? channel.guildId : j;
        String str7 = (i6 & 8) != 0 ? channel.name : null;
        long j9 = (i6 & 16) != 0 ? channel.lastMessageId : j2;
        long j10 = (i6 & 32) != 0 ? channel.f2019id : j3;
        long j11 = (i6 & 64) != 0 ? channel.ownerId : j4;
        List<Long> list6 = (i6 & 128) != 0 ? channel.recipientIds : null;
        List list7 = (i6 & 256) != 0 ? channel.recipients : list2;
        int i8 = (i6 & 512) != 0 ? channel.position : i2;
        List<PermissionOverwrite> list8 = (i6 & 1024) != 0 ? channel.permissionOverwrites : null;
        int i9 = (i6 & 2048) != 0 ? channel.userLimit : i3;
        int i10 = (i6 & 4096) != 0 ? channel.bitrate : i4;
        String str8 = (i6 & 8192) != 0 ? channel.icon : null;
        List<Long> list9 = list6;
        List list10 = list7;
        long j12 = (i6 & 16384) != 0 ? channel.originChannelId : j5;
        long j13 = (32768 & i6) != 0 ? channel.applicationId : j6;
        List list11 = (65536 & i6) != 0 ? channel.nicks : list4;
        boolean z3 = (i6 & 131072) != 0 ? channel.nsfw : z2;
        long j14 = j13;
        long j15 = (i6 & 262144) != 0 ? channel.parentId : j7;
        String str9 = (i6 & 524288) != 0 ? channel.memberListId : null;
        int i11 = (1048576 & i6) != 0 ? channel.rateLimitPerUser : i5;
        Integer num4 = (i6 & 2097152) != 0 ? channel.defaultAutoArchiveDuration : null;
        String str10 = (i6 & 4194304) != 0 ? channel.rtcRegion : null;
        GuildHashes guildHashes2 = (i6 & 8388608) != 0 ? channel.guildHashes : null;
        ThreadMetadata threadMetadata2 = (i6 & 16777216) != 0 ? channel.threadMetadata : null;
        Integer num5 = (i6 & 33554432) != 0 ? channel.messageCount : null;
        Integer num6 = (i6 & 67108864) != 0 ? channel.memberCount : null;
        List<Long> list12 = (i6 & 134217728) != 0 ? channel.memberIdsPreview : null;
        ThreadMember threadMember2 = (i6 & 268435456) != 0 ? channel.member : null;
        Objects.requireNonNull(channel);
        return new Channel(str6, i7, j8, str7, j9, j10, j11, list9, list10, i8, list8, i9, i10, str8, j12, j14, list11, z3, j15, str9, i11, num4, str10, guildHashes2, threadMetadata2, num5, num6, list12, threadMember2);
    }

    public final int A() {
        return this.type;
    }

    public final int B() {
        return this.userLimit;
    }

    public final long b() {
        return this.applicationId;
    }

    public final int c() {
        return this.bitrate;
    }

    public final Integer d() {
        return this.defaultAutoArchiveDuration;
    }

    public final GuildHashes e() {
        return this.guildHashes;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Channel)) {
            return false;
        }
        Channel channel = (Channel) obj;
        return m.areEqual(this.topic, channel.topic) && this.type == channel.type && this.guildId == channel.guildId && m.areEqual(this.name, channel.name) && this.lastMessageId == channel.lastMessageId && this.f2019id == channel.f2019id && this.ownerId == channel.ownerId && m.areEqual(this.recipientIds, channel.recipientIds) && m.areEqual(this.recipients, channel.recipients) && this.position == channel.position && m.areEqual(this.permissionOverwrites, channel.permissionOverwrites) && this.userLimit == channel.userLimit && this.bitrate == channel.bitrate && m.areEqual(this.icon, channel.icon) && this.originChannelId == channel.originChannelId && this.applicationId == channel.applicationId && m.areEqual(this.nicks, channel.nicks) && this.nsfw == channel.nsfw && this.parentId == channel.parentId && m.areEqual(this.memberListId, channel.memberListId) && this.rateLimitPerUser == channel.rateLimitPerUser && m.areEqual(this.defaultAutoArchiveDuration, channel.defaultAutoArchiveDuration) && m.areEqual(this.rtcRegion, channel.rtcRegion) && m.areEqual(this.guildHashes, channel.guildHashes) && m.areEqual(this.threadMetadata, channel.threadMetadata) && m.areEqual(this.messageCount, channel.messageCount) && m.areEqual(this.memberCount, channel.memberCount) && m.areEqual(this.memberIdsPreview, channel.memberIdsPreview) && m.areEqual(this.member, channel.member);
    }

    public final long f() {
        return this.guildId;
    }

    public final String g() {
        return this.icon;
    }

    public final long h() {
        return this.f2019id;
    }

    public int hashCode() {
        String str = this.topic;
        int i = 0;
        int hashCode = str != null ? str.hashCode() : 0;
        long j = this.guildId;
        int i2 = ((((hashCode * 31) + this.type) * 31) + ((int) (j ^ (j >>> 32)))) * 31;
        String str2 = this.name;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        long j2 = this.lastMessageId;
        long j3 = this.f2019id;
        long j4 = this.ownerId;
        int i3 = (((((((i2 + hashCode2) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + ((int) (j4 ^ (j4 >>> 32)))) * 31;
        List<Long> list = this.recipientIds;
        int hashCode3 = (i3 + (list != null ? list.hashCode() : 0)) * 31;
        List<User> list2 = this.recipients;
        int hashCode4 = (((hashCode3 + (list2 != null ? list2.hashCode() : 0)) * 31) + this.position) * 31;
        List<PermissionOverwrite> list3 = this.permissionOverwrites;
        int hashCode5 = (((((hashCode4 + (list3 != null ? list3.hashCode() : 0)) * 31) + this.userLimit) * 31) + this.bitrate) * 31;
        String str3 = this.icon;
        int hashCode6 = str3 != null ? str3.hashCode() : 0;
        long j5 = this.originChannelId;
        long j6 = this.applicationId;
        int i4 = (((((hashCode5 + hashCode6) * 31) + ((int) (j5 ^ (j5 >>> 32)))) * 31) + ((int) (j6 ^ (j6 >>> 32)))) * 31;
        List<ChannelRecipientNick> list4 = this.nicks;
        int hashCode7 = (i4 + (list4 != null ? list4.hashCode() : 0)) * 31;
        boolean z2 = this.nsfw;
        if (z2) {
            z2 = true;
        }
        int i5 = z2 ? 1 : 0;
        int i6 = z2 ? 1 : 0;
        long j7 = this.parentId;
        int i7 = (((hashCode7 + i5) * 31) + ((int) (j7 ^ (j7 >>> 32)))) * 31;
        String str4 = this.memberListId;
        int hashCode8 = (((i7 + (str4 != null ? str4.hashCode() : 0)) * 31) + this.rateLimitPerUser) * 31;
        Integer num = this.defaultAutoArchiveDuration;
        int hashCode9 = (hashCode8 + (num != null ? num.hashCode() : 0)) * 31;
        String str5 = this.rtcRegion;
        int hashCode10 = (hashCode9 + (str5 != null ? str5.hashCode() : 0)) * 31;
        GuildHashes guildHashes = this.guildHashes;
        int hashCode11 = (hashCode10 + (guildHashes != null ? guildHashes.hashCode() : 0)) * 31;
        ThreadMetadata threadMetadata = this.threadMetadata;
        int hashCode12 = (hashCode11 + (threadMetadata != null ? threadMetadata.hashCode() : 0)) * 31;
        Integer num2 = this.messageCount;
        int hashCode13 = (hashCode12 + (num2 != null ? num2.hashCode() : 0)) * 31;
        Integer num3 = this.memberCount;
        int hashCode14 = (hashCode13 + (num3 != null ? num3.hashCode() : 0)) * 31;
        List<Long> list5 = this.memberIdsPreview;
        int hashCode15 = (hashCode14 + (list5 != null ? list5.hashCode() : 0)) * 31;
        ThreadMember threadMember = this.member;
        if (threadMember != null) {
            i = threadMember.hashCode();
        }
        return hashCode15 + i;
    }

    public final long i() {
        return this.lastMessageId;
    }

    public final ThreadMember j() {
        return this.member;
    }

    public final String k() {
        return this.memberListId;
    }

    public final Integer l() {
        return this.messageCount;
    }

    public final String m() {
        return this.name;
    }

    public final List<ChannelRecipientNick> n() {
        return this.nicks;
    }

    public final boolean o() {
        return this.nsfw;
    }

    public final long p() {
        return this.originChannelId;
    }

    public final long q() {
        return this.ownerId;
    }

    public final long r() {
        return this.parentId;
    }

    public final List<PermissionOverwrite> s() {
        return this.permissionOverwrites;
    }

    public final int t() {
        return this.position;
    }

    public String toString() {
        StringBuilder R = a.R("Channel(topic=");
        R.append(this.topic);
        R.append(", type=");
        R.append(this.type);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", name=");
        R.append(this.name);
        R.append(", lastMessageId=");
        R.append(this.lastMessageId);
        R.append(", id=");
        R.append(this.f2019id);
        R.append(", ownerId=");
        R.append(this.ownerId);
        R.append(", recipientIds=");
        R.append(this.recipientIds);
        R.append(", recipients=");
        R.append(this.recipients);
        R.append(", position=");
        R.append(this.position);
        R.append(", permissionOverwrites=");
        R.append(this.permissionOverwrites);
        R.append(", userLimit=");
        R.append(this.userLimit);
        R.append(", bitrate=");
        R.append(this.bitrate);
        R.append(", icon=");
        R.append(this.icon);
        R.append(", originChannelId=");
        R.append(this.originChannelId);
        R.append(", applicationId=");
        R.append(this.applicationId);
        R.append(", nicks=");
        R.append(this.nicks);
        R.append(", nsfw=");
        R.append(this.nsfw);
        R.append(", parentId=");
        R.append(this.parentId);
        R.append(", memberListId=");
        R.append(this.memberListId);
        R.append(", rateLimitPerUser=");
        R.append(this.rateLimitPerUser);
        R.append(", defaultAutoArchiveDuration=");
        R.append(this.defaultAutoArchiveDuration);
        R.append(", rtcRegion=");
        R.append(this.rtcRegion);
        R.append(", guildHashes=");
        R.append(this.guildHashes);
        R.append(", threadMetadata=");
        R.append(this.threadMetadata);
        R.append(", messageCount=");
        R.append(this.messageCount);
        R.append(", memberCount=");
        R.append(this.memberCount);
        R.append(", memberIdsPreview=");
        R.append(this.memberIdsPreview);
        R.append(", member=");
        R.append(this.member);
        R.append(")");
        return R.toString();
    }

    public final int u() {
        return this.rateLimitPerUser;
    }

    public final List<Long> v() {
        return this.recipientIds;
    }

    public final List<User> w() {
        return this.recipients;
    }

    public final String x() {
        return this.rtcRegion;
    }

    public final ThreadMetadata y() {
        return this.threadMetadata;
    }

    public final String z() {
        return this.topic;
    }
}
