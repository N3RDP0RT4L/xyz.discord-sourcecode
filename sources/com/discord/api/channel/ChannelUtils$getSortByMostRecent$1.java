package com.discord.api.channel;

import com.discord.utilities.message.MessageUtils;
import java.util.Comparator;
import java.util.Map;
import kotlin.Metadata;
/* compiled from: ChannelUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ChannelUtils$getSortByMostRecent$1<T> implements Comparator<Channel> {
    public final /* synthetic */ Map $mostRecentMessageIds;

    public ChannelUtils$getSortByMostRecent$1(Map map) {
        this.$mostRecentMessageIds = map;
    }

    @Override // java.util.Comparator
    public int compare(Channel channel, Channel channel2) {
        long h = channel.h();
        long h2 = channel2.h();
        return MessageUtils.compareMessages(this.$mostRecentMessageIds.containsKey(Long.valueOf(h2)) ? (Long) this.$mostRecentMessageIds.get(Long.valueOf(h2)) : Long.valueOf(h2), this.$mostRecentMessageIds.containsKey(Long.valueOf(h)) ? (Long) this.$mostRecentMessageIds.get(Long.valueOf(h)) : Long.valueOf(h));
    }
}
