package com.discord.api.channel;

import android.content.Context;
import androidx.exifinterface.media.ExifInterface;
import com.discord.api.channel.Channel;
import com.discord.api.guild.GuildFeature;
import com.discord.api.permission.Permission;
import com.discord.api.permission.PermissionOverwrite;
import com.discord.api.role.GuildRole;
import com.discord.api.thread.ThreadMetadata;
import com.discord.api.user.User;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.guild.Guild;
import com.discord.models.presence.Presence;
import com.discord.models.user.CoreUser;
import com.discord.utilities.PermissionOverwriteUtilsKt;
import com.discord.utilities.guilds.GuildUtilsKt;
import com.discord.utilities.guilds.RoleUtils;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.user.UserUtils;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: ChannelUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000j\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\u001a\u0015\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004\u001a\u0011\u0010\u0006\u001a\u00020\u0002*\u00020\u0005¢\u0006\u0004\b\u0006\u0010\u0007\u001a\u0011\u0010\b\u001a\u00020\u0002*\u00020\u0005¢\u0006\u0004\b\b\u0010\u0007\u001a\u0011\u0010\t\u001a\u00020\u0002*\u00020\u0005¢\u0006\u0004\b\t\u0010\u0007\u001a\u0011\u0010\n\u001a\u00020\u0002*\u00020\u0005¢\u0006\u0004\b\n\u0010\u0007\u001a\u0011\u0010\u000b\u001a\u00020\u0002*\u00020\u0005¢\u0006\u0004\b\u000b\u0010\u0007\u001a\u0011\u0010\f\u001a\u00020\u0002*\u00020\u0005¢\u0006\u0004\b\f\u0010\u0007\u001a\u0011\u0010\r\u001a\u00020\u0002*\u00020\u0005¢\u0006\u0004\b\r\u0010\u0007\u001a\u0011\u0010\u000e\u001a\u00020\u0002*\u00020\u0005¢\u0006\u0004\b\u000e\u0010\u0007\u001a\u0011\u0010\u000f\u001a\u00020\u0002*\u00020\u0005¢\u0006\u0004\b\u000f\u0010\u0007\u001a\u0011\u0010\u0010\u001a\u00020\u0002*\u00020\u0005¢\u0006\u0004\b\u0010\u0010\u0007\u001a\u0011\u0010\u0011\u001a\u00020\u0002*\u00020\u0005¢\u0006\u0004\b\u0011\u0010\u0007\u001a\u0011\u0010\u0012\u001a\u00020\u0002*\u00020\u0005¢\u0006\u0004\b\u0012\u0010\u0007\u001a\u0011\u0010\u0013\u001a\u00020\u0002*\u00020\u0005¢\u0006\u0004\b\u0013\u0010\u0007\u001a\u0011\u0010\u0014\u001a\u00020\u0002*\u00020\u0005¢\u0006\u0004\b\u0014\u0010\u0007\u001a\u0011\u0010\u0015\u001a\u00020\u0002*\u00020\u0005¢\u0006\u0004\b\u0015\u0010\u0007\u001a\u0011\u0010\u0016\u001a\u00020\u0002*\u00020\u0005¢\u0006\u0004\b\u0016\u0010\u0007\u001a\u0011\u0010\u0017\u001a\u00020\u0002*\u00020\u0005¢\u0006\u0004\b\u0017\u0010\u0007\u001a\u0011\u0010\u0018\u001a\u00020\u0002*\u00020\u0005¢\u0006\u0004\b\u0018\u0010\u0007\u001a\u0011\u0010\u0019\u001a\u00020\u0002*\u00020\u0005¢\u0006\u0004\b\u0019\u0010\u0007\u001a?\u0010\"\u001a\u00020\u0002*\u00020\u00052\b\u0010\u001b\u001a\u0004\u0018\u00010\u001a2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001c2\u0018\u0010!\u001a\u0014\u0012\b\u0012\u00060\u001fj\u0002` \u0012\u0004\u0012\u00020\u001c\u0018\u00010\u001e¢\u0006\u0004\b\"\u0010#\u001a\u0017\u0010&\u001a\b\u0012\u0004\u0012\u00020%0$*\u00020\u0005¢\u0006\u0004\b&\u0010'\u001a\u0013\u0010(\u001a\u0004\u0018\u00010%*\u00020\u0005¢\u0006\u0004\b(\u0010)\u001a\u001b\u0010,\u001a\u00020\u0002*\u00020\u00052\b\u0010+\u001a\u0004\u0018\u00010*¢\u0006\u0004\b,\u0010-\u001a\u0011\u0010.\u001a\u00020\u0002*\u00020\u0005¢\u0006\u0004\b.\u0010\u0007\u001a\u0011\u00100\u001a\u00020/*\u00020\u0005¢\u0006\u0004\b0\u00101\u001a#\u00105\u001a\u00020/*\u00020\u00052\u0006\u00103\u001a\u0002022\b\b\u0002\u00104\u001a\u00020\u0002¢\u0006\u0004\b5\u00106\u001a\u0013\u00107\u001a\u0004\u0018\u00010\u0000*\u00020\u0005¢\u0006\u0004\b7\u00108\u001a\u0019\u0010;\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050:*\u000209¢\u0006\u0004\b;\u0010<\u001a\u001b\u0010?\u001a\u0004\u0018\u00010>*\u00020\u00052\u0006\u0010=\u001a\u00020\u001f¢\u0006\u0004\b?\u0010@\u001a\u0011\u0010A\u001a\u00020\u0002*\u00020\u0005¢\u0006\u0004\bA\u0010\u0007\"\u0016\u0010B\u001a\u00020\u001f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\bB\u0010C¨\u0006D"}, d2 = {"", "type", "", "l", "(I)Z", "Lcom/discord/api/channel/Channel;", "B", "(Lcom/discord/api/channel/Channel;)Z", "s", ExifInterface.LONGITUDE_EAST, "t", "z", "i", "r", "k", "m", "p", "w", ExifInterface.GPS_MEASUREMENT_IN_PROGRESS, "x", "D", "C", "o", "y", "j", "v", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/api/role/GuildRole;", "everyoneRole", "", "", "Lcom/discord/primitives/RoleId;", "guildRoles", "q", "(Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;Lcom/discord/api/role/GuildRole;Ljava/util/Map;)Z", "", "Lcom/discord/models/user/User;", "g", "(Lcom/discord/api/channel/Channel;)Ljava/util/List;", "a", "(Lcom/discord/api/channel/Channel;)Lcom/discord/models/user/User;", "Lcom/discord/models/presence/Presence;", "presence", "n", "(Lcom/discord/api/channel/Channel;Lcom/discord/models/presence/Presence;)Z", "F", "", "c", "(Lcom/discord/api/channel/Channel;)Ljava/lang/String;", "Landroid/content/Context;", "context", "includePrefix", "d", "(Lcom/discord/api/channel/Channel;Landroid/content/Context;Z)Ljava/lang/String;", "b", "(Lcom/discord/api/channel/Channel;)Ljava/lang/Integer;", "Lcom/discord/api/channel/Channel$Companion;", "Ljava/util/Comparator;", "h", "(Lcom/discord/api/channel/Channel$Companion;)Ljava/util/Comparator;", ModelAuditLogEntry.CHANGE_KEY_ID, "Lcom/discord/api/permission/PermissionOverwrite;", "f", "(Lcom/discord/api/channel/Channel;J)Lcom/discord/api/permission/PermissionOverwrite;", "u", "HQ_DIRECTORY_CHANNEL_ID", "J", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ChannelUtils {
    private static final long HQ_DIRECTORY_CHANNEL_ID = 883060064561299456L;

    public static final boolean A(Channel channel) {
        User user;
        m.checkNotNullParameter(channel, "$this$isSystemDM");
        if (channel.A() == 1) {
            List<User> w = channel.w();
            if (m.areEqual((w == null || (user = (User) u.firstOrNull((List<? extends Object>) w)) == null) ? null : user.p(), Boolean.TRUE)) {
                return true;
            }
        }
        return false;
    }

    public static final boolean B(Channel channel) {
        m.checkNotNullParameter(channel, "$this$isTextChannel");
        return x(channel) || s(channel);
    }

    public static final boolean C(Channel channel) {
        m.checkNotNullParameter(channel, "$this$isThread");
        return channel.A() == 10 || channel.A() == 11 || channel.A() == 12;
    }

    public static final boolean D(Channel channel) {
        m.checkNotNullParameter(channel, "$this$isThreaded");
        return channel.A() == 0 || channel.A() == 5 || channel.A() == 15;
    }

    public static final boolean E(Channel channel) {
        m.checkNotNullParameter(channel, "$this$isVoiceChannel");
        return channel.A() == 2;
    }

    public static final boolean F(Channel channel) {
        m.checkNotNullParameter(channel, "$this$shouldPlayJoinLeaveSounds");
        return E(channel) || x(channel);
    }

    public static final com.discord.models.user.User a(Channel channel) {
        List<User> w;
        List<User> w2;
        User user;
        m.checkNotNullParameter(channel, "$this$getDMRecipient");
        if (!m(channel) || (w = channel.w()) == null || w.size() != 1 || (w2 = channel.w()) == null || (user = (User) u.first((List<? extends Object>) w2)) == null) {
            return null;
        }
        return new CoreUser(user);
    }

    public static final Integer b(Channel channel) {
        m.checkNotNullParameter(channel, "$this$getDeletedMessage");
        if (k(channel)) {
            return Integer.valueOf((int) R.string.category_has_been_deleted);
        }
        if (C(channel)) {
            return Integer.valueOf((int) R.string.thread_has_been_deleted);
        }
        if (x(channel)) {
            return null;
        }
        return Integer.valueOf((int) R.string.channel_has_been_deleted);
    }

    public static final String c(Channel channel) {
        String str;
        m.checkNotNullParameter(channel, "$this$getDisplayName");
        if (x(channel)) {
            String m = channel.m();
            if (m == null || m.length() == 0) {
                List<User> w = channel.w();
                str = w != null ? u.joinToString$default(w, null, null, null, 0, null, ChannelUtils$getDisplayName$1.INSTANCE, 31, null) : null;
                if (str == null) {
                    return "";
                }
                return str;
            }
        }
        str = channel.m();
        if (str == null) {
            return "";
        }
        return str;
    }

    public static final String d(Channel channel, Context context, boolean z2) {
        m.checkNotNullParameter(channel, "$this$getDisplayNameOrDefault");
        m.checkNotNullParameter(context, "context");
        String c = c(channel);
        boolean z3 = false;
        if (r(channel) || i(channel)) {
            if (c.length() == 0) {
                z3 = true;
            }
            if (z3) {
                String string = context.getString(R.string.invalid_text_channel);
                m.checkNotNullExpressionValue(string, "context.getString(R.string.invalid_text_channel)");
                return string;
            } else if (z2) {
                StringBuilder sb = new StringBuilder();
                m.checkNotNullParameter(Channel.Companion, "$this$DISPLAY_PREFIX_GUILD");
                sb.append("#");
                sb.append(c);
                return sb.toString();
            }
        } else if (t(channel)) {
            if (c.length() == 0) {
                z3 = true;
            }
            if (z3) {
                String string2 = context.getString(R.string.invalid_voice_channel);
                m.checkNotNullExpressionValue(string2, "context.getString(R.string.invalid_voice_channel)");
                return string2;
            }
        } else if (p(channel)) {
            if (c.length() == 0) {
                z3 = true;
            }
            if (z3) {
                String string3 = context.getString(R.string.unnamed);
                m.checkNotNullExpressionValue(string3, "context.getString(R.string.unnamed)");
                return string3;
            }
        } else if (m(channel)) {
            if (c.length() == 0) {
                z3 = true;
            }
            if (z3) {
                String string4 = context.getString(R.string.direct_message);
                m.checkNotNullExpressionValue(string4, "context.getString(R.string.direct_message)");
                return string4;
            } else if (z2) {
                StringBuilder sb2 = new StringBuilder();
                m.checkNotNullParameter(Channel.Companion, "$this$DISPLAY_PREFIX_DM");
                sb2.append("@");
                sb2.append(c);
                return sb2.toString();
            }
        }
        return c;
    }

    public static /* synthetic */ String e(Channel channel, Context context, boolean z2, int i) {
        if ((i & 2) != 0) {
            z2 = true;
        }
        return d(channel, context, z2);
    }

    public static final PermissionOverwrite f(Channel channel, long j) {
        boolean z2;
        m.checkNotNullParameter(channel, "$this$getPermissionOverwriteForId");
        List<PermissionOverwrite> s2 = channel.s();
        Object obj = null;
        if (s2 == null) {
            return null;
        }
        Iterator<T> it = s2.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            if (((PermissionOverwrite) next).e() == j) {
                z2 = true;
                continue;
            } else {
                z2 = false;
                continue;
            }
            if (z2) {
                obj = next;
                break;
            }
        }
        return (PermissionOverwrite) obj;
    }

    public static final List<com.discord.models.user.User> g(Channel channel) {
        m.checkNotNullParameter(channel, "$this$getRecipients");
        List<User> w = channel.w();
        if (w == null) {
            return n.emptyList();
        }
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(w, 10));
        for (User user : w) {
            arrayList.add(new CoreUser(user));
        }
        return arrayList;
    }

    public static final Comparator<Channel> h(Channel.Companion companion) {
        m.checkNotNullParameter(companion, "$this$getSortByNameAndType");
        return new ChannelUtils$getSortByNameAndType$1(companion);
    }

    public static final boolean i(Channel channel) {
        m.checkNotNullParameter(channel, "$this$isAnnouncementChannel");
        return channel.A() == 5;
    }

    public static final boolean j(Channel channel) {
        m.checkNotNullParameter(channel, "$this$isArchivedThread");
        ThreadMetadata y2 = channel.y();
        return y2 != null && y2.b();
    }

    public static final boolean k(Channel channel) {
        m.checkNotNullParameter(channel, "$this$isCategory");
        return channel.A() == 4;
    }

    public static final boolean l(int i) {
        return i == 0 || i == 5 || i == 10 || i == 11 || i == 12 || i == 14 || i == 15;
    }

    public static final boolean m(Channel channel) {
        m.checkNotNullParameter(channel, "$this$isDM");
        return channel.A() == 1;
    }

    public static final boolean n(Channel channel, Presence presence) {
        m.checkNotNullParameter(channel, "$this$isDMStatusVisible");
        com.discord.models.user.User a = a(channel);
        return m(channel) && !A(channel) && (a != null ? UserUtils.INSTANCE.isStatusVisible(a, presence, true) : false);
    }

    public static final boolean o(Channel channel) {
        m.checkNotNullParameter(channel, "$this$isDirectory");
        return channel.A() == 14;
    }

    public static final boolean p(Channel channel) {
        m.checkNotNullParameter(channel, "$this$isGroup");
        return channel.A() == 3;
    }

    public static final boolean q(Channel channel, Guild guild, GuildRole guildRole, Map<Long, GuildRole> map) {
        Object obj;
        boolean z2;
        m.checkNotNullParameter(channel, "$this$isGuildRoleSubscriptionChannel");
        if (guild != null && guild.hasFeature(GuildFeature.ROLE_SUBSCRIPTIONS_AVAILABLE_FOR_PURCHASE)) {
            List<PermissionOverwrite> s2 = channel.s();
            if (s2 == null) {
                s2 = n.emptyList();
            }
            for (PermissionOverwrite permissionOverwrite : s2) {
                GuildRole guildRole2 = GuildUtilsKt.getGuildRole(Long.valueOf(permissionOverwrite.e()));
                if (guildRole2 != null && RoleUtils.isSubscriptionRolePurchasableOrHasSubscribers(guildRole2) && PermissionOverwriteUtilsKt.grantsAccessTo(permissionOverwrite, channel)) {
                    return true;
                }
            }
            boolean isFullyGatedGuildRoleSubscriptionGuild = GuildUtilsKt.isFullyGatedGuildRoleSubscriptionGuild(guild, guildRole);
            Iterator<T> it = s2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (((PermissionOverwrite) obj).e() == guild.getId()) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    break;
                }
            }
            PermissionOverwrite permissionOverwrite2 = (PermissionOverwrite) obj;
            boolean deniesAccessTo = permissionOverwrite2 != null ? PermissionOverwriteUtilsKt.deniesAccessTo(permissionOverwrite2, channel) : false;
            if (map != null && !deniesAccessTo && isFullyGatedGuildRoleSubscriptionGuild) {
                for (GuildRole guildRole3 : map.values()) {
                    if (RoleUtils.isSubscriptionRolePurchasableOrHasSubscribers(guildRole3) && PermissionUtils.INSTANCE.canRole(Permission.VIEW_CHANNEL, guildRole3, null)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static final boolean r(Channel channel) {
        m.checkNotNullParameter(channel, "$this$isGuildTextChannel");
        return channel.A() == 0 || channel.A() == 15;
    }

    public static final boolean s(Channel channel) {
        m.checkNotNullParameter(channel, "$this$isGuildTextyChannel");
        return l(channel.A());
    }

    public static final boolean t(Channel channel) {
        m.checkNotNullParameter(channel, "$this$isGuildVocalChannel");
        return channel.A() == 2 || channel.A() == 13;
    }

    public static final boolean u(Channel channel) {
        m.checkNotNullParameter(channel, "$this$isHQDirectoryChannel");
        return channel.h() == HQ_DIRECTORY_CHANNEL_ID;
    }

    public static final boolean v(Channel channel) {
        m.checkNotNullParameter(channel, "$this$isManaged");
        return channel.b() != 0;
    }

    public static final boolean w(Channel channel) {
        m.checkNotNullParameter(channel, "$this$isMultiUserDM");
        return channel.A() == 3;
    }

    public static final boolean x(Channel channel) {
        m.checkNotNullParameter(channel, "$this$isPrivate");
        return channel.A() == 1 || channel.A() == 3;
    }

    public static final boolean y(Channel channel) {
        m.checkNotNullParameter(channel, "$this$isPrivateThread");
        return channel.A() == 12;
    }

    public static final boolean z(Channel channel) {
        m.checkNotNullParameter(channel, "$this$isStageVoiceChannel");
        return channel.A() == 13;
    }
}
