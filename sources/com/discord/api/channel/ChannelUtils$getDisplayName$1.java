package com.discord.api.channel;

import com.discord.api.user.User;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: ChannelUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ChannelUtils$getDisplayName$1 extends o implements Function1<User, CharSequence> {
    public static final ChannelUtils$getDisplayName$1 INSTANCE = new ChannelUtils$getDisplayName$1();

    public ChannelUtils$getDisplayName$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public CharSequence invoke(User user) {
        User user2 = user;
        m.checkNotNullParameter(user2, "it");
        return user2.r();
    }
}
