package com.discord.api.presence;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.activity.Activity;
import com.discord.api.user.User;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: Presence.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001BQ\u0012\u0006\u0010\u0013\u001a\u00020\u0012\u0012\u0010\b\u0002\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\f\u0012\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0017\u0012\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u001c\u0012\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010!\u0012\n\b\u0002\u0010&\u001a\u0004\u0018\u00010!¢\u0006\u0004\b(\u0010)J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR!\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0013\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 R\u001b\u0010\"\u001a\u0004\u0018\u00010!8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%R\u001b\u0010&\u001a\u0004\u0018\u00010!8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010#\u001a\u0004\b'\u0010%¨\u0006*"}, d2 = {"Lcom/discord/api/presence/Presence;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "Lcom/discord/api/activity/Activity;", "activities", "Ljava/util/List;", "b", "()Ljava/util/List;", "Lcom/discord/api/presence/ClientStatus;", "status", "Lcom/discord/api/presence/ClientStatus;", "e", "()Lcom/discord/api/presence/ClientStatus;", "Lcom/discord/api/presence/ClientStatuses;", "clientStatus", "Lcom/discord/api/presence/ClientStatuses;", "c", "()Lcom/discord/api/presence/ClientStatuses;", "Lcom/discord/api/user/User;", "user", "Lcom/discord/api/user/User;", "f", "()Lcom/discord/api/user/User;", "", "userId", "Ljava/lang/Long;", "g", "()Ljava/lang/Long;", "guildId", "d", HookHelper.constructorName, "(Lcom/discord/api/presence/ClientStatus;Ljava/util/List;Lcom/discord/api/presence/ClientStatuses;Lcom/discord/api/user/User;Ljava/lang/Long;Ljava/lang/Long;)V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class Presence {
    private final List<Activity> activities;
    private final ClientStatuses clientStatus;
    private final Long guildId;
    private final ClientStatus status;
    private final User user;
    private final Long userId;

    public Presence(ClientStatus clientStatus, List<Activity> list, ClientStatuses clientStatuses, User user, Long l, Long l2) {
        m.checkNotNullParameter(clientStatus, "status");
        this.status = clientStatus;
        this.activities = list;
        this.clientStatus = clientStatuses;
        this.user = user;
        this.userId = l;
        this.guildId = l2;
    }

    public static Presence a(Presence presence, ClientStatus clientStatus, List list, ClientStatuses clientStatuses, User user, Long l, Long l2, int i) {
        Long l3 = null;
        ClientStatus clientStatus2 = (i & 1) != 0 ? presence.status : null;
        List<Activity> list2 = (i & 2) != 0 ? presence.activities : null;
        ClientStatuses clientStatuses2 = (i & 4) != 0 ? presence.clientStatus : null;
        if ((i & 8) != 0) {
            user = presence.user;
        }
        User user2 = user;
        Long l4 = (i & 16) != 0 ? presence.userId : null;
        if ((i & 32) != 0) {
            l3 = presence.guildId;
        }
        m.checkNotNullParameter(clientStatus2, "status");
        return new Presence(clientStatus2, list2, clientStatuses2, user2, l4, l3);
    }

    public final List<Activity> b() {
        return this.activities;
    }

    public final ClientStatuses c() {
        return this.clientStatus;
    }

    public final Long d() {
        return this.guildId;
    }

    public final ClientStatus e() {
        return this.status;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Presence)) {
            return false;
        }
        Presence presence = (Presence) obj;
        return m.areEqual(this.status, presence.status) && m.areEqual(this.activities, presence.activities) && m.areEqual(this.clientStatus, presence.clientStatus) && m.areEqual(this.user, presence.user) && m.areEqual(this.userId, presence.userId) && m.areEqual(this.guildId, presence.guildId);
    }

    public final User f() {
        return this.user;
    }

    public final Long g() {
        return this.userId;
    }

    public int hashCode() {
        ClientStatus clientStatus = this.status;
        int i = 0;
        int hashCode = (clientStatus != null ? clientStatus.hashCode() : 0) * 31;
        List<Activity> list = this.activities;
        int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
        ClientStatuses clientStatuses = this.clientStatus;
        int hashCode3 = (hashCode2 + (clientStatuses != null ? clientStatuses.hashCode() : 0)) * 31;
        User user = this.user;
        int hashCode4 = (hashCode3 + (user != null ? user.hashCode() : 0)) * 31;
        Long l = this.userId;
        int hashCode5 = (hashCode4 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.guildId;
        if (l2 != null) {
            i = l2.hashCode();
        }
        return hashCode5 + i;
    }

    public String toString() {
        StringBuilder R = a.R("Presence(status=");
        R.append(this.status);
        R.append(", activities=");
        R.append(this.activities);
        R.append(", clientStatus=");
        R.append(this.clientStatus);
        R.append(", user=");
        R.append(this.user);
        R.append(", userId=");
        R.append(this.userId);
        R.append(", guildId=");
        return a.F(R, this.guildId, ")");
    }
}
