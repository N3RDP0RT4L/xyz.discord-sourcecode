package com.discord.api.directory;

import b.d.b.a.a;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: DirectoryEntryGuild.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u0004R\u0019\u0010\u000f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0007R\u0019\u0010\u0013\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u0019\u0010\u0017\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0014\u001a\u0004\b\u0018\u0010\u0016R\u0019\u0010\u0019\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u0014\u001a\u0004\b\u001a\u0010\u0016R\u0019\u0010\u001c\u001a\u00020\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001fR\u0019\u0010!\u001a\u00020 8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010$¨\u0006%"}, d2 = {"Lcom/discord/api/directory/DirectoryEntryEvent;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "Ljava/lang/String;", "getDescription", "primaryCategoryId", "I", "getPrimaryCategoryId", "", "authorId", "J", "getAuthorId", "()J", "directoryChannelId", "getDirectoryChannelId", "entityId", "getEntityId", "Lcom/discord/api/utcdatetime/UtcDateTime;", "createdAt", "Lcom/discord/api/utcdatetime/UtcDateTime;", "getCreatedAt", "()Lcom/discord/api/utcdatetime/UtcDateTime;", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "guildScheduledEvent", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "a", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class DirectoryEntryEvent {
    private final long authorId;
    private final UtcDateTime createdAt;
    private final String description;
    private final long directoryChannelId;
    private final long entityId;
    private final GuildScheduledEvent guildScheduledEvent;
    private final int primaryCategoryId;

    public final GuildScheduledEvent a() {
        return this.guildScheduledEvent;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DirectoryEntryEvent)) {
            return false;
        }
        DirectoryEntryEvent directoryEntryEvent = (DirectoryEntryEvent) obj;
        return this.directoryChannelId == directoryEntryEvent.directoryChannelId && this.entityId == directoryEntryEvent.entityId && this.authorId == directoryEntryEvent.authorId && m.areEqual(this.description, directoryEntryEvent.description) && this.primaryCategoryId == directoryEntryEvent.primaryCategoryId && m.areEqual(this.createdAt, directoryEntryEvent.createdAt) && m.areEqual(this.guildScheduledEvent, directoryEntryEvent.guildScheduledEvent);
    }

    public int hashCode() {
        long j = this.directoryChannelId;
        long j2 = this.entityId;
        long j3 = this.authorId;
        int i = ((((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31;
        String str = this.description;
        int i2 = 0;
        int hashCode = (((i + (str != null ? str.hashCode() : 0)) * 31) + this.primaryCategoryId) * 31;
        UtcDateTime utcDateTime = this.createdAt;
        int hashCode2 = (hashCode + (utcDateTime != null ? utcDateTime.hashCode() : 0)) * 31;
        GuildScheduledEvent guildScheduledEvent = this.guildScheduledEvent;
        if (guildScheduledEvent != null) {
            i2 = guildScheduledEvent.hashCode();
        }
        return hashCode2 + i2;
    }

    public String toString() {
        StringBuilder R = a.R("DirectoryEntryEvent(directoryChannelId=");
        R.append(this.directoryChannelId);
        R.append(", entityId=");
        R.append(this.entityId);
        R.append(", authorId=");
        R.append(this.authorId);
        R.append(", description=");
        R.append(this.description);
        R.append(", primaryCategoryId=");
        R.append(this.primaryCategoryId);
        R.append(", createdAt=");
        R.append(this.createdAt);
        R.append(", guildScheduledEvent=");
        R.append(this.guildScheduledEvent);
        R.append(")");
        return R.toString();
    }
}
