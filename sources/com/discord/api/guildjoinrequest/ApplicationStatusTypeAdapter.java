package com.discord.api.guildjoinrequest;

import andhook.lib.HookHelper;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonWriter;
import d0.z.d.m;
import java.util.Locale;
import java.util.Objects;
import kotlin.Metadata;
/* compiled from: ApplicationStatus.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/api/guildjoinrequest/ApplicationStatusTypeAdapter;", "Lcom/google/gson/TypeAdapter;", "Lcom/discord/api/guildjoinrequest/ApplicationStatus;", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ApplicationStatusTypeAdapter extends TypeAdapter<ApplicationStatus> {
    /* JADX WARN: Code restructure failed: missing block: B:13:0x0030, code lost:
        if (r2.equals("PENDING") != false) goto L16;
     */
    /* JADX WARN: Code restructure failed: missing block: B:15:0x0039, code lost:
        if (r2.equals("SUBMITTED") != false) goto L16;
     */
    /* JADX WARN: Code restructure failed: missing block: B:24:?, code lost:
        return com.discord.api.guildjoinrequest.ApplicationStatus.PENDING;
     */
    @Override // com.google.gson.TypeAdapter
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public com.discord.api.guildjoinrequest.ApplicationStatus read(com.google.gson.stream.JsonReader r2) {
        /*
            r1 = this;
            java.lang.String r0 = "in"
            d0.z.d.m.checkNotNullParameter(r2, r0)
            java.lang.String r2 = r2.J()
            if (r2 != 0) goto Lc
            goto L49
        Lc:
            int r0 = r2.hashCode()
            switch(r0) {
                case -1179202463: goto L3e;
                case -1159694117: goto L33;
                case 35394935: goto L2a;
                case 174130302: goto L1f;
                case 1967871671: goto L14;
                default: goto L13;
            }
        L13:
            goto L49
        L14:
            java.lang.String r0 = "APPROVED"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L49
            com.discord.api.guildjoinrequest.ApplicationStatus r2 = com.discord.api.guildjoinrequest.ApplicationStatus.APPROVED
            goto L4b
        L1f:
            java.lang.String r0 = "REJECTED"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L49
            com.discord.api.guildjoinrequest.ApplicationStatus r2 = com.discord.api.guildjoinrequest.ApplicationStatus.REJECTED
            goto L4b
        L2a:
            java.lang.String r0 = "PENDING"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L49
            goto L3b
        L33:
            java.lang.String r0 = "SUBMITTED"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L49
        L3b:
            com.discord.api.guildjoinrequest.ApplicationStatus r2 = com.discord.api.guildjoinrequest.ApplicationStatus.PENDING
            goto L4b
        L3e:
            java.lang.String r0 = "STARTED"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L49
            com.discord.api.guildjoinrequest.ApplicationStatus r2 = com.discord.api.guildjoinrequest.ApplicationStatus.STARTED
            goto L4b
        L49:
            com.discord.api.guildjoinrequest.ApplicationStatus r2 = com.discord.api.guildjoinrequest.ApplicationStatus.UNKNOWN
        L4b:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.api.guildjoinrequest.ApplicationStatusTypeAdapter.read(com.google.gson.stream.JsonReader):java.lang.Object");
    }

    @Override // com.google.gson.TypeAdapter
    public void write(JsonWriter jsonWriter, ApplicationStatus applicationStatus) {
        ApplicationStatus applicationStatus2 = applicationStatus;
        m.checkNotNullParameter(jsonWriter, "out");
        if (applicationStatus2 != null) {
            String name = applicationStatus2.name();
            Locale locale = Locale.ROOT;
            m.checkNotNullExpressionValue(locale, "Locale.ROOT");
            Objects.requireNonNull(name, "null cannot be cast to non-null type java.lang.String");
            String upperCase = name.toUpperCase(locale);
            m.checkNotNullExpressionValue(upperCase, "(this as java.lang.String).toUpperCase(locale)");
            jsonWriter.H(upperCase);
            return;
        }
        jsonWriter.s();
    }
}
