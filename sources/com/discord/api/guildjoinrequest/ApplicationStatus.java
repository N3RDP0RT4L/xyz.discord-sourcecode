package com.discord.api.guildjoinrequest;

import andhook.lib.HookHelper;
import kotlin.Metadata;
/* compiled from: ApplicationStatus.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\b\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\t"}, d2 = {"Lcom/discord/api/guildjoinrequest/ApplicationStatus;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "STARTED", "PENDING", "REJECTED", "APPROVED", "UNKNOWN", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public enum ApplicationStatus {
    STARTED,
    PENDING,
    REJECTED,
    APPROVED,
    UNKNOWN
}
