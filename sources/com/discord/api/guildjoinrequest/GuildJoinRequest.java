package com.discord.api.guildjoinrequest;

import b.d.b.a.a;
import com.discord.api.user.User;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: GuildJoinRequest.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\r\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0004R\u0019\u0010\u0014\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u000e\u001a\u0004\b\u0015\u0010\u0010R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0012\u001a\u0004\b\u0017\u0010\u0004R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0012\u001a\u0004\b\u0019\u0010\u0004R\u0019\u0010\u001b\u001a\u00020\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u001b\u0010 \u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#R\u0019\u0010$\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u0012\u001a\u0004\b%\u0010\u0004R\u001b\u0010&\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010\u0012\u001a\u0004\b'\u0010\u0004¨\u0006("}, d2 = {"Lcom/discord/api/guildjoinrequest/GuildJoinRequest;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "userId", "J", "e", "()J", "actionedAt", "Ljava/lang/String;", "getActionedAt", "guildId", "b", "rejectionReason", "d", "lastSeen", "c", "Lcom/discord/api/guildjoinrequest/ApplicationStatus;", "applicationStatus", "Lcom/discord/api/guildjoinrequest/ApplicationStatus;", "a", "()Lcom/discord/api/guildjoinrequest/ApplicationStatus;", "Lcom/discord/api/user/User;", "user", "Lcom/discord/api/user/User;", "getUser", "()Lcom/discord/api/user/User;", "createdAt", "getCreatedAt", "inviteCode", "getInviteCode", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildJoinRequest {
    private final String actionedAt;
    private final ApplicationStatus applicationStatus;
    private final String createdAt;
    private final long guildId;
    private final String inviteCode;
    private final String lastSeen;
    private final String rejectionReason;
    private final User user;
    private final long userId;

    public final ApplicationStatus a() {
        return this.applicationStatus;
    }

    public final long b() {
        return this.guildId;
    }

    public final String c() {
        return this.lastSeen;
    }

    public final String d() {
        return this.rejectionReason;
    }

    public final long e() {
        return this.userId;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildJoinRequest)) {
            return false;
        }
        GuildJoinRequest guildJoinRequest = (GuildJoinRequest) obj;
        return this.userId == guildJoinRequest.userId && m.areEqual(this.user, guildJoinRequest.user) && this.guildId == guildJoinRequest.guildId && m.areEqual(this.createdAt, guildJoinRequest.createdAt) && m.areEqual(this.applicationStatus, guildJoinRequest.applicationStatus) && m.areEqual(this.rejectionReason, guildJoinRequest.rejectionReason) && m.areEqual(this.inviteCode, guildJoinRequest.inviteCode) && m.areEqual(this.lastSeen, guildJoinRequest.lastSeen) && m.areEqual(this.actionedAt, guildJoinRequest.actionedAt);
    }

    public int hashCode() {
        long j = this.userId;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        User user = this.user;
        int i2 = 0;
        int hashCode = user != null ? user.hashCode() : 0;
        long j2 = this.guildId;
        int i3 = (((i + hashCode) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31;
        String str = this.createdAt;
        int hashCode2 = (i3 + (str != null ? str.hashCode() : 0)) * 31;
        ApplicationStatus applicationStatus = this.applicationStatus;
        int hashCode3 = (hashCode2 + (applicationStatus != null ? applicationStatus.hashCode() : 0)) * 31;
        String str2 = this.rejectionReason;
        int hashCode4 = (hashCode3 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.inviteCode;
        int hashCode5 = (hashCode4 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.lastSeen;
        int hashCode6 = (hashCode5 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.actionedAt;
        if (str5 != null) {
            i2 = str5.hashCode();
        }
        return hashCode6 + i2;
    }

    public String toString() {
        StringBuilder R = a.R("GuildJoinRequest(userId=");
        R.append(this.userId);
        R.append(", user=");
        R.append(this.user);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", createdAt=");
        R.append(this.createdAt);
        R.append(", applicationStatus=");
        R.append(this.applicationStatus);
        R.append(", rejectionReason=");
        R.append(this.rejectionReason);
        R.append(", inviteCode=");
        R.append(this.inviteCode);
        R.append(", lastSeen=");
        R.append(this.lastSeen);
        R.append(", actionedAt=");
        return a.H(R, this.actionedAt, ")");
    }
}
