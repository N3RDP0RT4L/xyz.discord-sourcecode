package com.discord.api.message.reaction;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: MessageReactionUpdate.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\u0013\u001a\u00020\f\u0012\u0006\u0010\r\u001a\u00020\f\u0012\u0006\u0010\u0011\u001a\u00020\f\u0012\u0006\u0010\u0016\u001a\u00020\u0015¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\r\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0011\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u000e\u001a\u0004\b\u0012\u0010\u0010R\u0019\u0010\u0013\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u000e\u001a\u0004\b\u0014\u0010\u0010R\u0019\u0010\u0016\u001a\u00020\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019¨\u0006\u001c"}, d2 = {"Lcom/discord/api/message/reaction/MessageReactionUpdate;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "channelId", "J", "a", "()J", "messageId", "c", "userId", "d", "Lcom/discord/api/message/reaction/MessageReactionEmoji;", "emoji", "Lcom/discord/api/message/reaction/MessageReactionEmoji;", "b", "()Lcom/discord/api/message/reaction/MessageReactionEmoji;", HookHelper.constructorName, "(JJJLcom/discord/api/message/reaction/MessageReactionEmoji;)V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class MessageReactionUpdate {
    private final long channelId;
    private final MessageReactionEmoji emoji;
    private final long messageId;
    private final long userId;

    public MessageReactionUpdate(long j, long j2, long j3, MessageReactionEmoji messageReactionEmoji) {
        m.checkNotNullParameter(messageReactionEmoji, "emoji");
        this.userId = j;
        this.channelId = j2;
        this.messageId = j3;
        this.emoji = messageReactionEmoji;
    }

    public final long a() {
        return this.channelId;
    }

    public final MessageReactionEmoji b() {
        return this.emoji;
    }

    public final long c() {
        return this.messageId;
    }

    public final long d() {
        return this.userId;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MessageReactionUpdate)) {
            return false;
        }
        MessageReactionUpdate messageReactionUpdate = (MessageReactionUpdate) obj;
        return this.userId == messageReactionUpdate.userId && this.channelId == messageReactionUpdate.channelId && this.messageId == messageReactionUpdate.messageId && m.areEqual(this.emoji, messageReactionUpdate.emoji);
    }

    public int hashCode() {
        long j = this.userId;
        long j2 = this.channelId;
        long j3 = this.messageId;
        int i = ((((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31;
        MessageReactionEmoji messageReactionEmoji = this.emoji;
        return i + (messageReactionEmoji != null ? messageReactionEmoji.hashCode() : 0);
    }

    public String toString() {
        StringBuilder R = a.R("MessageReactionUpdate(userId=");
        R.append(this.userId);
        R.append(", channelId=");
        R.append(this.channelId);
        R.append(", messageId=");
        R.append(this.messageId);
        R.append(", emoji=");
        R.append(this.emoji);
        R.append(")");
        return R.toString();
    }
}
