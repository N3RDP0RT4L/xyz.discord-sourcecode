package com.discord.api.message.reaction;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: MessageReaction.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0011\u001a\u00020\b\u0012\u0006\u0010\u0015\u001a\u00020\u0014\u0012\u0006\u0010\u000e\u001a\u00020\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u001a\u0010\f\u001a\u00020\u00022\b\u0010\u000b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\f\u0010\rR\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0004R\u0019\u0010\u0011\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\nR\u0019\u0010\u0015\u001a\u00020\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018¨\u0006\u001b"}, d2 = {"Lcom/discord/api/message/reaction/MessageReaction;", "", "", "c", "()Z", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "me", "Z", "getMe", "count", "I", "a", "Lcom/discord/api/message/reaction/MessageReactionEmoji;", "emoji", "Lcom/discord/api/message/reaction/MessageReactionEmoji;", "b", "()Lcom/discord/api/message/reaction/MessageReactionEmoji;", HookHelper.constructorName, "(ILcom/discord/api/message/reaction/MessageReactionEmoji;Z)V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class MessageReaction {
    private final int count;
    private final MessageReactionEmoji emoji;

    /* renamed from: me  reason: collision with root package name */
    private final boolean f2044me;

    public MessageReaction(int i, MessageReactionEmoji messageReactionEmoji, boolean z2) {
        m.checkNotNullParameter(messageReactionEmoji, "emoji");
        this.count = i;
        this.emoji = messageReactionEmoji;
        this.f2044me = z2;
    }

    public final int a() {
        return this.count;
    }

    public final MessageReactionEmoji b() {
        return this.emoji;
    }

    public final boolean c() {
        return this.f2044me;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MessageReaction)) {
            return false;
        }
        MessageReaction messageReaction = (MessageReaction) obj;
        return this.count == messageReaction.count && m.areEqual(this.emoji, messageReaction.emoji) && this.f2044me == messageReaction.f2044me;
    }

    public int hashCode() {
        int i = this.count * 31;
        MessageReactionEmoji messageReactionEmoji = this.emoji;
        int hashCode = (i + (messageReactionEmoji != null ? messageReactionEmoji.hashCode() : 0)) * 31;
        boolean z2 = this.f2044me;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        return hashCode + i2;
    }

    public String toString() {
        StringBuilder R = a.R("MessageReaction(count=");
        R.append(this.count);
        R.append(", emoji=");
        R.append(this.emoji);
        R.append(", me=");
        return a.M(R, this.f2044me, ")");
    }
}
