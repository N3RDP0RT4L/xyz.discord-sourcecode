package com.discord.api.message.reaction;

import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: MessageReactionEmoji.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\r\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\r\u001a\u00020\u00052\b\u0010\f\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\r\u0010\u000eR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0004R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0010\u001a\u0004\b\u0013\u0010\u0004R\u0019\u0010\u0014\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0007¨\u0006\u0017"}, d2 = {"Lcom/discord/api/message/reaction/MessageReactionEmoji;", "", "", "c", "()Ljava/lang/String;", "", "e", "()Z", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", ModelAuditLogEntry.CHANGE_KEY_ID, "Ljava/lang/String;", "b", ModelAuditLogEntry.CHANGE_KEY_NAME, "d", "animated", "Z", "a", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class MessageReactionEmoji {
    private final boolean animated;

    /* renamed from: id  reason: collision with root package name */
    private final String f2045id;
    private final String name;

    public final boolean a() {
        return this.animated;
    }

    public final String b() {
        return this.f2045id;
    }

    public final String c() {
        String str = this.f2045id;
        if (str != null) {
            return str;
        }
        String str2 = this.name;
        m.checkNotNull(str2);
        return str2;
    }

    public final String d() {
        return this.name;
    }

    public final boolean e() {
        return this.f2045id != null;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MessageReactionEmoji)) {
            return false;
        }
        MessageReactionEmoji messageReactionEmoji = (MessageReactionEmoji) obj;
        return m.areEqual(this.f2045id, messageReactionEmoji.f2045id) && m.areEqual(this.name, messageReactionEmoji.name) && this.animated == messageReactionEmoji.animated;
    }

    public int hashCode() {
        String str = this.f2045id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.name;
        if (str2 != null) {
            i = str2.hashCode();
        }
        int i2 = (hashCode + i) * 31;
        boolean z2 = this.animated;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        return i2 + i3;
    }

    public String toString() {
        StringBuilder R = a.R("MessageReactionEmoji(id=");
        R.append(this.f2045id);
        R.append(", name=");
        R.append(this.name);
        R.append(", animated=");
        return a.M(R, this.animated, ")");
    }
}
