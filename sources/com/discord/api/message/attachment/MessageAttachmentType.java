package com.discord.api.message.attachment;

import andhook.lib.HookHelper;
import kotlin.Metadata;
/* compiled from: MessageAttachmentType.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/api/message/attachment/MessageAttachmentType;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "VIDEO", "IMAGE", "FILE", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public enum MessageAttachmentType {
    VIDEO,
    IMAGE,
    FILE
}
