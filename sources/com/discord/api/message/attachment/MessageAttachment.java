package com.discord.api.message.attachment;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.g0.t;
import d0.g0.w;
import d0.z.d.m;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: MessageAttachment.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\t\n\u0002\b\u0012\b\u0086\b\u0018\u0000 %2\u00020\u0001:\u0001%J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\r\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u000f\u001a\u00020\u00022\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0011\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\nR\u0019\u0010\u0015\u001a\u00020\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018R\u0019\u0010\u0019\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u0012\u001a\u0004\b\u001a\u0010\nR\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u0019\u0010\u001f\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u0012\u001a\u0004\b \u0010\nR\u001b\u0010!\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u001c\u001a\u0004\b\"\u0010\u001eR\u0019\u0010#\u001a\u00020\u00148\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u0016\u001a\u0004\b$\u0010\u0018¨\u0006&"}, d2 = {"Lcom/discord/api/message/attachment/MessageAttachment;", "", "", "h", "()Z", "Lcom/discord/api/message/attachment/MessageAttachmentType;", "e", "()Lcom/discord/api/message/attachment/MessageAttachmentType;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "url", "Ljava/lang/String;", "f", "", "size", "J", "d", "()J", "proxyUrl", "c", "width", "Ljava/lang/Integer;", "g", "()Ljava/lang/Integer;", "filename", "a", "height", "b", ModelAuditLogEntry.CHANGE_KEY_ID, "getId", "Companion", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class MessageAttachment {
    public static final Companion Companion = new Companion(null);
    public static final String SPOILER_PREFIX = "SPOILER_";
    private final String filename;
    private final Integer height;

    /* renamed from: id  reason: collision with root package name */
    private final long f2043id;
    private final String proxyUrl;
    private final long size;
    private final String url;
    private final Integer width;

    /* compiled from: MessageAttachment.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/api/message/attachment/MessageAttachment$Companion;", "", "", "SPOILER_PREFIX", "Ljava/lang/String;", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        public Companion() {
        }

        public Companion(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    public final String a() {
        return this.filename;
    }

    public final Integer b() {
        return this.height;
    }

    public final String c() {
        return this.proxyUrl;
    }

    public final long d() {
        return this.size;
    }

    public final MessageAttachmentType e() {
        boolean z2;
        String str = this.url;
        Locale locale = Locale.ROOT;
        m.checkNotNullExpressionValue(locale, "Locale.ROOT");
        Objects.requireNonNull(str, "null cannot be cast to non-null type java.lang.String");
        String lowerCase = str.toLowerCase(locale);
        m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
        List<String> a = MessageAttachmentKt.a();
        boolean z3 = true;
        if (!(a instanceof Collection) || !a.isEmpty()) {
            for (String str2 : a) {
                if (w.contains$default((CharSequence) lowerCase, (CharSequence) str2, false, 2, (Object) null)) {
                    z2 = true;
                    break;
                }
            }
        }
        z2 = false;
        if (z2) {
            return MessageAttachmentType.IMAGE;
        }
        List<String> b2 = MessageAttachmentKt.b();
        if (!(b2 instanceof Collection) || !b2.isEmpty()) {
            for (String str3 : b2) {
                if (w.contains$default((CharSequence) lowerCase, (CharSequence) str3, false, 2, (Object) null)) {
                    break;
                }
            }
        }
        z3 = false;
        return z3 ? MessageAttachmentType.VIDEO : MessageAttachmentType.FILE;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MessageAttachment)) {
            return false;
        }
        MessageAttachment messageAttachment = (MessageAttachment) obj;
        return m.areEqual(this.url, messageAttachment.url) && this.size == messageAttachment.size && this.f2043id == messageAttachment.f2043id && m.areEqual(this.proxyUrl, messageAttachment.proxyUrl) && m.areEqual(this.filename, messageAttachment.filename) && m.areEqual(this.width, messageAttachment.width) && m.areEqual(this.height, messageAttachment.height);
    }

    public final String f() {
        return this.url;
    }

    public final Integer g() {
        return this.width;
    }

    public final boolean h() {
        return t.startsWith$default(this.filename, SPOILER_PREFIX, false, 2, null);
    }

    public int hashCode() {
        String str = this.url;
        int i = 0;
        int hashCode = str != null ? str.hashCode() : 0;
        long j = this.size;
        long j2 = this.f2043id;
        int i2 = ((((hashCode * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31;
        String str2 = this.proxyUrl;
        int hashCode2 = (i2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.filename;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        Integer num = this.width;
        int hashCode4 = (hashCode3 + (num != null ? num.hashCode() : 0)) * 31;
        Integer num2 = this.height;
        if (num2 != null) {
            i = num2.hashCode();
        }
        return hashCode4 + i;
    }

    public String toString() {
        StringBuilder R = a.R("MessageAttachment(url=");
        R.append(this.url);
        R.append(", size=");
        R.append(this.size);
        R.append(", id=");
        R.append(this.f2043id);
        R.append(", proxyUrl=");
        R.append(this.proxyUrl);
        R.append(", filename=");
        R.append(this.filename);
        R.append(", width=");
        R.append(this.width);
        R.append(", height=");
        return a.E(R, this.height, ")");
    }
}
