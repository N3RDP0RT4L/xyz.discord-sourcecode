package com.discord.api.message;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: LocalAttachment.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\r\u001a\u00020\f\u0012\u0006\u0010\u0011\u001a\u00020\u0002\u0012\u0006\u0010\u0014\u001a\u00020\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\r\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0011\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0004R\u0019\u0010\u0014\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0012\u001a\u0004\b\u0015\u0010\u0004¨\u0006\u0018"}, d2 = {"Lcom/discord/api/message/LocalAttachment;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", ModelAuditLogEntry.CHANGE_KEY_ID, "J", "b", "()J", "uriString", "Ljava/lang/String;", "c", "displayName", "a", HookHelper.constructorName, "(JLjava/lang/String;Ljava/lang/String;)V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class LocalAttachment {
    private final String displayName;

    /* renamed from: id  reason: collision with root package name */
    private final long f2041id;
    private final String uriString;

    public LocalAttachment(long j, String str, String str2) {
        m.checkNotNullParameter(str, "uriString");
        m.checkNotNullParameter(str2, "displayName");
        this.f2041id = j;
        this.uriString = str;
        this.displayName = str2;
    }

    public final String a() {
        return this.displayName;
    }

    public final long b() {
        return this.f2041id;
    }

    public final String c() {
        return this.uriString;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LocalAttachment)) {
            return false;
        }
        LocalAttachment localAttachment = (LocalAttachment) obj;
        return this.f2041id == localAttachment.f2041id && m.areEqual(this.uriString, localAttachment.uriString) && m.areEqual(this.displayName, localAttachment.displayName);
    }

    public int hashCode() {
        long j = this.f2041id;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        String str = this.uriString;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.displayName;
        if (str2 != null) {
            i2 = str2.hashCode();
        }
        return hashCode + i2;
    }

    public String toString() {
        StringBuilder R = a.R("LocalAttachment(id=");
        R.append(this.f2041id);
        R.append(", uriString=");
        R.append(this.uriString);
        R.append(", displayName=");
        return a.H(R, this.displayName, ")");
    }
}
