package com.discord.api.message;

import andhook.lib.HookHelper;
import kotlin.Metadata;
/* compiled from: MessageTypes.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b#\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b#\u0010$R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0007\u0010\u0004R\u0016\u0010\b\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\b\u0010\u0004R\u0016\u0010\t\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\t\u0010\u0004R\u0016\u0010\n\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\n\u0010\u0004R\u0016\u0010\u000b\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000b\u0010\u0004R\u0016\u0010\f\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\f\u0010\u0004R\u0016\u0010\r\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\r\u0010\u0004R\u0016\u0010\u000e\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000e\u0010\u0004R\u0016\u0010\u000f\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000f\u0010\u0004R\u0016\u0010\u0010\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0010\u0010\u0004R\u0016\u0010\u0011\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0011\u0010\u0004R\u0016\u0010\u0012\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0012\u0010\u0004R\u0016\u0010\u0013\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0013\u0010\u0004R\u0016\u0010\u0014\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0014\u0010\u0004R\u0016\u0010\u0015\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0015\u0010\u0004R\u0016\u0010\u0016\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0016\u0010\u0004R\u0016\u0010\u0017\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0017\u0010\u0004R\u0016\u0010\u0018\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0018\u0010\u0004R\u0016\u0010\u0019\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0019\u0010\u0004R\u0016\u0010\u001a\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u001a\u0010\u0004R\u0016\u0010\u001b\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u001b\u0010\u0004R\u0016\u0010\u001c\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u001c\u0010\u0004R\u0016\u0010\u001d\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u001d\u0010\u0004R\u0016\u0010\u001e\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u001e\u0010\u0004R\u0016\u0010\u001f\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u001f\u0010\u0004R\u0016\u0010 \u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b \u0010\u0004R\u0016\u0010!\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b!\u0010\u0004R\u0016\u0010\"\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\"\u0010\u0004¨\u0006%"}, d2 = {"Lcom/discord/api/message/MessageTypes;", "", "", "THREAD_CREATED", "I", "LOCAL_INVALID_ATTACHMENTS", "USER_PREMIUM_GUILD_SUBSCRIPTION", "CALL", "USER_PREMIUM_GUILD_SUBSCRIPTION_TIER_1", "GUILD_DISCOVERY_REQUALIFIED", "CHAT_INPUT_COMMAND", "THREAD_STARTER_MESSAGE_NOT_FOUND", "THREAD_STARTER_MESSAGE", "RECIPIENT_REMOVE", "LOCAL_APPLICATION_COMMAND_SEND_FAILED", "LOCAL_CAPTCHA_REQUIRED", "GUILD_DISCOVERY_DISQUALIFIED", "LOCAL_SEND_FAILED", "REPLY", "DEFAULT", "USER_PREMIUM_GUILD_SUBSCRIPTION_TIER_3", "LOCAL_APPLICATION_COMMAND", "GUILD_DISCOVERY_GRACE_PERIOD_INITIAL_WARNING", "CHANNEL_NAME_CHANGE", "LOCAL", "CONTEXT_MENU_COMMAND", "GUILD_STREAM", "USER_PREMIUM_GUILD_SUBSCRIPTION_TIER_2", "GUILD_DISCOVERY_GRACE_PERIOD_FINAL_WARNING", "RECIPIENT_ADD", "CHANNEL_PINNED_MESSAGE", "USER_JOIN", "CHANNEL_FOLLOW_ADD", "CHANNEL_ICON_CHANGE", "GUILD_INVITE_REMINDER", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class MessageTypes {
    public static final int CALL = 3;
    public static final int CHANNEL_FOLLOW_ADD = 12;
    public static final int CHANNEL_ICON_CHANGE = 5;
    public static final int CHANNEL_NAME_CHANGE = 4;
    public static final int CHANNEL_PINNED_MESSAGE = 6;
    public static final int CHAT_INPUT_COMMAND = 20;
    public static final int CONTEXT_MENU_COMMAND = 23;
    public static final int DEFAULT = 0;
    public static final int GUILD_DISCOVERY_DISQUALIFIED = 14;
    public static final int GUILD_DISCOVERY_GRACE_PERIOD_FINAL_WARNING = 17;
    public static final int GUILD_DISCOVERY_GRACE_PERIOD_INITIAL_WARNING = 16;
    public static final int GUILD_DISCOVERY_REQUALIFIED = 15;
    public static final int GUILD_INVITE_REMINDER = 22;
    public static final int GUILD_STREAM = 13;
    public static final MessageTypes INSTANCE = new MessageTypes();
    public static final int LOCAL = -1;
    public static final int LOCAL_APPLICATION_COMMAND = -5;
    public static final int LOCAL_APPLICATION_COMMAND_SEND_FAILED = -4;
    public static final int LOCAL_CAPTCHA_REQUIRED = -6;
    public static final int LOCAL_INVALID_ATTACHMENTS = -3;
    public static final int LOCAL_SEND_FAILED = -2;
    public static final int RECIPIENT_ADD = 1;
    public static final int RECIPIENT_REMOVE = 2;
    public static final int REPLY = 19;
    public static final int THREAD_CREATED = 18;
    public static final int THREAD_STARTER_MESSAGE = 21;
    public static final int THREAD_STARTER_MESSAGE_NOT_FOUND = 24;
    public static final int USER_JOIN = 7;
    public static final int USER_PREMIUM_GUILD_SUBSCRIPTION = 8;
    public static final int USER_PREMIUM_GUILD_SUBSCRIPTION_TIER_1 = 9;
    public static final int USER_PREMIUM_GUILD_SUBSCRIPTION_TIER_2 = 10;
    public static final int USER_PREMIUM_GUILD_SUBSCRIPTION_TIER_3 = 11;
}
