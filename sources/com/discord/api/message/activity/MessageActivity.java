package com.discord.api.message.activity;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: MessageActivity.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0010\u001a\u00020\u000f\u0012\u0006\u0010\f\u001a\u00020\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u0004R\u0019\u0010\u0010\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013¨\u0006\u0016"}, d2 = {"Lcom/discord/api/message/activity/MessageActivity;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "partyId", "Ljava/lang/String;", "a", "Lcom/discord/api/message/activity/MessageActivityType;", "type", "Lcom/discord/api/message/activity/MessageActivityType;", "b", "()Lcom/discord/api/message/activity/MessageActivityType;", HookHelper.constructorName, "(Lcom/discord/api/message/activity/MessageActivityType;Ljava/lang/String;)V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class MessageActivity {
    private final String partyId;
    private final MessageActivityType type;

    public MessageActivity(MessageActivityType messageActivityType, String str) {
        m.checkNotNullParameter(messageActivityType, "type");
        m.checkNotNullParameter(str, "partyId");
        this.type = messageActivityType;
        this.partyId = str;
    }

    public final String a() {
        return this.partyId;
    }

    public final MessageActivityType b() {
        return this.type;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MessageActivity)) {
            return false;
        }
        MessageActivity messageActivity = (MessageActivity) obj;
        return m.areEqual(this.type, messageActivity.type) && m.areEqual(this.partyId, messageActivity.partyId);
    }

    public int hashCode() {
        MessageActivityType messageActivityType = this.type;
        int i = 0;
        int hashCode = (messageActivityType != null ? messageActivityType.hashCode() : 0) * 31;
        String str = this.partyId;
        if (str != null) {
            i = str.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder R = a.R("MessageActivity(type=");
        R.append(this.type);
        R.append(", partyId=");
        return a.H(R, this.partyId, ")");
    }
}
