package com.discord.api.message.activity;

import andhook.lib.HookHelper;
import b.c.a.a0.d;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: MessageActivityType.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/api/message/activity/MessageActivityTypeTypeAdapter;", "Lcom/google/gson/TypeAdapter;", "Lcom/discord/api/message/activity/MessageActivityType;", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class MessageActivityTypeTypeAdapter extends TypeAdapter<MessageActivityType> {
    @Override // com.google.gson.TypeAdapter
    public MessageActivityType read(JsonReader jsonReader) {
        m.checkNotNullParameter(jsonReader, "in");
        return MessageActivityType.Companion.a(d.n1(jsonReader));
    }

    @Override // com.google.gson.TypeAdapter
    public void write(JsonWriter jsonWriter, MessageActivityType messageActivityType) {
        MessageActivityType messageActivityType2 = messageActivityType;
        m.checkNotNullParameter(jsonWriter, "out");
        if (messageActivityType2 != null) {
            jsonWriter.D(Integer.valueOf(messageActivityType2.getApiInt()));
        } else {
            jsonWriter.s();
        }
    }
}
