package com.discord.api.message;

import androidx.appcompat.widget.ActivityChooserModel;
import androidx.constraintlayout.solver.widgets.analyzer.BasicMeasure;
import androidx.core.app.NotificationCompat;
import androidx.exifinterface.media.ExifInterface;
import b.d.b.a.a;
import com.discord.api.application.Application;
import com.discord.api.botuikit.Component;
import com.discord.api.channel.Channel;
import com.discord.api.guildmember.GuildMember;
import com.discord.api.interaction.Interaction;
import com.discord.api.message.activity.MessageActivity;
import com.discord.api.message.attachment.MessageAttachment;
import com.discord.api.message.call.MessageCall;
import com.discord.api.message.embed.MessageEmbed;
import com.discord.api.message.reaction.MessageReaction;
import com.discord.api.sticker.Sticker;
import com.discord.api.sticker.StickerPartial;
import com.discord.api.user.User;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: Message.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000¤\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\f\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000fR!\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u0011\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u0019\u0010\u001c\u001a\u00020\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001fR\u001b\u0010 \u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010\u0004R!\u0010$\u001a\n\u0012\u0004\u0012\u00020#\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u0013\u001a\u0004\b%\u0010\u0015R!\u0010'\u001a\n\u0012\u0004\u0012\u00020&\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010\u0013\u001a\u0004\b(\u0010\u0015R\u001b\u0010)\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010*\u001a\u0004\b+\u0010,R\u001b\u0010-\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\r\u001a\u0004\b.\u0010\u000fR\u001b\u00100\u001a\u0004\u0018\u00010/8\u0006@\u0006¢\u0006\f\n\u0004\b0\u00101\u001a\u0004\b2\u00103R\u001b\u00104\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b4\u00105\u001a\u0004\b6\u00107R\u001b\u00109\u001a\u0004\u0018\u0001088\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010:\u001a\u0004\b;\u0010<R\u001b\u0010=\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b=\u0010>\u001a\u0004\b?\u0010@R\u001b\u0010B\u001a\u0004\u0018\u00010A8\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010C\u001a\u0004\bD\u0010ER\u001b\u0010G\u001a\u0004\u0018\u00010F8\u0006@\u0006¢\u0006\f\n\u0004\bG\u0010H\u001a\u0004\bI\u0010JR\u0019\u0010K\u001a\u00020\u001b8\u0006@\u0006¢\u0006\f\n\u0004\bK\u0010\u001d\u001a\u0004\bL\u0010\u001fR\u001b\u0010M\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\bM\u0010*\u001a\u0004\bN\u0010,R\u001b\u0010O\u001a\u0004\u0018\u0001088\u0006@\u0006¢\u0006\f\n\u0004\bO\u0010:\u001a\u0004\bP\u0010<R\u001b\u0010Q\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\bQ\u0010*\u001a\u0004\bR\u0010,R!\u0010T\u001a\n\u0012\u0004\u0012\u00020S\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\bT\u0010\u0013\u001a\u0004\bU\u0010\u0015R!\u0010W\u001a\n\u0012\u0004\u0012\u00020V\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\bW\u0010\u0013\u001a\u0004\bX\u0010\u0015R\u001b\u0010Y\u001a\u0004\u0018\u00010\u00008\u0006@\u0006¢\u0006\f\n\u0004\bY\u0010Z\u001a\u0004\b[\u0010\\R\u001b\u0010]\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b]\u0010*\u001a\u0004\b^\u0010,R\u001b\u0010`\u001a\u0004\u0018\u00010_8\u0006@\u0006¢\u0006\f\n\u0004\b`\u0010a\u001a\u0004\bb\u0010cR\u001b\u0010d\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\bd\u0010\r\u001a\u0004\be\u0010\u000fR\u001b\u0010f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\bf\u0010!\u001a\u0004\bg\u0010\u0004R!\u0010h\u001a\n\u0012\u0004\u0012\u00020\u001b\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\bh\u0010\u0013\u001a\u0004\bi\u0010\u0015R\u001b\u0010j\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\bj\u0010\r\u001a\u0004\bk\u0010\u000fR\u001b\u0010m\u001a\u0004\u0018\u00010l8\u0006@\u0006¢\u0006\f\n\u0004\bm\u0010n\u001a\u0004\bo\u0010pR!\u0010r\u001a\n\u0012\u0004\u0012\u00020q\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\br\u0010\u0013\u001a\u0004\bs\u0010\u0015R!\u0010u\u001a\n\u0012\u0004\u0012\u00020t\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\bu\u0010\u0013\u001a\u0004\bv\u0010\u0015R\u001b\u0010x\u001a\u0004\u0018\u00010w8\u0006@\u0006¢\u0006\f\n\u0004\bx\u0010y\u001a\u0004\bz\u0010{¨\u0006|"}, d2 = {"Lcom/discord/api/message/Message;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "mentionEveryone", "Ljava/lang/Boolean;", "r", "()Ljava/lang/Boolean;", "", "Lcom/discord/api/user/User;", "mentions", "Ljava/util/List;", "t", "()Ljava/util/List;", "Lcom/discord/api/interaction/Interaction;", "interaction", "Lcom/discord/api/interaction/Interaction;", "p", "()Lcom/discord/api/interaction/Interaction;", "", "channelId", "J", "g", "()J", "nonce", "Ljava/lang/String;", "v", "Lcom/discord/api/message/attachment/MessageAttachment;", "attachments", "d", "Lcom/discord/api/message/embed/MessageEmbed;", "embeds", "k", "webhookId", "Ljava/lang/Long;", "F", "()Ljava/lang/Long;", "tts", "D", "Lcom/discord/api/channel/Channel;", "thread", "Lcom/discord/api/channel/Channel;", "B", "()Lcom/discord/api/channel/Channel;", "author", "Lcom/discord/api/user/User;", "e", "()Lcom/discord/api/user/User;", "Lcom/discord/api/utcdatetime/UtcDateTime;", "timestamp", "Lcom/discord/api/utcdatetime/UtcDateTime;", "C", "()Lcom/discord/api/utcdatetime/UtcDateTime;", "type", "Ljava/lang/Integer;", ExifInterface.LONGITUDE_EAST, "()Ljava/lang/Integer;", "Lcom/discord/api/message/call/MessageCall;", NotificationCompat.CATEGORY_CALL, "Lcom/discord/api/message/call/MessageCall;", "f", "()Lcom/discord/api/message/call/MessageCall;", "Lcom/discord/api/message/activity/MessageActivity;", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "Lcom/discord/api/message/activity/MessageActivity;", "a", "()Lcom/discord/api/message/activity/MessageActivity;", ModelAuditLogEntry.CHANGE_KEY_ID, "o", "applicationId", "c", "editedTimestamp", "j", "flags", "l", "Lcom/discord/api/message/reaction/MessageReaction;", "reactions", "x", "Lcom/discord/api/sticker/Sticker;", "stickers", ExifInterface.GPS_MEASUREMENT_IN_PROGRESS, "referencedMessage", "Lcom/discord/api/message/Message;", "y", "()Lcom/discord/api/message/Message;", "guildId", "m", "Lcom/discord/api/guildmember/GuildMember;", "member", "Lcom/discord/api/guildmember/GuildMember;", "q", "()Lcom/discord/api/guildmember/GuildMember;", "hit", "n", "content", "i", "mentionRoles", "s", "pinned", "w", "Lcom/discord/api/application/Application;", "application", "Lcom/discord/api/application/Application;", "b", "()Lcom/discord/api/application/Application;", "Lcom/discord/api/botuikit/Component;", "components", "h", "Lcom/discord/api/sticker/StickerPartial;", "stickerItems", "z", "Lcom/discord/api/message/MessageReference;", "messageReference", "Lcom/discord/api/message/MessageReference;", "u", "()Lcom/discord/api/message/MessageReference;", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class Message {
    private final MessageActivity activity;
    private final Application application;
    private final Long applicationId;
    private final List<MessageAttachment> attachments;
    private final User author;
    private final MessageCall call;
    private final long channelId;
    private final List<Component> components;
    private final String content;
    private final UtcDateTime editedTimestamp;
    private final List<MessageEmbed> embeds;
    private final Long flags;
    private final Long guildId;
    private final Boolean hit;

    /* renamed from: id  reason: collision with root package name */
    private final long f2042id;
    private final Interaction interaction;
    private final GuildMember member;
    private final Boolean mentionEveryone;
    private final List<Long> mentionRoles;
    private final List<User> mentions;
    private final MessageReference messageReference;
    private final String nonce;
    private final Boolean pinned;
    private final List<MessageReaction> reactions;
    private final Message referencedMessage;
    private final List<StickerPartial> stickerItems;
    private final List<Sticker> stickers;
    private final Channel thread;
    private final UtcDateTime timestamp;
    private final Boolean tts;
    private final Integer type;
    private final Long webhookId;

    public Message(long j, long j2, User user, String str, UtcDateTime utcDateTime, UtcDateTime utcDateTime2, Boolean bool, Boolean bool2, List list, List list2, List list3, List list4, List list5, String str2, Boolean bool3, Long l, Integer num, MessageActivity messageActivity, Application application, Long l2, MessageReference messageReference, Long l3, List list6, List list7, Message message, Interaction interaction, Channel channel, List list8, MessageCall messageCall, Long l4, GuildMember guildMember, Boolean bool4, int i) {
        User user2 = (i & 4) != 0 ? null : user;
        String str3 = (i & 8) != 0 ? null : str;
        UtcDateTime utcDateTime3 = (i & 16) != 0 ? null : utcDateTime;
        UtcDateTime utcDateTime4 = (i & 32) != 0 ? null : utcDateTime2;
        Boolean bool5 = (i & 64) != 0 ? null : bool;
        Boolean bool6 = (i & 128) != 0 ? null : bool2;
        List list9 = (i & 256) != 0 ? null : list;
        List list10 = (i & 512) != 0 ? null : list2;
        List list11 = (i & 1024) != 0 ? null : list3;
        List list12 = (i & 2048) != 0 ? null : list4;
        List list13 = (i & 4096) != 0 ? null : list5;
        String str4 = (i & 8192) != 0 ? null : str2;
        Boolean bool7 = (i & 16384) != 0 ? null : bool3;
        Long l5 = (i & 32768) != 0 ? null : l;
        Integer num2 = (i & 65536) != 0 ? null : num;
        MessageActivity messageActivity2 = (i & 131072) != 0 ? null : messageActivity;
        Application application2 = (i & 262144) != 0 ? null : application;
        Long l6 = (i & 524288) != 0 ? null : l2;
        MessageReference messageReference2 = (i & 1048576) != 0 ? null : messageReference;
        Long l7 = (i & 2097152) != 0 ? null : l3;
        List list14 = (i & 4194304) != 0 ? null : list6;
        List list15 = (i & 8388608) != 0 ? null : list7;
        Message message2 = (i & 16777216) != 0 ? null : message;
        Interaction interaction2 = (i & 33554432) != 0 ? null : interaction;
        Channel channel2 = (i & 67108864) != 0 ? null : channel;
        List list16 = (i & 134217728) != 0 ? null : list8;
        MessageCall messageCall2 = (i & 268435456) != 0 ? null : messageCall;
        Long l8 = (i & 536870912) != 0 ? null : l4;
        int i2 = i & BasicMeasure.EXACTLY;
        int i3 = i & Integer.MIN_VALUE;
        this.f2042id = j;
        this.channelId = j2;
        this.author = user2;
        this.content = str3;
        this.timestamp = utcDateTime3;
        this.editedTimestamp = utcDateTime4;
        this.tts = bool5;
        this.mentionEveryone = bool6;
        this.mentions = list9;
        this.mentionRoles = list10;
        this.attachments = list11;
        this.embeds = list12;
        this.reactions = list13;
        this.nonce = str4;
        this.pinned = bool7;
        this.webhookId = l5;
        this.type = num2;
        this.activity = messageActivity2;
        this.application = application2;
        this.applicationId = l6;
        this.messageReference = messageReference2;
        this.flags = l7;
        this.stickers = list14;
        this.stickerItems = list15;
        this.referencedMessage = message2;
        this.interaction = interaction2;
        this.thread = channel2;
        this.components = list16;
        this.call = messageCall2;
        this.guildId = l8;
        this.member = null;
        this.hit = null;
    }

    public final List<Sticker> A() {
        return this.stickers;
    }

    public final Channel B() {
        return this.thread;
    }

    public final UtcDateTime C() {
        return this.timestamp;
    }

    public final Boolean D() {
        return this.tts;
    }

    public final Integer E() {
        return this.type;
    }

    public final Long F() {
        return this.webhookId;
    }

    public final MessageActivity a() {
        return this.activity;
    }

    public final Application b() {
        return this.application;
    }

    public final Long c() {
        return this.applicationId;
    }

    public final List<MessageAttachment> d() {
        return this.attachments;
    }

    public final User e() {
        return this.author;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Message)) {
            return false;
        }
        Message message = (Message) obj;
        return this.f2042id == message.f2042id && this.channelId == message.channelId && m.areEqual(this.author, message.author) && m.areEqual(this.content, message.content) && m.areEqual(this.timestamp, message.timestamp) && m.areEqual(this.editedTimestamp, message.editedTimestamp) && m.areEqual(this.tts, message.tts) && m.areEqual(this.mentionEveryone, message.mentionEveryone) && m.areEqual(this.mentions, message.mentions) && m.areEqual(this.mentionRoles, message.mentionRoles) && m.areEqual(this.attachments, message.attachments) && m.areEqual(this.embeds, message.embeds) && m.areEqual(this.reactions, message.reactions) && m.areEqual(this.nonce, message.nonce) && m.areEqual(this.pinned, message.pinned) && m.areEqual(this.webhookId, message.webhookId) && m.areEqual(this.type, message.type) && m.areEqual(this.activity, message.activity) && m.areEqual(this.application, message.application) && m.areEqual(this.applicationId, message.applicationId) && m.areEqual(this.messageReference, message.messageReference) && m.areEqual(this.flags, message.flags) && m.areEqual(this.stickers, message.stickers) && m.areEqual(this.stickerItems, message.stickerItems) && m.areEqual(this.referencedMessage, message.referencedMessage) && m.areEqual(this.interaction, message.interaction) && m.areEqual(this.thread, message.thread) && m.areEqual(this.components, message.components) && m.areEqual(this.call, message.call) && m.areEqual(this.guildId, message.guildId) && m.areEqual(this.member, message.member) && m.areEqual(this.hit, message.hit);
    }

    public final MessageCall f() {
        return this.call;
    }

    public final long g() {
        return this.channelId;
    }

    public final List<Component> h() {
        return this.components;
    }

    public int hashCode() {
        long j = this.f2042id;
        long j2 = this.channelId;
        int i = ((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31;
        User user = this.author;
        int i2 = 0;
        int hashCode = (i + (user != null ? user.hashCode() : 0)) * 31;
        String str = this.content;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        UtcDateTime utcDateTime = this.timestamp;
        int hashCode3 = (hashCode2 + (utcDateTime != null ? utcDateTime.hashCode() : 0)) * 31;
        UtcDateTime utcDateTime2 = this.editedTimestamp;
        int hashCode4 = (hashCode3 + (utcDateTime2 != null ? utcDateTime2.hashCode() : 0)) * 31;
        Boolean bool = this.tts;
        int hashCode5 = (hashCode4 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.mentionEveryone;
        int hashCode6 = (hashCode5 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        List<User> list = this.mentions;
        int hashCode7 = (hashCode6 + (list != null ? list.hashCode() : 0)) * 31;
        List<Long> list2 = this.mentionRoles;
        int hashCode8 = (hashCode7 + (list2 != null ? list2.hashCode() : 0)) * 31;
        List<MessageAttachment> list3 = this.attachments;
        int hashCode9 = (hashCode8 + (list3 != null ? list3.hashCode() : 0)) * 31;
        List<MessageEmbed> list4 = this.embeds;
        int hashCode10 = (hashCode9 + (list4 != null ? list4.hashCode() : 0)) * 31;
        List<MessageReaction> list5 = this.reactions;
        int hashCode11 = (hashCode10 + (list5 != null ? list5.hashCode() : 0)) * 31;
        String str2 = this.nonce;
        int hashCode12 = (hashCode11 + (str2 != null ? str2.hashCode() : 0)) * 31;
        Boolean bool3 = this.pinned;
        int hashCode13 = (hashCode12 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        Long l = this.webhookId;
        int hashCode14 = (hashCode13 + (l != null ? l.hashCode() : 0)) * 31;
        Integer num = this.type;
        int hashCode15 = (hashCode14 + (num != null ? num.hashCode() : 0)) * 31;
        MessageActivity messageActivity = this.activity;
        int hashCode16 = (hashCode15 + (messageActivity != null ? messageActivity.hashCode() : 0)) * 31;
        Application application = this.application;
        int hashCode17 = (hashCode16 + (application != null ? application.hashCode() : 0)) * 31;
        Long l2 = this.applicationId;
        int hashCode18 = (hashCode17 + (l2 != null ? l2.hashCode() : 0)) * 31;
        MessageReference messageReference = this.messageReference;
        int hashCode19 = (hashCode18 + (messageReference != null ? messageReference.hashCode() : 0)) * 31;
        Long l3 = this.flags;
        int hashCode20 = (hashCode19 + (l3 != null ? l3.hashCode() : 0)) * 31;
        List<Sticker> list6 = this.stickers;
        int hashCode21 = (hashCode20 + (list6 != null ? list6.hashCode() : 0)) * 31;
        List<StickerPartial> list7 = this.stickerItems;
        int hashCode22 = (hashCode21 + (list7 != null ? list7.hashCode() : 0)) * 31;
        Message message = this.referencedMessage;
        int hashCode23 = (hashCode22 + (message != null ? message.hashCode() : 0)) * 31;
        Interaction interaction = this.interaction;
        int hashCode24 = (hashCode23 + (interaction != null ? interaction.hashCode() : 0)) * 31;
        Channel channel = this.thread;
        int hashCode25 = (hashCode24 + (channel != null ? channel.hashCode() : 0)) * 31;
        List<Component> list8 = this.components;
        int hashCode26 = (hashCode25 + (list8 != null ? list8.hashCode() : 0)) * 31;
        MessageCall messageCall = this.call;
        int hashCode27 = (hashCode26 + (messageCall != null ? messageCall.hashCode() : 0)) * 31;
        Long l4 = this.guildId;
        int hashCode28 = (hashCode27 + (l4 != null ? l4.hashCode() : 0)) * 31;
        GuildMember guildMember = this.member;
        int hashCode29 = (hashCode28 + (guildMember != null ? guildMember.hashCode() : 0)) * 31;
        Boolean bool4 = this.hit;
        if (bool4 != null) {
            i2 = bool4.hashCode();
        }
        return hashCode29 + i2;
    }

    public final String i() {
        return this.content;
    }

    public final UtcDateTime j() {
        return this.editedTimestamp;
    }

    public final List<MessageEmbed> k() {
        return this.embeds;
    }

    public final Long l() {
        return this.flags;
    }

    public final Long m() {
        return this.guildId;
    }

    public final Boolean n() {
        return this.hit;
    }

    public final long o() {
        return this.f2042id;
    }

    public final Interaction p() {
        return this.interaction;
    }

    public final GuildMember q() {
        return this.member;
    }

    public final Boolean r() {
        return this.mentionEveryone;
    }

    public final List<Long> s() {
        return this.mentionRoles;
    }

    public final List<User> t() {
        return this.mentions;
    }

    public String toString() {
        StringBuilder R = a.R("Message(id=");
        R.append(this.f2042id);
        R.append(", channelId=");
        R.append(this.channelId);
        R.append(", author=");
        R.append(this.author);
        R.append(", content=");
        R.append(this.content);
        R.append(", timestamp=");
        R.append(this.timestamp);
        R.append(", editedTimestamp=");
        R.append(this.editedTimestamp);
        R.append(", tts=");
        R.append(this.tts);
        R.append(", mentionEveryone=");
        R.append(this.mentionEveryone);
        R.append(", mentions=");
        R.append(this.mentions);
        R.append(", mentionRoles=");
        R.append(this.mentionRoles);
        R.append(", attachments=");
        R.append(this.attachments);
        R.append(", embeds=");
        R.append(this.embeds);
        R.append(", reactions=");
        R.append(this.reactions);
        R.append(", nonce=");
        R.append(this.nonce);
        R.append(", pinned=");
        R.append(this.pinned);
        R.append(", webhookId=");
        R.append(this.webhookId);
        R.append(", type=");
        R.append(this.type);
        R.append(", activity=");
        R.append(this.activity);
        R.append(", application=");
        R.append(this.application);
        R.append(", applicationId=");
        R.append(this.applicationId);
        R.append(", messageReference=");
        R.append(this.messageReference);
        R.append(", flags=");
        R.append(this.flags);
        R.append(", stickers=");
        R.append(this.stickers);
        R.append(", stickerItems=");
        R.append(this.stickerItems);
        R.append(", referencedMessage=");
        R.append(this.referencedMessage);
        R.append(", interaction=");
        R.append(this.interaction);
        R.append(", thread=");
        R.append(this.thread);
        R.append(", components=");
        R.append(this.components);
        R.append(", call=");
        R.append(this.call);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", member=");
        R.append(this.member);
        R.append(", hit=");
        return a.C(R, this.hit, ")");
    }

    public final MessageReference u() {
        return this.messageReference;
    }

    public final String v() {
        return this.nonce;
    }

    public final Boolean w() {
        return this.pinned;
    }

    public final List<MessageReaction> x() {
        return this.reactions;
    }

    public final Message y() {
        return this.referencedMessage;
    }

    public final List<StickerPartial> z() {
        return this.stickerItems;
    }
}
