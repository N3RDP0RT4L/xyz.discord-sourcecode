package com.discord.api.message;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: MessageReference.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\b\u0010\u0013\u001a\u0004\u0018\u00010\f\u0012\b\u0010\r\u001a\u0004\u0018\u00010\f\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\f¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\r\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0011\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u000e\u001a\u0004\b\u0012\u0010\u0010R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u000e\u001a\u0004\b\u0014\u0010\u0010¨\u0006\u0017"}, d2 = {"Lcom/discord/api/message/MessageReference;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "channelId", "Ljava/lang/Long;", "a", "()Ljava/lang/Long;", "messageId", "c", "guildId", "b", HookHelper.constructorName, "(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;)V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class MessageReference {
    private final Long channelId;
    private final Long guildId;
    private final Long messageId;

    public MessageReference(Long l, Long l2, Long l3) {
        this.guildId = l;
        this.channelId = l2;
        this.messageId = l3;
    }

    public final Long a() {
        return this.channelId;
    }

    public final Long b() {
        return this.guildId;
    }

    public final Long c() {
        return this.messageId;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MessageReference)) {
            return false;
        }
        MessageReference messageReference = (MessageReference) obj;
        return m.areEqual(this.guildId, messageReference.guildId) && m.areEqual(this.channelId, messageReference.channelId) && m.areEqual(this.messageId, messageReference.messageId);
    }

    public int hashCode() {
        Long l = this.guildId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        Long l2 = this.channelId;
        int hashCode2 = (hashCode + (l2 != null ? l2.hashCode() : 0)) * 31;
        Long l3 = this.messageId;
        if (l3 != null) {
            i = l3.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("MessageReference(guildId=");
        R.append(this.guildId);
        R.append(", channelId=");
        R.append(this.channelId);
        R.append(", messageId=");
        return a.F(R, this.messageId, ")");
    }
}
