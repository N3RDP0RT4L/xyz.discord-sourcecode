package com.discord.api.message.embed;

import kotlin.Metadata;
/* compiled from: EmbedType.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0003\"\u001c\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u00008\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"", "Lcom/discord/api/message/embed/EmbedType;", "EMBED_TYPES", "[Lcom/discord/api/message/embed/EmbedType;", "discord_api"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class EmbedTypeKt {
    private static final EmbedType[] EMBED_TYPES = EmbedType.values();
}
