package com.discord.api.message.embed;

import andhook.lib.HookHelper;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: EmbedType.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/api/message/embed/EmbedTypeTypeAdapter;", "Lcom/google/gson/TypeAdapter;", "Lcom/discord/api/message/embed/EmbedType;", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class EmbedTypeTypeAdapter extends TypeAdapter<EmbedType> {
    @Override // com.google.gson.TypeAdapter
    public EmbedType read(JsonReader jsonReader) {
        EmbedType[] embedTypeArr;
        EmbedType embedType;
        m.checkNotNullParameter(jsonReader, "in");
        String J = jsonReader.J();
        embedTypeArr = EmbedTypeKt.EMBED_TYPES;
        int length = embedTypeArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                embedType = null;
                break;
            }
            embedType = embedTypeArr[i];
            if (m.areEqual(embedType.getApiValue(), J)) {
                break;
            }
            i++;
        }
        return embedType != null ? embedType : EmbedType.UNKNOWN;
    }

    @Override // com.google.gson.TypeAdapter
    public void write(JsonWriter jsonWriter, EmbedType embedType) {
        EmbedType embedType2 = embedType;
        m.checkNotNullParameter(jsonWriter, "out");
        if (embedType2 != null) {
            jsonWriter.H(embedType2.getApiValue());
        } else {
            jsonWriter.s();
        }
    }
}
