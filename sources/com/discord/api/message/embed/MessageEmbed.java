package com.discord.api.message.embed;

import b.d.b.a.a;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import org.webrtc.MediaStreamTrack;
/* compiled from: MessageEmbed.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u0004R\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\r\u001a\u0004\b\u001a\u0010\u0004R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001fR\u001b\u0010 \u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#R\u001b\u0010%\u001a\u0004\u0018\u00010$8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010&\u001a\u0004\b'\u0010(R\u001b\u0010)\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010\r\u001a\u0004\b*\u0010\u0004R\u001b\u0010,\u001a\u0004\u0018\u00010+8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010-\u001a\u0004\b.\u0010/R\u001b\u00101\u001a\u0004\u0018\u0001008\u0006@\u0006¢\u0006\f\n\u0004\b1\u00102\u001a\u0004\b3\u00104R\u001b\u00106\u001a\u0004\u0018\u0001058\u0006@\u0006¢\u0006\f\n\u0004\b6\u00107\u001a\u0004\b8\u00109R!\u0010<\u001a\n\u0012\u0004\u0012\u00020;\u0018\u00010:8\u0006@\u0006¢\u0006\f\n\u0004\b<\u0010=\u001a\u0004\b>\u0010?R\u001b\u0010A\u001a\u0004\u0018\u00010@8\u0006@\u0006¢\u0006\f\n\u0004\bA\u0010B\u001a\u0004\bC\u0010D¨\u0006E"}, d2 = {"Lcom/discord/api/message/embed/MessageEmbed;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "Ljava/lang/String;", "c", "Lcom/discord/api/message/embed/EmbedProvider;", "provider", "Lcom/discord/api/message/embed/EmbedProvider;", "g", "()Lcom/discord/api/message/embed/EmbedProvider;", "Lcom/discord/api/message/embed/EmbedFooter;", "footer", "Lcom/discord/api/message/embed/EmbedFooter;", "e", "()Lcom/discord/api/message/embed/EmbedFooter;", "url", "l", "Lcom/discord/api/utcdatetime/UtcDateTime;", "timestamp", "Lcom/discord/api/utcdatetime/UtcDateTime;", "i", "()Lcom/discord/api/utcdatetime/UtcDateTime;", ModelAuditLogEntry.CHANGE_KEY_COLOR, "Ljava/lang/Integer;", "b", "()Ljava/lang/Integer;", "Lcom/discord/api/message/embed/EmbedImage;", "image", "Lcom/discord/api/message/embed/EmbedImage;", "f", "()Lcom/discord/api/message/embed/EmbedImage;", "title", "j", "Lcom/discord/api/message/embed/EmbedThumbnail;", "thumbnail", "Lcom/discord/api/message/embed/EmbedThumbnail;", "h", "()Lcom/discord/api/message/embed/EmbedThumbnail;", "Lcom/discord/api/message/embed/EmbedVideo;", MediaStreamTrack.VIDEO_TRACK_KIND, "Lcom/discord/api/message/embed/EmbedVideo;", "m", "()Lcom/discord/api/message/embed/EmbedVideo;", "Lcom/discord/api/message/embed/EmbedAuthor;", "author", "Lcom/discord/api/message/embed/EmbedAuthor;", "a", "()Lcom/discord/api/message/embed/EmbedAuthor;", "", "Lcom/discord/api/message/embed/EmbedField;", "fields", "Ljava/util/List;", "d", "()Ljava/util/List;", "Lcom/discord/api/message/embed/EmbedType;", "type", "Lcom/discord/api/message/embed/EmbedType;", "k", "()Lcom/discord/api/message/embed/EmbedType;", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class MessageEmbed {
    private final EmbedAuthor author;
    private final Integer color;
    private final String description;
    private final List<EmbedField> fields;
    private final EmbedFooter footer;
    private final EmbedImage image;
    private final EmbedProvider provider;
    private final EmbedThumbnail thumbnail;
    private final UtcDateTime timestamp;
    private final String title;
    private final EmbedType type;
    private final String url;
    private final EmbedVideo video;

    public final EmbedAuthor a() {
        return this.author;
    }

    public final Integer b() {
        return this.color;
    }

    public final String c() {
        return this.description;
    }

    public final List<EmbedField> d() {
        return this.fields;
    }

    public final EmbedFooter e() {
        return this.footer;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MessageEmbed)) {
            return false;
        }
        MessageEmbed messageEmbed = (MessageEmbed) obj;
        return m.areEqual(this.title, messageEmbed.title) && m.areEqual(this.type, messageEmbed.type) && m.areEqual(this.description, messageEmbed.description) && m.areEqual(this.url, messageEmbed.url) && m.areEqual(this.timestamp, messageEmbed.timestamp) && m.areEqual(this.color, messageEmbed.color) && m.areEqual(this.footer, messageEmbed.footer) && m.areEqual(this.image, messageEmbed.image) && m.areEqual(this.thumbnail, messageEmbed.thumbnail) && m.areEqual(this.video, messageEmbed.video) && m.areEqual(this.provider, messageEmbed.provider) && m.areEqual(this.author, messageEmbed.author) && m.areEqual(this.fields, messageEmbed.fields);
    }

    public final EmbedImage f() {
        return this.image;
    }

    public final EmbedProvider g() {
        return this.provider;
    }

    public final EmbedThumbnail h() {
        return this.thumbnail;
    }

    public int hashCode() {
        String str = this.title;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        EmbedType embedType = this.type;
        int hashCode2 = (hashCode + (embedType != null ? embedType.hashCode() : 0)) * 31;
        String str2 = this.description;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.url;
        int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
        UtcDateTime utcDateTime = this.timestamp;
        int hashCode5 = (hashCode4 + (utcDateTime != null ? utcDateTime.hashCode() : 0)) * 31;
        Integer num = this.color;
        int hashCode6 = (hashCode5 + (num != null ? num.hashCode() : 0)) * 31;
        EmbedFooter embedFooter = this.footer;
        int hashCode7 = (hashCode6 + (embedFooter != null ? embedFooter.hashCode() : 0)) * 31;
        EmbedImage embedImage = this.image;
        int hashCode8 = (hashCode7 + (embedImage != null ? embedImage.hashCode() : 0)) * 31;
        EmbedThumbnail embedThumbnail = this.thumbnail;
        int hashCode9 = (hashCode8 + (embedThumbnail != null ? embedThumbnail.hashCode() : 0)) * 31;
        EmbedVideo embedVideo = this.video;
        int hashCode10 = (hashCode9 + (embedVideo != null ? embedVideo.hashCode() : 0)) * 31;
        EmbedProvider embedProvider = this.provider;
        int hashCode11 = (hashCode10 + (embedProvider != null ? embedProvider.hashCode() : 0)) * 31;
        EmbedAuthor embedAuthor = this.author;
        int hashCode12 = (hashCode11 + (embedAuthor != null ? embedAuthor.hashCode() : 0)) * 31;
        List<EmbedField> list = this.fields;
        if (list != null) {
            i = list.hashCode();
        }
        return hashCode12 + i;
    }

    public final UtcDateTime i() {
        return this.timestamp;
    }

    public final String j() {
        return this.title;
    }

    public final EmbedType k() {
        return this.type;
    }

    public final String l() {
        return this.url;
    }

    public final EmbedVideo m() {
        return this.video;
    }

    public String toString() {
        StringBuilder R = a.R("MessageEmbed(title=");
        R.append(this.title);
        R.append(", type=");
        R.append(this.type);
        R.append(", description=");
        R.append(this.description);
        R.append(", url=");
        R.append(this.url);
        R.append(", timestamp=");
        R.append(this.timestamp);
        R.append(", color=");
        R.append(this.color);
        R.append(", footer=");
        R.append(this.footer);
        R.append(", image=");
        R.append(this.image);
        R.append(", thumbnail=");
        R.append(this.thumbnail);
        R.append(", video=");
        R.append(this.video);
        R.append(", provider=");
        R.append(this.provider);
        R.append(", author=");
        R.append(this.author);
        R.append(", fields=");
        return a.K(R, this.fields, ")");
    }
}
