package com.discord.api.message;

import andhook.lib.HookHelper;
import kotlin.Metadata;
/* compiled from: MessageFlags.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\f\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rR\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0007\u0010\u0004R\u0016\u0010\b\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\b\u0010\u0004R\u0016\u0010\t\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\t\u0010\u0004R\u0016\u0010\n\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\n\u0010\u0004R\u0016\u0010\u000b\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000b\u0010\u0004¨\u0006\u000e"}, d2 = {"Lcom/discord/api/message/MessageFlags;", "", "", "CROSSPOSTED", "J", "EPHEMERAL", "URGENT", "SUPPRESS_EMBEDS", "IS_CROSSPOST", "HAS_THREAD", "SOURCE_MESSAGE_DELETED", "LOADING", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class MessageFlags {
    public static final long CROSSPOSTED = 1;
    public static final long EPHEMERAL = 64;
    public static final long HAS_THREAD = 32;
    public static final MessageFlags INSTANCE = new MessageFlags();
    public static final long IS_CROSSPOST = 2;
    public static final long LOADING = 128;
    public static final long SOURCE_MESSAGE_DELETED = 8;
    public static final long SUPPRESS_EMBEDS = 4;
    public static final long URGENT = 16;
}
