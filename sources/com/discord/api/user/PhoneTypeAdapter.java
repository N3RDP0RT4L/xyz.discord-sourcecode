package com.discord.api.user;

import andhook.lib.HookHelper;
import com.discord.api.user.Phone;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
/* compiled from: Phone.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/api/user/PhoneTypeAdapter;", "Lcom/google/gson/TypeAdapter;", "Lcom/discord/api/user/Phone;", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class PhoneTypeAdapter extends TypeAdapter<Phone> {
    @Override // com.google.gson.TypeAdapter
    public Phone read(JsonReader jsonReader) {
        m.checkNotNullParameter(jsonReader, "in");
        if (jsonReader.N() == JsonToken.NULL) {
            jsonReader.H();
            return Phone.NoPhoneNumber.INSTANCE;
        }
        String J = jsonReader.J();
        m.checkNotNullExpressionValue(J, "number");
        return new Phone.PhoneNumber(J);
    }

    @Override // com.google.gson.TypeAdapter
    public void write(JsonWriter jsonWriter, Phone phone) {
        JsonWriter jsonWriter2;
        Phone phone2 = phone;
        m.checkNotNullParameter(jsonWriter, "out");
        if (phone2 instanceof Phone.PhoneNumber) {
            jsonWriter2 = jsonWriter.H(((Phone.PhoneNumber) phone2).a());
        } else if (m.areEqual(phone2, Phone.NoPhoneNumber.INSTANCE)) {
            jsonWriter2 = jsonWriter.s();
        } else if (phone2 == null) {
            jsonWriter2 = jsonWriter.s();
        } else {
            throw new NoWhenBranchMatchedException();
        }
        jsonWriter2.getClass();
    }
}
