package com.discord.api.user;

import b.d.b.a.a;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: UserSurvey.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u000f\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R!\u0010\u0011\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u000e\u001a\u0004\b\u0012\u0010\u0010R\u0019\u0010\u0013\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0004R\u0019\u0010\u0016\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0014\u001a\u0004\b\u0017\u0010\u0004R\u0019\u0010\u0018\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0014\u001a\u0004\b\u0019\u0010\u0004R\u0019\u0010\u001a\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0014\u001a\u0004\b\u001b\u0010\u0004R#\u0010\u001e\u001a\f\u0012\b\u0012\u00060\u001cj\u0002`\u001d0\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u000e\u001a\u0004\b\u001f\u0010\u0010¨\u0006 "}, d2 = {"Lcom/discord/api/user/UserSurvey;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "guild_requirements", "Ljava/util/List;", "c", "()Ljava/util/List;", "guild_size", "d", "cta", "Ljava/lang/String;", "a", "url", "g", "prompt", "f", "key", "e", "", "Lcom/discord/api/permission/PermissionBit;", "guild_permissions", "b", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class UserSurvey {
    private final String cta;
    private final List<Long> guild_permissions;
    private final List<String> guild_requirements;
    private final List<Integer> guild_size;
    private final String key;
    private final String prompt;
    private final String url;

    public final String a() {
        return this.cta;
    }

    public final List<Long> b() {
        return this.guild_permissions;
    }

    public final List<String> c() {
        return this.guild_requirements;
    }

    public final List<Integer> d() {
        return this.guild_size;
    }

    public final String e() {
        return this.key;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof UserSurvey)) {
            return false;
        }
        UserSurvey userSurvey = (UserSurvey) obj;
        return m.areEqual(this.prompt, userSurvey.prompt) && m.areEqual(this.cta, userSurvey.cta) && m.areEqual(this.url, userSurvey.url) && m.areEqual(this.key, userSurvey.key) && m.areEqual(this.guild_requirements, userSurvey.guild_requirements) && m.areEqual(this.guild_size, userSurvey.guild_size) && m.areEqual(this.guild_permissions, userSurvey.guild_permissions);
    }

    public final String f() {
        return this.prompt;
    }

    public final String g() {
        return this.url;
    }

    public int hashCode() {
        String str = this.prompt;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.cta;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.url;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.key;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        List<String> list = this.guild_requirements;
        int hashCode5 = (hashCode4 + (list != null ? list.hashCode() : 0)) * 31;
        List<Integer> list2 = this.guild_size;
        int hashCode6 = (hashCode5 + (list2 != null ? list2.hashCode() : 0)) * 31;
        List<Long> list3 = this.guild_permissions;
        if (list3 != null) {
            i = list3.hashCode();
        }
        return hashCode6 + i;
    }

    public String toString() {
        StringBuilder R = a.R("UserSurvey(prompt=");
        R.append(this.prompt);
        R.append(", cta=");
        R.append(this.cta);
        R.append(", url=");
        R.append(this.url);
        R.append(", key=");
        R.append(this.key);
        R.append(", guild_requirements=");
        R.append(this.guild_requirements);
        R.append(", guild_size=");
        R.append(this.guild_size);
        R.append(", guild_permissions=");
        return a.K(R, this.guild_permissions, ")");
    }
}
