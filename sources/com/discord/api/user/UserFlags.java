package com.discord.api.user;

import andhook.lib.HookHelper;
import kotlin.Metadata;
/* compiled from: UserFlags.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0014\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0014\u0010\u0015R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0007\u0010\u0004R\u0016\u0010\b\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\b\u0010\u0004R\u0016\u0010\t\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\t\u0010\u0004R\u0016\u0010\n\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\n\u0010\u0004R\u0016\u0010\u000b\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000b\u0010\u0004R\u0016\u0010\f\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\f\u0010\u0004R\u0016\u0010\r\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\r\u0010\u0004R\u0016\u0010\u000e\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000e\u0010\u0004R\u0016\u0010\u000f\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000f\u0010\u0004R\u0016\u0010\u0010\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0010\u0010\u0004R\u0016\u0010\u0011\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0011\u0010\u0004R\u0016\u0010\u0012\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0012\u0010\u0004R\u0016\u0010\u0013\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0013\u0010\u0004¨\u0006\u0016"}, d2 = {"Lcom/discord/api/user/UserFlags;", "", "", "HAS_UNREAD_URGENT_MESSAGES", "I", "HYPESQUAD", "VERIFIED_DEVELOPER", "SPAMMER", "HYPESQUAD_HOUSE1", "HYPESQUAD_HOUSE2", "BUG_HUNTER_LEVEL_2", "VERIFIED_BOT", "PREMIUM_EARLY_SUPPORTER", "BUG_HUNTER_LEVEL_1", "CERTIFIED_MODERATOR", "STAFF", "BOT_HTTP_INTERACTIONS", "PARTNER", "MFA_SMS", "HYPESQUAD_HOUSE3", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class UserFlags {
    public static final int BOT_HTTP_INTERACTIONS = 524288;
    public static final int BUG_HUNTER_LEVEL_1 = 8;
    public static final int BUG_HUNTER_LEVEL_2 = 16384;
    public static final int CERTIFIED_MODERATOR = 262144;
    public static final int HAS_UNREAD_URGENT_MESSAGES = 8192;
    public static final int HYPESQUAD = 4;
    public static final int HYPESQUAD_HOUSE1 = 64;
    public static final int HYPESQUAD_HOUSE2 = 128;
    public static final int HYPESQUAD_HOUSE3 = 256;
    public static final UserFlags INSTANCE = new UserFlags();
    public static final int MFA_SMS = 16;
    public static final int PARTNER = 2;
    public static final int PREMIUM_EARLY_SUPPORTER = 512;
    public static final int SPAMMER = 1048576;
    public static final int STAFF = 1;
    public static final int VERIFIED_BOT = 65536;
    public static final int VERIFIED_DEVELOPER = 131072;
}
