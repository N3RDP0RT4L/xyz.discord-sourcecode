package com.discord.api.user;

import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.api.guildmember.GuildMember;
import com.discord.api.premium.PremiumTier;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.nullserializable.NullSerializable;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: User.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0014\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\f\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000fR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R!\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u001b\u0010\u001c\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\r\u001a\u0004\b\u001d\u0010\u000fR\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"R\u001b\u0010$\u001a\u0004\u0018\u00010#8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'R\u0019\u0010)\u001a\u00020(8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010*\u001a\u0004\b+\u0010,R!\u0010-\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u0019\u001a\u0004\b.\u0010\u001bR\u0019\u0010/\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u0011\u001a\u0004\b0\u0010\u0004R\u001b\u00101\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010\u0011\u001a\u0004\b2\u0010\u0004R\u001b\u00103\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010\u0014\u001a\u0004\b4\u0010\u0016R!\u00105\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010\u0019\u001a\u0004\b6\u0010\u001bR\u001b\u00107\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010\u0014\u001a\u0004\b8\u0010\u0016R!\u00109\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010\u0019\u001a\u0004\b:\u0010\u001bR\u001b\u0010;\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010\r\u001a\u0004\b<\u0010\u000fR\u001b\u0010>\u001a\u0004\u0018\u00010=8\u0006@\u0006¢\u0006\f\n\u0004\b>\u0010?\u001a\u0004\b@\u0010AR\u0019\u0010B\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010\u0011\u001a\u0004\bC\u0010\u0004R\u001b\u0010D\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\bD\u0010\u0011\u001a\u0004\bE\u0010\u0004R\u001b\u0010G\u001a\u0004\u0018\u00010F8\u0006@\u0006¢\u0006\f\n\u0004\bG\u0010H\u001a\u0004\bI\u0010JR\u001b\u0010K\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\bK\u0010\r\u001a\u0004\bL\u0010\u000fR\u001b\u0010M\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\bM\u0010\u0011\u001a\u0004\bN\u0010\u0004¨\u0006O"}, d2 = {"Lcom/discord/api/user/User;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "system", "Ljava/lang/Boolean;", "p", "()Ljava/lang/Boolean;", "analyticsToken", "Ljava/lang/String;", "getAnalyticsToken", "approximateGuildCount", "Ljava/lang/Integer;", "getApproximateGuildCount", "()Ljava/lang/Integer;", "Lcom/discord/nullserializable/NullSerializable;", "avatar", "Lcom/discord/nullserializable/NullSerializable;", "a", "()Lcom/discord/nullserializable/NullSerializable;", "mfaEnabled", "k", "Lcom/discord/api/user/Phone;", "phone", "Lcom/discord/api/user/Phone;", "m", "()Lcom/discord/api/user/Phone;", "Lcom/discord/api/user/NsfwAllowance;", "nsfwAllowed", "Lcom/discord/api/user/NsfwAllowance;", "l", "()Lcom/discord/api/user/NsfwAllowance;", "", ModelAuditLogEntry.CHANGE_KEY_ID, "J", "i", "()J", "bio", "d", "username", "r", "token", "q", "publicFlags", "o", "banner", "b", "flags", "h", "bannerColor", "c", "bot", "e", "Lcom/discord/api/premium/PremiumTier;", "premiumType", "Lcom/discord/api/premium/PremiumTier;", "n", "()Lcom/discord/api/premium/PremiumTier;", "discriminator", "f", NotificationCompat.CATEGORY_EMAIL, "g", "Lcom/discord/api/guildmember/GuildMember;", "member", "Lcom/discord/api/guildmember/GuildMember;", "j", "()Lcom/discord/api/guildmember/GuildMember;", "verified", "s", "locale", "getLocale", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class User {
    private final String analyticsToken;
    private final Integer approximateGuildCount;
    private final NullSerializable<String> avatar;
    private final NullSerializable<String> banner;
    private final NullSerializable<String> bannerColor;
    private final NullSerializable<String> bio;
    private final Boolean bot;
    private final String discriminator;
    private final String email;
    private final Integer flags;

    /* renamed from: id  reason: collision with root package name */
    private final long f2060id;
    private final String locale;
    private final GuildMember member;
    private final Boolean mfaEnabled;
    private final NsfwAllowance nsfwAllowed;
    private final Phone phone;
    private final PremiumTier premiumType;
    private final Integer publicFlags;
    private final Boolean system;
    private final String token;
    private final String username;
    private final Boolean verified;

    public User(long j, String str, NullSerializable nullSerializable, NullSerializable nullSerializable2, String str2, Integer num, Integer num2, Boolean bool, Boolean bool2, String str3, String str4, Boolean bool3, String str5, NsfwAllowance nsfwAllowance, Boolean bool4, Phone phone, String str6, PremiumTier premiumTier, Integer num3, GuildMember guildMember, NullSerializable nullSerializable3, NullSerializable nullSerializable4, int i) {
        NullSerializable nullSerializable5 = (i & 4) != 0 ? null : nullSerializable;
        int i2 = i & 8;
        String str7 = (i & 16) != 0 ? "0000" : str2;
        Integer num4 = (i & 32) != 0 ? null : num;
        Integer num5 = (i & 64) != 0 ? null : num2;
        Boolean bool5 = (i & 128) != 0 ? null : bool;
        Boolean bool6 = (i & 256) != 0 ? null : bool2;
        int i3 = i & 512;
        int i4 = i & 1024;
        int i5 = i & 2048;
        int i6 = i & 4096;
        int i7 = i & 8192;
        int i8 = i & 16384;
        int i9 = 32768 & i;
        int i10 = 65536 & i;
        int i11 = 131072 & i;
        int i12 = 262144 & i;
        int i13 = 524288 & i;
        int i14 = 1048576 & i;
        int i15 = i & 2097152;
        m.checkNotNullParameter(str, "username");
        m.checkNotNullParameter(str7, "discriminator");
        this.f2060id = j;
        this.username = str;
        this.avatar = nullSerializable5;
        this.banner = null;
        this.discriminator = str7;
        this.publicFlags = num4;
        this.flags = num5;
        this.bot = bool5;
        this.system = bool6;
        this.token = null;
        this.email = null;
        this.verified = null;
        this.locale = null;
        this.nsfwAllowed = null;
        this.mfaEnabled = null;
        this.phone = null;
        this.analyticsToken = null;
        this.premiumType = null;
        this.approximateGuildCount = null;
        this.member = null;
        this.bio = null;
        this.bannerColor = null;
    }

    public final NullSerializable<String> a() {
        return this.avatar;
    }

    public final NullSerializable<String> b() {
        return this.banner;
    }

    public final NullSerializable<String> c() {
        return this.bannerColor;
    }

    public final NullSerializable<String> d() {
        return this.bio;
    }

    public final Boolean e() {
        return this.bot;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof User)) {
            return false;
        }
        User user = (User) obj;
        return this.f2060id == user.f2060id && m.areEqual(this.username, user.username) && m.areEqual(this.avatar, user.avatar) && m.areEqual(this.banner, user.banner) && m.areEqual(this.discriminator, user.discriminator) && m.areEqual(this.publicFlags, user.publicFlags) && m.areEqual(this.flags, user.flags) && m.areEqual(this.bot, user.bot) && m.areEqual(this.system, user.system) && m.areEqual(this.token, user.token) && m.areEqual(this.email, user.email) && m.areEqual(this.verified, user.verified) && m.areEqual(this.locale, user.locale) && m.areEqual(this.nsfwAllowed, user.nsfwAllowed) && m.areEqual(this.mfaEnabled, user.mfaEnabled) && m.areEqual(this.phone, user.phone) && m.areEqual(this.analyticsToken, user.analyticsToken) && m.areEqual(this.premiumType, user.premiumType) && m.areEqual(this.approximateGuildCount, user.approximateGuildCount) && m.areEqual(this.member, user.member) && m.areEqual(this.bio, user.bio) && m.areEqual(this.bannerColor, user.bannerColor);
    }

    public final String f() {
        return this.discriminator;
    }

    public final String g() {
        return this.email;
    }

    public final Integer h() {
        return this.flags;
    }

    public int hashCode() {
        long j = this.f2060id;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        String str = this.username;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        NullSerializable<String> nullSerializable = this.avatar;
        int hashCode2 = (hashCode + (nullSerializable != null ? nullSerializable.hashCode() : 0)) * 31;
        NullSerializable<String> nullSerializable2 = this.banner;
        int hashCode3 = (hashCode2 + (nullSerializable2 != null ? nullSerializable2.hashCode() : 0)) * 31;
        String str2 = this.discriminator;
        int hashCode4 = (hashCode3 + (str2 != null ? str2.hashCode() : 0)) * 31;
        Integer num = this.publicFlags;
        int hashCode5 = (hashCode4 + (num != null ? num.hashCode() : 0)) * 31;
        Integer num2 = this.flags;
        int hashCode6 = (hashCode5 + (num2 != null ? num2.hashCode() : 0)) * 31;
        Boolean bool = this.bot;
        int hashCode7 = (hashCode6 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.system;
        int hashCode8 = (hashCode7 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        String str3 = this.token;
        int hashCode9 = (hashCode8 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.email;
        int hashCode10 = (hashCode9 + (str4 != null ? str4.hashCode() : 0)) * 31;
        Boolean bool3 = this.verified;
        int hashCode11 = (hashCode10 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
        String str5 = this.locale;
        int hashCode12 = (hashCode11 + (str5 != null ? str5.hashCode() : 0)) * 31;
        NsfwAllowance nsfwAllowance = this.nsfwAllowed;
        int hashCode13 = (hashCode12 + (nsfwAllowance != null ? nsfwAllowance.hashCode() : 0)) * 31;
        Boolean bool4 = this.mfaEnabled;
        int hashCode14 = (hashCode13 + (bool4 != null ? bool4.hashCode() : 0)) * 31;
        Phone phone = this.phone;
        int hashCode15 = (hashCode14 + (phone != null ? phone.hashCode() : 0)) * 31;
        String str6 = this.analyticsToken;
        int hashCode16 = (hashCode15 + (str6 != null ? str6.hashCode() : 0)) * 31;
        PremiumTier premiumTier = this.premiumType;
        int hashCode17 = (hashCode16 + (premiumTier != null ? premiumTier.hashCode() : 0)) * 31;
        Integer num3 = this.approximateGuildCount;
        int hashCode18 = (hashCode17 + (num3 != null ? num3.hashCode() : 0)) * 31;
        GuildMember guildMember = this.member;
        int hashCode19 = (hashCode18 + (guildMember != null ? guildMember.hashCode() : 0)) * 31;
        NullSerializable<String> nullSerializable3 = this.bio;
        int hashCode20 = (hashCode19 + (nullSerializable3 != null ? nullSerializable3.hashCode() : 0)) * 31;
        NullSerializable<String> nullSerializable4 = this.bannerColor;
        if (nullSerializable4 != null) {
            i2 = nullSerializable4.hashCode();
        }
        return hashCode20 + i2;
    }

    public final long i() {
        return this.f2060id;
    }

    public final GuildMember j() {
        return this.member;
    }

    public final Boolean k() {
        return this.mfaEnabled;
    }

    public final NsfwAllowance l() {
        return this.nsfwAllowed;
    }

    public final Phone m() {
        return this.phone;
    }

    public final PremiumTier n() {
        return this.premiumType;
    }

    public final Integer o() {
        return this.publicFlags;
    }

    public final Boolean p() {
        return this.system;
    }

    public final String q() {
        return this.token;
    }

    public final String r() {
        return this.username;
    }

    public final Boolean s() {
        return this.verified;
    }

    public String toString() {
        StringBuilder R = a.R("User(id=");
        R.append(this.f2060id);
        R.append(", username=");
        R.append(this.username);
        R.append(", avatar=");
        R.append(this.avatar);
        R.append(", banner=");
        R.append(this.banner);
        R.append(", discriminator=");
        R.append(this.discriminator);
        R.append(", publicFlags=");
        R.append(this.publicFlags);
        R.append(", flags=");
        R.append(this.flags);
        R.append(", bot=");
        R.append(this.bot);
        R.append(", system=");
        R.append(this.system);
        R.append(", token=");
        R.append(this.token);
        R.append(", email=");
        R.append(this.email);
        R.append(", verified=");
        R.append(this.verified);
        R.append(", locale=");
        R.append(this.locale);
        R.append(", nsfwAllowed=");
        R.append(this.nsfwAllowed);
        R.append(", mfaEnabled=");
        R.append(this.mfaEnabled);
        R.append(", phone=");
        R.append(this.phone);
        R.append(", analyticsToken=");
        R.append(this.analyticsToken);
        R.append(", premiumType=");
        R.append(this.premiumType);
        R.append(", approximateGuildCount=");
        R.append(this.approximateGuildCount);
        R.append(", member=");
        R.append(this.member);
        R.append(", bio=");
        R.append(this.bio);
        R.append(", bannerColor=");
        R.append(this.bannerColor);
        R.append(")");
        return R.toString();
    }
}
