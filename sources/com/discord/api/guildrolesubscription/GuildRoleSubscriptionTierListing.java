package com.discord.api.guildrolesubscription;

import b.d.b.a.a;
import com.discord.api.guildscheduledevent.GuildRoleSubscriptionRoleBenefits;
import com.discord.api.premium.SubscriptionPlan;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: GuildRoleSubscriptionTierListing.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\r\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0011\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R\u0019\u0010\u0015\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0016\u001a\u0004\b\u0019\u0010\u0004R\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u0019\u0010 \u001a\u00020\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#R\u001d\u0010%\u001a\u00060\fj\u0002`$8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u000e\u001a\u0004\b&\u0010\u0010R\u001d\u0010(\u001a\u00060\fj\u0002`'8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010\u000e\u001a\u0004\b)\u0010\u0010R\u001f\u0010,\u001a\b\u0012\u0004\u0012\u00020+0*8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010-\u001a\u0004\b.\u0010/¨\u00060"}, d2 = {"Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionTierListing;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", ModelAuditLogEntry.CHANGE_KEY_ID, "J", "c", "()J", "published", "Z", "f", "()Z", ModelAuditLogEntry.CHANGE_KEY_NAME, "Ljava/lang/String;", "e", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "b", "Lcom/discord/api/guildrolesubscription/ImageAsset;", "imageAsset", "Lcom/discord/api/guildrolesubscription/ImageAsset;", "d", "()Lcom/discord/api/guildrolesubscription/ImageAsset;", "Lcom/discord/api/guildscheduledevent/GuildRoleSubscriptionRoleBenefits;", "roleBenefits", "Lcom/discord/api/guildscheduledevent/GuildRoleSubscriptionRoleBenefits;", "g", "()Lcom/discord/api/guildscheduledevent/GuildRoleSubscriptionRoleBenefits;", "Lcom/discord/primitives/RoleId;", "roleId", "h", "Lcom/discord/primitives/ApplicationId;", "applicationId", "a", "", "Lcom/discord/api/premium/SubscriptionPlan;", "subscriptionPlans", "Ljava/util/List;", "i", "()Ljava/util/List;", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildRoleSubscriptionTierListing {
    private final long applicationId;
    private final String description;

    /* renamed from: id  reason: collision with root package name */
    private final long f2033id;
    private final ImageAsset imageAsset;
    private final String name;
    private final boolean published;
    private final GuildRoleSubscriptionRoleBenefits roleBenefits;
    private final long roleId;
    private final List<SubscriptionPlan> subscriptionPlans;

    public final long a() {
        return this.applicationId;
    }

    public final String b() {
        return this.description;
    }

    public final long c() {
        return this.f2033id;
    }

    public final ImageAsset d() {
        return this.imageAsset;
    }

    public final String e() {
        return this.name;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildRoleSubscriptionTierListing)) {
            return false;
        }
        GuildRoleSubscriptionTierListing guildRoleSubscriptionTierListing = (GuildRoleSubscriptionTierListing) obj;
        return this.f2033id == guildRoleSubscriptionTierListing.f2033id && this.applicationId == guildRoleSubscriptionTierListing.applicationId && m.areEqual(this.name, guildRoleSubscriptionTierListing.name) && this.published == guildRoleSubscriptionTierListing.published && m.areEqual(this.description, guildRoleSubscriptionTierListing.description) && m.areEqual(this.imageAsset, guildRoleSubscriptionTierListing.imageAsset) && m.areEqual(this.subscriptionPlans, guildRoleSubscriptionTierListing.subscriptionPlans) && m.areEqual(this.roleBenefits, guildRoleSubscriptionTierListing.roleBenefits) && this.roleId == guildRoleSubscriptionTierListing.roleId;
    }

    public final boolean f() {
        return this.published;
    }

    public final GuildRoleSubscriptionRoleBenefits g() {
        return this.roleBenefits;
    }

    public final long h() {
        return this.roleId;
    }

    public int hashCode() {
        long j = this.f2033id;
        long j2 = this.applicationId;
        int i = ((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31;
        String str = this.name;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        boolean z2 = this.published;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        int i5 = (hashCode + i3) * 31;
        String str2 = this.description;
        int hashCode2 = (i5 + (str2 != null ? str2.hashCode() : 0)) * 31;
        ImageAsset imageAsset = this.imageAsset;
        int hashCode3 = (hashCode2 + (imageAsset != null ? imageAsset.hashCode() : 0)) * 31;
        List<SubscriptionPlan> list = this.subscriptionPlans;
        int hashCode4 = (hashCode3 + (list != null ? list.hashCode() : 0)) * 31;
        GuildRoleSubscriptionRoleBenefits guildRoleSubscriptionRoleBenefits = this.roleBenefits;
        if (guildRoleSubscriptionRoleBenefits != null) {
            i2 = guildRoleSubscriptionRoleBenefits.hashCode();
        }
        long j3 = this.roleId;
        return ((hashCode4 + i2) * 31) + ((int) (j3 ^ (j3 >>> 32)));
    }

    public final List<SubscriptionPlan> i() {
        return this.subscriptionPlans;
    }

    public String toString() {
        StringBuilder R = a.R("GuildRoleSubscriptionTierListing(id=");
        R.append(this.f2033id);
        R.append(", applicationId=");
        R.append(this.applicationId);
        R.append(", name=");
        R.append(this.name);
        R.append(", published=");
        R.append(this.published);
        R.append(", description=");
        R.append(this.description);
        R.append(", imageAsset=");
        R.append(this.imageAsset);
        R.append(", subscriptionPlans=");
        R.append(this.subscriptionPlans);
        R.append(", roleBenefits=");
        R.append(this.roleBenefits);
        R.append(", roleId=");
        return a.B(R, this.roleId, ")");
    }
}
