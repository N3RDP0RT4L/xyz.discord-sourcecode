package com.discord.api.guildrolesubscription;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: GuildRoleSubscriptionBenefit.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001BC\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0011\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\u001d\u001a\u00020\u0002\u0012\u0006\u0010\u0019\u001a\u00020\u0018\u0012\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u0011¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u0004R\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\r\u001a\u0004\b\u0010\u0010\u0004R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0013\u001a\u0004\b\u0017\u0010\u0015R\u0019\u0010\u0019\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001cR\u0019\u0010\u001d\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\r\u001a\u0004\b\u001e\u0010\u0004¨\u0006!"}, d2 = {"Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefit;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "emojiName", "Ljava/lang/String;", "c", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "a", "", "emojiId", "Ljava/lang/Long;", "b", "()Ljava/lang/Long;", "refId", "e", "Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;", "refType", "Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;", "f", "()Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;", ModelAuditLogEntry.CHANGE_KEY_NAME, "d", HookHelper.constructorName, "(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;Ljava/lang/String;Ljava/lang/Long;)V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildRoleSubscriptionBenefit {
    private final String description;
    private final Long emojiId;
    private final String emojiName;
    private final String name;
    private final Long refId;
    private final GuildRoleSubscriptionBenefitType refType;

    public GuildRoleSubscriptionBenefit(Long l, String str, String str2, GuildRoleSubscriptionBenefitType guildRoleSubscriptionBenefitType, String str3, Long l2) {
        m.checkNotNullParameter(str2, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(guildRoleSubscriptionBenefitType, "refType");
        this.emojiId = l;
        this.emojiName = str;
        this.name = str2;
        this.refType = guildRoleSubscriptionBenefitType;
        this.description = str3;
        this.refId = l2;
    }

    public final String a() {
        return this.description;
    }

    public final Long b() {
        return this.emojiId;
    }

    public final String c() {
        return this.emojiName;
    }

    public final String d() {
        return this.name;
    }

    public final Long e() {
        return this.refId;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildRoleSubscriptionBenefit)) {
            return false;
        }
        GuildRoleSubscriptionBenefit guildRoleSubscriptionBenefit = (GuildRoleSubscriptionBenefit) obj;
        return m.areEqual(this.emojiId, guildRoleSubscriptionBenefit.emojiId) && m.areEqual(this.emojiName, guildRoleSubscriptionBenefit.emojiName) && m.areEqual(this.name, guildRoleSubscriptionBenefit.name) && m.areEqual(this.refType, guildRoleSubscriptionBenefit.refType) && m.areEqual(this.description, guildRoleSubscriptionBenefit.description) && m.areEqual(this.refId, guildRoleSubscriptionBenefit.refId);
    }

    public final GuildRoleSubscriptionBenefitType f() {
        return this.refType;
    }

    public int hashCode() {
        Long l = this.emojiId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        String str = this.emojiName;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.name;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        GuildRoleSubscriptionBenefitType guildRoleSubscriptionBenefitType = this.refType;
        int hashCode4 = (hashCode3 + (guildRoleSubscriptionBenefitType != null ? guildRoleSubscriptionBenefitType.hashCode() : 0)) * 31;
        String str3 = this.description;
        int hashCode5 = (hashCode4 + (str3 != null ? str3.hashCode() : 0)) * 31;
        Long l2 = this.refId;
        if (l2 != null) {
            i = l2.hashCode();
        }
        return hashCode5 + i;
    }

    public String toString() {
        StringBuilder R = a.R("GuildRoleSubscriptionBenefit(emojiId=");
        R.append(this.emojiId);
        R.append(", emojiName=");
        R.append(this.emojiName);
        R.append(", name=");
        R.append(this.name);
        R.append(", refType=");
        R.append(this.refType);
        R.append(", description=");
        R.append(this.description);
        R.append(", refId=");
        return a.F(R, this.refId, ")");
    }
}
