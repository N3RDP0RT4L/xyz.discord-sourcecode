package com.discord.api.guildrolesubscription;

import andhook.lib.HookHelper;
import com.discord.api.guildrolesubscription.PayoutGroupStatus;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
/* compiled from: PayoutGroupStatus.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/api/guildrolesubscription/PayoutGroupStatusTypeAdapter;", "Lcom/google/gson/TypeAdapter;", "Lcom/discord/api/guildrolesubscription/PayoutGroupStatus;", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class PayoutGroupStatusTypeAdapter extends TypeAdapter<PayoutGroupStatus> {
    @Override // com.google.gson.TypeAdapter
    public PayoutGroupStatus read(JsonReader jsonReader) {
        m.checkNotNullParameter(jsonReader, "in");
        PayoutGroupStatus.Companion companion = PayoutGroupStatus.Companion;
        int y2 = jsonReader.y();
        Objects.requireNonNull(companion);
        if (y2 == 1) {
            return PayoutGroupStatus.OPEN;
        }
        if (y2 == 2) {
            return PayoutGroupStatus.PAYOUT_CREATED;
        }
        if (y2 != 3) {
            return PayoutGroupStatus.UNKNOWN;
        }
        return PayoutGroupStatus.CANCELED;
    }

    @Override // com.google.gson.TypeAdapter
    public void write(JsonWriter jsonWriter, PayoutGroupStatus payoutGroupStatus) {
        PayoutGroupStatus payoutGroupStatus2 = payoutGroupStatus;
        m.checkNotNullParameter(jsonWriter, "out");
        if (payoutGroupStatus2 != null) {
            jsonWriter.D(Integer.valueOf(payoutGroupStatus2.getApiValue()));
        } else {
            jsonWriter.s();
        }
    }
}
