package com.discord.api.guildrolesubscription;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
/* compiled from: GuildRoleSubscriptionGroupListing.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001Bc\u0012\u0006\u0010\"\u001a\u00020\u0010\u0012\n\u0010\u001b\u001a\u00060\u0010j\u0002`\u001a\u0012\n\u0010\u0016\u001a\u00060\u0010j\u0002`\u0015\u0012\b\u0010\u001e\u001a\u0004\u0018\u00010\u001d\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0002\u0012\u000e\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000f\u0012\u000e\u0010%\u001a\n\u0012\u0004\u0012\u00020$\u0018\u00010\u000f\u0012\u0006\u0010'\u001a\u00020\t¢\u0006\u0004\b+\u0010,J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u0004R!\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R\u001d\u0010\u0016\u001a\u00060\u0010j\u0002`\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u001d\u0010\u001b\u001a\u00060\u0010j\u0002`\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u0017\u001a\u0004\b\u001c\u0010\u0019R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010!R\u0019\u0010\"\u001a\u00020\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u0017\u001a\u0004\b#\u0010\u0019R!\u0010%\u001a\n\u0012\u0004\u0012\u00020$\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u0012\u001a\u0004\b&\u0010\u0014R\u0019\u0010'\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*¨\u0006-"}, d2 = {"Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionGroupListing;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "Ljava/lang/String;", "c", "", "", "subscriptionListingsIds", "Ljava/util/List;", "i", "()Ljava/util/List;", "Lcom/discord/primitives/ApplicationId;", "applicationId", "J", "b", "()J", "Lcom/discord/primitives/GuildId;", "guildId", "e", "Lcom/discord/api/guildrolesubscription/ImageAsset;", "imageAsset", "Lcom/discord/api/guildrolesubscription/ImageAsset;", "g", "()Lcom/discord/api/guildrolesubscription/ImageAsset;", ModelAuditLogEntry.CHANGE_KEY_ID, "f", "Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionTierListing;", "subscriptionListings", "h", "fullServerGate", "Z", "d", "()Z", HookHelper.constructorName, "(JJJLcom/discord/api/guildrolesubscription/ImageAsset;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Z)V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildRoleSubscriptionGroupListing {
    private final long applicationId;
    private final String description;
    private final boolean fullServerGate;
    private final long guildId;

    /* renamed from: id  reason: collision with root package name */
    private final long f2032id;
    private final ImageAsset imageAsset;
    private final List<GuildRoleSubscriptionTierListing> subscriptionListings;
    private final List<Long> subscriptionListingsIds;

    public GuildRoleSubscriptionGroupListing(long j, long j2, long j3, ImageAsset imageAsset, String str, List<Long> list, List<GuildRoleSubscriptionTierListing> list2, boolean z2) {
        this.f2032id = j;
        this.guildId = j2;
        this.applicationId = j3;
        this.imageAsset = imageAsset;
        this.description = str;
        this.subscriptionListingsIds = list;
        this.subscriptionListings = list2;
        this.fullServerGate = z2;
    }

    public static GuildRoleSubscriptionGroupListing a(GuildRoleSubscriptionGroupListing guildRoleSubscriptionGroupListing, long j, long j2, long j3, ImageAsset imageAsset, String str, List list, List list2, boolean z2, int i) {
        long j4 = (i & 1) != 0 ? guildRoleSubscriptionGroupListing.f2032id : j;
        long j5 = (i & 2) != 0 ? guildRoleSubscriptionGroupListing.guildId : j2;
        long j6 = (i & 4) != 0 ? guildRoleSubscriptionGroupListing.applicationId : j3;
        List<Long> list3 = null;
        ImageAsset imageAsset2 = (i & 8) != 0 ? guildRoleSubscriptionGroupListing.imageAsset : null;
        String str2 = (i & 16) != 0 ? guildRoleSubscriptionGroupListing.description : null;
        if ((i & 32) != 0) {
            list3 = guildRoleSubscriptionGroupListing.subscriptionListingsIds;
        }
        List list4 = (i & 64) != 0 ? guildRoleSubscriptionGroupListing.subscriptionListings : list2;
        boolean z3 = (i & 128) != 0 ? guildRoleSubscriptionGroupListing.fullServerGate : z2;
        Objects.requireNonNull(guildRoleSubscriptionGroupListing);
        return new GuildRoleSubscriptionGroupListing(j4, j5, j6, imageAsset2, str2, list3, list4, z3);
    }

    public final long b() {
        return this.applicationId;
    }

    public final String c() {
        return this.description;
    }

    public final boolean d() {
        return this.fullServerGate;
    }

    public final long e() {
        return this.guildId;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildRoleSubscriptionGroupListing)) {
            return false;
        }
        GuildRoleSubscriptionGroupListing guildRoleSubscriptionGroupListing = (GuildRoleSubscriptionGroupListing) obj;
        return this.f2032id == guildRoleSubscriptionGroupListing.f2032id && this.guildId == guildRoleSubscriptionGroupListing.guildId && this.applicationId == guildRoleSubscriptionGroupListing.applicationId && m.areEqual(this.imageAsset, guildRoleSubscriptionGroupListing.imageAsset) && m.areEqual(this.description, guildRoleSubscriptionGroupListing.description) && m.areEqual(this.subscriptionListingsIds, guildRoleSubscriptionGroupListing.subscriptionListingsIds) && m.areEqual(this.subscriptionListings, guildRoleSubscriptionGroupListing.subscriptionListings) && this.fullServerGate == guildRoleSubscriptionGroupListing.fullServerGate;
    }

    public final long f() {
        return this.f2032id;
    }

    public final ImageAsset g() {
        return this.imageAsset;
    }

    public final List<GuildRoleSubscriptionTierListing> h() {
        return this.subscriptionListings;
    }

    public int hashCode() {
        long j = this.f2032id;
        long j2 = this.guildId;
        long j3 = this.applicationId;
        int i = ((((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31;
        ImageAsset imageAsset = this.imageAsset;
        int i2 = 0;
        int hashCode = (i + (imageAsset != null ? imageAsset.hashCode() : 0)) * 31;
        String str = this.description;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        List<Long> list = this.subscriptionListingsIds;
        int hashCode3 = (hashCode2 + (list != null ? list.hashCode() : 0)) * 31;
        List<GuildRoleSubscriptionTierListing> list2 = this.subscriptionListings;
        if (list2 != null) {
            i2 = list2.hashCode();
        }
        int i3 = (hashCode3 + i2) * 31;
        boolean z2 = this.fullServerGate;
        if (z2) {
            z2 = true;
        }
        int i4 = z2 ? 1 : 0;
        int i5 = z2 ? 1 : 0;
        return i3 + i4;
    }

    public final List<Long> i() {
        return this.subscriptionListingsIds;
    }

    public String toString() {
        StringBuilder R = a.R("GuildRoleSubscriptionGroupListing(id=");
        R.append(this.f2032id);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", applicationId=");
        R.append(this.applicationId);
        R.append(", imageAsset=");
        R.append(this.imageAsset);
        R.append(", description=");
        R.append(this.description);
        R.append(", subscriptionListingsIds=");
        R.append(this.subscriptionListingsIds);
        R.append(", subscriptionListings=");
        R.append(this.subscriptionListings);
        R.append(", fullServerGate=");
        return a.M(R, this.fullServerGate, ")");
    }
}
