package com.discord.api.guildrolesubscription;

import andhook.lib.HookHelper;
import com.discord.widgets.chat.AutocompleteSelectionTypes;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: GuildRoleSubscriptionBenefitType.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\b\n\u0002\b\u000b\b\u0086\u0001\u0018\u0000 \t2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\tB\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\nj\u0002\b\u000bj\u0002\b\f¨\u0006\r"}, d2 = {"Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType;", "", "", "apiValue", "I", "getApiValue", "()I", HookHelper.constructorName, "(Ljava/lang/String;II)V", "Companion", "UNKNOWN", AutocompleteSelectionTypes.CHANNEL, "INTANGIBLE", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public enum GuildRoleSubscriptionBenefitType {
    UNKNOWN(0),
    CHANNEL(1),
    INTANGIBLE(2);
    
    public static final Companion Companion = new Companion(null);
    private final int apiValue;

    /* compiled from: GuildRoleSubscriptionBenefitType.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionBenefitType$Companion;", "", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        public Companion() {
        }

        public Companion(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    GuildRoleSubscriptionBenefitType(int i) {
        this.apiValue = i;
    }

    public final int getApiValue() {
        return this.apiValue;
    }
}
