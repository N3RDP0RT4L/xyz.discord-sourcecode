package com.discord.api.guildrolesubscription;

import andhook.lib.HookHelper;
import com.discord.api.guildrolesubscription.PayoutGroupType;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
/* compiled from: PayoutGroupType.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/api/guildrolesubscription/PayoutGroupTypeAdapter;", "Lcom/google/gson/TypeAdapter;", "Lcom/discord/api/guildrolesubscription/PayoutGroupType;", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class PayoutGroupTypeAdapter extends TypeAdapter<PayoutGroupType> {
    @Override // com.google.gson.TypeAdapter
    public PayoutGroupType read(JsonReader jsonReader) {
        m.checkNotNullParameter(jsonReader, "in");
        PayoutGroupType.Companion companion = PayoutGroupType.Companion;
        String J = jsonReader.J();
        m.checkNotNullExpressionValue(J, "`in`.nextString()");
        Objects.requireNonNull(companion);
        m.checkNotNullParameter(J, "apiValue");
        PayoutGroupType payoutGroupType = PayoutGroupType.MARKETPLACE;
        if (m.areEqual(J, payoutGroupType.getApiValue())) {
            return payoutGroupType;
        }
        PayoutGroupType payoutGroupType2 = PayoutGroupType.STAGE_EVENT_SKU;
        if (m.areEqual(J, payoutGroupType2.getApiValue())) {
            return payoutGroupType2;
        }
        PayoutGroupType payoutGroupType3 = PayoutGroupType.SERVER_ROLE_SUBSCRIPTION;
        return m.areEqual(J, payoutGroupType3.getApiValue()) ? payoutGroupType3 : PayoutGroupType.UNKNOWN;
    }

    @Override // com.google.gson.TypeAdapter
    public void write(JsonWriter jsonWriter, PayoutGroupType payoutGroupType) {
        PayoutGroupType payoutGroupType2 = payoutGroupType;
        m.checkNotNullParameter(jsonWriter, "out");
        if (payoutGroupType2 != null) {
            jsonWriter.H(payoutGroupType2.getApiValue());
        } else {
            jsonWriter.s();
        }
    }
}
