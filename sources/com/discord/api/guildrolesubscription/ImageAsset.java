package com.discord.api.guildrolesubscription;

import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ImageAsset.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0010\t\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u0004R\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0010\u001a\u0004\b\u0014\u0010\u0012R\u0019\u0010\u0016\u001a\u00020\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u0019\u0010\u001a\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\r\u001a\u0004\b\u001b\u0010\u0004R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u0010\u001a\u0004\b\u001d\u0010\u0012¨\u0006\u001e"}, d2 = {"Lcom/discord/api/guildrolesubscription/ImageAsset;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "filename", "Ljava/lang/String;", "getFilename", "size", "Ljava/lang/Integer;", "getSize", "()Ljava/lang/Integer;", "height", "getHeight", "", ModelAuditLogEntry.CHANGE_KEY_ID, "J", "a", "()J", "mimeType", "getMimeType", "width", "getWidth", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ImageAsset {
    private final String filename;
    private final Integer height;

    /* renamed from: id  reason: collision with root package name */
    private final long f2034id;
    private final String mimeType;
    private final Integer size;
    private final Integer width;

    public final long a() {
        return this.f2034id;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ImageAsset)) {
            return false;
        }
        ImageAsset imageAsset = (ImageAsset) obj;
        return this.f2034id == imageAsset.f2034id && m.areEqual(this.size, imageAsset.size) && m.areEqual(this.mimeType, imageAsset.mimeType) && m.areEqual(this.filename, imageAsset.filename) && m.areEqual(this.width, imageAsset.width) && m.areEqual(this.height, imageAsset.height);
    }

    public int hashCode() {
        long j = this.f2034id;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        Integer num = this.size;
        int i2 = 0;
        int hashCode = (i + (num != null ? num.hashCode() : 0)) * 31;
        String str = this.mimeType;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.filename;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        Integer num2 = this.width;
        int hashCode4 = (hashCode3 + (num2 != null ? num2.hashCode() : 0)) * 31;
        Integer num3 = this.height;
        if (num3 != null) {
            i2 = num3.hashCode();
        }
        return hashCode4 + i2;
    }

    public String toString() {
        StringBuilder R = a.R("ImageAsset(id=");
        R.append(this.f2034id);
        R.append(", size=");
        R.append(this.size);
        R.append(", mimeType=");
        R.append(this.mimeType);
        R.append(", filename=");
        R.append(this.filename);
        R.append(", width=");
        R.append(this.width);
        R.append(", height=");
        return a.E(R, this.height, ")");
    }
}
