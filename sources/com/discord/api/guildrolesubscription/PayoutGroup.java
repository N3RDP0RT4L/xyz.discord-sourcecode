package com.discord.api.guildrolesubscription;

import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: PayoutGroup.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\r\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0011\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0004R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018R\u0019\u0010\u0019\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u000e\u001a\u0004\b\u001a\u0010\u0010R\u0019\u0010\u001b\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u000e\u001a\u0004\b\u001c\u0010\u0010R\u001d\u0010\u001e\u001a\u00060\fj\u0002`\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u000e\u001a\u0004\b\u001f\u0010\u0010R\u001d\u0010!\u001a\u00060\fj\u0002` 8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u000e\u001a\u0004\b\"\u0010\u0010R\u0019\u0010$\u001a\u00020#8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'R\u0019\u0010(\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010\u0012\u001a\u0004\b)\u0010\u0004R\u0019\u0010*\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010\u000e\u001a\u0004\b+\u0010\u0010R\u0019\u0010,\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010\u000e\u001a\u0004\b-\u0010\u0010R\u0019\u0010/\u001a\u00020.8\u0006@\u0006¢\u0006\f\n\u0004\b/\u00100\u001a\u0004\b1\u00102R\u0019\u00103\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010\u000e\u001a\u0004\b4\u0010\u0010R\u0019\u00105\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010\u000e\u001a\u0004\b6\u0010\u0010R\u0019\u00107\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010\u0012\u001a\u0004\b8\u0010\u0004R\u0019\u00109\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010\u000e\u001a\u0004\b:\u0010\u0010¨\u0006;"}, d2 = {"Lcom/discord/api/guildrolesubscription/PayoutGroup;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", ModelAuditLogEntry.CHANGE_KEY_ID, "J", "getId", "()J", "periodStartingAt", "Ljava/lang/String;", "e", "Lcom/discord/api/guildrolesubscription/Payout;", "payout", "Lcom/discord/api/guildrolesubscription/Payout;", "d", "()Lcom/discord/api/guildrolesubscription/Payout;", "amount", "getAmount", "amountDeducted", "getAmountDeducted", "Lcom/discord/primitives/UserId;", "userId", "getUserId", "Lcom/discord/primitives/ApplicationId;", "applicationId", "getApplicationId", "Lcom/discord/api/guildrolesubscription/PayoutGroupType;", "groupingType", "Lcom/discord/api/guildrolesubscription/PayoutGroupType;", "getGroupingType", "()Lcom/discord/api/guildrolesubscription/PayoutGroupType;", "periodEndingAt", "getPeriodEndingAt", "groupingId", "b", "deductionsCount", "getDeductionsCount", "Lcom/discord/api/guildrolesubscription/PayoutGroupStatus;", "status", "Lcom/discord/api/guildrolesubscription/PayoutGroupStatus;", "f", "()Lcom/discord/api/guildrolesubscription/PayoutGroupStatus;", "payoutId", "getPayoutId", "paymentsCount", "c", "currency", "getCurrency", "amountPayable", "a", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class PayoutGroup {
    private final long amount;
    private final long amountDeducted;
    private final long amountPayable;
    private final long applicationId;
    private final String currency;
    private final long deductionsCount;
    private final long groupingId;
    private final PayoutGroupType groupingType;

    /* renamed from: id  reason: collision with root package name */
    private final long f2036id;
    private final long paymentsCount;
    private final Payout payout;
    private final long payoutId;
    private final String periodEndingAt;
    private final String periodStartingAt;
    private final PayoutGroupStatus status;
    private final long userId;

    public final long a() {
        return this.amountPayable;
    }

    public final long b() {
        return this.groupingId;
    }

    public final long c() {
        return this.paymentsCount;
    }

    public final Payout d() {
        return this.payout;
    }

    public final String e() {
        return this.periodStartingAt;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PayoutGroup)) {
            return false;
        }
        PayoutGroup payoutGroup = (PayoutGroup) obj;
        return this.f2036id == payoutGroup.f2036id && this.payoutId == payoutGroup.payoutId && this.userId == payoutGroup.userId && this.applicationId == payoutGroup.applicationId && this.groupingId == payoutGroup.groupingId && m.areEqual(this.status, payoutGroup.status) && this.amount == payoutGroup.amount && this.amountPayable == payoutGroup.amountPayable && this.amountDeducted == payoutGroup.amountDeducted && this.paymentsCount == payoutGroup.paymentsCount && this.deductionsCount == payoutGroup.deductionsCount && m.areEqual(this.currency, payoutGroup.currency) && m.areEqual(this.groupingType, payoutGroup.groupingType) && m.areEqual(this.periodStartingAt, payoutGroup.periodStartingAt) && m.areEqual(this.periodEndingAt, payoutGroup.periodEndingAt) && m.areEqual(this.payout, payoutGroup.payout);
    }

    public final PayoutGroupStatus f() {
        return this.status;
    }

    public int hashCode() {
        long j = this.f2036id;
        long j2 = this.payoutId;
        long j3 = this.userId;
        long j4 = this.applicationId;
        long j5 = this.groupingId;
        int i = ((((((((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + ((int) (j4 ^ (j4 >>> 32)))) * 31) + ((int) (j5 ^ (j5 >>> 32)))) * 31;
        PayoutGroupStatus payoutGroupStatus = this.status;
        int i2 = 0;
        int hashCode = payoutGroupStatus != null ? payoutGroupStatus.hashCode() : 0;
        long j6 = this.amount;
        long j7 = this.amountPayable;
        long j8 = this.amountDeducted;
        long j9 = this.paymentsCount;
        long j10 = this.deductionsCount;
        int i3 = (((((((((((i + hashCode) * 31) + ((int) (j6 ^ (j6 >>> 32)))) * 31) + ((int) (j7 ^ (j7 >>> 32)))) * 31) + ((int) (j8 ^ (j8 >>> 32)))) * 31) + ((int) (j9 ^ (j9 >>> 32)))) * 31) + ((int) (j10 ^ (j10 >>> 32)))) * 31;
        String str = this.currency;
        int hashCode2 = (i3 + (str != null ? str.hashCode() : 0)) * 31;
        PayoutGroupType payoutGroupType = this.groupingType;
        int hashCode3 = (hashCode2 + (payoutGroupType != null ? payoutGroupType.hashCode() : 0)) * 31;
        String str2 = this.periodStartingAt;
        int hashCode4 = (hashCode3 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.periodEndingAt;
        int hashCode5 = (hashCode4 + (str3 != null ? str3.hashCode() : 0)) * 31;
        Payout payout = this.payout;
        if (payout != null) {
            i2 = payout.hashCode();
        }
        return hashCode5 + i2;
    }

    public String toString() {
        StringBuilder R = a.R("PayoutGroup(id=");
        R.append(this.f2036id);
        R.append(", payoutId=");
        R.append(this.payoutId);
        R.append(", userId=");
        R.append(this.userId);
        R.append(", applicationId=");
        R.append(this.applicationId);
        R.append(", groupingId=");
        R.append(this.groupingId);
        R.append(", status=");
        R.append(this.status);
        R.append(", amount=");
        R.append(this.amount);
        R.append(", amountPayable=");
        R.append(this.amountPayable);
        R.append(", amountDeducted=");
        R.append(this.amountDeducted);
        R.append(", paymentsCount=");
        R.append(this.paymentsCount);
        R.append(", deductionsCount=");
        R.append(this.deductionsCount);
        R.append(", currency=");
        R.append(this.currency);
        R.append(", groupingType=");
        R.append(this.groupingType);
        R.append(", periodStartingAt=");
        R.append(this.periodStartingAt);
        R.append(", periodEndingAt=");
        R.append(this.periodEndingAt);
        R.append(", payout=");
        R.append(this.payout);
        R.append(")");
        return R.toString();
    }
}
