package com.discord.api.guildrolesubscription;

import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: PayoutGroup.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u0004R\u0019\u0010\u0010\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R\u0019\u0010\u0014\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\r\u001a\u0004\b\u0015\u0010\u0004R\u0019\u0010\u0017\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u0019\u0010\u001f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010\r\u001a\u0004\b \u0010\u0004R\u001d\u0010\"\u001a\u00060\u000fj\u0002`!8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u0011\u001a\u0004\b#\u0010\u0013¨\u0006$"}, d2 = {"Lcom/discord/api/guildrolesubscription/Payout;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "periodStart", "Ljava/lang/String;", "getPeriodStart", "", ModelAuditLogEntry.CHANGE_KEY_ID, "J", "getId", "()J", "periodEnd", "getPeriodEnd", "Lcom/discord/api/guildrolesubscription/PayoutStatus;", "status", "Lcom/discord/api/guildrolesubscription/PayoutStatus;", "a", "()Lcom/discord/api/guildrolesubscription/PayoutStatus;", "amount", "Ljava/lang/Long;", "getAmount", "()Ljava/lang/Long;", "payoutDate", "getPayoutDate", "Lcom/discord/primitives/UserId;", "userId", "getUserId", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class Payout {
    private final Long amount;

    /* renamed from: id  reason: collision with root package name */
    private final long f2035id;
    private final String payoutDate;
    private final String periodEnd;
    private final String periodStart;
    private final PayoutStatus status;
    private final long userId;

    public final PayoutStatus a() {
        return this.status;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Payout)) {
            return false;
        }
        Payout payout = (Payout) obj;
        return this.f2035id == payout.f2035id && this.userId == payout.userId && m.areEqual(this.periodStart, payout.periodStart) && m.areEqual(this.periodEnd, payout.periodEnd) && m.areEqual(this.payoutDate, payout.payoutDate) && m.areEqual(this.status, payout.status) && m.areEqual(this.amount, payout.amount);
    }

    public int hashCode() {
        long j = this.f2035id;
        long j2 = this.userId;
        int i = ((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31;
        String str = this.periodStart;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.periodEnd;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.payoutDate;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        PayoutStatus payoutStatus = this.status;
        int hashCode4 = (hashCode3 + (payoutStatus != null ? payoutStatus.hashCode() : 0)) * 31;
        Long l = this.amount;
        if (l != null) {
            i2 = l.hashCode();
        }
        return hashCode4 + i2;
    }

    public String toString() {
        StringBuilder R = a.R("Payout(id=");
        R.append(this.f2035id);
        R.append(", userId=");
        R.append(this.userId);
        R.append(", periodStart=");
        R.append(this.periodStart);
        R.append(", periodEnd=");
        R.append(this.periodEnd);
        R.append(", payoutDate=");
        R.append(this.payoutDate);
        R.append(", status=");
        R.append(this.status);
        R.append(", amount=");
        return a.F(R, this.amount, ")");
    }
}
