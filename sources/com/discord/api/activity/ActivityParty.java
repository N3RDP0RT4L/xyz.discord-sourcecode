package com.discord.api.activity;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: ActivityParty.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B!\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0002\u0012\u000e\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000f¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u0004R!\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014¨\u0006\u0017"}, d2 = {"Lcom/discord/api/activity/ActivityParty;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", ModelAuditLogEntry.CHANGE_KEY_ID, "Ljava/lang/String;", "a", "", "", "size", "Ljava/util/List;", "b", "()Ljava/util/List;", HookHelper.constructorName, "(Ljava/lang/String;Ljava/util/List;)V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ActivityParty {

    /* renamed from: id  reason: collision with root package name */
    private final String f2012id;
    private final List<Long> size;

    public ActivityParty(String str, List<Long> list) {
        this.f2012id = str;
        this.size = list;
    }

    public final String a() {
        return this.f2012id;
    }

    public final List<Long> b() {
        return this.size;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ActivityParty)) {
            return false;
        }
        ActivityParty activityParty = (ActivityParty) obj;
        return m.areEqual(this.f2012id, activityParty.f2012id) && m.areEqual(this.size, activityParty.size);
    }

    public int hashCode() {
        String str = this.f2012id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        List<Long> list = this.size;
        if (list != null) {
            i = list.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder R = a.R("ActivityParty(id=");
        R.append(this.f2012id);
        R.append(", size=");
        return a.K(R, this.size, ")");
    }
}
