package com.discord.api.activity;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: Activity.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001BÁ\u0001\u0012\u0006\u00108\u001a\u00020\u0002\u0012\u0006\u0010/\u001a\u00020.\u0012\b\u0010\u0018\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010:\u001a\u00020)\u0012\b\u0010C\u001a\u0004\u0018\u00010B\u0012\b\u0010*\u001a\u0004\u0018\u00010)\u0012\b\u0010>\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0015\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\r\u001a\u0004\u0018\u00010\f\u0012\b\u0010\u001b\u001a\u0004\u0018\u00010\u001a\u0012\b\u0010 \u001a\u0004\u0018\u00010\u001f\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010G\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010@\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010L\u001a\u0004\u0018\u00010K\u0012\b\u0010%\u001a\u0004\u0018\u00010$\u0012\u000e\u00104\u001a\n\u0012\u0004\u0012\u00020$\u0018\u000103\u0012\u000e\u0010I\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u000103¢\u0006\u0004\bP\u0010QJ\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\r\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0016\u001a\u0004\b\u0019\u0010\u0004R\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u001b\u0010 \u001a\u0004\u0018\u00010\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#R\u001b\u0010%\u001a\u0004\u0018\u00010$8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010&\u001a\u0004\b'\u0010(R\u001b\u0010*\u001a\u0004\u0018\u00010)8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010+\u001a\u0004\b,\u0010-R\u0019\u0010/\u001a\u00020.8\u0006@\u0006¢\u0006\f\n\u0004\b/\u00100\u001a\u0004\b1\u00102R!\u00104\u001a\n\u0012\u0004\u0012\u00020$\u0018\u0001038\u0006@\u0006¢\u0006\f\n\u0004\b4\u00105\u001a\u0004\b6\u00107R\u0019\u00108\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010\u0016\u001a\u0004\b9\u0010\u0004R\u0019\u0010:\u001a\u00020)8\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010;\u001a\u0004\b<\u0010=R\u001b\u0010>\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b>\u0010\u0016\u001a\u0004\b?\u0010\u0004R\u001b\u0010@\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b@\u0010\u0016\u001a\u0004\bA\u0010\u0004R\u001b\u0010C\u001a\u0004\u0018\u00010B8\u0006@\u0006¢\u0006\f\n\u0004\bC\u0010D\u001a\u0004\bE\u0010FR\u001b\u0010G\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\bG\u0010\u0016\u001a\u0004\bH\u0010\u0004R!\u0010I\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u0001038\u0006@\u0006¢\u0006\f\n\u0004\bI\u00105\u001a\u0004\bJ\u00107R\u001b\u0010L\u001a\u0004\u0018\u00010K8\u0006@\u0006¢\u0006\f\n\u0004\bL\u0010M\u001a\u0004\bN\u0010O¨\u0006R"}, d2 = {"Lcom/discord/api/activity/Activity;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/activity/ActivityEmoji;", "emoji", "Lcom/discord/api/activity/ActivityEmoji;", "f", "()Lcom/discord/api/activity/ActivityEmoji;", "flags", "Ljava/lang/Integer;", "g", "()Ljava/lang/Integer;", "state", "Ljava/lang/String;", "l", "url", "getUrl", "Lcom/discord/api/activity/ActivityParty;", "party", "Lcom/discord/api/activity/ActivityParty;", "i", "()Lcom/discord/api/activity/ActivityParty;", "Lcom/discord/api/activity/ActivityAssets;", "assets", "Lcom/discord/api/activity/ActivityAssets;", "b", "()Lcom/discord/api/activity/ActivityAssets;", "Lcom/discord/api/activity/ActivityPlatform;", "platform", "Lcom/discord/api/activity/ActivityPlatform;", "j", "()Lcom/discord/api/activity/ActivityPlatform;", "", "applicationId", "Ljava/lang/Long;", "a", "()Ljava/lang/Long;", "Lcom/discord/api/activity/ActivityType;", "type", "Lcom/discord/api/activity/ActivityType;", "p", "()Lcom/discord/api/activity/ActivityType;", "", "supportedPlatforms", "Ljava/util/List;", "m", "()Ljava/util/List;", ModelAuditLogEntry.CHANGE_KEY_NAME, "h", "createdAt", "J", "d", "()J", "details", "e", "sessionId", "k", "Lcom/discord/api/activity/ActivityTimestamps;", "timestamps", "Lcom/discord/api/activity/ActivityTimestamps;", "o", "()Lcom/discord/api/activity/ActivityTimestamps;", "syncId", "n", "buttons", "c", "Lcom/discord/api/activity/ActivityMetadata;", "metadata", "Lcom/discord/api/activity/ActivityMetadata;", "getMetadata", "()Lcom/discord/api/activity/ActivityMetadata;", HookHelper.constructorName, "(Ljava/lang/String;Lcom/discord/api/activity/ActivityType;Ljava/lang/String;JLcom/discord/api/activity/ActivityTimestamps;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/discord/api/activity/ActivityEmoji;Lcom/discord/api/activity/ActivityParty;Lcom/discord/api/activity/ActivityAssets;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/discord/api/activity/ActivityMetadata;Lcom/discord/api/activity/ActivityPlatform;Ljava/util/List;Ljava/util/List;)V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class Activity {
    private final Long applicationId;
    private final ActivityAssets assets;
    private final long createdAt;
    private final String details;
    private final ActivityEmoji emoji;
    private final Integer flags;
    private final String name;
    private final ActivityParty party;
    private final ActivityPlatform platform;
    private final String state;
    private final String syncId;
    private final ActivityTimestamps timestamps;
    private final ActivityType type;
    private final String url = null;
    private final String sessionId = null;
    private final ActivityMetadata metadata = null;
    private final List<ActivityPlatform> supportedPlatforms = null;
    private final List<String> buttons = null;

    public Activity(String str, ActivityType activityType, String str2, long j, ActivityTimestamps activityTimestamps, Long l, String str3, String str4, ActivityEmoji activityEmoji, ActivityParty activityParty, ActivityAssets activityAssets, Integer num, String str5, String str6, ActivityMetadata activityMetadata, ActivityPlatform activityPlatform, List<? extends ActivityPlatform> list, List<String> list2) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(activityType, "type");
        this.name = str;
        this.type = activityType;
        this.createdAt = j;
        this.timestamps = activityTimestamps;
        this.applicationId = l;
        this.details = str3;
        this.state = str4;
        this.emoji = activityEmoji;
        this.party = activityParty;
        this.assets = activityAssets;
        this.flags = num;
        this.syncId = str5;
        this.platform = activityPlatform;
    }

    public final Long a() {
        return this.applicationId;
    }

    public final ActivityAssets b() {
        return this.assets;
    }

    public final List<String> c() {
        return this.buttons;
    }

    public final long d() {
        return this.createdAt;
    }

    public final String e() {
        return this.details;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Activity)) {
            return false;
        }
        Activity activity = (Activity) obj;
        return m.areEqual(this.name, activity.name) && m.areEqual(this.type, activity.type) && m.areEqual(this.url, activity.url) && this.createdAt == activity.createdAt && m.areEqual(this.timestamps, activity.timestamps) && m.areEqual(this.applicationId, activity.applicationId) && m.areEqual(this.details, activity.details) && m.areEqual(this.state, activity.state) && m.areEqual(this.emoji, activity.emoji) && m.areEqual(this.party, activity.party) && m.areEqual(this.assets, activity.assets) && m.areEqual(this.flags, activity.flags) && m.areEqual(this.syncId, activity.syncId) && m.areEqual(this.sessionId, activity.sessionId) && m.areEqual(this.metadata, activity.metadata) && m.areEqual(this.platform, activity.platform) && m.areEqual(this.supportedPlatforms, activity.supportedPlatforms) && m.areEqual(this.buttons, activity.buttons);
    }

    public final ActivityEmoji f() {
        return this.emoji;
    }

    public final Integer g() {
        return this.flags;
    }

    public final String h() {
        return this.name;
    }

    public int hashCode() {
        String str = this.name;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        ActivityType activityType = this.type;
        int hashCode2 = (hashCode + (activityType != null ? activityType.hashCode() : 0)) * 31;
        String str2 = this.url;
        int hashCode3 = str2 != null ? str2.hashCode() : 0;
        long j = this.createdAt;
        int i2 = (((hashCode2 + hashCode3) * 31) + ((int) (j ^ (j >>> 32)))) * 31;
        ActivityTimestamps activityTimestamps = this.timestamps;
        int hashCode4 = (i2 + (activityTimestamps != null ? activityTimestamps.hashCode() : 0)) * 31;
        Long l = this.applicationId;
        int hashCode5 = (hashCode4 + (l != null ? l.hashCode() : 0)) * 31;
        String str3 = this.details;
        int hashCode6 = (hashCode5 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.state;
        int hashCode7 = (hashCode6 + (str4 != null ? str4.hashCode() : 0)) * 31;
        ActivityEmoji activityEmoji = this.emoji;
        int hashCode8 = (hashCode7 + (activityEmoji != null ? activityEmoji.hashCode() : 0)) * 31;
        ActivityParty activityParty = this.party;
        int hashCode9 = (hashCode8 + (activityParty != null ? activityParty.hashCode() : 0)) * 31;
        ActivityAssets activityAssets = this.assets;
        int hashCode10 = (hashCode9 + (activityAssets != null ? activityAssets.hashCode() : 0)) * 31;
        Integer num = this.flags;
        int hashCode11 = (hashCode10 + (num != null ? num.hashCode() : 0)) * 31;
        String str5 = this.syncId;
        int hashCode12 = (hashCode11 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.sessionId;
        int hashCode13 = (hashCode12 + (str6 != null ? str6.hashCode() : 0)) * 31;
        ActivityMetadata activityMetadata = this.metadata;
        int hashCode14 = (hashCode13 + (activityMetadata != null ? activityMetadata.hashCode() : 0)) * 31;
        ActivityPlatform activityPlatform = this.platform;
        int hashCode15 = (hashCode14 + (activityPlatform != null ? activityPlatform.hashCode() : 0)) * 31;
        List<ActivityPlatform> list = this.supportedPlatforms;
        int hashCode16 = (hashCode15 + (list != null ? list.hashCode() : 0)) * 31;
        List<String> list2 = this.buttons;
        if (list2 != null) {
            i = list2.hashCode();
        }
        return hashCode16 + i;
    }

    public final ActivityParty i() {
        return this.party;
    }

    public final ActivityPlatform j() {
        return this.platform;
    }

    public final String k() {
        return this.sessionId;
    }

    public final String l() {
        return this.state;
    }

    public final List<ActivityPlatform> m() {
        return this.supportedPlatforms;
    }

    public final String n() {
        return this.syncId;
    }

    public final ActivityTimestamps o() {
        return this.timestamps;
    }

    public final ActivityType p() {
        return this.type;
    }

    public String toString() {
        StringBuilder R = a.R("Activity(name=");
        R.append(this.name);
        R.append(", type=");
        R.append(this.type);
        R.append(", url=");
        R.append(this.url);
        R.append(", createdAt=");
        R.append(this.createdAt);
        R.append(", timestamps=");
        R.append(this.timestamps);
        R.append(", applicationId=");
        R.append(this.applicationId);
        R.append(", details=");
        R.append(this.details);
        R.append(", state=");
        R.append(this.state);
        R.append(", emoji=");
        R.append(this.emoji);
        R.append(", party=");
        R.append(this.party);
        R.append(", assets=");
        R.append(this.assets);
        R.append(", flags=");
        R.append(this.flags);
        R.append(", syncId=");
        R.append(this.syncId);
        R.append(", sessionId=");
        R.append(this.sessionId);
        R.append(", metadata=");
        R.append(this.metadata);
        R.append(", platform=");
        R.append(this.platform);
        R.append(", supportedPlatforms=");
        R.append(this.supportedPlatforms);
        R.append(", buttons=");
        return a.K(R, this.buttons, ")");
    }
}
