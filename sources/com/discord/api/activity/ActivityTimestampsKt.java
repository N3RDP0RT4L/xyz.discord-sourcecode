package com.discord.api.activity;

import kotlin.Metadata;
/* compiled from: ActivityTimestamps.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\b\n\u0002\b\u0003\"\u0016\u0010\u0001\u001a\u00020\u00008\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0001\u0010\u0002¨\u0006\u0003"}, d2 = {"", "CURRENT_EPOCH_MILLISECONDS_LENGTH", "I", "discord_api"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ActivityTimestampsKt {
    private static final int CURRENT_EPOCH_MILLISECONDS_LENGTH = 13;
}
