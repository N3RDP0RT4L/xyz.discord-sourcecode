package com.discord.api.activity;

import andhook.lib.HookHelper;
import b.c.a.a0.d;
import com.discord.api.activity.ActivityType;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
/* compiled from: ActivityType.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/api/activity/ActivityTypeTypeAdapter;", "Lcom/google/gson/TypeAdapter;", "Lcom/discord/api/activity/ActivityType;", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ActivityTypeTypeAdapter extends TypeAdapter<ActivityType> {
    @Override // com.google.gson.TypeAdapter
    public ActivityType read(JsonReader jsonReader) {
        m.checkNotNullParameter(jsonReader, "in");
        ActivityType.Companion companion = ActivityType.Companion;
        Integer n1 = d.n1(jsonReader);
        Objects.requireNonNull(companion);
        ActivityType activityType = ActivityType.PLAYING;
        int apiInt$discord_api = activityType.getApiInt$discord_api();
        if (n1 != null && n1.intValue() == apiInt$discord_api) {
            return activityType;
        }
        ActivityType activityType2 = ActivityType.STREAMING;
        int apiInt$discord_api2 = activityType2.getApiInt$discord_api();
        if (n1 != null && n1.intValue() == apiInt$discord_api2) {
            return activityType2;
        }
        ActivityType activityType3 = ActivityType.LISTENING;
        int apiInt$discord_api3 = activityType3.getApiInt$discord_api();
        if (n1 != null && n1.intValue() == apiInt$discord_api3) {
            return activityType3;
        }
        ActivityType activityType4 = ActivityType.WATCHING;
        int apiInt$discord_api4 = activityType4.getApiInt$discord_api();
        if (n1 != null && n1.intValue() == apiInt$discord_api4) {
            return activityType4;
        }
        ActivityType activityType5 = ActivityType.CUSTOM_STATUS;
        int apiInt$discord_api5 = activityType5.getApiInt$discord_api();
        if (n1 != null && n1.intValue() == apiInt$discord_api5) {
            return activityType5;
        }
        ActivityType activityType6 = ActivityType.COMPETING;
        return (n1 != null && n1.intValue() == activityType6.getApiInt$discord_api()) ? activityType6 : ActivityType.UNKNOWN;
    }

    @Override // com.google.gson.TypeAdapter
    public void write(JsonWriter jsonWriter, ActivityType activityType) {
        ActivityType activityType2 = activityType;
        m.checkNotNullParameter(jsonWriter, "out");
        if (activityType2 != null) {
            jsonWriter.D(Integer.valueOf(activityType2.getApiInt$discord_api()));
        } else {
            jsonWriter.s();
        }
    }
}
