package com.discord.api.activity;

import andhook.lib.HookHelper;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import d0.z.d.m;
import java.util.Locale;
import kotlin.Metadata;
/* compiled from: ActivityPlatform.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/api/activity/ActivityPlatformTypeAdapter;", "Lcom/google/gson/TypeAdapter;", "Lcom/discord/api/activity/ActivityPlatform;", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ActivityPlatformTypeAdapter extends TypeAdapter<ActivityPlatform> {
    @Override // com.google.gson.TypeAdapter
    public ActivityPlatform read(JsonReader jsonReader) {
        m.checkNotNullParameter(jsonReader, "in");
        String J = jsonReader.J();
        if (J != null) {
            switch (J.hashCode()) {
                case -1998723398:
                    if (J.equals("spotify")) {
                        return ActivityPlatform.SPOTIFY;
                    }
                    break;
                case -861391249:
                    if (J.equals("android")) {
                        return ActivityPlatform.ANDROID;
                    }
                    break;
                case 104461:
                    if (J.equals("ios")) {
                        return ActivityPlatform.IOS;
                    }
                    break;
                case 111249:
                    if (J.equals("ps4")) {
                        return ActivityPlatform.PS4;
                    }
                    break;
                case 111250:
                    if (J.equals("ps5")) {
                        return ActivityPlatform.PS5;
                    }
                    break;
                case 3672659:
                    if (J.equals("xbox")) {
                        return ActivityPlatform.XBOX;
                    }
                    break;
                case 1557106716:
                    if (J.equals("desktop")) {
                        return ActivityPlatform.DESKTOP;
                    }
                    break;
                case 1864941562:
                    if (J.equals("samsung")) {
                        return ActivityPlatform.SAMSUNG;
                    }
                    break;
            }
        }
        return ActivityPlatform.UNKNOWN;
    }

    @Override // com.google.gson.TypeAdapter
    public void write(JsonWriter jsonWriter, ActivityPlatform activityPlatform) {
        String str;
        String name;
        ActivityPlatform activityPlatform2 = activityPlatform;
        m.checkNotNullParameter(jsonWriter, "out");
        if (activityPlatform2 == null || (name = activityPlatform2.name()) == null) {
            str = null;
        } else {
            Locale locale = Locale.ROOT;
            m.checkNotNullExpressionValue(locale, "Locale.ROOT");
            str = name.toLowerCase(locale);
            m.checkNotNullExpressionValue(str, "(this as java.lang.String).toLowerCase(locale)");
        }
        jsonWriter.H(str);
    }
}
