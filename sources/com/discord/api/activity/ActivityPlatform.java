package com.discord.api.activity;

import andhook.lib.HookHelper;
import kotlin.Metadata;
/* compiled from: ActivityPlatform.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\f\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\f¨\u0006\r"}, d2 = {"Lcom/discord/api/activity/ActivityPlatform;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "DESKTOP", "XBOX", "ANDROID", "IOS", "SAMSUNG", "SPOTIFY", "PS4", "PS5", "UNKNOWN", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public enum ActivityPlatform {
    DESKTOP,
    XBOX,
    ANDROID,
    IOS,
    SAMSUNG,
    SPOTIFY,
    PS4,
    PS5,
    UNKNOWN
}
