package com.discord.api.application;

import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel;
import d0.g0.w;
import d0.t.n;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
/* compiled from: Application.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0013\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00020\u0005¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000e\u001a\u00020\r2\b\u0010\f\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u000e\u0010\u000fR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004R!\u0010\u0015\u001a\n\u0018\u00010\u0013j\u0004\u0018\u0001`\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00198\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u001dR\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u0011\u001a\u0004\b\u001f\u0010\u0004R\u0019\u0010 \u001a\u00020\u00138\u0006@\u0006¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#R\u001b\u0010$\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u0011\u001a\u0004\b%\u0010\u0004R!\u0010'\u001a\n\u0012\u0004\u0012\u00020&\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b)\u0010\u0007R\u0019\u0010*\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010\u0011\u001a\u0004\b+\u0010\u0004R\u001b\u0010-\u001a\u0004\u0018\u00010,8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010.\u001a\u0004\b/\u00100R\u001b\u00101\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010\u0011\u001a\u0004\b2\u0010\u0004R\u001b\u00103\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010\u0011\u001a\u0004\b4\u0010\u0004¨\u00065"}, d2 = {"Lcom/discord/api/application/Application;", "", "", "a", "()Ljava/lang/String;", "", "d", "()Ljava/util/List;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "coverImage", "Ljava/lang/String;", "b", "", "Lcom/discord/primitives/GuildId;", "guildId", "Ljava/lang/Long;", "e", "()Ljava/lang/Long;", "Lcom/discord/api/application/ApplicationType;", "type", "Lcom/discord/api/application/ApplicationType;", "k", "()Lcom/discord/api/application/ApplicationType;", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "c", ModelAuditLogEntry.CHANGE_KEY_ID, "J", "g", "()J", "icon", "f", "Lcom/discord/api/application/ThirdPartySku;", "thirdPartySkus", "Ljava/util/List;", "getThirdPartySkus", ModelAuditLogEntry.CHANGE_KEY_NAME, "h", "Lcom/discord/api/application/Team;", "team", "Lcom/discord/api/application/Team;", "j", "()Lcom/discord/api/application/Team;", "deeplinkUri", "getDeeplinkUri", "splash", "i", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class Application {
    private final String coverImage;
    private final String deeplinkUri;
    private final String description;
    private final Long guildId;
    private final String icon;

    /* renamed from: id  reason: collision with root package name */
    private final long f2013id;
    private final String name;
    private final String splash;
    private final Team team;
    private final List<ThirdPartySku> thirdPartySkus;
    private final ApplicationType type;

    public final String a() {
        String removeSuffix;
        String v;
        String str = this.deeplinkUri;
        return (str == null || (removeSuffix = w.removeSuffix(str, AutocompleteViewModel.COMMAND_DISCOVER_TOKEN)) == null || (v = a.v(removeSuffix, "/_discord")) == null) ? a.B(a.R("dscd"), this.f2013id, "://connect/_discord") : v;
    }

    public final String b() {
        return this.coverImage;
    }

    public final String c() {
        return this.description;
    }

    public final List<String> d() {
        List<ThirdPartySku> list = this.thirdPartySkus;
        if (list == null) {
            return n.emptyList();
        }
        ArrayList<ThirdPartySku> arrayList = new ArrayList();
        for (Object obj : list) {
            if (m.areEqual(((ThirdPartySku) obj).a(), "google_play")) {
                arrayList.add(obj);
            }
        }
        ArrayList arrayList2 = new ArrayList();
        for (ThirdPartySku thirdPartySku : arrayList) {
            String b2 = thirdPartySku.b();
            if (b2 != null) {
                arrayList2.add(b2);
            }
        }
        return arrayList2;
    }

    public final Long e() {
        return this.guildId;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Application)) {
            return false;
        }
        Application application = (Application) obj;
        return this.f2013id == application.f2013id && m.areEqual(this.name, application.name) && m.areEqual(this.description, application.description) && m.areEqual(this.splash, application.splash) && m.areEqual(this.coverImage, application.coverImage) && m.areEqual(this.icon, application.icon) && m.areEqual(this.thirdPartySkus, application.thirdPartySkus) && m.areEqual(this.deeplinkUri, application.deeplinkUri) && m.areEqual(this.type, application.type) && m.areEqual(this.guildId, application.guildId) && m.areEqual(this.team, application.team);
    }

    public final String f() {
        return this.icon;
    }

    public final long g() {
        return this.f2013id;
    }

    public final String h() {
        return this.name;
    }

    public int hashCode() {
        long j = this.f2013id;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        String str = this.name;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.description;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.splash;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.coverImage;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.icon;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        List<ThirdPartySku> list = this.thirdPartySkus;
        int hashCode6 = (hashCode5 + (list != null ? list.hashCode() : 0)) * 31;
        String str6 = this.deeplinkUri;
        int hashCode7 = (hashCode6 + (str6 != null ? str6.hashCode() : 0)) * 31;
        ApplicationType applicationType = this.type;
        int hashCode8 = (hashCode7 + (applicationType != null ? applicationType.hashCode() : 0)) * 31;
        Long l = this.guildId;
        int hashCode9 = (hashCode8 + (l != null ? l.hashCode() : 0)) * 31;
        Team team = this.team;
        if (team != null) {
            i2 = team.hashCode();
        }
        return hashCode9 + i2;
    }

    public final String i() {
        return this.splash;
    }

    public final Team j() {
        return this.team;
    }

    public final ApplicationType k() {
        return this.type;
    }

    public String toString() {
        StringBuilder R = a.R("Application(id=");
        R.append(this.f2013id);
        R.append(", name=");
        R.append(this.name);
        R.append(", description=");
        R.append(this.description);
        R.append(", splash=");
        R.append(this.splash);
        R.append(", coverImage=");
        R.append(this.coverImage);
        R.append(", icon=");
        R.append(this.icon);
        R.append(", thirdPartySkus=");
        R.append(this.thirdPartySkus);
        R.append(", deeplinkUri=");
        R.append(this.deeplinkUri);
        R.append(", type=");
        R.append(this.type);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", team=");
        R.append(this.team);
        R.append(")");
        return R.toString();
    }
}
