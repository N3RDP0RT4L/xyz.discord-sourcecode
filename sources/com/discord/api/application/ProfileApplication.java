package com.discord.api.application;

import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ProfileApplication.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u0004R\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R\u0019\u0010\u0014\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\r\u001a\u0004\b\u0015\u0010\u0004¨\u0006\u0016"}, d2 = {"Lcom/discord/api/application/ProfileApplication;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "customInstallUrl", "Ljava/lang/String;", "a", "Lcom/discord/api/application/ApplicationInstallParams;", "installParams", "Lcom/discord/api/application/ApplicationInstallParams;", "c", "()Lcom/discord/api/application/ApplicationInstallParams;", ModelAuditLogEntry.CHANGE_KEY_ID, "b", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ProfileApplication {
    private final String customInstallUrl;

    /* renamed from: id  reason: collision with root package name */
    private final String f2015id;
    private final ApplicationInstallParams installParams;

    public final String a() {
        return this.customInstallUrl;
    }

    public final String b() {
        return this.f2015id;
    }

    public final ApplicationInstallParams c() {
        return this.installParams;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ProfileApplication)) {
            return false;
        }
        ProfileApplication profileApplication = (ProfileApplication) obj;
        return m.areEqual(this.f2015id, profileApplication.f2015id) && m.areEqual(this.customInstallUrl, profileApplication.customInstallUrl) && m.areEqual(this.installParams, profileApplication.installParams);
    }

    public int hashCode() {
        String str = this.f2015id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.customInstallUrl;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        ApplicationInstallParams applicationInstallParams = this.installParams;
        if (applicationInstallParams != null) {
            i = applicationInstallParams.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("ProfileApplication(id=");
        R.append(this.f2015id);
        R.append(", customInstallUrl=");
        R.append(this.customInstallUrl);
        R.append(", installParams=");
        R.append(this.installParams);
        R.append(")");
        return R.toString();
    }
}
