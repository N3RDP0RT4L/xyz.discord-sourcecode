package com.discord.api.application;

import kotlin.Metadata;
/* compiled from: Application.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000e\n\u0002\b\u0003\"\u0016\u0010\u0001\u001a\u00020\u00008\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0001\u0010\u0002¨\u0006\u0003"}, d2 = {"", "DISTRIBUTOR_GPLAY", "Ljava/lang/String;", "discord_api"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ApplicationKt {
    private static final String DISTRIBUTOR_GPLAY = "google_play";
}
