package com.discord.api.commands;

import andhook.lib.HookHelper;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ApplicationCommandPermission.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/api/commands/ApplicationCommandPermissionTypeTypeAdapter;", "Lcom/google/gson/TypeAdapter;", "Lcom/discord/api/commands/ApplicationCommandPermissionType;", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ApplicationCommandPermissionTypeTypeAdapter extends TypeAdapter<ApplicationCommandPermissionType> {

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            ApplicationCommandPermissionType.values();
            int[] iArr = new int[3];
            $EnumSwitchMapping$0 = iArr;
            iArr[ApplicationCommandPermissionType.ROLE.ordinal()] = 1;
            iArr[ApplicationCommandPermissionType.USER.ordinal()] = 2;
        }
    }

    @Override // com.google.gson.TypeAdapter
    public ApplicationCommandPermissionType read(JsonReader jsonReader) {
        m.checkNotNullParameter(jsonReader, "in");
        int y2 = jsonReader.y();
        if (y2 == 1) {
            return ApplicationCommandPermissionType.ROLE;
        }
        if (y2 != 2) {
            return ApplicationCommandPermissionType.UNKNOWN;
        }
        return ApplicationCommandPermissionType.USER;
    }

    /* JADX WARN: Code restructure failed: missing block: B:6:0x0013, code lost:
        if (r4 != 2) goto L7;
     */
    @Override // com.google.gson.TypeAdapter
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void write(com.google.gson.stream.JsonWriter r3, com.discord.api.commands.ApplicationCommandPermissionType r4) {
        /*
            r2 = this;
            com.discord.api.commands.ApplicationCommandPermissionType r4 = (com.discord.api.commands.ApplicationCommandPermissionType) r4
            java.lang.String r0 = "out"
            d0.z.d.m.checkNotNullParameter(r3, r0)
            r0 = 2
            r1 = 1
            if (r4 != 0) goto Ld
            goto L15
        Ld:
            int r4 = r4.ordinal()
            if (r4 == r1) goto L17
            if (r4 == r0) goto L18
        L15:
            r0 = 0
            goto L18
        L17:
            r0 = 1
        L18:
            java.lang.Integer r4 = java.lang.Integer.valueOf(r0)
            r3.D(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.api.commands.ApplicationCommandPermissionTypeTypeAdapter.write(com.google.gson.stream.JsonWriter, java.lang.Object):void");
    }
}
