package com.discord.api.commands;

import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ApplicationCommandPermission.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\r\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0012\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u0019\u0010\u0016\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019¨\u0006\u001a"}, d2 = {"Lcom/discord/api/commands/ApplicationCommandPermission;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/commands/ApplicationCommandPermissionType;", "type", "Lcom/discord/api/commands/ApplicationCommandPermissionType;", "getType", "()Lcom/discord/api/commands/ApplicationCommandPermissionType;", "", ModelAuditLogEntry.CHANGE_KEY_ID, "J", "a", "()J", "permission", "Z", "b", "()Z", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ApplicationCommandPermission {

    /* renamed from: id  reason: collision with root package name */
    private final long f2024id;
    private final boolean permission;
    private final ApplicationCommandPermissionType type;

    public final long a() {
        return this.f2024id;
    }

    public final boolean b() {
        return this.permission;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ApplicationCommandPermission)) {
            return false;
        }
        ApplicationCommandPermission applicationCommandPermission = (ApplicationCommandPermission) obj;
        return this.f2024id == applicationCommandPermission.f2024id && m.areEqual(this.type, applicationCommandPermission.type) && this.permission == applicationCommandPermission.permission;
    }

    public int hashCode() {
        long j = this.f2024id;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        ApplicationCommandPermissionType applicationCommandPermissionType = this.type;
        int hashCode = (i + (applicationCommandPermissionType != null ? applicationCommandPermissionType.hashCode() : 0)) * 31;
        boolean z2 = this.permission;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        return hashCode + i2;
    }

    public String toString() {
        StringBuilder R = a.R("ApplicationCommandPermission(id=");
        R.append(this.f2024id);
        R.append(", type=");
        R.append(this.type);
        R.append(", permission=");
        return a.M(R, this.permission, ")");
    }
}
