package com.discord.api.commands;

import b.d.b.a.a;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: ApplicationCommandAutocompleteResult.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0012\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/api/commands/ApplicationCommandAutocompleteResult;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "Lcom/discord/api/commands/ApplicationCommandAutocompleteChoice;", "choices", "Ljava/util/List;", "a", "()Ljava/util/List;", "nonce", "Ljava/lang/String;", "b", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ApplicationCommandAutocompleteResult {
    private final List<ApplicationCommandAutocompleteChoice> choices;
    private final String nonce;

    public final List<ApplicationCommandAutocompleteChoice> a() {
        return this.choices;
    }

    public final String b() {
        return this.nonce;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ApplicationCommandAutocompleteResult)) {
            return false;
        }
        ApplicationCommandAutocompleteResult applicationCommandAutocompleteResult = (ApplicationCommandAutocompleteResult) obj;
        return m.areEqual(this.nonce, applicationCommandAutocompleteResult.nonce) && m.areEqual(this.choices, applicationCommandAutocompleteResult.choices);
    }

    public int hashCode() {
        String str = this.nonce;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        List<ApplicationCommandAutocompleteChoice> list = this.choices;
        if (list != null) {
            i = list.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder R = a.R("ApplicationCommandAutocompleteResult(nonce=");
        R.append(this.nonce);
        R.append(", choices=");
        return a.K(R, this.choices, ")");
    }
}
