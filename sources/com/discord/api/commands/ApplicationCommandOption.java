package com.discord.api.commands;

import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: ApplicationCommandOption.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u0004\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u0004R\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\r\u001a\u0004\b\u0010\u0010\u0004R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R!\u0010\u0017\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u0019\u0010\u001b\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u001f\u0010 \u001a\b\u0012\u0004\u0012\u00020\u001f0\u00168\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0018\u001a\u0004\b!\u0010\u001aR!\u0010\"\u001a\n\u0012\u0004\u0012\u00020\u0000\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u0018\u001a\u0004\b#\u0010\u001aR\u0019\u0010$\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u001c\u001a\u0004\b%\u0010\u001eR\u0019\u0010&\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010\u001c\u001a\u0004\b'\u0010\u001eR\u001b\u0010(\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010\u0013\u001a\u0004\b)\u0010\u0015R\u0019\u0010+\u001a\u00020*8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010,\u001a\u0004\b-\u0010.¨\u0006/"}, d2 = {"Lcom/discord/api/commands/ApplicationCommandOption;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", ModelAuditLogEntry.CHANGE_KEY_NAME, "Ljava/lang/String;", "h", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "e", "", "minValue", "Ljava/lang/Number;", "g", "()Ljava/lang/Number;", "", "channelTypes", "Ljava/util/List;", "b", "()Ljava/util/List;", "required", "Z", "j", "()Z", "Lcom/discord/api/commands/CommandChoice;", "choices", "c", "options", "i", "default", "d", "autocomplete", "a", "maxValue", "f", "Lcom/discord/api/commands/ApplicationCommandType;", "type", "Lcom/discord/api/commands/ApplicationCommandType;", "k", "()Lcom/discord/api/commands/ApplicationCommandType;", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ApplicationCommandOption {
    private final boolean autocomplete;
    private final List<Integer> channelTypes;
    private final List<CommandChoice> choices;

    /* renamed from: default  reason: not valid java name */
    private final boolean f3default;
    private final String description;
    private final Number maxValue;
    private final Number minValue;
    private final String name;
    private final List<ApplicationCommandOption> options;
    private final boolean required;
    private final ApplicationCommandType type;

    public final boolean a() {
        return this.autocomplete;
    }

    public final List<Integer> b() {
        return this.channelTypes;
    }

    public final List<CommandChoice> c() {
        return this.choices;
    }

    public final boolean d() {
        return this.f3default;
    }

    public final String e() {
        return this.description;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ApplicationCommandOption)) {
            return false;
        }
        ApplicationCommandOption applicationCommandOption = (ApplicationCommandOption) obj;
        return m.areEqual(this.type, applicationCommandOption.type) && m.areEqual(this.name, applicationCommandOption.name) && m.areEqual(this.description, applicationCommandOption.description) && this.required == applicationCommandOption.required && this.f3default == applicationCommandOption.f3default && m.areEqual(this.channelTypes, applicationCommandOption.channelTypes) && m.areEqual(this.choices, applicationCommandOption.choices) && m.areEqual(this.options, applicationCommandOption.options) && this.autocomplete == applicationCommandOption.autocomplete && m.areEqual(this.minValue, applicationCommandOption.minValue) && m.areEqual(this.maxValue, applicationCommandOption.maxValue);
    }

    public final Number f() {
        return this.maxValue;
    }

    public final Number g() {
        return this.minValue;
    }

    public final String h() {
        return this.name;
    }

    public int hashCode() {
        ApplicationCommandType applicationCommandType = this.type;
        int i = 0;
        int hashCode = (applicationCommandType != null ? applicationCommandType.hashCode() : 0) * 31;
        String str = this.name;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.description;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        boolean z2 = this.required;
        int i2 = 1;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        int i5 = (hashCode3 + i3) * 31;
        boolean z3 = this.f3default;
        if (z3) {
            z3 = true;
        }
        int i6 = z3 ? 1 : 0;
        int i7 = z3 ? 1 : 0;
        int i8 = (i5 + i6) * 31;
        List<Integer> list = this.channelTypes;
        int hashCode4 = (i8 + (list != null ? list.hashCode() : 0)) * 31;
        List<CommandChoice> list2 = this.choices;
        int hashCode5 = (hashCode4 + (list2 != null ? list2.hashCode() : 0)) * 31;
        List<ApplicationCommandOption> list3 = this.options;
        int hashCode6 = (hashCode5 + (list3 != null ? list3.hashCode() : 0)) * 31;
        boolean z4 = this.autocomplete;
        if (!z4) {
            i2 = z4 ? 1 : 0;
        }
        int i9 = (hashCode6 + i2) * 31;
        Number number = this.minValue;
        int hashCode7 = (i9 + (number != null ? number.hashCode() : 0)) * 31;
        Number number2 = this.maxValue;
        if (number2 != null) {
            i = number2.hashCode();
        }
        return hashCode7 + i;
    }

    public final List<ApplicationCommandOption> i() {
        return this.options;
    }

    public final boolean j() {
        return this.required;
    }

    public final ApplicationCommandType k() {
        return this.type;
    }

    public String toString() {
        StringBuilder R = a.R("ApplicationCommandOption(type=");
        R.append(this.type);
        R.append(", name=");
        R.append(this.name);
        R.append(", description=");
        R.append(this.description);
        R.append(", required=");
        R.append(this.required);
        R.append(", default=");
        R.append(this.f3default);
        R.append(", channelTypes=");
        R.append(this.channelTypes);
        R.append(", choices=");
        R.append(this.choices);
        R.append(", options=");
        R.append(this.options);
        R.append(", autocomplete=");
        R.append(this.autocomplete);
        R.append(", minValue=");
        R.append(this.minValue);
        R.append(", maxValue=");
        R.append(this.maxValue);
        R.append(")");
        return R.toString();
    }
}
