package com.discord.api.commands;

import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: ApplicationCommand.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u0004R!\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\r\u001a\u0004\b\u0016\u0010\u0004R\u0019\u0010\u0018\u001a\u00020\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u001b\u0010\u001c\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001fR\u001b\u0010 \u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\r\u001a\u0004\b!\u0010\u0004R\u0019\u0010\"\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\r\u001a\u0004\b#\u0010\u0004R!\u0010%\u001a\n\u0012\u0004\u0012\u00020$\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u0012\u001a\u0004\b&\u0010\u0014R\u0019\u0010'\u001a\u00020\u00178\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010\u0019\u001a\u0004\b(\u0010\u001b¨\u0006)"}, d2 = {"Lcom/discord/api/commands/ApplicationCommand;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "version", "Ljava/lang/String;", "i", "", "Lcom/discord/api/commands/ApplicationCommandPermission;", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS, "Ljava/util/List;", "h", "()Ljava/util/List;", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "c", "", ModelAuditLogEntry.CHANGE_KEY_ID, "J", "e", "()J", "defaultPermissions", "Ljava/lang/Boolean;", "b", "()Ljava/lang/Boolean;", "guildId", "d", ModelAuditLogEntry.CHANGE_KEY_NAME, "f", "Lcom/discord/api/commands/ApplicationCommandOption;", "options", "g", "applicationId", "a", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ApplicationCommand {
    private final long applicationId;
    private final Boolean defaultPermissions;
    private final String description;
    private final String guildId;

    /* renamed from: id  reason: collision with root package name */
    private final long f2022id;
    private final String name;
    private final List<ApplicationCommandOption> options;
    private final List<ApplicationCommandPermission> permissions;
    private final String version;

    public final long a() {
        return this.applicationId;
    }

    public final Boolean b() {
        return this.defaultPermissions;
    }

    public final String c() {
        return this.description;
    }

    public final String d() {
        return this.guildId;
    }

    public final long e() {
        return this.f2022id;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ApplicationCommand)) {
            return false;
        }
        ApplicationCommand applicationCommand = (ApplicationCommand) obj;
        return this.f2022id == applicationCommand.f2022id && this.applicationId == applicationCommand.applicationId && m.areEqual(this.name, applicationCommand.name) && m.areEqual(this.description, applicationCommand.description) && m.areEqual(this.options, applicationCommand.options) && m.areEqual(this.version, applicationCommand.version) && m.areEqual(this.guildId, applicationCommand.guildId) && m.areEqual(this.defaultPermissions, applicationCommand.defaultPermissions) && m.areEqual(this.permissions, applicationCommand.permissions);
    }

    public final String f() {
        return this.name;
    }

    public final List<ApplicationCommandOption> g() {
        return this.options;
    }

    public final List<ApplicationCommandPermission> h() {
        return this.permissions;
    }

    public int hashCode() {
        long j = this.f2022id;
        long j2 = this.applicationId;
        int i = ((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31;
        String str = this.name;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.description;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        List<ApplicationCommandOption> list = this.options;
        int hashCode3 = (hashCode2 + (list != null ? list.hashCode() : 0)) * 31;
        String str3 = this.version;
        int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.guildId;
        int hashCode5 = (hashCode4 + (str4 != null ? str4.hashCode() : 0)) * 31;
        Boolean bool = this.defaultPermissions;
        int hashCode6 = (hashCode5 + (bool != null ? bool.hashCode() : 0)) * 31;
        List<ApplicationCommandPermission> list2 = this.permissions;
        if (list2 != null) {
            i2 = list2.hashCode();
        }
        return hashCode6 + i2;
    }

    public final String i() {
        return this.version;
    }

    public String toString() {
        StringBuilder R = a.R("ApplicationCommand(id=");
        R.append(this.f2022id);
        R.append(", applicationId=");
        R.append(this.applicationId);
        R.append(", name=");
        R.append(this.name);
        R.append(", description=");
        R.append(this.description);
        R.append(", options=");
        R.append(this.options);
        R.append(", version=");
        R.append(this.version);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", defaultPermissions=");
        R.append(this.defaultPermissions);
        R.append(", permissions=");
        return a.K(R, this.permissions, ")");
    }
}
