package com.discord.api.commands;

import andhook.lib.HookHelper;
import com.discord.api.commands.ApplicationCommandType;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
/* compiled from: ApplicationCommandType.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/api/commands/CommandTypeAdapter;", "Lcom/google/gson/TypeAdapter;", "Lcom/discord/api/commands/ApplicationCommandType;", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class CommandTypeAdapter extends TypeAdapter<ApplicationCommandType> {
    @Override // com.google.gson.TypeAdapter
    public ApplicationCommandType read(JsonReader jsonReader) {
        m.checkNotNullParameter(jsonReader, "in");
        ApplicationCommandType.Companion companion = ApplicationCommandType.Companion;
        int y2 = jsonReader.y();
        Objects.requireNonNull(companion);
        switch (y2) {
            case 1:
                return ApplicationCommandType.SUBCOMMAND;
            case 2:
                return ApplicationCommandType.SUBCOMMAND_GROUP;
            case 3:
                return ApplicationCommandType.STRING;
            case 4:
                return ApplicationCommandType.INTEGER;
            case 5:
                return ApplicationCommandType.BOOLEAN;
            case 6:
                return ApplicationCommandType.USER;
            case 7:
                return ApplicationCommandType.CHANNEL;
            case 8:
                return ApplicationCommandType.ROLE;
            case 9:
                return ApplicationCommandType.MENTIONABLE;
            case 10:
                return ApplicationCommandType.NUMBER;
            case 11:
                return ApplicationCommandType.ATTACHMENT;
            default:
                return ApplicationCommandType.UNKNOWN;
        }
    }

    @Override // com.google.gson.TypeAdapter
    public void write(JsonWriter jsonWriter, ApplicationCommandType applicationCommandType) {
        ApplicationCommandType applicationCommandType2 = applicationCommandType;
        m.checkNotNullParameter(jsonWriter, "out");
        if (applicationCommandType2 != null) {
            jsonWriter.D(Integer.valueOf(applicationCommandType2.getType()));
        } else {
            jsonWriter.s();
        }
    }
}
