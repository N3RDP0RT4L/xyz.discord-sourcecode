package com.discord.api.friendsuggestions;

import andhook.lib.HookHelper;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: AllowedInSuggestionsType.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/api/friendsuggestions/AllowedInSuggestionsTypeAdapter;", "Lcom/google/gson/TypeAdapter;", "Lcom/discord/api/friendsuggestions/AllowedInSuggestionsType;", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AllowedInSuggestionsTypeAdapter extends TypeAdapter<AllowedInSuggestionsType> {
    @Override // com.google.gson.TypeAdapter
    public AllowedInSuggestionsType read(JsonReader jsonReader) {
        AllowedInSuggestionsType allowedInSuggestionsType;
        m.checkNotNullParameter(jsonReader, "in");
        int y2 = jsonReader.y();
        AllowedInSuggestionsType[] values = AllowedInSuggestionsType.values();
        int i = 0;
        while (true) {
            if (i >= 3) {
                allowedInSuggestionsType = null;
                break;
            }
            allowedInSuggestionsType = values[i];
            if (allowedInSuggestionsType.getApiValue() == y2) {
                break;
            }
            i++;
        }
        return allowedInSuggestionsType != null ? allowedInSuggestionsType : AllowedInSuggestionsType.DISABLED;
    }

    @Override // com.google.gson.TypeAdapter
    public void write(JsonWriter jsonWriter, AllowedInSuggestionsType allowedInSuggestionsType) {
        AllowedInSuggestionsType allowedInSuggestionsType2 = allowedInSuggestionsType;
        m.checkNotNullParameter(jsonWriter, "out");
        if (allowedInSuggestionsType2 != null) {
            jsonWriter.D(Integer.valueOf(allowedInSuggestionsType2.getApiValue()));
        } else {
            jsonWriter.s();
        }
    }
}
