package com.discord.api.friendsuggestions;

import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: FriendSuggestionReason.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\r\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0004R\u0019\u0010\u0014\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0012\u001a\u0004\b\u0015\u0010\u0004¨\u0006\u0016"}, d2 = {"Lcom/discord/api/friendsuggestions/FriendSuggestionReason;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/friendsuggestions/FriendSuggestionReasonType;", "type", "Lcom/discord/api/friendsuggestions/FriendSuggestionReasonType;", "getType", "()Lcom/discord/api/friendsuggestions/FriendSuggestionReasonType;", ModelAuditLogEntry.CHANGE_KEY_NAME, "Ljava/lang/String;", "a", "platformType", "b", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class FriendSuggestionReason {
    private final String name;
    private final String platformType;
    private final FriendSuggestionReasonType type;

    public final String a() {
        return this.name;
    }

    public final String b() {
        return this.platformType;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof FriendSuggestionReason)) {
            return false;
        }
        FriendSuggestionReason friendSuggestionReason = (FriendSuggestionReason) obj;
        return m.areEqual(this.type, friendSuggestionReason.type) && m.areEqual(this.platformType, friendSuggestionReason.platformType) && m.areEqual(this.name, friendSuggestionReason.name);
    }

    public int hashCode() {
        FriendSuggestionReasonType friendSuggestionReasonType = this.type;
        int i = 0;
        int hashCode = (friendSuggestionReasonType != null ? friendSuggestionReasonType.hashCode() : 0) * 31;
        String str = this.platformType;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.name;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("FriendSuggestionReason(type=");
        R.append(this.type);
        R.append(", platformType=");
        R.append(this.platformType);
        R.append(", name=");
        return a.H(R, this.name, ")");
    }
}
