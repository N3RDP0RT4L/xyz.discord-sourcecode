package com.discord.api.botuikit;

import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ButtonComponent.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\u000b\u001a\u00020\n2\b\u0010\t\u001a\u0004\u0018\u00010\bHÖ\u0003¢\u0006\u0004\b\u000b\u0010\fR\u001c\u0010\u000e\u001a\u00020\r8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0012\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u0004R\u0019\u0010\u001f\u001a\u00020\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"R\u001b\u0010#\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u001c\u001a\u0004\b$\u0010\u0004R\u001b\u0010%\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u001c\u001a\u0004\b&\u0010\u0004¨\u0006'"}, d2 = {"Lcom/discord/api/botuikit/ButtonComponent;", "Lcom/discord/api/botuikit/ActionComponent;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/botuikit/ComponentType;", "type", "Lcom/discord/api/botuikit/ComponentType;", "getType", "()Lcom/discord/api/botuikit/ComponentType;", "disabled", "Z", "b", "()Z", "Lcom/discord/api/botuikit/ComponentEmoji;", "emoji", "Lcom/discord/api/botuikit/ComponentEmoji;", "c", "()Lcom/discord/api/botuikit/ComponentEmoji;", "label", "Ljava/lang/String;", "d", "Lcom/discord/api/botuikit/ButtonStyle;", "style", "Lcom/discord/api/botuikit/ButtonStyle;", "e", "()Lcom/discord/api/botuikit/ButtonStyle;", "url", "f", "customId", "a", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ButtonComponent extends ActionComponent {
    private final String customId;
    private final boolean disabled;
    private final ComponentEmoji emoji;
    private final String label;
    private final ButtonStyle style;
    private final ComponentType type;
    private final String url;

    public final String a() {
        return this.customId;
    }

    public final boolean b() {
        return this.disabled;
    }

    public final ComponentEmoji c() {
        return this.emoji;
    }

    public final String d() {
        return this.label;
    }

    public final ButtonStyle e() {
        return this.style;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ButtonComponent)) {
            return false;
        }
        ButtonComponent buttonComponent = (ButtonComponent) obj;
        return m.areEqual(this.type, buttonComponent.type) && m.areEqual(this.customId, buttonComponent.customId) && m.areEqual(this.label, buttonComponent.label) && m.areEqual(this.style, buttonComponent.style) && this.disabled == buttonComponent.disabled && m.areEqual(this.emoji, buttonComponent.emoji) && m.areEqual(this.url, buttonComponent.url);
    }

    public final String f() {
        return this.url;
    }

    @Override // com.discord.api.botuikit.Component
    public ComponentType getType() {
        return this.type;
    }

    public int hashCode() {
        ComponentType componentType = this.type;
        int i = 0;
        int hashCode = (componentType != null ? componentType.hashCode() : 0) * 31;
        String str = this.customId;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.label;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        ButtonStyle buttonStyle = this.style;
        int hashCode4 = (hashCode3 + (buttonStyle != null ? buttonStyle.hashCode() : 0)) * 31;
        boolean z2 = this.disabled;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        int i4 = (hashCode4 + i2) * 31;
        ComponentEmoji componentEmoji = this.emoji;
        int hashCode5 = (i4 + (componentEmoji != null ? componentEmoji.hashCode() : 0)) * 31;
        String str3 = this.url;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode5 + i;
    }

    public String toString() {
        StringBuilder R = a.R("ButtonComponent(type=");
        R.append(this.type);
        R.append(", customId=");
        R.append(this.customId);
        R.append(", label=");
        R.append(this.label);
        R.append(", style=");
        R.append(this.style);
        R.append(", disabled=");
        R.append(this.disabled);
        R.append(", emoji=");
        R.append(this.emoji);
        R.append(", url=");
        return a.H(R, this.url, ")");
    }
}
