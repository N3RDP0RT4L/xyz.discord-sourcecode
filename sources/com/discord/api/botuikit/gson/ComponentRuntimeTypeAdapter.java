package com.discord.api.botuikit.gson;

import andhook.lib.HookHelper;
import com.discord.api.botuikit.Component;
import com.discord.api.botuikit.ComponentType;
import com.discord.gsonutils.RuntimeTypeAdapterFactory;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ComponentRuntimeTypeAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tR\u001f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/api/botuikit/gson/ComponentRuntimeTypeAdapter;", "", "Lcom/discord/gsonutils/RuntimeTypeAdapterFactory;", "Lcom/discord/api/botuikit/Component;", "componentRuntimeTypeAdapterFactory", "Lcom/discord/gsonutils/RuntimeTypeAdapterFactory;", "a", "()Lcom/discord/gsonutils/RuntimeTypeAdapterFactory;", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ComponentRuntimeTypeAdapter {
    public static final ComponentRuntimeTypeAdapter INSTANCE = new ComponentRuntimeTypeAdapter();
    private static final RuntimeTypeAdapterFactory<Component> componentRuntimeTypeAdapterFactory;

    static {
        RuntimeTypeAdapterFactory<Component> runtimeTypeAdapterFactory = new RuntimeTypeAdapterFactory<>(Component.class, "type", true);
        ComponentType[] values = ComponentType.values();
        for (int i = 0; i < 4; i++) {
            ComponentType componentType = values[i];
            Class<? extends Component> clazz = componentType.getClazz();
            String valueOf = String.valueOf(componentType.getType());
            if (clazz == null || valueOf == null) {
                throw null;
            } else if (runtimeTypeAdapterFactory.m.containsKey(clazz) || runtimeTypeAdapterFactory.l.containsKey(valueOf)) {
                throw new IllegalArgumentException("types and labels must be unique");
            } else {
                runtimeTypeAdapterFactory.l.put(valueOf, clazz);
                runtimeTypeAdapterFactory.m.put(clazz, valueOf);
            }
        }
        m.checkNotNullExpressionValue(runtimeTypeAdapterFactory, "RuntimeTypeAdapterFactor…ype.toString())\n    }\n  }");
        componentRuntimeTypeAdapterFactory = runtimeTypeAdapterFactory;
    }

    public final RuntimeTypeAdapterFactory<Component> a() {
        return componentRuntimeTypeAdapterFactory;
    }
}
