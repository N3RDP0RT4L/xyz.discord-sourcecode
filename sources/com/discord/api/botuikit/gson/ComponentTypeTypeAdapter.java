package com.discord.api.botuikit.gson;

import andhook.lib.HookHelper;
import b.c.a.a0.d;
import com.discord.api.botuikit.ComponentType;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ComponentTypeTypeAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/api/botuikit/gson/ComponentTypeTypeAdapter;", "Lcom/google/gson/TypeAdapter;", "Lcom/discord/api/botuikit/ComponentType;", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ComponentTypeTypeAdapter extends TypeAdapter<ComponentType> {
    @Override // com.google.gson.TypeAdapter
    public ComponentType read(JsonReader jsonReader) {
        ComponentType componentType;
        m.checkNotNullParameter(jsonReader, "in");
        Integer n1 = d.n1(jsonReader);
        ComponentType[] values = ComponentType.values();
        int i = 0;
        while (true) {
            if (i >= 4) {
                componentType = null;
                break;
            }
            componentType = values[i];
            if (n1 != null && componentType.getType() == n1.intValue()) {
                break;
            }
            i++;
        }
        return componentType != null ? componentType : ComponentType.UNKNOWN;
    }

    @Override // com.google.gson.TypeAdapter
    public void write(JsonWriter jsonWriter, ComponentType componentType) {
        ComponentType componentType2 = componentType;
        m.checkNotNullParameter(jsonWriter, "out");
        if (componentType2 != null) {
            jsonWriter.D(Integer.valueOf(componentType2.getType()));
        }
    }
}
