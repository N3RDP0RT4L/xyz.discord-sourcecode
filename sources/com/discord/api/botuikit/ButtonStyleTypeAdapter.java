package com.discord.api.botuikit;

import andhook.lib.HookHelper;
import b.c.a.a0.d;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
/* compiled from: ButtonComponent.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/api/botuikit/ButtonStyleTypeAdapter;", "Lcom/google/gson/TypeAdapter;", "Lcom/discord/api/botuikit/ButtonStyle;", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ButtonStyleTypeAdapter extends TypeAdapter<ButtonStyle> {

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            ButtonStyle.values();
            int[] iArr = new int[6];
            $EnumSwitchMapping$0 = iArr;
            iArr[ButtonStyle.PRIMARY.ordinal()] = 1;
            iArr[ButtonStyle.SECONDARY.ordinal()] = 2;
            iArr[ButtonStyle.SUCCESS.ordinal()] = 3;
            iArr[ButtonStyle.DANGER.ordinal()] = 4;
            iArr[ButtonStyle.LINK.ordinal()] = 5;
            iArr[ButtonStyle.UNKNOWN.ordinal()] = 6;
        }
    }

    @Override // com.google.gson.TypeAdapter
    public ButtonStyle read(JsonReader jsonReader) {
        m.checkNotNullParameter(jsonReader, "in");
        Integer n1 = d.n1(jsonReader);
        return (n1 != null && n1.intValue() == 1) ? ButtonStyle.PRIMARY : (n1 != null && n1.intValue() == 2) ? ButtonStyle.SECONDARY : (n1 != null && n1.intValue() == 3) ? ButtonStyle.SUCCESS : (n1 != null && n1.intValue() == 4) ? ButtonStyle.DANGER : (n1 != null && n1.intValue() == 5) ? ButtonStyle.LINK : ButtonStyle.UNKNOWN;
    }

    @Override // com.google.gson.TypeAdapter
    public void write(JsonWriter jsonWriter, ButtonStyle buttonStyle) {
        Integer num;
        ButtonStyle buttonStyle2 = buttonStyle;
        m.checkNotNullParameter(jsonWriter, "out");
        if (buttonStyle2 != null) {
            int ordinal = buttonStyle2.ordinal();
            if (ordinal == 0) {
                num = null;
            } else if (ordinal == 1) {
                num = 1;
            } else if (ordinal == 2) {
                num = 2;
            } else if (ordinal == 3) {
                num = 3;
            } else if (ordinal == 4) {
                num = 4;
            } else if (ordinal == 5) {
                num = 5;
            } else {
                throw new NoWhenBranchMatchedException();
            }
            jsonWriter.D(num);
        }
    }
}
