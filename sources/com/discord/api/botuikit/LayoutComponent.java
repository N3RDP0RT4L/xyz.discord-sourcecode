package com.discord.api.botuikit;

import andhook.lib.HookHelper;
import java.util.List;
import kotlin.Metadata;
/* compiled from: Component.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0005\b&\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u0005\u0010\u0006J\u0015\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00010\u0002H&¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/api/botuikit/LayoutComponent;", "Lcom/discord/api/botuikit/Component;", "", "a", "()Ljava/util/List;", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public abstract class LayoutComponent implements Component {
    public abstract List<Component> a();
}
