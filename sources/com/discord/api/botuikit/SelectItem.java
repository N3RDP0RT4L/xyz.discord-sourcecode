package com.discord.api.botuikit;

import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.io.Serializable;
import kotlin.Metadata;
/* compiled from: SelectComponent.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\u000b\u001a\u00020\n2\b\u0010\t\u001a\u0004\u0018\u00010\bHÖ\u0003¢\u0006\u0004\b\u000b\u0010\fR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0012\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0013\u001a\u0004\b\u0016\u0010\u0004R\u0019\u0010\u0017\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u0019\u0010\u001b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u0013\u001a\u0004\b\u001c\u0010\u0004¨\u0006\u001d"}, d2 = {"Lcom/discord/api/botuikit/SelectItem;", "Ljava/io/Serializable;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/botuikit/ComponentEmoji;", "emoji", "Lcom/discord/api/botuikit/ComponentEmoji;", "c", "()Lcom/discord/api/botuikit/ComponentEmoji;", "value", "Ljava/lang/String;", "e", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "b", "default", "Z", "a", "()Z", "label", "d", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class SelectItem implements Serializable {

    /* renamed from: default  reason: not valid java name */
    private final boolean f2default;
    private final String description;
    private final ComponentEmoji emoji;
    private final String label;
    private final String value;

    public final boolean a() {
        return this.f2default;
    }

    public final String b() {
        return this.description;
    }

    public final ComponentEmoji c() {
        return this.emoji;
    }

    public final String d() {
        return this.label;
    }

    public final String e() {
        return this.value;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SelectItem)) {
            return false;
        }
        SelectItem selectItem = (SelectItem) obj;
        return m.areEqual(this.label, selectItem.label) && m.areEqual(this.value, selectItem.value) && m.areEqual(this.emoji, selectItem.emoji) && m.areEqual(this.description, selectItem.description) && this.f2default == selectItem.f2default;
    }

    public int hashCode() {
        String str = this.label;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.value;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        ComponentEmoji componentEmoji = this.emoji;
        int hashCode3 = (hashCode2 + (componentEmoji != null ? componentEmoji.hashCode() : 0)) * 31;
        String str3 = this.description;
        if (str3 != null) {
            i = str3.hashCode();
        }
        int i2 = (hashCode3 + i) * 31;
        boolean z2 = this.f2default;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        return i2 + i3;
    }

    public String toString() {
        StringBuilder R = a.R("SelectItem(label=");
        R.append(this.label);
        R.append(", value=");
        R.append(this.value);
        R.append(", emoji=");
        R.append(this.emoji);
        R.append(", description=");
        R.append(this.description);
        R.append(", default=");
        return a.M(R, this.f2default, ")");
    }
}
