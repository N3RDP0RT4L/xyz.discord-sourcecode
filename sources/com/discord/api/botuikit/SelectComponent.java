package com.discord.api.botuikit;

import b.d.b.a.a;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: SelectComponent.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000e\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\u000b\u001a\u00020\n2\b\u0010\t\u001a\u0004\u0018\u00010\bHÖ\u0003¢\u0006\u0004\b\u000b\u0010\fR\u0019\u0010\r\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0004R\u0019\u0010\u0014\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0007R\u0019\u0010\u0017\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0015\u001a\u0004\b\u0018\u0010\u0007R\u001f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001a0\u00198\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u001c\u0010 \u001a\u00020\u001f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#R\u0019\u0010$\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u0012\u001a\u0004\b%\u0010\u0004¨\u0006&"}, d2 = {"Lcom/discord/api/botuikit/SelectComponent;", "Lcom/discord/api/botuikit/ActionComponent;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "disabled", "Z", "b", "()Z", "placeholder", "Ljava/lang/String;", "f", "maxValues", "I", "c", "minValues", "d", "", "Lcom/discord/api/botuikit/SelectItem;", "options", "Ljava/util/List;", "e", "()Ljava/util/List;", "Lcom/discord/api/botuikit/ComponentType;", "type", "Lcom/discord/api/botuikit/ComponentType;", "getType", "()Lcom/discord/api/botuikit/ComponentType;", "customId", "a", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class SelectComponent extends ActionComponent {
    private final String customId;
    private final boolean disabled;
    private final int maxValues;
    private final int minValues;
    private final List<SelectItem> options;
    private final String placeholder;
    private final ComponentType type;

    public final String a() {
        return this.customId;
    }

    public final boolean b() {
        return this.disabled;
    }

    public final int c() {
        return this.maxValues;
    }

    public final int d() {
        return this.minValues;
    }

    public final List<SelectItem> e() {
        return this.options;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SelectComponent)) {
            return false;
        }
        SelectComponent selectComponent = (SelectComponent) obj;
        return m.areEqual(this.type, selectComponent.type) && m.areEqual(this.customId, selectComponent.customId) && this.disabled == selectComponent.disabled && m.areEqual(this.placeholder, selectComponent.placeholder) && this.minValues == selectComponent.minValues && this.maxValues == selectComponent.maxValues && m.areEqual(this.options, selectComponent.options);
    }

    public final String f() {
        return this.placeholder;
    }

    @Override // com.discord.api.botuikit.Component
    public ComponentType getType() {
        return this.type;
    }

    public int hashCode() {
        ComponentType componentType = this.type;
        int i = 0;
        int hashCode = (componentType != null ? componentType.hashCode() : 0) * 31;
        String str = this.customId;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        boolean z2 = this.disabled;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        int i4 = (hashCode2 + i2) * 31;
        String str2 = this.placeholder;
        int hashCode3 = (((((i4 + (str2 != null ? str2.hashCode() : 0)) * 31) + this.minValues) * 31) + this.maxValues) * 31;
        List<SelectItem> list = this.options;
        if (list != null) {
            i = list.hashCode();
        }
        return hashCode3 + i;
    }

    public String toString() {
        StringBuilder R = a.R("SelectComponent(type=");
        R.append(this.type);
        R.append(", customId=");
        R.append(this.customId);
        R.append(", disabled=");
        R.append(this.disabled);
        R.append(", placeholder=");
        R.append(this.placeholder);
        R.append(", minValues=");
        R.append(this.minValues);
        R.append(", maxValues=");
        R.append(this.maxValues);
        R.append(", options=");
        return a.K(R, this.options, ")");
    }
}
