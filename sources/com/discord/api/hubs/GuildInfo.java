package com.discord.api.hubs;

import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: EmailVerification.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u0004R\u0019\u0010\u0010\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R\u0019\u0010\u0014\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\r\u001a\u0004\b\u0015\u0010\u0004¨\u0006\u0016"}, d2 = {"Lcom/discord/api/hubs/GuildInfo;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "icon", "Ljava/lang/String;", "a", "", ModelAuditLogEntry.CHANGE_KEY_ID, "J", "b", "()J", ModelAuditLogEntry.CHANGE_KEY_NAME, "c", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildInfo {
    private final String icon;

    /* renamed from: id  reason: collision with root package name */
    private final long f2038id;
    private final String name;

    public final String a() {
        return this.icon;
    }

    public final long b() {
        return this.f2038id;
    }

    public final String c() {
        return this.name;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildInfo)) {
            return false;
        }
        GuildInfo guildInfo = (GuildInfo) obj;
        return m.areEqual(this.icon, guildInfo.icon) && this.f2038id == guildInfo.f2038id && m.areEqual(this.name, guildInfo.name);
    }

    public int hashCode() {
        String str = this.icon;
        int i = 0;
        int hashCode = str != null ? str.hashCode() : 0;
        long j = this.f2038id;
        int i2 = ((hashCode * 31) + ((int) (j ^ (j >>> 32)))) * 31;
        String str2 = this.name;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return i2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("GuildInfo(icon=");
        R.append(this.icon);
        R.append(", id=");
        R.append(this.f2038id);
        R.append(", name=");
        return a.H(R, this.name, ")");
    }
}
