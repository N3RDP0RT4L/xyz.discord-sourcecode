package com.discord.api.hubs;

import b.d.b.a.a;
import com.discord.api.guild.Guild;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: EmailVerification.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\f\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000fR\u0019\u0010\u0011\u001a\u00020\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014¨\u0006\u0015"}, d2 = {"Lcom/discord/api/hubs/EmailVerificationCode;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "joined", "Z", "b", "()Z", "Lcom/discord/api/guild/Guild;", "guild", "Lcom/discord/api/guild/Guild;", "a", "()Lcom/discord/api/guild/Guild;", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class EmailVerificationCode {
    private final Guild guild;
    private final boolean joined;

    public final Guild a() {
        return this.guild;
    }

    public final boolean b() {
        return this.joined;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof EmailVerificationCode)) {
            return false;
        }
        EmailVerificationCode emailVerificationCode = (EmailVerificationCode) obj;
        return m.areEqual(this.guild, emailVerificationCode.guild) && this.joined == emailVerificationCode.joined;
    }

    public int hashCode() {
        Guild guild = this.guild;
        int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
        boolean z2 = this.joined;
        if (z2) {
            z2 = true;
        }
        int i = z2 ? 1 : 0;
        int i2 = z2 ? 1 : 0;
        return hashCode + i;
    }

    public String toString() {
        StringBuilder R = a.R("EmailVerificationCode(guild=");
        R.append(this.guild);
        R.append(", joined=");
        return a.M(R, this.joined, ")");
    }
}
