package com.discord.api.guildmember;

import b.d.b.a.a;
import com.discord.api.presence.Presence;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: GuildMembersChunk.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0013\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R!\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\u0017\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u000f\u001a\u0004\b\u0019\u0010\u0011R!\u0010\u001a\u001a\n\u0012\u0004\u0012\u00020\u0012\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u000f\u001a\u0004\b\u001b\u0010\u0011¨\u0006\u001c"}, d2 = {"Lcom/discord/api/guildmember/GuildMembersChunk;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "Lcom/discord/api/guildmember/GuildMember;", "members", "Ljava/util/List;", "b", "()Ljava/util/List;", "", "guildId", "J", "a", "()J", "Lcom/discord/api/presence/Presence;", "presences", "d", "notFound", "c", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildMembersChunk {
    private final long guildId;
    private final List<GuildMember> members;
    private final List<Long> notFound;
    private final List<Presence> presences;

    public final long a() {
        return this.guildId;
    }

    public final List<GuildMember> b() {
        return this.members;
    }

    public final List<Long> c() {
        return this.notFound;
    }

    public final List<Presence> d() {
        return this.presences;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildMembersChunk)) {
            return false;
        }
        GuildMembersChunk guildMembersChunk = (GuildMembersChunk) obj;
        return this.guildId == guildMembersChunk.guildId && m.areEqual(this.members, guildMembersChunk.members) && m.areEqual(this.presences, guildMembersChunk.presences) && m.areEqual(this.notFound, guildMembersChunk.notFound);
    }

    public int hashCode() {
        long j = this.guildId;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        List<GuildMember> list = this.members;
        int i2 = 0;
        int hashCode = (i + (list != null ? list.hashCode() : 0)) * 31;
        List<Presence> list2 = this.presences;
        int hashCode2 = (hashCode + (list2 != null ? list2.hashCode() : 0)) * 31;
        List<Long> list3 = this.notFound;
        if (list3 != null) {
            i2 = list3.hashCode();
        }
        return hashCode2 + i2;
    }

    public String toString() {
        StringBuilder R = a.R("GuildMembersChunk(guildId=");
        R.append(this.guildId);
        R.append(", members=");
        R.append(this.members);
        R.append(", presences=");
        R.append(this.presences);
        R.append(", notFound=");
        return a.K(R, this.notFound, ")");
    }
}
