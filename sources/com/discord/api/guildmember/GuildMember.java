package com.discord.api.guildmember;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.presence.Presence;
import com.discord.api.user.User;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
/* compiled from: GuildMember.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0013\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B\u008d\u0001\u0012\u0006\u0010\u0016\u001a\u00020\u0011\u0012\u0006\u0010+\u001a\u00020*\u0012\f\u00100\u001a\b\u0012\u0004\u0012\u00020\u00110/\u0012\b\u00104\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u001a\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\r\u001a\u0004\u0018\u00010\f\u0012\u0006\u0010\u001d\u001a\u00020\t\u0012\b\u0010&\u001a\u0004\u0018\u00010%\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0011\u0012\b\u0010!\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u00106\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u00108\u001a\u0004\u0018\u00010\f¢\u0006\u0004\b:\u0010;J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\r\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u0019\u0010\u0016\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u0004R\u0019\u0010\u001d\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 R\u001b\u0010!\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u001b\u001a\u0004\b\"\u0010\u0004R\u001b\u0010#\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\u001b\u001a\u0004\b$\u0010\u0004R\u001b\u0010&\u001a\u0004\u0018\u00010%8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010'\u001a\u0004\b(\u0010)R\u0019\u0010+\u001a\u00020*8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010,\u001a\u0004\b-\u0010.R\u001f\u00100\u001a\b\u0012\u0004\u0012\u00020\u00110/8\u0006@\u0006¢\u0006\f\n\u0004\b0\u00101\u001a\u0004\b2\u00103R\u001b\u00104\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010\u001b\u001a\u0004\b5\u0010\u0004R\u001b\u00106\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010\u001b\u001a\u0004\b7\u0010\u0004R\u001b\u00108\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010\u000e\u001a\u0004\b9\u0010\u0010¨\u0006<"}, d2 = {"Lcom/discord/api/guildmember/GuildMember;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/utcdatetime/UtcDateTime;", "joinedAt", "Lcom/discord/api/utcdatetime/UtcDateTime;", "g", "()Lcom/discord/api/utcdatetime/UtcDateTime;", "", "userId", "Ljava/lang/Long;", "n", "()Ljava/lang/Long;", "guildId", "J", "f", "()J", "premiumSince", "Ljava/lang/String;", "j", "pending", "Z", "i", "()Z", "avatar", "b", "bio", "d", "Lcom/discord/api/presence/Presence;", "presence", "Lcom/discord/api/presence/Presence;", "k", "()Lcom/discord/api/presence/Presence;", "Lcom/discord/api/user/User;", "user", "Lcom/discord/api/user/User;", "m", "()Lcom/discord/api/user/User;", "", "roles", "Ljava/util/List;", "l", "()Ljava/util/List;", ModelAuditLogEntry.CHANGE_KEY_NICK, "h", "banner", "c", "communicationDisabledUntil", "e", HookHelper.constructorName, "(JLcom/discord/api/user/User;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/discord/api/utcdatetime/UtcDateTime;ZLcom/discord/api/presence/Presence;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/discord/api/utcdatetime/UtcDateTime;)V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GuildMember {
    private final String avatar;
    private final String banner;
    private final String bio;
    private final UtcDateTime communicationDisabledUntil;
    private final long guildId;
    private final UtcDateTime joinedAt;
    private final String nick;
    private final boolean pending;
    private final String premiumSince;
    private final Presence presence;
    private final List<Long> roles;
    private final User user;
    private final Long userId;

    public GuildMember(long j, User user, List<Long> list, String str, String str2, UtcDateTime utcDateTime, boolean z2, Presence presence, Long l, String str3, String str4, String str5, UtcDateTime utcDateTime2) {
        m.checkNotNullParameter(user, "user");
        m.checkNotNullParameter(list, "roles");
        this.guildId = j;
        this.user = user;
        this.roles = list;
        this.nick = str;
        this.premiumSince = str2;
        this.joinedAt = utcDateTime;
        this.pending = z2;
        this.presence = presence;
        this.userId = l;
        this.avatar = str3;
        this.bio = str4;
        this.banner = str5;
        this.communicationDisabledUntil = utcDateTime2;
    }

    public static GuildMember a(GuildMember guildMember, long j, User user, List list, String str, String str2, UtcDateTime utcDateTime, boolean z2, Presence presence, Long l, String str3, String str4, String str5, UtcDateTime utcDateTime2, int i) {
        long j2 = (i & 1) != 0 ? guildMember.guildId : j;
        User user2 = (i & 2) != 0 ? guildMember.user : user;
        List<Long> list2 = (i & 4) != 0 ? guildMember.roles : null;
        String str6 = (i & 8) != 0 ? guildMember.nick : null;
        String str7 = (i & 16) != 0 ? guildMember.premiumSince : null;
        UtcDateTime utcDateTime3 = (i & 32) != 0 ? guildMember.joinedAt : null;
        boolean z3 = (i & 64) != 0 ? guildMember.pending : z2;
        Presence presence2 = (i & 128) != 0 ? guildMember.presence : null;
        Long l2 = (i & 256) != 0 ? guildMember.userId : null;
        String str8 = (i & 512) != 0 ? guildMember.avatar : null;
        String str9 = (i & 1024) != 0 ? guildMember.bio : str4;
        String str10 = (i & 2048) != 0 ? guildMember.banner : str5;
        UtcDateTime utcDateTime4 = (i & 4096) != 0 ? guildMember.communicationDisabledUntil : null;
        Objects.requireNonNull(guildMember);
        m.checkNotNullParameter(user2, "user");
        m.checkNotNullParameter(list2, "roles");
        return new GuildMember(j2, user2, list2, str6, str7, utcDateTime3, z3, presence2, l2, str8, str9, str10, utcDateTime4);
    }

    public final String b() {
        return this.avatar;
    }

    public final String c() {
        return this.banner;
    }

    public final String d() {
        return this.bio;
    }

    public final UtcDateTime e() {
        return this.communicationDisabledUntil;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildMember)) {
            return false;
        }
        GuildMember guildMember = (GuildMember) obj;
        return this.guildId == guildMember.guildId && m.areEqual(this.user, guildMember.user) && m.areEqual(this.roles, guildMember.roles) && m.areEqual(this.nick, guildMember.nick) && m.areEqual(this.premiumSince, guildMember.premiumSince) && m.areEqual(this.joinedAt, guildMember.joinedAt) && this.pending == guildMember.pending && m.areEqual(this.presence, guildMember.presence) && m.areEqual(this.userId, guildMember.userId) && m.areEqual(this.avatar, guildMember.avatar) && m.areEqual(this.bio, guildMember.bio) && m.areEqual(this.banner, guildMember.banner) && m.areEqual(this.communicationDisabledUntil, guildMember.communicationDisabledUntil);
    }

    public final long f() {
        return this.guildId;
    }

    public final UtcDateTime g() {
        return this.joinedAt;
    }

    public final String h() {
        return this.nick;
    }

    public int hashCode() {
        long j = this.guildId;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        User user = this.user;
        int i2 = 0;
        int hashCode = (i + (user != null ? user.hashCode() : 0)) * 31;
        List<Long> list = this.roles;
        int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
        String str = this.nick;
        int hashCode3 = (hashCode2 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.premiumSince;
        int hashCode4 = (hashCode3 + (str2 != null ? str2.hashCode() : 0)) * 31;
        UtcDateTime utcDateTime = this.joinedAt;
        int hashCode5 = (hashCode4 + (utcDateTime != null ? utcDateTime.hashCode() : 0)) * 31;
        boolean z2 = this.pending;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        int i5 = (hashCode5 + i3) * 31;
        Presence presence = this.presence;
        int hashCode6 = (i5 + (presence != null ? presence.hashCode() : 0)) * 31;
        Long l = this.userId;
        int hashCode7 = (hashCode6 + (l != null ? l.hashCode() : 0)) * 31;
        String str3 = this.avatar;
        int hashCode8 = (hashCode7 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.bio;
        int hashCode9 = (hashCode8 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.banner;
        int hashCode10 = (hashCode9 + (str5 != null ? str5.hashCode() : 0)) * 31;
        UtcDateTime utcDateTime2 = this.communicationDisabledUntil;
        if (utcDateTime2 != null) {
            i2 = utcDateTime2.hashCode();
        }
        return hashCode10 + i2;
    }

    public final boolean i() {
        return this.pending;
    }

    public final String j() {
        return this.premiumSince;
    }

    public final Presence k() {
        return this.presence;
    }

    public final List<Long> l() {
        return this.roles;
    }

    public final User m() {
        return this.user;
    }

    public final Long n() {
        return this.userId;
    }

    public String toString() {
        StringBuilder R = a.R("GuildMember(guildId=");
        R.append(this.guildId);
        R.append(", user=");
        R.append(this.user);
        R.append(", roles=");
        R.append(this.roles);
        R.append(", nick=");
        R.append(this.nick);
        R.append(", premiumSince=");
        R.append(this.premiumSince);
        R.append(", joinedAt=");
        R.append(this.joinedAt);
        R.append(", pending=");
        R.append(this.pending);
        R.append(", presence=");
        R.append(this.presence);
        R.append(", userId=");
        R.append(this.userId);
        R.append(", avatar=");
        R.append(this.avatar);
        R.append(", bio=");
        R.append(this.bio);
        R.append(", banner=");
        R.append(this.banner);
        R.append(", communicationDisabledUntil=");
        R.append(this.communicationDisabledUntil);
        R.append(")");
        return R.toString();
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ GuildMember(long j, User user, List list, String str, String str2, UtcDateTime utcDateTime, boolean z2, Presence presence, Long l, String str3, String str4, String str5, UtcDateTime utcDateTime2, int i) {
        this(j, user, list, str, str2, utcDateTime, z2, null, l, str3, null, null, (i & 4096) != 0 ? null : utcDateTime2);
        int i2 = i & 1024;
        int i3 = i & 2048;
    }
}
