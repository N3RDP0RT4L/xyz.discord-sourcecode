package com.discord.workers;

import andhook.lib.HookHelper;
import android.app.Application;
import android.content.Context;
import androidx.work.ListenableWorker;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import com.discord.stores.StoreStream;
import d0.z.d.m;
import j0.k.b;
import kotlin.Metadata;
/* compiled from: BackgroundMessageSendWorker.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u000b"}, d2 = {"Lcom/discord/workers/BackgroundMessageSendWorker;", "Landroidx/work/Worker;", "Landroidx/work/ListenableWorker$Result;", "doWork", "()Landroidx/work/ListenableWorker$Result;", "Landroid/content/Context;", "context", "Landroidx/work/WorkerParameters;", "params", HookHelper.constructorName, "(Landroid/content/Context;Landroidx/work/WorkerParameters;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class BackgroundMessageSendWorker extends Worker {

    /* compiled from: BackgroundMessageSendWorker.kt */
    /* loaded from: classes2.dex */
    public static final class a<T, R> implements b<Boolean, Boolean> {
        public static final a j = new a();

        @Override // j0.k.b
        public Boolean call(Boolean bool) {
            return Boolean.valueOf(m.areEqual(bool, Boolean.TRUE));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public BackgroundMessageSendWorker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(workerParameters, "params");
    }

    @Override // androidx.work.Worker
    public ListenableWorker.Result doWork() {
        Context applicationContext = getApplicationContext();
        if (!(applicationContext instanceof Application)) {
            applicationContext = null;
        }
        Application application = (Application) applicationContext;
        if (application != null) {
            StoreStream.Companion companion = StoreStream.Companion;
            companion.initialize(application);
            new j0.m.a(companion.getMessages().observeInitResendFinished().x(a.j).Z(1)).b();
            ListenableWorker.Result success = ListenableWorker.Result.success();
            m.checkNotNullExpressionValue(success, "Result.success()");
            return success;
        }
        ListenableWorker.Result failure = ListenableWorker.Result.failure();
        m.checkNotNullExpressionValue(failure, "Result.failure()");
        return failure;
    }
}
