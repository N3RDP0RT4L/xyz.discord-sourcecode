package com.discord.workers;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.work.Data;
import androidx.work.ListenableWorker;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import com.discord.api.message.Message;
import com.discord.app.AppLog;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreStream;
import com.discord.utilities.fcm.NotificationCache;
import com.discord.utilities.fcm.NotificationClient;
import com.discord.utilities.fcm.NotificationData;
import com.discord.utilities.fcm.NotificationRenderer;
import com.discord.utilities.io.NetworkUtils;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: MessageSendWorker.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \u000b2\u00020\u0001:\u0001\u000bB\u0017\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\f"}, d2 = {"Lcom/discord/workers/MessageSendWorker;", "Landroidx/work/Worker;", "Landroidx/work/ListenableWorker$Result;", "doWork", "()Landroidx/work/ListenableWorker$Result;", "Landroid/content/Context;", "context", "Landroidx/work/WorkerParameters;", "params", HookHelper.constructorName, "(Landroid/content/Context;Landroidx/work/WorkerParameters;)V", "a", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MessageSendWorker extends Worker {
    public static final a a = new a(null);

    /* compiled from: MessageSendWorker.kt */
    /* loaded from: classes2.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: MessageSendWorker.kt */
    /* loaded from: classes2.dex */
    public static final class b extends o implements Function1<Integer, Unit> {
        public final /* synthetic */ long $channelId;
        public final /* synthetic */ String $channelName;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(long j, String str) {
            super(1);
            this.$channelId = j;
            this.$channelName = str;
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(Integer num) {
            int intValue = num.intValue();
            NotificationRenderer notificationRenderer = NotificationRenderer.INSTANCE;
            Context applicationContext = MessageSendWorker.this.getApplicationContext();
            m.checkNotNullExpressionValue(applicationContext, "applicationContext");
            notificationRenderer.displaySent(applicationContext, this.$channelId, this.$channelName, false, intValue);
            return Unit.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MessageSendWorker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(workerParameters, "params");
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r13v4 */
    /* JADX WARN: Type inference failed for: r13v6, types: [rx.Observable] */
    @Override // androidx.work.Worker
    public ListenableWorker.Result doWork() {
        boolean z2;
        Throwable th;
        AppLog appLog = AppLog.g;
        String simpleName = MessageSendWorker.class.getSimpleName();
        m.checkNotNullExpressionValue(simpleName, "javaClass.simpleName");
        StringBuilder sb = new StringBuilder();
        sb.append("Starting for [");
        Data inputData = getInputData();
        m.checkNotNullExpressionValue(inputData, "inputData");
        sb.append(inputData.getKeyValueMap());
        sb.append(']');
        Logger.i$default(appLog, simpleName, sb.toString(), null, 4, null);
        NotificationClient notificationClient = NotificationClient.INSTANCE;
        if (!notificationClient.isAuthed()) {
            String simpleName2 = MessageSendWorker.class.getSimpleName();
            m.checkNotNullExpressionValue(simpleName2, "javaClass.simpleName");
            Logger.d$default(appLog, simpleName2, "Not authenticated. Aborting job request.", null, 4, null);
            ListenableWorker.Result failure = ListenableWorker.Result.failure();
            m.checkNotNullExpressionValue(failure, "Result.failure()");
            return failure;
        }
        NetworkUtils networkUtils = NetworkUtils.INSTANCE;
        Context applicationContext = getApplicationContext();
        m.checkNotNullExpressionValue(applicationContext, "applicationContext");
        if (NetworkUtils.isDeviceConnected$default(networkUtils, applicationContext, null, null, 6, null)) {
            long j = getInputData().getLong("com.discord.intent.extra.EXTRA_CHANNEL_ID", -1L);
            String string = getInputData().getString("com.discord.intent.extra.EXTRA_CHANNEL_NAME");
            if (string == null) {
                string = "";
            }
            String str = string;
            String string2 = getInputData().getString("MESSAGE_CONTENT");
            if (string2 != null) {
                String string3 = getInputData().getString("com.discord.intent.extra.EXTRA_MESSAGE_ID");
                long j2 = getInputData().getLong("com.discord.intent.extra.EXTRA_STICKER_ID", -1L);
                try {
                    z2 = ObservableExtensionsKt.restSubscribeOn(RestAPI.Companion.getApi().sendMessage(j, new RestAPIParams.Message(string2, string3, null, null, j2 != -1 ? d0.t.m.listOf(Long.valueOf(j2)) : null, null, null, null, 236, null)), false);
                    Observable takeSingleUntilTimeout$default = ObservableExtensionsKt.takeSingleUntilTimeout$default(z2, 0L, false, 3, null);
                    Objects.requireNonNull(takeSingleUntilTimeout$default);
                    Message message = (Message) new j0.m.a(takeSingleUntilTimeout$default).b();
                    NotificationData.DisplayPayload displayPayload = NotificationCache.INSTANCE.get(j);
                    try {
                        if (displayPayload != null) {
                            NotificationData notificationData = (NotificationData) u.lastOrNull((List<? extends Object>) displayPayload.getExtras());
                            if (notificationData != null) {
                                m.checkNotNullExpressionValue(message, "message");
                                NotificationData copyForDirectReply = notificationData.copyForDirectReply(message);
                                NotificationRenderer notificationRenderer = NotificationRenderer.INSTANCE;
                                Context applicationContext2 = getApplicationContext();
                                m.checkNotNullExpressionValue(applicationContext2, "applicationContext");
                                notificationRenderer.display(applicationContext2, copyForDirectReply, notificationClient.getSettings$app_productionGoogleRelease());
                                String simpleName3 = getClass().getSimpleName();
                                m.checkNotNullExpressionValue(simpleName3, "javaClass.simpleName");
                                Logger.d$default(appLog, simpleName3, "Direct reply: " + j + '-' + message.o(), null, 4, null);
                                StoreStream.Companion.getAnalytics().ackMessage(j);
                                ListenableWorker.Result success = ListenableWorker.Result.success();
                                m.checkNotNullExpressionValue(success, "Result.success()");
                                return success;
                            }
                            ListenableWorker.Result failure2 = ListenableWorker.Result.failure();
                            m.checkNotNullExpressionValue(failure2, "Result.failure()");
                            return failure2;
                        }
                        ListenableWorker.Result failure3 = ListenableWorker.Result.failure();
                        m.checkNotNullExpressionValue(failure3, "Result.failure()");
                        return failure3;
                    } catch (Throwable th2) {
                        th = th2;
                        AppLog appLog2 = AppLog.g;
                        String simpleName4 = MessageSendWorker.class.getSimpleName();
                        m.checkNotNullExpressionValue(simpleName4, "javaClass.simpleName");
                        appLog2.w(simpleName4, "Direct reply failure: " + j, th);
                        NotificationCache.INSTANCE.remove(j, z2, new b(j, str));
                        ListenableWorker.Result failure4 = ListenableWorker.Result.failure();
                        m.checkNotNullExpressionValue(failure4, "Result.failure()");
                        return failure4;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    z2 = 0;
                }
            } else {
                ListenableWorker.Result success2 = ListenableWorker.Result.success();
                m.checkNotNullExpressionValue(success2, "Result.success()");
                return success2;
            }
        } else if (getRunAttemptCount() < 3) {
            ListenableWorker.Result retry = ListenableWorker.Result.retry();
            m.checkNotNullExpressionValue(retry, "Result.retry()");
            return retry;
        } else {
            ListenableWorker.Result failure5 = ListenableWorker.Result.failure();
            m.checkNotNullExpressionValue(failure5, "Result.failure()");
            return failure5;
        }
    }
}
