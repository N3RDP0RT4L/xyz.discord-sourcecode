package com.discord.workers;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.work.Data;
import androidx.work.ListenableWorker;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import com.discord.app.AppLog;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreStream;
import com.discord.utilities.fcm.NotificationClient;
import com.discord.utilities.io.NetworkUtils;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.rest.RestAPI;
import d0.z.d.m;
import j0.m.a;
import java.util.Objects;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: MessageAckWorker.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u000b"}, d2 = {"Lcom/discord/workers/MessageAckWorker;", "Landroidx/work/Worker;", "Landroidx/work/ListenableWorker$Result;", "doWork", "()Landroidx/work/ListenableWorker$Result;", "Landroid/content/Context;", "context", "Landroidx/work/WorkerParameters;", "params", HookHelper.constructorName, "(Landroid/content/Context;Landroidx/work/WorkerParameters;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MessageAckWorker extends Worker {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MessageAckWorker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(workerParameters, "params");
    }

    @Override // androidx.work.Worker
    public ListenableWorker.Result doWork() {
        long j;
        long j2;
        Throwable th;
        AppLog appLog = AppLog.g;
        String simpleName = MessageAckWorker.class.getSimpleName();
        m.checkNotNullExpressionValue(simpleName, "javaClass.simpleName");
        StringBuilder sb = new StringBuilder();
        sb.append("Starting for [");
        Data inputData = getInputData();
        m.checkNotNullExpressionValue(inputData, "inputData");
        sb.append(inputData.getKeyValueMap());
        sb.append(']');
        Logger.i$default(appLog, simpleName, sb.toString(), null, 4, null);
        NotificationClient notificationClient = NotificationClient.INSTANCE;
        if (!notificationClient.isAuthed()) {
            String simpleName2 = MessageAckWorker.class.getSimpleName();
            m.checkNotNullExpressionValue(simpleName2, "javaClass.simpleName");
            Logger.d$default(appLog, simpleName2, "Not authenticated. Aborting job request.", null, 4, null);
            ListenableWorker.Result failure = ListenableWorker.Result.failure();
            m.checkNotNullExpressionValue(failure, "Result.failure()");
            return failure;
        }
        NetworkUtils networkUtils = NetworkUtils.INSTANCE;
        Context applicationContext = getApplicationContext();
        m.checkNotNullExpressionValue(applicationContext, "applicationContext");
        if (!NetworkUtils.isDeviceConnected$default(networkUtils, applicationContext, null, null, 6, null)) {
            ListenableWorker.Result retry = ListenableWorker.Result.retry();
            m.checkNotNullExpressionValue(retry, "Result.retry()");
            return retry;
        }
        long j3 = getInputData().getLong("com.discord.intent.extra.EXTRA_CHANNEL_ID", -1L);
        long j4 = getInputData().getLong("com.discord.intent.extra.EXTRA_MESSAGE_ID", -1L);
        try {
            Observable<Void> postChannelMessagesAck = RestAPI.Companion.getApi().postChannelMessagesAck(j3, Long.valueOf(j4), new RestAPIParams.ChannelMessagesAck(Boolean.FALSE, 0));
            Objects.requireNonNull(postChannelMessagesAck);
            new a(postChannelMessagesAck).b();
            String simpleName3 = getClass().getSimpleName();
            m.checkNotNullExpressionValue(simpleName3, "javaClass.simpleName");
            Logger.d$default(appLog, simpleName3, "Marked as read: " + j3 + '-' + j4, null, 4, null);
            j = j4;
            j2 = j3;
            try {
                NotificationClient.clear$default(notificationClient, j3, getApplicationContext(), false, 4, null);
                StoreStream.Companion.getAnalytics().ackMessage(j2);
                ListenableWorker.Result success = ListenableWorker.Result.success();
                m.checkNotNullExpressionValue(success, "Result.success()");
                return success;
            } catch (Throwable th2) {
                th = th2;
                AppLog appLog2 = AppLog.g;
                String simpleName4 = MessageAckWorker.class.getSimpleName();
                m.checkNotNullExpressionValue(simpleName4, "javaClass.simpleName");
                appLog2.w(simpleName4, "Marked as read failure: " + j2 + '-' + j, th);
                ListenableWorker.Result retry2 = getRunAttemptCount() < 5 ? ListenableWorker.Result.retry() : ListenableWorker.Result.failure();
                m.checkNotNullExpressionValue(retry2, "if (runAttemptCount < MA…y() else Result.failure()");
                return retry2;
            }
        } catch (Throwable th3) {
            th = th3;
            j = j4;
            j2 = j3;
        }
    }
}
