package com.discord.gsonutils;

import b.d.b.a.a;
import b.i.a.f.e.o.f;
import b.i.d.k;
import b.i.d.o;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.TypeAdapter;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.internal.bind.TypeAdapters;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.util.ConcurrentModificationException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;
/* loaded from: classes.dex */
public final class RuntimeTypeAdapterFactory<T> implements o {
    public final Class<?> j;
    public final String k;
    public final Map<String, Class<?>> l = new LinkedHashMap();
    public final Map<Class<?>, String> m = new LinkedHashMap();
    public final boolean n;

    public RuntimeTypeAdapterFactory(Class<?> cls, String str, boolean z2) {
        if (str == null || cls == null) {
            throw null;
        }
        this.j = cls;
        this.k = str;
        this.n = z2;
    }

    @Override // b.i.d.o
    public <R> TypeAdapter<R> create(Gson gson, TypeToken<R> typeToken) {
        if (typeToken.getRawType() != this.j) {
            return null;
        }
        final LinkedHashMap linkedHashMap = new LinkedHashMap();
        final LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        for (Map.Entry<String, Class<?>> entry : this.l.entrySet()) {
            TypeAdapter<T> j = gson.j(this, TypeToken.get((Class) entry.getValue()));
            linkedHashMap.put(entry.getKey(), j);
            linkedHashMap2.put(entry.getValue(), j);
        }
        return new TypeAdapter<R>() { // from class: com.discord.gsonutils.RuntimeTypeAdapterFactory.1
            /* JADX WARN: Type inference failed for: r4v4, types: [R, java.lang.Object] */
            @Override // com.google.gson.TypeAdapter
            public R read(JsonReader jsonReader) throws IOException {
                JsonElement jsonElement;
                JsonElement S0 = f.S0(jsonReader);
                if (RuntimeTypeAdapterFactory.this.n) {
                    LinkedTreeMap.e<String, JsonElement> c = S0.d().a.c(RuntimeTypeAdapterFactory.this.k);
                    jsonElement = c != null ? c.p : null;
                } else {
                    jsonElement = S0.d().a.remove(RuntimeTypeAdapterFactory.this.k);
                }
                if (jsonElement != null) {
                    String g = jsonElement.g();
                    TypeAdapter typeAdapter = (TypeAdapter) linkedHashMap.get(g);
                    if (typeAdapter != null) {
                        return typeAdapter.fromJsonTree(S0);
                    }
                    StringBuilder R = a.R("cannot deserialize ");
                    R.append(RuntimeTypeAdapterFactory.this.j);
                    R.append(" subtype named ");
                    R.append(g);
                    R.append("; did you forget to register a subtype?");
                    throw new JsonParseException(R.toString());
                }
                StringBuilder R2 = a.R("cannot deserialize ");
                R2.append(RuntimeTypeAdapterFactory.this.j);
                R2.append(" because it does not define a field named ");
                R2.append(RuntimeTypeAdapterFactory.this.k);
                throw new JsonParseException(R2.toString());
            }

            @Override // com.google.gson.TypeAdapter
            public void write(JsonWriter jsonWriter, R r) throws IOException {
                Class<?> cls = r.getClass();
                String str = RuntimeTypeAdapterFactory.this.m.get(cls);
                TypeAdapter typeAdapter = (TypeAdapter) linkedHashMap2.get(cls);
                if (typeAdapter != null) {
                    JsonObject d = typeAdapter.toJsonTree(r).d();
                    if (RuntimeTypeAdapterFactory.this.n) {
                        TypeAdapters.X.write(jsonWriter, d);
                        return;
                    }
                    JsonObject jsonObject = new JsonObject();
                    if (!(d.a.c(RuntimeTypeAdapterFactory.this.k) != null)) {
                        jsonObject.a.put(RuntimeTypeAdapterFactory.this.k, new k(str));
                        LinkedTreeMap linkedTreeMap = LinkedTreeMap.this;
                        LinkedTreeMap.e eVar = linkedTreeMap.header.m;
                        int i = linkedTreeMap.modCount;
                        while (true) {
                            LinkedTreeMap.e eVar2 = linkedTreeMap.header;
                            if (!(eVar != eVar2)) {
                                TypeAdapters.X.write(jsonWriter, jsonObject);
                                return;
                            } else if (eVar == eVar2) {
                                throw new NoSuchElementException();
                            } else if (linkedTreeMap.modCount == i) {
                                LinkedTreeMap.e eVar3 = eVar.m;
                                jsonObject.h((String) eVar.o, (JsonElement) eVar.p);
                                eVar = eVar3;
                            } else {
                                throw new ConcurrentModificationException();
                            }
                        }
                    } else {
                        StringBuilder R = a.R("cannot serialize ");
                        a.i0(cls, R, " because it already defines a field named ");
                        R.append(RuntimeTypeAdapterFactory.this.k);
                        throw new JsonParseException(R.toString());
                    }
                } else {
                    throw new JsonParseException(a.n(cls, a.R("cannot serialize "), "; did you forget to register a subtype?"));
                }
            }
        }.nullSafe();
    }
}
