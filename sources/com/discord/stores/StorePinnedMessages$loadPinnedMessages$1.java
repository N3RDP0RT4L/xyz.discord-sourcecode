package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.api.message.Message;
import d0.t.o;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
/* compiled from: StorePinnedMessages.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0002*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00000\u00002\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "Lcom/discord/api/message/Message;", "kotlin.jvm.PlatformType", "messages", "Lcom/discord/models/message/Message;", NotificationCompat.CATEGORY_CALL, "(Ljava/util/List;)Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StorePinnedMessages$loadPinnedMessages$1<T, R> implements b<List<? extends Message>, List<? extends com.discord.models.message.Message>> {
    public static final StorePinnedMessages$loadPinnedMessages$1 INSTANCE = new StorePinnedMessages$loadPinnedMessages$1();

    @Override // j0.k.b
    public /* bridge */ /* synthetic */ List<? extends com.discord.models.message.Message> call(List<? extends Message> list) {
        return call2((List<Message>) list);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final List<com.discord.models.message.Message> call2(List<Message> list) {
        m.checkNotNullExpressionValue(list, "messages");
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
        for (Message message : list) {
            arrayList.add(new com.discord.models.message.Message(message));
        }
        return arrayList;
    }
}
