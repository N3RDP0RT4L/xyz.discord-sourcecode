package com.discord.stores;

import com.discord.api.sticker.Sticker;
import d0.t.h0;
import d0.z.d.o;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreGuildStickers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildStickers$handleFetchedSticker$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ Sticker $sticker;
    public final /* synthetic */ StoreGuildStickers this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGuildStickers$handleFetchedSticker$1(StoreGuildStickers storeGuildStickers, long j, Sticker sticker) {
        super(0);
        this.this$0 = storeGuildStickers;
        this.$guildId = j;
        this.$sticker = sticker;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        Map map;
        Map map2;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        map = this.this$0.allGuildStickers;
        Map map3 = (Map) map.get(Long.valueOf(this.$guildId));
        if (map3 == null) {
            map3 = h0.emptyMap();
        }
        linkedHashMap.putAll(map3);
        linkedHashMap.put(Long.valueOf(this.$sticker.getId()), this.$sticker);
        map2 = this.this$0.allGuildStickers;
        map2.put(Long.valueOf(this.$guildId), linkedHashMap);
        this.this$0.markChanged();
    }
}
