package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.stores.StoreMessageAck;
import d0.z.d.m;
import j0.k.b;
import kotlin.Metadata;
/* compiled from: StoreMessageAck.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/stores/StoreMessageAck$PendingAck;", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/stores/StoreMessageAck$PendingAck;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessageAck$postPendingAck$2<T, R> implements b<StoreMessageAck.PendingAck, Boolean> {
    public static final StoreMessageAck$postPendingAck$2 INSTANCE = new StoreMessageAck$postPendingAck$2();

    public final Boolean call(StoreMessageAck.PendingAck pendingAck) {
        return Boolean.valueOf(!m.areEqual(pendingAck, StoreMessageAck.PendingAck.Companion.getEMPTY()));
    }
}
