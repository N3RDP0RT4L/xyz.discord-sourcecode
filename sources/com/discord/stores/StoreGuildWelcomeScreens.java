package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.guild.welcome.GuildWelcomeScreen;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import d0.t.h0;
import d0.t.n0;
import d0.z.d.m;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreGuildWelcomeScreens.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u00011B\u0019\u0012\u0006\u0010\u001c\u001a\u00020\u001b\u0012\b\b\u0002\u0010*\u001a\u00020)¢\u0006\u0004\b/\u00100J!\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004H\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u001b\u0010\n\u001a\u00020\u00062\n\u0010\u0003\u001a\u00060\u0002j\u0002`\tH\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u001b\u0010\f\u001a\u00020\u00062\n\u0010\u0003\u001a\u00060\u0002j\u0002`\tH\u0003¢\u0006\u0004\b\f\u0010\u000bJ\u001b\u0010\u000e\u001a\u0004\u0018\u00010\r2\n\u0010\u0003\u001a\u00060\u0002j\u0002`\t¢\u0006\u0004\b\u000e\u0010\u000fJ\u0019\u0010\u0011\u001a\u00020\u00102\n\u0010\u0003\u001a\u00060\u0002j\u0002`\t¢\u0006\u0004\b\u0011\u0010\u0012J!\u0010\u0014\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\r0\u00132\n\u0010\u0003\u001a\u00060\u0002j\u0002`\t¢\u0006\u0004\b\u0014\u0010\u0015J\u0019\u0010\u0016\u001a\u00020\u00062\n\u0010\u0003\u001a\u00060\u0002j\u0002`\t¢\u0006\u0004\b\u0016\u0010\u000bJ\u0019\u0010\u0017\u001a\u00020\u00062\n\u0010\u0003\u001a\u00060\u0002j\u0002`\t¢\u0006\u0004\b\u0017\u0010\u000bJ\u001f\u0010\u0018\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004¢\u0006\u0004\b\u0018\u0010\bJ\u000f\u0010\u0019\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001c\u001a\u00020\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR \u0010\u001f\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\t0\u001e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001f\u0010 R:\u0010#\u001a&\u0012\b\u0012\u00060\u0002j\u0002`\t\u0012\u0004\u0012\u00020\r0!j\u0012\u0012\b\u0012\u00060\u0002j\u0002`\t\u0012\u0004\u0012\u00020\r`\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$R.\u0010'\u001a\u001a\u0012\b\u0012\u00060\u0002j\u0002`\t0%j\f\u0012\b\u0012\u00060\u0002j\u0002`\t`&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R\u0016\u0010*\u001a\u00020)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010+R&\u0010-\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\t\u0012\u0004\u0012\u00020\r0,8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b-\u0010.¨\u00062"}, d2 = {"Lcom/discord/stores/StoreGuildWelcomeScreens;", "Lcom/discord/stores/StoreV2;", "", "guildId", "Lcom/discord/api/guild/welcome/GuildWelcomeScreen;", "guildWelcomeScreen", "", "handleGuildWelcomeScreen", "(JLcom/discord/api/guild/welcome/GuildWelcomeScreen;)V", "Lcom/discord/primitives/GuildId;", "handleGuildWelcomeScreenFetchStart", "(J)V", "handleGuildWelcomeScreenFetchFailed", "Lcom/discord/stores/StoreGuildWelcomeScreens$State;", "getGuildWelcomeScreen", "(J)Lcom/discord/stores/StoreGuildWelcomeScreens$State;", "", "hasWelcomeScreenBeenSeen", "(J)Z", "Lrx/Observable;", "observeGuildWelcomeScreen", "(J)Lrx/Observable;", "markWelcomeScreenShown", "fetchIfNonexisting", "handleGuildJoined", "snapshotData", "()V", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "", "guildWelcomeScreensSeenSnapshot", "Ljava/util/Set;", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "guildWelcomeScreensState", "Ljava/util/HashMap;", "Ljava/util/HashSet;", "Lkotlin/collections/HashSet;", "guildWelcomeScreensSeen", "Ljava/util/HashSet;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "", "guildWelcomeScreensSnapshot", "Ljava/util/Map;", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V", "State", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildWelcomeScreens extends StoreV2 {
    private final Dispatcher dispatcher;
    private final HashSet<Long> guildWelcomeScreensSeen;
    private Set<Long> guildWelcomeScreensSeenSnapshot;
    private Map<Long, ? extends State> guildWelcomeScreensSnapshot;
    private final HashMap<Long, State> guildWelcomeScreensState;
    private final ObservationDeck observationDeck;

    /* compiled from: StoreGuildWelcomeScreens.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/stores/StoreGuildWelcomeScreens$State;", "", HookHelper.constructorName, "()V", "Failure", "Fetching", "Loaded", "Lcom/discord/stores/StoreGuildWelcomeScreens$State$Loaded;", "Lcom/discord/stores/StoreGuildWelcomeScreens$State$Failure;", "Lcom/discord/stores/StoreGuildWelcomeScreens$State$Fetching;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class State {

        /* compiled from: StoreGuildWelcomeScreens.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreGuildWelcomeScreens$State$Failure;", "Lcom/discord/stores/StoreGuildWelcomeScreens$State;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Failure extends State {
            public static final Failure INSTANCE = new Failure();

            private Failure() {
                super(null);
            }
        }

        /* compiled from: StoreGuildWelcomeScreens.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreGuildWelcomeScreens$State$Fetching;", "Lcom/discord/stores/StoreGuildWelcomeScreens$State;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Fetching extends State {
            public static final Fetching INSTANCE = new Fetching();

            private Fetching() {
                super(null);
            }
        }

        /* compiled from: StoreGuildWelcomeScreens.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0011\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001c\u0010\u0006\u001a\u00020\u00002\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0005\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/stores/StoreGuildWelcomeScreens$State$Loaded;", "Lcom/discord/stores/StoreGuildWelcomeScreens$State;", "Lcom/discord/api/guild/welcome/GuildWelcomeScreen;", "component1", "()Lcom/discord/api/guild/welcome/GuildWelcomeScreen;", "data", "copy", "(Lcom/discord/api/guild/welcome/GuildWelcomeScreen;)Lcom/discord/stores/StoreGuildWelcomeScreens$State$Loaded;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/guild/welcome/GuildWelcomeScreen;", "getData", HookHelper.constructorName, "(Lcom/discord/api/guild/welcome/GuildWelcomeScreen;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Loaded extends State {
            private final GuildWelcomeScreen data;

            public Loaded(GuildWelcomeScreen guildWelcomeScreen) {
                super(null);
                this.data = guildWelcomeScreen;
            }

            public static /* synthetic */ Loaded copy$default(Loaded loaded, GuildWelcomeScreen guildWelcomeScreen, int i, Object obj) {
                if ((i & 1) != 0) {
                    guildWelcomeScreen = loaded.data;
                }
                return loaded.copy(guildWelcomeScreen);
            }

            public final GuildWelcomeScreen component1() {
                return this.data;
            }

            public final Loaded copy(GuildWelcomeScreen guildWelcomeScreen) {
                return new Loaded(guildWelcomeScreen);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Loaded) && m.areEqual(this.data, ((Loaded) obj).data);
                }
                return true;
            }

            public final GuildWelcomeScreen getData() {
                return this.data;
            }

            public int hashCode() {
                GuildWelcomeScreen guildWelcomeScreen = this.data;
                if (guildWelcomeScreen != null) {
                    return guildWelcomeScreen.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(data=");
                R.append(this.data);
                R.append(")");
                return R.toString();
            }
        }

        private State() {
        }

        public /* synthetic */ State(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ StoreGuildWelcomeScreens(Dispatcher dispatcher, ObservationDeck observationDeck, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(dispatcher, (i & 2) != 0 ? ObservationDeckProvider.get() : observationDeck);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleGuildWelcomeScreen(long j, GuildWelcomeScreen guildWelcomeScreen) {
        this.guildWelcomeScreensState.put(Long.valueOf(j), new State.Loaded(guildWelcomeScreen));
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleGuildWelcomeScreenFetchFailed(long j) {
        this.guildWelcomeScreensState.put(Long.valueOf(j), State.Failure.INSTANCE);
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleGuildWelcomeScreenFetchStart(long j) {
        this.guildWelcomeScreensState.put(Long.valueOf(j), State.Fetching.INSTANCE);
        markChanged();
    }

    public final void fetchIfNonexisting(long j) {
        this.dispatcher.schedule(new StoreGuildWelcomeScreens$fetchIfNonexisting$1(this, j));
    }

    public final State getGuildWelcomeScreen(long j) {
        return this.guildWelcomeScreensSnapshot.get(Long.valueOf(j));
    }

    public final void handleGuildJoined(long j, GuildWelcomeScreen guildWelcomeScreen) {
        handleGuildWelcomeScreen(j, guildWelcomeScreen);
    }

    public final boolean hasWelcomeScreenBeenSeen(long j) {
        return this.guildWelcomeScreensSeenSnapshot.contains(Long.valueOf(j));
    }

    public final void markWelcomeScreenShown(long j) {
        this.dispatcher.schedule(new StoreGuildWelcomeScreens$markWelcomeScreenShown$1(this, j));
    }

    public final Observable<State> observeGuildWelcomeScreen(long j) {
        Observable<State> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreGuildWelcomeScreens$observeGuildWelcomeScreen$1(this, j), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        super.snapshotData();
        this.guildWelcomeScreensSnapshot = new HashMap(this.guildWelcomeScreensState);
        this.guildWelcomeScreensSeenSnapshot = new HashSet(this.guildWelcomeScreensSeen);
    }

    public StoreGuildWelcomeScreens(Dispatcher dispatcher, ObservationDeck observationDeck) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
        this.guildWelcomeScreensSnapshot = h0.emptyMap();
        this.guildWelcomeScreensSeenSnapshot = n0.emptySet();
        this.guildWelcomeScreensState = new HashMap<>();
        this.guildWelcomeScreensSeen = new HashSet<>();
    }
}
