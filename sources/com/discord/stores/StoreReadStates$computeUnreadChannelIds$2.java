package com.discord.stores;

import d0.z.d.o;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.subjects.SerializedSubject;
/* compiled from: StoreReadStates.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\n\u001a\u00020\u00072N\u0010\u0006\u001aJ\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\u0001\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00040\u0001 \u0005*$\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\u0001\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00040\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\b\u0010\t"}, d2 = {"Lkotlin/Pair;", "", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/primitives/GuildId;", "kotlin.jvm.PlatformType", "<name for destructuring parameter 0>", "", "invoke", "(Lkotlin/Pair;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreReadStates$computeUnreadChannelIds$2 extends o implements Function1<Pair<? extends Set<? extends Long>, ? extends Set<? extends Long>>, Unit> {
    public final /* synthetic */ StoreReadStates this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreReadStates$computeUnreadChannelIds$2(StoreReadStates storeReadStates) {
        super(1);
        this.this$0 = storeReadStates;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Pair<? extends Set<? extends Long>, ? extends Set<? extends Long>> pair) {
        invoke2((Pair<? extends Set<Long>, ? extends Set<Long>>) pair);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Pair<? extends Set<Long>, ? extends Set<Long>> pair) {
        SerializedSubject serializedSubject;
        SerializedSubject serializedSubject2;
        serializedSubject = this.this$0.unreadChannelIds;
        serializedSubject.k.onNext(pair.component1());
        serializedSubject2 = this.this$0.unreadGuildIds;
        serializedSubject2.k.onNext(pair.component2());
    }
}
