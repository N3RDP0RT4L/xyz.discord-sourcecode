package com.discord.stores;

import b.d.b.a.a;
import com.discord.app.AppLog;
import com.discord.models.domain.ModelRtcLatencyRegion;
import com.discord.utilities.time.Clock;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreRtcRegion.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreRtcRegion$maybePerformLatencyTest$1 extends o implements Function0<Unit> {
    public final /* synthetic */ List $regionsWithIps;
    public final /* synthetic */ StoreRtcRegion this$0;

    /* compiled from: StoreRtcRegion.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "", "latencyRankedRegions", "", "invoke", "(Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreRtcRegion$maybePerformLatencyTest$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<List<? extends String>, Unit> {
        public final /* synthetic */ List $newGeoRankedRegions;
        public final /* synthetic */ long $timeNowMs;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(List list, long j) {
            super(1);
            this.$newGeoRankedRegions = list;
            this.$timeNowMs = j;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(List<? extends String> list) {
            invoke2((List<String>) list);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(List<String> list) {
            m.checkNotNullParameter(list, "latencyRankedRegions");
            AppLog.i("RTC region latency test ranking is " + list);
            StoreRtcRegion$maybePerformLatencyTest$1.this.this$0.updateLastTestResult(new RtcLatencyTestResult(list, this.$newGeoRankedRegions, this.$timeNowMs));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreRtcRegion$maybePerformLatencyTest$1(StoreRtcRegion storeRtcRegion, List list) {
        super(0);
        this.this$0 = storeRtcRegion;
        this.$regionsWithIps = list;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        Clock clock;
        boolean shouldPerformLatencyTest;
        StoreMediaEngine storeMediaEngine;
        List<ModelRtcLatencyRegion> list = this.$regionsWithIps;
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(list, 10));
        for (ModelRtcLatencyRegion modelRtcLatencyRegion : list) {
            arrayList.add(modelRtcLatencyRegion.getRegion());
        }
        clock = this.this$0.clock;
        long currentTimeMillis = clock.currentTimeMillis();
        shouldPerformLatencyTest = this.this$0.shouldPerformLatencyTest(arrayList, currentTimeMillis);
        if (shouldPerformLatencyTest) {
            storeMediaEngine = this.this$0.storeMediaEngine;
            storeMediaEngine.getRankedRtcRegions(this.$regionsWithIps, new AnonymousClass1(arrayList, currentTimeMillis));
            return;
        }
        StringBuilder R = a.R("RTC region latency test cached preferred region is ");
        R.append(this.this$0.getPreferredRegion());
        AppLog.i(R.toString());
    }
}
