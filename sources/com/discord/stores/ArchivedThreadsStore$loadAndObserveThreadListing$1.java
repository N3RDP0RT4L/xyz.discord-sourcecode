package com.discord.stores;

import com.discord.stores.ArchivedThreadsStore;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.functions.Function0;
/* compiled from: ArchivedThreadsStore.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/stores/ArchivedThreadsStore$ThreadListingState;", "invoke", "()Lcom/discord/stores/ArchivedThreadsStore$ThreadListingState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ArchivedThreadsStore$loadAndObserveThreadListing$1 extends o implements Function0<ArchivedThreadsStore.ThreadListingState> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ ArchivedThreadsStore.ThreadListingType $threadListingType;
    public final /* synthetic */ ArchivedThreadsStore this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ArchivedThreadsStore$loadAndObserveThreadListing$1(ArchivedThreadsStore archivedThreadsStore, long j, ArchivedThreadsStore.ThreadListingType threadListingType) {
        super(0);
        this.this$0 = archivedThreadsStore;
        this.$channelId = j;
        this.$threadListingType = threadListingType;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final ArchivedThreadsStore.ThreadListingState invoke() {
        Map map;
        map = this.this$0.listingsSnapshot;
        ArchivedThreadsStore.ThreadListingState threadListingState = (ArchivedThreadsStore.ThreadListingState) map.get(new Pair(Long.valueOf(this.$channelId), this.$threadListingType));
        return threadListingState != null ? threadListingState : ArchivedThreadsStore.ThreadListingState.Uninitialized.INSTANCE;
    }
}
