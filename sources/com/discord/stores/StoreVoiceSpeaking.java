package com.discord.stores;

import andhook.lib.HookHelper;
import com.discord.stores.StoreRtcConnection;
import com.discord.stores.updates.ObservationDeck;
import d0.z.d.m;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: StoreVoiceSpeaking.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u001a\u001a\u00020\u0019¢\u0006\u0004\b\u001c\u0010\u001dJ\u0013\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0019\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00020\u0006¢\u0006\u0004\b\u0007\u0010\bJ\u001d\u0010\r\u001a\u00020\f2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0007¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0010\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u0003H\u0007¢\u0006\u0004\b\u0010\u0010\u0011J\u000f\u0010\u0012\u001a\u00020\fH\u0016¢\u0006\u0004\b\u0012\u0010\u0013R\u001c\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u001c\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00030\u00168\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u0016\u0010\u001a\u001a\u00020\u00198\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001a\u0010\u001b¨\u0006\u001e"}, d2 = {"Lcom/discord/stores/StoreVoiceSpeaking;", "Lcom/discord/stores/StoreV2;", "", "", "getSpeakingUsers", "()Ljava/util/Set;", "Lrx/Observable;", "observeSpeakingUsers", "()Lrx/Observable;", "", "Lcom/discord/stores/StoreRtcConnection$SpeakingUserUpdate;", "speakingList", "", "handleSpeakingUpdates", "(Ljava/util/List;)V", "voiceChannelId", "handleVoiceChannelSelected", "(J)V", "snapshotData", "()V", "speakingUsersSnapshot", "Ljava/util/Set;", "Ljava/util/HashSet;", "speakingUsers", "Ljava/util/HashSet;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", HookHelper.constructorName, "(Lcom/discord/stores/updates/ObservationDeck;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreVoiceSpeaking extends StoreV2 {
    private final ObservationDeck observationDeck;
    private final HashSet<Long> speakingUsers = new HashSet<>();
    private Set<Long> speakingUsersSnapshot = new HashSet();

    public StoreVoiceSpeaking(ObservationDeck observationDeck) {
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.observationDeck = observationDeck;
    }

    public final Set<Long> getSpeakingUsers() {
        return this.speakingUsersSnapshot;
    }

    @StoreThread
    public final void handleSpeakingUpdates(List<StoreRtcConnection.SpeakingUserUpdate> list) {
        boolean z2;
        m.checkNotNullParameter(list, "speakingList");
        boolean z3 = false;
        for (StoreRtcConnection.SpeakingUserUpdate speakingUserUpdate : list) {
            long component1 = speakingUserUpdate.component1();
            if (speakingUserUpdate.component2()) {
                z2 = this.speakingUsers.add(Long.valueOf(component1));
            } else {
                z2 = this.speakingUsers.remove(Long.valueOf(component1));
            }
            if (!z3 && z2) {
                z3 = true;
            }
        }
        if (z3) {
            markChanged();
        }
    }

    @StoreThread
    public final void handleVoiceChannelSelected(long j) {
        if (!(j > 0)) {
            this.speakingUsers.clear();
            markChanged();
        }
    }

    public final Observable<Set<Long>> observeSpeakingUsers() {
        Observable<Set<Long>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreVoiceSpeaking$observeSpeakingUsers$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck\n        …  .distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        super.snapshotData();
        this.speakingUsersSnapshot = new HashSet(this.speakingUsers);
    }
}
