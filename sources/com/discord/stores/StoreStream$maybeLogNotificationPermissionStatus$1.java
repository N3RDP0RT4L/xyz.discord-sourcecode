package com.discord.stores;

import android.app.Application;
import android.content.SharedPreferences;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.fcm.NotificationClient;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreStream.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStream$maybeLogNotificationPermissionStatus$1 extends o implements Function1<NotificationClient.SettingsV2, Unit> {
    public final /* synthetic */ Application $context;
    public final /* synthetic */ long $currentTime;
    public final /* synthetic */ SharedPreferences $sharedPrefs;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreStream$maybeLogNotificationPermissionStatus$1(SharedPreferences sharedPreferences, long j, Application application) {
        super(1);
        this.$sharedPrefs = sharedPreferences;
        this.$currentTime = j;
        this.$context = application;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(NotificationClient.SettingsV2 settingsV2) {
        invoke2(settingsV2);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(NotificationClient.SettingsV2 settingsV2) {
        String str;
        SharedPreferences.Editor edit = this.$sharedPrefs.edit();
        str = StoreStream.LAST_LOGGED_NOTIFICATION_PERMISSIONS_KEY;
        edit.putLong(str, this.$currentTime).apply();
        AnalyticsTracker analyticsTracker = AnalyticsTracker.INSTANCE;
        Application application = this.$context;
        m.checkNotNullExpressionValue(settingsV2, "it");
        analyticsTracker.notificationPermissionStatus(application, settingsV2);
    }
}
