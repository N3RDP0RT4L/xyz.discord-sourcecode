package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.SharedPreferences;
import com.discord.api.channel.Channel;
import com.discord.api.guild.Guild;
import com.discord.models.domain.ModelPayload;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.cache.SharedPreferenceExtensionsKt;
import d0.z.d.m;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreCollapsedChannelCategories.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0010#\n\u0002\b\u0007\u0018\u0000 +2\u00020\u0001:\u0001+B\u0017\u0012\u0006\u0010\"\u001a\u00020!\u0012\u0006\u0010\u001f\u001a\u00020\u001e¢\u0006\u0004\b)\u0010*J/\u0010\n\u001a\u00020\t2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\n\u0010\u000bJ'\u0010\u000e\u001a\u001c\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00050\r0\f¢\u0006\u0004\b\u000e\u0010\u000fJ)\u0010\u0011\u001a\u0012\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00050\r0\u00102\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0011\u0010\u0012J-\u0010\u0013\u001a\u00020\t2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\u0013\u0010\u000bJ\u0017\u0010\u0016\u001a\u00020\t2\u0006\u0010\u0015\u001a\u00020\u0014H\u0017¢\u0006\u0004\b\u0016\u0010\u0017J\u0017\u0010\u001a\u001a\u00020\t2\u0006\u0010\u0019\u001a\u00020\u0018H\u0007¢\u0006\u0004\b\u001a\u0010\u001bJ\u000f\u0010\u001c\u001a\u00020\tH\u0016¢\u0006\u0004\b\u001c\u0010\u001dR\u0016\u0010\u001f\u001a\u00020\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u0016\u0010\"\u001a\u00020!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010#R0\u0010&\u001a\u001c\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00050%0$8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b&\u0010'R0\u0010(\u001a\u001c\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00050%0$8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b(\u0010'¨\u0006,"}, d2 = {"Lcom/discord/stores/StoreCollapsedChannelCategories;", "Lcom/discord/stores/StoreV2;", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/ChannelId;", "categoryId", "", "collapsed", "", "setCollapsedState", "(JJZ)V", "", "", "getCollapsedCategories", "()Ljava/util/Map;", "Lrx/Observable;", "observeCollapsedCategories", "(J)Lrx/Observable;", "setCollapsedCategory", "Landroid/content/Context;", "context", "init", "(Landroid/content/Context;)V", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "snapshotData", "()V", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "", "", "collapsedCategories", "Ljava/util/Map;", "collapsedCategoriesSnapshot", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreCollapsedChannelCategories extends StoreV2 {
    private static final String CACHE_KEY_COLLAPSED_CATEGORIES = "STORE_COLLAPSED_CATEGORIES_V2";
    public static final Companion Companion = new Companion(null);
    private Map<Long, Set<Long>> collapsedCategories = new HashMap();
    private Map<Long, Set<Long>> collapsedCategoriesSnapshot = new HashMap();
    private final Dispatcher dispatcher;
    private final ObservationDeck observationDeck;

    /* compiled from: StoreCollapsedChannelCategories.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010#\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0012\u0010\u0013J1\u0010\t\u001a\u001c\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0005j\u0002`\b0\u00070\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\t\u0010\nJ9\u0010\r\u001a\u00020\f2\u0006\u0010\u0003\u001a\u00020\u00022 \u0010\u000b\u001a\u001c\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0005j\u0002`\b0\u00070\u0004H\u0002¢\u0006\u0004\b\r\u0010\u000eR\u0016\u0010\u0010\u001a\u00020\u000f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011¨\u0006\u0014"}, d2 = {"Lcom/discord/stores/StoreCollapsedChannelCategories$Companion;", "", "Landroid/content/SharedPreferences;", "prefs", "", "", "Lcom/discord/primitives/GuildId;", "", "Lcom/discord/primitives/ChannelId;", "fromCache", "(Landroid/content/SharedPreferences;)Ljava/util/Map;", "collapsedCategories", "", "toCache", "(Landroid/content/SharedPreferences;Ljava/util/Map;)V", "", "CACHE_KEY_COLLAPSED_CATEGORIES", "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Map<Long, Set<Long>> fromCache(SharedPreferences sharedPreferences) {
            return SharedPreferenceExtensionsKt.getStringEntrySetAsMap$default(sharedPreferences, StoreCollapsedChannelCategories.CACHE_KEY_COLLAPSED_CATEGORIES, null, StoreCollapsedChannelCategories$Companion$fromCache$1.INSTANCE, 2, null);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final void toCache(SharedPreferences sharedPreferences, Map<Long, Set<Long>> map) {
            SharedPreferences.Editor edit = sharedPreferences.edit();
            m.checkNotNullExpressionValue(edit, "editor");
            SharedPreferenceExtensionsKt.putStringEntrySetAsMap$default(edit, StoreCollapsedChannelCategories.CACHE_KEY_COLLAPSED_CATEGORIES, map, null, StoreCollapsedChannelCategories$Companion$toCache$1$1.INSTANCE, 4, null);
            edit.apply();
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public StoreCollapsedChannelCategories(Dispatcher dispatcher, ObservationDeck observationDeck) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void setCollapsedState(long j, long j2, boolean z2) {
        Map<Long, Set<Long>> map = this.collapsedCategories;
        Long valueOf = Long.valueOf(j);
        Set<Long> set = map.get(valueOf);
        if (set == null) {
            set = new HashSet<>();
            map.put(valueOf, set);
        }
        Set<Long> set2 = set;
        if (z2) {
            set2.add(Long.valueOf(j2));
        } else {
            set2.remove(Long.valueOf(j2));
            if (set2.isEmpty()) {
                this.collapsedCategories.remove(Long.valueOf(j));
            }
        }
        markChanged();
    }

    public final Map<Long, Set<Long>> getCollapsedCategories() {
        return this.collapsedCategoriesSnapshot;
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        HashSet<Long> hashSet = new HashSet(this.collapsedCategories.keySet());
        List<Guild> guilds = modelPayload.getGuilds();
        m.checkNotNullExpressionValue(guilds, "payload.guilds");
        for (Guild guild : guilds) {
            Set<Long> set = this.collapsedCategories.get(Long.valueOf(guild.r()));
            if (set != null) {
                HashSet<Long> hashSet2 = new HashSet(set);
                List<Channel> g = guild.g();
                if (g != null) {
                    for (Channel channel : g) {
                        hashSet2.remove(Long.valueOf(channel.h()));
                    }
                }
                for (Long l : hashSet2) {
                    long r = guild.r();
                    m.checkNotNullExpressionValue(l, "channelId");
                    setCollapsedState(r, l.longValue(), false);
                }
                hashSet.remove(Long.valueOf(guild.r()));
            }
        }
        for (Long l2 : hashSet) {
            this.collapsedCategories.remove(l2);
            markChanged();
        }
    }

    @Override // com.discord.stores.Store
    @StoreThread
    public void init(Context context) {
        m.checkNotNullParameter(context, "context");
        super.init(context);
        this.collapsedCategories = Companion.fromCache(getPrefs());
        markChanged();
    }

    public final Observable<Set<Long>> observeCollapsedCategories(long j) {
        Observable<Set<Long>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreCollapsedChannelCategories$observeCollapsedCategories$1(this, j), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck\n        …  .distinctUntilChanged()");
        return q;
    }

    public final void setCollapsedCategory(long j, long j2, boolean z2) {
        this.dispatcher.schedule(new StoreCollapsedChannelCategories$setCollapsedCategory$1(this, j, j2, z2));
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        super.snapshotData();
        Companion.toCache(getPrefs(), this.collapsedCategories);
        HashMap hashMap = new HashMap(this.collapsedCategories);
        Iterator<T> it = this.collapsedCategories.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            hashMap.put(entry.getKey(), new HashSet((Set) entry.getValue()));
        }
        this.collapsedCategoriesSnapshot = hashMap;
    }
}
