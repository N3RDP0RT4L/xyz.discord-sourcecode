package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guild.GuildMaxVideoChannelUsers;
import com.discord.api.guild.GuildVerificationLevel;
import com.discord.api.stageinstance.StageInstance;
import com.discord.api.voice.state.VoiceState;
import com.discord.models.domain.ModelApplicationStream;
import com.discord.models.domain.ModelPayload;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.user.MeUser;
import com.discord.rtcconnection.RtcConnection;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.guildmember.GuildMemberUtilsKt;
import com.discord.utilities.media.AppSound;
import com.discord.utilities.media.AppSoundManager;
import com.discord.utilities.time.Clock;
import com.discord.utilities.user.UserUtils;
import com.discord.utilities.voice.VoiceChannelJoinability;
import com.discord.utilities.voice.VoiceChannelJoinabilityUtils;
import d0.t.h0;
import d0.z.d.m;
import java.util.Collection;
import java.util.Map;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.PublishSubject;
/* compiled from: StoreVoiceChannelSelected.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000²\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 Z2\u00020\u00012\u00020\u0002:\u0002Z[B)\u0012\u0006\u0010L\u001a\u00020K\u0012\u0006\u0010V\u001a\u00020U\u0012\u0006\u0010G\u001a\u00020F\u0012\b\b\u0002\u0010O\u001a\u00020N¢\u0006\u0004\bX\u0010YJ#\u0010\t\u001a\u00020\b2\n\u0010\u0005\u001a\u00060\u0003j\u0002`\u00042\u0006\u0010\u0007\u001a\u00020\u0006H\u0003¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\f\u001a\u00020\u000bH\u0003¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000e\u001a\u00020\u000bH\u0003¢\u0006\u0004\b\u000e\u0010\rJ\u0017\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0010\u001a\u00020\u000fH\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u001b\u0010\u0016\u001a\u00020\u00152\n\u0010\u0014\u001a\u00060\u0003j\u0002`\u0004H\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0011\u0010\u0018\u001a\u00060\u0003j\u0002`\u0004¢\u0006\u0004\b\u0018\u0010\u0019J\u000f\u0010\u001b\u001a\u0004\u0018\u00010\u001a¢\u0006\u0004\b\u001b\u0010\u001cJ\u0011\u0010\u001d\u001a\u00060\u0003j\u0002`\u0004¢\u0006\u0004\b\u001d\u0010\u0019J\u0017\u0010\u001f\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u001e¢\u0006\u0004\b\u001f\u0010 J\u0017\u0010\"\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`!0\u001e¢\u0006\u0004\b\"\u0010 J\u0015\u0010#\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u001a0\u001e¢\u0006\u0004\b#\u0010 J\u001f\u0010$\u001a\b\u0012\u0004\u0012\u00020\b0\u001e2\n\u0010\u0014\u001a\u00060\u0003j\u0002`\u0004¢\u0006\u0004\b$\u0010%J\r\u0010&\u001a\u00020\u000b¢\u0006\u0004\b&\u0010\rJ\u0017\u0010)\u001a\u00020\u000b2\u0006\u0010(\u001a\u00020'H\u0007¢\u0006\u0004\b)\u0010*J\u000f\u0010+\u001a\u00020\u000bH\u0007¢\u0006\u0004\b+\u0010\rJ\u000f\u0010,\u001a\u00020\u000bH\u0007¢\u0006\u0004\b,\u0010\rJ\u000f\u0010-\u001a\u00020\u000bH\u0007¢\u0006\u0004\b-\u0010\rJ\u000f\u0010.\u001a\u00020\u000bH\u0007¢\u0006\u0004\b.\u0010\rJ\u000f\u0010/\u001a\u00020\u000bH\u0007¢\u0006\u0004\b/\u0010\rJ\u0017\u00102\u001a\u00020\u000b2\u0006\u00101\u001a\u000200H\u0007¢\u0006\u0004\b2\u00103J\u0017\u00106\u001a\u00020\u000b2\u0006\u00105\u001a\u000204H\u0007¢\u0006\u0004\b6\u00107J\u0019\u0010:\u001a\u00020\u000b2\b\u00109\u001a\u0004\u0018\u000108H\u0007¢\u0006\u0004\b:\u0010;J\u001b\u0010>\u001a\u00020\u000b2\n\u0010=\u001a\u000608j\u0002`<H\u0007¢\u0006\u0004\b>\u0010;J\u0017\u0010A\u001a\u00020\u000b2\u0006\u0010@\u001a\u00020?H\u0007¢\u0006\u0004\bA\u0010BJ\u000f\u0010C\u001a\u00020\u000bH\u0017¢\u0006\u0004\bC\u0010\rR\u001e\u0010D\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00048\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bD\u0010ER\u0016\u0010G\u001a\u00020F8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bG\u0010HR\u001a\u0010I\u001a\u00060\u0003j\u0002`\u00048\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bI\u0010JR\u001a\u0010\u0005\u001a\u00060\u0003j\u0002`\u00048\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0005\u0010JR\u0016\u0010L\u001a\u00020K8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bL\u0010MR\u0016\u0010O\u001a\u00020N8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bO\u0010PR\u001e\u0010R\u001a\n\u0018\u000108j\u0004\u0018\u0001`Q8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bR\u0010SR\u001a\u0010T\u001a\u00060\u0003j\u0002`!8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bT\u0010JR\u0016\u0010V\u001a\u00020U8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bV\u0010W¨\u0006\\"}, d2 = {"Lcom/discord/stores/StoreVoiceChannelSelected;", "Lcom/discord/stores/StoreV2;", "Lcom/discord/stores/DispatchHandler;", "", "Lcom/discord/primitives/ChannelId;", "selectedVoiceChannelId", "", "forceMoved", "Lcom/discord/stores/StoreVoiceChannelSelected$JoinVoiceChannelResult;", "selectVoiceChannelInternal", "(JZ)Lcom/discord/stores/StoreVoiceChannelSelected$JoinVoiceChannelResult;", "", "clearInternal", "()V", "validateSelectedVoiceChannel", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/api/guild/GuildVerificationLevel;", "getVerificationLevelTriggered", "(Lcom/discord/models/guild/Guild;)Lcom/discord/api/guild/GuildVerificationLevel;", "channelId", "Lcom/discord/utilities/voice/VoiceChannelJoinability;", "getJoinability", "(J)Lcom/discord/utilities/voice/VoiceChannelJoinability;", "getSelectedVoiceChannelId", "()J", "Lcom/discord/api/channel/Channel;", "getSelectedVoiceChannel", "()Lcom/discord/api/channel/Channel;", "getLastSelectedVoiceChannelId", "Lrx/Observable;", "observeSelectedVoiceChannelId", "()Lrx/Observable;", "Lcom/discord/primitives/Timestamp;", "observeTimeSelectedMs", "observeSelectedChannel", "selectVoiceChannel", "(J)Lrx/Observable;", "clear", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "handleGuildRemove", "handleChannelOrThreadCreateOrUpdate", "handleChannelOrThreadDelete", "handleGuildRoleRemove", "handleGuildRoleCreateOrUpdate", "Lcom/discord/api/guildmember/GuildMember;", "member", "handleGuildMemberAdd", "(Lcom/discord/api/guildmember/GuildMember;)V", "Lcom/discord/api/voice/state/VoiceState;", "voiceState", "handleVoiceStateUpdates", "(Lcom/discord/api/voice/state/VoiceState;)V", "", "authToken", "handleAuthToken", "(Ljava/lang/String;)V", "Lcom/discord/primitives/StreamKey;", "streamKey", "handleStreamTargeted", "Lcom/discord/rtcconnection/RtcConnection$State;", "state", "handleRtcConnectionStateChanged", "(Lcom/discord/rtcconnection/RtcConnection$State;)V", "snapshotData", "preselectedVoiceChannelId", "Ljava/lang/Long;", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "lastSelectedVoiceChannelId", "J", "Lcom/discord/stores/StoreStream;", "stream", "Lcom/discord/stores/StoreStream;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/primitives/SessionId;", "sessionId", "Ljava/lang/String;", "timeSelectedMs", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;Lcom/discord/stores/updates/ObservationDeck;)V", "Companion", "JoinVoiceChannelResult", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreVoiceChannelSelected extends StoreV2 implements DispatchHandler {
    public static final Companion Companion = new Companion(null);
    public static final long VOICE_CHANNEL_ID_NONE = 0;
    private final Clock clock;
    private final Dispatcher dispatcher;
    private long lastSelectedVoiceChannelId;
    private final ObservationDeck observationDeck;
    private Long preselectedVoiceChannelId;
    private long selectedVoiceChannelId;
    private String sessionId;
    private final StoreStream stream;
    private long timeSelectedMs;

    /* compiled from: StoreVoiceChannelSelected.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/stores/StoreVoiceChannelSelected$Companion;", "", "", "VOICE_CHANNEL_ID_NONE", "J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreVoiceChannelSelected.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\n\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\n¨\u0006\u000b"}, d2 = {"Lcom/discord/stores/StoreVoiceChannelSelected$JoinVoiceChannelResult;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "SUCCESS", "DEFERRED_UNTIL_SESSION_START", "ALREADY_CONNECTED", "FAILED_PERMISSIONS_MISSING", "FAILED_GUILD_VIDEO_AT_CAPACITY", "FAILED_CHANNEL_FULL", "FAILED_CHANNEL_DOES_NOT_EXIST", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum JoinVoiceChannelResult {
        SUCCESS,
        DEFERRED_UNTIL_SESSION_START,
        ALREADY_CONNECTED,
        FAILED_PERMISSIONS_MISSING,
        FAILED_GUILD_VIDEO_AT_CAPACITY,
        FAILED_CHANNEL_FULL,
        FAILED_CHANNEL_DOES_NOT_EXIST
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;
        public static final /* synthetic */ int[] $EnumSwitchMapping$2;

        static {
            VoiceChannelJoinability.values();
            int[] iArr = new int[5];
            $EnumSwitchMapping$0 = iArr;
            VoiceChannelJoinability voiceChannelJoinability = VoiceChannelJoinability.CAN_JOIN;
            iArr[voiceChannelJoinability.ordinal()] = 1;
            VoiceChannelJoinability voiceChannelJoinability2 = VoiceChannelJoinability.PERMISSIONS_MISSING;
            iArr[voiceChannelJoinability2.ordinal()] = 2;
            VoiceChannelJoinability voiceChannelJoinability3 = VoiceChannelJoinability.CHANNEL_FULL;
            iArr[voiceChannelJoinability3.ordinal()] = 3;
            VoiceChannelJoinability voiceChannelJoinability4 = VoiceChannelJoinability.GUILD_VIDEO_AT_CAPACITY;
            iArr[voiceChannelJoinability4.ordinal()] = 4;
            VoiceChannelJoinability voiceChannelJoinability5 = VoiceChannelJoinability.CHANNEL_DOES_NOT_EXIST;
            iArr[voiceChannelJoinability5.ordinal()] = 5;
            VoiceChannelJoinability.values();
            int[] iArr2 = new int[5];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[voiceChannelJoinability2.ordinal()] = 1;
            iArr2[voiceChannelJoinability4.ordinal()] = 2;
            iArr2[voiceChannelJoinability3.ordinal()] = 3;
            iArr2[voiceChannelJoinability5.ordinal()] = 4;
            iArr2[voiceChannelJoinability.ordinal()] = 5;
            GuildVerificationLevel.values();
            int[] iArr3 = new int[5];
            $EnumSwitchMapping$2 = iArr3;
            iArr3[GuildVerificationLevel.HIGHEST.ordinal()] = 1;
            iArr3[GuildVerificationLevel.HIGH.ordinal()] = 2;
            iArr3[GuildVerificationLevel.MEDIUM.ordinal()] = 3;
            iArr3[GuildVerificationLevel.LOW.ordinal()] = 4;
            iArr3[GuildVerificationLevel.NONE.ordinal()] = 5;
        }
    }

    public /* synthetic */ StoreVoiceChannelSelected(StoreStream storeStream, Dispatcher dispatcher, Clock clock, ObservationDeck observationDeck, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(storeStream, dispatcher, clock, (i & 8) != 0 ? ObservationDeckProvider.get() : observationDeck);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void clearInternal() {
        Channel channel = StoreStream.Companion.getChannels().getChannel(this.selectedVoiceChannelId);
        if (channel != null && ChannelUtils.F(channel)) {
            AppSoundManager.Provider.INSTANCE.get().play(AppSound.Companion.getSOUND_USER_LEFT());
        }
        selectVoiceChannelInternal(0L, false);
    }

    @StoreThread
    private final VoiceChannelJoinability getJoinability(long j) {
        GuildVerificationLevel guildVerificationLevel;
        GuildMaxVideoChannelUsers guildMaxVideoChannelUsers;
        StoreChannels channels$app_productionGoogleRelease = this.stream.getChannels$app_productionGoogleRelease();
        StoreGuilds guilds$app_productionGoogleRelease = this.stream.getGuilds$app_productionGoogleRelease();
        StorePermissions permissions$app_productionGoogleRelease = this.stream.getPermissions$app_productionGoogleRelease();
        StoreVoiceStates voiceStates$app_productionGoogleRelease = this.stream.getVoiceStates$app_productionGoogleRelease();
        StoreStageInstances stageInstances$app_productionGoogleRelease = this.stream.getStageInstances$app_productionGoogleRelease();
        Channel findChannelByIdInternal$app_productionGoogleRelease = channels$app_productionGoogleRelease.findChannelByIdInternal$app_productionGoogleRelease(j);
        if (findChannelByIdInternal$app_productionGoogleRelease == null) {
            return VoiceChannelJoinability.CHANNEL_DOES_NOT_EXIST;
        }
        StageInstance stageInstanceForChannelInternal = stageInstances$app_productionGoogleRelease.getStageInstanceForChannelInternal(j);
        Map map = (Map) a.u0(findChannelByIdInternal$app_productionGoogleRelease, voiceStates$app_productionGoogleRelease.get());
        if (map == null) {
            map = h0.emptyMap();
        }
        Long l = permissions$app_productionGoogleRelease.getPermissionsByChannel().get(Long.valueOf(j));
        Guild guild = (Guild) a.u0(findChannelByIdInternal$app_productionGoogleRelease, guilds$app_productionGoogleRelease.getGuilds());
        if (guild != null) {
            guildVerificationLevel = getVerificationLevelTriggered(guild);
        } else {
            guildVerificationLevel = GuildVerificationLevel.NONE;
        }
        GuildVerificationLevel guildVerificationLevel2 = guildVerificationLevel;
        VoiceChannelJoinabilityUtils voiceChannelJoinabilityUtils = VoiceChannelJoinabilityUtils.INSTANCE;
        Collection<VoiceState> values = map.values();
        if (guild == null || (guildMaxVideoChannelUsers = guild.getMaxVideoChannelUsers()) == null) {
            guildMaxVideoChannelUsers = GuildMaxVideoChannelUsers.Unlimited.INSTANCE;
        }
        return voiceChannelJoinabilityUtils.computeJoinability(findChannelByIdInternal$app_productionGoogleRelease, values, l, guildMaxVideoChannelUsers, guildVerificationLevel2, Long.valueOf(this.selectedVoiceChannelId), stageInstanceForChannelInternal);
    }

    @StoreThread
    private final GuildVerificationLevel getVerificationLevelTriggered(Guild guild) {
        StoreGuilds guilds$app_productionGoogleRelease = this.stream.getGuilds$app_productionGoogleRelease();
        StoreUser users$app_productionGoogleRelease = this.stream.getUsers$app_productionGoogleRelease();
        GuildVerificationLevel verificationLevel = guild.getVerificationLevel();
        Map map = (Map) a.d(guild, guilds$app_productionGoogleRelease.getMembers());
        MeUser me2 = users$app_productionGoogleRelease.getMe();
        GuildMember guildMember = map != null ? (GuildMember) map.get(Long.valueOf(me2.getId())) : null;
        boolean isOwner = guild.isOwner(me2.getId());
        boolean z2 = guildMember != null && (guildMember.getRoles().isEmpty() ^ true);
        UserUtils userUtils = UserUtils.INSTANCE;
        boolean hasPhone = userUtils.getHasPhone(me2);
        Long l = (Long) a.d(guild, guilds$app_productionGoogleRelease.getGuildsJoinedAt());
        if (isOwner || z2 || hasPhone) {
            return GuildVerificationLevel.NONE;
        }
        int ordinal = verificationLevel.ordinal();
        if (ordinal == 0) {
            return GuildVerificationLevel.NONE;
        }
        if (ordinal != 1) {
            if (ordinal != 2) {
                if (ordinal != 3) {
                    if (ordinal == 4) {
                        return GuildVerificationLevel.HIGH;
                    }
                } else if (l != null && !GuildMemberUtilsKt.isGuildMemberOldEnough(l.longValue())) {
                    return GuildVerificationLevel.HIGH;
                }
            } else if (!userUtils.isAccountOldEnough(me2, this.clock)) {
                return GuildVerificationLevel.MEDIUM;
            }
        } else if (!me2.isVerified()) {
            return GuildVerificationLevel.LOW;
        }
        return GuildVerificationLevel.NONE;
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final JoinVoiceChannelResult selectVoiceChannelInternal(long j, boolean z2) {
        boolean z3;
        Channel channel;
        if (this.sessionId == null) {
            this.preselectedVoiceChannelId = Long.valueOf(j);
            return JoinVoiceChannelResult.DEFERRED_UNTIL_SESSION_START;
        }
        this.preselectedVoiceChannelId = null;
        if (j == this.selectedVoiceChannelId) {
            return JoinVoiceChannelResult.ALREADY_CONNECTED;
        }
        VoiceChannelJoinability joinability = getJoinability(j);
        int ordinal = joinability.ordinal();
        if (ordinal != 0) {
            if (ordinal != 1) {
                if (ordinal != 2) {
                    if (ordinal != 3) {
                        if (ordinal != 4) {
                            throw new NoWhenBranchMatchedException();
                        }
                    }
                }
                z3 = false;
            }
            z3 = z2;
        } else {
            z3 = true;
        }
        long j2 = this.selectedVoiceChannelId;
        if (z3) {
            if (!z2 && (channel = StoreStream.Companion.getChannels().getChannel(j)) != null && ChannelUtils.F(channel)) {
                AppSoundManager.Provider.INSTANCE.get().play(AppSound.Companion.getSOUND_USER_JOINED());
            }
            this.selectedVoiceChannelId = j;
            if (j > 0) {
                this.lastSelectedVoiceChannelId = j;
            }
        } else {
            this.selectedVoiceChannelId = 0L;
        }
        if (j2 != this.selectedVoiceChannelId) {
            markChanged();
        }
        if (z3) {
            return JoinVoiceChannelResult.SUCCESS;
        }
        int ordinal2 = joinability.ordinal();
        if (ordinal2 == 0) {
            return JoinVoiceChannelResult.SUCCESS;
        }
        if (ordinal2 == 1) {
            return JoinVoiceChannelResult.FAILED_PERMISSIONS_MISSING;
        }
        if (ordinal2 == 2) {
            return JoinVoiceChannelResult.FAILED_GUILD_VIDEO_AT_CAPACITY;
        }
        if (ordinal2 == 3) {
            return JoinVoiceChannelResult.FAILED_CHANNEL_FULL;
        }
        if (ordinal2 == 4) {
            return JoinVoiceChannelResult.FAILED_CHANNEL_DOES_NOT_EXIST;
        }
        throw new NoWhenBranchMatchedException();
    }

    @StoreThread
    private final void validateSelectedVoiceChannel() {
        long j = this.selectedVoiceChannelId;
        if (j != 0) {
            if (this.stream.getChannels$app_productionGoogleRelease().findChannelByIdInternal$app_productionGoogleRelease(this.selectedVoiceChannelId) == null) {
                clearInternal();
            }
            if (this.selectedVoiceChannelId != j) {
                markChanged();
            }
        }
    }

    public final void clear() {
        this.dispatcher.schedule(new StoreVoiceChannelSelected$clear$1(this));
    }

    public final long getLastSelectedVoiceChannelId() {
        return this.lastSelectedVoiceChannelId;
    }

    public final Channel getSelectedVoiceChannel() {
        return this.stream.getChannels$app_productionGoogleRelease().findChannelByIdInternal$app_productionGoogleRelease(this.selectedVoiceChannelId);
    }

    public final long getSelectedVoiceChannelId() {
        return this.selectedVoiceChannelId;
    }

    @StoreThread
    public final void handleAuthToken(String str) {
        if (str == null) {
            clearInternal();
        }
    }

    @StoreThread
    public final void handleChannelOrThreadCreateOrUpdate() {
        validateSelectedVoiceChannel();
    }

    @StoreThread
    public final void handleChannelOrThreadDelete() {
        validateSelectedVoiceChannel();
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        this.sessionId = modelPayload.getSessionId();
        Long l = this.preselectedVoiceChannelId;
        if (l != null) {
            selectVoiceChannelInternal(l.longValue(), false);
        }
    }

    @StoreThread
    public final void handleGuildMemberAdd(com.discord.api.guildmember.GuildMember guildMember) {
        m.checkNotNullParameter(guildMember, "member");
        if (guildMember.m().i() == this.stream.getUsers$app_productionGoogleRelease().getMeInternal$app_productionGoogleRelease().getId()) {
            validateSelectedVoiceChannel();
        }
    }

    @StoreThread
    public final void handleGuildRemove() {
        validateSelectedVoiceChannel();
    }

    @StoreThread
    public final void handleGuildRoleCreateOrUpdate() {
        validateSelectedVoiceChannel();
    }

    @StoreThread
    public final void handleGuildRoleRemove() {
        validateSelectedVoiceChannel();
    }

    @StoreThread
    public final void handleRtcConnectionStateChanged(RtcConnection.State state) {
        m.checkNotNullParameter(state, "state");
        if (state instanceof RtcConnection.State.f) {
            Channel findChannelByIdInternal$app_productionGoogleRelease = this.stream.getChannels$app_productionGoogleRelease().findChannelByIdInternal$app_productionGoogleRelease(this.selectedVoiceChannelId);
            Guild guild = this.stream.getGuilds$app_productionGoogleRelease().getGuildsInternal$app_productionGoogleRelease().get(findChannelByIdInternal$app_productionGoogleRelease != null ? Long.valueOf(findChannelByIdInternal$app_productionGoogleRelease.f()) : null);
            if (guild != null) {
                Long afkChannelId = guild.getAfkChannelId();
                long j = this.selectedVoiceChannelId;
                if (afkChannelId != null && afkChannelId.longValue() == j && this.stream.getMediaEngine$app_productionGoogleRelease().getSelectedVideoInputDeviceBlocking() != null) {
                    this.stream.getMediaEngine$app_productionGoogleRelease().selectVideoInputDevice(null);
                }
            }
        }
    }

    @StoreThread
    public final void handleStreamTargeted(String str) {
        m.checkNotNullParameter(str, "streamKey");
        selectVoiceChannelInternal(ModelApplicationStream.Companion.decodeStreamKey(str).getChannelId(), false);
    }

    @StoreThread
    public final void handleVoiceStateUpdates(VoiceState voiceState) {
        Channel findChannelByIdInternal$app_productionGoogleRelease;
        m.checkNotNullParameter(voiceState, "voiceState");
        if (this.stream.getUsers$app_productionGoogleRelease().getMeInternal$app_productionGoogleRelease().getId() == voiceState.m() && (findChannelByIdInternal$app_productionGoogleRelease = this.stream.getChannels$app_productionGoogleRelease().findChannelByIdInternal$app_productionGoogleRelease(this.selectedVoiceChannelId)) != null && findChannelByIdInternal$app_productionGoogleRelease.f() == voiceState.c()) {
            Long a = voiceState.a();
            long j = this.selectedVoiceChannelId;
            if ((a != null && a.longValue() == j) || !m.areEqual(voiceState.k(), this.sessionId)) {
                if (!m.areEqual(voiceState.k(), this.sessionId)) {
                    clearInternal();
                }
            } else if (a != null) {
                selectVoiceChannelInternal(a.longValue(), true);
                AppSoundManager.Provider.INSTANCE.get().play(AppSound.Companion.getSOUND_USER_MOVED());
            } else {
                clearInternal();
            }
        }
    }

    public final Observable<Channel> observeSelectedChannel() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this, this.stream.getChannels$app_productionGoogleRelease()}, false, null, null, new StoreVoiceChannelSelected$observeSelectedChannel$1(this), 14, null);
    }

    public final Observable<Long> observeSelectedVoiceChannelId() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreVoiceChannelSelected$observeSelectedVoiceChannelId$1(this), 14, null);
    }

    public final Observable<Long> observeTimeSelectedMs() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreVoiceChannelSelected$observeTimeSelectedMs$1(this), 14, null);
    }

    public final Observable<JoinVoiceChannelResult> selectVoiceChannel(long j) {
        Channel channel = this.stream.getChannels$app_productionGoogleRelease().getChannel(j);
        if (channel != null && ChannelUtils.z(channel)) {
            StoreStream.Companion.getMediaEngine().setAudioInputEnabled(false);
        }
        PublishSubject k0 = PublishSubject.k0();
        this.dispatcher.schedule(new StoreVoiceChannelSelected$selectVoiceChannel$1(this, j, k0));
        m.checkNotNullExpressionValue(k0, "resultSubject");
        return k0;
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        super.snapshotData();
        long j = 0;
        if (this.selectedVoiceChannelId > 0) {
            j = this.clock.currentTimeMillis();
        }
        this.timeSelectedMs = j;
    }

    public StoreVoiceChannelSelected(StoreStream storeStream, Dispatcher dispatcher, Clock clock, ObservationDeck observationDeck) {
        m.checkNotNullParameter(storeStream, "stream");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.stream = storeStream;
        this.dispatcher = dispatcher;
        this.clock = clock;
        this.observationDeck = observationDeck;
    }
}
