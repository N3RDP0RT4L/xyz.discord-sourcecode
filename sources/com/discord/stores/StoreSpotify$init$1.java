package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.models.domain.spotify.ModelSpotifyTrack;
import j0.k.b;
import j0.l.e.k;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: StoreSpotify.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0006\u001a\u001e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0000 \u0003*\u000e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0000\u0018\u00010\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/models/domain/spotify/ModelSpotifyTrack;", "track", "Lrx/Observable;", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/domain/spotify/ModelSpotifyTrack;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreSpotify$init$1<T, R> implements b<ModelSpotifyTrack, Observable<? extends ModelSpotifyTrack>> {
    public static final StoreSpotify$init$1 INSTANCE = new StoreSpotify$init$1();

    /* compiled from: StoreSpotify.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u0001\n\u0002\b\u0003\u0010\u0006\u001a\u0004\u0018\u00010\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Long;)Ljava/lang/Void;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreSpotify$init$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1<T, R> implements b {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public final Void call(Long l) {
            return null;
        }
    }

    public final Observable<? extends ModelSpotifyTrack> call(ModelSpotifyTrack modelSpotifyTrack) {
        if (modelSpotifyTrack != null) {
            return new k(modelSpotifyTrack);
        }
        return (Observable<R>) Observable.d0(1L, TimeUnit.SECONDS).F(AnonymousClass1.INSTANCE);
    }
}
