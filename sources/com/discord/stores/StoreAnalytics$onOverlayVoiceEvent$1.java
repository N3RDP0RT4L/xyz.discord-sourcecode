package com.discord.stores;

import com.discord.api.channel.Channel;
import com.discord.rtcconnection.RtcConnection;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.AnalyticsUtils;
import d0.t.h0;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreAnalytics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAnalytics$onOverlayVoiceEvent$1 extends o implements Function0<Unit> {
    public final /* synthetic */ boolean $isActive;
    public final /* synthetic */ StoreAnalytics this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreAnalytics$onOverlayVoiceEvent$1(StoreAnalytics storeAnalytics, boolean z2) {
        super(0);
        this.this$0 = storeAnalytics;
        this.$isActive = z2;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreStream storeStream;
        Map<String, String> map;
        Channel channel;
        Channel channel2;
        storeStream = this.this$0.stores;
        RtcConnection rtcConnection$app_productionGoogleRelease = storeStream.getRtcConnection$app_productionGoogleRelease().getRtcConnection$app_productionGoogleRelease();
        if (rtcConnection$app_productionGoogleRelease == null || (map = AnalyticsUtils.INSTANCE.getProperties$app_productionGoogleRelease(rtcConnection$app_productionGoogleRelease)) == null) {
            map = h0.emptyMap();
        }
        if (this.$isActive) {
            channel = this.this$0.selectedVoiceChannel;
            Map<String, Object> map2 = null;
            Map guildAnalyticsPropertiesInternal = channel != null ? this.this$0.getGuildAnalyticsPropertiesInternal(channel.f()) : null;
            if (guildAnalyticsPropertiesInternal == null) {
                guildAnalyticsPropertiesInternal = h0.emptyMap();
            }
            Map plus = h0.plus(guildAnalyticsPropertiesInternal, map);
            channel2 = this.this$0.selectedVoiceChannel;
            if (channel2 != null) {
                map2 = AnalyticsUtils.INSTANCE.getProperties$app_productionGoogleRelease(channel2);
            }
            if (map2 == null) {
                map2 = h0.emptyMap();
            }
            AnalyticsTracker.INSTANCE.overlayVoiceOpened(h0.plus(plus, map2));
            return;
        }
        AnalyticsTracker.INSTANCE.overlayVoiceClosed(map);
    }
}
