package com.discord.stores;

import com.discord.api.channel.Channel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function2;
/* compiled from: ReadyPayloadUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0005\u0010\u0005\u001a\u00020\u00002\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/channel/Channel;", "channel", "channelUpdate", "invoke", "(Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;)Lcom/discord/api/channel/Channel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ReadyPayloadUtils$hydrateGuild$6 extends o implements Function2<Channel, Channel, Channel> {
    public static final ReadyPayloadUtils$hydrateGuild$6 INSTANCE = new ReadyPayloadUtils$hydrateGuild$6();

    public ReadyPayloadUtils$hydrateGuild$6() {
        super(2);
    }

    public final Channel invoke(Channel channel, Channel channel2) {
        Channel hydrate;
        m.checkNotNullParameter(channel, "channel");
        m.checkNotNullParameter(channel2, "channelUpdate");
        hydrate = ReadyPayloadUtils.INSTANCE.hydrate(channel, channel2);
        return hydrate;
    }
}
