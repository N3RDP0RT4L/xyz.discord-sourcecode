package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.stores.StoreAnalytics;
import com.discord.widgets.home.WidgetHome;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: StoreAnalytics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a*\u0012\u000e\b\u0001\u0012\n \u0001*\u0004\u0018\u00010\u00000\u0000 \u0001*\u0014\u0012\u000e\b\u0001\u0012\n \u0001*\u0004\u0018\u00010\u00000\u0000\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/stores/StoreAnalytics$ScreenViewed;", "kotlin.jvm.PlatformType", "screenViewed", "Lrx/Observable;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/stores/StoreAnalytics$ScreenViewed;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAnalytics$init$3<T, R> implements b<StoreAnalytics.ScreenViewed, Observable<? extends StoreAnalytics.ScreenViewed>> {
    public static final StoreAnalytics$init$3 INSTANCE = new StoreAnalytics$init$3();

    public final Observable<? extends StoreAnalytics.ScreenViewed> call(final StoreAnalytics.ScreenViewed screenViewed) {
        if (m.areEqual(screenViewed.getScreen(), WidgetHome.class)) {
            return (Observable<R>) Observable.d0(2L, TimeUnit.SECONDS).F(new b<Long, StoreAnalytics.ScreenViewed>() { // from class: com.discord.stores.StoreAnalytics$init$3.1
                public final StoreAnalytics.ScreenViewed call(Long l) {
                    return StoreAnalytics.ScreenViewed.this;
                }
            });
        }
        return new k(screenViewed);
    }
}
