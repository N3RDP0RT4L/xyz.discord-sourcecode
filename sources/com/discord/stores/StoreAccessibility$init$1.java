package com.discord.stores;

import com.discord.utilities.accessibility.AccessibilityState;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreAccessibility.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/accessibility/AccessibilityState;", "it", "", "invoke", "(Lcom/discord/utilities/accessibility/AccessibilityState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAccessibility$init$1 extends o implements Function1<AccessibilityState, Unit> {
    public final /* synthetic */ StoreAccessibility this$0;

    /* compiled from: StoreAccessibility.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreAccessibility$init$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ AccessibilityState $it;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(AccessibilityState accessibilityState) {
            super(0);
            this.$it = accessibilityState;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            StoreAccessibility$init$1.this.this$0.updateMonitoredAccessibilityState(this.$it);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreAccessibility$init$1(StoreAccessibility storeAccessibility) {
        super(1);
        this.this$0 = storeAccessibility;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(AccessibilityState accessibilityState) {
        invoke2(accessibilityState);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(AccessibilityState accessibilityState) {
        Dispatcher dispatcher;
        m.checkNotNullParameter(accessibilityState, "it");
        dispatcher = this.this$0.dispatcher;
        dispatcher.schedule(new AnonymousClass1(accessibilityState));
    }
}
