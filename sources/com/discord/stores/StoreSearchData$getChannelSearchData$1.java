package com.discord.stores;

import com.discord.api.channel.Channel;
import com.discord.models.member.GuildMember;
import com.discord.models.user.MeUser;
import com.discord.utilities.search.validation.SearchData;
import d0.t.h0;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreSearchData.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/utilities/search/validation/SearchData;", "invoke", "()Lcom/discord/utilities/search/validation/SearchData;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreSearchData$getChannelSearchData$1 extends o implements Function0<SearchData> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ StoreSearchData this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreSearchData$getChannelSearchData$1(StoreSearchData storeSearchData, long j) {
        super(0);
        this.this$0 = storeSearchData;
        this.$channelId = j;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final SearchData invoke() {
        StoreChannels storeChannels;
        StoreUser storeUser;
        StoreGuilds storeGuilds;
        storeChannels = this.this$0.storeChannels;
        Channel channel = storeChannels.getChannel(this.$channelId);
        storeUser = this.this$0.storeUser;
        MeUser me2 = storeUser.getMe();
        storeGuilds = this.this$0.storeGuilds;
        Map<Long, GuildMember> map = storeGuilds.getMembers().get(channel != null ? Long.valueOf(channel.f()) : null);
        if (map == null) {
            map = h0.emptyMap();
        }
        return new SearchData.Builder().buildForChannel(channel, me2, map);
    }
}
