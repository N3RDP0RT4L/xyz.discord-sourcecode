package com.discord.stores;

import com.discord.api.user.UserSurvey;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreUserSurvey.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/api/user/UserSurvey;", "invoke", "()Lcom/discord/api/user/UserSurvey;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserSurvey$observeUserSurvey$1 extends o implements Function0<UserSurvey> {
    public final /* synthetic */ StoreUserSurvey this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreUserSurvey$observeUserSurvey$1(StoreUserSurvey storeUserSurvey) {
        super(0);
        this.this$0 = storeUserSurvey;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final UserSurvey invoke() {
        UserSurvey userSurvey;
        userSurvey = this.this$0.getUserSurvey();
        return userSurvey;
    }
}
