package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.models.domain.ModelGuildFolder;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreGuildsSorted;
import d0.z.d.m;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import rx.functions.Func4;
/* compiled from: StoreGuildsSorted.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0010\u001a\n \u0004*\u0004\u0018\u00010\r0\r2.\u0010\u0005\u001a*\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003 \u0004*\u0014\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00000\u00002\"\u0010\u0007\u001a\u001e\u0012\b\u0012\u00060\u0001j\u0002`\u0002 \u0004*\u000e\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0018\u00010\u00060\u000626\u0010\t\u001a2\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\b\u0012\u00060\u0001j\u0002`\b \u0004*\u0018\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\b\u0012\u00060\u0001j\u0002`\b\u0018\u00010\u00000\u00002\u001a\u0010\f\u001a\u0016\u0012\u0004\u0012\u00020\u000b \u0004*\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\n0\nH\n¢\u0006\u0004\b\u000e\u0010\u000f"}, d2 = {"", "", "Lcom/discord/primitives/GuildId;", "Lcom/discord/models/guild/Guild;", "kotlin.jvm.PlatformType", "<anonymous parameter 0>", "", "mutedGuilds", "Lcom/discord/primitives/Timestamp;", "joinedAt", "", "Lcom/discord/models/domain/ModelGuildFolder;", "folders", "Lcom/discord/stores/StoreGuildsSorted$State;", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Map;Ljava/util/Set;Ljava/util/Map;Ljava/util/List;)Lcom/discord/stores/StoreGuildsSorted$State;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildsSorted$observeStores$2<T1, T2, T3, T4, R> implements Func4<Map<Long, ? extends Guild>, Set<? extends Long>, Map<Long, ? extends Long>, List<? extends ModelGuildFolder>, StoreGuildsSorted.State> {
    public static final StoreGuildsSorted$observeStores$2 INSTANCE = new StoreGuildsSorted$observeStores$2();

    @Override // rx.functions.Func4
    public /* bridge */ /* synthetic */ StoreGuildsSorted.State call(Map<Long, ? extends Guild> map, Set<? extends Long> set, Map<Long, ? extends Long> map2, List<? extends ModelGuildFolder> list) {
        return call2((Map<Long, Guild>) map, (Set<Long>) set, (Map<Long, Long>) map2, (List<ModelGuildFolder>) list);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final StoreGuildsSorted.State call2(Map<Long, Guild> map, Set<Long> set, Map<Long, Long> map2, List<ModelGuildFolder> list) {
        m.checkNotNullExpressionValue(set, "mutedGuilds");
        m.checkNotNullExpressionValue(map2, "joinedAt");
        m.checkNotNullExpressionValue(list, "folders");
        return new StoreGuildsSorted.State(set, map2, list);
    }
}
