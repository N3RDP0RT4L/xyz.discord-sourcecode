package com.discord.stores;

import com.discord.models.domain.ModelPaymentSource;
import com.discord.models.domain.PaymentSourceRaw;
import com.discord.stores.StorePaymentSources;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import rx.Observable;
/* compiled from: StorePaymentSources.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StorePaymentSources$fetchPaymentSources$1 extends o implements Function0<Unit> {
    public final /* synthetic */ StorePaymentSources this$0;

    /* compiled from: StorePaymentSources.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "Lcom/discord/models/domain/ModelPaymentSource;", "kotlin.jvm.PlatformType", "paymentSources", "", "invoke", "(Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StorePaymentSources$fetchPaymentSources$1$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2 extends o implements Function1<List<? extends ModelPaymentSource>, Unit> {

        /* compiled from: StorePaymentSources.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StorePaymentSources$fetchPaymentSources$1$2$1  reason: invalid class name */
        /* loaded from: classes.dex */
        public static final class AnonymousClass1 extends o implements Function0<Unit> {
            public final /* synthetic */ List $paymentSources;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public AnonymousClass1(List list) {
                super(0);
                this.$paymentSources = list;
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                StorePaymentSources storePaymentSources = StorePaymentSources$fetchPaymentSources$1.this.this$0;
                List list = this.$paymentSources;
                m.checkNotNullExpressionValue(list, "paymentSources");
                storePaymentSources.handlePaymentSourcesFetchSuccess(list);
            }
        }

        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(List<? extends ModelPaymentSource> list) {
            invoke2(list);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(List<? extends ModelPaymentSource> list) {
            Dispatcher dispatcher;
            dispatcher = StorePaymentSources$fetchPaymentSources$1.this.this$0.dispatcher;
            dispatcher.schedule(new AnonymousClass1(list));
        }
    }

    /* compiled from: StorePaymentSources.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StorePaymentSources$fetchPaymentSources$1$3  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass3 extends o implements Function1<Error, Unit> {

        /* compiled from: StorePaymentSources.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StorePaymentSources$fetchPaymentSources$1$3$1  reason: invalid class name */
        /* loaded from: classes.dex */
        public static final class AnonymousClass1 extends o implements Function0<Unit> {
            public AnonymousClass1() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                StorePaymentSources$fetchPaymentSources$1.this.this$0.handlePaymentSourcesFetchFailure();
            }
        }

        public AnonymousClass3() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Error error) {
            invoke2(error);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Error error) {
            Dispatcher dispatcher;
            m.checkNotNullParameter(error, "it");
            dispatcher = StorePaymentSources$fetchPaymentSources$1.this.this$0.dispatcher;
            dispatcher.schedule(new AnonymousClass1());
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StorePaymentSources$fetchPaymentSources$1(StorePaymentSources storePaymentSources) {
        super(0);
        this.this$0 = storePaymentSources;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StorePaymentSources.PaymentSourcesState paymentSourcesState;
        RestAPI restAPI;
        paymentSourcesState = this.this$0.paymentSourcesState;
        if (!(paymentSourcesState instanceof StorePaymentSources.PaymentSourcesState.Loading)) {
            this.this$0.handlePaymentSourcesFetchStart();
            restAPI = this.this$0.restAPI;
            Observable F = ObservableExtensionsKt.restSubscribeOn$default(restAPI.getPaymentSources(), false, 1, null).F(new b<List<? extends PaymentSourceRaw>, List<? extends ModelPaymentSource>>() { // from class: com.discord.stores.StorePaymentSources$fetchPaymentSources$1.1
                @Override // j0.k.b
                public /* bridge */ /* synthetic */ List<? extends ModelPaymentSource> call(List<? extends PaymentSourceRaw> list) {
                    return call2((List<PaymentSourceRaw>) list);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final List<ModelPaymentSource> call2(List<PaymentSourceRaw> list) {
                    List<ModelPaymentSource> ensureDefaultPaymentSource;
                    StorePaymentSources storePaymentSources = StorePaymentSources$fetchPaymentSources$1.this.this$0;
                    m.checkNotNullExpressionValue(list, "it");
                    ensureDefaultPaymentSource = storePaymentSources.ensureDefaultPaymentSource(list);
                    return ensureDefaultPaymentSource;
                }
            });
            m.checkNotNullExpressionValue(F, "restAPI\n          .getPa…efaultPaymentSource(it) }");
            ObservableExtensionsKt.appSubscribe(F, this.this$0.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new AnonymousClass3(), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2());
        }
    }
}
