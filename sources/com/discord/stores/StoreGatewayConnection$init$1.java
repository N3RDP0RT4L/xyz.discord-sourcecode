package com.discord.stores;

import com.discord.stores.StoreGatewayConnection;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreGatewayConnection.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/stores/StoreGatewayConnection$ClientState;", "it", "", "invoke", "(Lcom/discord/stores/StoreGatewayConnection$ClientState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGatewayConnection$init$1 extends o implements Function1<StoreGatewayConnection.ClientState, Unit> {
    public final /* synthetic */ StoreGatewayConnection this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGatewayConnection$init$1(StoreGatewayConnection storeGatewayConnection) {
        super(1);
        this.this$0 = storeGatewayConnection;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(StoreGatewayConnection.ClientState clientState) {
        invoke2(clientState);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(StoreGatewayConnection.ClientState clientState) {
        m.checkNotNullParameter(clientState, "it");
        this.this$0.handleClientStateUpdate(clientState);
    }
}
