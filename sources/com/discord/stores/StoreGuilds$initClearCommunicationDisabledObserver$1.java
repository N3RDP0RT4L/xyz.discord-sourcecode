package com.discord.stores;

import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.models.member.GuildMember;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import rx.functions.Func2;
/* compiled from: StoreGuilds.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\u0010\t\u001a\u001e\u0012\b\u0012\u00060\u0001j\u0002`\u0002 \u0003*\u000e\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0018\u00010\u00000\u00002\"\u0010\u0004\u001a\u001e\u0012\b\u0012\u00060\u0001j\u0002`\u0002 \u0003*\u000e\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0018\u00010\u00000\u00002\u000e\u0010\u0006\u001a\n \u0003*\u0004\u0018\u00010\u00050\u0005H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"", "Lcom/discord/models/member/GuildMember;", "Lcom/discord/stores/ClientGuildMember;", "kotlin.jvm.PlatformType", "members", "", "<anonymous parameter 1>", NotificationCompat.CATEGORY_CALL, "(Ljava/util/List;Ljava/lang/Long;)Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuilds$initClearCommunicationDisabledObserver$1<T1, T2, R> implements Func2<List<? extends GuildMember>, Long, List<? extends GuildMember>> {
    public static final StoreGuilds$initClearCommunicationDisabledObserver$1 INSTANCE = new StoreGuilds$initClearCommunicationDisabledObserver$1();

    @Override // rx.functions.Func2
    public /* bridge */ /* synthetic */ List<? extends GuildMember> call(List<? extends GuildMember> list, Long l) {
        return call2((List<GuildMember>) list, l);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final List<GuildMember> call2(List<GuildMember> list, Long l) {
        ArrayList Y = a.Y(list, "members");
        for (Object obj : list) {
            if (!((GuildMember) obj).isCommunicationDisabled()) {
                Y.add(obj);
            }
        }
        return Y;
    }
}
