package com.discord.stores;

import com.discord.models.domain.ModelGift;
import com.discord.stores.StoreGifting;
import com.discord.utilities.analytics.AnalyticsTracker;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreAnalytics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAnalytics$trackOpenGiftAcceptModal$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ String $giftCode;
    public final /* synthetic */ String $location;
    public final /* synthetic */ StoreAnalytics this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreAnalytics$trackOpenGiftAcceptModal$1(StoreAnalytics storeAnalytics, String str, long j, String str2) {
        super(0);
        this.this$0 = storeAnalytics;
        this.$giftCode = str;
        this.$channelId = j;
        this.$location = str2;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreStream storeStream;
        ModelGift modelGift;
        StoreStream storeStream2;
        storeStream = this.this$0.stores;
        StoreGifting.GiftState giftState = storeStream.getGifting$app_productionGoogleRelease().getKnownGifts().get(this.$giftCode);
        if (giftState instanceof StoreGifting.GiftState.Resolved) {
            modelGift = ((StoreGifting.GiftState.Resolved) giftState).getGift();
        } else if (giftState instanceof StoreGifting.GiftState.Redeeming) {
            modelGift = ((StoreGifting.GiftState.Redeeming) giftState).getGift();
        } else {
            modelGift = giftState instanceof StoreGifting.GiftState.RedeemedFailed ? ((StoreGifting.GiftState.RedeemedFailed) giftState).getGift() : null;
        }
        if (modelGift != null) {
            storeStream2 = this.this$0.stores;
            AnalyticsTracker.openGiftModal(modelGift, storeStream2.getChannels$app_productionGoogleRelease().findChannelByIdInternal$app_productionGoogleRelease(this.$channelId), this.$location);
        }
    }
}
