package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.core.content.pm.ShortcutManagerCompat;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guild.Guild;
import com.discord.api.permission.Permission;
import com.discord.models.domain.ModelPayload;
import com.discord.models.message.Message;
import com.discord.models.thread.dto.ModelThreadListSync;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.collections.CollectionExtensionsKt;
import com.discord.utilities.frecency.FrecencyTracker;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.persister.Persister;
import com.discord.utilities.search.network.state.SearchState;
import d0.d0.f;
import d0.f0.q;
import d0.t.g0;
import d0.t.h0;
import d0.t.n;
import d0.t.o;
import d0.t.r;
import d0.t.u;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreChannels.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Ä\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u001e\n\u0002\b\b\n\u0002\u0010%\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 \u009d\u00012\u00020\u0001:\u0002\u009d\u0001B\u0094\u0001\u0012\b\u0010\u0099\u0001\u001a\u00030\u0098\u0001\u0012\b\u0010\u008e\u0001\u001a\u00030\u008d\u0001\u00123\u0010\u0085\u0001\u001a.\u0012\b\u0012\u00060\u0005j\u0002`\u000b\u0012\u001f\u0012\u001d\u0012\u0019\u0012\u0017\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\t\u0012\u00070\u0005j\u0003`\u0084\u00010\r0\u00160\u0083\u0001\u0012\u0018\u0010\u0094\u0001\u001a\u0013\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0005j\u0002`\u000b0\u001e0\u0090\u0001\u0012\u000e\u0010\u0091\u0001\u001a\t\u0012\u0004\u0012\u00020\u00170\u0090\u0001\u0012\u0016\b\u0002\u0010\u008b\u0001\u001a\u000f\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u001e0\u008a\u0001¢\u0006\u0006\b\u009b\u0001\u0010\u009c\u0001J\u000f\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001b\u0010\t\u001a\u0004\u0018\u00010\b2\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006¢\u0006\u0004\b\t\u0010\nJ)\u0010\u000e\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\b0\r2\n\u0010\f\u001a\u00060\u0005j\u0002`\u000b¢\u0006\u0004\b\u000e\u0010\u000fJ\u001d\u0010\u0011\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u00100\r¢\u0006\u0004\b\u0011\u0010\u0012J)\u0010\u0015\u001a\u0004\u0018\u00010\b2\n\u0010\f\u001a\u00060\u0005j\u0002`\u000b2\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006H\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0013\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00170\u0016¢\u0006\u0004\b\u0018\u0010\u0019J#\u0010\u001a\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u00100\r0\u0016¢\u0006\u0004\b\u001a\u0010\u0019J#\u0010\u001b\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\b0\r0\u0016¢\u0006\u0004\b\u001b\u0010\u0019J\u001d\u0010\u001c\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00162\u0006\u0010\f\u001a\u00020\u0005¢\u0006\u0004\b\u001c\u0010\u001dJ1\u0010!\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\b0\r0\u00162\f\u0010 \u001a\b\u0012\u0004\u0012\u00020\u001f0\u001e¢\u0006\u0004\b!\u0010\"J\u001f\u0010$\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u00100\rH\u0001¢\u0006\u0004\b#\u0010\u0012J#\u0010%\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\b0\r0\u0016¢\u0006\u0004\b%\u0010\u0019J#\u0010&\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\b0\r0\u0016¢\u0006\u0004\b&\u0010\u0019J!\u0010'\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00162\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006¢\u0006\u0004\b'\u0010\u001dJ!\u0010(\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00162\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006¢\u0006\u0004\b(\u0010\u001dJ7\u0010*\u001a\"\u0012\u001e\u0012\u001c\u0012\b\u0012\u00060\u0005j\u0002`\u000b\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0005j\u0002`\u00060\u001e0\r0\u00162\b\b\u0002\u0010)\u001a\u00020\u0017¢\u0006\u0004\b*\u0010+J\u0019\u0010,\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u001e0\u0016¢\u0006\u0004\b,\u0010\u0019J=\u0010/\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\b0\r0\u00162\n\u0010\f\u001a\u00060\u0005j\u0002`\u000b2\n\b\u0002\u0010.\u001a\u0004\u0018\u00010-H\u0007¢\u0006\u0004\b/\u00100J%\u00101\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u001e0\u00162\n\u0010\f\u001a\u00060\u0005j\u0002`\u000b¢\u0006\u0004\b1\u0010\u001dJ5\u0010\u001a\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u00100\r0\u00162\u0010\u00103\u001a\f\u0012\b\u0012\u00060\u0005j\u0002`\u000602¢\u0006\u0004\b\u001a\u00104J)\u00106\u001a\u0004\u0018\u00010\b2\n\u0010\f\u001a\u00060\u0005j\u0002`\u000b2\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006H\u0001¢\u0006\u0004\b5\u0010\u0014J/\u00108\u001a\"\u0012\b\u0012\u00060\u0005j\u0002`\u000b\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\b0\r0\rH\u0001¢\u0006\u0004\b7\u0010\u0012J\u001f\u0010:\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\b0\rH\u0001¢\u0006\u0004\b9\u0010\u0012J-\u0010=\u001a\u0014\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\b\u0018\u00010;2\n\u0010\f\u001a\u00060\u0005j\u0002`\u000bH\u0001¢\u0006\u0004\b<\u0010\u000fJ\u001f\u0010?\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\b0\rH\u0001¢\u0006\u0004\b>\u0010\u0012J!\u0010B\u001a\b\u0012\u0004\u0012\u00020\b0\u001e2\n\u0010\f\u001a\u00060\u0005j\u0002`\u000bH\u0001¢\u0006\u0004\b@\u0010AJ!\u0010D\u001a\b\u0012\u0004\u0012\u00020\b0\u001e2\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006H\u0001¢\u0006\u0004\bC\u0010AJ\u000f\u0010E\u001a\u00020\u0002H\u0007¢\u0006\u0004\bE\u0010\u0004J\u0017\u0010G\u001a\u00020\u00022\u0006\u0010F\u001a\u00020\u0017H\u0007¢\u0006\u0004\bG\u0010HJ\u0017\u0010K\u001a\u00020\u00022\u0006\u0010J\u001a\u00020IH\u0007¢\u0006\u0004\bK\u0010LJ\u0017\u0010O\u001a\u00020\u00022\u0006\u0010N\u001a\u00020MH\u0007¢\u0006\u0004\bO\u0010PJ\u0017\u0010Q\u001a\u00020\u00022\u0006\u0010N\u001a\u00020MH\u0007¢\u0006\u0004\bQ\u0010PJ\u0017\u0010S\u001a\u00020\u00022\u0006\u0010R\u001a\u00020\bH\u0007¢\u0006\u0004\bS\u0010TJ\u0017\u0010U\u001a\u00020\u00022\u0006\u0010R\u001a\u00020\bH\u0007¢\u0006\u0004\bU\u0010TJ\u0017\u0010V\u001a\u00020\u00022\u0006\u0010R\u001a\u00020\bH\u0007¢\u0006\u0004\bV\u0010TJ\u0017\u0010X\u001a\u00020\u00022\u0006\u0010J\u001a\u00020WH\u0007¢\u0006\u0004\bX\u0010YJ\u001f\u0010]\u001a\u00020\u00022\u0006\u0010[\u001a\u00020Z2\u0006\u0010\\\u001a\u00020\u0017H\u0007¢\u0006\u0004\b]\u0010^J\u001d\u0010_\u001a\u00020\u00022\f\u0010 \u001a\b\u0012\u0004\u0012\u00020\u001f0\u001eH\u0007¢\u0006\u0004\b_\u0010`J\u0017\u0010c\u001a\u00020\u00022\u0006\u0010b\u001a\u00020aH\u0007¢\u0006\u0004\bc\u0010dJ\u000f\u0010e\u001a\u00020\u0002H\u0007¢\u0006\u0004\be\u0010\u0004J\u000f\u0010f\u001a\u00020\u0002H\u0017¢\u0006\u0004\bf\u0010\u0004J1\u0010l\u001a\u00020\u00022\u0006\u0010g\u001a\u00020\b2\f\b\u0002\u0010h\u001a\u00060\u0005j\u0002`\u000b2\n\b\u0002\u0010i\u001a\u0004\u0018\u00010\bH\u0001¢\u0006\u0004\bj\u0010kJ\u001d\u0010n\u001a\u0004\u0018\u00010\b2\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006H\u0001¢\u0006\u0004\bm\u0010\nJ\u001b\u0010o\u001a\u0004\u0018\u00010\b2\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006¢\u0006\u0004\bo\u0010\nJ-\u0010s\u001a\b\u0012\u0004\u0012\u00020\b0\u001e2\n\u0010\f\u001a\u00060\u0005j\u0002`\u000b2\n\u0010p\u001a\u00060\u0005j\u0002`\u0006H\u0001¢\u0006\u0004\bq\u0010rJ!\u0010v\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u001e0\u00162\u0006\u0010u\u001a\u00020t¢\u0006\u0004\bv\u0010wJ\u0015\u0010x\u001a\u00020\u00022\u0006\u0010R\u001a\u00020\b¢\u0006\u0004\bx\u0010TR6\u0010y\u001a\"\u0012\b\u0012\u00060\u0005j\u0002`\u000b\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\b0;0;8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\by\u0010zR\"\u0010{\u001a\u00020\u00178\u0000@\u0000X\u0080\u000e¢\u0006\u0012\n\u0004\b{\u0010|\u001a\u0004\b}\u0010~\"\u0004\b\u007f\u0010HR\u0018\u0010\u0080\u0001\u001a\u00020\u00178\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u0080\u0001\u0010|R\u0018\u0010\u0081\u0001\u001a\u00020\u00178\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u0081\u0001\u0010|R(\u0010\u0082\u0001\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\b0\r8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u0082\u0001\u0010zRE\u0010\u0085\u0001\u001a.\u0012\b\u0012\u00060\u0005j\u0002`\u000b\u0012\u001f\u0012\u001d\u0012\u0019\u0012\u0017\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\t\u0012\u00070\u0005j\u0003`\u0084\u00010\r0\u00160\u0083\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0085\u0001\u0010\u0086\u0001R(\u0010\u0087\u0001\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u00100;8\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b\u0087\u0001\u0010zR(\u0010\u0088\u0001\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u00100\r8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u0088\u0001\u0010zR(\u0010\u0089\u0001\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\b0;8\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b\u0089\u0001\u0010zR&\u0010\u008b\u0001\u001a\u000f\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u001e0\u008a\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u008b\u0001\u0010\u008c\u0001R\u001a\u0010\u008e\u0001\u001a\u00030\u008d\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u008e\u0001\u0010\u008f\u0001R \u0010\u0091\u0001\u001a\t\u0012\u0004\u0012\u00020\u00170\u0090\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0091\u0001\u0010\u0092\u0001R8\u0010\u0093\u0001\u001a\"\u0012\b\u0012\u00060\u0005j\u0002`\u000b\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\b0\r0\r8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u0093\u0001\u0010zR*\u0010\u0094\u0001\u001a\u0013\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0005j\u0002`\u000b0\u001e0\u0090\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0094\u0001\u0010\u0092\u0001R(\u0010\u0095\u0001\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\b0;8\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b\u0095\u0001\u0010zR\u0018\u0010\u0096\u0001\u001a\u00020\u00178\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u0096\u0001\u0010|R(\u0010\u0097\u0001\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\b0\r8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u0097\u0001\u0010zR\u001a\u0010\u0099\u0001\u001a\u00030\u0098\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0099\u0001\u0010\u009a\u0001¨\u0006\u009e\u0001"}, d2 = {"Lcom/discord/stores/StoreChannels;", "Lcom/discord/stores/StoreV2;", "", "updateInitializationState", "()V", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/api/channel/Channel;", "getChannel", "(J)Lcom/discord/api/channel/Channel;", "Lcom/discord/primitives/GuildId;", "guildId", "", "getChannelsForGuild", "(J)Ljava/util/Map;", "", "getChannelNames", "()Ljava/util/Map;", "getChannelInternal$app_productionGoogleRelease", "(JJ)Lcom/discord/api/channel/Channel;", "getChannelInternal", "Lrx/Observable;", "", "observeInitializedForAuthedUser", "()Lrx/Observable;", "observeNames", "observePrivateChannels", "observeDefaultChannel", "(J)Lrx/Observable;", "", "Lcom/discord/models/message/Message;", "messages", "observeThreadsFromMessages", "(Ljava/util/List;)Lrx/Observable;", "getChannelNamesInternal$app_productionGoogleRelease", "getChannelNamesInternal", "observeGuildAndPrivateChannels", "observeAllChannels", "observeChannel", "observePrivateChannel", "includeThreads", "observeIds", "(Z)Lrx/Observable;", "observeDMs", "", "type", "observeChannelsForGuild", "(JLjava/lang/Integer;)Lrx/Observable;", "observeChannelCategories", "", "channelIds", "(Ljava/util/Collection;)Lrx/Observable;", "getGuildChannelInternal$app_productionGoogleRelease", "getGuildChannelInternal", "getChannelsByGuildInternal$app_productionGoogleRelease", "getChannelsByGuildInternal", "getChannelsByIdInternal$app_productionGoogleRelease", "getChannelsByIdInternal", "", "getChannelsForGuildInternal$app_productionGoogleRelease", "getChannelsForGuildInternal", "getThreadsByIdInternal$app_productionGoogleRelease", "getThreadsByIdInternal", "getThreadsForGuildInternal$app_productionGoogleRelease", "(J)Ljava/util/List;", "getThreadsForGuildInternal", "getThreadsForChannelInternal$app_productionGoogleRelease", "getThreadsForChannelInternal", "init", "connected", "handleConnected", "(Z)V", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "Lcom/discord/api/guild/Guild;", "guild", "handleGuildAdd", "(Lcom/discord/api/guild/Guild;)V", "handleGuildRemove", "channel", "handleChannelOrThreadCreateOrUpdate", "(Lcom/discord/api/channel/Channel;)V", "handleThreadCreateOrUpdate", "handleChannelOrThreadDelete", "Lcom/discord/models/thread/dto/ModelThreadListSync;", "handleThreadListSync", "(Lcom/discord/models/thread/dto/ModelThreadListSync;)V", "Lcom/discord/api/channel/ChannelRecipient;", "recipient", "add", "handleGroupDMRecipient", "(Lcom/discord/api/channel/ChannelRecipient;Z)V", "handleMessagesLoaded", "(Ljava/util/List;)V", "Lcom/discord/utilities/search/network/state/SearchState;", "searchState", "handleSearchFinish", "(Lcom/discord/utilities/search/network/state/SearchState;)V", "handleStoreInitTimeout", "snapshotData", "thread", "fallbackGuildId", "parentChannel", "storeThread$app_productionGoogleRelease", "(Lcom/discord/api/channel/Channel;JLcom/discord/api/channel/Channel;)V", "storeThread", "findChannelByIdInternal$app_productionGoogleRelease", "findChannelByIdInternal", "findChannelById", "categoryId", "findChannelsByCategoryInternal$app_productionGoogleRelease", "(JJ)Ljava/util/List;", "findChannelsByCategoryInternal", "Landroid/content/Context;", "context", "observeDirectShareCandidates", "(Landroid/content/Context;)Lrx/Observable;", "onGroupCreated", "channelsByGuild", "Ljava/util/Map;", "initializedForAuthedUser", "Z", "getInitializedForAuthedUser$app_productionGoogleRelease", "()Z", "setInitializedForAuthedUser$app_productionGoogleRelease", "isConnectionOpen", "isStoreInitTimedOut", "threadsByIdSnapshot", "Lkotlin/Function1;", "Lcom/discord/api/permission/PermissionBit;", "observeChannelPermissionsForGuild", "Lkotlin/jvm/functions/Function1;", "channelNames", "channelNamesSnapshot", "preloadedThreads", "Lcom/discord/utilities/persister/Persister;", "channelsCache", "Lcom/discord/utilities/persister/Persister;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lkotlin/Function0;", "isAuthenticated", "Lkotlin/jvm/functions/Function0;", "channelsByGuildSnapshot", "getLurkingGuildIds", "threadsById", "handledReadyPayload", "channelsByIdSnapshot", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lcom/discord/utilities/persister/Persister;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreChannels extends StoreV2 {
    public static final Companion Companion = new Companion(null);
    private static final StoreChannels$Companion$InitializedUpdateSource$1 InitializedUpdateSource = new ObservationDeck.UpdateSource() { // from class: com.discord.stores.StoreChannels$Companion$InitializedUpdateSource$1
    };
    public static final long PRIVATE_CHANNELS_ID = 0;
    private final Map<Long, String> channelNames;
    private Map<Long, String> channelNamesSnapshot;
    private final Map<Long, Map<Long, Channel>> channelsByGuild;
    private Map<Long, ? extends Map<Long, Channel>> channelsByGuildSnapshot;
    private Map<Long, Channel> channelsByIdSnapshot;
    private final Persister<List<Channel>> channelsCache;
    private final Dispatcher dispatcher;
    private final Function0<List<Long>> getLurkingGuildIds;
    private boolean handledReadyPayload;
    private boolean initializedForAuthedUser;
    private final Function0<Boolean> isAuthenticated;
    private boolean isConnectionOpen;
    private boolean isStoreInitTimedOut;
    private final ObservationDeck observationDeck;
    private final Function1<Long, Observable<Map<Long, Long>>> observeChannelPermissionsForGuild;
    private final Map<Long, Channel> preloadedThreads;
    private final Map<Long, Channel> threadsById;
    private Map<Long, Channel> threadsByIdSnapshot;

    /* compiled from: StoreChannels.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001b\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005*\u0001\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u001a\u0010\u0007\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/stores/StoreChannels$Companion;", "", "com/discord/stores/StoreChannels$Companion$InitializedUpdateSource$1", "InitializedUpdateSource", "Lcom/discord/stores/StoreChannels$Companion$InitializedUpdateSource$1;", "", "Lcom/discord/primitives/GuildId;", "PRIVATE_CHANNELS_ID", "J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ StoreChannels(Dispatcher dispatcher, ObservationDeck observationDeck, Function1 function1, Function0 function0, Function0 function02, Persister persister, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(dispatcher, observationDeck, function1, function0, function02, (i & 32) != 0 ? new Persister("STORE_CHANNELS_V26", new ArrayList()) : persister);
    }

    public static /* synthetic */ Observable observeChannelsForGuild$default(StoreChannels storeChannels, long j, Integer num, int i, Object obj) {
        if ((i & 2) != 0) {
            num = null;
        }
        return storeChannels.observeChannelsForGuild(j, num);
    }

    public static /* synthetic */ Observable observeIds$default(StoreChannels storeChannels, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = false;
        }
        return storeChannels.observeIds(z2);
    }

    public static /* synthetic */ void storeThread$app_productionGoogleRelease$default(StoreChannels storeChannels, Channel channel, long j, Channel channel2, int i, Object obj) {
        if ((i & 2) != 0) {
            j = 0;
        }
        if ((i & 4) != 0) {
            channel2 = null;
        }
        storeChannels.storeThread$app_productionGoogleRelease(channel, j, channel2);
    }

    @StoreThread
    private final void updateInitializationState() {
        boolean z2 = this.initializedForAuthedUser;
        boolean z3 = this.isAuthenticated.invoke().booleanValue() && ((getChannelsByIdInternal$app_productionGoogleRelease().isEmpty() ^ true) || this.handledReadyPayload || this.isStoreInitTimedOut);
        if (!z2 && z3) {
            this.initializedForAuthedUser = true;
            markChanged(InitializedUpdateSource);
        }
    }

    public final Channel findChannelById(long j) {
        if (this.threadsByIdSnapshot.containsKey(Long.valueOf(j))) {
            return this.threadsByIdSnapshot.get(Long.valueOf(j));
        }
        Iterator<T> it = this.channelsByGuildSnapshot.values().iterator();
        while (it.hasNext()) {
            Map map = (Map) it.next();
            if (map.containsKey(Long.valueOf(j))) {
                return (Channel) map.get(Long.valueOf(j));
            }
        }
        return null;
    }

    @StoreThread
    public final Channel findChannelByIdInternal$app_productionGoogleRelease(long j) {
        if (this.threadsById.containsKey(Long.valueOf(j))) {
            return this.threadsById.get(Long.valueOf(j));
        }
        Iterator<T> it = this.channelsByGuild.values().iterator();
        while (it.hasNext()) {
            Map map = (Map) it.next();
            if (map.containsKey(Long.valueOf(j))) {
                return (Channel) map.get(Long.valueOf(j));
            }
        }
        return null;
    }

    @StoreThread
    public final List<Channel> findChannelsByCategoryInternal$app_productionGoogleRelease(long j, long j2) {
        Collection<Channel> values;
        Map<Long, Channel> map = this.channelsByGuild.get(Long.valueOf(j));
        if (map == null || (values = map.values()) == null) {
            return n.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        for (Object obj : values) {
            if (((Channel) obj).r() == j2) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }

    public final Channel getChannel(long j) {
        Channel channel = this.channelsByIdSnapshot.get(Long.valueOf(j));
        return channel != null ? channel : this.threadsByIdSnapshot.get(Long.valueOf(j));
    }

    @StoreThread
    public final Channel getChannelInternal$app_productionGoogleRelease(long j, long j2) {
        Channel channel;
        Map<Long, Channel> map = this.channelsByGuild.get(Long.valueOf(j));
        return (map == null || (channel = map.get(Long.valueOf(j2))) == null) ? this.threadsById.get(Long.valueOf(j2)) : channel;
    }

    public final Map<Long, String> getChannelNames() {
        return this.channelNamesSnapshot;
    }

    @StoreThread
    public final Map<Long, String> getChannelNamesInternal$app_productionGoogleRelease() {
        return this.channelNames;
    }

    @StoreThread
    public final Map<Long, Map<Long, Channel>> getChannelsByGuildInternal$app_productionGoogleRelease() {
        return this.channelsByGuild;
    }

    @StoreThread
    public final Map<Long, Channel> getChannelsByIdInternal$app_productionGoogleRelease() {
        Collection<Map<Long, Channel>> values = this.channelsByGuild.values();
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = values.iterator();
        while (it.hasNext()) {
            r.addAll(arrayList, ((Map) it.next()).values());
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(o.collectionSizeOrDefault(arrayList, 10)), 16));
        for (Object obj : arrayList) {
            linkedHashMap.put(Long.valueOf(((Channel) obj).h()), obj);
        }
        return linkedHashMap;
    }

    public final Map<Long, Channel> getChannelsForGuild(long j) {
        Map<Long, Channel> map = this.channelsByGuildSnapshot.get(Long.valueOf(j));
        return map != null ? map : h0.emptyMap();
    }

    @StoreThread
    public final Map<Long, Channel> getChannelsForGuildInternal$app_productionGoogleRelease(long j) {
        return this.channelsByGuild.get(Long.valueOf(j));
    }

    @StoreThread
    public final Channel getGuildChannelInternal$app_productionGoogleRelease(long j, long j2) {
        Map<Long, Channel> map = this.channelsByGuild.get(Long.valueOf(j));
        if (map != null) {
            return map.get(Long.valueOf(j2));
        }
        return null;
    }

    public final boolean getInitializedForAuthedUser$app_productionGoogleRelease() {
        return this.initializedForAuthedUser;
    }

    @StoreThread
    public final Map<Long, Channel> getThreadsByIdInternal$app_productionGoogleRelease() {
        return this.threadsById;
    }

    @StoreThread
    public final List<Channel> getThreadsForChannelInternal$app_productionGoogleRelease(long j) {
        Collection<Channel> values = this.threadsById.values();
        ArrayList arrayList = new ArrayList();
        for (Object obj : values) {
            if (((Channel) obj).r() == j) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }

    @StoreThread
    public final List<Channel> getThreadsForGuildInternal$app_productionGoogleRelease(long j) {
        Collection<Channel> values = this.threadsById.values();
        ArrayList arrayList = new ArrayList();
        for (Object obj : values) {
            if (((Channel) obj).f() == j) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }

    @StoreThread
    public final void handleChannelOrThreadCreateOrUpdate(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        if (!ChannelUtils.v(channel)) {
            long f = !ChannelUtils.x(channel) ? channel.f() : 0L;
            long h = channel.h();
            if (ChannelUtils.C(channel)) {
                storeThread$app_productionGoogleRelease$default(this, channel, f, null, 4, null);
            } else {
                Map<Long, Map<Long, Channel>> map = this.channelsByGuild;
                Long valueOf = Long.valueOf(f);
                Map<Long, Channel> map2 = map.get(valueOf);
                if (map2 == null) {
                    map2 = new HashMap<>();
                    map.put(valueOf, map2);
                }
                Map<Long, Channel> map3 = map2;
                Channel channel2 = map3.get(Long.valueOf(h));
                if (!m.areEqual(channel, channel2)) {
                    if (channel2 != null) {
                        map3.put(Long.valueOf(h), Channel.a(channel, null, 0, channel.f() != 0 ? channel.f() : f, null, 0L, 0L, 0L, null, channel2.w(), 0, null, 0, 0, null, 0L, 0L, null, false, 0L, null, 0, null, null, null, null, null, null, null, null, 536870651));
                    } else {
                        map3.put(Long.valueOf(h), Channel.a(channel, null, 0, channel.f() != 0 ? channel.f() : f, null, 0L, 0L, 0L, null, null, 0, null, 0, 0, null, 0L, 0L, null, false, 0L, null, 0, null, null, null, null, null, null, null, null, 536870907));
                    }
                }
                boolean o = channel.o();
                if (channel2 == null || o != channel2.o()) {
                    for (Channel channel3 : getThreadsForChannelInternal$app_productionGoogleRelease(channel.h())) {
                        storeThread$app_productionGoogleRelease(channel3, f, channel);
                    }
                }
                this.channelNames.put(Long.valueOf(h), ChannelUtils.c(channel));
            }
            markChanged();
        }
    }

    @StoreThread
    public final void handleChannelOrThreadDelete(Channel channel) {
        Map<Long, Channel> map;
        m.checkNotNullParameter(channel, "channel");
        long f = !ChannelUtils.x(channel) ? channel.f() : 0L;
        long h = channel.h();
        if (this.channelsByGuild.containsKey(Long.valueOf(f)) && (map = this.channelsByGuild.get(Long.valueOf(f))) != null && map.containsKey(Long.valueOf(h))) {
            Map<Long, Channel> map2 = this.channelsByGuild.get(Long.valueOf(f));
            if (map2 != null) {
                map2.remove(Long.valueOf(h));
            }
            markChanged();
        }
        if (this.channelNames.containsKey(Long.valueOf(h))) {
            this.channelNames.remove(Long.valueOf(h));
            markChanged();
        }
        if (this.threadsById.containsKey(Long.valueOf(h))) {
            this.threadsById.remove(Long.valueOf(h));
            markChanged();
        }
    }

    @StoreThread
    public final void handleConnected(boolean z2) {
        this.isConnectionOpen = z2;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        this.isConnectionOpen = true;
        this.channelsByGuild.clear();
        this.channelNames.clear();
        this.threadsById.clear();
        Map<Long, Map<Long, Channel>> map = this.channelsByGuild;
        Map<Long, Channel> map2 = map.get(0L);
        if (map2 == null) {
            map2 = new HashMap<>();
            map.put(0L, map2);
        }
        Map<Long, Channel> map3 = map2;
        List<Channel> privateChannels = modelPayload.getPrivateChannels();
        ArrayList<Channel> Y = a.Y(privateChannels, "payload.privateChannels");
        for (Object obj : privateChannels) {
            Channel channel = (Channel) obj;
            m.checkNotNullExpressionValue(channel, "it");
            if (!ChannelUtils.v(channel)) {
                Y.add(obj);
            }
        }
        for (Channel channel2 : Y) {
            Long valueOf = Long.valueOf(channel2.h());
            m.checkNotNullExpressionValue(channel2, "privateChannel");
            map3.put(valueOf, channel2);
        }
        List<Guild> guilds = modelPayload.getGuilds();
        m.checkNotNullExpressionValue(guilds, "payload.guilds");
        for (Guild guild : guilds) {
            Map<Long, Map<Long, Channel>> map4 = this.channelsByGuild;
            Long valueOf2 = Long.valueOf(guild.r());
            Map<Long, Channel> map5 = map4.get(valueOf2);
            if (map5 == null) {
                map5 = new HashMap<>();
                map4.put(valueOf2, map5);
            }
            Map<Long, Channel> map6 = map5;
            List<Channel> g = guild.g();
            if (g != null) {
                for (Channel channel3 : g) {
                    map6.put(Long.valueOf(channel3.h()), Channel.a(channel3, null, 0, channel3.f() != 0 ? channel3.f() : guild.r(), null, 0L, 0L, 0L, null, null, 0, null, 0, 0, null, 0L, 0L, null, false, 0L, null, 0, null, null, null, null, null, null, null, null, 536870907));
                }
            }
            List<Channel> N = guild.N();
            if (N != null) {
                for (Channel channel4 : N) {
                    if (ChannelUtils.C(channel4)) {
                        storeThread$app_productionGoogleRelease$default(this, channel4, guild.r(), null, 4, null);
                    }
                }
            }
        }
        for (Channel channel5 : this.preloadedThreads.values()) {
            if (!this.threadsById.containsKey(Long.valueOf(channel5.h()))) {
                storeThread$app_productionGoogleRelease$default(this, channel5, channel5.f(), null, 4, null);
            }
        }
        this.preloadedThreads.clear();
        for (Map.Entry<Long, Map<Long, Channel>> entry : this.channelsByGuild.entrySet()) {
            Map<Long, String> map7 = this.channelNames;
            Iterator<T> it = entry.getValue().entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry entry2 = (Map.Entry) it.next();
                map7.put(entry2.getKey(), ChannelUtils.c((Channel) entry2.getValue()));
            }
        }
        this.handledReadyPayload = true;
        updateInitializationState();
        markChanged();
    }

    /* JADX WARN: Removed duplicated region for block: B:44:0x012d  */
    @com.discord.stores.StoreThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void handleGroupDMRecipient(com.discord.api.channel.ChannelRecipient r48, boolean r49) {
        /*
            Method dump skipped, instructions count: 315
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreChannels.handleGroupDMRecipient(com.discord.api.channel.ChannelRecipient, boolean):void");
    }

    @StoreThread
    public final void handleGuildAdd(Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        Map<Long, Map<Long, Channel>> map = this.channelsByGuild;
        Long valueOf = Long.valueOf(guild.r());
        Map<Long, Channel> map2 = map.get(valueOf);
        if (map2 == null) {
            map2 = new HashMap<>();
            map.put(valueOf, map2);
        }
        Map<Long, Channel> map3 = map2;
        List<Channel> g = guild.g();
        if (g != null) {
            for (Channel channel : g) {
                map3.put(Long.valueOf(channel.h()), Channel.a(channel, null, 0, channel.f() != 0 ? channel.f() : guild.r(), null, 0L, 0L, 0L, null, null, 0, null, 0, 0, null, 0L, 0L, null, false, 0L, null, 0, null, null, null, null, null, null, null, null, 536870907));
                this.channelNames.put(Long.valueOf(channel.h()), ChannelUtils.c(channel));
            }
        }
        List<Channel> N = guild.N();
        if (N != null) {
            for (Channel channel2 : N) {
                if (ChannelUtils.C(channel2)) {
                    storeThread$app_productionGoogleRelease$default(this, channel2, guild.r(), null, 4, null);
                }
            }
        }
        markChanged();
    }

    @StoreThread
    public final void handleGuildRemove(Guild guild) {
        Set<Long> keySet;
        m.checkNotNullParameter(guild, "guild");
        long r = guild.r();
        if (this.channelsByGuild.containsKey(Long.valueOf(r))) {
            Map<Long, Channel> map = this.channelsByGuild.get(Long.valueOf(r));
            if (!(map == null || (keySet = map.keySet()) == null)) {
                for (Number number : keySet) {
                    this.channelNames.remove(Long.valueOf(number.longValue()));
                }
            }
            this.channelsByGuild.remove(Long.valueOf(guild.r()));
            r.removeAll(this.threadsById.values(), new StoreChannels$handleGuildRemove$2(guild));
        }
        markChanged();
    }

    @StoreThread
    public final void handleMessagesLoaded(List<Message> list) {
        m.checkNotNullParameter(list, "messages");
        for (Message message : list) {
            Channel thread = message.getThread();
            if (thread != null) {
                if (!this.threadsById.containsKey(Long.valueOf(thread.h())) && ChannelUtils.C(thread)) {
                    storeThread$app_productionGoogleRelease$default(this, thread, 0L, null, 6, null);
                    markChanged();
                }
                if (!this.isConnectionOpen) {
                    this.preloadedThreads.put(Long.valueOf(thread.h()), thread);
                }
            }
        }
    }

    @StoreThread
    public final void handleSearchFinish(SearchState searchState) {
        m.checkNotNullParameter(searchState, "searchState");
        List<Channel> threads = searchState.getThreads();
        if (threads != null) {
            for (Channel channel : threads) {
                if (!this.threadsById.containsKey(Long.valueOf(channel.h())) && ChannelUtils.C(channel)) {
                    storeThread$app_productionGoogleRelease$default(this, channel, 0L, null, 6, null);
                    markChanged();
                }
            }
        }
        List<Message> hits = searchState.getHits();
        if (hits != null) {
            for (Message message : hits) {
                Channel thread = message.getThread();
                if (thread != null && !this.threadsById.containsKey(Long.valueOf(thread.h())) && ChannelUtils.C(thread)) {
                    storeThread$app_productionGoogleRelease$default(this, thread, 0L, null, 6, null);
                    markChanged();
                }
            }
        }
    }

    @StoreThread
    public final void handleStoreInitTimeout() {
        this.isStoreInitTimedOut = true;
        updateInitializationState();
    }

    @StoreThread
    public final void handleThreadCreateOrUpdate(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        if (ChannelUtils.C(channel)) {
            handleChannelOrThreadCreateOrUpdate(channel);
        }
    }

    @StoreThread
    public final void handleThreadListSync(ModelThreadListSync modelThreadListSync) {
        m.checkNotNullParameter(modelThreadListSync, "payload");
        if (!modelThreadListSync.getThreads().isEmpty()) {
            for (Channel channel : modelThreadListSync.getThreads()) {
                if (ChannelUtils.C(channel)) {
                    storeThread$app_productionGoogleRelease$default(this, channel, modelThreadListSync.getGuildId(), null, 4, null);
                }
            }
            markChanged();
        }
    }

    @StoreThread
    public final void init() {
        List filterNotNull = u.filterNotNull(this.channelsCache.get());
        Map<Long, Map<Long, Channel>> map = this.channelsByGuild;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Object obj : filterNotNull) {
            Long valueOf = Long.valueOf(((Channel) obj).f());
            Object obj2 = linkedHashMap.get(valueOf);
            if (obj2 == null) {
                obj2 = new ArrayList();
                linkedHashMap.put(valueOf, obj2);
            }
            ((List) obj2).add(obj);
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap(g0.mapCapacity(linkedHashMap.size()));
        for (Map.Entry entry : linkedHashMap.entrySet()) {
            Object key = entry.getKey();
            HashMap hashMap = new HashMap();
            for (Object obj3 : (List) entry.getValue()) {
                hashMap.put(Long.valueOf(((Channel) obj3).h()), obj3);
            }
            linkedHashMap2.put(key, hashMap);
        }
        map.putAll(linkedHashMap2);
        updateInitializationState();
        markChanged();
    }

    public final Observable<Map<Long, Channel>> observeAllChannels() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreChannels$observeAllChannels$1(this), 14, null);
    }

    public final Observable<Channel> observeChannel(long j) {
        Observable<Channel> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreChannels$observeChannel$1(this, j), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck\n        …  .distinctUntilChanged()");
        return q;
    }

    public final Observable<List<Channel>> observeChannelCategories(long j) {
        Observable<List<Channel>> q = observeChannelsForGuild$default(this, j, null, 2, null).F(StoreChannels$observeChannelCategories$1.INSTANCE).q();
        m.checkNotNullExpressionValue(q, "observeChannelsForGuild(…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Map<Long, Channel>> observeChannelsForGuild(long j) {
        return observeChannelsForGuild$default(this, j, null, 2, null);
    }

    public final Observable<Map<Long, Channel>> observeChannelsForGuild(final long j, final Integer num) {
        Observable<Map<Long, Channel>> q = observeGuildAndPrivateChannels().Y(new b<Map<Long, ? extends Channel>, Observable<? extends Map<Long, ? extends Channel>>>() { // from class: com.discord.stores.StoreChannels$observeChannelsForGuild$1
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Observable<? extends Map<Long, ? extends Channel>> call(Map<Long, ? extends Channel> map) {
                return call2((Map<Long, Channel>) map);
            }

            /* JADX WARN: Code restructure failed: missing block: B:12:0x003f, code lost:
                if (r2 != r3.intValue()) goto L14;
             */
            /* JADX WARN: Removed duplicated region for block: B:19:0x0046 A[SYNTHETIC] */
            /* JADX WARN: Removed duplicated region for block: B:22:0x0012 A[SYNTHETIC] */
            /* renamed from: call  reason: avoid collision after fix types in other method */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public final rx.Observable<? extends java.util.Map<java.lang.Long, com.discord.api.channel.Channel>> call2(java.util.Map<java.lang.Long, com.discord.api.channel.Channel> r9) {
                /*
                    r8 = this;
                    java.lang.String r0 = "channels"
                    d0.z.d.m.checkNotNullExpressionValue(r9, r0)
                    java.util.LinkedHashMap r0 = new java.util.LinkedHashMap
                    r0.<init>()
                    java.util.Set r9 = r9.entrySet()
                    java.util.Iterator r9 = r9.iterator()
                L12:
                    boolean r1 = r9.hasNext()
                    if (r1 == 0) goto L52
                    java.lang.Object r1 = r9.next()
                    java.util.Map$Entry r1 = (java.util.Map.Entry) r1
                    java.lang.Object r2 = r1.getValue()
                    com.discord.api.channel.Channel r2 = (com.discord.api.channel.Channel) r2
                    long r3 = r2.f()
                    long r5 = r1
                    int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                    if (r7 != 0) goto L43
                    java.lang.Integer r3 = r3
                    if (r3 == 0) goto L41
                    int r2 = r2.A()
                    java.lang.Integer r3 = r3
                    if (r3 != 0) goto L3b
                    goto L43
                L3b:
                    int r3 = r3.intValue()
                    if (r2 != r3) goto L43
                L41:
                    r2 = 1
                    goto L44
                L43:
                    r2 = 0
                L44:
                    if (r2 == 0) goto L12
                    java.lang.Object r2 = r1.getKey()
                    java.lang.Object r1 = r1.getValue()
                    r0.put(r2, r1)
                    goto L12
                L52:
                    j0.l.e.k r9 = new j0.l.e.k
                    r9.<init>(r0)
                    return r9
                */
                throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreChannels$observeChannelsForGuild$1.call2(java.util.Map):rx.Observable");
            }
        }).q();
        m.checkNotNullExpressionValue(q, "observeGuildAndPrivateCh…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<List<Channel>> observeDMs() {
        Observable F = observePrivateChannels().F(StoreChannels$observeDMs$1.INSTANCE);
        m.checkNotNullExpressionValue(F, "observePrivateChannels()…nel -> channel.isDM() } }");
        return F;
    }

    public final Observable<Channel> observeDefaultChannel(final long j) {
        Observable<Channel> q = this.observeChannelPermissionsForGuild.invoke(Long.valueOf(j)).Y(new b<Map<Long, ? extends Long>, Observable<? extends Channel>>() { // from class: com.discord.stores.StoreChannels$observeDefaultChannel$1
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Observable<? extends Channel> call(Map<Long, ? extends Long> map) {
                return call2((Map<Long, Long>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Observable<? extends Channel> call2(final Map<Long, Long> map) {
                return (Observable<R>) StoreChannels.this.observeChannelsForGuild(j, 0).F(new b<Map<Long, ? extends Channel>, Channel>() { // from class: com.discord.stores.StoreChannels$observeDefaultChannel$1.1

                    /* compiled from: StoreChannels.kt */
                    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/channel/Channel;", "channel", "", "invoke", "(Lcom/discord/api/channel/Channel;)Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                    /* renamed from: com.discord.stores.StoreChannels$observeDefaultChannel$1$1$1  reason: invalid class name and collision with other inner class name */
                    /* loaded from: classes.dex */
                    public static final class C02001 extends d0.z.d.o implements Function1<Channel, Boolean> {
                        public C02001() {
                            super(1);
                        }

                        @Override // kotlin.jvm.functions.Function1
                        public /* bridge */ /* synthetic */ Boolean invoke(Channel channel) {
                            return Boolean.valueOf(invoke2(channel));
                        }

                        /* renamed from: invoke  reason: avoid collision after fix types in other method */
                        public final boolean invoke2(Channel channel) {
                            m.checkNotNullParameter(channel, "channel");
                            return PermissionUtils.can(Permission.VIEW_CHANNEL, (Long) a.c(channel, map));
                        }
                    }

                    @Override // j0.k.b
                    public /* bridge */ /* synthetic */ Channel call(Map<Long, ? extends Channel> map2) {
                        return call2((Map<Long, Channel>) map2);
                    }

                    /* renamed from: call  reason: avoid collision after fix types in other method */
                    public final Channel call2(Map<Long, Channel> map2) {
                        return (Channel) q.firstOrNull(q.sortedWith(q.filter(u.asSequence(map2.values()), new C02001()), ChannelUtils.h(Channel.Companion)));
                    }
                });
            }
        }).q();
        m.checkNotNullExpressionValue(q, "observeChannelPermission…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<List<Channel>> observeDirectShareCandidates(final Context context) {
        m.checkNotNullParameter(context, "context");
        Observable F = observeGuildAndPrivateChannels().x(StoreChannels$observeDirectShareCandidates$1.INSTANCE).F(new b<Map<Long, ? extends Channel>, List<? extends Channel>>() { // from class: com.discord.stores.StoreChannels$observeDirectShareCandidates$2
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ List<? extends Channel> call(Map<Long, ? extends Channel> map) {
                return call2((Map<Long, Channel>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final List<Channel> call2(Map<Long, Channel> map) {
                Collection<Number> sortedKeys$default = FrecencyTracker.getSortedKeys$default(StoreStream.Companion.getChannelsSelected().getFrecency(), 0L, 1, null);
                ArrayList arrayList = new ArrayList();
                for (Number number : sortedKeys$default) {
                    Channel channel = map.get(Long.valueOf(number.longValue()));
                    if (channel != null) {
                        arrayList.add(channel);
                    }
                }
                ArrayList arrayList2 = new ArrayList();
                for (T t : arrayList) {
                    if (ChannelUtils.m((Channel) t)) {
                        arrayList2.add(t);
                    }
                }
                return u.take(arrayList2, ShortcutManagerCompat.getMaxShortcutCountPerActivity(context));
            }
        });
        m.checkNotNullExpressionValue(F, "observeGuildAndPrivateCh…ity(context))\n          }");
        return F;
    }

    public final Observable<Map<Long, Channel>> observeGuildAndPrivateChannels() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreChannels$observeGuildAndPrivateChannels$1(this), 14, null);
    }

    public final Observable<Map<Long, List<Long>>> observeIds(boolean z2) {
        Observable<Map<Long, List<Long>>> q = (z2 ? observeAllChannels() : observeGuildAndPrivateChannels()).F(StoreChannels$observeIds$1.INSTANCE).q();
        m.checkNotNullExpressionValue(q, "channelsObservable\n     …  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Boolean> observeInitializedForAuthedUser() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{InitializedUpdateSource}, false, null, null, new StoreChannels$observeInitializedForAuthedUser$1(this), 14, null);
    }

    public final Observable<Map<Long, String>> observeNames() {
        Observable<Map<Long, String>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreChannels$observeNames$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck\n        …  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Channel> observePrivateChannel(final long j) {
        Observable<Channel> q = observePrivateChannels().F(new b<Map<Long, ? extends Channel>, Channel>() { // from class: com.discord.stores.StoreChannels$observePrivateChannel$1
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Channel call(Map<Long, ? extends Channel> map) {
                return call2((Map<Long, Channel>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Channel call2(Map<Long, Channel> map) {
                return map.get(Long.valueOf(j));
            }
        }).q();
        m.checkNotNullExpressionValue(q, "observePrivateChannels()…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Map<Long, Channel>> observePrivateChannels() {
        return observeChannelsForGuild$default(this, 0L, null, 2, null);
    }

    public final Observable<Map<Long, Channel>> observeThreadsFromMessages(List<Message> list) {
        m.checkNotNullParameter(list, "messages");
        Observable<Map<Long, Channel>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreChannels$observeThreadsFromMessages$1(this, list), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck\n        …  .distinctUntilChanged()");
        return q;
    }

    public final void onGroupCreated(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        this.dispatcher.schedule(new StoreChannels$onGroupCreated$1(this, channel));
    }

    public final void setInitializedForAuthedUser$app_productionGoogleRelease(boolean z2) {
        this.initializedForAuthedUser = z2;
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        Collection<Map<Long, Channel>> values = this.channelsByGuild.values();
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = values.iterator();
        while (it.hasNext()) {
            r.addAll(arrayList, ((Map) it.next()).values());
        }
        ArrayList arrayList2 = new ArrayList();
        Iterator<T> it2 = this.getLurkingGuildIds.invoke().iterator();
        while (true) {
            Set<Long> set = null;
            if (!it2.hasNext()) {
                break;
            }
            Map<Long, Channel> map = this.channelsByGuild.get(Long.valueOf(((Number) it2.next()).longValue()));
            if (map != null) {
                set = map.keySet();
            }
            if (set != null) {
                arrayList2.add(set);
            }
        }
        List flatten = o.flatten(arrayList2);
        Persister<List<Channel>> persister = this.channelsCache;
        ArrayList arrayList3 = new ArrayList();
        for (Object obj : arrayList) {
            if (!flatten.contains(Long.valueOf(((Channel) obj).h()))) {
                arrayList3.add(obj);
            }
        }
        Persister.set$default(persister, arrayList3, false, 2, null);
        LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(o.collectionSizeOrDefault(arrayList, 10)), 16));
        for (Object obj2 : arrayList) {
            linkedHashMap.put(Long.valueOf(((Channel) obj2).h()), obj2);
        }
        this.channelsByIdSnapshot = linkedHashMap;
        Map<Long, Map<Long, Channel>> map2 = this.channelsByGuild;
        LinkedHashMap linkedHashMap2 = new LinkedHashMap(g0.mapCapacity(map2.size()));
        Iterator<T> it3 = map2.entrySet().iterator();
        while (it3.hasNext()) {
            Map.Entry entry = (Map.Entry) it3.next();
            linkedHashMap2.put(entry.getKey(), CollectionExtensionsKt.snapshot$default((Map) entry.getValue(), 0, 0.0f, 3, null));
        }
        this.channelsByGuildSnapshot = linkedHashMap2;
        this.channelNamesSnapshot = CollectionExtensionsKt.snapshot$default(this.channelNames, 0, 0.0f, 3, null);
        this.threadsByIdSnapshot = CollectionExtensionsKt.snapshot$default(this.threadsById, 0, 0.0f, 3, null);
    }

    /* JADX WARN: Removed duplicated region for block: B:14:0x003e  */
    /* JADX WARN: Removed duplicated region for block: B:15:0x0043  */
    @com.discord.stores.StoreThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void storeThread$app_productionGoogleRelease(com.discord.api.channel.Channel r44, long r45, com.discord.api.channel.Channel r47) {
        /*
            r43 = this;
            r0 = r43
            java.lang.String r1 = "thread"
            r5 = r44
            d0.z.d.m.checkNotNullParameter(r5, r1)
            long r1 = r44.f()
            r3 = 0
            int r6 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r6 == 0) goto L1b
            long r1 = r44.f()
            r40 = r1
            goto L1d
        L1b:
            r40 = r45
        L1d:
            if (r47 == 0) goto L28
            boolean r1 = r47.o()
        L23:
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)
            goto L3c
        L28:
            long r1 = r44.f()
            long r3 = r44.r()
            com.discord.api.channel.Channel r1 = r0.getChannelInternal$app_productionGoogleRelease(r1, r3)
            if (r1 == 0) goto L3b
            boolean r1 = r1.o()
            goto L23
        L3b:
            r1 = 0
        L3c:
            if (r1 == 0) goto L43
            boolean r1 = r1.booleanValue()
            goto L47
        L43:
            boolean r1 = r44.o()
        L47:
            r26 = r1
            java.util.Map<java.lang.Long, com.discord.api.channel.Channel> r1 = r0.threadsById
            long r2 = r44.h()
            java.lang.Long r6 = java.lang.Long.valueOf(r2)
            r3 = 0
            r4 = 0
            r7 = 0
            r8 = 0
            r10 = 0
            r12 = 0
            r14 = 0
            r15 = 0
            r16 = 0
            r17 = 0
            r18 = 0
            r19 = 0
            r20 = 0
            r21 = 0
            r23 = 0
            r25 = 0
            r27 = 0
            r29 = 0
            r30 = 0
            r31 = 0
            r32 = 0
            r33 = 0
            r34 = 0
            r35 = 0
            r36 = 0
            r37 = 0
            r38 = 0
            r39 = 536739835(0x1ffdfffb, float:1.0757315E-19)
            r2 = r44
            r42 = r6
            r5 = r40
            com.discord.api.channel.Channel r2 = com.discord.api.channel.Channel.a(r2, r3, r4, r5, r7, r8, r10, r12, r14, r15, r16, r17, r18, r19, r20, r21, r23, r25, r26, r27, r29, r30, r31, r32, r33, r34, r35, r36, r37, r38, r39)
            r3 = r42
            r1.put(r3, r2)
            java.util.Map<java.lang.Long, java.lang.String> r1 = r0.channelNames
            long r2 = r44.h()
            java.lang.Long r2 = java.lang.Long.valueOf(r2)
            java.lang.String r3 = com.discord.api.channel.ChannelUtils.c(r44)
            r1.put(r2, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreChannels.storeThread$app_productionGoogleRelease(com.discord.api.channel.Channel, long, com.discord.api.channel.Channel):void");
    }

    /* JADX WARN: Multi-variable type inference failed */
    public StoreChannels(Dispatcher dispatcher, ObservationDeck observationDeck, Function1<? super Long, ? extends Observable<Map<Long, Long>>> function1, Function0<? extends List<Long>> function0, Function0<Boolean> function02, Persister<List<Channel>> persister) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(function1, "observeChannelPermissionsForGuild");
        m.checkNotNullParameter(function0, "getLurkingGuildIds");
        m.checkNotNullParameter(function02, "isAuthenticated");
        m.checkNotNullParameter(persister, "channelsCache");
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
        this.observeChannelPermissionsForGuild = function1;
        this.getLurkingGuildIds = function0;
        this.isAuthenticated = function02;
        this.channelsCache = persister;
        this.channelsByIdSnapshot = new HashMap();
        this.channelNamesSnapshot = new HashMap();
        this.channelNames = new HashMap();
        this.channelsByGuildSnapshot = h0.emptyMap();
        HashMap hashMap = new HashMap();
        this.channelsByGuild = hashMap;
        this.threadsByIdSnapshot = new HashMap();
        this.threadsById = new HashMap();
        this.preloadedThreads = new HashMap();
        hashMap.put(0L, new HashMap());
    }

    public final Observable<Map<Long, String>> observeNames(Collection<Long> collection) {
        m.checkNotNullParameter(collection, "channelIds");
        Observable k = observeNames().k(b.a.d.o.a(collection));
        m.checkNotNullExpressionValue(k, "observeNames().compose(A…rs.filterMap(channelIds))");
        return k;
    }
}
