package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.message.MessageReference;
import com.discord.models.domain.ModelMessageDelete;
import com.discord.models.message.Message;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.collections.SnowflakePartitionMap;
import d0.t.h0;
import d0.z.d.m;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StorePendingReplies.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0001.B\u0019\u0012\u0006\u0010*\u001a\u00020)\u0012\b\b\u0002\u0010$\u001a\u00020#¢\u0006\u0004\b,\u0010-J\u001b\u0010\u0006\u001a\u0004\u0018\u00010\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0006\u0010\u0007J!\u0010\t\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\t\u0010\nJ1\u0010\u0013\u001a\u00020\u00122\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\r2\b\b\u0002\u0010\u0010\u001a\u00020\u000f2\b\b\u0002\u0010\u0011\u001a\u00020\u000f¢\u0006\u0004\b\u0013\u0010\u0014J!\u0010\u0015\u001a\u00020\u00122\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b\u0015\u0010\u0016J\u0019\u0010\u0017\u001a\u00020\u00122\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0017\u0010\u0018J\u0017\u0010\u001b\u001a\u00020\u00122\u0006\u0010\u001a\u001a\u00020\u0019H\u0007¢\u0006\u0004\b\u001b\u0010\u001cJ\u000f\u0010\u001d\u001a\u00020\u0012H\u0007¢\u0006\u0004\b\u001d\u0010\u001eJ\u000f\u0010\u001f\u001a\u00020\u0012H\u0017¢\u0006\u0004\b\u001f\u0010\u001eR\u001c\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00050 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"R\u0016\u0010$\u001a\u00020#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%R&\u0010'\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00050&8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b'\u0010(R\u0016\u0010*\u001a\u00020)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010+¨\u0006/"}, d2 = {"Lcom/discord/stores/StorePendingReplies;", "Lcom/discord/stores/StoreV2;", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/stores/StorePendingReplies$PendingReply;", "getPendingReply", "(J)Lcom/discord/stores/StorePendingReplies$PendingReply;", "Lrx/Observable;", "observePendingReply", "(J)Lrx/Observable;", "Lcom/discord/api/channel/Channel;", "channel", "Lcom/discord/models/message/Message;", "message", "", "shouldMention", "showMentionToggle", "", "onCreatePendingReply", "(Lcom/discord/api/channel/Channel;Lcom/discord/models/message/Message;ZZ)V", "onSetPendingReplyShouldMention", "(JZ)V", "onDeletePendingReply", "(J)V", "Lcom/discord/models/domain/ModelMessageDelete;", "messageDeleteBulk", "handleMessageDelete", "(Lcom/discord/models/domain/ModelMessageDelete;)V", "handlePreLogout", "()V", "snapshotData", "Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;", "pendingReplies", "Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "", "pendingRepliesSnapshot", "Ljava/util/Map;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V", "PendingReply", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StorePendingReplies extends StoreV2 {
    private final Dispatcher dispatcher;
    private final ObservationDeck observationDeck;
    private final SnowflakePartitionMap.CopiablePartitionMap<PendingReply> pendingReplies;
    private Map<Long, PendingReply> pendingRepliesSnapshot;

    /* compiled from: StorePendingReplies.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0011\b\u0086\b\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\r\u001a\u00020\u0005\u0012\u0006\u0010\u000e\u001a\u00020\b\u0012\u0006\u0010\u000f\u001a\u00020\b¢\u0006\u0004\b$\u0010%J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u000b\u0010\nJ8\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\b\b\u0002\u0010\r\u001a\u00020\u00052\b\b\u0002\u0010\u000e\u001a\u00020\b2\b\b\u0002\u0010\u000f\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u001a\u0010\u0019\u001a\u00020\b2\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0019\u0010\u001aR\u0019\u0010\r\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001b\u001a\u0004\b\u001c\u0010\u0007R\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001d\u001a\u0004\b\u001e\u0010\u0004R\u0019\u0010\u000f\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001f\u001a\u0004\b \u0010\nR\"\u0010\u000e\u001a\u00020\b8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u000e\u0010\u001f\u001a\u0004\b!\u0010\n\"\u0004\b\"\u0010#¨\u0006&"}, d2 = {"Lcom/discord/stores/StorePendingReplies$PendingReply;", "", "Lcom/discord/api/message/MessageReference;", "component1", "()Lcom/discord/api/message/MessageReference;", "Lcom/discord/models/message/Message;", "component2", "()Lcom/discord/models/message/Message;", "", "component3", "()Z", "component4", "messageReference", "originalMessage", "shouldMention", "showMentionToggle", "copy", "(Lcom/discord/api/message/MessageReference;Lcom/discord/models/message/Message;ZZ)Lcom/discord/stores/StorePendingReplies$PendingReply;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/message/Message;", "getOriginalMessage", "Lcom/discord/api/message/MessageReference;", "getMessageReference", "Z", "getShowMentionToggle", "getShouldMention", "setShouldMention", "(Z)V", HookHelper.constructorName, "(Lcom/discord/api/message/MessageReference;Lcom/discord/models/message/Message;ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class PendingReply {
        private final MessageReference messageReference;
        private final Message originalMessage;
        private boolean shouldMention;
        private final boolean showMentionToggle;

        public PendingReply(MessageReference messageReference, Message message, boolean z2, boolean z3) {
            m.checkNotNullParameter(messageReference, "messageReference");
            m.checkNotNullParameter(message, "originalMessage");
            this.messageReference = messageReference;
            this.originalMessage = message;
            this.shouldMention = z2;
            this.showMentionToggle = z3;
        }

        public static /* synthetic */ PendingReply copy$default(PendingReply pendingReply, MessageReference messageReference, Message message, boolean z2, boolean z3, int i, Object obj) {
            if ((i & 1) != 0) {
                messageReference = pendingReply.messageReference;
            }
            if ((i & 2) != 0) {
                message = pendingReply.originalMessage;
            }
            if ((i & 4) != 0) {
                z2 = pendingReply.shouldMention;
            }
            if ((i & 8) != 0) {
                z3 = pendingReply.showMentionToggle;
            }
            return pendingReply.copy(messageReference, message, z2, z3);
        }

        public final MessageReference component1() {
            return this.messageReference;
        }

        public final Message component2() {
            return this.originalMessage;
        }

        public final boolean component3() {
            return this.shouldMention;
        }

        public final boolean component4() {
            return this.showMentionToggle;
        }

        public final PendingReply copy(MessageReference messageReference, Message message, boolean z2, boolean z3) {
            m.checkNotNullParameter(messageReference, "messageReference");
            m.checkNotNullParameter(message, "originalMessage");
            return new PendingReply(messageReference, message, z2, z3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof PendingReply)) {
                return false;
            }
            PendingReply pendingReply = (PendingReply) obj;
            return m.areEqual(this.messageReference, pendingReply.messageReference) && m.areEqual(this.originalMessage, pendingReply.originalMessage) && this.shouldMention == pendingReply.shouldMention && this.showMentionToggle == pendingReply.showMentionToggle;
        }

        public final MessageReference getMessageReference() {
            return this.messageReference;
        }

        public final Message getOriginalMessage() {
            return this.originalMessage;
        }

        public final boolean getShouldMention() {
            return this.shouldMention;
        }

        public final boolean getShowMentionToggle() {
            return this.showMentionToggle;
        }

        public int hashCode() {
            MessageReference messageReference = this.messageReference;
            int i = 0;
            int hashCode = (messageReference != null ? messageReference.hashCode() : 0) * 31;
            Message message = this.originalMessage;
            if (message != null) {
                i = message.hashCode();
            }
            int i2 = (hashCode + i) * 31;
            boolean z2 = this.shouldMention;
            int i3 = 1;
            if (z2) {
                z2 = true;
            }
            int i4 = z2 ? 1 : 0;
            int i5 = z2 ? 1 : 0;
            int i6 = (i2 + i4) * 31;
            boolean z3 = this.showMentionToggle;
            if (!z3) {
                i3 = z3 ? 1 : 0;
            }
            return i6 + i3;
        }

        public final void setShouldMention(boolean z2) {
            this.shouldMention = z2;
        }

        public String toString() {
            StringBuilder R = a.R("PendingReply(messageReference=");
            R.append(this.messageReference);
            R.append(", originalMessage=");
            R.append(this.originalMessage);
            R.append(", shouldMention=");
            R.append(this.shouldMention);
            R.append(", showMentionToggle=");
            return a.M(R, this.showMentionToggle, ")");
        }
    }

    public /* synthetic */ StorePendingReplies(Dispatcher dispatcher, ObservationDeck observationDeck, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(dispatcher, (i & 2) != 0 ? ObservationDeckProvider.get() : observationDeck);
    }

    public static /* synthetic */ void onCreatePendingReply$default(StorePendingReplies storePendingReplies, Channel channel, Message message, boolean z2, boolean z3, int i, Object obj) {
        if ((i & 4) != 0) {
            z2 = true;
        }
        if ((i & 8) != 0) {
            z3 = true;
        }
        storePendingReplies.onCreatePendingReply(channel, message, z2, z3);
    }

    public final PendingReply getPendingReply(long j) {
        return this.pendingRepliesSnapshot.get(Long.valueOf(j));
    }

    @StoreThread
    public final void handleMessageDelete(ModelMessageDelete modelMessageDelete) {
        m.checkNotNullParameter(modelMessageDelete, "messageDeleteBulk");
        PendingReply pendingReply = this.pendingReplies.get(Long.valueOf(modelMessageDelete.getChannelId()));
        if (pendingReply != null && modelMessageDelete.getMessageIds().contains(pendingReply.getMessageReference().c())) {
            this.pendingReplies.remove(Long.valueOf(modelMessageDelete.getChannelId()));
            markChanged();
        }
    }

    @StoreThread
    public final void handlePreLogout() {
        this.pendingReplies.clear();
        markChanged();
    }

    public final Observable<PendingReply> observePendingReply(long j) {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StorePendingReplies$observePendingReply$1(this, j), 14, null);
    }

    public final void onCreatePendingReply(Channel channel, Message message, boolean z2, boolean z3) {
        m.checkNotNullParameter(channel, "channel");
        m.checkNotNullParameter(message, "message");
        this.dispatcher.schedule(new StorePendingReplies$onCreatePendingReply$1(this, channel, message, z2, z3));
    }

    public final void onDeletePendingReply(long j) {
        this.dispatcher.schedule(new StorePendingReplies$onDeletePendingReply$1(this, j));
    }

    public final void onSetPendingReplyShouldMention(long j, boolean z2) {
        this.dispatcher.schedule(new StorePendingReplies$onSetPendingReplyShouldMention$1(this, j, z2));
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        this.pendingRepliesSnapshot = this.pendingReplies.fastCopy();
    }

    public StorePendingReplies(Dispatcher dispatcher, ObservationDeck observationDeck) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
        this.pendingReplies = new SnowflakePartitionMap.CopiablePartitionMap<>(0, 1, null);
        this.pendingRepliesSnapshot = h0.emptyMap();
    }
}
