package com.discord.stores;

import com.discord.models.presence.Presence;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreUserPresence.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00060\u0000j\u0002`\u0001H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/models/presence/Presence;", "Lcom/discord/stores/AppPresence;", "invoke", "()Lcom/discord/models/presence/Presence;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserPresence$observeLocalPresence$1 extends o implements Function0<Presence> {
    public final /* synthetic */ StoreUserPresence this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreUserPresence$observeLocalPresence$1(StoreUserPresence storeUserPresence) {
        super(0);
        this.this$0 = storeUserPresence;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final Presence invoke() {
        Presence presence;
        presence = this.this$0.localPresenceSnapshot;
        return presence;
    }
}
