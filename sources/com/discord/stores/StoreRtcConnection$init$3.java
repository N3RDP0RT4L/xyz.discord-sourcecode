package com.discord.stores;

import androidx.core.app.NotificationCompat;
import co.discord.media_engine.VideoInputDeviceDescription;
import j0.k.b;
import kotlin.Metadata;
/* compiled from: StoreRtcConnection.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\u0010\u0006\u001a\n \u0003*\u0004\u0018\u00010\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lco/discord/media_engine/VideoInputDeviceDescription;", "deviceDescription", "", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "(Lco/discord/media_engine/VideoInputDeviceDescription;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreRtcConnection$init$3<T, R> implements b<VideoInputDeviceDescription, Boolean> {
    public static final StoreRtcConnection$init$3 INSTANCE = new StoreRtcConnection$init$3();

    public final Boolean call(VideoInputDeviceDescription videoInputDeviceDescription) {
        return Boolean.valueOf(videoInputDeviceDescription != null);
    }
}
