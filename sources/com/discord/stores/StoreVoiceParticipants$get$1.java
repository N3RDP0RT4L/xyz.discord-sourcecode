package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.voice.state.VoiceState;
import com.discord.models.domain.ModelCall;
import com.discord.models.member.GuildMember;
import com.discord.models.user.MeUser;
import com.discord.models.user.User;
import com.discord.stores.StoreMediaSettings;
import com.discord.stores.StoreVideoStreams;
import com.discord.stores.StoreVoiceParticipants;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.streams.StreamContext;
import d0.o;
import d0.t.h0;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Pair;
import rx.Observable;
import rx.functions.Func2;
import rx.functions.Func7;
/* compiled from: StoreVoiceParticipants.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\n\u001aj\u0012.\b\u0001\u0012*\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u0006 \u0007*\u0014\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00030\u0003 \u0007*4\u0012.\b\u0001\u0012*\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u0006 \u0007*\u0014\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00030\u0003\u0018\u00010\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\b\u0010\t"}, d2 = {"Lcom/discord/api/channel/Channel;", "channel", "Lrx/Observable;", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/api/channel/Channel;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreVoiceParticipants$get$1<T, R> implements b<Channel, Observable<? extends Map<Long, ? extends StoreVoiceParticipants.VoiceUser>>> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ StoreVoiceParticipants this$0;

    /* compiled from: StoreVoiceParticipants.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\n\u001aj\u0012.\b\u0001\u0012*\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0007 \u0004*\u0014\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u00000\u0000 \u0004*4\u0012.\b\u0001\u0012*\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0007 \u0004*\u0014\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u00000\u0000\u0018\u00010\u00060\u00062.\u0010\u0005\u001a*\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003 \u0004*\u0014\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\b\u0010\t"}, d2 = {"", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/api/voice/state/VoiceState;", "kotlin.jvm.PlatformType", "voiceStates", "Lrx/Observable;", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Map;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreVoiceParticipants$get$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1<T, R> implements b<Map<Long, ? extends VoiceState>, Observable<? extends Map<Long, ? extends StoreVoiceParticipants.VoiceUser>>> {
        public final /* synthetic */ Channel $channel;
        public final /* synthetic */ long $guildId;

        /* compiled from: StoreVoiceParticipants.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\t\u001aZ\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00000\u0000\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0004 \u0001*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u0003 \u0001*,\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00000\u0000\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0004 \u0001*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u0003\u0018\u00010\u00060\u00062\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u001a\u0010\u0005\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0001*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"Lcom/discord/models/user/MeUser;", "kotlin.jvm.PlatformType", "meUser", "", "Lcom/discord/models/user/User;", "otherUsers", "Lkotlin/Pair;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/user/MeUser;Ljava/util/Collection;)Lkotlin/Pair;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreVoiceParticipants$get$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C02181<T1, T2, R> implements Func2<MeUser, Collection<? extends User>, Pair<? extends MeUser, ? extends Collection<? extends User>>> {
            public static final C02181 INSTANCE = new C02181();

            public final Pair<MeUser, Collection<User>> call(MeUser meUser, Collection<? extends User> collection) {
                return o.to(meUser, collection);
            }
        }

        public AnonymousClass1(Channel channel, long j) {
            this.$channel = channel;
            this.$guildId = j;
        }

        @Override // j0.k.b
        public /* bridge */ /* synthetic */ Observable<? extends Map<Long, ? extends StoreVoiceParticipants.VoiceUser>> call(Map<Long, ? extends VoiceState> map) {
            return call2((Map<Long, VoiceState>) map);
        }

        /* renamed from: call  reason: avoid collision after fix types in other method */
        public final Observable<? extends Map<Long, StoreVoiceParticipants.VoiceUser>> call2(final Map<Long, VoiceState> map) {
            Observable otherVoiceUsers;
            Observable observeMe$default = StoreUser.observeMe$default(StoreVoiceParticipants$get$1.this.this$0.getStream().getUsers$app_productionGoogleRelease(), false, 1, null);
            StoreVoiceParticipants storeVoiceParticipants = StoreVoiceParticipants$get$1.this.this$0;
            Channel channel = this.$channel;
            m.checkNotNullExpressionValue(map, "voiceStates");
            otherVoiceUsers = storeVoiceParticipants.getOtherVoiceUsers(channel, map);
            return Observable.j(observeMe$default, otherVoiceUsers, C02181.INSTANCE).Y(new b<Pair<? extends MeUser, ? extends Collection<? extends User>>, Observable<? extends Map<Long, ? extends StoreVoiceParticipants.VoiceUser>>>() { // from class: com.discord.stores.StoreVoiceParticipants.get.1.1.2

                /* compiled from: StoreVoiceParticipants.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\b\u0004\u0010\u0007\u001a&\u0012\f\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003 \u0004*\u0012\u0012\f\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003\u0018\u00010\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/models/domain/ModelCall;", "it", "", "", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/domain/ModelCall;)Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.stores.StoreVoiceParticipants$get$1$1$2$1  reason: invalid class name and collision with other inner class name */
                /* loaded from: classes.dex */
                public static final class C02191<T, R> implements b<ModelCall, List<? extends Long>> {
                    public static final C02191 INSTANCE = new C02191();

                    public final List<Long> call(ModelCall modelCall) {
                        List<Long> ringing;
                        return (modelCall == null || (ringing = modelCall.getRinging()) == null) ? n.emptyList() : ringing;
                    }
                }

                @Override // j0.k.b
                public /* bridge */ /* synthetic */ Observable<? extends Map<Long, ? extends StoreVoiceParticipants.VoiceUser>> call(Pair<? extends MeUser, ? extends Collection<? extends User>> pair) {
                    return call2((Pair<MeUser, ? extends Collection<? extends User>>) pair);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final Observable<? extends Map<Long, StoreVoiceParticipants.VoiceUser>> call2(Pair<MeUser, ? extends Collection<? extends User>> pair) {
                    Observable streamContextsForUsers;
                    final MeUser component1 = pair.component1();
                    final Collection<? extends User> component2 = pair.component2();
                    m.checkNotNullExpressionValue(component2, "otherUsers");
                    ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(component2, 10));
                    for (User user : component2) {
                        arrayList.add(Long.valueOf(user.getId()));
                    }
                    List plus = u.plus((Collection<? extends Long>) arrayList, Long.valueOf(component1.getId()));
                    Observable leadingEdgeThrottle = ObservableExtensionsKt.leadingEdgeThrottle(StoreVoiceParticipants$get$1.this.this$0.getStream().getVoiceSpeaking$app_productionGoogleRelease().observeSpeakingUsers(), 250L, TimeUnit.MILLISECONDS);
                    Observable<R> F = StoreVoiceParticipants$get$1.this.this$0.getStream().getCalls$app_productionGoogleRelease().get(StoreVoiceParticipants$get$1.this.$channelId).F(C02191.INSTANCE);
                    Observable<Map<Long, StoreVideoStreams.UserStreams>> observeUserStreams = StoreVoiceParticipants$get$1.this.this$0.getStream().getVideoStreams$app_productionGoogleRelease().observeUserStreams();
                    Observable leadingEdgeThrottle2 = ObservableExtensionsKt.leadingEdgeThrottle(StoreVoiceParticipants$get$1.this.this$0.getStream().getGuilds$app_productionGoogleRelease().observeComputed(AnonymousClass1.this.$guildId), 1L, TimeUnit.SECONDS);
                    Observable<Map<String, List<Long>>> observeStreamSpectators = StoreVoiceParticipants$get$1.this.this$0.getStream().getApplicationStreaming$app_productionGoogleRelease().observeStreamSpectators();
                    Observable<StoreMediaSettings.VoiceConfiguration> voiceConfig = StoreVoiceParticipants$get$1.this.this$0.getStream().getMediaSettings$app_productionGoogleRelease().getVoiceConfig();
                    streamContextsForUsers = StoreVoiceParticipants$get$1.this.this$0.getStreamContextsForUsers(plus);
                    return Observable.e(leadingEdgeThrottle, F, observeUserStreams, leadingEdgeThrottle2, observeStreamSpectators, voiceConfig, streamContextsForUsers, new Func7<Set<? extends Long>, List<? extends Long>, Map<Long, ? extends StoreVideoStreams.UserStreams>, Map<Long, ? extends GuildMember>, Map<String, ? extends List<? extends Long>>, StoreMediaSettings.VoiceConfiguration, Map<Long, ? extends StreamContext>, Map<Long, ? extends StoreVoiceParticipants.VoiceUser>>() { // from class: com.discord.stores.StoreVoiceParticipants.get.1.1.2.2
                        @Override // rx.functions.Func7
                        public /* bridge */ /* synthetic */ Map<Long, ? extends StoreVoiceParticipants.VoiceUser> call(Set<? extends Long> set, List<? extends Long> list, Map<Long, ? extends StoreVideoStreams.UserStreams> map2, Map<Long, ? extends GuildMember> map3, Map<String, ? extends List<? extends Long>> map4, StoreMediaSettings.VoiceConfiguration voiceConfiguration, Map<Long, ? extends StreamContext> map5) {
                            return call2((Set<Long>) set, (List<Long>) list, (Map<Long, StoreVideoStreams.UserStreams>) map2, (Map<Long, GuildMember>) map3, (Map<String, ? extends List<Long>>) map4, voiceConfiguration, (Map<Long, StreamContext>) map5);
                        }

                        /* renamed from: call  reason: avoid collision after fix types in other method */
                        public final Map<Long, StoreVoiceParticipants.VoiceUser> call2(Set<Long> set, List<Long> list, Map<Long, StoreVideoStreams.UserStreams> map2, Map<Long, GuildMember> map3, Map<String, ? extends List<Long>> map4, StoreMediaSettings.VoiceConfiguration voiceConfiguration, Map<Long, StreamContext> map5) {
                            Map<Long, StoreVoiceParticipants.VoiceUser> create;
                            StoreVoiceParticipants storeVoiceParticipants2 = StoreVoiceParticipants$get$1.this.this$0;
                            MeUser meUser = component1;
                            m.checkNotNullExpressionValue(meUser, "meUser");
                            Collection collection = component2;
                            m.checkNotNullExpressionValue(collection, "otherUsers");
                            Map map6 = map;
                            m.checkNotNullExpressionValue(map6, "voiceStates");
                            m.checkNotNullExpressionValue(set, "speakingUsers");
                            m.checkNotNullExpressionValue(list, "ringingUsers");
                            m.checkNotNullExpressionValue(map2, "videoStreams");
                            m.checkNotNullExpressionValue(map3, "guildMembers");
                            m.checkNotNullExpressionValue(map4, "streamSpectators");
                            m.checkNotNullExpressionValue(voiceConfiguration, "voiceConfig");
                            m.checkNotNullExpressionValue(map5, "streamContexts");
                            create = storeVoiceParticipants2.create(meUser, collection, map6, set, list, map2, map3, map4, voiceConfiguration, map5);
                            return create;
                        }
                    });
                }
            });
        }
    }

    public StoreVoiceParticipants$get$1(StoreVoiceParticipants storeVoiceParticipants, long j) {
        this.this$0 = storeVoiceParticipants;
        this.$channelId = j;
    }

    public final Observable<? extends Map<Long, StoreVoiceParticipants.VoiceUser>> call(Channel channel) {
        if (channel == null) {
            return new k(h0.emptyMap());
        }
        long f = ChannelUtils.x(channel) ? 0L : channel.f();
        return (Observable<R>) this.this$0.getStream().getVoiceStates$app_productionGoogleRelease().observe(f, channel.h()).Y(new AnonymousClass1(channel, f));
    }
}
