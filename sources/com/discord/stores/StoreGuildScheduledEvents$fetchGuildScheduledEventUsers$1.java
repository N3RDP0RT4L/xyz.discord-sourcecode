package com.discord.stores;

import com.discord.api.guildscheduledevent.ApiGuildScheduledEventUser;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventUser;
import d0.f0.q;
import d0.t.n0;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreGuildScheduledEvents.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildScheduledEvents$fetchGuildScheduledEventUsers$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $eventId;
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ StoreGuildScheduledEvents this$0;

    /* compiled from: StoreGuildScheduledEvents.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreGuildScheduledEvents$fetchGuildScheduledEventUsers$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<Error, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Error error) {
            invoke2(error);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Error error) {
            m.checkNotNullParameter(error, "it");
            StoreGuildScheduledEvents$fetchGuildScheduledEventUsers$1.this.this$0.handleFetchRsvpUsersFailure();
        }
    }

    /* compiled from: StoreGuildScheduledEvents.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/api/guildscheduledevent/ApiGuildScheduledEventUser;", "apiGuildScheduledEventUsers", "", "invoke", "(Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreGuildScheduledEvents$fetchGuildScheduledEventUsers$1$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2 extends o implements Function1<List<? extends ApiGuildScheduledEventUser>, Unit> {

        /* compiled from: StoreGuildScheduledEvents.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreGuildScheduledEvents$fetchGuildScheduledEventUsers$1$2$1  reason: invalid class name */
        /* loaded from: classes.dex */
        public static final class AnonymousClass1 extends o implements Function0<Unit> {
            public final /* synthetic */ List $apiGuildScheduledEventUsers;

            /* compiled from: StoreGuildScheduledEvents.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/guildscheduledevent/ApiGuildScheduledEventUser;", "apiEventUser", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventUser;", "invoke", "(Lcom/discord/api/guildscheduledevent/ApiGuildScheduledEventUser;)Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventUser;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.stores.StoreGuildScheduledEvents$fetchGuildScheduledEventUsers$1$2$1$1  reason: invalid class name and collision with other inner class name */
            /* loaded from: classes.dex */
            public static final class C02051 extends o implements Function1<ApiGuildScheduledEventUser, GuildScheduledEventUser> {
                public C02051() {
                    super(1);
                }

                public final GuildScheduledEventUser invoke(ApiGuildScheduledEventUser apiGuildScheduledEventUser) {
                    m.checkNotNullParameter(apiGuildScheduledEventUser, "apiEventUser");
                    return GuildScheduledEventUser.Companion.from(apiGuildScheduledEventUser, StoreGuildScheduledEvents$fetchGuildScheduledEventUsers$1.this.$guildId);
                }
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public AnonymousClass1(List list) {
                super(0);
                this.$apiGuildScheduledEventUsers = list;
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                StoreUser storeUser;
                StoreGuilds storeGuilds;
                HashMap hashMap;
                HashMap hashMap2;
                HashMap hashMap3;
                HashMap hashMap4;
                storeUser = StoreGuildScheduledEvents$fetchGuildScheduledEventUsers$1.this.this$0.userStore;
                storeUser.handleGuildScheduledEventUsersFetch(this.$apiGuildScheduledEventUsers);
                storeGuilds = StoreGuildScheduledEvents$fetchGuildScheduledEventUsers$1.this.this$0.guildsStore;
                storeGuilds.handleGuildScheduledEventUsersFetch(this.$apiGuildScheduledEventUsers, StoreGuildScheduledEvents$fetchGuildScheduledEventUsers$1.this.$guildId);
                hashMap = StoreGuildScheduledEvents$fetchGuildScheduledEventUsers$1.this.this$0.guildScheduledEventUsers;
                HashMap hashMap5 = (HashMap) hashMap.get(Long.valueOf(StoreGuildScheduledEvents$fetchGuildScheduledEventUsers$1.this.$eventId));
                if (hashMap5 == null) {
                    hashMap5 = new HashMap();
                }
                for (GuildScheduledEventUser guildScheduledEventUser : q.mapNotNull(u.asSequence(this.$apiGuildScheduledEventUsers), new C02051())) {
                    hashMap5.put(Long.valueOf(guildScheduledEventUser.getUser().getId()), guildScheduledEventUser);
                }
                hashMap2 = StoreGuildScheduledEvents$fetchGuildScheduledEventUsers$1.this.this$0.guildScheduledEventUsers;
                hashMap2.put(Long.valueOf(StoreGuildScheduledEvents$fetchGuildScheduledEventUsers$1.this.$eventId), hashMap5);
                hashMap3 = StoreGuildScheduledEvents$fetchGuildScheduledEventUsers$1.this.this$0.guildScheduledEventUsersFetches;
                Set set = (Set) hashMap3.get(Long.valueOf(StoreGuildScheduledEvents$fetchGuildScheduledEventUsers$1.this.$guildId));
                if (set == null) {
                    set = new LinkedHashSet();
                }
                set.add(Long.valueOf(StoreGuildScheduledEvents$fetchGuildScheduledEventUsers$1.this.$eventId));
                hashMap4 = StoreGuildScheduledEvents$fetchGuildScheduledEventUsers$1.this.this$0.guildScheduledEventUsersFetches;
                hashMap4.put(Long.valueOf(StoreGuildScheduledEvents$fetchGuildScheduledEventUsers$1.this.$guildId), set);
                StoreGuildScheduledEvents$fetchGuildScheduledEventUsers$1.this.this$0.isFetchingGuildScheduledEventUsers = false;
                StoreGuildScheduledEvents$fetchGuildScheduledEventUsers$1.this.this$0.isGuildScheduledEventUsersError = false;
                StoreGuildScheduledEvents$fetchGuildScheduledEventUsers$1.this.this$0.markChanged();
            }
        }

        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(List<? extends ApiGuildScheduledEventUser> list) {
            invoke2((List<ApiGuildScheduledEventUser>) list);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(List<ApiGuildScheduledEventUser> list) {
            Dispatcher dispatcher;
            m.checkNotNullParameter(list, "apiGuildScheduledEventUsers");
            dispatcher = StoreGuildScheduledEvents$fetchGuildScheduledEventUsers$1.this.this$0.dispatcher;
            dispatcher.schedule(new AnonymousClass1(list));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGuildScheduledEvents$fetchGuildScheduledEventUsers$1(StoreGuildScheduledEvents storeGuildScheduledEvents, long j, long j2) {
        super(0);
        this.this$0 = storeGuildScheduledEvents;
        this.$guildId = j;
        this.$eventId = j2;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        HashMap hashMap;
        hashMap = this.this$0.guildScheduledEventUsersFetches;
        Set set = (Set) hashMap.get(Long.valueOf(this.$guildId));
        if (set == null) {
            set = n0.emptySet();
        }
        if (!set.contains(Long.valueOf(this.$eventId))) {
            this.this$0.isFetchingGuildScheduledEventUsers = true;
            this.this$0.isGuildScheduledEventUsersError = false;
            this.this$0.markChanged();
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().getGuildScheduledEventUsers(this.$guildId, this.$eventId, 100, true, true), false, 1, null), this.this$0.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new AnonymousClass1(), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2());
        }
    }
}
