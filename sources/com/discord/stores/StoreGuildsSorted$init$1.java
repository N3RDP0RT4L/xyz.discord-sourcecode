package com.discord.stores;

import com.discord.stores.StoreGuildsSorted;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreGuildsSorted.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/stores/StoreGuildsSorted$State;", "state", "", "invoke", "(Lcom/discord/stores/StoreGuildsSorted$State;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildsSorted$init$1 extends o implements Function1<StoreGuildsSorted.State, Unit> {
    public final /* synthetic */ StoreGuildsSorted this$0;

    /* compiled from: StoreGuildsSorted.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreGuildsSorted$init$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ StoreGuildsSorted.State $state;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(StoreGuildsSorted.State state) {
            super(0);
            this.$state = state;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            StoreGuildsSorted$init$1.this.this$0.handleNewState(this.$state);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGuildsSorted$init$1(StoreGuildsSorted storeGuildsSorted) {
        super(1);
        this.this$0 = storeGuildsSorted;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(StoreGuildsSorted.State state) {
        invoke2(state);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(StoreGuildsSorted.State state) {
        Dispatcher dispatcher;
        m.checkNotNullParameter(state, "state");
        dispatcher = this.this$0.dispatcher;
        dispatcher.schedule(new AnonymousClass1(state));
    }
}
