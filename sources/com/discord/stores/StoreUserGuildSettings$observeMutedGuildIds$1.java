package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.models.domain.ModelNotificationSettings;
import d0.t.o;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
/* compiled from: StoreUserGuildSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0003\u0010\t\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0004*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00060\u00062.\u0010\u0005\u001a*\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003 \u0004*\u0014\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"", "", "Lcom/discord/primitives/GuildId;", "Lcom/discord/models/domain/ModelNotificationSettings;", "kotlin.jvm.PlatformType", "guildIdToSettingsMap", "", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Map;)Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserGuildSettings$observeMutedGuildIds$1<T, R> implements b<Map<Long, ? extends ModelNotificationSettings>, List<? extends Long>> {
    public static final StoreUserGuildSettings$observeMutedGuildIds$1 INSTANCE = new StoreUserGuildSettings$observeMutedGuildIds$1();

    public final List<Long> call(Map<Long, ? extends ModelNotificationSettings> map) {
        Collection<? extends ModelNotificationSettings> values = map.values();
        ArrayList<ModelNotificationSettings> arrayList = new ArrayList();
        for (T t : values) {
            if (((ModelNotificationSettings) t).isMuted()) {
                arrayList.add(t);
            }
        }
        ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(arrayList, 10));
        for (ModelNotificationSettings modelNotificationSettings : arrayList) {
            arrayList2.add(Long.valueOf(modelNotificationSettings.getGuildId()));
        }
        return arrayList2;
    }
}
