package com.discord.stores;

import com.discord.api.connectedaccounts.ConnectedAccount;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreUserConnections.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/api/connectedaccounts/ConnectedAccount;", "connectedAccounts", "", "invoke", "(Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserConnections$fetchConnectedAccounts$1 extends o implements Function1<List<? extends ConnectedAccount>, Unit> {
    public final /* synthetic */ StoreUserConnections this$0;

    /* compiled from: StoreUserConnections.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreUserConnections$fetchConnectedAccounts$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ List $connectedAccounts;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(List list) {
            super(0);
            this.$connectedAccounts = list;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            StoreUserConnections$fetchConnectedAccounts$1.this.this$0.getStream().handleUserConnections(this.$connectedAccounts);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreUserConnections$fetchConnectedAccounts$1(StoreUserConnections storeUserConnections) {
        super(1);
        this.this$0 = storeUserConnections;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(List<? extends ConnectedAccount> list) {
        invoke2((List<ConnectedAccount>) list);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<ConnectedAccount> list) {
        m.checkNotNullParameter(list, "connectedAccounts");
        this.this$0.getDispatcher().schedule(new AnonymousClass1(list));
    }
}
