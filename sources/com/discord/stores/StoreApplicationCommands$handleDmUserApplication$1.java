package com.discord.stores;

import com.discord.models.commands.Application;
import com.discord.models.user.User;
import com.discord.utilities.user.UserUtils;
import d0.t.n;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreApplicationCommands.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreApplicationCommands$handleDmUserApplication$1 extends o implements Function0<Unit> {
    public final /* synthetic */ User $botUser;
    public final /* synthetic */ StoreApplicationCommands this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreApplicationCommands$handleDmUserApplication$1(StoreApplicationCommands storeApplicationCommands, User user) {
        super(0);
        this.this$0 = storeApplicationCommands;
        this.$botUser = user;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        Map map;
        Map map2;
        BuiltInCommandsProvider builtInCommandsProvider;
        BuiltInCommandsProvider builtInCommandsProvider2;
        map = this.this$0.frecencyCommands;
        map.put(0L, n.emptyList());
        this.this$0.pendingGatewayGuildId = null;
        this.this$0.applications = n.emptyList();
        map2 = this.this$0.applicationCommandIndexes;
        map2.clear();
        StoreApplicationCommands storeApplicationCommands = this.this$0;
        builtInCommandsProvider = storeApplicationCommands.builtInCommandsProvider;
        builtInCommandsProvider2 = this.this$0.builtInCommandsProvider;
        storeApplicationCommands.handleGuildApplicationsUpdate(n.listOf((Object[]) new Application[]{builtInCommandsProvider.getFrecencyApplication(), new Application(this.$botUser.getId(), this.$botUser.getUsername(), null, null, 0, UserUtils.INSTANCE.synthesizeApiUser(this.$botUser), false, 8, null), builtInCommandsProvider2.getBuiltInApplication()}));
        this.this$0.markChanged(StoreApplicationCommands.Companion.getFrecencyCommandsUpdate());
    }
}
