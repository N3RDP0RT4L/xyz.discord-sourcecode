package com.discord.stores;

import com.discord.models.domain.ModelLibraryApplication;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreLibrary.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u0010\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "", "Lcom/discord/models/domain/ModelLibraryApplication;", "invoke", "()Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreLibrary$observeApplications$1 extends o implements Function0<Map<Long, ? extends ModelLibraryApplication>> {
    public final /* synthetic */ StoreLibrary this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreLibrary$observeApplications$1(StoreLibrary storeLibrary) {
        super(0);
        this.this$0 = storeLibrary;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Map<Long, ? extends ModelLibraryApplication> invoke() {
        Map<Long, ? extends ModelLibraryApplication> map;
        map = this.this$0.libraryApplicationSnapshot;
        return map;
    }
}
