package com.discord.stores;

import andhook.lib.HookHelper;
import com.discord.api.activity.Activity;
import com.discord.api.activity.ActivityParty;
import com.discord.api.guild.Guild;
import com.discord.api.presence.ClientStatus;
import com.discord.api.presence.Presence;
import com.discord.api.thread.AugmentedThreadMember;
import com.discord.api.thread.ThreadListMember;
import com.discord.api.thread.ThreadMemberListUpdate;
import com.discord.api.thread.ThreadMembersUpdate;
import com.discord.api.user.User;
import com.discord.models.domain.ModelPayload;
import com.discord.stores.updates.ObservationDeck;
import d0.t.n;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: StoreGameParty.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0094\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u00101\u001a\u000200\u0012\u0006\u0010B\u001a\u00020A\u0012\u0006\u00106\u001a\u000205¢\u0006\u0004\bG\u0010HJ\u001d\u0010\u0006\u001a\u00020\u00052\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J'\u0010\r\u001a\u00020\u00052\n\u0010\n\u001a\u00060\bj\u0002`\t2\n\u0010\f\u001a\u00060\bj\u0002`\u000bH\u0003¢\u0006\u0004\b\r\u0010\u000eJ7\u0010\u0012\u001a\u00020\u00052\n\u0010\n\u001a\u00060\bj\u0002`\t2\n\u0010\f\u001a\u00060\bj\u0002`\u000b2\u000e\u0010\u0011\u001a\n\u0018\u00010\u000fj\u0004\u0018\u0001`\u0010H\u0003¢\u0006\u0004\b\u0012\u0010\u0013J#\u0010\u0015\u001a\u00020\u00052\u0006\u0010\u0014\u001a\u00020\u00032\n\u0010\f\u001a\u00060\bj\u0002`\u000bH\u0003¢\u0006\u0004\b\u0015\u0010\u0016J3\u0010\u001a\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\bj\u0002`\t\u0012\u0004\u0012\u00020\u00190\u00180\u00172\u000e\u0010\u0011\u001a\n\u0018\u00010\u000fj\u0004\u0018\u0001`\u0010¢\u0006\u0004\b\u001a\u0010\u001bJ\u0017\u0010\u001e\u001a\u00020\u00052\u0006\u0010\u001d\u001a\u00020\u001cH\u0007¢\u0006\u0004\b\u001e\u0010\u001fJ\u0017\u0010\"\u001a\u00020\u00052\u0006\u0010!\u001a\u00020 H\u0007¢\u0006\u0004\b\"\u0010#J%\u0010$\u001a\u00020\u00052\u0006\u0010\u0014\u001a\u00020\u00032\f\b\u0002\u0010\f\u001a\u00060\bj\u0002`\u000bH\u0007¢\u0006\u0004\b$\u0010\u0016J\u001d\u0010%\u001a\u00020\u00052\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0007¢\u0006\u0004\b%\u0010\u0007J\u0017\u0010(\u001a\u00020\u00052\u0006\u0010'\u001a\u00020&H\u0007¢\u0006\u0004\b(\u0010)J\u0017\u0010,\u001a\u00020\u00052\u0006\u0010+\u001a\u00020*H\u0007¢\u0006\u0004\b,\u0010-J\u000f\u0010.\u001a\u00020\u0005H\u0017¢\u0006\u0004\b.\u0010/R\u0019\u00101\u001a\u0002008\u0006@\u0006¢\u0006\f\n\u0004\b1\u00102\u001a\u0004\b3\u00104R\u0019\u00106\u001a\u0002058\u0006@\u0006¢\u0006\f\n\u0004\b6\u00107\u001a\u0004\b8\u00109Rj\u0010>\u001aV\u0012\b\u0012\u00060\u000fj\u0002`\u0010\u0012\u001c\u0012\u001a\u0012\b\u0012\u00060\bj\u0002`\t0;j\f\u0012\b\u0012\u00060\bj\u0002`\t`<0:j*\u0012\b\u0012\u00060\u000fj\u0002`\u0010\u0012\u001c\u0012\u001a\u0012\b\u0012\u00060\bj\u0002`\t0;j\f\u0012\b\u0012\u00060\bj\u0002`\t`<`=8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b>\u0010?Rj\u0010@\u001aV\u0012\b\u0012\u00060\u000fj\u0002`\u0010\u0012\u001c\u0012\u001a\u0012\b\u0012\u00060\bj\u0002`\t0;j\f\u0012\b\u0012\u00060\bj\u0002`\t`<0:j*\u0012\b\u0012\u00060\u000fj\u0002`\u0010\u0012\u001c\u0012\u001a\u0012\b\u0012\u00060\bj\u0002`\t0;j\f\u0012\b\u0012\u00060\bj\u0002`\t`<`=8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b@\u0010?R\u0019\u0010B\u001a\u00020A8\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010C\u001a\u0004\bD\u0010ER\u0092\u0001\u0010F\u001a~\u0012\b\u0012\u00060\bj\u0002`\t\u00120\u0012.\u0012\b\u0012\u00060\bj\u0002`\u000b\u0012\b\u0012\u00060\u000fj\u0002`\u00100:j\u0016\u0012\b\u0012\u00060\bj\u0002`\u000b\u0012\b\u0012\u00060\u000fj\u0002`\u0010`=0:j>\u0012\b\u0012\u00060\bj\u0002`\t\u00120\u0012.\u0012\b\u0012\u00060\bj\u0002`\u000b\u0012\b\u0012\u00060\u000fj\u0002`\u00100:j\u0016\u0012\b\u0012\u00060\bj\u0002`\u000b\u0012\b\u0012\u00060\u000fj\u0002`\u0010`=`=8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bF\u0010?¨\u0006I"}, d2 = {"Lcom/discord/stores/StoreGameParty;", "Lcom/discord/stores/StoreV2;", "", "Lcom/discord/api/presence/Presence;", "presences", "", "handlePresences", "(Ljava/util/List;)V", "", "Lcom/discord/primitives/UserId;", "userId", "Lcom/discord/primitives/GuildId;", "guildId", "removeUserFromParty", "(JJ)V", "", "Lcom/discord/primitives/ActivityPartyId;", "partyId", "addUserToParty", "(JJLjava/lang/String;)V", "presence", "updateParty", "(Lcom/discord/api/presence/Presence;J)V", "Lrx/Observable;", "", "Lcom/discord/models/user/User;", "observeUsersForPartyId", "(Ljava/lang/String;)Lrx/Observable;", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "Lcom/discord/api/guild/Guild;", "guild", "handleGuildCreateOrSync", "(Lcom/discord/api/guild/Guild;)V", "handlePresenceUpdate", "handlePresenceReplace", "Lcom/discord/api/thread/ThreadMemberListUpdate;", "threadMemberListUpdate", "handleThreadMemberListUpdate", "(Lcom/discord/api/thread/ThreadMemberListUpdate;)V", "Lcom/discord/api/thread/ThreadMembersUpdate;", "threadMembersUpdate", "handleThreadMembersUpdate", "(Lcom/discord/api/thread/ThreadMembersUpdate;)V", "snapshotData", "()V", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "getObservationDeck", "()Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreUser;", "getStoreUser", "()Lcom/discord/stores/StoreUser;", "Ljava/util/HashMap;", "Ljava/util/HashSet;", "Lkotlin/collections/HashSet;", "Lkotlin/collections/HashMap;", "parties", "Ljava/util/HashMap;", "partiesSnapshot", "Lcom/discord/stores/StoreUserPresence;", "storeUserPresence", "Lcom/discord/stores/StoreUserPresence;", "getStoreUserPresence", "()Lcom/discord/stores/StoreUserPresence;", "userParties", HookHelper.constructorName, "(Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/StoreUserPresence;Lcom/discord/stores/StoreUser;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGameParty extends StoreV2 {
    private final ObservationDeck observationDeck;
    private final StoreUser storeUser;
    private final StoreUserPresence storeUserPresence;
    private final HashMap<Long, HashMap<Long, String>> userParties = new HashMap<>();
    private final HashMap<String, HashSet<Long>> parties = new HashMap<>();
    private HashMap<String, HashSet<Long>> partiesSnapshot = new HashMap<>();

    public StoreGameParty(ObservationDeck observationDeck, StoreUserPresence storeUserPresence, StoreUser storeUser) {
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(storeUserPresence, "storeUserPresence");
        m.checkNotNullParameter(storeUser, "storeUser");
        this.observationDeck = observationDeck;
        this.storeUserPresence = storeUserPresence;
        this.storeUser = storeUser;
    }

    @StoreThread
    private final void addUserToParty(long j, long j2, String str) {
        if (str != null) {
            if (j2 > 0) {
                HashMap<Long, HashMap<Long, String>> hashMap = this.userParties;
                Long valueOf = Long.valueOf(j);
                HashMap<Long, String> hashMap2 = hashMap.get(valueOf);
                if (hashMap2 == null) {
                    hashMap2 = new HashMap<>();
                    hashMap.put(valueOf, hashMap2);
                }
                hashMap2.put(Long.valueOf(j2), str);
            }
            HashSet<Long> hashSet = this.parties.get(str);
            HashSet<Long> hashSet2 = hashSet != null ? new HashSet<>(hashSet) : new HashSet<>();
            hashSet2.add(Long.valueOf(j));
            this.parties.put(str, hashSet2);
            markChanged();
        }
    }

    public static /* synthetic */ void handlePresenceUpdate$default(StoreGameParty storeGameParty, Presence presence, long j, int i, Object obj) {
        if ((i & 2) != 0) {
            Long d = presence.d();
            j = d != null ? d.longValue() : 0L;
        }
        storeGameParty.handlePresenceUpdate(presence, j);
    }

    @StoreThread
    private final void handlePresences(List<Presence> list) {
        for (Presence presence : list) {
            Long d = presence.d();
            updateParty(presence, d != null ? d.longValue() : 0L);
        }
    }

    @StoreThread
    private final void removeUserFromParty(long j, long j2) {
        String remove;
        HashMap<Long, String> hashMap = this.userParties.get(Long.valueOf(j));
        if (hashMap != null && (remove = hashMap.remove(Long.valueOf(j2))) != null) {
            m.checkNotNullExpressionValue(remove, "userParties[userId]?.remove(guildId) ?: return");
            HashSet<Long> hashSet = this.parties.get(remove);
            if (hashSet != null) {
                if (hashSet.size() != 1) {
                    HashMap<String, HashSet<Long>> hashMap2 = this.parties;
                    HashSet<Long> hashSet2 = new HashSet<>(hashSet);
                    hashSet2.remove(Long.valueOf(j));
                    hashMap2.put(remove, hashSet2);
                } else {
                    this.parties.remove(remove);
                }
            }
            markChanged();
        }
    }

    @StoreThread
    private final void updateParty(Presence presence, long j) {
        List<Activity> list;
        boolean z2;
        Object obj;
        ActivityParty i;
        boolean z3;
        User f = presence.f();
        if (f != null) {
            HashMap<Long, String> hashMap = this.userParties.get(Long.valueOf(f.i()));
            String str = null;
            String str2 = hashMap != null ? hashMap.get(Long.valueOf(j)) : null;
            Map<Long, com.discord.models.presence.Presence> presences = this.storeUserPresence.m14getPresences();
            User f2 = presence.f();
            com.discord.models.presence.Presence presence2 = presences.get(f2 != null ? Long.valueOf(f2.i()) : null);
            if (presence2 == null || (list = presence2.getActivities()) == null) {
                list = n.emptyList();
            }
            Iterator<T> it = list.iterator();
            while (true) {
                z2 = false;
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                ActivityParty i2 = ((Activity) obj).i();
                if ((i2 != null ? i2.a() : null) != null) {
                    z3 = true;
                    continue;
                } else {
                    z3 = false;
                    continue;
                }
                if (z3) {
                    break;
                }
            }
            Activity activity = (Activity) obj;
            if (!(activity == null || (i = activity.i()) == null)) {
                str = i.a();
            }
            String str3 = str;
            if (!(str3 == null || presence.e() == ClientStatus.OFFLINE)) {
                z2 = true;
            }
            if (z2) {
                if (!m.areEqual(str2, str3)) {
                    removeUserFromParty(f.i(), j);
                }
                addUserToParty(f.i(), j, str3);
                return;
            }
            removeUserFromParty(f.i(), j);
        }
    }

    public final ObservationDeck getObservationDeck() {
        return this.observationDeck;
    }

    public final StoreUser getStoreUser() {
        return this.storeUser;
    }

    public final StoreUserPresence getStoreUserPresence() {
        return this.storeUserPresence;
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        List<Presence> presences = modelPayload.getPresences();
        if (presences != null) {
            handlePresences(presences);
        }
        List<Guild> guilds = modelPayload.getGuilds();
        m.checkNotNullExpressionValue(guilds, "payload.guilds");
        for (Guild guild : guilds) {
            handleGuildCreateOrSync(guild);
        }
    }

    @StoreThread
    public final void handleGuildCreateOrSync(Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        List<Presence> D = guild.D();
        if (D != null) {
            handlePresences(D);
        }
    }

    @StoreThread
    public final void handlePresenceReplace(List<Presence> list) {
        m.checkNotNullParameter(list, "presences");
        handlePresences(list);
    }

    @StoreThread
    public final void handlePresenceUpdate(Presence presence) {
        handlePresenceUpdate$default(this, presence, 0L, 2, null);
    }

    @StoreThread
    public final void handlePresenceUpdate(Presence presence, long j) {
        m.checkNotNullParameter(presence, "presence");
        updateParty(presence, j);
    }

    @StoreThread
    public final void handleThreadMemberListUpdate(ThreadMemberListUpdate threadMemberListUpdate) {
        m.checkNotNullParameter(threadMemberListUpdate, "threadMemberListUpdate");
        List<ThreadListMember> b2 = threadMemberListUpdate.b();
        if (b2 != null) {
            ArrayList<Presence> arrayList = new ArrayList();
            for (ThreadListMember threadListMember : b2) {
                Presence b3 = threadListMember.b();
                if (b3 != null) {
                    arrayList.add(b3);
                }
            }
            for (Presence presence : arrayList) {
                updateParty(presence, threadMemberListUpdate.a());
            }
        }
    }

    @StoreThread
    public final void handleThreadMembersUpdate(ThreadMembersUpdate threadMembersUpdate) {
        m.checkNotNullParameter(threadMembersUpdate, "threadMembersUpdate");
        List<AugmentedThreadMember> a = threadMembersUpdate.a();
        if (a != null) {
            ArrayList<Presence> arrayList = new ArrayList();
            for (AugmentedThreadMember augmentedThreadMember : a) {
                Presence f = augmentedThreadMember.f();
                if (f != null) {
                    arrayList.add(f);
                }
            }
            for (Presence presence : arrayList) {
                updateParty(presence, threadMembersUpdate.b());
            }
        }
    }

    public final Observable<Map<Long, com.discord.models.user.User>> observeUsersForPartyId(String str) {
        Observable<Map<Long, com.discord.models.user.User>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this, StoreUser.Companion.getUsersUpdate()}, false, null, null, new StoreGameParty$observeUsersForPartyId$1(this, str), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR…  .distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        super.snapshotData();
        this.partiesSnapshot = new HashMap<>();
        for (Map.Entry<String, HashSet<Long>> entry : this.parties.entrySet()) {
            this.partiesSnapshot.put(entry.getKey(), new HashSet<>(entry.getValue()));
        }
    }
}
