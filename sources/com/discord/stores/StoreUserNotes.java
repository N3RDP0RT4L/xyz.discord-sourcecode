package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.Context;
import b.d.b.a.a;
import com.discord.models.domain.ModelUserNote;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.g0.t;
import d0.t.h0;
import d0.z.d.m;
import java.util.HashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreUserNotes.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u00011B#\u0012\u0006\u0010(\u001a\u00020'\u0012\b\b\u0002\u0010#\u001a\u00020\"\u0012\b\b\u0002\u0010+\u001a\u00020*¢\u0006\u0004\b/\u00100J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u001b\u0010\n\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\bH\u0003¢\u0006\u0004\b\n\u0010\u000bJ#\u0010\r\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\u0006\u0010\u0003\u001a\u00020\fH\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u001d\u0010\u0011\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020\u00100\u000f¢\u0006\u0004\b\u0011\u0010\u0012J\u001f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00100\u00132\n\u0010\t\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\b\u0014\u0010\u0015J\u000f\u0010\u0016\u001a\u00020\u0004H\u0007¢\u0006\u0004\b\u0016\u0010\u0017J\u0017\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u0019\u001a\u00020\u0018H\u0007¢\u0006\u0004\b\u001a\u0010\u001bJ\u0019\u0010\u001c\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\b\u001c\u0010\u000bJ+\u0010\u001f\u001a\u00020\u00042\b\u0010\u001e\u001a\u0004\u0018\u00010\u001d2\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\u0006\u0010\u0003\u001a\u00020\f¢\u0006\u0004\b\u001f\u0010 J\u000f\u0010!\u001a\u00020\u0004H\u0016¢\u0006\u0004\b!\u0010\u0017R\u0016\u0010#\u001a\u00020\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$R&\u0010%\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020\u00100\u000f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b%\u0010&R\u0016\u0010(\u001a\u00020'8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u0010)R\u0016\u0010+\u001a\u00020*8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b+\u0010,R&\u0010.\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020\u00100-8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b.\u0010&¨\u00062"}, d2 = {"Lcom/discord/stores/StoreUserNotes;", "Lcom/discord/stores/StoreV2;", "Lcom/discord/models/domain/ModelUserNote;", "note", "", "handleRequestUserNoteSuccess", "(Lcom/discord/models/domain/ModelUserNote;)V", "", "Lcom/discord/primitives/UserId;", "userId", "handleRequestUserNoteError", "(J)V", "", "updateNoteInternal", "(JLjava/lang/String;)V", "", "Lcom/discord/stores/StoreUserNotes$UserNoteState;", "getNotesByUserId", "()Ljava/util/Map;", "Lrx/Observable;", "observeUserNote", "(J)Lrx/Observable;", "handleConnectionOpen", "()V", "Lcom/discord/models/domain/ModelUserNote$Update;", "update", "handleNoteUpdate", "(Lcom/discord/models/domain/ModelUserNote$Update;)V", "loadNote", "Landroid/content/Context;", "context", "saveNote", "(Landroid/content/Context;JLjava/lang/String;)V", "snapshotData", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "notesByUserIdSnapshot", "Ljava/util/Map;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "", "notesByUserId", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/updates/ObservationDeck;)V", "UserNoteState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserNotes extends StoreV2 {
    private final Dispatcher dispatcher;
    private final Map<Long, UserNoteState> notesByUserId;
    private Map<Long, ? extends UserNoteState> notesByUserIdSnapshot;
    private final ObservationDeck observationDeck;
    private final RestAPI restAPI;

    /* compiled from: StoreUserNotes.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/stores/StoreUserNotes$UserNoteState;", "", HookHelper.constructorName, "()V", "Empty", "Loaded", "Loading", "Lcom/discord/stores/StoreUserNotes$UserNoteState$Loading;", "Lcom/discord/stores/StoreUserNotes$UserNoteState$Empty;", "Lcom/discord/stores/StoreUserNotes$UserNoteState$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class UserNoteState {

        /* compiled from: StoreUserNotes.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreUserNotes$UserNoteState$Empty;", "Lcom/discord/stores/StoreUserNotes$UserNoteState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Empty extends UserNoteState {
            public static final Empty INSTANCE = new Empty();

            private Empty() {
                super(null);
            }
        }

        /* compiled from: StoreUserNotes.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/stores/StoreUserNotes$UserNoteState$Loaded;", "Lcom/discord/stores/StoreUserNotes$UserNoteState;", "Lcom/discord/models/domain/ModelUserNote;", "component1", "()Lcom/discord/models/domain/ModelUserNote;", "note", "copy", "(Lcom/discord/models/domain/ModelUserNote;)Lcom/discord/stores/StoreUserNotes$UserNoteState$Loaded;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/ModelUserNote;", "getNote", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelUserNote;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Loaded extends UserNoteState {
            private final ModelUserNote note;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loaded(ModelUserNote modelUserNote) {
                super(null);
                m.checkNotNullParameter(modelUserNote, "note");
                this.note = modelUserNote;
            }

            public static /* synthetic */ Loaded copy$default(Loaded loaded, ModelUserNote modelUserNote, int i, Object obj) {
                if ((i & 1) != 0) {
                    modelUserNote = loaded.note;
                }
                return loaded.copy(modelUserNote);
            }

            public final ModelUserNote component1() {
                return this.note;
            }

            public final Loaded copy(ModelUserNote modelUserNote) {
                m.checkNotNullParameter(modelUserNote, "note");
                return new Loaded(modelUserNote);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Loaded) && m.areEqual(this.note, ((Loaded) obj).note);
                }
                return true;
            }

            public final ModelUserNote getNote() {
                return this.note;
            }

            public int hashCode() {
                ModelUserNote modelUserNote = this.note;
                if (modelUserNote != null) {
                    return modelUserNote.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(note=");
                R.append(this.note);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: StoreUserNotes.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreUserNotes$UserNoteState$Loading;", "Lcom/discord/stores/StoreUserNotes$UserNoteState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Loading extends UserNoteState {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        private UserNoteState() {
        }

        public /* synthetic */ UserNoteState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ StoreUserNotes(Dispatcher dispatcher, RestAPI restAPI, ObservationDeck observationDeck, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(dispatcher, (i & 2) != 0 ? RestAPI.Companion.getApi() : restAPI, (i & 4) != 0 ? ObservationDeckProvider.get() : observationDeck);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleRequestUserNoteError(long j) {
        this.notesByUserId.put(Long.valueOf(j), UserNoteState.Empty.INSTANCE);
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleRequestUserNoteSuccess(ModelUserNote modelUserNote) {
        long noteUserId = modelUserNote.getNoteUserId();
        String note = modelUserNote.getNote();
        if (note == null) {
            note = "";
        }
        updateNoteInternal(noteUserId, note);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void updateNoteInternal(long j, String str) {
        if (t.isBlank(str)) {
            this.notesByUserId.put(Long.valueOf(j), UserNoteState.Empty.INSTANCE);
        } else {
            this.notesByUserId.put(Long.valueOf(j), new UserNoteState.Loaded(new ModelUserNote(j, str)));
        }
        markChanged();
    }

    public final Map<Long, UserNoteState> getNotesByUserId() {
        return this.notesByUserIdSnapshot;
    }

    @StoreThread
    public final void handleConnectionOpen() {
        this.notesByUserId.clear();
        markChanged();
    }

    @StoreThread
    public final void handleNoteUpdate(ModelUserNote.Update update) {
        m.checkNotNullParameter(update, "update");
        updateNoteInternal(update.getId(), update.getNote());
    }

    public final void loadNote(long j) {
        this.dispatcher.schedule(new StoreUserNotes$loadNote$1(this, j));
    }

    public final Observable<UserNoteState> observeUserNote(long j) {
        Observable<UserNoteState> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreUserNotes$observeUserNote$1(this, j), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    public final void saveNote(Context context, long j, String str) {
        m.checkNotNullParameter(str, "note");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.updateUserNotes(j, new RestAPIParams.UserNoteUpdate(str)), false, 1, null), StoreUserNotes.class, (r18 & 2) != 0 ? null : context, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreUserNotes$saveNote$1(this, j, str));
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        super.snapshotData();
        this.notesByUserIdSnapshot = new HashMap(this.notesByUserId);
    }

    public StoreUserNotes(Dispatcher dispatcher, RestAPI restAPI, ObservationDeck observationDeck) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(restAPI, "restAPI");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.dispatcher = dispatcher;
        this.restAPI = restAPI;
        this.observationDeck = observationDeck;
        this.notesByUserId = new HashMap();
        this.notesByUserIdSnapshot = h0.emptyMap();
    }
}
