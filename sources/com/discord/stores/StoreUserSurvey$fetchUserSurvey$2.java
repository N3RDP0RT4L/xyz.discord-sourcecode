package com.discord.stores;

import com.discord.api.user.UserSurveyFetchResponse;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreUserSurvey.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/user/UserSurveyFetchResponse;", "res", "", "invoke", "(Lcom/discord/api/user/UserSurveyFetchResponse;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserSurvey$fetchUserSurvey$2 extends o implements Function1<UserSurveyFetchResponse, Unit> {
    public final /* synthetic */ StoreUserSurvey this$0;

    /* compiled from: StoreUserSurvey.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreUserSurvey$fetchUserSurvey$2$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ UserSurveyFetchResponse $res;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(UserSurveyFetchResponse userSurveyFetchResponse) {
            super(0);
            this.$res = userSurveyFetchResponse;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            StoreUserSurvey$fetchUserSurvey$2.this.this$0.handleUserSurveyFetchSuccess(this.$res);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreUserSurvey$fetchUserSurvey$2(StoreUserSurvey storeUserSurvey) {
        super(1);
        this.this$0 = storeUserSurvey;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(UserSurveyFetchResponse userSurveyFetchResponse) {
        invoke2(userSurveyFetchResponse);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(UserSurveyFetchResponse userSurveyFetchResponse) {
        Dispatcher dispatcher;
        m.checkNotNullParameter(userSurveyFetchResponse, "res");
        dispatcher = this.this$0.dispatcher;
        dispatcher.schedule(new AnonymousClass1(userSurveyFetchResponse));
    }
}
