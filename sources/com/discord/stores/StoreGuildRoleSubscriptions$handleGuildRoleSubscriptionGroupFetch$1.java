package com.discord.stores;

import com.discord.api.guildrolesubscription.GuildRoleSubscriptionGroupListing;
import com.discord.api.guildrolesubscription.GuildRoleSubscriptionTierListing;
import com.discord.stores.StoreGuildRoleSubscriptions;
import d0.z.d.o;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import org.objectweb.asm.Opcodes;
/* compiled from: StoreGuildRoleSubscriptions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildRoleSubscriptions$handleGuildRoleSubscriptionGroupFetch$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ GuildRoleSubscriptionGroupListing $guildRoleSubscriptionGroupListing;
    public final /* synthetic */ StoreGuildRoleSubscriptions this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGuildRoleSubscriptions$handleGuildRoleSubscriptionGroupFetch$1(StoreGuildRoleSubscriptions storeGuildRoleSubscriptions, GuildRoleSubscriptionGroupListing guildRoleSubscriptionGroupListing, long j) {
        super(0);
        this.this$0 = storeGuildRoleSubscriptions;
        this.$guildRoleSubscriptionGroupListing = guildRoleSubscriptionGroupListing;
        this.$guildId = j;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        Map map;
        Map map2;
        Map map3;
        Map map4;
        Map map5;
        Map map6;
        if (this.$guildRoleSubscriptionGroupListing == null) {
            map5 = this.this$0.guildRoleSubscriptionGroups;
            map5.put(Long.valueOf(this.$guildId), new StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState.Loaded(this.$guildRoleSubscriptionGroupListing));
            map6 = this.this$0.guildRoleSubscriptionTiers;
            map6.remove(Long.valueOf(this.$guildId));
        } else {
            map = this.this$0.guildRoleSubscriptionGroups;
            map.put(Long.valueOf(this.$guildId), new StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState.Loaded(GuildRoleSubscriptionGroupListing.a(this.$guildRoleSubscriptionGroupListing, 0L, 0L, 0L, null, null, null, null, false, Opcodes.ATHROW)));
            List<Long> i = this.$guildRoleSubscriptionGroupListing.i();
            if (i == null || i.isEmpty()) {
                map4 = this.this$0.guildRoleSubscriptionTiers;
                map4.remove(Long.valueOf(this.$guildId));
            } else {
                map2 = this.this$0.guildRoleSubscriptionTiers;
                Map map7 = (Map) map2.get(Long.valueOf(this.$guildId));
                if (map7 == null) {
                    map7 = new LinkedHashMap();
                }
                List<GuildRoleSubscriptionTierListing> h = this.$guildRoleSubscriptionGroupListing.h();
                if (h != null) {
                    for (GuildRoleSubscriptionTierListing guildRoleSubscriptionTierListing : h) {
                        map7.put(Long.valueOf(guildRoleSubscriptionTierListing.c()), guildRoleSubscriptionTierListing);
                    }
                }
                map3 = this.this$0.guildRoleSubscriptionTiers;
                map3.put(Long.valueOf(this.$guildId), map7);
            }
        }
        this.this$0.markChanged();
    }
}
