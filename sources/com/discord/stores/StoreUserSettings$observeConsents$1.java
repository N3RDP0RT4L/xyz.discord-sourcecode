package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.models.domain.Consents;
import j0.k.b;
import kotlin.Metadata;
/* compiled from: StoreUserSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "it", "Lcom/discord/models/domain/Consents;", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Throwable;)Lcom/discord/models/domain/Consents;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserSettings$observeConsents$1<T, R> implements b<Throwable, Consents> {
    public static final StoreUserSettings$observeConsents$1 INSTANCE = new StoreUserSettings$observeConsents$1();

    public final Consents call(Throwable th) {
        return Consents.Companion.getDEFAULT();
    }
}
