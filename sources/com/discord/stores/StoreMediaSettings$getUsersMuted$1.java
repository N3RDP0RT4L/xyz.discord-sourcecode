package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.stores.StoreMediaSettings;
import j0.k.b;
import java.util.Map;
import kotlin.Metadata;
/* compiled from: StoreMediaSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\t\u001a*\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u0006 \u0001*\u0014\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "kotlin.jvm.PlatformType", "it", "", "", "Lcom/discord/primitives/UserId;", "", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;)Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMediaSettings$getUsersMuted$1<T, R> implements b<StoreMediaSettings.VoiceConfiguration, Map<Long, ? extends Boolean>> {
    public static final StoreMediaSettings$getUsersMuted$1 INSTANCE = new StoreMediaSettings$getUsersMuted$1();

    public final Map<Long, Boolean> call(StoreMediaSettings.VoiceConfiguration voiceConfiguration) {
        return voiceConfiguration.getMutedUsers();
    }
}
