package com.discord.stores;

import com.discord.api.botuikit.SelectItem;
import d0.t.h0;
import d0.z.d.o;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreLocalActionComponentState.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreLocalActionComponentState$setSelectComponentSelection$1 extends o implements Function0<Unit> {
    public final /* synthetic */ int $componentIndex;
    public final /* synthetic */ long $messageId;
    public final /* synthetic */ List $selectedItems;
    public final /* synthetic */ StoreLocalActionComponentState this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreLocalActionComponentState$setSelectComponentSelection$1(StoreLocalActionComponentState storeLocalActionComponentState, long j, List list, int i) {
        super(0);
        this.this$0 = storeLocalActionComponentState;
        this.$messageId = j;
        this.$selectedItems = list;
        this.$componentIndex = i;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        Map<Integer, List<SelectItem>> map;
        Map<Integer, List<SelectItem>> map2 = this.this$0.getSelectComponentSelections().get(Long.valueOf(this.$messageId));
        if (map2 == null || (map = h0.toMutableMap(map2)) == null) {
            map = new LinkedHashMap<>();
        }
        map.put(Integer.valueOf(this.$componentIndex), this.$selectedItems);
        this.this$0.getSelectComponentSelections().put(Long.valueOf(this.$messageId), map);
        this.this$0.markChanged();
    }
}
