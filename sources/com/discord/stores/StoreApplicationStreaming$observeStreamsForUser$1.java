package com.discord.stores;

import com.discord.models.domain.ModelApplicationStream;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreApplicationStreaming.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/models/domain/ModelApplicationStream;", "invoke", "()Lcom/discord/models/domain/ModelApplicationStream;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreApplicationStreaming$observeStreamsForUser$1 extends o implements Function0<ModelApplicationStream> {
    public final /* synthetic */ long $userId;
    public final /* synthetic */ StoreApplicationStreaming this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreApplicationStreaming$observeStreamsForUser$1(StoreApplicationStreaming storeApplicationStreaming, long j) {
        super(0);
        this.this$0 = storeApplicationStreaming;
        this.$userId = j;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final ModelApplicationStream invoke() {
        Map map;
        map = this.this$0.streamsByUserSnapshot;
        return (ModelApplicationStream) map.get(Long.valueOf(this.$userId));
    }
}
