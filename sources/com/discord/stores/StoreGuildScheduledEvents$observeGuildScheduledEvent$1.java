package com.discord.stores;

import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreGuildScheduledEvents.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "invoke", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildScheduledEvents$observeGuildScheduledEvent$1 extends o implements Function0<GuildScheduledEvent> {
    public final /* synthetic */ Long $eventId;
    public final /* synthetic */ Long $guildId;
    public final /* synthetic */ StoreGuildScheduledEvents this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGuildScheduledEvents$observeGuildScheduledEvent$1(StoreGuildScheduledEvents storeGuildScheduledEvents, Long l, Long l2) {
        super(0);
        this.this$0 = storeGuildScheduledEvents;
        this.$eventId = l;
        this.$guildId = l2;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final GuildScheduledEvent invoke() {
        Long l = this.$eventId;
        if (l != null) {
            return this.this$0.findEventFromStore(l.longValue(), this.$guildId);
        }
        return null;
    }
}
