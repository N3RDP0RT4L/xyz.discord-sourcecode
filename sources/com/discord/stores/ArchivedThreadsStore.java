package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.thread.ThreadListing;
import com.discord.api.thread.ThreadMetadata;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.rest.RestAPI;
import d0.t.u;
import d0.z.d.m;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Subscription;
/* compiled from: ArchivedThreadsStore.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0002&'B!\u0012\u0006\u0010\u001f\u001a\u00020\u001e\u0012\u0006\u0010\"\u001a\u00020!\u0012\b\b\u0002\u0010\u0018\u001a\u00020\u0017¢\u0006\u0004\b$\u0010%J\u000f\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0003\u0010\u0004J'\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\n2\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u00062\u0006\u0010\t\u001a\u00020\b¢\u0006\u0004\b\f\u0010\rJ+\u0010\u0010\u001a\u00020\u00022\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u00062\u0006\u0010\t\u001a\u00020\b2\b\b\u0002\u0010\u000f\u001a\u00020\u000e¢\u0006\u0004\b\u0010\u0010\u0011J\u000f\u0010\u0012\u001a\u00020\u0002H\u0017¢\u0006\u0004\b\u0012\u0010\u0004R2\u0010\u0015\u001a\u001e\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\b0\u0014\u0012\u0004\u0012\u00020\u000b0\u00138\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0018\u001a\u00020\u00178\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R2\u0010\u001c\u001a\u001e\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\b0\u0014\u0012\u0004\u0012\u00020\u001b0\u001a8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001c\u0010\u0016R2\u0010\u001d\u001a\u001e\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\b0\u0014\u0012\u0004\u0012\u00020\u000b0\u001a8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001d\u0010\u0016R\u0016\u0010\u001f\u001a\u00020\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u0016\u0010\"\u001a\u00020!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010#¨\u0006("}, d2 = {"Lcom/discord/stores/ArchivedThreadsStore;", "Lcom/discord/stores/StoreV2;", "", "handleConnectionOpen", "()V", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/stores/ArchivedThreadsStore$ThreadListingType;", "threadListingType", "Lrx/Observable;", "Lcom/discord/stores/ArchivedThreadsStore$ThreadListingState;", "loadAndObserveThreadListing", "(JLcom/discord/stores/ArchivedThreadsStore$ThreadListingType;)Lrx/Observable;", "", "reload", "fetchListing", "(JLcom/discord/stores/ArchivedThreadsStore$ThreadListingType;Z)V", "snapshotData", "", "Lkotlin/Pair;", "listingsSnapshot", "Ljava/util/Map;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "", "Lrx/Subscription;", "fetchSubscriptions", "listings", "Lcom/discord/stores/StoreStream;", "storeStream", "Lcom/discord/stores/StoreStream;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V", "ThreadListingState", "ThreadListingType", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ArchivedThreadsStore extends StoreV2 {
    private final Dispatcher dispatcher;
    private Map<Pair<Long, ThreadListingType>, Subscription> fetchSubscriptions;
    private Map<Pair<Long, ThreadListingType>, ThreadListingState> listings;
    private Map<Pair<Long, ThreadListingType>, ? extends ThreadListingState> listingsSnapshot;
    private final ObservationDeck observationDeck;
    private final StoreStream storeStream;

    /* compiled from: ArchivedThreadsStore.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/stores/ArchivedThreadsStore$ThreadListingState;", "", HookHelper.constructorName, "()V", "Error", "Listing", "Uninitialized", "Lcom/discord/stores/ArchivedThreadsStore$ThreadListingState$Uninitialized;", "Lcom/discord/stores/ArchivedThreadsStore$ThreadListingState$Error;", "Lcom/discord/stores/ArchivedThreadsStore$ThreadListingState$Listing;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class ThreadListingState {

        /* compiled from: ArchivedThreadsStore.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/ArchivedThreadsStore$ThreadListingState$Error;", "Lcom/discord/stores/ArchivedThreadsStore$ThreadListingState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Error extends ThreadListingState {
            public static final Error INSTANCE = new Error();

            private Error() {
                super(null);
            }
        }

        /* compiled from: ArchivedThreadsStore.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\u0006\u0010\u000b\u001a\u00020\u0006\u0012\u0006\u0010\f\u001a\u00020\u0006¢\u0006\u0004\b\u001d\u0010\u001eJ\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\t\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\t\u0010\bJ4\u0010\r\u001a\u00020\u00002\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\b\b\u0002\u0010\u000b\u001a\u00020\u00062\b\b\u0002\u0010\f\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0017\u001a\u00020\u00062\b\u0010\u0016\u001a\u0004\u0018\u00010\u0015HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u0019\u0010\u000b\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0019\u001a\u0004\b\u001a\u0010\bR\u0019\u0010\f\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0019\u001a\u0004\b\f\u0010\bR\u001f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u001b\u001a\u0004\b\u001c\u0010\u0005¨\u0006\u001f"}, d2 = {"Lcom/discord/stores/ArchivedThreadsStore$ThreadListingState$Listing;", "Lcom/discord/stores/ArchivedThreadsStore$ThreadListingState;", "", "Lcom/discord/api/channel/Channel;", "component1", "()Ljava/util/List;", "", "component2", "()Z", "component3", "threads", "hasMore", "isLoadingMore", "copy", "(Ljava/util/List;ZZ)Lcom/discord/stores/ArchivedThreadsStore$ThreadListingState$Listing;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getHasMore", "Ljava/util/List;", "getThreads", HookHelper.constructorName, "(Ljava/util/List;ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Listing extends ThreadListingState {
            private final boolean hasMore;
            private final boolean isLoadingMore;
            private final List<Channel> threads;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Listing(List<Channel> list, boolean z2, boolean z3) {
                super(null);
                m.checkNotNullParameter(list, "threads");
                this.threads = list;
                this.hasMore = z2;
                this.isLoadingMore = z3;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Listing copy$default(Listing listing, List list, boolean z2, boolean z3, int i, Object obj) {
                if ((i & 1) != 0) {
                    list = listing.threads;
                }
                if ((i & 2) != 0) {
                    z2 = listing.hasMore;
                }
                if ((i & 4) != 0) {
                    z3 = listing.isLoadingMore;
                }
                return listing.copy(list, z2, z3);
            }

            public final List<Channel> component1() {
                return this.threads;
            }

            public final boolean component2() {
                return this.hasMore;
            }

            public final boolean component3() {
                return this.isLoadingMore;
            }

            public final Listing copy(List<Channel> list, boolean z2, boolean z3) {
                m.checkNotNullParameter(list, "threads");
                return new Listing(list, z2, z3);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Listing)) {
                    return false;
                }
                Listing listing = (Listing) obj;
                return m.areEqual(this.threads, listing.threads) && this.hasMore == listing.hasMore && this.isLoadingMore == listing.isLoadingMore;
            }

            public final boolean getHasMore() {
                return this.hasMore;
            }

            public final List<Channel> getThreads() {
                return this.threads;
            }

            public int hashCode() {
                List<Channel> list = this.threads;
                int hashCode = (list != null ? list.hashCode() : 0) * 31;
                boolean z2 = this.hasMore;
                int i = 1;
                if (z2) {
                    z2 = true;
                }
                int i2 = z2 ? 1 : 0;
                int i3 = z2 ? 1 : 0;
                int i4 = (hashCode + i2) * 31;
                boolean z3 = this.isLoadingMore;
                if (!z3) {
                    i = z3 ? 1 : 0;
                }
                return i4 + i;
            }

            public final boolean isLoadingMore() {
                return this.isLoadingMore;
            }

            public String toString() {
                StringBuilder R = a.R("Listing(threads=");
                R.append(this.threads);
                R.append(", hasMore=");
                R.append(this.hasMore);
                R.append(", isLoadingMore=");
                return a.M(R, this.isLoadingMore, ")");
            }
        }

        /* compiled from: ArchivedThreadsStore.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/ArchivedThreadsStore$ThreadListingState$Uninitialized;", "Lcom/discord/stores/ArchivedThreadsStore$ThreadListingState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Uninitialized extends ThreadListingState {
            public static final Uninitialized INSTANCE = new Uninitialized();

            private Uninitialized() {
                super(null);
            }
        }

        private ThreadListingState() {
        }

        public /* synthetic */ ThreadListingState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: Failed to restore enum class, 'enum' modifier removed */
    /* compiled from: ArchivedThreadsStore.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\t\b\u0086\u0001\u0018\u0000 \u000e2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u000eB\t\b\u0002¢\u0006\u0004\b\f\u0010\rJ/\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H&¢\u0006\u0004\b\n\u0010\u000bj\u0002\b\u000fj\u0002\b\u0010j\u0002\b\u0011¨\u0006\u0012"}, d2 = {"Lcom/discord/stores/ArchivedThreadsStore$ThreadListingType;", "", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "Lcom/discord/api/channel/Channel;", "threads", "Lrx/Observable;", "Lcom/discord/api/thread/ThreadListing;", "fetchNext", "(JLjava/util/List;)Lrx/Observable;", HookHelper.constructorName, "(Ljava/lang/String;I)V", "Companion", "MY_ARCHIVED_PRIVATE_THREADS", "ALL_ARCHIVED_PUBLIC_THREADS", "ALL_ARCHIVED_PRIVATE_THREADS", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ThreadListingType extends Enum<ThreadListingType> {
        private static final /* synthetic */ ThreadListingType[] $VALUES;
        public static final ThreadListingType ALL_ARCHIVED_PRIVATE_THREADS;
        public static final ThreadListingType ALL_ARCHIVED_PUBLIC_THREADS;
        public static final Companion Companion = new Companion(null);
        public static final ThreadListingType MY_ARCHIVED_PRIVATE_THREADS;

        /* compiled from: ArchivedThreadsStore.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0001\u0018\u00002\u00020\u0001J/\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u0016¢\u0006\u0004\b\n\u0010\u000b¨\u0006\f"}, d2 = {"Lcom/discord/stores/ArchivedThreadsStore$ThreadListingType$ALL_ARCHIVED_PRIVATE_THREADS;", "Lcom/discord/stores/ArchivedThreadsStore$ThreadListingType;", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "Lcom/discord/api/channel/Channel;", "threads", "Lrx/Observable;", "Lcom/discord/api/thread/ThreadListing;", "fetchNext", "(JLjava/util/List;)Lrx/Observable;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class ALL_ARCHIVED_PRIVATE_THREADS extends ThreadListingType {
            public ALL_ARCHIVED_PRIVATE_THREADS(String str, int i) {
                super(str, i, null);
            }

            @Override // com.discord.stores.ArchivedThreadsStore.ThreadListingType
            public Observable<ThreadListing> fetchNext(long j, List<Channel> list) {
                m.checkNotNullParameter(list, "threads");
                try {
                    return RestAPI.Companion.getApi().getAllPrivateArchivedThreads(j, ThreadListingType.Companion.getLastArchiveTimestamp(list));
                } catch (IllegalStateException e) {
                    Observable<ThreadListing> w = Observable.w(e);
                    m.checkNotNullExpressionValue(w, "Observable.error(e)");
                    return w;
                }
            }
        }

        /* compiled from: ArchivedThreadsStore.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0001\u0018\u00002\u00020\u0001J/\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u0016¢\u0006\u0004\b\n\u0010\u000b¨\u0006\f"}, d2 = {"Lcom/discord/stores/ArchivedThreadsStore$ThreadListingType$ALL_ARCHIVED_PUBLIC_THREADS;", "Lcom/discord/stores/ArchivedThreadsStore$ThreadListingType;", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "Lcom/discord/api/channel/Channel;", "threads", "Lrx/Observable;", "Lcom/discord/api/thread/ThreadListing;", "fetchNext", "(JLjava/util/List;)Lrx/Observable;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class ALL_ARCHIVED_PUBLIC_THREADS extends ThreadListingType {
            public ALL_ARCHIVED_PUBLIC_THREADS(String str, int i) {
                super(str, i, null);
            }

            @Override // com.discord.stores.ArchivedThreadsStore.ThreadListingType
            public Observable<ThreadListing> fetchNext(long j, List<Channel> list) {
                m.checkNotNullParameter(list, "threads");
                try {
                    return RestAPI.Companion.getApi().getAllPublicArchivedThreads(j, ThreadListingType.Companion.getLastArchiveTimestamp(list));
                } catch (IllegalStateException e) {
                    Observable<ThreadListing> w = Observable.w(e);
                    m.checkNotNullExpressionValue(w, "Observable.error(e)");
                    return w;
                }
            }
        }

        /* compiled from: ArchivedThreadsStore.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tJ\u001f\u0010\u0006\u001a\u0004\u0018\u00010\u00052\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0002¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/stores/ArchivedThreadsStore$ThreadListingType$Companion;", "", "", "Lcom/discord/api/channel/Channel;", "threads", "", "getLastArchiveTimestamp", "(Ljava/util/List;)Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Companion {
            private Companion() {
            }

            /* JADX INFO: Access modifiers changed from: private */
            public final String getLastArchiveTimestamp(List<Channel> list) {
                String a;
                if (list.isEmpty()) {
                    return null;
                }
                ThreadMetadata y2 = ((Channel) u.last((List<? extends Object>) list)).y();
                if (y2 != null && (a = y2.a()) != null) {
                    return a;
                }
                throw new IllegalStateException("Thread missing threadMetadata");
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* compiled from: ArchivedThreadsStore.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0001\u0018\u00002\u00020\u0001J/\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u0016¢\u0006\u0004\b\n\u0010\u000b¨\u0006\f"}, d2 = {"Lcom/discord/stores/ArchivedThreadsStore$ThreadListingType$MY_ARCHIVED_PRIVATE_THREADS;", "Lcom/discord/stores/ArchivedThreadsStore$ThreadListingType;", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "Lcom/discord/api/channel/Channel;", "threads", "Lrx/Observable;", "Lcom/discord/api/thread/ThreadListing;", "fetchNext", "(JLjava/util/List;)Lrx/Observable;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class MY_ARCHIVED_PRIVATE_THREADS extends ThreadListingType {
            public MY_ARCHIVED_PRIVATE_THREADS(String str, int i) {
                super(str, i, null);
            }

            @Override // com.discord.stores.ArchivedThreadsStore.ThreadListingType
            public Observable<ThreadListing> fetchNext(long j, List<Channel> list) {
                m.checkNotNullParameter(list, "threads");
                Channel channel = (Channel) u.lastOrNull((List<? extends Object>) list);
                return RestAPI.Companion.getApi().getMyPrivateArchivedThreads(j, channel != null ? Long.valueOf(channel.h()) : null);
            }
        }

        static {
            MY_ARCHIVED_PRIVATE_THREADS my_archived_private_threads = new MY_ARCHIVED_PRIVATE_THREADS("MY_ARCHIVED_PRIVATE_THREADS", 0);
            MY_ARCHIVED_PRIVATE_THREADS = my_archived_private_threads;
            ALL_ARCHIVED_PUBLIC_THREADS all_archived_public_threads = new ALL_ARCHIVED_PUBLIC_THREADS("ALL_ARCHIVED_PUBLIC_THREADS", 1);
            ALL_ARCHIVED_PUBLIC_THREADS = all_archived_public_threads;
            ALL_ARCHIVED_PRIVATE_THREADS all_archived_private_threads = new ALL_ARCHIVED_PRIVATE_THREADS("ALL_ARCHIVED_PRIVATE_THREADS", 2);
            ALL_ARCHIVED_PRIVATE_THREADS = all_archived_private_threads;
            $VALUES = new ThreadListingType[]{my_archived_private_threads, all_archived_public_threads, all_archived_private_threads};
        }

        private ThreadListingType(String str, int i) {
        }

        public static ThreadListingType valueOf(String str) {
            return (ThreadListingType) Enum.valueOf(ThreadListingType.class, str);
        }

        public static ThreadListingType[] values() {
            return (ThreadListingType[]) $VALUES.clone();
        }

        public abstract Observable<ThreadListing> fetchNext(long j, List<Channel> list);

        public /* synthetic */ ThreadListingType(String str, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(str, i);
        }
    }

    public /* synthetic */ ArchivedThreadsStore(StoreStream storeStream, Dispatcher dispatcher, ObservationDeck observationDeck, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(storeStream, dispatcher, (i & 4) != 0 ? ObservationDeckProvider.get() : observationDeck);
    }

    public static /* synthetic */ void fetchListing$default(ArchivedThreadsStore archivedThreadsStore, long j, ThreadListingType threadListingType, boolean z2, int i, Object obj) {
        if ((i & 4) != 0) {
            z2 = false;
        }
        archivedThreadsStore.fetchListing(j, threadListingType, z2);
    }

    public final void fetchListing(long j, ThreadListingType threadListingType, boolean z2) {
        m.checkNotNullParameter(threadListingType, "threadListingType");
        this.dispatcher.schedule(new ArchivedThreadsStore$fetchListing$1(this, j, threadListingType, z2));
    }

    @StoreThread
    public final void handleConnectionOpen() {
        this.listings = new HashMap();
        for (Subscription subscription : this.fetchSubscriptions.values()) {
            subscription.unsubscribe();
        }
        this.fetchSubscriptions = new HashMap();
        markChanged();
    }

    public final Observable<ThreadListingState> loadAndObserveThreadListing(long j, ThreadListingType threadListingType) {
        m.checkNotNullParameter(threadListingType, "threadListingType");
        fetchListing(j, threadListingType, true);
        Observable<ThreadListingState> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new ArchivedThreadsStore$loadAndObserveThreadListing$1(this, j, threadListingType), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        this.listingsSnapshot = new HashMap(this.listings);
    }

    public ArchivedThreadsStore(StoreStream storeStream, Dispatcher dispatcher, ObservationDeck observationDeck) {
        m.checkNotNullParameter(storeStream, "storeStream");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.storeStream = storeStream;
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
        this.listings = new HashMap();
        this.listingsSnapshot = new HashMap();
        this.fetchSubscriptions = new HashMap();
    }
}
