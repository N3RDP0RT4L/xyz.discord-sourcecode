package com.discord.stores;

import android.content.Context;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.FragmentManager;
import com.discord.app.AppComponent;
import com.discord.models.domain.ModelCall;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.voice.call.WidgetCallFailed;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import j0.l.e.k;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import rx.Observable;
/* compiled from: StoreCalls.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\n\u0010\u0002\u001a\u00060\u0000j\u0002`\u0001H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/primitives/UserId;", "userId", "", "invoke", "(J)V", "doCallIfCallable"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreCalls$call$2 extends o implements Function1<Long, Unit> {
    public final /* synthetic */ AppComponent $appComponent;
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ Context $context;
    public final /* synthetic */ StoreCalls$call$1 $doCall$1;
    public final /* synthetic */ FragmentManager $fragmentManager;
    public final /* synthetic */ Function0 $onError;
    public final /* synthetic */ StoreCalls this$0;

    /* compiled from: StoreCalls.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "isRingable", "", "invoke", "(Ljava/lang/Boolean;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreCalls$call$2$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2 extends o implements Function1<Boolean, Unit> {
        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Boolean bool) {
            invoke2(bool);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Boolean bool) {
            StoreCalls$call$1 storeCalls$call$1 = StoreCalls$call$2.this.$doCall$1;
            m.checkNotNullExpressionValue(bool, "isRingable");
            storeCalls$call$1.invoke(bool.booleanValue());
        }
    }

    /* compiled from: StoreCalls.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "error", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreCalls$call$2$3  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass3 extends o implements Function1<Error, Unit> {
        public final /* synthetic */ long $userId;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass3(long j) {
            super(1);
            this.$userId = j;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Error error) {
            invoke2(error);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Error error) {
            m.checkNotNullParameter(error, "error");
            Function0 function0 = StoreCalls$call$2.this.$onError;
            if (function0 != null) {
                Unit unit = (Unit) function0.invoke();
            }
            Error.Response response = error.getResponse();
            m.checkNotNullExpressionValue(response, "error.response");
            if (response.getCode() == 50013) {
                error.setShowErrorToasts(false);
                WidgetCallFailed.Companion.show(StoreCalls$call$2.this.$fragmentManager, this.$userId);
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreCalls$call$2(StoreCalls storeCalls, long j, AppComponent appComponent, Context context, StoreCalls$call$1 storeCalls$call$1, Function0 function0, FragmentManager fragmentManager) {
        super(1);
        this.this$0 = storeCalls;
        this.$channelId = j;
        this.$appComponent = appComponent;
        this.$context = context;
        this.$doCall$1 = storeCalls$call$1;
        this.$onError = function0;
        this.$fragmentManager = fragmentManager;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Long l) {
        invoke(l.longValue());
        return Unit.a;
    }

    public final void invoke(long j) {
        Observable<R> z2 = StoreStream.Companion.getUserRelationships().observe(j).z(new b<Integer, Observable<? extends Boolean>>() { // from class: com.discord.stores.StoreCalls$call$2.1

            /* compiled from: StoreCalls.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/models/domain/ModelCall$Ringable;", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/domain/ModelCall$Ringable;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.stores.StoreCalls$call$2$1$1  reason: invalid class name and collision with other inner class name */
            /* loaded from: classes.dex */
            public static final class C01971<T, R> implements b<ModelCall.Ringable, Boolean> {
                public static final C01971 INSTANCE = new C01971();

                public final Boolean call(ModelCall.Ringable ringable) {
                    m.checkNotNullExpressionValue(ringable, "it");
                    return Boolean.valueOf(ringable.isRingable());
                }
            }

            public final Observable<? extends Boolean> call(Integer num) {
                if (num != null && num.intValue() == 1) {
                    return new k(Boolean.TRUE);
                }
                return ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().call(StoreCalls$call$2.this.$channelId), false, 1, null).F(C01971.INSTANCE);
            }
        });
        m.checkNotNullExpressionValue(z2, "StoreStream\n          .g…            }\n          }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.takeSingleUntilTimeout$default(z2, 0L, false, 3, null), this.$appComponent, null, 2, null), this.this$0.getClass(), (r18 & 2) != 0 ? null : this.$context, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new AnonymousClass3(j), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2());
    }
}
