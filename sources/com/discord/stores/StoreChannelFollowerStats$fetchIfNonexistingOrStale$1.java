package com.discord.stores;

import com.discord.models.domain.ModelChannelFollowerStats;
import com.discord.models.domain.ModelChannelFollowerStatsDto;
import com.discord.utilities.error.Error;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreChannelFollowerStats.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreChannelFollowerStats$fetchIfNonexistingOrStale$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ StoreChannelFollowerStats this$0;

    /* compiled from: StoreChannelFollowerStats.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/ModelChannelFollowerStatsDto;", "channelFollowerStats", "", "invoke", "(Lcom/discord/models/domain/ModelChannelFollowerStatsDto;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreChannelFollowerStats$fetchIfNonexistingOrStale$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<ModelChannelFollowerStatsDto, Unit> {

        /* compiled from: StoreChannelFollowerStats.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreChannelFollowerStats$fetchIfNonexistingOrStale$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C01981 extends o implements Function0<Unit> {
            public final /* synthetic */ ModelChannelFollowerStatsDto $channelFollowerStats;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C01981(ModelChannelFollowerStatsDto modelChannelFollowerStatsDto) {
                super(0);
                this.$channelFollowerStats = modelChannelFollowerStatsDto;
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                StoreChannelFollowerStats$fetchIfNonexistingOrStale$1 storeChannelFollowerStats$fetchIfNonexistingOrStale$1 = StoreChannelFollowerStats$fetchIfNonexistingOrStale$1.this;
                storeChannelFollowerStats$fetchIfNonexistingOrStale$1.this$0.handleChannelFollowerStatsFetchSuccess(storeChannelFollowerStats$fetchIfNonexistingOrStale$1.$channelId, ModelChannelFollowerStats.Companion.fromResponse(this.$channelFollowerStats));
            }
        }

        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(ModelChannelFollowerStatsDto modelChannelFollowerStatsDto) {
            invoke2(modelChannelFollowerStatsDto);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(ModelChannelFollowerStatsDto modelChannelFollowerStatsDto) {
            Dispatcher dispatcher;
            dispatcher = StoreChannelFollowerStats$fetchIfNonexistingOrStale$1.this.this$0.dispatcher;
            dispatcher.schedule(new C01981(modelChannelFollowerStatsDto));
        }
    }

    /* compiled from: StoreChannelFollowerStats.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreChannelFollowerStats$fetchIfNonexistingOrStale$1$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2 extends o implements Function1<Error, Unit> {

        /* compiled from: StoreChannelFollowerStats.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreChannelFollowerStats$fetchIfNonexistingOrStale$1$2$1  reason: invalid class name */
        /* loaded from: classes.dex */
        public static final class AnonymousClass1 extends o implements Function0<Unit> {
            public AnonymousClass1() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                StoreChannelFollowerStats$fetchIfNonexistingOrStale$1 storeChannelFollowerStats$fetchIfNonexistingOrStale$1 = StoreChannelFollowerStats$fetchIfNonexistingOrStale$1.this;
                storeChannelFollowerStats$fetchIfNonexistingOrStale$1.this$0.handleChannelFollowerStatsFetchFailed(storeChannelFollowerStats$fetchIfNonexistingOrStale$1.$channelId);
            }
        }

        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Error error) {
            invoke2(error);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Error error) {
            Dispatcher dispatcher;
            m.checkNotNullParameter(error, "it");
            dispatcher = StoreChannelFollowerStats$fetchIfNonexistingOrStale$1.this.this$0.dispatcher;
            dispatcher.schedule(new AnonymousClass1());
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreChannelFollowerStats$fetchIfNonexistingOrStale$1(StoreChannelFollowerStats storeChannelFollowerStats, long j) {
        super(0);
        this.this$0 = storeChannelFollowerStats;
        this.$channelId = j;
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x002e  */
    /* JADX WARN: Removed duplicated region for block: B:11:0x0033  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0038  */
    /* JADX WARN: Removed duplicated region for block: B:15:0x003a  */
    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void invoke2() {
        /*
            r15 = this;
            com.discord.stores.StoreChannelFollowerStats r0 = r15.this$0
            long r1 = r15.$channelId
            boolean r0 = com.discord.stores.StoreChannelFollowerStats.access$isExisting(r0, r1)
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L18
            com.discord.stores.StoreChannelFollowerStats r0 = r15.this$0
            long r3 = r15.$channelId
            boolean r0 = com.discord.stores.StoreChannelFollowerStats.access$isStale(r0, r3)
            if (r0 != 0) goto L18
            r0 = 1
            goto L19
        L18:
            r0 = 0
        L19:
            com.discord.stores.StoreChannelFollowerStats r3 = r15.this$0
            java.util.Map r3 = com.discord.stores.StoreChannelFollowerStats.access$getChannelFollowerStatsState$p(r3)
            long r4 = r15.$channelId
            java.lang.Long r4 = java.lang.Long.valueOf(r4)
            java.lang.Object r3 = r3.get(r4)
            com.discord.stores.StoreChannelFollowerStats$ChannelFollowerStatData r3 = (com.discord.stores.StoreChannelFollowerStats.ChannelFollowerStatData) r3
            r4 = 0
            if (r3 == 0) goto L33
            com.discord.stores.StoreChannelFollowerStats$FetchState r3 = r3.getFetchState()
            goto L34
        L33:
            r3 = r4
        L34:
            com.discord.stores.StoreChannelFollowerStats$FetchState r5 = com.discord.stores.StoreChannelFollowerStats.FetchState.FETCHING
            if (r3 != r5) goto L3a
            r3 = 1
            goto L3b
        L3a:
            r3 = 0
        L3b:
            if (r0 != 0) goto L71
            if (r3 == 0) goto L40
            goto L71
        L40:
            com.discord.stores.StoreChannelFollowerStats r0 = r15.this$0
            long r5 = r15.$channelId
            com.discord.stores.StoreChannelFollowerStats.access$handleChannelFollowerStatsFetchStart(r0, r5)
            com.discord.utilities.rest.RestAPI$Companion r0 = com.discord.utilities.rest.RestAPI.Companion
            com.discord.utilities.rest.RestAPI r0 = r0.getApi()
            long r5 = r15.$channelId
            rx.Observable r0 = r0.getChannelFollowerStats(r5)
            rx.Observable r5 = com.discord.utilities.rx.ObservableExtensionsKt.restSubscribeOn$default(r0, r2, r1, r4)
            com.discord.stores.StoreChannelFollowerStats r0 = r15.this$0
            java.lang.Class r6 = r0.getClass()
            r7 = 0
            r8 = 0
            com.discord.stores.StoreChannelFollowerStats$fetchIfNonexistingOrStale$1$1 r12 = new com.discord.stores.StoreChannelFollowerStats$fetchIfNonexistingOrStale$1$1
            r12.<init>()
            r10 = 0
            r11 = 0
            com.discord.stores.StoreChannelFollowerStats$fetchIfNonexistingOrStale$1$2 r9 = new com.discord.stores.StoreChannelFollowerStats$fetchIfNonexistingOrStale$1$2
            r9.<init>()
            r13 = 54
            r14 = 0
            com.discord.utilities.rx.ObservableExtensionsKt.appSubscribe$default(r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
        L71:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreChannelFollowerStats$fetchIfNonexistingOrStale$1.invoke2():void");
    }
}
