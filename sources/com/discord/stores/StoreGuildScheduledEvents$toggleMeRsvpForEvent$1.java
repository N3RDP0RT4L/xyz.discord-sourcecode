package com.discord.stores;

import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import java.util.HashSet;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreGuildScheduledEvents.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildScheduledEvents$toggleMeRsvpForEvent$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $eventId;
    public final /* synthetic */ GuildScheduledEvent $guildScheduledEvent;
    public final /* synthetic */ GuildScheduledEvent $storeEvent;
    public final /* synthetic */ StoreGuildScheduledEvents this$0;

    /* compiled from: StoreGuildScheduledEvents.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreGuildScheduledEvents$toggleMeRsvpForEvent$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<Error, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Error error) {
            invoke2(error);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Error error) {
            GuildScheduledEvent findEventFromStore;
            StoreUser storeUser;
            HashSet hashSet;
            m.checkNotNullParameter(error, "it");
            StoreGuildScheduledEvents$toggleMeRsvpForEvent$1 storeGuildScheduledEvents$toggleMeRsvpForEvent$1 = StoreGuildScheduledEvents$toggleMeRsvpForEvent$1.this;
            StoreGuildScheduledEvents storeGuildScheduledEvents = storeGuildScheduledEvents$toggleMeRsvpForEvent$1.this$0;
            findEventFromStore = storeGuildScheduledEvents.findEventFromStore(storeGuildScheduledEvents$toggleMeRsvpForEvent$1.$storeEvent);
            storeUser = StoreGuildScheduledEvents$toggleMeRsvpForEvent$1.this.this$0.userStore;
            storeGuildScheduledEvents.processRsvpCreate(findEventFromStore, storeUser.getMe().getId());
            hashSet = StoreGuildScheduledEvents$toggleMeRsvpForEvent$1.this.this$0.rsvpsAwaitingNetwork;
            hashSet.remove(Long.valueOf(StoreGuildScheduledEvents$toggleMeRsvpForEvent$1.this.$eventId));
        }
    }

    /* compiled from: StoreGuildScheduledEvents.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Ljava/lang/Void;", "it", "", "invoke", "(Ljava/lang/Void;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreGuildScheduledEvents$toggleMeRsvpForEvent$1$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2 extends o implements Function1<Void, Unit> {
        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Void r1) {
            invoke2(r1);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Void r3) {
            HashSet hashSet;
            hashSet = StoreGuildScheduledEvents$toggleMeRsvpForEvent$1.this.this$0.rsvpsAwaitingNetwork;
            hashSet.remove(Long.valueOf(StoreGuildScheduledEvents$toggleMeRsvpForEvent$1.this.$eventId));
        }
    }

    /* compiled from: StoreGuildScheduledEvents.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreGuildScheduledEvents$toggleMeRsvpForEvent$1$3  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass3 extends o implements Function1<Error, Unit> {
        public AnonymousClass3() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Error error) {
            invoke2(error);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Error error) {
            GuildScheduledEvent findEventFromStore;
            StoreUser storeUser;
            HashSet hashSet;
            m.checkNotNullParameter(error, "it");
            StoreGuildScheduledEvents$toggleMeRsvpForEvent$1 storeGuildScheduledEvents$toggleMeRsvpForEvent$1 = StoreGuildScheduledEvents$toggleMeRsvpForEvent$1.this;
            StoreGuildScheduledEvents storeGuildScheduledEvents = storeGuildScheduledEvents$toggleMeRsvpForEvent$1.this$0;
            findEventFromStore = storeGuildScheduledEvents.findEventFromStore(storeGuildScheduledEvents$toggleMeRsvpForEvent$1.$storeEvent);
            storeUser = StoreGuildScheduledEvents$toggleMeRsvpForEvent$1.this.this$0.userStore;
            storeGuildScheduledEvents.processRsvpDelete(findEventFromStore, storeUser.getMe().getId());
            hashSet = StoreGuildScheduledEvents$toggleMeRsvpForEvent$1.this.this$0.rsvpsAwaitingNetwork;
            hashSet.remove(Long.valueOf(StoreGuildScheduledEvents$toggleMeRsvpForEvent$1.this.$eventId));
        }
    }

    /* compiled from: StoreGuildScheduledEvents.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"", "it", "invoke", "(Lkotlin/Unit;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreGuildScheduledEvents$toggleMeRsvpForEvent$1$4  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass4 extends o implements Function1<Unit, Unit> {
        public AnonymousClass4() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Unit unit) {
            invoke2(unit);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Unit unit) {
            HashSet hashSet;
            m.checkNotNullParameter(unit, "it");
            hashSet = StoreGuildScheduledEvents$toggleMeRsvpForEvent$1.this.this$0.rsvpsAwaitingNetwork;
            hashSet.remove(Long.valueOf(StoreGuildScheduledEvents$toggleMeRsvpForEvent$1.this.$eventId));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGuildScheduledEvents$toggleMeRsvpForEvent$1(StoreGuildScheduledEvents storeGuildScheduledEvents, GuildScheduledEvent guildScheduledEvent, long j, GuildScheduledEvent guildScheduledEvent2) {
        super(0);
        this.this$0 = storeGuildScheduledEvents;
        this.$guildScheduledEvent = guildScheduledEvent;
        this.$eventId = j;
        this.$storeEvent = guildScheduledEvent2;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreUser storeUser;
        StoreUser storeUser2;
        if (this.this$0.isMeRsvpedToEvent(this.$guildScheduledEvent.h(), this.$eventId)) {
            StoreGuildScheduledEvents storeGuildScheduledEvents = this.this$0;
            GuildScheduledEvent guildScheduledEvent = this.$storeEvent;
            storeUser2 = storeGuildScheduledEvents.userStore;
            storeGuildScheduledEvents.processRsvpDelete(guildScheduledEvent, storeUser2.getMe().getId());
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().deleteGuildScheduledEventRsvp(this.$storeEvent.h(), this.$storeEvent.i()), false, 1, null), this.this$0.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new AnonymousClass1(), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2());
            return;
        }
        StoreGuildScheduledEvents storeGuildScheduledEvents2 = this.this$0;
        GuildScheduledEvent guildScheduledEvent2 = this.$storeEvent;
        storeUser = storeGuildScheduledEvents2.userStore;
        storeGuildScheduledEvents2.processRsvpCreate(guildScheduledEvent2, storeUser.getMe().getId());
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().createGuildScheduledEventRsvp(this.$guildScheduledEvent.h(), this.$storeEvent.i()), false, 1, null), this.this$0.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new AnonymousClass3(), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass4());
    }
}
