package com.discord.stores;

import com.discord.api.sticker.Sticker;
import com.discord.utilities.frecency.FrecencyTracker;
import com.discord.utilities.media.MediaFrecencyTracker;
import com.discord.utilities.persister.Persister;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreStickers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStickers$onStickerUsed$1 extends o implements Function0<Unit> {
    public final /* synthetic */ Sticker $sticker;
    public final /* synthetic */ StoreStickers this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreStickers$onStickerUsed$1(StoreStickers storeStickers, Sticker sticker) {
        super(0);
        this.this$0 = storeStickers;
        this.$sticker = sticker;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        MediaFrecencyTracker mediaFrecencyTracker;
        Persister persister;
        MediaFrecencyTracker mediaFrecencyTracker2;
        mediaFrecencyTracker = this.this$0.frecency;
        FrecencyTracker.track$default(mediaFrecencyTracker, String.valueOf(this.$sticker.getId()), 0L, 2, null);
        persister = this.this$0.frecencyCache;
        mediaFrecencyTracker2 = this.this$0.frecency;
        persister.set(mediaFrecencyTracker2, true);
    }
}
