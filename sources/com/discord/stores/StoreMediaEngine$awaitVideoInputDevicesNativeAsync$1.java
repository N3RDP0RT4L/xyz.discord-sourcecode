package com.discord.stores;

import d0.w.i.a.d;
import d0.w.i.a.e;
import kotlin.Metadata;
import kotlin.coroutines.Continuation;
/* compiled from: StoreMediaEngine.kt */
@e(c = "com.discord.stores.StoreMediaEngine", f = "StoreMediaEngine.kt", l = {264}, m = "awaitVideoInputDevicesNativeAsync")
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0012\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00020\u00010\u0000H\u0082@¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lkotlin/coroutines/Continuation;", "", "Lco/discord/media_engine/VideoInputDeviceDescription;", "continuation", "", "awaitVideoInputDevicesNativeAsync", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMediaEngine$awaitVideoInputDevicesNativeAsync$1 extends d {
    public Object L$0;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ StoreMediaEngine this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreMediaEngine$awaitVideoInputDevicesNativeAsync$1(StoreMediaEngine storeMediaEngine, Continuation continuation) {
        super(continuation);
        this.this$0 = storeMediaEngine;
    }

    @Override // d0.w.i.a.a
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.awaitVideoInputDevicesNativeAsync(this);
    }
}
