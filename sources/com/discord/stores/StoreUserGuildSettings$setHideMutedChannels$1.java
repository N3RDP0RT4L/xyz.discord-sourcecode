package com.discord.stores;

import d0.z.d.o;
import java.util.HashSet;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreUserGuildSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserGuildSettings$setHideMutedChannels$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ boolean $hideChannels;
    public final /* synthetic */ StoreUserGuildSettings this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreUserGuildSettings$setHideMutedChannels$1(StoreUserGuildSettings storeUserGuildSettings, boolean z2, long j) {
        super(0);
        this.this$0 = storeUserGuildSettings;
        this.$hideChannels = z2;
        this.$guildId = j;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        HashSet hashSet;
        HashSet hashSet2;
        if (this.$hideChannels) {
            hashSet2 = this.this$0.guildsToHideMutedChannelsIn;
            hashSet2.add(Long.valueOf(this.$guildId));
        } else {
            hashSet = this.this$0.guildsToHideMutedChannelsIn;
            hashSet.remove(Long.valueOf(this.$guildId));
        }
        this.this$0.markChanged();
    }
}
