package com.discord.stores;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.android.billingclient.api.Purchase;
import com.discord.restapi.RestAPIParams;
import com.discord.restapi.utils.RetryWithDelay;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.Clock;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import retrofit2.HttpException;
import rx.Observable;
import rx.subjects.PublishSubject;
/* compiled from: StoreGooglePlayPurchases.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Â\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u0000 v2\u00020\u0001:\u0006wvxyz{B7\u0012\u0006\u0010Q\u001a\u00020P\u0012\u0006\u0010r\u001a\u00020q\u0012\u0006\u0010k\u001a\u00020j\u0012\u0006\u0010f\u001a\u00020e\u0012\u0006\u0010^\u001a\u00020]\u0012\u0006\u0010[\u001a\u00020Z¢\u0006\u0004\bt\u0010uJ\u001f\u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0002H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J'\u0010\n\u001a\u00020\t2\u0016\u0010\b\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0002H\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u001b\u0010\r\u001a\u00020\t2\n\u0010\f\u001a\u00060\u0003j\u0002`\u0004H\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u001d\u0010\u000f\u001a\u0004\u0018\u00010\u00052\n\u0010\f\u001a\u00060\u0003j\u0002`\u0004H\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u0013\u001a\u00020\t2\u0006\u0010\u0012\u001a\u00020\u0011H\u0003¢\u0006\u0004\b\u0013\u0010\u0014J'\u0010\u0019\u001a\u00020\t2\u000e\u0010\u0017\u001a\n\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u00152\u0006\u0010\u0018\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0019\u0010\u001aJ\u0017\u0010\u001c\u001a\u00020\t2\u0006\u0010\u001b\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u001c\u0010\u000eJ\u0017\u0010\u001d\u001a\u00020\t2\u0006\u0010\u001b\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u001d\u0010\u000eJ\u0017\u0010!\u001a\u00020 2\u0006\u0010\u001f\u001a\u00020\u001eH\u0002¢\u0006\u0004\b!\u0010\"J\r\u0010$\u001a\u00020#¢\u0006\u0004\b$\u0010%J\u0013\u0010'\u001a\b\u0012\u0004\u0012\u00020#0&¢\u0006\u0004\b'\u0010(J\r\u0010*\u001a\u00020)¢\u0006\u0004\b*\u0010+J\u0013\u0010,\u001a\b\u0012\u0004\u0012\u00020)0&¢\u0006\u0004\b,\u0010(J\u0013\u0010.\u001a\b\u0012\u0004\u0012\u00020-0&¢\u0006\u0004\b.\u0010(J=\u00108\u001a\u00020\t2\n\u0010\f\u001a\u00060\u0003j\u0002`\u00042\n\u00101\u001a\u00060/j\u0002`02\u0006\u00103\u001a\u0002022\u0006\u00105\u001a\u0002042\u0006\u00107\u001a\u000206¢\u0006\u0004\b8\u00109J)\u0010<\u001a\u00020\t2\n\u0010\f\u001a\u00060\u0003j\u0002`\u00042\u0006\u0010:\u001a\u00020\u00032\u0006\u0010;\u001a\u00020\u0003¢\u0006\u0004\b<\u0010=J\u0019\u0010>\u001a\u00020\t2\n\u0010\f\u001a\u00060\u0003j\u0002`\u0004¢\u0006\u0004\b>\u0010\u000eJ\u0019\u0010?\u001a\u00020\t2\n\u0010\f\u001a\u00060\u0003j\u0002`\u0004¢\u0006\u0004\b?\u0010\u000eJ\u0017\u0010A\u001a\u00020\t2\b\u0010@\u001a\u0004\u0018\u00010\u0011¢\u0006\u0004\bA\u0010\u0014J\u000f\u0010B\u001a\u00020\tH\u0016¢\u0006\u0004\bB\u0010CJ%\u0010E\u001a\u00020\t2\u000e\u0010D\u001a\n\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u00152\u0006\u0010\u0018\u001a\u00020\u0003¢\u0006\u0004\bE\u0010\u001aJ\r\u0010F\u001a\u00020\t¢\u0006\u0004\bF\u0010CJ\r\u0010G\u001a\u00020 ¢\u0006\u0004\bG\u0010HJ\r\u0010I\u001a\u00020\t¢\u0006\u0004\bI\u0010CJ\r\u0010J\u001a\u00020\t¢\u0006\u0004\bJ\u0010CJ\u0015\u0010M\u001a\u00020\t2\u0006\u0010L\u001a\u00020K¢\u0006\u0004\bM\u0010NJ\u0015\u0010O\u001a\u00020\t2\u0006\u0010L\u001a\u00020K¢\u0006\u0004\bO\u0010NR\u0016\u0010Q\u001a\u00020P8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bQ\u0010RR\u001c\u0010U\u001a\b\u0012\u0004\u0012\u00020\u00160\u00158B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\bS\u0010TR\u001c\u0010W\u001a\b\u0012\u0004\u0012\u00020\u00160\u00158B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\bV\u0010TR\u0016\u0010X\u001a\u00020#8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bX\u0010YR\u0016\u0010[\u001a\u00020Z8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b[\u0010\\R\u0016\u0010^\u001a\u00020]8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b^\u0010_R\u0018\u0010b\u001a\u0004\u0018\u00010\u00118B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b`\u0010aR\u0016\u0010c\u001a\u00020)8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bc\u0010dR\u0016\u0010f\u001a\u00020e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bf\u0010gR\u0016\u0010h\u001a\u00020#8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bh\u0010YR\u0016\u0010i\u001a\u00020)8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bi\u0010dR\u0016\u0010k\u001a\u00020j8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bk\u0010lR:\u0010o\u001a&\u0012\f\u0012\n n*\u0004\u0018\u00010-0- n*\u0012\u0012\f\u0012\n n*\u0004\u0018\u00010-0-\u0018\u00010m0m8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bo\u0010pR\u0016\u0010r\u001a\u00020q8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\br\u0010s¨\u0006|"}, d2 = {"Lcom/discord/stores/StoreGooglePlayPurchases;", "Lcom/discord/stores/StoreV2;", "", "", "Lcom/discord/primitives/PaymentGatewaySkuId;", "Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;", "getCachedAnalyticsTraitsMap", "()Ljava/util/Map;", "analyticsTraitsMap", "", "cacheAnalyticsTraits", "(Ljava/util/Map;)V", "paymentGatewaySkuId", "clearAnalyticsTraits", "(Ljava/lang/String;)V", "getOrClearAnalyticsTraits", "(Ljava/lang/String;)Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;", "Lcom/discord/stores/PendingDowngrade;", "downgrade", "doDowngrade", "(Lcom/discord/stores/PendingDowngrade;)V", "", "Lcom/android/billingclient/api/Purchase;", "newPurchases", "skuType", "handlePurchases", "(Ljava/util/List;Ljava/lang/String;)V", "newSkuName", "handleDowngradeSuccess", "handleDowngradeFailure", "", "throwable", "", "shouldRetryDowngrade", "(Ljava/lang/Throwable;)Z", "Lcom/discord/stores/StoreGooglePlayPurchases$State;", "getState", "()Lcom/discord/stores/StoreGooglePlayPurchases$State;", "Lrx/Observable;", "observeState", "()Lrx/Observable;", "Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;", "getQueryState", "()Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;", "observeQueryState", "Lcom/discord/stores/StoreGooglePlayPurchases$Event;", "observeEvents", "", "Lcom/discord/primitives/SkuId;", "skuId", "Lcom/discord/utilities/analytics/Traits$Location;", "locationTrait", "Lcom/discord/utilities/analytics/Traits$StoreSku;", "storeSkuTrait", "Lcom/discord/utilities/analytics/Traits$Payment;", "paymentTrait", "trackPaymentFlowStarted", "(Ljava/lang/String;JLcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$StoreSku;Lcom/discord/utilities/analytics/Traits$Payment;)V", "fromStep", "toStep", "trackPaymentFlowStep", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "trackPaymentFlowFailed", "trackPaymentFlowCompleted", "newPendingDowngrade", "updatePendingDowngrade", "snapshotData", "()V", "purchases", "processPurchases", "downgradePurchase", "hasSeenGiftingWarning", "()Z", "markViewedGiftingWarning", "onVerificationStart", "Lcom/discord/stores/StoreGooglePlayPurchases$VerificationResult;", "verificationResult", "onVerificationSuccess", "(Lcom/discord/stores/StoreGooglePlayPurchases$VerificationResult;)V", "onVerificationFailure", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "getIapPurchases", "()Ljava/util/List;", "iapPurchases", "getSubscriptionPurchases", "subscriptionPurchases", "storeStateSnapshot", "Lcom/discord/stores/StoreGooglePlayPurchases$State;", "Lcom/discord/utilities/analytics/AnalyticsTracker;", "analyticsTracker", "Lcom/discord/utilities/analytics/AnalyticsTracker;", "Lcom/google/gson/Gson;", "gson", "Lcom/google/gson/Gson;", "getPendingDowngrade", "()Lcom/discord/stores/PendingDowngrade;", "pendingDowngrade", "queryState", "Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "storeState", "queryStateSnapshot", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", HookHelper.constructorName, "(Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/utilities/time/Clock;Lcom/google/gson/Gson;Lcom/discord/utilities/analytics/AnalyticsTracker;)V", "Companion", "AnalyticsTrait", "Event", "QueryState", "State", "VerificationResult", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGooglePlayPurchases extends StoreV2 {
    private static final long CACHED_ANALYTICS_TTL = 259200000;
    private static final String CACHE_KEY_PAYMENT_FLOW_ANALYTICS = "CACHE_KEY_PAYMENT_FLOW_ANALYTICS";
    public static final Companion Companion = new Companion(null);
    private static final String VIEWED_GIFTING_WARNING_DIALOG = "VIEWED_GIFTING_WARNING_DIALOG";
    private final AnalyticsTracker analyticsTracker;
    private final Clock clock;
    private final Dispatcher dispatcher;
    private final PublishSubject<Event> eventSubject = PublishSubject.k0();
    private final Gson gson;
    private final ObservationDeck observationDeck;
    private QueryState queryState;
    private QueryState queryStateSnapshot;
    private final RestAPI restAPI;
    private State storeState;
    private State storeStateSnapshot;

    /* compiled from: StoreGooglePlayPurchases.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001B7\u0012\n\u0010\u0011\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010\u0012\u001a\u00060\u0002j\u0002`\u0006\u0012\u0006\u0010\u0013\u001a\u00020\b\u0012\u0006\u0010\u0014\u001a\u00020\u000b\u0012\u0006\u0010\u0015\u001a\u00020\u000e¢\u0006\u0004\b+\u0010,J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\u0007\u001a\u00060\u0002j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0005J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010JJ\u0010\u0016\u001a\u00020\u00002\f\b\u0002\u0010\u0011\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010\u0012\u001a\u00060\u0002j\u0002`\u00062\b\b\u0002\u0010\u0013\u001a\u00020\b2\b\b\u0002\u0010\u0014\u001a\u00020\u000b2\b\b\u0002\u0010\u0015\u001a\u00020\u000eHÆ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u001c\u001a\u00020\u001bHÖ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u001a\u0010 \u001a\u00020\u001f2\b\u0010\u001e\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b \u0010!R\u0019\u0010\u0013\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\"\u001a\u0004\b#\u0010\nR\u0019\u0010\u0015\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010$\u001a\u0004\b%\u0010\u0010R\u0019\u0010\u0014\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010&\u001a\u0004\b'\u0010\rR\u001d\u0010\u0012\u001a\u00060\u0002j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010(\u001a\u0004\b)\u0010\u0005R\u001d\u0010\u0011\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010(\u001a\u0004\b*\u0010\u0005¨\u0006-"}, d2 = {"Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;", "", "", "Lcom/discord/primitives/SkuId;", "component1", "()J", "Lcom/discord/primitives/Timestamp;", "component2", "Lcom/discord/utilities/analytics/Traits$Location;", "component3", "()Lcom/discord/utilities/analytics/Traits$Location;", "Lcom/discord/utilities/analytics/Traits$StoreSku;", "component4", "()Lcom/discord/utilities/analytics/Traits$StoreSku;", "Lcom/discord/utilities/analytics/Traits$Payment;", "component5", "()Lcom/discord/utilities/analytics/Traits$Payment;", "skuId", "timestamp", "locationTrait", "storeSkuTrait", "paymentTrait", "copy", "(JJLcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$StoreSku;Lcom/discord/utilities/analytics/Traits$Payment;)Lcom/discord/stores/StoreGooglePlayPurchases$AnalyticsTrait;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/utilities/analytics/Traits$Location;", "getLocationTrait", "Lcom/discord/utilities/analytics/Traits$Payment;", "getPaymentTrait", "Lcom/discord/utilities/analytics/Traits$StoreSku;", "getStoreSkuTrait", "J", "getTimestamp", "getSkuId", HookHelper.constructorName, "(JJLcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$StoreSku;Lcom/discord/utilities/analytics/Traits$Payment;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class AnalyticsTrait {
        private final Traits.Location locationTrait;
        private final Traits.Payment paymentTrait;
        private final long skuId;
        private final Traits.StoreSku storeSkuTrait;
        private final long timestamp;

        public AnalyticsTrait(long j, long j2, Traits.Location location, Traits.StoreSku storeSku, Traits.Payment payment) {
            m.checkNotNullParameter(location, "locationTrait");
            m.checkNotNullParameter(storeSku, "storeSkuTrait");
            m.checkNotNullParameter(payment, "paymentTrait");
            this.skuId = j;
            this.timestamp = j2;
            this.locationTrait = location;
            this.storeSkuTrait = storeSku;
            this.paymentTrait = payment;
        }

        public final long component1() {
            return this.skuId;
        }

        public final long component2() {
            return this.timestamp;
        }

        public final Traits.Location component3() {
            return this.locationTrait;
        }

        public final Traits.StoreSku component4() {
            return this.storeSkuTrait;
        }

        public final Traits.Payment component5() {
            return this.paymentTrait;
        }

        public final AnalyticsTrait copy(long j, long j2, Traits.Location location, Traits.StoreSku storeSku, Traits.Payment payment) {
            m.checkNotNullParameter(location, "locationTrait");
            m.checkNotNullParameter(storeSku, "storeSkuTrait");
            m.checkNotNullParameter(payment, "paymentTrait");
            return new AnalyticsTrait(j, j2, location, storeSku, payment);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof AnalyticsTrait)) {
                return false;
            }
            AnalyticsTrait analyticsTrait = (AnalyticsTrait) obj;
            return this.skuId == analyticsTrait.skuId && this.timestamp == analyticsTrait.timestamp && m.areEqual(this.locationTrait, analyticsTrait.locationTrait) && m.areEqual(this.storeSkuTrait, analyticsTrait.storeSkuTrait) && m.areEqual(this.paymentTrait, analyticsTrait.paymentTrait);
        }

        public final Traits.Location getLocationTrait() {
            return this.locationTrait;
        }

        public final Traits.Payment getPaymentTrait() {
            return this.paymentTrait;
        }

        public final long getSkuId() {
            return this.skuId;
        }

        public final Traits.StoreSku getStoreSkuTrait() {
            return this.storeSkuTrait;
        }

        public final long getTimestamp() {
            return this.timestamp;
        }

        public int hashCode() {
            int a = (b.a(this.timestamp) + (b.a(this.skuId) * 31)) * 31;
            Traits.Location location = this.locationTrait;
            int i = 0;
            int hashCode = (a + (location != null ? location.hashCode() : 0)) * 31;
            Traits.StoreSku storeSku = this.storeSkuTrait;
            int hashCode2 = (hashCode + (storeSku != null ? storeSku.hashCode() : 0)) * 31;
            Traits.Payment payment = this.paymentTrait;
            if (payment != null) {
                i = payment.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("AnalyticsTrait(skuId=");
            R.append(this.skuId);
            R.append(", timestamp=");
            R.append(this.timestamp);
            R.append(", locationTrait=");
            R.append(this.locationTrait);
            R.append(", storeSkuTrait=");
            R.append(this.storeSkuTrait);
            R.append(", paymentTrait=");
            R.append(this.paymentTrait);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: StoreGooglePlayPurchases.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007R\u0016\u0010\b\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\b\u0010\u0007¨\u0006\u000b"}, d2 = {"Lcom/discord/stores/StoreGooglePlayPurchases$Companion;", "", "", "CACHED_ANALYTICS_TTL", "J", "", StoreGooglePlayPurchases.CACHE_KEY_PAYMENT_FLOW_ANALYTICS, "Ljava/lang/String;", StoreGooglePlayPurchases.VIEWED_GIFTING_WARNING_DIALOG, HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreGooglePlayPurchases.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/stores/StoreGooglePlayPurchases$Event;", "", HookHelper.constructorName, "()V", "PurchaseQueryFailure", "PurchaseQuerySuccess", "Lcom/discord/stores/StoreGooglePlayPurchases$Event$PurchaseQuerySuccess;", "Lcom/discord/stores/StoreGooglePlayPurchases$Event$PurchaseQueryFailure;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class Event {

        /* compiled from: StoreGooglePlayPurchases.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/stores/StoreGooglePlayPurchases$Event$PurchaseQueryFailure;", "Lcom/discord/stores/StoreGooglePlayPurchases$Event;", "", "component1", "()Ljava/lang/String;", "newSkuName", "copy", "(Ljava/lang/String;)Lcom/discord/stores/StoreGooglePlayPurchases$Event$PurchaseQueryFailure;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getNewSkuName", HookHelper.constructorName, "(Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class PurchaseQueryFailure extends Event {
            private final String newSkuName;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public PurchaseQueryFailure(String str) {
                super(null);
                m.checkNotNullParameter(str, "newSkuName");
                this.newSkuName = str;
            }

            public static /* synthetic */ PurchaseQueryFailure copy$default(PurchaseQueryFailure purchaseQueryFailure, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = purchaseQueryFailure.newSkuName;
                }
                return purchaseQueryFailure.copy(str);
            }

            public final String component1() {
                return this.newSkuName;
            }

            public final PurchaseQueryFailure copy(String str) {
                m.checkNotNullParameter(str, "newSkuName");
                return new PurchaseQueryFailure(str);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof PurchaseQueryFailure) && m.areEqual(this.newSkuName, ((PurchaseQueryFailure) obj).newSkuName);
                }
                return true;
            }

            public final String getNewSkuName() {
                return this.newSkuName;
            }

            public int hashCode() {
                String str = this.newSkuName;
                if (str != null) {
                    return str.hashCode();
                }
                return 0;
            }

            public String toString() {
                return a.H(a.R("PurchaseQueryFailure(newSkuName="), this.newSkuName, ")");
            }
        }

        /* compiled from: StoreGooglePlayPurchases.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B?\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0010\b\u0002\u0010\r\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006\u0012\u0010\b\u0002\u0010\u000e\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\t\u0012\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b!\u0010\"J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0018\u0010\u0007\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0018\u0010\n\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\tHÆ\u0003¢\u0006\u0004\b\n\u0010\bJ\u0012\u0010\u000b\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u000b\u0010\u0004JJ\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\u0010\b\u0002\u0010\r\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00062\u0010\b\u0002\u0010\u000e\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\t2\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0004J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0019\u001a\u00020\u00182\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016HÖ\u0003¢\u0006\u0004\b\u0019\u0010\u001aR!\u0010\r\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001b\u001a\u0004\b\u001c\u0010\bR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001d\u001a\u0004\b\u001e\u0010\u0004R!\u0010\u000e\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001b\u001a\u0004\b\u001f\u0010\bR\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001d\u001a\u0004\b \u0010\u0004¨\u0006#"}, d2 = {"Lcom/discord/stores/StoreGooglePlayPurchases$Event$PurchaseQuerySuccess;", "Lcom/discord/stores/StoreGooglePlayPurchases$Event;", "", "component1", "()Ljava/lang/String;", "", "Lcom/discord/primitives/SkuId;", "component2", "()Ljava/lang/Long;", "Lcom/discord/primitives/PlanId;", "component3", "component4", "newSkuName", "skuId", "subscriptionPlanId", "giftCode", "copy", "(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;)Lcom/discord/stores/StoreGooglePlayPurchases$Event$PurchaseQuerySuccess;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Long;", "getSkuId", "Ljava/lang/String;", "getGiftCode", "getSubscriptionPlanId", "getNewSkuName", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class PurchaseQuerySuccess extends Event {
            private final String giftCode;
            private final String newSkuName;
            private final Long skuId;
            private final Long subscriptionPlanId;

            public /* synthetic */ PurchaseQuerySuccess(String str, Long l, Long l2, String str2, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this(str, (i & 2) != 0 ? null : l, (i & 4) != 0 ? null : l2, (i & 8) != 0 ? null : str2);
            }

            public static /* synthetic */ PurchaseQuerySuccess copy$default(PurchaseQuerySuccess purchaseQuerySuccess, String str, Long l, Long l2, String str2, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = purchaseQuerySuccess.newSkuName;
                }
                if ((i & 2) != 0) {
                    l = purchaseQuerySuccess.skuId;
                }
                if ((i & 4) != 0) {
                    l2 = purchaseQuerySuccess.subscriptionPlanId;
                }
                if ((i & 8) != 0) {
                    str2 = purchaseQuerySuccess.giftCode;
                }
                return purchaseQuerySuccess.copy(str, l, l2, str2);
            }

            public final String component1() {
                return this.newSkuName;
            }

            public final Long component2() {
                return this.skuId;
            }

            public final Long component3() {
                return this.subscriptionPlanId;
            }

            public final String component4() {
                return this.giftCode;
            }

            public final PurchaseQuerySuccess copy(String str, Long l, Long l2, String str2) {
                m.checkNotNullParameter(str, "newSkuName");
                return new PurchaseQuerySuccess(str, l, l2, str2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof PurchaseQuerySuccess)) {
                    return false;
                }
                PurchaseQuerySuccess purchaseQuerySuccess = (PurchaseQuerySuccess) obj;
                return m.areEqual(this.newSkuName, purchaseQuerySuccess.newSkuName) && m.areEqual(this.skuId, purchaseQuerySuccess.skuId) && m.areEqual(this.subscriptionPlanId, purchaseQuerySuccess.subscriptionPlanId) && m.areEqual(this.giftCode, purchaseQuerySuccess.giftCode);
            }

            public final String getGiftCode() {
                return this.giftCode;
            }

            public final String getNewSkuName() {
                return this.newSkuName;
            }

            public final Long getSkuId() {
                return this.skuId;
            }

            public final Long getSubscriptionPlanId() {
                return this.subscriptionPlanId;
            }

            public int hashCode() {
                String str = this.newSkuName;
                int i = 0;
                int hashCode = (str != null ? str.hashCode() : 0) * 31;
                Long l = this.skuId;
                int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
                Long l2 = this.subscriptionPlanId;
                int hashCode3 = (hashCode2 + (l2 != null ? l2.hashCode() : 0)) * 31;
                String str2 = this.giftCode;
                if (str2 != null) {
                    i = str2.hashCode();
                }
                return hashCode3 + i;
            }

            public String toString() {
                StringBuilder R = a.R("PurchaseQuerySuccess(newSkuName=");
                R.append(this.newSkuName);
                R.append(", skuId=");
                R.append(this.skuId);
                R.append(", subscriptionPlanId=");
                R.append(this.subscriptionPlanId);
                R.append(", giftCode=");
                return a.H(R, this.giftCode, ")");
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public PurchaseQuerySuccess(String str, Long l, Long l2, String str2) {
                super(null);
                m.checkNotNullParameter(str, "newSkuName");
                this.newSkuName = str;
                this.skuId = l;
                this.subscriptionPlanId = l2;
                this.giftCode = str2;
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreGooglePlayPurchases.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;", "", HookHelper.constructorName, "()V", "InProgress", "NotInProgress", "Lcom/discord/stores/StoreGooglePlayPurchases$QueryState$NotInProgress;", "Lcom/discord/stores/StoreGooglePlayPurchases$QueryState$InProgress;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class QueryState {

        /* compiled from: StoreGooglePlayPurchases.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreGooglePlayPurchases$QueryState$InProgress;", "Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class InProgress extends QueryState {
            public static final InProgress INSTANCE = new InProgress();

            private InProgress() {
                super(null);
            }
        }

        /* compiled from: StoreGooglePlayPurchases.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreGooglePlayPurchases$QueryState$NotInProgress;", "Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class NotInProgress extends QueryState {
            public static final NotInProgress INSTANCE = new NotInProgress();

            private NotInProgress() {
                super(null);
            }
        }

        private QueryState() {
        }

        public /* synthetic */ QueryState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreGooglePlayPurchases.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/stores/StoreGooglePlayPurchases$State;", "", HookHelper.constructorName, "()V", "Loaded", "Uninitialized", "Lcom/discord/stores/StoreGooglePlayPurchases$State$Uninitialized;", "Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class State {

        /* compiled from: StoreGooglePlayPurchases.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreGooglePlayPurchases$State$Uninitialized;", "Lcom/discord/stores/StoreGooglePlayPurchases$State;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Uninitialized extends State {
            public static final Uninitialized INSTANCE = new Uninitialized();

            private Uninitialized() {
                super(null);
            }
        }

        private State() {
        }

        /* compiled from: StoreGooglePlayPurchases.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B1\u0012\u000e\b\u0002\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\u000e\b\u0002\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\b\u0010\r\u001a\u0004\u0018\u00010\b¢\u0006\u0004\b \u0010!J\u0013\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0016\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0005J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0005J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ<\u0010\u000e\u001a\u00020\u00002\u000e\b\u0002\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\u000e\b\u0002\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0019\u001a\u00020\u00182\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016HÖ\u0003¢\u0006\u0004\b\u0019\u0010\u001aR\u001f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001b\u001a\u0004\b\u001c\u0010\u0005R\u001f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001b\u001a\u0004\b\u001d\u0010\u0005R\u001b\u0010\r\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001e\u001a\u0004\b\u001f\u0010\n¨\u0006\""}, d2 = {"Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;", "Lcom/discord/stores/StoreGooglePlayPurchases$State;", "", "Lcom/android/billingclient/api/Purchase;", "getPurchases", "()Ljava/util/List;", "component1", "component2", "Lcom/discord/stores/PendingDowngrade;", "component3", "()Lcom/discord/stores/PendingDowngrade;", "subscriptionPurchases", "iapPurchases", "pendingDowngrade", "copy", "(Ljava/util/List;Ljava/util/List;Lcom/discord/stores/PendingDowngrade;)Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getSubscriptionPurchases", "getIapPurchases", "Lcom/discord/stores/PendingDowngrade;", "getPendingDowngrade", HookHelper.constructorName, "(Ljava/util/List;Ljava/util/List;Lcom/discord/stores/PendingDowngrade;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Loaded extends State {
            private final List<Purchase> iapPurchases;
            private final PendingDowngrade pendingDowngrade;
            private final List<Purchase> subscriptionPurchases;

            public /* synthetic */ Loaded(List list, List list2, PendingDowngrade pendingDowngrade, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this((i & 1) != 0 ? n.emptyList() : list, (i & 2) != 0 ? n.emptyList() : list2, pendingDowngrade);
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Loaded copy$default(Loaded loaded, List list, List list2, PendingDowngrade pendingDowngrade, int i, Object obj) {
                if ((i & 1) != 0) {
                    list = loaded.subscriptionPurchases;
                }
                if ((i & 2) != 0) {
                    list2 = loaded.iapPurchases;
                }
                if ((i & 4) != 0) {
                    pendingDowngrade = loaded.pendingDowngrade;
                }
                return loaded.copy(list, list2, pendingDowngrade);
            }

            public final List<Purchase> component1() {
                return this.subscriptionPurchases;
            }

            public final List<Purchase> component2() {
                return this.iapPurchases;
            }

            public final PendingDowngrade component3() {
                return this.pendingDowngrade;
            }

            public final Loaded copy(List<? extends Purchase> list, List<? extends Purchase> list2, PendingDowngrade pendingDowngrade) {
                m.checkNotNullParameter(list, "subscriptionPurchases");
                m.checkNotNullParameter(list2, "iapPurchases");
                return new Loaded(list, list2, pendingDowngrade);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return m.areEqual(this.subscriptionPurchases, loaded.subscriptionPurchases) && m.areEqual(this.iapPurchases, loaded.iapPurchases) && m.areEqual(this.pendingDowngrade, loaded.pendingDowngrade);
            }

            public final List<Purchase> getIapPurchases() {
                return this.iapPurchases;
            }

            public final PendingDowngrade getPendingDowngrade() {
                return this.pendingDowngrade;
            }

            public final List<Purchase> getPurchases() {
                return u.plus((Collection) this.subscriptionPurchases, (Iterable) this.iapPurchases);
            }

            public final List<Purchase> getSubscriptionPurchases() {
                return this.subscriptionPurchases;
            }

            public int hashCode() {
                List<Purchase> list = this.subscriptionPurchases;
                int i = 0;
                int hashCode = (list != null ? list.hashCode() : 0) * 31;
                List<Purchase> list2 = this.iapPurchases;
                int hashCode2 = (hashCode + (list2 != null ? list2.hashCode() : 0)) * 31;
                PendingDowngrade pendingDowngrade = this.pendingDowngrade;
                if (pendingDowngrade != null) {
                    i = pendingDowngrade.hashCode();
                }
                return hashCode2 + i;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(subscriptionPurchases=");
                R.append(this.subscriptionPurchases);
                R.append(", iapPurchases=");
                R.append(this.iapPurchases);
                R.append(", pendingDowngrade=");
                R.append(this.pendingDowngrade);
                R.append(")");
                return R.toString();
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Loaded(List<? extends Purchase> list, List<? extends Purchase> list2, PendingDowngrade pendingDowngrade) {
                super(null);
                m.checkNotNullParameter(list, "subscriptionPurchases");
                m.checkNotNullParameter(list2, "iapPurchases");
                this.subscriptionPurchases = list;
                this.iapPurchases = list2;
                this.pendingDowngrade = pendingDowngrade;
            }
        }

        public /* synthetic */ State(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreGooglePlayPurchases.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J&\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u001b\u0010\t\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0016\u001a\u0004\b\u0017\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0018\u001a\u0004\b\u0019\u0010\u0004¨\u0006\u001c"}, d2 = {"Lcom/discord/stores/StoreGooglePlayPurchases$VerificationResult;", "", "Lcom/android/billingclient/api/Purchase;", "component1", "()Lcom/android/billingclient/api/Purchase;", "Lcom/discord/restapi/RestAPIParams$VerifyPurchaseResponse;", "component2", "()Lcom/discord/restapi/RestAPIParams$VerifyPurchaseResponse;", "purchase", "apiResponse", "copy", "(Lcom/android/billingclient/api/Purchase;Lcom/discord/restapi/RestAPIParams$VerifyPurchaseResponse;)Lcom/discord/stores/StoreGooglePlayPurchases$VerificationResult;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/restapi/RestAPIParams$VerifyPurchaseResponse;", "getApiResponse", "Lcom/android/billingclient/api/Purchase;", "getPurchase", HookHelper.constructorName, "(Lcom/android/billingclient/api/Purchase;Lcom/discord/restapi/RestAPIParams$VerifyPurchaseResponse;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class VerificationResult {
        private final RestAPIParams.VerifyPurchaseResponse apiResponse;
        private final Purchase purchase;

        public VerificationResult(Purchase purchase, RestAPIParams.VerifyPurchaseResponse verifyPurchaseResponse) {
            m.checkNotNullParameter(purchase, "purchase");
            this.purchase = purchase;
            this.apiResponse = verifyPurchaseResponse;
        }

        public static /* synthetic */ VerificationResult copy$default(VerificationResult verificationResult, Purchase purchase, RestAPIParams.VerifyPurchaseResponse verifyPurchaseResponse, int i, Object obj) {
            if ((i & 1) != 0) {
                purchase = verificationResult.purchase;
            }
            if ((i & 2) != 0) {
                verifyPurchaseResponse = verificationResult.apiResponse;
            }
            return verificationResult.copy(purchase, verifyPurchaseResponse);
        }

        public final Purchase component1() {
            return this.purchase;
        }

        public final RestAPIParams.VerifyPurchaseResponse component2() {
            return this.apiResponse;
        }

        public final VerificationResult copy(Purchase purchase, RestAPIParams.VerifyPurchaseResponse verifyPurchaseResponse) {
            m.checkNotNullParameter(purchase, "purchase");
            return new VerificationResult(purchase, verifyPurchaseResponse);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof VerificationResult)) {
                return false;
            }
            VerificationResult verificationResult = (VerificationResult) obj;
            return m.areEqual(this.purchase, verificationResult.purchase) && m.areEqual(this.apiResponse, verificationResult.apiResponse);
        }

        public final RestAPIParams.VerifyPurchaseResponse getApiResponse() {
            return this.apiResponse;
        }

        public final Purchase getPurchase() {
            return this.purchase;
        }

        public int hashCode() {
            Purchase purchase = this.purchase;
            int i = 0;
            int hashCode = (purchase != null ? purchase.hashCode() : 0) * 31;
            RestAPIParams.VerifyPurchaseResponse verifyPurchaseResponse = this.apiResponse;
            if (verifyPurchaseResponse != null) {
                i = verifyPurchaseResponse.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("VerificationResult(purchase=");
            R.append(this.purchase);
            R.append(", apiResponse=");
            R.append(this.apiResponse);
            R.append(")");
            return R.toString();
        }
    }

    public StoreGooglePlayPurchases(ObservationDeck observationDeck, Dispatcher dispatcher, RestAPI restAPI, Clock clock, Gson gson, AnalyticsTracker analyticsTracker) {
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(restAPI, "restAPI");
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(gson, "gson");
        m.checkNotNullParameter(analyticsTracker, "analyticsTracker");
        this.observationDeck = observationDeck;
        this.dispatcher = dispatcher;
        this.restAPI = restAPI;
        this.clock = clock;
        this.gson = gson;
        this.analyticsTracker = analyticsTracker;
        State.Uninitialized uninitialized = State.Uninitialized.INSTANCE;
        this.storeState = uninitialized;
        this.storeStateSnapshot = uninitialized;
        QueryState.NotInProgress notInProgress = QueryState.NotInProgress.INSTANCE;
        this.queryState = notInProgress;
        this.queryStateSnapshot = notInProgress;
    }

    private final void cacheAnalyticsTraits(Map<String, AnalyticsTrait> map) {
        getPrefs().edit().putString(CACHE_KEY_PAYMENT_FLOW_ANALYTICS, this.gson.m(map)).apply();
    }

    private final void clearAnalyticsTraits(String str) {
        Map<String, AnalyticsTrait> cachedAnalyticsTraitsMap = getCachedAnalyticsTraitsMap();
        cachedAnalyticsTraitsMap.remove(str);
        cacheAnalyticsTraits(cachedAnalyticsTraitsMap);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void doDowngrade(PendingDowngrade pendingDowngrade) {
        String component1 = pendingDowngrade.component1();
        String component2 = pendingDowngrade.component2();
        String component3 = pendingDowngrade.component3();
        RestAPIParams.DowngradeSubscriptionBody downgradeSubscriptionBody = new RestAPIParams.DowngradeSubscriptionBody(component2, component1, component3);
        RetryWithDelay retryWithDelay = RetryWithDelay.INSTANCE;
        Observable<Object> X = this.restAPI.downgradeSubscription(downgradeSubscriptionBody).X(j0.p.a.c());
        m.checkNotNullExpressionValue(X, "restAPI\n        .downgra…scribeOn(Schedulers.io())");
        ObservableExtensionsKt.appSubscribe(RetryWithDelay.restRetry$default(retryWithDelay, X, 0L, null, null, new StoreGooglePlayPurchases$doDowngrade$1(this), 1, null), StoreGooglePlayPurchases.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new StoreGooglePlayPurchases$doDowngrade$3(this, component3), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreGooglePlayPurchases$doDowngrade$2(this, component3));
    }

    private final Map<String, AnalyticsTrait> getCachedAnalyticsTraitsMap() {
        String string = getPrefs().getString(CACHE_KEY_PAYMENT_FLOW_ANALYTICS, null);
        if (string != null) {
            Map<String, AnalyticsTrait> map = (Map) this.gson.g(string, new TypeToken<Map<String, ? extends AnalyticsTrait>>() { // from class: com.discord.stores.StoreGooglePlayPurchases$getCachedAnalyticsTraitsMap$1$typeToken$1
            }.getType());
            if (map != null) {
                return map;
            }
        }
        return new LinkedHashMap();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final List<Purchase> getIapPurchases() {
        State state = getState();
        List<Purchase> list = null;
        if (!(state instanceof State.Loaded)) {
            state = null;
        }
        State.Loaded loaded = (State.Loaded) state;
        if (loaded != null) {
            list = loaded.getIapPurchases();
        }
        return list != null ? list : n.emptyList();
    }

    private final AnalyticsTrait getOrClearAnalyticsTraits(String str) {
        AnalyticsTrait analyticsTrait = getCachedAnalyticsTraitsMap().get(str);
        if (analyticsTrait == null) {
            return null;
        }
        if (!(this.clock.currentTimeMillis() - analyticsTrait.getTimestamp() > CACHED_ANALYTICS_TTL)) {
            return analyticsTrait;
        }
        clearAnalyticsTraits(str);
        return null;
    }

    private final PendingDowngrade getPendingDowngrade() {
        State state = getState();
        if (!(state instanceof State.Loaded)) {
            state = null;
        }
        State.Loaded loaded = (State.Loaded) state;
        if (loaded != null) {
            return loaded.getPendingDowngrade();
        }
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final List<Purchase> getSubscriptionPurchases() {
        State state = getState();
        List<Purchase> list = null;
        if (!(state instanceof State.Loaded)) {
            state = null;
        }
        State.Loaded loaded = (State.Loaded) state;
        if (loaded != null) {
            list = loaded.getSubscriptionPurchases();
        }
        return list != null ? list : n.emptyList();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleDowngradeFailure(String str) {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.PurchaseQueryFailure(str));
        updatePendingDowngrade(null);
        this.queryState = QueryState.NotInProgress.INSTANCE;
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleDowngradeSuccess(String str) {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.PurchaseQuerySuccess(str, null, null, null, 14, null));
        updatePendingDowngrade(null);
        this.queryState = QueryState.NotInProgress.INSTANCE;
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handlePurchases(List<? extends Purchase> list, String str) {
        State state;
        int hashCode = str.hashCode();
        boolean z2 = false;
        if (hashCode == 3541555 ? !(!str.equals("subs") || list == null || list.size() != getSubscriptionPurchases().size() || !list.containsAll(getSubscriptionPurchases()) || !getSubscriptionPurchases().containsAll(list)) : !(hashCode != 100343516 || !str.equals("inapp") || list == null || list.size() != getIapPurchases().size() || !list.containsAll(getIapPurchases()) || !getIapPurchases().containsAll(list))) {
            z2 = true;
        }
        if (list == null || !(!list.isEmpty()) || !z2) {
            int hashCode2 = str.hashCode();
            if (hashCode2 != 3541555) {
                if (hashCode2 == 100343516 && str.equals("inapp")) {
                    if (list == null) {
                        list = n.emptyList();
                    }
                    state = new State.Loaded(getSubscriptionPurchases(), list, getPendingDowngrade());
                    this.storeState = state;
                    markChanged();
                }
                state = State.Uninitialized.INSTANCE;
                this.storeState = state;
                markChanged();
            }
            if (str.equals("subs")) {
                if (list == null) {
                    list = n.emptyList();
                }
                state = new State.Loaded(list, getIapPurchases(), getPendingDowngrade());
                this.storeState = state;
                markChanged();
            }
            state = State.Uninitialized.INSTANCE;
            this.storeState = state;
            markChanged();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final boolean shouldRetryDowngrade(Throwable th) {
        if (th instanceof TimeoutException) {
            return false;
        }
        if (!(th instanceof HttpException)) {
            return th instanceof IOException;
        }
        int a = ((HttpException) th).a();
        return 500 <= a && 599 >= a;
    }

    public final void downgradePurchase() {
        this.dispatcher.schedule(new StoreGooglePlayPurchases$downgradePurchase$1(this));
    }

    public final QueryState getQueryState() {
        return this.queryStateSnapshot;
    }

    public final State getState() {
        return this.storeStateSnapshot;
    }

    public final boolean hasSeenGiftingWarning() {
        return getPrefs().getBoolean(VIEWED_GIFTING_WARNING_DIALOG, false);
    }

    public final void markViewedGiftingWarning() {
        getPrefs().edit().putBoolean(VIEWED_GIFTING_WARNING_DIALOG, true).apply();
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    public final Observable<QueryState> observeQueryState() {
        Observable<QueryState> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreGooglePlayPurchases$observeQueryState$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<State> observeState() {
        Observable<State> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreGooglePlayPurchases$observeState$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR…  .distinctUntilChanged()");
        return q;
    }

    public final void onVerificationFailure(VerificationResult verificationResult) {
        m.checkNotNullParameter(verificationResult, "verificationResult");
        this.dispatcher.schedule(new StoreGooglePlayPurchases$onVerificationFailure$1(this, verificationResult));
    }

    public final void onVerificationStart() {
        this.dispatcher.schedule(new StoreGooglePlayPurchases$onVerificationStart$1(this));
    }

    public final void onVerificationSuccess(VerificationResult verificationResult) {
        m.checkNotNullParameter(verificationResult, "verificationResult");
        ArrayList<String> b2 = verificationResult.getPurchase().b();
        m.checkNotNullExpressionValue(b2, "verificationResult.purchase.skus");
        for (String str : b2) {
            this.dispatcher.schedule(new StoreGooglePlayPurchases$onVerificationSuccess$$inlined$forEach$lambda$1(str, this, verificationResult));
        }
    }

    public final void processPurchases(List<? extends Purchase> list, String str) {
        m.checkNotNullParameter(str, "skuType");
        this.dispatcher.schedule(new StoreGooglePlayPurchases$processPurchases$1(this, list, str));
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        super.snapshotData();
        State state = this.storeState;
        if (state instanceof State.Loaded) {
            State.Loaded loaded = (State.Loaded) state;
            state = State.Loaded.copy$default(loaded, new ArrayList(loaded.getSubscriptionPurchases()), new ArrayList(loaded.getIapPurchases()), null, 4, null);
        } else if (!m.areEqual(state, State.Uninitialized.INSTANCE)) {
            throw new NoWhenBranchMatchedException();
        }
        this.storeStateSnapshot = state;
        this.queryStateSnapshot = this.queryState;
    }

    public final void trackPaymentFlowCompleted(String str) {
        m.checkNotNullParameter(str, "paymentGatewaySkuId");
        AnalyticsTrait orClearAnalyticsTraits = getOrClearAnalyticsTraits(str);
        if (orClearAnalyticsTraits != null) {
            this.analyticsTracker.paymentFlowCompleted(orClearAnalyticsTraits.getLocationTrait(), (r13 & 2) != 0 ? null : null, (r13 & 4) != 0 ? null : orClearAnalyticsTraits.getPaymentTrait(), (r13 & 8) != 0 ? null : orClearAnalyticsTraits.getStoreSkuTrait(), (r13 & 16) != 0 ? null : null);
            clearAnalyticsTraits(str);
        }
    }

    public final void trackPaymentFlowFailed(String str) {
        m.checkNotNullParameter(str, "paymentGatewaySkuId");
        AnalyticsTrait orClearAnalyticsTraits = getOrClearAnalyticsTraits(str);
        if (orClearAnalyticsTraits != null) {
            AnalyticsTracker.paymentFlowFailed$default(this.analyticsTracker, orClearAnalyticsTraits.getLocationTrait(), null, orClearAnalyticsTraits.getStoreSkuTrait(), orClearAnalyticsTraits.getPaymentTrait(), 2, null);
            clearAnalyticsTraits(str);
        }
    }

    public final void trackPaymentFlowStarted(String str, long j, Traits.Location location, Traits.StoreSku storeSku, Traits.Payment payment) {
        m.checkNotNullParameter(str, "paymentGatewaySkuId");
        m.checkNotNullParameter(location, "locationTrait");
        m.checkNotNullParameter(storeSku, "storeSkuTrait");
        m.checkNotNullParameter(payment, "paymentTrait");
        AnalyticsTrait analyticsTrait = new AnalyticsTrait(j, this.clock.currentTimeMillis(), location, storeSku, payment);
        Map<String, AnalyticsTrait> cachedAnalyticsTraitsMap = getCachedAnalyticsTraitsMap();
        cachedAnalyticsTraitsMap.put(str, analyticsTrait);
        cacheAnalyticsTraits(cachedAnalyticsTraitsMap);
        AnalyticsTracker.paymentFlowStarted$default(this.analyticsTracker, analyticsTrait.getLocationTrait(), null, analyticsTrait.getStoreSkuTrait(), analyticsTrait.getPaymentTrait(), 2, null);
    }

    public final void trackPaymentFlowStep(String str, String str2, String str3) {
        a.n0(str, "paymentGatewaySkuId", str2, "fromStep", str3, "toStep");
        AnalyticsTrait orClearAnalyticsTraits = getOrClearAnalyticsTraits(str);
        if (orClearAnalyticsTraits != null) {
            this.analyticsTracker.paymentFlowStep(orClearAnalyticsTraits.getLocationTrait(), (r16 & 2) != 0 ? null : null, str3, str2, (r16 & 16) != 0 ? null : orClearAnalyticsTraits.getStoreSkuTrait(), (r16 & 32) != 0 ? null : orClearAnalyticsTraits.getPaymentTrait());
        }
    }

    public final void updatePendingDowngrade(PendingDowngrade pendingDowngrade) {
        this.dispatcher.schedule(new StoreGooglePlayPurchases$updatePendingDowngrade$1(this, pendingDowngrade));
    }
}
