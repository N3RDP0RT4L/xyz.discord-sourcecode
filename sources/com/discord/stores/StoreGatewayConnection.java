package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.exifinterface.media.ExifInterface;
import b.a.d.k;
import b.a.d.l;
import b.a.e.d;
import b.d.b.a.a;
import com.discord.BuildConfig;
import com.discord.api.activity.Activity;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelRecipient;
import com.discord.api.commands.ApplicationCommandAutocompleteResult;
import com.discord.api.commands.GuildApplicationCommands;
import com.discord.api.embeddedactivities.EmbeddedActivityInboundUpdate;
import com.discord.api.emoji.GuildEmojisUpdate;
import com.discord.api.friendsuggestions.FriendSuggestion;
import com.discord.api.friendsuggestions.FriendSuggestionDelete;
import com.discord.api.guild.Guild;
import com.discord.api.guildjoinrequest.GuildJoinRequestCreateOrUpdate;
import com.discord.api.guildjoinrequest.GuildJoinRequestDelete;
import com.discord.api.guildmember.GuildMember;
import com.discord.api.guildmember.GuildMemberRemove;
import com.discord.api.guildmember.GuildMembersChunk;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.guildscheduledevent.GuildScheduledEventUserUpdate;
import com.discord.api.interaction.InteractionStateUpdate;
import com.discord.api.message.Message;
import com.discord.api.message.reaction.MessageReactionUpdate;
import com.discord.api.presence.ClientStatus;
import com.discord.api.presence.Presence;
import com.discord.api.requiredaction.UserRequiredActionUpdate;
import com.discord.api.role.GuildRoleCreate;
import com.discord.api.role.GuildRoleDelete;
import com.discord.api.role.GuildRoleUpdate;
import com.discord.api.stageinstance.StageInstance;
import com.discord.api.sticker.GuildStickersUpdate;
import com.discord.api.thread.ThreadMemberListUpdate;
import com.discord.api.thread.ThreadMemberUpdate;
import com.discord.api.thread.ThreadMembersUpdate;
import com.discord.api.user.TypingUser;
import com.discord.api.user.User;
import com.discord.api.voice.server.VoiceServer;
import com.discord.api.voice.state.VoiceState;
import com.discord.app.App;
import com.discord.app.AppLog;
import com.discord.gateway.GatewayEventHandler;
import com.discord.gateway.GatewaySocket;
import com.discord.gateway.io.OutgoingPayload;
import com.discord.gateway.rest.RestConfig;
import com.discord.models.domain.ModelApplicationStream;
import com.discord.models.domain.ModelBan;
import com.discord.models.domain.ModelCall;
import com.discord.models.domain.ModelChannelUnreadUpdate;
import com.discord.models.domain.ModelGuildIntegration;
import com.discord.models.domain.ModelGuildMemberListUpdate;
import com.discord.models.domain.ModelMessageDelete;
import com.discord.models.domain.ModelNotificationSettings;
import com.discord.models.domain.ModelPayload;
import com.discord.models.domain.ModelReadState;
import com.discord.models.domain.ModelSession;
import com.discord.models.domain.ModelUserNote;
import com.discord.models.domain.ModelUserRelationship;
import com.discord.models.domain.ModelUserSettings;
import com.discord.models.domain.StreamCreateOrUpdate;
import com.discord.models.domain.StreamDelete;
import com.discord.models.domain.StreamServerUpdate;
import com.discord.models.thread.dto.ModelThreadListSync;
import com.discord.rtcconnection.RtcConnection;
import com.discord.stores.StoreClientDataState;
import com.discord.stores.utilities.BatchManager;
import com.discord.stores.utilities.Batched;
import com.discord.utilities.analytics.AnalyticSuperProperties;
import com.discord.utilities.fcm.NotificationData;
import com.discord.utilities.lazy.subscriptions.GuildSubscriptions;
import com.discord.utilities.logging.AppGatewaySocketLogger;
import com.discord.utilities.networking.NetworkMonitor;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$3;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$4;
import com.discord.utilities.ssl.SecureSocketsLayerUtils;
import com.discord.utilities.time.Clock;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import javax.net.ssl.SSLSocketFactory;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function5;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlinx.coroutines.ExecutorCoroutineDispatcher;
import okhttp3.Interceptor;
import rx.Observable;
import rx.Scheduler;
import rx.functions.Func5;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import s.a.w0;
/* compiled from: StoreGatewayConnection.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000ô\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u00002\u00020\u0001:\u0002Î\u0002BA\b\u0007\u0012\b\u0010\u0098\u0002\u001a\u00030\u0097\u0002\u0012\b\u0010ó\u0001\u001a\u00030ò\u0001\u0012\n\b\u0002\u0010º\u0002\u001a\u00030¹\u0002\u0012\n\b\u0002\u0010Ä\u0002\u001a\u00030Ã\u0002\u0012\b\u0010¤\u0002\u001a\u00030£\u0002¢\u0006\u0006\bÌ\u0002\u0010Í\u0002J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u001b\u0010\b\u001a\u00020\u00022\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006H\u0002¢\u0006\u0004\b\b\u0010\tJ\u001f\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\fH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u0011\u0010\u0012\u001a\u0004\u0018\u00010\u0011H\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0016\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u0014H\u0002¢\u0006\u0004\b\u0016\u0010\u0017J#\u0010\u001b\u001a\u00020\u001a2\u0012\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u00020\u0018H\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ.\u0010!\u001a\u00020\u0002\"\u0004\b\u0000\u0010\u001d*\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00000\u001e2\u0006\u0010 \u001a\u00020\u001fH\u0082\b¢\u0006\u0004\b!\u0010\"J\u001d\u0010#\u001a\u00020\u00022\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b#\u0010$J\u0019\u0010(\u001a\u00020\u001a2\n\u0010'\u001a\u00060%j\u0002`&¢\u0006\u0004\b(\u0010)JA\u00101\u001a\u00020\u001a2\b\u0010+\u001a\u0004\u0018\u00010*2\n\b\u0002\u0010,\u001a\u0004\u0018\u00010%2\u0010\b\u0002\u0010/\u001a\n\u0012\u0004\u0012\u00020.\u0018\u00010-2\n\b\u0002\u00100\u001a\u0004\u0018\u00010\u001a¢\u0006\u0004\b1\u00102J[\u0010:\u001a\u00020\u001a2\u000e\u00104\u001a\n\u0018\u00010%j\u0004\u0018\u0001`32\u000e\u0010'\u001a\n\u0018\u00010%j\u0004\u0018\u0001`&2\u0006\u00105\u001a\u00020\u001a2\u0006\u00106\u001a\u00020\u001a2\u0006\u00107\u001a\u00020\u001a2\n\b\u0002\u00108\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u00109\u001a\u00020\u001a¢\u0006\u0004\b:\u0010;JI\u0010A\u001a\u00020\u001a2\n\u00104\u001a\u00060%j\u0002`32\n\b\u0002\u0010<\u001a\u0004\u0018\u00010\u00052\u0014\b\u0002\u0010>\u001a\u000e\u0012\b\u0012\u00060%j\u0002`=\u0018\u00010-2\n\b\u0002\u0010@\u001a\u0004\u0018\u00010?H\u0007¢\u0006\u0004\bA\u0010BJ!\u0010E\u001a\u00020\u001a2\n\u00104\u001a\u00060%j\u0002`32\u0006\u0010D\u001a\u00020C¢\u0006\u0004\bE\u0010FJ[\u0010L\u001a\u00020\u001a2\n\u00104\u001a\u00060%j\u0002`32\u0006\u0010G\u001a\u00020\u00052\u0006\u0010H\u001a\u00020\u001a2\n\b\u0002\u0010<\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010I\u001a\u0004\u0018\u00010?2\u0006\u0010J\u001a\u00020?2\u0010\b\u0002\u0010K\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010-¢\u0006\u0004\bL\u0010MJ\r\u0010N\u001a\u00020\u0002¢\u0006\u0004\bN\u0010\u0004J\u001b\u0010O\u001a\u00020\u00022\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006H\u0007¢\u0006\u0004\bO\u0010\tJ!\u0010P\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u00052\b\u00108\u001a\u0004\u0018\u00010\u0005H\u0007¢\u0006\u0004\bP\u0010QJ\u0019\u0010R\u001a\u00020\u00022\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006¢\u0006\u0004\bR\u0010\tJ\r\u0010S\u001a\u00020\u0002¢\u0006\u0004\bS\u0010\u0004J\u0015\u0010V\u001a\u00020\u00022\u0006\u0010U\u001a\u00020T¢\u0006\u0004\bV\u0010WJ\u0013\u0010Y\u001a\b\u0012\u0004\u0012\u00020\u001a0X¢\u0006\u0004\bY\u0010ZJ\u0013\u0010[\u001a\b\u0012\u0004\u0012\u00020\u001a0X¢\u0006\u0004\b[\u0010ZJ\u000f\u0010\\\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\\\u0010]J\u0017\u0010_\u001a\u00020\u00022\u0006\u0010^\u001a\u00020\u001aH\u0016¢\u0006\u0004\b_\u0010`J\u0017\u0010b\u001a\u00020\u00022\u0006\u0010a\u001a\u00020\u001aH\u0016¢\u0006\u0004\bb\u0010`J!\u0010d\u001a\u00020\u00022\b\u0010c\u001a\u0004\u0018\u00010\u00052\u0006\u0010 \u001a\u00020\u001fH\u0016¢\u0006\u0004\bd\u0010eJ\u0017\u0010g\u001a\u00020\u00022\u0006\u0010f\u001a\u00020\u001aH\u0016¢\u0006\u0004\bg\u0010`R5\u0010j\u001a\u001e\u0012\f\u0012\n i*\u0004\u0018\u00010h0h\u0012\f\u0012\n i*\u0004\u0018\u00010h0h0\u001e8\u0006@\u0006¢\u0006\f\n\u0004\bj\u0010k\u001a\u0004\bl\u0010mR5\u0010o\u001a\u001e\u0012\f\u0012\n i*\u0004\u0018\u00010n0n\u0012\f\u0012\n i*\u0004\u0018\u00010n0n0\u001e8\u0006@\u0006¢\u0006\f\n\u0004\bo\u0010k\u001a\u0004\bp\u0010mR5\u0010r\u001a\u001e\u0012\f\u0012\n i*\u0004\u0018\u00010q0q\u0012\f\u0012\n i*\u0004\u0018\u00010q0q0\u001e8\u0006@\u0006¢\u0006\f\n\u0004\br\u0010k\u001a\u0004\bs\u0010mR5\u0010u\u001a\u001e\u0012\f\u0012\n i*\u0004\u0018\u00010t0t\u0012\f\u0012\n i*\u0004\u0018\u00010t0t0\u001e8\u0006@\u0006¢\u0006\f\n\u0004\bu\u0010k\u001a\u0004\bv\u0010mR5\u0010x\u001a\u001e\u0012\f\u0012\n i*\u0004\u0018\u00010w0w\u0012\f\u0012\n i*\u0004\u0018\u00010w0w0\u001e8\u0006@\u0006¢\u0006\f\n\u0004\bx\u0010k\u001a\u0004\by\u0010mR5\u0010z\u001a\u001e\u0012\f\u0012\n i*\u0004\u0018\u00010h0h\u0012\f\u0012\n i*\u0004\u0018\u00010h0h0\u001e8\u0006@\u0006¢\u0006\f\n\u0004\bz\u0010k\u001a\u0004\b{\u0010mRM\u0010}\u001a6\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020| i*\n\u0012\u0004\u0012\u00020|\u0018\u00010-0-\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020| i*\n\u0012\u0004\u0012\u00020|\u0018\u00010-0-0\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b}\u0010k\u001a\u0004\b~\u0010mR2\u0010^\u001a\u001e\u0012\f\u0012\n i*\u0004\u0018\u00010\u001a0\u001a\u0012\f\u0012\n i*\u0004\u0018\u00010\u001a0\u001a0\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b^\u0010kR8\u0010\u0080\u0001\u001a\u001e\u0012\f\u0012\n i*\u0004\u0018\u00010\u007f0\u007f\u0012\f\u0012\n i*\u0004\u0018\u00010\u007f0\u007f0\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b\u0080\u0001\u0010k\u001a\u0005\b\u0081\u0001\u0010mR<\u0010\u0083\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u0082\u00010\u0082\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u0082\u00010\u0082\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b\u0083\u0001\u0010k\u001a\u0005\b\u0084\u0001\u0010mR<\u0010\u0086\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u0085\u00010\u0085\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u0085\u00010\u0085\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b\u0086\u0001\u0010k\u001a\u0005\b\u0087\u0001\u0010mR<\u0010\u0089\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u0088\u00010\u0088\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u0088\u00010\u0088\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b\u0089\u0001\u0010k\u001a\u0005\b\u008a\u0001\u0010mR<\u0010\u008c\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u008b\u00010\u008b\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u008b\u00010\u008b\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b\u008c\u0001\u0010k\u001a\u0005\b\u008d\u0001\u0010mR<\u0010\u008f\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u008e\u00010\u008e\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u008e\u00010\u008e\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b\u008f\u0001\u0010k\u001a\u0005\b\u0090\u0001\u0010mR<\u0010\u0092\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u0091\u00010\u0091\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u0091\u00010\u0091\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b\u0092\u0001\u0010k\u001a\u0005\b\u0093\u0001\u0010mR<\u0010\u0095\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u0094\u00010\u0094\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u0094\u00010\u0094\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b\u0095\u0001\u0010k\u001a\u0005\b\u0096\u0001\u0010mR<\u0010\u0098\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u0097\u00010\u0097\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u0097\u00010\u0097\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b\u0098\u0001\u0010k\u001a\u0005\b\u0099\u0001\u0010mR<\u0010\u009b\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u009a\u00010\u009a\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u009a\u00010\u009a\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b\u009b\u0001\u0010k\u001a\u0005\b\u009c\u0001\u0010mR<\u0010\u009e\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u009d\u00010\u009d\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u009d\u00010\u009d\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b\u009e\u0001\u0010k\u001a\u0005\b\u009f\u0001\u0010mR<\u0010¡\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010 \u00010 \u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010 \u00010 \u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b¡\u0001\u0010k\u001a\u0005\b¢\u0001\u0010mR<\u0010¤\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010£\u00010£\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010£\u00010£\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b¤\u0001\u0010k\u001a\u0005\b¥\u0001\u0010mR<\u0010§\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010¦\u00010¦\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010¦\u00010¦\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b§\u0001\u0010k\u001a\u0005\b¨\u0001\u0010mR8\u0010©\u0001\u001a\u001e\u0012\f\u0012\n i*\u0004\u0018\u00010h0h\u0012\f\u0012\n i*\u0004\u0018\u00010h0h0\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b©\u0001\u0010k\u001a\u0005\bª\u0001\u0010mR<\u0010¬\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010«\u00010«\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010«\u00010«\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b¬\u0001\u0010k\u001a\u0005\b\u00ad\u0001\u0010mR<\u0010¯\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010®\u00010®\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010®\u00010®\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b¯\u0001\u0010k\u001a\u0005\b°\u0001\u0010mR\u001f\u0010²\u0001\u001a\u00030±\u00018\u0006@\u0006¢\u0006\u0010\n\u0006\b²\u0001\u0010³\u0001\u001a\u0006\b´\u0001\u0010µ\u0001R<\u0010¶\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u008b\u00010\u008b\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u008b\u00010\u008b\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b¶\u0001\u0010k\u001a\u0005\b·\u0001\u0010mR<\u0010¹\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010¸\u00010¸\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010¸\u00010¸\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b¹\u0001\u0010k\u001a\u0005\bº\u0001\u0010mR<\u0010¼\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010»\u00010»\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010»\u00010»\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b¼\u0001\u0010k\u001a\u0005\b½\u0001\u0010mR<\u0010¿\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010¾\u00010¾\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010¾\u00010¾\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b¿\u0001\u0010k\u001a\u0005\bÀ\u0001\u0010mR<\u0010Â\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010Á\u00010Á\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010Á\u00010Á\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bÂ\u0001\u0010k\u001a\u0005\bÃ\u0001\u0010mR<\u0010Ä\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010 \u00010 \u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010 \u00010 \u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bÄ\u0001\u0010k\u001a\u0005\bÅ\u0001\u0010mR<\u0010Æ\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u008b\u00010\u008b\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u008b\u00010\u008b\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bÆ\u0001\u0010k\u001a\u0005\bÇ\u0001\u0010mR\u0019\u0010\u0015\u001a\u0004\u0018\u00010\u00148\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u0015\u0010È\u0001R<\u0010É\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u0088\u00010\u0088\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u0088\u00010\u0088\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bÉ\u0001\u0010k\u001a\u0005\bÊ\u0001\u0010mR<\u0010Ì\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010Ë\u00010Ë\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010Ë\u00010Ë\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bÌ\u0001\u0010k\u001a\u0005\bÍ\u0001\u0010mR<\u0010Ï\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010Î\u00010Î\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010Î\u00010Î\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bÏ\u0001\u0010k\u001a\u0005\bÐ\u0001\u0010mR<\u0010Ò\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010Ñ\u00010Ñ\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010Ñ\u00010Ñ\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bÒ\u0001\u0010k\u001a\u0005\bÓ\u0001\u0010mR\u001a\u0010Õ\u0001\u001a\u00030Ô\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bÕ\u0001\u0010Ö\u0001R<\u0010Ø\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010×\u00010×\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010×\u00010×\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bØ\u0001\u0010k\u001a\u0005\bÙ\u0001\u0010mRT\u0010Û\u0001\u001a:\u0012\u001a\u0012\u0018\u0012\u0005\u0012\u00030Ú\u0001 i*\u000b\u0012\u0005\u0012\u00030Ú\u0001\u0018\u00010-0-\u0012\u001a\u0012\u0018\u0012\u0005\u0012\u00030Ú\u0001 i*\u000b\u0012\u0005\u0012\u00030Ú\u0001\u0018\u00010-0-0\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bÛ\u0001\u0010k\u001a\u0005\bÜ\u0001\u0010mR<\u0010Þ\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010Ý\u00010Ý\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010Ý\u00010Ý\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bÞ\u0001\u0010k\u001a\u0005\bß\u0001\u0010mR<\u0010à\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u0085\u00010\u0085\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u0085\u00010\u0085\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bà\u0001\u0010k\u001a\u0005\bá\u0001\u0010mR<\u0010ã\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010â\u00010â\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010â\u00010â\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bã\u0001\u0010k\u001a\u0005\bä\u0001\u0010mR<\u0010æ\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010å\u00010å\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010å\u00010å\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bæ\u0001\u0010k\u001a\u0005\bç\u0001\u0010mR<\u0010é\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010è\u00010è\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010è\u00010è\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bé\u0001\u0010k\u001a\u0005\bê\u0001\u0010mR8\u0010ë\u0001\u001a\u001e\u0012\f\u0012\n i*\u0004\u0018\u00010q0q\u0012\f\u0012\n i*\u0004\u0018\u00010q0q0\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bë\u0001\u0010k\u001a\u0005\bì\u0001\u0010mR<\u0010í\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u0097\u00010\u0097\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u0097\u00010\u0097\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bí\u0001\u0010k\u001a\u0005\bî\u0001\u0010mR<\u0010ð\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010ï\u00010ï\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010ï\u00010ï\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bð\u0001\u0010k\u001a\u0005\bñ\u0001\u0010mR\u001a\u0010ó\u0001\u001a\u00030ò\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bó\u0001\u0010ô\u0001R8\u0010õ\u0001\u001a\u001e\u0012\f\u0012\n i*\u0004\u0018\u00010w0w\u0012\f\u0012\n i*\u0004\u0018\u00010w0w0\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bõ\u0001\u0010k\u001a\u0005\bö\u0001\u0010mR<\u0010÷\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u0085\u00010\u0085\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u0085\u00010\u0085\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b÷\u0001\u0010k\u001a\u0005\bø\u0001\u0010mR<\u0010ú\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010ù\u00010ù\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010ù\u00010ù\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bú\u0001\u0010k\u001a\u0005\bû\u0001\u0010mR<\u0010ý\u0001\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010ü\u00010ü\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010ü\u00010ü\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bý\u0001\u0010k\u001a\u0005\bþ\u0001\u0010mR<\u0010\u0080\u0002\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010ÿ\u00010ÿ\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010ÿ\u00010ÿ\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b\u0080\u0002\u0010k\u001a\u0005\b\u0081\u0002\u0010mR<\u0010\u0082\u0002\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u0097\u00010\u0097\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u0097\u00010\u0097\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b\u0082\u0002\u0010k\u001a\u0005\b\u0083\u0002\u0010mR<\u0010\u0085\u0002\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u0084\u00020\u0084\u0002\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u0084\u00020\u0084\u00020\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b\u0085\u0002\u0010k\u001a\u0005\b\u0086\u0002\u0010mR:\u0010P\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010Ý\u00010Ý\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010Ý\u00010Ý\u00010\u001e8\u0006@\u0006¢\u0006\r\n\u0004\bP\u0010k\u001a\u0005\b\u0087\u0002\u0010mR$\u0010:\u001a\n\u0012\u0005\u0012\u00030\u0089\u00020\u0088\u00028\u0006@\u0006¢\u0006\u000f\n\u0005\b:\u0010\u008a\u0002\u001a\u0006\b\u008b\u0002\u0010\u008c\u0002R<\u0010\u008d\u0002\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010Á\u00010Á\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010Á\u00010Á\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b\u008d\u0002\u0010k\u001a\u0005\b\u008e\u0002\u0010mR8\u0010\u008f\u0002\u001a\u001e\u0012\f\u0012\n i*\u0004\u0018\u00010n0n\u0012\f\u0012\n i*\u0004\u0018\u00010n0n0\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b\u008f\u0002\u0010k\u001a\u0005\b\u0090\u0002\u0010mR<\u0010\u0092\u0002\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u0091\u00020\u0091\u0002\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u0091\u00020\u0091\u00020\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b\u0092\u0002\u0010k\u001a\u0005\b\u0093\u0002\u0010mR:\u00101\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010Ú\u00010Ú\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010Ú\u00010Ú\u00010\u001e8\u0006@\u0006¢\u0006\r\n\u0004\b1\u0010k\u001a\u0005\b\u0094\u0002\u0010mR\u001b\u0010\u0095\u0002\u001a\u0004\u0018\u00010\u000e8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u0095\u0002\u0010\u0096\u0002R\u001a\u0010\u0098\u0002\u001a\u00030\u0097\u00028\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0098\u0002\u0010\u0099\u0002R<\u0010\u009b\u0002\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u009a\u00020\u009a\u0002\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u009a\u00020\u009a\u00020\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b\u009b\u0002\u0010k\u001a\u0005\b\u009c\u0002\u0010mR<\u0010\u009e\u0002\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u009d\u00020\u009d\u0002\u0012\u000e\u0012\f i*\u0005\u0018\u00010\u009d\u00020\u009d\u00020\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b\u009e\u0002\u0010k\u001a\u0005\b\u009f\u0002\u0010mR<\u0010¡\u0002\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010 \u00020 \u0002\u0012\u000e\u0012\f i*\u0005\u0018\u00010 \u00020 \u00020\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b¡\u0002\u0010k\u001a\u0005\b¢\u0002\u0010mR\u001a\u0010¤\u0002\u001a\u00030£\u00028\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b¤\u0002\u0010¥\u0002R2\u0010a\u001a\u001e\u0012\f\u0012\n i*\u0004\u0018\u00010\u001a0\u001a\u0012\f\u0012\n i*\u0004\u0018\u00010\u001a0\u001a0\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\ba\u0010kR:\u0010R\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010¦\u00020¦\u0002\u0012\u000e\u0012\f i*\u0005\u0018\u00010¦\u00020¦\u00020\u001e8\u0006@\u0006¢\u0006\r\n\u0004\bR\u0010k\u001a\u0005\b§\u0002\u0010mR8\u0010¨\u0002\u001a\u001e\u0012\f\u0012\n i*\u0004\u0018\u00010\u00020\u0002\u0012\f\u0012\n i*\u0004\u0018\u00010\u00020\u00020\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b¨\u0002\u0010k\u001a\u0005\b©\u0002\u0010mR<\u0010«\u0002\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010ª\u00020ª\u0002\u0012\u000e\u0012\f i*\u0005\u0018\u00010ª\u00020ª\u00020\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b«\u0002\u0010k\u001a\u0005\b¬\u0002\u0010mR8\u0010\u00ad\u0002\u001a\u001e\u0012\f\u0012\n i*\u0004\u0018\u00010\u00020\u0002\u0012\f\u0012\n i*\u0004\u0018\u00010\u00020\u00020\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b\u00ad\u0002\u0010k\u001a\u0005\b®\u0002\u0010mR<\u0010°\u0002\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010¯\u00020¯\u0002\u0012\u000e\u0012\f i*\u0005\u0018\u00010¯\u00020¯\u00020\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b°\u0002\u0010k\u001a\u0005\b±\u0002\u0010mR8\u0010²\u0002\u001a\u001e\u0012\f\u0012\n i*\u0004\u0018\u00010\u00020\u0002\u0012\f\u0012\n i*\u0004\u0018\u00010\u00020\u00020\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b²\u0002\u0010k\u001a\u0005\b³\u0002\u0010mR<\u0010´\u0002\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010®\u00010®\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010®\u00010®\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b´\u0002\u0010k\u001a\u0005\bµ\u0002\u0010mR<\u0010·\u0002\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010¶\u00020¶\u0002\u0012\u000e\u0012\f i*\u0005\u0018\u00010¶\u00020¶\u00020\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b·\u0002\u0010k\u001a\u0005\b¸\u0002\u0010mR\u001a\u0010º\u0002\u001a\u00030¹\u00028\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bº\u0002\u0010»\u0002R<\u0010¼\u0002\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010Á\u00010Á\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010Á\u00010Á\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b¼\u0002\u0010k\u001a\u0005\b½\u0002\u0010mR<\u0010¾\u0002\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010®\u00010®\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010®\u00010®\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\b¾\u0002\u0010k\u001a\u0005\b¿\u0002\u0010mR<\u0010Á\u0002\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010À\u00020À\u0002\u0012\u000e\u0012\f i*\u0005\u0018\u00010À\u00020À\u00020\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bÁ\u0002\u0010k\u001a\u0005\bÂ\u0002\u0010mR\u001a\u0010Ä\u0002\u001a\u00030Ã\u00028\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bÄ\u0002\u0010Å\u0002R<\u0010Æ\u0002\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010ü\u00010ü\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010ü\u00010ü\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bÆ\u0002\u0010k\u001a\u0005\bÇ\u0002\u0010mR<\u0010È\u0002\u001a\"\u0012\u000e\u0012\f i*\u0005\u0018\u00010Á\u00010Á\u0001\u0012\u000e\u0012\f i*\u0005\u0018\u00010Á\u00010Á\u00010\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bÈ\u0002\u0010k\u001a\u0005\bÉ\u0002\u0010mR8\u0010Ê\u0002\u001a\u001e\u0012\f\u0012\n i*\u0004\u0018\u00010h0h\u0012\f\u0012\n i*\u0004\u0018\u00010h0h0\u001e8\u0006@\u0006¢\u0006\u000e\n\u0005\bÊ\u0002\u0010k\u001a\u0005\bË\u0002\u0010m¨\u0006Ï\u0002"}, d2 = {"Lcom/discord/stores/StoreGatewayConnection;", "Lcom/discord/gateway/GatewayEventHandler;", "", "voiceServerPing", "()V", "", "Lcom/discord/primitives/StreamKey;", "streamKey", "streamPing", "(Ljava/lang/String;)V", "Landroid/content/Context;", "context", "Lcom/discord/utilities/networking/NetworkMonitor;", "networkMonitor", "Lcom/discord/gateway/GatewaySocket;", "buildGatewaySocket", "(Landroid/content/Context;Lcom/discord/utilities/networking/NetworkMonitor;)Lcom/discord/gateway/GatewaySocket;", "Lcom/discord/gateway/GatewaySocket$IdentifyData;", "getIdentifyData", "()Lcom/discord/gateway/GatewaySocket$IdentifyData;", "Lcom/discord/stores/StoreGatewayConnection$ClientState;", "clientState", "handleClientStateUpdate", "(Lcom/discord/stores/StoreGatewayConnection$ClientState;)V", "Lkotlin/Function1;", "socketCallback", "", "requestIfSessionEstablished", "(Lkotlin/jvm/functions/Function1;)Z", ExifInterface.GPS_DIRECTION_TRUE, "Lrx/subjects/SerializedSubject;", "", "data", "onNext", "(Lrx/subjects/SerializedSubject;Ljava/lang/Object;)V", "init", "(Landroid/content/Context;Lcom/discord/utilities/networking/NetworkMonitor;)V", "", "Lcom/discord/primitives/ChannelId;", "channelId", "callConnect", "(J)Z", "Lcom/discord/api/presence/ClientStatus;", "status", "since", "", "Lcom/discord/api/activity/Activity;", "activities", "afk", "presenceUpdate", "(Lcom/discord/api/presence/ClientStatus;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Boolean;)Z", "Lcom/discord/primitives/GuildId;", "guildId", "selfMute", "selfDeaf", "selfVideo", "preferredRegion", "shouldIncludePreferredRegion", "voiceStateUpdate", "(Ljava/lang/Long;Ljava/lang/Long;ZZZLjava/lang/String;Z)Z", "query", "Lcom/discord/primitives/UserId;", "userIds", "", "limit", "requestGuildMembers", "(JLjava/lang/String;Ljava/util/List;Ljava/lang/Integer;)Z", "Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;", "guildSubscriptions", "updateGuildSubscriptions", "(JLcom/discord/utilities/lazy/subscriptions/GuildSubscriptions;)Z", "nonce", "applications", "offset", "commandLimit", "commandIds", "requestApplicationCommands", "(JLjava/lang/String;ZLjava/lang/String;Ljava/lang/Integer;ILjava/util/List;)Z", "simulateReconnectForTesting", "streamWatch", "streamCreate", "(Ljava/lang/String;Ljava/lang/String;)V", "streamDelete", "handlePreLogout", "Lcom/discord/rtcconnection/RtcConnection$StateChange;", "stateChange", "handleRtcConnectionStateChanged", "(Lcom/discord/rtcconnection/RtcConnection$StateChange;)V", "Lrx/Observable;", "getConnected", "()Lrx/Observable;", "getConnectionReady", "resetOnError", "()Lkotlin/Unit;", "connectionReady", "handleConnectionReady", "(Z)V", "connected", "handleConnected", "type", "handleDispatch", "(Ljava/lang/String;Ljava/lang/Object;)V", "authenticationFailed", "handleDisconnect", "Lcom/discord/api/message/reaction/MessageReactionUpdate;", "kotlin.jvm.PlatformType", "messageReactionRemoveAll", "Lrx/subjects/SerializedSubject;", "getMessageReactionRemoveAll", "()Lrx/subjects/SerializedSubject;", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventUserUpdate;", "guildScheduledEventUserAdd", "getGuildScheduledEventUserAdd", "Lcom/discord/models/domain/ModelUserRelationship;", "relationshipRemove", "getRelationshipRemove", "Lcom/discord/models/domain/ModelUserNote$Update;", "userNoteUpdate", "getUserNoteUpdate", "Lcom/discord/models/domain/ModelBan;", "guildBanAdd", "getGuildBanAdd", "messageReactionAdd", "getMessageReactionAdd", "Lcom/discord/models/domain/ModelSession;", "sessionsReplace", "getSessionsReplace", "Lcom/discord/api/guildjoinrequest/GuildJoinRequestDelete;", "guildJoinRequestDelete", "getGuildJoinRequestDelete", "Lcom/discord/models/domain/ModelGuildIntegration$Update;", "guildIntegrationsUpdate", "getGuildIntegrationsUpdate", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "guildScheduledEventUpdate", "getGuildScheduledEventUpdate", "Lcom/discord/models/domain/ModelCall;", "callCreateOrUpdate", "getCallCreateOrUpdate", "Lcom/discord/api/stageinstance/StageInstance;", "stageInstanceDelete", "getStageInstanceDelete", "Lcom/discord/api/requiredaction/UserRequiredActionUpdate;", "userRequiredActionUpdate", "getUserRequiredActionUpdate", "Lcom/discord/api/thread/ThreadMembersUpdate;", "threadMembersUpdate", "getThreadMembersUpdate", "Lcom/discord/models/domain/ModelGuildMemberListUpdate;", "guildMemberListUpdate", "getGuildMemberListUpdate", "Lcom/discord/api/guild/Guild;", "guildUpdate", "getGuildUpdate", "Lcom/discord/api/voice/server/VoiceServer;", "voiceServerUpdate", "getVoiceServerUpdate", "Lcom/discord/api/guildjoinrequest/GuildJoinRequestCreateOrUpdate;", "guildJoinRequestCreateOrUpdate", "getGuildJoinRequestCreateOrUpdate", "Lcom/discord/api/channel/ChannelRecipient;", "channelRecipientAdd", "getChannelRecipientAdd", "Lcom/discord/api/commands/GuildApplicationCommands;", "guildApplicationCommands", "getGuildApplicationCommands", "Lcom/discord/models/domain/ModelMessageDelete;", "messageDelete", "getMessageDelete", "messageReactionRemove", "getMessageReactionRemove", "Lcom/discord/api/emoji/GuildEmojisUpdate;", "guildEmojisUpdate", "getGuildEmojisUpdate", "Lcom/discord/api/interaction/InteractionStateUpdate;", "interactionCreate", "getInteractionCreate", "Lkotlinx/coroutines/ExecutorCoroutineDispatcher;", "coroutineDispatcher", "Lkotlinx/coroutines/ExecutorCoroutineDispatcher;", "getCoroutineDispatcher", "()Lkotlinx/coroutines/ExecutorCoroutineDispatcher;", "stageInstanceCreate", "getStageInstanceCreate", "Lcom/discord/api/guildmember/GuildMembersChunk;", "guildMembersChunk", "getGuildMembersChunk", "Lcom/discord/api/guildmember/GuildMemberRemove;", "guildMemberRemove", "getGuildMemberRemove", "Lcom/discord/api/user/TypingUser;", "typingStart", "getTypingStart", "Lcom/discord/api/channel/Channel;", "threadDelete", "getThreadDelete", "channelRecipientRemove", "getChannelRecipientRemove", "stageInstanceUpdate", "getStageInstanceUpdate", "Lcom/discord/stores/StoreGatewayConnection$ClientState;", "callDelete", "getCallDelete", "Lcom/discord/api/friendsuggestions/FriendSuggestionDelete;", "friendSuggestionDelete", "getFriendSuggestionDelete", "Lcom/discord/models/domain/ModelPayload;", "ready", "getReady", "Lcom/discord/api/commands/ApplicationCommandAutocompleteResult;", "applicationCommandAutocompleteResult", "getApplicationCommandAutocompleteResult", "Lcom/discord/stores/utilities/BatchManager;", "batches", "Lcom/discord/stores/utilities/BatchManager;", "Lcom/discord/api/role/GuildRoleCreate;", "guildRoleCreate", "getGuildRoleCreate", "Lcom/discord/api/presence/Presence;", "presenceReplace", "getPresenceReplace", "Lcom/discord/models/domain/StreamCreateOrUpdate;", "streamUpdate", "getStreamUpdate", "guildScheduledEventDelete", "getGuildScheduledEventDelete", "Lcom/discord/models/domain/ModelUserSettings;", "userSettingsUpdate", "getUserSettingsUpdate", "Lcom/discord/api/role/GuildRoleUpdate;", "guildRoleUpdate", "getGuildRoleUpdate", "Lcom/discord/models/domain/ModelChannelUnreadUpdate;", "channelUnreadUpdate", "getChannelUnreadUpdate", "relationshipAdd", "getRelationshipAdd", "guildDeleted", "getGuildDeleted", "Lcom/discord/models/domain/StreamServerUpdate;", "streamServerUpdate", "getStreamServerUpdate", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "guildBanRemove", "getGuildBanRemove", "guildScheduledEventCreate", "getGuildScheduledEventCreate", "Lcom/discord/api/role/GuildRoleDelete;", "guildRoleDelete", "getGuildRoleDelete", "Lcom/discord/api/message/Message;", "messageCreate", "getMessageCreate", "Lcom/discord/api/thread/ThreadMemberListUpdate;", "threadMemberListUpdate", "getThreadMemberListUpdate", "guildCreate", "getGuildCreate", "Lcom/discord/api/sticker/GuildStickersUpdate;", "guildStickersUpdate", "getGuildStickersUpdate", "getStreamCreate", "Lcom/discord/stores/utilities/Batched;", "Lcom/discord/api/voice/state/VoiceState;", "Lcom/discord/stores/utilities/Batched;", "getVoiceStateUpdate", "()Lcom/discord/stores/utilities/Batched;", "channelCreateOrUpdate", "getChannelCreateOrUpdate", "guildScheduledEventUserRemove", "getGuildScheduledEventUserRemove", "Lcom/discord/api/guildmember/GuildMember;", "guildMembersAdd", "getGuildMembersAdd", "getPresenceUpdate", "socket", "Lcom/discord/gateway/GatewaySocket;", "Lcom/discord/stores/StoreStream;", "stream", "Lcom/discord/stores/StoreStream;", "Lcom/discord/api/embeddedactivities/EmbeddedActivityInboundUpdate;", "embeddedActivityInboundUpdate", "getEmbeddedActivityInboundUpdate", "Lcom/discord/models/thread/dto/ModelThreadListSync;", "threadListSync", "getThreadListSync", "Lcom/discord/api/thread/ThreadMemberUpdate;", "threadMemberUpdate", "getThreadMemberUpdate", "Lcom/discord/utilities/logging/AppGatewaySocketLogger;", "gatewaySocketLogger", "Lcom/discord/utilities/logging/AppGatewaySocketLogger;", "Lcom/discord/models/domain/StreamDelete;", "getStreamDelete", "userPaymentSourcesUpdate", "getUserPaymentSourcesUpdate", "Lcom/discord/models/domain/ModelNotificationSettings;", "userGuildSettingsUpdate", "getUserGuildSettingsUpdate", "userSubscriptionsUpdate", "getUserSubscriptionsUpdate", "Lcom/discord/api/user/User;", "userUpdate", "getUserUpdate", "userConnectionUpdate", "getUserConnectionUpdate", "interactionSuccess", "getInteractionSuccess", "Lcom/discord/models/domain/ModelReadState;", "messageAck", "getMessageAck", "Ljava/util/concurrent/ExecutorService;", "executor", "Ljava/util/concurrent/ExecutorService;", "threadCreateOrUpdate", "getThreadCreateOrUpdate", "interactionFailure", "getInteractionFailure", "Lcom/discord/api/friendsuggestions/FriendSuggestion;", "friendSuggestionCreate", "getFriendSuggestionCreate", "Lrx/Scheduler;", "scheduler", "Lrx/Scheduler;", "messageUpdate", "getMessageUpdate", "channelDeleted", "getChannelDeleted", "messageReactionRemoveEmoji", "getMessageReactionRemoveEmoji", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;Lcom/discord/utilities/time/Clock;Ljava/util/concurrent/ExecutorService;Lrx/Scheduler;Lcom/discord/utilities/logging/AppGatewaySocketLogger;)V", "ClientState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGatewayConnection implements GatewayEventHandler {
    private final SerializedSubject<ApplicationCommandAutocompleteResult, ApplicationCommandAutocompleteResult> applicationCommandAutocompleteResult;
    private final BatchManager batches;
    private final SerializedSubject<ModelCall, ModelCall> callCreateOrUpdate;
    private final SerializedSubject<ModelCall, ModelCall> callDelete;
    private final SerializedSubject<Channel, Channel> channelCreateOrUpdate;
    private final SerializedSubject<Channel, Channel> channelDeleted;
    private final SerializedSubject<ChannelRecipient, ChannelRecipient> channelRecipientAdd;
    private final SerializedSubject<ChannelRecipient, ChannelRecipient> channelRecipientRemove;
    private final SerializedSubject<ModelChannelUnreadUpdate, ModelChannelUnreadUpdate> channelUnreadUpdate;
    private ClientState clientState;
    private final Clock clock;
    private final SerializedSubject<Boolean, Boolean> connected;
    private final SerializedSubject<Boolean, Boolean> connectionReady;
    private final ExecutorCoroutineDispatcher coroutineDispatcher;
    private final SerializedSubject<EmbeddedActivityInboundUpdate, EmbeddedActivityInboundUpdate> embeddedActivityInboundUpdate;
    private final ExecutorService executor;
    private final SerializedSubject<FriendSuggestion, FriendSuggestion> friendSuggestionCreate;
    private final SerializedSubject<FriendSuggestionDelete, FriendSuggestionDelete> friendSuggestionDelete;
    private final AppGatewaySocketLogger gatewaySocketLogger;
    private final SerializedSubject<GuildApplicationCommands, GuildApplicationCommands> guildApplicationCommands;
    private final SerializedSubject<ModelBan, ModelBan> guildBanAdd;
    private final SerializedSubject<ModelBan, ModelBan> guildBanRemove;
    private final SerializedSubject<Guild, Guild> guildCreate;
    private final SerializedSubject<Guild, Guild> guildDeleted;
    private final SerializedSubject<GuildEmojisUpdate, GuildEmojisUpdate> guildEmojisUpdate;
    private final SerializedSubject<ModelGuildIntegration.Update, ModelGuildIntegration.Update> guildIntegrationsUpdate;
    private final SerializedSubject<GuildJoinRequestCreateOrUpdate, GuildJoinRequestCreateOrUpdate> guildJoinRequestCreateOrUpdate;
    private final SerializedSubject<GuildJoinRequestDelete, GuildJoinRequestDelete> guildJoinRequestDelete;
    private final SerializedSubject<ModelGuildMemberListUpdate, ModelGuildMemberListUpdate> guildMemberListUpdate;
    private final SerializedSubject<GuildMemberRemove, GuildMemberRemove> guildMemberRemove;
    private final SerializedSubject<GuildMember, GuildMember> guildMembersAdd;
    private final SerializedSubject<GuildMembersChunk, GuildMembersChunk> guildMembersChunk;
    private final SerializedSubject<GuildRoleCreate, GuildRoleCreate> guildRoleCreate;
    private final SerializedSubject<GuildRoleDelete, GuildRoleDelete> guildRoleDelete;
    private final SerializedSubject<GuildRoleUpdate, GuildRoleUpdate> guildRoleUpdate;
    private final SerializedSubject<GuildScheduledEvent, GuildScheduledEvent> guildScheduledEventCreate;
    private final SerializedSubject<GuildScheduledEvent, GuildScheduledEvent> guildScheduledEventDelete;
    private final SerializedSubject<GuildScheduledEvent, GuildScheduledEvent> guildScheduledEventUpdate;
    private final SerializedSubject<GuildScheduledEventUserUpdate, GuildScheduledEventUserUpdate> guildScheduledEventUserAdd;
    private final SerializedSubject<GuildScheduledEventUserUpdate, GuildScheduledEventUserUpdate> guildScheduledEventUserRemove;
    private final SerializedSubject<GuildStickersUpdate, GuildStickersUpdate> guildStickersUpdate;
    private final SerializedSubject<Guild, Guild> guildUpdate;
    private final SerializedSubject<InteractionStateUpdate, InteractionStateUpdate> interactionCreate;
    private final SerializedSubject<InteractionStateUpdate, InteractionStateUpdate> interactionFailure;
    private final SerializedSubject<InteractionStateUpdate, InteractionStateUpdate> interactionSuccess;
    private final SerializedSubject<ModelReadState, ModelReadState> messageAck;
    private final SerializedSubject<Message, Message> messageCreate;
    private final SerializedSubject<ModelMessageDelete, ModelMessageDelete> messageDelete;
    private final SerializedSubject<MessageReactionUpdate, MessageReactionUpdate> messageReactionAdd;
    private final SerializedSubject<MessageReactionUpdate, MessageReactionUpdate> messageReactionRemove;
    private final SerializedSubject<MessageReactionUpdate, MessageReactionUpdate> messageReactionRemoveAll;
    private final SerializedSubject<MessageReactionUpdate, MessageReactionUpdate> messageReactionRemoveEmoji;
    private final SerializedSubject<Message, Message> messageUpdate;
    private final SerializedSubject<List<Presence>, List<Presence>> presenceReplace;
    private final SerializedSubject<Presence, Presence> presenceUpdate;
    private final SerializedSubject<ModelPayload, ModelPayload> ready;
    private final SerializedSubject<ModelUserRelationship, ModelUserRelationship> relationshipAdd;
    private final SerializedSubject<ModelUserRelationship, ModelUserRelationship> relationshipRemove;
    private final Scheduler scheduler;
    private final SerializedSubject<List<ModelSession>, List<ModelSession>> sessionsReplace;
    private GatewaySocket socket;
    private final SerializedSubject<StageInstance, StageInstance> stageInstanceCreate;
    private final SerializedSubject<StageInstance, StageInstance> stageInstanceDelete;
    private final SerializedSubject<StageInstance, StageInstance> stageInstanceUpdate;
    private final StoreStream stream;
    private final SerializedSubject<StreamCreateOrUpdate, StreamCreateOrUpdate> streamCreate;
    private final SerializedSubject<StreamDelete, StreamDelete> streamDelete;
    private final SerializedSubject<StreamServerUpdate, StreamServerUpdate> streamServerUpdate;
    private final SerializedSubject<StreamCreateOrUpdate, StreamCreateOrUpdate> streamUpdate;
    private final SerializedSubject<Channel, Channel> threadCreateOrUpdate;
    private final SerializedSubject<Channel, Channel> threadDelete;
    private final SerializedSubject<ModelThreadListSync, ModelThreadListSync> threadListSync;
    private final SerializedSubject<ThreadMemberListUpdate, ThreadMemberListUpdate> threadMemberListUpdate;
    private final SerializedSubject<ThreadMemberUpdate, ThreadMemberUpdate> threadMemberUpdate;
    private final SerializedSubject<ThreadMembersUpdate, ThreadMembersUpdate> threadMembersUpdate;
    private final SerializedSubject<TypingUser, TypingUser> typingStart;
    private final SerializedSubject<Unit, Unit> userConnectionUpdate;
    private final SerializedSubject<ModelNotificationSettings, ModelNotificationSettings> userGuildSettingsUpdate;
    private final SerializedSubject<ModelUserNote.Update, ModelUserNote.Update> userNoteUpdate;
    private final SerializedSubject<Unit, Unit> userPaymentSourcesUpdate;
    private final SerializedSubject<UserRequiredActionUpdate, UserRequiredActionUpdate> userRequiredActionUpdate;
    private final SerializedSubject<ModelUserSettings, ModelUserSettings> userSettingsUpdate;
    private final SerializedSubject<Unit, Unit> userSubscriptionsUpdate;
    private final SerializedSubject<User, User> userUpdate;
    private final SerializedSubject<VoiceServer, VoiceServer> voiceServerUpdate;
    private final Batched<VoiceState> voiceStateUpdate;

    /* compiled from: StoreGatewayConnection.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u000f\b\u0082\b\u0018\u0000 \"2\u00020\u0001:\u0001\"B!\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u000f\u001a\u00020\b\u0012\u0006\u0010\u0010\u001a\u00020\u000b¢\u0006\u0004\b \u0010!J\u000f\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ0\u0010\u0011\u001a\u00020\u00002\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u000f\u001a\u00020\b2\b\b\u0002\u0010\u0010\u001a\u00020\u000bHÆ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0007J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u001a\u0010\u0018\u001a\u00020\b2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u0019\u0010\u0010\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001a\u001a\u0004\b\u001b\u0010\rR\u0019\u0010\u000f\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001c\u001a\u0004\b\u001d\u0010\nR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001e\u001a\u0004\b\u001f\u0010\u0007¨\u0006#"}, d2 = {"Lcom/discord/stores/StoreGatewayConnection$ClientState;", "", "Lcom/discord/gateway/GatewaySocket$IdentifyData;", "getIdentifyData", "()Lcom/discord/gateway/GatewaySocket$IdentifyData;", "", "component1", "()Ljava/lang/String;", "", "component2", "()Z", "Lcom/discord/stores/StoreClientDataState$ClientDataState;", "component3", "()Lcom/discord/stores/StoreClientDataState$ClientDataState;", "tokenIfAvailable", "authed", "clientDataState", "copy", "(Ljava/lang/String;ZLcom/discord/stores/StoreClientDataState$ClientDataState;)Lcom/discord/stores/StoreGatewayConnection$ClientState;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StoreClientDataState$ClientDataState;", "getClientDataState", "Z", "getAuthed", "Ljava/lang/String;", "getTokenIfAvailable", HookHelper.constructorName, "(Ljava/lang/String;ZLcom/discord/stores/StoreClientDataState$ClientDataState;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ClientState {
        public static final Companion Companion = new Companion(null);
        private final boolean authed;
        private final StoreClientDataState.ClientDataState clientDataState;
        private final String tokenIfAvailable;

        /* compiled from: StoreGatewayConnection.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0018\u0010\u0019J=\u0010\r\u001a\u00020\f2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\n\u0010\b\u001a\u00060\u0006j\u0002`\u00072\u0006\u0010\t\u001a\u00020\u00022\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\r\u0010\u000eJ1\u0010\u0016\u001a\u00020\u00142\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u00112\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u00140\u0013¢\u0006\u0004\b\u0016\u0010\u0017¨\u0006\u001a"}, d2 = {"Lcom/discord/stores/StoreGatewayConnection$ClientState$Companion;", "", "", "backgrounded", "", "token", "", "Lcom/discord/primitives/ChannelId;", "selectedVoiceChannelId", "hasGatewayConnectionConsumers", "Lcom/discord/stores/StoreClientDataState$ClientDataState;", "clientDataState", "Lcom/discord/stores/StoreGatewayConnection$ClientState;", "create", "(ZLjava/lang/String;JZLcom/discord/stores/StoreClientDataState$ClientDataState;)Lcom/discord/stores/StoreGatewayConnection$ClientState;", "Lcom/discord/stores/StoreStream;", "stream", "Lrx/Scheduler;", "scheduler", "Lkotlin/Function1;", "", "callback", "initialize", "(Lcom/discord/stores/StoreStream;Lrx/Scheduler;Lkotlin/jvm/functions/Function1;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Companion {
            private Companion() {
            }

            public final ClientState create(boolean z2, String str, long j, boolean z3, StoreClientDataState.ClientDataState clientDataState) {
                boolean z4 = str != null;
                if (z2 && j <= 0 && !z3) {
                    str = null;
                }
                return new ClientState(str, z4, clientDataState);
            }

            public final void initialize(StoreStream storeStream, Scheduler scheduler, Function1<? super ClientState, Unit> function1) {
                m.checkNotNullParameter(storeStream, "stream");
                m.checkNotNullParameter(scheduler, "scheduler");
                m.checkNotNullParameter(function1, "callback");
                Observable<Boolean> a = d.d.a();
                Observable<String> authedToken$app_productionGoogleRelease = storeStream.getAuthentication$app_productionGoogleRelease().getAuthedToken$app_productionGoogleRelease();
                Observable<Long> observeSelectedVoiceChannelId = storeStream.getVoiceChannelSelected$app_productionGoogleRelease().observeSelectedVoiceChannelId();
                l lVar = l.c;
                Observable q = l.f59b.F(k.j).q();
                m.checkNotNullExpressionValue(q, "numGatewayConnectionCons…  .distinctUntilChanged()");
                Observable<StoreClientDataState.ClientDataState> observeClientState = storeStream.getClientDataState$app_productionGoogleRelease().observeClientState();
                final StoreGatewayConnection$ClientState$Companion$initialize$1 storeGatewayConnection$ClientState$Companion$initialize$1 = new StoreGatewayConnection$ClientState$Companion$initialize$1(this);
                Observable g = Observable.g(a, authedToken$app_productionGoogleRelease, observeSelectedVoiceChannelId, q, observeClientState, new Func5() { // from class: com.discord.stores.StoreGatewayConnection$sam$rx_functions_Func5$0
                    @Override // rx.functions.Func5
                    public final /* synthetic */ Object call(Object obj, Object obj2, Object obj3, Object obj4, Object obj5) {
                        return Function5.this.invoke(obj, obj2, obj3, obj4, obj5);
                    }
                });
                m.checkNotNullExpressionValue(g, "Observable\n            .…   ::create\n            )");
                Observable I = ObservableExtensionsKt.computationLatest(g).I(scheduler);
                m.checkNotNullExpressionValue(I, "Observable\n            .…    .observeOn(scheduler)");
                ObservableExtensionsKt.appSubscribe(I, (r18 & 1) != 0 ? null : null, "clientState", (r18 & 4) != 0 ? null : null, function1, (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public ClientState(String str, boolean z2, StoreClientDataState.ClientDataState clientDataState) {
            m.checkNotNullParameter(clientDataState, "clientDataState");
            this.tokenIfAvailable = str;
            this.authed = z2;
            this.clientDataState = clientDataState;
        }

        public static /* synthetic */ ClientState copy$default(ClientState clientState, String str, boolean z2, StoreClientDataState.ClientDataState clientDataState, int i, Object obj) {
            if ((i & 1) != 0) {
                str = clientState.tokenIfAvailable;
            }
            if ((i & 2) != 0) {
                z2 = clientState.authed;
            }
            if ((i & 4) != 0) {
                clientDataState = clientState.clientDataState;
            }
            return clientState.copy(str, z2, clientDataState);
        }

        public final String component1() {
            return this.tokenIfAvailable;
        }

        public final boolean component2() {
            return this.authed;
        }

        public final StoreClientDataState.ClientDataState component3() {
            return this.clientDataState;
        }

        public final ClientState copy(String str, boolean z2, StoreClientDataState.ClientDataState clientDataState) {
            m.checkNotNullParameter(clientDataState, "clientDataState");
            return new ClientState(str, z2, clientDataState);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ClientState)) {
                return false;
            }
            ClientState clientState = (ClientState) obj;
            return m.areEqual(this.tokenIfAvailable, clientState.tokenIfAvailable) && this.authed == clientState.authed && m.areEqual(this.clientDataState, clientState.clientDataState);
        }

        public final boolean getAuthed() {
            return this.authed;
        }

        public final StoreClientDataState.ClientDataState getClientDataState() {
            return this.clientDataState;
        }

        public final GatewaySocket.IdentifyData getIdentifyData() {
            String str = this.tokenIfAvailable;
            if (str != null) {
                return new GatewaySocket.IdentifyData(str, this.clientDataState.toIdentifyData());
            }
            return null;
        }

        public final String getTokenIfAvailable() {
            return this.tokenIfAvailable;
        }

        public int hashCode() {
            String str = this.tokenIfAvailable;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            boolean z2 = this.authed;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (hashCode + i2) * 31;
            StoreClientDataState.ClientDataState clientDataState = this.clientDataState;
            if (clientDataState != null) {
                i = clientDataState.hashCode();
            }
            return i4 + i;
        }

        public String toString() {
            StringBuilder R = a.R("ClientState(tokenIfAvailable=");
            R.append(this.tokenIfAvailable);
            R.append(", authed=");
            R.append(this.authed);
            R.append(", clientDataState=");
            R.append(this.clientDataState);
            R.append(")");
            return R.toString();
        }
    }

    public StoreGatewayConnection(StoreStream storeStream, Clock clock, AppGatewaySocketLogger appGatewaySocketLogger) {
        this(storeStream, clock, null, null, appGatewaySocketLogger, 12, null);
    }

    public StoreGatewayConnection(StoreStream storeStream, Clock clock, ExecutorService executorService, AppGatewaySocketLogger appGatewaySocketLogger) {
        this(storeStream, clock, executorService, null, appGatewaySocketLogger, 8, null);
    }

    public StoreGatewayConnection(StoreStream storeStream, Clock clock, ExecutorService executorService, Scheduler scheduler, AppGatewaySocketLogger appGatewaySocketLogger) {
        m.checkNotNullParameter(storeStream, "stream");
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(executorService, "executor");
        m.checkNotNullParameter(scheduler, "scheduler");
        m.checkNotNullParameter(appGatewaySocketLogger, "gatewaySocketLogger");
        this.stream = storeStream;
        this.clock = clock;
        this.executor = executorService;
        this.scheduler = scheduler;
        this.gatewaySocketLogger = appGatewaySocketLogger;
        w0 w0Var = new w0(executorService);
        this.coroutineDispatcher = w0Var;
        BatchManager batchManager = new BatchManager(w0Var, null, 2, null);
        this.batches = batchManager;
        Boolean bool = Boolean.FALSE;
        this.connected = new SerializedSubject<>(BehaviorSubject.l0(bool));
        this.connectionReady = new SerializedSubject<>(BehaviorSubject.l0(bool));
        this.callCreateOrUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.callDelete = new SerializedSubject<>(BehaviorSubject.k0());
        this.channelCreateOrUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.channelDeleted = new SerializedSubject<>(BehaviorSubject.k0());
        this.channelRecipientAdd = new SerializedSubject<>(BehaviorSubject.k0());
        this.channelRecipientRemove = new SerializedSubject<>(BehaviorSubject.k0());
        this.channelUnreadUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.embeddedActivityInboundUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.guildApplicationCommands = new SerializedSubject<>(BehaviorSubject.k0());
        this.guildBanAdd = new SerializedSubject<>(PublishSubject.k0());
        this.guildBanRemove = new SerializedSubject<>(PublishSubject.k0());
        this.guildCreate = new SerializedSubject<>(BehaviorSubject.k0());
        this.guildUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.guildEmojisUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.guildStickersUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.guildDeleted = new SerializedSubject<>(BehaviorSubject.k0());
        this.guildMembersAdd = new SerializedSubject<>(BehaviorSubject.k0());
        this.guildMembersChunk = new SerializedSubject<>(BehaviorSubject.k0());
        this.guildMemberRemove = new SerializedSubject<>(BehaviorSubject.k0());
        this.guildJoinRequestCreateOrUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.guildJoinRequestDelete = new SerializedSubject<>(BehaviorSubject.k0());
        this.guildRoleCreate = new SerializedSubject<>(BehaviorSubject.k0());
        this.guildRoleUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.guildRoleDelete = new SerializedSubject<>(BehaviorSubject.k0());
        this.guildIntegrationsUpdate = new SerializedSubject<>(PublishSubject.k0());
        this.messageAck = new SerializedSubject<>(BehaviorSubject.k0());
        this.messageCreate = new SerializedSubject<>(BehaviorSubject.k0());
        this.messageDelete = new SerializedSubject<>(BehaviorSubject.k0());
        this.messageUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.messageReactionAdd = new SerializedSubject<>(BehaviorSubject.k0());
        this.messageReactionRemove = new SerializedSubject<>(BehaviorSubject.k0());
        this.messageReactionRemoveEmoji = new SerializedSubject<>(BehaviorSubject.k0());
        this.messageReactionRemoveAll = new SerializedSubject<>(BehaviorSubject.k0());
        this.presenceUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.presenceReplace = new SerializedSubject<>(BehaviorSubject.k0());
        this.ready = new SerializedSubject<>(BehaviorSubject.k0());
        this.relationshipAdd = new SerializedSubject<>(BehaviorSubject.k0());
        this.relationshipRemove = new SerializedSubject<>(BehaviorSubject.k0());
        this.typingStart = new SerializedSubject<>(BehaviorSubject.k0());
        this.userConnectionUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.userUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.userSettingsUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.userGuildSettingsUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.userNoteUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.userRequiredActionUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.sessionsReplace = new SerializedSubject<>(BehaviorSubject.k0());
        this.voiceStateUpdate = BatchManager.createBatched$default(batchManager, "VOICE_STATE_UPDATE", 0L, 0L, 6, null);
        this.voiceServerUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.guildMemberListUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.userPaymentSourcesUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.userSubscriptionsUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.streamCreate = new SerializedSubject<>(BehaviorSubject.k0());
        this.streamUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.streamServerUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.streamDelete = new SerializedSubject<>(BehaviorSubject.k0());
        this.threadCreateOrUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.threadDelete = new SerializedSubject<>(BehaviorSubject.k0());
        this.threadListSync = new SerializedSubject<>(BehaviorSubject.k0());
        this.threadMemberUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.threadMembersUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.threadMemberListUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.interactionCreate = new SerializedSubject<>(BehaviorSubject.k0());
        this.interactionSuccess = new SerializedSubject<>(BehaviorSubject.k0());
        this.interactionFailure = new SerializedSubject<>(BehaviorSubject.k0());
        this.applicationCommandAutocompleteResult = new SerializedSubject<>(BehaviorSubject.k0());
        this.stageInstanceCreate = new SerializedSubject<>(BehaviorSubject.k0());
        this.stageInstanceUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.stageInstanceDelete = new SerializedSubject<>(BehaviorSubject.k0());
        this.friendSuggestionCreate = new SerializedSubject<>(BehaviorSubject.k0());
        this.friendSuggestionDelete = new SerializedSubject<>(BehaviorSubject.k0());
        this.guildScheduledEventCreate = new SerializedSubject<>(BehaviorSubject.k0());
        this.guildScheduledEventUpdate = new SerializedSubject<>(BehaviorSubject.k0());
        this.guildScheduledEventDelete = new SerializedSubject<>(BehaviorSubject.k0());
        this.guildScheduledEventUserAdd = new SerializedSubject<>(BehaviorSubject.k0());
        this.guildScheduledEventUserRemove = new SerializedSubject<>(BehaviorSubject.k0());
    }

    private final GatewaySocket buildGatewaySocket(Context context, NetworkMonitor networkMonitor) {
        boolean z2;
        boolean z3;
        RestAPI.Companion companion = RestAPI.Companion;
        List listOf = n.listOf((Object[]) new Interceptor[]{companion.buildAnalyticsInterceptor(), companion.buildLoggingInterceptor()});
        App.a aVar = App.Companion;
        Objects.requireNonNull(aVar);
        z2 = App.IS_LOCAL;
        SSLSocketFactory createSocketFactory$default = z2 ? null : SecureSocketsLayerUtils.createSocketFactory$default(null, 1, null);
        Objects.requireNonNull(aVar);
        z3 = App.IS_LOCAL;
        GatewaySocket gatewaySocket = new GatewaySocket(new StoreGatewayConnection$buildGatewaySocket$socket$1(this), StoreGatewayConnection$buildGatewaySocket$socket$2.INSTANCE, this, this.scheduler, AppLog.g, networkMonitor, new RestConfig(BuildConfig.HOST_API, RestAPI.AppHeadersProvider.INSTANCE, listOf), context, z3 ? StoreGatewayConnection$buildGatewaySocket$gatewayUrlTransform$1.INSTANCE : null, createSocketFactory$default, AnalyticSuperProperties.INSTANCE.getSuperProperties(), this.gatewaySocketLogger);
        this.stream.getConnectionTimeStats$app_productionGoogleRelease().addListener(gatewaySocket);
        return gatewaySocket;
    }

    public final GatewaySocket.IdentifyData getIdentifyData() {
        ClientState clientState = this.clientState;
        if (clientState != null) {
            return clientState.getIdentifyData();
        }
        return null;
    }

    public final void handleClientStateUpdate(ClientState clientState) {
        this.clientState = clientState;
        if (clientState.getTokenIfAvailable() != null) {
            GatewaySocket gatewaySocket = this.socket;
            if (gatewaySocket != null) {
                gatewaySocket.connect();
                return;
            }
            return;
        }
        GatewaySocket gatewaySocket2 = this.socket;
        if (gatewaySocket2 != null) {
            gatewaySocket2.close(!clientState.getAuthed());
        }
    }

    private final <T> void onNext(SerializedSubject<T, T> serializedSubject, Object obj) {
        serializedSubject.k.onNext(obj);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ boolean presenceUpdate$default(StoreGatewayConnection storeGatewayConnection, ClientStatus clientStatus, Long l, List list, Boolean bool, int i, Object obj) {
        if ((i & 2) != 0) {
            l = Long.valueOf(storeGatewayConnection.clock.currentTimeMillis());
        }
        if ((i & 4) != 0) {
            list = null;
        }
        if ((i & 8) != 0) {
            bool = null;
        }
        return storeGatewayConnection.presenceUpdate(clientStatus, l, list, bool);
    }

    public static /* synthetic */ boolean requestGuildMembers$default(StoreGatewayConnection storeGatewayConnection, long j, String str, List list, Integer num, int i, Object obj) {
        String str2 = (i & 2) != 0 ? null : str;
        List list2 = (i & 4) != 0 ? null : list;
        if ((i & 8) != 0) {
            num = 100;
        }
        return storeGatewayConnection.requestGuildMembers(j, str2, list2, num);
    }

    private final boolean requestIfSessionEstablished(Function1<? super GatewaySocket, Unit> function1) {
        GatewaySocket gatewaySocket = this.socket;
        if (gatewaySocket == null) {
            return false;
        }
        boolean isSessionEstablished = gatewaySocket.isSessionEstablished();
        if (isSessionEstablished) {
            function1.invoke(gatewaySocket);
        }
        return isSessionEstablished;
    }

    private final void streamPing(String str) {
        requestIfSessionEstablished(new StoreGatewayConnection$streamPing$1(str));
    }

    private final void voiceServerPing() {
        requestIfSessionEstablished(StoreGatewayConnection$voiceServerPing$1.INSTANCE);
    }

    public static /* synthetic */ boolean voiceStateUpdate$default(StoreGatewayConnection storeGatewayConnection, Long l, Long l2, boolean z2, boolean z3, boolean z4, String str, boolean z5, int i, Object obj) {
        return storeGatewayConnection.voiceStateUpdate(l, l2, z2, z3, z4, (i & 32) != 0 ? null : str, (i & 64) != 0 ? true : z5);
    }

    public final boolean callConnect(long j) {
        return requestIfSessionEstablished(new StoreGatewayConnection$callConnect$1(j));
    }

    public final SerializedSubject<ApplicationCommandAutocompleteResult, ApplicationCommandAutocompleteResult> getApplicationCommandAutocompleteResult() {
        return this.applicationCommandAutocompleteResult;
    }

    public final SerializedSubject<ModelCall, ModelCall> getCallCreateOrUpdate() {
        return this.callCreateOrUpdate;
    }

    public final SerializedSubject<ModelCall, ModelCall> getCallDelete() {
        return this.callDelete;
    }

    public final SerializedSubject<Channel, Channel> getChannelCreateOrUpdate() {
        return this.channelCreateOrUpdate;
    }

    public final SerializedSubject<Channel, Channel> getChannelDeleted() {
        return this.channelDeleted;
    }

    public final SerializedSubject<ChannelRecipient, ChannelRecipient> getChannelRecipientAdd() {
        return this.channelRecipientAdd;
    }

    public final SerializedSubject<ChannelRecipient, ChannelRecipient> getChannelRecipientRemove() {
        return this.channelRecipientRemove;
    }

    public final SerializedSubject<ModelChannelUnreadUpdate, ModelChannelUnreadUpdate> getChannelUnreadUpdate() {
        return this.channelUnreadUpdate;
    }

    public final Observable<Boolean> getConnected() {
        Observable<Boolean> q = this.connected.q();
        m.checkNotNullExpressionValue(q, "connected.distinctUntilChanged()");
        return q;
    }

    public final Observable<Boolean> getConnectionReady() {
        Observable<Boolean> q = this.connectionReady.q();
        m.checkNotNullExpressionValue(q, "connectionReady.distinctUntilChanged()");
        return q;
    }

    public final ExecutorCoroutineDispatcher getCoroutineDispatcher() {
        return this.coroutineDispatcher;
    }

    public final SerializedSubject<EmbeddedActivityInboundUpdate, EmbeddedActivityInboundUpdate> getEmbeddedActivityInboundUpdate() {
        return this.embeddedActivityInboundUpdate;
    }

    public final SerializedSubject<FriendSuggestion, FriendSuggestion> getFriendSuggestionCreate() {
        return this.friendSuggestionCreate;
    }

    public final SerializedSubject<FriendSuggestionDelete, FriendSuggestionDelete> getFriendSuggestionDelete() {
        return this.friendSuggestionDelete;
    }

    public final SerializedSubject<GuildApplicationCommands, GuildApplicationCommands> getGuildApplicationCommands() {
        return this.guildApplicationCommands;
    }

    public final SerializedSubject<ModelBan, ModelBan> getGuildBanAdd() {
        return this.guildBanAdd;
    }

    public final SerializedSubject<ModelBan, ModelBan> getGuildBanRemove() {
        return this.guildBanRemove;
    }

    public final SerializedSubject<Guild, Guild> getGuildCreate() {
        return this.guildCreate;
    }

    public final SerializedSubject<Guild, Guild> getGuildDeleted() {
        return this.guildDeleted;
    }

    public final SerializedSubject<GuildEmojisUpdate, GuildEmojisUpdate> getGuildEmojisUpdate() {
        return this.guildEmojisUpdate;
    }

    public final SerializedSubject<ModelGuildIntegration.Update, ModelGuildIntegration.Update> getGuildIntegrationsUpdate() {
        return this.guildIntegrationsUpdate;
    }

    public final SerializedSubject<GuildJoinRequestCreateOrUpdate, GuildJoinRequestCreateOrUpdate> getGuildJoinRequestCreateOrUpdate() {
        return this.guildJoinRequestCreateOrUpdate;
    }

    public final SerializedSubject<GuildJoinRequestDelete, GuildJoinRequestDelete> getGuildJoinRequestDelete() {
        return this.guildJoinRequestDelete;
    }

    public final SerializedSubject<ModelGuildMemberListUpdate, ModelGuildMemberListUpdate> getGuildMemberListUpdate() {
        return this.guildMemberListUpdate;
    }

    public final SerializedSubject<GuildMemberRemove, GuildMemberRemove> getGuildMemberRemove() {
        return this.guildMemberRemove;
    }

    public final SerializedSubject<GuildMember, GuildMember> getGuildMembersAdd() {
        return this.guildMembersAdd;
    }

    public final SerializedSubject<GuildMembersChunk, GuildMembersChunk> getGuildMembersChunk() {
        return this.guildMembersChunk;
    }

    public final SerializedSubject<GuildRoleCreate, GuildRoleCreate> getGuildRoleCreate() {
        return this.guildRoleCreate;
    }

    public final SerializedSubject<GuildRoleDelete, GuildRoleDelete> getGuildRoleDelete() {
        return this.guildRoleDelete;
    }

    public final SerializedSubject<GuildRoleUpdate, GuildRoleUpdate> getGuildRoleUpdate() {
        return this.guildRoleUpdate;
    }

    public final SerializedSubject<GuildScheduledEvent, GuildScheduledEvent> getGuildScheduledEventCreate() {
        return this.guildScheduledEventCreate;
    }

    public final SerializedSubject<GuildScheduledEvent, GuildScheduledEvent> getGuildScheduledEventDelete() {
        return this.guildScheduledEventDelete;
    }

    public final SerializedSubject<GuildScheduledEvent, GuildScheduledEvent> getGuildScheduledEventUpdate() {
        return this.guildScheduledEventUpdate;
    }

    public final SerializedSubject<GuildScheduledEventUserUpdate, GuildScheduledEventUserUpdate> getGuildScheduledEventUserAdd() {
        return this.guildScheduledEventUserAdd;
    }

    public final SerializedSubject<GuildScheduledEventUserUpdate, GuildScheduledEventUserUpdate> getGuildScheduledEventUserRemove() {
        return this.guildScheduledEventUserRemove;
    }

    public final SerializedSubject<GuildStickersUpdate, GuildStickersUpdate> getGuildStickersUpdate() {
        return this.guildStickersUpdate;
    }

    public final SerializedSubject<Guild, Guild> getGuildUpdate() {
        return this.guildUpdate;
    }

    public final SerializedSubject<InteractionStateUpdate, InteractionStateUpdate> getInteractionCreate() {
        return this.interactionCreate;
    }

    public final SerializedSubject<InteractionStateUpdate, InteractionStateUpdate> getInteractionFailure() {
        return this.interactionFailure;
    }

    public final SerializedSubject<InteractionStateUpdate, InteractionStateUpdate> getInteractionSuccess() {
        return this.interactionSuccess;
    }

    public final SerializedSubject<ModelReadState, ModelReadState> getMessageAck() {
        return this.messageAck;
    }

    public final SerializedSubject<Message, Message> getMessageCreate() {
        return this.messageCreate;
    }

    public final SerializedSubject<ModelMessageDelete, ModelMessageDelete> getMessageDelete() {
        return this.messageDelete;
    }

    public final SerializedSubject<MessageReactionUpdate, MessageReactionUpdate> getMessageReactionAdd() {
        return this.messageReactionAdd;
    }

    public final SerializedSubject<MessageReactionUpdate, MessageReactionUpdate> getMessageReactionRemove() {
        return this.messageReactionRemove;
    }

    public final SerializedSubject<MessageReactionUpdate, MessageReactionUpdate> getMessageReactionRemoveAll() {
        return this.messageReactionRemoveAll;
    }

    public final SerializedSubject<MessageReactionUpdate, MessageReactionUpdate> getMessageReactionRemoveEmoji() {
        return this.messageReactionRemoveEmoji;
    }

    public final SerializedSubject<Message, Message> getMessageUpdate() {
        return this.messageUpdate;
    }

    public final SerializedSubject<List<Presence>, List<Presence>> getPresenceReplace() {
        return this.presenceReplace;
    }

    public final SerializedSubject<Presence, Presence> getPresenceUpdate() {
        return this.presenceUpdate;
    }

    public final SerializedSubject<ModelPayload, ModelPayload> getReady() {
        return this.ready;
    }

    public final SerializedSubject<ModelUserRelationship, ModelUserRelationship> getRelationshipAdd() {
        return this.relationshipAdd;
    }

    public final SerializedSubject<ModelUserRelationship, ModelUserRelationship> getRelationshipRemove() {
        return this.relationshipRemove;
    }

    public final SerializedSubject<List<ModelSession>, List<ModelSession>> getSessionsReplace() {
        return this.sessionsReplace;
    }

    public final SerializedSubject<StageInstance, StageInstance> getStageInstanceCreate() {
        return this.stageInstanceCreate;
    }

    public final SerializedSubject<StageInstance, StageInstance> getStageInstanceDelete() {
        return this.stageInstanceDelete;
    }

    public final SerializedSubject<StageInstance, StageInstance> getStageInstanceUpdate() {
        return this.stageInstanceUpdate;
    }

    public final SerializedSubject<StreamCreateOrUpdate, StreamCreateOrUpdate> getStreamCreate() {
        return this.streamCreate;
    }

    public final SerializedSubject<StreamDelete, StreamDelete> getStreamDelete() {
        return this.streamDelete;
    }

    public final SerializedSubject<StreamServerUpdate, StreamServerUpdate> getStreamServerUpdate() {
        return this.streamServerUpdate;
    }

    public final SerializedSubject<StreamCreateOrUpdate, StreamCreateOrUpdate> getStreamUpdate() {
        return this.streamUpdate;
    }

    public final SerializedSubject<Channel, Channel> getThreadCreateOrUpdate() {
        return this.threadCreateOrUpdate;
    }

    public final SerializedSubject<Channel, Channel> getThreadDelete() {
        return this.threadDelete;
    }

    public final SerializedSubject<ModelThreadListSync, ModelThreadListSync> getThreadListSync() {
        return this.threadListSync;
    }

    public final SerializedSubject<ThreadMemberListUpdate, ThreadMemberListUpdate> getThreadMemberListUpdate() {
        return this.threadMemberListUpdate;
    }

    public final SerializedSubject<ThreadMemberUpdate, ThreadMemberUpdate> getThreadMemberUpdate() {
        return this.threadMemberUpdate;
    }

    public final SerializedSubject<ThreadMembersUpdate, ThreadMembersUpdate> getThreadMembersUpdate() {
        return this.threadMembersUpdate;
    }

    public final SerializedSubject<TypingUser, TypingUser> getTypingStart() {
        return this.typingStart;
    }

    public final SerializedSubject<Unit, Unit> getUserConnectionUpdate() {
        return this.userConnectionUpdate;
    }

    public final SerializedSubject<ModelNotificationSettings, ModelNotificationSettings> getUserGuildSettingsUpdate() {
        return this.userGuildSettingsUpdate;
    }

    public final SerializedSubject<ModelUserNote.Update, ModelUserNote.Update> getUserNoteUpdate() {
        return this.userNoteUpdate;
    }

    public final SerializedSubject<Unit, Unit> getUserPaymentSourcesUpdate() {
        return this.userPaymentSourcesUpdate;
    }

    public final SerializedSubject<UserRequiredActionUpdate, UserRequiredActionUpdate> getUserRequiredActionUpdate() {
        return this.userRequiredActionUpdate;
    }

    public final SerializedSubject<ModelUserSettings, ModelUserSettings> getUserSettingsUpdate() {
        return this.userSettingsUpdate;
    }

    public final SerializedSubject<Unit, Unit> getUserSubscriptionsUpdate() {
        return this.userSubscriptionsUpdate;
    }

    public final SerializedSubject<User, User> getUserUpdate() {
        return this.userUpdate;
    }

    public final SerializedSubject<VoiceServer, VoiceServer> getVoiceServerUpdate() {
        return this.voiceServerUpdate;
    }

    public final Batched<VoiceState> getVoiceStateUpdate() {
        return this.voiceStateUpdate;
    }

    @Override // com.discord.gateway.GatewayEventHandler
    public void handleConnected(boolean z2) {
        SerializedSubject<Boolean, Boolean> serializedSubject = this.connected;
        serializedSubject.k.onNext(Boolean.valueOf(z2));
    }

    @Override // com.discord.gateway.GatewayEventHandler
    public void handleConnectionReady(boolean z2) {
        SerializedSubject<Boolean, Boolean> serializedSubject = this.connectionReady;
        serializedSubject.k.onNext(Boolean.valueOf(z2));
    }

    @Override // com.discord.gateway.GatewayEventHandler
    public void handleDisconnect(boolean z2) {
        if (z2) {
            this.stream.getAuthentication$app_productionGoogleRelease().logout();
        }
    }

    @Override // com.discord.gateway.GatewayEventHandler
    public void handleDispatch(String str, Object obj) {
        m.checkNotNullParameter(obj, "data");
        this.batches.flushBatches(str);
        if (str != null) {
            switch (str.hashCode()) {
                case -2137365335:
                    if (str.equals("MESSAGE_REACTION_REMOVE_EMOJI")) {
                        this.messageReactionRemoveEmoji.k.onNext(obj);
                        return;
                    }
                    return;
                case -2067017195:
                    if (!str.equals("GUILD_JOIN_REQUEST_CREATE")) {
                        return;
                    }
                    this.guildJoinRequestCreateOrUpdate.k.onNext(obj);
                    return;
                case -2050181436:
                    if (str.equals("GUILD_JOIN_REQUEST_DELETE")) {
                        this.guildJoinRequestDelete.k.onNext(obj);
                        return;
                    }
                    return;
                case -2026522382:
                    if (str.equals("SESSIONS_REPLACE")) {
                        this.sessionsReplace.k.onNext(obj);
                        return;
                    }
                    return;
                case -2002723137:
                    if (str.equals("GUILD_SCHEDULED_EVENT_CREATE")) {
                        this.guildScheduledEventCreate.k.onNext(obj);
                        return;
                    }
                    return;
                case -1985887378:
                    if (str.equals("GUILD_SCHEDULED_EVENT_DELETE")) {
                        this.guildScheduledEventDelete.k.onNext(obj);
                        return;
                    }
                    return;
                case -1967037287:
                    if (str.equals("THREAD_MEMBER_UPDATE")) {
                        this.threadMemberUpdate.k.onNext(obj);
                        return;
                    }
                    return;
                case -1921449515:
                    if (str.equals("APPLICATION_COMMAND_AUTOCOMPLETE_RESPONSE")) {
                        this.applicationCommandAutocompleteResult.k.onNext(obj);
                        return;
                    }
                    return;
                case -1862771270:
                    if (str.equals("THREAD_MEMBER_LIST_UPDATE")) {
                        this.threadMemberListUpdate.k.onNext(obj);
                        return;
                    }
                    return;
                case -1825641445:
                    if (str.equals("GUILD_MEMBERS_CHUNK")) {
                        this.guildMembersChunk.k.onNext(obj);
                        return;
                    }
                    return;
                case -1580624828:
                    if (str.equals("EMBEDDED_ACTIVITY_UPDATE")) {
                        this.embeddedActivityInboundUpdate.k.onNext(obj);
                        return;
                    }
                    return;
                case -1553569310:
                    if (!str.equals("GUILD_JOIN_REQUEST_UPDATE")) {
                        return;
                    }
                    this.guildJoinRequestCreateOrUpdate.k.onNext(obj);
                    return;
                case -1553064252:
                    if (str.equals("MESSAGE_REACTION_REMOVE_ALL")) {
                        this.messageReactionRemoveAll.k.onNext(obj);
                        return;
                    }
                    return;
                case -1489275252:
                    if (str.equals(NotificationData.TYPE_GUILD_SCHEDULED_EVENT_UPDATE)) {
                        this.guildScheduledEventUpdate.k.onNext(obj);
                        return;
                    }
                    return;
                case -1484942443:
                    if (str.equals("GUILD_SCHEDULED_EVENT_USER_REMOVE")) {
                        this.guildScheduledEventUserRemove.k.onNext(obj);
                        return;
                    }
                    return;
                case -1467383482:
                    if (str.equals("USER_REQUIRED_ACTION_UPDATE")) {
                        this.userRequiredActionUpdate.k.onNext(obj);
                        return;
                    }
                    return;
                case -1446088218:
                    if (str.equals("GUILD_STICKERS_UPDATE")) {
                        this.guildStickersUpdate.k.onNext(obj);
                        return;
                    }
                    return;
                case -1327124998:
                    if (str.equals(NotificationData.TYPE_RELATIONSHIP_ADD)) {
                        this.relationshipAdd.k.onNext(obj);
                        return;
                    }
                    return;
                case -1263316859:
                    if (str.equals(NotificationData.TYPE_STAGE_INSTANCE_CREATE)) {
                        this.stageInstanceCreate.k.onNext(obj);
                        return;
                    }
                    return;
                case -1261304891:
                    if (str.equals("GUILD_BAN_ADD")) {
                        this.guildBanAdd.k.onNext(obj);
                        return;
                    }
                    return;
                case -1248965304:
                    if (!str.equals("GUILD_MEMBER_ADD")) {
                        return;
                    }
                    this.guildMembersAdd.k.onNext(obj);
                    return;
                case -1246481100:
                    if (str.equals("STAGE_INSTANCE_DELETE")) {
                        this.stageInstanceDelete.k.onNext(obj);
                        return;
                    }
                    return;
                case -1238538557:
                    if (str.equals("MESSAGE_REACTION_ADD")) {
                        this.messageReactionAdd.k.onNext(obj);
                        return;
                    }
                    return;
                case -1201943215:
                    if (str.equals("GUILD_MEMBER_LIST_UPDATE")) {
                        this.guildMemberListUpdate.k.onNext(obj);
                        return;
                    }
                    return;
                case -974414266:
                    if (str.equals("USER_SUBSCRIPTIONS_UPDATE")) {
                        this.userSubscriptionsUpdate.k.onNext(obj);
                        return;
                    }
                    return;
                case -960563390:
                    if (str.equals("USER_NOTE_UPDATE")) {
                        this.userNoteUpdate.k.onNext(obj);
                        return;
                    }
                    return;
                case -903406451:
                    if (str.equals("PRESENCE_UPDATE")) {
                        this.presenceUpdate.k.onNext(obj);
                        return;
                    }
                    return;
                case -850953239:
                    if (str.equals("INTERACTION_CREATE")) {
                        this.interactionCreate.k.onNext(obj);
                        return;
                    }
                    return;
                case -843352707:
                    if (str.equals("GUILD_INTEGRATIONS_UPDATE")) {
                        this.guildIntegrationsUpdate.k.onNext(obj);
                        return;
                    }
                    return;
                case -778017807:
                    if (str.equals("USER_SETTINGS_UPDATE")) {
                        this.userSettingsUpdate.k.onNext(obj);
                        return;
                    }
                    return;
                case -767350043:
                    if (str.equals("GUILD_APPLICATION_COMMANDS_UPDATE")) {
                        this.guildApplicationCommands.k.onNext(obj);
                        return;
                    }
                    return;
                case -749868974:
                    if (str.equals("STAGE_INSTANCE_UPDATE")) {
                        this.stageInstanceUpdate.k.onNext(obj);
                        return;
                    }
                    return;
                case -718889877:
                    if (str.equals("RELATIONSHIP_REMOVE")) {
                        this.relationshipRemove.k.onNext(obj);
                        return;
                    }
                    return;
                case -675064872:
                    if (!str.equals("CHANNEL_CREATE")) {
                        return;
                    }
                    this.channelCreateOrUpdate.k.onNext(obj);
                    return;
                case -658229113:
                    if (str.equals("CHANNEL_DELETE")) {
                        this.channelDeleted.k.onNext(obj);
                        return;
                    }
                    return;
                case -548091546:
                    if (str.equals("CHANNEL_RECIPIENT_REMOVE")) {
                        this.channelRecipientRemove.k.onNext(obj);
                        return;
                    }
                    return;
                case -510741638:
                    if (str.equals("TYPING_START")) {
                        this.typingStart.k.onNext(obj);
                        return;
                    }
                    return;
                case -300870211:
                    if (str.equals("USER_PAYMENT_SOURCES_UPDATE")) {
                        this.userPaymentSourcesUpdate.k.onNext(obj);
                        return;
                    }
                    return;
                case -273749272:
                    if (str.equals("GUILD_CREATE")) {
                        this.guildCreate.k.onNext(obj);
                        return;
                    }
                    return;
                case -256913513:
                    if (str.equals("GUILD_DELETE")) {
                        this.guildDeleted.k.onNext(obj);
                        return;
                    }
                    return;
                case -233612803:
                    if (!str.equals("CALL_CREATE")) {
                        return;
                    }
                    this.callCreateOrUpdate.k.onNext(obj);
                    return;
                case -216777044:
                    if (str.equals("CALL_DELETE")) {
                        this.callDelete.k.onNext(obj);
                        return;
                    }
                    return;
                case -207118083:
                    if (str.equals("CHANNEL_UNREAD_UPDATE")) {
                        this.channelUnreadUpdate.k.onNext(obj);
                        return;
                    }
                    return;
                case -161616987:
                    if (!str.equals("CHANNEL_UPDATE")) {
                        return;
                    }
                    this.channelCreateOrUpdate.k.onNext(obj);
                    return;
                case -135720355:
                    if (str.equals("GUILD_MEMBER_REMOVE")) {
                        this.guildMemberRemove.k.onNext(obj);
                        return;
                    }
                    return;
                case -107601202:
                    if (!str.equals("MESSAGE_DELETE_BULK")) {
                        return;
                    }
                    this.messageDelete.k.onNext(obj);
                    return;
                case -45642698:
                    if (str.equals("FRIEND_SUGGESTION_CREATE")) {
                        this.friendSuggestionCreate.k.onNext(obj);
                        return;
                    }
                    return;
                case -39955806:
                    if (!str.equals("GUILD_MEMBER_UPDATE")) {
                        return;
                    }
                    this.guildMembersAdd.k.onNext(obj);
                    return;
                case -28806939:
                    if (str.equals("FRIEND_SUGGESTION_DELETE")) {
                        this.friendSuggestionDelete.k.onNext(obj);
                        return;
                    }
                    return;
                case 77848963:
                    if (str.equals("READY")) {
                        this.ready.k.onNext(obj);
                        return;
                    }
                    return;
                case 85547718:
                    if (str.equals("STREAM_SERVER_UPDATE")) {
                        this.streamServerUpdate.k.onNext(obj);
                        return;
                    }
                    return;
                case 151900580:
                    if (str.equals("THREAD_MEMBERS_UPDATE")) {
                        this.threadMembersUpdate.k.onNext(obj);
                        return;
                    }
                    return;
                case 239698613:
                    if (str.equals("GUILD_UPDATE")) {
                        this.guildUpdate.k.onNext(obj);
                        return;
                    }
                    return;
                case 279835082:
                    if (!str.equals("CALL_UPDATE")) {
                        return;
                    }
                    this.callCreateOrUpdate.k.onNext(obj);
                    return;
                case 391412669:
                    if (str.equals("USER_UPDATE")) {
                        this.userUpdate.k.onNext(obj);
                        return;
                    }
                    return;
                case 789496470:
                    if (str.equals("INTERACTION_SUCCESS")) {
                        this.interactionSuccess.k.onNext(obj);
                        return;
                    }
                    return;
                case 998188116:
                    if (str.equals(NotificationData.TYPE_MESSAGE_CREATE)) {
                        this.messageCreate.k.onNext(obj);
                        return;
                    }
                    return;
                case 1015023875:
                    if (!str.equals("MESSAGE_DELETE")) {
                        return;
                    }
                    this.messageDelete.k.onNext(obj);
                    return;
                case 1190664927:
                    if (str.equals("CHANNEL_RECIPIENT_ADD")) {
                        this.channelRecipientAdd.k.onNext(obj);
                        return;
                    }
                    return;
                case 1249854210:
                    if (str.equals("MESSAGE_REACTION_REMOVE")) {
                        this.messageReactionRemove.k.onNext(obj);
                        return;
                    }
                    return;
                case 1268388049:
                    if (!str.equals("THREAD_CREATE")) {
                        return;
                    }
                    this.threadCreateOrUpdate.k.onNext(obj);
                    return;
                case 1276846319:
                    if (str.equals("GUILD_EMOJIS_UPDATE")) {
                        this.guildEmojisUpdate.k.onNext(obj);
                        return;
                    }
                    return;
                case 1281125393:
                    if (str.equals("MESSAGE_ACK")) {
                        this.messageAck.k.onNext(obj);
                        return;
                    }
                    return;
                case 1285223808:
                    if (str.equals("THREAD_DELETE")) {
                        this.threadDelete.k.onNext(obj);
                        return;
                    }
                    return;
                case 1389895301:
                    if (str.equals("USER_GUILD_SETTINGS_UPDATE")) {
                        this.userGuildSettingsUpdate.k.onNext(obj);
                        return;
                    }
                    return;
                case 1476675193:
                    if (str.equals("GUILD_ROLE_CREATE")) {
                        this.guildRoleCreate.k.onNext(obj);
                        return;
                    }
                    return;
                case 1478484999:
                    if (str.equals("USER_CONNECTIONS_UPDATE")) {
                        SerializedSubject<Unit, Unit> serializedSubject = this.userConnectionUpdate;
                        serializedSubject.k.onNext(Unit.a);
                        return;
                    }
                    return;
                case 1493510952:
                    if (str.equals("GUILD_ROLE_DELETE")) {
                        this.guildRoleDelete.k.onNext(obj);
                        return;
                    }
                    return;
                case 1511636001:
                    if (str.equals("MESSAGE_UPDATE")) {
                        this.messageUpdate.k.onNext(obj);
                        return;
                    }
                    return;
                case 1570092061:
                    if (str.equals("INTERACTION_FAILURE")) {
                        this.interactionFailure.k.onNext(obj);
                        return;
                    }
                    return;
                case 1616207515:
                    if (str.equals("STREAM_CREATE")) {
                        this.streamCreate.k.onNext(obj);
                        return;
                    }
                    return;
                case 1622830784:
                    if (str.equals("GUILD_BAN_REMOVE")) {
                        this.guildBanRemove.k.onNext(obj);
                        return;
                    }
                    return;
                case 1633043274:
                    if (str.equals("STREAM_DELETE")) {
                        this.streamDelete.k.onNext(obj);
                        return;
                    }
                    return;
                case 1689894925:
                    if (str.equals("PRESENCES_REPLACE")) {
                        this.presenceReplace.k.onNext(obj);
                        return;
                    }
                    return;
                case 1699412580:
                    if (str.equals("VOICE_STATE_UPDATE")) {
                        this.voiceStateUpdate.onNext(obj);
                        return;
                    }
                    return;
                case 1737758480:
                    if (str.equals("GUILD_SCHEDULED_EVENT_USER_ADD")) {
                        this.guildScheduledEventUserAdd.k.onNext(obj);
                        return;
                    }
                    return;
                case 1781835934:
                    if (!str.equals("THREAD_UPDATE")) {
                        return;
                    }
                    this.threadCreateOrUpdate.k.onNext(obj);
                    return;
                case 1882183896:
                    if (str.equals("VOICE_SERVER_UPDATE")) {
                        this.voiceServerUpdate.k.onNext(obj);
                        return;
                    }
                    return;
                case 1928220071:
                    if (str.equals("THREAD_LIST_SYNC")) {
                        this.threadListSync.k.onNext(obj);
                        return;
                    }
                    return;
                case 1990123078:
                    if (str.equals("GUILD_ROLE_UPDATE")) {
                        this.guildRoleUpdate.k.onNext(obj);
                        return;
                    }
                    return;
                case 2129655400:
                    if (str.equals("STREAM_UPDATE")) {
                        this.streamUpdate.k.onNext(obj);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public final void handlePreLogout() {
        voiceStateUpdate$default(this, null, null, false, false, false, null, false, 96, null);
    }

    public final void handleRtcConnectionStateChanged(RtcConnection.StateChange stateChange) {
        m.checkNotNullParameter(stateChange, "stateChange");
        RtcConnection.State state = stateChange.a;
        if ((state instanceof RtcConnection.State.d) && ((RtcConnection.State.d) state).a) {
            RtcConnection.Metadata metadata = stateChange.f2754b;
            String str = metadata != null ? metadata.e : null;
            if (str instanceof String) {
                streamPing(str);
            } else {
                voiceServerPing();
            }
        }
    }

    public final void init(Context context, NetworkMonitor networkMonitor) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(networkMonitor, "networkMonitor");
        this.socket = buildGatewaySocket(context, networkMonitor);
        ClientState.Companion.initialize(this.stream, this.scheduler, new StoreGatewayConnection$init$1(this));
    }

    public final boolean presenceUpdate(ClientStatus clientStatus, Long l, List<Activity> list, Boolean bool) {
        return requestIfSessionEstablished(new StoreGatewayConnection$presenceUpdate$1(clientStatus, l, list, bool));
    }

    public final boolean requestApplicationCommands(long j, String str, boolean z2, String str2, Integer num, int i, List<String> list) {
        m.checkNotNullParameter(str, "nonce");
        return requestIfSessionEstablished(new StoreGatewayConnection$requestApplicationCommands$1(j, str, z2, num, str2, i, list));
    }

    public final boolean requestGuildMembers(long j) {
        return requestGuildMembers$default(this, j, null, null, null, 14, null);
    }

    public final boolean requestGuildMembers(long j, String str) {
        return requestGuildMembers$default(this, j, str, null, null, 12, null);
    }

    public final boolean requestGuildMembers(long j, String str, List<Long> list) {
        return requestGuildMembers$default(this, j, str, list, null, 8, null);
    }

    public final boolean requestGuildMembers(long j, String str, List<Long> list, Integer num) {
        return requestIfSessionEstablished(new StoreGatewayConnection$requestGuildMembers$1(j, str, list, num));
    }

    public final Unit resetOnError() {
        GatewaySocket gatewaySocket = this.socket;
        if (gatewaySocket == null) {
            return null;
        }
        gatewaySocket.resetOnError();
        return Unit.a;
    }

    public final void simulateReconnectForTesting() {
        GatewaySocket gatewaySocket = this.socket;
        if (gatewaySocket != null) {
            gatewaySocket.simulateReconnectForTesting();
        }
    }

    @StoreThread
    public final void streamCreate(String str, String str2) {
        m.checkNotNullParameter(str, "streamKey");
        requestIfSessionEstablished(new StoreGatewayConnection$streamCreate$1(this, str, ModelApplicationStream.Companion.decodeStreamKey(str), str2));
    }

    public final void streamDelete(String str) {
        m.checkNotNullParameter(str, "streamKey");
        requestIfSessionEstablished(new StoreGatewayConnection$streamDelete$1(str));
    }

    @StoreThread
    public final void streamWatch(String str) {
        m.checkNotNullParameter(str, "streamKey");
        requestIfSessionEstablished(new StoreGatewayConnection$streamWatch$1(this, str));
    }

    public final boolean updateGuildSubscriptions(long j, GuildSubscriptions guildSubscriptions) {
        m.checkNotNullParameter(guildSubscriptions, "guildSubscriptions");
        Map<Long, List<List<Integer>>> serializedRanges = guildSubscriptions.getSerializedRanges();
        Boolean typing = guildSubscriptions.getTyping();
        Boolean activities = guildSubscriptions.getActivities();
        Boolean threads = guildSubscriptions.getThreads();
        Set<Long> members = guildSubscriptions.getMembers();
        return requestIfSessionEstablished(new StoreGatewayConnection$updateGuildSubscriptions$1(j, new OutgoingPayload.GuildSubscriptions(serializedRanges, typing, activities, members != null ? u.toList(members) : null, threads, guildSubscriptions.getThreadMemberLists())));
    }

    public final boolean voiceStateUpdate(Long l, Long l2, boolean z2, boolean z3, boolean z4, String str, boolean z5) {
        return requestIfSessionEstablished(new StoreGatewayConnection$voiceStateUpdate$1(l, l2, z2, z3, z4, str, z5));
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public StoreGatewayConnection(com.discord.stores.StoreStream r7, com.discord.utilities.time.Clock r8, java.util.concurrent.ExecutorService r9, rx.Scheduler r10, com.discord.utilities.logging.AppGatewaySocketLogger r11, int r12, kotlin.jvm.internal.DefaultConstructorMarker r13) {
        /*
            r6 = this;
            r13 = r12 & 4
            if (r13 == 0) goto Le
            r9 = 1
            java.util.concurrent.ExecutorService r9 = java.util.concurrent.Executors.newFixedThreadPool(r9)
            java.lang.String r13 = "Executors.newFixedThreadPool(1)"
            d0.z.d.m.checkNotNullExpressionValue(r9, r13)
        Le:
            r3 = r9
            r9 = r12 & 8
            if (r9 == 0) goto L1f
            java.util.concurrent.atomic.AtomicReference<j0.p.a> r9 = j0.p.a.a
            j0.l.c.c r10 = new j0.l.c.c
            r10.<init>(r3)
            java.lang.String r9 = "Schedulers.from(executor)"
            d0.z.d.m.checkNotNullExpressionValue(r10, r9)
        L1f:
            r4 = r10
            r0 = r6
            r1 = r7
            r2 = r8
            r5 = r11
            r0.<init>(r1, r2, r3, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreGatewayConnection.<init>(com.discord.stores.StoreStream, com.discord.utilities.time.Clock, java.util.concurrent.ExecutorService, rx.Scheduler, com.discord.utilities.logging.AppGatewaySocketLogger, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }
}
