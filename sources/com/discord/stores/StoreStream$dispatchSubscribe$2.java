package com.discord.stores;

import androidx.exifinterface.media.ExifInterface;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreStream.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u0003\"\u0004\b\u0000\u0010\u00002\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00028\u00000\u0001H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {ExifInterface.GPS_DIRECTION_TRUE, "", "batch", "", "invoke", "(Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStream$dispatchSubscribe$2 extends o implements Function1<List<? extends T>, Unit> {
    public final /* synthetic */ Function1 $onNext;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreStream$dispatchSubscribe$2(Function1 function1) {
        super(1);
        this.$onNext = function1;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Object obj) {
        invoke((List) obj);
        return Unit.a;
    }

    public final void invoke(List<? extends T> list) {
        m.checkNotNullParameter(list, "batch");
        Iterator it = list.iterator();
        while (it.hasNext()) {
            this.$onNext.invoke(it.next());
        }
    }
}
