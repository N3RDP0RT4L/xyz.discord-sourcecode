package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.utilities.messagesend.MessageResult;
import kotlin.Metadata;
import rx.Emitter;
import rx.functions.Action1;
/* compiled from: StoreMessages.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042*\u0010\u0003\u001a&\u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00010\u0001 \u0002*\u0012\u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00010\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lrx/Emitter;", "Lcom/discord/utilities/messagesend/MessageResult;", "kotlin.jvm.PlatformType", "emitter", "", NotificationCompat.CATEGORY_CALL, "(Lrx/Emitter;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessages$sendMessage$2<T> implements Action1<Emitter<MessageResult>> {
    public static final StoreMessages$sendMessage$2 INSTANCE = new StoreMessages$sendMessage$2();

    public final void call(Emitter<MessageResult> emitter) {
        emitter.onNext(MessageResult.NoValidContent.INSTANCE);
        emitter.onCompleted();
    }
}
