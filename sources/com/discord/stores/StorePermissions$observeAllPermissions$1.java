package com.discord.stores;

import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StorePermissions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a&\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0001j\u0002`\u0003\u0012\b\u0012\u00060\u0001j\u0002`\u00040\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "", "Lcom/discord/primitives/GuildId;", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/permission/PermissionBit;", "invoke", "()Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StorePermissions$observeAllPermissions$1 extends o implements Function0<Map<Long, ? extends Map<Long, ? extends Long>>> {
    public final /* synthetic */ StorePermissions this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StorePermissions$observeAllPermissions$1(StorePermissions storePermissions) {
        super(0);
        this.this$0 = storePermissions;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Map<Long, ? extends Map<Long, ? extends Long>> invoke() {
        Map<Long, ? extends Map<Long, ? extends Long>> map;
        map = this.this$0.permissionsForChannels;
        return map;
    }
}
