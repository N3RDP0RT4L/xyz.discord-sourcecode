package com.discord.stores;

import com.discord.stores.StoreMediaFavorites;
import d0.z.d.o;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreMediaFavorites.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"", "Lcom/discord/stores/StoreMediaFavorites$Favorite;", "invoke", "()Ljava/util/Set;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMediaFavorites$observeFavorites$1 extends o implements Function0<Set<? extends StoreMediaFavorites.Favorite>> {
    public final /* synthetic */ Set $types;
    public final /* synthetic */ StoreMediaFavorites this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreMediaFavorites$observeFavorites$1(StoreMediaFavorites storeMediaFavorites, Set set) {
        super(0);
        this.this$0 = storeMediaFavorites;
        this.$types = set;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Set<? extends StoreMediaFavorites.Favorite> invoke() {
        return this.this$0.getFavorites(this.$types);
    }
}
