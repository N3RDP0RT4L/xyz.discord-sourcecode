package com.discord.stores;

import com.discord.models.guild.Guild;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreGuildSelected.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/models/guild/Guild;", "invoke", "()Lcom/discord/models/guild/Guild;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildSelected$observeSelectedGuild$1 extends o implements Function0<Guild> {
    public final /* synthetic */ StoreGuildSelected this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGuildSelected$observeSelectedGuild$1(StoreGuildSelected storeGuildSelected) {
        super(0);
        this.this$0 = storeGuildSelected;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final Guild invoke() {
        StoreGuilds storeGuilds;
        storeGuilds = this.this$0.guildStore;
        return storeGuilds.getGuilds().get(Long.valueOf(this.this$0.getSelectedGuildId()));
    }
}
