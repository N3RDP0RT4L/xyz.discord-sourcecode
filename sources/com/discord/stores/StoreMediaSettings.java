package com.discord.stores;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.permission.Permission;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.rtcconnection.mediaengine.MediaEngine;
import com.discord.rtcconnection.mediaengine.MediaEngineConnection;
import com.discord.utilities.cache.SharedPreferencesProvider;
import com.discord.utilities.media.AppSound;
import com.discord.utilities.media.AppSoundManager;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.t.h0;
import d0.t.k;
import d0.z.d.m;
import j0.k.b;
import java.util.HashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.webrtc.PeerConnectionFactory;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subjects.SerializedSubject;
/* compiled from: StoreMediaSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0098\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001:\u0004lmnoB3\u0012\u0006\u0010]\u001a\u00020\\\u0012\u0006\u0010h\u001a\u00020g\u0012\u0006\u0010V\u001a\u00020U\u0012\b\b\u0002\u0010`\u001a\u00020_\u0012\b\b\u0002\u0010c\u001a\u00020b¢\u0006\u0004\bj\u0010kJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\r\u0010\t\u001a\u00020\u0004¢\u0006\u0004\b\t\u0010\bJ\u0013\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00020\n¢\u0006\u0004\b\u000b\u0010\fJ\u0013\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00020\n¢\u0006\u0004\b\r\u0010\fJ\u001b\u0010\u0011\u001a\u00020\u00042\n\u0010\u0010\u001a\u00060\u000ej\u0002`\u000fH\u0007¢\u0006\u0004\b\u0011\u0010\u0012J\u0015\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0014\u001a\u00020\u0013¢\u0006\u0004\b\u0015\u0010\u0016J\u000f\u0010\u0018\u001a\u0004\u0018\u00010\u0017¢\u0006\u0004\b\u0018\u0010\u0019J\u0017\u0010\u001b\u001a\u0004\u0018\u00010\u00172\u0006\u0010\u001a\u001a\u00020\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ\r\u0010\u001d\u001a\u00020\u0004¢\u0006\u0004\b\u001d\u0010\bJ\u0015\u0010\u001f\u001a\u00020\u00042\u0006\u0010\u001e\u001a\u00020\u0002¢\u0006\u0004\b\u001f\u0010\u0006J!\u0010$\u001a\u00020\u00042\n\u0010!\u001a\u00060\u000ej\u0002` 2\u0006\u0010#\u001a\u00020\"¢\u0006\u0004\b$\u0010%J\u0015\u0010'\u001a\u00020\u00042\u0006\u0010&\u001a\u00020\"¢\u0006\u0004\b'\u0010(J\u0019\u0010)\u001a\u00020\u00042\n\u0010!\u001a\u00060\u000ej\u0002` ¢\u0006\u0004\b)\u0010\u0012J\u0015\u0010+\u001a\u00020\u00042\u0006\u0010*\u001a\u00020\"¢\u0006\u0004\b+\u0010(J\u0013\u0010-\u001a\b\u0012\u0004\u0012\u00020,0\n¢\u0006\u0004\b-\u0010\fJ\r\u0010.\u001a\u00020,¢\u0006\u0004\b.\u0010/J#\u00101\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u000ej\u0002` \u0012\u0004\u0012\u00020\u0002000\n¢\u0006\u0004\b1\u0010\fJ\u0013\u00102\u001a\b\u0012\u0004\u0012\u00020\u00130\n¢\u0006\u0004\b2\u0010\fJ#\u00103\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u000ej\u0002` \u0012\u0004\u0012\u00020\"000\n¢\u0006\u0004\b3\u0010\fJ\u0013\u00104\u001a\b\u0012\u0004\u0012\u00020,0\n¢\u0006\u0004\b4\u0010\fJ\u001d\u00105\u001a\u0012\u0012\b\u0012\u00060\u000ej\u0002` \u0012\u0004\u0012\u00020\u000200¢\u0006\u0004\b5\u00106J!\u00108\u001a\u00020\u00042\n\u0010!\u001a\u00060\u000ej\u0002` 2\u0006\u00107\u001a\u00020\u0002¢\u0006\u0004\b8\u00109J#\u0010:\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u000ej\u0002` \u0012\u0004\u0012\u00020\u0002000\n¢\u0006\u0004\b:\u0010\fJ\r\u0010;\u001a\u00020\u0002¢\u0006\u0004\b;\u0010<J\r\u0010=\u001a\u00020\u0004¢\u0006\u0004\b=\u0010\bJ\u0015\u0010@\u001a\u00020\u00042\u0006\u0010?\u001a\u00020>¢\u0006\u0004\b@\u0010AJ\u0015\u0010C\u001a\u00020\u00042\u0006\u0010B\u001a\u00020\u0002¢\u0006\u0004\bC\u0010\u0006J\r\u0010D\u001a\u00020\u0004¢\u0006\u0004\bD\u0010\bJ\r\u0010E\u001a\u00020\u0004¢\u0006\u0004\bE\u0010\bJ\u0015\u0010F\u001a\u00020\u00042\u0006\u0010?\u001a\u00020>¢\u0006\u0004\bF\u0010AJ\u0015\u0010I\u001a\u00020\u00042\u0006\u0010H\u001a\u00020G¢\u0006\u0004\bI\u0010JJ\r\u0010K\u001a\u00020\u0004¢\u0006\u0004\bK\u0010\bJ\r\u0010L\u001a\u00020\u0004¢\u0006\u0004\bL\u0010\bJ\r\u0010M\u001a\u00020\u0004¢\u0006\u0004\bM\u0010\bJ\r\u0010N\u001a\u00020\u0004¢\u0006\u0004\bN\u0010\bJ\r\u0010O\u001a\u00020\u0004¢\u0006\u0004\bO\u0010\bR2\u0010R\u001a\u001e\u0012\f\u0012\n Q*\u0004\u0018\u00010,0,\u0012\f\u0012\n Q*\u0004\u0018\u00010,0,0P8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bR\u0010SR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0003\u0010TR\u0016\u0010V\u001a\u00020U8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bV\u0010WR$\u0010Y\u001a\u00020,2\u0006\u0010X\u001a\u00020,8\u0002@BX\u0082\u000e¢\u0006\f\n\u0004\bY\u0010Z\"\u0004\bF\u0010[R\u0016\u0010]\u001a\u00020\\8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b]\u0010^R\u0016\u0010`\u001a\u00020_8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b`\u0010aR\u0016\u0010c\u001a\u00020b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bc\u0010dR\u0018\u0010e\u001a\u0004\u0018\u00010\u00178\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\be\u0010fR\u0016\u0010h\u001a\u00020g8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bh\u0010i¨\u0006p"}, d2 = {"Lcom/discord/stores/StoreMediaSettings;", "Lcom/discord/stores/Store;", "", "canUseVad", "", "handleCanUseVad", "(Z)V", "updateForceMute", "()V", "init", "Lrx/Observable;", "isSelfMuted", "()Lrx/Observable;", "isSelfDeafened", "", "Lcom/discord/primitives/ChannelId;", "channelId", "handleVoiceChannelSelected", "(J)V", "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;", "inputMode", "setVoiceInputMode", "(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;)V", "Lcom/discord/stores/StoreMediaSettings$SelfMuteFailure;", "toggleSelfMuted", "()Lcom/discord/stores/StoreMediaSettings$SelfMuteFailure;", "isMuted", "setSelfMuted", "(Z)Lcom/discord/stores/StoreMediaSettings$SelfMuteFailure;", "toggleSelfDeafened", "isDeafened", "setSelfDeafen", "Lcom/discord/primitives/UserId;", "userId", "", "volume", "setUserOutputVolume", "(JF)V", "sensitivity", "setSensitivity", "(F)V", "toggleUserMuted", "outputVolume", "setOutputVolume", "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "getVoiceConfiguration", "getVoiceConfigurationBlocking", "()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "", "getUsersMuted", "getInputMode", "getUsersVolume", "getVoiceConfig", "getMutedUsers", "()Ljava/util/Map;", "offScreen", "setUserOffScreen", "(JZ)V", "getUsersOffScreen", "getVideoHardwareScalingBlocking", "()Z", "toggleAutomaticGainControl", "Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;", "noiseProcessing", "setNoiseProcessing", "(Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;)V", "hidden", "updateVoiceParticipantsHidden", "toggleNoiseSuppression", "toggleNoiseCancellation", "setVoiceConfiguration", "Lcom/discord/stores/StoreMediaSettings$VadUseKrisp;", "status", "setVADUseKrisp", "(Lcom/discord/stores/StoreMediaSettings$VadUseKrisp;)V", "toggleVADUseKrisp", "revertTemporaryDisableKrisp", "toggleEchoCancellation", "toggleAutomaticVAD", "toggleEnableVideoHardwareScaling", "Lrx/subjects/SerializedSubject;", "kotlin.jvm.PlatformType", "voiceConfigurationSubject", "Lrx/subjects/SerializedSubject;", "Z", "Lcom/discord/stores/StorePermissions;", "storePermissions", "Lcom/discord/stores/StorePermissions;", "value", "voiceConfiguration", "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "(Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;)V", "Lcom/discord/stores/StoreVoiceChannelSelected;", "storeVoiceChannelSelected", "Lcom/discord/stores/StoreVoiceChannelSelected;", "Lcom/discord/stores/VoiceConfigurationCache;", "voiceConfigurationCache", "Lcom/discord/stores/VoiceConfigurationCache;", "Lcom/discord/utilities/media/AppSoundManager;", "appSoundManager", "Lcom/discord/utilities/media/AppSoundManager;", "forceSelfMuteReason", "Lcom/discord/stores/StoreMediaSettings$SelfMuteFailure;", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreChannels;", HookHelper.constructorName, "(Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/VoiceConfigurationCache;Lcom/discord/utilities/media/AppSoundManager;)V", "NoiseProcessing", "SelfMuteFailure", "VadUseKrisp", "VoiceConfiguration", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMediaSettings extends Store {
    private final AppSoundManager appSoundManager;
    private boolean canUseVad;
    private SelfMuteFailure forceSelfMuteReason;
    private final StoreChannels storeChannels;
    private final StorePermissions storePermissions;
    private final StoreVoiceChannelSelected storeVoiceChannelSelected;
    private VoiceConfiguration voiceConfiguration;
    private final VoiceConfigurationCache voiceConfigurationCache;
    private final SerializedSubject<VoiceConfiguration, VoiceConfiguration> voiceConfigurationSubject;

    /* compiled from: StoreMediaSettings.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "None", "Suppression", "Cancellation", "CancellationTemporarilyDisabled", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum NoiseProcessing {
        None,
        Suppression,
        Cancellation,
        CancellationTemporarilyDisabled
    }

    /* compiled from: StoreMediaSettings.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/stores/StoreMediaSettings$SelfMuteFailure;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "CANNOT_USE_VAD", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum SelfMuteFailure {
        CANNOT_USE_VAD
    }

    /* compiled from: StoreMediaSettings.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/stores/StoreMediaSettings$VadUseKrisp;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "Disabled", PeerConnectionFactory.TRIAL_ENABLED, "TemporarilyDisabled", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum VadUseKrisp {
        Disabled,
        Enabled,
        TemporarilyDisabled
    }

    /* compiled from: StoreMediaSettings.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0016\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u001c\b\u0086\b\u0018\u0000 Q2\u00020\u0001:\u0001QBÍ\u0001\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0002\u0012\b\b\u0002\u0010#\u001a\u00020\u0002\u0012\b\b\u0002\u0010$\u001a\u00020\u0002\u0012\b\b\u0002\u0010%\u001a\u00020\u000b\u0012\b\b\u0002\u0010&\u001a\u00020\u0002\u0012\b\b\u0002\u0010'\u001a\u00020\u0002\u0012\b\b\u0002\u0010(\u001a\u00020\u0010\u0012\b\b\u0002\u0010)\u001a\u00020\u0013\u0012\b\b\u0002\u0010*\u001a\u00020\u0016\u0012\b\b\u0002\u0010+\u001a\u00020\u0013\u0012\u0018\b\u0002\u0010,\u001a\u0012\u0012\b\u0012\u00060\u001bj\u0002`\u001c\u0012\u0004\u0012\u00020\u00020\u001a\u0012\u0018\b\u0002\u0010-\u001a\u0012\u0012\b\u0012\u00060\u001bj\u0002`\u001c\u0012\u0004\u0012\u00020\u00130\u001a\u0012\u0018\b\u0002\u0010.\u001a\u0012\u0012\b\u0012\u00060\u001bj\u0002`\u001c\u0012\u0004\u0012\u00020\u00020\u001a\u0012\b\b\u0002\u0010/\u001a\u00020\u0002\u0012\b\b\u0002\u00100\u001a\u00020\u0002¢\u0006\u0004\bO\u0010PJ\u0010\u0010\u0003\u001a\u00020\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\r\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0004J\r\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\t\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\u0004J\u0010\u0010\n\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\n\u0010\u0004J\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u000e\u0010\u0004J\u0010\u0010\u000f\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0004J\u0010\u0010\u0011\u001a\u00020\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0017\u001a\u00020\u0016HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0019\u001a\u00020\u0013HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u0015J \u0010\u001d\u001a\u0012\u0012\b\u0012\u00060\u001bj\u0002`\u001c\u0012\u0004\u0012\u00020\u00020\u001aHÆ\u0003¢\u0006\u0004\b\u001d\u0010\u001eJ \u0010\u001f\u001a\u0012\u0012\b\u0012\u00060\u001bj\u0002`\u001c\u0012\u0004\u0012\u00020\u00130\u001aHÆ\u0003¢\u0006\u0004\b\u001f\u0010\u001eJ \u0010 \u001a\u0012\u0012\b\u0012\u00060\u001bj\u0002`\u001c\u0012\u0004\u0012\u00020\u00020\u001aHÆ\u0003¢\u0006\u0004\b \u0010\u001eJ\u0010\u0010!\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b!\u0010\u0004J\u0010\u0010\"\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\"\u0010\u0004JÖ\u0001\u00101\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u00022\b\b\u0002\u0010#\u001a\u00020\u00022\b\b\u0002\u0010$\u001a\u00020\u00022\b\b\u0002\u0010%\u001a\u00020\u000b2\b\b\u0002\u0010&\u001a\u00020\u00022\b\b\u0002\u0010'\u001a\u00020\u00022\b\b\u0002\u0010(\u001a\u00020\u00102\b\b\u0002\u0010)\u001a\u00020\u00132\b\b\u0002\u0010*\u001a\u00020\u00162\b\b\u0002\u0010+\u001a\u00020\u00132\u0018\b\u0002\u0010,\u001a\u0012\u0012\b\u0012\u00060\u001bj\u0002`\u001c\u0012\u0004\u0012\u00020\u00020\u001a2\u0018\b\u0002\u0010-\u001a\u0012\u0012\b\u0012\u00060\u001bj\u0002`\u001c\u0012\u0004\u0012\u00020\u00130\u001a2\u0018\b\u0002\u0010.\u001a\u0012\u0012\b\u0012\u00060\u001bj\u0002`\u001c\u0012\u0004\u0012\u00020\u00020\u001a2\b\b\u0002\u0010/\u001a\u00020\u00022\b\b\u0002\u00100\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b1\u00102J\u0010\u00104\u001a\u000203HÖ\u0001¢\u0006\u0004\b4\u00105J\u0010\u00107\u001a\u000206HÖ\u0001¢\u0006\u0004\b7\u00108J\u001a\u0010:\u001a\u00020\u00022\b\u00109\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b:\u0010;R\u0019\u0010'\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010<\u001a\u0004\b=\u0010\u0004R\u0019\u0010(\u001a\u00020\u00108\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010>\u001a\u0004\b?\u0010\u0012R\u0019\u0010)\u001a\u00020\u00138\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010@\u001a\u0004\bA\u0010\u0015R\u0019\u0010*\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010B\u001a\u0004\bC\u0010\u0018R)\u0010-\u001a\u0012\u0012\b\u0012\u00060\u001bj\u0002`\u001c\u0012\u0004\u0012\u00020\u00130\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010D\u001a\u0004\bE\u0010\u001eR\u0019\u0010/\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010<\u001a\u0004\bF\u0010\u0004R)\u0010.\u001a\u0012\u0012\b\u0012\u00060\u001bj\u0002`\u001c\u0012\u0004\u0012\u00020\u00020\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010D\u001a\u0004\bG\u0010\u001eR\u0019\u00100\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010<\u001a\u0004\bH\u0010\u0004R\u0019\u0010+\u001a\u00020\u00138\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010@\u001a\u0004\bI\u0010\u0015R)\u0010,\u001a\u0012\u0012\b\u0012\u00060\u001bj\u0002`\u001c\u0012\u0004\u0012\u00020\u00020\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010D\u001a\u0004\bJ\u0010\u001eR\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010<R\u0019\u0010#\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010<\u001a\u0004\b#\u0010\u0004R\u0019\u0010$\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010<\u001a\u0004\bK\u0010\u0004R\u0019\u0010%\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010L\u001a\u0004\bM\u0010\rR\u0019\u0010&\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010<\u001a\u0004\bN\u0010\u0004¨\u0006R"}, d2 = {"Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "", "", "component1", "()Z", "isSelfMuted", "Lcom/discord/rtcconnection/mediaengine/MediaEngine$VoiceConfig;", "toMediaEngineVoiceConfig", "()Lcom/discord/rtcconnection/mediaengine/MediaEngine$VoiceConfig;", "component2", "component3", "Lcom/discord/stores/StoreMediaSettings$VadUseKrisp;", "component4", "()Lcom/discord/stores/StoreMediaSettings$VadUseKrisp;", "component5", "component6", "Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;", "component7", "()Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;", "", "component8", "()F", "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;", "component9", "()Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;", "component10", "", "", "Lcom/discord/primitives/UserId;", "component11", "()Ljava/util/Map;", "component12", "component13", "component14", "component15", "isSelfDeafened", "automaticVad", "vadUseKrisp", "automaticGainControl", "echoCancellation", "noiseProcessing", "sensitivity", "inputMode", "outputVolume", "mutedUsers", "userOutputVolumes", "offScreenUsers", "enableVideoHardwareScaling", "voiceParticipantsHidden", "copy", "(ZZZLcom/discord/stores/StoreMediaSettings$VadUseKrisp;ZZLcom/discord/stores/StoreMediaSettings$NoiseProcessing;FLcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;FLjava/util/Map;Ljava/util/Map;Ljava/util/Map;ZZ)Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getEchoCancellation", "Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;", "getNoiseProcessing", "F", "getSensitivity", "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;", "getInputMode", "Ljava/util/Map;", "getUserOutputVolumes", "getEnableVideoHardwareScaling", "getOffScreenUsers", "getVoiceParticipantsHidden", "getOutputVolume", "getMutedUsers", "getAutomaticVad", "Lcom/discord/stores/StoreMediaSettings$VadUseKrisp;", "getVadUseKrisp", "getAutomaticGainControl", HookHelper.constructorName, "(ZZZLcom/discord/stores/StoreMediaSettings$VadUseKrisp;ZZLcom/discord/stores/StoreMediaSettings$NoiseProcessing;FLcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;FLjava/util/Map;Ljava/util/Map;Ljava/util/Map;ZZ)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class VoiceConfiguration {
        public static final Companion Companion = new Companion(null);
        private static final int DEFAULT_NOISE_PROCESSING;
        public static final float DEFAULT_OUTPUT_VOLUME = 100.0f;
        public static final float DEFAULT_SENSITIVITY = -50.0f;
        private static final VoiceConfiguration DEFAULT_VOICE_CONFIG;
        private final boolean automaticGainControl;
        private final boolean automaticVad;
        private final boolean echoCancellation;
        private final boolean enableVideoHardwareScaling;
        private final MediaEngineConnection.InputMode inputMode;
        private final boolean isSelfDeafened;
        private final boolean isSelfMuted;
        private final Map<Long, Boolean> mutedUsers;
        private final NoiseProcessing noiseProcessing;
        private final Map<Long, Boolean> offScreenUsers;
        private final float outputVolume;
        private final float sensitivity;
        private final Map<Long, Float> userOutputVolumes;
        private final VadUseKrisp vadUseKrisp;
        private final boolean voiceParticipantsHidden;

        /* compiled from: StoreMediaSettings.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0007\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0019\u0010\b\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0016\u0010\u000f\u001a\u00020\f8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000f\u0010\u000e¨\u0006\u0012"}, d2 = {"Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration$Companion;", "", "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "DEFAULT_VOICE_CONFIG", "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "getDEFAULT_VOICE_CONFIG", "()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "", "DEFAULT_NOISE_PROCESSING", "I", "getDEFAULT_NOISE_PROCESSING", "()I", "", "DEFAULT_OUTPUT_VOLUME", "F", "DEFAULT_SENSITIVITY", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Companion {
            private Companion() {
            }

            public final int getDEFAULT_NOISE_PROCESSING() {
                return VoiceConfiguration.DEFAULT_NOISE_PROCESSING;
            }

            public final VoiceConfiguration getDEFAULT_VOICE_CONFIG() {
                return VoiceConfiguration.DEFAULT_VOICE_CONFIG;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        static {
            VoiceConfiguration voiceConfiguration = new VoiceConfiguration(false, false, false, null, false, false, null, 0.0f, null, 0.0f, null, null, null, false, false, 32767, null);
            DEFAULT_VOICE_CONFIG = voiceConfiguration;
            DEFAULT_NOISE_PROCESSING = voiceConfiguration.noiseProcessing.ordinal();
        }

        public VoiceConfiguration() {
            this(false, false, false, null, false, false, null, 0.0f, null, 0.0f, null, null, null, false, false, 32767, null);
        }

        public VoiceConfiguration(boolean z2, boolean z3, boolean z4, VadUseKrisp vadUseKrisp, boolean z5, boolean z6, NoiseProcessing noiseProcessing, float f, MediaEngineConnection.InputMode inputMode, float f2, Map<Long, Boolean> map, Map<Long, Float> map2, Map<Long, Boolean> map3, boolean z7, boolean z8) {
            m.checkNotNullParameter(vadUseKrisp, "vadUseKrisp");
            m.checkNotNullParameter(noiseProcessing, "noiseProcessing");
            m.checkNotNullParameter(inputMode, "inputMode");
            m.checkNotNullParameter(map, "mutedUsers");
            m.checkNotNullParameter(map2, "userOutputVolumes");
            m.checkNotNullParameter(map3, "offScreenUsers");
            this.isSelfMuted = z2;
            this.isSelfDeafened = z3;
            this.automaticVad = z4;
            this.vadUseKrisp = vadUseKrisp;
            this.automaticGainControl = z5;
            this.echoCancellation = z6;
            this.noiseProcessing = noiseProcessing;
            this.sensitivity = f;
            this.inputMode = inputMode;
            this.outputVolume = f2;
            this.mutedUsers = map;
            this.userOutputVolumes = map2;
            this.offScreenUsers = map3;
            this.enableVideoHardwareScaling = z7;
            this.voiceParticipantsHidden = z8;
        }

        private final boolean component1() {
            return this.isSelfMuted;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ VoiceConfiguration copy$default(VoiceConfiguration voiceConfiguration, boolean z2, boolean z3, boolean z4, VadUseKrisp vadUseKrisp, boolean z5, boolean z6, NoiseProcessing noiseProcessing, float f, MediaEngineConnection.InputMode inputMode, float f2, Map map, Map map2, Map map3, boolean z7, boolean z8, int i, Object obj) {
            return voiceConfiguration.copy((i & 1) != 0 ? voiceConfiguration.isSelfMuted : z2, (i & 2) != 0 ? voiceConfiguration.isSelfDeafened : z3, (i & 4) != 0 ? voiceConfiguration.automaticVad : z4, (i & 8) != 0 ? voiceConfiguration.vadUseKrisp : vadUseKrisp, (i & 16) != 0 ? voiceConfiguration.automaticGainControl : z5, (i & 32) != 0 ? voiceConfiguration.echoCancellation : z6, (i & 64) != 0 ? voiceConfiguration.noiseProcessing : noiseProcessing, (i & 128) != 0 ? voiceConfiguration.sensitivity : f, (i & 256) != 0 ? voiceConfiguration.inputMode : inputMode, (i & 512) != 0 ? voiceConfiguration.outputVolume : f2, (i & 1024) != 0 ? voiceConfiguration.mutedUsers : map, (i & 2048) != 0 ? voiceConfiguration.userOutputVolumes : map2, (i & 4096) != 0 ? voiceConfiguration.offScreenUsers : map3, (i & 8192) != 0 ? voiceConfiguration.enableVideoHardwareScaling : z7, (i & 16384) != 0 ? voiceConfiguration.voiceParticipantsHidden : z8);
        }

        public final float component10() {
            return this.outputVolume;
        }

        public final Map<Long, Boolean> component11() {
            return this.mutedUsers;
        }

        public final Map<Long, Float> component12() {
            return this.userOutputVolumes;
        }

        public final Map<Long, Boolean> component13() {
            return this.offScreenUsers;
        }

        public final boolean component14() {
            return this.enableVideoHardwareScaling;
        }

        public final boolean component15() {
            return this.voiceParticipantsHidden;
        }

        public final boolean component2() {
            return this.isSelfDeafened;
        }

        public final boolean component3() {
            return this.automaticVad;
        }

        public final VadUseKrisp component4() {
            return this.vadUseKrisp;
        }

        public final boolean component5() {
            return this.automaticGainControl;
        }

        public final boolean component6() {
            return this.echoCancellation;
        }

        public final NoiseProcessing component7() {
            return this.noiseProcessing;
        }

        public final float component8() {
            return this.sensitivity;
        }

        public final MediaEngineConnection.InputMode component9() {
            return this.inputMode;
        }

        public final VoiceConfiguration copy(boolean z2, boolean z3, boolean z4, VadUseKrisp vadUseKrisp, boolean z5, boolean z6, NoiseProcessing noiseProcessing, float f, MediaEngineConnection.InputMode inputMode, float f2, Map<Long, Boolean> map, Map<Long, Float> map2, Map<Long, Boolean> map3, boolean z7, boolean z8) {
            m.checkNotNullParameter(vadUseKrisp, "vadUseKrisp");
            m.checkNotNullParameter(noiseProcessing, "noiseProcessing");
            m.checkNotNullParameter(inputMode, "inputMode");
            m.checkNotNullParameter(map, "mutedUsers");
            m.checkNotNullParameter(map2, "userOutputVolumes");
            m.checkNotNullParameter(map3, "offScreenUsers");
            return new VoiceConfiguration(z2, z3, z4, vadUseKrisp, z5, z6, noiseProcessing, f, inputMode, f2, map, map2, map3, z7, z8);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof VoiceConfiguration)) {
                return false;
            }
            VoiceConfiguration voiceConfiguration = (VoiceConfiguration) obj;
            return this.isSelfMuted == voiceConfiguration.isSelfMuted && this.isSelfDeafened == voiceConfiguration.isSelfDeafened && this.automaticVad == voiceConfiguration.automaticVad && m.areEqual(this.vadUseKrisp, voiceConfiguration.vadUseKrisp) && this.automaticGainControl == voiceConfiguration.automaticGainControl && this.echoCancellation == voiceConfiguration.echoCancellation && m.areEqual(this.noiseProcessing, voiceConfiguration.noiseProcessing) && Float.compare(this.sensitivity, voiceConfiguration.sensitivity) == 0 && m.areEqual(this.inputMode, voiceConfiguration.inputMode) && Float.compare(this.outputVolume, voiceConfiguration.outputVolume) == 0 && m.areEqual(this.mutedUsers, voiceConfiguration.mutedUsers) && m.areEqual(this.userOutputVolumes, voiceConfiguration.userOutputVolumes) && m.areEqual(this.offScreenUsers, voiceConfiguration.offScreenUsers) && this.enableVideoHardwareScaling == voiceConfiguration.enableVideoHardwareScaling && this.voiceParticipantsHidden == voiceConfiguration.voiceParticipantsHidden;
        }

        public final boolean getAutomaticGainControl() {
            return this.automaticGainControl;
        }

        public final boolean getAutomaticVad() {
            return this.automaticVad;
        }

        public final boolean getEchoCancellation() {
            return this.echoCancellation;
        }

        public final boolean getEnableVideoHardwareScaling() {
            return this.enableVideoHardwareScaling;
        }

        public final MediaEngineConnection.InputMode getInputMode() {
            return this.inputMode;
        }

        public final Map<Long, Boolean> getMutedUsers() {
            return this.mutedUsers;
        }

        public final NoiseProcessing getNoiseProcessing() {
            return this.noiseProcessing;
        }

        public final Map<Long, Boolean> getOffScreenUsers() {
            return this.offScreenUsers;
        }

        public final float getOutputVolume() {
            return this.outputVolume;
        }

        public final float getSensitivity() {
            return this.sensitivity;
        }

        public final Map<Long, Float> getUserOutputVolumes() {
            return this.userOutputVolumes;
        }

        public final VadUseKrisp getVadUseKrisp() {
            return this.vadUseKrisp;
        }

        public final boolean getVoiceParticipantsHidden() {
            return this.voiceParticipantsHidden;
        }

        public int hashCode() {
            boolean z2 = this.isSelfMuted;
            int i = 1;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = i2 * 31;
            boolean z3 = this.isSelfDeafened;
            if (z3) {
                z3 = true;
            }
            int i5 = z3 ? 1 : 0;
            int i6 = z3 ? 1 : 0;
            int i7 = (i4 + i5) * 31;
            boolean z4 = this.automaticVad;
            if (z4) {
                z4 = true;
            }
            int i8 = z4 ? 1 : 0;
            int i9 = z4 ? 1 : 0;
            int i10 = (i7 + i8) * 31;
            VadUseKrisp vadUseKrisp = this.vadUseKrisp;
            int i11 = 0;
            int hashCode = (i10 + (vadUseKrisp != null ? vadUseKrisp.hashCode() : 0)) * 31;
            boolean z5 = this.automaticGainControl;
            if (z5) {
                z5 = true;
            }
            int i12 = z5 ? 1 : 0;
            int i13 = z5 ? 1 : 0;
            int i14 = (hashCode + i12) * 31;
            boolean z6 = this.echoCancellation;
            if (z6) {
                z6 = true;
            }
            int i15 = z6 ? 1 : 0;
            int i16 = z6 ? 1 : 0;
            int i17 = (i14 + i15) * 31;
            NoiseProcessing noiseProcessing = this.noiseProcessing;
            int floatToIntBits = (Float.floatToIntBits(this.sensitivity) + ((i17 + (noiseProcessing != null ? noiseProcessing.hashCode() : 0)) * 31)) * 31;
            MediaEngineConnection.InputMode inputMode = this.inputMode;
            int floatToIntBits2 = (Float.floatToIntBits(this.outputVolume) + ((floatToIntBits + (inputMode != null ? inputMode.hashCode() : 0)) * 31)) * 31;
            Map<Long, Boolean> map = this.mutedUsers;
            int hashCode2 = (floatToIntBits2 + (map != null ? map.hashCode() : 0)) * 31;
            Map<Long, Float> map2 = this.userOutputVolumes;
            int hashCode3 = (hashCode2 + (map2 != null ? map2.hashCode() : 0)) * 31;
            Map<Long, Boolean> map3 = this.offScreenUsers;
            if (map3 != null) {
                i11 = map3.hashCode();
            }
            int i18 = (hashCode3 + i11) * 31;
            boolean z7 = this.enableVideoHardwareScaling;
            if (z7) {
                z7 = true;
            }
            int i19 = z7 ? 1 : 0;
            int i20 = z7 ? 1 : 0;
            int i21 = (i18 + i19) * 31;
            boolean z8 = this.voiceParticipantsHidden;
            if (!z8) {
                i = z8 ? 1 : 0;
            }
            return i21 + i;
        }

        public final boolean isSelfDeafened() {
            return this.isSelfDeafened;
        }

        public final boolean isSelfMuted() {
            return this.isSelfMuted || this.isSelfDeafened;
        }

        public final MediaEngine.VoiceConfig toMediaEngineVoiceConfig() {
            boolean z2 = true & true;
            boolean z3 = true & true;
            boolean z4 = true & true;
            return new MediaEngine.VoiceConfig(this.outputVolume, this.echoCancellation, k.contains(new NoiseProcessing[]{NoiseProcessing.Suppression, NoiseProcessing.CancellationTemporarilyDisabled}, this.noiseProcessing), this.noiseProcessing == NoiseProcessing.Cancellation, this.automaticGainControl, this.inputMode, new MediaEngineConnection.c((int) this.sensitivity, true & true ? 10 : 0, true & true ? 40 : 0, this.automaticVad, this.vadUseKrisp == VadUseKrisp.Enabled, true & true ? 5 : 0), this.isSelfDeafened, isSelfMuted());
        }

        public String toString() {
            StringBuilder R = a.R("VoiceConfiguration(isSelfMuted=");
            R.append(this.isSelfMuted);
            R.append(", isSelfDeafened=");
            R.append(this.isSelfDeafened);
            R.append(", automaticVad=");
            R.append(this.automaticVad);
            R.append(", vadUseKrisp=");
            R.append(this.vadUseKrisp);
            R.append(", automaticGainControl=");
            R.append(this.automaticGainControl);
            R.append(", echoCancellation=");
            R.append(this.echoCancellation);
            R.append(", noiseProcessing=");
            R.append(this.noiseProcessing);
            R.append(", sensitivity=");
            R.append(this.sensitivity);
            R.append(", inputMode=");
            R.append(this.inputMode);
            R.append(", outputVolume=");
            R.append(this.outputVolume);
            R.append(", mutedUsers=");
            R.append(this.mutedUsers);
            R.append(", userOutputVolumes=");
            R.append(this.userOutputVolumes);
            R.append(", offScreenUsers=");
            R.append(this.offScreenUsers);
            R.append(", enableVideoHardwareScaling=");
            R.append(this.enableVideoHardwareScaling);
            R.append(", voiceParticipantsHidden=");
            return a.M(R, this.voiceParticipantsHidden, ")");
        }

        public /* synthetic */ VoiceConfiguration(boolean z2, boolean z3, boolean z4, VadUseKrisp vadUseKrisp, boolean z5, boolean z6, NoiseProcessing noiseProcessing, float f, MediaEngineConnection.InputMode inputMode, float f2, Map map, Map map2, Map map3, boolean z7, boolean z8, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? false : z2, (i & 2) != 0 ? false : z3, (i & 4) != 0 ? true : z4, (i & 8) != 0 ? VadUseKrisp.Enabled : vadUseKrisp, (i & 16) != 0 ? true : z5, (i & 32) == 0 ? z6 : true, (i & 64) != 0 ? NoiseProcessing.Cancellation : noiseProcessing, (i & 128) != 0 ? -50.0f : f, (i & 256) != 0 ? MediaEngineConnection.InputMode.VOICE_ACTIVITY : inputMode, (i & 512) != 0 ? 100.0f : f2, (i & 1024) != 0 ? h0.emptyMap() : map, (i & 2048) != 0 ? h0.emptyMap() : map2, (i & 4096) != 0 ? h0.emptyMap() : map3, (i & 8192) != 0 ? false : z7, (i & 16384) == 0 ? z8 : false);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            VadUseKrisp.values();
            int[] iArr = new int[3];
            $EnumSwitchMapping$0 = iArr;
            iArr[VadUseKrisp.TemporarilyDisabled.ordinal()] = 1;
            NoiseProcessing.values();
            int[] iArr2 = new int[4];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[NoiseProcessing.CancellationTemporarilyDisabled.ordinal()] = 1;
        }
    }

    public /* synthetic */ StoreMediaSettings(StoreVoiceChannelSelected storeVoiceChannelSelected, StoreChannels storeChannels, StorePermissions storePermissions, VoiceConfigurationCache voiceConfigurationCache, AppSoundManager appSoundManager, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(storeVoiceChannelSelected, storeChannels, storePermissions, (i & 8) != 0 ? new VoiceConfigurationCache(SharedPreferencesProvider.INSTANCE.get()) : voiceConfigurationCache, (i & 16) != 0 ? AppSoundManager.Provider.INSTANCE.get() : appSoundManager);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void handleCanUseVad(boolean z2) {
        this.canUseVad = z2;
        updateForceMute();
    }

    private final void setVoiceConfiguration(VoiceConfiguration voiceConfiguration) {
        this.voiceConfiguration = voiceConfiguration;
        this.voiceConfigurationSubject.k.onNext(voiceConfiguration);
        this.voiceConfigurationCache.write(voiceConfiguration);
    }

    private final void updateForceMute() {
        this.forceSelfMuteReason = null;
        if (!this.canUseVad && this.voiceConfiguration.getInputMode() == MediaEngineConnection.InputMode.VOICE_ACTIVITY) {
            this.forceSelfMuteReason = SelfMuteFailure.CANNOT_USE_VAD;
        }
        if (this.forceSelfMuteReason != null) {
            setVoiceConfiguration(VoiceConfiguration.copy$default(this.voiceConfiguration, true, false, false, null, false, false, null, 0.0f, null, 0.0f, null, null, null, false, false, 32766, null));
        }
    }

    public final Observable<MediaEngineConnection.InputMode> getInputMode() {
        Observable<R> F = this.voiceConfigurationSubject.F(StoreMediaSettings$getInputMode$1.INSTANCE);
        m.checkNotNullExpressionValue(F, "voiceConfigurationSubjec…    .map { it.inputMode }");
        Observable<MediaEngineConnection.InputMode> q = ObservableExtensionsKt.computationLatest(F).q();
        m.checkNotNullExpressionValue(q, "voiceConfigurationSubjec…  .distinctUntilChanged()");
        return q;
    }

    public final synchronized Map<Long, Boolean> getMutedUsers() {
        return this.voiceConfiguration.getMutedUsers();
    }

    public final Observable<Map<Long, Boolean>> getUsersMuted() {
        Observable<R> F = this.voiceConfigurationSubject.F(StoreMediaSettings$getUsersMuted$1.INSTANCE);
        m.checkNotNullExpressionValue(F, "voiceConfigurationSubjec…   .map { it.mutedUsers }");
        Observable<Map<Long, Boolean>> q = ObservableExtensionsKt.computationLatest(F).q();
        m.checkNotNullExpressionValue(q, "voiceConfigurationSubjec…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Map<Long, Boolean>> getUsersOffScreen() {
        Observable<R> F = this.voiceConfigurationSubject.F(StoreMediaSettings$getUsersOffScreen$1.INSTANCE);
        m.checkNotNullExpressionValue(F, "voiceConfigurationSubjec…map { it.offScreenUsers }");
        Observable<Map<Long, Boolean>> q = ObservableExtensionsKt.computationLatest(F).q();
        m.checkNotNullExpressionValue(q, "voiceConfigurationSubjec…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Map<Long, Float>> getUsersVolume() {
        Observable<R> F = this.voiceConfigurationSubject.F(StoreMediaSettings$getUsersVolume$1.INSTANCE);
        m.checkNotNullExpressionValue(F, "voiceConfigurationSubjec… { it.userOutputVolumes }");
        Observable<Map<Long, Float>> q = ObservableExtensionsKt.computationLatest(F).q();
        m.checkNotNullExpressionValue(q, "voiceConfigurationSubjec…  .distinctUntilChanged()");
        return q;
    }

    public final synchronized boolean getVideoHardwareScalingBlocking() {
        return this.voiceConfiguration.getEnableVideoHardwareScaling();
    }

    public final Observable<VoiceConfiguration> getVoiceConfig() {
        Observable<VoiceConfiguration> q = ObservableExtensionsKt.computationLatest(this.voiceConfigurationSubject).q();
        m.checkNotNullExpressionValue(q, "voiceConfigurationSubjec…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<VoiceConfiguration> getVoiceConfiguration() {
        return this.voiceConfigurationSubject;
    }

    public final synchronized VoiceConfiguration getVoiceConfigurationBlocking() {
        return this.voiceConfiguration;
    }

    @StoreThread
    public final void handleVoiceChannelSelected(long j) {
        Channel findChannelByIdInternal$app_productionGoogleRelease = this.storeChannels.findChannelByIdInternal$app_productionGoogleRelease(j);
        if (findChannelByIdInternal$app_productionGoogleRelease != null && ChannelUtils.x(findChannelByIdInternal$app_productionGoogleRelease)) {
            setSelfDeafen(false);
        }
    }

    public final void init() {
        setVoiceConfiguration(this.voiceConfigurationCache.read());
        Observable<R> Y = this.storeVoiceChannelSelected.observeSelectedVoiceChannelId().Y(new b<Long, Observable<? extends Boolean>>() { // from class: com.discord.stores.StoreMediaSettings$init$1
            public final Observable<? extends Boolean> call(final Long l) {
                StoreChannels storeChannels;
                storeChannels = StoreMediaSettings.this.storeChannels;
                m.checkNotNullExpressionValue(l, ModelAuditLogEntry.CHANGE_KEY_ID);
                return (Observable<R>) storeChannels.observeChannel(l.longValue()).Y(new b<Channel, Observable<? extends Boolean>>() { // from class: com.discord.stores.StoreMediaSettings$init$1.1

                    /* compiled from: StoreMediaSettings.kt */
                    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\u0010\u0007\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n\u0018\u00010\u0000j\u0004\u0018\u0001`\u0001H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "Lcom/discord/api/permission/PermissionBit;", "it", "", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Long;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                    /* renamed from: com.discord.stores.StoreMediaSettings$init$1$1$1  reason: invalid class name and collision with other inner class name */
                    /* loaded from: classes.dex */
                    public static final class C02091<T, R> implements b<Long, Boolean> {
                        public static final C02091 INSTANCE = new C02091();

                        public final Boolean call(Long l) {
                            return Boolean.valueOf(PermissionUtils.can(Permission.USE_VAD, l));
                        }
                    }

                    public final Observable<? extends Boolean> call(Channel channel) {
                        StorePermissions storePermissions;
                        if (channel == null || ChannelUtils.x(channel) || ChannelUtils.z(channel)) {
                            return new j0.l.e.k(Boolean.TRUE);
                        }
                        storePermissions = StoreMediaSettings.this.storePermissions;
                        Long l2 = l;
                        m.checkNotNullExpressionValue(l2, ModelAuditLogEntry.CHANGE_KEY_ID);
                        return (Observable<R>) storePermissions.observePermissionsForChannel(l2.longValue()).F(C02091.INSTANCE);
                    }
                });
            }
        });
        m.checkNotNullExpressionValue(Y, "storeVoiceChannelSelecte…              }\n        }");
        ObservableExtensionsKt.appSubscribe(Y, StoreMediaSettings.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreMediaSettings$init$2(this));
    }

    public final Observable<Boolean> isSelfDeafened() {
        Observable<R> F = this.voiceConfigurationSubject.F(StoreMediaSettings$isSelfDeafened$1.INSTANCE);
        m.checkNotNullExpressionValue(F, "voiceConfigurationSubjec…map { it.isSelfDeafened }");
        Observable<Boolean> q = ObservableExtensionsKt.computationLatest(F).q();
        m.checkNotNullExpressionValue(q, "voiceConfigurationSubjec…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Boolean> isSelfMuted() {
        Observable<R> F = this.voiceConfigurationSubject.F(StoreMediaSettings$isSelfMuted$1.INSTANCE);
        m.checkNotNullExpressionValue(F, "voiceConfigurationSubjec….map { it.isSelfMuted() }");
        Observable<Boolean> q = ObservableExtensionsKt.computationLatest(F).q();
        m.checkNotNullExpressionValue(q, "voiceConfigurationSubjec…  .distinctUntilChanged()");
        return q;
    }

    public final synchronized void revertTemporaryDisableKrisp() {
        VadUseKrisp vadUseKrisp;
        NoiseProcessing noiseProcessing;
        if (this.voiceConfiguration.getVadUseKrisp().ordinal() != 2) {
            vadUseKrisp = this.voiceConfiguration.getVadUseKrisp();
        } else {
            vadUseKrisp = VadUseKrisp.Enabled;
        }
        VadUseKrisp vadUseKrisp2 = vadUseKrisp;
        if (this.voiceConfiguration.getNoiseProcessing().ordinal() != 3) {
            noiseProcessing = this.voiceConfiguration.getNoiseProcessing();
        } else {
            noiseProcessing = NoiseProcessing.Cancellation;
        }
        NoiseProcessing noiseProcessing2 = noiseProcessing;
        if (!(noiseProcessing2 == this.voiceConfiguration.getNoiseProcessing() && vadUseKrisp2 == this.voiceConfiguration.getVadUseKrisp())) {
            setVoiceConfiguration(VoiceConfiguration.copy$default(this.voiceConfiguration, false, false, false, vadUseKrisp2, false, false, noiseProcessing2, 0.0f, null, 0.0f, null, null, null, false, false, 32695, null));
        }
    }

    public final synchronized void setNoiseProcessing(NoiseProcessing noiseProcessing) {
        m.checkNotNullParameter(noiseProcessing, "noiseProcessing");
        if (noiseProcessing != this.voiceConfiguration.getNoiseProcessing()) {
            setVoiceConfiguration(VoiceConfiguration.copy$default(this.voiceConfiguration, false, false, false, null, false, false, noiseProcessing, 0.0f, null, 0.0f, null, null, null, false, false, 32703, null));
        }
    }

    public final synchronized void setOutputVolume(float f) {
        setVoiceConfiguration(VoiceConfiguration.copy$default(this.voiceConfiguration, false, false, false, null, false, false, null, 0.0f, null, f, null, null, null, false, false, 32255, null));
    }

    public final synchronized void setSelfDeafen(boolean z2) {
        AppSound appSound;
        if (this.voiceConfiguration.isSelfDeafened() != z2) {
            setVoiceConfiguration(VoiceConfiguration.copy$default(this.voiceConfiguration, false, z2, false, null, false, false, null, 0.0f, null, 0.0f, null, null, null, false, false, 32765, null));
            AppSoundManager appSoundManager = this.appSoundManager;
            if (z2) {
                appSound = AppSound.Companion.getSOUND_DEAFEN();
            } else {
                appSound = AppSound.Companion.getSOUND_UNDEAFEN();
            }
            appSoundManager.play(appSound);
        }
    }

    public final synchronized SelfMuteFailure setSelfMuted(boolean z2) {
        AppSound appSound;
        if (this.voiceConfiguration.isSelfMuted() == z2) {
            return null;
        }
        SelfMuteFailure selfMuteFailure = this.forceSelfMuteReason;
        if (selfMuteFailure != null) {
            return selfMuteFailure;
        }
        VoiceConfiguration voiceConfiguration = this.voiceConfiguration;
        setVoiceConfiguration(VoiceConfiguration.copy$default(voiceConfiguration, z2, voiceConfiguration.isSelfDeafened() && z2, false, null, false, false, null, 0.0f, null, 0.0f, null, null, null, false, false, 32764, null));
        AppSoundManager appSoundManager = this.appSoundManager;
        if (z2) {
            appSound = AppSound.Companion.getSOUND_MUTE();
        } else {
            appSound = AppSound.Companion.getSOUND_UNMUTE();
        }
        appSoundManager.play(appSound);
        return null;
    }

    public final synchronized void setSensitivity(float f) {
        setVoiceConfiguration(VoiceConfiguration.copy$default(this.voiceConfiguration, false, false, false, null, false, false, null, f, null, 0.0f, null, null, null, false, false, 32639, null));
    }

    public final synchronized void setUserOffScreen(long j, boolean z2) {
        VoiceConfiguration voiceConfiguration = this.voiceConfiguration;
        HashMap hashMap = new HashMap(this.voiceConfiguration.getOffScreenUsers());
        hashMap.put(Long.valueOf(j), Boolean.valueOf(z2));
        setVoiceConfiguration(VoiceConfiguration.copy$default(voiceConfiguration, false, false, false, null, false, false, null, 0.0f, null, 0.0f, null, null, hashMap, false, false, 28671, null));
    }

    public final synchronized void setUserOutputVolume(long j, float f) {
        VoiceConfiguration voiceConfiguration = this.voiceConfiguration;
        HashMap hashMap = new HashMap(this.voiceConfiguration.getUserOutputVolumes());
        hashMap.put(Long.valueOf(j), Float.valueOf(f));
        setVoiceConfiguration(VoiceConfiguration.copy$default(voiceConfiguration, false, false, false, null, false, false, null, 0.0f, null, 0.0f, null, hashMap, null, false, false, 30719, null));
    }

    public final synchronized void setVADUseKrisp(VadUseKrisp vadUseKrisp) {
        m.checkNotNullParameter(vadUseKrisp, "status");
        if (vadUseKrisp != this.voiceConfiguration.getVadUseKrisp()) {
            setVoiceConfiguration(VoiceConfiguration.copy$default(this.voiceConfiguration, false, false, false, vadUseKrisp, false, false, null, 0.0f, null, 0.0f, null, null, null, false, false, 32759, null));
        }
    }

    public final synchronized void setVoiceInputMode(MediaEngineConnection.InputMode inputMode) {
        m.checkNotNullParameter(inputMode, "inputMode");
        setVoiceConfiguration(VoiceConfiguration.copy$default(this.voiceConfiguration, false, false, false, null, false, false, null, 0.0f, inputMode, 0.0f, null, null, null, false, false, 32511, null));
        updateForceMute();
    }

    public final synchronized void toggleAutomaticGainControl() {
        VoiceConfiguration voiceConfiguration = this.voiceConfiguration;
        setVoiceConfiguration(VoiceConfiguration.copy$default(voiceConfiguration, false, false, false, null, !voiceConfiguration.getAutomaticGainControl(), false, null, 0.0f, null, 0.0f, null, null, null, false, false, 32751, null));
    }

    public final synchronized void toggleAutomaticVAD() {
        VoiceConfiguration voiceConfiguration = this.voiceConfiguration;
        setVoiceConfiguration(VoiceConfiguration.copy$default(voiceConfiguration, false, false, !voiceConfiguration.getAutomaticVad(), null, false, false, null, 0.0f, null, 0.0f, null, null, null, false, false, 32763, null));
    }

    public final synchronized void toggleEchoCancellation() {
        VoiceConfiguration voiceConfiguration = this.voiceConfiguration;
        setVoiceConfiguration(VoiceConfiguration.copy$default(voiceConfiguration, false, false, false, null, false, !voiceConfiguration.getEchoCancellation(), null, 0.0f, null, 0.0f, null, null, null, false, false, 32735, null));
    }

    public final synchronized void toggleEnableVideoHardwareScaling() {
        VoiceConfiguration voiceConfiguration = this.voiceConfiguration;
        setVoiceConfiguration(VoiceConfiguration.copy$default(voiceConfiguration, false, false, false, null, false, false, null, 0.0f, null, 0.0f, null, null, null, !voiceConfiguration.getEnableVideoHardwareScaling(), false, 24575, null));
    }

    public final synchronized void toggleNoiseCancellation() {
        NoiseProcessing noiseProcessing = this.voiceConfiguration.getNoiseProcessing();
        NoiseProcessing noiseProcessing2 = NoiseProcessing.Cancellation;
        if (noiseProcessing == noiseProcessing2) {
            setVoiceConfiguration(NoiseProcessing.Suppression);
        } else {
            setVoiceConfiguration(noiseProcessing2);
        }
    }

    public final synchronized void toggleNoiseSuppression() {
        NoiseProcessing noiseProcessing = this.voiceConfiguration.getNoiseProcessing();
        NoiseProcessing noiseProcessing2 = NoiseProcessing.Suppression;
        if (noiseProcessing == noiseProcessing2) {
            setVoiceConfiguration(NoiseProcessing.None);
        } else {
            setVoiceConfiguration(noiseProcessing2);
        }
    }

    public final synchronized void toggleSelfDeafened() {
        setSelfDeafen(!this.voiceConfiguration.isSelfDeafened());
    }

    public final synchronized SelfMuteFailure toggleSelfMuted() {
        return setSelfMuted(!this.voiceConfiguration.isSelfMuted());
    }

    public final synchronized void toggleUserMuted(long j) {
        VoiceConfiguration voiceConfiguration = this.voiceConfiguration;
        HashMap hashMap = new HashMap(this.voiceConfiguration.getMutedUsers());
        Boolean bool = (Boolean) hashMap.get(Long.valueOf(j));
        if (bool == null) {
            bool = Boolean.FALSE;
        }
        m.checkNotNullExpressionValue(bool, "get(userId) ?: false");
        hashMap.put(Long.valueOf(j), Boolean.valueOf(!bool.booleanValue()));
        setVoiceConfiguration(VoiceConfiguration.copy$default(voiceConfiguration, false, false, false, null, false, false, null, 0.0f, null, 0.0f, hashMap, null, null, false, false, 31743, null));
    }

    public final synchronized void toggleVADUseKrisp() {
        VadUseKrisp vadUseKrisp = this.voiceConfiguration.getVadUseKrisp();
        VadUseKrisp vadUseKrisp2 = VadUseKrisp.Enabled;
        if (vadUseKrisp == vadUseKrisp2) {
            vadUseKrisp2 = VadUseKrisp.Disabled;
        }
        setVoiceConfiguration(VoiceConfiguration.copy$default(this.voiceConfiguration, false, false, false, vadUseKrisp2, false, false, null, 0.0f, null, 0.0f, null, null, null, false, false, 32759, null));
    }

    public final synchronized void updateVoiceParticipantsHidden(boolean z2) {
        if (this.voiceConfiguration.getVoiceParticipantsHidden() != z2) {
            setVoiceConfiguration(VoiceConfiguration.copy$default(this.voiceConfiguration, false, false, false, null, false, false, null, 0.0f, null, 0.0f, null, null, null, false, z2, 16383, null));
        }
    }

    public StoreMediaSettings(StoreVoiceChannelSelected storeVoiceChannelSelected, StoreChannels storeChannels, StorePermissions storePermissions, VoiceConfigurationCache voiceConfigurationCache, AppSoundManager appSoundManager) {
        m.checkNotNullParameter(storeVoiceChannelSelected, "storeVoiceChannelSelected");
        m.checkNotNullParameter(storeChannels, "storeChannels");
        m.checkNotNullParameter(storePermissions, "storePermissions");
        m.checkNotNullParameter(voiceConfigurationCache, "voiceConfigurationCache");
        m.checkNotNullParameter(appSoundManager, "appSoundManager");
        this.storeVoiceChannelSelected = storeVoiceChannelSelected;
        this.storeChannels = storeChannels;
        this.storePermissions = storePermissions;
        this.voiceConfigurationCache = voiceConfigurationCache;
        this.appSoundManager = appSoundManager;
        VoiceConfiguration default_voice_config = VoiceConfiguration.Companion.getDEFAULT_VOICE_CONFIG();
        this.voiceConfiguration = default_voice_config;
        this.voiceConfigurationSubject = new SerializedSubject<>(BehaviorSubject.l0(default_voice_config));
        this.canUseVad = true;
    }

    public final synchronized void setVoiceConfiguration(NoiseProcessing noiseProcessing) {
        m.checkNotNullParameter(noiseProcessing, "noiseProcessing");
        if (noiseProcessing != this.voiceConfiguration.getNoiseProcessing()) {
            setVoiceConfiguration(VoiceConfiguration.copy$default(this.voiceConfiguration, false, false, false, null, false, false, noiseProcessing, 0.0f, null, 0.0f, null, null, null, false, false, 32703, null));
        }
    }
}
