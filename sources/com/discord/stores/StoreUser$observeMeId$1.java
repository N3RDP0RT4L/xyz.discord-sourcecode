package com.discord.stores;

import com.discord.models.user.MeUser;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreUser.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00060\u0000j\u0002`\u0001H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"", "Lcom/discord/primitives/UserId;", "invoke", "()J", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUser$observeMeId$1 extends o implements Function0<Long> {
    public final /* synthetic */ StoreUser this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreUser$observeMeId$1(StoreUser storeUser) {
        super(0);
        this.this$0 = storeUser;
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [long, java.lang.Long] */
    @Override // kotlin.jvm.functions.Function0
    public final Long invoke() {
        MeUser meUser;
        meUser = this.this$0.f2785me;
        return meUser.getId();
    }
}
