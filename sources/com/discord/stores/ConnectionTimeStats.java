package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.voice.state.VoiceState;
import com.discord.app.AppLog;
import com.discord.gateway.GatewaySocket;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelPayload;
import com.discord.stores.ConnectionTimeStats;
import com.discord.stores.StoreMediaEngine;
import com.discord.stores.StoreRtcConnection;
import com.discord.stores.StoreStreamRtcConnection;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.time.Clock;
import d0.o;
import d0.t.g0;
import d0.t.h0;
import d0.t.u;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ConnectionTimeStats.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0084\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0002DEB\u000f\u0012\u0006\u0010A\u001a\u00020@¢\u0006\u0004\bB\u0010CJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0019\u0010\t\u001a\u00020\u00042\b\b\u0002\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0015\u0010\r\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\r\u0010\u000eJ\u0015\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b\u0011\u0010\u0012J\u0019\u0010\u0016\u001a\u00020\u00042\n\u0010\u0015\u001a\u00060\u0013j\u0002`\u0014¢\u0006\u0004\b\u0016\u0010\u0017J7\u0010\u001f\u001a\u00020\u00042\u0006\u0010\u0019\u001a\u00020\u00182\b\u0010\u001b\u001a\u0004\u0018\u00010\u001a2\u0006\u0010\u001c\u001a\u00020\u001a2\u0006\u0010\u001d\u001a\u00020\u001a2\u0006\u0010\u001e\u001a\u00020\u001a¢\u0006\u0004\b\u001f\u0010 J)\u0010#\u001a\u00020\u00042\n\u0010\u0019\u001a\u00060\u0018j\u0002`!2\u000e\u0010\u001b\u001a\n\u0018\u00010\u001aj\u0004\u0018\u0001`\"¢\u0006\u0004\b#\u0010$J\u0015\u0010'\u001a\u00020\u00042\u0006\u0010&\u001a\u00020%¢\u0006\u0004\b'\u0010(J\u0015\u0010'\u001a\u00020\u00042\u0006\u0010*\u001a\u00020)¢\u0006\u0004\b'\u0010+J\u0015\u0010'\u001a\u00020\u00042\u0006\u0010-\u001a\u00020,¢\u0006\u0004\b'\u0010.J\u0015\u0010'\u001a\u00020\u00042\u0006\u00100\u001a\u00020/¢\u0006\u0004\b'\u00101R\u0016\u00103\u001a\u0002028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b3\u00104R\u0016\u00105\u001a\u0002028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b5\u00104R\u0016\u00106\u001a\u0002028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b6\u00104R\u0016\u00107\u001a\u0002028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b7\u00104R\u0016\u00108\u001a\u0002028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b8\u00104R\u0016\u00109\u001a\u0002028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b9\u00104R\u0016\u0010:\u001a\u0002028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b:\u00104R\u0016\u0010;\u001a\u0002028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b;\u00104R\u0016\u0010<\u001a\u0002028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b<\u00104R\u0016\u0010=\u001a\u0002028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b=\u00104R\u001e\u0010>\u001a\n\u0018\u00010\u0018j\u0004\u0018\u0001`!8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b>\u0010?¨\u0006F"}, d2 = {"Lcom/discord/stores/ConnectionTimeStats;", "", "Lcom/discord/stores/ConnectionTimeStats$StatType;", "statType", "", "sendAnalyticsEvent", "(Lcom/discord/stores/ConnectionTimeStats$StatType;)V", "", "onlyCallStats", "clear", "(Z)V", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "Lcom/discord/api/voice/state/VoiceState;", "voiceState", "handleVoiceStateUpdate", "(Lcom/discord/api/voice/state/VoiceState;)V", "", "Lcom/discord/primitives/StreamKey;", "streamKey", "handleStreamWatch", "(Ljava/lang/String;)V", "", "userId", "", "streamId", "audioSsrc", "videoSsrc", "rtxSsrc", "handleVideoStreamUpdate", "(JLjava/lang/Integer;III)V", "Lcom/discord/primitives/UserId;", "Lcom/discord/primitives/StreamId;", "handleApplicationStreamUpdate", "(JLjava/lang/Integer;)V", "Lcom/discord/gateway/GatewaySocket;", "socket", "addListener", "(Lcom/discord/gateway/GatewaySocket;)V", "Lcom/discord/stores/StoreRtcConnection;", "rtcConnection", "(Lcom/discord/stores/StoreRtcConnection;)V", "Lcom/discord/stores/StoreStreamRtcConnection;", "streamRtcConnection", "(Lcom/discord/stores/StoreStreamRtcConnection;)V", "Lcom/discord/stores/StoreMediaEngine;", "storeMediaEngine", "(Lcom/discord/stores/StoreMediaEngine;)V", "Lcom/discord/stores/ConnectionTimeStats$Stat;", "streamFirstFrame", "Lcom/discord/stores/ConnectionTimeStats$Stat;", "videoFirstFrame", "mediaEngineConnection", "connectionStreamFirstFrame", "voiceConnection", "connectionVideoFirstFrame", "gatewayConnection", "streamRequested", "streamConnection", "gatewayHello", "myUserId", "Ljava/lang/Long;", "Lcom/discord/utilities/time/Clock;", "clock", HookHelper.constructorName, "(Lcom/discord/utilities/time/Clock;)V", "Stat", "StatType", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ConnectionTimeStats {
    private final Stat connectionStreamFirstFrame;
    private final Stat connectionVideoFirstFrame;
    private final Stat gatewayConnection;
    private final Stat gatewayHello;
    private final Stat mediaEngineConnection;
    private Long myUserId;
    private final Stat streamConnection;
    private final Stat streamFirstFrame;
    private final Stat streamRequested;
    private final Stat videoFirstFrame;
    private final Stat voiceConnection;

    /* compiled from: ConnectionTimeStats.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0018\u001a\u00020\u0017\u0012\u0006\u0010\u0012\u001a\u00020\u0011\u0012\u0006\u0010\u000f\u001a\u00020\u000e¢\u0006\u0004\b\u001a\u0010\u001bJ\u0017\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\u0007\u001a\u00020\u00042\b\b\u0002\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\u0006J\u0015\u0010\n\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t¢\u0006\u0004\b\n\u0010\u000bJ\r\u0010\f\u001a\u00020\u0004¢\u0006\u0004\b\f\u0010\rR\u0016\u0010\u000f\u001a\u00020\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0012\u001a\u00020\u00118\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013R\u001e\u0010\u0014\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u001e\u0010\u0016\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0016\u0010\u0015R\u0016\u0010\u0018\u001a\u00020\u00178\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019¨\u0006\u001c"}, d2 = {"Lcom/discord/stores/ConnectionTimeStats$Stat;", "", "", "ignoreSubsequentCalls", "", "start", "(Z)V", "end", "", "Lcom/discord/utilities/time/ClockMilliseconds;", "elapsed", "()Ljava/lang/Long;", "clear", "()V", "Lcom/discord/stores/ConnectionTimeStats$StatType;", "type", "Lcom/discord/stores/ConnectionTimeStats$StatType;", "Lcom/discord/stores/ConnectionTimeStats;", "owner", "Lcom/discord/stores/ConnectionTimeStats;", "startTime", "Ljava/lang/Long;", "endTime", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", HookHelper.constructorName, "(Lcom/discord/utilities/time/Clock;Lcom/discord/stores/ConnectionTimeStats;Lcom/discord/stores/ConnectionTimeStats$StatType;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Stat {
        private final Clock clock;
        private Long endTime;
        private final ConnectionTimeStats owner;
        private Long startTime;
        private final StatType type;

        public Stat(Clock clock, ConnectionTimeStats connectionTimeStats, StatType statType) {
            m.checkNotNullParameter(clock, "clock");
            m.checkNotNullParameter(connectionTimeStats, "owner");
            m.checkNotNullParameter(statType, "type");
            this.clock = clock;
            this.owner = connectionTimeStats;
            this.type = statType;
        }

        public static /* synthetic */ void end$default(Stat stat, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                z2 = false;
            }
            stat.end(z2);
        }

        public static /* synthetic */ void start$default(Stat stat, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                z2 = false;
            }
            stat.start(z2);
        }

        public final void clear() {
            this.startTime = null;
            this.endTime = null;
        }

        public final Long elapsed() {
            Long l = this.startTime;
            Long l2 = this.endTime;
            if (l == null || l2 == null) {
                return null;
            }
            return Long.valueOf(l2.longValue() - l.longValue());
        }

        public final void end(boolean z2) {
            if (this.endTime == null) {
                Long l = this.startTime;
                long currentTimeMillis = this.clock.currentTimeMillis();
                if (l == null) {
                    AppLog appLog = AppLog.g;
                    StringBuilder R = a.R("ConnectionTimeStats: \"");
                    R.append(this.type);
                    R.append("\" ended without starting!");
                    Logger.w$default(appLog, R.toString(), null, 2, null);
                    return;
                }
                long longValue = currentTimeMillis - l.longValue();
                if (longValue < 0) {
                    AppLog appLog2 = AppLog.g;
                    StringBuilder R2 = a.R("ConnectionTimeStats: \"");
                    R2.append(this.type);
                    R2.append("\" has a negative time!");
                    R2.append(u.joinToString$default(h0.mapOf(o.to(this.type.toString(), "type"), o.to(String.valueOf(longValue), "elapsedMs"), o.to(String.valueOf(l.longValue()), "start"), o.to(String.valueOf(currentTimeMillis), "end")).entrySet(), "\n\t", null, null, 0, null, null, 62, null));
                    Logger.w$default(appLog2, R2.toString(), null, 2, null);
                    return;
                }
                this.endTime = Long.valueOf(currentTimeMillis);
                StringBuilder R3 = a.R("ConnectionTimeStats: \"");
                R3.append(this.type);
                R3.append("\" took ");
                R3.append(longValue);
                R3.append(" ms (");
                R3.append(l);
                R3.append(" to ");
                R3.append(currentTimeMillis);
                R3.append(')');
                AppLog.i(R3.toString());
                this.owner.sendAnalyticsEvent(this.type);
            } else if (!z2) {
                AppLog appLog3 = AppLog.g;
                StringBuilder R4 = a.R("ConnectionTimeStats: \"");
                R4.append(this.type);
                R4.append("\" attempting to end while endTime is set!");
                Logger.w$default(appLog3, R4.toString(), null, 2, null);
            }
        }

        public final void start(boolean z2) {
            if (this.startTime != null) {
                if (!z2) {
                    AppLog appLog = AppLog.g;
                    StringBuilder R = a.R("ConnectionTimeStats: \"");
                    R.append(this.type);
                    R.append("\" attempting to re-start without reset!");
                    Logger.w$default(appLog, R.toString(), null, 2, null);
                }
            } else if (this.endTime != null) {
                AppLog appLog2 = AppLog.g;
                StringBuilder R2 = a.R("ConnectionTimeStats: \"");
                R2.append(this.type);
                R2.append("\" attempting to re-start while endTime is set!");
                Logger.w$default(appLog2, R2.toString(), null, 2, null);
            } else {
                long currentTimeMillis = this.clock.currentTimeMillis();
                this.startTime = Long.valueOf(currentTimeMillis);
                StringBuilder R3 = a.R("ConnectionTimeStats: \"");
                R3.append(this.type);
                R3.append("\" started @ ");
                R3.append(currentTimeMillis);
                AppLog.i(R3.toString());
            }
        }
    }

    /* compiled from: ConnectionTimeStats.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\r\b\u0082\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\r¨\u0006\u000e"}, d2 = {"Lcom/discord/stores/ConnectionTimeStats$StatType;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "GatewayConnection", "GatewayHello", "VoiceConnection", "StreamRequested", "StreamConnection", "MediaEngineConnection", "StreamFirstFrame", "VideoFirstFrame", "ConnectionStreamFirstFrame", "ConnectionVideoFirstFrame", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum StatType {
        GatewayConnection,
        GatewayHello,
        VoiceConnection,
        StreamRequested,
        StreamConnection,
        MediaEngineConnection,
        StreamFirstFrame,
        VideoFirstFrame,
        ConnectionStreamFirstFrame,
        ConnectionVideoFirstFrame
    }

    public ConnectionTimeStats(Clock clock) {
        m.checkNotNullParameter(clock, "clock");
        StatType statType = StatType.VideoFirstFrame;
        if (!m.areEqual(statType.toString(), "VideoFirstFrame")) {
            Logger.e$default(AppLog.g, "ConnectionTimeStats.StatType has been renamed!", null, g0.mapOf(o.to(ModelAuditLogEntry.CHANGE_KEY_NAME, statType.toString())), 2, null);
        }
        this.gatewayConnection = new Stat(clock, this, StatType.GatewayConnection);
        this.gatewayHello = new Stat(clock, this, StatType.GatewayHello);
        this.voiceConnection = new Stat(clock, this, StatType.VoiceConnection);
        this.streamRequested = new Stat(clock, this, StatType.StreamRequested);
        this.streamConnection = new Stat(clock, this, StatType.StreamConnection);
        this.streamFirstFrame = new Stat(clock, this, StatType.StreamFirstFrame);
        this.videoFirstFrame = new Stat(clock, this, statType);
        this.mediaEngineConnection = new Stat(clock, this, StatType.MediaEngineConnection);
        this.connectionVideoFirstFrame = new Stat(clock, this, StatType.ConnectionVideoFirstFrame);
        this.connectionStreamFirstFrame = new Stat(clock, this, StatType.ConnectionStreamFirstFrame);
    }

    private final void clear(boolean z2) {
        if (!z2) {
            this.gatewayConnection.clear();
            this.gatewayHello.clear();
        }
        this.voiceConnection.clear();
        this.streamRequested.clear();
        this.streamConnection.clear();
        this.streamFirstFrame.clear();
        this.videoFirstFrame.clear();
        this.mediaEngineConnection.clear();
        this.connectionVideoFirstFrame.clear();
        this.connectionStreamFirstFrame.clear();
    }

    public static /* synthetic */ void clear$default(ConnectionTimeStats connectionTimeStats, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = false;
        }
        connectionTimeStats.clear(z2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void sendAnalyticsEvent(StatType statType) {
        AnalyticsTracker.INSTANCE.videoEventTimes(statType.toString(), this.gatewayConnection.elapsed(), this.gatewayHello.elapsed(), this.voiceConnection.elapsed(), this.streamRequested.elapsed(), this.streamConnection.elapsed(), this.streamFirstFrame.elapsed(), this.videoFirstFrame.elapsed(), this.mediaEngineConnection.elapsed(), this.connectionVideoFirstFrame.elapsed(), this.connectionStreamFirstFrame.elapsed());
    }

    public final void addListener(GatewaySocket gatewaySocket) {
        m.checkNotNullParameter(gatewaySocket, "socket");
        gatewaySocket.getListeners().add(new GatewaySocket.DefaultListener() { // from class: com.discord.stores.ConnectionTimeStats$addListener$1
            @Override // com.discord.gateway.GatewaySocket.DefaultListener, com.discord.gateway.GatewaySocket.Listener
            public void onConnected(GatewaySocket gatewaySocket2) {
                ConnectionTimeStats.Stat stat;
                ConnectionTimeStats.Stat stat2;
                m.checkNotNullParameter(gatewaySocket2, "gatewaySocket");
                stat = ConnectionTimeStats.this.gatewayConnection;
                ConnectionTimeStats.Stat.end$default(stat, false, 1, null);
                stat2 = ConnectionTimeStats.this.gatewayHello;
                ConnectionTimeStats.Stat.start$default(stat2, false, 1, null);
            }

            @Override // com.discord.gateway.GatewaySocket.DefaultListener, com.discord.gateway.GatewaySocket.Listener
            public void onConnecting(GatewaySocket gatewaySocket2) {
                ConnectionTimeStats.Stat stat;
                m.checkNotNullParameter(gatewaySocket2, "gatewaySocket");
                stat = ConnectionTimeStats.this.gatewayConnection;
                ConnectionTimeStats.Stat.start$default(stat, false, 1, null);
            }

            @Override // com.discord.gateway.GatewaySocket.DefaultListener, com.discord.gateway.GatewaySocket.Listener
            public void onDisconnected(GatewaySocket gatewaySocket2) {
                ConnectionTimeStats.Stat stat;
                ConnectionTimeStats.Stat stat2;
                m.checkNotNullParameter(gatewaySocket2, "gatewaySocket");
                stat = ConnectionTimeStats.this.gatewayConnection;
                stat.clear();
                stat2 = ConnectionTimeStats.this.gatewayHello;
                stat2.clear();
            }

            @Override // com.discord.gateway.GatewaySocket.DefaultListener, com.discord.gateway.GatewaySocket.Listener
            public void onHello(GatewaySocket gatewaySocket2) {
                ConnectionTimeStats.Stat stat;
                m.checkNotNullParameter(gatewaySocket2, "gatewaySocket");
                stat = ConnectionTimeStats.this.gatewayHello;
                ConnectionTimeStats.Stat.end$default(stat, false, 1, null);
            }
        });
    }

    public final void handleApplicationStreamUpdate(long j, Integer num) {
        this.streamFirstFrame.start(true);
    }

    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        clear$default(this, false, 1, null);
        this.myUserId = Long.valueOf(modelPayload.getMe().i());
    }

    public final void handleStreamWatch(String str) {
        m.checkNotNullParameter(str, "streamKey");
        Stat.start$default(this.streamRequested, false, 1, null);
    }

    public final void handleVideoStreamUpdate(long j, Integer num, int i, int i2, int i3) {
        if (i2 != 0) {
            this.videoFirstFrame.start(true);
        }
    }

    public final void handleVoiceStateUpdate(VoiceState voiceState) {
        m.checkNotNullParameter(voiceState, "voiceState");
        long m = voiceState.m();
        Long l = this.myUserId;
        if (l != null && m == l.longValue() && voiceState.a() == null) {
            clear(true);
        }
    }

    public final void addListener(StoreRtcConnection storeRtcConnection) {
        m.checkNotNullParameter(storeRtcConnection, "rtcConnection");
        storeRtcConnection.getListeners().add(new StoreRtcConnection.DefaultListener() { // from class: com.discord.stores.ConnectionTimeStats$addListener$2
            @Override // com.discord.stores.StoreRtcConnection.DefaultListener, com.discord.stores.StoreRtcConnection.Listener
            public void onConnected() {
                ConnectionTimeStats.Stat stat;
                stat = ConnectionTimeStats.this.voiceConnection;
                ConnectionTimeStats.Stat.end$default(stat, false, 1, null);
            }

            @Override // com.discord.stores.StoreRtcConnection.DefaultListener, com.discord.stores.StoreRtcConnection.Listener
            public void onConnecting() {
                ConnectionTimeStats.Stat stat;
                ConnectionTimeStats.Stat stat2;
                ConnectionTimeStats.Stat stat3;
                stat = ConnectionTimeStats.this.voiceConnection;
                ConnectionTimeStats.Stat.start$default(stat, false, 1, null);
                stat2 = ConnectionTimeStats.this.connectionVideoFirstFrame;
                ConnectionTimeStats.Stat.start$default(stat2, false, 1, null);
                stat3 = ConnectionTimeStats.this.connectionStreamFirstFrame;
                ConnectionTimeStats.Stat.start$default(stat3, false, 1, null);
            }

            @Override // com.discord.stores.StoreRtcConnection.DefaultListener, com.discord.stores.StoreRtcConnection.Listener
            public void onFirstFrameReceived(long j) {
                ConnectionTimeStats.Stat stat;
                ConnectionTimeStats.Stat stat2;
                stat = ConnectionTimeStats.this.videoFirstFrame;
                ConnectionTimeStats.Stat.end$default(stat, false, 1, null);
                stat2 = ConnectionTimeStats.this.connectionVideoFirstFrame;
                ConnectionTimeStats.Stat.end$default(stat2, false, 1, null);
            }
        });
    }

    public final void addListener(StoreStreamRtcConnection storeStreamRtcConnection) {
        m.checkNotNullParameter(storeStreamRtcConnection, "streamRtcConnection");
        storeStreamRtcConnection.getListeners().add(new StoreStreamRtcConnection.DefaultListener() { // from class: com.discord.stores.ConnectionTimeStats$addListener$3
            @Override // com.discord.stores.StoreStreamRtcConnection.DefaultListener, com.discord.stores.StoreStreamRtcConnection.Listener
            public void onConnected() {
                ConnectionTimeStats.Stat stat;
                ConnectionTimeStats.Stat stat2;
                stat = ConnectionTimeStats.this.streamConnection;
                ConnectionTimeStats.Stat.end$default(stat, false, 1, null);
                stat2 = ConnectionTimeStats.this.streamFirstFrame;
                stat2.start(true);
            }

            @Override // com.discord.stores.StoreStreamRtcConnection.DefaultListener, com.discord.stores.StoreStreamRtcConnection.Listener
            public void onConnecting() {
                ConnectionTimeStats.Stat stat;
                ConnectionTimeStats.Stat stat2;
                stat = ConnectionTimeStats.this.streamRequested;
                ConnectionTimeStats.Stat.end$default(stat, false, 1, null);
                stat2 = ConnectionTimeStats.this.streamConnection;
                ConnectionTimeStats.Stat.start$default(stat2, false, 1, null);
            }

            @Override // com.discord.stores.StoreStreamRtcConnection.DefaultListener, com.discord.stores.StoreStreamRtcConnection.Listener
            public void onFirstFrameReceived(long j) {
                ConnectionTimeStats.Stat stat;
                ConnectionTimeStats.Stat stat2;
                stat = ConnectionTimeStats.this.streamFirstFrame;
                ConnectionTimeStats.Stat.end$default(stat, false, 1, null);
                stat2 = ConnectionTimeStats.this.connectionStreamFirstFrame;
                ConnectionTimeStats.Stat.end$default(stat2, false, 1, null);
            }
        });
    }

    public final void addListener(StoreMediaEngine storeMediaEngine) {
        m.checkNotNullParameter(storeMediaEngine, "storeMediaEngine");
        storeMediaEngine.getListeners().add(new StoreMediaEngine.DefaultListener() { // from class: com.discord.stores.ConnectionTimeStats$addListener$4
            @Override // com.discord.stores.StoreMediaEngine.DefaultListener, com.discord.stores.StoreMediaEngine.Listener
            public void onConnected() {
                ConnectionTimeStats.Stat stat;
                stat = ConnectionTimeStats.this.mediaEngineConnection;
                stat.end(true);
            }

            @Override // com.discord.stores.StoreMediaEngine.DefaultListener, com.discord.stores.StoreMediaEngine.Listener
            public void onConnecting() {
                ConnectionTimeStats.Stat stat;
                stat = ConnectionTimeStats.this.mediaEngineConnection;
                ConnectionTimeStats.Stat.start$default(stat, false, 1, null);
            }
        });
    }
}
