package com.discord.stores;

import com.discord.stores.StoreGuildSubscriptions;
import com.discord.utilities.lazy.subscriptions.GuildSubscriptionsManager;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.ranges.IntRange;
/* compiled from: StoreGuildSubscriptions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildSubscriptions$subscribeChannelRange$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ IntRange $range;
    public final /* synthetic */ StoreGuildSubscriptions this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGuildSubscriptions$subscribeChannelRange$1(StoreGuildSubscriptions storeGuildSubscriptions, IntRange intRange, long j, long j2) {
        super(0);
        this.this$0 = storeGuildSubscriptions;
        this.$range = intRange;
        this.$guildId = j;
        this.$channelId = j2;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        GuildSubscriptionsManager guildSubscriptionsManager;
        List<IntRange> computeRanges$default = StoreGuildSubscriptions.RangeComputer.computeRanges$default(StoreGuildSubscriptions.RangeComputer.INSTANCE, this.$range, 0, 2, null);
        guildSubscriptionsManager = this.this$0.subscriptionsManager;
        guildSubscriptionsManager.subscribeChannel(this.$guildId, this.$channelId, computeRanges$default);
        this.this$0.markChanged();
    }
}
