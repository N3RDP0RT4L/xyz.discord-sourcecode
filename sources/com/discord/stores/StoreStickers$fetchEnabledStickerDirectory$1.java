package com.discord.stores;

import com.discord.models.sticker.dto.ModelStickerStoreDirectory;
import com.discord.utilities.time.Clock;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreStickers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/sticker/dto/ModelStickerStoreDirectory;", "directory", "", "invoke", "(Lcom/discord/models/sticker/dto/ModelStickerStoreDirectory;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStickers$fetchEnabledStickerDirectory$1 extends o implements Function1<ModelStickerStoreDirectory, Unit> {
    public final /* synthetic */ StoreStickers this$0;

    /* compiled from: StoreStickers.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreStickers$fetchEnabledStickerDirectory$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ ModelStickerStoreDirectory $directory;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(ModelStickerStoreDirectory modelStickerStoreDirectory) {
            super(0);
            this.$directory = modelStickerStoreDirectory;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            Clock clock;
            StoreStickers storeStickers = StoreStickers$fetchEnabledStickerDirectory$1.this.this$0;
            clock = storeStickers.clock;
            storeStickers.lastFetchedEnabledPacks = clock.currentTimeMillis();
            StoreStickers$fetchEnabledStickerDirectory$1.this.this$0.handleNewEnabledStickerDirectory(this.$directory.getStickerPacks());
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreStickers$fetchEnabledStickerDirectory$1(StoreStickers storeStickers) {
        super(1);
        this.this$0 = storeStickers;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ModelStickerStoreDirectory modelStickerStoreDirectory) {
        invoke2(modelStickerStoreDirectory);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ModelStickerStoreDirectory modelStickerStoreDirectory) {
        Dispatcher dispatcher;
        m.checkNotNullParameter(modelStickerStoreDirectory, "directory");
        dispatcher = this.this$0.dispatcher;
        dispatcher.schedule(new AnonymousClass1(modelStickerStoreDirectory));
    }
}
