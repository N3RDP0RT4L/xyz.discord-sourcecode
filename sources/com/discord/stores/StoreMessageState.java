package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.message.Message;
import com.discord.models.domain.ModelMessageDelete;
import d0.o;
import d0.t.h0;
import d0.t.n0;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.BehaviorSubject;
/* compiled from: StoreMessageState.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u00010B\u000f\u0012\u0006\u0010,\u001a\u00020+¢\u0006\u0004\b.\u0010/J/\u0010\t\u001a\u00020\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u0005H\u0003¢\u0006\u0004\b\t\u0010\nJ!\u0010\r\u001a\u00020\b2\u0010\u0010\f\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\u000bH\u0003¢\u0006\u0004\b\r\u0010\u000eJ#\u0010\u0011\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00060\u00100\u000f¢\u0006\u0004\b\u0011\u0010\u0012J%\u0010\u0016\u001a\u00020\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0015\u001a\u00060\u0013j\u0002`\u0014¢\u0006\u0004\b\u0016\u0010\u0017J%\u0010\u0019\u001a\u00020\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0018\u001a\u00060\u0013j\u0002`\u0014¢\u0006\u0004\b\u0019\u0010\u0017J-\u0010\u001c\u001a\u00020\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0018\u001a\u00060\u0013j\u0002`\u00142\u0006\u0010\u001b\u001a\u00020\u001a¢\u0006\u0004\b\u001c\u0010\u001dJ\u000f\u0010\u001e\u001a\u00020\bH\u0007¢\u0006\u0004\b\u001e\u0010\u001fJ\u0017\u0010\"\u001a\u00020\b2\u0006\u0010!\u001a\u00020 H\u0007¢\u0006\u0004\b\"\u0010#J\u0017\u0010&\u001a\u00020\b2\u0006\u0010%\u001a\u00020$H\u0007¢\u0006\u0004\b&\u0010'R,\u0010)\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00060\u00100(8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b)\u0010*R\u0016\u0010,\u001a\u00020+8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b,\u0010-¨\u00061"}, d2 = {"Lcom/discord/stores/StoreMessageState;", "", "", "Lcom/discord/primitives/MessageId;", "messageId", "Lkotlin/Function1;", "Lcom/discord/stores/StoreMessageState$State;", "updateFunction", "", "updateState", "(JLkotlin/jvm/functions/Function1;)V", "", "messagesList", "resetState", "(Ljava/util/List;)V", "Lrx/Observable;", "", "getMessageState", "()Lrx/Observable;", "", "Lcom/discord/primitives/Index;", "spoilerIndex", "revealSpoiler", "(JI)V", "spoilerEmbedIndex", "revealSpoilerEmbed", "", "key", "revealSpoilerEmbedData", "(JILjava/lang/String;)V", "handleChannelSelected", "()V", "Lcom/discord/api/message/Message;", "message", "handleMessageUpdate", "(Lcom/discord/api/message/Message;)V", "Lcom/discord/models/domain/ModelMessageDelete;", "messageDelete", "handleMessageDelete", "(Lcom/discord/models/domain/ModelMessageDelete;)V", "Lrx/subjects/BehaviorSubject;", "messageStateSubject", "Lrx/subjects/BehaviorSubject;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;)V", "State", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessageState {
    private final Dispatcher dispatcher;
    private final BehaviorSubject<Map<Long, State>> messageStateSubject;

    /* compiled from: StoreMessageState.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\"\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B?\u0012\u0012\b\u0002\u0010\f\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002\u0012\"\b\u0002\u0010\r\u001a\u001c\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u000e\u0012\f\u0012\u0004\u0012\u00020\b0\u0002j\u0002`\t0\u0007¢\u0006\u0004\b\u001c\u0010\u001dJ\u001a\u0010\u0005\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0006J*\u0010\n\u001a\u001c\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u000e\u0012\f\u0012\u0004\u0012\u00020\b0\u0002j\u0002`\t0\u0007HÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJH\u0010\u000e\u001a\u00020\u00002\u0012\b\u0002\u0010\f\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00022\"\b\u0002\u0010\r\u001a\u001c\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u000e\u0012\f\u0012\u0004\u0012\u00020\b0\u0002j\u0002`\t0\u0007HÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R#\u0010\f\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0018\u001a\u0004\b\u0019\u0010\u0006R3\u0010\r\u001a\u001c\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u000e\u0012\f\u0012\u0004\u0012\u00020\b0\u0002j\u0002`\t0\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001a\u001a\u0004\b\u001b\u0010\u000b¨\u0006\u001e"}, d2 = {"Lcom/discord/stores/StoreMessageState$State;", "", "", "", "Lcom/discord/primitives/Index;", "component1", "()Ljava/util/Set;", "", "", "Lcom/discord/stores/VisibleKeys;", "component2", "()Ljava/util/Map;", "visibleSpoilerNodeIndices", "visibleSpoilerEmbedMap", "copy", "(Ljava/util/Set;Ljava/util/Map;)Lcom/discord/stores/StoreMessageState$State;", "toString", "()Ljava/lang/String;", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Set;", "getVisibleSpoilerNodeIndices", "Ljava/util/Map;", "getVisibleSpoilerEmbedMap", HookHelper.constructorName, "(Ljava/util/Set;Ljava/util/Map;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class State {
        private final Map<Integer, Set<String>> visibleSpoilerEmbedMap;
        private final Set<Integer> visibleSpoilerNodeIndices;

        public State() {
            this(null, null, 3, null);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public State(Set<Integer> set, Map<Integer, ? extends Set<String>> map) {
            m.checkNotNullParameter(set, "visibleSpoilerNodeIndices");
            m.checkNotNullParameter(map, "visibleSpoilerEmbedMap");
            this.visibleSpoilerNodeIndices = set;
            this.visibleSpoilerEmbedMap = map;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ State copy$default(State state, Set set, Map map, int i, Object obj) {
            if ((i & 1) != 0) {
                set = state.visibleSpoilerNodeIndices;
            }
            if ((i & 2) != 0) {
                map = state.visibleSpoilerEmbedMap;
            }
            return state.copy(set, map);
        }

        public final Set<Integer> component1() {
            return this.visibleSpoilerNodeIndices;
        }

        public final Map<Integer, Set<String>> component2() {
            return this.visibleSpoilerEmbedMap;
        }

        public final State copy(Set<Integer> set, Map<Integer, ? extends Set<String>> map) {
            m.checkNotNullParameter(set, "visibleSpoilerNodeIndices");
            m.checkNotNullParameter(map, "visibleSpoilerEmbedMap");
            return new State(set, map);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof State)) {
                return false;
            }
            State state = (State) obj;
            return m.areEqual(this.visibleSpoilerNodeIndices, state.visibleSpoilerNodeIndices) && m.areEqual(this.visibleSpoilerEmbedMap, state.visibleSpoilerEmbedMap);
        }

        public final Map<Integer, Set<String>> getVisibleSpoilerEmbedMap() {
            return this.visibleSpoilerEmbedMap;
        }

        public final Set<Integer> getVisibleSpoilerNodeIndices() {
            return this.visibleSpoilerNodeIndices;
        }

        public int hashCode() {
            Set<Integer> set = this.visibleSpoilerNodeIndices;
            int i = 0;
            int hashCode = (set != null ? set.hashCode() : 0) * 31;
            Map<Integer, Set<String>> map = this.visibleSpoilerEmbedMap;
            if (map != null) {
                i = map.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("State(visibleSpoilerNodeIndices=");
            R.append(this.visibleSpoilerNodeIndices);
            R.append(", visibleSpoilerEmbedMap=");
            return a.L(R, this.visibleSpoilerEmbedMap, ")");
        }

        public /* synthetic */ State(Set set, Map map, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? n0.emptySet() : set, (i & 2) != 0 ? h0.emptyMap() : map);
        }
    }

    public StoreMessageState(Dispatcher dispatcher) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        this.dispatcher = dispatcher;
        BehaviorSubject<Map<Long, State>> l0 = BehaviorSubject.l0(h0.emptyMap());
        m.checkNotNullExpressionValue(l0, "BehaviorSubject.create(emptyMap())");
        this.messageStateSubject = l0;
    }

    @StoreThread
    private final void resetState(List<Long> list) {
        Map<Long, State> n0 = this.messageStateSubject.n0();
        ArrayList arrayList = new ArrayList();
        for (Object obj : list) {
            long longValue = ((Number) obj).longValue();
            m.checkNotNullExpressionValue(n0, "messageStateMap");
            if (n0.containsKey(Long.valueOf(longValue))) {
                arrayList.add(obj);
            }
        }
        if (!arrayList.isEmpty()) {
            BehaviorSubject<Map<Long, State>> behaviorSubject = this.messageStateSubject;
            m.checkNotNullExpressionValue(n0, "messageStateMap");
            behaviorSubject.onNext(h0.minus((Map) n0, (Iterable) arrayList));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void updateState(long j, Function1<? super State, State> function1) {
        Map<Long, State> n0 = this.messageStateSubject.n0();
        State state = n0.get(Long.valueOf(j));
        if (state == null) {
            state = new State(null, null, 3, null);
        }
        BehaviorSubject<Map<Long, State>> behaviorSubject = this.messageStateSubject;
        m.checkNotNullExpressionValue(n0, "messageStateMap");
        behaviorSubject.onNext(h0.plus(n0, o.to(Long.valueOf(j), function1.invoke(state))));
    }

    public final Observable<Map<Long, State>> getMessageState() {
        Observable<Map<Long, State>> q = this.messageStateSubject.q();
        m.checkNotNullExpressionValue(q, "messageStateSubject.distinctUntilChanged()");
        return q;
    }

    @StoreThread
    public final void handleChannelSelected() {
        this.messageStateSubject.onNext(h0.emptyMap());
    }

    @StoreThread
    public final void handleMessageDelete(ModelMessageDelete modelMessageDelete) {
        m.checkNotNullParameter(modelMessageDelete, "messageDelete");
        List<Long> messageIds = modelMessageDelete.getMessageIds();
        m.checkNotNullExpressionValue(messageIds, "messageDelete.messageIds");
        resetState(messageIds);
    }

    @StoreThread
    public final void handleMessageUpdate(Message message) {
        m.checkNotNullParameter(message, "message");
        resetState(d0.t.m.listOf(Long.valueOf(message.o())));
    }

    public final void revealSpoiler(long j, int i) {
        if (i >= 0) {
            this.dispatcher.schedule(new StoreMessageState$revealSpoiler$1(this, j, i));
        }
    }

    public final void revealSpoilerEmbed(long j, int i) {
        if (i >= 0) {
            this.dispatcher.schedule(new StoreMessageState$revealSpoilerEmbed$1(this, j, i));
        }
    }

    public final void revealSpoilerEmbedData(long j, int i, String str) {
        m.checkNotNullParameter(str, "key");
        if (i >= 0) {
            this.dispatcher.schedule(new StoreMessageState$revealSpoilerEmbedData$1(this, j, i, str));
        }
    }
}
