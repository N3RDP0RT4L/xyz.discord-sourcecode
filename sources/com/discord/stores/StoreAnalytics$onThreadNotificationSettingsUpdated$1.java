package com.discord.stores;

import com.discord.api.channel.Channel;
import com.discord.models.domain.ModelNotificationSettings;
import com.discord.models.guild.Guild;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.channel.ChannelNotificationSettingsUtils;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreAnalytics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAnalytics$onThreadNotificationSettingsUpdated$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ int $flags;
    public final /* synthetic */ int $oldFlags;
    public final /* synthetic */ long $parentChannelId;
    public final /* synthetic */ StoreAnalytics this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreAnalytics$onThreadNotificationSettingsUpdated$1(StoreAnalytics storeAnalytics, long j, long j2, int i, int i2) {
        super(0);
        this.this$0 = storeAnalytics;
        this.$channelId = j;
        this.$parentChannelId = j2;
        this.$oldFlags = i;
        this.$flags = i2;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreStream storeStream;
        StoreStream storeStream2;
        StoreStream storeStream3;
        StoreStream storeStream4;
        Map<String, ? extends Object> threadSnapshotAnalyticsProperties;
        storeStream = this.this$0.stores;
        Channel findChannelByIdInternal$app_productionGoogleRelease = storeStream.getChannels$app_productionGoogleRelease().findChannelByIdInternal$app_productionGoogleRelease(this.$channelId);
        if (findChannelByIdInternal$app_productionGoogleRelease != null) {
            storeStream2 = this.this$0.stores;
            Channel findChannelByIdInternal$app_productionGoogleRelease2 = storeStream2.getChannels$app_productionGoogleRelease().findChannelByIdInternal$app_productionGoogleRelease(this.$parentChannelId);
            if (findChannelByIdInternal$app_productionGoogleRelease2 != null) {
                boolean z2 = (this.$oldFlags & 1) != 0;
                long f = findChannelByIdInternal$app_productionGoogleRelease.f();
                storeStream3 = this.this$0.stores;
                Guild guild = storeStream3.getGuilds$app_productionGoogleRelease().getGuild(f);
                if (guild != null) {
                    storeStream4 = this.this$0.stores;
                    ModelNotificationSettings modelNotificationSettings = storeStream4.getGuildSettings$app_productionGoogleRelease().getGuildSettingsInternal$app_productionGoogleRelease().get(Long.valueOf(f));
                    if (modelNotificationSettings != null) {
                        boolean isGuildOrCategoryOrChannelMuted = NotificationTextUtils.INSTANCE.isGuildOrCategoryOrChannelMuted(modelNotificationSettings, findChannelByIdInternal$app_productionGoogleRelease2);
                        int computeNotificationSetting = ChannelNotificationSettingsUtils.INSTANCE.computeNotificationSetting(guild, findChannelByIdInternal$app_productionGoogleRelease, modelNotificationSettings);
                        AnalyticsTracker analyticsTracker = AnalyticsTracker.INSTANCE;
                        threadSnapshotAnalyticsProperties = this.this$0.getThreadSnapshotAnalyticsProperties(findChannelByIdInternal$app_productionGoogleRelease);
                        analyticsTracker.threadNotificationSettingsUpdated(threadSnapshotAnalyticsProperties, z2, isGuildOrCategoryOrChannelMuted, computeNotificationSetting, this.$oldFlags, this.$flags);
                    }
                }
            }
        }
    }
}
