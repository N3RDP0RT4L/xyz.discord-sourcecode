package com.discord.stores;

import com.discord.api.activity.Activity;
import com.discord.models.message.Message;
import com.discord.stores.StoreSlowMode;
import com.discord.utilities.messagesend.MessageRequest;
import com.discord.utilities.messagesend.MessageResult;
import com.discord.utilities.rest.SendUtils;
import d0.t.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Ref$ObjectRef;
import rx.Emitter;
/* compiled from: StoreMessages.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lrx/Emitter;", "Lcom/discord/utilities/messagesend/MessageResult;", "emitter", "Lcom/discord/utilities/messagesend/MessageRequest$Send;", "invoke", "(Lrx/Emitter;)Lcom/discord/utilities/messagesend/MessageRequest$Send;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessages$sendMessage$createRequest$1 extends o implements Function1<Emitter<MessageResult>, MessageRequest.Send> {
    public final /* synthetic */ Activity $activity;
    public final /* synthetic */ long $attemptTimestamp;
    public final /* synthetic */ Message $localMessage;
    public final /* synthetic */ Ref$ObjectRef $validAttachments;
    public final /* synthetic */ StoreMessages this$0;

    /* compiled from: StoreMessages.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/utilities/messagesend/MessageResult;", "result", "", "isLastMessage", "", "invoke", "(Lcom/discord/utilities/messagesend/MessageResult;Z)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreMessages$sendMessage$createRequest$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function2<MessageResult, Boolean, Unit> {
        public final /* synthetic */ Emitter $emitter;

        /* compiled from: StoreMessages.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreMessages$sendMessage$createRequest$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C02101 extends o implements Function0<Unit> {
            public final /* synthetic */ boolean $isLastMessage;
            public final /* synthetic */ MessageResult $result;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C02101(boolean z2, MessageResult messageResult) {
                super(0);
                this.$isLastMessage = z2;
                this.$result = messageResult;
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                StoreStream storeStream;
                StoreStream storeStream2;
                if (this.$isLastMessage) {
                    StoreMessages.Companion.cancelBackgroundSendingWork(StoreMessages.access$getContext$p(StoreMessages$sendMessage$createRequest$1.this.this$0));
                }
                MessageResult messageResult = this.$result;
                if (messageResult instanceof MessageResult.Success) {
                    storeStream2 = StoreMessages$sendMessage$createRequest$1.this.this$0.stream;
                    storeStream2.getSlowMode$app_productionGoogleRelease().onMessageSent(((MessageResult.Success) this.$result).getMessage().g());
                    StoreMessages$sendMessage$createRequest$1.this.this$0.handleMessageCreate(m.listOf(((MessageResult.Success) this.$result).getMessage()));
                    Integer numRetries = StoreMessages$sendMessage$createRequest$1.this.$localMessage.getNumRetries();
                    if ((numRetries != null ? numRetries.intValue() : 0) > 0) {
                        StoreMessages$sendMessage$createRequest$1 storeMessages$sendMessage$createRequest$1 = StoreMessages$sendMessage$createRequest$1.this;
                        storeMessages$sendMessage$createRequest$1.this$0.trackFailedLocalMessageResolved(storeMessages$sendMessage$createRequest$1.$localMessage, FailedMessageResolutionType.RESENT);
                    }
                } else if (messageResult instanceof MessageResult.Slowmode) {
                    StoreMessages$sendMessage$createRequest$1 storeMessages$sendMessage$createRequest$12 = StoreMessages$sendMessage$createRequest$1.this;
                    storeMessages$sendMessage$createRequest$12.this$0.handleSendMessageFailure(storeMessages$sendMessage$createRequest$12.$localMessage);
                    storeStream = StoreMessages$sendMessage$createRequest$1.this.this$0.stream;
                    storeStream.getSlowMode$app_productionGoogleRelease().onCooldown(StoreMessages$sendMessage$createRequest$1.this.$localMessage.getChannelId(), ((MessageResult.Slowmode) this.$result).getCooldownMs(), StoreSlowMode.Type.MessageSend.INSTANCE);
                } else if (!(messageResult instanceof MessageResult.RateLimited)) {
                    if (messageResult instanceof MessageResult.UserCancelled) {
                        StoreMessages$sendMessage$createRequest$1 storeMessages$sendMessage$createRequest$13 = StoreMessages$sendMessage$createRequest$1.this;
                        storeMessages$sendMessage$createRequest$13.this$0.handleLocalMessageDelete(storeMessages$sendMessage$createRequest$13.$localMessage);
                    } else if (messageResult instanceof MessageResult.CaptchaRequired) {
                        StoreMessages$sendMessage$createRequest$1 storeMessages$sendMessage$createRequest$14 = StoreMessages$sendMessage$createRequest$1.this;
                        storeMessages$sendMessage$createRequest$14.this$0.handleSendMessageCaptchaRequired(storeMessages$sendMessage$createRequest$14.$localMessage);
                    } else if (messageResult instanceof MessageResult.UnknownFailure) {
                        StoreMessages$sendMessage$createRequest$1 storeMessages$sendMessage$createRequest$15 = StoreMessages$sendMessage$createRequest$1.this;
                        storeMessages$sendMessage$createRequest$15.this$0.handleSendMessageFailure(storeMessages$sendMessage$createRequest$15.$localMessage);
                    } else if (messageResult instanceof MessageResult.ValidationError) {
                        StoreMessages$sendMessage$createRequest$1 storeMessages$sendMessage$createRequest$16 = StoreMessages$sendMessage$createRequest$1.this;
                        storeMessages$sendMessage$createRequest$16.this$0.handleSendMessageValidationError(storeMessages$sendMessage$createRequest$16.$localMessage, ((MessageResult.ValidationError) messageResult).getMessage());
                    } else if (!(messageResult instanceof MessageResult.NetworkFailure) && (messageResult instanceof MessageResult.Timeout)) {
                        StoreMessages$sendMessage$createRequest$1 storeMessages$sendMessage$createRequest$17 = StoreMessages$sendMessage$createRequest$1.this;
                        storeMessages$sendMessage$createRequest$17.this$0.handleSendMessageFailure(storeMessages$sendMessage$createRequest$17.$localMessage);
                    }
                }
                AnonymousClass1.this.$emitter.onNext(this.$result);
                AnonymousClass1.this.$emitter.onCompleted();
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Emitter emitter) {
            super(2);
            this.$emitter = emitter;
        }

        @Override // kotlin.jvm.functions.Function2
        public /* bridge */ /* synthetic */ Unit invoke(MessageResult messageResult, Boolean bool) {
            invoke(messageResult, bool.booleanValue());
            return Unit.a;
        }

        public final void invoke(MessageResult messageResult, boolean z2) {
            Dispatcher dispatcher;
            d0.z.d.m.checkNotNullParameter(messageResult, "result");
            dispatcher = StoreMessages$sendMessage$createRequest$1.this.this$0.dispatcher;
            dispatcher.schedule(new C02101(z2, messageResult));
        }
    }

    /* compiled from: StoreMessages.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/rest/SendUtils$SendPayload$Preprocessing;", "<name for destructuring parameter 0>", "", "invoke", "(Lcom/discord/utilities/rest/SendUtils$SendPayload$Preprocessing;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreMessages$sendMessage$createRequest$1$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2 extends o implements Function1<SendUtils.SendPayload.Preprocessing, Unit> {

        /* compiled from: StoreMessages.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreMessages$sendMessage$createRequest$1$2$1  reason: invalid class name */
        /* loaded from: classes.dex */
        public static final class AnonymousClass1 extends o implements Function0<Unit> {
            public final /* synthetic */ String $displayName;
            public final /* synthetic */ String $mimeType;
            public final /* synthetic */ int $numFiles;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public AnonymousClass1(int i, String str, String str2) {
                super(0);
                this.$numFiles = i;
                this.$displayName = str;
                this.$mimeType = str2;
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                StoreStream storeStream;
                storeStream = StoreMessages$sendMessage$createRequest$1.this.this$0.stream;
                StoreMessageUploads messageUploads$app_productionGoogleRelease = storeStream.getMessageUploads$app_productionGoogleRelease();
                String nonce = StoreMessages$sendMessage$createRequest$1.this.$localMessage.getNonce();
                d0.z.d.m.checkNotNull(nonce);
                messageUploads$app_productionGoogleRelease.onPreprocessing(nonce, this.$numFiles, this.$displayName, this.$mimeType);
            }
        }

        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(SendUtils.SendPayload.Preprocessing preprocessing) {
            invoke2(preprocessing);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(SendUtils.SendPayload.Preprocessing preprocessing) {
            Dispatcher dispatcher;
            d0.z.d.m.checkNotNullParameter(preprocessing, "<name for destructuring parameter 0>");
            int component1 = preprocessing.component1();
            String component2 = preprocessing.component2();
            String component3 = preprocessing.component3();
            dispatcher = StoreMessages$sendMessage$createRequest$1.this.this$0.dispatcher;
            dispatcher.schedule(new AnonymousClass1(component1, component2, component3));
        }
    }

    /* compiled from: StoreMessages.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/utilities/rest/SendUtils$FileUpload;", "uploads", "", "invoke", "(Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreMessages$sendMessage$createRequest$1$3  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass3 extends o implements Function1<List<? extends SendUtils.FileUpload>, Unit> {

        /* compiled from: StoreMessages.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreMessages$sendMessage$createRequest$1$3$1  reason: invalid class name */
        /* loaded from: classes.dex */
        public static final class AnonymousClass1 extends o implements Function0<Unit> {
            public final /* synthetic */ List $uploads;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public AnonymousClass1(List list) {
                super(0);
                this.$uploads = list;
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                StoreStream storeStream;
                storeStream = StoreMessages$sendMessage$createRequest$1.this.this$0.stream;
                StoreMessageUploads messageUploads$app_productionGoogleRelease = storeStream.getMessageUploads$app_productionGoogleRelease();
                String nonce = StoreMessages$sendMessage$createRequest$1.this.$localMessage.getNonce();
                d0.z.d.m.checkNotNull(nonce);
                messageUploads$app_productionGoogleRelease.bindUpload(nonce, this.$uploads);
            }
        }

        public AnonymousClass3() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(List<? extends SendUtils.FileUpload> list) {
            invoke2((List<SendUtils.FileUpload>) list);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(List<SendUtils.FileUpload> list) {
            Dispatcher dispatcher;
            d0.z.d.m.checkNotNullParameter(list, "uploads");
            dispatcher = StoreMessages$sendMessage$createRequest$1.this.this$0.dispatcher;
            dispatcher.schedule(new AnonymousClass1(list));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreMessages$sendMessage$createRequest$1(StoreMessages storeMessages, Message message, Ref$ObjectRef ref$ObjectRef, Activity activity, long j) {
        super(1);
        this.this$0 = storeMessages;
        this.$localMessage = message;
        this.$validAttachments = ref$ObjectRef;
        this.$activity = activity;
        this.$attemptTimestamp = j;
    }

    public final MessageRequest.Send invoke(Emitter<MessageResult> emitter) {
        d0.z.d.m.checkNotNullParameter(emitter, "emitter");
        return new MessageRequest.Send(this.$localMessage, this.$activity, (List) this.$validAttachments.element, new AnonymousClass1(emitter), new AnonymousClass2(), new AnonymousClass3(), this.$attemptTimestamp);
    }
}
