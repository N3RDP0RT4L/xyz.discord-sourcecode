package com.discord.stores;

import com.discord.api.channel.Channel;
import com.discord.stores.StoreChannelsSelected;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreChannelsSelected.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;", "invoke", "()Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreChannelsSelected$validateSelectedChannel$1 extends o implements Function0<StoreChannelsSelected.ResolvedSelectedChannel> {
    public final /* synthetic */ Map $allChannels;
    public final /* synthetic */ StoreChannelsSelected.UserChannelSelection $channelSelection;
    public final /* synthetic */ boolean $isChannelStoreInitializedForAuthedUser;
    public final /* synthetic */ Map $permissionsForChannelsInGuild;
    public final /* synthetic */ long $selectedGuildId;
    public final /* synthetic */ StoreChannelsSelected this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreChannelsSelected$validateSelectedChannel$1(StoreChannelsSelected storeChannelsSelected, StoreChannelsSelected.UserChannelSelection userChannelSelection, Map map, long j, Map map2, boolean z2) {
        super(0);
        this.this$0 = storeChannelsSelected;
        this.$channelSelection = userChannelSelection;
        this.$allChannels = map;
        this.$selectedGuildId = j;
        this.$permissionsForChannelsInGuild = map2;
        this.$isChannelStoreInitializedForAuthedUser = z2;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final StoreChannelsSelected.ResolvedSelectedChannel invoke() {
        Channel channel;
        StoreChannelsSelected.ResolvedSelectedChannel resolveSelectedChannel;
        StoreChannels storeChannels;
        StoreChannelsSelected.UserChannelSelection userChannelSelection = this.$channelSelection;
        if (userChannelSelection != null) {
            storeChannels = this.this$0.storeChannels;
            channel = storeChannels.getChannel(userChannelSelection.getId());
        } else {
            channel = null;
        }
        resolveSelectedChannel = this.this$0.resolveSelectedChannel(this.$channelSelection, channel, this.$allChannels, this.$selectedGuildId, this.$permissionsForChannelsInGuild, this.$isChannelStoreInitializedForAuthedUser);
        return resolveSelectedChannel;
    }
}
