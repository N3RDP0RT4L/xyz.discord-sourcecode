package com.discord.stores;

import com.discord.models.user.User;
import d0.t.g0;
import d0.t.n;
import d0.t.u;
import d0.z.d.o;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.SortedMap;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreGameParty.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u00030\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/user/User;", "invoke", "()Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGameParty$observeUsersForPartyId$1 extends o implements Function0<Map<Long, ? extends User>> {
    public final /* synthetic */ String $partyId;
    public final /* synthetic */ StoreGameParty this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGameParty$observeUsersForPartyId$1(StoreGameParty storeGameParty, String str) {
        super(0);
        this.this$0 = storeGameParty;
        this.$partyId = str;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Map<Long, ? extends User> invoke() {
        HashMap hashMap;
        List list;
        hashMap = this.this$0.partiesSnapshot;
        HashSet hashSet = (HashSet) hashMap.get(this.$partyId);
        StoreUser storeUser = this.this$0.getStoreUser();
        if (hashSet == null || (list = u.toList(hashSet)) == null) {
            list = n.emptyList();
        }
        SortedMap sortedMap = g0.toSortedMap(storeUser.getUsers(list, false));
        Objects.requireNonNull(sortedMap, "null cannot be cast to non-null type kotlin.collections.Map<com.discord.primitives.UserId /* = kotlin.Long */, com.discord.models.user.User>");
        return sortedMap;
    }
}
