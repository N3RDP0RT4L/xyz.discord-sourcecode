package com.discord.stores;

import com.discord.stores.StoreInstantInvites;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreInstantInvites.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "", "Lcom/discord/stores/StoreInstantInvites$InviteState;", "invoke", "()Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreInstantInvites$observeKnownInvites$1 extends o implements Function0<Map<String, ? extends StoreInstantInvites.InviteState>> {
    public final /* synthetic */ StoreInstantInvites this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreInstantInvites$observeKnownInvites$1(StoreInstantInvites storeInstantInvites) {
        super(0);
        this.this$0 = storeInstantInvites;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Map<String, ? extends StoreInstantInvites.InviteState> invoke() {
        Map<String, ? extends StoreInstantInvites.InviteState> knownInvites;
        knownInvites = this.this$0.getKnownInvites();
        return knownInvites;
    }
}
