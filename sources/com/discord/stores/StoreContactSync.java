package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.Context;
import com.discord.api.connectedaccounts.ConnectedAccount;
import com.discord.api.friendsuggestions.AllowedInSuggestionsType;
import com.discord.models.experiments.domain.Experiment;
import com.discord.models.user.MeUser;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreNotices;
import com.discord.stores.StoreUserConnections;
import com.discord.stores.utilities.RestCallStateKt;
import com.discord.utilities.contacts.ContactsFetcher;
import com.discord.utilities.persister.Persister;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.time.Clock;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.user.UserUtils;
import d0.t.g0;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
/* compiled from: StoreContactSync.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0092\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001BG\u0012\u0006\u0010:\u001a\u000209\u0012\u0006\u0010\u001f\u001a\u00020\u001e\u0012\u0006\u00102\u001a\u000201\u0012\u0006\u0010\u0019\u001a\u00020\u0018\u0012\u0006\u0010\"\u001a\u00020!\u0012\u0006\u0010-\u001a\u00020,\u0012\u0006\u0010=\u001a\u00020<\u0012\u0006\u0010\u001c\u001a\u00020\u001b¢\u0006\u0004\b?\u0010@J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\r\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\u000b\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\bH\u0016¢\u0006\u0004\b\u000b\u0010\fJ\u000f\u0010\r\u001a\u00020\nH\u0007¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u000f\u001a\u00020\nH\u0007¢\u0006\u0004\b\u000f\u0010\u000eJ\r\u0010\u0010\u001a\u00020\n¢\u0006\u0004\b\u0010\u0010\u000eJ\r\u0010\u0011\u001a\u00020\n¢\u0006\u0004\b\u0011\u0010\u000eJ\r\u0010\u0012\u001a\u00020\n¢\u0006\u0004\b\u0012\u0010\u000eJ\u0015\u0010\u0015\u001a\u00020\n2\u0006\u0010\u0014\u001a\u00020\u0013¢\u0006\u0004\b\u0015\u0010\u0016J\r\u0010\u0017\u001a\u00020\n¢\u0006\u0004\b\u0017\u0010\u000eR\u0016\u0010\u0019\u001a\u00020\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001c\u001a\u00020\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR\u0016\u0010\u001f\u001a\u00020\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u0016\u0010\"\u001a\u00020!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010#R\u0016\u0010%\u001a\u00020$8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b%\u0010&R(\u0010*\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020)\u0012\u0004\u0012\u00020\u00050(0'8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010+R\u0016\u0010-\u001a\u00020,8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b-\u0010.R\u0016\u0010/\u001a\u00020\u00058\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b/\u00100R\u0016\u00102\u001a\u0002018\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b2\u00103R2\u00106\u001a\u001e\u0012\u0004\u0012\u00020)\u0012\u0004\u0012\u00020\u000504j\u000e\u0012\u0004\u0012\u00020)\u0012\u0004\u0012\u00020\u0005`58\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b6\u00107R\u001c\u00108\u001a\b\u0012\u0004\u0012\u00020\u00130'8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b8\u0010+R\u0016\u0010:\u001a\u0002098\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b:\u0010;R\u0016\u0010=\u001a\u00020<8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b=\u0010>¨\u0006A"}, d2 = {"Lcom/discord/stores/StoreContactSync;", "Lcom/discord/stores/StoreV2;", "Lcom/discord/stores/StoreNotices$Notice;", "createContactSyncNotice", "()Lcom/discord/stores/StoreNotices$Notice;", "", "getFriendsListUpsellDismissed", "()Z", "Landroid/content/Context;", "context", "", "init", "(Landroid/content/Context;)V", "handleConnectionOpen", "()V", "handlePostConnectionOpen", "backgroundUploadContacts", "dismissUpsell", "dismissFriendsListUpsell", "", "timestamp", "setContactSyncUploadTimestamp", "(J)V", "clearDismissStates", "Lcom/discord/stores/StoreUserConnections;", "connectionsStore", "Lcom/discord/stores/StoreUserConnections;", "Lcom/discord/stores/StoreUserSettings;", "userSettingsStore", "Lcom/discord/stores/StoreUserSettings;", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "Lcom/discord/stores/StoreUser;", "usersStore", "Lcom/discord/stores/StoreUser;", "Lcom/discord/utilities/contacts/ContactsFetcher;", "contactsFetcher", "Lcom/discord/utilities/contacts/ContactsFetcher;", "Lcom/discord/utilities/persister/Persister;", "", "", "dismissStateCache", "Lcom/discord/utilities/persister/Persister;", "Lcom/discord/stores/StoreExperiments;", "experimentsStore", "Lcom/discord/stores/StoreExperiments;", "shouldTryUploadContacts", "Z", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "dismissState", "Ljava/util/HashMap;", "uploadTimestampCache", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/stores/StoreNotices;", "noticesStore", "Lcom/discord/stores/StoreNotices;", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreUserConnections;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreExperiments;Lcom/discord/stores/StoreNotices;Lcom/discord/stores/StoreUserSettings;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreContactSync extends StoreV2 {
    private final Clock clock;
    private final StoreUserConnections connectionsStore;
    private ContactsFetcher contactsFetcher;
    private final Dispatcher dispatcher;
    private final StoreExperiments experimentsStore;
    private final StoreNotices noticesStore;
    private final RestAPI restAPI;
    private boolean shouldTryUploadContacts;
    private final StoreUserSettings userSettingsStore;
    private final StoreUser usersStore;
    private final Persister<Map<String, Boolean>> dismissStateCache = new Persister<>("CONTACT_SYNC_DISMISS_STATE", new HashMap());
    private final Persister<Long> uploadTimestampCache = new Persister<>("CONTACT_SYNC_LAST_UPLOAD_TIME", 0L);
    private final HashMap<String, Boolean> dismissState = new HashMap<>();

    public StoreContactSync(Dispatcher dispatcher, RestAPI restAPI, Clock clock, StoreUserConnections storeUserConnections, StoreUser storeUser, StoreExperiments storeExperiments, StoreNotices storeNotices, StoreUserSettings storeUserSettings) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(restAPI, "restAPI");
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(storeUserConnections, "connectionsStore");
        m.checkNotNullParameter(storeUser, "usersStore");
        m.checkNotNullParameter(storeExperiments, "experimentsStore");
        m.checkNotNullParameter(storeNotices, "noticesStore");
        m.checkNotNullParameter(storeUserSettings, "userSettingsStore");
        this.dispatcher = dispatcher;
        this.restAPI = restAPI;
        this.clock = clock;
        this.connectionsStore = storeUserConnections;
        this.usersStore = storeUser;
        this.experimentsStore = storeExperiments;
        this.noticesStore = storeNotices;
        this.userSettingsStore = storeUserSettings;
    }

    private final StoreNotices.Notice createContactSyncNotice() {
        return new StoreNotices.Notice("CONTACT_SYNC_UPSELL", null, 1336L, 0, true, null, 0L, false, 0L, StoreContactSync$createContactSyncNotice$1.INSTANCE, 482, null);
    }

    public final void backgroundUploadContacts() {
        ContactsFetcher contactsFetcher = this.contactsFetcher;
        if (contactsFetcher == null) {
            m.throwUninitializedPropertyAccessException("contactsFetcher");
        }
        Set<String> fetchContacts = contactsFetcher.fetchContacts();
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(fetchContacts, 10));
        for (String str : fetchContacts) {
            arrayList.add(new RestAPIParams.ContactEntry(str, str, g0.mapOf(new Pair("number", str))));
        }
        RestCallStateKt.logNetworkAction(this.restAPI.uploadContacts(new RestAPIParams.UploadContacts(arrayList, true, AllowedInSuggestionsType.ANYONE_WITH_CONTACT_INFO)), StoreContactSync$backgroundUploadContacts$1.INSTANCE);
        setContactSyncUploadTimestamp(this.clock.currentTimeMillis());
    }

    public final void clearDismissStates() {
        this.dismissStateCache.clear(true);
        getPrefsSessionDurable().edit().remove("CONTACT_SYNC_DISMISS_UPSELL").apply();
        this.dispatcher.schedule(new StoreContactSync$clearDismissStates$1(this));
    }

    public final void dismissFriendsListUpsell() {
        this.dismissStateCache.set(this.dismissState, true);
        this.dispatcher.schedule(new StoreContactSync$dismissFriendsListUpsell$1(this));
    }

    public final void dismissUpsell() {
        getPrefsSessionDurable().edit().putBoolean("CONTACT_SYNC_DISMISS_UPSELL", true).apply();
        this.dispatcher.schedule(new StoreContactSync$dismissUpsell$1(this));
    }

    public final boolean getFriendsListUpsellDismissed() {
        return m.areEqual(this.dismissState.get("CONTACT_SYNC_DISMISS_FRIENDS_UPSELL"), Boolean.TRUE);
    }

    @StoreThread
    public final void handleConnectionOpen() {
        this.dispatcher.schedule(new StoreContactSync$handleConnectionOpen$1(this));
    }

    @StoreThread
    public final void handlePostConnectionOpen() {
        ConnectedAccount connectedAccount;
        MeUser me2 = this.usersStore.getMe();
        StoreUserConnections.State connectedAccounts = this.connectionsStore.getConnectedAccounts();
        ListIterator<ConnectedAccount> listIterator = connectedAccounts.listIterator(connectedAccounts.size());
        while (true) {
            if (!listIterator.hasPrevious()) {
                connectedAccount = null;
                break;
            }
            connectedAccount = listIterator.previous();
            if (m.areEqual(connectedAccount.g(), "contacts")) {
                break;
            }
        }
        ConnectedAccount connectedAccount2 = connectedAccount;
        Boolean bool = this.dismissState.get("CONTACT_SYNC_DISMISS_UPSELL");
        Boolean contactSyncUpsellShown = this.userSettingsStore.getContactSyncUpsellShown();
        boolean z2 = false;
        Experiment userExperiment = this.experimentsStore.getUserExperiment("2021-04_contact_sync_android_main", connectedAccount2 == null);
        if (userExperiment != null && userExperiment.getBucket() == 1 && connectedAccount2 == null) {
            UserUtils userUtils = UserUtils.INSTANCE;
            if (userUtils.getHasPhone(me2)) {
                Boolean bool2 = Boolean.TRUE;
                if ((!m.areEqual(bool, bool2)) && (!m.areEqual(contactSyncUpsellShown, bool2)) && userUtils.getAgeMs(me2, ClockFactory.get()) > 604800000) {
                    z2 = true;
                }
            }
        }
        Boolean bool3 = Boolean.TRUE;
        if (m.areEqual(bool, bool3) && (!m.areEqual(contactSyncUpsellShown, bool3))) {
            this.userSettingsStore.updateContactSyncShown();
        }
        if (z2) {
            this.noticesStore.requestToShow(createContactSyncNotice());
        }
        if (this.shouldTryUploadContacts) {
            backgroundUploadContacts();
        }
    }

    @Override // com.discord.stores.Store
    public void init(Context context) {
        m.checkNotNullParameter(context, "context");
        super.init(context);
        this.contactsFetcher = new ContactsFetcher(context);
        this.dismissState.putAll(this.dismissStateCache.get());
        boolean z2 = true;
        if (m.areEqual(this.dismissState.get("CONTACT_SYNC_DISMISS_UPSELL"), Boolean.TRUE)) {
            getPrefsSessionDurable().edit().putBoolean("CONTACT_SYNC_DISMISS_UPSELL", true).apply();
        }
        this.dismissState.put("CONTACT_SYNC_DISMISS_UPSELL", Boolean.valueOf(getPrefsSessionDurable().getBoolean("CONTACT_SYNC_DISMISS_UPSELL", false)));
        if (this.clock.currentTimeMillis() - this.uploadTimestampCache.get().longValue() <= 86400000) {
            z2 = false;
        }
        this.shouldTryUploadContacts = z2;
        markChanged();
    }

    public final void setContactSyncUploadTimestamp(long j) {
        this.uploadTimestampCache.set(Long.valueOf(j), true);
        this.dispatcher.schedule(new StoreContactSync$setContactSyncUploadTimestamp$1(this, j));
    }
}
