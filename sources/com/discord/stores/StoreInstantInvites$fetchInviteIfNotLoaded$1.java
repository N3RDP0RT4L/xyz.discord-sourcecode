package com.discord.stores;

import com.discord.analytics.generated.events.network_action.TrackNetworkActionInviteResolve;
import com.discord.analytics.generated.traits.TrackNetworkMetadataReceiver;
import com.discord.api.channel.Channel;
import com.discord.api.guild.Guild;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.user.User;
import com.discord.models.domain.ModelInvite;
import com.discord.restapi.RestAPIAbortCodes;
import com.discord.stores.StoreInstantInvites;
import com.discord.stores.utilities.RestCallStateKt;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreInstantInvites.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreInstantInvites$fetchInviteIfNotLoaded$1 extends o implements Function0<Unit> {
    public final /* synthetic */ Long $eventId;
    public final /* synthetic */ String $inviteCode;
    public final /* synthetic */ String $inviteKey;
    public final /* synthetic */ Boolean $inviteResolved;
    public final /* synthetic */ String $location;
    public final /* synthetic */ Function0 $onError;
    public final /* synthetic */ StoreInstantInvites this$0;

    /* compiled from: StoreInstantInvites.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/ModelInvite;", "it", "Lcom/discord/analytics/generated/traits/TrackNetworkMetadataReceiver;", "invoke", "(Lcom/discord/models/domain/ModelInvite;)Lcom/discord/analytics/generated/traits/TrackNetworkMetadataReceiver;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreInstantInvites$fetchInviteIfNotLoaded$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<ModelInvite, TrackNetworkMetadataReceiver> {
        public AnonymousClass1() {
            super(1);
        }

        public final TrackNetworkMetadataReceiver invoke(ModelInvite modelInvite) {
            User inviter;
            Channel channel;
            Channel channel2;
            Guild guild;
            StoreInstantInvites$fetchInviteIfNotLoaded$1 storeInstantInvites$fetchInviteIfNotLoaded$1 = StoreInstantInvites$fetchInviteIfNotLoaded$1.this;
            return new TrackNetworkActionInviteResolve(storeInstantInvites$fetchInviteIfNotLoaded$1.$inviteResolved, storeInstantInvites$fetchInviteIfNotLoaded$1.$inviteCode, Boolean.valueOf(StoreStream.Companion.getAuthentication().isAuthed()), (modelInvite == null || (guild = modelInvite.guild) == null) ? null : Long.valueOf(guild.r()), (modelInvite == null || (channel2 = modelInvite.getChannel()) == null) ? null : Long.valueOf(channel2.h()), (modelInvite == null || (channel = modelInvite.getChannel()) == null) ? null : Long.valueOf(channel.A()), (modelInvite == null || (inviter = modelInvite.getInviter()) == null) ? null : Long.valueOf(inviter.i()), modelInvite != null ? Long.valueOf(modelInvite.getApproximateMemberCount()) : null, modelInvite != null ? Long.valueOf(modelInvite.getApproximatePresenceCount()) : null, modelInvite != null ? modelInvite.getInviteType() : null, null, modelInvite != null ? Boolean.valueOf(m.areEqual(modelInvite.code, String.valueOf((int) RestAPIAbortCodes.USER_BANNED))) : null, null);
        }
    }

    /* compiled from: StoreInstantInvites.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/ModelInvite;", "it", "", "invoke", "(Lcom/discord/models/domain/ModelInvite;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreInstantInvites$fetchInviteIfNotLoaded$1$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2 extends o implements Function1<ModelInvite, Unit> {

        /* compiled from: StoreInstantInvites.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreInstantInvites$fetchInviteIfNotLoaded$1$2$1  reason: invalid class name */
        /* loaded from: classes.dex */
        public static final class AnonymousClass1 extends o implements Function0<Unit> {
            public final /* synthetic */ ModelInvite $it;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public AnonymousClass1(ModelInvite modelInvite) {
                super(0);
                this.$it = modelInvite;
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                StoreInstantInvites$fetchInviteIfNotLoaded$1 storeInstantInvites$fetchInviteIfNotLoaded$1 = StoreInstantInvites$fetchInviteIfNotLoaded$1.this;
                StoreInstantInvites storeInstantInvites = storeInstantInvites$fetchInviteIfNotLoaded$1.this$0;
                String str = storeInstantInvites$fetchInviteIfNotLoaded$1.$inviteKey;
                m.checkNotNullExpressionValue(str, "inviteKey");
                storeInstantInvites.setChatInvites(str, new StoreInstantInvites.InviteState.Resolved(this.$it));
                GuildScheduledEvent guildScheduledEvent = this.$it.getGuildScheduledEvent();
                if (guildScheduledEvent != null) {
                    StoreGuildScheduledEvents guildScheduledEvents = StoreStream.Companion.getGuildScheduledEvents();
                    m.checkNotNullExpressionValue(guildScheduledEvent, "event");
                    guildScheduledEvents.addGuildScheduledEventFromInvite(guildScheduledEvent);
                }
            }
        }

        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(ModelInvite modelInvite) {
            invoke2(modelInvite);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(ModelInvite modelInvite) {
            Dispatcher dispatcher;
            m.checkNotNullParameter(modelInvite, "it");
            dispatcher = StoreInstantInvites$fetchInviteIfNotLoaded$1.this.this$0.dispatcher;
            dispatcher.schedule(new AnonymousClass1(modelInvite));
            String str = StoreInstantInvites$fetchInviteIfNotLoaded$1.this.$location;
            if (str != null) {
                AnalyticsTracker.INSTANCE.inviteResolved(modelInvite, str);
            }
        }
    }

    /* compiled from: StoreInstantInvites.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "error", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreInstantInvites$fetchInviteIfNotLoaded$1$3  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass3 extends o implements Function1<Error, Unit> {

        /* compiled from: StoreInstantInvites.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreInstantInvites$fetchInviteIfNotLoaded$1$3$1  reason: invalid class name */
        /* loaded from: classes.dex */
        public static final class AnonymousClass1 extends o implements Function0<Unit> {
            public AnonymousClass1() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                StoreInstantInvites$fetchInviteIfNotLoaded$1 storeInstantInvites$fetchInviteIfNotLoaded$1 = StoreInstantInvites$fetchInviteIfNotLoaded$1.this;
                StoreInstantInvites storeInstantInvites = storeInstantInvites$fetchInviteIfNotLoaded$1.this$0;
                String str = storeInstantInvites$fetchInviteIfNotLoaded$1.$inviteKey;
                m.checkNotNullExpressionValue(str, "inviteKey");
                storeInstantInvites.setChatInvites(str, StoreInstantInvites.InviteState.Invalid.INSTANCE);
            }
        }

        /* compiled from: StoreInstantInvites.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreInstantInvites$fetchInviteIfNotLoaded$1$3$2  reason: invalid class name */
        /* loaded from: classes.dex */
        public static final class AnonymousClass2 extends o implements Function0<Unit> {
            public AnonymousClass2() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                StoreInstantInvites$fetchInviteIfNotLoaded$1 storeInstantInvites$fetchInviteIfNotLoaded$1 = StoreInstantInvites$fetchInviteIfNotLoaded$1.this;
                StoreInstantInvites storeInstantInvites = storeInstantInvites$fetchInviteIfNotLoaded$1.this$0;
                String str = storeInstantInvites$fetchInviteIfNotLoaded$1.$inviteKey;
                m.checkNotNullExpressionValue(str, "inviteKey");
                storeInstantInvites.setChatInvites(str, StoreInstantInvites.InviteState.LoadFailed.INSTANCE);
            }
        }

        public AnonymousClass3() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Error error) {
            invoke2(error);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Error error) {
            Dispatcher dispatcher;
            Dispatcher dispatcher2;
            m.checkNotNullParameter(error, "error");
            Function0 function0 = StoreInstantInvites$fetchInviteIfNotLoaded$1.this.$onError;
            if (function0 != null) {
                Unit unit = (Unit) function0.invoke();
            }
            int ordinal = error.getType().ordinal();
            if (ordinal == 3) {
                error.setShowErrorToasts(false);
                dispatcher = StoreInstantInvites$fetchInviteIfNotLoaded$1.this.this$0.dispatcher;
                dispatcher.schedule(new AnonymousClass1());
                StoreInstantInvites$fetchInviteIfNotLoaded$1 storeInstantInvites$fetchInviteIfNotLoaded$1 = StoreInstantInvites$fetchInviteIfNotLoaded$1.this;
                String str = storeInstantInvites$fetchInviteIfNotLoaded$1.$location;
                if (str != null) {
                    String str2 = storeInstantInvites$fetchInviteIfNotLoaded$1.$inviteCode;
                    Error.Response response = error.getResponse();
                    m.checkNotNullExpressionValue(response, "error.response");
                    String message = response.getMessage();
                    Error.Response response2 = error.getResponse();
                    m.checkNotNullExpressionValue(response2, "error.response");
                    AnalyticsTracker.inviteResolveFailed(str2, str, message, Integer.valueOf(response2.getCode()));
                }
            } else if (ordinal == 11) {
                dispatcher2 = StoreInstantInvites$fetchInviteIfNotLoaded$1.this.this$0.dispatcher;
                dispatcher2.schedule(new AnonymousClass2());
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreInstantInvites$fetchInviteIfNotLoaded$1(StoreInstantInvites storeInstantInvites, String str, String str2, Long l, Boolean bool, String str3, Function0 function0) {
        super(0);
        this.this$0 = storeInstantInvites;
        this.$inviteKey = str;
        this.$inviteCode = str2;
        this.$eventId = l;
        this.$inviteResolved = bool;
        this.$location = str3;
        this.$onError = function0;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        Map map;
        RestAPI restAPI;
        Map map2;
        map = this.this$0.knownInvites;
        if (map.containsKey(this.$inviteKey)) {
            map2 = this.this$0.knownInvites;
            if (!(map2.get(this.$inviteKey) instanceof StoreInstantInvites.InviteState.LoadFailed)) {
                return;
            }
        }
        AnalyticsTracker.inviteOpened(this.$inviteCode);
        StoreInstantInvites storeInstantInvites = this.this$0;
        String str = this.$inviteKey;
        m.checkNotNullExpressionValue(str, "inviteKey");
        storeInstantInvites.setChatInvites(str, StoreInstantInvites.InviteState.Loading.INSTANCE);
        restAPI = this.this$0.restAPI;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestCallStateKt.logNetworkAction(restAPI.getInviteCode(this.$inviteCode, true, this.$eventId), new AnonymousClass1()), false, 1, null), this.this$0.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new AnonymousClass3(), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2());
    }
}
