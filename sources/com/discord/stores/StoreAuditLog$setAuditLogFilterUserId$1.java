package com.discord.stores;

import com.discord.stores.StoreAuditLog;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreAuditLog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAuditLog$setAuditLogFilterUserId$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $userId;
    public final /* synthetic */ StoreAuditLog this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreAuditLog$setAuditLogFilterUserId$1(StoreAuditLog storeAuditLog, long j) {
        super(0);
        this.this$0 = storeAuditLog;
        this.$userId = j;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreAuditLog.AuditLogState auditLogState;
        StoreAuditLog.AuditLogState auditLogState2;
        StoreAuditLog.AuditLogState auditLogState3;
        StoreAuditLog.AuditLogState auditLogState4;
        long j = this.$userId;
        auditLogState = this.this$0.state;
        if (j != auditLogState.getFilter().getUserFilter()) {
            StoreAuditLog storeAuditLog = this.this$0;
            auditLogState4 = storeAuditLog.state;
            storeAuditLog.state = StoreAuditLog.AuditLogState.copy$default(auditLogState4, 0L, null, null, null, null, null, null, null, null, null, false, 2043, null);
        }
        StoreAuditLog storeAuditLog2 = this.this$0;
        auditLogState2 = storeAuditLog2.state;
        auditLogState3 = this.this$0.state;
        storeAuditLog2.state = StoreAuditLog.AuditLogState.copy$default(auditLogState2, 0L, null, null, null, null, null, null, null, StoreAuditLog.AuditLogFilter.copy$default(auditLogState3.getFilter(), this.$userId, 0, 2, null), null, false, 1791, null);
        this.this$0.markChanged();
    }
}
