package com.discord.stores;

import b.d.b.a.a;
import com.discord.app.AppLog;
import com.discord.models.domain.ModelApplicationStream;
import com.discord.rtcconnection.RtcConnection;
import com.discord.stores.StoreApplicationStreaming;
import com.discord.utilities.logging.Logger;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreApplicationStreaming.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreApplicationStreaming$createStream$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ Long $guildId;
    public final /* synthetic */ String $preferredRegion;
    public final /* synthetic */ StoreApplicationStreaming this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreApplicationStreaming$createStream$1(StoreApplicationStreaming storeApplicationStreaming, long j, Long l, String str) {
        super(0);
        this.this$0 = storeApplicationStreaming;
        this.$channelId = j;
        this.$guildId = l;
        this.$preferredRegion = str;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreRtcConnection storeRtcConnection;
        StoreUser storeUser;
        ModelApplicationStream modelApplicationStream;
        StoreApplicationStreaming.ActiveApplicationStream activeApplicationStream;
        StoreStream storeStream;
        StoreApplicationStreaming.ActiveApplicationStream.State state;
        StoreApplicationStreaming.ActiveApplicationStream.State state2;
        ModelApplicationStream stream;
        storeRtcConnection = this.this$0.rtcConnectionStore;
        RtcConnection rtcConnection$app_productionGoogleRelease = storeRtcConnection.getRtcConnection$app_productionGoogleRelease();
        Object obj = null;
        boolean z2 = false;
        if (rtcConnection$app_productionGoogleRelease == null || rtcConnection$app_productionGoogleRelease.P != this.$channelId || (!m.areEqual(rtcConnection$app_productionGoogleRelease.O, this.$guildId))) {
            AppLog appLog = AppLog.g;
            StringBuilder R = a.R("\n                Failed to start stream.\n                rtcConnection == null: ");
            R.append(rtcConnection$app_productionGoogleRelease == null);
            R.append("\n                rtcConnection.channelId != channelId: ");
            if (rtcConnection$app_productionGoogleRelease == null || rtcConnection$app_productionGoogleRelease.P != this.$channelId) {
                z2 = true;
            }
            R.append(z2);
            R.append("\n                rtcConnection.guildId != guildId: ");
            if (rtcConnection$app_productionGoogleRelease != null) {
                obj = rtcConnection$app_productionGoogleRelease.O;
            }
            R.append(!m.areEqual(obj, this.$guildId));
            R.append("\n              ");
            Logger.e$default(appLog, "Failed to start stream.", new IllegalStateException(d0.g0.m.trimIndent(R.toString())), null, 4, null);
            return;
        }
        storeUser = this.this$0.userStore;
        long id2 = storeUser.getMe().getId();
        if (this.$guildId != null) {
            modelApplicationStream = new ModelApplicationStream.GuildStream(this.$guildId.longValue(), this.$channelId, id2);
        } else {
            modelApplicationStream = new ModelApplicationStream.CallStream(this.$channelId, id2);
        }
        activeApplicationStream = this.this$0.activeApplicationStream;
        if (!(activeApplicationStream == null || (stream = activeApplicationStream.getStream()) == null)) {
            obj = stream.getEncodedStreamKey();
        }
        if ((obj == modelApplicationStream.getEncodedStreamKey()) && activeApplicationStream != null && (state2 = activeApplicationStream.getState()) != null && state2.isStreamActive()) {
            z2 = true;
        }
        if (!z2) {
            if (!(activeApplicationStream == null || (state = activeApplicationStream.getState()) == null || !state.isStreamActive())) {
                this.this$0.stopStreamInternal(activeApplicationStream.getStream().getEncodedStreamKey());
            }
            storeStream = this.this$0.storeStream;
            storeStream.streamCreate(modelApplicationStream.getEncodedStreamKey(), this.$preferredRegion);
        }
    }
}
