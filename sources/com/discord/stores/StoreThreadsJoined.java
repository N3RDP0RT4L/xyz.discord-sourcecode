package com.discord.stores;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guild.Guild;
import com.discord.api.thread.AugmentedThreadMember;
import com.discord.api.thread.ThreadMember;
import com.discord.api.thread.ThreadMemberUpdate;
import com.discord.api.thread.ThreadMembersUpdate;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.models.domain.ModelMuteConfig;
import com.discord.models.domain.ModelPayload;
import com.discord.models.thread.dto.ModelThreadListSync;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.search.network.state.SearchState;
import d0.t.h0;
import d0.t.r;
import d0.z.d.m;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreThreadsJoined.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u008a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010%\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 E2\u00020\u0001:\u0002EFB\u0019\u0012\u0006\u0010>\u001a\u00020=\u0012\b\b\u0002\u0010A\u001a\u00020@¢\u0006\u0004\bC\u0010DJ\u001f\u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0002H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\u000b\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u001b\u0010\u000f\u001a\u00020\n2\n\u0010\u000e\u001a\u00060\u0003j\u0002`\rH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0002H\u0001¢\u0006\u0004\b\u0011\u0010\u0007J\u0019\u0010\u0015\u001a\u00020\u00142\n\u0010\u0013\u001a\u00060\u0003j\u0002`\u0004¢\u0006\u0004\b\u0015\u0010\u0016J\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u00052\n\u0010\u0013\u001a\u00060\u0003j\u0002`\u0004¢\u0006\u0004\b\u0017\u0010\u0018J\u001b\u0010\u0019\u001a\u00020\u00142\n\u0010\u0013\u001a\u00060\u0003j\u0002`\u0004H\u0007¢\u0006\u0004\b\u0019\u0010\u0016J!\u0010\u001b\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u001a2\n\u0010\u0013\u001a\u00060\u0003j\u0002`\u0004¢\u0006\u0004\b\u001b\u0010\u001cJ#\u0010\u001d\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00020\u001a¢\u0006\u0004\b\u001d\u0010\u001eJ\u0017\u0010!\u001a\u00020\n2\u0006\u0010 \u001a\u00020\u001fH\u0007¢\u0006\u0004\b!\u0010\"J\u0017\u0010#\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\bH\u0007¢\u0006\u0004\b#\u0010\fJ\u001b\u0010$\u001a\u00020\n2\n\u0010\u000e\u001a\u00060\u0003j\u0002`\rH\u0007¢\u0006\u0004\b$\u0010\u0010J\u0017\u0010'\u001a\u00020\n2\u0006\u0010&\u001a\u00020%H\u0007¢\u0006\u0004\b'\u0010(J\u0017\u0010*\u001a\u00020\n2\u0006\u0010 \u001a\u00020)H\u0007¢\u0006\u0004\b*\u0010+J\u0017\u0010,\u001a\u00020\n2\u0006\u0010&\u001a\u00020%H\u0007¢\u0006\u0004\b,\u0010(J\u0017\u0010.\u001a\u00020\n2\u0006\u0010 \u001a\u00020-H\u0007¢\u0006\u0004\b.\u0010/J\u0017\u00101\u001a\u00020\n2\u0006\u0010 \u001a\u000200H\u0007¢\u0006\u0004\b1\u00102J\u0017\u00105\u001a\u00020\n2\u0006\u00104\u001a\u000203H\u0007¢\u0006\u0004\b5\u00106J\u000f\u00107\u001a\u00020\nH\u0017¢\u0006\u0004\b7\u00108R&\u0010:\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u0005098\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b:\u0010;R&\u0010<\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b<\u0010;R\u0016\u0010>\u001a\u00020=8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b>\u0010?R\u0016\u0010A\u001a\u00020@8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bA\u0010B¨\u0006G"}, d2 = {"Lcom/discord/stores/StoreThreadsJoined;", "Lcom/discord/stores/StoreV2;", "", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/stores/StoreThreadsJoined$JoinedThread;", "getAllJoinedThreads", "()Ljava/util/Map;", "Lcom/discord/api/guild/Guild;", "guild", "", "saveThreads", "(Lcom/discord/api/guild/Guild;)V", "Lcom/discord/primitives/GuildId;", "guildId", "deleteThreads", "(J)V", "getAllJoinedThreadsInternal$app_productionGoogleRelease", "getAllJoinedThreadsInternal", "threadId", "", "hasJoined", "(J)Z", "getJoinedThread", "(J)Lcom/discord/stores/StoreThreadsJoined$JoinedThread;", "hasJoinedInternal", "Lrx/Observable;", "observeJoinedThread", "(J)Lrx/Observable;", "observeJoinedThreads", "()Lrx/Observable;", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "handleGuildCreate", "handleGuildDelete", "Lcom/discord/api/channel/Channel;", "channel", "handleThreadCreateOrUpdate", "(Lcom/discord/api/channel/Channel;)V", "Lcom/discord/models/thread/dto/ModelThreadListSync;", "handleThreadListSync", "(Lcom/discord/models/thread/dto/ModelThreadListSync;)V", "handleThreadDelete", "Lcom/discord/api/thread/ThreadMemberUpdate;", "handleThreadMemberUpdate", "(Lcom/discord/api/thread/ThreadMemberUpdate;)V", "Lcom/discord/api/thread/ThreadMembersUpdate;", "handleThreadMembersUpdate", "(Lcom/discord/api/thread/ThreadMembersUpdate;)V", "Lcom/discord/utilities/search/network/state/SearchState;", "searchState", "handleSearchFinish", "(Lcom/discord/utilities/search/network/state/SearchState;)V", "snapshotData", "()V", "", "joinedThreads", "Ljava/util/Map;", "joinedThreadsSnapshot", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreUser;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", HookHelper.constructorName, "(Lcom/discord/stores/StoreUser;Lcom/discord/stores/updates/ObservationDeck;)V", "Companion", "JoinedThread", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreThreadsJoined extends StoreV2 {
    public static final int ALL_FLAGS = 15;
    public static final Companion Companion = new Companion(null);
    public static final int NOTIFICATION_FLAGS = 14;
    private final Map<Long, JoinedThread> joinedThreads;
    private Map<Long, JoinedThread> joinedThreadsSnapshot;
    private final ObservationDeck observationDeck;
    private final StoreUser storeUser;

    /* compiled from: StoreThreadsJoined.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/stores/StoreThreadsJoined$Companion;", "", "", "ALL_FLAGS", "I", "NOTIFICATION_FLAGS", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreThreadsJoined.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u0014\b\u0086\b\u0018\u00002\u00020\u0001B?\u0012\n\u0010\u0014\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010\u0015\u001a\u00060\u0002j\u0002`\u0006\u0012\u0006\u0010\u0016\u001a\u00020\b\u0012\u0006\u0010\u0017\u001a\u00020\u000b\u0012\u0006\u0010\u0018\u001a\u00020\u000e\u0012\u0006\u0010\u0019\u001a\u00020\u0011¢\u0006\u0004\b.\u0010/J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\u0007\u001a\u00060\u0002j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0005J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013JT\u0010\u001a\u001a\u00020\u00002\f\b\u0002\u0010\u0014\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010\u0015\u001a\u00060\u0002j\u0002`\u00062\b\b\u0002\u0010\u0016\u001a\u00020\b2\b\b\u0002\u0010\u0017\u001a\u00020\u000b2\b\b\u0002\u0010\u0018\u001a\u00020\u000e2\b\b\u0002\u0010\u0019\u001a\u00020\u0011HÆ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u001d\u001a\u00020\u001cHÖ\u0001¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010\u001f\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u001f\u0010\nJ\u001a\u0010!\u001a\u00020\u000b2\b\u0010 \u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b!\u0010\"R\u0019\u0010\u0016\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010#\u001a\u0004\b$\u0010\nR\u001d\u0010\u0014\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010%\u001a\u0004\b&\u0010\u0005R\u0019\u0010\u0019\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010'\u001a\u0004\b(\u0010\u0013R\u001d\u0010\u0015\u001a\u00060\u0002j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010%\u001a\u0004\b)\u0010\u0005R\u0019\u0010\u0018\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010*\u001a\u0004\b+\u0010\u0010R\u0019\u0010\u0017\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010,\u001a\u0004\b-\u0010\r¨\u00060"}, d2 = {"Lcom/discord/stores/StoreThreadsJoined$JoinedThread;", "", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "Lcom/discord/primitives/GuildId;", "component2", "", "component3", "()I", "", "component4", "()Z", "Lcom/discord/models/domain/ModelMuteConfig;", "component5", "()Lcom/discord/models/domain/ModelMuteConfig;", "Lcom/discord/api/utcdatetime/UtcDateTime;", "component6", "()Lcom/discord/api/utcdatetime/UtcDateTime;", "threadId", "guildId", "flags", "muted", "muteConfig", "joinTimestamp", "copy", "(JJIZLcom/discord/models/domain/ModelMuteConfig;Lcom/discord/api/utcdatetime/UtcDateTime;)Lcom/discord/stores/StoreThreadsJoined$JoinedThread;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "I", "getFlags", "J", "getThreadId", "Lcom/discord/api/utcdatetime/UtcDateTime;", "getJoinTimestamp", "getGuildId", "Lcom/discord/models/domain/ModelMuteConfig;", "getMuteConfig", "Z", "getMuted", HookHelper.constructorName, "(JJIZLcom/discord/models/domain/ModelMuteConfig;Lcom/discord/api/utcdatetime/UtcDateTime;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class JoinedThread {
        private final int flags;
        private final long guildId;
        private final UtcDateTime joinTimestamp;
        private final ModelMuteConfig muteConfig;
        private final boolean muted;
        private final long threadId;

        public JoinedThread(long j, long j2, int i, boolean z2, ModelMuteConfig modelMuteConfig, UtcDateTime utcDateTime) {
            m.checkNotNullParameter(modelMuteConfig, "muteConfig");
            m.checkNotNullParameter(utcDateTime, "joinTimestamp");
            this.threadId = j;
            this.guildId = j2;
            this.flags = i;
            this.muted = z2;
            this.muteConfig = modelMuteConfig;
            this.joinTimestamp = utcDateTime;
        }

        public final long component1() {
            return this.threadId;
        }

        public final long component2() {
            return this.guildId;
        }

        public final int component3() {
            return this.flags;
        }

        public final boolean component4() {
            return this.muted;
        }

        public final ModelMuteConfig component5() {
            return this.muteConfig;
        }

        public final UtcDateTime component6() {
            return this.joinTimestamp;
        }

        public final JoinedThread copy(long j, long j2, int i, boolean z2, ModelMuteConfig modelMuteConfig, UtcDateTime utcDateTime) {
            m.checkNotNullParameter(modelMuteConfig, "muteConfig");
            m.checkNotNullParameter(utcDateTime, "joinTimestamp");
            return new JoinedThread(j, j2, i, z2, modelMuteConfig, utcDateTime);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof JoinedThread)) {
                return false;
            }
            JoinedThread joinedThread = (JoinedThread) obj;
            return this.threadId == joinedThread.threadId && this.guildId == joinedThread.guildId && this.flags == joinedThread.flags && this.muted == joinedThread.muted && m.areEqual(this.muteConfig, joinedThread.muteConfig) && m.areEqual(this.joinTimestamp, joinedThread.joinTimestamp);
        }

        public final int getFlags() {
            return this.flags;
        }

        public final long getGuildId() {
            return this.guildId;
        }

        public final UtcDateTime getJoinTimestamp() {
            return this.joinTimestamp;
        }

        public final ModelMuteConfig getMuteConfig() {
            return this.muteConfig;
        }

        public final boolean getMuted() {
            return this.muted;
        }

        public final long getThreadId() {
            return this.threadId;
        }

        public int hashCode() {
            int a = (((b.a(this.guildId) + (b.a(this.threadId) * 31)) * 31) + this.flags) * 31;
            boolean z2 = this.muted;
            if (z2) {
                z2 = true;
            }
            int i = z2 ? 1 : 0;
            int i2 = z2 ? 1 : 0;
            int i3 = (a + i) * 31;
            ModelMuteConfig modelMuteConfig = this.muteConfig;
            int i4 = 0;
            int hashCode = (i3 + (modelMuteConfig != null ? modelMuteConfig.hashCode() : 0)) * 31;
            UtcDateTime utcDateTime = this.joinTimestamp;
            if (utcDateTime != null) {
                i4 = utcDateTime.hashCode();
            }
            return hashCode + i4;
        }

        public String toString() {
            StringBuilder R = a.R("JoinedThread(threadId=");
            R.append(this.threadId);
            R.append(", guildId=");
            R.append(this.guildId);
            R.append(", flags=");
            R.append(this.flags);
            R.append(", muted=");
            R.append(this.muted);
            R.append(", muteConfig=");
            R.append(this.muteConfig);
            R.append(", joinTimestamp=");
            R.append(this.joinTimestamp);
            R.append(")");
            return R.toString();
        }
    }

    public /* synthetic */ StoreThreadsJoined(StoreUser storeUser, ObservationDeck observationDeck, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(storeUser, (i & 2) != 0 ? ObservationDeckProvider.get() : observationDeck);
    }

    private final void deleteThreads(long j) {
        if (r.removeAll(this.joinedThreads.values(), new StoreThreadsJoined$deleteThreads$removed$1(j))) {
            markChanged();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Map<Long, JoinedThread> getAllJoinedThreads() {
        return this.joinedThreadsSnapshot;
    }

    private final void saveThreads(Guild guild) {
        ThreadMember j;
        List<Channel> N = guild.N();
        if (N != null) {
            for (Channel channel : N) {
                if (ChannelUtils.C(channel) && (j = channel.j()) != null) {
                    this.joinedThreads.put(Long.valueOf(channel.h()), new JoinedThread(channel.h(), guild.r(), j.a(), j.e(), new ModelMuteConfig(j.d()), j.c()));
                    markChanged();
                }
            }
        }
    }

    @StoreThread
    public final Map<Long, JoinedThread> getAllJoinedThreadsInternal$app_productionGoogleRelease() {
        return this.joinedThreads;
    }

    public final JoinedThread getJoinedThread(long j) {
        return this.joinedThreadsSnapshot.get(Long.valueOf(j));
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        this.joinedThreads.clear();
        List<Guild> guilds = modelPayload.getGuilds();
        m.checkNotNullExpressionValue(guilds, "payload.guilds");
        for (Guild guild : guilds) {
            m.checkNotNullExpressionValue(guild, "guild");
            saveThreads(guild);
        }
        markChanged();
    }

    @StoreThread
    public final void handleGuildCreate(Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        deleteThreads(guild.r());
        saveThreads(guild);
    }

    @StoreThread
    public final void handleGuildDelete(long j) {
        deleteThreads(j);
    }

    @StoreThread
    public final void handleSearchFinish(SearchState searchState) {
        Channel channel;
        m.checkNotNullParameter(searchState, "searchState");
        List<Channel> threads = searchState.getThreads();
        if (!(threads == null || (channel = threads.get(0)) == null)) {
            long f = channel.f();
            List<ThreadMember> threadMembers = searchState.getThreadMembers();
            if (threadMembers != null) {
                for (ThreadMember threadMember : threadMembers) {
                    if (threadMember.f() == this.storeUser.getMe().getId()) {
                        this.joinedThreads.put(Long.valueOf(threadMember.b()), new JoinedThread(threadMember.b(), f, threadMember.a(), threadMember.e(), new ModelMuteConfig(threadMember.d()), threadMember.c()));
                    }
                }
            }
        }
    }

    @StoreThread
    public final void handleThreadCreateOrUpdate(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        ThreadMember j = channel.j();
        if (j != null && j.f() == this.storeUser.getMe().getId()) {
            this.joinedThreads.put(Long.valueOf(channel.h()), new JoinedThread(channel.h(), channel.f(), j.a(), j.e(), new ModelMuteConfig(j.d()), j.c()));
            markChanged();
        }
    }

    @StoreThread
    public final void handleThreadDelete(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        if (this.joinedThreads.containsKey(Long.valueOf(channel.h()))) {
            this.joinedThreads.remove(Long.valueOf(channel.h()));
            markChanged();
        }
    }

    @StoreThread
    public final void handleThreadListSync(ModelThreadListSync modelThreadListSync) {
        m.checkNotNullParameter(modelThreadListSync, "payload");
        List<ThreadMember> members = modelThreadListSync.getMembers();
        if (members != null) {
            for (ThreadMember threadMember : members) {
                this.joinedThreads.put(Long.valueOf(threadMember.b()), new JoinedThread(threadMember.b(), modelThreadListSync.getGuildId(), threadMember.a(), threadMember.e(), new ModelMuteConfig(threadMember.d()), threadMember.c()));
                markChanged();
            }
        }
    }

    @StoreThread
    public final void handleThreadMemberUpdate(ThreadMemberUpdate threadMemberUpdate) {
        m.checkNotNullParameter(threadMemberUpdate, "payload");
        if (threadMemberUpdate.g() == this.storeUser.getMe().getId()) {
            this.joinedThreads.put(Long.valueOf(threadMemberUpdate.c()), new JoinedThread(threadMemberUpdate.c(), threadMemberUpdate.b(), threadMemberUpdate.a(), threadMemberUpdate.f(), new ModelMuteConfig(threadMemberUpdate.e()), threadMemberUpdate.d()));
            markChanged();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    @StoreThread
    public final void handleThreadMembersUpdate(ThreadMembersUpdate threadMembersUpdate) {
        boolean z2;
        List<Long> d;
        m.checkNotNullParameter(threadMembersUpdate, "payload");
        long id2 = this.storeUser.getMe().getId();
        if (this.joinedThreads.containsKey(Long.valueOf(threadMembersUpdate.c())) && (d = threadMembersUpdate.d()) != null && d.contains(Long.valueOf(id2))) {
            this.joinedThreads.remove(Long.valueOf(threadMembersUpdate.c()));
            markChanged();
        }
        List<AugmentedThreadMember> a = threadMembersUpdate.a();
        AugmentedThreadMember augmentedThreadMember = null;
        if (a != null) {
            Iterator<T> it = a.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Object next = it.next();
                if (((AugmentedThreadMember) next).g() == id2) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    augmentedThreadMember = next;
                    break;
                }
            }
            augmentedThreadMember = augmentedThreadMember;
        }
        if (augmentedThreadMember != null) {
            this.joinedThreads.put(Long.valueOf(threadMembersUpdate.c()), new JoinedThread(threadMembersUpdate.c(), threadMembersUpdate.b(), augmentedThreadMember.a(), augmentedThreadMember.e(), new ModelMuteConfig(augmentedThreadMember.d()), augmentedThreadMember.b()));
            markChanged();
        }
    }

    public final boolean hasJoined(long j) {
        return this.joinedThreadsSnapshot.containsKey(Long.valueOf(j));
    }

    @StoreThread
    public final boolean hasJoinedInternal(long j) {
        return this.joinedThreads.containsKey(Long.valueOf(j));
    }

    public final Observable<JoinedThread> observeJoinedThread(long j) {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreThreadsJoined$observeJoinedThread$1(this, j), 14, null);
    }

    public final Observable<Map<Long, JoinedThread>> observeJoinedThreads() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreThreadsJoined$observeJoinedThreads$1(this), 14, null);
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        this.joinedThreadsSnapshot = new HashMap(this.joinedThreads);
    }

    public StoreThreadsJoined(StoreUser storeUser, ObservationDeck observationDeck) {
        m.checkNotNullParameter(storeUser, "storeUser");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.storeUser = storeUser;
        this.observationDeck = observationDeck;
        this.joinedThreads = new HashMap();
        this.joinedThreadsSnapshot = h0.emptyMap();
    }
}
