package com.discord.stores;

import com.discord.models.domain.ModelBan;
import d0.z.d.m;
import d0.z.d.o;
import java.util.HashMap;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreBans.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/models/domain/ModelBan;", "bans", "", "invoke", "(Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreBans$observeBans$1 extends o implements Function1<List<? extends ModelBan>, Unit> {
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ StoreBans this$0;

    /* compiled from: StoreBans.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreBans$observeBans$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ List $bans;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(List list) {
            super(0);
            this.$bans = list;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            HashMap hashMap;
            hashMap = StoreBans$observeBans$1.this.this$0.bannedUsers;
            Long valueOf = Long.valueOf(StoreBans$observeBans$1.this.$guildId);
            Object obj = hashMap.get(valueOf);
            if (obj == null) {
                obj = new HashMap();
                hashMap.put(valueOf, obj);
            }
            HashMap hashMap2 = (HashMap) obj;
            for (ModelBan modelBan : this.$bans) {
                hashMap2.put(Long.valueOf(modelBan.getUser().i()), modelBan);
            }
            StoreBans$observeBans$1.this.this$0.markChanged();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreBans$observeBans$1(StoreBans storeBans, long j) {
        super(1);
        this.this$0 = storeBans;
        this.$guildId = j;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(List<? extends ModelBan> list) {
        invoke2(list);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<? extends ModelBan> list) {
        Dispatcher dispatcher;
        m.checkNotNullParameter(list, "bans");
        dispatcher = this.this$0.dispatcher;
        dispatcher.schedule(new AnonymousClass1(list));
    }
}
