package com.discord.stores;

import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreCollapsedChannelCategories.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreCollapsedChannelCategories$setCollapsedCategory$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $categoryId;
    public final /* synthetic */ boolean $collapsed;
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ StoreCollapsedChannelCategories this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreCollapsedChannelCategories$setCollapsedCategory$1(StoreCollapsedChannelCategories storeCollapsedChannelCategories, long j, long j2, boolean z2) {
        super(0);
        this.this$0 = storeCollapsedChannelCategories;
        this.$guildId = j;
        this.$categoryId = j2;
        this.$collapsed = z2;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        this.this$0.setCollapsedState(this.$guildId, this.$categoryId, this.$collapsed);
    }
}
