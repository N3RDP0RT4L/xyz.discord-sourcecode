package com.discord.stores;

import com.discord.stores.StoreChannelsSelected;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreChannelsSelected.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;", "kotlin.jvm.PlatformType", "selected", "", "invoke", "(Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreChannelsSelected$validateSelectedChannel$3 extends o implements Function1<StoreChannelsSelected.ResolvedSelectedChannel, Unit> {
    public final /* synthetic */ StoreChannelsSelected this$0;

    /* compiled from: StoreChannelsSelected.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreChannelsSelected$validateSelectedChannel$3$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ StoreChannelsSelected.ResolvedSelectedChannel $selected;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(StoreChannelsSelected.ResolvedSelectedChannel resolvedSelectedChannel) {
            super(0);
            this.$selected = resolvedSelectedChannel;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            StoreChannelsSelected storeChannelsSelected = StoreChannelsSelected$validateSelectedChannel$3.this.this$0;
            StoreChannelsSelected.ResolvedSelectedChannel resolvedSelectedChannel = this.$selected;
            m.checkNotNullExpressionValue(resolvedSelectedChannel, "selected");
            storeChannelsSelected.onSelectedChannelResolved(resolvedSelectedChannel);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreChannelsSelected$validateSelectedChannel$3(StoreChannelsSelected storeChannelsSelected) {
        super(1);
        this.this$0 = storeChannelsSelected;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(StoreChannelsSelected.ResolvedSelectedChannel resolvedSelectedChannel) {
        invoke2(resolvedSelectedChannel);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(StoreChannelsSelected.ResolvedSelectedChannel resolvedSelectedChannel) {
        Dispatcher dispatcher;
        dispatcher = this.this$0.dispatcher;
        dispatcher.schedule(new AnonymousClass1(resolvedSelectedChannel));
    }
}
