package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.annotation.MainThread;
import androidx.appcompat.widget.ActivityChooserModel;
import androidx.fragment.app.FragmentActivity;
import com.discord.app.AppComponent;
import com.discord.app.AppLog;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.persister.Persister;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.Clock;
import d0.e0.c;
import d0.g;
import d0.g0.w;
import d0.t.u;
import d0.u.a;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.concurrent.TimeUnit;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.objectweb.asm.Opcodes;
import rx.Observable;
import rx.subjects.BehaviorSubject;
/* compiled from: StoreNotices.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u0000 E2\u00020\u0001:\u0004EFGHB\u0017\u0012\u0006\u00105\u001a\u000204\u0012\u0006\u0010?\u001a\u00020>¢\u0006\u0004\bC\u0010DJ\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0015\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u0005¢\u0006\u0004\b\u0007\u0010\bJ#\u0010\r\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\n\u0012\b\u0012\u00060\u000bj\u0002`\f0\t0\u0005¢\u0006\u0004\b\r\u0010\bJ\u0017\u0010\u0010\u001a\u00020\u00022\u0006\u0010\u000f\u001a\u00020\u000eH\u0016¢\u0006\u0004\b\u0010\u0010\u0011J\u0015\u0010\u0013\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u0006¢\u0006\u0004\b\u0013\u0010\u0014J\u0015\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0015\u001a\u00020\n¢\u0006\u0004\b\u0017\u0010\u0018J#\u0010\u001a\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\n2\f\b\u0002\u0010\u0019\u001a\u00060\u000bj\u0002`\f¢\u0006\u0004\b\u001a\u0010\u001bJ\r\u0010\u001c\u001a\u00020\u0002¢\u0006\u0004\b\u001c\u0010\u0004J\u0015\u0010\u001a\u001a\u00020\u00022\u0006\u0010\u001e\u001a\u00020\u001d¢\u0006\u0004\b\u001a\u0010\u001fJ\u0015\u0010!\u001a\u00020\u00022\u0006\u0010 \u001a\u00020\n¢\u0006\u0004\b!\u0010\"J\u0015\u0010#\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\n¢\u0006\u0004\b#\u0010\"R!\u0010(\u001a\u00060\u000bj\u0002`\f8F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'R$\u0010*\u001a\u00020\u000b2\u0006\u0010)\u001a\u00020\u000b8\u0002@BX\u0082\u000e¢\u0006\f\n\u0004\b*\u0010+\"\u0004\b,\u0010-R\u001e\u0010/\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060.8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b/\u00100R&\u00102\u001a\u0012\u0012\u0004\u0012\u000201\u0012\b\u0012\u00060\u000bj\u0002`\f0\t8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b2\u00103R\u0016\u00105\u001a\u0002048\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b5\u00106R,\u00108\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\n\u0012\b\u0012\u00060\u000bj\u0002`\f0\t078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b8\u00109R\u001c\u0010;\u001a\b\u0012\u0004\u0012\u00020\u00060:8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b;\u0010<R\u001c\u0010=\u001a\b\u0012\u0004\u0012\u00020\u00020.8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b=\u00100R\u0019\u0010?\u001a\u00020>8\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010@\u001a\u0004\bA\u0010B¨\u0006I"}, d2 = {"Lcom/discord/stores/StoreNotices;", "Lcom/discord/stores/Store;", "", "processNextNotice", "()V", "Lrx/Observable;", "Lcom/discord/stores/StoreNotices$Notice;", "getNotices", "()Lrx/Observable;", "Ljava/util/HashMap;", "", "", "Lcom/discord/primitives/Timestamp;", "observeNoticesSeen", "Landroid/content/Context;", "context", "init", "(Landroid/content/Context;)V", "notice", "requestToShow", "(Lcom/discord/stores/StoreNotices$Notice;)V", "noticeName", "", "hasBeenSeen", "(Ljava/lang/String;)Z", "seenAtMs", "markSeen", "(Ljava/lang/String;J)V", "markInAppSeen", "Lcom/discord/stores/StoreNotices$Dialog$Type;", "type", "(Lcom/discord/stores/StoreNotices$Dialog$Type;)V", "noticeDialogType", "markDialogSeen", "(Ljava/lang/String;)V", "clearSeen", "firstUseTimestamp$delegate", "Lkotlin/Lazy;", "getFirstUseTimestamp", "()J", "firstUseTimestamp", "value", "pollRateMs", "J", "setPollRateMs", "(J)V", "Lrx/subjects/BehaviorSubject;", "noticePublisher", "Lrx/subjects/BehaviorSubject;", "", "lastShownTimes", "Ljava/util/HashMap;", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "Lcom/discord/utilities/persister/Persister;", "noticesSeenCache", "Lcom/discord/utilities/persister/Persister;", "Ljava/util/PriorityQueue;", "noticeQueue", "Ljava/util/PriorityQueue;", "processTrigger", "Lcom/discord/stores/StoreStream;", "stream", "Lcom/discord/stores/StoreStream;", "getStream", "()Lcom/discord/stores/StoreStream;", HookHelper.constructorName, "(Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreStream;)V", "Companion", "Dialog", "Notice", "PassiveNotice", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreNotices extends Store {
    public static final Companion Companion = new Companion(null);
    public static final String IN_APP_NOTICE_TAG = "InAppNotif";
    public static final String NOTICE_POPUP_TAG = "Popup";
    public static final int PRIORITY_HIGH = 0;
    public static final int PRIORITY_INAPP_NOTIFICATION = 1;
    public static final int PRIORITY_PASSIVE_NOTICE = 10;
    public static final int PRIORITY_USER_SURVEY = 5;
    public static final long PROCESS_PERIOD_MS = 30000;
    public static final long PROCESS_THROTTLE_MS = 50;
    private final Clock clock;
    private final BehaviorSubject<Notice> noticePublisher;
    private final BehaviorSubject<Unit> processTrigger;
    private final StoreStream stream;
    private final Persister<HashMap<String, Long>> noticesSeenCache = new Persister<>("NOTICES_SHOWN_V2", new HashMap());
    private final PriorityQueue<Notice> noticeQueue = new PriorityQueue<>(11, a.compareBy(StoreNotices$noticeQueue$1.INSTANCE, StoreNotices$noticeQueue$2.INSTANCE, StoreNotices$noticeQueue$3.INSTANCE));
    private long pollRateMs = 30000;
    private HashMap<Integer, Long> lastShownTimes = new HashMap<>();
    private final Lazy firstUseTimestamp$delegate = g.lazy(new StoreNotices$firstUseTimestamp$2(this));

    /* compiled from: StoreNotices.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0007\u0010\bR\u0016\u0010\t\u001a\u00020\u00068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\t\u0010\bR\u0016\u0010\n\u001a\u00020\u00068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\n\u0010\bR\u0016\u0010\u000b\u001a\u00020\u00068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000b\u0010\bR\u0016\u0010\r\u001a\u00020\f8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0016\u0010\u000f\u001a\u00020\f8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000f\u0010\u000e¨\u0006\u0012"}, d2 = {"Lcom/discord/stores/StoreNotices$Companion;", "", "", "IN_APP_NOTICE_TAG", "Ljava/lang/String;", "NOTICE_POPUP_TAG", "", "PRIORITY_HIGH", "I", "PRIORITY_INAPP_NOTIFICATION", "PRIORITY_PASSIVE_NOTICE", "PRIORITY_USER_SURVEY", "", "PROCESS_PERIOD_MS", "J", "PROCESS_THROTTLE_MS", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreNotices.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u001cB'\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\u0016\b\u0002\u0010\n\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001e\u0010\u0007\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ2\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\t\u001a\u00020\u00022\u0016\b\u0002\u0010\n\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R'\u0010\n\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0016\u001a\u0004\b\u0017\u0010\bR\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0018\u001a\u0004\b\u0019\u0010\u0004¨\u0006\u001d"}, d2 = {"Lcom/discord/stores/StoreNotices$Dialog;", "", "Lcom/discord/stores/StoreNotices$Dialog$Type;", "component1", "()Lcom/discord/stores/StoreNotices$Dialog$Type;", "", "", "component2", "()Ljava/util/Map;", "type", "metadata", "copy", "(Lcom/discord/stores/StoreNotices$Dialog$Type;Ljava/util/Map;)Lcom/discord/stores/StoreNotices$Dialog;", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getMetadata", "Lcom/discord/stores/StoreNotices$Dialog$Type;", "getType", HookHelper.constructorName, "(Lcom/discord/stores/StoreNotices$Dialog$Type;Ljava/util/Map;)V", "Type", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Dialog {
        private final Map<String, Object> metadata;
        private final Type type;

        /* compiled from: StoreNotices.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rJ5\u0010\n\u001a\u00020\t2\b\b\u0002\u0010\u0003\u001a\u00020\u00022\b\b\u0002\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0006\u001a\u00020\u00042\b\b\u0002\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\n\u0010\u000bj\u0002\b\u000ej\u0002\b\u000f¨\u0006\u0010"}, d2 = {"Lcom/discord/stores/StoreNotices$Dialog$Type;", "", "", "priority", "", "sinceLastPeriodMs", "delayPeriodMs", "", "persist", "Lcom/discord/stores/StoreNotices$PassiveNotice;", "buildPassiveNotice", "(IJJZ)Lcom/discord/stores/StoreNotices$PassiveNotice;", HookHelper.constructorName, "(Ljava/lang/String;I)V", "REQUEST_RATING_MODAL", "DELETE_CONNECTION_MODAL", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public enum Type {
            REQUEST_RATING_MODAL,
            DELETE_CONNECTION_MODAL;

            public static /* synthetic */ PassiveNotice buildPassiveNotice$default(Type type, int i, long j, long j2, boolean z2, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = 10;
                }
                if ((i2 & 2) != 0) {
                    j = 31536000000L;
                }
                long j3 = j;
                if ((i2 & 4) != 0) {
                    j2 = 21600000;
                }
                return type.buildPassiveNotice(i, j3, j2, (i2 & 8) != 0 ? false : z2);
            }

            public final PassiveNotice buildPassiveNotice(int i, long j, long j2, boolean z2) {
                return new PassiveNotice(name(), i, z2, j, j2, new StoreNotices$Dialog$Type$buildPassiveNotice$1(this));
            }
        }

        public Dialog(Type type, Map<String, ? extends Object> map) {
            m.checkNotNullParameter(type, "type");
            this.type = type;
            this.metadata = map;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ Dialog copy$default(Dialog dialog, Type type, Map map, int i, Object obj) {
            if ((i & 1) != 0) {
                type = dialog.type;
            }
            if ((i & 2) != 0) {
                map = dialog.metadata;
            }
            return dialog.copy(type, map);
        }

        public final Type component1() {
            return this.type;
        }

        public final Map<String, Object> component2() {
            return this.metadata;
        }

        public final Dialog copy(Type type, Map<String, ? extends Object> map) {
            m.checkNotNullParameter(type, "type");
            return new Dialog(type, map);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Dialog)) {
                return false;
            }
            Dialog dialog = (Dialog) obj;
            return m.areEqual(this.type, dialog.type) && m.areEqual(this.metadata, dialog.metadata);
        }

        public final Map<String, Object> getMetadata() {
            return this.metadata;
        }

        public final Type getType() {
            return this.type;
        }

        public int hashCode() {
            Type type = this.type;
            int i = 0;
            int hashCode = (type != null ? type.hashCode() : 0) * 31;
            Map<String, Object> map = this.metadata;
            if (map != null) {
                i = map.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("Dialog(type=");
            R.append(this.type);
            R.append(", metadata=");
            return b.d.b.a.a.L(R, this.metadata, ")");
        }

        public /* synthetic */ Dialog(Type type, Map map, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(type, (i & 2) != 0 ? null : map);
        }
    }

    /* compiled from: StoreNotices.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0013\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\t\b\u0016\u0018\u00002\u00020\u0001B\u0087\u0001\u0012\u0006\u00103\u001a\u00020\u0014\u0012\b\b\u0002\u0010%\u001a\u00020$\u0012\f\b\u0002\u00101\u001a\u00060\u0006j\u0002`\u0007\u0012\b\b\u0002\u0010)\u001a\u00020\f\u0012\b\b\u0002\u00106\u001a\u00020\u0002\u0012\u0018\b\u0002\u0010;\u001a\u0012\u0012\f\u0012\n\u0012\u0006\b\u0001\u0012\u00020:09\u0018\u000108\u0012\b\b\u0002\u0010?\u001a\u00020\u0006\u0012\b\b\u0002\u0010\"\u001a\u00020\u0002\u0012\b\b\u0002\u0010-\u001a\u00020\u0006\u0012\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00020\u0017¢\u0006\u0004\bA\u0010BJ\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\r\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0004J\u001f\u0010\t\u001a\u00020\u00022\u000e\u0010\b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007H\u0016¢\u0006\u0004\b\t\u0010\nJ'\u0010\u000e\u001a\u00020\u00022\u0016\u0010\r\u001a\u0012\u0012\u0004\u0012\u00020\f\u0012\b\u0012\u00060\u0006j\u0002`\u00070\u000bH\u0016¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0012\u001a\u00020\u00022\u0006\u0010\u0011\u001a\u00020\u0010H\u0007¢\u0006\u0004\b\u0012\u0010\u0013J\u000f\u0010\u0015\u001a\u00020\u0014H\u0016¢\u0006\u0004\b\u0015\u0010\u0016R(\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00020\u00178\u0004@\u0004X\u0084\u0004¢\u0006\f\n\u0004\b\u0012\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR(\u0010\u001b\u001a\u00020\u00028\u0004@\u0004X\u0084\u000e¢\u0006\u0018\n\u0004\b\u001b\u0010\u001c\u0012\u0004\b \u0010!\u001a\u0004\b\u001d\u0010\u0004\"\u0004\b\u001e\u0010\u001fR\u0019\u0010\"\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u001c\u001a\u0004\b#\u0010\u0004R\u0019\u0010%\u001a\u00020$8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010&\u001a\u0004\b'\u0010(R\u0019\u0010)\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010*\u001a\u0004\b+\u0010,R\u001c\u0010-\u001a\u00020\u00068\u0004@\u0004X\u0084\u0004¢\u0006\f\n\u0004\b-\u0010.\u001a\u0004\b/\u00100R\u001d\u00101\u001a\u00060\u0006j\u0002`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010.\u001a\u0004\b2\u00100R\u0019\u00103\u001a\u00020\u00148\u0006@\u0006¢\u0006\f\n\u0004\b3\u00104\u001a\u0004\b5\u0010\u0016R\u0019\u00106\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010\u001c\u001a\u0004\b7\u0010\u0004R)\u0010;\u001a\u0012\u0012\f\u0012\n\u0012\u0006\b\u0001\u0012\u00020:09\u0018\u0001088\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010<\u001a\u0004\b=\u0010>R\u0019\u0010?\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010.\u001a\u0004\b@\u00100¨\u0006C"}, d2 = {"Lcom/discord/stores/StoreNotices$Notice;", "", "", "isInAppNotification", "()Z", "isPopup", "", "Lcom/discord/primitives/Timestamp;", "lastSeenMs", "canShow", "(Ljava/lang/Long;)Z", "", "", "lastShownTimes", "shouldShow", "(Ljava/util/Map;)Z", "Landroidx/fragment/app/FragmentActivity;", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "show", "(Landroidx/fragment/app/FragmentActivity;)Z", "", "toString", "()Ljava/lang/String;", "Lkotlin/Function1;", "Lkotlin/jvm/functions/Function1;", "getShow", "()Lkotlin/jvm/functions/Function1;", "hasShown", "Z", "getHasShown", "setHasShown", "(Z)V", "getHasShown$annotations", "()V", "autoMarkSeen", "getAutoMarkSeen", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "getClock", "()Lcom/discord/utilities/time/Clock;", "priority", "I", "getPriority", "()I", "sinceLastPeriodMs", "J", "getSinceLastPeriodMs", "()J", "requestedShowTimestamp", "getRequestedShowTimestamp", ModelAuditLogEntry.CHANGE_KEY_NAME, "Ljava/lang/String;", "getName", "persist", "getPersist", "", "Ld0/e0/c;", "Lcom/discord/app/AppComponent;", "validScreens", "Ljava/util/List;", "getValidScreens", "()Ljava/util/List;", "delayPeriodMs", "getDelayPeriodMs", HookHelper.constructorName, "(Ljava/lang/String;Lcom/discord/utilities/time/Clock;JIZLjava/util/List;JZJLkotlin/jvm/functions/Function1;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static class Notice {
        private final boolean autoMarkSeen;
        private final Clock clock;
        private final long delayPeriodMs;
        private boolean hasShown;
        private final String name;
        private final boolean persist;
        private final int priority;
        private final long requestedShowTimestamp;
        private final Function1<FragmentActivity, Boolean> show;
        private final long sinceLastPeriodMs;
        private final List<c<? extends AppComponent>> validScreens;

        /* JADX WARN: Multi-variable type inference failed */
        public Notice(String str, Clock clock, long j, int i, boolean z2, List<? extends c<? extends AppComponent>> list, long j2, boolean z3, long j3, Function1<? super FragmentActivity, Boolean> function1) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkNotNullParameter(clock, "clock");
            m.checkNotNullParameter(function1, "show");
            this.name = str;
            this.clock = clock;
            this.requestedShowTimestamp = j;
            this.priority = i;
            this.persist = z2;
            this.validScreens = list;
            this.delayPeriodMs = j2;
            this.autoMarkSeen = z3;
            this.sinceLastPeriodMs = j3;
            this.show = function1;
        }

        public static /* synthetic */ void getHasShown$annotations() {
        }

        public boolean canShow(Long l) {
            return l == null || this.clock.currentTimeMillis() - l.longValue() > this.sinceLastPeriodMs;
        }

        public final boolean getAutoMarkSeen() {
            return this.autoMarkSeen;
        }

        public final Clock getClock() {
            return this.clock;
        }

        public final long getDelayPeriodMs() {
            return this.delayPeriodMs;
        }

        public final boolean getHasShown() {
            return this.hasShown;
        }

        public final String getName() {
            return this.name;
        }

        public final boolean getPersist() {
            return this.persist;
        }

        public final int getPriority() {
            return this.priority;
        }

        public final long getRequestedShowTimestamp() {
            return this.requestedShowTimestamp;
        }

        public final Function1<FragmentActivity, Boolean> getShow() {
            return this.show;
        }

        public final long getSinceLastPeriodMs() {
            return this.sinceLastPeriodMs;
        }

        public final List<c<? extends AppComponent>> getValidScreens() {
            return this.validScreens;
        }

        public final boolean isInAppNotification() {
            return w.contains((CharSequence) this.name, (CharSequence) StoreNotices.IN_APP_NOTICE_TAG, true);
        }

        public final boolean isPopup() {
            return w.contains((CharSequence) this.name, (CharSequence) StoreNotices.NOTICE_POPUP_TAG, true);
        }

        public final void setHasShown(boolean z2) {
            this.hasShown = z2;
        }

        public boolean shouldShow(Map<Integer, Long> map) {
            m.checkNotNullParameter(map, "lastShownTimes");
            Long l = map.get(Integer.valueOf(this.priority));
            return this.delayPeriodMs < this.clock.currentTimeMillis() - (l != null ? l.longValue() : 0L);
        }

        @MainThread
        public final boolean show(FragmentActivity fragmentActivity) {
            m.checkNotNullParameter(fragmentActivity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
            if (this.hasShown || fragmentActivity.isFinishing()) {
                return false;
            }
            Boolean invoke = this.show.invoke(fragmentActivity);
            this.hasShown = invoke.booleanValue();
            return invoke.booleanValue();
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("Notice<");
            R.append(this.name);
            R.append(">(pri=");
            R.append(this.priority);
            R.append(", ts=");
            R.append(this.requestedShowTimestamp);
            R.append(')');
            return R.toString();
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public /* synthetic */ Notice(java.lang.String r17, com.discord.utilities.time.Clock r18, long r19, int r21, boolean r22, java.util.List r23, long r24, boolean r26, long r27, kotlin.jvm.functions.Function1 r29, int r30, kotlin.jvm.internal.DefaultConstructorMarker r31) {
            /*
                r16 = this;
                r0 = r30
                r1 = r0 & 2
                if (r1 == 0) goto Lc
                com.discord.utilities.time.Clock r1 = com.discord.utilities.time.ClockFactory.get()
                r4 = r1
                goto Le
            Lc:
                r4 = r18
            Le:
                r1 = r0 & 4
                if (r1 == 0) goto L18
                long r1 = r4.currentTimeMillis()
                r5 = r1
                goto L1a
            L18:
                r5 = r19
            L1a:
                r1 = r0 & 8
                if (r1 == 0) goto L23
                r1 = 10
                r7 = 10
                goto L25
            L23:
                r7 = r21
            L25:
                r1 = r0 & 16
                r2 = 0
                if (r1 == 0) goto L2c
                r8 = 0
                goto L2e
            L2c:
                r8 = r22
            L2e:
                r1 = r0 & 32
                if (r1 == 0) goto L4c
                r1 = 2
                d0.e0.c[] r1 = new d0.e0.c[r1]
                java.lang.Class<com.discord.widgets.home.WidgetHome> r3 = com.discord.widgets.home.WidgetHome.class
                d0.e0.c r3 = d0.z.d.a0.getOrCreateKotlinClass(r3)
                r1[r2] = r3
                r3 = 1
                java.lang.Class<com.discord.widgets.voice.fullscreen.WidgetCallFullscreen> r9 = com.discord.widgets.voice.fullscreen.WidgetCallFullscreen.class
                d0.e0.c r9 = d0.z.d.a0.getOrCreateKotlinClass(r9)
                r1[r3] = r9
                java.util.List r1 = d0.t.n.listOf(r1)
                r9 = r1
                goto L4e
            L4c:
                r9 = r23
            L4e:
                r1 = r0 & 64
                if (r1 == 0) goto L55
                r10 = 15000(0x3a98, double:7.411E-320)
                goto L57
            L55:
                r10 = r24
            L57:
                r1 = r0 & 128(0x80, float:1.794E-43)
                if (r1 == 0) goto L5d
                r12 = 0
                goto L5f
            L5d:
                r12 = r26
            L5f:
                r0 = r0 & 256(0x100, float:3.59E-43)
                if (r0 == 0) goto L6a
                r0 = 31536000000(0x757b12c00, double:1.55808542072E-313)
                r13 = r0
                goto L6c
            L6a:
                r13 = r27
            L6c:
                r2 = r16
                r3 = r17
                r15 = r29
                r2.<init>(r3, r4, r5, r7, r8, r9, r10, r12, r13, r15)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreNotices.Notice.<init>(java.lang.String, com.discord.utilities.time.Clock, long, int, boolean, java.util.List, long, boolean, long, kotlin.jvm.functions.Function1, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
        }
    }

    /* compiled from: StoreNotices.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\b\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0016\u0018\u00002\u00020\u0001BI\u0012\u0006\u0010\u000b\u001a\u00020\n\u0012\u0006\u0010\f\u001a\u00020\u0003\u0012\b\b\u0002\u0010\r\u001a\u00020\u0007\u0012\b\b\u0002\u0010\u000e\u001a\u00020\u0004\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u0004\u0012\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00070\u0010¢\u0006\u0004\b\u0013\u0010\u0014J'\u0010\b\u001a\u00020\u00072\u0016\u0010\u0006\u001a\u0012\u0012\u0004\u0012\u00020\u0003\u0012\b\u0012\u00060\u0004j\u0002`\u00050\u0002H\u0016¢\u0006\u0004\b\b\u0010\t¨\u0006\u0015"}, d2 = {"Lcom/discord/stores/StoreNotices$PassiveNotice;", "Lcom/discord/stores/StoreNotices$Notice;", "", "", "", "Lcom/discord/primitives/Timestamp;", "lastShownTimes", "", "shouldShow", "(Ljava/util/Map;)Z", "", ModelAuditLogEntry.CHANGE_KEY_NAME, "priority", "persist", "sinceLastPeriodMs", "delayPeriodMs", "Lkotlin/Function1;", "Landroidx/fragment/app/FragmentActivity;", "show", HookHelper.constructorName, "(Ljava/lang/String;IZJJLkotlin/jvm/functions/Function1;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static class PassiveNotice extends Notice {
        public /* synthetic */ PassiveNotice(String str, int i, boolean z2, long j, long j2, Function1 function1, int i2, DefaultConstructorMarker defaultConstructorMarker) {
            this(str, i, (i2 & 4) != 0 ? false : z2, (i2 & 8) != 0 ? 31536000000L : j, (i2 & 16) != 0 ? 21600000L : j2, function1);
        }

        @Override // com.discord.stores.StoreNotices.Notice
        public boolean shouldShow(Map<Integer, Long> map) {
            m.checkNotNullParameter(map, "lastShownTimes");
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            Iterator<Map.Entry<Integer, Long>> it = map.entrySet().iterator();
            while (true) {
                boolean z2 = true;
                if (!it.hasNext()) {
                    break;
                }
                Map.Entry<Integer, Long> next = it.next();
                if (next.getKey().intValue() > getPriority()) {
                    z2 = false;
                }
                if (z2) {
                    linkedHashMap.put(next.getKey(), next.getValue());
                }
            }
            Long l = (Long) u.maxOrNull((Iterable<? extends Comparable>) linkedHashMap.values());
            return getDelayPeriodMs() < getClock().currentTimeMillis() - (l != null ? l.longValue() : 0L);
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public PassiveNotice(String str, int i, boolean z2, long j, long j2, Function1<? super FragmentActivity, Boolean> function1) {
            super(str, null, 0L, i, z2, null, j2, false, j, function1, Opcodes.IF_ACMPNE, null);
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkNotNullParameter(function1, "show");
        }
    }

    public StoreNotices(Clock clock, StoreStream storeStream) {
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(storeStream, "stream");
        this.clock = clock;
        this.stream = storeStream;
        BehaviorSubject<Unit> k0 = BehaviorSubject.k0();
        m.checkNotNullExpressionValue(k0, "BehaviorSubject.create()");
        this.processTrigger = k0;
        BehaviorSubject<Notice> l0 = BehaviorSubject.l0(null);
        m.checkNotNullExpressionValue(l0, "BehaviorSubject.create(null as Notice?)");
        this.noticePublisher = l0;
    }

    public static /* synthetic */ void markSeen$default(StoreNotices storeNotices, String str, long j, int i, Object obj) {
        if ((i & 2) != 0) {
            j = storeNotices.clock.currentTimeMillis();
        }
        storeNotices.markSeen(str, j);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void processNextNotice() {
        Object obj;
        Iterator<T> it = this.noticeQueue.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (((Notice) obj).shouldShow(this.lastShownTimes)) {
                break;
            }
        }
        this.noticePublisher.onNext((Notice) obj);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void setPollRateMs(long j) {
        this.pollRateMs = Math.max(500L, j);
    }

    public final synchronized void clearSeen(String str) {
        m.checkNotNullParameter(str, "noticeName");
        this.noticesSeenCache.getAndSet(true, new StoreNotices$clearSeen$1(str));
    }

    public final long getFirstUseTimestamp() {
        return ((Number) this.firstUseTimestamp$delegate.getValue()).longValue();
    }

    public final Observable<Notice> getNotices() {
        Observable<Notice> q = this.noticePublisher.q();
        m.checkNotNullExpressionValue(q, "noticePublisher.distinctUntilChanged()");
        return q;
    }

    public final StoreStream getStream() {
        return this.stream;
    }

    public final synchronized boolean hasBeenSeen(String str) {
        m.checkNotNullParameter(str, "noticeName");
        return this.noticesSeenCache.get().get(str) != null;
    }

    @Override // com.discord.stores.Store
    public void init(Context context) {
        m.checkNotNullParameter(context, "context");
        super.init(context);
        AppLog appLog = AppLog.g;
        Logger.d$default(appLog, "Notices init", null, 2, null);
        Logger.d$default(appLog, "Notices prev seen: " + this.noticesSeenCache.get().entrySet(), null, 2, null);
        Observable o = this.processTrigger.Y(new b<Unit, Observable<? extends Long>>() { // from class: com.discord.stores.StoreNotices$init$1
            public final Observable<? extends Long> call(Unit unit) {
                long j;
                j = StoreNotices.this.pollRateMs;
                return Observable.D(0L, j, TimeUnit.MILLISECONDS);
            }
        }).o(50L, TimeUnit.MILLISECONDS);
        m.checkNotNullExpressionValue(o, "processTrigger\n        .…S, TimeUnit.MILLISECONDS)");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.computationLatest(o), StoreNotices.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new StoreNotices$init$3(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreNotices$init$2(this));
    }

    public final void markDialogSeen(String str) {
        m.checkNotNullParameter(str, "noticeDialogType");
        try {
            markSeen(Dialog.Type.valueOf(str));
        } catch (IllegalArgumentException unused) {
            markSeen$default(this, str, 0L, 2, null);
        }
    }

    public final synchronized void markInAppSeen() {
        PriorityQueue<Notice> priorityQueue = this.noticeQueue;
        ArrayList<Notice> arrayList = new ArrayList();
        for (Object obj : priorityQueue) {
            if (w.contains$default((CharSequence) ((Notice) obj).getName(), (CharSequence) IN_APP_NOTICE_TAG, false, 2, (Object) null)) {
                arrayList.add(obj);
            }
        }
        for (Notice notice : arrayList) {
            markSeen$default(this, notice.getName(), 0L, 2, null);
        }
    }

    public final synchronized void markSeen(String str, long j) {
        Object obj;
        Object obj2;
        m.checkNotNullParameter(str, "noticeName");
        AppLog.i("Notice seen: " + str + " @ " + j);
        Iterator<T> it = this.noticeQueue.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (m.areEqual(((Notice) obj).getName(), str)) {
                break;
            }
        }
        Notice notice = (Notice) obj;
        if (notice != null) {
            this.noticeQueue.remove(notice);
            if (notice.getDelayPeriodMs() <= this.pollRateMs) {
                Iterator<T> it2 = this.noticeQueue.iterator();
                if (!it2.hasNext()) {
                    obj2 = null;
                } else {
                    obj2 = it2.next();
                    if (it2.hasNext()) {
                        long delayPeriodMs = ((Notice) obj2).getDelayPeriodMs();
                        do {
                            Object next = it2.next();
                            long delayPeriodMs2 = ((Notice) next).getDelayPeriodMs();
                            if (delayPeriodMs > delayPeriodMs2) {
                                obj2 = next;
                                delayPeriodMs = delayPeriodMs2;
                            }
                        } while (it2.hasNext());
                    }
                }
                Notice notice2 = (Notice) obj2;
                setPollRateMs(notice2 != null ? notice2.getDelayPeriodMs() : 30000L);
            }
            this.lastShownTimes.put(Integer.valueOf(notice.getPriority()), Long.valueOf(j));
            if (notice.getPersist()) {
                HashMap<String, Long> hashMap = this.noticesSeenCache.get();
                hashMap.put(notice.getName(), Long.valueOf(j));
                this.noticesSeenCache.set(hashMap, true);
                Logger.v$default(AppLog.g, "Notice seen saved: " + str + " @ " + j, null, 2, null);
            }
            this.noticePublisher.onNext(null);
        }
        this.processTrigger.onNext(Unit.a);
    }

    public final Observable<HashMap<String, Long>> observeNoticesSeen() {
        return this.noticesSeenCache.getObservable();
    }

    public final synchronized void requestToShow(Notice notice) {
        m.checkNotNullParameter(notice, "notice");
        if (notice.canShow(this.noticesSeenCache.get().get(notice.getName()))) {
            PriorityQueue<Notice> priorityQueue = this.noticeQueue;
            boolean z2 = true;
            if (!(priorityQueue instanceof Collection) || !priorityQueue.isEmpty()) {
                Iterator<T> it = priorityQueue.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (m.areEqual(((Notice) it.next()).getName(), notice.getName())) {
                            z2 = false;
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
            if (z2) {
                this.noticeQueue.add(notice);
                AppLog.i("Notice Request: " + notice.getName());
                AppLog appLog = AppLog.g;
                Logger.d$default(appLog, "Notice queues: " + this.noticeQueue, null, 2, null);
                long delayPeriodMs = notice.getDelayPeriodMs();
                if (delayPeriodMs < this.pollRateMs) {
                    setPollRateMs(delayPeriodMs);
                }
                this.processTrigger.onNext(Unit.a);
            }
        }
    }

    public final void markSeen(Dialog.Type type) {
        m.checkNotNullParameter(type, "type");
        markSeen$default(this, type.name(), 0L, 2, null);
    }
}
