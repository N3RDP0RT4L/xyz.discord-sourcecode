package com.discord.stores;

import com.discord.stores.StoreMediaFavorites;
import d0.e0.c;
import d0.t.u;
import d0.z.d.a0;
import d0.z.d.o;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreMediaFavorites.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u0010\u0012\f\u0012\n\u0012\u0006\b\u0001\u0012\u00020\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "Ld0/e0/c;", "Lcom/discord/stores/StoreMediaFavorites$Favorite;", "invoke", "()Ljava/util/Set;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMediaFavorites$Favorite$Companion$AllTypes$2 extends o implements Function0<Set<? extends c<? extends StoreMediaFavorites.Favorite>>> {
    public static final StoreMediaFavorites$Favorite$Companion$AllTypes$2 INSTANCE = new StoreMediaFavorites$Favorite$Companion$AllTypes$2();

    public StoreMediaFavorites$Favorite$Companion$AllTypes$2() {
        super(0);
    }

    @Override // kotlin.jvm.functions.Function0
    public final Set<? extends c<? extends StoreMediaFavorites.Favorite>> invoke() {
        return u.toSet(a0.getOrCreateKotlinClass(StoreMediaFavorites.Favorite.class).getSealedSubclasses());
    }
}
