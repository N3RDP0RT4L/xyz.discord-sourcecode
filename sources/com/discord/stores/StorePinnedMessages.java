package com.discord.stores;

import andhook.lib.HookHelper;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.models.message.Message;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StorePinnedMessages.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u001e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010#\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0010!\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010$\n\u0002\b\u0005\u0018\u0000 32\u00020\u0001:\u00013B\u0017\u0012\u0006\u0010&\u001a\u00020%\u0012\u0006\u0010+\u001a\u00020*¢\u0006\u0004\b1\u00102J\u001b\u0010\u0006\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J)\u0010\u000b\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0003¢\u0006\u0004\b\u000b\u0010\fJ%\u0010\u000e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b0\r2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ-\u0010\u0012\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\t0\r2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0011\u001a\u00060\u0002j\u0002`\u0010¢\u0006\u0004\b\u0012\u0010\u0013J1\u0010\u0016\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0014\u0010\u0015\u001a\u0010\u0012\f\u0012\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00100\u0014H\u0007¢\u0006\u0004\b\u0016\u0010\u0017J\u0017\u0010\u001a\u001a\u00020\u00052\u0006\u0010\u0019\u001a\u00020\u0018H\u0007¢\u0006\u0004\b\u001a\u0010\u001bJ\u000f\u0010\u001c\u001a\u00020\u0005H\u0017¢\u0006\u0004\b\u001c\u0010\u001dR \u0010\u001f\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 R,\u0010#\u001a\u0018\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\"0!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$R\u0019\u0010&\u001a\u00020%8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010'\u001a\u0004\b(\u0010)R\u0019\u0010+\u001a\u00020*8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010,\u001a\u0004\b-\u0010.R,\u00100\u001a\u0018\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b0/8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b0\u0010$¨\u00064"}, d2 = {"Lcom/discord/stores/StorePinnedMessages;", "Lcom/discord/stores/StoreV2;", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "loadPinnedMessages", "(J)V", "", "Lcom/discord/models/message/Message;", "messages", "handlePinnedMessagesLoaded", "(JLjava/util/List;)V", "Lrx/Observable;", "observeForChannel", "(J)Lrx/Observable;", "Lcom/discord/primitives/MessageId;", "messageId", "observePinnedMessage", "(JJ)Lrx/Observable;", "", "messageIds", "handleMessageDeleteBulk", "(JLjava/util/Collection;)V", "Lcom/discord/api/message/Message;", "updatedMessage", "handleMessageUpdate", "(Lcom/discord/api/message/Message;)V", "snapshotData", "()V", "", "updatedChannelIds", "Ljava/util/Set;", "", "", "pinnedMessages", "Ljava/util/Map;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "getDispatcher", "()Lcom/discord/stores/Dispatcher;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "getObservationDeck", "()Lcom/discord/stores/updates/ObservationDeck;", "", "pinnedMessagesSnapshot", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StorePinnedMessages extends StoreV2 {
    public static final Companion Companion = new Companion(null);
    private final Dispatcher dispatcher;
    private final ObservationDeck observationDeck;
    private final Map<Long, List<Message>> pinnedMessages = new HashMap();
    private Map<Long, ? extends List<Message>> pinnedMessagesSnapshot = new HashMap();
    private final Set<Long> updatedChannelIds = new HashSet();

    /* compiled from: StorePinnedMessages.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ)\u0010\t\u001a\u00020\b2\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006H\u0002¢\u0006\u0004\b\t\u0010\nJ-\u0010\f\u001a\u0004\u0018\u00010\u00032\u000e\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00022\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006H\u0002¢\u0006\u0004\b\f\u0010\r¨\u0006\u0010"}, d2 = {"Lcom/discord/stores/StorePinnedMessages$Companion;", "", "", "Lcom/discord/models/message/Message;", "channelPinnedMessages", "", "Lcom/discord/primitives/MessageId;", "messageId", "", "getMessageIndex", "(Ljava/util/List;J)I", "channelMessages", "getMessage", "(Ljava/util/List;J)Lcom/discord/models/message/Message;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Message getMessage(List<Message> list, long j) {
            boolean z2;
            Object obj = null;
            if (list == null) {
                return null;
            }
            Iterator<T> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Object next = it.next();
                if (((Message) next).getId() == j) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    obj = next;
                    break;
                }
            }
            return (Message) obj;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final int getMessageIndex(List<Message> list, long j) {
            Iterator<Message> it = list.iterator();
            int i = 0;
            while (it.hasNext()) {
                if (it.next().getId() == j) {
                    return i;
                }
                i++;
            }
            return -1;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public StorePinnedMessages(Dispatcher dispatcher, ObservationDeck observationDeck) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handlePinnedMessagesLoaded(long j, List<Message> list) {
        this.pinnedMessages.put(Long.valueOf(j), u.toMutableList((Collection) list));
        this.updatedChannelIds.add(Long.valueOf(j));
        markChanged();
    }

    private final void loadPinnedMessages(long j) {
        if (!this.pinnedMessagesSnapshot.containsKey(Long.valueOf(j))) {
            Observable F = ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().getChannelPins(j), false, 1, null).F(StorePinnedMessages$loadPinnedMessages$1.INSTANCE);
            m.checkNotNullExpressionValue(F, "api\n        .getChannelP…messages.map(::Message) }");
            ObservableExtensionsKt.appSubscribe(F, StorePinnedMessages.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StorePinnedMessages$loadPinnedMessages$2(this, j));
        }
    }

    public final Dispatcher getDispatcher() {
        return this.dispatcher;
    }

    public final ObservationDeck getObservationDeck() {
        return this.observationDeck;
    }

    @StoreThread
    public final void handleMessageDeleteBulk(long j, Collection<Long> collection) {
        m.checkNotNullParameter(collection, "messageIds");
        List<Message> list = this.pinnedMessages.get(Long.valueOf(j));
        if (list != null) {
            Iterator<Message> it = list.iterator();
            while (it.hasNext()) {
                if (collection.contains(Long.valueOf(it.next().getId()))) {
                    it.remove();
                    this.updatedChannelIds.add(Long.valueOf(j));
                }
            }
            if (!this.updatedChannelIds.isEmpty()) {
                markChanged();
            }
        }
    }

    @StoreThread
    public final void handleMessageUpdate(com.discord.api.message.Message message) {
        UtcDateTime timestamp;
        m.checkNotNullParameter(message, "updatedMessage");
        long g = message.g();
        long o = message.o();
        boolean areEqual = m.areEqual(message.w(), Boolean.TRUE);
        List<Message> list = this.pinnedMessages.get(Long.valueOf(g));
        if (list != null) {
            int messageIndex = Companion.getMessageIndex(list, o);
            if (messageIndex != -1) {
                if (!areEqual) {
                    list.remove(messageIndex);
                } else {
                    list.set(messageIndex, list.get(messageIndex).merge(message));
                }
                this.updatedChannelIds.add(Long.valueOf(g));
                markChanged();
            } else if (areEqual) {
                UtcDateTime C = message.C();
                long j = 0;
                long g2 = C != null ? C.g() : 0L;
                int i = 0;
                if (list.size() > 0 && (timestamp = list.get(0).getTimestamp()) != null) {
                    j = timestamp.g();
                }
                while (i < list.size() && g2 < j) {
                    i++;
                }
                list.add(i, new Message(message));
                this.updatedChannelIds.add(Long.valueOf(g));
                markChanged();
            }
        }
    }

    public final Observable<List<Message>> observeForChannel(long j) {
        loadPinnedMessages(j);
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StorePinnedMessages$observeForChannel$1(this, j), 14, null);
    }

    public final Observable<Message> observePinnedMessage(long j, long j2) {
        Observable<Message> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StorePinnedMessages$observePinnedMessage$1(this, j, j2), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        HashMap hashMap = new HashMap(this.pinnedMessages);
        for (Long l : this.updatedChannelIds) {
            long longValue = l.longValue();
            Long valueOf = Long.valueOf(longValue);
            List list = (List) hashMap.get(Long.valueOf(longValue));
            if (list == null) {
                list = n.emptyList();
            }
            hashMap.put(valueOf, new ArrayList(list));
        }
        this.pinnedMessagesSnapshot = hashMap;
        this.updatedChannelIds.clear();
    }
}
