package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.api.directory.DirectoryEntryGuild;
import d0.t.u;
import d0.u.a;
import d0.z.d.m;
import j0.k.b;
import java.util.Comparator;
import java.util.List;
import kotlin.Metadata;
/* compiled from: StoreDirectories.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\u0010\u0006\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u00002\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/api/directory/DirectoryEntryGuild;", "kotlin.jvm.PlatformType", "entries", NotificationCompat.CATEGORY_CALL, "(Ljava/util/List;)Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreDirectories$fetchDirectoriesForChannel$1<T, R> implements b<List<? extends DirectoryEntryGuild>, List<? extends DirectoryEntryGuild>> {
    public static final StoreDirectories$fetchDirectoriesForChannel$1 INSTANCE = new StoreDirectories$fetchDirectoriesForChannel$1();

    @Override // j0.k.b
    public /* bridge */ /* synthetic */ List<? extends DirectoryEntryGuild> call(List<? extends DirectoryEntryGuild> list) {
        return call2((List<DirectoryEntryGuild>) list);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final List<DirectoryEntryGuild> call2(List<DirectoryEntryGuild> list) {
        m.checkNotNullExpressionValue(list, "entries");
        return u.sortedWith(list, new Comparator() { // from class: com.discord.stores.StoreDirectories$fetchDirectoriesForChannel$1$$special$$inlined$sortedByDescending$1
            @Override // java.util.Comparator
            public final int compare(T t, T t2) {
                Integer a = ((DirectoryEntryGuild) t2).e().a();
                int i = 0;
                Integer valueOf = Integer.valueOf(a != null ? a.intValue() : 0);
                Integer a2 = ((DirectoryEntryGuild) t).e().a();
                if (a2 != null) {
                    i = a2.intValue();
                }
                return a.compareValues(valueOf, Integer.valueOf(i));
            }
        });
    }
}
