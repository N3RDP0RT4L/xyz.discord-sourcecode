package com.discord.stores;

import com.discord.stores.StoreGuildRoleSubscriptions;
import com.discord.utilities.error.Error;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreGuildRoleSubscriptions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildRoleSubscriptions$fetchGuildRoleSubscriptionGroupsForGuild$3 extends o implements Function1<Error, Unit> {
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ StoreGuildRoleSubscriptions this$0;

    /* compiled from: StoreGuildRoleSubscriptions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreGuildRoleSubscriptions$fetchGuildRoleSubscriptionGroupsForGuild$3$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ Error $it;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Error error) {
            super(0);
            this.$it = error;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            Map map;
            map = StoreGuildRoleSubscriptions$fetchGuildRoleSubscriptionGroupsForGuild$3.this.this$0.guildRoleSubscriptionGroups;
            Long valueOf = Long.valueOf(StoreGuildRoleSubscriptions$fetchGuildRoleSubscriptionGroupsForGuild$3.this.$guildId);
            Error.Response response = this.$it.getResponse();
            m.checkNotNullExpressionValue(response, "it.response");
            map.put(valueOf, new StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState.Failed(response.getMessage()));
            StoreGuildRoleSubscriptions$fetchGuildRoleSubscriptionGroupsForGuild$3.this.this$0.markChanged();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGuildRoleSubscriptions$fetchGuildRoleSubscriptionGroupsForGuild$3(StoreGuildRoleSubscriptions storeGuildRoleSubscriptions, long j) {
        super(1);
        this.this$0 = storeGuildRoleSubscriptions;
        this.$guildId = j;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Error error) {
        invoke2(error);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Error error) {
        Dispatcher dispatcher;
        m.checkNotNullParameter(error, "it");
        dispatcher = this.this$0.dispatcher;
        dispatcher.schedule(new AnonymousClass1(error));
    }
}
