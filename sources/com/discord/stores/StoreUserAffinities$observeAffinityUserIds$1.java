package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.models.domain.ModelUserAffinities;
import com.discord.models.domain.ModelUserAffinity;
import d0.t.o;
import j0.k.b;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
/* compiled from: StoreUserAffinities.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\b\u0003\u0010\u0007\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0001*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/models/domain/ModelUserAffinities;", "kotlin.jvm.PlatformType", "affinities", "", "", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/domain/ModelUserAffinities;)Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserAffinities$observeAffinityUserIds$1<T, R> implements b<ModelUserAffinities, List<? extends Long>> {
    public static final StoreUserAffinities$observeAffinityUserIds$1 INSTANCE = new StoreUserAffinities$observeAffinityUserIds$1();

    public final List<Long> call(ModelUserAffinities modelUserAffinities) {
        List<ModelUserAffinity> userAffinities = modelUserAffinities.getUserAffinities();
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(userAffinities, 10));
        for (ModelUserAffinity modelUserAffinity : userAffinities) {
            arrayList.add(Long.valueOf(modelUserAffinity.getUserId()));
        }
        return arrayList;
    }
}
