package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.guild.Guild;
import com.discord.api.guildmember.GuildMember;
import com.discord.app.AppLog;
import com.discord.models.domain.ModelPayload;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import d0.t.h0;
import d0.z.d.m;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: StoreGuildMemberCounts.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b$\u0010\u001cJ\u001f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0019\u0010\t\u001a\u00020\u00062\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\u000e\u001a\u00020\r2\u0006\u0010\f\u001a\u00020\u000bH\u0007¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0012\u001a\u00020\r2\u0006\u0010\u0011\u001a\u00020\u0010H\u0007¢\u0006\u0004\b\u0012\u0010\u0013J\u001b\u0010\u0014\u001a\u00020\r2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0007¢\u0006\u0004\b\u0014\u0010\u0015J\u0017\u0010\u0018\u001a\u00020\r2\u0006\u0010\u0017\u001a\u00020\u0016H\u0007¢\u0006\u0004\b\u0018\u0010\u0019J\u001b\u0010\u001a\u001a\u00020\r2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0007¢\u0006\u0004\b\u001a\u0010\u0015J\u000f\u0010\u001b\u001a\u00020\rH\u0017¢\u0006\u0004\b\u001b\u0010\u001cR:\u0010\u001f\u001a&\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00060\u001dj\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u0006`\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 R&\u0010\"\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00060!8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\"\u0010#¨\u0006%"}, d2 = {"Lcom/discord/stores/StoreGuildMemberCounts;", "Lcom/discord/stores/StoreV2;", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lrx/Observable;", "", "observeApproximateMemberCount", "(J)Lrx/Observable;", "getApproximateMemberCount", "(J)I", "Lcom/discord/models/domain/ModelPayload;", "payload", "", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "Lcom/discord/api/guild/Guild;", "guild", "handleGuildCreate", "(Lcom/discord/api/guild/Guild;)V", "handleGuildDelete", "(J)V", "Lcom/discord/api/guildmember/GuildMember;", "member", "handleGuildMemberAdd", "(Lcom/discord/api/guildmember/GuildMember;)V", "handleGuildMemberRemove", "snapshotData", "()V", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "guildMemberCounts", "Ljava/util/HashMap;", "", "guildMemberCountsSnapshot", "Ljava/util/Map;", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildMemberCounts extends StoreV2 {
    private final HashMap<Long, Integer> guildMemberCounts = new HashMap<>();
    private Map<Long, Integer> guildMemberCountsSnapshot = h0.emptyMap();

    public final int getApproximateMemberCount(long j) {
        Integer num = this.guildMemberCountsSnapshot.get(Long.valueOf(j));
        if (num != null) {
            return num.intValue();
        }
        return 0;
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        List<Guild> guilds = modelPayload.getGuilds();
        if (guilds != null) {
            for (Guild guild : guilds) {
                this.guildMemberCounts.put(Long.valueOf(guild.r()), Integer.valueOf(guild.u()));
            }
        }
        markChanged();
    }

    @StoreThread
    public final void handleGuildCreate(Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        this.guildMemberCounts.put(Long.valueOf(guild.r()), Integer.valueOf(guild.u()));
        markChanged();
    }

    @StoreThread
    public final void handleGuildDelete(long j) {
        this.guildMemberCounts.remove(Long.valueOf(j));
        markChanged();
    }

    @StoreThread
    public final void handleGuildMemberAdd(GuildMember guildMember) {
        m.checkNotNullParameter(guildMember, "member");
        Integer num = this.guildMemberCounts.get(Long.valueOf(guildMember.f()));
        if (num != null) {
            this.guildMemberCounts.put(Long.valueOf(guildMember.f()), Integer.valueOf(num.intValue() + 1));
            markChanged();
        }
    }

    @StoreThread
    public final void handleGuildMemberRemove(long j) {
        Integer num = this.guildMemberCounts.get(Long.valueOf(j));
        if (num != null) {
            this.guildMemberCounts.put(Long.valueOf(j), Integer.valueOf(num.intValue() - 1));
            markChanged();
        }
    }

    public final Observable<Integer> observeApproximateMemberCount(long j) {
        Observable<Integer> q = ObservationDeck.connectRx$default(ObservationDeckProvider.get(), new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreGuildMemberCounts$observeApproximateMemberCount$1(this, j), 14, null).q();
        m.checkNotNullExpressionValue(q, "ObservationDeckProvider.… }.distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        super.snapshotData();
        try {
            this.guildMemberCountsSnapshot = new HashMap(this.guildMemberCounts);
        } catch (OutOfMemoryError e) {
            AppLog appLog = AppLog.g;
            StringBuilder R = a.R("OOM in StoreGuildMemberCounts. size: ");
            R.append(this.guildMemberCounts.size());
            appLog.recordBreadcrumb(R.toString(), "StoreGuildMemberCounts");
            throw e;
        }
    }
}
