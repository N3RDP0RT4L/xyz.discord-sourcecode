package com.discord.stores.utilities;

import andhook.lib.HookHelper;
import kotlin.Metadata;
/* compiled from: RestCallState.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/discord/stores/utilities/Default;", "Lcom/discord/stores/utilities/RestCallState;", "", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class Default extends RestCallState {
    public static final Default INSTANCE = new Default();

    private Default() {
        super(null);
    }
}
