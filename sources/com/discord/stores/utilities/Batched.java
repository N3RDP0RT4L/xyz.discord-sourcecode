package com.discord.stores.utilities;

import andhook.lib.HookHelper;
import androidx.exifinterface.media.ExifInterface;
import b.i.a.f.e.o.f;
import com.discord.app.AppLog;
import com.discord.utilities.time.Clock;
import com.discord.utilities.time.ClockFactory;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Job;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subjects.SerializedSubject;
/* compiled from: Batched.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010!\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 **\u0004\b\u0000\u0010\u00012\u00020\u0002:\u0001*B5\u0012\u0006\u0010!\u001a\u00020 \u0012\b\b\u0002\u0010\u0017\u001a\u00020\u0011\u0012\b\b\u0002\u0010\u001f\u001a\u00020\u0011\u0012\u0006\u0010&\u001a\u00020%\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u000e¢\u0006\u0004\b(\u0010)J\u0019\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u00040\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\r\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\b\u0010\tJ\u0018\u0010\u000b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\u0002H\u0086\b¢\u0006\u0004\b\u000b\u0010\fJ\u0015\u0010\r\u001a\u00020\u00072\u0006\u0010\n\u001a\u00028\u0000¢\u0006\u0004\b\r\u0010\fR\u0016\u0010\u000f\u001a\u00020\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0012\u001a\u00020\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013R\u0018\u0010\u0015\u001a\u0004\u0018\u00010\u00148\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0017\u001a\u00020\u00118\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0013RJ\u0010\u001a\u001a6\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00028\u0000 \u0019*\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u00040\u0004\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00028\u0000 \u0019*\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u00040\u00040\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001a\u0010\u001bR\u001c\u0010\u001d\u001a\b\u0012\u0004\u0012\u00028\u00000\u001c8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u0016\u0010\u001f\u001a\u00020\u00118\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010\u0013R\u0019\u0010!\u001a\u00020 8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010$R\u0016\u0010&\u001a\u00020%8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b&\u0010'¨\u0006+"}, d2 = {"Lcom/discord/stores/utilities/Batched;", ExifInterface.GPS_DIRECTION_TRUE, "", "Lrx/Observable;", "", "observe", "()Lrx/Observable;", "", "flush", "()V", "value", "onNextAny", "(Ljava/lang/Object;)V", "onNext", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "", "queueStartTime", "J", "Lkotlinx/coroutines/Job;", "debounceJob", "Lkotlinx/coroutines/Job;", "debounceDelayMs", "Lrx/subjects/SerializedSubject;", "kotlin.jvm.PlatformType", "subject", "Lrx/subjects/SerializedSubject;", "", "queue", "Ljava/util/List;", "maxDebounceDelayMs", "", "type", "Ljava/lang/String;", "getType", "()Ljava/lang/String;", "Lkotlinx/coroutines/CoroutineScope;", "scope", "Lkotlinx/coroutines/CoroutineScope;", HookHelper.constructorName, "(Ljava/lang/String;JJLkotlinx/coroutines/CoroutineScope;Lcom/discord/utilities/time/Clock;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class Batched<T> {
    public static final Companion Companion = new Companion(null);
    public static final long DEFAULT_DEBOUNCE_DELAY_MS = 100;
    public static final long DEFAULT_MAX_DEBOUNCE_DELAY_MS = 300;
    private static final long QUEUE_NOT_STARTED = -1;
    private final Clock clock;
    private final long debounceDelayMs;
    private Job debounceJob;
    private final long maxDebounceDelayMs;
    private List<T> queue;
    private long queueStartTime;
    private final CoroutineScope scope;
    private final SerializedSubject<List<T>, List<T>> subject;
    private final String type;

    /* compiled from: Batched.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004¨\u0006\t"}, d2 = {"Lcom/discord/stores/utilities/Batched$Companion;", "", "", "DEFAULT_DEBOUNCE_DELAY_MS", "J", "DEFAULT_MAX_DEBOUNCE_DELAY_MS", "QUEUE_NOT_STARTED", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public Batched(String str, long j, long j2, CoroutineScope coroutineScope, Clock clock) {
        m.checkNotNullParameter(str, "type");
        m.checkNotNullParameter(coroutineScope, "scope");
        m.checkNotNullParameter(clock, "clock");
        this.type = str;
        this.debounceDelayMs = j;
        this.maxDebounceDelayMs = j2;
        this.scope = coroutineScope;
        this.clock = clock;
        this.subject = new SerializedSubject<>(BehaviorSubject.k0());
        this.queue = new ArrayList();
        this.queueStartTime = -1L;
    }

    public final synchronized void flush() {
        Job job = this.debounceJob;
        if (job != null) {
            f.t(job, null, 1, null);
        }
        this.debounceJob = null;
        this.queueStartTime = -1L;
        List<T> list = this.queue;
        this.queue = new ArrayList();
        this.subject.k.onNext(list);
    }

    public final String getType() {
        return this.type;
    }

    public final Observable<List<T>> observe() {
        return this.subject;
    }

    public final synchronized void onNext(T t) {
        if (t == null) {
            AppLog.i("onNext received a NULL value for Batched[" + this.type + ']');
        }
        this.queue.add(t);
        long currentTimeMillis = this.clock.currentTimeMillis();
        if (this.queueStartTime == -1) {
            this.queueStartTime = currentTimeMillis;
        }
        if (currentTimeMillis - this.queueStartTime >= this.maxDebounceDelayMs) {
            flush();
        } else {
            Job job = this.debounceJob;
            if (job != null) {
                f.t(job, null, 1, null);
            }
            this.debounceJob = f.H0(this.scope, null, null, new Batched$onNext$1(this, null), 3, null);
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final void onNextAny(Object obj) {
        m.checkNotNullParameter(obj, "value");
        onNext(obj);
    }

    public /* synthetic */ Batched(String str, long j, long j2, CoroutineScope coroutineScope, Clock clock, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(str, (i & 2) != 0 ? 100L : j, (i & 4) != 0 ? 300L : j2, coroutineScope, (i & 16) != 0 ? ClockFactory.get() : clock);
    }
}
