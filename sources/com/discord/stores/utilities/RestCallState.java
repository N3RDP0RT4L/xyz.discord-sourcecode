package com.discord.stores.utilities;

import andhook.lib.HookHelper;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: RestCallState.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000*\u0006\b\u0000\u0010\u0001 \u00012\u00020\u0002B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0012\u0010\u0003\u001a\u0004\u0018\u00018\u0000H\u0096\u0002¢\u0006\u0004\b\u0003\u0010\u0004\u0082\u0001\u0004\u0007\b\t\n¨\u0006\u000b"}, d2 = {"Lcom/discord/stores/utilities/RestCallState;", "Response", "", "invoke", "()Ljava/lang/Object;", HookHelper.constructorName, "()V", "Lcom/discord/stores/utilities/Default;", "Lcom/discord/stores/utilities/Loading;", "Lcom/discord/stores/utilities/Failure;", "Lcom/discord/stores/utilities/Success;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public abstract class RestCallState<Response> {
    private RestCallState() {
    }

    public Response invoke() {
        return null;
    }

    public /* synthetic */ RestCallState(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }
}
