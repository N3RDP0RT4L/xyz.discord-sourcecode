package com.discord.stores.utilities;

import android.content.Context;
import androidx.annotation.MainThread;
import androidx.exifinterface.media.ExifInterface;
import com.discord.analytics.generated.traits.TrackNetworkMetadata;
import com.discord.analytics.generated.traits.TrackNetworkMetadataReceiver;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.AnalyticsUtils;
import com.discord.utilities.features.GrowthTeamFeatures;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.testing.TestUtilsKt;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import okhttp3.Request;
import retrofit2.HttpException;
import retrofit2.Response;
import rx.Observable;
import rx.functions.Action1;
/* compiled from: RestCallState.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\u001au\u0010\f\u001a\u00020\u0006\"\u0004\b\u0000\u0010\u0000*\b\u0012\u0004\u0012\u00028\u00000\u00012\u0006\u0010\u0003\u001a\u00020\u00022\u0014\b\u0002\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00042\u001a\b\u0002\u0010\t\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\b\u0012\u0004\u0012\u00020\u00060\u00042\u001a\b\u0002\u0010\u000b\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\n\u0012\u0004\u0012\u00020\u00060\u0004H\u0007¢\u0006\u0004\b\f\u0010\r\u001a7\u0010\u0010\u001a\u00020\u0006\"\u0004\b\u0000\u0010\u0000*\b\u0012\u0004\u0012\u00028\u00000\u000e2\u0018\u0010\u000f\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0001\u0012\u0004\u0012\u00020\u00060\u0004¢\u0006\u0004\b\u0010\u0010\u0011\u001aA\u0010\u0015\u001a\b\u0012\u0004\u0012\u00028\u00000\u000e\"\u0004\b\u0000\u0010\u0000*\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u00120\u000e2\u0016\u0010\u0014\u001a\u0012\u0012\u0006\u0012\u0004\u0018\u00018\u0000\u0012\u0006\u0012\u0004\u0018\u00010\u00130\u0004¢\u0006\u0004\b\u0015\u0010\u0016\u001aC\u0010\u001a\u001a\u0004\u0018\u00010\u0013\"\u0004\b\u0000\u0010\u00002\u0016\u0010\u0014\u001a\u0012\u0012\u0006\u0012\u0004\u0018\u00018\u0000\u0012\u0006\u0012\u0004\u0018\u00010\u00130\u00042\b\u0010\u0018\u001a\u0004\u0018\u00010\u00172\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00018\u0000¢\u0006\u0004\b\u001a\u0010\u001b\u001a\u0015\u0010\u001d\u001a\u00020\u001c*\u0006\u0012\u0002\b\u00030\u0012¢\u0006\u0004\b\u001d\u0010\u001e¨\u0006\u001f"}, d2 = {ExifInterface.GPS_DIRECTION_TRUE, "Lcom/discord/stores/utilities/RestCallState;", "Landroid/content/Context;", "context", "Lkotlin/Function1;", "Lcom/discord/stores/utilities/Loading;", "", "onLoading", "Lcom/discord/stores/utilities/Failure;", "onFailure", "Lcom/discord/stores/utilities/Success;", "onSuccess", "handleResponse", "(Lcom/discord/stores/utilities/RestCallState;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "Lrx/Observable;", "resultHandler", "executeRequest", "(Lrx/Observable;Lkotlin/jvm/functions/Function1;)V", "Lretrofit2/Response;", "Lcom/discord/analytics/generated/traits/TrackNetworkMetadataReceiver;", "networkActionProvider", "logNetworkAction", "(Lrx/Observable;Lkotlin/jvm/functions/Function1;)Lrx/Observable;", "Lcom/discord/analytics/generated/traits/TrackNetworkMetadata;", "metadata", "body", "getSchema", "(Lkotlin/jvm/functions/Function1;Lcom/discord/analytics/generated/traits/TrackNetworkMetadata;Ljava/lang/Object;)Lcom/discord/analytics/generated/traits/TrackNetworkMetadataReceiver;", "Lokhttp3/Request;", "getRequest", "(Lretrofit2/Response;)Lokhttp3/Request;", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class RestCallStateKt {
    public static final <T> void executeRequest(Observable<T> observable, Function1<? super RestCallState<? extends T>, Unit> function1) {
        m.checkNotNullParameter(observable, "$this$executeRequest");
        m.checkNotNullParameter(function1, "resultHandler");
        function1.invoke(Loading.INSTANCE);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(observable, false, 1, null), observable.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new RestCallStateKt$executeRequest$2(function1), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new RestCallStateKt$executeRequest$1(function1));
    }

    public static final Request getRequest(Response<?> response) {
        m.checkNotNullParameter(response, "$this$getRequest");
        okhttp3.Response response2 = response.a;
        Objects.requireNonNull(response2, "null cannot be cast to non-null type okhttp3.Response");
        return response2.j;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static final <T> TrackNetworkMetadataReceiver getSchema(Function1<? super T, ? extends TrackNetworkMetadataReceiver> function1, TrackNetworkMetadata trackNetworkMetadata, T t) {
        m.checkNotNullParameter(function1, "networkActionProvider");
        TrackNetworkMetadataReceiver invoke = function1.invoke(t);
        if (invoke != null) {
            invoke.a(trackNetworkMetadata);
        }
        return invoke;
    }

    public static /* synthetic */ TrackNetworkMetadataReceiver getSchema$default(Function1 function1, TrackNetworkMetadata trackNetworkMetadata, Object obj, int i, Object obj2) {
        if ((i & 4) != 0) {
            obj = null;
        }
        return getSchema(function1, trackNetworkMetadata, obj);
    }

    @MainThread
    public static final <T> void handleResponse(RestCallState<? extends T> restCallState, Context context, Function1<? super Loading, Unit> function1, Function1<? super Failure<? extends T>, Unit> function12, Function1<? super Success<? extends T>, Unit> function13) {
        m.checkNotNullParameter(restCallState, "$this$handleResponse");
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(function1, "onLoading");
        m.checkNotNullParameter(function12, "onFailure");
        m.checkNotNullParameter(function13, "onSuccess");
        if (restCallState instanceof Loading) {
            function1.invoke(restCallState);
        } else if (restCallState instanceof Success) {
            function13.invoke(restCallState);
        } else if (restCallState instanceof Failure) {
            function12.invoke(restCallState);
        }
    }

    public static /* synthetic */ void handleResponse$default(RestCallState restCallState, Context context, Function1 function1, Function1 function12, Function1 function13, int i, Object obj) {
        if ((i & 2) != 0) {
            function1 = RestCallStateKt$handleResponse$1.INSTANCE;
        }
        if ((i & 4) != 0) {
            function12 = new RestCallStateKt$handleResponse$2(context);
        }
        if ((i & 8) != 0) {
            function13 = RestCallStateKt$handleResponse$3.INSTANCE;
        }
        handleResponse(restCallState, context, function1, function12, function13);
    }

    public static final <T> Observable<T> logNetworkAction(Observable<Response<T>> observable, final Function1<? super T, ? extends TrackNetworkMetadataReceiver> function1) {
        m.checkNotNullParameter(observable, "$this$logNetworkAction");
        m.checkNotNullParameter(function1, "networkActionProvider");
        if (TestUtilsKt.getIS_JUNIT_TEST()) {
            Observable<T> observable2 = (Observable<T>) observable.F(RestCallStateKt$logNetworkAction$1.INSTANCE);
            m.checkNotNullExpressionValue(observable2, "map { it.body() }");
            return observable2;
        }
        final AnalyticsUtils.Tracker tracker = AnalyticsTracker.INSTANCE.getTracker();
        Observable<T> observable3 = (Observable<T>) observable.t(new Action1<Response<T>>() { // from class: com.discord.stores.utilities.RestCallStateKt$logNetworkAction$2
            @Override // rx.functions.Action1
            public /* bridge */ /* synthetic */ void call(Object obj) {
                call((Response) ((Response) obj));
            }

            public final void call(Response<T> response) {
                TrackNetworkMetadataReceiver schema;
                m.checkNotNullExpressionValue(response, "response");
                if (response.a()) {
                    Request request = RestCallStateKt.getRequest(response);
                    if (GrowthTeamFeatures.INSTANCE.isNetworkActionLoggingEnabled() && (schema = RestCallStateKt.getSchema(Function1.this, new TrackNetworkMetadata(request.f3784b.l, request.c, Long.valueOf(response.a.m), null, null, 24), response.f3796b)) != null) {
                        tracker.track(schema);
                        return;
                    }
                    return;
                }
                throw new HttpException(response);
            }
        }).s(new Action1<Throwable>() { // from class: com.discord.stores.utilities.RestCallStateKt$logNetworkAction$3
            /* JADX WARN: Removed duplicated region for block: B:30:0x0071  */
            /* JADX WARN: Removed duplicated region for block: B:35:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public final void call(java.lang.Throwable r10) {
                /*
                    r9 = this;
                    com.discord.utilities.features.GrowthTeamFeatures r0 = com.discord.utilities.features.GrowthTeamFeatures.INSTANCE
                    boolean r0 = r0.isNetworkActionLoggingEnabled()
                    if (r0 == 0) goto L7f
                    boolean r0 = r10 instanceof retrofit2.HttpException
                    r1 = 0
                    if (r0 == 0) goto L56
                    retrofit2.HttpException r10 = (retrofit2.HttpException) r10
                    retrofit2.Response<?> r0 = r10.j
                    if (r0 == 0) goto L6e
                    okhttp3.Request r0 = com.discord.stores.utilities.RestCallStateKt.getRequest(r0)
                    if (r0 == 0) goto L6e
                    com.discord.analytics.generated.traits.TrackNetworkMetadata r8 = new com.discord.analytics.generated.traits.TrackNetworkMetadata
                    f0.w r2 = r0.f3784b
                    java.lang.String r3 = r2.l
                    java.lang.String r4 = r0.c
                    retrofit2.Response<?> r0 = r10.j
                    if (r0 == 0) goto L30
                    okhttp3.Response r0 = r0.a
                    int r0 = r0.m
                    long r5 = (long) r0
                    java.lang.Long r0 = java.lang.Long.valueOf(r5)
                    r5 = r0
                    goto L31
                L30:
                    r5 = r1
                L31:
                    retrofit2.Response<?> r0 = r10.j
                    if (r0 == 0) goto L40
                    okhttp3.Response r0 = r0.a
                    int r0 = r0.m
                    long r6 = (long) r0
                    java.lang.Long r0 = java.lang.Long.valueOf(r6)
                    r6 = r0
                    goto L41
                L40:
                    r6 = r1
                L41:
                    retrofit2.Response<?> r10 = r10.j
                    if (r10 == 0) goto L4a
                    okhttp3.Response r10 = r10.a
                    java.lang.String r10 = r10.l
                    goto L4b
                L4a:
                    r10 = r1
                L4b:
                    if (r10 == 0) goto L4e
                    goto L50
                L4e:
                    java.lang.String r10 = ""
                L50:
                    r7 = r10
                    r2 = r8
                    r2.<init>(r3, r4, r5, r6, r7)
                    goto L6f
                L56:
                    boolean r0 = r10 instanceof java.net.UnknownHostException
                    if (r0 == 0) goto L6e
                    com.discord.analytics.generated.traits.TrackNetworkMetadata r0 = new com.discord.analytics.generated.traits.TrackNetworkMetadata
                    r3 = 0
                    r4 = 0
                    r5 = 0
                    r6 = 0
                    java.net.UnknownHostException r10 = (java.net.UnknownHostException) r10
                    java.lang.String r7 = r10.getLocalizedMessage()
                    r8 = 15
                    r2 = r0
                    r2.<init>(r3, r4, r5, r6, r7, r8)
                    r8 = r0
                    goto L6f
                L6e:
                    r8 = r1
                L6f:
                    if (r8 == 0) goto L7f
                    kotlin.jvm.functions.Function1 r10 = kotlin.jvm.functions.Function1.this
                    r0 = 4
                    com.discord.analytics.generated.traits.TrackNetworkMetadataReceiver r10 = com.discord.stores.utilities.RestCallStateKt.getSchema$default(r10, r8, r1, r0, r1)
                    if (r10 == 0) goto L7f
                    com.discord.utilities.analytics.AnalyticsUtils$Tracker r0 = r2
                    r0.track(r10)
                L7f:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.utilities.RestCallStateKt$logNetworkAction$3.call(java.lang.Throwable):void");
            }
        }).F(RestCallStateKt$logNetworkAction$4.INSTANCE);
        m.checkNotNullExpressionValue(observable3, "doOnNext { response ->\n …rectly.\n    it.body()\n  }");
        return observable3;
    }
}
