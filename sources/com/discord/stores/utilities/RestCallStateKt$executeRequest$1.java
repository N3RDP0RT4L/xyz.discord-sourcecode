package com.discord.stores.utilities;

import androidx.exifinterface.media.ExifInterface;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: RestCallState.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u0002\"\u0004\b\u0000\u0010\u00002\u0006\u0010\u0001\u001a\u00028\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {ExifInterface.GPS_DIRECTION_TRUE, "response", "", "invoke", "(Ljava/lang/Object;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class RestCallStateKt$executeRequest$1 extends o implements Function1<T, Unit> {
    public final /* synthetic */ Function1 $resultHandler;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public RestCallStateKt$executeRequest$1(Function1 function1) {
        super(1);
        this.$resultHandler = function1;
    }

    @Override // kotlin.jvm.functions.Function1
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(T t) {
        this.$resultHandler.invoke(new Success(t));
    }
}
