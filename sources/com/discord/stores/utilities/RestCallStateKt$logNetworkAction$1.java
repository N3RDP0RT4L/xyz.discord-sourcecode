package com.discord.stores.utilities;

import androidx.core.app.NotificationCompat;
import androidx.exifinterface.media.ExifInterface;
import j0.k.b;
import kotlin.Metadata;
import retrofit2.Response;
/* compiled from: RestCallState.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0010\u0006\u001a\n \u0002*\u0004\u0018\u00018\u00008\u0000\"\u0004\b\u0000\u0010\u00002\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00028\u0000 \u0002*\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u00010\u0001H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {ExifInterface.GPS_DIRECTION_TRUE, "Lretrofit2/Response;", "kotlin.jvm.PlatformType", "it", NotificationCompat.CATEGORY_CALL, "(Lretrofit2/Response;)Ljava/lang/Object;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class RestCallStateKt$logNetworkAction$1<T, R> implements b<Response<T>, T> {
    public static final RestCallStateKt$logNetworkAction$1 INSTANCE = new RestCallStateKt$logNetworkAction$1();

    @Override // j0.k.b
    public /* bridge */ /* synthetic */ Object call(Object obj) {
        return call((Response) ((Response) obj));
    }

    public final T call(Response<T> response) {
        return response.f3796b;
    }
}
