package com.discord.stores.utilities;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import androidx.exifinterface.media.ExifInterface;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: RestCallState.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u0003\"\u0004\b\u0000\u0010\u00002\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00028\u00000\u0001H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {ExifInterface.GPS_DIRECTION_TRUE, "Lcom/discord/stores/utilities/Failure;", "failure", "", "invoke", "(Lcom/discord/stores/utilities/Failure;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class RestCallStateKt$handleResponse$2 extends o implements Function1<Failure<? extends T>, Unit> {
    public final /* synthetic */ Context $context;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public RestCallStateKt$handleResponse$2(Context context) {
        super(1);
        this.$context = context;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Object obj) {
        invoke((Failure) obj);
        return Unit.a;
    }

    public final void invoke(final Failure<? extends T> failure) {
        m.checkNotNullParameter(failure, "failure");
        new Handler(Looper.getMainLooper()).post(new Runnable() { // from class: com.discord.stores.utilities.RestCallStateKt$handleResponse$2.1
            @Override // java.lang.Runnable
            public final void run() {
                failure.getError().showToasts(RestCallStateKt$handleResponse$2.this.$context);
            }
        });
    }
}
