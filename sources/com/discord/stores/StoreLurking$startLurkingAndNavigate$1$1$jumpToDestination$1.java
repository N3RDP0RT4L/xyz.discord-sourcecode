package com.discord.stores;

import com.discord.stores.StoreLurking$startLurkingAndNavigate$1;
import com.discord.utilities.channel.ChannelSelector;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreLurking.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreLurking$startLurkingAndNavigate$1$1$jumpToDestination$1 extends o implements Function0<Unit> {
    public final /* synthetic */ Long $channelId;
    public final /* synthetic */ StoreLurking$startLurkingAndNavigate$1.AnonymousClass1 this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreLurking$startLurkingAndNavigate$1$1$jumpToDestination$1(StoreLurking$startLurkingAndNavigate$1.AnonymousClass1 r1, Long l) {
        super(0);
        this.this$0 = r1;
        this.$channelId = l;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreStream storeStream;
        Long l;
        Long l2 = this.$channelId;
        if (l2 == null || ((l2 != null && l2.longValue() == 0) || ((l = this.$channelId) != null && l.longValue() == -1))) {
            storeStream = StoreLurking$startLurkingAndNavigate$1.this.this$0.stream;
            storeStream.getGuildSelected$app_productionGoogleRelease().set(StoreLurking$startLurkingAndNavigate$1.this.$guildId);
            return;
        }
        ChannelSelector.Companion.getInstance().selectChannel(StoreLurking$startLurkingAndNavigate$1.this.$guildId, this.$channelId.longValue(), (r16 & 4) != 0 ? null : null, (r16 & 8) != 0 ? null : null);
    }
}
