package com.discord.stores;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.models.domain.ModelAuditLog;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelGuildIntegration;
import com.discord.models.domain.ModelWebhook;
import com.discord.models.user.User;
import com.discord.stores.StoreAuditLog;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.auditlogs.AuditLogChangeUtils;
import com.discord.utilities.auditlogs.AuditLogUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$1;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$2;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.o;
import d0.t.g0;
import d0.t.h0;
import d0.t.n;
import d0.t.s;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Action1;
/* compiled from: StoreAuditLog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 C2\u00020\u0001:\u0003DECB\u001f\u0012\u0006\u00103\u001a\u000202\u0012\u0006\u0010<\u001a\u00020;\u0012\u0006\u0010?\u001a\u00020>¢\u0006\u0004\bA\u0010BJ#\u0010\b\u001a\u00020\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\b\u0010\tJ\u000f\u0010\n\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\n\u0010\u000bJ#\u0010\u000e\u001a\u00020\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\r\u001a\u00020\fH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u000f\u0010\u0010\u001a\u00020\u0007H\u0003¢\u0006\u0004\b\u0010\u0010\u000bJ=\u0010\u0019\u001a\u00020\u00182\b\u0010\u0012\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0013\u001a\u00020\u00112\u0006\u0010\u0015\u001a\u00020\u00142\b\b\u0002\u0010\u0016\u001a\u00020\u00142\b\b\u0002\u0010\u0017\u001a\u00020\u0014H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ/\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00110\u001b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00110\u001bH\u0002¢\u0006\u0004\b\u001d\u0010\u001eJ\u001b\u0010 \u001a\u0004\u0018\u00010\u001f2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b \u0010!J\u001f\u0010#\u001a\b\u0012\u0004\u0012\u00020\u001f0\"2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b#\u0010$J\u0015\u0010&\u001a\u00020\u00072\u0006\u0010%\u001a\u00020\u0014¢\u0006\u0004\b&\u0010'J\u0019\u0010*\u001a\u00020\u00072\n\u0010)\u001a\u00060\u0002j\u0002`(¢\u0006\u0004\b*\u0010+J\u0015\u0010-\u001a\u00020\u00072\u0006\u0010,\u001a\u00020\u0002¢\u0006\u0004\b-\u0010+J\r\u0010.\u001a\u00020\u0007¢\u0006\u0004\b.\u0010\u000bJ\u0019\u0010/\u001a\u00020\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b/\u0010+J\u000f\u00100\u001a\u00020\u0007H\u0016¢\u0006\u0004\b0\u0010\u000bJ\r\u00101\u001a\u00020\u0007¢\u0006\u0004\b1\u0010\u000bR\u0016\u00103\u001a\u0002028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b3\u00104R\u001e\u00106\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`58\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b6\u00107R\u0016\u00108\u001a\u00020\u001f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b8\u00109R\u0016\u0010:\u001a\u00020\u001f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b:\u00109R\u0016\u0010<\u001a\u00020;8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b<\u0010=R\u0016\u0010?\u001a\u00020>8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b?\u0010@¨\u0006F"}, d2 = {"Lcom/discord/stores/StoreAuditLog;", "Lcom/discord/stores/StoreV2;", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/stores/StoreAuditLog$AuditLogFilter;", "filter", "", "fetchAuditLogs", "(JLcom/discord/stores/StoreAuditLog$AuditLogFilter;)V", "handleFetchFailure", "()V", "Lcom/discord/models/domain/ModelAuditLog;", "newAuditLog", "handleFetchSuccess", "(JLcom/discord/models/domain/ModelAuditLog;)V", "clearStateInternal", "Lcom/discord/models/domain/ModelAuditLogEntry;", "prevEntry", "entry", "", "numMerges", "timeWindowMins", "maxMerges", "", "shouldMergeEntries", "(Lcom/discord/models/domain/ModelAuditLogEntry;Lcom/discord/models/domain/ModelAuditLogEntry;III)Z", "", "rawEntries", "transformEntries", "(JLjava/util/List;)Ljava/util/List;", "Lcom/discord/stores/StoreAuditLog$AuditLogState;", "getAuditLogState", "(J)Lcom/discord/stores/StoreAuditLog$AuditLogState;", "Lrx/Observable;", "observeAuditLogState", "(J)Lrx/Observable;", "actionId", "setAuditLogFilterActionId", "(I)V", "Lcom/discord/primitives/UserId;", "userId", "setAuditLogFilterUserId", "(J)V", "selectedItemId", "toggleSelectedState", "fetchMoreAuditLogEntries", "fetchAuditLogIfNeeded", "snapshotData", "clearState", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/primitives/Timestamp;", "cutoffTimestamp", "Ljava/lang/Long;", "stateSnapshot", "Lcom/discord/stores/StoreAuditLog$AuditLogState;", "state", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", HookHelper.constructorName, "(Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/rest/RestAPI;)V", "Companion", "AuditLogFilter", "AuditLogState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAuditLog extends StoreV2 {
    public static final Companion Companion = new Companion(null);
    private static final long NO_GUILD = -1;
    private Long cutoffTimestamp;
    private final Dispatcher dispatcher;
    private final ObservationDeck observationDeck;
    private final RestAPI restAPI;
    private AuditLogState state;
    private AuditLogState stateSnapshot;

    /* compiled from: StoreAuditLog.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u0000 \u001b2\u00020\u0001:\u0001\u001bB\u001b\u0012\n\u0010\t\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\n\u001a\u00020\u0006¢\u0006\u0004\b\u0019\u0010\u001aJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ(\u0010\u000b\u001a\u00020\u00002\f\b\u0002\u0010\t\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\n\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0010\u0010\bJ\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u0019\u0010\n\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0015\u001a\u0004\b\u0016\u0010\bR\u001d\u0010\t\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0005¨\u0006\u001c"}, d2 = {"Lcom/discord/stores/StoreAuditLog$AuditLogFilter;", "", "", "Lcom/discord/primitives/UserId;", "component1", "()J", "", "component2", "()I", "userFilter", "actionFilter", "copy", "(JI)Lcom/discord/stores/StoreAuditLog$AuditLogFilter;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getActionFilter", "J", "getUserFilter", HookHelper.constructorName, "(JI)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class AuditLogFilter {
        public static final Companion Companion = new Companion(null);
        private final int actionFilter;
        private final long userFilter;

        /* compiled from: StoreAuditLog.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/stores/StoreAuditLog$AuditLogFilter$Companion;", "", "Lcom/discord/stores/StoreAuditLog$AuditLogFilter;", "emptyFilter", "()Lcom/discord/stores/StoreAuditLog$AuditLogFilter;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Companion {
            private Companion() {
            }

            public final AuditLogFilter emptyFilter() {
                return new AuditLogFilter(0L, 0);
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public AuditLogFilter(long j, int i) {
            this.userFilter = j;
            this.actionFilter = i;
        }

        public static /* synthetic */ AuditLogFilter copy$default(AuditLogFilter auditLogFilter, long j, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                j = auditLogFilter.userFilter;
            }
            if ((i2 & 2) != 0) {
                i = auditLogFilter.actionFilter;
            }
            return auditLogFilter.copy(j, i);
        }

        public final long component1() {
            return this.userFilter;
        }

        public final int component2() {
            return this.actionFilter;
        }

        public final AuditLogFilter copy(long j, int i) {
            return new AuditLogFilter(j, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof AuditLogFilter)) {
                return false;
            }
            AuditLogFilter auditLogFilter = (AuditLogFilter) obj;
            return this.userFilter == auditLogFilter.userFilter && this.actionFilter == auditLogFilter.actionFilter;
        }

        public final int getActionFilter() {
            return this.actionFilter;
        }

        public final long getUserFilter() {
            return this.userFilter;
        }

        public int hashCode() {
            return (b.a(this.userFilter) * 31) + this.actionFilter;
        }

        public String toString() {
            StringBuilder R = a.R("AuditLogFilter(userFilter=");
            R.append(this.userFilter);
            R.append(", actionFilter=");
            return a.A(R, this.actionFilter, ")");
        }
    }

    /* compiled from: StoreAuditLog.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000f\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0018\b\u0086\b\u0018\u00002\u00020\u0001BÉ\u0001\u0012\f\b\u0002\u0010$\u001a\u00060\u0002j\u0002`\u0003\u0012\u0018\b\u0002\u0010%\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0006\u0012\u0010\b\u0002\u0010&\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u000b\u0012\u000e\b\u0002\u0010'\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000b\u0012\u000e\b\u0002\u0010(\u001a\b\u0012\u0004\u0012\u00020\u00110\u000b\u0012\u000e\b\u0002\u0010)\u001a\b\u0012\u0004\u0012\u00020\u00130\u000b\u0012\u000e\b\u0002\u0010*\u001a\b\u0012\u0004\u0012\u00020\u00150\u000b\u0012\n\b\u0002\u0010+\u001a\u0004\u0018\u00010\u0002\u0012\b\b\u0002\u0010,\u001a\u00020\u0019\u0012&\b\u0002\u0010-\u001a \u0012\u0004\u0012\u00020\u001c\u0012\u0016\u0012\u0014\u0012\b\u0012\u00060\u0002j\u0002`\u001e\u0012\u0006\u0012\u0004\u0018\u00010\u001f0\u001d0\u0006\u0012\b\b\u0002\u0010.\u001a\u00020!¢\u0006\u0004\bJ\u0010KJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J \u0010\t\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0006HÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0018\u0010\r\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0016\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000bHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u000eJ\u0016\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00110\u000bHÆ\u0003¢\u0006\u0004\b\u0012\u0010\u000eJ\u0016\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00130\u000bHÆ\u0003¢\u0006\u0004\b\u0014\u0010\u000eJ\u0016\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00150\u000bHÆ\u0003¢\u0006\u0004\b\u0016\u0010\u000eJ\u0012\u0010\u0017\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u001a\u001a\u00020\u0019HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u001bJ.\u0010 \u001a \u0012\u0004\u0012\u00020\u001c\u0012\u0016\u0012\u0014\u0012\b\u0012\u00060\u0002j\u0002`\u001e\u0012\u0006\u0012\u0004\u0018\u00010\u001f0\u001d0\u0006HÆ\u0003¢\u0006\u0004\b \u0010\nJ\u0010\u0010\"\u001a\u00020!HÆ\u0003¢\u0006\u0004\b\"\u0010#JÒ\u0001\u0010/\u001a\u00020\u00002\f\b\u0002\u0010$\u001a\u00060\u0002j\u0002`\u00032\u0018\b\u0002\u0010%\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u00062\u0010\b\u0002\u0010&\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u000b2\u000e\b\u0002\u0010'\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000b2\u000e\b\u0002\u0010(\u001a\b\u0012\u0004\u0012\u00020\u00110\u000b2\u000e\b\u0002\u0010)\u001a\b\u0012\u0004\u0012\u00020\u00130\u000b2\u000e\b\u0002\u0010*\u001a\b\u0012\u0004\u0012\u00020\u00150\u000b2\n\b\u0002\u0010+\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010,\u001a\u00020\u00192&\b\u0002\u0010-\u001a \u0012\u0004\u0012\u00020\u001c\u0012\u0016\u0012\u0014\u0012\b\u0012\u00060\u0002j\u0002`\u001e\u0012\u0006\u0012\u0004\u0018\u00010\u001f0\u001d0\u00062\b\b\u0002\u0010.\u001a\u00020!HÆ\u0001¢\u0006\u0004\b/\u00100J\u0010\u00102\u001a\u000201HÖ\u0001¢\u0006\u0004\b2\u00103J\u0010\u00105\u001a\u000204HÖ\u0001¢\u0006\u0004\b5\u00106J\u001a\u00108\u001a\u00020!2\b\u00107\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b8\u00109R\u0019\u0010.\u001a\u00020!8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010:\u001a\u0004\b.\u0010#R\u001f\u0010'\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010;\u001a\u0004\b<\u0010\u000eR\u001b\u0010+\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010=\u001a\u0004\b>\u0010\u0018R!\u0010&\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010;\u001a\u0004\b?\u0010\u000eR\u001f\u0010*\u001a\b\u0012\u0004\u0012\u00020\u00150\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010;\u001a\u0004\b@\u0010\u000eR\u001f\u0010)\u001a\b\u0012\u0004\u0012\u00020\u00130\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010;\u001a\u0004\bA\u0010\u000eR\u0019\u0010,\u001a\u00020\u00198\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010B\u001a\u0004\bC\u0010\u001bR\u001d\u0010$\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010D\u001a\u0004\bE\u0010\u0005R7\u0010-\u001a \u0012\u0004\u0012\u00020\u001c\u0012\u0016\u0012\u0014\u0012\b\u0012\u00060\u0002j\u0002`\u001e\u0012\u0006\u0012\u0004\u0018\u00010\u001f0\u001d0\u00068\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010F\u001a\u0004\bG\u0010\nR\u001f\u0010(\u001a\b\u0012\u0004\u0012\u00020\u00110\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010;\u001a\u0004\bH\u0010\u000eR)\u0010%\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u00068\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010F\u001a\u0004\bI\u0010\n¨\u0006L"}, d2 = {"Lcom/discord/stores/StoreAuditLog$AuditLogState;", "", "", "Lcom/discord/primitives/GuildId;", "component1", "()J", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/user/User;", "component2", "()Ljava/util/Map;", "", "Lcom/discord/models/domain/ModelAuditLogEntry;", "component3", "()Ljava/util/List;", "Lcom/discord/models/domain/ModelWebhook;", "component4", "Lcom/discord/models/domain/ModelGuildIntegration;", "component5", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "component6", "Lcom/discord/api/channel/Channel;", "component7", "component8", "()Ljava/lang/Long;", "Lcom/discord/stores/StoreAuditLog$AuditLogFilter;", "component9", "()Lcom/discord/stores/StoreAuditLog$AuditLogFilter;", "Lcom/discord/models/domain/ModelAuditLogEntry$TargetType;", "", "Lcom/discord/stores/TargetId;", "", "component10", "", "component11", "()Z", "guildId", "users", "entries", "webhooks", "integrations", "guildScheduledEvents", "threads", "selectedItemId", "filter", "deletedTargets", "isLoading", "copy", "(JLjava/util/Map;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/Long;Lcom/discord/stores/StoreAuditLog$AuditLogFilter;Ljava/util/Map;Z)Lcom/discord/stores/StoreAuditLog$AuditLogState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Ljava/util/List;", "getWebhooks", "Ljava/lang/Long;", "getSelectedItemId", "getEntries", "getThreads", "getGuildScheduledEvents", "Lcom/discord/stores/StoreAuditLog$AuditLogFilter;", "getFilter", "J", "getGuildId", "Ljava/util/Map;", "getDeletedTargets", "getIntegrations", "getUsers", HookHelper.constructorName, "(JLjava/util/Map;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/Long;Lcom/discord/stores/StoreAuditLog$AuditLogFilter;Ljava/util/Map;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class AuditLogState {
        private final Map<ModelAuditLogEntry.TargetType, Map<Long, CharSequence>> deletedTargets;
        private final List<ModelAuditLogEntry> entries;
        private final AuditLogFilter filter;
        private final long guildId;
        private final List<GuildScheduledEvent> guildScheduledEvents;
        private final List<ModelGuildIntegration> integrations;
        private final boolean isLoading;
        private final Long selectedItemId;
        private final List<Channel> threads;
        private final Map<Long, User> users;
        private final List<ModelWebhook> webhooks;

        public AuditLogState() {
            this(0L, null, null, null, null, null, null, null, null, null, false, 2047, null);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public AuditLogState(long j, Map<Long, ? extends User> map, List<? extends ModelAuditLogEntry> list, List<? extends ModelWebhook> list2, List<? extends ModelGuildIntegration> list3, List<GuildScheduledEvent> list4, List<Channel> list5, Long l, AuditLogFilter auditLogFilter, Map<ModelAuditLogEntry.TargetType, ? extends Map<Long, CharSequence>> map2, boolean z2) {
            m.checkNotNullParameter(map, "users");
            m.checkNotNullParameter(list2, "webhooks");
            m.checkNotNullParameter(list3, "integrations");
            m.checkNotNullParameter(list4, "guildScheduledEvents");
            m.checkNotNullParameter(list5, "threads");
            m.checkNotNullParameter(auditLogFilter, "filter");
            m.checkNotNullParameter(map2, "deletedTargets");
            this.guildId = j;
            this.users = map;
            this.entries = list;
            this.webhooks = list2;
            this.integrations = list3;
            this.guildScheduledEvents = list4;
            this.threads = list5;
            this.selectedItemId = l;
            this.filter = auditLogFilter;
            this.deletedTargets = map2;
            this.isLoading = z2;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ AuditLogState copy$default(AuditLogState auditLogState, long j, Map map, List list, List list2, List list3, List list4, List list5, Long l, AuditLogFilter auditLogFilter, Map map2, boolean z2, int i, Object obj) {
            return auditLogState.copy((i & 1) != 0 ? auditLogState.guildId : j, (i & 2) != 0 ? auditLogState.users : map, (i & 4) != 0 ? auditLogState.entries : list, (i & 8) != 0 ? auditLogState.webhooks : list2, (i & 16) != 0 ? auditLogState.integrations : list3, (i & 32) != 0 ? auditLogState.guildScheduledEvents : list4, (i & 64) != 0 ? auditLogState.threads : list5, (i & 128) != 0 ? auditLogState.selectedItemId : l, (i & 256) != 0 ? auditLogState.filter : auditLogFilter, (i & 512) != 0 ? auditLogState.deletedTargets : map2, (i & 1024) != 0 ? auditLogState.isLoading : z2);
        }

        public final long component1() {
            return this.guildId;
        }

        public final Map<ModelAuditLogEntry.TargetType, Map<Long, CharSequence>> component10() {
            return this.deletedTargets;
        }

        public final boolean component11() {
            return this.isLoading;
        }

        public final Map<Long, User> component2() {
            return this.users;
        }

        public final List<ModelAuditLogEntry> component3() {
            return this.entries;
        }

        public final List<ModelWebhook> component4() {
            return this.webhooks;
        }

        public final List<ModelGuildIntegration> component5() {
            return this.integrations;
        }

        public final List<GuildScheduledEvent> component6() {
            return this.guildScheduledEvents;
        }

        public final List<Channel> component7() {
            return this.threads;
        }

        public final Long component8() {
            return this.selectedItemId;
        }

        public final AuditLogFilter component9() {
            return this.filter;
        }

        public final AuditLogState copy(long j, Map<Long, ? extends User> map, List<? extends ModelAuditLogEntry> list, List<? extends ModelWebhook> list2, List<? extends ModelGuildIntegration> list3, List<GuildScheduledEvent> list4, List<Channel> list5, Long l, AuditLogFilter auditLogFilter, Map<ModelAuditLogEntry.TargetType, ? extends Map<Long, CharSequence>> map2, boolean z2) {
            m.checkNotNullParameter(map, "users");
            m.checkNotNullParameter(list2, "webhooks");
            m.checkNotNullParameter(list3, "integrations");
            m.checkNotNullParameter(list4, "guildScheduledEvents");
            m.checkNotNullParameter(list5, "threads");
            m.checkNotNullParameter(auditLogFilter, "filter");
            m.checkNotNullParameter(map2, "deletedTargets");
            return new AuditLogState(j, map, list, list2, list3, list4, list5, l, auditLogFilter, map2, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof AuditLogState)) {
                return false;
            }
            AuditLogState auditLogState = (AuditLogState) obj;
            return this.guildId == auditLogState.guildId && m.areEqual(this.users, auditLogState.users) && m.areEqual(this.entries, auditLogState.entries) && m.areEqual(this.webhooks, auditLogState.webhooks) && m.areEqual(this.integrations, auditLogState.integrations) && m.areEqual(this.guildScheduledEvents, auditLogState.guildScheduledEvents) && m.areEqual(this.threads, auditLogState.threads) && m.areEqual(this.selectedItemId, auditLogState.selectedItemId) && m.areEqual(this.filter, auditLogState.filter) && m.areEqual(this.deletedTargets, auditLogState.deletedTargets) && this.isLoading == auditLogState.isLoading;
        }

        public final Map<ModelAuditLogEntry.TargetType, Map<Long, CharSequence>> getDeletedTargets() {
            return this.deletedTargets;
        }

        public final List<ModelAuditLogEntry> getEntries() {
            return this.entries;
        }

        public final AuditLogFilter getFilter() {
            return this.filter;
        }

        public final long getGuildId() {
            return this.guildId;
        }

        public final List<GuildScheduledEvent> getGuildScheduledEvents() {
            return this.guildScheduledEvents;
        }

        public final List<ModelGuildIntegration> getIntegrations() {
            return this.integrations;
        }

        public final Long getSelectedItemId() {
            return this.selectedItemId;
        }

        public final List<Channel> getThreads() {
            return this.threads;
        }

        public final Map<Long, User> getUsers() {
            return this.users;
        }

        public final List<ModelWebhook> getWebhooks() {
            return this.webhooks;
        }

        public int hashCode() {
            int a = b.a(this.guildId) * 31;
            Map<Long, User> map = this.users;
            int i = 0;
            int hashCode = (a + (map != null ? map.hashCode() : 0)) * 31;
            List<ModelAuditLogEntry> list = this.entries;
            int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
            List<ModelWebhook> list2 = this.webhooks;
            int hashCode3 = (hashCode2 + (list2 != null ? list2.hashCode() : 0)) * 31;
            List<ModelGuildIntegration> list3 = this.integrations;
            int hashCode4 = (hashCode3 + (list3 != null ? list3.hashCode() : 0)) * 31;
            List<GuildScheduledEvent> list4 = this.guildScheduledEvents;
            int hashCode5 = (hashCode4 + (list4 != null ? list4.hashCode() : 0)) * 31;
            List<Channel> list5 = this.threads;
            int hashCode6 = (hashCode5 + (list5 != null ? list5.hashCode() : 0)) * 31;
            Long l = this.selectedItemId;
            int hashCode7 = (hashCode6 + (l != null ? l.hashCode() : 0)) * 31;
            AuditLogFilter auditLogFilter = this.filter;
            int hashCode8 = (hashCode7 + (auditLogFilter != null ? auditLogFilter.hashCode() : 0)) * 31;
            Map<ModelAuditLogEntry.TargetType, Map<Long, CharSequence>> map2 = this.deletedTargets;
            if (map2 != null) {
                i = map2.hashCode();
            }
            int i2 = (hashCode8 + i) * 31;
            boolean z2 = this.isLoading;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            return i2 + i3;
        }

        public final boolean isLoading() {
            return this.isLoading;
        }

        public String toString() {
            StringBuilder R = a.R("AuditLogState(guildId=");
            R.append(this.guildId);
            R.append(", users=");
            R.append(this.users);
            R.append(", entries=");
            R.append(this.entries);
            R.append(", webhooks=");
            R.append(this.webhooks);
            R.append(", integrations=");
            R.append(this.integrations);
            R.append(", guildScheduledEvents=");
            R.append(this.guildScheduledEvents);
            R.append(", threads=");
            R.append(this.threads);
            R.append(", selectedItemId=");
            R.append(this.selectedItemId);
            R.append(", filter=");
            R.append(this.filter);
            R.append(", deletedTargets=");
            R.append(this.deletedTargets);
            R.append(", isLoading=");
            return a.M(R, this.isLoading, ")");
        }

        public /* synthetic */ AuditLogState(long j, Map map, List list, List list2, List list3, List list4, List list5, Long l, AuditLogFilter auditLogFilter, Map map2, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? -1L : j, (i & 2) != 0 ? h0.emptyMap() : map, (i & 4) != 0 ? null : list, (i & 8) != 0 ? n.emptyList() : list2, (i & 16) != 0 ? n.emptyList() : list3, (i & 32) != 0 ? n.emptyList() : list4, (i & 64) != 0 ? n.emptyList() : list5, (i & 128) == 0 ? l : null, (i & 256) != 0 ? AuditLogFilter.Companion.emptyFilter() : auditLogFilter, (i & 512) != 0 ? h0.emptyMap() : map2, (i & 1024) != 0 ? false : z2);
        }
    }

    /* compiled from: StoreAuditLog.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/stores/StoreAuditLog$Companion;", "", "", "NO_GUILD", "J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public StoreAuditLog(ObservationDeck observationDeck, Dispatcher dispatcher, RestAPI restAPI) {
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(restAPI, "restAPI");
        this.observationDeck = observationDeck;
        this.dispatcher = dispatcher;
        this.restAPI = restAPI;
        AuditLogState auditLogState = new AuditLogState(0L, null, null, null, null, null, null, null, null, null, false, 2047, null);
        this.state = auditLogState;
        this.stateSnapshot = auditLogState;
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void clearStateInternal() {
        this.cutoffTimestamp = null;
        this.state = new AuditLogState(0L, null, null, null, null, null, null, null, null, null, false, 2047, null);
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void fetchAuditLogs(long j, AuditLogFilter auditLogFilter) {
        ModelAuditLogEntry modelAuditLogEntry;
        List<ModelAuditLogEntry> entries = this.state.getEntries();
        Long valueOf = (entries == null || (modelAuditLogEntry = (ModelAuditLogEntry) u.lastOrNull((List<? extends Object>) entries)) == null) ? null : Long.valueOf(modelAuditLogEntry.getId());
        if (this.cutoffTimestamp == null) {
            this.cutoffTimestamp = 0L;
        }
        if (!m.areEqual(valueOf, this.cutoffTimestamp)) {
            this.cutoffTimestamp = valueOf;
            this.state = AuditLogState.copy$default(this.state, j, null, null, null, null, null, null, null, null, null, true, 1022, null);
            markChanged();
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.getAuditLogs(j, valueOf, Long.valueOf(auditLogFilter.getUserFilter()), Integer.valueOf(auditLogFilter.getActionFilter())), false, 1, null), StoreAuditLog.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new StoreAuditLog$fetchAuditLogs$1(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreAuditLog$fetchAuditLogs$2(this, j));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleFetchFailure() {
        this.dispatcher.schedule(new StoreAuditLog$handleFetchFailure$1(this));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleFetchSuccess(long j, ModelAuditLog modelAuditLog) {
        this.dispatcher.schedule(new StoreAuditLog$handleFetchSuccess$1(this, modelAuditLog, j));
    }

    private final boolean shouldMergeEntries(ModelAuditLogEntry modelAuditLogEntry, ModelAuditLogEntry modelAuditLogEntry2, int i, int i2, int i3) {
        if (!(modelAuditLogEntry == null || modelAuditLogEntry.getActionTypeId() != modelAuditLogEntry2.getActionTypeId() || modelAuditLogEntry.getTargetId() != modelAuditLogEntry2.getTargetId() || modelAuditLogEntry.getUserId() != modelAuditLogEntry2.getUserId() || !m.areEqual(modelAuditLogEntry.getOptions(), modelAuditLogEntry2.getOptions()) || i >= i3 || modelAuditLogEntry2.getTargetType() == ModelAuditLogEntry.TargetType.INVITE || modelAuditLogEntry2.getActionTypeId() == 72 || modelAuditLogEntry2.getActionTypeId() == 73 || modelAuditLogEntry2.getActionTypeId() == 26 || modelAuditLogEntry2.getActionTypeId() == 27)) {
            AuditLogUtils auditLogUtils = AuditLogUtils.INSTANCE;
            if (Math.abs(auditLogUtils.getTimestampStart(modelAuditLogEntry2) - auditLogUtils.getTimestampStart(modelAuditLogEntry)) < i2 * 60000) {
                return true;
            }
        }
        return false;
    }

    public static /* synthetic */ boolean shouldMergeEntries$default(StoreAuditLog storeAuditLog, ModelAuditLogEntry modelAuditLogEntry, ModelAuditLogEntry modelAuditLogEntry2, int i, int i2, int i3, int i4, Object obj) {
        return storeAuditLog.shouldMergeEntries(modelAuditLogEntry, modelAuditLogEntry2, i, (i4 & 8) != 0 ? 30 : i2, (i4 & 16) != 0 ? 50 : i3);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final List<ModelAuditLogEntry> transformEntries(long j, List<? extends ModelAuditLogEntry> list) {
        ModelAuditLogEntry.Change change;
        ModelAuditLogEntry.Change change2;
        ModelAuditLogEntry.Options options;
        List<ModelAuditLogEntry.Change> changes;
        ArrayList arrayList = new ArrayList();
        while (true) {
            int i = 0;
            for (ModelAuditLogEntry modelAuditLogEntry : s.asReversed(list)) {
                ArrayList arrayList2 = new ArrayList();
                if (modelAuditLogEntry.getReason() != null) {
                    arrayList2.add(new ModelAuditLogEntry.Change(ModelAuditLogEntry.CHANGE_KEY_REASON, null, modelAuditLogEntry.getReason()));
                }
                List<ModelAuditLogEntry.Change> changes2 = modelAuditLogEntry.getChanges();
                if (changes2 != null) {
                    ModelAuditLogEntry.Change change3 = null;
                    ModelAuditLogEntry.Change change4 = null;
                    for (ModelAuditLogEntry.Change change5 : changes2) {
                        m.checkNotNullExpressionValue(change5, "change");
                        String key = change5.getKey();
                        if (key != null) {
                            switch (key.hashCode()) {
                                case 3079692:
                                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_DENIED)) {
                                        arrayList2.addAll(AuditLogChangeUtils.INSTANCE.transformPermissionOverride(change5));
                                        break;
                                    } else {
                                        break;
                                    }
                                case 92906313:
                                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_GRANTED)) {
                                        arrayList2.addAll(AuditLogChangeUtils.INSTANCE.transformPermissionOverride(change5));
                                        break;
                                    } else {
                                        break;
                                    }
                                case 108404047:
                                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_RESET)) {
                                        arrayList2.addAll(AuditLogChangeUtils.INSTANCE.transformPermissionOverride(change5));
                                        break;
                                    } else {
                                        break;
                                    }
                                case 1133704324:
                                    if (key.equals(ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS)) {
                                        arrayList2.addAll(AuditLogChangeUtils.INSTANCE.transformPermissionChange(change5));
                                        break;
                                    } else {
                                        break;
                                    }
                            }
                        }
                        arrayList2.add(change5);
                        String key2 = change5.getKey();
                        if (key2 != null) {
                            int hashCode = key2.hashCode();
                            if (hashCode != 3373707) {
                                if (hashCode == 3575610 && key2.equals("type")) {
                                    change4 = change5;
                                }
                            } else if (key2.equals(ModelAuditLogEntry.CHANGE_KEY_NAME)) {
                                change3 = change5;
                            }
                        }
                    }
                    change2 = change3;
                    change = change4;
                } else {
                    change2 = null;
                    change = null;
                }
                if (modelAuditLogEntry.getActionTypeId() == 21) {
                    ModelAuditLogEntry.Options options2 = modelAuditLogEntry.getOptions();
                    arrayList2.add(new ModelAuditLogEntry.Change(ModelAuditLogEntry.CHANGE_KEY_PRUNE_DELETE_DAYS, null, Integer.valueOf(options2 != null ? options2.getDeleteMemberDays() : 1)));
                }
                ModelAuditLogEntry modelAuditLogEntry2 = new ModelAuditLogEntry(modelAuditLogEntry.getId(), modelAuditLogEntry.getActionTypeId(), modelAuditLogEntry.getTargetId(), modelAuditLogEntry.getUserId(), arrayList2, modelAuditLogEntry.getOptions(), j, null);
                ModelAuditLogEntry modelAuditLogEntry3 = (ModelAuditLogEntry) u.firstOrNull((List<? extends Object>) arrayList);
                List list2 = null;
                if (shouldMergeEntries$default(this, modelAuditLogEntry3, modelAuditLogEntry2, i, 0, 0, 24, null)) {
                    long id2 = modelAuditLogEntry3 != null ? modelAuditLogEntry3.getId() : modelAuditLogEntry2.getId();
                    int actionTypeId = modelAuditLogEntry3 != null ? modelAuditLogEntry3.getActionTypeId() : modelAuditLogEntry2.getActionTypeId();
                    long targetId = modelAuditLogEntry3 != null ? modelAuditLogEntry3.getTargetId() : modelAuditLogEntry2.getTargetId();
                    long userId = modelAuditLogEntry3 != null ? modelAuditLogEntry3.getUserId() : modelAuditLogEntry2.getUserId();
                    if (!(modelAuditLogEntry3 == null || (changes = modelAuditLogEntry3.getChanges()) == null)) {
                        list2 = u.toMutableList((Collection) changes);
                    }
                    if (list2 != null) {
                        List<ModelAuditLogEntry.Change> changes3 = modelAuditLogEntry2.getChanges();
                        if (changes3 == null) {
                            changes3 = n.emptyList();
                        }
                        list2.addAll(changes3);
                    } else {
                        modelAuditLogEntry2.getChanges();
                    }
                    if (modelAuditLogEntry3 == null || (options = modelAuditLogEntry3.getOptions()) == null) {
                        options = modelAuditLogEntry2.getOptions();
                    }
                    arrayList.set(0, new ModelAuditLogEntry(id2, actionTypeId, targetId, userId, list2, options, j, Long.valueOf(AuditLogUtils.INSTANCE.getTimestampStart(modelAuditLogEntry2))));
                    i++;
                } else {
                    Map mutableMap = h0.toMutableMap(this.state.getDeletedTargets());
                    if (modelAuditLogEntry2.getActionType() == ModelAuditLogEntry.ActionType.DELETE && change2 != null) {
                        String str = (String) change2.getOldValue();
                        if (modelAuditLogEntry2.getTargetType() == ModelAuditLogEntry.TargetType.CHANNEL) {
                            if (m.areEqual(change != null ? change.getOldValue() : null, (Object) 0)) {
                                str = MentionUtilsKt.CHANNELS_CHAR + str;
                            }
                        }
                        if (mutableMap.get(modelAuditLogEntry2.getTargetType()) == null) {
                            ModelAuditLogEntry.TargetType targetType = modelAuditLogEntry2.getTargetType();
                            m.checkNotNullExpressionValue(targetType, "entry.targetType");
                            mutableMap.put(targetType, h0.mutableMapOf(o.to(Long.valueOf(modelAuditLogEntry2.getTargetId()), str)));
                        } else {
                            Map map = (Map) mutableMap.get(modelAuditLogEntry2.getTargetType());
                            if (map != null) {
                                CharSequence charSequence = (CharSequence) map.put(Long.valueOf(modelAuditLogEntry2.getTargetId()), str);
                            }
                        }
                        this.state = AuditLogState.copy$default(this.state, 0L, null, null, null, null, null, null, null, null, mutableMap, false, 1535, null);
                    }
                    arrayList.add(0, modelAuditLogEntry2);
                }
            }
            return arrayList;
        }
    }

    public final void clearState() {
        this.dispatcher.schedule(new StoreAuditLog$clearState$1(this));
    }

    public final void fetchAuditLogIfNeeded(long j) {
        this.dispatcher.schedule(new StoreAuditLog$fetchAuditLogIfNeeded$1(this, j));
    }

    public final void fetchMoreAuditLogEntries() {
        this.dispatcher.schedule(new StoreAuditLog$fetchMoreAuditLogEntries$1(this));
    }

    public final AuditLogState getAuditLogState(long j) {
        AuditLogState auditLogState = this.stateSnapshot;
        if (j == auditLogState.getGuildId()) {
            return auditLogState;
        }
        return null;
    }

    public final Observable<AuditLogState> observeAuditLogState(final long j) {
        Observable t = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreAuditLog$observeAuditLogState$1(this, j), 14, null).q().t(new Action1<AuditLogState>() { // from class: com.discord.stores.StoreAuditLog$observeAuditLogState$2
            public final void call(StoreAuditLog.AuditLogState auditLogState) {
                StoreAuditLog.this.fetchAuditLogIfNeeded(j);
            }
        });
        m.checkNotNullExpressionValue(t, "observationDeck.connectR…ditLogIfNeeded(guildId) }");
        Observable<AuditLogState> F = t.x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
        m.checkNotNullExpressionValue(F, "filter { it != null }.map { it!! }");
        return F;
    }

    public final void setAuditLogFilterActionId(int i) {
        this.dispatcher.schedule(new StoreAuditLog$setAuditLogFilterActionId$1(this, i));
    }

    public final void setAuditLogFilterUserId(long j) {
        this.dispatcher.schedule(new StoreAuditLog$setAuditLogFilterUserId$1(this, j));
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        super.snapshotData();
        AuditLogState auditLogState = this.state;
        HashMap hashMap = new HashMap(this.state.getUsers());
        List<ModelAuditLogEntry> entries = this.state.getEntries();
        ArrayList arrayList = entries != null ? new ArrayList(entries) : null;
        ArrayList arrayList2 = new ArrayList(this.state.getWebhooks());
        ArrayList arrayList3 = new ArrayList(this.state.getIntegrations());
        ArrayList arrayList4 = new ArrayList(this.state.getGuildScheduledEvents());
        ArrayList arrayList5 = new ArrayList(this.state.getThreads());
        Map<ModelAuditLogEntry.TargetType, Map<Long, CharSequence>> deletedTargets = this.state.getDeletedTargets();
        LinkedHashMap linkedHashMap = new LinkedHashMap(g0.mapCapacity(deletedTargets.size()));
        Iterator<T> it = deletedTargets.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            linkedHashMap.put(entry.getKey(), new HashMap((Map) entry.getValue()));
        }
        this.stateSnapshot = AuditLogState.copy$default(auditLogState, 0L, hashMap, arrayList, arrayList2, arrayList3, arrayList4, arrayList5, null, null, linkedHashMap, false, 1409, null);
    }

    public final void toggleSelectedState(long j) {
        this.dispatcher.schedule(new StoreAuditLog$toggleSelectedState$1(this, j));
    }
}
