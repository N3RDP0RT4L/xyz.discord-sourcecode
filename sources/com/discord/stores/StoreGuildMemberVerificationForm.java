package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelMemberVerificationForm;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.rest.RestAPI;
import d0.t.h0;
import d0.z.d.m;
import java.util.HashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreGuildMemberVerificationForm.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0002()B#\u0012\u0006\u0010$\u001a\u00020#\u0012\b\b\u0002\u0010\u0017\u001a\u00020\u0016\u0012\b\b\u0002\u0010\u001a\u001a\u00020\u0019¢\u0006\u0004\b&\u0010'J#\u0010\b\u001a\u00020\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\b\u0010\tJ\u001b\u0010\n\u001a\u00020\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u001b\u0010\f\u001a\u00020\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0003¢\u0006\u0004\b\f\u0010\u000bJ\u001b\u0010\u000e\u001a\u0004\u0018\u00010\r2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ!\u0010\u0011\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\r0\u00102\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0019\u0010\u0013\u001a\u00020\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0013\u0010\u000bJ\u000f\u0010\u0014\u001a\u00020\u0007H\u0017¢\u0006\u0004\b\u0014\u0010\u0015R\u0016\u0010\u0017\u001a\u00020\u00168\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u0016\u0010\u001a\u001a\u00020\u00198\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001a\u0010\u001bR&\u0010\u001d\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\r0\u001c8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR:\u0010!\u001a&\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\r0\u001fj\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\r` 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"R\u0016\u0010$\u001a\u00020#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%¨\u0006*"}, d2 = {"Lcom/discord/stores/StoreGuildMemberVerificationForm;", "Lcom/discord/stores/StoreV2;", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/models/domain/ModelMemberVerificationForm;", "memberVerificationForm", "", "handleMemberVerificationFormFetchSuccess", "(JLcom/discord/models/domain/ModelMemberVerificationForm;)V", "handleMemberVerificationFormFetchStart", "(J)V", "handleMemberVerificationFormFetchFailed", "Lcom/discord/stores/StoreGuildMemberVerificationForm$MemberVerificationFormData;", "getMemberVerificationFormData", "(J)Lcom/discord/stores/StoreGuildMemberVerificationForm$MemberVerificationFormData;", "Lrx/Observable;", "observeMemberVerificationFormData", "(J)Lrx/Observable;", "fetchMemberVerificationForm", "snapshotData", "()V", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "", "memberVerificationFormSnapshot", "Ljava/util/Map;", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "memberVerificationFormState", "Ljava/util/HashMap;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/updates/ObservationDeck;)V", "FetchStates", "MemberVerificationFormData", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildMemberVerificationForm extends StoreV2 {
    private final Dispatcher dispatcher;
    private Map<Long, MemberVerificationFormData> memberVerificationFormSnapshot;
    private final HashMap<Long, MemberVerificationFormData> memberVerificationFormState;
    private final ObservationDeck observationDeck;
    private final RestAPI restAPI;

    /* compiled from: StoreGuildMemberVerificationForm.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/stores/StoreGuildMemberVerificationForm$FetchStates;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "FETCHING", "FAILED", "SUCCEEDED", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum FetchStates {
        FETCHING,
        FAILED,
        SUCCEEDED
    }

    /* compiled from: StoreGuildMemberVerificationForm.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J&\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u001b\u0010\t\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0016\u001a\u0004\b\u0017\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0018\u001a\u0004\b\u0019\u0010\u0004¨\u0006\u001c"}, d2 = {"Lcom/discord/stores/StoreGuildMemberVerificationForm$MemberVerificationFormData;", "", "Lcom/discord/stores/StoreGuildMemberVerificationForm$FetchStates;", "component1", "()Lcom/discord/stores/StoreGuildMemberVerificationForm$FetchStates;", "Lcom/discord/models/domain/ModelMemberVerificationForm;", "component2", "()Lcom/discord/models/domain/ModelMemberVerificationForm;", "fetchState", "form", "copy", "(Lcom/discord/stores/StoreGuildMemberVerificationForm$FetchStates;Lcom/discord/models/domain/ModelMemberVerificationForm;)Lcom/discord/stores/StoreGuildMemberVerificationForm$MemberVerificationFormData;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/ModelMemberVerificationForm;", "getForm", "Lcom/discord/stores/StoreGuildMemberVerificationForm$FetchStates;", "getFetchState", HookHelper.constructorName, "(Lcom/discord/stores/StoreGuildMemberVerificationForm$FetchStates;Lcom/discord/models/domain/ModelMemberVerificationForm;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class MemberVerificationFormData {
        private final FetchStates fetchState;
        private final ModelMemberVerificationForm form;

        public MemberVerificationFormData(FetchStates fetchStates, ModelMemberVerificationForm modelMemberVerificationForm) {
            m.checkNotNullParameter(fetchStates, "fetchState");
            this.fetchState = fetchStates;
            this.form = modelMemberVerificationForm;
        }

        public static /* synthetic */ MemberVerificationFormData copy$default(MemberVerificationFormData memberVerificationFormData, FetchStates fetchStates, ModelMemberVerificationForm modelMemberVerificationForm, int i, Object obj) {
            if ((i & 1) != 0) {
                fetchStates = memberVerificationFormData.fetchState;
            }
            if ((i & 2) != 0) {
                modelMemberVerificationForm = memberVerificationFormData.form;
            }
            return memberVerificationFormData.copy(fetchStates, modelMemberVerificationForm);
        }

        public final FetchStates component1() {
            return this.fetchState;
        }

        public final ModelMemberVerificationForm component2() {
            return this.form;
        }

        public final MemberVerificationFormData copy(FetchStates fetchStates, ModelMemberVerificationForm modelMemberVerificationForm) {
            m.checkNotNullParameter(fetchStates, "fetchState");
            return new MemberVerificationFormData(fetchStates, modelMemberVerificationForm);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof MemberVerificationFormData)) {
                return false;
            }
            MemberVerificationFormData memberVerificationFormData = (MemberVerificationFormData) obj;
            return m.areEqual(this.fetchState, memberVerificationFormData.fetchState) && m.areEqual(this.form, memberVerificationFormData.form);
        }

        public final FetchStates getFetchState() {
            return this.fetchState;
        }

        public final ModelMemberVerificationForm getForm() {
            return this.form;
        }

        public int hashCode() {
            FetchStates fetchStates = this.fetchState;
            int i = 0;
            int hashCode = (fetchStates != null ? fetchStates.hashCode() : 0) * 31;
            ModelMemberVerificationForm modelMemberVerificationForm = this.form;
            if (modelMemberVerificationForm != null) {
                i = modelMemberVerificationForm.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("MemberVerificationFormData(fetchState=");
            R.append(this.fetchState);
            R.append(", form=");
            R.append(this.form);
            R.append(")");
            return R.toString();
        }
    }

    public /* synthetic */ StoreGuildMemberVerificationForm(Dispatcher dispatcher, RestAPI restAPI, ObservationDeck observationDeck, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(dispatcher, (i & 2) != 0 ? RestAPI.Companion.getApi() : restAPI, (i & 4) != 0 ? ObservationDeckProvider.get() : observationDeck);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleMemberVerificationFormFetchFailed(long j) {
        this.memberVerificationFormState.put(Long.valueOf(j), new MemberVerificationFormData(FetchStates.FAILED, null));
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleMemberVerificationFormFetchStart(long j) {
        this.memberVerificationFormState.put(Long.valueOf(j), new MemberVerificationFormData(FetchStates.FETCHING, null));
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleMemberVerificationFormFetchSuccess(long j, ModelMemberVerificationForm modelMemberVerificationForm) {
        this.memberVerificationFormState.put(Long.valueOf(j), new MemberVerificationFormData(FetchStates.SUCCEEDED, modelMemberVerificationForm));
        markChanged();
    }

    public final void fetchMemberVerificationForm(long j) {
        this.dispatcher.schedule(new StoreGuildMemberVerificationForm$fetchMemberVerificationForm$1(this, j));
    }

    public final MemberVerificationFormData getMemberVerificationFormData(long j) {
        return this.memberVerificationFormSnapshot.get(Long.valueOf(j));
    }

    public final Observable<MemberVerificationFormData> observeMemberVerificationFormData(long j) {
        Observable<MemberVerificationFormData> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreGuildMemberVerificationForm$observeMemberVerificationFormData$1(this, j), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        super.snapshotData();
        this.memberVerificationFormSnapshot = new HashMap(this.memberVerificationFormState);
    }

    public StoreGuildMemberVerificationForm(Dispatcher dispatcher, RestAPI restAPI, ObservationDeck observationDeck) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(restAPI, "restAPI");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.dispatcher = dispatcher;
        this.restAPI = restAPI;
        this.observationDeck = observationDeck;
        this.memberVerificationFormSnapshot = h0.emptyMap();
        this.memberVerificationFormState = new HashMap<>();
    }
}
