package com.discord.stores;

import com.discord.stores.StoreMessagesLoader;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreMessagesLoader.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;", "it", "invoke", "(Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;)Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessagesLoader$handleLoadedMessages$1 extends o implements Function1<StoreMessagesLoader.ChannelLoadedState, StoreMessagesLoader.ChannelLoadedState> {
    public final /* synthetic */ boolean $isAllLoaded;
    public final /* synthetic */ boolean $isAppendingTop;
    public final /* synthetic */ boolean $isInitial;
    public final /* synthetic */ List $messages;
    public final /* synthetic */ StoreMessagesLoader this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreMessagesLoader$handleLoadedMessages$1(StoreMessagesLoader storeMessagesLoader, boolean z2, boolean z3, boolean z4, List list) {
        super(1);
        this.this$0 = storeMessagesLoader;
        this.$isInitial = z2;
        this.$isAppendingTop = z3;
        this.$isAllLoaded = z4;
        this.$messages = list;
    }

    /* JADX WARN: Removed duplicated region for block: B:28:0x005f A[EDGE_INSN: B:28:0x005f->B:23:0x005f ?: BREAK  , SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final com.discord.stores.StoreMessagesLoader.ChannelLoadedState invoke(com.discord.stores.StoreMessagesLoader.ChannelLoadedState r14) {
        /*
            r13 = this;
            java.lang.String r0 = "it"
            d0.z.d.m.checkNotNullParameter(r14, r0)
            boolean r0 = r13.$isInitial
            r1 = 1
            if (r0 == 0) goto Lc
            r3 = 1
            goto L11
        Lc:
            boolean r0 = r14.isInitialMessagesLoaded()
            r3 = r0
        L11:
            boolean r0 = r13.$isInitial
            if (r0 != 0) goto L1f
            boolean r0 = r13.$isAppendingTop
            if (r0 == 0) goto L1a
            goto L1f
        L1a:
            boolean r0 = r14.isOldestMessagesLoaded()
            goto L21
        L1f:
            boolean r0 = r13.$isAllLoaded
        L21:
            r4 = r0
            r5 = 0
            r6 = 0
            java.util.List r0 = r13.$messages
            java.util.Iterator r0 = r0.iterator()
        L2a:
            boolean r2 = r0.hasNext()
            r7 = 0
            if (r2 == 0) goto L5e
            java.lang.Object r2 = r0.next()
            r8 = r2
            com.discord.models.message.Message r8 = (com.discord.models.message.Message) r8
            com.discord.api.user.User r8 = r8.getAuthor()
            if (r8 == 0) goto L5a
            long r8 = r8.i()
            com.discord.stores.StoreMessagesLoader r10 = r13.this$0
            com.discord.stores.StoreStream r10 = com.discord.stores.StoreMessagesLoader.access$getStream$p(r10)
            com.discord.stores.StoreUser r10 = r10.getUsers$app_productionGoogleRelease()
            com.discord.models.user.MeUser r10 = r10.getMe()
            long r10 = r10.getId()
            int r12 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r12 != 0) goto L5a
            r8 = 1
            goto L5b
        L5a:
            r8 = 0
        L5b:
            if (r8 == 0) goto L2a
            goto L5f
        L5e:
            r2 = r7
        L5f:
            com.discord.models.message.Message r2 = (com.discord.models.message.Message) r2
            if (r2 == 0) goto L6c
            long r0 = r2.getId()
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r7 = r0
        L6c:
            r8 = 8
            r9 = 0
            r2 = r14
            com.discord.stores.StoreMessagesLoader$ChannelLoadedState r14 = com.discord.stores.StoreMessagesLoader.ChannelLoadedState.copy$default(r2, r3, r4, r5, r6, r7, r8, r9)
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreMessagesLoader$handleLoadedMessages$1.invoke(com.discord.stores.StoreMessagesLoader$ChannelLoadedState):com.discord.stores.StoreMessagesLoader$ChannelLoadedState");
    }
}
