package com.discord.stores;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import b.a.q.j;
import b.a.q.w;
import b.d.b.a.a;
import com.discord.app.App;
import com.discord.app.AppLog;
import com.discord.models.domain.ModelApplicationStream;
import com.discord.models.domain.ModelPayload;
import com.discord.models.domain.StreamCreateOrUpdate;
import com.discord.models.domain.StreamServerUpdate;
import com.discord.rtcconnection.MediaSinkWantsManager;
import com.discord.rtcconnection.RtcConnection;
import com.discord.rtcconnection.VideoMetadata;
import com.discord.rtcconnection.mediaengine.MediaEngine;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.collections.ListenerCollection;
import com.discord.utilities.collections.ListenerCollectionSubject;
import com.discord.utilities.debug.DebugPrintBuilder;
import com.discord.utilities.debug.DebugPrintable;
import com.discord.utilities.debug.DebugPrintableCollection;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.networking.NetworkMonitor;
import com.discord.utilities.ssl.SecureSocketsLayerUtils;
import com.discord.utilities.systemlog.SystemLogUtils;
import com.discord.utilities.time.Clock;
import d0.z.d.m;
import java.util.Map;
import java.util.Objects;
import javax.net.ssl.SSLSocketFactory;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreStreamRtcConnection.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u009c\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010%\n\u0002\u0010\u0000\n\u0002\b\u0007\n\u0002\u0010\u0003\n\u0002\b\u0004\n\u0002\u0010$\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u0000 \u0091\u00012\u00020\u00012\u00020\u0002:\n\u0091\u0001\u0092\u0001\u0093\u0001\u0094\u0001\u0095\u0001Bc\u0012\u0006\u0010{\u001a\u00020z\u0012\b\u0010\u0084\u0001\u001a\u00030\u0083\u0001\u0012\u0006\u0010m\u001a\u00020l\u0012\u0006\u0010`\u001a\u00020_\u0012\u0006\u0010~\u001a\u00020}\u0012\b\u0010\u0081\u0001\u001a\u00030\u0080\u0001\u0012\u0006\u0010c\u001a\u00020b\u0012\b\b\u0002\u0010j\u001a\u00020i\u0012\b\b\u0002\u0010g\u001a\u00020f\u0012\b\b\u0002\u0010w\u001a\u00020v¢\u0006\u0006\b\u008f\u0001\u0010\u0090\u0001J\u000f\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0017\u0010\b\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0003¢\u0006\u0004\b\b\u0010\tJg\u0010\u0019\u001a\u00020\u00182\n\u0010\f\u001a\u00060\nj\u0002`\u000b2\u000e\u0010\u000e\u001a\n\u0018\u00010\nj\u0004\u0018\u0001`\r2\n\u0010\u0010\u001a\u00060\nj\u0002`\u000f2\n\u0010\u0013\u001a\u00060\u0011j\u0002`\u00122\u0006\u0010\u0014\u001a\u00020\u00112\n\u0010\u0015\u001a\u00060\nj\u0002`\u000b2\u000e\u0010\u0017\u001a\n\u0018\u00010\u0011j\u0004\u0018\u0001`\u0016H\u0003¢\u0006\u0004\b\u0019\u0010\u001aJ\u000f\u0010\u001b\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u001b\u0010\u0005J\u0019\u0010\u001d\u001a\u00020\u00032\b\u0010\u001c\u001a\u0004\u0018\u00010\u0018H\u0003¢\u0006\u0004\b\u001d\u0010\u001eJ#\u0010\"\u001a\u00020\u00032\u0012\u0010!\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020 0\u001fH\u0003¢\u0006\u0004\b\"\u0010#J\u0017\u0010%\u001a\u00020\u00032\u0006\u0010$\u001a\u00020\u0011H\u0002¢\u0006\u0004\b%\u0010&J#\u0010*\u001a\u00020\u00032\u0006\u0010'\u001a\u00020\u00112\n\b\u0002\u0010)\u001a\u0004\u0018\u00010(H\u0002¢\u0006\u0004\b*\u0010+J\u0017\u0010,\u001a\u00020\u00032\u0006\u0010'\u001a\u00020\u0011H\u0002¢\u0006\u0004\b,\u0010&J;\u0010/\u001a\u00020\u00032\u0006\u0010'\u001a\u00020\u00112\n\b\u0002\u0010)\u001a\u0004\u0018\u00010(2\u0016\b\u0002\u0010.\u001a\u0010\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u0011\u0018\u00010-H\u0002¢\u0006\u0004\b/\u00100J\u0015\u00103\u001a\u00020\u00032\u0006\u00102\u001a\u000201¢\u0006\u0004\b3\u00104J\u0017\u00107\u001a\u00020\u00032\u0006\u00106\u001a\u000205H\u0016¢\u0006\u0004\b7\u00108J\u000f\u00109\u001a\u00020\u0003H\u0004¢\u0006\u0004\b9\u0010\u0005J\r\u0010;\u001a\u00020:¢\u0006\u0004\b;\u0010<J\r\u0010>\u001a\u00020=¢\u0006\u0004\b>\u0010?J\u0013\u0010A\u001a\b\u0012\u0004\u0012\u00020:0@¢\u0006\u0004\bA\u0010BJ\u0015\u0010C\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00180@¢\u0006\u0004\bC\u0010BJ\u0015\u0010D\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060@¢\u0006\u0004\bD\u0010BJ\u0017\u0010G\u001a\u00020\u00032\u0006\u0010F\u001a\u00020EH\u0007¢\u0006\u0004\bG\u0010HJ\u0017\u0010K\u001a\u00020\u00032\u0006\u0010J\u001a\u00020IH\u0007¢\u0006\u0004\bK\u0010LJ\u0017\u0010O\u001a\u00020\u00032\u0006\u0010N\u001a\u00020MH\u0007¢\u0006\u0004\bO\u0010PJ\u000f\u0010Q\u001a\u00020\u0003H\u0007¢\u0006\u0004\bQ\u0010\u0005J\u0017\u0010T\u001a\u00020\u00032\u0006\u0010S\u001a\u00020RH\u0007¢\u0006\u0004\bT\u0010UJ\u0015\u0010W\u001a\u00020\u00032\u0006\u0010V\u001a\u00020:¢\u0006\u0004\bW\u0010XJ\u001d\u0010Z\u001a\u00020\u00032\u000e\u0010Y\u001a\n\u0018\u00010\nj\u0004\u0018\u0001`\u000b¢\u0006\u0004\bZ\u0010[R\u001a\u0010]\u001a\u00060\nj\u0002`\\8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b]\u0010^R\u0016\u0010`\u001a\u00020_8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b`\u0010aR\u0016\u0010c\u001a\u00020b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bc\u0010dR\u0016\u0010S\u001a\u00020=8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bS\u0010eR\u0016\u0010g\u001a\u00020f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bg\u0010hR\u0016\u0010j\u001a\u00020i8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bj\u0010kR\u0016\u0010m\u001a\u00020l8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bm\u0010nR\u0016\u0010o\u001a\u00020:8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bo\u0010pR\u0016\u00102\u001a\u0002018\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b2\u0010qR\u001c\u0010t\u001a\b\u0012\u0004\u0012\u00020s0r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bt\u0010uR\u0016\u0010w\u001a\u00020v8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bw\u0010xR\u0018\u0010\u001c\u001a\u0004\u0018\u00010\u00188\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001c\u0010yR\u0016\u0010{\u001a\u00020z8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b{\u0010|R\u0016\u0010~\u001a\u00020}8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b~\u0010\u007fR\u001a\u0010\u0081\u0001\u001a\u00030\u0080\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0081\u0001\u0010\u0082\u0001R\u001a\u0010\u0084\u0001\u001a\u00030\u0083\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0084\u0001\u0010\u0085\u0001R!\u0010\u0086\u0001\u001a\n\u0018\u00010\nj\u0004\u0018\u0001`\u000b8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u0086\u0001\u0010\u0087\u0001R\u0019\u0010\u0088\u0001\u001a\u00020\u00118\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0088\u0001\u0010\u0089\u0001R\u0019\u0010\u0013\u001a\u0004\u0018\u00010\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u0013\u0010\u0089\u0001R%\u0010\u008b\u0001\u001a\t\u0012\u0004\u0012\u00020s0\u008a\u00018\u0006@\u0006¢\u0006\u0010\n\u0006\b\u008b\u0001\u0010\u008c\u0001\u001a\u0006\b\u008d\u0001\u0010\u008e\u0001¨\u0006\u0096\u0001"}, d2 = {"Lcom/discord/stores/StoreStreamRtcConnection;", "Lcom/discord/stores/StoreV2;", "Lcom/discord/utilities/debug/DebugPrintable;", "", "handleMediaSessionIdReceived", "()V", "Lcom/discord/rtcconnection/RtcConnection$Quality;", "quality", "handleQualityUpdate", "(Lcom/discord/rtcconnection/RtcConnection$Quality;)V", "", "Lcom/discord/primitives/UserId;", "userId", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/ChannelId;", "channelId", "", "Lcom/discord/primitives/SessionId;", "sessionId", "rtcServerId", "senderId", "Lcom/discord/primitives/StreamKey;", "streamKey", "Lcom/discord/rtcconnection/RtcConnection;", "createRtcConnection", "(JLjava/lang/Long;JLjava/lang/String;Ljava/lang/String;JLjava/lang/String;)Lcom/discord/rtcconnection/RtcConnection;", "destroyRtcConnection", "rtcConnection", "updateRtcConnection", "(Lcom/discord/rtcconnection/RtcConnection;)V", "", "", "properties", "handleVideoStreamEndedAnalyticsEvent", "(Ljava/util/Map;)V", "message", "recordBreadcrumb", "(Ljava/lang/String;)V", NotificationCompat.CATEGORY_MESSAGE, "", "e", "logi", "(Ljava/lang/String;Ljava/lang/Throwable;)V", "logw", "", "metadata", "loge", "(Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V", "Lcom/discord/utilities/networking/NetworkMonitor;", "networkMonitor", "init", "(Lcom/discord/utilities/networking/NetworkMonitor;)V", "Lcom/discord/utilities/debug/DebugPrintBuilder;", "dp", "debugPrint", "(Lcom/discord/utilities/debug/DebugPrintBuilder;)V", "finalize", "", "getStreamVolume", "()F", "Lcom/discord/stores/StoreStreamRtcConnection$State;", "getState", "()Lcom/discord/stores/StoreStreamRtcConnection$State;", "Lrx/Observable;", "observeStreamVolume", "()Lrx/Observable;", "observeRtcConnection", "observeConnectionQuality", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "Lcom/discord/models/domain/StreamCreateOrUpdate;", "streamCreate", "handleStreamCreate", "(Lcom/discord/models/domain/StreamCreateOrUpdate;)V", "Lcom/discord/models/domain/StreamServerUpdate;", "streamServerUpdate", "handleStreamServerUpdate", "(Lcom/discord/models/domain/StreamServerUpdate;)V", "handleStreamDelete", "Lcom/discord/rtcconnection/RtcConnection$State;", "state", "handleStreamRtcConnectionStateChange", "(Lcom/discord/rtcconnection/RtcConnection$State;)V", "volume", "updateStreamVolume", "(F)V", "focusedParticipant", "updateFocusedParticipant", "(Ljava/lang/Long;)V", "Lcom/discord/utilities/debug/DebugPrintableId;", "debugDisplayId", "J", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/stores/StoreRtcConnection;", "storeRtcConnection", "Lcom/discord/stores/StoreRtcConnection;", "Lcom/discord/stores/StoreStreamRtcConnection$State;", "Lcom/discord/utilities/logging/Logger;", "logger", "Lcom/discord/utilities/logging/Logger;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/stores/StoreStream;", "storeStream", "Lcom/discord/stores/StoreStream;", "streamVolume", "F", "Lcom/discord/utilities/networking/NetworkMonitor;", "Lcom/discord/utilities/collections/ListenerCollectionSubject;", "Lcom/discord/stores/StoreStreamRtcConnection$Listener;", "listenerSubject", "Lcom/discord/utilities/collections/ListenerCollectionSubject;", "Lcom/discord/utilities/debug/DebugPrintableCollection;", "dpc", "Lcom/discord/utilities/debug/DebugPrintableCollection;", "Lcom/discord/rtcconnection/RtcConnection;", "Lcom/discord/stores/StoreMediaEngine;", "mediaEngineStore", "Lcom/discord/stores/StoreMediaEngine;", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "Lcom/discord/stores/StoreAnalytics;", "analyticsStore", "Lcom/discord/stores/StoreAnalytics;", "Lcom/discord/stores/StoreUser;", "userStore", "Lcom/discord/stores/StoreUser;", "streamOwner", "Ljava/lang/Long;", "loggingTag", "Ljava/lang/String;", "Lcom/discord/utilities/collections/ListenerCollection;", "listeners", "Lcom/discord/utilities/collections/ListenerCollection;", "getListeners", "()Lcom/discord/utilities/collections/ListenerCollection;", HookHelper.constructorName, "(Lcom/discord/stores/StoreMediaEngine;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreAnalytics;Lcom/discord/stores/StoreRtcConnection;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/utilities/logging/Logger;Lcom/discord/utilities/debug/DebugPrintableCollection;)V", "Companion", "DefaultListener", "Listener", "RtcConnectionListener", "State", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStreamRtcConnection extends StoreV2 implements DebugPrintable {
    public static final Companion Companion = new Companion(null);
    public static final float MAX_STREAM_VOLUME = 300.0f;
    private static int instanceCounter;
    private final StoreAnalytics analyticsStore;
    private final Clock clock;
    private final long debugDisplayId;
    private final Dispatcher dispatcher;
    private final DebugPrintableCollection dpc;
    private final ListenerCollectionSubject<Listener> listenerSubject;
    private final ListenerCollection<Listener> listeners;
    private final Logger logger;
    private final String loggingTag;
    private final StoreMediaEngine mediaEngineStore;
    private NetworkMonitor networkMonitor;
    private final ObservationDeck observationDeck;
    private RtcConnection rtcConnection;
    private String sessionId;
    private State state;
    private final StoreRtcConnection storeRtcConnection;
    private final StoreStream storeStream;
    private Long streamOwner;
    private float streamVolume;
    private final StoreUser userStore;

    /* compiled from: StoreStreamRtcConnection.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00058\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/stores/StoreStreamRtcConnection$Companion;", "", "", "MAX_STREAM_VOLUME", "F", "", "instanceCounter", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreStreamRtcConnection.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\b&\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\f\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0004J\u000f\u0010\u0006\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0006\u0010\u0004J\u001b\u0010\n\u001a\u00020\u00022\n\u0010\t\u001a\u00060\u0007j\u0002`\bH\u0016¢\u0006\u0004\b\n\u0010\u000b¨\u0006\r"}, d2 = {"Lcom/discord/stores/StoreStreamRtcConnection$DefaultListener;", "Lcom/discord/stores/StoreStreamRtcConnection$Listener;", "", "onConnecting", "()V", "onConnected", "onFirstFrameSent", "", "Lcom/discord/primitives/SSRC;", "ssrc", "onFirstFrameReceived", "(J)V", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class DefaultListener implements Listener {
        @Override // com.discord.stores.StoreStreamRtcConnection.Listener
        public void onConnected() {
        }

        @Override // com.discord.stores.StoreStreamRtcConnection.Listener
        public void onConnecting() {
        }

        @Override // com.discord.stores.StoreStreamRtcConnection.Listener
        public void onFirstFrameReceived(long j) {
        }

        @Override // com.discord.stores.StoreStreamRtcConnection.Listener
        public void onFirstFrameSent() {
        }
    }

    /* compiled from: StoreStreamRtcConnection.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001J\u000f\u0010\u0003\u001a\u00020\u0002H&¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H&¢\u0006\u0004\b\u0005\u0010\u0004¨\u0006\u0006"}, d2 = {"Lcom/discord/stores/StoreStreamRtcConnection$Listener;", "", "", "onConnecting", "()V", "onConnected", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public interface Listener {
        void onConnected();

        void onConnecting();

        /* synthetic */ void onFirstFrameReceived(long j);

        /* synthetic */ void onFirstFrameSent();
    }

    /* compiled from: StoreStreamRtcConnection.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b'\u0010(J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J9\u0010\u000e\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00072\b\u0010\n\u001a\u0004\u0018\u00010\t2\u0006\u0010\u000b\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\tH\u0016¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u0010H\u0016¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0014H\u0016¢\u0006\u0004\b\u0016\u0010\u0017J\u000f\u0010\u0018\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0018\u0010\u0019J+\u0010 \u001a\u00020\u00042\u0006\u0010\u001b\u001a\u00020\u001a2\u0012\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\u001e0\u001cH\u0016¢\u0006\u0004\b \u0010!J\u000f\u0010\"\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\"\u0010\u0019J\u001b\u0010%\u001a\u00020\u00042\n\u0010$\u001a\u00060\u0007j\u0002`#H\u0016¢\u0006\u0004\b%\u0010&¨\u0006)"}, d2 = {"Lcom/discord/stores/StoreStreamRtcConnection$RtcConnectionListener;", "Lcom/discord/rtcconnection/RtcConnection$b;", "Lcom/discord/rtcconnection/RtcConnection$StateChange;", "stateChange", "", "onStateChange", "(Lcom/discord/rtcconnection/RtcConnection$StateChange;)V", "", "userId", "", "streamId", "audioSsrc", "videoSsrc", "rtxSsrc", "onVideoStream", "(JLjava/lang/Integer;III)V", "Lcom/discord/rtcconnection/VideoMetadata;", "metadata", "onVideoMetadata", "(Lcom/discord/rtcconnection/VideoMetadata;)V", "Lcom/discord/rtcconnection/RtcConnection$Quality;", "quality", "onQualityUpdate", "(Lcom/discord/rtcconnection/RtcConnection$Quality;)V", "onMediaSessionIdReceived", "()V", "Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;", "event", "", "", "", "properties", "onAnalyticsEvent", "(Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;Ljava/util/Map;)V", "onFirstFrameSent", "Lcom/discord/primitives/SSRC;", "ssrc", "onFirstFrameReceived", "(J)V", HookHelper.constructorName, "(Lcom/discord/stores/StoreStreamRtcConnection;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final class RtcConnectionListener extends RtcConnection.b {

        @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public final /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                RtcConnection.AnalyticsEvent.values();
                int[] iArr = new int[5];
                $EnumSwitchMapping$0 = iArr;
                iArr[RtcConnection.AnalyticsEvent.VIDEO_STREAM_ENDED.ordinal()] = 1;
                iArr[RtcConnection.AnalyticsEvent.MEDIA_SESSION_JOINED.ordinal()] = 2;
            }
        }

        public RtcConnectionListener() {
        }

        @Override // com.discord.rtcconnection.RtcConnection.b, com.discord.rtcconnection.RtcConnection.c
        public void onAnalyticsEvent(RtcConnection.AnalyticsEvent analyticsEvent, Map<String, Object> map) {
            m.checkNotNullParameter(analyticsEvent, "event");
            m.checkNotNullParameter(map, "properties");
            int ordinal = analyticsEvent.ordinal();
            if (ordinal == 3) {
                StoreStreamRtcConnection.this.dispatcher.schedule(new StoreStreamRtcConnection$RtcConnectionListener$onAnalyticsEvent$1(this, map));
            } else if (ordinal == 4) {
                StoreStreamRtcConnection.this.analyticsStore.trackMediaSessionJoined(map);
            }
        }

        @Override // com.discord.rtcconnection.RtcConnection.b
        public void onFirstFrameReceived(long j) {
            StoreStreamRtcConnection.this.listenerSubject.notify(new StoreStreamRtcConnection$RtcConnectionListener$onFirstFrameReceived$1(j));
        }

        @Override // com.discord.rtcconnection.RtcConnection.b
        public void onFirstFrameSent() {
            StoreStreamRtcConnection.this.listenerSubject.notify(StoreStreamRtcConnection$RtcConnectionListener$onFirstFrameSent$1.INSTANCE);
        }

        @Override // com.discord.rtcconnection.RtcConnection.b, com.discord.rtcconnection.RtcConnection.c
        public void onMediaSessionIdReceived() {
            StoreStreamRtcConnection.this.dispatcher.schedule(new StoreStreamRtcConnection$RtcConnectionListener$onMediaSessionIdReceived$1(this));
        }

        @Override // com.discord.rtcconnection.RtcConnection.b, com.discord.rtcconnection.RtcConnection.c
        public void onQualityUpdate(RtcConnection.Quality quality) {
            m.checkNotNullParameter(quality, "quality");
            StoreStreamRtcConnection.this.dispatcher.schedule(new StoreStreamRtcConnection$RtcConnectionListener$onQualityUpdate$1(this, quality));
        }

        @Override // com.discord.rtcconnection.RtcConnection.b, com.discord.rtcconnection.RtcConnection.c
        public void onStateChange(RtcConnection.StateChange stateChange) {
            m.checkNotNullParameter(stateChange, "stateChange");
            StoreStreamRtcConnection storeStreamRtcConnection = StoreStreamRtcConnection.this;
            storeStreamRtcConnection.recordBreadcrumb("store state change: " + stateChange);
            if (m.areEqual(stateChange.a, RtcConnection.State.f.a)) {
                StoreStreamRtcConnection.this.listenerSubject.notify(StoreStreamRtcConnection$RtcConnectionListener$onStateChange$1.INSTANCE);
            }
            StoreStreamRtcConnection.this.dispatcher.schedule(new StoreStreamRtcConnection$RtcConnectionListener$onStateChange$2(this, stateChange));
        }

        @Override // com.discord.rtcconnection.RtcConnection.b, com.discord.rtcconnection.RtcConnection.c
        public void onVideoMetadata(VideoMetadata videoMetadata) {
            m.checkNotNullParameter(videoMetadata, "metadata");
            StoreStreamRtcConnection.this.dispatcher.schedule(new StoreStreamRtcConnection$RtcConnectionListener$onVideoMetadata$1(this, videoMetadata));
        }

        @Override // com.discord.rtcconnection.RtcConnection.b, com.discord.rtcconnection.RtcConnection.c
        public void onVideoStream(long j, Integer num, int i, int i2, int i3) {
            StoreStreamRtcConnection.this.dispatcher.schedule(new StoreStreamRtcConnection$RtcConnectionListener$onVideoStream$1(this, j, num));
        }
    }

    /* compiled from: StoreStreamRtcConnection.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B3\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\u0005\u0012\u000e\u0010\u0011\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\f¢\u0006\u0004\b%\u0010&J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0018\u0010\n\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0012\u0010\r\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJD\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u000f\u001a\u00020\u00022\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00052\u0010\b\u0002\u0010\u0011\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t2\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\fHÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u0015\u0010\u000bJ\u0010\u0010\u0017\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u001a\u0010\u001b\u001a\u00020\u001a2\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001b\u0010\u001cR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001d\u001a\u0004\b\u001e\u0010\u0007R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u001f\u001a\u0004\b \u0010\u000eR!\u0010\u0011\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010!\u001a\u0004\b\"\u0010\u000bR\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010#\u001a\u0004\b$\u0010\u0004¨\u0006'"}, d2 = {"Lcom/discord/stores/StoreStreamRtcConnection$State;", "", "Lcom/discord/rtcconnection/RtcConnection$State;", "component1", "()Lcom/discord/rtcconnection/RtcConnection$State;", "Lcom/discord/rtcconnection/RtcConnection$Quality;", "component2", "()Lcom/discord/rtcconnection/RtcConnection$Quality;", "", "Lcom/discord/primitives/MediaSessionId;", "component3", "()Ljava/lang/String;", "Lcom/discord/rtcconnection/RtcConnection;", "component4", "()Lcom/discord/rtcconnection/RtcConnection;", "rtcConnectionState", "connectionQuality", "mediaSessionId", "rtcConnection", "copy", "(Lcom/discord/rtcconnection/RtcConnection$State;Lcom/discord/rtcconnection/RtcConnection$Quality;Ljava/lang/String;Lcom/discord/rtcconnection/RtcConnection;)Lcom/discord/stores/StoreStreamRtcConnection$State;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/rtcconnection/RtcConnection$Quality;", "getConnectionQuality", "Lcom/discord/rtcconnection/RtcConnection;", "getRtcConnection", "Ljava/lang/String;", "getMediaSessionId", "Lcom/discord/rtcconnection/RtcConnection$State;", "getRtcConnectionState", HookHelper.constructorName, "(Lcom/discord/rtcconnection/RtcConnection$State;Lcom/discord/rtcconnection/RtcConnection$Quality;Ljava/lang/String;Lcom/discord/rtcconnection/RtcConnection;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class State {
        private final RtcConnection.Quality connectionQuality;
        private final String mediaSessionId;
        private final RtcConnection rtcConnection;
        private final RtcConnection.State rtcConnectionState;

        public State(RtcConnection.State state, RtcConnection.Quality quality, String str, RtcConnection rtcConnection) {
            m.checkNotNullParameter(state, "rtcConnectionState");
            this.rtcConnectionState = state;
            this.connectionQuality = quality;
            this.mediaSessionId = str;
            this.rtcConnection = rtcConnection;
        }

        public static /* synthetic */ State copy$default(State state, RtcConnection.State state2, RtcConnection.Quality quality, String str, RtcConnection rtcConnection, int i, Object obj) {
            if ((i & 1) != 0) {
                state2 = state.rtcConnectionState;
            }
            if ((i & 2) != 0) {
                quality = state.connectionQuality;
            }
            if ((i & 4) != 0) {
                str = state.mediaSessionId;
            }
            if ((i & 8) != 0) {
                rtcConnection = state.rtcConnection;
            }
            return state.copy(state2, quality, str, rtcConnection);
        }

        public final RtcConnection.State component1() {
            return this.rtcConnectionState;
        }

        public final RtcConnection.Quality component2() {
            return this.connectionQuality;
        }

        public final String component3() {
            return this.mediaSessionId;
        }

        public final RtcConnection component4() {
            return this.rtcConnection;
        }

        public final State copy(RtcConnection.State state, RtcConnection.Quality quality, String str, RtcConnection rtcConnection) {
            m.checkNotNullParameter(state, "rtcConnectionState");
            return new State(state, quality, str, rtcConnection);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof State)) {
                return false;
            }
            State state = (State) obj;
            return m.areEqual(this.rtcConnectionState, state.rtcConnectionState) && m.areEqual(this.connectionQuality, state.connectionQuality) && m.areEqual(this.mediaSessionId, state.mediaSessionId) && m.areEqual(this.rtcConnection, state.rtcConnection);
        }

        public final RtcConnection.Quality getConnectionQuality() {
            return this.connectionQuality;
        }

        public final String getMediaSessionId() {
            return this.mediaSessionId;
        }

        public final RtcConnection getRtcConnection() {
            return this.rtcConnection;
        }

        public final RtcConnection.State getRtcConnectionState() {
            return this.rtcConnectionState;
        }

        public int hashCode() {
            RtcConnection.State state = this.rtcConnectionState;
            int i = 0;
            int hashCode = (state != null ? state.hashCode() : 0) * 31;
            RtcConnection.Quality quality = this.connectionQuality;
            int hashCode2 = (hashCode + (quality != null ? quality.hashCode() : 0)) * 31;
            String str = this.mediaSessionId;
            int hashCode3 = (hashCode2 + (str != null ? str.hashCode() : 0)) * 31;
            RtcConnection rtcConnection = this.rtcConnection;
            if (rtcConnection != null) {
                i = rtcConnection.hashCode();
            }
            return hashCode3 + i;
        }

        public String toString() {
            StringBuilder R = a.R("State(rtcConnectionState=");
            R.append(this.rtcConnectionState);
            R.append(", connectionQuality=");
            R.append(this.connectionQuality);
            R.append(", mediaSessionId=");
            R.append(this.mediaSessionId);
            R.append(", rtcConnection=");
            R.append(this.rtcConnection);
            R.append(")");
            return R.toString();
        }
    }

    public /* synthetic */ StoreStreamRtcConnection(StoreMediaEngine storeMediaEngine, StoreUser storeUser, StoreStream storeStream, Dispatcher dispatcher, Clock clock, StoreAnalytics storeAnalytics, StoreRtcConnection storeRtcConnection, ObservationDeck observationDeck, Logger logger, DebugPrintableCollection debugPrintableCollection, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(storeMediaEngine, storeUser, storeStream, dispatcher, clock, storeAnalytics, storeRtcConnection, (i & 128) != 0 ? ObservationDeckProvider.get() : observationDeck, (i & 256) != 0 ? AppLog.g : logger, (i & 512) != 0 ? SystemLogUtils.INSTANCE.getDebugPrintables$app_productionGoogleRelease() : debugPrintableCollection);
    }

    @StoreThread
    private final RtcConnection createRtcConnection(long j, Long l, long j2, String str, String str2, long j3, String str3) {
        destroyRtcConnection();
        RtcConnection.Metadata rtcConnectionMetadata = this.storeRtcConnection.getRtcConnectionMetadata();
        String str4 = null;
        Long l2 = rtcConnectionMetadata != null ? rtcConnectionMetadata.c : null;
        if (l2 != null && l2.longValue() == j2) {
            str4 = rtcConnectionMetadata.f2753b;
        }
        String str5 = str4;
        this.listenerSubject.notify(StoreStreamRtcConnection$createRtcConnection$1.INSTANCE);
        MediaEngine mediaEngine = this.mediaEngineStore.getMediaEngine();
        Logger logger = this.logger;
        Clock clock = this.clock;
        RtcConnection.d.b bVar = new RtcConnection.d.b(j3);
        NetworkMonitor networkMonitor = this.networkMonitor;
        if (networkMonitor == null) {
            m.throwUninitializedPropertyAccessException("networkMonitor");
        }
        RtcConnection rtcConnection = new RtcConnection(l, j2, str, true, str2, j, mediaEngine, logger, clock, bVar, networkMonitor, null, null, str5, false, this.loggingTag, str3, 6144);
        rtcConnection.c(new RtcConnectionListener());
        return rtcConnection;
    }

    @StoreThread
    private final void destroyRtcConnection() {
        if (this.rtcConnection != null) {
            recordBreadcrumb("destroying stream rtc connection");
            updateRtcConnection(null);
            this.state = State.copy$default(this.state, null, null, null, null, 9, null);
            markChanged();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleMediaSessionIdReceived() {
        RtcConnection rtcConnection = this.rtcConnection;
        this.state = State.copy$default(this.state, null, null, rtcConnection != null ? rtcConnection.K : null, null, 11, null);
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleQualityUpdate(RtcConnection.Quality quality) {
        this.state = State.copy$default(this.state, null, quality, null, null, 13, null);
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleVideoStreamEndedAnalyticsEvent(Map<String, Object> map) {
        this.analyticsStore.trackVideoStreamEnded(map);
    }

    private final void loge(String str, Throwable th, Map<String, String> map) {
        this.logger.e(this.loggingTag, str, th, map);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void loge$default(StoreStreamRtcConnection storeStreamRtcConnection, String str, Throwable th, Map map, int i, Object obj) {
        if ((i & 2) != 0) {
            th = null;
        }
        if ((i & 4) != 0) {
            map = null;
        }
        storeStreamRtcConnection.loge(str, th, map);
    }

    private final void logi(String str, Throwable th) {
        this.logger.i(this.loggingTag, str, th);
    }

    public static /* synthetic */ void logi$default(StoreStreamRtcConnection storeStreamRtcConnection, String str, Throwable th, int i, Object obj) {
        if ((i & 2) != 0) {
            th = null;
        }
        storeStreamRtcConnection.logi(str, th);
    }

    private final void logw(String str) {
        Logger.w$default(this.logger, this.loggingTag, str, null, 4, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void recordBreadcrumb(String str) {
        this.logger.recordBreadcrumb(str, this.loggingTag);
    }

    @StoreThread
    private final void updateRtcConnection(RtcConnection rtcConnection) {
        RtcConnection rtcConnection2 = this.rtcConnection;
        if (rtcConnection2 != null) {
            rtcConnection2.e();
        }
        this.rtcConnection = rtcConnection;
        this.state = State.copy$default(this.state, null, null, null, rtcConnection, 7, null);
        markChanged();
    }

    @Override // com.discord.utilities.debug.DebugPrintable
    public void debugPrint(DebugPrintBuilder debugPrintBuilder) {
        m.checkNotNullParameter(debugPrintBuilder, "dp");
        debugPrintBuilder.appendKeyValue("sessionId", this.sessionId);
        debugPrintBuilder.appendKeyValue("streamOwner", this.streamOwner);
        debugPrintBuilder.appendKeyValue("streamVolume", Float.valueOf(this.streamVolume));
        debugPrintBuilder.appendKeyValue("state", this.state);
        debugPrintBuilder.appendKeyValue("rtcConnection", (DebugPrintable) this.rtcConnection);
    }

    public final void finalize() {
        this.dpc.remove(this.debugDisplayId);
    }

    public final ListenerCollection<Listener> getListeners() {
        return this.listeners;
    }

    public final State getState() {
        return this.state;
    }

    public final float getStreamVolume() {
        return this.streamVolume;
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        this.sessionId = modelPayload.getSessionId();
    }

    @StoreThread
    public final void handleStreamCreate(StreamCreateOrUpdate streamCreateOrUpdate) {
        Long l;
        m.checkNotNullParameter(streamCreateOrUpdate, "streamCreate");
        ModelApplicationStream decodeStreamKey = ModelApplicationStream.Companion.decodeStreamKey(streamCreateOrUpdate.getStreamKey());
        long id2 = this.userStore.getMeInternal$app_productionGoogleRelease().getId();
        String str = this.sessionId;
        if (str != null) {
            RtcConnection rtcConnection = this.rtcConnection;
            if (rtcConnection != null && rtcConnection.P == decodeStreamKey.getChannelId()) {
                RtcConnection rtcConnection2 = this.rtcConnection;
                if (m.areEqual(rtcConnection2 != null ? rtcConnection2.Q : null, str)) {
                    return;
                }
            }
            if (decodeStreamKey instanceof ModelApplicationStream.GuildStream) {
                l = Long.valueOf(((ModelApplicationStream.GuildStream) decodeStreamKey).getGuildId());
            } else if (decodeStreamKey instanceof ModelApplicationStream.CallStream) {
                l = null;
            } else {
                throw new NoWhenBranchMatchedException();
            }
            long channelId = decodeStreamKey.getChannelId();
            String rtcServerId = streamCreateOrUpdate.getRtcServerId();
            m.checkNotNull(rtcServerId);
            updateRtcConnection(createRtcConnection(id2, l, channelId, str, rtcServerId, decodeStreamKey.getOwnerId(), streamCreateOrUpdate.getStreamKey()));
            this.streamOwner = Long.valueOf(decodeStreamKey.getOwnerId());
        }
    }

    @StoreThread
    public final void handleStreamDelete() {
        RtcConnection rtcConnection = this.rtcConnection;
        if (rtcConnection != null) {
            rtcConnection.t(null, null);
        }
        destroyRtcConnection();
        this.streamOwner = null;
    }

    @StoreThread
    public final void handleStreamRtcConnectionStateChange(RtcConnection.State state) {
        Long l;
        m.checkNotNullParameter(state, "state");
        if (m.areEqual(state, RtcConnection.State.f.a) && (l = this.streamOwner) != null) {
            long longValue = l.longValue();
            RtcConnection rtcConnection = this.rtcConnection;
            if (rtcConnection != null) {
                rtcConnection.v(longValue, this.streamVolume);
            }
        }
        this.state = State.copy$default(this.state, state, null, null, null, 12, null);
        markChanged();
    }

    @StoreThread
    public final void handleStreamServerUpdate(StreamServerUpdate streamServerUpdate) {
        m.checkNotNullParameter(streamServerUpdate, "streamServerUpdate");
        Objects.requireNonNull(App.Companion);
        SSLSocketFactory sSLSocketFactory = null;
        if (!App.access$getIS_LOCAL$cp()) {
            sSLSocketFactory = SecureSocketsLayerUtils.createSocketFactory$default(null, 1, null);
        }
        StringBuilder R = a.R("Voice stream update, connect to server w/ endpoint: ");
        R.append(streamServerUpdate.getEndpoint());
        recordBreadcrumb(R.toString());
        RtcConnection rtcConnection = this.rtcConnection;
        if (rtcConnection == null) {
            loge$default(this, "handleStreamServerUpdate() rtcConnection is null.", null, null, 6, null);
        } else {
            rtcConnection.s(new w(rtcConnection, streamServerUpdate.getEndpoint(), streamServerUpdate.getToken(), sSLSocketFactory));
        }
    }

    public final void init(NetworkMonitor networkMonitor) {
        m.checkNotNullParameter(networkMonitor, "networkMonitor");
        this.networkMonitor = networkMonitor;
    }

    public final Observable<RtcConnection.Quality> observeConnectionQuality() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreStreamRtcConnection$observeConnectionQuality$1(this), 14, null);
    }

    public final Observable<RtcConnection> observeRtcConnection() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreStreamRtcConnection$observeRtcConnection$1(this), 14, null);
    }

    public final Observable<Float> observeStreamVolume() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreStreamRtcConnection$observeStreamVolume$1(this), 14, null);
    }

    public final void updateFocusedParticipant(Long l) {
        MediaSinkWantsManager mediaSinkWantsManager;
        RtcConnection rtcConnection = this.rtcConnection;
        if (rtcConnection != null && (mediaSinkWantsManager = rtcConnection.H) != null) {
            mediaSinkWantsManager.b(new j(mediaSinkWantsManager, l));
        }
    }

    public final void updateStreamVolume(float f) {
        this.dispatcher.schedule(new StoreStreamRtcConnection$updateStreamVolume$1(this, f));
    }

    public StoreStreamRtcConnection(StoreMediaEngine storeMediaEngine, StoreUser storeUser, StoreStream storeStream, Dispatcher dispatcher, Clock clock, StoreAnalytics storeAnalytics, StoreRtcConnection storeRtcConnection, ObservationDeck observationDeck, Logger logger, DebugPrintableCollection debugPrintableCollection) {
        m.checkNotNullParameter(storeMediaEngine, "mediaEngineStore");
        m.checkNotNullParameter(storeUser, "userStore");
        m.checkNotNullParameter(storeStream, "storeStream");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(storeAnalytics, "analyticsStore");
        m.checkNotNullParameter(storeRtcConnection, "storeRtcConnection");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(logger, "logger");
        m.checkNotNullParameter(debugPrintableCollection, "dpc");
        this.mediaEngineStore = storeMediaEngine;
        this.userStore = storeUser;
        this.storeStream = storeStream;
        this.dispatcher = dispatcher;
        this.clock = clock;
        this.analyticsStore = storeAnalytics;
        this.storeRtcConnection = storeRtcConnection;
        this.observationDeck = observationDeck;
        this.logger = logger;
        this.dpc = debugPrintableCollection;
        ListenerCollectionSubject<Listener> listenerCollectionSubject = new ListenerCollectionSubject<>();
        this.listenerSubject = listenerCollectionSubject;
        this.listeners = listenerCollectionSubject;
        this.state = new State(new RtcConnection.State.d(false), null, null, this.rtcConnection);
        this.streamVolume = 300.0f;
        StringBuilder R = a.R("StoreStreamRtcConnection ");
        int i = instanceCounter + 1;
        instanceCounter = i;
        R.append(i);
        String sb = R.toString();
        this.loggingTag = sb;
        this.debugDisplayId = debugPrintableCollection.add(this, sb);
    }
}
