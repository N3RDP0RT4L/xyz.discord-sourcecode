package com.discord.stores;

import andhook.lib.HookHelper;
import androidx.annotation.VisibleForTesting;
import com.discord.utilities.lazy.subscriptions.GuildSubscriptionsManager;
import d0.d0.f;
import d0.t.n;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.ranges.IntProgression;
import kotlin.ranges.IntRange;
/* compiled from: StoreGuildSubscriptions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0001)B\u0017\u0012\u0006\u0010\u001f\u001a\u00020\u001e\u0012\u0006\u0010%\u001a\u00020$¢\u0006\u0004\b'\u0010(J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0005\u0010\u0006J\u001b\u0010\n\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\bH\u0007¢\u0006\u0004\b\n\u0010\u000bJ\u001b\u0010\f\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\bH\u0007¢\u0006\u0004\b\f\u0010\u000bJ\u000f\u0010\r\u001a\u00020\u0004H\u0007¢\u0006\u0004\b\r\u0010\u000eJ'\u0010\u0011\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\n\u0010\u0010\u001a\u00060\u0007j\u0002`\u000fH\u0007¢\u0006\u0004\b\u0011\u0010\u0012J'\u0010\u0013\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\n\u0010\u0010\u001a\u00060\u0007j\u0002`\u000fH\u0007¢\u0006\u0004\b\u0013\u0010\u0012J\u000f\u0010\u0014\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0014\u0010\u000eJ-\u0010\u0019\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\n\u0010\u0016\u001a\u00060\u0007j\u0002`\u00152\u0006\u0010\u0018\u001a\u00020\u0017¢\u0006\u0004\b\u0019\u0010\u001aJ%\u0010\u001b\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\n\u0010\u0016\u001a\u00060\u0007j\u0002`\u0015¢\u0006\u0004\b\u001b\u0010\u0012J%\u0010\u001c\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\n\u0010\u0010\u001a\u00060\u0007j\u0002`\u000f¢\u0006\u0004\b\u001c\u0010\u0012J%\u0010\u001d\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\n\u0010\u0010\u001a\u00060\u0007j\u0002`\u000f¢\u0006\u0004\b\u001d\u0010\u0012R\u0016\u0010\u001f\u001a\u00020\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u0016\u0010\"\u001a\u00020!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010#R\u0016\u0010%\u001a\u00020$8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b%\u0010&¨\u0006*"}, d2 = {"Lcom/discord/stores/StoreGuildSubscriptions;", "Lcom/discord/stores/StoreV2;", "", "isConnectionReady", "", "handleConnectionReady", "(Z)V", "", "Lcom/discord/primitives/GuildId;", "guildId", "handleGuildSelect", "(J)V", "handleGuildRemove", "handlePreLogout", "()V", "Lcom/discord/primitives/UserId;", "userId", "handleSubscribeMember", "(JJ)V", "handleUnsubscribeMember", "snapshotData", "Lcom/discord/primitives/ChannelId;", "channelId", "Lkotlin/ranges/IntRange;", "range", "subscribeChannelRange", "(JJLkotlin/ranges/IntRange;)V", "subscribeThread", "subscribeUser", "unsubscribeUser", "Lcom/discord/stores/StoreStream;", "storeStream", "Lcom/discord/stores/StoreStream;", "Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;", "subscriptionsManager", "Lcom/discord/utilities/lazy/subscriptions/GuildSubscriptionsManager;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;)V", "RangeComputer", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildSubscriptions extends StoreV2 {
    private final Dispatcher dispatcher;
    private final StoreStream storeStream;
    private final GuildSubscriptionsManager subscriptionsManager = new GuildSubscriptionsManager(new StoreGuildSubscriptions$subscriptionsManager$1(this));

    /* compiled from: StoreGuildSubscriptions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\b\u0007\bÁ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fJ%\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\b\b\u0002\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\t\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\t\u0010\n¨\u0006\r"}, d2 = {"Lcom/discord/stores/StoreGuildSubscriptions$RangeComputer;", "", "Lkotlin/ranges/IntRange;", "range", "", "chunkSize", "", "computeRanges", "(Lkotlin/ranges/IntRange;I)Ljava/util/List;", "DEFAULT_CHUNK_SIZE", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    @VisibleForTesting
    /* loaded from: classes.dex */
    public static final class RangeComputer {
        private static final int DEFAULT_CHUNK_SIZE = 100;
        public static final RangeComputer INSTANCE = new RangeComputer();

        private RangeComputer() {
        }

        public static /* synthetic */ List computeRanges$default(RangeComputer rangeComputer, IntRange intRange, int i, int i2, Object obj) {
            if ((i2 & 2) != 0) {
                i = 100;
            }
            return rangeComputer.computeRanges(intRange, i);
        }

        public final List<IntRange> computeRanges(IntRange intRange, int i) {
            m.checkNotNullParameter(intRange, "range");
            ArrayList arrayList = new ArrayList(3);
            int invoke = StoreGuildSubscriptions$RangeComputer$computeRanges$1.INSTANCE.invoke(intRange.getFirst(), i);
            if (invoke > 0) {
                arrayList.add(f.until(0, i));
            }
            IntProgression step = f.step(f.until(invoke, intRange.getLast()), i);
            int first = step.getFirst();
            int last = step.getLast();
            int step2 = step.getStep();
            if (step2 < 0 ? first >= last : first <= last) {
                while (true) {
                    arrayList.add(f.until(first, first + i));
                    if (first == last) {
                        break;
                    }
                    first += step2;
                }
            }
            return arrayList;
        }
    }

    public StoreGuildSubscriptions(StoreStream storeStream, Dispatcher dispatcher) {
        m.checkNotNullParameter(storeStream, "storeStream");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        this.storeStream = storeStream;
        this.dispatcher = dispatcher;
    }

    @StoreThread
    public final void handleConnectionReady(boolean z2) {
        if (z2) {
            long selectedGuildId = this.storeStream.getGuildSelected$app_productionGoogleRelease().getSelectedGuildId();
            if (selectedGuildId > 0) {
                handleGuildSelect(selectedGuildId);
            }
            this.subscriptionsManager.queueExistingSubscriptions();
            markChanged();
            return;
        }
        this.subscriptionsManager.retainAll(n.listOf((Object[]) new Long[]{Long.valueOf(this.storeStream.getGuildSelected$app_productionGoogleRelease().getSelectedGuildId()), Long.valueOf(this.storeStream.getRtcConnection$app_productionGoogleRelease().getConnectedGuildId())}));
        markChanged();
    }

    @StoreThread
    public final void handleGuildRemove(long j) {
        this.subscriptionsManager.remove(j);
        markChanged();
    }

    @StoreThread
    public final void handleGuildSelect(long j) {
        if (j > 0) {
            this.subscriptionsManager.subscribeTyping(j);
            this.subscriptionsManager.subscribeActivities(j);
            this.subscriptionsManager.subscribeThreads(j);
            markChanged();
        }
    }

    @StoreThread
    public final void handlePreLogout() {
        this.subscriptionsManager.reset();
        markChanged();
    }

    @StoreThread
    public final void handleSubscribeMember(long j, long j2) {
        if (j > 0) {
            this.subscriptionsManager.subscribeMember(j, j2);
            markChanged();
        }
    }

    @StoreThread
    public final void handleUnsubscribeMember(long j, long j2) {
        if (j > 0) {
            this.subscriptionsManager.unsubscribeMember(j, j2);
            markChanged();
        }
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        super.snapshotData();
        this.subscriptionsManager.flush();
    }

    public final void subscribeChannelRange(long j, long j2, IntRange intRange) {
        m.checkNotNullParameter(intRange, "range");
        if (j > 0 && j2 > 0) {
            this.dispatcher.schedule(new StoreGuildSubscriptions$subscribeChannelRange$1(this, intRange, j, j2));
        }
    }

    public final void subscribeThread(long j, long j2) {
        if (j > 0 && j2 > 0) {
            this.dispatcher.schedule(new StoreGuildSubscriptions$subscribeThread$1(this, j, j2));
        }
    }

    public final void subscribeUser(long j, long j2) {
        this.dispatcher.schedule(new StoreGuildSubscriptions$subscribeUser$1(this, j, j2));
    }

    public final void unsubscribeUser(long j, long j2) {
        this.dispatcher.schedule(new StoreGuildSubscriptions$unsubscribeUser$1(this, j, j2));
    }
}
