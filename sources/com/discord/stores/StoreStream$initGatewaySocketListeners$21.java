package com.discord.stores;

import com.discord.models.domain.ModelChannelUnreadUpdate;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreStream.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/ModelChannelUnreadUpdate;", "p1", "", "invoke", "(Lcom/discord/models/domain/ModelChannelUnreadUpdate;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final /* synthetic */ class StoreStream$initGatewaySocketListeners$21 extends k implements Function1<ModelChannelUnreadUpdate, Unit> {
    public StoreStream$initGatewaySocketListeners$21(StoreStream storeStream) {
        super(1, storeStream, StoreStream.class, "handleChannelUnreadUpdate", "handleChannelUnreadUpdate(Lcom/discord/models/domain/ModelChannelUnreadUpdate;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ModelChannelUnreadUpdate modelChannelUnreadUpdate) {
        invoke2(modelChannelUnreadUpdate);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ModelChannelUnreadUpdate modelChannelUnreadUpdate) {
        m.checkNotNullParameter(modelChannelUnreadUpdate, "p1");
        ((StoreStream) this.receiver).handleChannelUnreadUpdate(modelChannelUnreadUpdate);
    }
}
