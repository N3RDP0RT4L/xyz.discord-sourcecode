package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelGuildTemplate;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.t.h0;
import d0.z.d.m;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreGuildTemplates.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001:\u00010B\u001f\u0012\u0006\u0010'\u001a\u00020&\u0012\u0006\u0010*\u001a\u00020)\u0012\u0006\u0010\"\u001a\u00020!¢\u0006\u0004\b.\u0010/J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0003¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\r\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\f\u001a\u00020\u000bH\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0015\u0010\u0010\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u000f¢\u0006\u0004\b\u0010\u0010\u0011J\u000f\u0010\u0012\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0014\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0014\u0010\u0006J\r\u0010\u0015\u001a\u00020\u0004¢\u0006\u0004\b\u0015\u0010\u0016J\u0017\u0010\u0018\u001a\u0004\u0018\u00010\u00172\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u001b\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00170\u000f2\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u001a\u0010\u001bJ\u0015\u0010\u001c\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u001c\u0010\u0006J\u000f\u0010\u001d\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u001d\u0010\u0016R\"\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00170\u001e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u0016\u0010\"\u001a\u00020!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010#R\"\u0010%\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00170$8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b%\u0010 R\u0016\u0010'\u001a\u00020&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R\u0016\u0010*\u001a\u00020)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010+R\u0018\u0010,\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b,\u0010-¨\u00061"}, d2 = {"Lcom/discord/stores/StoreGuildTemplates;", "Lcom/discord/stores/StoreV2;", "", "guildTemplateCode", "", "requestGuildTemplate", "(Ljava/lang/String;)V", "Lcom/discord/models/domain/ModelGuildTemplate;", "guildTemplate", "handleRequestGuildTemplateSuccess", "(Lcom/discord/models/domain/ModelGuildTemplate;)V", "Lcom/discord/utilities/error/Error$Type;", "errorType", "handleRequestGuildTemplateError", "(Ljava/lang/String;Lcom/discord/utilities/error/Error$Type;)V", "Lrx/Observable;", "observeDynamicLinkGuildTemplateCode", "()Lrx/Observable;", "getDynamicLinkGuildTemplateCode", "()Ljava/lang/String;", "setDynamicLinkGuildTemplateCode", "clearDynamicLinkGuildTemplateCode", "()V", "Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;", "getGuildTemplate", "(Ljava/lang/String;)Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;", "observeGuildTemplate", "(Ljava/lang/String;)Lrx/Observable;", "maybeInitTemplateState", "snapshotData", "", "guildTemplatesByCodeSnapshot", "Ljava/util/Map;", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "", "guildTemplatesByCode", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "dynamicLinkGuildTemplateCode", "Ljava/lang/String;", HookHelper.constructorName, "(Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/rest/RestAPI;)V", "GuildTemplateState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildTemplates extends StoreV2 {
    private final Dispatcher dispatcher;
    private String dynamicLinkGuildTemplateCode;
    private final Map<String, GuildTemplateState> guildTemplatesByCode = new LinkedHashMap();
    private Map<String, ? extends GuildTemplateState> guildTemplatesByCodeSnapshot = h0.emptyMap();
    private final ObservationDeck observationDeck;
    private final RestAPI restAPI;

    /* compiled from: StoreGuildTemplates.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0005\u0004\u0005\u0006\u0007\bB\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0005\t\n\u000b\f\r¨\u0006\u000e"}, d2 = {"Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;", "", HookHelper.constructorName, "()V", "Invalid", "LoadFailed", "Loading", "None", "Resolved", "Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$None;", "Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Loading;", "Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$LoadFailed;", "Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Invalid;", "Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Resolved;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class GuildTemplateState {

        /* compiled from: StoreGuildTemplates.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Invalid;", "Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Invalid extends GuildTemplateState {
            public static final Invalid INSTANCE = new Invalid();

            private Invalid() {
                super(null);
            }
        }

        /* compiled from: StoreGuildTemplates.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$LoadFailed;", "Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class LoadFailed extends GuildTemplateState {
            public static final LoadFailed INSTANCE = new LoadFailed();

            private LoadFailed() {
                super(null);
            }
        }

        /* compiled from: StoreGuildTemplates.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Loading;", "Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Loading extends GuildTemplateState {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        /* compiled from: StoreGuildTemplates.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$None;", "Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class None extends GuildTemplateState {
            public static final None INSTANCE = new None();

            private None() {
                super(null);
            }
        }

        /* compiled from: StoreGuildTemplates.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Resolved;", "Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;", "Lcom/discord/models/domain/ModelGuildTemplate;", "component1", "()Lcom/discord/models/domain/ModelGuildTemplate;", "guildTemplate", "copy", "(Lcom/discord/models/domain/ModelGuildTemplate;)Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState$Resolved;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/ModelGuildTemplate;", "getGuildTemplate", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelGuildTemplate;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Resolved extends GuildTemplateState {
            private final ModelGuildTemplate guildTemplate;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Resolved(ModelGuildTemplate modelGuildTemplate) {
                super(null);
                m.checkNotNullParameter(modelGuildTemplate, "guildTemplate");
                this.guildTemplate = modelGuildTemplate;
            }

            public static /* synthetic */ Resolved copy$default(Resolved resolved, ModelGuildTemplate modelGuildTemplate, int i, Object obj) {
                if ((i & 1) != 0) {
                    modelGuildTemplate = resolved.guildTemplate;
                }
                return resolved.copy(modelGuildTemplate);
            }

            public final ModelGuildTemplate component1() {
                return this.guildTemplate;
            }

            public final Resolved copy(ModelGuildTemplate modelGuildTemplate) {
                m.checkNotNullParameter(modelGuildTemplate, "guildTemplate");
                return new Resolved(modelGuildTemplate);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Resolved) && m.areEqual(this.guildTemplate, ((Resolved) obj).guildTemplate);
                }
                return true;
            }

            public final ModelGuildTemplate getGuildTemplate() {
                return this.guildTemplate;
            }

            public int hashCode() {
                ModelGuildTemplate modelGuildTemplate = this.guildTemplate;
                if (modelGuildTemplate != null) {
                    return modelGuildTemplate.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Resolved(guildTemplate=");
                R.append(this.guildTemplate);
                R.append(")");
                return R.toString();
            }
        }

        private GuildTemplateState() {
        }

        public /* synthetic */ GuildTemplateState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            Error.Type.values();
            int[] iArr = new int[17];
            $EnumSwitchMapping$0 = iArr;
            iArr[Error.Type.DISCORD_REQUEST_ERROR.ordinal()] = 1;
            iArr[Error.Type.NETWORK.ordinal()] = 2;
        }
    }

    public StoreGuildTemplates(ObservationDeck observationDeck, Dispatcher dispatcher, RestAPI restAPI) {
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(restAPI, "restAPI");
        this.observationDeck = observationDeck;
        this.dispatcher = dispatcher;
        this.restAPI = restAPI;
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleRequestGuildTemplateError(String str, Error.Type type) {
        int ordinal = type.ordinal();
        if (ordinal == 3) {
            this.guildTemplatesByCode.put(str, GuildTemplateState.Invalid.INSTANCE);
            markChanged();
            AnalyticsTracker.guildTemplateResolveFailed(str);
        } else if (ordinal == 11) {
            this.guildTemplatesByCode.put(str, GuildTemplateState.LoadFailed.INSTANCE);
            markChanged();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleRequestGuildTemplateSuccess(ModelGuildTemplate modelGuildTemplate) {
        this.guildTemplatesByCode.put(modelGuildTemplate.getCode(), new GuildTemplateState.Resolved(modelGuildTemplate));
        markChanged();
        AnalyticsTracker.INSTANCE.guildTemplateResolved(modelGuildTemplate);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void requestGuildTemplate(String str) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.getGuildTemplateCode(str), false, 1, null), StoreGuildTemplates.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new StoreGuildTemplates$requestGuildTemplate$2(this, str), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreGuildTemplates$requestGuildTemplate$1(this));
    }

    public final void clearDynamicLinkGuildTemplateCode() {
        setDynamicLinkGuildTemplateCode(null);
    }

    public final String getDynamicLinkGuildTemplateCode() {
        return this.dynamicLinkGuildTemplateCode;
    }

    public final GuildTemplateState getGuildTemplate(String str) {
        m.checkNotNullParameter(str, "guildTemplateCode");
        return this.guildTemplatesByCodeSnapshot.get(str);
    }

    public final void maybeInitTemplateState(String str) {
        m.checkNotNullParameter(str, "guildTemplateCode");
        this.dispatcher.schedule(new StoreGuildTemplates$maybeInitTemplateState$1(this, str));
    }

    public final Observable<String> observeDynamicLinkGuildTemplateCode() {
        Observable<String> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreGuildTemplates$observeDynamicLinkGuildTemplateCode$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<GuildTemplateState> observeGuildTemplate(String str) {
        m.checkNotNullParameter(str, "guildTemplateCode");
        Observable<GuildTemplateState> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreGuildTemplates$observeGuildTemplate$1(this, str), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    public final void setDynamicLinkGuildTemplateCode(String str) {
        this.dispatcher.schedule(new StoreGuildTemplates$setDynamicLinkGuildTemplateCode$1(this, str));
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        super.snapshotData();
        this.guildTemplatesByCodeSnapshot = new HashMap(this.guildTemplatesByCode);
    }
}
