package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.SharedPreferences;
import com.discord.api.guild.GuildFeature;
import com.discord.api.user.SurveyGuildRequirements;
import com.discord.api.user.UserSurvey;
import com.discord.api.user.UserSurveyFetchResponse;
import com.discord.models.guild.Guild;
import com.discord.models.user.MeUser;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.utilities.RestCallStateKt;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.time.Clock;
import d0.z.d.m;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreUserSurvey.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001BI\u0012\u0006\u0010(\u001a\u00020'\u0012\u0006\u0010%\u001a\u00020$\u0012\u0006\u0010\u001c\u001a\u00020\u001b\u0012\u0006\u0010.\u001a\u00020-\u0012\u0006\u0010\u001f\u001a\u00020\u001e\u0012\b\b\u0002\u0010+\u001a\u00020*\u0012\u0006\u0010\u0016\u001a\u00020\u0015\u0012\u0006\u0010\"\u001a\u00020!¢\u0006\u0004\b2\u00103J\u0011\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0006\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\n\u001a\u00020\t2\u0006\u0010\b\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0019\u0010\u000e\u001a\u00020\u00052\b\u0010\r\u001a\u0004\u0018\u00010\fH\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u000f\u0010\u0010\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\u0010\u0010\u0007J\u0015\u0010\u0012\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u0011¢\u0006\u0004\b\u0012\u0010\u0013J\u000f\u0010\u0014\u001a\u00020\u0005H\u0007¢\u0006\u0004\b\u0014\u0010\u0007R\u0016\u0010\u0016\u001a\u00020\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0019\u001a\u00020\u00188\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001c\u001a\u00020\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR\u0016\u0010\u001f\u001a\u00020\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u0016\u0010\"\u001a\u00020!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010#R\u0016\u0010%\u001a\u00020$8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b%\u0010&R\u0016\u0010(\u001a\u00020'8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u0010)R\u0016\u0010+\u001a\u00020*8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b+\u0010,R\u0016\u0010.\u001a\u00020-8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b.\u0010/R\u0018\u00100\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b0\u00101¨\u00064"}, d2 = {"Lcom/discord/stores/StoreUserSurvey;", "Lcom/discord/stores/StoreV2;", "Lcom/discord/api/user/UserSurvey;", "getUserSurvey", "()Lcom/discord/api/user/UserSurvey;", "", "setLastFetched", "()V", "survey", "", "meetsGuildRequirements", "(Lcom/discord/api/user/UserSurvey;)Z", "Lcom/discord/api/user/UserSurveyFetchResponse;", "res", "handleUserSurveyFetchSuccess", "(Lcom/discord/api/user/UserSurveyFetchResponse;)V", "fetchUserSurvey", "Lrx/Observable;", "observeUserSurvey", "()Lrx/Observable;", "handleConnectionOpen", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "", "refetchIntervalMs", "I", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreGuilds;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Landroid/content/SharedPreferences;", "sharedPreferences", "Landroid/content/SharedPreferences;", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreUser;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/utilities/rest/RestAPI;", "restApi", "Lcom/discord/utilities/rest/RestAPI;", "Lcom/discord/stores/StorePermissions;", "storePermissions", "Lcom/discord/stores/StorePermissions;", "userSurvey", "Lcom/discord/api/user/UserSurvey;", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/utilities/time/Clock;Landroid/content/SharedPreferences;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserSurvey extends StoreV2 {
    private final Clock clock;
    private final Dispatcher dispatcher;
    private final ObservationDeck observationDeck;
    private int refetchIntervalMs;
    private final RestAPI restApi;
    private final SharedPreferences sharedPreferences;
    private final StoreGuilds storeGuilds;
    private final StorePermissions storePermissions;
    private final StoreUser storeUser;
    private UserSurvey userSurvey;

    public /* synthetic */ StoreUserSurvey(Dispatcher dispatcher, StoreUser storeUser, StoreGuilds storeGuilds, StorePermissions storePermissions, ObservationDeck observationDeck, RestAPI restAPI, Clock clock, SharedPreferences sharedPreferences, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(dispatcher, storeUser, storeGuilds, storePermissions, observationDeck, (i & 32) != 0 ? RestAPI.Companion.getApi() : restAPI, clock, sharedPreferences);
    }

    @StoreThread
    private final void fetchUserSurvey() {
        setLastFetched();
        ObservableExtensionsKt.appSubscribe$default(ObservableExtensionsKt.restSubscribeOn$default(RestCallStateKt.logNetworkAction(this.restApi.getUserSurvey(), StoreUserSurvey$fetchUserSurvey$1.INSTANCE), false, 1, null), StoreUserSurvey.class, (Context) null, (Function1) null, (Function1) null, (Function0) null, (Function0) null, new StoreUserSurvey$fetchUserSurvey$2(this), 62, (Object) null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final UserSurvey getUserSurvey() {
        return this.userSurvey;
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleUserSurveyFetchSuccess(UserSurveyFetchResponse userSurveyFetchResponse) {
        UserSurvey a;
        if (userSurveyFetchResponse != null && (a = userSurveyFetchResponse.a()) != null && meetsGuildRequirements(a)) {
            this.userSurvey = a;
            markChanged();
        }
    }

    private final boolean meetsGuildRequirements(UserSurvey userSurvey) {
        boolean z2;
        List<String> c = userSurvey.c();
        if (c.isEmpty()) {
            return true;
        }
        for (String str : c) {
            if (!StoreUserSurveyKt.getIMPLEMENTED_GUILD_REQUIREMENTS().contains(str)) {
                return false;
            }
        }
        MeUser me2 = this.storeUser.getMe();
        for (Map.Entry<Long, Guild> entry : this.storeGuilds.getGuilds().entrySet()) {
            long longValue = entry.getKey().longValue();
            Guild value = entry.getValue();
            if (!c.contains(SurveyGuildRequirements.IS_COMMUNITY.getValue()) || value.getFeatures().contains(GuildFeature.COMMUNITY)) {
                if (!c.contains(SurveyGuildRequirements.IS_HUB.getValue()) || value.getFeatures().contains(GuildFeature.HUB)) {
                    if (c.contains(SurveyGuildRequirements.GUILD_SIZE.getValue())) {
                        Integer num = userSurvey.d().get(0);
                        Integer num2 = userSurvey.d().get(1);
                        int memberCount = value.getMemberCount();
                        if (num == null || memberCount >= num.intValue()) {
                            if (num2 != null && memberCount > num2.intValue()) {
                            }
                        }
                    }
                    Map<Long, Long> guildPermissions = this.storePermissions.getGuildPermissions();
                    if (c.contains(SurveyGuildRequirements.GUILD_PERMISSIONS.getValue())) {
                        List<Long> b2 = userSurvey.b();
                        if (b2.isEmpty()) {
                            continue;
                        } else {
                            Iterator<Long> it = b2.iterator();
                            while (true) {
                                if (it.hasNext()) {
                                    if (PermissionUtils.can(it.next().longValue(), guildPermissions.get(Long.valueOf(longValue)))) {
                                        z2 = true;
                                        break;
                                    }
                                } else {
                                    z2 = false;
                                    break;
                                }
                            }
                            if (!z2) {
                                continue;
                            }
                        }
                    }
                    boolean isOwner = value.isOwner(me2.getId());
                    boolean can = PermissionUtils.can(8L, guildPermissions.get(Long.valueOf(longValue)));
                    if (!c.contains(SurveyGuildRequirements.IS_OWNER.getValue()) || isOwner) {
                        if (!c.contains(SurveyGuildRequirements.IS_ADMIN.getValue()) || can) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private final void setLastFetched() {
        this.sharedPreferences.edit().putLong("CACHE_KEY_USER_SURVEY_LAST_FETCHED", this.clock.currentTimeMillis()).apply();
    }

    @StoreThread
    public final void handleConnectionOpen() {
        if (this.clock.currentTimeMillis() - this.sharedPreferences.getLong("CACHE_KEY_USER_SURVEY_LAST_FETCHED", 0L) > this.refetchIntervalMs) {
            fetchUserSurvey();
        }
    }

    public final Observable<UserSurvey> observeUserSurvey() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreUserSurvey$observeUserSurvey$1(this), 14, null);
    }

    public StoreUserSurvey(Dispatcher dispatcher, StoreUser storeUser, StoreGuilds storeGuilds, StorePermissions storePermissions, ObservationDeck observationDeck, RestAPI restAPI, Clock clock, SharedPreferences sharedPreferences) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(storeUser, "storeUser");
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        m.checkNotNullParameter(storePermissions, "storePermissions");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(restAPI, "restApi");
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(sharedPreferences, "sharedPreferences");
        this.dispatcher = dispatcher;
        this.storeUser = storeUser;
        this.storeGuilds = storeGuilds;
        this.storePermissions = storePermissions;
        this.observationDeck = observationDeck;
        this.restApi = restAPI;
        this.clock = clock;
        this.sharedPreferences = sharedPreferences;
        this.refetchIntervalMs = 86400000;
    }
}
