package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.emoji.GuildEmoji;
import com.discord.api.guild.Guild;
import com.discord.api.guild.GuildExplicitContentFilter;
import com.discord.api.guild.GuildFeature;
import com.discord.api.guild.GuildMaxVideoChannelUsers;
import com.discord.api.guildmember.GuildMember;
import com.discord.api.presence.Presence;
import com.discord.api.role.GuildRole;
import com.discord.api.sticker.Sticker;
import com.discord.api.user.User;
import com.discord.models.domain.ModelPayload;
import com.discord.models.domain.ModelUserRelationship;
import com.discord.models.domain.emoji.ModelEmojiCustom;
import com.discord.utilities.guilds.GuildUtilsKt;
import d0.d0.f;
import d0.t.g0;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ReadyPayloadUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u009a\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\bÆ\u0002\u0018\u00002\u00020\u0001:\u0003ABCB\t\b\u0002¢\u0006\u0004\b?\u0010@J/\u0010\t\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00022\u0016\u0010\b\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u00070\u0004H\u0002¢\u0006\u0004\b\t\u0010\nJ\u001d\u0010\r\u001a\u00020\u000b*\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u000bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ'\u0010\r\u001a\u00020\u000b*\u00020\u000b2\u0012\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00070\u0004H\u0002¢\u0006\u0004\b\r\u0010\u000fJ7\u0010\u001b\u001a\u00020\u001a2\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u0018H\u0007¢\u0006\u0004\b\u001b\u0010\u001cJ\u0017\u0010\u001e\u001a\u00020\u00102\u0006\u0010\u001d\u001a\u00020\u0010H\u0007¢\u0006\u0004\b\u001e\u0010\u001fJ-\u0010\"\u001a\u00020 2\u0006\u0010!\u001a\u00020 2\u0016\u0010\b\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u00070\u0004¢\u0006\u0004\b\"\u0010#JA\u0010*\u001a\u00020)2\u0006\u0010%\u001a\u00020$2\u0010\b\u0002\u0010'\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010&2\u0016\b\u0002\u0010(\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020 \u0018\u00010\u0004H\u0007¢\u0006\u0004\b*\u0010+J\u009f\u0001\u0010\r\u001a\u00020$2\u0006\u0010,\u001a\u00020$2\b\u0010-\u001a\u0004\u0018\u00010$2\u000e\u0010/\u001a\n\u0012\u0004\u0012\u00020.\u0018\u00010&2\u000e\u00101\u001a\n\u0012\u0004\u0012\u000200\u0018\u00010&2\u000e\u00102\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010&2\u000e\u00104\u001a\n\u0012\u0004\u0012\u000203\u0018\u00010&2\u000e\u0010'\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010&2\u0014\u0010(\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020 \u0018\u00010\u00042\u0018\u00106\u001a\u0014\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000b05¢\u0006\u0004\b\r\u00107R5\u0010;\u001a\u001e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020908j\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u000209`:8\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010<\u001a\u0004\b=\u0010>¨\u0006D"}, d2 = {"Lcom/discord/stores/ReadyPayloadUtils;", "", "Lcom/discord/api/presence/Presence;", "presence", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/api/user/User;", "users", "hydratePresence", "(Lcom/discord/api/presence/Presence;Ljava/util/Map;)Lcom/discord/api/presence/Presence;", "Lcom/discord/api/channel/Channel;", "channel", "hydrate", "(Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;)Lcom/discord/api/channel/Channel;", "(Lcom/discord/api/channel/Channel;Ljava/util/Map;)Lcom/discord/api/channel/Channel;", "Lcom/discord/models/domain/ModelPayload;", "payload_", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreEmojiCustom;", "storeEmojiCustom", "Lcom/discord/stores/StoreGuildStickers;", "storeGuildStickers", "Lcom/discord/stores/ReadyPayloadUtils$HydrateResult;", "hydrateReadyPayload", "(Lcom/discord/models/domain/ModelPayload;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreEmojiCustom;Lcom/discord/stores/StoreGuildStickers;)Lcom/discord/stores/ReadyPayloadUtils$HydrateResult;", "payload", "hydrateUsers", "(Lcom/discord/models/domain/ModelPayload;)Lcom/discord/models/domain/ModelPayload;", "Lcom/discord/api/guildmember/GuildMember;", "guildMember", "hydrateGuildMember", "(Lcom/discord/api/guildmember/GuildMember;Ljava/util/Map;)Lcom/discord/api/guildmember/GuildMember;", "Lcom/discord/api/guild/Guild;", "guild", "", "presences", "members", "Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult;", "hydrateGuild", "(Lcom/discord/api/guild/Guild;Ljava/util/List;Ljava/util/Map;)Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult;", "unhydratedGuild", "oldGuild", "Lcom/discord/api/emoji/GuildEmoji;", "oldCustomEmojis", "Lcom/discord/api/sticker/Sticker;", "oldStickers", "oldChannels", "Lcom/discord/api/role/GuildRole;", "oldRoles", "Lkotlin/Function2;", "onHydrateChannel", "(Lcom/discord/api/guild/Guild;Lcom/discord/api/guild/Guild;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/Map;Lkotlin/jvm/functions/Function2;)Lcom/discord/api/guild/Guild;", "Ljava/util/HashMap;", "Lcom/discord/stores/ReadyPayloadUtils$GuildCache;", "Lkotlin/collections/HashMap;", "cache", "Ljava/util/HashMap;", "getCache", "()Ljava/util/HashMap;", HookHelper.constructorName, "()V", "GuildCache", "HydrateGuildResult", "HydrateResult", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ReadyPayloadUtils {
    public static final ReadyPayloadUtils INSTANCE = new ReadyPayloadUtils();
    private static final HashMap<Long, GuildCache> cache = new HashMap<>();

    /* compiled from: ReadyPayloadUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001BO\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\u000e\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005\u0012\u000e\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u0005\u0012\u000e\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u0005\u0012\u000e\u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\u0005¢\u0006\u0004\b'\u0010(J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0018\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0018\u0010\n\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\n\u0010\bJ\u0018\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\f\u0010\bJ\u0018\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u000e\u0010\bJb\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u000f\u001a\u00020\u00022\u0010\b\u0002\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00052\u0010\b\u0002\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u00052\u0010\b\u0002\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u00052\u0010\b\u0002\u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0017\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u001a\u001a\u00020\u0019HÖ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u001a\u0010\u001e\u001a\u00020\u001d2\b\u0010\u001c\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001e\u0010\u001fR!\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010 \u001a\u0004\b!\u0010\bR\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\"\u001a\u0004\b#\u0010\u0004R!\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010 \u001a\u0004\b$\u0010\bR!\u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010 \u001a\u0004\b%\u0010\bR!\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010 \u001a\u0004\b&\u0010\b¨\u0006)"}, d2 = {"Lcom/discord/stores/ReadyPayloadUtils$GuildCache;", "", "Lcom/discord/api/guild/Guild;", "component1", "()Lcom/discord/api/guild/Guild;", "", "Lcom/discord/api/emoji/GuildEmoji;", "component2", "()Ljava/util/List;", "Lcom/discord/api/sticker/Sticker;", "component3", "Lcom/discord/api/channel/Channel;", "component4", "Lcom/discord/api/role/GuildRole;", "component5", "guild", "emojis", "stickers", "channels", "roles", "copy", "(Lcom/discord/api/guild/Guild;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/discord/stores/ReadyPayloadUtils$GuildCache;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getEmojis", "Lcom/discord/api/guild/Guild;", "getGuild", "getChannels", "getRoles", "getStickers", HookHelper.constructorName, "(Lcom/discord/api/guild/Guild;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class GuildCache {
        private final List<Channel> channels;
        private final List<GuildEmoji> emojis;
        private final Guild guild;
        private final List<GuildRole> roles;
        private final List<Sticker> stickers;

        public GuildCache(Guild guild, List<GuildEmoji> list, List<Sticker> list2, List<Channel> list3, List<GuildRole> list4) {
            m.checkNotNullParameter(guild, "guild");
            this.guild = guild;
            this.emojis = list;
            this.stickers = list2;
            this.channels = list3;
            this.roles = list4;
        }

        public static /* synthetic */ GuildCache copy$default(GuildCache guildCache, Guild guild, List list, List list2, List list3, List list4, int i, Object obj) {
            if ((i & 1) != 0) {
                guild = guildCache.guild;
            }
            List<GuildEmoji> list5 = list;
            if ((i & 2) != 0) {
                list5 = guildCache.emojis;
            }
            List list6 = list5;
            List<Sticker> list7 = list2;
            if ((i & 4) != 0) {
                list7 = guildCache.stickers;
            }
            List list8 = list7;
            List<Channel> list9 = list3;
            if ((i & 8) != 0) {
                list9 = guildCache.channels;
            }
            List list10 = list9;
            List<GuildRole> list11 = list4;
            if ((i & 16) != 0) {
                list11 = guildCache.roles;
            }
            return guildCache.copy(guild, list6, list8, list10, list11);
        }

        public final Guild component1() {
            return this.guild;
        }

        public final List<GuildEmoji> component2() {
            return this.emojis;
        }

        public final List<Sticker> component3() {
            return this.stickers;
        }

        public final List<Channel> component4() {
            return this.channels;
        }

        public final List<GuildRole> component5() {
            return this.roles;
        }

        public final GuildCache copy(Guild guild, List<GuildEmoji> list, List<Sticker> list2, List<Channel> list3, List<GuildRole> list4) {
            m.checkNotNullParameter(guild, "guild");
            return new GuildCache(guild, list, list2, list3, list4);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof GuildCache)) {
                return false;
            }
            GuildCache guildCache = (GuildCache) obj;
            return m.areEqual(this.guild, guildCache.guild) && m.areEqual(this.emojis, guildCache.emojis) && m.areEqual(this.stickers, guildCache.stickers) && m.areEqual(this.channels, guildCache.channels) && m.areEqual(this.roles, guildCache.roles);
        }

        public final List<Channel> getChannels() {
            return this.channels;
        }

        public final List<GuildEmoji> getEmojis() {
            return this.emojis;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final List<GuildRole> getRoles() {
            return this.roles;
        }

        public final List<Sticker> getStickers() {
            return this.stickers;
        }

        public int hashCode() {
            Guild guild = this.guild;
            int i = 0;
            int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
            List<GuildEmoji> list = this.emojis;
            int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
            List<Sticker> list2 = this.stickers;
            int hashCode3 = (hashCode2 + (list2 != null ? list2.hashCode() : 0)) * 31;
            List<Channel> list3 = this.channels;
            int hashCode4 = (hashCode3 + (list3 != null ? list3.hashCode() : 0)) * 31;
            List<GuildRole> list4 = this.roles;
            if (list4 != null) {
                i = list4.hashCode();
            }
            return hashCode4 + i;
        }

        public String toString() {
            StringBuilder R = a.R("GuildCache(guild=");
            R.append(this.guild);
            R.append(", emojis=");
            R.append(this.emojis);
            R.append(", stickers=");
            R.append(this.stickers);
            R.append(", channels=");
            R.append(this.channels);
            R.append(", roles=");
            return a.K(R, this.roles, ")");
        }
    }

    /* compiled from: ReadyPayloadUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult;", "", HookHelper.constructorName, "()V", "Error", "Success", "Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult$Success;", "Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult$Error;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class HydrateGuildResult {

        /* compiled from: ReadyPayloadUtils.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult$Error;", "Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Error extends HydrateGuildResult {
            public static final Error INSTANCE = new Error();

            private Error() {
                super(null);
            }
        }

        /* compiled from: ReadyPayloadUtils.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult$Success;", "Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult;", "Lcom/discord/api/guild/Guild;", "component1", "()Lcom/discord/api/guild/Guild;", "guild", "copy", "(Lcom/discord/api/guild/Guild;)Lcom/discord/stores/ReadyPayloadUtils$HydrateGuildResult$Success;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/guild/Guild;", "getGuild", HookHelper.constructorName, "(Lcom/discord/api/guild/Guild;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Success extends HydrateGuildResult {
            private final Guild guild;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Success(Guild guild) {
                super(null);
                m.checkNotNullParameter(guild, "guild");
                this.guild = guild;
            }

            public static /* synthetic */ Success copy$default(Success success, Guild guild, int i, Object obj) {
                if ((i & 1) != 0) {
                    guild = success.guild;
                }
                return success.copy(guild);
            }

            public final Guild component1() {
                return this.guild;
            }

            public final Success copy(Guild guild) {
                m.checkNotNullParameter(guild, "guild");
                return new Success(guild);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Success) && m.areEqual(this.guild, ((Success) obj).guild);
                }
                return true;
            }

            public final Guild getGuild() {
                return this.guild;
            }

            public int hashCode() {
                Guild guild = this.guild;
                if (guild != null) {
                    return guild.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Success(guild=");
                R.append(this.guild);
                R.append(")");
                return R.toString();
            }
        }

        private HydrateGuildResult() {
        }

        public /* synthetic */ HydrateGuildResult(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: ReadyPayloadUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/stores/ReadyPayloadUtils$HydrateResult;", "", HookHelper.constructorName, "()V", "Error", "Success", "Lcom/discord/stores/ReadyPayloadUtils$HydrateResult$Success;", "Lcom/discord/stores/ReadyPayloadUtils$HydrateResult$Error;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class HydrateResult {

        /* compiled from: ReadyPayloadUtils.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/ReadyPayloadUtils$HydrateResult$Error;", "Lcom/discord/stores/ReadyPayloadUtils$HydrateResult;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Error extends HydrateResult {
            public static final Error INSTANCE = new Error();

            private Error() {
                super(null);
            }
        }

        /* compiled from: ReadyPayloadUtils.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/stores/ReadyPayloadUtils$HydrateResult$Success;", "Lcom/discord/stores/ReadyPayloadUtils$HydrateResult;", "Lcom/discord/models/domain/ModelPayload;", "component1", "()Lcom/discord/models/domain/ModelPayload;", "payload", "copy", "(Lcom/discord/models/domain/ModelPayload;)Lcom/discord/stores/ReadyPayloadUtils$HydrateResult$Success;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/ModelPayload;", "getPayload", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelPayload;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Success extends HydrateResult {
            private final ModelPayload payload;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Success(ModelPayload modelPayload) {
                super(null);
                m.checkNotNullParameter(modelPayload, "payload");
                this.payload = modelPayload;
            }

            public static /* synthetic */ Success copy$default(Success success, ModelPayload modelPayload, int i, Object obj) {
                if ((i & 1) != 0) {
                    modelPayload = success.payload;
                }
                return success.copy(modelPayload);
            }

            public final ModelPayload component1() {
                return this.payload;
            }

            public final Success copy(ModelPayload modelPayload) {
                m.checkNotNullParameter(modelPayload, "payload");
                return new Success(modelPayload);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Success) && m.areEqual(this.payload, ((Success) obj).payload);
                }
                return true;
            }

            public final ModelPayload getPayload() {
                return this.payload;
            }

            public int hashCode() {
                ModelPayload modelPayload = this.payload;
                if (modelPayload != null) {
                    return modelPayload.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Success(payload=");
                R.append(this.payload);
                R.append(")");
                return R.toString();
            }
        }

        private HydrateResult() {
        }

        public /* synthetic */ HydrateResult(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    private ReadyPayloadUtils() {
    }

    public final Channel hydrate(Channel channel, Channel channel2) {
        return (channel2 == null || channel2.i() == 0) ? channel : Channel.a(channel, null, 0, 0L, null, channel2.i(), 0L, 0L, null, null, 0, null, 0, 0, null, 0L, 0L, null, false, 0L, null, 0, null, null, null, null, null, null, null, null, 536870895);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ HydrateGuildResult hydrateGuild$default(ReadyPayloadUtils readyPayloadUtils, Guild guild, List list, Map map, int i, Object obj) {
        if ((i & 2) != 0) {
            list = null;
        }
        if ((i & 4) != 0) {
            map = null;
        }
        return readyPayloadUtils.hydrateGuild(guild, list, map);
    }

    private final Presence hydratePresence(Presence presence, Map<Long, User> map) {
        User user;
        Long g = presence.g();
        if (g == null || (user = map.get(g)) == null) {
            return Presence.a(presence, null, null, null, null, null, null, 47);
        }
        return Presence.a(presence, null, null, null, user, null, null, 39);
    }

    public final HashMap<Long, GuildCache> getCache() {
        return cache;
    }

    /* JADX WARN: Removed duplicated region for block: B:39:0x00a5  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x00b4  */
    /* JADX WARN: Removed duplicated region for block: B:53:0x00c4  */
    /* JADX WARN: Removed duplicated region for block: B:60:0x00d4  */
    @com.discord.stores.StoreThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final com.discord.stores.ReadyPayloadUtils.HydrateGuildResult hydrateGuild(com.discord.api.guild.Guild r13, java.util.List<com.discord.api.presence.Presence> r14, java.util.Map<java.lang.Long, com.discord.api.guildmember.GuildMember> r15) {
        /*
            Method dump skipped, instructions count: 247
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.ReadyPayloadUtils.hydrateGuild(com.discord.api.guild.Guild, java.util.List, java.util.Map):com.discord.stores.ReadyPayloadUtils$HydrateGuildResult");
    }

    public final GuildMember hydrateGuildMember(GuildMember guildMember, Map<Long, User> map) {
        User user;
        m.checkNotNullParameter(guildMember, "guildMember");
        m.checkNotNullParameter(map, "users");
        if (guildMember.n() == null || (user = map.get(guildMember.n())) == null) {
            return guildMember;
        }
        return GuildMember.a(guildMember, 0L, user, null, null, null, null, false, null, null, null, null, null, null, 7933);
    }

    @StoreThread
    public final HydrateResult hydrateReadyPayload(ModelPayload modelPayload, StoreGuilds storeGuilds, StoreChannels storeChannels, StoreEmojiCustom storeEmojiCustom, StoreGuildStickers storeGuildStickers) {
        LinkedHashMap linkedHashMap;
        List<GuildMember> list;
        ArrayList arrayList;
        m.checkNotNullParameter(modelPayload, "payload_");
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        m.checkNotNullParameter(storeChannels, "storeChannels");
        m.checkNotNullParameter(storeEmojiCustom, "storeEmojiCustom");
        m.checkNotNullParameter(storeGuildStickers, "storeGuildStickers");
        ModelPayload hydrateUsers = hydrateUsers(modelPayload);
        Map<Long, Map<Long, GuildRole>> roles = storeGuilds.getRoles();
        Iterator<T> it = storeGuilds.getGuilds().values().iterator();
        while (true) {
            List list2 = null;
            if (!it.hasNext()) {
                break;
            }
            com.discord.models.guild.Guild guild = (com.discord.models.guild.Guild) it.next();
            Map map = (Map) a.d(guild, roles);
            Collection values = map != null ? map.values() : null;
            Map<Long, Channel> channelsForGuildInternal$app_productionGoogleRelease = storeChannels.getChannelsForGuildInternal$app_productionGoogleRelease(guild.getId());
            Collection<Channel> values2 = channelsForGuildInternal$app_productionGoogleRelease != null ? channelsForGuildInternal$app_productionGoogleRelease.values() : null;
            Map<Long, ModelEmojiCustom> emojiForGuildInternal = storeEmojiCustom.getEmojiForGuildInternal(guild.getId());
            Collection<ModelEmojiCustom> values3 = emojiForGuildInternal != null ? emojiForGuildInternal.values() : null;
            Collection<Sticker> values4 = storeGuildStickers.getStickersForGuild(guild.getId()).values();
            HashMap<Long, GuildCache> hashMap = cache;
            Long valueOf = Long.valueOf(guild.getId());
            Guild createApiGuild = GuildUtilsKt.createApiGuild(guild);
            if (values3 != null) {
                ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(values3, 10));
                for (ModelEmojiCustom modelEmojiCustom : values3) {
                    arrayList2.add(modelEmojiCustom.toApiEmoji());
                }
                arrayList = arrayList2;
            } else {
                arrayList = null;
            }
            List list3 = u.toList(values4);
            List list4 = values2 != null ? u.toList(values2) : null;
            if (values != null) {
                list2 = u.toList(values);
            }
            hashMap.put(valueOf, new GuildCache(createApiGuild, arrayList, list3, list4, list2));
        }
        ArrayList arrayList3 = new ArrayList();
        List<Guild> guilds = hydrateUsers.getGuilds();
        m.checkNotNullExpressionValue(guilds, "payload.guilds");
        int i = 0;
        for (Object obj : guilds) {
            i++;
            if (i < 0) {
                n.throwIndexOverflow();
            }
            Guild guild2 = (Guild) obj;
            List<List<Presence>> guildPresences = hydrateUsers.getGuildPresences();
            List<Presence> list5 = guildPresences != null ? guildPresences.get(i) : null;
            List<List<GuildMember>> guildMembers = hydrateUsers.getGuildMembers();
            if (guildMembers == null || (list = guildMembers.get(i)) == null) {
                linkedHashMap = null;
            } else {
                linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(o.collectionSizeOrDefault(list, 10)), 16));
                for (Object obj2 : list) {
                    linkedHashMap.put(Long.valueOf(((GuildMember) obj2).m().i()), obj2);
                }
            }
            ReadyPayloadUtils readyPayloadUtils = INSTANCE;
            m.checkNotNullExpressionValue(guild2, "guild");
            HydrateGuildResult hydrateGuild = readyPayloadUtils.hydrateGuild(guild2, list5, linkedHashMap);
            if (!(hydrateGuild instanceof HydrateGuildResult.Success)) {
                return HydrateResult.Error.INSTANCE;
            }
            arrayList3.add(((HydrateGuildResult.Success) hydrateGuild).getGuild());
        }
        ModelPayload withGuilds = hydrateUsers.withGuilds(arrayList3);
        m.checkNotNullExpressionValue(withGuilds, "payload.withGuilds(guilds)");
        return new HydrateResult.Success(withGuilds);
    }

    @StoreThread
    public final ModelPayload hydrateUsers(ModelPayload modelPayload) {
        ArrayList arrayList;
        ArrayList arrayList2;
        ArrayList arrayList3;
        ArrayList arrayList4;
        m.checkNotNullParameter(modelPayload, "payload");
        List<User> users = modelPayload.getUsers();
        if (users == null) {
            return modelPayload;
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(o.collectionSizeOrDefault(users, 10)), 16));
        for (Object obj : users) {
            linkedHashMap.put(Long.valueOf(((User) obj).i()), obj);
        }
        List<ModelUserRelationship> relationships = modelPayload.getRelationships();
        ArrayList arrayList5 = null;
        if (relationships != null) {
            ArrayList arrayList6 = new ArrayList(o.collectionSizeOrDefault(relationships, 10));
            for (ModelUserRelationship modelUserRelationship : relationships) {
                arrayList6.add(modelUserRelationship.hydrate(linkedHashMap));
            }
            arrayList = arrayList6;
        } else {
            arrayList = null;
        }
        List<Channel> privateChannels = modelPayload.getPrivateChannels();
        if (privateChannels != null) {
            ArrayList arrayList7 = new ArrayList(o.collectionSizeOrDefault(privateChannels, 10));
            for (Channel channel : privateChannels) {
                ReadyPayloadUtils readyPayloadUtils = INSTANCE;
                m.checkNotNullExpressionValue(channel, "channel");
                arrayList7.add(readyPayloadUtils.hydrate(channel, linkedHashMap));
            }
            arrayList2 = arrayList7;
        } else {
            arrayList2 = null;
        }
        List<List<Presence>> guildPresences = modelPayload.getGuildPresences();
        if (guildPresences != null) {
            ArrayList arrayList8 = new ArrayList(o.collectionSizeOrDefault(guildPresences, 10));
            Iterator<T> it = guildPresences.iterator();
            while (it.hasNext()) {
                List<Presence> list = (List) it.next();
                m.checkNotNullExpressionValue(list, "guildPresences");
                ArrayList arrayList9 = new ArrayList(o.collectionSizeOrDefault(list, 10));
                for (Presence presence : list) {
                    ReadyPayloadUtils readyPayloadUtils2 = INSTANCE;
                    m.checkNotNullExpressionValue(presence, "presence");
                    arrayList9.add(readyPayloadUtils2.hydratePresence(presence, linkedHashMap));
                }
                arrayList8.add(arrayList9);
            }
            arrayList3 = arrayList8;
        } else {
            arrayList3 = null;
        }
        List<Presence> presences = modelPayload.getPresences();
        if (presences != null) {
            ArrayList arrayList10 = new ArrayList(o.collectionSizeOrDefault(presences, 10));
            for (Presence presence2 : presences) {
                ReadyPayloadUtils readyPayloadUtils3 = INSTANCE;
                m.checkNotNullExpressionValue(presence2, "presence");
                arrayList10.add(readyPayloadUtils3.hydratePresence(presence2, linkedHashMap));
            }
            arrayList4 = arrayList10;
        } else {
            arrayList4 = null;
        }
        List<List<GuildMember>> guildMembers = modelPayload.getGuildMembers();
        if (guildMembers != null) {
            arrayList5 = new ArrayList(o.collectionSizeOrDefault(guildMembers, 10));
            Iterator<T> it2 = guildMembers.iterator();
            while (it2.hasNext()) {
                List<GuildMember> list2 = (List) it2.next();
                m.checkNotNullExpressionValue(list2, "members");
                ArrayList arrayList11 = new ArrayList(o.collectionSizeOrDefault(list2, 10));
                for (GuildMember guildMember : list2) {
                    ReadyPayloadUtils readyPayloadUtils4 = INSTANCE;
                    m.checkNotNullExpressionValue(guildMember, "member");
                    arrayList11.add(readyPayloadUtils4.hydrateGuildMember(guildMember, linkedHashMap));
                }
                arrayList5.add(arrayList11);
            }
        }
        ModelPayload withHydratedUserData = modelPayload.withHydratedUserData(arrayList, arrayList2, arrayList3, arrayList5, arrayList4);
        m.checkNotNullExpressionValue(withHydratedUserData, "payload.withHydratedUser…    friendPresences\n    )");
        return withHydratedUserData;
    }

    private final Channel hydrate(Channel channel, Map<Long, User> map) {
        List<Long> v = channel.v();
        if (v == null) {
            return channel;
        }
        ArrayList arrayList = new ArrayList();
        for (Number number : v) {
            User user = map.get(Long.valueOf(number.longValue()));
            if (user != null) {
                arrayList.add(user);
            }
        }
        return Channel.a(channel, null, 0, 0L, null, 0L, 0L, 0L, null, arrayList, 0, null, 0, 0, null, 0L, 0L, null, false, 0L, null, 0, null, null, null, null, null, null, null, null, 536870527);
    }

    public final Guild hydrate(Guild guild, Guild guild2, List<GuildEmoji> list, List<Sticker> list2, List<Channel> list3, List<GuildRole> list4, List<Presence> list5, Map<Long, GuildMember> map, Function2<? super Channel, ? super Channel, Channel> function2) {
        Guild guild3 = guild;
        m.checkNotNullParameter(guild3, "unhydratedGuild");
        m.checkNotNullParameter(function2, "onHydrateChannel");
        if (guild2 != null) {
            Long b2 = guild2.b();
            int c = guild2.c();
            String e = guild2.e();
            Integer h = guild2.h();
            String i = guild2.i();
            GuildExplicitContentFilter l = guild2.l();
            List<GuildFeature> m = guild2.m();
            String q = guild2.q();
            GuildMaxVideoChannelUsers t = guild2.t();
            int w = guild2.w();
            String x2 = guild2.x();
            long z2 = guild2.z();
            String A = guild2.A();
            int C = guild2.C();
            Long E = guild2.E();
            String F = guild2.F();
            Long H = guild2.H();
            guild3 = Guild.a(guild, null, null, null, x2, i, h, 0L, F, z2, q, guild2.Q(), l, null, null, null, null, false, w, c, b2, guild2.M(), m, 0, e, guild2.I(), C, 0, guild2.L(), null, H, E, A, null, t, guild2.P(), 0, 0, null, null, null, null, false, null, null, null, null, 339865671, 16377);
        }
        if (list != null) {
            guild3 = Guild.a(guild3, null, list, null, null, null, null, 0L, null, 0L, null, null, null, null, null, null, null, false, 0, 0, null, null, null, 0, null, null, 0, 0, 0, null, null, null, null, null, null, null, 0, 0, null, null, null, null, false, null, null, null, null, -3, 16383);
        }
        Guild guild4 = guild3;
        if (list2 != null) {
            guild4 = Guild.a(guild4, null, null, list2, null, null, null, 0L, null, 0L, null, null, null, null, null, null, null, false, 0, 0, null, null, null, 0, null, null, 0, 0, 0, null, null, null, null, null, null, null, 0, 0, null, null, null, null, false, null, null, null, null, -5, 16383);
        }
        Guild guild5 = guild4;
        if (list3 != null) {
            HashMap hashMap = new HashMap();
            if (guild5.f() != null) {
                List<Channel> f = guild5.f();
                m.checkNotNull(f);
                for (Channel channel : f) {
                    hashMap.put(Long.valueOf(channel.h()), channel);
                }
                guild5 = Guild.a(guild5, null, null, null, null, null, null, 0L, null, 0L, null, null, null, null, null, null, null, false, 0, 0, null, null, null, 0, null, null, 0, 0, 0, null, null, null, null, null, null, null, 0, 0, null, null, null, null, false, null, null, null, null, -1, 16255);
            }
            Guild guild6 = guild5;
            ArrayList arrayList = new ArrayList();
            for (Channel channel2 : list3) {
                Channel channel3 = (Channel) hashMap.get(Long.valueOf(channel2.h()));
                if (channel3 != null) {
                    arrayList.add(function2.invoke(channel2, channel3));
                } else {
                    arrayList.add(channel2);
                }
            }
            guild5 = Guild.a(guild6, null, null, null, null, null, null, 0L, null, 0L, null, null, null, null, arrayList, null, null, false, 0, 0, null, null, null, 0, null, null, 0, 0, 0, null, null, null, null, null, null, null, 0, 0, null, null, null, null, false, null, null, null, null, -8193, 16383);
        }
        Guild guild7 = guild5;
        if (list4 != null) {
            guild7 = Guild.a(guild7, list4, null, null, null, null, null, 0L, null, 0L, null, null, null, null, null, null, null, false, 0, 0, null, null, null, 0, null, null, 0, 0, 0, null, null, null, null, null, null, null, 0, 0, null, null, null, null, false, null, null, null, null, -2, 16383);
        }
        Guild guild8 = guild7;
        if (list5 != null) {
            guild8 = Guild.a(guild8, null, null, null, null, null, null, 0L, null, 0L, null, null, null, list5, null, null, null, false, 0, 0, null, null, null, 0, null, null, 0, 0, 0, null, null, null, null, null, null, null, 0, 0, null, null, null, null, false, null, null, null, null, -4097, 16383);
        }
        Guild guild9 = guild8;
        return map != null ? Guild.a(guild9, null, null, null, null, null, null, 0L, null, 0L, null, null, null, null, null, u.toList(map.values()), null, false, 0, 0, null, null, null, 0, null, null, 0, 0, 0, null, null, null, null, null, null, null, 0, 0, null, null, null, null, false, null, null, null, null, -16385, 16383) : guild9;
    }
}
