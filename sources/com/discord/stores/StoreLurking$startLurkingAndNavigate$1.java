package com.discord.stores;

import android.content.Context;
import androidx.fragment.app.FragmentActivity;
import b.d.b.a.a;
import com.discord.app.AppLog;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreLurking;
import com.discord.stores.StoreNavigation;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function3;
/* compiled from: StoreLurking.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreLurking$startLurkingAndNavigate$1 extends o implements Function0<Unit> {
    public final /* synthetic */ Long $channelId;
    public final /* synthetic */ Context $context;
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ StoreLurking this$0;

    /* compiled from: StoreLurking.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\n\u001a\u00020\u00072\u0006\u0010\u0001\u001a\u00020\u00002\u000e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\n¢\u0006\u0004\b\b\u0010\t"}, d2 = {"Lcom/discord/models/guild/Guild;", "guild", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "alreadyJoined", "", "invoke", "(Lcom/discord/models/guild/Guild;Ljava/lang/Long;Z)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreLurking$startLurkingAndNavigate$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function3<Guild, Long, Boolean, Unit> {

        /* compiled from: StoreLurking.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroidx/fragment/app/FragmentActivity;", "it", "", "invoke", "(Landroidx/fragment/app/FragmentActivity;)Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreLurking$startLurkingAndNavigate$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C02081 extends o implements Function1<FragmentActivity, Boolean> {
            public static final C02081 INSTANCE = new C02081();

            public C02081() {
                super(1);
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Boolean invoke(FragmentActivity fragmentActivity) {
                return Boolean.valueOf(invoke2(fragmentActivity));
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final boolean invoke2(FragmentActivity fragmentActivity) {
                m.checkNotNullParameter(fragmentActivity, "it");
                StoreNavigation.setNavigationPanelAction$default(StoreStream.Companion.getNavigation(), StoreNavigation.PanelAction.OPEN, null, 2, null);
                return true;
            }
        }

        public AnonymousClass1() {
            super(3);
        }

        @Override // kotlin.jvm.functions.Function3
        public /* bridge */ /* synthetic */ Unit invoke(Guild guild, Long l, Boolean bool) {
            invoke(guild, l, bool.booleanValue());
            return Unit.a;
        }

        public final void invoke(Guild guild, Long l, boolean z2) {
            StoreStream storeStream;
            m.checkNotNullParameter(guild, "guild");
            StoreLurking$startLurkingAndNavigate$1$1$jumpToDestination$1 storeLurking$startLurkingAndNavigate$1$1$jumpToDestination$1 = new StoreLurking$startLurkingAndNavigate$1$1$jumpToDestination$1(this, l);
            if (z2) {
                storeLurking$startLurkingAndNavigate$1$1$jumpToDestination$1.invoke();
            } else if (!z2) {
                storeLurking$startLurkingAndNavigate$1$1$jumpToDestination$1.invoke();
                storeStream = StoreLurking$startLurkingAndNavigate$1.this.this$0.stream;
                storeStream.handleGuildJoined(StoreLurking$startLurkingAndNavigate$1.this.$guildId, guild.getWelcomeScreen());
                StoreNavigation navigation = StoreStream.Companion.getNavigation();
                StringBuilder R = a.R("LURK:");
                R.append(StoreLurking$startLurkingAndNavigate$1.this.$guildId);
                navigation.launchNotice(R.toString(), C02081.INSTANCE);
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreLurking$startLurkingAndNavigate$1(StoreLurking storeLurking, long j, Long l, Context context) {
        super(0);
        this.this$0 = storeLurking;
        this.$guildId = j;
        this.$channelId = l;
        this.$context = context;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        String str;
        str = this.this$0.sessionId;
        if (str != null) {
            this.this$0.startLurkingInternal(this.$guildId, this.$channelId, (r17 & 4) != 0 ? false : false, (r17 & 8) != 0 ? StoreLurking$startLurkingInternal$1.INSTANCE : new AnonymousClass1(), (r17 & 16) != 0 ? StoreLurking$startLurkingInternal$2.INSTANCE : null, (r17 & 32) != 0 ? null : this.$context);
            return;
        }
        StringBuilder R = a.R("Queue lurk request: ");
        R.append(this.$guildId);
        R.append(MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR);
        R.append(this.$channelId);
        AppLog.i(R.toString());
        this.this$0.lurkRequest = new StoreLurking.LurkRequest(this.$guildId, this.$channelId);
    }
}
