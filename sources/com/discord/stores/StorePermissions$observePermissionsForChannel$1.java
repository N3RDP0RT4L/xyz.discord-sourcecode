package com.discord.stores;

import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StorePermissions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\n\u0018\u00010\u0000j\u0004\u0018\u0001`\u0001H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"", "Lcom/discord/api/permission/PermissionBit;", "invoke", "()Ljava/lang/Long;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StorePermissions$observePermissionsForChannel$1 extends o implements Function0<Long> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ StorePermissions this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StorePermissions$observePermissionsForChannel$1(StorePermissions storePermissions, long j) {
        super(0);
        this.this$0 = storePermissions;
        this.$channelId = j;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final Long invoke() {
        Map map;
        map = this.this$0.permissionsForChannelsFlattenedSnapshot;
        return (Long) map.get(Long.valueOf(this.$channelId));
    }
}
