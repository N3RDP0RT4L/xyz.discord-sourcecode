package com.discord.stores;

import com.discord.models.message.Message;
import d0.t.n;
import d0.z.d.o;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StorePinnedMessages.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"", "Lcom/discord/models/message/Message;", "invoke", "()Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StorePinnedMessages$observeForChannel$1 extends o implements Function0<List<? extends Message>> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ StorePinnedMessages this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StorePinnedMessages$observeForChannel$1(StorePinnedMessages storePinnedMessages, long j) {
        super(0);
        this.this$0 = storePinnedMessages;
        this.$channelId = j;
    }

    @Override // kotlin.jvm.functions.Function0
    public final List<? extends Message> invoke() {
        Map map;
        map = this.this$0.pinnedMessages;
        List<? extends Message> list = (List) map.get(Long.valueOf(this.$channelId));
        return list != null ? list : n.emptyList();
    }
}
