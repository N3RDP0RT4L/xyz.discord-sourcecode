package com.discord.stores;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.Context;
import b.d.b.a.a;
import com.adjust.sdk.Constants;
import com.discord.models.domain.Model;
import com.discord.models.domain.emoji.Emoji;
import com.discord.models.domain.emoji.EmojiCategory;
import com.discord.models.domain.emoji.EmojiSet;
import com.discord.models.domain.emoji.ModelEmojiCustom;
import com.discord.models.domain.emoji.ModelEmojiUnicode;
import com.discord.stores.StoreEmoji;
import com.discord.stores.StoreMediaFavorites;
import com.discord.utilities.Quad;
import com.discord.utilities.collections.ShallowPartitionMap;
import com.discord.utilities.frecency.FrecencyTracker;
import com.discord.utilities.media.MediaFrecencyTracker;
import com.discord.utilities.persister.Persister;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.textprocessing.Rules;
import d0.f0.q;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import j0.l.e.k;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreEmoji.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000È\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 ^2\u00020\u0001:\u0002^_B/\u0012\u0006\u0010Q\u001a\u00020P\u0012\u0006\u0010G\u001a\u00020F\u0012\u0006\u0010M\u001a\u00020L\u0012\u0006\u0010T\u001a\u00020S\u0012\u0006\u0010Z\u001a\u00020Y¢\u0006\u0004\b\\\u0010]J{\u0010\u0014\u001a\u00020\u00132\"\u0010\u0006\u001a\u001e\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00050\u00020\u00022\u0006\u0010\b\u001a\u00020\u00072\u0010\u0010\n\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\t2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\u000b2\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J)\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00170\t2\u0012\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00170\u0002H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u0017\u0010\u001e\u001a\u00020\u001d2\u0006\u0010\u001c\u001a\u00020\u001bH\u0002¢\u0006\u0004\b\u001e\u0010\u001fJ\u0017\u0010\"\u001a\u00020!2\u0006\u0010 \u001a\u00020\u001dH\u0002¢\u0006\u0004\b\"\u0010#J\u000f\u0010%\u001a\u00020$H\u0002¢\u0006\u0004\b%\u0010&J\u001b\u0010(\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020'0\u0002H\u0016¢\u0006\u0004\b(\u0010)J\u001b\u0010*\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020'0\u0002H\u0016¢\u0006\u0004\b*\u0010)J\u000f\u0010+\u001a\u00020$H\u0016¢\u0006\u0004\b+\u0010&J\u0015\u0010,\u001a\u00020!2\u0006\u0010\u001c\u001a\u00020\u001b¢\u0006\u0004\b,\u0010-J?\u00102\u001a\b\u0012\u0004\u0012\u00020\u0013012\n\u0010.\u001a\u00060\u0003j\u0002`\u00042\n\u00100\u001a\u00060\u0003j\u0002`/2\b\b\u0002\u0010\r\u001a\u00020\u000b2\b\b\u0002\u0010\u000e\u001a\u00020\u000b¢\u0006\u0004\b2\u00103J+\u00102\u001a\b\u0012\u0004\u0012\u00020\u0013012\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\r\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\u000b¢\u0006\u0004\b2\u00104J\u001d\u00107\u001a\u0004\u0018\u00010\u00052\n\u00106\u001a\u00060\u0003j\u0002`5H\u0007¢\u0006\u0004\b7\u00108J\u000f\u00109\u001a\u00020!H\u0007¢\u0006\u0004\b9\u0010:J\u0015\u0010<\u001a\u00020!2\u0006\u0010;\u001a\u00020\u0017¢\u0006\u0004\b<\u0010=J\u0015\u0010<\u001a\u00020!2\u0006\u0010>\u001a\u00020\u0016¢\u0006\u0004\b<\u0010?R(\u0010A\u001a\u0014\u0012\u0004\u0012\u00020@\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00170\t0\u00028\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\bA\u0010BR\u0016\u0010D\u001a\u00020C8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bD\u0010ER\u0016\u0010G\u001a\u00020F8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bG\u0010HR\u001c\u0010J\u001a\b\u0012\u0004\u0012\u00020C0I8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bJ\u0010KR\u0016\u0010M\u001a\u00020L8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bM\u0010NR\"\u0010O\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020'0\u00028\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\bO\u0010BR\u0016\u0010Q\u001a\u00020P8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bQ\u0010RR\u0016\u0010T\u001a\u00020S8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bT\u0010UR\u0016\u0010V\u001a\u00020$8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\bV\u0010WR\"\u0010X\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020'0\u00028\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\bX\u0010BR\u0016\u0010Z\u001a\u00020Y8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bZ\u0010[¨\u0006`"}, d2 = {"Lcom/discord/stores/StoreEmoji;", "Lcom/discord/utilities/textprocessing/Rules$EmojiDataProvider;", "", "", "Lcom/discord/primitives/GuildId;", "Lcom/discord/models/domain/emoji/ModelEmojiCustom;", "allCustomEmojis", "Lcom/discord/stores/StoreEmoji$EmojiContext;", "emojiContext", "", "sortedGuildIds", "", "isMePremium", "includeUnusableEmojis", "includeUnavailableEmojis", "hasExternalEmojiPermission", "", "Lcom/discord/stores/StoreMediaFavorites$Favorite;", "favorites", "Lcom/discord/models/domain/emoji/EmojiSet;", "buildUsableEmojiSet", "(Ljava/util/Map;Lcom/discord/stores/StoreEmoji$EmojiContext;Ljava/util/List;ZZZZLjava/util/Set;)Lcom/discord/models/domain/emoji/EmojiSet;", "", "Lcom/discord/models/domain/emoji/Emoji;", "emojiIdsMap", "getFrequentlyUsedEmojis", "(Ljava/util/Map;)Ljava/util/List;", "Landroid/content/Context;", "context", "Lcom/discord/models/domain/emoji/ModelEmojiUnicode$Bundle;", "loadUnicodeEmojisFromDisk", "(Landroid/content/Context;)Lcom/discord/models/domain/emoji/ModelEmojiUnicode$Bundle;", "unicodeEmojisBundle", "", "handleLoadedUnicodeEmojis", "(Lcom/discord/models/domain/emoji/ModelEmojiUnicode$Bundle;)V", "Ljava/util/regex/Pattern;", "compileSurrogatesPattern", "()Ljava/util/regex/Pattern;", "Lcom/discord/models/domain/emoji/ModelEmojiUnicode;", "getUnicodeEmojiSurrogateMap", "()Ljava/util/Map;", "getUnicodeEmojisNamesMap", "getUnicodeEmojisPattern", "initBlocking", "(Landroid/content/Context;)V", "guildId", "Lcom/discord/primitives/ChannelId;", "channelId", "Lrx/Observable;", "getEmojiSet", "(JJZZ)Lrx/Observable;", "(Lcom/discord/stores/StoreEmoji$EmojiContext;ZZ)Lrx/Observable;", "Lcom/discord/primitives/EmojiId;", "emojiId", "getCustomEmojiInternal", "(J)Lcom/discord/models/domain/emoji/ModelEmojiCustom;", "handlePreLogout", "()V", "emoji", "onEmojiUsed", "(Lcom/discord/models/domain/emoji/Emoji;)V", "emojiKey", "(Ljava/lang/String;)V", "Lcom/discord/models/domain/emoji/EmojiCategory;", "unicodeEmojis", "Ljava/util/Map;", "Lcom/discord/utilities/media/MediaFrecencyTracker;", "frecency", "Lcom/discord/utilities/media/MediaFrecencyTracker;", "Lcom/discord/stores/StoreUser;", "userStore", "Lcom/discord/stores/StoreUser;", "Lcom/discord/utilities/persister/Persister;", "frecencyCache", "Lcom/discord/utilities/persister/Persister;", "Lcom/discord/stores/StorePermissions;", "permissionsStore", "Lcom/discord/stores/StorePermissions;", "unicodeEmojisNamesMap", "Lcom/discord/stores/StoreEmojiCustom;", "customEmojiStore", "Lcom/discord/stores/StoreEmojiCustom;", "Lcom/discord/stores/StoreGuildsSorted;", "sortedGuildsStore", "Lcom/discord/stores/StoreGuildsSorted;", "unicodeEmojisPattern", "Ljava/util/regex/Pattern;", "unicodeEmojiSurrogateMap", "Lcom/discord/stores/StoreMediaFavorites;", "mediaFavoritesStore", "Lcom/discord/stores/StoreMediaFavorites;", HookHelper.constructorName, "(Lcom/discord/stores/StoreEmojiCustom;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreGuildsSorted;Lcom/discord/stores/StoreMediaFavorites;)V", "Companion", "EmojiContext", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreEmoji implements Rules.EmojiDataProvider {
    public static final Companion Companion = new Companion(null);
    private static final String[] DEFAULT_FREQUENT_EMOJIS = {"bread", "fork_and_knife", "yum", "weary", "tired_face", "poop", "thumbsup", "100"};
    private static final int MAX_FREQUENTLY_USED_EMOJIS = 40;
    private final StoreEmojiCustom customEmojiStore;
    private final MediaFrecencyTracker frecency;
    private final Persister<MediaFrecencyTracker> frecencyCache;
    private final StoreMediaFavorites mediaFavoritesStore;
    private final StorePermissions permissionsStore;
    private final StoreGuildsSorted sortedGuildsStore;
    private Map<String, ? extends ModelEmojiUnicode> unicodeEmojiSurrogateMap;
    private Map<EmojiCategory, ? extends List<? extends Emoji>> unicodeEmojis;
    private Map<String, ? extends ModelEmojiUnicode> unicodeEmojisNamesMap;
    private Pattern unicodeEmojisPattern;
    private final StoreUser userStore;

    /* compiled from: StoreEmoji.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fR\u001f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007R\u0016\u0010\t\u001a\u00020\b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\t\u0010\n¨\u0006\r"}, d2 = {"Lcom/discord/stores/StoreEmoji$Companion;", "", "", "", "DEFAULT_FREQUENT_EMOJIS", "[Ljava/lang/String;", "getDEFAULT_FREQUENT_EMOJIS", "()[Ljava/lang/String;", "", "MAX_FREQUENTLY_USED_EMOJIS", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public final String[] getDEFAULT_FREQUENT_EMOJIS() {
            return StoreEmoji.DEFAULT_FREQUENT_EMOJIS;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreEmoji.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\u0004\u0005\u0006\u0007B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0004\b\t\n\u000b¨\u0006\f"}, d2 = {"Lcom/discord/stores/StoreEmoji$EmojiContext;", "", HookHelper.constructorName, "()V", "Chat", "Global", "Guild", "GuildProfile", "Lcom/discord/stores/StoreEmoji$EmojiContext$Chat;", "Lcom/discord/stores/StoreEmoji$EmojiContext$Global;", "Lcom/discord/stores/StoreEmoji$EmojiContext$GuildProfile;", "Lcom/discord/stores/StoreEmoji$EmojiContext$Guild;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class EmojiContext {

        /* compiled from: StoreEmoji.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\n\u0010\b\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010\t\u001a\u00060\u0002j\u0002`\u0006¢\u0006\u0004\b\u001a\u0010\u001bJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\u0007\u001a\u00060\u0002j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0005J,\u0010\n\u001a\u00020\u00002\f\b\u0002\u0010\b\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010\t\u001a\u00060\u0002j\u0002`\u0006HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u001d\u0010\t\u001a\u00060\u0002j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0005R\u001d\u0010\b\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0017\u001a\u0004\b\u0019\u0010\u0005¨\u0006\u001c"}, d2 = {"Lcom/discord/stores/StoreEmoji$EmojiContext$Chat;", "Lcom/discord/stores/StoreEmoji$EmojiContext;", "", "Lcom/discord/primitives/GuildId;", "component1", "()J", "Lcom/discord/primitives/ChannelId;", "component2", "guildId", "channelId", "copy", "(JJ)Lcom/discord/stores/StoreEmoji$EmojiContext$Chat;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getChannelId", "getGuildId", HookHelper.constructorName, "(JJ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Chat extends EmojiContext {
            private final long channelId;
            private final long guildId;

            public Chat(long j, long j2) {
                super(null);
                this.guildId = j;
                this.channelId = j2;
            }

            public static /* synthetic */ Chat copy$default(Chat chat, long j, long j2, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = chat.guildId;
                }
                if ((i & 2) != 0) {
                    j2 = chat.channelId;
                }
                return chat.copy(j, j2);
            }

            public final long component1() {
                return this.guildId;
            }

            public final long component2() {
                return this.channelId;
            }

            public final Chat copy(long j, long j2) {
                return new Chat(j, j2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Chat)) {
                    return false;
                }
                Chat chat = (Chat) obj;
                return this.guildId == chat.guildId && this.channelId == chat.channelId;
            }

            public final long getChannelId() {
                return this.channelId;
            }

            public final long getGuildId() {
                return this.guildId;
            }

            public int hashCode() {
                return b.a(this.channelId) + (b.a(this.guildId) * 31);
            }

            public String toString() {
                StringBuilder R = a.R("Chat(guildId=");
                R.append(this.guildId);
                R.append(", channelId=");
                return a.B(R, this.channelId, ")");
            }
        }

        /* compiled from: StoreEmoji.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreEmoji$EmojiContext$Global;", "Lcom/discord/stores/StoreEmoji$EmojiContext;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Global extends EmojiContext {
            public static final Global INSTANCE = new Global();

            private Global() {
                super(null);
            }
        }

        /* compiled from: StoreEmoji.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u001e\u0010\u0007\u001a\u00020\u00002\f\b\u0002\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001d\u0010\u0006\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/stores/StoreEmoji$EmojiContext$Guild;", "Lcom/discord/stores/StoreEmoji$EmojiContext;", "", "Lcom/discord/primitives/GuildId;", "component1", "()J", "guildId", "copy", "(J)Lcom/discord/stores/StoreEmoji$EmojiContext$Guild;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getGuildId", HookHelper.constructorName, "(J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Guild extends EmojiContext {
            private final long guildId;

            public Guild(long j) {
                super(null);
                this.guildId = j;
            }

            public static /* synthetic */ Guild copy$default(Guild guild, long j, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = guild.guildId;
                }
                return guild.copy(j);
            }

            public final long component1() {
                return this.guildId;
            }

            public final Guild copy(long j) {
                return new Guild(j);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Guild) && this.guildId == ((Guild) obj).guildId;
                }
                return true;
            }

            public final long getGuildId() {
                return this.guildId;
            }

            public int hashCode() {
                return b.a(this.guildId);
            }

            public String toString() {
                return a.B(a.R("Guild(guildId="), this.guildId, ")");
            }
        }

        /* compiled from: StoreEmoji.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u001e\u0010\u0007\u001a\u00020\u00002\f\b\u0002\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001d\u0010\u0006\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/stores/StoreEmoji$EmojiContext$GuildProfile;", "Lcom/discord/stores/StoreEmoji$EmojiContext;", "", "Lcom/discord/primitives/GuildId;", "component1", "()J", "guildId", "copy", "(J)Lcom/discord/stores/StoreEmoji$EmojiContext$GuildProfile;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getGuildId", HookHelper.constructorName, "(J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class GuildProfile extends EmojiContext {
            private final long guildId;

            public GuildProfile(long j) {
                super(null);
                this.guildId = j;
            }

            public static /* synthetic */ GuildProfile copy$default(GuildProfile guildProfile, long j, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = guildProfile.guildId;
                }
                return guildProfile.copy(j);
            }

            public final long component1() {
                return this.guildId;
            }

            public final GuildProfile copy(long j) {
                return new GuildProfile(j);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof GuildProfile) && this.guildId == ((GuildProfile) obj).guildId;
                }
                return true;
            }

            public final long getGuildId() {
                return this.guildId;
            }

            public int hashCode() {
                return b.a(this.guildId);
            }

            public String toString() {
                return a.B(a.R("GuildProfile(guildId="), this.guildId, ")");
            }
        }

        private EmojiContext() {
        }

        public /* synthetic */ EmojiContext(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public StoreEmoji(StoreEmojiCustom storeEmojiCustom, StoreUser storeUser, StorePermissions storePermissions, StoreGuildsSorted storeGuildsSorted, StoreMediaFavorites storeMediaFavorites) {
        m.checkNotNullParameter(storeEmojiCustom, "customEmojiStore");
        m.checkNotNullParameter(storeUser, "userStore");
        m.checkNotNullParameter(storePermissions, "permissionsStore");
        m.checkNotNullParameter(storeGuildsSorted, "sortedGuildsStore");
        m.checkNotNullParameter(storeMediaFavorites, "mediaFavoritesStore");
        this.customEmojiStore = storeEmojiCustom;
        this.userStore = storeUser;
        this.permissionsStore = storePermissions;
        this.sortedGuildsStore = storeGuildsSorted;
        this.mediaFavoritesStore = storeMediaFavorites;
        Persister<MediaFrecencyTracker> persister = new Persister<>("EMOJI_HISTORY_V4", new MediaFrecencyTracker(0, 0, 3, null));
        this.frecencyCache = persister;
        this.frecency = persister.get();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final EmojiSet buildUsableEmojiSet(Map<Long, ? extends Map<Long, ? extends ModelEmojiCustom>> map, EmojiContext emojiContext, List<Long> list, boolean z2, boolean z3, boolean z4, boolean z5, Set<? extends StoreMediaFavorites.Favorite> set) {
        StoreEmoji$buildUsableEmojiSet$1 storeEmoji$buildUsableEmojiSet$1 = new StoreEmoji$buildUsableEmojiSet$1(emojiContext);
        StoreEmoji$buildUsableEmojiSet$2 storeEmoji$buildUsableEmojiSet$2 = new StoreEmoji$buildUsableEmojiSet$2(z5, emojiContext);
        Set<Long> keySet = map.keySet();
        ArrayList<Number> arrayList = new ArrayList();
        for (Object obj : keySet) {
            if (!list.contains(Long.valueOf(((Number) obj).longValue()))) {
                arrayList.add(obj);
            }
        }
        int size = list.size() + arrayList.size();
        int i = 0;
        for (List<Number> list2 : n.listOf((Object[]) new List[]{list, arrayList})) {
            int i2 = 0;
            for (Number number : list2) {
                long longValue = number.longValue();
                Map<Long, ? extends ModelEmojiCustom> map2 = map.get(Long.valueOf(longValue));
                i2 += (map2 == null || storeEmoji$buildUsableEmojiSet$2.invoke(storeEmoji$buildUsableEmojiSet$1.invoke(longValue))) ? 0 : map2.size();
            }
            i += i2;
        }
        Map<String, ? extends ModelEmojiUnicode> map3 = this.unicodeEmojisNamesMap;
        if (map3 == null) {
            m.throwUninitializedPropertyAccessException("unicodeEmojisNamesMap");
        }
        int size2 = map3.size();
        Map<EmojiCategory, ? extends List<? extends Emoji>> map4 = this.unicodeEmojis;
        if (map4 == null) {
            m.throwUninitializedPropertyAccessException("unicodeEmojis");
        }
        Iterator<T> it = map4.values().iterator();
        int i3 = 0;
        while (it.hasNext()) {
            i3 += ((List) it.next()).size();
        }
        ShallowPartitionMap.Companion companion = ShallowPartitionMap.Companion;
        ShallowPartitionMap create$default = ShallowPartitionMap.Companion.create$default(companion, i + size2, 0, 0, null, 14, null);
        HashMap hashMap = new HashMap(size);
        ShallowPartitionMap create$default2 = ShallowPartitionMap.Companion.create$default(companion, i + i3, 0, 0, null, 14, null);
        if (!(emojiContext instanceof EmojiContext.GuildProfile)) {
            Map<EmojiCategory, ? extends List<? extends Emoji>> map5 = this.unicodeEmojis;
            if (map5 == null) {
                m.throwUninitializedPropertyAccessException("unicodeEmojis");
            }
            Iterator<T> it2 = map5.values().iterator();
            while (it2.hasNext()) {
                for (Emoji emoji : (List) it2.next()) {
                    String uniqueId = emoji.getUniqueId();
                    m.checkNotNullExpressionValue(uniqueId, "emoji.uniqueId");
                    create$default2.put(uniqueId, emoji);
                }
            }
        }
        StoreEmoji$buildUsableEmojiSet$4 storeEmoji$buildUsableEmojiSet$4 = StoreEmoji$buildUsableEmojiSet$4.INSTANCE;
        ShallowPartitionMap shallowPartitionMap = create$default2;
        StoreEmoji$buildUsableEmojiSet$5 storeEmoji$buildUsableEmojiSet$5 = new StoreEmoji$buildUsableEmojiSet$5(map, storeEmoji$buildUsableEmojiSet$1, storeEmoji$buildUsableEmojiSet$2, z4, emojiContext, z2, z3, create$default, shallowPartitionMap, hashMap);
        Map<String, ? extends ModelEmojiUnicode> map6 = this.unicodeEmojisNamesMap;
        if (map6 == null) {
            m.throwUninitializedPropertyAccessException("unicodeEmojisNamesMap");
        }
        for (String str : map6.keySet()) {
            StoreEmoji$buildUsableEmojiSet$4.INSTANCE.invoke((Map<ShallowPartitionMap, Integer>) create$default, (ShallowPartitionMap) str);
        }
        long guildId = emojiContext instanceof EmojiContext.Chat ? ((EmojiContext.Chat) emojiContext).getGuildId() : 0L;
        storeEmoji$buildUsableEmojiSet$5.invoke(guildId);
        ArrayList<Number> arrayList2 = new ArrayList();
        for (Object obj2 : list) {
            if (((Number) obj2).longValue() != guildId) {
                arrayList2.add(obj2);
            }
        }
        for (Number number2 : arrayList2) {
            storeEmoji$buildUsableEmojiSet$5.invoke(number2.longValue());
        }
        for (Number number3 : arrayList) {
            storeEmoji$buildUsableEmojiSet$5.invoke(number3.longValue());
        }
        ArrayList<StoreMediaFavorites.FavoriteEmoji> arrayList3 = new ArrayList();
        for (Object obj3 : set) {
            if (obj3 instanceof StoreMediaFavorites.FavoriteEmoji) {
                arrayList3.add(obj3);
            }
        }
        ArrayList arrayList4 = new ArrayList();
        for (StoreMediaFavorites.FavoriteEmoji favoriteEmoji : arrayList3) {
            ShallowPartitionMap shallowPartitionMap2 = shallowPartitionMap;
            Emoji emoji2 = (Emoji) shallowPartitionMap2.get(favoriteEmoji.getEmojiUniqueId());
            if (emoji2 != null) {
                arrayList4.add(emoji2);
            }
            shallowPartitionMap = shallowPartitionMap2;
        }
        ShallowPartitionMap shallowPartitionMap3 = shallowPartitionMap;
        Set set2 = u.toSet(arrayList4);
        Map<EmojiCategory, ? extends List<? extends Emoji>> map7 = this.unicodeEmojis;
        if (map7 == null) {
            m.throwUninitializedPropertyAccessException("unicodeEmojis");
        }
        return new EmojiSet(map7, hashMap, shallowPartitionMap3, getFrequentlyUsedEmojis(shallowPartitionMap3), set2);
    }

    private final Pattern compileSurrogatesPattern() {
        Map<String, ? extends ModelEmojiUnicode> map = this.unicodeEmojiSurrogateMap;
        if (map == null) {
            m.throwUninitializedPropertyAccessException("unicodeEmojiSurrogateMap");
        }
        Pattern compile = Pattern.compile(u.joinToString$default(u.sortedWith(map.keySet(), new Comparator() { // from class: com.discord.stores.StoreEmoji$compileSurrogatesPattern$$inlined$sortedBy$1
            @Override // java.util.Comparator
            public final int compare(T t, T t2) {
                return d0.u.a.compareValues(Integer.valueOf(-((String) t).length()), Integer.valueOf(-((String) t2).length()));
            }
        }), "|", null, null, 0, null, StoreEmoji$compileSurrogatesPattern$emojiSurrogatesPattern$2.INSTANCE, 30, null));
        m.checkNotNullExpressionValue(compile, "Pattern.compile(emojiSurrogatesPattern)");
        return compile;
    }

    private final List<Emoji> getFrequentlyUsedEmojis(Map<String, ? extends Emoji> map) {
        Collection<String> sortedKeys$default = FrecencyTracker.getSortedKeys$default(this.frecency, 0L, 1, null);
        ArrayList arrayList = new ArrayList();
        for (String str : sortedKeys$default) {
            Emoji emoji = map.get(str);
            if (emoji != null) {
                arrayList.add(emoji);
            }
        }
        List<Emoji> take = u.take(arrayList, 40);
        if (take.size() >= 40) {
            return take;
        }
        String[] strArr = DEFAULT_FREQUENT_EMOJIS;
        ArrayList arrayList2 = new ArrayList();
        for (String str2 : strArr) {
            Map<String, ? extends ModelEmojiUnicode> map2 = this.unicodeEmojisNamesMap;
            if (map2 == null) {
                m.throwUninitializedPropertyAccessException("unicodeEmojisNamesMap");
            }
            ModelEmojiUnicode modelEmojiUnicode = map2.get(str2);
            if (modelEmojiUnicode != null) {
                arrayList2.add(modelEmojiUnicode);
            }
        }
        return u.distinct(q.toList(d0.f0.n.flattenSequenceOfIterable(d0.f0.n.sequenceOf(take, u.take(arrayList2, 40 - take.size())))));
    }

    private final void handleLoadedUnicodeEmojis(ModelEmojiUnicode.Bundle bundle) {
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        HashMap hashMap3 = new HashMap();
        StoreEmoji$handleLoadedUnicodeEmojis$1 storeEmoji$handleLoadedUnicodeEmojis$1 = new StoreEmoji$handleLoadedUnicodeEmojis$1(hashMap3, hashMap2);
        Map<EmojiCategory, List<ModelEmojiUnicode>> emojis = bundle.getEmojis();
        m.checkNotNullExpressionValue(emojis, "unicodeEmojisBundle.emojis");
        for (Map.Entry<EmojiCategory, List<ModelEmojiUnicode>> entry : emojis.entrySet()) {
            EmojiCategory key = entry.getKey();
            List<ModelEmojiUnicode> value = entry.getValue();
            m.checkNotNullExpressionValue(key, "category");
            m.checkNotNullExpressionValue(value, "categoryEmojis");
            hashMap.put(key, value);
            for (ModelEmojiUnicode modelEmojiUnicode : value) {
                m.checkNotNullExpressionValue(modelEmojiUnicode, "unicodeEmoji");
                storeEmoji$handleLoadedUnicodeEmojis$1.invoke2(modelEmojiUnicode);
                List<ModelEmojiUnicode> asDiverse = modelEmojiUnicode.getAsDiverse();
                m.checkNotNullExpressionValue(asDiverse, "unicodeEmoji\n                .asDiverse");
                for (ModelEmojiUnicode modelEmojiUnicode2 : asDiverse) {
                    m.checkNotNullExpressionValue(modelEmojiUnicode2, "diverseEmoji");
                    storeEmoji$handleLoadedUnicodeEmojis$1.invoke2(modelEmojiUnicode2);
                }
            }
        }
        this.unicodeEmojis = hashMap;
        this.unicodeEmojisNamesMap = hashMap2;
        this.unicodeEmojiSurrogateMap = hashMap3;
        this.unicodeEmojisPattern = compileSurrogatesPattern();
    }

    private final ModelEmojiUnicode.Bundle loadUnicodeEmojisFromDisk(Context context) {
        Model parse = new Model.JsonReader(new InputStreamReader(context.getAssets().open("data/emojis.json"), Constants.ENCODING)).parse(new ModelEmojiUnicode.Bundle());
        m.checkNotNullExpressionValue(parse, "jsonReader.parse(ModelEmojiUnicode.Bundle())");
        return (ModelEmojiUnicode.Bundle) parse;
    }

    @StoreThread
    public final ModelEmojiCustom getCustomEmojiInternal(long j) {
        for (Map.Entry<Long, Map<Long, ModelEmojiCustom>> entry : this.customEmojiStore.getAllGuildEmojiInternal().entrySet()) {
            ModelEmojiCustom modelEmojiCustom = entry.getValue().get(Long.valueOf(j));
            if (modelEmojiCustom != null) {
                return modelEmojiCustom;
            }
        }
        return null;
    }

    public final Observable<EmojiSet> getEmojiSet(long j, long j2, boolean z2, boolean z3) {
        return getEmojiSet(new EmojiContext.Chat(j, j2), z2, z3);
    }

    @Override // com.discord.utilities.textprocessing.Rules.EmojiDataProvider
    public Map<String, ModelEmojiUnicode> getUnicodeEmojiSurrogateMap() {
        Map map = this.unicodeEmojiSurrogateMap;
        if (map == null) {
            m.throwUninitializedPropertyAccessException("unicodeEmojiSurrogateMap");
        }
        return map;
    }

    @Override // com.discord.utilities.textprocessing.Rules.EmojiDataProvider
    public Map<String, ModelEmojiUnicode> getUnicodeEmojisNamesMap() {
        Map map = this.unicodeEmojisNamesMap;
        if (map == null) {
            m.throwUninitializedPropertyAccessException("unicodeEmojisNamesMap");
        }
        return map;
    }

    @Override // com.discord.utilities.textprocessing.Rules.EmojiDataProvider
    public Pattern getUnicodeEmojisPattern() {
        Pattern pattern = this.unicodeEmojisPattern;
        if (pattern == null) {
            m.throwUninitializedPropertyAccessException("unicodeEmojisPattern");
        }
        return pattern;
    }

    @StoreThread
    public final void handlePreLogout() {
        Persister.clear$default(this.frecencyCache, false, 1, null);
    }

    public final void initBlocking(Context context) {
        m.checkNotNullParameter(context, "context");
        handleLoadedUnicodeEmojis(loadUnicodeEmojisFromDisk(context));
    }

    public final void onEmojiUsed(Emoji emoji) {
        m.checkNotNullParameter(emoji, "emoji");
        String uniqueId = emoji.getUniqueId();
        m.checkNotNullExpressionValue(uniqueId, "emoji.uniqueId");
        onEmojiUsed(uniqueId);
    }

    public final void onEmojiUsed(String str) {
        m.checkNotNullParameter(str, "emojiKey");
        FrecencyTracker.track$default(this.frecency, str, 0L, 2, null);
        Persister.set$default(this.frecencyCache, this.frecency, false, 2, null);
    }

    public final Observable<EmojiSet> getEmojiSet(final EmojiContext emojiContext, final boolean z2, final boolean z3) {
        Observable observable;
        k kVar;
        m.checkNotNullParameter(emojiContext, "emojiContext");
        if (emojiContext instanceof EmojiContext.Chat) {
            EmojiContext.Chat chat = (EmojiContext.Chat) emojiContext;
            if (chat.getGuildId() != 0) {
                observable = this.permissionsStore.observePermissionsForChannel(chat.getChannelId()).F(StoreEmoji$getEmojiSet$hasExternalEmojiPermissionObservable$1.INSTANCE);
                Observable<EmojiSet> Y = Observable.h(StoreUser.observeMe$default(this.userStore, false, 1, null).F(StoreEmoji$getEmojiSet$1.INSTANCE), observable, this.sortedGuildsStore.observeOrderedGuilds().F(StoreEmoji$getEmojiSet$2.INSTANCE), this.mediaFavoritesStore.observeFavorites(StoreMediaFavorites.Favorite.Companion.getEmojiTypes()), StoreEmoji$getEmojiSet$3.INSTANCE).q().Y(new j0.k.b<Quad<? extends Boolean, ? extends Boolean, ? extends List<? extends Long>, ? extends Set<? extends StoreMediaFavorites.Favorite>>, Observable<? extends EmojiSet>>() { // from class: com.discord.stores.StoreEmoji$getEmojiSet$4
                    @Override // j0.k.b
                    public /* bridge */ /* synthetic */ Observable<? extends EmojiSet> call(Quad<? extends Boolean, ? extends Boolean, ? extends List<? extends Long>, ? extends Set<? extends StoreMediaFavorites.Favorite>> quad) {
                        return call2((Quad<Boolean, Boolean, ? extends List<Long>, ? extends Set<? extends StoreMediaFavorites.Favorite>>) quad);
                    }

                    /* renamed from: call  reason: avoid collision after fix types in other method */
                    public final Observable<? extends EmojiSet> call2(Quad<Boolean, Boolean, ? extends List<Long>, ? extends Set<? extends StoreMediaFavorites.Favorite>> quad) {
                        StoreEmojiCustom storeEmojiCustom;
                        final Boolean component1 = quad.component1();
                        final Boolean component2 = quad.component2();
                        final List<Long> component3 = quad.component3();
                        final Set<? extends StoreMediaFavorites.Favorite> component4 = quad.component4();
                        storeEmojiCustom = StoreEmoji.this.customEmojiStore;
                        return ObservableExtensionsKt.computationLatest(storeEmojiCustom.observeAllowedGuildEmoji()).F(new j0.k.b<Map<Long, ? extends Map<Long, ? extends ModelEmojiCustom>>, EmojiSet>() { // from class: com.discord.stores.StoreEmoji$getEmojiSet$4.1
                            public final EmojiSet call(Map<Long, ? extends Map<Long, ? extends ModelEmojiCustom>> map) {
                                EmojiSet buildUsableEmojiSet;
                                StoreEmoji storeEmoji = StoreEmoji.this;
                                m.checkNotNullExpressionValue(map, "allowedCustomEmoji");
                                StoreEmoji.EmojiContext emojiContext2 = emojiContext;
                                List list = component3;
                                m.checkNotNullExpressionValue(list, "sortedGuildIds");
                                Boolean bool = component1;
                                m.checkNotNullExpressionValue(bool, "isPremium");
                                boolean booleanValue = bool.booleanValue();
                                StoreEmoji$getEmojiSet$4 storeEmoji$getEmojiSet$4 = StoreEmoji$getEmojiSet$4.this;
                                boolean z4 = z3;
                                boolean z5 = z2;
                                Boolean bool2 = component2;
                                m.checkNotNullExpressionValue(bool2, "hasExternalEmojiPermission");
                                boolean booleanValue2 = bool2.booleanValue();
                                Set set = component4;
                                m.checkNotNullExpressionValue(set, "favorites");
                                buildUsableEmojiSet = storeEmoji.buildUsableEmojiSet(map, emojiContext2, list, booleanValue, z5, z4, booleanValue2, set);
                                return buildUsableEmojiSet;
                            }
                        });
                    }
                });
                m.checkNotNullExpressionValue(Y, "Observable\n        .comb…              }\n        }");
                return Y;
            }
        }
        if (emojiContext instanceof EmojiContext.Guild) {
            kVar = new k(Boolean.FALSE);
        } else {
            kVar = new k(Boolean.TRUE);
        }
        observable = kVar;
        Observable<EmojiSet> Y2 = Observable.h(StoreUser.observeMe$default(this.userStore, false, 1, null).F(StoreEmoji$getEmojiSet$1.INSTANCE), observable, this.sortedGuildsStore.observeOrderedGuilds().F(StoreEmoji$getEmojiSet$2.INSTANCE), this.mediaFavoritesStore.observeFavorites(StoreMediaFavorites.Favorite.Companion.getEmojiTypes()), StoreEmoji$getEmojiSet$3.INSTANCE).q().Y(new j0.k.b<Quad<? extends Boolean, ? extends Boolean, ? extends List<? extends Long>, ? extends Set<? extends StoreMediaFavorites.Favorite>>, Observable<? extends EmojiSet>>() { // from class: com.discord.stores.StoreEmoji$getEmojiSet$4
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Observable<? extends EmojiSet> call(Quad<? extends Boolean, ? extends Boolean, ? extends List<? extends Long>, ? extends Set<? extends StoreMediaFavorites.Favorite>> quad) {
                return call2((Quad<Boolean, Boolean, ? extends List<Long>, ? extends Set<? extends StoreMediaFavorites.Favorite>>) quad);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Observable<? extends EmojiSet> call2(Quad<Boolean, Boolean, ? extends List<Long>, ? extends Set<? extends StoreMediaFavorites.Favorite>> quad) {
                StoreEmojiCustom storeEmojiCustom;
                final Boolean component1 = quad.component1();
                final Boolean component2 = quad.component2();
                final List component3 = quad.component3();
                final Set component4 = quad.component4();
                storeEmojiCustom = StoreEmoji.this.customEmojiStore;
                return ObservableExtensionsKt.computationLatest(storeEmojiCustom.observeAllowedGuildEmoji()).F(new j0.k.b<Map<Long, ? extends Map<Long, ? extends ModelEmojiCustom>>, EmojiSet>() { // from class: com.discord.stores.StoreEmoji$getEmojiSet$4.1
                    public final EmojiSet call(Map<Long, ? extends Map<Long, ? extends ModelEmojiCustom>> map) {
                        EmojiSet buildUsableEmojiSet;
                        StoreEmoji storeEmoji = StoreEmoji.this;
                        m.checkNotNullExpressionValue(map, "allowedCustomEmoji");
                        StoreEmoji.EmojiContext emojiContext2 = emojiContext;
                        List list = component3;
                        m.checkNotNullExpressionValue(list, "sortedGuildIds");
                        Boolean bool = component1;
                        m.checkNotNullExpressionValue(bool, "isPremium");
                        boolean booleanValue = bool.booleanValue();
                        StoreEmoji$getEmojiSet$4 storeEmoji$getEmojiSet$4 = StoreEmoji$getEmojiSet$4.this;
                        boolean z4 = z3;
                        boolean z5 = z2;
                        Boolean bool2 = component2;
                        m.checkNotNullExpressionValue(bool2, "hasExternalEmojiPermission");
                        boolean booleanValue2 = bool2.booleanValue();
                        Set set = component4;
                        m.checkNotNullExpressionValue(set, "favorites");
                        buildUsableEmojiSet = storeEmoji.buildUsableEmojiSet(map, emojiContext2, list, booleanValue, z5, z4, booleanValue2, set);
                        return buildUsableEmojiSet;
                    }
                });
            }
        });
        m.checkNotNullExpressionValue(Y2, "Observable\n        .comb…              }\n        }");
        return Y2;
    }
}
