package com.discord.stores;

import com.discord.utilities.applicationcommands.ApplicationCommandFrecencyTracker;
import com.discord.utilities.frecency.FrecencyTracker;
import com.discord.utilities.persister.Persister;
import d0.z.d.o;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreApplicationCommandFrecency.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreApplicationCommandFrecency$onCommandUsed$1 extends o implements Function0<Unit> {
    public final /* synthetic */ String $commandId;
    public final /* synthetic */ Long $guildId;
    public final /* synthetic */ StoreApplicationCommandFrecency this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreApplicationCommandFrecency$onCommandUsed$1(StoreApplicationCommandFrecency storeApplicationCommandFrecency, Long l, String str) {
        super(0);
        this.this$0 = storeApplicationCommandFrecency;
        this.$guildId = l;
        this.$commandId = str;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        ApplicationCommandFrecencyTracker applicationCommandFrecencyTracker;
        String key;
        Persister persister;
        ApplicationCommandFrecencyTracker applicationCommandFrecencyTracker2;
        Map map;
        List allTopCommandIds;
        applicationCommandFrecencyTracker = this.this$0.frecency;
        key = this.this$0.getKey(this.$guildId, this.$commandId);
        FrecencyTracker.track$default(applicationCommandFrecencyTracker, key, 0L, 2, null);
        persister = this.this$0.frecencyCache;
        applicationCommandFrecencyTracker2 = this.this$0.frecency;
        persister.set(applicationCommandFrecencyTracker2, true);
        map = this.this$0.topCommandIds;
        Long l = this.$guildId;
        Long valueOf = Long.valueOf(l != null ? l.longValue() : 0L);
        allTopCommandIds = this.this$0.getAllTopCommandIds(this.$guildId);
        map.put(valueOf, allTopCommandIds);
        StoreApplicationCommandFrecency storeApplicationCommandFrecency = this.this$0;
        storeApplicationCommandFrecency.markChanged(storeApplicationCommandFrecency);
    }
}
