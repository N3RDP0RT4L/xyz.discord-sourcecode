package com.discord.stores;

import andhook.lib.HookHelper;
import com.discord.api.application.Application;
import com.discord.stores.updates.ObservationDeck;
import d0.z.d.m;
import j0.l.e.k;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: StoreApplication.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0000\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0017\u001a\u00020\u0016\u0012\u0006\u0010\u0014\u001a\u00020\u0013¢\u0006\u0004\b\"\u0010#J%\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00052\u000e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003¢\u0006\u0004\b\u0007\u0010\bJ5\u0010\f\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00060\u000b0\u00052\u0010\u0010\n\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\t¢\u0006\u0004\b\f\u0010\rJ\u0019\u0010\u000f\u001a\u00020\u000e2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u000f\u0010\u0011\u001a\u00020\u000eH\u0016¢\u0006\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0014\u001a\u00020\u00138\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u0016\u0010\u0017\u001a\u00020\u00168\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R:\u0010\u001b\u001a&\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00060\u0019j\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u0006`\u001a8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR.\u0010\u001f\u001a\u001a\u0012\b\u0012\u00060\u0002j\u0002`\u00030\u001dj\f\u0012\b\u0012\u00060\u0002j\u0002`\u0003`\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 R:\u0010!\u001a&\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00060\u0019j\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u0006`\u001a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\u001c¨\u0006$"}, d2 = {"Lcom/discord/stores/StoreApplication;", "Lcom/discord/stores/StoreV2;", "", "Lcom/discord/primitives/ApplicationId;", "appId", "Lrx/Observable;", "Lcom/discord/api/application/Application;", "observeApplication", "(Ljava/lang/Long;)Lrx/Observable;", "", "applicationIds", "", "observeApplications", "(Ljava/util/Collection;)Lrx/Observable;", "", "fetchIfNonexisting", "(J)V", "snapshotData", "()V", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "applicationsSnapshot", "Ljava/util/HashMap;", "Ljava/util/HashSet;", "Lkotlin/collections/HashSet;", "applicationsLoading", "Ljava/util/HashSet;", "applications", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreApplication extends StoreV2 {
    private final HashMap<Long, Application> applications = new HashMap<>();
    private final HashSet<Long> applicationsLoading = new HashSet<>();
    private HashMap<Long, Application> applicationsSnapshot = new HashMap<>();
    private final Dispatcher dispatcher;
    private final ObservationDeck observationDeck;

    public StoreApplication(Dispatcher dispatcher, ObservationDeck observationDeck) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
    }

    public final void fetchIfNonexisting(long j) {
        this.dispatcher.schedule(new StoreApplication$fetchIfNonexisting$1(this, j));
    }

    public final Observable<Application> observeApplication(Long l) {
        if (l == null) {
            k kVar = new k(null);
            m.checkNotNullExpressionValue(kVar, "Observable\n          .just(null)");
            return kVar;
        }
        fetchIfNonexisting(l.longValue());
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreApplication$observeApplication$1(this, l), 14, null);
    }

    public final Observable<Map<Long, Application>> observeApplications(Collection<Long> collection) {
        m.checkNotNullParameter(collection, "applicationIds");
        Observable<Map<Long, Application>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreApplication$observeApplications$1(this, collection), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        super.snapshotData();
        this.applicationsSnapshot = new HashMap<>(this.applications);
    }
}
