package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: StoreGooglePlayPurchases.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0007\u001a\u00020\u0002\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J.\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\u0007\u001a\u00020\u00022\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\f\u0010\u0004J\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0014\u001a\u0004\b\u0015\u0010\u0004R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0014\u001a\u0004\b\u0016\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0014\u001a\u0004\b\u0017\u0010\u0004¨\u0006\u001a"}, d2 = {"Lcom/discord/stores/PendingDowngrade;", "", "", "component1", "()Ljava/lang/String;", "component2", "component3", "purchaseToken", "subscriptionId", "newSkuName", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/stores/PendingDowngrade;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getSubscriptionId", "getPurchaseToken", "getNewSkuName", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class PendingDowngrade {
    private final String newSkuName;
    private final String purchaseToken;
    private final String subscriptionId;

    public PendingDowngrade(String str, String str2, String str3) {
        a.n0(str, "purchaseToken", str2, "subscriptionId", str3, "newSkuName");
        this.purchaseToken = str;
        this.subscriptionId = str2;
        this.newSkuName = str3;
    }

    public static /* synthetic */ PendingDowngrade copy$default(PendingDowngrade pendingDowngrade, String str, String str2, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = pendingDowngrade.purchaseToken;
        }
        if ((i & 2) != 0) {
            str2 = pendingDowngrade.subscriptionId;
        }
        if ((i & 4) != 0) {
            str3 = pendingDowngrade.newSkuName;
        }
        return pendingDowngrade.copy(str, str2, str3);
    }

    public final String component1() {
        return this.purchaseToken;
    }

    public final String component2() {
        return this.subscriptionId;
    }

    public final String component3() {
        return this.newSkuName;
    }

    public final PendingDowngrade copy(String str, String str2, String str3) {
        m.checkNotNullParameter(str, "purchaseToken");
        m.checkNotNullParameter(str2, "subscriptionId");
        m.checkNotNullParameter(str3, "newSkuName");
        return new PendingDowngrade(str, str2, str3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PendingDowngrade)) {
            return false;
        }
        PendingDowngrade pendingDowngrade = (PendingDowngrade) obj;
        return m.areEqual(this.purchaseToken, pendingDowngrade.purchaseToken) && m.areEqual(this.subscriptionId, pendingDowngrade.subscriptionId) && m.areEqual(this.newSkuName, pendingDowngrade.newSkuName);
    }

    public final String getNewSkuName() {
        return this.newSkuName;
    }

    public final String getPurchaseToken() {
        return this.purchaseToken;
    }

    public final String getSubscriptionId() {
        return this.subscriptionId;
    }

    public int hashCode() {
        String str = this.purchaseToken;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.subscriptionId;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.newSkuName;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("PendingDowngrade(purchaseToken=");
        R.append(this.purchaseToken);
        R.append(", subscriptionId=");
        R.append(this.subscriptionId);
        R.append(", newSkuName=");
        return a.H(R, this.newSkuName, ")");
    }
}
