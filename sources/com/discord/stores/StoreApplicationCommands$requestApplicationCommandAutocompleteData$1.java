package com.discord.stores;

import com.discord.models.commands.ApplicationCommandLocalSendDataKt;
import com.discord.restapi.RestAPIParams;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.chat.input.models.ApplicationCommandData;
import com.discord.widgets.chat.input.models.ApplicationCommandValue;
import d0.t.n;
import d0.t.r;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreApplicationCommands.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreApplicationCommands$requestApplicationCommandAutocompleteData$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ ApplicationCommandData $data;
    public final /* synthetic */ Long $guildId;
    public final /* synthetic */ StoreApplicationCommands this$0;

    /* compiled from: StoreApplicationCommands.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreApplicationCommands$requestApplicationCommandAutocompleteData$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<Error, Unit> {
        public final /* synthetic */ ApplicationCommandValue $option;

        /* compiled from: StoreApplicationCommands.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreApplicationCommands$requestApplicationCommandAutocompleteData$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C01951 extends o implements Function0<Unit> {
            public C01951() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                AnonymousClass1 r0 = AnonymousClass1.this;
                StoreApplicationCommands$requestApplicationCommandAutocompleteData$1.this.this$0.setAutocompleteFailed(r0.$option.getName(), String.valueOf(AnonymousClass1.this.$option.getValue()));
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(ApplicationCommandValue applicationCommandValue) {
            super(1);
            this.$option = applicationCommandValue;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Error error) {
            invoke2(error);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Error error) {
            Dispatcher dispatcher;
            m.checkNotNullParameter(error, "it");
            dispatcher = StoreApplicationCommands$requestApplicationCommandAutocompleteData$1.this.this$0.dispatcher;
            dispatcher.schedule(new C01951());
        }
    }

    /* compiled from: StoreApplicationCommands.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Ljava/lang/Void;", "it", "", "invoke", "(Ljava/lang/Void;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreApplicationCommands$requestApplicationCommandAutocompleteData$1$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2 extends o implements Function1<Void, Unit> {
        public static final AnonymousClass2 INSTANCE = new AnonymousClass2();

        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Void r1) {
            invoke2(r1);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Void r1) {
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreApplicationCommands$requestApplicationCommandAutocompleteData$1(StoreApplicationCommands storeApplicationCommands, ApplicationCommandData applicationCommandData, long j, Long l) {
        super(0);
        this.this$0 = storeApplicationCommands;
        this.$data = applicationCommandData;
        this.$channelId = j;
        this.$guildId = l;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        String generateNonce;
        String str;
        Object obj;
        Map map;
        RestAPI restAPI;
        generateNonce = this.this$0.generateNonce();
        String version = this.$data.getApplicationCommand().getVersion();
        Long guildId = this.$data.getApplicationCommand().getGuildId();
        com.discord.api.commands.ApplicationCommandData applicationCommandData = new com.discord.api.commands.ApplicationCommandData(version, guildId != null ? String.valueOf(guildId.longValue()) : null, this.$data.getApplicationCommand().getId(), this.$data.getApplicationCommand().getName(), ApplicationCommandLocalSendDataKt.toRestParams(this.$data.getValues()));
        String valueOf = String.valueOf(this.$channelId);
        String valueOf2 = String.valueOf(this.$data.getApplication().getId());
        Long l = this.$guildId;
        String valueOf3 = l != null ? String.valueOf(l.longValue()) : null;
        str = this.this$0.sessionId;
        RestAPIParams.ApplicationCommand applicationCommand = new RestAPIParams.ApplicationCommand(4L, valueOf, valueOf2, valueOf3, applicationCommandData, str, generateNonce);
        List<ApplicationCommandValue> values = this.$data.getValues();
        ArrayList arrayList = new ArrayList();
        for (ApplicationCommandValue applicationCommandValue : values) {
            List mutableListOf = n.mutableListOf(applicationCommandValue);
            List<ApplicationCommandValue> options = applicationCommandValue.getOptions();
            if (options == null) {
                options = n.emptyList();
            }
            mutableListOf.addAll(options);
            r.addAll(arrayList, mutableListOf);
        }
        Iterator it = arrayList.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (m.areEqual(((ApplicationCommandValue) obj).getFocused(), Boolean.TRUE)) {
                break;
            }
        }
        ApplicationCommandValue applicationCommandValue2 = (ApplicationCommandValue) obj;
        if (applicationCommandValue2 != null) {
            CommandOptionAutocompleteQuery commandOptionAutocompleteQuery = new CommandOptionAutocompleteQuery(String.valueOf(applicationCommandValue2.getValue()), applicationCommandValue2.getName());
            map = this.this$0.autocompleteNonceData;
            map.put(generateNonce, commandOptionAutocompleteQuery);
            restAPI = this.this$0.restApi;
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(restAPI.sendApplicationCommand(applicationCommand), false, 1, null), this.this$0.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new AnonymousClass1(applicationCommandValue2), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, AnonymousClass2.INSTANCE);
        }
    }
}
