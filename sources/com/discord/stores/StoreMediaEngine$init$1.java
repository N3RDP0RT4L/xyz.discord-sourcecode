package com.discord.stores;

import com.discord.models.experiments.domain.Experiment;
import com.discord.rtcconnection.mediaengine.MediaEngine;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreMediaEngine.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/models/experiments/domain/Experiment;", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Lcom/discord/models/experiments/domain/Experiment;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMediaEngine$init$1 extends o implements Function1<Experiment, Unit> {
    public final /* synthetic */ StoreMediaEngine this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreMediaEngine$init$1(StoreMediaEngine storeMediaEngine) {
        super(1);
        this.this$0 = storeMediaEngine;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Experiment experiment) {
        invoke2(experiment);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Experiment experiment) {
        MediaEngine.OpenSLUsageMode openSLUsageMode;
        int bucket = experiment.getBucket();
        if (bucket == 1 || bucket == 3) {
            openSLUsageMode = MediaEngine.OpenSLUsageMode.EXCLUDE_LIST;
        } else {
            openSLUsageMode = MediaEngine.OpenSLUsageMode.ALLOW_LIST;
        }
        this.this$0.getMediaEngine().h(openSLUsageMode);
    }
}
