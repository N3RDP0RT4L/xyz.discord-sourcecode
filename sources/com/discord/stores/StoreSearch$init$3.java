package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.stores.StoreSearch;
import com.discord.utilities.search.query.node.QueryNode;
import j0.k.b;
import java.util.List;
import kotlin.Metadata;
/* compiled from: StoreSearch.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a\n \u0002*\u0004\u0018\u00010\u00040\u00042\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "Lcom/discord/utilities/search/query/node/QueryNode;", "kotlin.jvm.PlatformType", "it", "Lcom/discord/stores/StoreSearch$DisplayState;", NotificationCompat.CATEGORY_CALL, "(Ljava/util/List;)Lcom/discord/stores/StoreSearch$DisplayState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreSearch$init$3<T, R> implements b<List<? extends QueryNode>, StoreSearch.DisplayState> {
    public static final StoreSearch$init$3 INSTANCE = new StoreSearch$init$3();

    public final StoreSearch.DisplayState call(List<? extends QueryNode> list) {
        return StoreSearch.DisplayState.SUGGESTIONS;
    }
}
