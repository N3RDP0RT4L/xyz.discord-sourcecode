package com.discord.stores;

import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreGuildTemplates.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000e\n\u0002\b\u0003\u0010\u0003\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()Ljava/lang/String;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildTemplates$observeDynamicLinkGuildTemplateCode$1 extends o implements Function0<String> {
    public final /* synthetic */ StoreGuildTemplates this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGuildTemplates$observeDynamicLinkGuildTemplateCode$1(StoreGuildTemplates storeGuildTemplates) {
        super(0);
        this.this$0 = storeGuildTemplates;
    }

    @Override // kotlin.jvm.functions.Function0
    public final String invoke() {
        String str;
        str = this.this$0.dynamicLinkGuildTemplateCode;
        return str;
    }
}
