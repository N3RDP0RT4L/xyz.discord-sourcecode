package com.discord.stores;

import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreAuditLog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAuditLog$fetchAuditLogIfNeeded$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ StoreAuditLog this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreAuditLog$fetchAuditLogIfNeeded$1(StoreAuditLog storeAuditLog, long j) {
        super(0);
        this.this$0 = storeAuditLog;
        this.$guildId = j;
    }

    /* JADX WARN: Code restructure failed: missing block: B:9:0x002b, code lost:
        if (r1.isLoading() == false) goto L11;
     */
    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void invoke2() {
        /*
            r7 = this;
            long r0 = r7.$guildId
            com.discord.stores.StoreAuditLog r2 = r7.this$0
            com.discord.stores.StoreAuditLog$AuditLogState r2 = com.discord.stores.StoreAuditLog.access$getState$p(r2)
            long r2 = r2.getGuildId()
            r4 = 1
            r5 = 0
            int r6 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r6 == 0) goto L14
            r0 = 1
            goto L15
        L14:
            r0 = 0
        L15:
            com.discord.stores.StoreAuditLog r1 = r7.this$0
            com.discord.stores.StoreAuditLog$AuditLogState r1 = com.discord.stores.StoreAuditLog.access$getState$p(r1)
            java.util.List r1 = r1.getEntries()
            if (r1 != 0) goto L2e
            com.discord.stores.StoreAuditLog r1 = r7.this$0
            com.discord.stores.StoreAuditLog$AuditLogState r1 = com.discord.stores.StoreAuditLog.access$getState$p(r1)
            boolean r1 = r1.isLoading()
            if (r1 != 0) goto L2e
            goto L2f
        L2e:
            r4 = 0
        L2f:
            if (r0 == 0) goto L36
            com.discord.stores.StoreAuditLog r1 = r7.this$0
            com.discord.stores.StoreAuditLog.access$clearStateInternal(r1)
        L36:
            if (r0 != 0) goto L3a
            if (r4 == 0) goto L49
        L3a:
            com.discord.stores.StoreAuditLog r0 = r7.this$0
            long r1 = r7.$guildId
            com.discord.stores.StoreAuditLog$AuditLogState r3 = com.discord.stores.StoreAuditLog.access$getState$p(r0)
            com.discord.stores.StoreAuditLog$AuditLogFilter r3 = r3.getFilter()
            com.discord.stores.StoreAuditLog.access$fetchAuditLogs(r0, r1, r3)
        L49:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreAuditLog$fetchAuditLogIfNeeded$1.invoke2():void");
    }
}
