package com.discord.stores;

import com.discord.utilities.media.MediaFrecencyTracker;
import com.discord.utilities.persister.Persister;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreStickers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStickers$handleDeletedStickers$1 extends o implements Function0<Unit> {
    public final /* synthetic */ List $stickerIds;
    public final /* synthetic */ StoreStickers this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreStickers$handleDeletedStickers$1(StoreStickers storeStickers, List list) {
        super(0);
        this.this$0 = storeStickers;
        this.$stickerIds = list;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        Persister persister;
        MediaFrecencyTracker mediaFrecencyTracker;
        MediaFrecencyTracker mediaFrecencyTracker2;
        for (Number number : this.$stickerIds) {
            long longValue = number.longValue();
            mediaFrecencyTracker2 = this.this$0.frecency;
            mediaFrecencyTracker2.removeEntry(String.valueOf(longValue));
        }
        persister = this.this$0.frecencyCache;
        mediaFrecencyTracker = this.this$0.frecency;
        persister.set(mediaFrecencyTracker, true);
    }
}
