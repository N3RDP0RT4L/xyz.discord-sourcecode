package com.discord.stores;

import com.discord.widgets.stage.model.StageChannel;
import d0.f0.q;
import d0.t.h0;
import d0.t.u;
import d0.z.d.o;
import java.util.Iterator;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreStageChannels.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u00030\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/widgets/stage/model/StageChannel;", "invoke", "()Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStageChannels$observeStageChannels$1 extends o implements Function0<Map<Long, ? extends StageChannel>> {
    public final /* synthetic */ StoreStageChannels this$0;

    /* compiled from: StoreStageChannels.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\b\u001a\u0012\u0012\b\u0012\u00060\u0000j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00032\n\u0010\u0002\u001a\u00060\u0000j\u0002`\u0001H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "Lcom/discord/primitives/GuildId;", "guildId", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/widgets/stage/model/StageChannel;", "invoke", "(J)Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreStageChannels$observeStageChannels$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<Long, Map<Long, ? extends StageChannel>> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Map<Long, ? extends StageChannel> invoke(Long l) {
            return invoke(l.longValue());
        }

        public final Map<Long, StageChannel> invoke(long j) {
            return StoreStageChannels.getStageChannelsInGuild$default(StoreStageChannels$observeStageChannels$1.this.this$0, j, null, 0L, null, null, 30, null);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreStageChannels$observeStageChannels$1(StoreStageChannels storeStageChannels) {
        super(0);
        this.this$0 = storeStageChannels;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Map<Long, ? extends StageChannel> invoke() {
        StoreGuilds storeGuilds;
        Object obj;
        storeGuilds = this.this$0.guildsStore;
        Iterator it = q.map(u.asSequence(storeGuilds.getGuilds().keySet()), new AnonymousClass1()).iterator();
        if (!it.hasNext()) {
            obj = null;
        } else {
            Object next = it.next();
            while (it.hasNext()) {
                next = h0.plus((Map) next, (Map) it.next());
            }
            obj = next;
        }
        Map<Long, ? extends StageChannel> map = (Map) obj;
        return map != null ? map : h0.emptyMap();
    }
}
