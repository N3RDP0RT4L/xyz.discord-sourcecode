package com.discord.stores;

import com.discord.utilities.fcm.NotificationClient;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreNotifications.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\b\u001a\u00020\u00052F\u0010\u0004\u001aB\u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00010\u0001\u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00030\u0003 \u0002* \u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00010\u0001\u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00030\u0003\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Lkotlin/Pair;", "Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;", "kotlin.jvm.PlatformType", "", "<name for destructuring parameter 0>", "", "invoke", "(Lkotlin/Pair;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreNotifications$configureNotificationClient$3 extends o implements Function1<Pair<? extends NotificationClient.SettingsV2, ? extends Boolean>, Unit> {
    public static final StoreNotifications$configureNotificationClient$3 INSTANCE = new StoreNotifications$configureNotificationClient$3();

    public StoreNotifications$configureNotificationClient$3() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Pair<? extends NotificationClient.SettingsV2, ? extends Boolean> pair) {
        invoke2((Pair<NotificationClient.SettingsV2, Boolean>) pair);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Pair<NotificationClient.SettingsV2, Boolean> pair) {
        NotificationClient.SettingsV2 component1 = pair.component1();
        Boolean component2 = pair.component2();
        NotificationClient notificationClient = NotificationClient.INSTANCE;
        m.checkNotNullExpressionValue(component1, "settings");
        m.checkNotNullExpressionValue(component2, "isBackgrounded");
        notificationClient.updateSettings$app_productionGoogleRelease(component1, component2.booleanValue());
    }
}
