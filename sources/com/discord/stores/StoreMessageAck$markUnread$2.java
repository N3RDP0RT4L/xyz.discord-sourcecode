package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.models.message.Message;
import com.discord.stores.StoreMessageAck;
import java.util.List;
import kotlin.Metadata;
import kotlin.Pair;
import rx.functions.Func2;
/* compiled from: StoreMessageAck.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\n\u001aj\u0012 \u0012\u001e\u0012\b\u0012\u00060\u0001j\u0002`\u0002 \u0003*\u000e\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0018\u00010\u00000\u0000\u0012\f\u0012\n \u0003*\u0004\u0018\u00010\u00050\u0005 \u0003*4\u0012 \u0012\u001e\u0012\b\u0012\u00060\u0001j\u0002`\u0002 \u0003*\u000e\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0018\u00010\u00000\u0000\u0012\f\u0012\n \u0003*\u0004\u0018\u00010\u00050\u0005\u0018\u00010\u00070\u00072\"\u0010\u0004\u001a\u001e\u0012\b\u0012\u00060\u0001j\u0002`\u0002 \u0003*\u000e\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0018\u00010\u00000\u00002\u000e\u0010\u0006\u001a\n \u0003*\u0004\u0018\u00010\u00050\u0005H\n¢\u0006\u0004\b\b\u0010\t"}, d2 = {"", "Lcom/discord/models/message/Message;", "Lcom/discord/stores/ClientMessage;", "kotlin.jvm.PlatformType", "channelMessages", "Lcom/discord/stores/StoreMessageAck$ThreadState;", "threadState", "Lkotlin/Pair;", NotificationCompat.CATEGORY_CALL, "(Ljava/util/List;Lcom/discord/stores/StoreMessageAck$ThreadState;)Lkotlin/Pair;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessageAck$markUnread$2<T1, T2, R> implements Func2<List<? extends Message>, StoreMessageAck.ThreadState, Pair<? extends List<? extends Message>, ? extends StoreMessageAck.ThreadState>> {
    public static final StoreMessageAck$markUnread$2 INSTANCE = new StoreMessageAck$markUnread$2();

    @Override // rx.functions.Func2
    public /* bridge */ /* synthetic */ Pair<? extends List<? extends Message>, ? extends StoreMessageAck.ThreadState> call(List<? extends Message> list, StoreMessageAck.ThreadState threadState) {
        return call2((List<Message>) list, threadState);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final Pair<List<Message>, StoreMessageAck.ThreadState> call2(List<Message> list, StoreMessageAck.ThreadState threadState) {
        return new Pair<>(list, threadState);
    }
}
