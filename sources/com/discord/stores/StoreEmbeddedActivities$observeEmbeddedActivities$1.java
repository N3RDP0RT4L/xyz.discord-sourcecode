package com.discord.stores;

import com.discord.models.embeddedactivities.EmbeddedActivity;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreEmbeddedActivities.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\b\u001a&\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0001j\u0002`\u0003\u0012\b\u0012\u00060\u0004j\u0002`\u00050\u00000\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/primitives/ApplicationId;", "Lcom/discord/models/embeddedactivities/EmbeddedActivity;", "Lcom/discord/stores/ClientEmbeddedActivity;", "invoke", "()Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreEmbeddedActivities$observeEmbeddedActivities$1 extends o implements Function0<Map<Long, ? extends Map<Long, ? extends EmbeddedActivity>>> {
    public final /* synthetic */ StoreEmbeddedActivities this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreEmbeddedActivities$observeEmbeddedActivities$1(StoreEmbeddedActivities storeEmbeddedActivities) {
        super(0);
        this.this$0 = storeEmbeddedActivities;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Map<Long, ? extends Map<Long, ? extends EmbeddedActivity>> invoke() {
        return this.this$0.getEmbeddedActivities();
    }
}
