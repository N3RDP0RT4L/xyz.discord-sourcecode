package com.discord.stores;

import com.discord.stores.StoreSpotify;
import com.discord.utilities.time.Clock;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import rx.subjects.BehaviorSubject;
/* compiled from: StoreSpotify.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreSpotify$setPlayingStatus$1 extends o implements Function0<Unit> {
    public final /* synthetic */ boolean $playing;
    public final /* synthetic */ int $position;
    public final /* synthetic */ StoreSpotify this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreSpotify$setPlayingStatus$1(StoreSpotify storeSpotify, boolean z2, int i) {
        super(0);
        this.this$0 = storeSpotify;
        this.$playing = z2;
        this.$position = i;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreSpotify.SpotifyState spotifyState;
        StoreSpotify.SpotifyState spotifyState2;
        StoreSpotify.SpotifyState spotifyState3;
        BehaviorSubject behaviorSubject;
        Clock clock;
        spotifyState = this.this$0.spotifyState;
        if (spotifyState == null) {
            this.this$0.spotifyState = new StoreSpotify.SpotifyState(null, false, 0, 0L, 15, null);
        }
        StoreSpotify storeSpotify = this.this$0;
        spotifyState2 = storeSpotify.spotifyState;
        if (spotifyState2 != null) {
            boolean z2 = this.$playing;
            int i = this.$position;
            clock = this.this$0.clock;
            spotifyState3 = StoreSpotify.SpotifyState.copy$default(spotifyState2, null, z2, i, clock.currentTimeMillis() - this.$position, 1, null);
        } else {
            spotifyState3 = null;
        }
        storeSpotify.spotifyState = spotifyState3;
        behaviorSubject = this.this$0.publishStateTrigger;
        behaviorSubject.onNext(Unit.a);
    }
}
