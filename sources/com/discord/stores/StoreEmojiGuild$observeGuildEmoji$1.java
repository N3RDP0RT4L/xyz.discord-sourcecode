package com.discord.stores;

import com.discord.models.domain.emoji.ModelEmojiGuild;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreEmojiGuild.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"", "Lcom/discord/models/domain/emoji/ModelEmojiGuild;", "invoke", "()Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreEmojiGuild$observeGuildEmoji$1 extends o implements Function0<List<? extends ModelEmojiGuild>> {
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ StoreEmojiGuild this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreEmojiGuild$observeGuildEmoji$1(StoreEmojiGuild storeEmojiGuild, long j) {
        super(0);
        this.this$0 = storeEmojiGuild;
        this.$guildId = j;
    }

    @Override // kotlin.jvm.functions.Function0
    public final List<? extends ModelEmojiGuild> invoke() {
        return this.this$0.getGuildEmoji(this.$guildId);
    }
}
