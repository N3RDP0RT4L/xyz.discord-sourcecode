package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import d0.t.u;
import d0.z.d.m;
import j0.k.b;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
/* compiled from: StoreInviteSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0003\u0010\u0007\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00040\u00042\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "Lcom/discord/api/channel/Channel;", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Collection;)Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreInviteSettings$generateInviteDefaultChannel$2<T, R> implements b<Collection<? extends Channel>, List<? extends Channel>> {
    public static final StoreInviteSettings$generateInviteDefaultChannel$2 INSTANCE = new StoreInviteSettings$generateInviteDefaultChannel$2();

    @Override // j0.k.b
    public /* bridge */ /* synthetic */ List<? extends Channel> call(Collection<? extends Channel> collection) {
        return call2((Collection<Channel>) collection);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final List<Channel> call2(Collection<Channel> collection) {
        m.checkNotNullExpressionValue(collection, "it");
        return u.sortedWith(collection, ChannelUtils.h(Channel.Companion));
    }
}
