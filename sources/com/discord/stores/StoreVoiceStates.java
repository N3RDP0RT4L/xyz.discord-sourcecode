package com.discord.stores;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import com.discord.api.guild.Guild;
import com.discord.api.voice.state.VoiceState;
import com.discord.models.domain.ModelPayload;
import com.discord.stores.updates.ObservationDeck;
import d0.t.h0;
import d0.z.d.m;
import j0.k.b;
import j0.l.a.l0;
import j0.l.e.m;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import rx.Observable;
/* compiled from: StoreVoiceStates.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B;\u0012*\u0010>\u001a&\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\b\u0012\u00060\u0004j\u0002`\u0011\u0012\b\u0012\u00060\u0004j\u0002`\u000e\u0012\u0004\u0012\u00020\u00070=\u0012\u0006\u00104\u001a\u000203¢\u0006\u0004\b@\u0010AJ)\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\u0010\b\u0002\u0010\u0006\u001a\n\u0018\u00010\u0004j\u0004\u0018\u0001`\u0005H\u0003¢\u0006\u0004\b\b\u0010\tJ\u000f\u0010\n\u001a\u00020\u0007H\u0003¢\u0006\u0004\b\n\u0010\u000bJ/\u0010\u000f\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u000e\u0012\u0004\u0012\u00020\u00020\r0\f2\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u0005¢\u0006\u0004\b\u000f\u0010\u0010J;\u0010\u000f\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u000e\u0012\u0004\u0012\u00020\u00020\r0\f2\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\n\u0010\u0012\u001a\u00060\u0004j\u0002`\u0011¢\u0006\u0004\b\u000f\u0010\u0013J/\u0010\u0014\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u000e\u0012\u0004\u0012\u00020\u00020\r0\f2\n\u0010\u0012\u001a\u00060\u0004j\u0002`\u0011¢\u0006\u0004\b\u0014\u0010\u0010J5\u0010\u0015\u001a\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u000e\u0012\u0004\u0012\u00020\u00020\r2\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\n\u0010\u0012\u001a\u00060\u0004j\u0002`\u0011¢\u0006\u0004\b\u0015\u0010\u0016J-\u0010\u0017\u001a\"\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u000e\u0012\u0004\u0012\u00020\u00020\r0\r¢\u0006\u0004\b\u0017\u0010\u0018J/\u0010\u001a\u001a\"\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u000e\u0012\u0004\u0012\u00020\u00020\r0\rH\u0001¢\u0006\u0004\b\u0019\u0010\u0018J\u0017\u0010\u001d\u001a\u00020\u00072\u0006\u0010\u001c\u001a\u00020\u001bH\u0007¢\u0006\u0004\b\u001d\u0010\u001eJ\u0017\u0010!\u001a\u00020\u00072\u0006\u0010 \u001a\u00020\u001fH\u0007¢\u0006\u0004\b!\u0010\"J\u0017\u0010#\u001a\u00020\u00072\u0006\u0010 \u001a\u00020\u001fH\u0007¢\u0006\u0004\b#\u0010\"J\u0017\u0010$\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b$\u0010%J\u0019\u0010(\u001a\u00020\u00072\b\u0010'\u001a\u0004\u0018\u00010&H\u0007¢\u0006\u0004\b(\u0010)J\u000f\u0010*\u001a\u00020\u0007H\u0017¢\u0006\u0004\b*\u0010\u000bR6\u0010+\u001a\"\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u000e\u0012\u0004\u0012\u00020\u00020\r0\r8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b+\u0010,R.\u0010/\u001a\u001a\u0012\b\u0012\u00060\u0004j\u0002`\u00050-j\f\u0012\b\u0012\u00060\u0004j\u0002`\u0005`.8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b/\u00100R\u001e\u00101\u001a\n\u0018\u00010\u0004j\u0004\u0018\u0001`\u000e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b1\u00102R\u0016\u00104\u001a\u0002038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b4\u00105R\u001e\u00107\u001a\n\u0018\u00010&j\u0004\u0018\u0001`68\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b7\u00108R\u0082\u0001\u0010;\u001an\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012(\u0012&\u0012\b\u0012\u00060\u0004j\u0002`\u000e\u0012\u0004\u0012\u00020\u000209j\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u000e\u0012\u0004\u0012\u00020\u0002`:09j6\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012(\u0012&\u0012\b\u0012\u00060\u0004j\u0002`\u000e\u0012\u0004\u0012\u00020\u000209j\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u000e\u0012\u0004\u0012\u00020\u0002`:`:8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b;\u0010<R:\u0010>\u001a&\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\b\u0012\u00060\u0004j\u0002`\u0011\u0012\b\u0012\u00060\u0004j\u0002`\u000e\u0012\u0004\u0012\u00020\u00070=8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b>\u0010?¨\u0006B"}, d2 = {"Lcom/discord/stores/StoreVoiceStates;", "Lcom/discord/stores/StoreV2;", "Lcom/discord/api/voice/state/VoiceState;", "voiceState", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "updateVoiceState", "(Lcom/discord/api/voice/state/VoiceState;Ljava/lang/Long;)V", "clear", "()V", "Lrx/Observable;", "", "Lcom/discord/primitives/UserId;", "observe", "(J)Lrx/Observable;", "Lcom/discord/primitives/ChannelId;", "channelId", "(JJ)Lrx/Observable;", "observeForPrivateChannels", "getForChannel", "(JJ)Ljava/util/Map;", "get", "()Ljava/util/Map;", "getInternal$app_productionGoogleRelease", "getInternal", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "Lcom/discord/api/guild/Guild;", "guild", "handleGuildAdd", "(Lcom/discord/api/guild/Guild;)V", "handleGuildRemove", "handleVoiceStateUpdate", "(Lcom/discord/api/voice/state/VoiceState;)V", "", "authToken", "handleAuthToken", "(Ljava/lang/String;)V", "snapshotData", "voiceStatesSnapshot", "Ljava/util/Map;", "Ljava/util/HashSet;", "Lkotlin/collections/HashSet;", "dirtyGuildIds", "Ljava/util/HashSet;", "myUserId", "Ljava/lang/Long;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/primitives/SessionId;", "sessionId", "Ljava/lang/String;", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "voiceStates", "Ljava/util/HashMap;", "Lkotlin/Function3;", "notifyVoiceStatesUpdated", "Lkotlin/jvm/functions/Function3;", HookHelper.constructorName, "(Lkotlin/jvm/functions/Function3;Lcom/discord/stores/updates/ObservationDeck;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreVoiceStates extends StoreV2 {
    private final HashSet<Long> dirtyGuildIds = new HashSet<>();
    private Long myUserId;
    private final Function3<Long, Long, Long, Unit> notifyVoiceStatesUpdated;
    private final ObservationDeck observationDeck;
    private String sessionId;
    private final HashMap<Long, HashMap<Long, VoiceState>> voiceStates;
    private Map<Long, ? extends Map<Long, VoiceState>> voiceStatesSnapshot;

    /* JADX WARN: Multi-variable type inference failed */
    public StoreVoiceStates(Function3<? super Long, ? super Long, ? super Long, Unit> function3, ObservationDeck observationDeck) {
        m.checkNotNullParameter(function3, "notifyVoiceStatesUpdated");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.notifyVoiceStatesUpdated = function3;
        this.observationDeck = observationDeck;
        HashMap<Long, HashMap<Long, VoiceState>> hashMap = new HashMap<>();
        this.voiceStates = hashMap;
        this.voiceStatesSnapshot = new HashMap(hashMap);
    }

    @StoreThread
    private final void clear() {
        this.dirtyGuildIds.addAll(this.voiceStates.keySet());
        this.voiceStates.clear();
        markChanged();
    }

    /* JADX WARN: Removed duplicated region for block: B:37:0x00bf  */
    @com.discord.stores.StoreThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void updateVoiceState(com.discord.api.voice.state.VoiceState r12, java.lang.Long r13) {
        /*
            r11 = this;
            if (r13 == 0) goto L7
            long r0 = r13.longValue()
            goto Lb
        L7:
            long r0 = r12.c()
        Lb:
            long r2 = r12.m()
            java.lang.String r13 = r12.k()
            java.lang.String r4 = r11.sessionId
            boolean r13 = d0.z.d.m.areEqual(r13, r4)
            r4 = 1
            r13 = r13 ^ r4
            if (r13 == 0) goto L4f
            java.lang.Long r13 = r11.myUserId
            if (r13 != 0) goto L22
            goto L4f
        L22:
            long r5 = r13.longValue()
            int r13 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r13 != 0) goto L4f
            java.util.HashMap<java.lang.Long, java.util.HashMap<java.lang.Long, com.discord.api.voice.state.VoiceState>> r12 = r11.voiceStates
            java.lang.Long r13 = java.lang.Long.valueOf(r0)
            java.lang.Object r12 = r12.get(r13)
            java.util.HashMap r12 = (java.util.HashMap) r12
            if (r12 == 0) goto Ld9
            java.lang.Long r13 = java.lang.Long.valueOf(r2)
            java.lang.Object r12 = r12.remove(r13)
            com.discord.api.voice.state.VoiceState r12 = (com.discord.api.voice.state.VoiceState) r12
            if (r12 == 0) goto Ld9
            java.util.HashSet<java.lang.Long> r12 = r11.dirtyGuildIds
            java.lang.Long r13 = java.lang.Long.valueOf(r0)
            r12.add(r13)
            goto Ld9
        L4f:
            r13 = 0
            java.util.HashMap<java.lang.Long, java.util.HashMap<java.lang.Long, com.discord.api.voice.state.VoiceState>> r5 = r11.voiceStates
            java.lang.Long r6 = java.lang.Long.valueOf(r0)
            java.util.HashMap<java.lang.Long, java.util.HashMap<java.lang.Long, com.discord.api.voice.state.VoiceState>> r7 = r11.voiceStates
            java.lang.Long r8 = java.lang.Long.valueOf(r0)
            java.lang.Object r7 = r7.get(r8)
            java.util.HashMap r7 = (java.util.HashMap) r7
            if (r7 == 0) goto L65
            goto L6a
        L65:
            java.util.HashMap r7 = new java.util.HashMap
            r7.<init>()
        L6a:
            boolean r8 = b.c.a.a0.d.X0(r12)
            r9 = 0
            if (r8 == 0) goto L8a
            java.lang.Long r12 = java.lang.Long.valueOf(r2)
            java.lang.Object r12 = r7.remove(r12)
            com.discord.api.voice.state.VoiceState r12 = (com.discord.api.voice.state.VoiceState) r12
            if (r12 == 0) goto Lba
            java.lang.Long r12 = r12.a()
            if (r12 == 0) goto Lb9
            long r12 = r12.longValue()
            r9 = r12
            goto Lb9
        L8a:
            java.lang.Long r8 = java.lang.Long.valueOf(r2)
            java.lang.Object r8 = r7.get(r8)
            com.discord.api.voice.state.VoiceState r8 = (com.discord.api.voice.state.VoiceState) r8
            boolean r8 = d0.z.d.m.areEqual(r12, r8)
            r8 = r8 ^ r4
            if (r8 == 0) goto Lba
            java.lang.Long r13 = java.lang.Long.valueOf(r2)
            java.lang.Object r13 = r7.get(r13)
            com.discord.api.voice.state.VoiceState r13 = (com.discord.api.voice.state.VoiceState) r13
            if (r13 == 0) goto Lb2
            java.lang.Long r13 = r13.a()
            if (r13 == 0) goto Lb2
            long r8 = r13.longValue()
            r9 = r8
        Lb2:
            java.lang.Long r13 = java.lang.Long.valueOf(r2)
            r7.put(r13, r12)
        Lb9:
            r13 = 1
        Lba:
            r5.put(r6, r7)
            if (r13 == 0) goto Ld9
            java.util.HashSet<java.lang.Long> r12 = r11.dirtyGuildIds
            java.lang.Long r13 = java.lang.Long.valueOf(r0)
            r12.add(r13)
            kotlin.jvm.functions.Function3<java.lang.Long, java.lang.Long, java.lang.Long, kotlin.Unit> r12 = r11.notifyVoiceStatesUpdated
            java.lang.Long r13 = java.lang.Long.valueOf(r0)
            java.lang.Long r0 = java.lang.Long.valueOf(r9)
            java.lang.Long r1 = java.lang.Long.valueOf(r2)
            r12.invoke(r13, r0, r1)
        Ld9:
            java.util.HashSet<java.lang.Long> r12 = r11.dirtyGuildIds
            boolean r12 = r12.isEmpty()
            r12 = r12 ^ r4
            if (r12 == 0) goto Le5
            r11.markChanged()
        Le5:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreVoiceStates.updateVoiceState(com.discord.api.voice.state.VoiceState, java.lang.Long):void");
    }

    public static /* synthetic */ void updateVoiceState$default(StoreVoiceStates storeVoiceStates, VoiceState voiceState, Long l, int i, Object obj) {
        if ((i & 2) != 0) {
            l = null;
        }
        storeVoiceStates.updateVoiceState(voiceState, l);
    }

    public final Map<Long, Map<Long, VoiceState>> get() {
        return this.voiceStatesSnapshot;
    }

    public final Map<Long, VoiceState> getForChannel(long j, long j2) {
        Map<Long, VoiceState> map = this.voiceStatesSnapshot.get(Long.valueOf(j));
        if (map == null) {
            map = h0.emptyMap();
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry<Long, VoiceState> entry : map.entrySet()) {
            Long a = entry.getValue().a();
            if (a != null && a.longValue() == j2) {
                linkedHashMap.put(entry.getKey(), entry.getValue());
            }
        }
        return linkedHashMap;
    }

    @StoreThread
    public final Map<Long, Map<Long, VoiceState>> getInternal$app_productionGoogleRelease() {
        return this.voiceStates;
    }

    @StoreThread
    public final void handleAuthToken(String str) {
        if (str == null) {
            clear();
        }
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        this.sessionId = modelPayload.getSessionId();
        this.myUserId = Long.valueOf(modelPayload.getMe().i());
        clear();
        List<Guild> guilds = modelPayload.getGuilds();
        m.checkNotNullExpressionValue(guilds, "payload.guilds");
        for (Guild guild : guilds) {
            List<VoiceState> R = guild.R();
            if (R != null) {
                for (VoiceState voiceState : R) {
                    updateVoiceState(voiceState, Long.valueOf(guild.r()));
                }
            }
        }
    }

    @StoreThread
    public final void handleGuildAdd(Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        List<VoiceState> R = guild.R();
        if (R != null) {
            for (VoiceState voiceState : R) {
                updateVoiceState(voiceState, Long.valueOf(guild.r()));
            }
        }
    }

    @StoreThread
    public final void handleGuildRemove(Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        this.voiceStates.remove(Long.valueOf(guild.r()));
        this.dirtyGuildIds.add(Long.valueOf(guild.r()));
        markChanged();
    }

    @StoreThread
    public final void handleVoiceStateUpdate(VoiceState voiceState) {
        m.checkNotNullParameter(voiceState, "voiceState");
        updateVoiceState$default(this, voiceState, null, 2, null);
    }

    public final Observable<Map<Long, VoiceState>> observe(long j) {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreVoiceStates$observe$1(this, j), 14, null);
    }

    public final Observable<Map<Long, VoiceState>> observeForPrivateChannels(long j) {
        return observe(0L, j);
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        HashMap hashMap = new HashMap(this.voiceStates.size());
        for (Map.Entry<Long, HashMap<Long, VoiceState>> entry : this.voiceStates.entrySet()) {
            long longValue = entry.getKey().longValue();
            HashMap<Long, VoiceState> value = entry.getValue();
            if (this.dirtyGuildIds.contains(Long.valueOf(longValue))) {
                hashMap.put(Long.valueOf(longValue), new HashMap(value));
            } else {
                Map<Long, VoiceState> map = this.voiceStatesSnapshot.get(Long.valueOf(longValue));
                if (map != null) {
                    hashMap.put(Long.valueOf(longValue), map);
                }
            }
        }
        this.voiceStatesSnapshot = hashMap;
        this.dirtyGuildIds.clear();
    }

    public final Observable<Map<Long, VoiceState>> observe(long j, final long j2) {
        Observable<Map<Long, VoiceState>> q = observe(j).Y(new b<Map<Long, ? extends VoiceState>, Observable<? extends Map<Long, VoiceState>>>() { // from class: com.discord.stores.StoreVoiceStates$observe$2

            /* compiled from: StoreVoiceStates.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/api/voice/state/VoiceState;", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/api/voice/state/VoiceState;)Ljava/lang/Long;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.stores.StoreVoiceStates$observe$2$2  reason: invalid class name */
            /* loaded from: classes.dex */
            public static final class AnonymousClass2<T, R> implements b<VoiceState, Long> {
                public static final AnonymousClass2 INSTANCE = new AnonymousClass2();

                public final Long call(VoiceState voiceState) {
                    return Long.valueOf(voiceState.m());
                }
            }

            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Observable<? extends Map<Long, VoiceState>> call(Map<Long, ? extends VoiceState> map) {
                return call2((Map<Long, VoiceState>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Observable<? extends Map<Long, VoiceState>> call2(Map<Long, VoiceState> map) {
                return Observable.h0(new l0(Observable.A(map.values()).x(new b<VoiceState, Boolean>() { // from class: com.discord.stores.StoreVoiceStates$observe$2.1
                    public final Boolean call(VoiceState voiceState) {
                        Long a = voiceState.a();
                        return Boolean.valueOf(a != null && a.longValue() == j2);
                    }
                }), AnonymousClass2.INSTANCE, m.a.INSTANCE));
            }
        }).q();
        d0.z.d.m.checkNotNullExpressionValue(q, "observe(guildId)\n       …  .distinctUntilChanged()");
        return q;
    }
}
