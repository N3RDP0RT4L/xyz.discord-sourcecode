package com.discord.stores;

import com.discord.stores.StoreSearch;
import com.discord.utilities.search.network.SearchQuery;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreSearchQuery.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/search/network/SearchQuery;", "searchQuery", "", "invoke", "(Lcom/discord/utilities/search/network/SearchQuery;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreSearchQuery$parseAndQuery$4 extends o implements Function1<SearchQuery, Unit> {
    public final /* synthetic */ StoreSearch.SearchTarget $searchTarget;
    public final /* synthetic */ StoreSearchQuery this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreSearchQuery$parseAndQuery$4(StoreSearchQuery storeSearchQuery, StoreSearch.SearchTarget searchTarget) {
        super(1);
        this.this$0 = storeSearchQuery;
        this.$searchTarget = searchTarget;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(SearchQuery searchQuery) {
        invoke2(searchQuery);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(SearchQuery searchQuery) {
        if (searchQuery == null) {
            this.this$0.handleError();
        } else {
            this.this$0.performInitialLoad(this.$searchTarget, searchQuery);
        }
    }
}
