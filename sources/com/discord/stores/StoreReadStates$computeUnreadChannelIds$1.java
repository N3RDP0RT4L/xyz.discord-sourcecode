package com.discord.stores;

import com.discord.api.channel.Channel;
import com.discord.models.domain.ModelNotificationSettings;
import com.discord.stores.StoreMessageAck;
import com.discord.stores.StoreThreadsActiveJoined;
import d0.z.d.k;
import d0.z.d.m;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.functions.Function7;
/* compiled from: StoreReadStates.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\b\u0003\u0010\u0016\u001a\"\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0001j\u0002`\u00020\u0013\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0001j\u0002`\u00070\u00130\u00122\u001a\u0010\u0004\u001a\u0016\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\b\u0012\u00060\u0001j\u0002`\u00030\u00002\u0016\u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u00050\u00002\u001a\u0010\t\u001a\u0016\u0012\b\u0012\u00060\u0001j\u0002`\u0007\u0012\b\u0012\u00060\u0001j\u0002`\b0\u00002\u0016\u0010\u000b\u001a\u0012\u0012\b\u0012\u00060\u0001j\u0002`\u0007\u0012\u0004\u0012\u00020\n0\u00002\u0016\u0010\r\u001a\u0012\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\f0\u00002\u001a\u0010\u000f\u001a\u0016\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\b\u0012\u00060\u0001j\u0002`\u000e0\u00002\u0016\u0010\u0011\u001a\u0012\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u00100\u0000¢\u0006\u0004\b\u0014\u0010\u0015"}, d2 = {"", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/permission/PermissionBit;", "p1", "Lcom/discord/api/channel/Channel;", "p2", "Lcom/discord/primitives/GuildId;", "Lcom/discord/primitives/Timestamp;", "p3", "Lcom/discord/models/domain/ModelNotificationSettings;", "p4", "Lcom/discord/stores/StoreMessageAck$Ack;", "p5", "Lcom/discord/primitives/MessageId;", "p6", "Lcom/discord/stores/StoreThreadsActiveJoined$ActiveJoinedThread;", "p7", "Lkotlin/Pair;", "", "invoke", "(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Lkotlin/Pair;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final /* synthetic */ class StoreReadStates$computeUnreadChannelIds$1 extends k implements Function7<Map<Long, ? extends Long>, Map<Long, ? extends Channel>, Map<Long, ? extends Long>, Map<Long, ? extends ModelNotificationSettings>, Map<Long, ? extends StoreMessageAck.Ack>, Map<Long, ? extends Long>, Map<Long, ? extends StoreThreadsActiveJoined.ActiveJoinedThread>, Pair<? extends Set<? extends Long>, ? extends Set<? extends Long>>> {
    public StoreReadStates$computeUnreadChannelIds$1(StoreReadStates storeReadStates) {
        super(7, storeReadStates, StoreReadStates.class, "computeUnreadIds", "computeUnreadIds(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Lkotlin/Pair;", 0);
    }

    @Override // kotlin.jvm.functions.Function7
    public /* bridge */ /* synthetic */ Pair<? extends Set<? extends Long>, ? extends Set<? extends Long>> invoke(Map<Long, ? extends Long> map, Map<Long, ? extends Channel> map2, Map<Long, ? extends Long> map3, Map<Long, ? extends ModelNotificationSettings> map4, Map<Long, ? extends StoreMessageAck.Ack> map5, Map<Long, ? extends Long> map6, Map<Long, ? extends StoreThreadsActiveJoined.ActiveJoinedThread> map7) {
        return invoke2((Map<Long, Long>) map, (Map<Long, Channel>) map2, (Map<Long, Long>) map3, map4, (Map<Long, StoreMessageAck.Ack>) map5, (Map<Long, Long>) map6, (Map<Long, StoreThreadsActiveJoined.ActiveJoinedThread>) map7);
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final Pair<Set<Long>, Set<Long>> invoke2(Map<Long, Long> map, Map<Long, Channel> map2, Map<Long, Long> map3, Map<Long, ? extends ModelNotificationSettings> map4, Map<Long, StoreMessageAck.Ack> map5, Map<Long, Long> map6, Map<Long, StoreThreadsActiveJoined.ActiveJoinedThread> map7) {
        Pair<Set<Long>, Set<Long>> computeUnreadIds;
        m.checkNotNullParameter(map, "p1");
        m.checkNotNullParameter(map2, "p2");
        m.checkNotNullParameter(map3, "p3");
        m.checkNotNullParameter(map4, "p4");
        m.checkNotNullParameter(map5, "p5");
        m.checkNotNullParameter(map6, "p6");
        m.checkNotNullParameter(map7, "p7");
        computeUnreadIds = ((StoreReadStates) this.receiver).computeUnreadIds(map, map2, map3, map4, map5, map6, map7);
        return computeUnreadIds;
    }
}
