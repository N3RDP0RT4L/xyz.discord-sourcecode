package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.api.channel.Channel;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreNavigation;
import com.discord.utilities.SnowflakeUtils;
import com.discord.utilities.time.TimeUtils;
import kotlin.Metadata;
import rx.functions.Func3;
/* compiled from: StoreAuthentication.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\n\u001a\u0004\u0018\u00010\u00072\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\b\u0010\u0004\u001a\u0004\u0018\u00010\u00032\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\n¢\u0006\u0004\b\b\u0010\t"}, d2 = {"Lcom/discord/models/user/MeUser;", "kotlin.jvm.PlatformType", "me", "", "ageGateError", "Lcom/discord/api/channel/Channel;", "channel", "Lcom/discord/stores/StoreNavigation$AgeGate;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/user/MeUser;Ljava/lang/String;Lcom/discord/api/channel/Channel;)Lcom/discord/stores/StoreNavigation$AgeGate;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAuthentication$getShouldShowAgeGate$1<T1, T2, T3, R> implements Func3<MeUser, String, Channel, StoreNavigation.AgeGate> {
    public static final StoreAuthentication$getShouldShowAgeGate$1 INSTANCE = new StoreAuthentication$getShouldShowAgeGate$1();

    public final StoreNavigation.AgeGate call(MeUser meUser, String str, Channel channel) {
        long parseUTCDate = TimeUtils.parseUTCDate("2021-02-05T12:00:00+0000");
        if (!meUser.getHasBirthday() && (meUser.getId() >>> 22) + SnowflakeUtils.DISCORD_EPOCH > parseUTCDate) {
            return StoreNavigation.AgeGate.REGISTER_AGE_GATE;
        }
        if (str != null || meUser.getHasBirthday() || channel == null || !channel.o()) {
            return null;
        }
        return StoreNavigation.AgeGate.NSFW_CHANNEL_AGE_GATE;
    }
}
