package com.discord.stores;

import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreStream.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\n\u0018\u00010\u0000j\u0004\u0018\u0001`\u0001H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"", "Lcom/discord/primitives/FingerPrint;", "invoke", "()Ljava/lang/String;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStream$Companion$initialize$2 extends o implements Function0<String> {
    public static final StoreStream$Companion$initialize$2 INSTANCE = new StoreStream$Companion$initialize$2();

    public StoreStream$Companion$initialize$2() {
        super(0);
    }

    @Override // kotlin.jvm.functions.Function0
    public final String invoke() {
        StoreStream collector;
        collector = StoreStream.Companion.getCollector();
        return collector.getAuthentication$app_productionGoogleRelease().getFingerprint$app_productionGoogleRelease();
    }
}
