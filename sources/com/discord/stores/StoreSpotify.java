package com.discord.stores;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.Context;
import b.d.b.a.a;
import com.discord.api.activity.ActivityType;
import com.discord.api.connectedaccounts.ConnectedAccount;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelPayload;
import com.discord.models.domain.spotify.ModelSpotifyAlbum;
import com.discord.models.domain.spotify.ModelSpotifyTrack;
import com.discord.stores.StoreUserConnections;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.integrations.SpotifyHelper;
import com.discord.utilities.platform.Platform;
import com.discord.utilities.presence.ActivityUtilsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.spotify.SpotifyApiClient;
import com.discord.utilities.time.Clock;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel;
import d0.g0.t;
import d0.g0.w;
import d0.t.u;
import d0.z.d.m;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Subscription;
import rx.subjects.BehaviorSubject;
/* compiled from: StoreSpotify.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0084\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0001<B\u001f\u0012\u0006\u00108\u001a\u000207\u0012\u0006\u00105\u001a\u000204\u0012\u0006\u0010%\u001a\u00020$¢\u0006\u0004\b:\u0010;J\u000f\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\u000b\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\t¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u000e\u001a\u00020\rH\u0007¢\u0006\u0004\b\u000f\u0010\u0010J\u000f\u0010\u0011\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0011\u0010\u0004J\u0017\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u0012H\u0007¢\u0006\u0004\b\u0014\u0010\u0015J\u001d\u0010\u0019\u001a\u00020\u00022\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00170\u0016H\u0007¢\u0006\u0004\b\u0019\u0010\u001aJ\u001d\u0010\u001e\u001a\u00020\u00022\u0006\u0010\u001b\u001a\u00020\r2\u0006\u0010\u001d\u001a\u00020\u001c¢\u0006\u0004\b\u001e\u0010\u001fJ\u0015\u0010\"\u001a\u00020\u00022\u0006\u0010!\u001a\u00020 ¢\u0006\u0004\b\"\u0010#R\u0016\u0010%\u001a\u00020$8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b%\u0010&R\u0016\u0010(\u001a\u00020'8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u0010)R\u0018\u0010+\u001a\u0004\u0018\u00010*8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b+\u0010,R:\u0010/\u001a&\u0012\f\u0012\n .*\u0004\u0018\u00010\u00020\u0002 .*\u0012\u0012\f\u0012\n .*\u0004\u0018\u00010\u00020\u0002\u0018\u00010-0-8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b/\u00100R\u0018\u00102\u001a\u0004\u0018\u0001018\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b2\u00103R\u0016\u00105\u001a\u0002048\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b5\u00106R\u0016\u00108\u001a\u0002078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b8\u00109¨\u0006="}, d2 = {"Lcom/discord/stores/StoreSpotify;", "", "", "publishState", "()V", "", "timeLeftMs", "startStateExpiration", "(J)V", "Landroid/content/Context;", "context", "init", "(Landroid/content/Context;)V", "", "ready", "handleConnectionReady", "(Z)V", "handlePreLogout", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "", "Lcom/discord/api/connectedaccounts/ConnectedAccount;", "accounts", "handleUserConnections", "(Ljava/util/List;)V", "playing", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "setPlayingStatus", "(ZI)V", "", "trackId", "setCurrentTrackId", "(Ljava/lang/String;)V", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "Lcom/discord/utilities/spotify/SpotifyApiClient;", "spotifyApiClient", "Lcom/discord/utilities/spotify/SpotifyApiClient;", "Lrx/Subscription;", "expireStateSub", "Lrx/Subscription;", "Lrx/subjects/BehaviorSubject;", "kotlin.jvm.PlatformType", "publishStateTrigger", "Lrx/subjects/BehaviorSubject;", "Lcom/discord/stores/StoreSpotify$SpotifyState;", "spotifyState", "Lcom/discord/stores/StoreSpotify$SpotifyState;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/stores/StoreStream;", "stream", "Lcom/discord/stores/StoreStream;", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;)V", "SpotifyState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreSpotify {
    private final Clock clock;
    private final Dispatcher dispatcher;
    private Subscription expireStateSub;
    private final BehaviorSubject<Unit> publishStateTrigger = BehaviorSubject.k0();
    private final SpotifyApiClient spotifyApiClient;
    private SpotifyState spotifyState;
    private final StoreStream stream;

    /* compiled from: StoreSpotify.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0011\b\u0086\b\u0018\u00002\u00020\u0001B1\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u0002\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0010\u001a\u00020\b\u0012\b\b\u0002\u0010\u0011\u001a\u00020\u000b¢\u0006\u0004\b#\u0010$J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ:\u0010\u0012\u001a\u00020\u00002\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\u000f\u001a\u00020\u00052\b\b\u0002\u0010\u0010\u001a\u00020\b2\b\b\u0002\u0010\u0011\u001a\u00020\u000bHÆ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u0017\u0010\nJ\u001a\u0010\u0019\u001a\u00020\u00052\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0019\u0010\u001aR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001b\u001a\u0004\b\u001c\u0010\u0004R\u0019\u0010\u0011\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001d\u001a\u0004\b\u001e\u0010\rR\u0019\u0010\u000f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001f\u001a\u0004\b \u0010\u0007R\u0019\u0010\u0010\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010!\u001a\u0004\b\"\u0010\n¨\u0006%"}, d2 = {"Lcom/discord/stores/StoreSpotify$SpotifyState;", "", "Lcom/discord/models/domain/spotify/ModelSpotifyTrack;", "component1", "()Lcom/discord/models/domain/spotify/ModelSpotifyTrack;", "", "component2", "()Z", "", "component3", "()I", "", "component4", "()J", "track", "playing", ModelAuditLogEntry.CHANGE_KEY_POSITION, "start", "copy", "(Lcom/discord/models/domain/spotify/ModelSpotifyTrack;ZIJ)Lcom/discord/stores/StoreSpotify$SpotifyState;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/spotify/ModelSpotifyTrack;", "getTrack", "J", "getStart", "Z", "getPlaying", "I", "getPosition", HookHelper.constructorName, "(Lcom/discord/models/domain/spotify/ModelSpotifyTrack;ZIJ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class SpotifyState {
        private final boolean playing;
        private final int position;
        private final long start;
        private final ModelSpotifyTrack track;

        public SpotifyState() {
            this(null, false, 0, 0L, 15, null);
        }

        public SpotifyState(ModelSpotifyTrack modelSpotifyTrack, boolean z2, int i, long j) {
            this.track = modelSpotifyTrack;
            this.playing = z2;
            this.position = i;
            this.start = j;
        }

        public static /* synthetic */ SpotifyState copy$default(SpotifyState spotifyState, ModelSpotifyTrack modelSpotifyTrack, boolean z2, int i, long j, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                modelSpotifyTrack = spotifyState.track;
            }
            if ((i2 & 2) != 0) {
                z2 = spotifyState.playing;
            }
            boolean z3 = z2;
            if ((i2 & 4) != 0) {
                i = spotifyState.position;
            }
            int i3 = i;
            if ((i2 & 8) != 0) {
                j = spotifyState.start;
            }
            return spotifyState.copy(modelSpotifyTrack, z3, i3, j);
        }

        public final ModelSpotifyTrack component1() {
            return this.track;
        }

        public final boolean component2() {
            return this.playing;
        }

        public final int component3() {
            return this.position;
        }

        public final long component4() {
            return this.start;
        }

        public final SpotifyState copy(ModelSpotifyTrack modelSpotifyTrack, boolean z2, int i, long j) {
            return new SpotifyState(modelSpotifyTrack, z2, i, j);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof SpotifyState)) {
                return false;
            }
            SpotifyState spotifyState = (SpotifyState) obj;
            return m.areEqual(this.track, spotifyState.track) && this.playing == spotifyState.playing && this.position == spotifyState.position && this.start == spotifyState.start;
        }

        public final boolean getPlaying() {
            return this.playing;
        }

        public final int getPosition() {
            return this.position;
        }

        public final long getStart() {
            return this.start;
        }

        public final ModelSpotifyTrack getTrack() {
            return this.track;
        }

        public int hashCode() {
            ModelSpotifyTrack modelSpotifyTrack = this.track;
            int hashCode = (modelSpotifyTrack != null ? modelSpotifyTrack.hashCode() : 0) * 31;
            boolean z2 = this.playing;
            if (z2) {
                z2 = true;
            }
            int i = z2 ? 1 : 0;
            int i2 = z2 ? 1 : 0;
            return b.a(this.start) + ((((hashCode + i) * 31) + this.position) * 31);
        }

        public String toString() {
            StringBuilder R = a.R("SpotifyState(track=");
            R.append(this.track);
            R.append(", playing=");
            R.append(this.playing);
            R.append(", position=");
            R.append(this.position);
            R.append(", start=");
            return a.B(R, this.start, ")");
        }

        public /* synthetic */ SpotifyState(ModelSpotifyTrack modelSpotifyTrack, boolean z2, int i, long j, int i2, DefaultConstructorMarker defaultConstructorMarker) {
            this((i2 & 1) != 0 ? null : modelSpotifyTrack, (i2 & 2) != 0 ? false : z2, (i2 & 4) == 0 ? i : 0, (i2 & 8) != 0 ? 0L : j);
        }
    }

    public StoreSpotify(StoreStream storeStream, Dispatcher dispatcher, Clock clock) {
        m.checkNotNullParameter(storeStream, "stream");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(clock, "clock");
        this.stream = storeStream;
        this.dispatcher = dispatcher;
        this.clock = clock;
        this.spotifyApiClient = new SpotifyApiClient(clock);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void publishState() {
        boolean z2;
        Platform platform;
        List<ModelSpotifyAlbum.AlbumImage> images;
        List<ModelSpotifyAlbum.AlbumImage> images2;
        ModelSpotifyAlbum.AlbumImage albumImage;
        String url;
        List split$default;
        boolean z3;
        String str = null;
        boolean z4 = true;
        if (this.spotifyState == null) {
            this.stream.getPresences$app_productionGoogleRelease().updateActivity(ActivityType.LISTENING, null, true);
            return;
        }
        StoreUserConnections.State connectedAccounts = this.stream.getUserConnections$app_productionGoogleRelease().getConnectedAccounts();
        if (!(connectedAccounts instanceof Collection) || !connectedAccounts.isEmpty()) {
            for (ConnectedAccount connectedAccount : connectedAccounts) {
                if (!m.areEqual(connectedAccount.g(), Platform.SPOTIFY.getPlatformId()) || !connectedAccount.f()) {
                    z3 = false;
                    continue;
                } else {
                    z3 = true;
                    continue;
                }
                if (z3) {
                    z2 = true;
                    break;
                }
            }
        }
        z2 = false;
        if (z2) {
            SpotifyState spotifyState = this.spotifyState;
            m.checkNotNull(spotifyState);
            ModelSpotifyTrack component1 = spotifyState.component1();
            boolean component2 = spotifyState.component2();
            long component4 = spotifyState.component4();
            if (!component2 || component1 == null) {
                this.stream.getPresences$app_productionGoogleRelease().updateActivity(ActivityType.LISTENING, null, true);
                return;
            }
            ModelSpotifyAlbum album = component1.getAlbum();
            String str2 = (album == null || (images2 = album.getImages()) == null || (albumImage = (ModelSpotifyAlbum.AlbumImage) u.firstOrNull((List<? extends Object>) images2)) == null || (url = albumImage.getUrl()) == null || (split$default = w.split$default((CharSequence) url, new String[]{AutocompleteViewModel.COMMAND_DISCOVER_TOKEN}, false, 0, 6, (Object) null)) == null) ? null : (String) u.last((List<? extends Object>) split$default);
            String str3 = str2 != null ? Platform.SPOTIFY.getPlatformId() + MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR + str2 : null;
            long currentTimeMillis = this.clock.currentTimeMillis();
            String properName = Platform.SPOTIFY.getProperName();
            String name = component1.getName();
            String id2 = component1.getId();
            ModelSpotifyAlbum album2 = component1.getAlbum();
            if (album2 != null) {
                str = album2.getName();
            }
            this.stream.getPresences$app_productionGoogleRelease().updateActivity(ActivityType.LISTENING, ActivityUtilsKt.createSpotifyListeningActivity(currentTimeMillis, properName, name, id2, str, str3, u.joinToString$default(component1.getArtists(), null, null, null, 0, null, StoreSpotify$publishState$activity$1.INSTANCE, 31, null), component4, component1.getDurationMs() + component4, platform.getPlatformId() + MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR + this.stream.getUsers$app_productionGoogleRelease().getMeInternal$app_productionGoogleRelease().getId()), true);
            AnalyticsTracker analyticsTracker = AnalyticsTracker.INSTANCE;
            String id3 = component1.getId();
            ModelSpotifyAlbum album3 = component1.getAlbum();
            if (album3 == null || (images = album3.getImages()) == null || images.isEmpty()) {
                z4 = false;
            }
            analyticsTracker.activityUpdatedSpotify(id3, z4);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void startStateExpiration(long j) {
        Observable<Long> d02 = Observable.d0(j + 5000, TimeUnit.MILLISECONDS);
        m.checkNotNullExpressionValue(d02, "Observable\n        .time…), TimeUnit.MILLISECONDS)");
        ObservableExtensionsKt.appSubscribe(d02, StoreSpotify.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new StoreSpotify$startStateExpiration$1(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreSpotify$startStateExpiration$2(this));
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        List<ConnectedAccount> connectedAccounts = modelPayload.getConnectedAccounts();
        m.checkNotNullExpressionValue(connectedAccounts, "payload.connectedAccounts");
        handleUserConnections(connectedAccounts);
        this.publishStateTrigger.onNext(Unit.a);
    }

    @StoreThread
    public final void handleConnectionReady(boolean z2) {
        if (z2) {
            this.publishStateTrigger.onNext(Unit.a);
        }
    }

    @StoreThread
    public final void handlePreLogout() {
        this.spotifyState = null;
    }

    @StoreThread
    public final void handleUserConnections(List<ConnectedAccount> list) {
        String str;
        Object obj;
        m.checkNotNullParameter(list, "accounts");
        Iterator<T> it = list.iterator();
        while (true) {
            str = null;
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (t.equals(Platform.SPOTIFY.name(), ((ConnectedAccount) obj).g(), true)) {
                break;
            }
        }
        ConnectedAccount connectedAccount = (ConnectedAccount) obj;
        SpotifyApiClient spotifyApiClient = this.spotifyApiClient;
        if (connectedAccount != null) {
            str = connectedAccount.b();
        }
        spotifyApiClient.setSpotifyAccountId(str);
    }

    public final void init(Context context) {
        m.checkNotNullParameter(context, "context");
        SpotifyHelper.registerSpotifyBroadcastReceivers(context);
        Observable o = Observable.j(this.publishStateTrigger, this.spotifyApiClient.getSpotifyTrack().Y(StoreSpotify$init$1.INSTANCE).q(), StoreSpotify$init$2.INSTANCE).o(2L, TimeUnit.SECONDS);
        m.checkNotNullExpressionValue(o, "Observable.combineLatest…unce(2, TimeUnit.SECONDS)");
        ObservableExtensionsKt.appSubscribe(o, StoreSpotify.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreSpotify$init$3(this));
    }

    public final void setCurrentTrackId(String str) {
        m.checkNotNullParameter(str, "trackId");
        this.spotifyApiClient.fetchSpotifyTrack(str);
    }

    public final void setPlayingStatus(boolean z2, int i) {
        this.dispatcher.schedule(new StoreSpotify$setPlayingStatus$1(this, z2, i));
    }
}
