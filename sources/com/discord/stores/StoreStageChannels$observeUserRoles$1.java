package com.discord.stores;

import com.discord.api.channel.Channel;
import com.discord.widgets.stage.StageRoles;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreStageChannels.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/widgets/stage/StageRoles;", "invoke", "()Lcom/discord/widgets/stage/StageRoles;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStageChannels$observeUserRoles$1 extends o implements Function0<StageRoles> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ long $userId;
    public final /* synthetic */ StoreStageChannels this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreStageChannels$observeUserRoles$1(StoreStageChannels storeStageChannels, long j, long j2) {
        super(0);
        this.this$0 = storeStageChannels;
        this.$channelId = j;
        this.$userId = j2;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final StageRoles invoke() {
        StoreChannels storeChannels;
        storeChannels = this.this$0.channelsStore;
        Channel channel = storeChannels.getChannel(this.$channelId);
        if (channel != null) {
            return this.this$0.m12getUserRolesuOBN1zc(this.$userId, channel.h());
        }
        return null;
    }
}
