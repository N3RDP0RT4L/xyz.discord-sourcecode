package com.discord.stores;

import com.discord.analytics.generated.events.network_action.TrackNetworkActionUserConnectionsUpdate;
import com.discord.analytics.generated.traits.TrackNetworkMetadataReceiver;
import com.discord.api.connectedaccounts.ConnectedAccount;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreUserConnections.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/connectedaccounts/ConnectedAccount;", "it", "Lcom/discord/analytics/generated/traits/TrackNetworkMetadataReceiver;", "invoke", "(Lcom/discord/api/connectedaccounts/ConnectedAccount;)Lcom/discord/analytics/generated/traits/TrackNetworkMetadataReceiver;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserConnections$updateUserConnection$1 extends o implements Function1<ConnectedAccount, TrackNetworkMetadataReceiver> {
    public final /* synthetic */ ConnectedAccount $connectedAccount;
    public final /* synthetic */ boolean $syncFriends;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreUserConnections$updateUserConnection$1(ConnectedAccount connectedAccount, boolean z2) {
        super(1);
        this.$connectedAccount = connectedAccount;
        this.$syncFriends = z2;
    }

    public final TrackNetworkMetadataReceiver invoke(ConnectedAccount connectedAccount) {
        return new TrackNetworkActionUserConnectionsUpdate(this.$connectedAccount.d(), Boolean.valueOf(this.$syncFriends), null, null, 12);
    }
}
