package com.discord.stores;

import com.discord.api.application.Application;
import d0.z.d.o;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreApplication.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/api/application/Application;", "invoke", "()Lcom/discord/api/application/Application;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreApplication$observeApplication$1 extends o implements Function0<Application> {
    public final /* synthetic */ Long $appId;
    public final /* synthetic */ StoreApplication this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreApplication$observeApplication$1(StoreApplication storeApplication, Long l) {
        super(0);
        this.this$0 = storeApplication;
        this.$appId = l;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final Application invoke() {
        HashMap hashMap;
        hashMap = this.this$0.applicationsSnapshot;
        return (Application) hashMap.get(this.$appId);
    }
}
