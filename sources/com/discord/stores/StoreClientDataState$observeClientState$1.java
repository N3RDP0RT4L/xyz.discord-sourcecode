package com.discord.stores;

import com.discord.stores.StoreClientDataState;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreClientDataState.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/stores/StoreClientDataState$ClientDataState;", "invoke", "()Lcom/discord/stores/StoreClientDataState$ClientDataState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreClientDataState$observeClientState$1 extends o implements Function0<StoreClientDataState.ClientDataState> {
    public final /* synthetic */ StoreClientDataState this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreClientDataState$observeClientState$1(StoreClientDataState storeClientDataState) {
        super(0);
        this.this$0 = storeClientDataState;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final StoreClientDataState.ClientDataState invoke() {
        StoreClientDataState.ClientDataState clientDataState;
        clientDataState = this.this$0.clientDataStateSnapshot;
        return clientDataState;
    }
}
