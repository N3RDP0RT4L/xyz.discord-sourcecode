package com.discord.stores;

import d0.z.d.o;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreUserTyping.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u001c\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0001j\u0002`\u00040\u00030\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "", "Lcom/discord/primitives/ChannelId;", "", "Lcom/discord/primitives/UserId;", "invoke", "()Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserTyping$observeTypingUsers$1 extends o implements Function0<Map<Long, ? extends Set<? extends Long>>> {
    public final /* synthetic */ StoreUserTyping this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreUserTyping$observeTypingUsers$1(StoreUserTyping storeUserTyping) {
        super(0);
        this.this$0 = storeUserTyping;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Map<Long, ? extends Set<? extends Long>> invoke() {
        Map<Long, ? extends Set<? extends Long>> typingUsers;
        typingUsers = this.this$0.getTypingUsers();
        return typingUsers;
    }
}
