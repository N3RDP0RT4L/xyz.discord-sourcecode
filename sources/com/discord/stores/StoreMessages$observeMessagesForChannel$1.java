package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.models.message.Message;
import d0.t.u;
import d0.z.d.m;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import rx.functions.Func3;
/* compiled from: StoreMessages.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0004\u0010\n\u001a\u001e\u0012\b\u0012\u00060\u0001j\u0002`\u0002 \u0003*\u000e\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0018\u00010\u00000\u00002\"\u0010\u0004\u001a\u001e\u0012\b\u0012\u00060\u0001j\u0002`\u0002 \u0003*\u000e\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0018\u00010\u00000\u00002\"\u0010\u0005\u001a\u001e\u0012\b\u0012\u00060\u0001j\u0002`\u0002 \u0003*\u000e\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0018\u00010\u00000\u00002\u000e\u0010\u0007\u001a\n \u0003*\u0004\u0018\u00010\u00060\u0006H\n¢\u0006\u0004\b\b\u0010\t"}, d2 = {"", "Lcom/discord/models/message/Message;", "Lcom/discord/stores/ClientMessage;", "kotlin.jvm.PlatformType", "messages", "localMessages", "", "isDetached", NotificationCompat.CATEGORY_CALL, "(Ljava/util/List;Ljava/util/List;Ljava/lang/Boolean;)Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessages$observeMessagesForChannel$1<T1, T2, T3, R> implements Func3<List<? extends Message>, List<? extends Message>, Boolean, List<? extends Message>> {
    public static final StoreMessages$observeMessagesForChannel$1 INSTANCE = new StoreMessages$observeMessagesForChannel$1();

    @Override // rx.functions.Func3
    public /* bridge */ /* synthetic */ List<? extends Message> call(List<? extends Message> list, List<? extends Message> list2, Boolean bool) {
        return call2((List<Message>) list, (List<Message>) list2, bool);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final List<Message> call2(List<Message> list, List<Message> list2, Boolean bool) {
        m.checkNotNullExpressionValue(bool, "isDetached");
        if (bool.booleanValue()) {
            return list;
        }
        m.checkNotNullExpressionValue(list, "messages");
        m.checkNotNullExpressionValue(list2, "localMessages");
        return u.plus((Collection) list, (Iterable) list2);
    }
}
