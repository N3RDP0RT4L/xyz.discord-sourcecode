package com.discord.stores;

import com.discord.models.domain.ModelReadState;
import d0.z.d.m;
import d0.z.d.o;
import java.util.HashSet;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreMentions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/ModelReadState;", "readState", "", "invoke", "(Lcom/discord/models/domain/ModelReadState;)Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMentions$handleConnectionOpen$3 extends o implements Function1<ModelReadState, Boolean> {
    public final /* synthetic */ HashSet $newReadStateChannelIds;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreMentions$handleConnectionOpen$3(HashSet hashSet) {
        super(1);
        this.$newReadStateChannelIds = hashSet;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Boolean invoke(ModelReadState modelReadState) {
        return Boolean.valueOf(invoke2(modelReadState));
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final boolean invoke2(ModelReadState modelReadState) {
        m.checkNotNullParameter(modelReadState, "readState");
        return this.$newReadStateChannelIds.contains(Long.valueOf(modelReadState.getChannelId()));
    }
}
