package com.discord.stores;

import com.discord.models.message.Message;
import com.discord.stores.StoreMessagesLoader;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.subjects.SerializedSubject;
/* compiled from: StoreMessagesLoader.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/message/Message;", "message", "", "invoke", "(Lcom/discord/models/message/Message;)V", "handleTargetChannelSelected"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessagesLoader$jumpToMessage$1 extends o implements Function1<Message, Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ long $messageId;
    public final /* synthetic */ StoreMessagesLoader this$0;

    /* compiled from: StoreMessagesLoader.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;", "it", "invoke", "(Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;)Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreMessagesLoader$jumpToMessage$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreMessagesLoader.ChannelLoadedState, StoreMessagesLoader.ChannelLoadedState> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public AnonymousClass1() {
            super(1);
        }

        public final StoreMessagesLoader.ChannelLoadedState invoke(StoreMessagesLoader.ChannelLoadedState channelLoadedState) {
            m.checkNotNullParameter(channelLoadedState, "it");
            return StoreMessagesLoader.ChannelLoadedState.copy$default(channelLoadedState, false, false, false, false, null, 29, null);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreMessagesLoader$jumpToMessage$1(StoreMessagesLoader storeMessagesLoader, long j, long j2) {
        super(1);
        this.this$0 = storeMessagesLoader;
        this.$channelId = j;
        this.$messageId = j2;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Message message) {
        invoke2(message);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final synchronized void invoke2(Message message) {
        SerializedSubject serializedSubject;
        if (message != null) {
            serializedSubject = this.this$0.scrollToSubject;
            serializedSubject.k.onNext(Long.valueOf(message.getId()));
        } else {
            this.this$0.channelLoadedStateUpdate(this.$channelId, AnonymousClass1.INSTANCE);
            StoreMessagesLoader.tryLoadMessages$default(this.this$0, 0L, true, false, false, Long.valueOf(this.$channelId), Long.valueOf(this.$messageId), 9, null);
        }
    }
}
