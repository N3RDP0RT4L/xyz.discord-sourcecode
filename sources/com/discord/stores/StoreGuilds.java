package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.Context;
import b.a.d.o;
import com.discord.api.channel.Channel;
import com.discord.api.guild.GuildVerificationLevel;
import com.discord.api.guildmember.GuildMember;
import com.discord.api.guildmember.GuildMembersChunk;
import com.discord.api.guildscheduledevent.ApiGuildScheduledEventUser;
import com.discord.api.role.GuildRole;
import com.discord.api.thread.AugmentedThreadMember;
import com.discord.api.thread.ThreadListMember;
import com.discord.api.thread.ThreadMemberListUpdate;
import com.discord.api.thread.ThreadMembersUpdate;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.app.AppComponent;
import com.discord.models.domain.ModelPayload;
import com.discord.models.guild.Guild;
import com.discord.models.user.MeUser;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.collections.CollectionExtensionsKt;
import com.discord.utilities.collections.SnowflakePartitionMap;
import com.discord.utilities.guildmember.GuildMemberUtilsKt;
import com.discord.utilities.guilds.GuildUtilsKt;
import com.discord.utilities.persister.Persister;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$3;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$4;
import d0.t.g0;
import d0.t.h0;
import d0.t.n0;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreGuilds.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000þ\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u001e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010#\n\u0002\b\u0006\u0018\u0000 ¨\u00012\u00020\u0001:\u0004©\u0001¨\u0001B)\u0012\b\u0010\u0099\u0001\u001a\u00030\u0098\u0001\u0012\b\u0010\u009d\u0001\u001a\u00030\u009c\u0001\u0012\n\b\u0002\u0010\u008f\u0001\u001a\u00030\u008e\u0001¢\u0006\u0006\b¦\u0001\u0010§\u0001J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J'\u0010\f\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\n\u0010\u000b\u001a\u00060\u0007j\u0002`\nH\u0003¢\u0006\u0004\b\f\u0010\rJ1\u0010\u0012\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\u00072\u0018\u0010\u0011\u001a\u0014\u0012\u0004\u0012\u00020\u0007\u0012\b\u0012\u00060\u000fj\u0002`\u0010\u0018\u00010\u000eH\u0003¢\u0006\u0004\b\u0012\u0010\u0013J#\u0010\u0019\u001a\u00020\u00042\n\u0010\u0016\u001a\u00060\u0014j\u0002`\u00152\u0006\u0010\u0018\u001a\u00020\u0017H\u0003¢\u0006\u0004\b\u0019\u0010\u001aJ'\u0010\u001c\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\u00072\u0006\u0010\u001b\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0017H\u0003¢\u0006\u0004\b\u001c\u0010\u001dJ7\u0010\"\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\u00072\u0006\u0010\u001e\u001a\u00020\u00072\u000e\u0010!\u001a\n\u0012\u0004\u0012\u00020 \u0018\u00010\u001f2\u0006\u0010\u0018\u001a\u00020\u0017H\u0003¢\u0006\u0004\b\"\u0010#J1\u0010$\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\u00072\u0018\u0010\u0011\u001a\u0014\u0012\u0004\u0012\u00020\u0007\u0012\b\u0012\u00060\u000fj\u0002`\u0010\u0018\u00010\u000eH\u0003¢\u0006\u0004\b$\u0010\u0013J;\u0010'\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\u00072\u0006\u0010%\u001a\u00020\u00072\u0012\u0010\u0011\u001a\u000e\u0012\b\u0012\u00060\u000fj\u0002`\u0010\u0018\u00010&2\u0006\u0010\u0018\u001a\u00020\u0017H\u0003¢\u0006\u0004\b'\u0010(J\u001d\u0010*\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020)0\u000e¢\u0006\u0004\b*\u0010+J\u001b\u0010,\u001a\u0004\u0018\u00010)2\n\u0010\t\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\b,\u0010-J-\u0010/\u001a\"\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0007j\u0002`.\u0012\u0004\u0012\u00020 0\u000e0\u000e¢\u0006\u0004\b/\u0010+J1\u00102\u001a&\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0007j\u0002`\n\u0012\b\u0012\u000600j\u0002`10\u000e0\u000e¢\u0006\u0004\b2\u0010+J\u0017\u00104\u001a\f\u0012\b\u0012\u00060\u0007j\u0002`\b03¢\u0006\u0004\b4\u00105J!\u00107\u001a\u0016\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\b\u0012\u00060\u0007j\u0002`60\u000e¢\u0006\u0004\b7\u0010+J-\u00108\u001a\n\u0018\u000100j\u0004\u0018\u0001`12\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\n\u0010\u000b\u001a\u00060\u0007j\u0002`\n¢\u0006\u0004\b8\u00109J\u001f\u0010;\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020)0\u000eH\u0001¢\u0006\u0004\b:\u0010+J/\u0010=\u001a\"\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0007j\u0002`.\u0012\u0004\u0012\u00020 0\u000e0\u000eH\u0001¢\u0006\u0004\b<\u0010+J3\u0010?\u001a&\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0007j\u0002`\n\u0012\b\u0012\u000600j\u0002`10\u000e0\u000eH\u0001¢\u0006\u0004\b>\u0010+J\u0019\u0010A\u001a\f\u0012\b\u0012\u00060\u0007j\u0002`\b03H\u0001¢\u0006\u0004\b@\u00105J#\u0010C\u001a\u0016\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\b\u0012\u00060\u0007j\u0002`60\u000eH\u0001¢\u0006\u0004\bB\u0010+J\u0017\u0010E\u001a\u00020\u00042\u0006\u0010D\u001a\u00020\u0002H\u0016¢\u0006\u0004\bE\u0010\u0006J#\u0010G\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020)0\u000e0F¢\u0006\u0004\bG\u0010HJ\u001d\u0010I\u001a\u0012\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0007j\u0002`\b030F¢\u0006\u0004\bI\u0010HJ!\u0010J\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010)0F2\n\u0010\t\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\bJ\u0010KJ!\u0010N\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010)0F2\n\u0010M\u001a\u00060\u0007j\u0002`L¢\u0006\u0004\bN\u0010KJ\u001f\u0010P\u001a\b\u0012\u0004\u0012\u00020O0F2\n\u0010\t\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\bP\u0010KJ7\u0010Q\u001a,\u0012(\u0012&\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0007j\u0002`\n\u0012\b\u0012\u000600j\u0002`10\u000e0\u000e0F¢\u0006\u0004\bQ\u0010HJ3\u0010Q\u001a\u001c\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0007j\u0002`\n\u0012\b\u0012\u000600j\u0002`10\u000e0F2\n\u0010\t\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\bQ\u0010KJE\u0010Q\u001a\u001c\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0007j\u0002`\n\u0012\b\u0012\u000600j\u0002`10\u000e0F2\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\u0010\u0010R\u001a\f\u0012\b\u0012\u00060\u0007j\u0002`\n0&¢\u0006\u0004\bQ\u0010SJ3\u0010T\u001a\u0010\u0012\f\u0012\n\u0018\u000100j\u0004\u0018\u0001`10F2\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\n\u0010\u000b\u001a\u00060\u0007j\u0002`\n¢\u0006\u0004\bT\u0010UJ3\u0010V\u001a(\u0012$\u0012\"\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0007j\u0002`.\u0012\u0004\u0012\u00020 0\u000e0\u000e0F¢\u0006\u0004\bV\u0010HJ/\u0010V\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0007j\u0002`.\u0012\u0004\u0012\u00020 0\u000e0F2\n\u0010\t\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\bV\u0010KJ%\u0010W\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020 0\u001f0F2\n\u0010\t\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\bW\u0010KJA\u0010V\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0007j\u0002`.\u0012\u0004\u0012\u00020 0\u000e0F2\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\u0010\u0010X\u001a\f\u0012\b\u0012\u00060\u0007j\u0002`.0&¢\u0006\u0004\bV\u0010SJ'\u0010Y\u001a\u001c\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\b\u0012\u00060\u0007j\u0002`60\u000e0F¢\u0006\u0004\bY\u0010HJ#\u0010Y\u001a\f\u0012\b\u0012\u00060\u0007j\u0002`60F2\n\u0010\t\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\bY\u0010KJ\u001d\u0010Z\u001a\u0012\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0007j\u0002`\b030F¢\u0006\u0004\bZ\u0010HJ\u008d\u0001\u0010\\\u001av\u00124\u00122\u0012\b\u0012\u00060\u0007j\u0002`\n\u0012\b\u0012\u000600j\u0002`1 [*\u0018\u0012\b\u0012\u00060\u0007j\u0002`\n\u0012\b\u0012\u000600j\u0002`1\u0018\u00010\u000e0\u000e [*:\u00124\u00122\u0012\b\u0012\u00060\u0007j\u0002`\n\u0012\b\u0012\u000600j\u0002`1 [*\u0018\u0012\b\u0012\u00060\u0007j\u0002`\n\u0012\b\u0012\u000600j\u0002`1\u0018\u00010\u000e0\u000e\u0018\u00010F0F2\n\u0010\t\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\b\\\u0010KJI\u0010]\u001a&\u0012\f\u0012\n\u0018\u000100j\u0004\u0018\u0001`1 [*\u0012\u0012\f\u0012\n\u0018\u000100j\u0004\u0018\u0001`1\u0018\u00010F0F2\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\n\u0010\u000b\u001a\u00060\u0007j\u0002`\n¢\u0006\u0004\b]\u0010UJ\u001d\u0010^\u001a\u0012\u0012\u000e\u0012\f\u0012\b\u0012\u000600j\u0002`10\u001f0F¢\u0006\u0004\b^\u0010HJ%\u0010_\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\n\u0010\u000b\u001a\u00060\u0007j\u0002`\n¢\u0006\u0004\b_\u0010\rJ\u0017\u0010b\u001a\u00020\u00042\u0006\u0010a\u001a\u00020`H\u0007¢\u0006\u0004\bb\u0010cJ\u0017\u0010d\u001a\u00020\u00042\u0006\u0010\u0016\u001a\u00020\u0014H\u0007¢\u0006\u0004\bd\u0010eJ#\u0010g\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\u0006\u0010f\u001a\u00020 H\u0007¢\u0006\u0004\bg\u0010hJ'\u0010j\u001a\u00020\u00042\n\u0010i\u001a\u00060\u0007j\u0002`.2\n\u0010\t\u001a\u00060\u0007j\u0002`\bH\u0007¢\u0006\u0004\bj\u0010\rJ\u001b\u0010k\u001a\u00020\u00042\n\u0010\u0016\u001a\u00060\u0014j\u0002`\u0015H\u0007¢\u0006\u0004\bk\u0010eJ\u001b\u0010m\u001a\u00020\u00042\n\u0010l\u001a\u00060\u000fj\u0002`\u0010H\u0007¢\u0006\u0004\bm\u0010nJ\u0017\u0010q\u001a\u00020\u00042\u0006\u0010p\u001a\u00020oH\u0007¢\u0006\u0004\bq\u0010rJ\u0017\u0010u\u001a\u00020\u00042\u0006\u0010t\u001a\u00020sH\u0007¢\u0006\u0004\bu\u0010vJ\u0017\u0010y\u001a\u00020\u00042\u0006\u0010x\u001a\u00020wH\u0007¢\u0006\u0004\by\u0010zJ)\u0010}\u001a\u00020\u00042\f\u0010|\u001a\b\u0012\u0004\u0012\u00020{0\u001f2\n\u0010\t\u001a\u00060\u0007j\u0002`\bH\u0007¢\u0006\u0004\b}\u0010~J'\u0010\u007f\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\n\u0010\u000b\u001a\u00060\u0007j\u0002`\nH\u0007¢\u0006\u0004\b\u007f\u0010\rJ5\u0010\u0081\u0001\u001a\u00020\u00042\n\u0010l\u001a\u00060\u000fj\u0002`\u00102\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\t\b\u0002\u0010\u0080\u0001\u001a\u00020\u0017H\u0007¢\u0006\u0006\b\u0081\u0001\u0010\u0082\u0001J\u0012\u0010\u0083\u0001\u001a\u00020\u0004H\u0017¢\u0006\u0006\b\u0083\u0001\u0010\u0084\u0001R-\u0010\u0085\u0001\u001a\u0016\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\b\u0012\u00060\u0007j\u0002`60\u000e8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u0085\u0001\u0010\u0086\u0001R,\u0010\u0088\u0001\u001a\u0015\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020)0\u000e0\u0087\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0088\u0001\u0010\u0089\u0001R*\u0010\u008b\u0001\u001a\u0013\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020)0\u008a\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u008b\u0001\u0010\u0086\u0001R9\u0010\u008c\u0001\u001a\"\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0007j\u0002`.\u0012\u0004\u0012\u00020 0\u000e0\u000e8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u008c\u0001\u0010\u0086\u0001R=\u0010\u008d\u0001\u001a&\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0007j\u0002`\n\u0012\b\u0012\u000600j\u0002`10\u000e0\u000e8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u008d\u0001\u0010\u0086\u0001R\u001a\u0010\u008f\u0001\u001a\u00030\u008e\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u008f\u0001\u0010\u0090\u0001R&\u0010\u0091\u0001\u001a\u000f\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00070\u008a\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0091\u0001\u0010\u0086\u0001R8\u0010\u0092\u0001\u001a!\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020 0\u000e0\u000e0\u0087\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0092\u0001\u0010\u0089\u0001R.\u0010\u0094\u0001\u001a\u0017\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\t\u0012\u00070)j\u0003`\u0093\u00010\u000e8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u0094\u0001\u0010\u0086\u0001R#\u0010\u0095\u0001\u001a\f\u0012\b\u0012\u00060\u0007j\u0002`\b038\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u0095\u0001\u0010\u0096\u0001R,\u0010\u0097\u0001\u001a\u0015\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00070\u000e0\u0087\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0097\u0001\u0010\u0089\u0001R\u001a\u0010\u0099\u0001\u001a\u00030\u0098\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0099\u0001\u0010\u009a\u0001R?\u0010\u009b\u0001\u001a(\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0019\u0012\u0017\u0012\b\u0012\u00060\u0007j\u0002`\n\u0012\b\u0012\u000600j\u0002`10\u008a\u00010\u008a\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u009b\u0001\u0010\u0086\u0001R\u001a\u0010\u009d\u0001\u001a\u00030\u009c\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u009d\u0001\u0010\u009e\u0001R;\u0010\u009f\u0001\u001a$\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0015\u0012\u0013\u0012\b\u0012\u00060\u0007j\u0002`.\u0012\u0004\u0012\u00020 0\u008a\u00010\u008a\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u009f\u0001\u0010\u0086\u0001R?\u0010 \u0001\u001a(\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0019\u0012\u0017\u0012\b\u0012\u00060\u0007j\u0002`\n\u0012\b\u0012\u00060\u000fj\u0002`\u00100\u008a\u00010\u008a\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b \u0001\u0010\u0086\u0001R\u001d\u0010£\u0001\u001a\u00060\u0007j\u0002`\n8B@\u0002X\u0082\u0004¢\u0006\b\u001a\u0006\b¡\u0001\u0010¢\u0001R$\u0010¥\u0001\u001a\r\u0012\b\u0012\u00060\u0007j\u0002`\b0¤\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b¥\u0001\u0010\u0096\u0001¨\u0006ª\u0001"}, d2 = {"Lcom/discord/stores/StoreGuilds;", "Lcom/discord/stores/StoreV2;", "Landroid/content/Context;", "ctx", "", "initClearCommunicationDisabledObserver", "(Landroid/content/Context;)V", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/UserId;", "userId", "handleGuildMemberCommunicationEnabledInternal", "(JJ)V", "", "Lcom/discord/api/guildmember/GuildMember;", "Lcom/discord/stores/ApiGuildMember;", "members", "handleHasRoleAndJoinedAt", "(JLjava/util/Map;)V", "Lcom/discord/api/guild/Guild;", "Lcom/discord/stores/ApiGuild;", "guild", "", "remove", "handleGuild", "(Lcom/discord/api/guild/Guild;Z)V", "unavailable", "handleGuildUnavailable", "(JZZ)V", "deletedRoleId", "", "Lcom/discord/api/role/GuildRole;", "roles", "handleGuildRoles", "(JJLjava/util/List;Z)V", "handleGuildMembersMap", "removedGuildMemberUserId", "", "handleGuildMembers", "(JJLjava/util/Collection;Z)V", "Lcom/discord/models/guild/Guild;", "getGuilds", "()Ljava/util/Map;", "getGuild", "(J)Lcom/discord/models/guild/Guild;", "Lcom/discord/primitives/RoleId;", "getRoles", "Lcom/discord/models/member/GuildMember;", "Lcom/discord/stores/ClientGuildMember;", "getMembers", "", "getUnavailableGuilds", "()Ljava/util/Set;", "Lcom/discord/primitives/Timestamp;", "getGuildsJoinedAt", "getMember", "(JJ)Lcom/discord/models/member/GuildMember;", "getGuildsInternal$app_productionGoogleRelease", "getGuildsInternal", "getGuildRolesInternal$app_productionGoogleRelease", "getGuildRolesInternal", "getGuildMembersComputedInternal$app_productionGoogleRelease", "getGuildMembersComputedInternal", "getUnavailableGuildsInternal$app_productionGoogleRelease", "getUnavailableGuildsInternal", "getGuildsJoinedAtInternal$app_productionGoogleRelease", "getGuildsJoinedAtInternal", "context", "init", "Lrx/Observable;", "observeGuilds", "()Lrx/Observable;", "observeGuildIds", "observeGuild", "(J)Lrx/Observable;", "Lcom/discord/primitives/ChannelId;", "channelId", "observeFromChannelId", "Lcom/discord/api/guild/GuildVerificationLevel;", "observeVerificationLevel", "observeComputed", "userIds", "(JLjava/util/Collection;)Lrx/Observable;", "observeComputedMember", "(JJ)Lrx/Observable;", "observeRoles", "observeSortedRoles", "roleIds", "observeJoinedAt", "observeUnavailableGuilds", "kotlin.jvm.PlatformType", "observeGuildMembers", "observeGuildMember", "observeCommunicationDisabledGuildMembers", "handleGuildMemberCommunicationEnabled", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "handleGuildAdd", "(Lcom/discord/api/guild/Guild;)V", "role", "handleGuildRoleCreateOrUpdate", "(JLcom/discord/api/role/GuildRole;)V", "roleId", "handleGuildRoleRemove", "handleGuildRemove", "member", "handleGuildMemberAdd", "(Lcom/discord/api/guildmember/GuildMember;)V", "Lcom/discord/api/guildmember/GuildMembersChunk;", "chunk", "handleGuildMembersChunk", "(Lcom/discord/api/guildmember/GuildMembersChunk;)V", "Lcom/discord/api/thread/ThreadMemberListUpdate;", "threadMemberListUpdate", "handleThreadMemberListUpdate", "(Lcom/discord/api/thread/ThreadMemberListUpdate;)V", "Lcom/discord/api/thread/ThreadMembersUpdate;", "threadMembersUpdate", "handleThreadMembersUpdate", "(Lcom/discord/api/thread/ThreadMembersUpdate;)V", "Lcom/discord/api/guildscheduledevent/ApiGuildScheduledEventUser;", "apiGuildScheduledEventUsers", "handleGuildScheduledEventUsersFetch", "(Ljava/util/List;J)V", "handleGuildMemberRemove", "isFullGuildMember", "handleGuildMember", "(Lcom/discord/api/guildmember/GuildMember;JZ)V", "snapshotData", "()V", "guildsJoinedAtSnapshot", "Ljava/util/Map;", "Lcom/discord/utilities/persister/Persister;", "guildsCache", "Lcom/discord/utilities/persister/Persister;", "", "guilds", "guildRolesSnapshot", "guildMembersComputedSnapshot", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "guildsJoinedAt", "guildRolesCache", "Lcom/discord/stores/ClientGuild;", "guildsSnapshot", "guildsUnavailableSnapshot", "Ljava/util/Set;", "guildsJoinedAtCache", "Lcom/discord/stores/StoreUser;", "userStore", "Lcom/discord/stores/StoreUser;", "guildMembersComputed", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "guildRoles", "guildMembers", "getMeId", "()J", "meId", "", "guildsUnavailable", HookHelper.constructorName, "(Lcom/discord/stores/StoreUser;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V", "Companion", "Actions", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuilds extends StoreV2 {
    private static final int HUGE_GUILD_SIZE = 2000;
    private final Dispatcher dispatcher;
    private final Map<Long, Map<Long, GuildMember>> guildMembers;
    private final Map<Long, Map<Long, com.discord.models.member.GuildMember>> guildMembersComputed;
    private Map<Long, ? extends Map<Long, com.discord.models.member.GuildMember>> guildMembersComputedSnapshot;
    private final Map<Long, Map<Long, GuildRole>> guildRoles;
    private final Persister<Map<Long, Map<Long, GuildRole>>> guildRolesCache;
    private Map<Long, ? extends Map<Long, GuildRole>> guildRolesSnapshot;
    private final Map<Long, Guild> guilds;
    private final Persister<Map<Long, Guild>> guildsCache;
    private final Map<Long, Long> guildsJoinedAt;
    private final Persister<Map<Long, Long>> guildsJoinedAtCache;
    private Map<Long, Long> guildsJoinedAtSnapshot;
    private Map<Long, Guild> guildsSnapshot;
    private final Set<Long> guildsUnavailable;
    private Set<Long> guildsUnavailableSnapshot;
    private final ObservationDeck observationDeck;
    private final StoreUser userStore;
    public static final Companion Companion = new Companion(null);
    private static final Map<Long, com.discord.models.member.GuildMember> emptyComputedMap = new HashMap();
    private static final Map<Long, GuildRole> emptyRoles = new HashMap();
    private static final StoreGuilds$Companion$GuildsUpdate$1 GuildsUpdate = new ObservationDeck.UpdateSource() { // from class: com.discord.stores.StoreGuilds$Companion$GuildsUpdate$1
    };
    private static final StoreGuilds$Companion$ComputedMembersUpdate$1 ComputedMembersUpdate = new ObservationDeck.UpdateSource() { // from class: com.discord.stores.StoreGuilds$Companion$ComputedMembersUpdate$1
    };

    /* compiled from: StoreGuilds.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0018\u0010\u0019J/\u0010\n\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\u000e\u0010\u0006\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0007¢\u0006\u0004\b\n\u0010\u000bJ?\u0010\u0016\u001a\u00020\t2\n\u0010\u000e\u001a\u00060\fj\u0002`\r2\n\u0010\u0010\u001a\u00060\fj\u0002`\u000f2\u0006\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u0014H\u0007¢\u0006\u0004\b\u0016\u0010\u0017¨\u0006\u001a"}, d2 = {"Lcom/discord/stores/StoreGuilds$Actions;", "", "Lcom/discord/app/AppComponent;", "fragment", "Lrx/Observable;", "", "partialUserNameTokenEmitted", "", "autocomplete", "", "requestMembers", "(Lcom/discord/app/AppComponent;Lrx/Observable;Z)V", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/RoleId;", "roleId", "appComponent", "Lcom/discord/utilities/rest/RestAPI;", "restApi", "Lcom/discord/stores/StoreGatewayConnection;", "storeGatewayConnection", "requestRoleMembers", "(JJLcom/discord/app/AppComponent;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreGatewayConnection;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Actions {
        public static final Actions INSTANCE = new Actions();

        private Actions() {
        }

        public static final void requestMembers(AppComponent appComponent, Observable<String> observable, final boolean z2) {
            m.checkNotNullParameter(appComponent, "fragment");
            m.checkNotNullParameter(observable, "partialUserNameTokenEmitted");
            Observable Y = observable.o(1000L, TimeUnit.MILLISECONDS).F(new b<String, String>() { // from class: com.discord.stores.StoreGuilds$Actions$requestMembers$1
                public final String call(String str) {
                    boolean z3 = false;
                    if (z2) {
                        if (!(str == null || str.length() == 0) && str.charAt(0) == '@') {
                            String substring = str.substring(1);
                            m.checkNotNullExpressionValue(substring, "(this as java.lang.String).substring(startIndex)");
                            return substring;
                        }
                    }
                    if (!z2) {
                        if (str == null || str.length() == 0) {
                            z3 = true;
                        }
                        if (!z3) {
                            return str;
                        }
                    }
                    return null;
                }
            }).x(StoreGuilds$Actions$requestMembers$2.INSTANCE).q().Y(StoreGuilds$Actions$requestMembers$3.INSTANCE);
            m.checkNotNullExpressionValue(Y, "partialUserNameTokenEmit…            }\n          }");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(Y), appComponent, null, 2, null), (r18 & 1) != 0 ? null : null, "requestGuildMembers", (r18 & 4) != 0 ? null : null, StoreGuilds$Actions$requestMembers$4.INSTANCE, (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
        }

        public static final void requestRoleMembers(long j, long j2, AppComponent appComponent, RestAPI restAPI, StoreGatewayConnection storeGatewayConnection) {
            m.checkNotNullParameter(appComponent, "appComponent");
            m.checkNotNullParameter(restAPI, "restApi");
            m.checkNotNullParameter(storeGatewayConnection, "storeGatewayConnection");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(restAPI.getGuildRoleMemberIds(j, j2)), appComponent, null, 2, null), INSTANCE.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreGuilds$Actions$requestRoleMembers$1(storeGatewayConnection, j));
        }
    }

    /* compiled from: StoreGuilds.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004*\u0002\u0002\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007R\u0016\u0010\t\u001a\u00020\b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\t\u0010\nR&\u0010\u000f\u001a\u0012\u0012\u0004\u0012\u00020\f\u0012\b\u0012\u00060\rj\u0002`\u000e0\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010R\"\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u00110\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u0010¨\u0006\u0015"}, d2 = {"Lcom/discord/stores/StoreGuilds$Companion;", "", "com/discord/stores/StoreGuilds$Companion$ComputedMembersUpdate$1", "ComputedMembersUpdate", "Lcom/discord/stores/StoreGuilds$Companion$ComputedMembersUpdate$1;", "com/discord/stores/StoreGuilds$Companion$GuildsUpdate$1", "GuildsUpdate", "Lcom/discord/stores/StoreGuilds$Companion$GuildsUpdate$1;", "", "HUGE_GUILD_SIZE", "I", "", "", "Lcom/discord/models/member/GuildMember;", "Lcom/discord/stores/ClientGuildMember;", "emptyComputedMap", "Ljava/util/Map;", "Lcom/discord/api/role/GuildRole;", "emptyRoles", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ StoreGuilds(StoreUser storeUser, Dispatcher dispatcher, ObservationDeck observationDeck, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(storeUser, dispatcher, (i & 4) != 0 ? ObservationDeckProvider.get() : observationDeck);
    }

    private final long getMeId() {
        return this.userStore.getMeInternal$app_productionGoogleRelease().getId();
    }

    @StoreThread
    private final void handleGuild(com.discord.api.guild.Guild guild, boolean z2) {
        long r = guild.r();
        if (!z2) {
            Guild guild2 = this.guilds.get(Long.valueOf(r));
            if (guild2 == null) {
                guild2 = new Guild(null, null, null, null, null, 0, 0L, null, 0L, null, null, null, false, 0, 0, null, null, null, 0, null, null, 0, 0, 0, null, null, null, null, null, null, null, 0, false, null, -1, 3, null);
            }
            Guild merge = guild2.merge(guild);
            if (!m.areEqual(merge, this.guilds.get(Long.valueOf(r)))) {
                this.guilds.put(Long.valueOf(r), merge);
                markChanged(GuildsUpdate);
            }
        } else if (this.guilds.containsKey(Long.valueOf(r))) {
            this.guilds.remove(Long.valueOf(r));
            markChanged(GuildsUpdate);
        }
    }

    public static /* synthetic */ void handleGuildMember$default(StoreGuilds storeGuilds, GuildMember guildMember, long j, boolean z2, int i, Object obj) {
        if ((i & 4) != 0) {
            z2 = false;
        }
        storeGuilds.handleGuildMember(guildMember, j, z2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleGuildMemberCommunicationEnabledInternal(long j, long j2) {
        Map<Long, GuildMember> map = this.guildMembers.get(Long.valueOf(j));
        UtcDateTime utcDateTime = null;
        GuildMember guildMember = map != null ? map.get(Long.valueOf(j2)) : null;
        if (guildMember != null) {
            utcDateTime = guildMember.e();
        }
        if (utcDateTime != null) {
            GuildMember a = GuildMember.a(guildMember, 0L, null, null, null, null, null, false, null, null, null, null, null, null, 4095);
            handleGuildMember$default(this, a, j, false, 4, null);
            if (j2 == getMeId()) {
                StoreStream.Companion.getPermissions().handleGuildMemberAdd(a);
            }
        }
    }

    @StoreThread
    private final void handleGuildMembers(long j, long j2, Collection<GuildMember> collection, boolean z2) {
        if (z2) {
            if (j2 > 0) {
                Map<Long, GuildMember> map = this.guildMembers.get(Long.valueOf(j));
                com.discord.models.member.GuildMember guildMember = null;
                if ((map != null ? map.remove(Long.valueOf(j2)) : null) != null) {
                    markChanged(GuildsUpdate);
                }
                Map<Long, com.discord.models.member.GuildMember> map2 = this.guildMembersComputed.get(Long.valueOf(j));
                if (map2 != null) {
                    guildMember = map2.remove(Long.valueOf(j2));
                }
                if (guildMember != null) {
                    markChanged(ComputedMembersUpdate);
                    return;
                }
                return;
            }
            if (this.guildMembers.remove(Long.valueOf(j)) != null) {
                markChanged(GuildsUpdate);
            }
            if (this.guildMembersComputed.remove(Long.valueOf(j)) != null) {
                markChanged(ComputedMembersUpdate);
            }
        } else if (collection != null) {
            for (GuildMember guildMember2 : collection) {
                handleGuildMember$default(this, guildMember2, j, false, 4, null);
            }
        }
    }

    @StoreThread
    private final void handleGuildMembersMap(long j, Map<Long, GuildMember> map) {
        handleGuildMembers(j, 0L, map != null ? map.values() : null, false);
    }

    @StoreThread
    private final void handleGuildRoles(long j, long j2, List<GuildRole> list, boolean z2) {
        if (!z2) {
            Map<Long, Map<Long, GuildRole>> map = this.guildRoles;
            Long valueOf = Long.valueOf(j);
            Map<Long, GuildRole> map2 = map.get(valueOf);
            if (map2 == null) {
                map2 = new HashMap<>();
                map.put(valueOf, map2);
            }
            Map<Long, GuildRole> map3 = map2;
            if (list != null) {
                for (GuildRole guildRole : list) {
                    if (!m.areEqual(map3.get(Long.valueOf(guildRole.getId())), guildRole)) {
                        map3.put(Long.valueOf(guildRole.getId()), guildRole);
                        markChanged(GuildsUpdate);
                    }
                }
            }
        } else if (j2 > 0) {
            Map<Long, GuildRole> map4 = this.guildRoles.get(Long.valueOf(j));
            if ((map4 != null ? map4.remove(Long.valueOf(j2)) : null) != null) {
                markChanged(GuildsUpdate);
            }
        } else if (this.guildRoles.containsKey(Long.valueOf(j))) {
            this.guildRoles.remove(Long.valueOf(j));
            markChanged(GuildsUpdate);
        }
    }

    @StoreThread
    private final void handleGuildUnavailable(long j, boolean z2, boolean z3) {
        if (z3) {
            if (z2 && !this.guildsUnavailable.contains(Long.valueOf(j))) {
                this.guildsUnavailable.add(Long.valueOf(j));
                markChanged(GuildsUpdate);
            }
        } else if (!z2 && this.guildsUnavailable.contains(Long.valueOf(j))) {
            this.guildsUnavailable.remove(Long.valueOf(j));
            markChanged(GuildsUpdate);
        }
    }

    @StoreThread
    private final void handleHasRoleAndJoinedAt(long j, Map<Long, GuildMember> map) {
        GuildMember guildMember;
        Long l;
        MeUser meInternal$app_productionGoogleRelease = this.userStore.getMeInternal$app_productionGoogleRelease();
        if (map != null && (guildMember = map.get(Long.valueOf(meInternal$app_productionGoogleRelease.getId()))) != null) {
            UtcDateTime g = guildMember.g();
            long g2 = g != null ? g.g() : 0L;
            if (!this.guildsJoinedAt.containsKey(Long.valueOf(j)) || (l = this.guildsJoinedAt.get(Long.valueOf(j))) == null || l.longValue() != g2) {
                this.guildsJoinedAt.put(Long.valueOf(j), Long.valueOf(g2));
                markChanged(GuildsUpdate);
            }
        }
    }

    private final void initClearCommunicationDisabledObserver(Context context) {
        Observable q = Observable.j(observeCommunicationDisabledGuildMembers(), Observable.D(0L, 10L, TimeUnit.SECONDS), StoreGuilds$initClearCommunicationDisabledObserver$1.INSTANCE).q();
        m.checkNotNullExpressionValue(q, "Observable.combineLatest…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.computationLatest(q), StoreGuilds.class, (r18 & 2) != 0 ? null : context, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreGuilds$initClearCommunicationDisabledObserver$2(this));
    }

    public final Guild getGuild(long j) {
        return this.guildsSnapshot.get(Long.valueOf(j));
    }

    @StoreThread
    public final Map<Long, Map<Long, com.discord.models.member.GuildMember>> getGuildMembersComputedInternal$app_productionGoogleRelease() {
        return this.guildMembersComputed;
    }

    @StoreThread
    public final Map<Long, Map<Long, GuildRole>> getGuildRolesInternal$app_productionGoogleRelease() {
        return this.guildRoles;
    }

    public final Map<Long, Guild> getGuilds() {
        return this.guildsSnapshot;
    }

    @StoreThread
    public final Map<Long, Guild> getGuildsInternal$app_productionGoogleRelease() {
        return this.guilds;
    }

    public final Map<Long, Long> getGuildsJoinedAt() {
        return this.guildsJoinedAtSnapshot;
    }

    @StoreThread
    public final Map<Long, Long> getGuildsJoinedAtInternal$app_productionGoogleRelease() {
        return this.guildsJoinedAt;
    }

    public final com.discord.models.member.GuildMember getMember(long j, long j2) {
        Map<Long, com.discord.models.member.GuildMember> map = this.guildMembersComputedSnapshot.get(Long.valueOf(j));
        if (map != null) {
            return map.get(Long.valueOf(j2));
        }
        return null;
    }

    public final Map<Long, Map<Long, com.discord.models.member.GuildMember>> getMembers() {
        return this.guildMembersComputedSnapshot;
    }

    public final Map<Long, Map<Long, GuildRole>> getRoles() {
        return this.guildRolesSnapshot;
    }

    public final Set<Long> getUnavailableGuilds() {
        return this.guildsUnavailableSnapshot;
    }

    @StoreThread
    public final Set<Long> getUnavailableGuildsInternal$app_productionGoogleRelease() {
        return this.guildsUnavailable;
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        Map<Long, GuildMember> map;
        m.checkNotNullParameter(modelPayload, "payload");
        this.guilds.clear();
        this.guildMembers.clear();
        this.guildMembersComputed.clear();
        this.guildRoles.clear();
        this.guildsUnavailable.clear();
        for (com.discord.api.guild.Guild guild : modelPayload.getGuilds()) {
            long r = guild.r();
            if (guild.O()) {
                this.guildsUnavailable.add(Long.valueOf(r));
            } else {
                handleGuildRoles(r, 0L, guild.G(), false);
                m.checkNotNullExpressionValue(guild, "guild");
                handleGuild(guild, false);
                List<GuildMember> v = guild.v();
                if (v == null || (map = GuildUtilsKt.asMap(v)) == null) {
                    map = h0.emptyMap();
                }
                handleGuildMembersMap(r, map);
                handleHasRoleAndJoinedAt(r, map);
            }
        }
        markChanged(GuildsUpdate, ComputedMembersUpdate);
    }

    @StoreThread
    public final void handleGuildAdd(com.discord.api.guild.Guild guild) {
        Map<Long, GuildMember> map;
        m.checkNotNullParameter(guild, "guild");
        handleGuildUnavailable(guild.r(), guild.O(), false);
        if (!guild.O()) {
            handleGuildRoles(guild.r(), 0L, guild.G(), false);
            handleGuild(guild, false);
            List<GuildMember> v = guild.v();
            if (v == null || (map = GuildUtilsKt.asMap(v)) == null) {
                map = h0.emptyMap();
            }
            handleGuildMembersMap(guild.r(), map);
            handleHasRoleAndJoinedAt(guild.r(), map);
        }
    }

    @StoreThread
    public final void handleGuildMember(GuildMember guildMember, long j, boolean z2) {
        com.discord.models.member.GuildMember from;
        GuildMember guildMember2 = guildMember;
        m.checkNotNullParameter(guildMember2, "member");
        long i = guildMember.m().i();
        if (!this.guildMembers.containsKey(Long.valueOf(j))) {
            this.guildMembers.put(Long.valueOf(j), new HashMap());
        }
        if (!this.guildMembersComputed.containsKey(Long.valueOf(j))) {
            this.guildMembersComputed.put(Long.valueOf(j), new HashMap());
        }
        Map<Long, GuildMember> map = this.guildMembers.get(Long.valueOf(j));
        m.checkNotNull(map);
        GuildMember guildMember3 = map.get(Long.valueOf(i));
        if (!z2 && guildMember3 != null) {
            guildMember2 = GuildMember.a(guildMember, 0L, null, null, null, null, null, false, null, null, null, guildMember3.d(), guildMember3.c(), null, 5119);
        }
        GuildMember guildMember4 = guildMember2;
        if (!m.areEqual(guildMember4, guildMember3)) {
            Map<Long, GuildMember> map2 = this.guildMembers.get(Long.valueOf(j));
            m.checkNotNull(map2);
            map2.put(Long.valueOf(i), guildMember4);
        }
        from = com.discord.models.member.GuildMember.Companion.from(guildMember4, j, (r13 & 4) != 0 ? null : this.guildRoles.get(Long.valueOf(j)), (r13 & 8) != 0 ? null : null);
        Map<Long, com.discord.models.member.GuildMember> map3 = this.guildMembersComputed.get(Long.valueOf(j));
        m.checkNotNull(map3);
        if (!m.areEqual(from, map3.get(Long.valueOf(i)))) {
            Map<Long, com.discord.models.member.GuildMember> map4 = this.guildMembersComputed.get(Long.valueOf(j));
            m.checkNotNull(map4);
            map4.put(Long.valueOf(i), from);
            markChanged(ComputedMembersUpdate);
        }
    }

    @StoreThread
    public final void handleGuildMemberAdd(GuildMember guildMember) {
        m.checkNotNullParameter(guildMember, "member");
        handleGuildMember$default(this, guildMember, guildMember.f(), false, 4, null);
    }

    public final void handleGuildMemberCommunicationEnabled(long j, long j2) {
        this.dispatcher.schedule(new StoreGuilds$handleGuildMemberCommunicationEnabled$1(this, j, j2));
    }

    @StoreThread
    public final void handleGuildMemberRemove(long j, long j2) {
        handleGuildMembers(j, j2, null, true);
    }

    @StoreThread
    public final void handleGuildMembersChunk(GuildMembersChunk guildMembersChunk) {
        m.checkNotNullParameter(guildMembersChunk, "chunk");
        handleGuildMembers(guildMembersChunk.a(), 0L, guildMembersChunk.b(), false);
    }

    @StoreThread
    public final void handleGuildRemove(com.discord.api.guild.Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        handleGuildUnavailable(guild.r(), guild.O(), true);
        handleGuild(guild, true);
        handleGuildRoles(guild.r(), 0L, null, true);
        handleGuildMembers(guild.r(), 0L, null, true);
    }

    @StoreThread
    public final void handleGuildRoleCreateOrUpdate(long j, GuildRole guildRole) {
        m.checkNotNullParameter(guildRole, "role");
        handleGuildRoles(j, 0L, d0.t.m.listOf(guildRole), false);
        handleGuildMembersMap(j, this.guildMembers.get(Long.valueOf(j)));
        handleHasRoleAndJoinedAt(j, this.guildMembers.get(Long.valueOf(j)));
    }

    @StoreThread
    public final void handleGuildRoleRemove(long j, long j2) {
        handleGuildRoles(j2, j, null, true);
        handleGuildMembersMap(j2, this.guildMembers.get(Long.valueOf(j2)));
        handleHasRoleAndJoinedAt(j2, this.guildMembers.get(Long.valueOf(j2)));
    }

    @StoreThread
    public final void handleGuildScheduledEventUsersFetch(List<ApiGuildScheduledEventUser> list, long j) {
        m.checkNotNullParameter(list, "apiGuildScheduledEventUsers");
        ArrayList arrayList = new ArrayList();
        for (ApiGuildScheduledEventUser apiGuildScheduledEventUser : list) {
            GuildMember a = apiGuildScheduledEventUser.a(j);
            if (a != null) {
                arrayList.add(a);
            }
        }
        handleGuildMembers(j, 0L, arrayList, false);
    }

    @StoreThread
    public final void handleThreadMemberListUpdate(ThreadMemberListUpdate threadMemberListUpdate) {
        ArrayList arrayList;
        m.checkNotNullParameter(threadMemberListUpdate, "threadMemberListUpdate");
        long a = threadMemberListUpdate.a();
        List<ThreadListMember> b2 = threadMemberListUpdate.b();
        if (b2 != null) {
            ArrayList arrayList2 = new ArrayList();
            for (ThreadListMember threadListMember : b2) {
                GuildMember a2 = threadListMember.a();
                if (a2 != null) {
                    arrayList2.add(a2);
                }
            }
            arrayList = arrayList2;
        } else {
            arrayList = null;
        }
        handleGuildMembers(a, 0L, arrayList, false);
    }

    @StoreThread
    public final void handleThreadMembersUpdate(ThreadMembersUpdate threadMembersUpdate) {
        ArrayList arrayList;
        m.checkNotNullParameter(threadMembersUpdate, "threadMembersUpdate");
        long b2 = threadMembersUpdate.b();
        List<AugmentedThreadMember> a = threadMembersUpdate.a();
        if (a != null) {
            ArrayList arrayList2 = new ArrayList();
            for (AugmentedThreadMember augmentedThreadMember : a) {
                GuildMember c = augmentedThreadMember.c();
                if (c != null) {
                    arrayList2.add(c);
                }
            }
            arrayList = arrayList2;
        } else {
            arrayList = null;
        }
        handleGuildMembers(b2, 0L, arrayList, false);
    }

    @Override // com.discord.stores.Store
    public void init(Context context) {
        m.checkNotNullParameter(context, "context");
        this.guilds.putAll(CollectionExtensionsKt.filterNonNullValues(this.guildsCache.get()));
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        Iterator<Map.Entry<Long, Map<Long, GuildRole>>> it = this.guildRolesCache.get().entrySet().iterator();
        while (true) {
            boolean z2 = true;
            if (!it.hasNext()) {
                break;
            }
            Map.Entry<Long, Map<Long, GuildRole>> next = it.next();
            if (next.getKey() == null) {
                z2 = false;
            }
            if (z2) {
                linkedHashMap.put(next.getKey(), next.getValue());
            }
        }
        for (Map.Entry entry : linkedHashMap.entrySet()) {
            this.guildRoles.put(Long.valueOf(((Number) entry.getKey()).longValue()), h0.toMutableMap((Map) entry.getValue()));
        }
        this.guildsJoinedAt.putAll(this.guildsJoinedAtCache.get());
        markChanged(GuildsUpdate, ComputedMembersUpdate);
        initClearCommunicationDisabledObserver(context);
    }

    public final Observable<List<com.discord.models.member.GuildMember>> observeCommunicationDisabledGuildMembers() {
        Observable<List<com.discord.models.member.GuildMember>> q = Observable.j(observeGuildIds(), observeComputed(), StoreGuilds$observeCommunicationDisabledGuildMembers$1.INSTANCE).q();
        m.checkNotNullExpressionValue(q, "Observable.combineLatest…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Map<Long, Map<Long, com.discord.models.member.GuildMember>>> observeComputed() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{ComputedMembersUpdate}, false, null, null, new StoreGuilds$observeComputed$1(this), 14, null);
    }

    public final Observable<com.discord.models.member.GuildMember> observeComputedMember(long j, long j2) {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{ComputedMembersUpdate}, false, null, null, new StoreGuilds$observeComputedMember$1(this, j, j2), 14, null);
    }

    public final Observable<Guild> observeFromChannelId(long j) {
        Observable Y = StoreStream.Companion.getChannels().observeChannel(j).Y(new b<Channel, Observable<? extends Guild>>() { // from class: com.discord.stores.StoreGuilds$observeFromChannelId$1
            public final Observable<? extends Guild> call(Channel channel) {
                if (channel != null) {
                    return StoreGuilds.this.observeGuild(channel.f());
                }
                return new k(null);
            }
        });
        m.checkNotNullExpressionValue(Y, "StoreStream\n        .get…ll)\n          }\n        }");
        return Y;
    }

    public final Observable<Guild> observeGuild(final long j) {
        Observable<Guild> q = observeGuilds().F(new b<Map<Long, ? extends Guild>, Guild>() { // from class: com.discord.stores.StoreGuilds$observeGuild$1
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Guild call(Map<Long, ? extends Guild> map) {
                return call2((Map<Long, Guild>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Guild call2(Map<Long, Guild> map) {
                return map.get(Long.valueOf(j));
            }
        }).q();
        m.checkNotNullExpressionValue(q, "observeGuilds()\n        …  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Set<Long>> observeGuildIds() {
        Observable<Set<Long>> q = observeGuilds().Y(StoreGuilds$observeGuildIds$1.INSTANCE).q();
        m.checkNotNullExpressionValue(q, "observeGuilds()\n        …  .distinctUntilChanged()");
        return q;
    }

    public final Observable<com.discord.models.member.GuildMember> observeGuildMember(final long j, final long j2) {
        return observeComputed().F(new b<Map<Long, ? extends Map<Long, ? extends com.discord.models.member.GuildMember>>, com.discord.models.member.GuildMember>() { // from class: com.discord.stores.StoreGuilds$observeGuildMember$1
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ com.discord.models.member.GuildMember call(Map<Long, ? extends Map<Long, ? extends com.discord.models.member.GuildMember>> map) {
                return call2((Map<Long, ? extends Map<Long, com.discord.models.member.GuildMember>>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final com.discord.models.member.GuildMember call2(Map<Long, ? extends Map<Long, com.discord.models.member.GuildMember>> map) {
                Map<Long, com.discord.models.member.GuildMember> map2 = map.get(Long.valueOf(j));
                if (map2 != null) {
                    return map2.get(Long.valueOf(j2));
                }
                return null;
            }
        }).q();
    }

    public final Observable<Map<Long, com.discord.models.member.GuildMember>> observeGuildMembers(final long j) {
        return observeComputed().F(new b<Map<Long, ? extends Map<Long, ? extends com.discord.models.member.GuildMember>>, Map<Long, ? extends com.discord.models.member.GuildMember>>() { // from class: com.discord.stores.StoreGuilds$observeGuildMembers$1
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Map<Long, ? extends com.discord.models.member.GuildMember> call(Map<Long, ? extends Map<Long, ? extends com.discord.models.member.GuildMember>> map) {
                return call2((Map<Long, ? extends Map<Long, com.discord.models.member.GuildMember>>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Map<Long, com.discord.models.member.GuildMember> call2(Map<Long, ? extends Map<Long, com.discord.models.member.GuildMember>> map) {
                Map<Long, com.discord.models.member.GuildMember> map2 = map.get(Long.valueOf(j));
                return map2 != null ? map2 : h0.emptyMap();
            }
        }).q();
    }

    public final Observable<Map<Long, Guild>> observeGuilds() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{GuildsUpdate}, false, null, null, new StoreGuilds$observeGuilds$1(this), 14, null);
    }

    public final Observable<Map<Long, Long>> observeJoinedAt() {
        Observable<Map<Long, Long>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{GuildsUpdate}, false, null, null, new StoreGuilds$observeJoinedAt$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck\n        …  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Map<Long, Map<Long, GuildRole>>> observeRoles() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{GuildsUpdate}, false, null, null, new StoreGuilds$observeRoles$1(this), 14, null);
    }

    public final Observable<List<GuildRole>> observeSortedRoles(long j) {
        Observable<List<GuildRole>> q = observeRoles(j).Y(StoreGuilds$observeSortedRoles$1.INSTANCE).q();
        m.checkNotNullExpressionValue(q, "observeRoles(guildId)\n  …  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Set<Long>> observeUnavailableGuilds() {
        Observable<Set<Long>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{GuildsUpdate}, false, null, null, new StoreGuilds$observeUnavailableGuilds$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck\n        …  .distinctUntilChanged()");
        return q;
    }

    public final Observable<GuildVerificationLevel> observeVerificationLevel(long j) {
        Observable<GuildVerificationLevel> q = observeGuild(j).F(StoreGuilds$observeVerificationLevel$1.INSTANCE).q();
        m.checkNotNullExpressionValue(q, "observeGuild(guildId)\n  …  .distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        SnowflakePartitionMap.CopiablePartitionMap copiablePartitionMap;
        super.snapshotData();
        if (getUpdateSources().contains(GuildsUpdate)) {
            HashMap hashMap = new HashMap(this.guildsJoinedAt);
            this.guildsJoinedAtSnapshot = hashMap;
            Persister.set$default(this.guildsJoinedAtCache, hashMap, false, 2, null);
            this.guildsSnapshot = new HashMap(this.guilds);
            Persister.set$default(this.guildsCache, h0.minus((Map) this.guilds, (Iterable) StoreStream.Companion.getLurking().getLurkingGuildIdsSync()), false, 2, null);
            this.guildsUnavailableSnapshot = new HashSet(this.guildsUnavailable);
            Map<Long, Map<Long, GuildRole>> map = this.guildRoles;
            LinkedHashMap linkedHashMap = new LinkedHashMap(g0.mapCapacity(map.size()));
            Iterator<T> it = map.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                linkedHashMap.put(entry.getKey(), new HashMap((Map) entry.getValue()));
            }
            this.guildRolesSnapshot = linkedHashMap;
            Persister.set$default(this.guildRolesCache, linkedHashMap, false, 2, null);
        }
        if (getUpdateSources().contains(ComputedMembersUpdate)) {
            HashMap hashMap2 = new HashMap();
            for (Map.Entry<Long, Map<Long, com.discord.models.member.GuildMember>> entry2 : this.guildMembersComputed.entrySet()) {
                Map<Long, com.discord.models.member.GuildMember> value = entry2.getValue();
                if (value.size() < 2000) {
                    hashMap2.put(entry2.getKey(), new HashMap(value));
                } else {
                    if (value instanceof SnowflakePartitionMap.CopiablePartitionMap) {
                        copiablePartitionMap = (SnowflakePartitionMap.CopiablePartitionMap) value;
                    } else {
                        SnowflakePartitionMap.CopiablePartitionMap copiablePartitionMap2 = new SnowflakePartitionMap.CopiablePartitionMap(0, 1, null);
                        copiablePartitionMap2.putAll(value);
                        entry2.setValue(copiablePartitionMap2);
                        copiablePartitionMap = copiablePartitionMap2;
                    }
                    hashMap2.put(entry2.getKey(), copiablePartitionMap.fastCopy());
                }
            }
            this.guildMembersComputedSnapshot = hashMap2;
        }
    }

    public StoreGuilds(StoreUser storeUser, Dispatcher dispatcher, ObservationDeck observationDeck) {
        m.checkNotNullParameter(storeUser, "userStore");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.userStore = storeUser;
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
        this.guildsSnapshot = h0.emptyMap();
        this.guildMembersComputedSnapshot = h0.emptyMap();
        this.guildRolesSnapshot = h0.emptyMap();
        this.guildsUnavailableSnapshot = n0.emptySet();
        this.guildsJoinedAtSnapshot = h0.emptyMap();
        this.guilds = new HashMap();
        this.guildMembersComputed = new HashMap();
        this.guildMembers = new HashMap();
        this.guildRoles = new HashMap();
        this.guildsUnavailable = new HashSet();
        this.guildsJoinedAt = new HashMap();
        this.guildsCache = new Persister<>("STORE_GUILDS_V34", new HashMap());
        this.guildRolesCache = new Persister<>("STORE_GUILD_ROLES_V7", new HashMap());
        this.guildsJoinedAtCache = new Persister<>("STORE_GUILD_JOINED_AT_V6", new HashMap());
    }

    public final Observable<Map<Long, com.discord.models.member.GuildMember>> observeComputed(final long j) {
        Observable<Map<Long, com.discord.models.member.GuildMember>> q = observeComputed().F(new b<Map<Long, ? extends Map<Long, ? extends com.discord.models.member.GuildMember>>, Map<Long, ? extends com.discord.models.member.GuildMember>>() { // from class: com.discord.stores.StoreGuilds$observeComputed$2
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Map<Long, ? extends com.discord.models.member.GuildMember> call(Map<Long, ? extends Map<Long, ? extends com.discord.models.member.GuildMember>> map) {
                return call2((Map<Long, ? extends Map<Long, com.discord.models.member.GuildMember>>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Map<Long, com.discord.models.member.GuildMember> call2(Map<Long, ? extends Map<Long, com.discord.models.member.GuildMember>> map) {
                Map<Long, com.discord.models.member.GuildMember> map2;
                Map<Long, com.discord.models.member.GuildMember> map3 = map.get(Long.valueOf(j));
                if (map3 != null) {
                    return map3;
                }
                map2 = StoreGuilds.emptyComputedMap;
                return map2;
            }
        }).q();
        m.checkNotNullExpressionValue(q, "observeComputed()\n      …  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Map<Long, GuildRole>> observeRoles(final long j) {
        Observable<Map<Long, GuildRole>> q = observeRoles().F(new b<Map<Long, ? extends Map<Long, ? extends GuildRole>>, Map<Long, ? extends GuildRole>>() { // from class: com.discord.stores.StoreGuilds$observeRoles$2
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Map<Long, ? extends GuildRole> call(Map<Long, ? extends Map<Long, ? extends GuildRole>> map) {
                return call2((Map<Long, ? extends Map<Long, GuildRole>>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Map<Long, GuildRole> call2(Map<Long, ? extends Map<Long, GuildRole>> map) {
                Map<Long, GuildRole> map2;
                Map<Long, GuildRole> map3 = map.get(Long.valueOf(j));
                if (map3 != null) {
                    return map3;
                }
                map2 = StoreGuilds.emptyRoles;
                return map2;
            }
        }).q();
        m.checkNotNullExpressionValue(q, "observeRoles()\n        .…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Long> observeJoinedAt(final long j) {
        Observable<Long> q = observeJoinedAt().F(new b<Map<Long, ? extends Long>, Long>() { // from class: com.discord.stores.StoreGuilds$observeJoinedAt$2
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Long call(Map<Long, ? extends Long> map) {
                return call2((Map<Long, Long>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Long call2(Map<Long, Long> map) {
                return Long.valueOf(GuildMemberUtilsKt.getJoinedAtOrNow(map.get(Long.valueOf(j))));
            }
        }).q();
        m.checkNotNullExpressionValue(q, "observeJoinedAt()\n      …  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Map<Long, com.discord.models.member.GuildMember>> observeComputed(long j, Collection<Long> collection) {
        m.checkNotNullParameter(collection, "userIds");
        Observable k = observeComputed(j).k(o.a(collection));
        m.checkNotNullExpressionValue(k, "observeComputed(guildId)…mpose(filterMap(userIds))");
        return k;
    }

    public final Observable<Map<Long, GuildRole>> observeRoles(long j, Collection<Long> collection) {
        m.checkNotNullParameter(collection, "roleIds");
        Observable k = observeRoles(j).k(o.a(collection));
        m.checkNotNullExpressionValue(k, "observeRoles(guildId)\n  …mpose(filterMap(roleIds))");
        return k;
    }
}
