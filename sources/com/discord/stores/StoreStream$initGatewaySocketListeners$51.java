package com.discord.stores;

import com.discord.api.emoji.GuildEmojisUpdate;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreStream.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/emoji/GuildEmojisUpdate;", "p1", "", "invoke", "(Lcom/discord/api/emoji/GuildEmojisUpdate;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final /* synthetic */ class StoreStream$initGatewaySocketListeners$51 extends k implements Function1<GuildEmojisUpdate, Unit> {
    public StoreStream$initGatewaySocketListeners$51(StoreStream storeStream) {
        super(1, storeStream, StoreStream.class, "handleEmojiUpdate", "handleEmojiUpdate(Lcom/discord/api/emoji/GuildEmojisUpdate;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(GuildEmojisUpdate guildEmojisUpdate) {
        invoke2(guildEmojisUpdate);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(GuildEmojisUpdate guildEmojisUpdate) {
        m.checkNotNullParameter(guildEmojisUpdate, "p1");
        ((StoreStream) this.receiver).handleEmojiUpdate(guildEmojisUpdate);
    }
}
