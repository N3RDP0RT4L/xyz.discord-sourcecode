package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.RecentlyNonNull;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import b.i.a.f.n.d;
import b.i.a.f.n.e;
import b.i.c.c;
import com.discord.utilities.intent.IntentUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import d0.g0.t;
import d0.g0.w;
import d0.z.d.k;
import d0.z.d.m;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Emitter;
import rx.Observable;
import rx.functions.Action1;
/* compiled from: StoreDynamicLink.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \u00192\u00020\u0001:\u0002\u0019\u001aB\u0017\u0012\u0006\u0010\u0012\u001a\u00020\u0011\u0012\u0006\u0010\u0015\u001a\u00020\u0014¢\u0006\u0004\b\u0017\u0010\u0018J!\u0010\u0007\u001a\u00020\u00062\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u001d\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u001d\u0010\u000f\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0012\u001a\u00020\u00118\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0015\u001a\u00020\u00148\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016¨\u0006\u001b"}, d2 = {"Lcom/discord/stores/StoreDynamicLink;", "", "Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;", "data", "Landroid/content/Context;", "context", "", "handleDataReceived", "(Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;Landroid/content/Context;)V", "Landroid/content/Intent;", "intent", "Lrx/Observable;", "Landroid/net/Uri;", "getDynamicLinkObservable", "(Landroid/content/Intent;)Lrx/Observable;", "storeLinkIfExists", "(Landroid/content/Intent;Landroid/content/Context;)V", "Lcom/discord/stores/StoreStream;", "stream", "Lcom/discord/stores/StoreStream;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;)V", "Companion", "DynamicLinkData", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreDynamicLink {
    private static final Companion Companion = new Companion(null);
    @Deprecated
    private static final long DYNAMIC_LINK_TIMEOUT_MS = 1000;
    private final Dispatcher dispatcher;
    private final StoreStream stream;

    /* compiled from: StoreDynamicLink.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0005\b\u0082\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/stores/StoreDynamicLink$Companion;", "", "", "DYNAMIC_LINK_TIMEOUT_MS", "J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreDynamicLink.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\r\b\u0082\b\u0018\u00002\u00020\u0001BI\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0002\u0012\u000e\u0010\u000e\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b%\u0010&J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0018\u0010\u0007\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\t\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\t\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\n\u0010\bJ\u0012\u0010\u000b\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u000b\u0010\bJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\f\u0010\bJ^\u0010\u0013\u001a\u00020\u00002\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00022\u0010\b\u0002\u0010\u000e\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00062\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0015\u0010\bJ\u0010\u0010\u0017\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u001a\u0010\u001b\u001a\u00020\u001a2\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001b\u0010\u001cR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001d\u001a\u0004\b\u001e\u0010\bR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001d\u001a\u0004\b\u001f\u0010\bR\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u001d\u001a\u0004\b \u0010\bR\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001d\u001a\u0004\b!\u0010\bR!\u0010\u000e\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001d\u001a\u0004\b\"\u0010\bR\u001b\u0010\r\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010#\u001a\u0004\b$\u0010\u0004¨\u0006'"}, d2 = {"Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;", "", "Landroid/net/Uri;", "component1", "()Landroid/net/Uri;", "", "Lcom/discord/primitives/FingerPrint;", "component2", "()Ljava/lang/String;", "component3", "component4", "component5", "component6", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "fingerprint", "attemptId", "inviteCode", "guildTemplateCode", "authToken", "copy", "(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getInviteCode", "getAttemptId", "getAuthToken", "getGuildTemplateCode", "getFingerprint", "Landroid/net/Uri;", "getUri", HookHelper.constructorName, "(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class DynamicLinkData {
        private final String attemptId;
        private final String authToken;
        private final String fingerprint;
        private final String guildTemplateCode;
        private final String inviteCode;
        private final Uri uri;

        public DynamicLinkData(Uri uri, String str, String str2, String str3, String str4, String str5) {
            this.uri = uri;
            this.fingerprint = str;
            this.attemptId = str2;
            this.inviteCode = str3;
            this.guildTemplateCode = str4;
            this.authToken = str5;
        }

        public static /* synthetic */ DynamicLinkData copy$default(DynamicLinkData dynamicLinkData, Uri uri, String str, String str2, String str3, String str4, String str5, int i, Object obj) {
            if ((i & 1) != 0) {
                uri = dynamicLinkData.uri;
            }
            if ((i & 2) != 0) {
                str = dynamicLinkData.fingerprint;
            }
            String str6 = str;
            if ((i & 4) != 0) {
                str2 = dynamicLinkData.attemptId;
            }
            String str7 = str2;
            if ((i & 8) != 0) {
                str3 = dynamicLinkData.inviteCode;
            }
            String str8 = str3;
            if ((i & 16) != 0) {
                str4 = dynamicLinkData.guildTemplateCode;
            }
            String str9 = str4;
            if ((i & 32) != 0) {
                str5 = dynamicLinkData.authToken;
            }
            return dynamicLinkData.copy(uri, str6, str7, str8, str9, str5);
        }

        public final Uri component1() {
            return this.uri;
        }

        public final String component2() {
            return this.fingerprint;
        }

        public final String component3() {
            return this.attemptId;
        }

        public final String component4() {
            return this.inviteCode;
        }

        public final String component5() {
            return this.guildTemplateCode;
        }

        public final String component6() {
            return this.authToken;
        }

        public final DynamicLinkData copy(Uri uri, String str, String str2, String str3, String str4, String str5) {
            return new DynamicLinkData(uri, str, str2, str3, str4, str5);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof DynamicLinkData)) {
                return false;
            }
            DynamicLinkData dynamicLinkData = (DynamicLinkData) obj;
            return m.areEqual(this.uri, dynamicLinkData.uri) && m.areEqual(this.fingerprint, dynamicLinkData.fingerprint) && m.areEqual(this.attemptId, dynamicLinkData.attemptId) && m.areEqual(this.inviteCode, dynamicLinkData.inviteCode) && m.areEqual(this.guildTemplateCode, dynamicLinkData.guildTemplateCode) && m.areEqual(this.authToken, dynamicLinkData.authToken);
        }

        public final String getAttemptId() {
            return this.attemptId;
        }

        public final String getAuthToken() {
            return this.authToken;
        }

        public final String getFingerprint() {
            return this.fingerprint;
        }

        public final String getGuildTemplateCode() {
            return this.guildTemplateCode;
        }

        public final String getInviteCode() {
            return this.inviteCode;
        }

        public final Uri getUri() {
            return this.uri;
        }

        public int hashCode() {
            Uri uri = this.uri;
            int i = 0;
            int hashCode = (uri != null ? uri.hashCode() : 0) * 31;
            String str = this.fingerprint;
            int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.attemptId;
            int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.inviteCode;
            int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
            String str4 = this.guildTemplateCode;
            int hashCode5 = (hashCode4 + (str4 != null ? str4.hashCode() : 0)) * 31;
            String str5 = this.authToken;
            if (str5 != null) {
                i = str5.hashCode();
            }
            return hashCode5 + i;
        }

        public String toString() {
            StringBuilder R = a.R("DynamicLinkData(uri=");
            R.append(this.uri);
            R.append(", fingerprint=");
            R.append(this.fingerprint);
            R.append(", attemptId=");
            R.append(this.attemptId);
            R.append(", inviteCode=");
            R.append(this.inviteCode);
            R.append(", guildTemplateCode=");
            R.append(this.guildTemplateCode);
            R.append(", authToken=");
            return a.H(R, this.authToken, ")");
        }
    }

    public StoreDynamicLink(StoreStream storeStream, Dispatcher dispatcher) {
        m.checkNotNullParameter(storeStream, "stream");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        this.stream = storeStream;
        this.dispatcher = dispatcher;
    }

    private final Observable<Uri> getDynamicLinkObservable(final Intent intent) {
        Observable n = Observable.n(new Action1<Emitter<Uri>>() { // from class: com.discord.stores.StoreDynamicLink$getDynamicLinkObservable$firebaseDynamicLinks$1

            /* compiled from: StoreDynamicLink.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "p1", "", "invoke", "(Ljava/lang/Throwable;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.stores.StoreDynamicLink$getDynamicLinkObservable$firebaseDynamicLinks$1$1  reason: invalid class name */
            /* loaded from: classes.dex */
            public static final /* synthetic */ class AnonymousClass1 extends k implements Function1<Throwable, Unit> {
                public AnonymousClass1(Emitter emitter) {
                    super(1, emitter, Emitter.class, "onError", "onError(Ljava/lang/Throwable;)V", 0);
                }

                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
                    invoke2(th);
                    return Unit.a;
                }

                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2(Throwable th) {
                    ((Emitter) this.receiver).onError(th);
                }
            }

            public final void call(final Emitter<Uri> emitter) {
                b.i.c.o.a aVar;
                synchronized (b.i.c.o.a.class) {
                    c b2 = c.b();
                    synchronized (b.i.c.o.a.class) {
                        b2.a();
                        aVar = (b.i.c.o.a) b2.g.a(b.i.c.o.a.class);
                    }
                    Task<PendingDynamicLinkData> a = aVar.a(intent);
                    final AnonymousClass1 r1 = new AnonymousClass1(emitter);
                    a.d(new d() { // from class: com.discord.stores.StoreDynamicLink$sam$com_google_android_gms_tasks_OnFailureListener$0
                        @Override // b.i.a.f.n.d
                        public final /* synthetic */ void onFailure(@RecentlyNonNull Exception exc) {
                            m.checkNotNullExpressionValue(Function1.this.invoke(exc), "invoke(...)");
                        }
                    }).f(new e<PendingDynamicLinkData>() { // from class: com.discord.stores.StoreDynamicLink$getDynamicLinkObservable$firebaseDynamicLinks$1.2
                        /* JADX WARN: Code restructure failed: missing block: B:9:0x0012, code lost:
                            if (r1 != null) goto L11;
                         */
                        /*
                            Code decompiled incorrectly, please refer to instructions dump.
                            To view partially-correct add '--show-bad-code' argument
                        */
                        public final void onSuccess(com.google.firebase.dynamiclinks.PendingDynamicLinkData r3) {
                            /*
                                r2 = this;
                                rx.Emitter r0 = rx.Emitter.this
                                if (r3 == 0) goto L15
                                com.google.firebase.dynamiclinks.internal.DynamicLinkData r3 = r3.a
                                r1 = 0
                                if (r3 != 0) goto La
                                goto L12
                            La:
                                java.lang.String r3 = r3.k
                                if (r3 == 0) goto L12
                                android.net.Uri r1 = android.net.Uri.parse(r3)
                            L12:
                                if (r1 == 0) goto L15
                                goto L17
                            L15:
                                android.net.Uri r1 = android.net.Uri.EMPTY
                            L17:
                                r0.onNext(r1)
                                rx.Emitter r3 = rx.Emitter.this
                                r3.onCompleted()
                                return
                            */
                            throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreDynamicLink$getDynamicLinkObservable$firebaseDynamicLinks$1.AnonymousClass2.onSuccess(com.google.firebase.dynamiclinks.PendingDynamicLinkData):void");
                        }
                    });
                }
                Task<PendingDynamicLinkData> a2 = aVar.a(intent);
                final Function1 r12 = new AnonymousClass1(emitter);
                a2.d(new d() { // from class: com.discord.stores.StoreDynamicLink$sam$com_google_android_gms_tasks_OnFailureListener$0
                    @Override // b.i.a.f.n.d
                    public final /* synthetic */ void onFailure(@RecentlyNonNull Exception exc) {
                        m.checkNotNullExpressionValue(Function1.this.invoke(exc), "invoke(...)");
                    }
                }).f(new e<PendingDynamicLinkData>() { // from class: com.discord.stores.StoreDynamicLink$getDynamicLinkObservable$firebaseDynamicLinks$1.2
                    /*
                        Code decompiled incorrectly, please refer to instructions dump.
                        To view partially-correct add '--show-bad-code' argument
                    */
                    public final void onSuccess(com.google.firebase.dynamiclinks.PendingDynamicLinkData r3) {
                        /*
                            r2 = this;
                            rx.Emitter r0 = rx.Emitter.this
                            if (r3 == 0) goto L15
                            com.google.firebase.dynamiclinks.internal.DynamicLinkData r3 = r3.a
                            r1 = 0
                            if (r3 != 0) goto La
                            goto L12
                        La:
                            java.lang.String r3 = r3.k
                            if (r3 == 0) goto L12
                            android.net.Uri r1 = android.net.Uri.parse(r3)
                        L12:
                            if (r1 == 0) goto L15
                            goto L17
                        L15:
                            android.net.Uri r1 = android.net.Uri.EMPTY
                        L17:
                            r0.onNext(r1)
                            rx.Emitter r3 = rx.Emitter.this
                            r3.onCompleted()
                            return
                        */
                        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreDynamicLink$getDynamicLinkObservable$firebaseDynamicLinks$1.AnonymousClass2.onSuccess(com.google.firebase.dynamiclinks.PendingDynamicLinkData):void");
                    }
                });
            }
        }, Emitter.BackpressureMode.BUFFER);
        m.checkNotNullExpressionValue(n, "Observable.create({\n    ….BackpressureMode.BUFFER)");
        Observable<Uri> L = Observable.H(new j0.l.e.k(Uri.EMPTY).p(1000L, TimeUnit.MILLISECONDS), n).L(StoreDynamicLink$getDynamicLinkObservable$1.INSTANCE);
        m.checkNotNullExpressionValue(L, "Observable\n        .merg…ErrorReturn { Uri.EMPTY }");
        return L;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleDataReceived(DynamicLinkData dynamicLinkData, Context context) {
        String guildTemplateCode;
        String inviteCode;
        this.dispatcher.schedule(new StoreDynamicLink$handleDataReceived$1(this, dynamicLinkData));
        if (dynamicLinkData != null && (inviteCode = dynamicLinkData.getInviteCode()) != null && (!t.isBlank(inviteCode))) {
            IntentUtils.RouteBuilders routeBuilders = IntentUtils.RouteBuilders.INSTANCE;
            String inviteCode2 = dynamicLinkData.getInviteCode();
            Objects.requireNonNull(inviteCode2, "null cannot be cast to non-null type kotlin.CharSequence");
            IntentUtils.consumeRoutingIntent$default(IntentUtils.INSTANCE, routeBuilders.selectInvite(w.trim(inviteCode2).toString(), StoreInviteSettings.LOCATION_DEEPLINK), context, null, 4, null);
        } else if (dynamicLinkData == null || (guildTemplateCode = dynamicLinkData.getGuildTemplateCode()) == null || !(!t.isBlank(guildTemplateCode))) {
            if ((dynamicLinkData != null ? dynamicLinkData.getUri() : null) != null) {
                Intent data = new Intent().setData(dynamicLinkData.getUri());
                m.checkNotNullExpressionValue(data, "Intent().setData(data.uri)");
                IntentUtils.consumeRoutingIntent$default(IntentUtils.INSTANCE, data, context, null, 4, null);
            }
        } else {
            IntentUtils.RouteBuilders routeBuilders2 = IntentUtils.RouteBuilders.INSTANCE;
            String guildTemplateCode2 = dynamicLinkData.getGuildTemplateCode();
            Objects.requireNonNull(guildTemplateCode2, "null cannot be cast to non-null type kotlin.CharSequence");
            IntentUtils.consumeRoutingIntent$default(IntentUtils.INSTANCE, routeBuilders2.selectGuildTemplate(w.trim(guildTemplateCode2).toString(), StoreInviteSettings.LOCATION_DEEPLINK), context, null, 4, null);
        }
    }

    public final void storeLinkIfExists(Intent intent, Context context) {
        m.checkNotNullParameter(intent, "intent");
        m.checkNotNullParameter(context, "context");
        Observable<R> F = getDynamicLinkObservable(intent).F(StoreDynamicLink$storeLinkIfExists$1.INSTANCE);
        m.checkNotNullExpressionValue(F, "getDynamicLinkObservable…ode, authToken)\n        }");
        Observable t = ObservableExtensionsKt.computationBuffered(F).q().t(StoreDynamicLink$storeLinkIfExists$2.INSTANCE);
        m.checkNotNullExpressionValue(t, "getDynamicLinkObservable…  )\n          }\n        }");
        ObservableExtensionsKt.appSubscribe(t, StoreDynamicLink.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreDynamicLink$storeLinkIfExists$3(this, context));
    }
}
