package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.FragmentManager;
import b.a.d.o;
import com.discord.app.AppComponent;
import com.discord.models.domain.ModelCall;
import com.discord.stores.StoreStream;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import j0.k.b;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subjects.SerializedSubject;
/* compiled from: StoreCalls.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000~\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u00102\u001a\u000201¢\u0006\u0004\b?\u0010@J\u0019\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ1\u0010\u000f\u001a\u00020\u00042\n\u0010\u000b\u001a\u00060\tj\u0002`\n2\u0014\u0010\u000e\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\r\u0012\u0004\u0012\u00020\u00040\fH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J!\u0010\u0012\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\r0\u00112\n\u0010\u000b\u001a\u00060\tj\u0002`\n¢\u0006\u0004\b\u0012\u0010\u0013JC\u0010\u001c\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u00182\n\u0010\u000b\u001a\u00060\tj\u0002`\n2\u0010\b\u0002\u0010\u001b\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u001a¢\u0006\u0004\b\u001c\u0010\u001dJ/\u0010!\u001a\u00020\u00042\n\u0010\u000b\u001a\u00060\tj\u0002`\n2\u0014\b\u0002\u0010 \u001a\u000e\u0012\b\u0012\u00060\tj\u0002`\u001f\u0018\u00010\u001e¢\u0006\u0004\b!\u0010\"J/\u0010#\u001a\u00020\u00042\n\u0010\u000b\u001a\u00060\tj\u0002`\n2\u0014\b\u0002\u0010 \u001a\u000e\u0012\b\u0012\u00060\tj\u0002`\u001f\u0018\u00010\u001e¢\u0006\u0004\b#\u0010\"J\r\u0010$\u001a\u00020\u0004¢\u0006\u0004\b$\u0010\bJ\u0015\u0010&\u001a\u00020\u00042\u0006\u0010%\u001a\u00020\u0002¢\u0006\u0004\b&\u0010\u0006J\u0019\u0010)\u001a\u00020\u00042\n\u0010(\u001a\u00060\tj\u0002`'¢\u0006\u0004\b)\u0010*J\u0019\u0010+\u001a\u00020\u00042\n\u0010\u000b\u001a\u00060\tj\u0002`\u001f¢\u0006\u0004\b+\u0010*J\u0015\u0010,\u001a\u00020\u00042\u0006\u0010\u001c\u001a\u00020\r¢\u0006\u0004\b,\u0010-J\u0015\u0010/\u001a\u00020\u00042\u0006\u0010.\u001a\u00020\r¢\u0006\u0004\b/\u0010-R\u0016\u0010%\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b%\u00100R\u0016\u00102\u001a\u0002018\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b2\u00103R>\u00106\u001a*\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0006\u0012\u0004\u0018\u00010\r04j\u0014\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0006\u0012\u0004\u0018\u00010\r`58\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b6\u00107R\u001a\u00108\u001a\u00060\tj\u0002`'8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b8\u00109R\u001a\u0010:\u001a\u00060\tj\u0002`\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b:\u00109R×\u0001\u0010=\u001aÂ\u0001\u0012^\u0012\\\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0006\u0012\u0004\u0018\u00010\r <*.\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0006\u0012\u0004\u0018\u00010\r\u0018\u000104j\u0016\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0006\u0012\u0004\u0018\u00010\r\u0018\u0001`504j\u0014\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0006\u0012\u0004\u0018\u00010\r`5\u0012^\u0012\\\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0006\u0012\u0004\u0018\u00010\r <*.\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0006\u0012\u0004\u0018\u00010\r\u0018\u000104j\u0016\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0006\u0012\u0004\u0018\u00010\r\u0018\u0001`504j\u0014\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0006\u0012\u0004\u0018\u00010\r`50;8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b=\u0010>¨\u0006A"}, d2 = {"Lcom/discord/stores/StoreCalls;", "", "", "updated", "", "callSubjectUpdate", "(Z)V", "callConnect", "()V", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lkotlin/Function1;", "Lcom/discord/models/domain/ModelCall;", "onFoundCall", "findCall", "(JLkotlin/jvm/functions/Function1;)V", "Lrx/Observable;", "get", "(J)Lrx/Observable;", "Lcom/discord/app/AppComponent;", "appComponent", "Landroid/content/Context;", "context", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Lkotlin/Function0;", "onError", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/app/AppComponent;Landroid/content/Context;Landroidx/fragment/app/FragmentManager;JLkotlin/jvm/functions/Function0;)V", "", "Lcom/discord/primitives/UserId;", "recipients", "ring", "(JLjava/util/List;)V", "stopRinging", "handleConnectionOpen", "connectionReady", "handleConnectionReady", "Lcom/discord/primitives/GuildId;", "guildId", "handleGuildSelect", "(J)V", "handleChannelSelect", "handleCallCreateOrUpdate", "(Lcom/discord/models/domain/ModelCall;)V", "callDelete", "handleCallDelete", "Z", "Lcom/discord/stores/StoreStream;", "stream", "Lcom/discord/stores/StoreStream;", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "calls", "Ljava/util/HashMap;", "selectedGuildId", "J", "selectedChannelId", "Lrx/subjects/SerializedSubject;", "kotlin.jvm.PlatformType", "callsSubject", "Lrx/subjects/SerializedSubject;", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreCalls {
    private final HashMap<Long, ModelCall> calls = new HashMap<>();
    private final SerializedSubject<HashMap<Long, ModelCall>, HashMap<Long, ModelCall>> callsSubject = new SerializedSubject<>(BehaviorSubject.k0());
    private boolean connectionReady;
    private long selectedChannelId;
    private long selectedGuildId;
    private final StoreStream stream;

    public StoreCalls(StoreStream storeStream) {
        m.checkNotNullParameter(storeStream, "stream");
        this.stream = storeStream;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void call$default(StoreCalls storeCalls, AppComponent appComponent, Context context, FragmentManager fragmentManager, long j, Function0 function0, int i, Object obj) {
        Function0<Unit> function02 = function0;
        if ((i & 16) != 0) {
            function02 = null;
        }
        storeCalls.call(appComponent, context, fragmentManager, j, function02);
    }

    private final void callConnect() {
        if (this.connectionReady && this.selectedGuildId == 0) {
            long j = this.selectedChannelId;
            if (j > 0 && !this.calls.containsKey(Long.valueOf(j))) {
                this.calls.put(Long.valueOf(this.selectedChannelId), null);
                this.stream.getGatewaySocket$app_productionGoogleRelease().callConnect(this.selectedChannelId);
            }
        }
    }

    private final void callSubjectUpdate(boolean z2) {
        if (z2) {
            SerializedSubject<HashMap<Long, ModelCall>, HashMap<Long, ModelCall>> serializedSubject = this.callsSubject;
            serializedSubject.k.onNext(new HashMap<>(this.calls));
        }
    }

    public static /* synthetic */ void callSubjectUpdate$default(StoreCalls storeCalls, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = true;
        }
        storeCalls.callSubjectUpdate(z2);
    }

    private final void findCall(long j, Function1<? super ModelCall, Unit> function1) {
        Observable<R> k = get(j).k(o.c(StoreCalls$findCall$1.INSTANCE, null, 3L, TimeUnit.SECONDS));
        m.checkNotNullExpressionValue(k, "get(channelId)\n        .…l?, 3, TimeUnit.SECONDS))");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.takeSingleUntilTimeout$default(k, 0L, false, 3, null), StoreCalls.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, function1);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void ring$default(StoreCalls storeCalls, long j, List list, int i, Object obj) {
        if ((i & 2) != 0) {
            list = null;
        }
        storeCalls.ring(j, list);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void stopRinging$default(StoreCalls storeCalls, long j, List list, int i, Object obj) {
        if ((i & 2) != 0) {
            list = null;
        }
        storeCalls.stopRinging(j, list);
    }

    public final void call(AppComponent appComponent, Context context, FragmentManager fragmentManager, long j, Function0<Unit> function0) {
        m.checkNotNullParameter(appComponent, "appComponent");
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        StoreCalls$call$1 storeCalls$call$1 = new StoreCalls$call$1(this, j);
        StoreCalls$call$2 storeCalls$call$2 = new StoreCalls$call$2(this, j, appComponent, context, storeCalls$call$1, function0, fragmentManager);
        StoreStream.Companion companion = StoreStream.Companion;
        Observable Z = Observable.j0(companion.getChannels().observeChannel(j).k(o.c(StoreCalls$call$3.INSTANCE, null, 5000L, TimeUnit.MILLISECONDS)), companion.getVoiceStates().observeForPrivateChannels(j), StoreCalls$call$4.INSTANCE).Z(1);
        m.checkNotNullExpressionValue(Z, "Observable\n        .zip(…tates) }\n        .take(1)");
        ObservableExtensionsKt.appSubscribe(Z, StoreCalls.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreCalls$call$5(storeCalls$call$2, storeCalls$call$1));
    }

    public final Observable<ModelCall> get(final long j) {
        Observable<R> F = this.callsSubject.F(new b<HashMap<Long, ModelCall>, ModelCall>() { // from class: com.discord.stores.StoreCalls$get$1
            public final ModelCall call(HashMap<Long, ModelCall> hashMap) {
                return hashMap.get(Long.valueOf(j));
            }
        });
        m.checkNotNullExpressionValue(F, "callsSubject\n          .…lls -> calls[channelId] }");
        Observable<ModelCall> q = ObservableExtensionsKt.computationLatest(F).q();
        m.checkNotNullExpressionValue(q, "callsSubject\n          .…  .distinctUntilChanged()");
        return q;
    }

    public final void handleCallCreateOrUpdate(ModelCall modelCall) {
        m.checkNotNullParameter(modelCall, NotificationCompat.CATEGORY_CALL);
        long channelId = modelCall.getChannelId();
        boolean z2 = true;
        if (!m.areEqual(modelCall, this.calls.get(Long.valueOf(channelId)))) {
            this.calls.put(Long.valueOf(channelId), modelCall);
        } else {
            z2 = false;
        }
        callSubjectUpdate(z2);
    }

    public final void handleCallDelete(ModelCall modelCall) {
        m.checkNotNullParameter(modelCall, "callDelete");
        long channelId = modelCall.getChannelId();
        if (this.calls.containsKey(Long.valueOf(channelId))) {
            this.calls.put(Long.valueOf(channelId), null);
            callSubjectUpdate$default(this, false, 1, null);
        }
    }

    public final void handleChannelSelect(long j) {
        this.selectedChannelId = j;
        callConnect();
    }

    public final void handleConnectionOpen() {
        this.calls.clear();
        callSubjectUpdate$default(this, false, 1, null);
        callConnect();
    }

    public final void handleConnectionReady(boolean z2) {
        this.connectionReady = z2;
        callConnect();
    }

    public final void handleGuildSelect(long j) {
        this.selectedGuildId = j;
        callConnect();
    }

    public final void ring(long j, List<Long> list) {
        findCall(j, new StoreCalls$ring$1(this, list));
    }

    public final void stopRinging(long j, List<Long> list) {
        findCall(j, new StoreCalls$stopRinging$1(this, j, list));
    }
}
