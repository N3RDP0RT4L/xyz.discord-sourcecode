package com.discord.stores;

import com.discord.models.domain.ModelTypingResponse;
import com.discord.stores.StoreSlowMode;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreUserTyping.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/ModelTypingResponse;", "response", "", "invoke", "(Lcom/discord/models/domain/ModelTypingResponse;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserTyping$setUserTyping$1 extends o implements Function1<ModelTypingResponse, Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ StoreUserTyping this$0;

    /* compiled from: StoreUserTyping.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreUserTyping$setUserTyping$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ long $messageSendCooldownSecs;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(long j) {
            super(0);
            this.$messageSendCooldownSecs = j;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            StoreStream.Companion.getSlowMode().onCooldown(StoreUserTyping$setUserTyping$1.this.$channelId, this.$messageSendCooldownSecs, StoreSlowMode.Type.MessageSend.INSTANCE);
        }
    }

    /* compiled from: StoreUserTyping.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreUserTyping$setUserTyping$1$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2 extends o implements Function0<Unit> {
        public final /* synthetic */ long $threadCreateCooldown;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass2(long j) {
            super(0);
            this.$threadCreateCooldown = j;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            StoreStream.Companion.getSlowMode().onCooldown(StoreUserTyping$setUserTyping$1.this.$channelId, this.$threadCreateCooldown, StoreSlowMode.Type.ThreadCreate.INSTANCE);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreUserTyping$setUserTyping$1(StoreUserTyping storeUserTyping, long j) {
        super(1);
        this.this$0 = storeUserTyping;
        this.$channelId = j;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ModelTypingResponse modelTypingResponse) {
        invoke2(modelTypingResponse);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ModelTypingResponse modelTypingResponse) {
        Dispatcher dispatcher;
        Long threadCreateCooldownMs;
        Dispatcher dispatcher2;
        Long messageSendCooldownMs;
        long longValue = (modelTypingResponse == null || (messageSendCooldownMs = modelTypingResponse.getMessageSendCooldownMs()) == null) ? 0L : messageSendCooldownMs.longValue();
        if (longValue > 0) {
            dispatcher2 = this.this$0.dispatcher;
            dispatcher2.schedule(new AnonymousClass1(longValue));
        }
        long longValue2 = (modelTypingResponse == null || (threadCreateCooldownMs = modelTypingResponse.getThreadCreateCooldownMs()) == null) ? 0L : threadCreateCooldownMs.longValue();
        if (longValue2 > 0) {
            dispatcher = this.this$0.dispatcher;
            dispatcher.schedule(new AnonymousClass2(longValue2));
        }
    }
}
