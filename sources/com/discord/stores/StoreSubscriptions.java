package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelSubscription;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.rest.RestAPI;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreSubscriptions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001#B\u001f\u0012\u0006\u0010\u0016\u001a\u00020\u0015\u0012\u0006\u0010\u001e\u001a\u00020\u001d\u0012\u0006\u0010\u0019\u001a\u00020\u0018¢\u0006\u0004\b!\u0010\"J\u000f\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001d\u0010\b\u001a\u00020\u00022\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u0003¢\u0006\u0004\b\b\u0010\tJ\u000f\u0010\n\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\n\u0010\u0004J\r\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\f\u0010\rJ\u0013\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000b0\u000e¢\u0006\u0004\b\u000f\u0010\u0010J\u000f\u0010\u0011\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0011\u0010\u0004J\u000f\u0010\u0012\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0012\u0010\u0004J\u000f\u0010\u0013\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0013\u0010\u0004J\r\u0010\u0014\u001a\u00020\u0002¢\u0006\u0004\b\u0014\u0010\u0004R\u0016\u0010\u0016\u001a\u00020\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0019\u001a\u00020\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001b\u001a\u00020\u000b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\u0016\u0010\u001e\u001a\u00020\u001d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR\u0016\u0010 \u001a\u00020\u000b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b \u0010\u001c¨\u0006$"}, d2 = {"Lcom/discord/stores/StoreSubscriptions;", "Lcom/discord/stores/StoreV2;", "", "handleSubscriptionsFetchStart", "()V", "", "Lcom/discord/models/domain/ModelSubscription;", "subscriptions", "handleSubscriptionsFetchSuccess", "(Ljava/util/List;)V", "handleSubscriptionsFetchFailure", "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;", "getSubscriptions", "()Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;", "Lrx/Observable;", "observeSubscriptions", "()Lrx/Observable;", "handlePreLogout", "handleUserSubscriptionsUpdate", "snapshotData", "fetchSubscriptions", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "subscriptionsStateSnapshot", "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "subscriptionsState", HookHelper.constructorName, "(Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/rest/RestAPI;)V", "SubscriptionsState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreSubscriptions extends StoreV2 {
    private final Dispatcher dispatcher;
    private final ObservationDeck observationDeck;
    private final RestAPI restAPI;
    private SubscriptionsState subscriptionsState;
    private SubscriptionsState subscriptionsStateSnapshot;

    /* compiled from: StoreSubscriptions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\u0004\u0005\u0006\u0007B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0004\b\t\n\u000b¨\u0006\f"}, d2 = {"Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;", "", HookHelper.constructorName, "()V", "Failure", "Loaded", "Loading", "Unfetched", "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;", "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loading;", "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Failure;", "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Unfetched;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class SubscriptionsState {

        /* compiled from: StoreSubscriptions.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Failure;", "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Failure extends SubscriptionsState {
            public static final Failure INSTANCE = new Failure();

            private Failure() {
                super(null);
            }
        }

        /* compiled from: StoreSubscriptions.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J \u0010\u0007\u001a\u00020\u00002\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;", "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;", "", "Lcom/discord/models/domain/ModelSubscription;", "component1", "()Ljava/util/List;", "subscriptions", "copy", "(Ljava/util/List;)Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getSubscriptions", HookHelper.constructorName, "(Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Loaded extends SubscriptionsState {
            private final List<ModelSubscription> subscriptions;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loaded(List<ModelSubscription> list) {
                super(null);
                m.checkNotNullParameter(list, "subscriptions");
                this.subscriptions = list;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Loaded copy$default(Loaded loaded, List list, int i, Object obj) {
                if ((i & 1) != 0) {
                    list = loaded.subscriptions;
                }
                return loaded.copy(list);
            }

            public final List<ModelSubscription> component1() {
                return this.subscriptions;
            }

            public final Loaded copy(List<ModelSubscription> list) {
                m.checkNotNullParameter(list, "subscriptions");
                return new Loaded(list);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Loaded) && m.areEqual(this.subscriptions, ((Loaded) obj).subscriptions);
                }
                return true;
            }

            public final List<ModelSubscription> getSubscriptions() {
                return this.subscriptions;
            }

            public int hashCode() {
                List<ModelSubscription> list = this.subscriptions;
                if (list != null) {
                    return list.hashCode();
                }
                return 0;
            }

            public String toString() {
                return a.K(a.R("Loaded(subscriptions="), this.subscriptions, ")");
            }
        }

        /* compiled from: StoreSubscriptions.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loading;", "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Loading extends SubscriptionsState {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        /* compiled from: StoreSubscriptions.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Unfetched;", "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Unfetched extends SubscriptionsState {
            public static final Unfetched INSTANCE = new Unfetched();

            private Unfetched() {
                super(null);
            }
        }

        private SubscriptionsState() {
        }

        public /* synthetic */ SubscriptionsState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public StoreSubscriptions(ObservationDeck observationDeck, Dispatcher dispatcher, RestAPI restAPI) {
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(restAPI, "restAPI");
        this.observationDeck = observationDeck;
        this.dispatcher = dispatcher;
        this.restAPI = restAPI;
        SubscriptionsState.Unfetched unfetched = SubscriptionsState.Unfetched.INSTANCE;
        this.subscriptionsState = unfetched;
        this.subscriptionsStateSnapshot = unfetched;
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleSubscriptionsFetchFailure() {
        this.subscriptionsState = SubscriptionsState.Failure.INSTANCE;
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleSubscriptionsFetchStart() {
        this.subscriptionsState = SubscriptionsState.Loading.INSTANCE;
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleSubscriptionsFetchSuccess(List<ModelSubscription> list) {
        this.subscriptionsState = new SubscriptionsState.Loaded(list);
        markChanged();
    }

    public final void fetchSubscriptions() {
        this.dispatcher.schedule(new StoreSubscriptions$fetchSubscriptions$1(this));
    }

    public final SubscriptionsState getSubscriptions() {
        return this.subscriptionsStateSnapshot;
    }

    @StoreThread
    public final void handlePreLogout() {
        this.subscriptionsState = SubscriptionsState.Unfetched.INSTANCE;
        markChanged();
    }

    @StoreThread
    public final void handleUserSubscriptionsUpdate() {
        fetchSubscriptions();
    }

    public final Observable<SubscriptionsState> observeSubscriptions() {
        Observable<SubscriptionsState> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreSubscriptions$observeSubscriptions$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR…  .distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        super.snapshotData();
        SubscriptionsState subscriptionsState = this.subscriptionsState;
        if (subscriptionsState instanceof SubscriptionsState.Loaded) {
            SubscriptionsState.Loaded loaded = (SubscriptionsState.Loaded) subscriptionsState;
            subscriptionsState = loaded.copy(new ArrayList(loaded.getSubscriptions()));
        } else if (!m.areEqual(subscriptionsState, SubscriptionsState.Failure.INSTANCE) && !m.areEqual(subscriptionsState, SubscriptionsState.Loading.INSTANCE) && !m.areEqual(subscriptionsState, SubscriptionsState.Unfetched.INSTANCE)) {
            throw new NoWhenBranchMatchedException();
        }
        this.subscriptionsStateSnapshot = subscriptionsState;
    }
}
