package com.discord.stores;

import com.discord.analytics.generated.events.network_action.TrackNetworkActionUserSurveyFetch;
import com.discord.analytics.generated.traits.TrackNetworkMetadataReceiver;
import com.discord.api.user.UserSurvey;
import com.discord.api.user.UserSurveyFetchResponse;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreUserSurvey.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/user/UserSurveyFetchResponse;", "response", "Lcom/discord/analytics/generated/traits/TrackNetworkMetadataReceiver;", "invoke", "(Lcom/discord/api/user/UserSurveyFetchResponse;)Lcom/discord/analytics/generated/traits/TrackNetworkMetadataReceiver;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserSurvey$fetchUserSurvey$1 extends o implements Function1<UserSurveyFetchResponse, TrackNetworkMetadataReceiver> {
    public static final StoreUserSurvey$fetchUserSurvey$1 INSTANCE = new StoreUserSurvey$fetchUserSurvey$1();

    public StoreUserSurvey$fetchUserSurvey$1() {
        super(1);
    }

    public final TrackNetworkMetadataReceiver invoke(UserSurveyFetchResponse userSurveyFetchResponse) {
        UserSurvey a;
        return new TrackNetworkActionUserSurveyFetch((userSurveyFetchResponse == null || (a = userSurveyFetchResponse.a()) == null) ? null : a.e());
    }
}
