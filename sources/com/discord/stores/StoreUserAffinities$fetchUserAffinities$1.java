package com.discord.stores;

import com.discord.models.domain.ModelUserAffinities;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreUserAffinities.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/ModelUserAffinities;", "affinities", "", "invoke", "(Lcom/discord/models/domain/ModelUserAffinities;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserAffinities$fetchUserAffinities$1 extends o implements Function1<ModelUserAffinities, Unit> {
    public final /* synthetic */ StoreUserAffinities this$0;

    /* compiled from: StoreUserAffinities.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreUserAffinities$fetchUserAffinities$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ ModelUserAffinities $affinities;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(ModelUserAffinities modelUserAffinities) {
            super(0);
            this.$affinities = modelUserAffinities;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            StoreUserAffinities$fetchUserAffinities$1.this.this$0.handleUserAffinitiesFetchSuccess(this.$affinities);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreUserAffinities$fetchUserAffinities$1(StoreUserAffinities storeUserAffinities) {
        super(1);
        this.this$0 = storeUserAffinities;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ModelUserAffinities modelUserAffinities) {
        invoke2(modelUserAffinities);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ModelUserAffinities modelUserAffinities) {
        Dispatcher dispatcher;
        m.checkNotNullParameter(modelUserAffinities, "affinities");
        dispatcher = this.this$0.dispatcher;
        dispatcher.schedule(new AnonymousClass1(modelUserAffinities));
    }
}
