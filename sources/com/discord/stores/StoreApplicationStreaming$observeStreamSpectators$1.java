package com.discord.stores;

import d0.z.d.o;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreApplicationStreaming.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\b\u001a\u001c\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0004j\u0002`\u00050\u00030\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "", "Lcom/discord/primitives/StreamKey;", "", "", "Lcom/discord/primitives/UserId;", "invoke", "()Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreApplicationStreaming$observeStreamSpectators$1 extends o implements Function0<Map<String, ? extends List<? extends Long>>> {
    public final /* synthetic */ StoreApplicationStreaming this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreApplicationStreaming$observeStreamSpectators$1(StoreApplicationStreaming storeApplicationStreaming) {
        super(0);
        this.this$0 = storeApplicationStreaming;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Map<String, ? extends List<? extends Long>> invoke() {
        Map<String, ? extends List<? extends Long>> map;
        map = this.this$0.streamSpectatorsSnapshot;
        return map;
    }
}
