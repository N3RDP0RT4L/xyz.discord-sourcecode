package com.discord.stores;

import andhook.lib.HookHelper;
import com.discord.api.emoji.GuildEmojisUpdate;
import com.discord.models.domain.emoji.ModelEmojiGuild;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.t.h0;
import d0.z.d.m;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: StoreEmojiGuild.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010$\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010)\u001a\u00020(\u0012\u0006\u0010#\u001a\u00020\"\u0012\u0006\u0010&\u001a\u00020%¢\u0006\u0004\b-\u0010.J)\u0010\t\u001a\u00020\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u0003¢\u0006\u0004\b\t\u0010\nJ!\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u000b\u0010\fJ'\u0010\u000e\u001a\u0010\u0012\f\u0012\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00050\r2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0019\u0010\u0010\u001a\u00020\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0019\u0010\u0012\u001a\u00020\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0012\u0010\u0011J\r\u0010\u0013\u001a\u00020\b¢\u0006\u0004\b\u0013\u0010\u0014J\u0017\u0010\u0017\u001a\u00020\b2\u0006\u0010\u0016\u001a\u00020\u0015H\u0007¢\u0006\u0004\b\u0017\u0010\u0018J!\u0010\u001a\u001a\u00020\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0019\u001a\u00020\u0002¢\u0006\u0004\b\u001a\u0010\u001bJ\u000f\u0010\u001c\u001a\u00020\bH\u0016¢\u0006\u0004\b\u001c\u0010\u0014R,\u0010\u001e\u001a\u0018\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u001d8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR\u001a\u0010 \u001a\u00060\u0002j\u0002`\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b \u0010!R\u0016\u0010#\u001a\u00020\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$R\u0016\u0010&\u001a\u00020%8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b&\u0010'R\u0016\u0010)\u001a\u00020(8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b)\u0010*R,\u0010,\u001a\u0018\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050+8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b,\u0010\u001f¨\u0006/"}, d2 = {"Lcom/discord/stores/StoreEmojiGuild;", "Lcom/discord/stores/StoreV2;", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "Lcom/discord/models/domain/emoji/ModelEmojiGuild;", "emojis", "", "handleGuildEmojisLoaded", "(JLjava/util/List;)V", "getGuildEmoji", "(J)Ljava/util/List;", "Lrx/Observable;", "observeGuildEmoji", "(J)Lrx/Observable;", "fetchGuildEmoji", "(J)V", "activate", "deactivate", "()V", "Lcom/discord/api/emoji/GuildEmojisUpdate;", "emojiUpdate", "handleEmojiUpdate", "(Lcom/discord/api/emoji/GuildEmojisUpdate;)V", "emojiId", "deleteEmoji", "(JJ)V", "snapshotData", "", "guildEmojiSnapshot", "Ljava/util/Map;", "activeGuildId", "J", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "", "guildEmoji", HookHelper.constructorName, "(Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/rest/RestAPI;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreEmojiGuild extends StoreV2 {
    private final Dispatcher dispatcher;
    private final ObservationDeck observationDeck;
    private final RestAPI restAPI;
    private final Map<Long, List<ModelEmojiGuild>> guildEmoji = new LinkedHashMap();
    private Map<Long, ? extends List<ModelEmojiGuild>> guildEmojiSnapshot = h0.emptyMap();
    private long activeGuildId = -1;

    public StoreEmojiGuild(ObservationDeck observationDeck, Dispatcher dispatcher, RestAPI restAPI) {
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(restAPI, "restAPI");
        this.observationDeck = observationDeck;
        this.dispatcher = dispatcher;
        this.restAPI = restAPI;
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleGuildEmojisLoaded(long j, List<ModelEmojiGuild> list) {
        this.dispatcher.schedule(new StoreEmojiGuild$handleGuildEmojisLoaded$1(this, list, j));
    }

    public final void activate(long j) {
        this.dispatcher.schedule(new StoreEmojiGuild$activate$1(this, j));
    }

    public final void deactivate() {
        this.dispatcher.schedule(new StoreEmojiGuild$deactivate$1(this));
    }

    public final void deleteEmoji(long j, long j2) {
        this.dispatcher.schedule(new StoreEmojiGuild$deleteEmoji$1(this, j, j2));
    }

    public final void fetchGuildEmoji(long j) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.getGuildEmojis(j), false, 1, null), StoreEmojiGuild.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreEmojiGuild$fetchGuildEmoji$1(this, j));
    }

    public final List<ModelEmojiGuild> getGuildEmoji(long j) {
        return this.guildEmojiSnapshot.get(Long.valueOf(j));
    }

    @StoreThread
    public final void handleEmojiUpdate(GuildEmojisUpdate guildEmojisUpdate) {
        m.checkNotNullParameter(guildEmojisUpdate, "emojiUpdate");
        long c = guildEmojisUpdate.c();
        long j = this.activeGuildId;
        if (c == j) {
            fetchGuildEmoji(j);
        }
    }

    public final Observable<List<ModelEmojiGuild>> observeGuildEmoji(long j) {
        Observable<List<ModelEmojiGuild>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreEmojiGuild$observeGuildEmoji$1(this, j), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR…  .distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        super.snapshotData();
        this.guildEmojiSnapshot = new HashMap(this.guildEmoji);
    }
}
