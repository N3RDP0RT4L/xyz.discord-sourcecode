package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.Context;
import com.adjust.sdk.Constants;
import com.discord.models.domain.ModelPayload;
import com.discord.models.phone.PhoneCountryCode;
import com.discord.utilities.persister.Persister;
import com.google.gson.Gson;
import d0.t.k;
import d0.z.d.m;
import java.io.InputStreamReader;
import java.util.List;
import kotlin.Metadata;
/* compiled from: StorePhone.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010&\u001a\u00020%¢\u0006\u0004\b(\u0010)J\u001d\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\t\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\t\u0010\nJ\r\u0010\u000b\u001a\u00020\u0005¢\u0006\u0004\b\u000b\u0010\fJ\u0015\u0010\u000f\u001a\u00020\b2\u0006\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b\u000f\u0010\u0010J\u0015\u0010\u0012\u001a\u00020\b2\u0006\u0010\u0011\u001a\u00020\u0005¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0016\u001a\u00020\b2\u0006\u0010\u0015\u001a\u00020\u0014H\u0007¢\u0006\u0004\b\u0016\u0010\u0017J\u000f\u0010\u0018\u001a\u00020\bH\u0017¢\u0006\u0004\b\u0018\u0010\u0019R\u001c\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00050\u001a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR0\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006@BX\u0086.¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010!R\u0016\u0010\"\u001a\u00020\u00058\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\"\u0010#R\u0016\u0010$\u001a\u00020\u00058\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b$\u0010#R\u0016\u0010&\u001a\u00020%8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b&\u0010'¨\u0006*"}, d2 = {"Lcom/discord/stores/StorePhone;", "Lcom/discord/stores/StoreV2;", "Landroid/content/Context;", "context", "", "Lcom/discord/models/phone/PhoneCountryCode;", "loadCountryCodesFromDisk", "(Landroid/content/Context;)Ljava/util/List;", "", "init", "(Landroid/content/Context;)V", "getCountryCode", "()Lcom/discord/models/phone/PhoneCountryCode;", "", "alpha2", "updateDefaultCountryCode", "(Ljava/lang/String;)V", "newSelectedCountryCode", "updateSelectedCountryCode", "(Lcom/discord/models/phone/PhoneCountryCode;)V", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "snapshotData", "()V", "Lcom/discord/utilities/persister/Persister;", "selectedCountryCodeCache", "Lcom/discord/utilities/persister/Persister;", "<set-?>", "countryCodes", "Ljava/util/List;", "getCountryCodes", "()Ljava/util/List;", "selectedCountryCode", "Lcom/discord/models/phone/PhoneCountryCode;", "defaultCountryCode", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StorePhone extends StoreV2 {
    private List<PhoneCountryCode> countryCodes;
    private PhoneCountryCode defaultCountryCode;
    private final Dispatcher dispatcher;
    private PhoneCountryCode selectedCountryCode;
    private final Persister<PhoneCountryCode> selectedCountryCodeCache;

    public StorePhone(Dispatcher dispatcher) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        this.dispatcher = dispatcher;
        PhoneCountryCode.Companion companion = PhoneCountryCode.Companion;
        this.defaultCountryCode = companion.getDEFAULT_COUNTRY_CODE();
        this.selectedCountryCode = companion.getMISSING_COUNTRY_CODE();
        this.selectedCountryCodeCache = new Persister<>("CACHE_KEY_PHONE_COUNTRY_CODE_V2", this.selectedCountryCode);
    }

    public static final /* synthetic */ List access$getCountryCodes$p(StorePhone storePhone) {
        List<PhoneCountryCode> list = storePhone.countryCodes;
        if (list == null) {
            m.throwUninitializedPropertyAccessException("countryCodes");
        }
        return list;
    }

    private final List<PhoneCountryCode> loadCountryCodesFromDisk(Context context) {
        InputStreamReader inputStreamReader;
        try {
            Object e = new Gson().e(new InputStreamReader(context.getAssets().open("data/country-codes.json"), Constants.ENCODING), PhoneCountryCode[].class);
            m.checkNotNullExpressionValue(e, "Gson().fromJson(it, Arra…CountryCode>::class.java)");
            th = null;
            return k.toList((Object[]) e);
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    public final PhoneCountryCode getCountryCode() {
        return m.areEqual(this.selectedCountryCode, PhoneCountryCode.Companion.getMISSING_COUNTRY_CODE()) ? this.defaultCountryCode : this.selectedCountryCode;
    }

    public final List<PhoneCountryCode> getCountryCodes() {
        List<PhoneCountryCode> list = this.countryCodes;
        if (list == null) {
            m.throwUninitializedPropertyAccessException("countryCodes");
        }
        return list;
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        String countryCode = modelPayload.getCountryCode();
        if (countryCode != null) {
            updateDefaultCountryCode(countryCode);
        }
    }

    @Override // com.discord.stores.Store
    public void init(Context context) {
        m.checkNotNullParameter(context, "context");
        super.init(context);
        this.countryCodes = loadCountryCodesFromDisk(context);
        this.selectedCountryCode = this.selectedCountryCodeCache.get();
        markChanged();
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        super.snapshotData();
        Persister.set$default(this.selectedCountryCodeCache, this.selectedCountryCode, false, 2, null);
    }

    public final void updateDefaultCountryCode(String str) {
        m.checkNotNullParameter(str, "alpha2");
        this.dispatcher.schedule(new StorePhone$updateDefaultCountryCode$1(this, str));
    }

    public final void updateSelectedCountryCode(PhoneCountryCode phoneCountryCode) {
        m.checkNotNullParameter(phoneCountryCode, "newSelectedCountryCode");
        this.dispatcher.schedule(new StorePhone$updateSelectedCountryCode$1(this, phoneCountryCode));
    }
}
