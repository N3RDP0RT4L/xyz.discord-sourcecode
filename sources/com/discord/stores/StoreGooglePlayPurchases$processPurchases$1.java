package com.discord.stores;

import com.discord.stores.StoreGooglePlayPurchases;
import com.discord.utilities.billing.BillingUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreGooglePlayPurchases.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGooglePlayPurchases$processPurchases$1 extends o implements Function0<Unit> {
    public final /* synthetic */ List $purchases;
    public final /* synthetic */ String $skuType;
    public final /* synthetic */ StoreGooglePlayPurchases this$0;

    /* compiled from: StoreGooglePlayPurchases.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreGooglePlayPurchases$processPurchases$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreGooglePlayPurchases.QueryState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreGooglePlayPurchases.QueryState queryState) {
            invoke2(queryState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreGooglePlayPurchases.QueryState queryState) {
            if (queryState instanceof StoreGooglePlayPurchases.QueryState.NotInProgress) {
                BillingUtils.INSTANCE.verifyPurchases(StoreGooglePlayPurchases$processPurchases$1.this.$purchases);
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGooglePlayPurchases$processPurchases$1(StoreGooglePlayPurchases storeGooglePlayPurchases, List list, String str) {
        super(0);
        this.this$0 = storeGooglePlayPurchases;
        this.$purchases = list;
        this.$skuType = str;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        this.this$0.handlePurchases(this.$purchases, this.$skuType);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.takeSingleUntilTimeout$default(this.this$0.observeQueryState(), 0L, false, 3, null), this.this$0.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
