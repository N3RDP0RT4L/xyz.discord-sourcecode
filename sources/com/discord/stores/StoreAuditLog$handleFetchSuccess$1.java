package com.discord.stores;

import androidx.media.AudioAttributesCompat;
import com.discord.api.channel.Channel;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.user.User;
import com.discord.models.domain.ModelAuditLog;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelGuildIntegration;
import com.discord.models.domain.ModelWebhook;
import com.discord.models.user.CoreUser;
import com.discord.stores.StoreAuditLog;
import d0.d0.f;
import d0.t.g0;
import d0.t.h0;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreAuditLog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAuditLog$handleFetchSuccess$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ ModelAuditLog $newAuditLog;
    public final /* synthetic */ StoreAuditLog this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreAuditLog$handleFetchSuccess$1(StoreAuditLog storeAuditLog, ModelAuditLog modelAuditLog, long j) {
        super(0);
        this.this$0 = storeAuditLog;
        this.$newAuditLog = modelAuditLog;
        this.$guildId = j;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreAuditLog.AuditLogState auditLogState;
        List transformEntries;
        StoreAuditLog.AuditLogState auditLogState2;
        StoreAuditLog.AuditLogState auditLogState3;
        StoreAuditLog.AuditLogState auditLogState4;
        StoreAuditLog.AuditLogState auditLogState5;
        StoreAuditLog.AuditLogState auditLogState6;
        StoreAuditLog.AuditLogState auditLogState7;
        StoreAuditLog.AuditLogState auditLogState8;
        StoreAuditLog.AuditLogState auditLogState9;
        StoreAuditLog.AuditLogState auditLogState10;
        StoreAuditLog.AuditLogState auditLogState11;
        StoreAuditLog.AuditLogState auditLogState12;
        StoreAuditLog.AuditLogState auditLogState13;
        if (this.$newAuditLog.getUsers() != null) {
            auditLogState12 = this.this$0.state;
            Map mutableMap = h0.toMutableMap(auditLogState12.getUsers());
            List<User> users = this.$newAuditLog.getUsers();
            m.checkNotNullExpressionValue(users, "newAuditLog.users");
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(users, 10));
            for (User user : users) {
                m.checkNotNullExpressionValue(user, "it");
                arrayList.add(new CoreUser(user));
            }
            LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(d0.t.o.collectionSizeOrDefault(arrayList, 10)), 16));
            for (Object obj : arrayList) {
                linkedHashMap.put(Long.valueOf(((CoreUser) obj).getId()), obj);
            }
            mutableMap.putAll(linkedHashMap);
            StoreAuditLog storeAuditLog = this.this$0;
            auditLogState13 = storeAuditLog.state;
            storeAuditLog.state = StoreAuditLog.AuditLogState.copy$default(auditLogState13, 0L, mutableMap, null, null, null, null, null, null, null, null, false, 2045, null);
        }
        if (this.$newAuditLog.getWebhooks() != null) {
            auditLogState10 = this.this$0.state;
            List mutableList = u.toMutableList((Collection) auditLogState10.getWebhooks());
            List<ModelWebhook> webhooks = this.$newAuditLog.getWebhooks();
            m.checkNotNullExpressionValue(webhooks, "newAuditLog.webhooks");
            mutableList.addAll(webhooks);
            StoreAuditLog storeAuditLog2 = this.this$0;
            auditLogState11 = storeAuditLog2.state;
            storeAuditLog2.state = StoreAuditLog.AuditLogState.copy$default(auditLogState11, 0L, null, null, mutableList, null, null, null, null, null, null, false, 2039, null);
        }
        if (this.$newAuditLog.getIntegrations() != null) {
            auditLogState8 = this.this$0.state;
            List mutableList2 = u.toMutableList((Collection) auditLogState8.getIntegrations());
            List<ModelGuildIntegration> integrations = this.$newAuditLog.getIntegrations();
            m.checkNotNullExpressionValue(integrations, "newAuditLog.integrations");
            mutableList2.addAll(integrations);
            StoreAuditLog storeAuditLog3 = this.this$0;
            auditLogState9 = storeAuditLog3.state;
            storeAuditLog3.state = StoreAuditLog.AuditLogState.copy$default(auditLogState9, 0L, null, null, null, mutableList2, null, null, null, null, null, false, 2031, null);
        }
        if (this.$newAuditLog.getGuildScheduledEvents() != null) {
            StoreAuditLog storeAuditLog4 = this.this$0;
            auditLogState6 = storeAuditLog4.state;
            auditLogState7 = this.this$0.state;
            List<GuildScheduledEvent> guildScheduledEvents = auditLogState7.getGuildScheduledEvents();
            List<GuildScheduledEvent> guildScheduledEvents2 = this.$newAuditLog.getGuildScheduledEvents();
            m.checkNotNullExpressionValue(guildScheduledEvents2, "newAuditLog.guildScheduledEvents");
            storeAuditLog4.state = StoreAuditLog.AuditLogState.copy$default(auditLogState6, 0L, null, null, null, null, u.plus((Collection) guildScheduledEvents, (Iterable) guildScheduledEvents2), null, null, null, null, false, 2015, null);
        }
        if (this.$newAuditLog.getThreads() != null) {
            StoreAuditLog storeAuditLog5 = this.this$0;
            auditLogState4 = storeAuditLog5.state;
            auditLogState5 = this.this$0.state;
            List<Channel> threads = auditLogState5.getThreads();
            List<Channel> threads2 = this.$newAuditLog.getThreads();
            m.checkNotNullExpressionValue(threads2, "newAuditLog.threads");
            storeAuditLog5.state = StoreAuditLog.AuditLogState.copy$default(auditLogState4, 0L, null, null, null, null, null, u.plus((Collection) threads, (Iterable) threads2), null, null, null, false, 1983, null);
        }
        auditLogState = this.this$0.state;
        List<ModelAuditLogEntry> entries = auditLogState.getEntries();
        if (entries == null) {
            entries = n.emptyList();
        }
        List mutableList3 = u.toMutableList((Collection) entries);
        StoreAuditLog storeAuditLog6 = this.this$0;
        long j = this.$guildId;
        List<ModelAuditLogEntry> auditLogEntries = this.$newAuditLog.getAuditLogEntries();
        m.checkNotNullExpressionValue(auditLogEntries, "newAuditLog.auditLogEntries");
        transformEntries = storeAuditLog6.transformEntries(j, auditLogEntries);
        mutableList3.addAll(transformEntries);
        StoreAuditLog storeAuditLog7 = this.this$0;
        auditLogState2 = storeAuditLog7.state;
        storeAuditLog7.state = StoreAuditLog.AuditLogState.copy$default(auditLogState2, 0L, null, mutableList3, null, null, null, null, null, null, null, false, 2043, null);
        StoreAuditLog storeAuditLog8 = this.this$0;
        auditLogState3 = storeAuditLog8.state;
        storeAuditLog8.state = StoreAuditLog.AuditLogState.copy$default(auditLogState3, 0L, null, null, null, null, null, null, null, null, null, false, AudioAttributesCompat.FLAG_ALL, null);
        this.this$0.markChanged();
    }
}
