package com.discord.stores;

import com.discord.stores.StoreStreamRtcConnection;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreStreamRtcConnection.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/stores/StoreStreamRtcConnection$Listener;", "it", "", "invoke", "(Lcom/discord/stores/StoreStreamRtcConnection$Listener;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStreamRtcConnection$createRtcConnection$1 extends o implements Function1<StoreStreamRtcConnection.Listener, Unit> {
    public static final StoreStreamRtcConnection$createRtcConnection$1 INSTANCE = new StoreStreamRtcConnection$createRtcConnection$1();

    public StoreStreamRtcConnection$createRtcConnection$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(StoreStreamRtcConnection.Listener listener) {
        invoke2(listener);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(StoreStreamRtcConnection.Listener listener) {
        m.checkNotNullParameter(listener, "it");
        listener.onConnecting();
    }
}
