package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.Context;
import b.a.d.o;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.friendsuggestions.FriendSuggestion;
import com.discord.api.guild.Guild;
import com.discord.api.guildmember.GuildMember;
import com.discord.api.guildmember.GuildMembersChunk;
import com.discord.api.guildscheduledevent.ApiGuildScheduledEventUser;
import com.discord.api.message.Message;
import com.discord.api.premium.PremiumTier;
import com.discord.api.presence.Presence;
import com.discord.api.thread.AugmentedThreadMember;
import com.discord.api.thread.ThreadListMember;
import com.discord.api.thread.ThreadMemberListUpdate;
import com.discord.api.thread.ThreadMembersUpdate;
import com.discord.api.user.NsfwAllowance;
import com.discord.api.user.User;
import com.discord.app.AppLog;
import com.discord.models.domain.ModelPayload;
import com.discord.models.domain.ModelUserRelationship;
import com.discord.models.user.CoreUser;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreMessagesLoader;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.collections.SnowflakePartitionMap;
import com.discord.utilities.persister.Persister;
import com.discord.utilities.user.UserRequestManager;
import com.discord.utilities.user.UserUtils;
import d0.d0.f;
import d0.t.g0;
import d0.t.h0;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Action1;
/* compiled from: StoreUser.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000þ\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 \u0087\u00012\u00020\u0001:\u0002\u0087\u0001BB\u0012\u0012\u0010|\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040{\u0012\u0006\u0010u\u001a\u00020t\u0012\n\b\u0002\u0010\u0083\u0001\u001a\u00030\u0082\u0001\u0012\u000f\b\u0002\u0010\u0080\u0001\u001a\b\u0012\u0004\u0012\u00020\u00130\u007f¢\u0006\u0006\b\u0085\u0001\u0010\u0086\u0001J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0005\u0010\u0006J3\u0010\r\u001a\u00020\u00042\u0010\u0010\n\u001a\f\u0012\b\u0012\u00060\bj\u0002`\t0\u00072\u0010\u0010\f\u001a\f\u0012\b\u0012\u00060\bj\u0002`\t0\u000bH\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u001d\u0010\u0011\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`\t\u0012\u0004\u0012\u00020\u00100\u000f¢\u0006\u0004\b\u0011\u0010\u0012J\r\u0010\u0014\u001a\u00020\u0013¢\u0006\u0004\b\u0014\u0010\u0015J\u000f\u0010\u0017\u001a\u00020\u0013H\u0001¢\u0006\u0004\b\u0016\u0010\u0015J\u001f\u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`\t\u0012\u0004\u0012\u00020\u00100\u000fH\u0001¢\u0006\u0004\b\u0018\u0010\u0012J\u0017\u0010\u001c\u001a\u00020\u00042\u0006\u0010\u001b\u001a\u00020\u001aH\u0016¢\u0006\u0004\b\u001c\u0010\u001dJ#\u0010\u001f\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\bj\u0002`\t\u0012\u0004\u0012\u00020\u00100\u000f0\u001e¢\u0006\u0004\b\u001f\u0010 J5\u0010\"\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\bj\u0002`\t\u0012\u0004\u0012\u00020\u00100\u000f0\u001e2\u0010\u0010!\u001a\f\u0012\b\u0012\u00060\bj\u0002`\t0\u0007¢\u0006\u0004\b\"\u0010#J=\u0010\"\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\bj\u0002`\t\u0012\u0004\u0012\u00020\u00100\u000f0\u001e2\u0010\u0010!\u001a\f\u0012\b\u0012\u00060\bj\u0002`\t0\u00072\u0006\u0010%\u001a\u00020$¢\u0006\u0004\b\"\u0010&J!\u0010(\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00100\u001e2\n\u0010'\u001a\u00060\bj\u0002`\t¢\u0006\u0004\b(\u0010)J7\u0010\u0011\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`\t\u0012\u0004\u0012\u00020\u00100\u000f2\u0010\u0010!\u001a\f\u0012\b\u0012\u00060\bj\u0002`\t0\u00072\u0006\u0010%\u001a\u00020$¢\u0006\u0004\b\u0011\u0010*J\u001f\u0010,\u001a\b\u0012\u0004\u0012\u00020\u00130\u001e2\b\b\u0002\u0010+\u001a\u00020$H\u0007¢\u0006\u0004\b,\u0010-J\u0017\u0010.\u001a\f\u0012\b\u0012\u00060\bj\u0002`\t0\u001e¢\u0006\u0004\b.\u0010 J5\u00100\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\bj\u0002`\t\u0012\u0004\u0012\u00020/0\u000f0\u001e2\u0010\u0010!\u001a\f\u0012\b\u0012\u00060\bj\u0002`\t0\u0007¢\u0006\u0004\b0\u0010#J\u001f\u00103\u001a\u00020\u00042\u0010\u00102\u001a\f\u0012\b\u0012\u00060\bj\u0002`\t01¢\u0006\u0004\b3\u00104J\u0019\u00106\u001a\u00020\u00042\b\u00105\u001a\u0004\u0018\u00010/H\u0007¢\u0006\u0004\b6\u00107J\u0017\u0010:\u001a\u00020\u00042\u0006\u00109\u001a\u000208H\u0007¢\u0006\u0004\b:\u0010;J\u0017\u0010>\u001a\u00020\u00042\u0006\u0010=\u001a\u00020<H\u0007¢\u0006\u0004\b>\u0010?J\u0017\u0010B\u001a\u00020\u00042\u0006\u0010A\u001a\u00020@H\u0007¢\u0006\u0004\bB\u0010CJ\u001d\u0010F\u001a\u00020\u00042\f\u0010E\u001a\b\u0012\u0004\u0012\u00020D01H\u0007¢\u0006\u0004\bF\u00104J\u0017\u0010H\u001a\u00020\u00042\u0006\u0010G\u001a\u00020DH\u0007¢\u0006\u0004\bH\u0010IJ\u0017\u0010J\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\bJ\u0010\u0006J\u0017\u0010M\u001a\u00020\u00042\u0006\u0010L\u001a\u00020KH\u0007¢\u0006\u0004\bM\u0010NJ\u0017\u0010Q\u001a\u00020\u00042\u0006\u0010P\u001a\u00020OH\u0007¢\u0006\u0004\bQ\u0010RJ\u0017\u0010U\u001a\u00020\u00042\u0006\u0010T\u001a\u00020SH\u0007¢\u0006\u0004\bU\u0010VJ\u0017\u0010Y\u001a\u00020\u00042\u0006\u0010X\u001a\u00020WH\u0007¢\u0006\u0004\bY\u0010ZJ\u0017\u0010\\\u001a\u00020\u00042\u0006\u00109\u001a\u00020[H\u0007¢\u0006\u0004\b\\\u0010]J\u0017\u0010`\u001a\u00020\u00042\u0006\u0010_\u001a\u00020^H\u0007¢\u0006\u0004\b`\u0010aJ\u0017\u0010d\u001a\u00020\u00042\u0006\u0010c\u001a\u00020bH\u0007¢\u0006\u0004\bd\u0010eJ\u0017\u0010h\u001a\u00020\u00042\u0006\u0010g\u001a\u00020fH\u0007¢\u0006\u0004\bh\u0010iJ\u001d\u0010l\u001a\u00020\u00042\f\u0010k\u001a\b\u0012\u0004\u0012\u00020j01H\u0007¢\u0006\u0004\bl\u00104J\u000f\u0010m\u001a\u00020\u0004H\u0017¢\u0006\u0004\bm\u0010nR\u0016\u0010p\u001a\u00020o8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bp\u0010qR\u0016\u0010r\u001a\u00020\u00138\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\br\u0010sR\u0016\u0010u\u001a\u00020t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bu\u0010vR\u001c\u00102\u001a\b\u0012\u0004\u0012\u00020\u00100w8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b2\u0010xR&\u0010y\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`\t\u0012\u0004\u0012\u00020\u00100\u000f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\by\u0010zR\"\u0010|\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040{8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b|\u0010}R\u0016\u0010~\u001a\u00020\u00138\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b~\u0010sR\u001f\u0010\u0080\u0001\u001a\b\u0012\u0004\u0012\u00020\u00130\u007f8\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0080\u0001\u0010\u0081\u0001R\u001a\u0010\u0083\u0001\u001a\u00030\u0082\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0083\u0001\u0010\u0084\u0001¨\u0006\u0088\u0001"}, d2 = {"Lcom/discord/stores/StoreUser;", "Lcom/discord/stores/StoreV2;", "Lcom/discord/api/user/User;", "user", "", "updateUser", "(Lcom/discord/api/user/User;)V", "", "", "Lcom/discord/primitives/UserId;", "totalUserIds", "", "existingUserIds", "fetchMissing", "(Ljava/util/Collection;Ljava/util/Set;)V", "", "Lcom/discord/models/user/User;", "getUsers", "()Ljava/util/Map;", "Lcom/discord/models/user/MeUser;", "getMe", "()Lcom/discord/models/user/MeUser;", "getMeInternal$app_productionGoogleRelease", "getMeInternal", "getUsersInternal$app_productionGoogleRelease", "getUsersInternal", "Landroid/content/Context;", "context", "init", "(Landroid/content/Context;)V", "Lrx/Observable;", "observeAllUsers", "()Lrx/Observable;", "userIds", "observeUsers", "(Ljava/util/Collection;)Lrx/Observable;", "", "fetchUserIfMissing", "(Ljava/util/Collection;Z)Lrx/Observable;", "userId", "observeUser", "(J)Lrx/Observable;", "(Ljava/util/Collection;Z)Ljava/util/Map;", "emitEmpty", "observeMe", "(Z)Lrx/Observable;", "observeMeId", "", "observeUsernames", "", "users", "fetchUsers", "(Ljava/util/List;)V", "authToken", "handleAuthToken", "(Ljava/lang/String;)V", "Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;", "chunk", "handleMessagesLoaded", "(Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;)V", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "Lcom/discord/models/domain/ModelUserRelationship;", "relationship", "handleUserRelationshipAdd", "(Lcom/discord/models/domain/ModelUserRelationship;)V", "Lcom/discord/api/friendsuggestions/FriendSuggestion;", "loadedSuggestions", "handleFriendSuggestionsLoaded", "suggestion", "handleFriendSuggestionCreate", "(Lcom/discord/api/friendsuggestions/FriendSuggestion;)V", "handleUserUpdated", "Lcom/discord/api/presence/Presence;", "presence", "handlePresenceUpdate", "(Lcom/discord/api/presence/Presence;)V", "Lcom/discord/api/channel/Channel;", "channel", "handleChannelCreated", "(Lcom/discord/api/channel/Channel;)V", "Lcom/discord/api/guild/Guild;", "guild", "handleGuildAddOrSync", "(Lcom/discord/api/guild/Guild;)V", "Lcom/discord/api/guildmember/GuildMember;", "member", "handleGuildMemberAdd", "(Lcom/discord/api/guildmember/GuildMember;)V", "Lcom/discord/api/guildmember/GuildMembersChunk;", "handleGuildMembersChunk", "(Lcom/discord/api/guildmember/GuildMembersChunk;)V", "Lcom/discord/api/thread/ThreadMemberListUpdate;", "threadMemberListUpdate", "handleThreadMemberListUpdate", "(Lcom/discord/api/thread/ThreadMemberListUpdate;)V", "Lcom/discord/api/thread/ThreadMembersUpdate;", "threadMembersUpdate", "handleThreadMembersUpdate", "(Lcom/discord/api/thread/ThreadMembersUpdate;)V", "Lcom/discord/api/message/Message;", "message", "handleMessageCreateOrUpdate", "(Lcom/discord/api/message/Message;)V", "Lcom/discord/api/guildscheduledevent/ApiGuildScheduledEventUser;", "apiGuildScheduledEventUsers", "handleGuildScheduledEventUsersFetch", "snapshotData", "()V", "Lcom/discord/utilities/user/UserRequestManager;", "userRequestManager", "Lcom/discord/utilities/user/UserRequestManager;", "me", "Lcom/discord/models/user/MeUser;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;", "Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;", "usersSnapshot", "Ljava/util/Map;", "Lkotlin/Function1;", "notifyUserUpdated", "Lkotlin/jvm/functions/Function1;", "meSnapshot", "Lcom/discord/utilities/persister/Persister;", "meCache", "Lcom/discord/utilities/persister/Persister;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", HookHelper.constructorName, "(Lkotlin/jvm/functions/Function1;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/utilities/persister/Persister;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUser extends StoreV2 {
    private final Dispatcher dispatcher;

    /* renamed from: me  reason: collision with root package name */
    private MeUser f2785me;
    private final Persister<MeUser> meCache;
    private MeUser meSnapshot;
    private final Function1<User, Unit> notifyUserUpdated;
    private final ObservationDeck observationDeck;
    private final UserRequestManager userRequestManager;
    private final SnowflakePartitionMap.CopiablePartitionMap<com.discord.models.user.User> users;
    private Map<Long, ? extends com.discord.models.user.User> usersSnapshot;
    public static final Companion Companion = new Companion(null);
    private static final ObservationDeck.UpdateSource MeUpdate = new ObservationDeck.UpdateSource() { // from class: com.discord.stores.StoreUser$Companion$MeUpdate$1
    };
    private static final ObservationDeck.UpdateSource UsersUpdate = new ObservationDeck.UpdateSource() { // from class: com.discord.stores.StoreUser$Companion$UsersUpdate$1
    };
    private static final MeUser EMPTY_ME_USER = new MeUser(0, "EMPTY_USERNAME", null, null, false, false, 0, PremiumTier.NONE, null, false, false, null, 0, 0, null, NsfwAllowance.UNKNOWN, null, null, 204800, null);

    /* compiled from: StoreUser.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0004\u001a\u0004\b\b\u0010\u0006R\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/stores/StoreUser$Companion;", "", "Lcom/discord/stores/updates/ObservationDeck$UpdateSource;", "MeUpdate", "Lcom/discord/stores/updates/ObservationDeck$UpdateSource;", "getMeUpdate", "()Lcom/discord/stores/updates/ObservationDeck$UpdateSource;", "UsersUpdate", "getUsersUpdate", "Lcom/discord/models/user/MeUser;", "EMPTY_ME_USER", "Lcom/discord/models/user/MeUser;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public final ObservationDeck.UpdateSource getMeUpdate() {
            return StoreUser.MeUpdate;
        }

        public final ObservationDeck.UpdateSource getUsersUpdate() {
            return StoreUser.UsersUpdate;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ StoreUser(Function1 function1, Dispatcher dispatcher, ObservationDeck observationDeck, Persister persister, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(function1, dispatcher, (i & 4) != 0 ? ObservationDeckProvider.get() : observationDeck, (i & 8) != 0 ? new Persister("STORE_USERS_ME_V13", EMPTY_ME_USER) : persister);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void fetchMissing(Collection<Long> collection, Set<Long> set) {
        HashSet hashSet = new HashSet();
        for (Long l : collection) {
            long longValue = l.longValue();
            if (!set.contains(Long.valueOf(longValue))) {
                hashSet.add(Long.valueOf(longValue));
            }
        }
        this.userRequestManager.requestUsers(hashSet);
    }

    public static /* synthetic */ Observable observeMe$default(StoreUser storeUser, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = false;
        }
        return storeUser.observeMe(z2);
    }

    @StoreThread
    private final void updateUser(User user) {
        CoreUser coreUser = new CoreUser(user);
        if (!m.areEqual(coreUser, this.users.get(Long.valueOf(user.i())))) {
            this.users.put(Long.valueOf(user.i()), coreUser);
            markChanged(UsersUpdate);
        }
    }

    public final void fetchUsers(List<Long> list) {
        m.checkNotNullParameter(list, "users");
        this.dispatcher.schedule(new StoreUser$fetchUsers$1(this, list));
    }

    public final MeUser getMe() {
        return this.meSnapshot;
    }

    @StoreThread
    public final MeUser getMeInternal$app_productionGoogleRelease() {
        return this.f2785me;
    }

    public final Map<Long, com.discord.models.user.User> getUsers() {
        return this.usersSnapshot;
    }

    @StoreThread
    public final Map<Long, com.discord.models.user.User> getUsersInternal$app_productionGoogleRelease() {
        return this.users;
    }

    @StoreThread
    public final void handleAuthToken(String str) {
        if (str == null) {
            markChanged(MeUpdate);
            this.f2785me = EMPTY_ME_USER;
        }
    }

    @StoreThread
    public final void handleChannelCreated(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        List<User> w = channel.w();
        if (w != null) {
            for (User user : w) {
                updateUser(user);
            }
        }
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        this.users.clear();
        User me2 = modelPayload.getMe();
        m.checkNotNullExpressionValue(me2, "payload.me");
        MeUser meUser = new MeUser(me2);
        this.f2785me = meUser;
        this.users.put(Long.valueOf(meUser.getId()), meUser);
        for (Channel channel : modelPayload.getPrivateChannels()) {
            m.checkNotNullExpressionValue(channel, "channel");
            for (com.discord.models.user.User user : ChannelUtils.g(channel)) {
                this.users.put(Long.valueOf(user.getId()), user);
            }
        }
        for (ModelUserRelationship modelUserRelationship : modelPayload.getRelationships()) {
            m.checkNotNullExpressionValue(modelUserRelationship, "relationship");
            if (modelUserRelationship.getUser() != null) {
                SnowflakePartitionMap.CopiablePartitionMap<com.discord.models.user.User> copiablePartitionMap = this.users;
                Long valueOf = Long.valueOf(modelUserRelationship.getUser().i());
                User user2 = modelUserRelationship.getUser();
                m.checkNotNullExpressionValue(user2, "relationship.user");
                copiablePartitionMap.put(valueOf, new CoreUser(user2));
            }
        }
        for (Guild guild : modelPayload.getGuilds()) {
            List<GuildMember> v = guild.v();
            if (v != null) {
                for (GuildMember guildMember : v) {
                    this.users.put(Long.valueOf(guildMember.m().i()), new CoreUser(guildMember.m()));
                }
            }
        }
        StringBuilder R = a.R("Discovered ");
        R.append(this.users.size());
        R.append(" initial users.");
        AppLog.i(R.toString());
        Long valueOf2 = Long.valueOf(meUser.getId());
        String email = meUser.getEmail();
        AppLog.g(valueOf2, email, meUser.getUsername() + UserUtils.INSTANCE.getDiscriminatorWithPadding(meUser));
        markChanged(MeUpdate, UsersUpdate);
    }

    @StoreThread
    public final void handleFriendSuggestionCreate(FriendSuggestion friendSuggestion) {
        m.checkNotNullParameter(friendSuggestion, "suggestion");
        updateUser(friendSuggestion.b());
    }

    @StoreThread
    public final void handleFriendSuggestionsLoaded(List<FriendSuggestion> list) {
        m.checkNotNullParameter(list, "loadedSuggestions");
        for (FriendSuggestion friendSuggestion : list) {
            updateUser(friendSuggestion.b());
        }
    }

    @StoreThread
    public final void handleGuildAddOrSync(Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        List<GuildMember> v = guild.v();
        if (v != null) {
            for (GuildMember guildMember : v) {
                updateUser(guildMember.m());
            }
        }
    }

    @StoreThread
    public final void handleGuildMemberAdd(GuildMember guildMember) {
        m.checkNotNullParameter(guildMember, "member");
        updateUser(guildMember.m());
    }

    @StoreThread
    public final void handleGuildMembersChunk(GuildMembersChunk guildMembersChunk) {
        m.checkNotNullParameter(guildMembersChunk, "chunk");
        for (GuildMember guildMember : guildMembersChunk.b()) {
            updateUser(guildMember.m());
        }
    }

    @StoreThread
    public final void handleGuildScheduledEventUsersFetch(List<ApiGuildScheduledEventUser> list) {
        m.checkNotNullParameter(list, "apiGuildScheduledEventUsers");
        for (ApiGuildScheduledEventUser apiGuildScheduledEventUser : list) {
            User c = apiGuildScheduledEventUser.c();
            if (c != null) {
                updateUser(c);
            }
        }
    }

    @StoreThread
    public final void handleMessageCreateOrUpdate(Message message) {
        MeUser copy;
        m.checkNotNullParameter(message, "message");
        List<User> t = message.t();
        if (t != null) {
            for (User user : t) {
                updateUser(user);
            }
        }
        Long l = message.l();
        if (((l != null ? l.longValue() : 0L) & 16) != 0) {
            copy = r7.copy((r38 & 1) != 0 ? r7.getId() : 0L, (r38 & 2) != 0 ? r7.getUsername() : null, (r38 & 4) != 0 ? r7.getAvatar() : null, (r38 & 8) != 0 ? r7.getBanner() : null, (r38 & 16) != 0 ? r7.isBot() : false, (r38 & 32) != 0 ? r7.isSystemUser() : false, (r38 & 64) != 0 ? r7.getDiscriminator() : 0, (r38 & 128) != 0 ? r7.getPremiumTier() : null, (r38 & 256) != 0 ? r7.email : null, (r38 & 512) != 0 ? r7.mfaEnabled : false, (r38 & 1024) != 0 ? r7.isVerified : false, (r38 & 2048) != 0 ? r7.token : null, (r38 & 4096) != 0 ? r7.getFlags() : this.f2785me.getFlags() | 8192, (r38 & 8192) != 0 ? r7.getPublicFlags() : 0, (r38 & 16384) != 0 ? r7.phoneNumber : null, (r38 & 32768) != 0 ? r7.nsfwAllowance : null, (r38 & 65536) != 0 ? r7.getBio() : null, (r38 & 131072) != 0 ? this.f2785me.getBannerColor() : null);
            this.f2785me = copy;
            markChanged(MeUpdate);
        }
    }

    @StoreThread
    public final void handleMessagesLoaded(StoreMessagesLoader.ChannelChunk channelChunk) {
        m.checkNotNullParameter(channelChunk, "chunk");
        for (com.discord.models.message.Message message : channelChunk.getMessages()) {
            User author = message.getAuthor();
            if (author != null) {
                updateUser(author);
            }
            List<User> mentions = message.getMentions();
            if (mentions != null) {
                for (User user : mentions) {
                    updateUser(user);
                }
            }
        }
    }

    @StoreThread
    public final void handlePresenceUpdate(Presence presence) {
        m.checkNotNullParameter(presence, "presence");
        User f = presence.f();
        if ((f != null ? f.f() : null) != null) {
            updateUser(f);
        }
    }

    @StoreThread
    public final void handleThreadMemberListUpdate(ThreadMemberListUpdate threadMemberListUpdate) {
        m.checkNotNullParameter(threadMemberListUpdate, "threadMemberListUpdate");
        List<ThreadListMember> b2 = threadMemberListUpdate.b();
        if (b2 != null) {
            for (ThreadListMember threadListMember : b2) {
                GuildMember a = threadListMember.a();
                if (a != null) {
                    updateUser(a.m());
                }
                Presence b3 = threadListMember.b();
                User f = b3 != null ? b3.f() : null;
                if (f != null) {
                    updateUser(f);
                }
            }
        }
    }

    @StoreThread
    public final void handleThreadMembersUpdate(ThreadMembersUpdate threadMembersUpdate) {
        m.checkNotNullParameter(threadMembersUpdate, "threadMembersUpdate");
        List<AugmentedThreadMember> a = threadMembersUpdate.a();
        if (a != null) {
            for (AugmentedThreadMember augmentedThreadMember : a) {
                GuildMember c = augmentedThreadMember.c();
                if (c != null) {
                    updateUser(c.m());
                }
                Presence f = augmentedThreadMember.f();
                User f2 = f != null ? f.f() : null;
                if (f2 != null) {
                    updateUser(f2);
                }
            }
        }
    }

    @StoreThread
    public final void handleUserRelationshipAdd(ModelUserRelationship modelUserRelationship) {
        m.checkNotNullParameter(modelUserRelationship, "relationship");
        User user = modelUserRelationship.getUser();
        m.checkNotNullExpressionValue(user, "relationship.user");
        updateUser(user);
    }

    @StoreThread
    public final void handleUserUpdated(User user) {
        m.checkNotNullParameter(user, "user");
        updateUser(user);
        if (this.f2785me.getId() == user.i()) {
            this.f2785me = MeUser.Companion.merge(this.f2785me, user);
            markChanged(MeUpdate);
        }
    }

    @Override // com.discord.stores.Store
    public void init(Context context) {
        m.checkNotNullParameter(context, "context");
        super.init(context);
        this.f2785me = this.meCache.get();
        markChanged(MeUpdate);
    }

    public final Observable<Map<Long, com.discord.models.user.User>> observeAllUsers() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreUser$observeAllUsers$1(this), 14, null);
    }

    public final Observable<MeUser> observeMe() {
        return observeMe$default(this, false, 1, null);
    }

    public final Observable<MeUser> observeMe(final boolean z2) {
        Observable<MeUser> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{MeUpdate}, false, null, null, new StoreUser$observeMe$1(this), 14, null).x(new b<MeUser, Boolean>() { // from class: com.discord.stores.StoreUser$observeMe$2
            public final Boolean call(MeUser meUser) {
                MeUser meUser2;
                meUser2 = StoreUser.EMPTY_ME_USER;
                return Boolean.valueOf(meUser != meUser2 || z2);
            }
        }).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Long> observeMeId() {
        Observable<Long> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{MeUpdate}, false, null, null, new StoreUser$observeMeId$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<com.discord.models.user.User> observeUser(final long j) {
        Observable<com.discord.models.user.User> q = observeAllUsers().F(new b<Map<Long, ? extends com.discord.models.user.User>, com.discord.models.user.User>() { // from class: com.discord.stores.StoreUser$observeUser$1
            public final com.discord.models.user.User call(Map<Long, ? extends com.discord.models.user.User> map) {
                m.checkNotNullParameter(map, "user");
                return map.get(Long.valueOf(j));
            }
        }).q();
        m.checkNotNullExpressionValue(q, "observeAllUsers()\n      …  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Map<Long, String>> observeUsernames(Collection<Long> collection) {
        m.checkNotNullParameter(collection, "userIds");
        Observable k = observeAllUsers().k(o.b(collection, StoreUser$observeUsernames$1.INSTANCE));
        m.checkNotNullExpressionValue(k, "observeAllUsers()\n      …er!!.username }\n        )");
        return k;
    }

    public final Observable<Map<Long, com.discord.models.user.User>> observeUsers(Collection<Long> collection) {
        m.checkNotNullParameter(collection, "userIds");
        return observeUsers(collection, false);
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        if (getUpdateSources().contains(UsersUpdate)) {
            this.usersSnapshot = this.users.fastCopy();
        }
        if (getUpdateSources().contains(MeUpdate)) {
            MeUser meUser = this.f2785me;
            this.meSnapshot = meUser;
            Persister.set$default(this.meCache, meUser, false, 2, null);
        }
    }

    public final Map<Long, com.discord.models.user.User> getUsers(Collection<Long> collection, boolean z2) {
        m.checkNotNullParameter(collection, "userIds");
        Map<Long, ? extends com.discord.models.user.User> map = this.usersSnapshot;
        ArrayList arrayList = new ArrayList();
        for (Number number : collection) {
            com.discord.models.user.User user = map.get(Long.valueOf(number.longValue()));
            if (user != null) {
                arrayList.add(user);
            }
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(d0.t.o.collectionSizeOrDefault(arrayList, 10)), 16));
        for (Object obj : arrayList) {
            linkedHashMap.put(Long.valueOf(((com.discord.models.user.User) obj).getId()), obj);
        }
        if (z2) {
            fetchMissing(collection, linkedHashMap.keySet());
        }
        return linkedHashMap;
    }

    public final Observable<Map<Long, com.discord.models.user.User>> observeUsers(final Collection<Long> collection, final boolean z2) {
        m.checkNotNullParameter(collection, "userIds");
        Observable<Map<Long, com.discord.models.user.User>> t = observeAllUsers().k(o.a(collection)).t(new Action1<Map<Long, ? extends com.discord.models.user.User>>() { // from class: com.discord.stores.StoreUser$observeUsers$1
            public final void call(Map<Long, ? extends com.discord.models.user.User> map) {
                if (z2) {
                    StoreUser.this.fetchMissing(collection, map.keySet());
                }
            }
        });
        m.checkNotNullExpressionValue(t, "observeAllUsers()\n      …ys)\n          }\n        }");
        return t;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public StoreUser(Function1<? super User, Unit> function1, Dispatcher dispatcher, ObservationDeck observationDeck, Persister<MeUser> persister) {
        m.checkNotNullParameter(function1, "notifyUserUpdated");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(persister, "meCache");
        this.notifyUserUpdated = function1;
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
        this.meCache = persister;
        MeUser meUser = EMPTY_ME_USER;
        this.f2785me = meUser;
        this.users = new SnowflakePartitionMap.CopiablePartitionMap<>(0, 1, null);
        this.meSnapshot = meUser;
        this.usersSnapshot = h0.emptyMap();
        this.userRequestManager = new UserRequestManager(new StoreUser$userRequestManager$1(this));
    }
}
