package com.discord.stores;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import b.d.b.a.a;
import b.i.a.f.e.o.f;
import com.discord.api.stageinstance.RecommendedStageInstance;
import com.discord.api.stageinstance.StageInstance;
import com.discord.stores.StoreRequestedStageChannels;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.rest.RestAPI;
import d0.t.h0;
import d0.t.u;
import d0.z.d.m;
import j0.k.b;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlinx.coroutines.CoroutineDispatcher;
import kotlinx.coroutines.Job;
import rx.Observable;
import s.a.a.n;
import s.a.k0;
import s.a.x0;
/* compiled from: StoreRequestedStageChannels.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010%\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010#\n\u0002\b\b\u0018\u0000 82\u00020\u0001:\u000389:B#\u0012\u0006\u0010'\u001a\u00020&\u0012\b\b\u0002\u0010+\u001a\u00020*\u0012\b\b\u0002\u0010.\u001a\u00020-¢\u0006\u0004\b6\u00107J)\u0010\t\u001a\u00020\b2\u0010\u0010\u0005\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00022\u0006\u0010\u0007\u001a\u00020\u0006H\u0003¢\u0006\u0004\b\t\u0010\nJ/\u0010\u000e\u001a\u00020\b2\u0010\u0010\u0005\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00022\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000bH\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u001d\u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00110\u0010¢\u0006\u0004\b\u0012\u0010\u0013J\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00152\n\u0010\u0014\u001a\u00060\u0003j\u0002`\u0004¢\u0006\u0004\b\u0016\u0010\u0017J#\u0010\u0019\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00110\u00100\u0018¢\u0006\u0004\b\u0019\u0010\u001aJ!\u0010\u001b\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00110\u00182\n\u0010\u0014\u001a\u00060\u0003j\u0002`\u0004¢\u0006\u0004\b\u001b\u0010\u001cJ\u001b\u0010\u001d\u001a\u00020\b2\n\u0010\u0014\u001a\u00060\u0003j\u0002`\u0004H\u0007¢\u0006\u0004\b\u001d\u0010\u001eJ\u001f\u0010\u001f\u001a\u00020\b2\u0010\u0010\u0005\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002¢\u0006\u0004\b\u001f\u0010 J\u000f\u0010!\u001a\u00020\bH\u0016¢\u0006\u0004\b!\u0010\"R&\u0010$\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00110#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%R\u0016\u0010'\u001a\u00020&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R&\u0010)\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00110\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b)\u0010%R\u0016\u0010+\u001a\u00020*8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b+\u0010,R\u0016\u0010.\u001a\u00020-8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b.\u0010/R\u0018\u00101\u001a\u0004\u0018\u0001008\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b1\u00102R \u00104\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u0004038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b4\u00105¨\u0006;"}, d2 = {"Lcom/discord/stores/StoreRequestedStageChannels;", "Lcom/discord/stores/StoreV2;", "", "", "Lcom/discord/primitives/ChannelId;", "channelIds", "Lcom/discord/stores/StoreRequestedStageChannels$FetchStatus;", "status", "", "updateStatus", "(Ljava/util/Set;Lcom/discord/stores/StoreRequestedStageChannels$FetchStatus;)V", "", "Lcom/discord/api/stageinstance/RecommendedStageInstance;", "stageInstances", "onLoaded", "(Ljava/util/Set;Ljava/util/List;)V", "", "Lcom/discord/stores/StoreRequestedStageChannels$StageInstanceState;", "getRequestedInstanceStatesByChannel", "()Ljava/util/Map;", "channelId", "Lcom/discord/api/stageinstance/StageInstance;", "getStageInstanceForChannel", "(J)Lcom/discord/api/stageinstance/StageInstance;", "Lrx/Observable;", "observeRequestedStageChannels", "()Lrx/Observable;", "observeRequestedStageChannel", "(J)Lrx/Observable;", "enqueueStageChannelFetch", "(J)V", "fetchStageChannels", "(Ljava/util/Set;)V", "snapshotData", "()V", "", "requestedStageChannels", "Ljava/util/Map;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "requestedStageChannelsSnapshot", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "Lkotlinx/coroutines/Job;", "enqueuedChannelFetchJob", "Lkotlinx/coroutines/Job;", "", "enqueuedChannelFetches", "Ljava/util/Set;", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/utilities/rest/RestAPI;)V", "Companion", "FetchStatus", "StageInstanceState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreRequestedStageChannels extends StoreV2 {
    public static final Companion Companion = new Companion(null);
    private static final long ENQUEUE_DEBOUNCE_DELAY_MS = 22;
    private final Dispatcher dispatcher;
    private Job enqueuedChannelFetchJob;
    private Set<Long> enqueuedChannelFetches;
    private final ObservationDeck observationDeck;
    private final Map<Long, StageInstanceState> requestedStageChannels;
    private Map<Long, StageInstanceState> requestedStageChannelsSnapshot;
    private final RestAPI restAPI;

    /* compiled from: StoreRequestedStageChannels.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/stores/StoreRequestedStageChannels$Companion;", "", "", "ENQUEUE_DEBOUNCE_DELAY_MS", "J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreRequestedStageChannels.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/stores/StoreRequestedStageChannels$FetchStatus;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "ERROR", "LOADING", "LOADED", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum FetchStatus {
        ERROR,
        LOADING,
        LOADED
    }

    /* compiled from: StoreRequestedStageChannels.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0007\u0012\u0006\u0010\u000e\u001a\u00020\n¢\u0006\u0004\b\u001e\u0010\u001fJ\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\r\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0004J\r\u0010\u0006\u001a\u00020\u0002¢\u0006\u0004\b\u0006\u0010\u0004J\u0012\u0010\b\u001a\u0004\u0018\u00010\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ&\u0010\u000f\u001a\u00020\u00002\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00072\b\b\u0002\u0010\u000e\u001a\u00020\nHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u001a\u0010\u0018\u001a\u00020\u00022\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u0019\u0010\u000e\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001a\u001a\u0004\b\u001b\u0010\fR\u001b\u0010\r\u001a\u0004\u0018\u00010\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001c\u001a\u0004\b\u001d\u0010\t¨\u0006 "}, d2 = {"Lcom/discord/stores/StoreRequestedStageChannels$StageInstanceState;", "", "", "isLoading", "()Z", "isError", "isLoaded", "Lcom/discord/api/stageinstance/RecommendedStageInstance;", "component1", "()Lcom/discord/api/stageinstance/RecommendedStageInstance;", "Lcom/discord/stores/StoreRequestedStageChannels$FetchStatus;", "component2", "()Lcom/discord/stores/StoreRequestedStageChannels$FetchStatus;", "stageInstance", "status", "copy", "(Lcom/discord/api/stageinstance/RecommendedStageInstance;Lcom/discord/stores/StoreRequestedStageChannels$FetchStatus;)Lcom/discord/stores/StoreRequestedStageChannels$StageInstanceState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StoreRequestedStageChannels$FetchStatus;", "getStatus", "Lcom/discord/api/stageinstance/RecommendedStageInstance;", "getStageInstance", HookHelper.constructorName, "(Lcom/discord/api/stageinstance/RecommendedStageInstance;Lcom/discord/stores/StoreRequestedStageChannels$FetchStatus;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class StageInstanceState {
        private final RecommendedStageInstance stageInstance;
        private final FetchStatus status;

        public StageInstanceState(RecommendedStageInstance recommendedStageInstance, FetchStatus fetchStatus) {
            m.checkNotNullParameter(fetchStatus, "status");
            this.stageInstance = recommendedStageInstance;
            this.status = fetchStatus;
        }

        public static /* synthetic */ StageInstanceState copy$default(StageInstanceState stageInstanceState, RecommendedStageInstance recommendedStageInstance, FetchStatus fetchStatus, int i, Object obj) {
            if ((i & 1) != 0) {
                recommendedStageInstance = stageInstanceState.stageInstance;
            }
            if ((i & 2) != 0) {
                fetchStatus = stageInstanceState.status;
            }
            return stageInstanceState.copy(recommendedStageInstance, fetchStatus);
        }

        public final RecommendedStageInstance component1() {
            return this.stageInstance;
        }

        public final FetchStatus component2() {
            return this.status;
        }

        public final StageInstanceState copy(RecommendedStageInstance recommendedStageInstance, FetchStatus fetchStatus) {
            m.checkNotNullParameter(fetchStatus, "status");
            return new StageInstanceState(recommendedStageInstance, fetchStatus);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StageInstanceState)) {
                return false;
            }
            StageInstanceState stageInstanceState = (StageInstanceState) obj;
            return m.areEqual(this.stageInstance, stageInstanceState.stageInstance) && m.areEqual(this.status, stageInstanceState.status);
        }

        public final RecommendedStageInstance getStageInstance() {
            return this.stageInstance;
        }

        public final FetchStatus getStatus() {
            return this.status;
        }

        public int hashCode() {
            RecommendedStageInstance recommendedStageInstance = this.stageInstance;
            int i = 0;
            int hashCode = (recommendedStageInstance != null ? recommendedStageInstance.hashCode() : 0) * 31;
            FetchStatus fetchStatus = this.status;
            if (fetchStatus != null) {
                i = fetchStatus.hashCode();
            }
            return hashCode + i;
        }

        public final boolean isError() {
            return this.status == FetchStatus.ERROR;
        }

        public final boolean isLoaded() {
            return this.status == FetchStatus.LOADED;
        }

        public final boolean isLoading() {
            return this.status == FetchStatus.LOADING;
        }

        public String toString() {
            StringBuilder R = a.R("StageInstanceState(stageInstance=");
            R.append(this.stageInstance);
            R.append(", status=");
            R.append(this.status);
            R.append(")");
            return R.toString();
        }
    }

    public /* synthetic */ StoreRequestedStageChannels(Dispatcher dispatcher, ObservationDeck observationDeck, RestAPI restAPI, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(dispatcher, (i & 2) != 0 ? ObservationDeckProvider.get() : observationDeck, (i & 4) != 0 ? RestAPI.Companion.getApi() : restAPI);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void onLoaded(Set<Long> set, List<RecommendedStageInstance> list) {
        Set<Number> mutableSet = u.toMutableSet(set);
        for (RecommendedStageInstance recommendedStageInstance : list) {
            this.requestedStageChannels.put(Long.valueOf(recommendedStageInstance.a().a()), new StageInstanceState(recommendedStageInstance, FetchStatus.LOADED));
            mutableSet.remove(Long.valueOf(recommendedStageInstance.a().a()));
        }
        for (Number number : mutableSet) {
            this.requestedStageChannels.put(Long.valueOf(number.longValue()), new StageInstanceState(null, FetchStatus.LOADED));
        }
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void updateStatus(Set<Long> set, FetchStatus fetchStatus) {
        for (Number number : set) {
            long longValue = number.longValue();
            StageInstanceState stageInstanceState = this.requestedStageChannels.get(Long.valueOf(longValue));
            this.requestedStageChannels.put(Long.valueOf(longValue), new StageInstanceState(stageInstanceState != null ? stageInstanceState.getStageInstance() : null, fetchStatus));
        }
        markChanged();
    }

    @MainThread
    public final void enqueueStageChannelFetch(long j) {
        StageInstanceState stageInstanceState = this.requestedStageChannelsSnapshot.get(Long.valueOf(j));
        if (stageInstanceState == null || stageInstanceState.isError()) {
            this.enqueuedChannelFetches.add(Long.valueOf(j));
            Job job = this.enqueuedChannelFetchJob;
            if (job == null || !job.a()) {
                x0 x0Var = x0.j;
                CoroutineDispatcher coroutineDispatcher = k0.a;
                this.enqueuedChannelFetchJob = f.H0(x0Var, n.f3802b, null, new StoreRequestedStageChannels$enqueueStageChannelFetch$1(this, null), 2, null);
            }
        }
    }

    public final void fetchStageChannels(Set<Long> set) {
        m.checkNotNullParameter(set, "channelIds");
        this.dispatcher.schedule(new StoreRequestedStageChannels$fetchStageChannels$1(this, set));
    }

    public final Map<Long, StageInstanceState> getRequestedInstanceStatesByChannel() {
        return this.requestedStageChannelsSnapshot;
    }

    public final StageInstance getStageInstanceForChannel(long j) {
        RecommendedStageInstance stageInstance;
        StageInstanceState stageInstanceState = getRequestedInstanceStatesByChannel().get(Long.valueOf(j));
        if (stageInstanceState == null || (stageInstance = stageInstanceState.getStageInstance()) == null) {
            return null;
        }
        return stageInstance.a();
    }

    public final Observable<StageInstanceState> observeRequestedStageChannel(final long j) {
        Observable F = observeRequestedStageChannels().F(new b<Map<Long, ? extends StageInstanceState>, StageInstanceState>() { // from class: com.discord.stores.StoreRequestedStageChannels$observeRequestedStageChannel$1
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ StoreRequestedStageChannels.StageInstanceState call(Map<Long, ? extends StoreRequestedStageChannels.StageInstanceState> map) {
                return call2((Map<Long, StoreRequestedStageChannels.StageInstanceState>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final StoreRequestedStageChannels.StageInstanceState call2(Map<Long, StoreRequestedStageChannels.StageInstanceState> map) {
                return map.get(Long.valueOf(j));
            }
        });
        m.checkNotNullExpressionValue(F, "observeRequestedStageCha…s().map { it[channelId] }");
        return F;
    }

    public final Observable<Map<Long, StageInstanceState>> observeRequestedStageChannels() {
        Observable<Map<Long, StageInstanceState>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreRequestedStageChannels$observeRequestedStageChannels$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        super.snapshotData();
        this.requestedStageChannelsSnapshot = h0.toMap(this.requestedStageChannels);
    }

    public StoreRequestedStageChannels(Dispatcher dispatcher, ObservationDeck observationDeck, RestAPI restAPI) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(restAPI, "restAPI");
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
        this.restAPI = restAPI;
        this.requestedStageChannels = new LinkedHashMap();
        this.requestedStageChannelsSnapshot = h0.emptyMap();
        this.enqueuedChannelFetches = new LinkedHashSet();
    }
}
