package com.discord.stores;

import com.discord.models.domain.ModelGift;
import com.discord.stores.StoreGifting;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreGifting.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/models/domain/ModelGift;", "gifts", "", "invoke", "(Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGifting$fetchMyGiftsForSku$3 extends o implements Function1<List<? extends ModelGift>, Unit> {
    public final /* synthetic */ String $comboId;
    public final /* synthetic */ Long $planId;
    public final /* synthetic */ long $skuId;
    public final /* synthetic */ StoreGifting this$0;

    /* compiled from: StoreGifting.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreGifting$fetchMyGiftsForSku$3$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ List $gifts;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(List list) {
            super(0);
            this.$gifts = list;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            StoreGifting$fetchMyGiftsForSku$3 storeGifting$fetchMyGiftsForSku$3 = StoreGifting$fetchMyGiftsForSku$3.this;
            storeGifting$fetchMyGiftsForSku$3.this$0.removeGiftCode(storeGifting$fetchMyGiftsForSku$3.$comboId);
            StoreGifting$fetchMyGiftsForSku$3 storeGifting$fetchMyGiftsForSku$32 = StoreGifting$fetchMyGiftsForSku$3.this;
            storeGifting$fetchMyGiftsForSku$32.this$0.clearGiftsForSku(storeGifting$fetchMyGiftsForSku$32.$skuId, storeGifting$fetchMyGiftsForSku$32.$planId);
            for (ModelGift modelGift : this.$gifts) {
                StoreGifting$fetchMyGiftsForSku$3.this.this$0.setGifts(modelGift.getCode(), new StoreGifting.GiftState.Resolved(modelGift));
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGifting$fetchMyGiftsForSku$3(StoreGifting storeGifting, String str, long j, Long l) {
        super(1);
        this.this$0 = storeGifting;
        this.$comboId = str;
        this.$skuId = j;
        this.$planId = l;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(List<? extends ModelGift> list) {
        invoke2((List<ModelGift>) list);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<ModelGift> list) {
        m.checkNotNullParameter(list, "gifts");
        this.this$0.getDispatcher().schedule(new AnonymousClass1(list));
    }
}
