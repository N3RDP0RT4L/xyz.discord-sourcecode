package com.discord.stores;

import com.discord.api.channel.Channel;
import com.discord.stores.StoreThreadsActiveJoined;
import d0.t.g0;
import d0.z.d.o;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreThreadsActiveJoined.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u00030\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "invoke", "()Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreThreadsActiveJoined$observeAllActiveJoinedThreadsChannelsById$1 extends o implements Function0<Map<Long, ? extends Channel>> {
    public final /* synthetic */ StoreThreadsActiveJoined this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreThreadsActiveJoined$observeAllActiveJoinedThreadsChannelsById$1(StoreThreadsActiveJoined storeThreadsActiveJoined) {
        super(0);
        this.this$0 = storeThreadsActiveJoined;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Map<Long, ? extends Channel> invoke() {
        Map map;
        map = this.this$0.activeJoinedThreadsByThreadIdSnapshot;
        LinkedHashMap linkedHashMap = new LinkedHashMap(g0.mapCapacity(map.size()));
        for (Map.Entry entry : map.entrySet()) {
            linkedHashMap.put(entry.getKey(), ((StoreThreadsActiveJoined.ActiveJoinedThread) entry.getValue()).getChannel());
        }
        return linkedHashMap;
    }
}
