package com.discord.stores;

import andhook.lib.HookHelper;
import com.discord.api.message.Message;
import com.discord.api.user.TypingUser;
import com.discord.api.user.User;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$3;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$4;
import d0.t.h0;
import d0.t.n0;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Subscription;
/* compiled from: StoreUserTyping.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010#\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010(\u001a\u00020'\u0012\u0006\u0010%\u001a\u00020$\u0012\b\b\u0002\u0010\u001c\u001a\u00020\u001b\u0012\b\b\u0002\u0010-\u001a\u00020,¢\u0006\u0004\b1\u00102J)\u0010\u0007\u001a\u001c\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0003j\u0002`\u00060\u00050\u0002H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\f\u001a\u00020\u000b2\u0006\u0010\n\u001a\u00020\tH\u0003¢\u0006\u0004\b\f\u0010\rJ)\u0010\u0010\u001a\u0012\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0003j\u0002`\u00060\u00050\u000f2\n\u0010\u000e\u001a\u00060\u0003j\u0002`\u0004¢\u0006\u0004\b\u0010\u0010\u0011J\u0019\u0010\u0012\u001a\u00020\u000b2\n\u0010\u000e\u001a\u00060\u0003j\u0002`\u0004¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0014\u001a\u00020\u000b2\u0006\u0010\n\u001a\u00020\tH\u0007¢\u0006\u0004\b\u0014\u0010\rJ\u0017\u0010\u0017\u001a\u00020\u000b2\u0006\u0010\u0016\u001a\u00020\u0015H\u0007¢\u0006\u0004\b\u0017\u0010\u0018J\u000f\u0010\u0019\u001a\u00020\u000bH\u0017¢\u0006\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001c\u001a\u00020\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dRZ\u0010\"\u001aF\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0006\u0012\u0004\u0012\u00020 0\u001f0\u001ej\"\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0006\u0012\u0004\u0012\u00020 0\u001f`!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010#R\u0016\u0010%\u001a\u00020$8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b%\u0010&R\u0016\u0010(\u001a\u00020'8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u0010)RN\u0010+\u001a:\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0003j\u0002`\u00060*0\u001ej\u001c\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0003j\u0002`\u00060*`!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b+\u0010#R\u0016\u0010-\u001a\u00020,8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b-\u0010.R0\u0010/\u001a\u001c\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0003j\u0002`\u00060\u00050\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b/\u00100¨\u00063"}, d2 = {"Lcom/discord/stores/StoreUserTyping;", "Lcom/discord/stores/StoreV2;", "", "", "Lcom/discord/primitives/ChannelId;", "", "Lcom/discord/primitives/UserId;", "getTypingUsers", "()Ljava/util/Map;", "Lcom/discord/api/user/TypingUser;", "typing", "", "handleTypingStop", "(Lcom/discord/api/user/TypingUser;)V", "channelId", "Lrx/Observable;", "observeTypingUsers", "(J)Lrx/Observable;", "setUserTyping", "(J)V", "handleTypingStart", "Lcom/discord/api/message/Message;", "message", "handleMessageCreate", "(Lcom/discord/api/message/Message;)V", "snapshotData", "()V", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Ljava/util/HashMap;", "", "Lrx/Subscription;", "Lkotlin/collections/HashMap;", "typingUsersRemoveCallbacks", "Ljava/util/HashMap;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/stores/StoreStream;", "stream", "Lcom/discord/stores/StoreStream;", "", "typingUsers", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "typingUsersSnapshot", "Ljava/util/Map;", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/utilities/rest/RestAPI;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserTyping extends StoreV2 {
    private final Dispatcher dispatcher;
    private final ObservationDeck observationDeck;
    private final RestAPI restAPI;
    private final StoreStream stream;
    private final HashMap<Long, Set<Long>> typingUsers;
    private final HashMap<Long, Map<Long, Subscription>> typingUsersRemoveCallbacks;
    private Map<Long, ? extends Set<Long>> typingUsersSnapshot;

    public /* synthetic */ StoreUserTyping(StoreStream storeStream, Dispatcher dispatcher, ObservationDeck observationDeck, RestAPI restAPI, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(storeStream, dispatcher, (i & 4) != 0 ? ObservationDeckProvider.get() : observationDeck, (i & 8) != 0 ? RestAPI.Companion.getApi() : restAPI);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Map<Long, Set<Long>> getTypingUsers() {
        return this.typingUsersSnapshot;
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleTypingStop(TypingUser typingUser) {
        Set<Long> set = this.typingUsers.get(Long.valueOf(typingUser.a()));
        if (set != null) {
            m.checkNotNullExpressionValue(set, "typingUsers[typing.channelId] ?: return");
            if (set.remove(Long.valueOf(typingUser.d()))) {
                markChanged();
            }
        }
    }

    @StoreThread
    public final void handleMessageCreate(Message message) {
        m.checkNotNullParameter(message, "message");
        User e = message.e();
        if (e != null) {
            long i = e.i();
            Set<Long> set = this.typingUsers.get(Long.valueOf(message.g()));
            if (set != null) {
                m.checkNotNullExpressionValue(set, "typingUsers[message.channelId] ?: return");
                if (set.remove(Long.valueOf(i))) {
                    markChanged();
                }
            }
        }
    }

    @StoreThread
    public final void handleTypingStart(TypingUser typingUser) {
        m.checkNotNullParameter(typingUser, "typing");
        long id2 = this.stream.getUsers$app_productionGoogleRelease().getMeInternal$app_productionGoogleRelease().getId();
        long d = typingUser.d();
        if (id2 != d) {
            long a = typingUser.a();
            HashMap<Long, Map<Long, Subscription>> hashMap = this.typingUsersRemoveCallbacks;
            Long valueOf = Long.valueOf(a);
            Map<Long, Subscription> map = hashMap.get(valueOf);
            if (map == null) {
                map = new HashMap<>();
                hashMap.put(valueOf, map);
            }
            Subscription subscription = map.get(Long.valueOf(d));
            if (subscription != null) {
                subscription.unsubscribe();
            }
            Observable<T> p = new k(typingUser).p(10L, TimeUnit.SECONDS);
            m.checkNotNullExpressionValue(p, "Observable\n        .just…lay(10, TimeUnit.SECONDS)");
            ObservableExtensionsKt.appSubscribe(p, (r18 & 1) != 0 ? null : null, "typingRemove", (r18 & 4) != 0 ? null : new StoreUserTyping$handleTypingStart$2(this, a, d), new StoreUserTyping$handleTypingStart$1(this), (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
            HashMap<Long, Set<Long>> hashMap2 = this.typingUsers;
            Long valueOf2 = Long.valueOf(a);
            Set<Long> set = hashMap2.get(valueOf2);
            if (set == null) {
                set = new HashSet<>();
                hashMap2.put(valueOf2, set);
            }
            if (set.add(Long.valueOf(d))) {
                markChanged();
            }
        }
    }

    public final Observable<Set<Long>> observeTypingUsers(final long j) {
        Observable<Set<Long>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreUserTyping$observeTypingUsers$1(this), 14, null).F(new b<Map<Long, ? extends Set<? extends Long>>, Set<? extends Long>>() { // from class: com.discord.stores.StoreUserTyping$observeTypingUsers$2
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Set<? extends Long> call(Map<Long, ? extends Set<? extends Long>> map) {
                return call2((Map<Long, ? extends Set<Long>>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Set<Long> call2(Map<Long, ? extends Set<Long>> map) {
                m.checkNotNullExpressionValue(map, "typingUsersByChannel");
                Set<Long> set = map.get(Long.valueOf(j));
                if (set == null) {
                    set = n0.emptySet();
                }
                return set;
            }
        }).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR…  .distinctUntilChanged()");
        return q;
    }

    public final void setUserTyping(long j) {
        if (j != 0) {
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.setUserTyping(j, new RestAPIParams.EmptyBody()), false, 1, null), (r18 & 1) != 0 ? null : null, "typingEvent", (r18 & 4) != 0 ? null : null, new StoreUserTyping$setUserTyping$1(this, j), (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
        }
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        super.snapshotData();
        HashMap hashMap = new HashMap();
        for (Map.Entry<Long, Set<Long>> entry : this.typingUsers.entrySet()) {
            hashMap.put(Long.valueOf(entry.getKey().longValue()), new HashSet(entry.getValue()));
        }
        this.typingUsersSnapshot = hashMap;
    }

    public StoreUserTyping(StoreStream storeStream, Dispatcher dispatcher, ObservationDeck observationDeck, RestAPI restAPI) {
        m.checkNotNullParameter(storeStream, "stream");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(restAPI, "restAPI");
        this.stream = storeStream;
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
        this.restAPI = restAPI;
        this.typingUsersRemoveCallbacks = new HashMap<>();
        this.typingUsers = new HashMap<>();
        this.typingUsersSnapshot = h0.emptyMap();
    }
}
