package com.discord.stores;

import com.discord.models.domain.emoji.ModelEmojiGuild;
import d0.t.n;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreEmojiGuild.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreEmojiGuild$deleteEmoji$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $emojiId;
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ StoreEmojiGuild this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreEmojiGuild$deleteEmoji$1(StoreEmojiGuild storeEmojiGuild, long j, long j2) {
        super(0);
        this.this$0 = storeEmojiGuild;
        this.$guildId = j;
        this.$emojiId = j2;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        Map map;
        Map map2;
        map = this.this$0.guildEmoji;
        Long valueOf = Long.valueOf(this.$guildId);
        map2 = this.this$0.guildEmoji;
        List list = (List) map2.get(Long.valueOf(this.$guildId));
        if (list == null) {
            list = n.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        for (Object obj : list) {
            if (this.$emojiId != ((ModelEmojiGuild) obj).getId()) {
                arrayList.add(obj);
            }
        }
        map.put(valueOf, arrayList);
        this.this$0.markChanged();
    }
}
