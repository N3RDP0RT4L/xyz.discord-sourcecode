package com.discord.stores;

import com.discord.app.AppComponent;
import com.discord.stores.StoreAnalytics;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.AppStartAnalyticsTracker;
import com.discord.widgets.auth.WidgetAuthLanding;
import com.discord.widgets.guilds.invite.WidgetGuildInvite;
import com.discord.widgets.home.WidgetHome;
import com.discord.widgets.share.WidgetIncomingShare;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreAnalytics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAnalytics$onScreenViewed$1 extends o implements Function0<Unit> {
    public final /* synthetic */ StoreAnalytics.ScreenViewed $screenViewed;
    public final /* synthetic */ StoreAnalytics this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreAnalytics$onScreenViewed$1(StoreAnalytics storeAnalytics, StoreAnalytics.ScreenViewed screenViewed) {
        super(0);
        this.this$0 = storeAnalytics;
        this.$screenViewed = screenViewed;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        String str;
        StoreStream storeStream;
        Class<? extends AppComponent> screen = this.$screenViewed.getScreen();
        if (m.areEqual(screen, WidgetHome.class)) {
            storeStream = this.this$0.stores;
            str = storeStream.getGuildSelected$app_productionGoogleRelease().getSelectedGuildId() == 0 ? "private_channel" : "guild";
        } else if (m.areEqual(screen, WidgetGuildInvite.class)) {
            str = "invite";
        } else if (m.areEqual(screen, WidgetIncomingShare.class)) {
            str = AnalyticsTracker.ATTACHMENT_SOURCE_SHARE;
        } else {
            str = m.areEqual(screen, WidgetAuthLanding.class) ? "app_landing" : this.$screenViewed.getScreen().getSimpleName();
        }
        AppStartAnalyticsTracker companion = AppStartAnalyticsTracker.Companion.getInstance();
        m.checkNotNullExpressionValue(str, "screenName");
        companion.appUiViewed(str, this.$screenViewed.getTimestamp());
    }
}
