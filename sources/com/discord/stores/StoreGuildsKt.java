package com.discord.stores;

import kotlin.Metadata;
/* compiled from: StoreGuilds.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002*\f\b\u0002\u0010\u0001\"\u00020\u00002\u00020\u0000*\f\b\u0002\u0010\u0003\"\u00020\u00022\u00020\u0002*\f\b\u0002\u0010\u0005\"\u00020\u00042\u00020\u0004*\f\b\u0002\u0010\u0007\"\u00020\u00062\u00020\u0006¨\u0006\b"}, d2 = {"Lcom/discord/api/guild/Guild;", "ApiGuild", "Lcom/discord/api/guildmember/GuildMember;", "ApiGuildMember", "Lcom/discord/models/guild/Guild;", "ClientGuild", "Lcom/discord/models/member/GuildMember;", "ClientGuildMember", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildsKt {
}
