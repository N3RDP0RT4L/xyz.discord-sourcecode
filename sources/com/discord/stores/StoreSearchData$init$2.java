package com.discord.stores;

import com.discord.utilities.search.validation.SearchData;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreSearchData.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/search/validation/SearchData;", "p1", "", "invoke", "(Lcom/discord/utilities/search/validation/SearchData;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final /* synthetic */ class StoreSearchData$init$2 extends k implements Function1<SearchData, Unit> {
    public StoreSearchData$init$2(StoreSearchData storeSearchData) {
        super(1, storeSearchData, StoreSearchData.class, "handleNewData", "handleNewData(Lcom/discord/utilities/search/validation/SearchData;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(SearchData searchData) {
        invoke2(searchData);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(SearchData searchData) {
        m.checkNotNullParameter(searchData, "p1");
        ((StoreSearchData) this.receiver).handleNewData(searchData);
    }
}
