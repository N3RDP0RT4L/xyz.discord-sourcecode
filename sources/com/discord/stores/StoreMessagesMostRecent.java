package com.discord.stores;

import andhook.lib.HookHelper;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guild.Guild;
import com.discord.api.message.Message;
import com.discord.models.domain.ModelChannelUnreadUpdate;
import com.discord.models.domain.ModelPayload;
import com.discord.models.domain.ModelReadState;
import com.discord.models.thread.dto.ModelThreadListSync;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.collections.CollectionExtensionsKt;
import com.discord.utilities.message.MessageUtils;
import com.discord.utilities.persister.Persister;
import d0.t.h0;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreMessagesMostRecent.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0086\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0011\u0012\b\b\u0002\u00109\u001a\u000208¢\u0006\u0004\b>\u0010?J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u001d\u0010\n\u001a\u00020\u00042\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u0007H\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\n\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\fH\u0003¢\u0006\u0004\b\n\u0010\u000eJ'\u0010\u0015\u001a\u00020\u00142\n\u0010\u0011\u001a\u00060\u000fj\u0002`\u00102\n\u0010\u0013\u001a\u00060\u000fj\u0002`\u0012H\u0003¢\u0006\u0004\b\u0015\u0010\u0016J!\u0010\u0018\u001a\u0016\u0012\b\u0012\u00060\u000fj\u0002`\u0010\u0012\b\u0012\u00060\u000fj\u0002`\u00120\u0017¢\u0006\u0004\b\u0018\u0010\u0019J'\u0010\u001b\u001a\u001c\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u000fj\u0002`\u0010\u0012\b\u0012\u00060\u000fj\u0002`\u00120\u00170\u001a¢\u0006\u0004\b\u001b\u0010\u001cJ#\u0010\u001b\u001a\f\u0012\b\u0012\u00060\u000fj\u0002`\u00120\u001a2\n\u0010\u0011\u001a\u00060\u000fj\u0002`\u0010¢\u0006\u0004\b\u001b\u0010\u001dJ\u0017\u0010 \u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\u001eH\u0007¢\u0006\u0004\b \u0010!J\u0017\u0010$\u001a\u00020\u00042\u0006\u0010#\u001a\u00020\"H\u0007¢\u0006\u0004\b$\u0010%J\u0017\u0010'\u001a\u00020\u00042\u0006\u0010&\u001a\u00020\bH\u0007¢\u0006\u0004\b'\u0010(J\u0017\u0010)\u001a\u00020\u00042\u0006\u0010&\u001a\u00020\bH\u0007¢\u0006\u0004\b)\u0010(J\u0017\u0010+\u001a\u00020\u00042\u0006\u0010*\u001a\u00020\fH\u0007¢\u0006\u0004\b+\u0010\u000eJ\u0017\u0010-\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020,H\u0007¢\u0006\u0004\b-\u0010.J\u0017\u0010/\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b/\u0010\u0006J\u000f\u00100\u001a\u00020\u0004H\u0017¢\u0006\u0004\b0\u00101RB\u00104\u001a.\u0012\b\u0012\u00060\u000fj\u0002`\u0010\u0012\b\u0012\u00060\u000fj\u0002`\u001202j\u0016\u0012\b\u0012\u00060\u000fj\u0002`\u0010\u0012\b\u0012\u00060\u000fj\u0002`\u0012`38\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b4\u00105R*\u00106\u001a\u0016\u0012\b\u0012\u00060\u000fj\u0002`\u0010\u0012\b\u0012\u00060\u000fj\u0002`\u00120\u00178\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b6\u00107R\u0016\u00109\u001a\u0002088\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b9\u0010:R0\u0010<\u001a\u001c\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u000fj\u0002`\u0010\u0012\b\u0012\u00060\u000fj\u0002`\u00120\u00170;8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b<\u0010=¨\u0006@"}, d2 = {"Lcom/discord/stores/StoreMessagesMostRecent;", "Lcom/discord/stores/StoreV2;", "Lcom/discord/api/message/Message;", "message", "", "mostRecentIdsUpdateFromMessage", "(Lcom/discord/api/message/Message;)V", "", "Lcom/discord/api/channel/Channel;", "channels", "mostRecentIdsUpdateFromChannels", "(Ljava/util/Collection;)V", "Lcom/discord/models/domain/ModelChannelUnreadUpdate;", "channelUnreadUpdate", "(Lcom/discord/models/domain/ModelChannelUnreadUpdate;)V", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/primitives/MessageId;", "messageId", "", "updateMostRecentIds", "(JJ)Z", "", "getMostRecentIds", "()Ljava/util/Map;", "Lrx/Observable;", "observeRecentMessageIds", "()Lrx/Observable;", "(J)Lrx/Observable;", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "Lcom/discord/api/guild/Guild;", "guild", "handleGuildAdd", "(Lcom/discord/api/guild/Guild;)V", "channel", "handleChannelCreateOrUpdate", "(Lcom/discord/api/channel/Channel;)V", "handleThreadCreateOrUpdate", "channelReadStateUpdate", "handleChannelUnreadUpdate", "Lcom/discord/models/thread/dto/ModelThreadListSync;", "handleThreadListSync", "(Lcom/discord/models/thread/dto/ModelThreadListSync;)V", "handleMessageCreate", "snapshotData", "()V", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "mostRecentIds", "Ljava/util/HashMap;", "mostRecentIdsSnapshot", "Ljava/util/Map;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/utilities/persister/Persister;", "mostRecentIdsCache", "Lcom/discord/utilities/persister/Persister;", HookHelper.constructorName, "(Lcom/discord/stores/updates/ObservationDeck;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessagesMostRecent extends StoreV2 {
    private final HashMap<Long, Long> mostRecentIds;
    private final Persister<Map<Long, Long>> mostRecentIdsCache;
    private Map<Long, Long> mostRecentIdsSnapshot;
    private final ObservationDeck observationDeck;

    public StoreMessagesMostRecent() {
        this(null, 1, null);
    }

    public /* synthetic */ StoreMessagesMostRecent(ObservationDeck observationDeck, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? ObservationDeckProvider.get() : observationDeck);
    }

    @StoreThread
    private final void mostRecentIdsUpdateFromChannels(ModelChannelUnreadUpdate modelChannelUnreadUpdate) {
        for (ModelReadState modelReadState : modelChannelUnreadUpdate.getChannelReadStates()) {
            if (updateMostRecentIds(modelReadState.getChannelId(), modelReadState.getLastMessageId())) {
                markChanged();
            }
        }
    }

    @StoreThread
    private final void mostRecentIdsUpdateFromMessage(Message message) {
        if (updateMostRecentIds(message.g(), message.o())) {
            markChanged();
        }
    }

    @StoreThread
    private final boolean updateMostRecentIds(long j, long j2) {
        boolean z2 = MessageUtils.compareMessages(this.mostRecentIds.get(Long.valueOf(j)), Long.valueOf(j2)) < 0;
        if (z2) {
            this.mostRecentIds.put(Long.valueOf(j), Long.valueOf(j2));
        }
        return z2;
    }

    public final Map<Long, Long> getMostRecentIds() {
        return this.mostRecentIdsSnapshot;
    }

    @StoreThread
    public final void handleChannelCreateOrUpdate(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        mostRecentIdsUpdateFromChannels(d0.t.m.listOf(channel));
    }

    @StoreThread
    public final void handleChannelUnreadUpdate(ModelChannelUnreadUpdate modelChannelUnreadUpdate) {
        m.checkNotNullParameter(modelChannelUnreadUpdate, "channelReadStateUpdate");
        mostRecentIdsUpdateFromChannels(modelChannelUnreadUpdate);
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        Collection<Channel> privateChannels = modelPayload.getPrivateChannels();
        m.checkNotNullExpressionValue(privateChannels, "payload.privateChannels");
        mostRecentIdsUpdateFromChannels(privateChannels);
        List<Guild> guilds = modelPayload.getGuilds();
        m.checkNotNullExpressionValue(guilds, "payload.guilds");
        for (Guild guild : guilds) {
            Collection<Channel> g = guild.g();
            if (g != null) {
                mostRecentIdsUpdateFromChannels(g);
            }
            List<Channel> N = guild.N();
            if (N != null) {
                Collection<Channel> arrayList = new ArrayList<>();
                for (Object obj : N) {
                    if (ChannelUtils.C((Channel) obj)) {
                        arrayList.add(obj);
                    }
                }
                mostRecentIdsUpdateFromChannels(arrayList);
            }
        }
        markChanged();
    }

    @StoreThread
    public final void handleGuildAdd(Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        Collection<Channel> g = guild.g();
        if (g != null) {
            mostRecentIdsUpdateFromChannels(g);
        }
        List<Channel> N = guild.N();
        if (N != null) {
            Collection<Channel> arrayList = new ArrayList<>();
            for (Object obj : N) {
                if (ChannelUtils.C((Channel) obj)) {
                    arrayList.add(obj);
                }
            }
            mostRecentIdsUpdateFromChannels(arrayList);
        }
    }

    @StoreThread
    public final void handleMessageCreate(Message message) {
        m.checkNotNullParameter(message, "message");
        mostRecentIdsUpdateFromMessage(message);
    }

    @StoreThread
    public final void handleThreadCreateOrUpdate(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        if (ChannelUtils.C(channel)) {
            mostRecentIdsUpdateFromChannels(d0.t.m.listOf(channel));
        }
    }

    @StoreThread
    public final void handleThreadListSync(ModelThreadListSync modelThreadListSync) {
        m.checkNotNullParameter(modelThreadListSync, "payload");
        List<Channel> threads = modelThreadListSync.getThreads();
        Collection<Channel> arrayList = new ArrayList<>();
        for (Object obj : threads) {
            if (ChannelUtils.C((Channel) obj)) {
                arrayList.add(obj);
            }
        }
        mostRecentIdsUpdateFromChannels(arrayList);
    }

    public final Observable<Map<Long, Long>> observeRecentMessageIds() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreMessagesMostRecent$observeRecentMessageIds$1(this), 14, null);
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        super.snapshotData();
        HashMap snapshot$default = CollectionExtensionsKt.snapshot$default(this.mostRecentIds, 0, 0.0f, 3, null);
        this.mostRecentIdsSnapshot = snapshot$default;
        Persister.set$default(this.mostRecentIdsCache, snapshot$default, false, 2, null);
    }

    public StoreMessagesMostRecent(ObservationDeck observationDeck) {
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.observationDeck = observationDeck;
        HashMap<Long, Long> hashMap = new HashMap<>();
        this.mostRecentIds = hashMap;
        this.mostRecentIdsSnapshot = h0.emptyMap();
        this.mostRecentIdsCache = new Persister<>("MOST_RECENT_MESSAGE_IDS", new HashMap(hashMap));
    }

    public final Observable<Long> observeRecentMessageIds(final long j) {
        Observable<Long> q = observeRecentMessageIds().F(new b<Map<Long, ? extends Long>, Long>() { // from class: com.discord.stores.StoreMessagesMostRecent$observeRecentMessageIds$2
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Long call(Map<Long, ? extends Long> map) {
                return call2((Map<Long, Long>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Long call2(Map<Long, Long> map) {
                Long l = map.get(Long.valueOf(j));
                return Long.valueOf(l != null ? l.longValue() : 0L);
            }
        }).q();
        m.checkNotNullExpressionValue(q, "observeRecentMessageIds(…  .distinctUntilChanged()");
        return q;
    }

    @StoreThread
    private final void mostRecentIdsUpdateFromChannels(Collection<Channel> collection) {
        for (Channel channel : collection) {
            if (ChannelUtils.B(channel) && updateMostRecentIds(channel.h(), channel.i())) {
                markChanged();
            }
        }
    }
}
