package com.discord.stores;

import com.discord.api.channel.Channel;
import com.discord.api.voice.state.StageRequestToSpeakState;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreStageChannels.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/api/voice/state/StageRequestToSpeakState;", "invoke", "()Lcom/discord/api/voice/state/StageRequestToSpeakState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStageChannels$observeMyRequestToSpeakState$1 extends o implements Function0<StageRequestToSpeakState> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ StoreStageChannels this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreStageChannels$observeMyRequestToSpeakState$1(StoreStageChannels storeStageChannels, long j) {
        super(0);
        this.this$0 = storeStageChannels;
        this.$channelId = j;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final StageRequestToSpeakState invoke() {
        StoreChannels storeChannels;
        StoreUser storeUser;
        storeChannels = this.this$0.channelsStore;
        Channel channel = storeChannels.getChannel(this.$channelId);
        if (channel == null) {
            return StageRequestToSpeakState.NONE;
        }
        StageChannelRoleContext roleContext$default = StoreStageChannels.roleContext$default(this.this$0, channel, null, null, null, 14, null);
        if (roleContext$default != null) {
            storeUser = this.this$0.userStore;
            StageRequestToSpeakState requestToSpeakState = roleContext$default.getRequestToSpeakState(storeUser.getMe().getId());
            if (requestToSpeakState != null) {
                return requestToSpeakState;
            }
        }
        return StageRequestToSpeakState.NONE;
    }
}
