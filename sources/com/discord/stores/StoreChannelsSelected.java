package com.discord.stores;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guild.Guild;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelPayload;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.frecency.FrecencyTracker;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.persister.Persister;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.user.search.ChannelFrecencyTracker;
import d0.t.g0;
import d0.t.h0;
import d0.t.n0;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import j0.l.e.k;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Subscription;
/* compiled from: StoreChannelsSelected.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Â\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 r2\u00020\u0001:\u0003rstB7\u0012\u0006\u0010X\u001a\u00020W\u0012\u0006\u0010j\u001a\u00020i\u0012\u0006\u0010\\\u001a\u00020[\u0012\u0006\u0010c\u001a\u00020b\u0012\u0006\u0010m\u001a\u00020l\u0012\u0006\u0010U\u001a\u00020T¢\u0006\u0004\bp\u0010qJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\t\u001a\u00020\u0004H\u0003¢\u0006\u0004\b\t\u0010\bJ\u000f\u0010\n\u001a\u00020\u0004H\u0003¢\u0006\u0004\b\n\u0010\bJk\u0010\u0019\u001a\u00020\u00022\b\u0010\f\u001a\u0004\u0018\u00010\u000b2\b\u0010\u000e\u001a\u0004\u0018\u00010\r2\u0016\u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\u0010j\u0002`\u0011\u0012\u0004\u0012\u00020\r0\u000f2\n\u0010\u0014\u001a\u00060\u0010j\u0002`\u00132\u001a\u0010\u0016\u001a\u0016\u0012\b\u0012\u00060\u0010j\u0002`\u0011\u0012\b\u0012\u00060\u0010j\u0002`\u00150\u000f2\u0006\u0010\u0018\u001a\u00020\u0017H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ?\u0010\u001c\u001a\u00020\u00172\u0006\u0010\u001b\u001a\u00020\r2\n\u0010\u0014\u001a\u00060\u0010j\u0002`\u00132\u001a\u0010\u0016\u001a\u0016\u0012\b\u0012\u00060\u0010j\u0002`\u0011\u0012\b\u0012\u00060\u0010j\u0002`\u00150\u000fH\u0002¢\u0006\u0004\b\u001c\u0010\u001dJQ\u0010\u001e\u001a\u0004\u0018\u00010\r2\u0016\u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\u0010j\u0002`\u0011\u0012\u0004\u0012\u00020\r0\u000f2\n\u0010\u0014\u001a\u00060\u0010j\u0002`\u00132\u001a\u0010\u0016\u001a\u0016\u0012\b\u0012\u00060\u0010j\u0002`\u0011\u0012\b\u0012\u00060\u0010j\u0002`\u00150\u000fH\u0002¢\u0006\u0004\b\u001e\u0010\u001fJ\u0013\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00170 ¢\u0006\u0004\b!\u0010\"J\u0017\u0010#\u001a\f\u0012\b\u0012\u00060\u0010j\u0002`\u00110 ¢\u0006\u0004\b#\u0010\"J\u0011\u0010$\u001a\u00060\u0010j\u0002`\u0011¢\u0006\u0004\b$\u0010%J\u000f\u0010&\u001a\u0004\u0018\u00010\r¢\u0006\u0004\b&\u0010'J\u0015\u0010(\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\r0 ¢\u0006\u0004\b(\u0010\"J\u0013\u0010)\u001a\b\u0012\u0004\u0012\u00020\u00020 ¢\u0006\u0004\b)\u0010\"J\u0017\u0010*\u001a\f\u0012\b\u0012\u00060\u0010j\u0002`\u00110 ¢\u0006\u0004\b*\u0010\"J\u000f\u0010+\u001a\u00020\u0004H\u0007¢\u0006\u0004\b+\u0010\bJ\u0017\u0010.\u001a\u00020\u00042\u0006\u0010-\u001a\u00020,H\u0007¢\u0006\u0004\b.\u0010/JA\u00104\u001a\u00020\u00042\n\u0010\u0014\u001a\u00060\u0010j\u0002`\u00132\n\u00100\u001a\u00060\u0010j\u0002`\u00112\u000e\u00101\u001a\n\u0018\u00010\u0010j\u0004\u0018\u0001`\u00112\b\u00103\u001a\u0004\u0018\u000102H\u0007¢\u0006\u0004\b4\u00105JA\u0010:\u001a\u00020\u00042\n\u0010\u0014\u001a\u00060\u0010j\u0002`\u00132\n\u00100\u001a\u00060\u0010j\u0002`\u00112\u000e\u00107\u001a\n\u0018\u00010\u0010j\u0004\u0018\u0001`62\b\u00109\u001a\u0004\u0018\u000108H\u0007¢\u0006\u0004\b:\u0010;J\u000f\u0010<\u001a\u00020\u0004H\u0007¢\u0006\u0004\b<\u0010\bJ\u000f\u0010=\u001a\u00020\u0004H\u0007¢\u0006\u0004\b=\u0010\bJ\u0017\u0010@\u001a\u00020\u00042\u0006\u0010?\u001a\u00020>H\u0007¢\u0006\u0004\b@\u0010AJ\u0017\u0010B\u001a\u00020\u00042\u0006\u0010?\u001a\u00020>H\u0007¢\u0006\u0004\bB\u0010AJ\u000f\u0010C\u001a\u00020\u0004H\u0017¢\u0006\u0004\bC\u0010\bJ\u000f\u0010D\u001a\u00020\u0004H\u0007¢\u0006\u0004\bD\u0010\bJ\u0017\u0010E\u001a\u00020\u00042\u0006\u0010\u001b\u001a\u00020\rH\u0007¢\u0006\u0004\bE\u0010FJ\u0017\u0010G\u001a\u00020\u00042\u0006\u0010\u001b\u001a\u00020\rH\u0007¢\u0006\u0004\bG\u0010FR0\u0010I\u001a\u001c\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0010j\u0002`\u0013\u0012\b\u0012\u00060\u0010j\u0002`\u00110\u000f0H8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bI\u0010JR\u0019\u0010L\u001a\u00020K8\u0006@\u0006¢\u0006\f\n\u0004\bL\u0010M\u001a\u0004\bN\u0010OR\u0016\u0010P\u001a\u00020\u00178\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bP\u0010QR\u0016\u0010R\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bR\u0010SR\u0016\u0010U\u001a\u00020T8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bU\u0010VR\u0016\u0010X\u001a\u00020W8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bX\u0010YR\u0016\u0010Z\u001a\u00020\u00178\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bZ\u0010QR\u0016\u0010\\\u001a\u00020[8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\\\u0010]R\u0018\u0010_\u001a\u0004\u0018\u00010^8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b_\u0010`R\u001c\u0010a\u001a\b\u0012\u0004\u0012\u00020K0H8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\ba\u0010JR\u0016\u0010c\u001a\u00020b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bc\u0010dR&\u0010f\u001a\u0012\u0012\b\u0012\u00060\u0010j\u0002`\u0013\u0012\u0004\u0012\u00020\u000b0e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bf\u0010gR\u0016\u0010h\u001a\u00020\u00178\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bh\u0010QR\u0016\u0010j\u001a\u00020i8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bj\u0010kR\u0016\u0010m\u001a\u00020l8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bm\u0010nR\u0016\u0010o\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bo\u0010S¨\u0006u"}, d2 = {"Lcom/discord/stores/StoreChannelsSelected;", "Lcom/discord/stores/StoreV2;", "Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;", "resolvedSelectedChannel", "", "onSelectedChannelResolved", "(Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;)V", "updateInitializationState", "()V", "validateSelectedChannel", "loadFromCache", "Lcom/discord/stores/StoreChannelsSelected$UserChannelSelection;", "userChannelSelection", "Lcom/discord/api/channel/Channel;", "resolvedChannel", "", "", "Lcom/discord/primitives/ChannelId;", "channels", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/api/permission/PermissionBit;", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS, "", "storeChannelsInitializedForAuthedUser", "resolveSelectedChannel", "(Lcom/discord/stores/StoreChannelsSelected$UserChannelSelection;Lcom/discord/api/channel/Channel;Ljava/util/Map;JLjava/util/Map;Z)Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;", "channel", "isValidResolution", "(Lcom/discord/api/channel/Channel;JLjava/util/Map;)Z", "getFirstAvailableChannel", "(Ljava/util/Map;JLjava/util/Map;)Lcom/discord/api/channel/Channel;", "Lrx/Observable;", "observeInitializedForAuthedUser", "()Lrx/Observable;", "observeId", "getId", "()J", "getSelectedChannel", "()Lcom/discord/api/channel/Channel;", "observeSelectedChannel", "observeResolvedSelectedChannel", "observePreviousId", "init", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "channelId", "peekParent", "Lcom/discord/stores/SelectedChannelAnalyticsLocation;", "analyticsLocation", "trySelectChannel", "(JJLjava/lang/Long;Lcom/discord/stores/SelectedChannelAnalyticsLocation;)V", "Lcom/discord/primitives/MessageId;", "parentMessageId", "", "threadStartLocation", "openCreateThread", "(JJLjava/lang/Long;Ljava/lang/String;)V", "dismissCreateThread", "handleGuildSelected", "Lcom/discord/api/guild/Guild;", "guild", "handleGuildAdd", "(Lcom/discord/api/guild/Guild;)V", "handleGuildRemove", "snapshotData", "handleStoreInitTimeout", "handleChannelOrThreadDelete", "(Lcom/discord/api/channel/Channel;)V", "handleChannelOrThreadCreateOrUpdate", "Lcom/discord/utilities/persister/Persister;", "selectedChannelIdsCache", "Lcom/discord/utilities/persister/Persister;", "Lcom/discord/widgets/user/search/ChannelFrecencyTracker;", "frecency", "Lcom/discord/widgets/user/search/ChannelFrecencyTracker;", "getFrecency", "()Lcom/discord/widgets/user/search/ChannelFrecencyTracker;", "handledReadyPayload", "Z", "previouslySelectedChannel", "Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/stores/StoreStream;", "stream", "Lcom/discord/stores/StoreStream;", "initializedForAuthedUser", "Lcom/discord/stores/StorePermissions;", "storePermissions", "Lcom/discord/stores/StorePermissions;", "Lrx/Subscription;", "validateSelectedChannelSubscription", "Lrx/Subscription;", "frecencyCache", "Lcom/discord/stores/StoreGuildSelected;", "storeGuildSelected", "Lcom/discord/stores/StoreGuildSelected;", "", "userChannelSelections", "Ljava/util/Map;", "isStoreInitTimedOut", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreChannels;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "selectedChannel", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreGuildSelected;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V", "Companion", "ResolvedSelectedChannel", "UserChannelSelection", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreChannelsSelected extends StoreV2 {
    private static final String CACHE_KEY_SELECTED_CHANNEL_IDS = "CACHE_KEY_SELECTED_CHANNEL_IDS";
    public static final long ID_THREAD_DRAFT = -3;
    public static final long ID_UNAVAILABLE = -1;
    public static final long ID_UNINITIALIZED = -2;
    public static final long ID_UNSELECTED = 0;
    private final Dispatcher dispatcher;
    private final ChannelFrecencyTracker frecency;
    private final Persister<ChannelFrecencyTracker> frecencyCache;
    private boolean handledReadyPayload;
    private boolean initializedForAuthedUser;
    private boolean isStoreInitTimedOut;
    private final ObservationDeck observationDeck;
    private ResolvedSelectedChannel previouslySelectedChannel;
    private ResolvedSelectedChannel selectedChannel;
    private final StoreChannels storeChannels;
    private final StoreGuildSelected storeGuildSelected;
    private final StorePermissions storePermissions;
    private final StoreStream stream;
    private Subscription validateSelectedChannelSubscription;
    public static final Companion Companion = new Companion(null);
    private static final StoreChannelsSelected$Companion$InitializedUpdateSource$1 InitializedUpdateSource = new ObservationDeck.UpdateSource() { // from class: com.discord.stores.StoreChannelsSelected$Companion$InitializedUpdateSource$1
    };
    private final Map<Long, UserChannelSelection> userChannelSelections = new HashMap();
    private final Persister<Map<Long, Long>> selectedChannelIdsCache = new Persister<>(CACHE_KEY_SELECTED_CHANNEL_IDS, new HashMap());

    /* compiled from: StoreChannelsSelected.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\b\u0006*\u0001\u000b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00058\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007R\u0016\u0010\b\u001a\u00020\u00058\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\b\u0010\u0007R\u0016\u0010\t\u001a\u00020\u00058\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\t\u0010\u0007R\u0016\u0010\n\u001a\u00020\u00058\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\n\u0010\u0007R\u0016\u0010\f\u001a\u00020\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\r¨\u0006\u0010"}, d2 = {"Lcom/discord/stores/StoreChannelsSelected$Companion;", "", "", StoreChannelsSelected.CACHE_KEY_SELECTED_CHANNEL_IDS, "Ljava/lang/String;", "", "ID_THREAD_DRAFT", "J", "ID_UNAVAILABLE", "ID_UNINITIALIZED", "ID_UNSELECTED", "com/discord/stores/StoreChannelsSelected$Companion$InitializedUpdateSource$1", "InitializedUpdateSource", "Lcom/discord/stores/StoreChannelsSelected$Companion$InitializedUpdateSource$1;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreChannelsSelected.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0005\u000f\u0010\u0011\u0012\u0013B\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eR\u0017\u0010\u0006\u001a\u00060\u0002j\u0002`\u00038F@\u0006¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005R\u0015\u0010\n\u001a\u0004\u0018\u00010\u00078F@\u0006¢\u0006\u0006\u001a\u0004\b\b\u0010\tR\u0015\u0010\f\u001a\u0004\u0018\u00010\u00078F@\u0006¢\u0006\u0006\u001a\u0004\b\u000b\u0010\t\u0082\u0001\u0005\u0014\u0015\u0016\u0017\u0018¨\u0006\u0019"}, d2 = {"Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;", "", "", "Lcom/discord/primitives/ChannelId;", "getId", "()J", ModelAuditLogEntry.CHANGE_KEY_ID, "Lcom/discord/api/channel/Channel;", "getMaybeChannel", "()Lcom/discord/api/channel/Channel;", "maybeChannel", "getChannelOrParent", "channelOrParent", HookHelper.constructorName, "()V", "Channel", "ThreadDraft", "Unavailable", "Uninitialized", "Unselected", "Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Channel;", "Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$ThreadDraft;", "Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Unselected;", "Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Unavailable;", "Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Uninitialized;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class ResolvedSelectedChannel {

        /* compiled from: StoreChannelsSelected.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u000e\u0010\r\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\t¢\u0006\u0004\b'\u0010(J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0018\u0010\u0007\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ8\u0010\u000f\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\u0010\b\u0002\u0010\r\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00062\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\tHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u001a\u0010\u001a\u001a\u00020\u00192\b\u0010\u0018\u001a\u0004\u0018\u00010\u0017HÖ\u0003¢\u0006\u0004\b\u001a\u0010\u001bR\u0019\u0010\u001d\u001a\u00020\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 R\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010!\u001a\u0004\b\"\u0010\u0004R!\u0010\r\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010#\u001a\u0004\b$\u0010\bR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010%\u001a\u0004\b&\u0010\u000b¨\u0006)"}, d2 = {"Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Channel;", "Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "Lcom/discord/primitives/ChannelId;", "component2", "()Ljava/lang/Long;", "Lcom/discord/stores/SelectedChannelAnalyticsLocation;", "component3", "()Lcom/discord/stores/SelectedChannelAnalyticsLocation;", "channel", "peekParent", "analyticsLocation", "copy", "(Lcom/discord/api/channel/Channel;Ljava/lang/Long;Lcom/discord/stores/SelectedChannelAnalyticsLocation;)Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Channel;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/ChannelAnalyticsViewType;", "analyticsViewType", "Lcom/discord/stores/ChannelAnalyticsViewType;", "getAnalyticsViewType", "()Lcom/discord/stores/ChannelAnalyticsViewType;", "Lcom/discord/api/channel/Channel;", "getChannel", "Ljava/lang/Long;", "getPeekParent", "Lcom/discord/stores/SelectedChannelAnalyticsLocation;", "getAnalyticsLocation", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Ljava/lang/Long;Lcom/discord/stores/SelectedChannelAnalyticsLocation;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Channel extends ResolvedSelectedChannel {
            private final SelectedChannelAnalyticsLocation analyticsLocation;
            private final ChannelAnalyticsViewType analyticsViewType;
            private final com.discord.api.channel.Channel channel;
            private final Long peekParent;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Channel(com.discord.api.channel.Channel channel, Long l, SelectedChannelAnalyticsLocation selectedChannelAnalyticsLocation) {
                super(null);
                ChannelAnalyticsViewType channelAnalyticsViewType;
                m.checkNotNullParameter(channel, "channel");
                this.channel = channel;
                this.peekParent = l;
                this.analyticsLocation = selectedChannelAnalyticsLocation;
                if (l != null) {
                    channelAnalyticsViewType = ChannelAnalyticsViewType.PEEK_VIEW;
                } else {
                    channelAnalyticsViewType = ChannelAnalyticsViewType.FULL_VIEW;
                }
                this.analyticsViewType = channelAnalyticsViewType;
            }

            public static /* synthetic */ Channel copy$default(Channel channel, com.discord.api.channel.Channel channel2, Long l, SelectedChannelAnalyticsLocation selectedChannelAnalyticsLocation, int i, Object obj) {
                if ((i & 1) != 0) {
                    channel2 = channel.channel;
                }
                if ((i & 2) != 0) {
                    l = channel.peekParent;
                }
                if ((i & 4) != 0) {
                    selectedChannelAnalyticsLocation = channel.analyticsLocation;
                }
                return channel.copy(channel2, l, selectedChannelAnalyticsLocation);
            }

            public final com.discord.api.channel.Channel component1() {
                return this.channel;
            }

            public final Long component2() {
                return this.peekParent;
            }

            public final SelectedChannelAnalyticsLocation component3() {
                return this.analyticsLocation;
            }

            public final Channel copy(com.discord.api.channel.Channel channel, Long l, SelectedChannelAnalyticsLocation selectedChannelAnalyticsLocation) {
                m.checkNotNullParameter(channel, "channel");
                return new Channel(channel, l, selectedChannelAnalyticsLocation);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Channel)) {
                    return false;
                }
                Channel channel = (Channel) obj;
                return m.areEqual(this.channel, channel.channel) && m.areEqual(this.peekParent, channel.peekParent) && m.areEqual(this.analyticsLocation, channel.analyticsLocation);
            }

            public final SelectedChannelAnalyticsLocation getAnalyticsLocation() {
                return this.analyticsLocation;
            }

            public final ChannelAnalyticsViewType getAnalyticsViewType() {
                return this.analyticsViewType;
            }

            public final com.discord.api.channel.Channel getChannel() {
                return this.channel;
            }

            public final Long getPeekParent() {
                return this.peekParent;
            }

            public int hashCode() {
                com.discord.api.channel.Channel channel = this.channel;
                int i = 0;
                int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
                Long l = this.peekParent;
                int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
                SelectedChannelAnalyticsLocation selectedChannelAnalyticsLocation = this.analyticsLocation;
                if (selectedChannelAnalyticsLocation != null) {
                    i = selectedChannelAnalyticsLocation.hashCode();
                }
                return hashCode2 + i;
            }

            public String toString() {
                StringBuilder R = a.R("Channel(channel=");
                R.append(this.channel);
                R.append(", peekParent=");
                R.append(this.peekParent);
                R.append(", analyticsLocation=");
                R.append(this.analyticsLocation);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: StoreChannelsSelected.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\u000f\u001a\u00020\u0005\u0012\u000e\u0010\u0010\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\f¢\u0006\u0004\b\"\u0010#J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0018\u0010\n\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0012\u0010\r\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ8\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u000f\u001a\u00020\u00052\u0010\b\u0002\u0010\u0010\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t2\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\fHÆ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\u0014\u0010\u000eJ\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u001a\u0010\u001a\u001a\u00020\u00022\b\u0010\u0019\u001a\u0004\u0018\u00010\u0018HÖ\u0003¢\u0006\u0004\b\u001a\u0010\u001bR\u0019\u0010\u000f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001c\u001a\u0004\b\u001d\u0010\u0007R!\u0010\u0010\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001e\u001a\u0004\b\u001f\u0010\u000bR\u001b\u0010\u0011\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010 \u001a\u0004\b!\u0010\u000e¨\u0006$"}, d2 = {"Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$ThreadDraft;", "Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;", "", "hasStarterMessage", "()Z", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "Lcom/discord/primitives/MessageId;", "component2", "()Ljava/lang/Long;", "", "component3", "()Ljava/lang/String;", "parentChannel", "starterMessageId", "threadStartLocation", "copy", "(Lcom/discord/api/channel/Channel;Ljava/lang/Long;Ljava/lang/String;)Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$ThreadDraft;", "toString", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/channel/Channel;", "getParentChannel", "Ljava/lang/Long;", "getStarterMessageId", "Ljava/lang/String;", "getThreadStartLocation", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Ljava/lang/Long;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class ThreadDraft extends ResolvedSelectedChannel {
            private final com.discord.api.channel.Channel parentChannel;
            private final Long starterMessageId;
            private final String threadStartLocation;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public ThreadDraft(com.discord.api.channel.Channel channel, Long l, String str) {
                super(null);
                m.checkNotNullParameter(channel, "parentChannel");
                this.parentChannel = channel;
                this.starterMessageId = l;
                this.threadStartLocation = str;
            }

            public static /* synthetic */ ThreadDraft copy$default(ThreadDraft threadDraft, com.discord.api.channel.Channel channel, Long l, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    channel = threadDraft.parentChannel;
                }
                if ((i & 2) != 0) {
                    l = threadDraft.starterMessageId;
                }
                if ((i & 4) != 0) {
                    str = threadDraft.threadStartLocation;
                }
                return threadDraft.copy(channel, l, str);
            }

            public final com.discord.api.channel.Channel component1() {
                return this.parentChannel;
            }

            public final Long component2() {
                return this.starterMessageId;
            }

            public final String component3() {
                return this.threadStartLocation;
            }

            public final ThreadDraft copy(com.discord.api.channel.Channel channel, Long l, String str) {
                m.checkNotNullParameter(channel, "parentChannel");
                return new ThreadDraft(channel, l, str);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof ThreadDraft)) {
                    return false;
                }
                ThreadDraft threadDraft = (ThreadDraft) obj;
                return m.areEqual(this.parentChannel, threadDraft.parentChannel) && m.areEqual(this.starterMessageId, threadDraft.starterMessageId) && m.areEqual(this.threadStartLocation, threadDraft.threadStartLocation);
            }

            public final com.discord.api.channel.Channel getParentChannel() {
                return this.parentChannel;
            }

            public final Long getStarterMessageId() {
                return this.starterMessageId;
            }

            public final String getThreadStartLocation() {
                return this.threadStartLocation;
            }

            public final boolean hasStarterMessage() {
                return this.starterMessageId != null;
            }

            public int hashCode() {
                com.discord.api.channel.Channel channel = this.parentChannel;
                int i = 0;
                int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
                Long l = this.starterMessageId;
                int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
                String str = this.threadStartLocation;
                if (str != null) {
                    i = str.hashCode();
                }
                return hashCode2 + i;
            }

            public String toString() {
                StringBuilder R = a.R("ThreadDraft(parentChannel=");
                R.append(this.parentChannel);
                R.append(", starterMessageId=");
                R.append(this.starterMessageId);
                R.append(", threadStartLocation=");
                return a.H(R, this.threadStartLocation, ")");
            }
        }

        /* compiled from: StoreChannelsSelected.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Unavailable;", "Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Unavailable extends ResolvedSelectedChannel {
            public static final Unavailable INSTANCE = new Unavailable();

            private Unavailable() {
                super(null);
            }
        }

        /* compiled from: StoreChannelsSelected.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Uninitialized;", "Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Uninitialized extends ResolvedSelectedChannel {
            public static final Uninitialized INSTANCE = new Uninitialized();

            private Uninitialized() {
                super(null);
            }
        }

        /* compiled from: StoreChannelsSelected.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$Unselected;", "Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Unselected extends ResolvedSelectedChannel {
            public static final Unselected INSTANCE = new Unselected();

            private Unselected() {
                super(null);
            }
        }

        private ResolvedSelectedChannel() {
        }

        public final com.discord.api.channel.Channel getChannelOrParent() {
            if (this instanceof Channel) {
                return ((Channel) this).getChannel();
            }
            if (this instanceof ThreadDraft) {
                return ((ThreadDraft) this).getParentChannel();
            }
            return null;
        }

        public final long getId() {
            if (this instanceof Channel) {
                return ((Channel) this).getChannel().h();
            }
            if (this instanceof ThreadDraft) {
                return -3L;
            }
            if (m.areEqual(this, Unselected.INSTANCE)) {
                return 0L;
            }
            if (m.areEqual(this, Unavailable.INSTANCE)) {
                return -1L;
            }
            if (m.areEqual(this, Uninitialized.INSTANCE)) {
                return -2L;
            }
            throw new NoWhenBranchMatchedException();
        }

        public final com.discord.api.channel.Channel getMaybeChannel() {
            Channel channel = (Channel) (!(this instanceof Channel) ? null : this);
            if (channel != null) {
                return channel.getChannel();
            }
            return null;
        }

        public /* synthetic */ ResolvedSelectedChannel(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreChannelsSelected.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u000e\u000f\u0010B\t\b\u0002¢\u0006\u0004\b\f\u0010\rJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0017\u0010\u000b\u001a\u00060\u0007j\u0002`\b8F@\u0006¢\u0006\u0006\u001a\u0004\b\t\u0010\n\u0082\u0001\u0003\u0011\u0012\u0013¨\u0006\u0014"}, d2 = {"Lcom/discord/stores/StoreChannelsSelected$UserChannelSelection;", "", "Lcom/discord/api/channel/Channel;", "channel", "Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;", "resolveWithChannel", "(Lcom/discord/api/channel/Channel;)Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;", "", "Lcom/discord/primitives/ChannelId;", "getId", "()J", ModelAuditLogEntry.CHANGE_KEY_ID, HookHelper.constructorName, "()V", "SelectedChannel", "ThreadDraft", "Unselected", "Lcom/discord/stores/StoreChannelsSelected$UserChannelSelection$Unselected;", "Lcom/discord/stores/StoreChannelsSelected$UserChannelSelection$SelectedChannel;", "Lcom/discord/stores/StoreChannelsSelected$UserChannelSelection$ThreadDraft;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class UserChannelSelection {

        /* compiled from: StoreChannelsSelected.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B1\u0012\n\u0010\u000b\u001a\u00060\u0002j\u0002`\u0003\u0012\u0010\b\u0002\u0010\f\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\b¢\u0006\u0004\b!\u0010\"J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0018\u0010\u0006\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ<\u0010\u000e\u001a\u00020\u00002\f\b\u0002\u0010\u000b\u001a\u00060\u0002j\u0002`\u00032\u0010\b\u0002\u0010\f\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0019\u001a\u00020\u00182\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016HÖ\u0003¢\u0006\u0004\b\u0019\u0010\u001aR\u001d\u0010\u000b\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001b\u001a\u0004\b\u001c\u0010\u0005R!\u0010\f\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001d\u001a\u0004\b\u001e\u0010\u0007R\u001b\u0010\r\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001f\u001a\u0004\b \u0010\n¨\u0006#"}, d2 = {"Lcom/discord/stores/StoreChannelsSelected$UserChannelSelection$SelectedChannel;", "Lcom/discord/stores/StoreChannelsSelected$UserChannelSelection;", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "component2", "()Ljava/lang/Long;", "Lcom/discord/stores/SelectedChannelAnalyticsLocation;", "component3", "()Lcom/discord/stores/SelectedChannelAnalyticsLocation;", "channelId", "peekParent", "analyticsLocation", "copy", "(JLjava/lang/Long;Lcom/discord/stores/SelectedChannelAnalyticsLocation;)Lcom/discord/stores/StoreChannelsSelected$UserChannelSelection$SelectedChannel;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getChannelId", "Ljava/lang/Long;", "getPeekParent", "Lcom/discord/stores/SelectedChannelAnalyticsLocation;", "getAnalyticsLocation", HookHelper.constructorName, "(JLjava/lang/Long;Lcom/discord/stores/SelectedChannelAnalyticsLocation;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class SelectedChannel extends UserChannelSelection {
            private final SelectedChannelAnalyticsLocation analyticsLocation;
            private final long channelId;
            private final Long peekParent;

            public /* synthetic */ SelectedChannel(long j, Long l, SelectedChannelAnalyticsLocation selectedChannelAnalyticsLocation, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this(j, (i & 2) != 0 ? null : l, (i & 4) != 0 ? null : selectedChannelAnalyticsLocation);
            }

            public static /* synthetic */ SelectedChannel copy$default(SelectedChannel selectedChannel, long j, Long l, SelectedChannelAnalyticsLocation selectedChannelAnalyticsLocation, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = selectedChannel.channelId;
                }
                if ((i & 2) != 0) {
                    l = selectedChannel.peekParent;
                }
                if ((i & 4) != 0) {
                    selectedChannelAnalyticsLocation = selectedChannel.analyticsLocation;
                }
                return selectedChannel.copy(j, l, selectedChannelAnalyticsLocation);
            }

            public final long component1() {
                return this.channelId;
            }

            public final Long component2() {
                return this.peekParent;
            }

            public final SelectedChannelAnalyticsLocation component3() {
                return this.analyticsLocation;
            }

            public final SelectedChannel copy(long j, Long l, SelectedChannelAnalyticsLocation selectedChannelAnalyticsLocation) {
                return new SelectedChannel(j, l, selectedChannelAnalyticsLocation);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof SelectedChannel)) {
                    return false;
                }
                SelectedChannel selectedChannel = (SelectedChannel) obj;
                return this.channelId == selectedChannel.channelId && m.areEqual(this.peekParent, selectedChannel.peekParent) && m.areEqual(this.analyticsLocation, selectedChannel.analyticsLocation);
            }

            public final SelectedChannelAnalyticsLocation getAnalyticsLocation() {
                return this.analyticsLocation;
            }

            public final long getChannelId() {
                return this.channelId;
            }

            public final Long getPeekParent() {
                return this.peekParent;
            }

            public int hashCode() {
                int a = b.a(this.channelId) * 31;
                Long l = this.peekParent;
                int i = 0;
                int hashCode = (a + (l != null ? l.hashCode() : 0)) * 31;
                SelectedChannelAnalyticsLocation selectedChannelAnalyticsLocation = this.analyticsLocation;
                if (selectedChannelAnalyticsLocation != null) {
                    i = selectedChannelAnalyticsLocation.hashCode();
                }
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("SelectedChannel(channelId=");
                R.append(this.channelId);
                R.append(", peekParent=");
                R.append(this.peekParent);
                R.append(", analyticsLocation=");
                R.append(this.analyticsLocation);
                R.append(")");
                return R.toString();
            }

            public SelectedChannel(long j, Long l, SelectedChannelAnalyticsLocation selectedChannelAnalyticsLocation) {
                super(null);
                this.channelId = j;
                this.peekParent = l;
                this.analyticsLocation = selectedChannelAnalyticsLocation;
            }
        }

        /* compiled from: StoreChannelsSelected.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B-\u0012\n\u0010\f\u001a\u00060\u0002j\u0002`\u0003\u0012\u000e\u0010\r\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0006\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\t¢\u0006\u0004\b \u0010!J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0018\u0010\u0007\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ<\u0010\u000f\u001a\u00020\u00002\f\b\u0002\u0010\f\u001a\u00060\u0002j\u0002`\u00032\u0010\b\u0002\u0010\r\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00062\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\tHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\u0011\u0010\u000bJ\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0018\u001a\u00020\u00172\b\u0010\u0016\u001a\u0004\u0018\u00010\u0015HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R!\u0010\r\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001a\u001a\u0004\b\u001b\u0010\bR\u001d\u0010\f\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001c\u001a\u0004\b\u001d\u0010\u0005R\u001b\u0010\u000e\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001e\u001a\u0004\b\u001f\u0010\u000b¨\u0006\""}, d2 = {"Lcom/discord/stores/StoreChannelsSelected$UserChannelSelection$ThreadDraft;", "Lcom/discord/stores/StoreChannelsSelected$UserChannelSelection;", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "Lcom/discord/primitives/MessageId;", "component2", "()Ljava/lang/Long;", "", "component3", "()Ljava/lang/String;", "parentChannelId", "starterMessageId", "locationThreadStart", "copy", "(JLjava/lang/Long;Ljava/lang/String;)Lcom/discord/stores/StoreChannelsSelected$UserChannelSelection$ThreadDraft;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Long;", "getStarterMessageId", "J", "getParentChannelId", "Ljava/lang/String;", "getLocationThreadStart", HookHelper.constructorName, "(JLjava/lang/Long;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class ThreadDraft extends UserChannelSelection {
            private final String locationThreadStart;
            private final long parentChannelId;
            private final Long starterMessageId;

            public ThreadDraft(long j, Long l, String str) {
                super(null);
                this.parentChannelId = j;
                this.starterMessageId = l;
                this.locationThreadStart = str;
            }

            public static /* synthetic */ ThreadDraft copy$default(ThreadDraft threadDraft, long j, Long l, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = threadDraft.parentChannelId;
                }
                if ((i & 2) != 0) {
                    l = threadDraft.starterMessageId;
                }
                if ((i & 4) != 0) {
                    str = threadDraft.locationThreadStart;
                }
                return threadDraft.copy(j, l, str);
            }

            public final long component1() {
                return this.parentChannelId;
            }

            public final Long component2() {
                return this.starterMessageId;
            }

            public final String component3() {
                return this.locationThreadStart;
            }

            public final ThreadDraft copy(long j, Long l, String str) {
                return new ThreadDraft(j, l, str);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof ThreadDraft)) {
                    return false;
                }
                ThreadDraft threadDraft = (ThreadDraft) obj;
                return this.parentChannelId == threadDraft.parentChannelId && m.areEqual(this.starterMessageId, threadDraft.starterMessageId) && m.areEqual(this.locationThreadStart, threadDraft.locationThreadStart);
            }

            public final String getLocationThreadStart() {
                return this.locationThreadStart;
            }

            public final long getParentChannelId() {
                return this.parentChannelId;
            }

            public final Long getStarterMessageId() {
                return this.starterMessageId;
            }

            public int hashCode() {
                int a = b.a(this.parentChannelId) * 31;
                Long l = this.starterMessageId;
                int i = 0;
                int hashCode = (a + (l != null ? l.hashCode() : 0)) * 31;
                String str = this.locationThreadStart;
                if (str != null) {
                    i = str.hashCode();
                }
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("ThreadDraft(parentChannelId=");
                R.append(this.parentChannelId);
                R.append(", starterMessageId=");
                R.append(this.starterMessageId);
                R.append(", locationThreadStart=");
                return a.H(R, this.locationThreadStart, ")");
            }
        }

        /* compiled from: StoreChannelsSelected.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreChannelsSelected$UserChannelSelection$Unselected;", "Lcom/discord/stores/StoreChannelsSelected$UserChannelSelection;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Unselected extends UserChannelSelection {
            public static final Unselected INSTANCE = new Unselected();

            private Unselected() {
                super(null);
            }
        }

        private UserChannelSelection() {
        }

        public final long getId() {
            if (this instanceof SelectedChannel) {
                return ((SelectedChannel) this).getChannelId();
            }
            if (this instanceof ThreadDraft) {
                return ((ThreadDraft) this).getParentChannelId();
            }
            if (m.areEqual(this, Unselected.INSTANCE)) {
                return 0L;
            }
            throw new NoWhenBranchMatchedException();
        }

        public final ResolvedSelectedChannel resolveWithChannel(Channel channel) {
            ResolvedSelectedChannel threadDraft;
            m.checkNotNullParameter(channel, "channel");
            if (this instanceof Unselected) {
                return ResolvedSelectedChannel.Unselected.INSTANCE;
            }
            if (this instanceof SelectedChannel) {
                SelectedChannel selectedChannel = (SelectedChannel) this;
                threadDraft = new ResolvedSelectedChannel.Channel(channel, selectedChannel.getPeekParent(), selectedChannel.getAnalyticsLocation());
            } else if (this instanceof ThreadDraft) {
                ThreadDraft threadDraft2 = (ThreadDraft) this;
                threadDraft = new ResolvedSelectedChannel.ThreadDraft(channel, threadDraft2.getStarterMessageId(), threadDraft2.getLocationThreadStart());
            } else {
                throw new NoWhenBranchMatchedException();
            }
            return threadDraft;
        }

        public /* synthetic */ UserChannelSelection(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public StoreChannelsSelected(StoreStream storeStream, StoreChannels storeChannels, StorePermissions storePermissions, StoreGuildSelected storeGuildSelected, Dispatcher dispatcher, ObservationDeck observationDeck) {
        m.checkNotNullParameter(storeStream, "stream");
        m.checkNotNullParameter(storeChannels, "storeChannels");
        m.checkNotNullParameter(storePermissions, "storePermissions");
        m.checkNotNullParameter(storeGuildSelected, "storeGuildSelected");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.stream = storeStream;
        this.storeChannels = storeChannels;
        this.storePermissions = storePermissions;
        this.storeGuildSelected = storeGuildSelected;
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
        ResolvedSelectedChannel.Uninitialized uninitialized = ResolvedSelectedChannel.Uninitialized.INSTANCE;
        this.selectedChannel = uninitialized;
        this.previouslySelectedChannel = uninitialized;
        Persister<ChannelFrecencyTracker> persister = new Persister<>("CHANNEL_HISTORY_V3", new ChannelFrecencyTracker());
        this.frecencyCache = persister;
        this.frecency = persister.get();
    }

    private final Channel getFirstAvailableChannel(Map<Long, Channel> map, long j, Map<Long, Long> map2) {
        Collection<Channel> values = map.values();
        ArrayList arrayList = new ArrayList();
        for (Object obj : values) {
            Channel channel = (Channel) obj;
            if (channel.f() == j && ChannelUtils.s(channel) && PermissionUtils.hasAccess(channel, map2)) {
                arrayList.add(obj);
            }
        }
        return (Channel) u.firstOrNull((List<? extends Object>) u.sortedWith(arrayList, ChannelUtils.h(Channel.Companion)));
    }

    private final boolean isValidResolution(Channel channel, long j, Map<Long, Long> map) {
        if (channel.f() == j) {
            m.checkNotNullParameter(channel, "$this$isTextOrVoiceChannel");
            if ((ChannelUtils.B(channel) || ChannelUtils.E(channel)) && PermissionUtils.hasAccess(channel, map)) {
                return true;
            }
        }
        return false;
    }

    @StoreThread
    private final void loadFromCache() {
        Map<Long, UserChannelSelection> map = this.userChannelSelections;
        Map<Long, Long> map2 = this.selectedChannelIdsCache.get();
        LinkedHashMap linkedHashMap = new LinkedHashMap(g0.mapCapacity(map2.size()));
        Iterator<T> it = map2.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            linkedHashMap.put(entry.getKey(), new UserChannelSelection.SelectedChannel(((Number) entry.getValue()).longValue(), null, null, 6, null));
        }
        map.putAll(linkedHashMap);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void onSelectedChannelResolved(ResolvedSelectedChannel resolvedSelectedChannel) {
        if (this.selectedChannel.getId() != resolvedSelectedChannel.getId()) {
            this.previouslySelectedChannel = this.selectedChannel;
        }
        this.selectedChannel = resolvedSelectedChannel;
        boolean z2 = resolvedSelectedChannel instanceof ResolvedSelectedChannel.Channel;
        if (z2) {
            ResolvedSelectedChannel.Channel channel = (ResolvedSelectedChannel.Channel) resolvedSelectedChannel;
            this.userChannelSelections.put(Long.valueOf(channel.getChannel().f()), new UserChannelSelection.SelectedChannel(channel.getChannel().h(), channel.getPeekParent(), null, 4, null));
            FrecencyTracker.track$default(this.frecency, Long.valueOf(channel.getChannel().h()), 0L, 2, null);
        }
        if (z2) {
            ResolvedSelectedChannel.Channel channel2 = (ResolvedSelectedChannel.Channel) resolvedSelectedChannel;
            this.stream.getAnalytics$app_productionGoogleRelease().trackChannelOpened(resolvedSelectedChannel.getId(), channel2.getAnalyticsViewType(), channel2.getAnalyticsLocation());
        }
        updateInitializationState();
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final ResolvedSelectedChannel resolveSelectedChannel(UserChannelSelection userChannelSelection, Channel channel, Map<Long, Channel> map, long j, Map<Long, Long> map2, boolean z2) {
        ResolvedSelectedChannel resolveWithChannel;
        if (!z2) {
            return ResolvedSelectedChannel.Uninitialized.INSTANCE;
        }
        if (channel != null && isValidResolution(channel, j, map2)) {
            return (userChannelSelection == null || (resolveWithChannel = userChannelSelection.resolveWithChannel(channel)) == null) ? ResolvedSelectedChannel.Unselected.INSTANCE : resolveWithChannel;
        }
        if (j == 0) {
            return ResolvedSelectedChannel.Unselected.INSTANCE;
        }
        Channel firstAvailableChannel = getFirstAvailableChannel(map, j, map2);
        if (firstAvailableChannel == null) {
            return ResolvedSelectedChannel.Unavailable.INSTANCE;
        }
        return new ResolvedSelectedChannel.Channel(firstAvailableChannel, null, null);
    }

    @StoreThread
    private final void updateInitializationState() {
        boolean z2 = this.stream.getAuthentication$app_productionGoogleRelease().getAuthToken$app_productionGoogleRelease() != null;
        boolean z3 = this.initializedForAuthedUser;
        boolean z4 = z2 && this.storeChannels.getInitializedForAuthedUser$app_productionGoogleRelease() && ((this.userChannelSelections.isEmpty() ^ true) || this.handledReadyPayload || this.isStoreInitTimedOut) && (m.areEqual(this.selectedChannel, ResolvedSelectedChannel.Uninitialized.INSTANCE) ^ true);
        if (!z3 && z4) {
            this.initializedForAuthedUser = true;
            markChanged(InitializedUpdateSource);
        }
    }

    @StoreThread
    private final void validateSelectedChannel() {
        Subscription subscription = this.validateSelectedChannelSubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        Map<Long, Channel> channelsByIdInternal$app_productionGoogleRelease = this.storeChannels.getChannelsByIdInternal$app_productionGoogleRelease();
        boolean initializedForAuthedUser$app_productionGoogleRelease = this.storeChannels.getInitializedForAuthedUser$app_productionGoogleRelease();
        long selectedGuildIdInternal$app_productionGoogleRelease = this.storeGuildSelected.getSelectedGuildIdInternal$app_productionGoogleRelease();
        Observable q = ObservableExtensionsKt.computationLatest(ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this.storeGuildSelected, this.storeChannels, this.storePermissions}, false, null, null, new StoreChannelsSelected$validateSelectedChannel$1(this, this.userChannelSelections.get(Long.valueOf(selectedGuildIdInternal$app_productionGoogleRelease)), channelsByIdInternal$app_productionGoogleRelease, selectedGuildIdInternal$app_productionGoogleRelease, this.storePermissions.getPermissionsByChannelInternal$app_productionGoogleRelease(selectedGuildIdInternal$app_productionGoogleRelease), initializedForAuthedUser$app_productionGoogleRelease), 14, null)).q();
        m.checkNotNullExpressionValue(q, "observationDeck\n        …  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(q, StoreChannelsSelected.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new StoreChannelsSelected$validateSelectedChannel$2(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreChannelsSelected$validateSelectedChannel$3(this));
    }

    @StoreThread
    public final void dismissCreateThread() {
        ResolvedSelectedChannel resolvedSelectedChannel = this.selectedChannel;
        if (!(resolvedSelectedChannel instanceof ResolvedSelectedChannel.ThreadDraft)) {
            resolvedSelectedChannel = null;
        }
        ResolvedSelectedChannel.ThreadDraft threadDraft = (ResolvedSelectedChannel.ThreadDraft) resolvedSelectedChannel;
        if (threadDraft != null) {
            Channel parentChannel = threadDraft.getParentChannel();
            this.userChannelSelections.put(Long.valueOf(parentChannel.f()), new UserChannelSelection.SelectedChannel(parentChannel.h(), null, null, 6, null));
            validateSelectedChannel();
        }
    }

    public final ChannelFrecencyTracker getFrecency() {
        return this.frecency;
    }

    public final long getId() {
        return this.selectedChannel.getId();
    }

    public final Channel getSelectedChannel() {
        ResolvedSelectedChannel resolvedSelectedChannel = this.selectedChannel;
        if (!(resolvedSelectedChannel instanceof ResolvedSelectedChannel.Channel)) {
            resolvedSelectedChannel = null;
        }
        ResolvedSelectedChannel.Channel channel = (ResolvedSelectedChannel.Channel) resolvedSelectedChannel;
        if (channel != null) {
            return channel.getChannel();
        }
        return null;
    }

    @StoreThread
    public final void handleChannelOrThreadCreateOrUpdate(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        if (channel.h() == this.selectedChannel.getId()) {
            validateSelectedChannel();
        }
    }

    @StoreThread
    public final void handleChannelOrThreadDelete(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        if (channel.h() == this.selectedChannel.getId()) {
            validateSelectedChannel();
        }
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        List<Guild> guilds = modelPayload.getGuilds();
        m.checkNotNullExpressionValue(guilds, "payload.guilds");
        HashSet hashSetOf = n0.hashSetOf(0L);
        for (Guild guild : guilds) {
            hashSetOf.add(Long.valueOf(guild.r()));
        }
        Map<Long, UserChannelSelection> map = this.userChannelSelections;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry<Long, UserChannelSelection> entry : map.entrySet()) {
            if (hashSetOf.contains(Long.valueOf(entry.getKey().longValue()))) {
                linkedHashMap.put(entry.getKey(), entry.getValue());
            }
        }
        Map<? extends Long, ? extends UserChannelSelection> mutableMap = h0.toMutableMap(linkedHashMap);
        if (!mutableMap.containsKey(0L)) {
            mutableMap.put(0L, UserChannelSelection.Unselected.INSTANCE);
        }
        this.userChannelSelections.clear();
        this.userChannelSelections.putAll(mutableMap);
        this.handledReadyPayload = true;
        validateSelectedChannel();
    }

    @StoreThread
    public final void handleGuildAdd(Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        if (this.storeGuildSelected.getSelectedGuildIdInternal$app_productionGoogleRelease() == guild.r()) {
            validateSelectedChannel();
        }
    }

    @StoreThread
    public final void handleGuildRemove(Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        Channel maybeChannel = this.selectedChannel.getMaybeChannel();
        if (maybeChannel != null && maybeChannel.f() == guild.r()) {
            validateSelectedChannel();
        }
    }

    @StoreThread
    public final void handleGuildSelected() {
        validateSelectedChannel();
    }

    @StoreThread
    public final void handleStoreInitTimeout() {
        this.isStoreInitTimedOut = true;
        updateInitializationState();
        if (m.areEqual(this.selectedChannel, ResolvedSelectedChannel.Uninitialized.INSTANCE)) {
            validateSelectedChannel();
        }
    }

    @StoreThread
    public final void init() {
        loadFromCache();
        validateSelectedChannel();
    }

    public final Observable<Long> observeId() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreChannelsSelected$observeId$1(this), 14, null);
    }

    public final Observable<Boolean> observeInitializedForAuthedUser() {
        Observable Y = this.storeChannels.observeInitializedForAuthedUser().Y(new j0.k.b<Boolean, Observable<? extends Boolean>>() { // from class: com.discord.stores.StoreChannelsSelected$observeInitializedForAuthedUser$1

            /* compiled from: StoreChannelsSelected.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.stores.StoreChannelsSelected$observeInitializedForAuthedUser$1$1  reason: invalid class name */
            /* loaded from: classes.dex */
            public static final class AnonymousClass1 extends o implements Function0<Boolean> {
                public AnonymousClass1() {
                    super(0);
                }

                /* JADX WARN: Type inference failed for: r0v2, types: [boolean, java.lang.Boolean] */
                @Override // kotlin.jvm.functions.Function0
                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final Boolean invoke2() {
                    ?? r0;
                    r0 = StoreChannelsSelected.this.initializedForAuthedUser;
                    return r0;
                }
            }

            public final Observable<? extends Boolean> call(Boolean bool) {
                ObservationDeck observationDeck;
                StoreChannelsSelected$Companion$InitializedUpdateSource$1 storeChannelsSelected$Companion$InitializedUpdateSource$1;
                m.checkNotNullExpressionValue(bool, "isStoreChannelsInitialized");
                if (!bool.booleanValue()) {
                    return new k(Boolean.FALSE);
                }
                observationDeck = StoreChannelsSelected.this.observationDeck;
                storeChannelsSelected$Companion$InitializedUpdateSource$1 = StoreChannelsSelected.InitializedUpdateSource;
                return ObservationDeck.connectRx$default(observationDeck, new ObservationDeck.UpdateSource[]{storeChannelsSelected$Companion$InitializedUpdateSource$1}, false, null, null, new AnonymousClass1(), 14, null);
            }
        });
        m.checkNotNullExpressionValue(Y, "storeChannels.observeIni…            }\n          }");
        return Y;
    }

    public final Observable<Long> observePreviousId() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreChannelsSelected$observePreviousId$1(this), 14, null);
    }

    public final Observable<ResolvedSelectedChannel> observeResolvedSelectedChannel() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreChannelsSelected$observeResolvedSelectedChannel$1(this), 14, null);
    }

    public final Observable<Channel> observeSelectedChannel() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreChannelsSelected$observeSelectedChannel$1(this), 14, null);
    }

    @StoreThread
    public final void openCreateThread(long j, long j2, Long l, String str) {
        this.userChannelSelections.put(Long.valueOf(j), new UserChannelSelection.ThreadDraft(j2, l, str));
        validateSelectedChannel();
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        Persister.set$default(this.frecencyCache, this.frecency, false, 2, null);
        Map<Long, UserChannelSelection> map = this.userChannelSelections;
        LinkedHashMap linkedHashMap = new LinkedHashMap(g0.mapCapacity(map.size()));
        Iterator<T> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            linkedHashMap.put(entry.getKey(), Long.valueOf(((UserChannelSelection) entry.getValue()).getId()));
        }
        this.selectedChannelIdsCache.set(linkedHashMap, true);
    }

    @StoreThread
    public final void trySelectChannel(long j, long j2, Long l, SelectedChannelAnalyticsLocation selectedChannelAnalyticsLocation) {
        UserChannelSelection userChannelSelection = this.userChannelSelections.get(Long.valueOf(j));
        if (!(userChannelSelection instanceof UserChannelSelection.SelectedChannel)) {
            userChannelSelection = null;
        }
        UserChannelSelection.SelectedChannel selectedChannel = (UserChannelSelection.SelectedChannel) userChannelSelection;
        if (selectedChannel == null || selectedChannel.getChannelId() != j2) {
            this.userChannelSelections.put(Long.valueOf(j), new UserChannelSelection.SelectedChannel(j2, l, selectedChannelAnalyticsLocation));
            validateSelectedChannel();
        }
    }
}
