package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.BehaviorSubject;
/* compiled from: StoreThreadDraft.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001\u0016B\u000f\u0012\u0006\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b\u0014\u0010\u0015J\u0013\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0015\u0010\b\u001a\u00020\u00072\u0006\u0010\u0006\u001a\u00020\u0003¢\u0006\u0004\b\b\u0010\tJ\r\u0010\n\u001a\u00020\u0007¢\u0006\u0004\b\n\u0010\u000bJ\r\u0010\f\u001a\u00020\u0007¢\u0006\u0004\b\f\u0010\u000bR\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR:\u0010\u0012\u001a&\u0012\f\u0012\n \u0011*\u0004\u0018\u00010\u00030\u0003 \u0011*\u0012\u0012\f\u0012\n \u0011*\u0004\u0018\u00010\u00030\u0003\u0018\u00010\u00100\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013¨\u0006\u0017"}, d2 = {"Lcom/discord/stores/StoreThreadDraft;", "Lcom/discord/stores/StoreV2;", "Lrx/Observable;", "Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;", "observeDraftState", "()Lrx/Observable;", "draftState", "", "setDraftState", "(Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;)V", "setDraftSending", "()V", "clearDraftState", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lrx/subjects/BehaviorSubject;", "kotlin.jvm.PlatformType", "draftStateSubject", "Lrx/subjects/BehaviorSubject;", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;)V", "ThreadDraftState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreThreadDraft extends StoreV2 {
    private final Dispatcher dispatcher;
    private final BehaviorSubject<ThreadDraftState> draftStateSubject = BehaviorSubject.l0(new ThreadDraftState(false, null, null, false, false, 31, null));

    /* compiled from: StoreThreadDraft.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u001a\b\u0086\b\u0018\u00002\u00020\u0001B=\u0012\b\b\u0002\u0010\r\u001a\u00020\u0002\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\b\u0012\b\b\u0002\u0010\u0010\u001a\u00020\u0002\u0012\b\b\u0002\u0010\u0011\u001a\u00020\u0002¢\u0006\u0004\b \u0010!J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u000b\u0010\u0004J\u0010\u0010\f\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\f\u0010\u0004JF\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\r\u001a\u00020\u00022\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\b2\b\b\u0002\u0010\u0010\u001a\u00020\u00022\b\b\u0002\u0010\u0011\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u0014\u0010\nJ\u0010\u0010\u0015\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u001a\u0010\u0018\u001a\u00020\u00022\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u001b\u0010\u000f\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001a\u001a\u0004\b\u001b\u0010\nR\u0019\u0010\r\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001c\u001a\u0004\b\r\u0010\u0004R\u0019\u0010\u0011\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001c\u001a\u0004\b\u001d\u0010\u0004R\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001e\u001a\u0004\b\u001f\u0010\u0007R\u0019\u0010\u0010\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001c\u001a\u0004\b\u0010\u0010\u0004¨\u0006\""}, d2 = {"Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;", "", "", "component1", "()Z", "", "component2", "()Ljava/lang/Integer;", "", "component3", "()Ljava/lang/String;", "component4", "component5", "isPrivate", "autoArchiveDuration", "threadName", "isSending", "shouldDisplayNameError", "copy", "(ZLjava/lang/Integer;Ljava/lang/String;ZZ)Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;", "toString", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getThreadName", "Z", "getShouldDisplayNameError", "Ljava/lang/Integer;", "getAutoArchiveDuration", HookHelper.constructorName, "(ZLjava/lang/Integer;Ljava/lang/String;ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ThreadDraftState {
        private final Integer autoArchiveDuration;
        private final boolean isPrivate;
        private final boolean isSending;
        private final boolean shouldDisplayNameError;
        private final String threadName;

        public ThreadDraftState() {
            this(false, null, null, false, false, 31, null);
        }

        public ThreadDraftState(boolean z2, Integer num, String str, boolean z3, boolean z4) {
            this.isPrivate = z2;
            this.autoArchiveDuration = num;
            this.threadName = str;
            this.isSending = z3;
            this.shouldDisplayNameError = z4;
        }

        public static /* synthetic */ ThreadDraftState copy$default(ThreadDraftState threadDraftState, boolean z2, Integer num, String str, boolean z3, boolean z4, int i, Object obj) {
            if ((i & 1) != 0) {
                z2 = threadDraftState.isPrivate;
            }
            if ((i & 2) != 0) {
                num = threadDraftState.autoArchiveDuration;
            }
            Integer num2 = num;
            if ((i & 4) != 0) {
                str = threadDraftState.threadName;
            }
            String str2 = str;
            if ((i & 8) != 0) {
                z3 = threadDraftState.isSending;
            }
            boolean z5 = z3;
            if ((i & 16) != 0) {
                z4 = threadDraftState.shouldDisplayNameError;
            }
            return threadDraftState.copy(z2, num2, str2, z5, z4);
        }

        public final boolean component1() {
            return this.isPrivate;
        }

        public final Integer component2() {
            return this.autoArchiveDuration;
        }

        public final String component3() {
            return this.threadName;
        }

        public final boolean component4() {
            return this.isSending;
        }

        public final boolean component5() {
            return this.shouldDisplayNameError;
        }

        public final ThreadDraftState copy(boolean z2, Integer num, String str, boolean z3, boolean z4) {
            return new ThreadDraftState(z2, num, str, z3, z4);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ThreadDraftState)) {
                return false;
            }
            ThreadDraftState threadDraftState = (ThreadDraftState) obj;
            return this.isPrivate == threadDraftState.isPrivate && m.areEqual(this.autoArchiveDuration, threadDraftState.autoArchiveDuration) && m.areEqual(this.threadName, threadDraftState.threadName) && this.isSending == threadDraftState.isSending && this.shouldDisplayNameError == threadDraftState.shouldDisplayNameError;
        }

        public final Integer getAutoArchiveDuration() {
            return this.autoArchiveDuration;
        }

        public final boolean getShouldDisplayNameError() {
            return this.shouldDisplayNameError;
        }

        public final String getThreadName() {
            return this.threadName;
        }

        public int hashCode() {
            boolean z2 = this.isPrivate;
            int i = 1;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = i2 * 31;
            Integer num = this.autoArchiveDuration;
            int i5 = 0;
            int hashCode = (i4 + (num != null ? num.hashCode() : 0)) * 31;
            String str = this.threadName;
            if (str != null) {
                i5 = str.hashCode();
            }
            int i6 = (hashCode + i5) * 31;
            boolean z3 = this.isSending;
            if (z3) {
                z3 = true;
            }
            int i7 = z3 ? 1 : 0;
            int i8 = z3 ? 1 : 0;
            int i9 = (i6 + i7) * 31;
            boolean z4 = this.shouldDisplayNameError;
            if (!z4) {
                i = z4 ? 1 : 0;
            }
            return i9 + i;
        }

        public final boolean isPrivate() {
            return this.isPrivate;
        }

        public final boolean isSending() {
            return this.isSending;
        }

        public String toString() {
            StringBuilder R = a.R("ThreadDraftState(isPrivate=");
            R.append(this.isPrivate);
            R.append(", autoArchiveDuration=");
            R.append(this.autoArchiveDuration);
            R.append(", threadName=");
            R.append(this.threadName);
            R.append(", isSending=");
            R.append(this.isSending);
            R.append(", shouldDisplayNameError=");
            return a.M(R, this.shouldDisplayNameError, ")");
        }

        public /* synthetic */ ThreadDraftState(boolean z2, Integer num, String str, boolean z3, boolean z4, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? false : z2, (i & 2) != 0 ? null : num, (i & 4) == 0 ? str : null, (i & 8) != 0 ? false : z3, (i & 16) != 0 ? false : z4);
        }
    }

    public StoreThreadDraft(Dispatcher dispatcher) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        this.dispatcher = dispatcher;
    }

    public final void clearDraftState() {
        this.dispatcher.schedule(new StoreThreadDraft$clearDraftState$1(this));
    }

    public final Observable<ThreadDraftState> observeDraftState() {
        BehaviorSubject<ThreadDraftState> behaviorSubject = this.draftStateSubject;
        m.checkNotNullExpressionValue(behaviorSubject, "draftStateSubject");
        return behaviorSubject;
    }

    public final void setDraftSending() {
        this.dispatcher.schedule(new StoreThreadDraft$setDraftSending$1(this));
    }

    public final void setDraftState(ThreadDraftState threadDraftState) {
        m.checkNotNullParameter(threadDraftState, "draftState");
        this.dispatcher.schedule(new StoreThreadDraft$setDraftState$1(this, threadDraftState));
    }
}
