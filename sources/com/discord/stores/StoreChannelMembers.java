package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.permission.Permission;
import com.discord.api.role.GuildRole;
import com.discord.app.AppLog;
import com.discord.models.domain.ModelGuildMemberListUpdate;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.presence.Presence;
import com.discord.models.user.User;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.lazy.memberlist.ChannelMemberList;
import com.discord.utilities.lazy.memberlist.MemberListRow;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.user.UserUtils;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import d0.t.g0;
import d0.t.h0;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.ranges.IntRange;
import rx.Observable;
import rx.subjects.PublishSubject;
import xyz.discord.R;
/* compiled from: StoreChannelMembers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000â\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010%\n\u0002\b\b\u0018\u0000 a2\u00020\u0001:\u0004abcdB\u008b\u0001\u0012\u0006\u0010E\u001a\u00020D\u0012\u0006\u0010T\u001a\u00020S\u0012\u0006\u0010B\u001a\u00020A\u0012\u0006\u0010W\u001a\u00020V\u0012\u0018\u0010I\u001a\u0014\u0012\b\u0012\u00060\nj\u0002`2\u0012\u0006\u0012\u0004\u0018\u00010H0G\u0012\u0016\u0010L\u001a\u0012\u0012\b\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020K0G\u0012\u0018\u0010N\u001a\u0014\u0012\b\u0012\u00060\nj\u0002`(\u0012\u0006\u0012\u0004\u0018\u00010M0G\u0012\u0016\u0010O\u001a\u0012\u0012\b\u0012\u00060\nj\u0002`(\u0012\u0004\u0012\u00020*0G¢\u0006\u0004\b_\u0010`J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J/\u0010\r\u001a\u00020\u00022\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u00062\u0006\u0010\t\u001a\u00020\b2\n\u0010\f\u001a\u00060\nj\u0002`\u000bH\u0003¢\u0006\u0004\b\r\u0010\u000eJ/\u0010\u0011\u001a\u00020\u00022\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u00062\u0006\u0010\u0010\u001a\u00020\u000f2\n\u0010\f\u001a\u00060\nj\u0002`\u000bH\u0003¢\u0006\u0004\b\u0011\u0010\u0012J/\u0010\u0015\u001a\u00020\u00022\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u00062\u0006\u0010\u0014\u001a\u00020\u00132\n\u0010\f\u001a\u00060\nj\u0002`\u000bH\u0002¢\u0006\u0004\b\u0015\u0010\u0016J/\u0010\u0019\u001a\u00020\u00022\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u00062\u0006\u0010\u0018\u001a\u00020\u00172\n\u0010\f\u001a\u00060\nj\u0002`\u000bH\u0003¢\u0006\u0004\b\u0019\u0010\u001aJ/\u0010\u001d\u001a\u00020\u00022\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u00062\u0006\u0010\u001c\u001a\u00020\u001b2\n\u0010\f\u001a\u00060\nj\u0002`\u000bH\u0003¢\u0006\u0004\b\u001d\u0010\u001eJ%\u0010\"\u001a\u0004\u0018\u00010!2\n\u0010\f\u001a\u00060\nj\u0002`\u000b2\u0006\u0010 \u001a\u00020\u001fH\u0003¢\u0006\u0004\b\"\u0010#J#\u0010&\u001a\u00020!2\n\u0010\f\u001a\u00060\nj\u0002`\u000b2\u0006\u0010%\u001a\u00020$H\u0003¢\u0006\u0004\b&\u0010'J1\u0010,\u001a\u0004\u0018\u00010!2\n\u0010\f\u001a\u00060\nj\u0002`\u000b2\n\u0010)\u001a\u00060\nj\u0002`(2\u0006\u0010+\u001a\u00020*H\u0003¢\u0006\u0004\b,\u0010-J)\u0010/\u001a\u0004\u0018\u00010.2\n\u0010\f\u001a\u00060\nj\u0002`\u000b2\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006H\u0003¢\u0006\u0004\b/\u00100J\u001b\u0010+\u001a\u00020*2\n\u0010\f\u001a\u00060\nj\u0002`\u000bH\u0003¢\u0006\u0004\b+\u00101J%\u00104\u001a\u00020.2\n\u0010\f\u001a\u00060\nj\u0002`\u000b2\n\u00103\u001a\u00060\nj\u0002`2¢\u0006\u0004\b4\u00105J+\u00107\u001a\b\u0012\u0004\u0012\u00020.062\n\u0010\f\u001a\u00060\nj\u0002`\u000b2\n\u00103\u001a\u00060\nj\u0002`2¢\u0006\u0004\b7\u00108J\u0017\u0010;\u001a\u00020\u00022\u0006\u0010:\u001a\u000209H\u0007¢\u0006\u0004\b;\u0010<J\u000f\u0010=\u001a\u00020\u0002H\u0016¢\u0006\u0004\b=\u0010\u0004J\u001b\u0010>\u001a\u00020\u00022\n\u0010\f\u001a\u00060\nj\u0002`\u000bH\u0007¢\u0006\u0004\b>\u0010?J\u001b\u0010@\u001a\u00020\u00022\n\u0010\f\u001a\u00060\nj\u0002`\u000bH\u0007¢\u0006\u0004\b@\u0010?R\u0016\u0010B\u001a\u00020A8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bB\u0010CR\u0016\u0010E\u001a\u00020D8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bE\u0010FR(\u0010I\u001a\u0014\u0012\b\u0012\u00060\nj\u0002`2\u0012\u0006\u0012\u0004\u0018\u00010H0G8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bI\u0010JR&\u0010L\u001a\u0012\u0012\b\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020K0G8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bL\u0010JR(\u0010N\u001a\u0014\u0012\b\u0012\u00060\nj\u0002`(\u0012\u0006\u0012\u0004\u0018\u00010M0G8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bN\u0010JR&\u0010O\u001a\u0012\u0012\b\u0012\u00060\nj\u0002`(\u0012\u0004\u0012\u00020*0G8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bO\u0010JR6\u0010Q\u001a\"\u0012\b\u0012\u00060\nj\u0002`\u000b\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020.0P0P8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bQ\u0010RR\u0016\u0010T\u001a\u00020S8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bT\u0010UR\u0016\u0010W\u001a\u00020V8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bW\u0010XR:\u0010[\u001a&\u0012\f\u0012\n Z*\u0004\u0018\u00010\u00020\u0002 Z*\u0012\u0012\f\u0012\n Z*\u0004\u0018\u00010\u00020\u0002\u0018\u00010Y0Y8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b[\u0010\\R6\u0010^\u001a\"\u0012\b\u0012\u00060\nj\u0002`\u000b\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020.0]0]8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b^\u0010R¨\u0006e"}, d2 = {"Lcom/discord/stores/StoreChannelMembers;", "Lcom/discord/stores/StoreV2;", "", "throttleMarkChanged", "()V", "", "Lcom/discord/primitives/MemberListId;", "memberListId", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Sync;", "syncOperation", "", "Lcom/discord/primitives/GuildId;", "guildId", "handleSync", "(Ljava/lang/String;Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Sync;J)V", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Insert;", "insertOperation", "handleInsert", "(Ljava/lang/String;Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Insert;J)V", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Update;", "updateOperation", "handleUpdate", "(Ljava/lang/String;Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Update;J)V", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Delete;", "deleteOperation", "handleDelete", "(Ljava/lang/String;Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Delete;J)V", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Invalidate;", "operation", "handleInvalidate", "(Ljava/lang/String;Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Invalidate;J)V", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;", "item", "Lcom/discord/utilities/lazy/memberlist/MemberListRow;", "makeRow", "(JLcom/discord/models/domain/ModelGuildMemberListUpdate$Operation$Item;)Lcom/discord/utilities/lazy/memberlist/MemberListRow;", "Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;", "group", "makeGroup", "(JLcom/discord/models/domain/ModelGuildMemberListUpdate$Group;)Lcom/discord/utilities/lazy/memberlist/MemberListRow;", "Lcom/discord/primitives/UserId;", "userId", "", "allowOwnerIndicator", "makeRowMember", "(JJZ)Lcom/discord/utilities/lazy/memberlist/MemberListRow;", "Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;", "getMemberList", "(JLjava/lang/String;)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;", "(J)Z", "Lcom/discord/primitives/ChannelId;", "channelId", "getChannelMemberList", "(JJ)Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;", "Lrx/Observable;", "observeChannelMemberList", "(JJ)Lrx/Observable;", "Lcom/discord/models/domain/ModelGuildMemberListUpdate;", "update", "handleGuildMemberListUpdate", "(Lcom/discord/models/domain/ModelGuildMemberListUpdate;)V", "snapshotData", "handleGuildRemove", "(J)V", "handleGuildRoleCreateOrUpdate", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreGuilds;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lkotlin/Function1;", "Lcom/discord/api/channel/Channel;", "getChannel", "Lkotlin/jvm/functions/Function1;", "", "getGuildMemberCount", "Lcom/discord/models/presence/Presence;", "getPresence", "isUserStreaming", "", "memberListsSnapshot", "Ljava/util/Map;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreUser;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "markChangedTrigger", "Lrx/subjects/PublishSubject;", "", "memberLists", HookHelper.constructorName, "(Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUser;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "Companion", "MemberListIdCalculator", "MemberListUpdateException", "MemberListUpdateLogger", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreChannelMembers extends StoreV2 {
    public static final Companion Companion = new Companion(null);
    private final Dispatcher dispatcher;
    private final Function1<Long, Channel> getChannel;
    private final Function1<Long, Integer> getGuildMemberCount;
    private final Function1<Long, Presence> getPresence;
    private final Function1<Long, Boolean> isUserStreaming;
    private final PublishSubject<Unit> markChangedTrigger;
    private final Map<Long, Map<String, ChannelMemberList>> memberLists = new LinkedHashMap();
    private Map<Long, ? extends Map<String, ChannelMemberList>> memberListsSnapshot = h0.emptyMap();
    private final ObservationDeck observationDeck;
    private final StoreGuilds storeGuilds;
    private final StoreUser storeUser;

    /* compiled from: StoreChannelMembers.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0005\u0010\u0005\u001a\u00020\u00002\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "kotlin.jvm.PlatformType", "it", "invoke", "(Lkotlin/Unit;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreChannelMembers$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<Unit, Unit> {

        /* compiled from: StoreChannelMembers.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreChannelMembers$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C01991 extends o implements Function0<Unit> {
            public C01991() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                StoreChannelMembers.this.markChanged();
            }
        }

        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Unit unit) {
            invoke2(unit);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Unit unit) {
            StoreChannelMembers.this.dispatcher.schedule(new C01991());
        }
    }

    /* compiled from: StoreChannelMembers.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0012\u0010\u0013Jg\u0010\u0010\u001a\u0004\u0018\u00010\u000f2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0018\u0010\u0007\u001a\u0014\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00052\u0016\u0010\t\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\b0\u00052\b\u0010\u000b\u001a\u0004\u0018\u00010\n2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\f¢\u0006\u0004\b\u0010\u0010\u0011¨\u0006\u0014"}, d2 = {"Lcom/discord/stores/StoreChannelMembers$Companion;", "", "", "Lcom/discord/primitives/UserId;", "userId", "", "Lcom/discord/models/member/GuildMember;", "guildMembers", "Lcom/discord/models/user/User;", "users", "Lcom/discord/models/presence/Presence;", "presence", "", "isUserStreaming", "showOwnerIndicator", "Lcom/discord/utilities/lazy/memberlist/MemberListRow$Member;", "makeRowMember", "(JLjava/util/Map;Ljava/util/Map;Lcom/discord/models/presence/Presence;ZZ)Lcom/discord/utilities/lazy/memberlist/MemberListRow$Member;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public final MemberListRow.Member makeRowMember(long j, Map<Long, GuildMember> map, Map<Long, ? extends User> map2, Presence presence, boolean z2, boolean z3) {
            m.checkNotNullParameter(map2, "users");
            Integer num = null;
            GuildMember guildMember = map != null ? map.get(Long.valueOf(j)) : null;
            User user = map2.get(Long.valueOf(j));
            if (user == null || guildMember == null) {
                return null;
            }
            if (guildMember.getColor() != -16777216) {
                num = Integer.valueOf(guildMember.getColor());
            }
            return new MemberListRow.Member(j, GuildMember.Companion.getNickOrUsername(guildMember, user), user.isBot(), Integer.valueOf(user.isSystemUser() ? R.string.system_dm_tag_system : R.string.bot_tag_bot), UserUtils.INSTANCE.isVerifiedBot(user), presence, num, IconUtils.getForGuildMemberOrUser$default(IconUtils.INSTANCE, user, guildMember, null, false, 12, null), z3, guildMember.getPremiumSince(), z2, user.getFlags() | user.getPublicFlags());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreChannelMembers.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bÂ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0013\u0010\u0004\u001a\u00020\u0003*\u00020\u0002H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u001f\u0010\n\u001a\u00020\t2\u000e\u0010\b\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006H\u0002¢\u0006\u0004\b\n\u0010\u000bJ;\u0010\u0013\u001a\u00020\t2\u0018\u0010\u000f\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002`\r\u0012\u0006\u0012\u0004\u0018\u00010\u000e0\f2\n\u0010\u0010\u001a\u00060\u0003j\u0002`\r2\u0006\u0010\u0012\u001a\u00020\u0011¢\u0006\u0004\b\u0013\u0010\u0014¨\u0006\u0017"}, d2 = {"Lcom/discord/stores/StoreChannelMembers$MemberListIdCalculator;", "", "", "", "toUnsignedLong", "(I)J", "", "Lcom/discord/api/permission/PermissionOverwrite;", "permissionOverwrites", "", "computeIdFromOverwrites", "(Ljava/util/List;)Ljava/lang/String;", "Lkotlin/Function1;", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "channelsProvider", "channelId", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "computeMemberListId", "(Lkotlin/jvm/functions/Function1;JLcom/discord/stores/StoreGuilds;)Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class MemberListIdCalculator {
        public static final MemberListIdCalculator INSTANCE = new MemberListIdCalculator();

        private MemberListIdCalculator() {
        }

        /* JADX WARN: Removed duplicated region for block: B:19:0x007d A[ORIG_RETURN, RETURN] */
        /* JADX WARN: Removed duplicated region for block: B:28:? A[RETURN, SYNTHETIC] */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        private final java.lang.String computeIdFromOverwrites(java.util.List<com.discord.api.permission.PermissionOverwrite> r12) {
            /*
                r11 = this;
                if (r12 == 0) goto L79
                java.util.ArrayList r0 = new java.util.ArrayList
                r0.<init>()
                java.util.Iterator r12 = r12.iterator()
            Lb:
                boolean r1 = r12.hasNext()
                if (r1 == 0) goto L4f
                java.lang.Object r1 = r12.next()
                com.discord.api.permission.PermissionOverwrite r1 = (com.discord.api.permission.PermissionOverwrite) r1
                r2 = 1024(0x400, double:5.06E-321)
                boolean r4 = com.discord.utilities.PermissionOverwriteUtilsKt.allows(r1, r2)
                if (r4 == 0) goto L34
                java.lang.String r2 = "allow:"
                java.lang.StringBuilder r2 = b.d.b.a.a.R(r2)
                long r3 = r1.e()
                r2.append(r3)
                java.lang.String r1 = r2.toString()
                r0.add(r1)
                goto Lb
            L34:
                boolean r2 = com.discord.utilities.PermissionOverwriteUtilsKt.denies(r1, r2)
                if (r2 == 0) goto Lb
                java.lang.String r2 = "deny:"
                java.lang.StringBuilder r2 = b.d.b.a.a.R(r2)
                long r3 = r1.e()
                r2.append(r3)
                java.lang.String r1 = r2.toString()
                r0.add(r1)
                goto Lb
            L4f:
                java.util.List r2 = d0.t.u.sorted(r0)
                if (r2 == 0) goto L79
                r4 = 0
                r5 = 0
                r6 = 0
                r7 = 0
                r8 = 0
                r9 = 62
                r10 = 0
                java.lang.String r3 = ","
                java.lang.String r12 = d0.t.u.joinToString$default(r2, r3, r4, r5, r6, r7, r8, r9, r10)
                if (r12 == 0) goto L79
                com.discord.stores.StoreChannelMembers$MemberListIdCalculator r0 = com.discord.stores.StoreChannelMembers.MemberListIdCalculator.INSTANCE
                int r1 = r12.length()
                r2 = 0
                int r12 = j0.l.e.m.a(r12, r2, r1, r2)
                long r0 = r0.toUnsignedLong(r12)
                java.lang.String r12 = java.lang.String.valueOf(r0)
                goto L7a
            L79:
                r12 = 0
            L7a:
                if (r12 == 0) goto L7d
                goto L7f
            L7d:
                java.lang.String r12 = ""
            L7f:
                return r12
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreChannelMembers.MemberListIdCalculator.computeIdFromOverwrites(java.util.List):java.lang.String");
        }

        private final long toUnsignedLong(int i) {
            return i & 4294967295L;
        }

        public final String computeMemberListId(Function1<? super Long, Channel> function1, long j, StoreGuilds storeGuilds) {
            m.checkNotNullParameter(function1, "channelsProvider");
            m.checkNotNullParameter(storeGuilds, "storeGuilds");
            Channel invoke = function1.invoke(Long.valueOf(j));
            String k = invoke != null ? invoke.k() : null;
            if (invoke != null) {
                if (k != null) {
                    return k;
                }
                Map map = (Map) a.u0(invoke, storeGuilds.getRoles());
                if (map == null) {
                    map = h0.emptyMap();
                }
                if (!PermissionUtils.canEveryone(Permission.VIEW_CHANNEL, invoke, null, map)) {
                    return computeIdFromOverwrites(invoke.s());
                }
            }
            return ModelGuildMemberListUpdate.EVERYONE_ID;
        }
    }

    /* compiled from: StoreChannelMembers.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0002\u0018\u00002\u00060\u0001j\u0002`\u0002B\u0013\u0012\n\u0010\u0005\u001a\u00060\u0003j\u0002`\u0004¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\b"}, d2 = {"Lcom/discord/stores/StoreChannelMembers$MemberListUpdateException;", "Ljava/lang/RuntimeException;", "Lkotlin/RuntimeException;", "Ljava/lang/Exception;", "Lkotlin/Exception;", "e", HookHelper.constructorName, "(Ljava/lang/Exception;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class MemberListUpdateException extends RuntimeException {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public MemberListUpdateException(Exception exc) {
            super(exc);
            m.checkNotNullParameter(exc, "e");
        }
    }

    /* compiled from: StoreChannelMembers.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010%\n\u0002\u0010!\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\bÂ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001d\u0010\u001eJ'\u0010\b\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0015\u0010\r\u001a\u00020\f2\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\r\u0010\u000eJ1\u0010\u0012\u001a\u00020\f2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u00062\n\u0010\u0011\u001a\u00060\u000fj\u0002`\u0010¢\u0006\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0014\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R(\u0010\u0018\u001a\u0014\u0012\u0004\u0012\u00020\u0005\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00170\u00168\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R\u0016\u0010\u001b\u001a\u00020\u001a8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001b\u0010\u001c¨\u0006\u001f"}, d2 = {"Lcom/discord/stores/StoreChannelMembers$MemberListUpdateLogger;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "Lcom/discord/primitives/MemberListId;", "memberListId", "makeLogKey", "(JLjava/lang/String;)Ljava/lang/String;", "Lcom/discord/models/domain/ModelGuildMemberListUpdate;", "update", "", "logUpdate", "(Lcom/discord/models/domain/ModelGuildMemberListUpdate;)V", "Ljava/lang/Exception;", "Lkotlin/Exception;", "exception", "dumpLogs", "(JLjava/lang/String;Ljava/lang/Exception;)V", "ERROR_TAG", "Ljava/lang/String;", "", "", "opLogs", "Ljava/util/Map;", "", "MAX_UPDATE_COUNT", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class MemberListUpdateLogger {
        private static final String ERROR_TAG = "MemberListUpdateError";
        private static final int MAX_UPDATE_COUNT = 20;
        public static final MemberListUpdateLogger INSTANCE = new MemberListUpdateLogger();
        private static final Map<String, List<String>> opLogs = new HashMap();

        private MemberListUpdateLogger() {
        }

        private final String makeLogKey(long j, String str) {
            return j + MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR + str;
        }

        public final void dumpLogs(long j, String str, Exception exc) {
            m.checkNotNullParameter(str, "memberListId");
            m.checkNotNullParameter(exc, "exception");
            List<String> list = opLogs.get(makeLogKey(j, str));
            StringBuilder sb = new StringBuilder();
            sb.append("guildId: ");
            sb.append(j);
            sb.append(" -- memberListId: ");
            sb.append(str);
            sb.append(" -- LAST 20 UPDATES:\n");
            sb.append(list != null ? u.joinToString$default(list, "\n", null, null, 0, null, null, 62, null) : null);
            FirebaseCrashlytics.getInstance().log(sb.toString());
            Logger.e$default(AppLog.g, ERROR_TAG, new MemberListUpdateException(exc), null, 4, null);
        }

        public final void logUpdate(ModelGuildMemberListUpdate modelGuildMemberListUpdate) {
            String str;
            m.checkNotNullParameter(modelGuildMemberListUpdate, "update");
            List<ModelGuildMemberListUpdate.Operation> operations = modelGuildMemberListUpdate.getOperations();
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(operations, 10));
            for (ModelGuildMemberListUpdate.Operation operation : operations) {
                if (operation instanceof ModelGuildMemberListUpdate.Operation.Sync) {
                    StringBuilder R = a.R("  SYNC: ");
                    R.append(((ModelGuildMemberListUpdate.Operation.Sync) operation).getRange());
                    str = R.toString();
                } else if (operation instanceof ModelGuildMemberListUpdate.Operation.Update) {
                    StringBuilder R2 = a.R("  UPDATE: ");
                    R2.append(((ModelGuildMemberListUpdate.Operation.Update) operation).getIndex());
                    str = R2.toString();
                } else if (operation instanceof ModelGuildMemberListUpdate.Operation.Insert) {
                    StringBuilder R3 = a.R("  INSERT: ");
                    R3.append(((ModelGuildMemberListUpdate.Operation.Insert) operation).getIndex());
                    str = R3.toString();
                } else if (operation instanceof ModelGuildMemberListUpdate.Operation.Delete) {
                    StringBuilder R4 = a.R("  DELETE: ");
                    R4.append(((ModelGuildMemberListUpdate.Operation.Delete) operation).getIndex());
                    str = R4.toString();
                } else if (operation instanceof ModelGuildMemberListUpdate.Operation.Invalidate) {
                    StringBuilder R5 = a.R("  INVALIDATE: ");
                    R5.append(((ModelGuildMemberListUpdate.Operation.Invalidate) operation).getRange());
                    str = R5.toString();
                } else {
                    throw new NoWhenBranchMatchedException();
                }
                arrayList.add(str);
            }
            String H = a.H(a.R("GROUPS: ["), u.joinToString$default(modelGuildMemberListUpdate.getGroups(), ",", null, null, 0, null, StoreChannelMembers$MemberListUpdateLogger$logUpdate$groupLog$1.INSTANCE, 30, null), "]");
            ArrayList arrayList2 = new ArrayList();
            arrayList2.addAll(arrayList);
            arrayList2.add(H);
            arrayList2.add("-----");
            String makeLogKey = makeLogKey(modelGuildMemberListUpdate.getGuildId(), modelGuildMemberListUpdate.getId());
            Map<String, List<String>> map = opLogs;
            List<String> list = map.get(makeLogKey);
            if (list == null) {
                list = new ArrayList<>();
            }
            list.add(u.joinToString$default(arrayList2, "\n", null, null, 0, null, null, 62, null));
            if (list.size() > 20) {
                list.remove(0);
            }
            map.put(makeLogKey, list);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            ModelGuildMemberListUpdate.Group.Type.values();
            int[] iArr = new int[3];
            $EnumSwitchMapping$0 = iArr;
            iArr[ModelGuildMemberListUpdate.Group.Type.ROLE.ordinal()] = 1;
            iArr[ModelGuildMemberListUpdate.Group.Type.OFFLINE.ordinal()] = 2;
            iArr[ModelGuildMemberListUpdate.Group.Type.ONLINE.ordinal()] = 3;
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public StoreChannelMembers(ObservationDeck observationDeck, Dispatcher dispatcher, StoreGuilds storeGuilds, StoreUser storeUser, Function1<? super Long, Channel> function1, Function1<? super Long, Integer> function12, Function1<? super Long, Presence> function13, Function1<? super Long, Boolean> function14) {
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        m.checkNotNullParameter(storeUser, "storeUser");
        m.checkNotNullParameter(function1, "getChannel");
        m.checkNotNullParameter(function12, "getGuildMemberCount");
        m.checkNotNullParameter(function13, "getPresence");
        m.checkNotNullParameter(function14, "isUserStreaming");
        this.observationDeck = observationDeck;
        this.dispatcher = dispatcher;
        this.storeGuilds = storeGuilds;
        this.storeUser = storeUser;
        this.getChannel = function1;
        this.getGuildMemberCount = function12;
        this.getPresence = function13;
        this.isUserStreaming = function14;
        PublishSubject<Unit> k0 = PublishSubject.k0();
        this.markChangedTrigger = k0;
        m.checkNotNullExpressionValue(k0, "markChangedTrigger");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.leadingEdgeThrottle(k0, 1L, TimeUnit.SECONDS), StoreChannelMembers.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }

    @StoreThread
    private final boolean allowOwnerIndicator(long j) {
        Collection<GuildRole> values;
        boolean z2;
        Map<Long, GuildRole> map = this.storeGuilds.getGuildRolesInternal$app_productionGoogleRelease().get(Long.valueOf(j));
        boolean z3 = false;
        if (map != null && (values = map.values()) != null && !values.isEmpty()) {
            Iterator<T> it = values.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                GuildRole guildRole = (GuildRole) it.next();
                if (!guildRole.c() || !PermissionUtils.can(8L, Long.valueOf(guildRole.h()))) {
                    z2 = false;
                    continue;
                } else {
                    z2 = true;
                    continue;
                }
                if (z2) {
                    z3 = true;
                    break;
                }
            }
        }
        return !z3;
    }

    @StoreThread
    private final ChannelMemberList getMemberList(long j, String str) {
        Map<String, ChannelMemberList> map = this.memberLists.get(Long.valueOf(j));
        if (map != null) {
            return map.get(str);
        }
        return null;
    }

    @StoreThread
    private final void handleDelete(String str, ModelGuildMemberListUpdate.Operation.Delete delete, long j) {
        ChannelMemberList memberList = getMemberList(j, str);
        if (memberList != null) {
            memberList.delete(delete.getIndex());
        }
    }

    @StoreThread
    private final void handleInsert(String str, ModelGuildMemberListUpdate.Operation.Insert insert, long j) {
        int index = insert.getIndex();
        ChannelMemberList memberList = getMemberList(j, str);
        if (memberList != null) {
            memberList.insert(index, makeRow(j, insert.getItem()));
        }
    }

    @StoreThread
    private final void handleInvalidate(String str, ModelGuildMemberListUpdate.Operation.Invalidate invalidate, long j) {
        IntRange range = invalidate.getRange();
        ChannelMemberList memberList = getMemberList(j, str);
        if (memberList != null) {
            memberList.invalidate(range);
        }
    }

    @StoreThread
    private final void handleSync(String str, ModelGuildMemberListUpdate.Operation.Sync sync, long j) {
        MemberListRow memberListRow;
        Map<String, ChannelMemberList> map = this.memberLists.get(Long.valueOf(j));
        if (map == null) {
            map = new HashMap<>();
            this.memberLists.put(Long.valueOf(j), map);
        }
        ChannelMemberList channelMemberList = map.get(str);
        if (channelMemberList == null) {
            AppLog appLog = AppLog.g;
            channelMemberList = new ChannelMemberList(str, 0, appLog, 2, null);
            map.put(str, channelMemberList);
            appLog.recordBreadcrumb(channelMemberList.getListId() + " INSTANTIATE", "ChannelMemberList");
        }
        boolean allowOwnerIndicator = allowOwnerIndicator(j);
        List<ModelGuildMemberListUpdate.Operation.Item> items = sync.getItems();
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(items, 10));
        for (ModelGuildMemberListUpdate.Operation.Item item : items) {
            if (item instanceof ModelGuildMemberListUpdate.Operation.Item.MemberItem) {
                memberListRow = makeRowMember(j, ((ModelGuildMemberListUpdate.Operation.Item.MemberItem) item).getMember().m().i(), allowOwnerIndicator);
            } else if (item instanceof ModelGuildMemberListUpdate.Operation.Item.GroupItem) {
                memberListRow = makeGroup(j, ((ModelGuildMemberListUpdate.Operation.Item.GroupItem) item).getGroup());
            } else {
                throw new NoWhenBranchMatchedException();
            }
            arrayList.add(memberListRow);
        }
        channelMemberList.sync(((Number) u.first(sync.getRange())).intValue(), arrayList);
    }

    private final void handleUpdate(String str, ModelGuildMemberListUpdate.Operation.Update update, long j) {
        int index = update.getIndex();
        ChannelMemberList memberList = getMemberList(j, str);
        if (memberList != null) {
            memberList.update(index, makeRow(j, update.getItem()));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final MemberListRow makeGroup(long j, ModelGuildMemberListUpdate.Group group) {
        String str;
        GuildRole guildRole;
        String id2 = group.getId();
        int ordinal = group.getType().ordinal();
        if (ordinal == 0) {
            long parseLong = Long.parseLong(id2);
            Map<Long, GuildRole> map = this.storeGuilds.getGuildRolesInternal$app_productionGoogleRelease().get(Long.valueOf(j));
            if (map == null || (guildRole = map.get(Long.valueOf(parseLong))) == null || (str = guildRole.g()) == null) {
                str = "";
            }
            return new MemberListRow.RoleHeader(parseLong, str, group.getCount());
        } else if (ordinal == 1) {
            return new MemberListRow.StatusHeader(id2, MemberListRow.StatusHeader.Type.OFFLINE, group.getCount());
        } else {
            if (ordinal == 2) {
                return new MemberListRow.StatusHeader(id2, MemberListRow.StatusHeader.Type.ONLINE, group.getCount());
            }
            throw new NoWhenBranchMatchedException();
        }
    }

    @StoreThread
    private final MemberListRow makeRow(long j, ModelGuildMemberListUpdate.Operation.Item item) {
        if (item instanceof ModelGuildMemberListUpdate.Operation.Item.MemberItem) {
            return makeRowMember(j, ((ModelGuildMemberListUpdate.Operation.Item.MemberItem) item).getMember().m().i(), allowOwnerIndicator(j));
        }
        if (item instanceof ModelGuildMemberListUpdate.Operation.Item.GroupItem) {
            return makeGroup(j, ((ModelGuildMemberListUpdate.Operation.Item.GroupItem) item).getGroup());
        }
        throw new NoWhenBranchMatchedException();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final MemberListRow makeRowMember(long j, long j2, boolean z2) {
        Guild guild;
        return Companion.makeRowMember(j2, this.storeGuilds.getGuildMembersComputedInternal$app_productionGoogleRelease().get(Long.valueOf(j)), this.storeUser.getUsersInternal$app_productionGoogleRelease(), this.getPresence.invoke(Long.valueOf(j2)), this.isUserStreaming.invoke(Long.valueOf(j2)).booleanValue(), z2 && (guild = this.storeGuilds.getGuildsInternal$app_productionGoogleRelease().get(Long.valueOf(j))) != null && guild.getOwnerId() == j2);
    }

    private final void throttleMarkChanged() {
        PublishSubject<Unit> publishSubject = this.markChangedTrigger;
        publishSubject.k.onNext(Unit.a);
    }

    public final ChannelMemberList getChannelMemberList(long j, long j2) {
        String computeMemberListId = MemberListIdCalculator.INSTANCE.computeMemberListId(this.getChannel, j2, this.storeGuilds);
        Map<String, ChannelMemberList> map = this.memberListsSnapshot.get(Long.valueOf(j));
        if (map == null) {
            map = h0.emptyMap();
        }
        ChannelMemberList channelMemberList = map.get(computeMemberListId);
        return channelMemberList != null ? channelMemberList : new ChannelMemberList(computeMemberListId, this.getGuildMemberCount.invoke(Long.valueOf(j)).intValue(), AppLog.g);
    }

    @StoreThread
    public final void handleGuildMemberListUpdate(ModelGuildMemberListUpdate modelGuildMemberListUpdate) {
        m.checkNotNullParameter(modelGuildMemberListUpdate, "update");
        long guildId = modelGuildMemberListUpdate.getGuildId();
        String id2 = modelGuildMemberListUpdate.getId();
        MemberListUpdateLogger.INSTANCE.logUpdate(modelGuildMemberListUpdate);
        try {
            for (ModelGuildMemberListUpdate.Operation operation : modelGuildMemberListUpdate.getOperations()) {
                if (operation instanceof ModelGuildMemberListUpdate.Operation.Sync) {
                    handleSync(id2, (ModelGuildMemberListUpdate.Operation.Sync) operation, guildId);
                } else if (operation instanceof ModelGuildMemberListUpdate.Operation.Update) {
                    handleUpdate(id2, (ModelGuildMemberListUpdate.Operation.Update) operation, guildId);
                } else if (operation instanceof ModelGuildMemberListUpdate.Operation.Insert) {
                    handleInsert(id2, (ModelGuildMemberListUpdate.Operation.Insert) operation, guildId);
                } else if (operation instanceof ModelGuildMemberListUpdate.Operation.Delete) {
                    handleDelete(id2, (ModelGuildMemberListUpdate.Operation.Delete) operation, guildId);
                } else if (operation instanceof ModelGuildMemberListUpdate.Operation.Invalidate) {
                    handleInvalidate(id2, (ModelGuildMemberListUpdate.Operation.Invalidate) operation, guildId);
                }
            }
            ChannelMemberList memberList = getMemberList(modelGuildMemberListUpdate.getGuildId(), id2);
            if (memberList != null) {
                memberList.setGroups(modelGuildMemberListUpdate.getGroups(), new StoreChannelMembers$handleGuildMemberListUpdate$2(this, guildId));
            }
        } catch (Exception e) {
            MemberListUpdateLogger.INSTANCE.dumpLogs(guildId, id2, e);
        }
        throttleMarkChanged();
    }

    @StoreThread
    public final void handleGuildRemove(long j) {
        if (this.memberLists.remove(Long.valueOf(j)) != null) {
            throttleMarkChanged();
        }
    }

    @StoreThread
    public final void handleGuildRoleCreateOrUpdate(long j) {
        boolean allowOwnerIndicator = allowOwnerIndicator(j);
        Map<String, ChannelMemberList> map = this.memberLists.get(Long.valueOf(j));
        if (map != null) {
            for (Map.Entry<String, ChannelMemberList> entry : map.entrySet()) {
                entry.getValue().rebuildMembers(new StoreChannelMembers$handleGuildRoleCreateOrUpdate$$inlined$forEach$lambda$1(this, j, allowOwnerIndicator));
            }
        }
        throttleMarkChanged();
    }

    public final Observable<ChannelMemberList> observeChannelMemberList(long j, long j2) {
        Observable<ChannelMemberList> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreChannelMembers$observeChannelMemberList$1(this, j, j2), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR…  .distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        super.snapshotData();
        Map<Long, Map<String, ChannelMemberList>> map = this.memberLists;
        LinkedHashMap linkedHashMap = new LinkedHashMap(g0.mapCapacity(map.size()));
        Iterator<T> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            Object key = entry.getKey();
            Map map2 = (Map) entry.getValue();
            LinkedHashMap linkedHashMap2 = new LinkedHashMap(g0.mapCapacity(map2.size()));
            for (Map.Entry entry2 : map2.entrySet()) {
                linkedHashMap2.put(entry2.getKey(), new ChannelMemberList((ChannelMemberList) entry2.getValue()));
            }
            linkedHashMap.put(key, linkedHashMap2);
        }
        this.memberListsSnapshot = linkedHashMap;
    }
}
