package com.discord.stores;

import com.discord.utilities.accessibility.AccessibilityFeatureFlags;
import d0.z.d.o;
import java.util.EnumSet;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreAccessibility.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Ljava/util/EnumSet;", "Lcom/discord/utilities/accessibility/AccessibilityFeatureFlags;", "invoke", "()Ljava/util/EnumSet;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAccessibility$observeAccessibilityFeatures$1 extends o implements Function0<EnumSet<AccessibilityFeatureFlags>> {
    public final /* synthetic */ StoreAccessibility this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreAccessibility$observeAccessibilityFeatures$1(StoreAccessibility storeAccessibility) {
        super(0);
        this.this$0 = storeAccessibility;
    }

    @Override // kotlin.jvm.functions.Function0
    public final EnumSet<AccessibilityFeatureFlags> invoke() {
        return this.this$0.getAccessibilityFeatures();
    }
}
