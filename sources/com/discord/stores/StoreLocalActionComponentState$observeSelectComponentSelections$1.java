package com.discord.stores;

import com.discord.api.botuikit.SelectItem;
import d0.z.d.o;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreLocalActionComponentState.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\t\u001a(\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u001a\u0012\u0018\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u00000\u0000H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"", "", "Lcom/discord/primitives/MessageId;", "", "Lcom/discord/widgets/botuikit/ComponentIndex;", "", "Lcom/discord/api/botuikit/SelectItem;", "invoke", "()Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreLocalActionComponentState$observeSelectComponentSelections$1 extends o implements Function0<Map<Long, ? extends Map<Integer, ? extends List<? extends SelectItem>>>> {
    public final /* synthetic */ StoreLocalActionComponentState this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreLocalActionComponentState$observeSelectComponentSelections$1(StoreLocalActionComponentState storeLocalActionComponentState) {
        super(0);
        this.this$0 = storeLocalActionComponentState;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Map<Long, ? extends Map<Integer, ? extends List<? extends SelectItem>>> invoke() {
        return this.this$0.getSelectComponentSelectionsData();
    }
}
