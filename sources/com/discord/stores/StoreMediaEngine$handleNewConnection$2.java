package com.discord.stores;

import co.discord.media_engine.VideoInputDeviceDescription;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreMediaEngine.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lco/discord/media_engine/VideoInputDeviceDescription;", "devices", "", "invoke", "([Lco/discord/media_engine/VideoInputDeviceDescription;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMediaEngine$handleNewConnection$2 extends o implements Function1<VideoInputDeviceDescription[], Unit> {
    public final /* synthetic */ StoreMediaEngine this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreMediaEngine$handleNewConnection$2(StoreMediaEngine storeMediaEngine) {
        super(1);
        this.this$0 = storeMediaEngine;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(VideoInputDeviceDescription[] videoInputDeviceDescriptionArr) {
        invoke2(videoInputDeviceDescriptionArr);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(VideoInputDeviceDescription[] videoInputDeviceDescriptionArr) {
        VideoInputDeviceDescription videoInputDeviceDescription;
        m.checkNotNullParameter(videoInputDeviceDescriptionArr, "devices");
        StoreMediaEngine storeMediaEngine = this.this$0;
        videoInputDeviceDescription = storeMediaEngine.selectedVideoInputDevice;
        StoreMediaEngine.handleVideoInputDevices$default(storeMediaEngine, videoInputDeviceDescriptionArr, videoInputDeviceDescription != null ? videoInputDeviceDescription.getGuid() : null, null, 4, null);
    }
}
