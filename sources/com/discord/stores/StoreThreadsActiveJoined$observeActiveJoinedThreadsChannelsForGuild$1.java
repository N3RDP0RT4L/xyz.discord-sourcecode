package com.discord.stores;

import com.discord.api.channel.Channel;
import com.discord.stores.StoreThreadsActiveJoined;
import d0.t.g0;
import d0.t.h0;
import d0.z.d.o;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreThreadsActiveJoined.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u00030\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "invoke", "()Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreThreadsActiveJoined$observeActiveJoinedThreadsChannelsForGuild$1 extends o implements Function0<Map<Long, ? extends Channel>> {
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ StoreThreadsActiveJoined this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreThreadsActiveJoined$observeActiveJoinedThreadsChannelsForGuild$1(StoreThreadsActiveJoined storeThreadsActiveJoined, long j) {
        super(0);
        this.this$0 = storeThreadsActiveJoined;
        this.$guildId = j;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Map<Long, ? extends Channel> invoke() {
        Map map;
        Collection<Map> values;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        map = this.this$0.activeJoinedThreadsHierarchicalSnapshot;
        Map map2 = (Map) map.get(Long.valueOf(this.$guildId));
        if (!(map2 == null || (values = map2.values()) == null)) {
            for (Map map3 : values) {
                LinkedHashMap linkedHashMap2 = new LinkedHashMap(g0.mapCapacity(map3.size()));
                for (Map.Entry entry : map3.entrySet()) {
                    linkedHashMap2.put(entry.getKey(), ((StoreThreadsActiveJoined.ActiveJoinedThread) entry.getValue()).getChannel());
                }
                linkedHashMap.putAll(linkedHashMap2);
            }
        }
        return h0.toMap(linkedHashMap);
    }
}
