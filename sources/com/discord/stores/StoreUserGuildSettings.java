package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.models.domain.ModelMuteConfig;
import com.discord.models.domain.ModelNotificationSettings;
import com.discord.models.domain.ModelPayload;
import com.discord.models.guild.Guild;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreThreadsJoined;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.persister.Persister;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.Clock;
import com.discord.widgets.chat.AutocompleteSelectionTypes;
import d0.t.h0;
import d0.t.n0;
import d0.t.u;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Subscription;
import rx.subjects.PublishSubject;
/* compiled from: StoreUserGuildSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000ô\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010$\n\u0002\b\u0004\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0003\u007f\u0080\u0001BC\u0012\u0006\u0010`\u001a\u00020_\u0012\u0006\u0010{\u001a\u00020z\u0012\u0006\u0010n\u001a\u00020m\u0012\u0006\u0010q\u001a\u00020p\u0012\u0006\u0010U\u001a\u00020T\u0012\b\b\u0002\u0010k\u001a\u00020j\u0012\b\b\u0002\u0010x\u001a\u00020w¢\u0006\u0004\b}\u0010~J3\u0010\f\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\f\u0010\rJ?\u0010\u0015\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u000f\u001a\u00060\u0004j\u0002`\u000e2\n\u0010\u0010\u001a\u00060\u0004j\u0002`\u000e2\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u001d\u0010\u001a\u001a\u00020\u000b2\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00180\u0017H\u0003¢\u0006\u0004\b\u001a\u0010\u001bJ\u0017\u0010\u001e\u001a\u00020\t2\u0006\u0010\u001d\u001a\u00020\u001cH\u0002¢\u0006\u0004\b\u001e\u0010\u001fJ\u000f\u0010 \u001a\u00020\u000bH\u0003¢\u0006\u0004\b \u0010!J\u0017\u0010\"\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\"\u0010#J\u001f\u0010'\u001a\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u00180$H\u0001¢\u0006\u0004\b%\u0010&J\u001d\u0010(\u001a\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u00180$¢\u0006\u0004\b(\u0010&J\u0017\u0010*\u001a\f\u0012\b\u0012\u00060\u0004j\u0002`\u00050)¢\u0006\u0004\b*\u0010+J#\u0010-\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u00180$0,¢\u0006\u0004\b-\u0010.J\u001f\u0010-\u001a\b\u0012\u0004\u0012\u00020\u00180,2\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u0005¢\u0006\u0004\b-\u0010/J\u001f\u00101\u001a\b\u0012\u0004\u0012\u0002000,2\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u0005¢\u0006\u0004\b1\u0010/J\u001d\u00102\u001a\u0012\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0004j\u0002`\u00050\u00170,¢\u0006\u0004\b2\u0010.J\u0013\u00104\u001a\b\u0012\u0004\u0012\u0002030,¢\u0006\u0004\b4\u0010.JM\u0010;\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u00106\u001a\u0002052\n\b\u0002\u00107\u001a\u0004\u0018\u0001002\n\b\u0002\u00108\u001a\u0004\u0018\u0001002\n\b\u0002\u00109\u001a\u0004\u0018\u0001002\n\b\u0002\u0010:\u001a\u0004\u0018\u000100¢\u0006\u0004\b;\u0010<J5\u0010?\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0006\u00109\u001a\u0002002\n\b\u0002\u0010>\u001a\u0004\u0018\u00010=¢\u0006\u0004\b?\u0010@J%\u0010B\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u00106\u001a\u0002052\u0006\u0010A\u001a\u00020\u0013¢\u0006\u0004\bB\u0010CJ5\u0010D\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u000f\u001a\u00060\u0004j\u0002`\u000e2\u0006\u00109\u001a\u0002002\n\b\u0002\u0010>\u001a\u0004\u0018\u00010=¢\u0006\u0004\bD\u0010@J%\u0010E\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u001d\u001a\u00020\u001c2\u0006\u0010A\u001a\u00020\u0013¢\u0006\u0004\bE\u0010FJ\u001d\u0010G\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u001d\u001a\u00020\u001c¢\u0006\u0004\bG\u0010HJ!\u0010J\u001a\u00020\u000b2\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010I\u001a\u000200¢\u0006\u0004\bJ\u0010KJ\u0017\u0010P\u001a\u00020\u000b2\u0006\u0010M\u001a\u00020LH\u0001¢\u0006\u0004\bN\u0010OJ\u001d\u0010R\u001a\u00020\u000b2\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00180\u0017H\u0001¢\u0006\u0004\bQ\u0010\u001bJ\u000f\u0010S\u001a\u00020\u000bH\u0016¢\u0006\u0004\bS\u0010!R\u0016\u0010U\u001a\u00020T8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bU\u0010VR&\u0010X\u001a\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u00180W8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bX\u0010YR,\u0010[\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u00180$0Z8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b[\u0010\\R&\u0010]\u001a\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u00180$8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b]\u0010YR&\u0010^\u001a\u0012\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0004j\u0002`\u00050)0Z8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b^\u0010\\R\u0016\u0010`\u001a\u00020_8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b`\u0010aR \u0010c\u001a\f\u0012\b\u0012\u00060\u0004j\u0002`\u00050b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bc\u0010dR \u0010e\u001a\f\u0012\b\u0012\u00060\u0004j\u0002`\u00050)8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\be\u0010fR\u0018\u0010h\u001a\u0004\u0018\u00010g8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bh\u0010iR\u0016\u0010k\u001a\u00020j8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bk\u0010lR\u0016\u0010n\u001a\u00020m8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bn\u0010oR\u0016\u0010q\u001a\u00020p8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bq\u0010rR:\u0010u\u001a&\u0012\f\u0012\n t*\u0004\u0018\u00010303 t*\u0012\u0012\f\u0012\n t*\u0004\u0018\u00010303\u0018\u00010s0s8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bu\u0010vR\u0016\u0010x\u001a\u00020w8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bx\u0010yR\u0016\u0010{\u001a\u00020z8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b{\u0010|¨\u0006\u0081\u0001"}, d2 = {"Lcom/discord/stores/StoreUserGuildSettings;", "Lcom/discord/stores/StoreV2;", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/restapi/RestAPIParams$UserGuildSettings;", "userGuildSettings", "Lcom/discord/stores/StoreUserGuildSettings$SettingsUpdateType;", "settingsUpdateType", "", "updateUserGuildSettings", "(Landroid/content/Context;JLcom/discord/restapi/RestAPIParams$UserGuildSettings;Lcom/discord/stores/StoreUserGuildSettings$SettingsUpdateType;)V", "Lcom/discord/primitives/ChannelId;", "channelId", "parentChannelId", "Lcom/discord/restapi/RestAPIParams$ThreadMemberSettings;", "threadMemberSettings", "", "oldFlags", "updateThreadMemberSettings", "(Landroid/content/Context;JJLcom/discord/restapi/RestAPIParams$ThreadMemberSettings;I)V", "", "Lcom/discord/models/domain/ModelNotificationSettings;", "guildSettingsList", "handleGuildSettings", "(Ljava/util/List;)V", "Lcom/discord/api/channel/Channel;", "channel", "getSettingsUpdateType", "(Lcom/discord/api/channel/Channel;)Lcom/discord/stores/StoreUserGuildSettings$SettingsUpdateType;", "recomputeMuteConfigs", "()V", "init", "(Landroid/content/Context;)V", "", "getGuildSettingsInternal$app_productionGoogleRelease", "()Ljava/util/Map;", "getGuildSettingsInternal", "getGuildSettings", "", "getGuildsToHideMutedChannelsIn", "()Ljava/util/Set;", "Lrx/Observable;", "observeGuildSettings", "()Lrx/Observable;", "(J)Lrx/Observable;", "", "observeHideMutedChannels", "observeMutedGuildIds", "Lcom/discord/stores/StoreUserGuildSettings$Event;", "observeEvents", "Lcom/discord/models/guild/Guild;", "guild", "suppressingEveryone", "suppressingRoles", "muted", "mobilePushEnabled", "setGuildToggles", "(Landroid/content/Context;Lcom/discord/models/guild/Guild;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V", "Lcom/discord/models/domain/ModelMuteConfig;", "muteConfig", "setGuildMuted", "(Landroid/content/Context;JZLcom/discord/models/domain/ModelMuteConfig;)V", "frequency", "setGuildFrequency", "(Landroid/content/Context;Lcom/discord/models/guild/Guild;I)V", "setChannelMuted", "setChannelFrequency", "(Landroid/content/Context;Lcom/discord/api/channel/Channel;I)V", "setChannelNotificationsDefault", "(Landroid/content/Context;Lcom/discord/api/channel/Channel;)V", "hideChannels", "setHideMutedChannels", "(JZ)V", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen$app_productionGoogleRelease", "(Lcom/discord/models/domain/ModelPayload;)V", "handleConnectionOpen", "handleGuildSettingUpdated$app_productionGoogleRelease", "handleGuildSettingUpdated", "snapshotData", "Lcom/discord/stores/StoreThreadsJoined;", "storeThreadsJoined", "Lcom/discord/stores/StoreThreadsJoined;", "", "guildSettings", "Ljava/util/Map;", "Lcom/discord/utilities/persister/Persister;", "guildSettingsCache", "Lcom/discord/utilities/persister/Persister;", "guildSettingsSnapshot", "guildsToHideMutedChannelsInCache", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Ljava/util/HashSet;", "guildsToHideMutedChannelsIn", "Ljava/util/HashSet;", "guildsToHideMutedChannelsInSnapshot", "Ljava/util/Set;", "Lrx/Subscription;", "recomputeSettingsSubscription", "Lrx/Subscription;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/stores/StoreAnalytics;", "analytics", "Lcom/discord/stores/StoreAnalytics;", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreChannels;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "Lcom/discord/utilities/rest/RestAPI;", "restApi", "Lcom/discord/utilities/rest/RestAPI;", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreAnalytics;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreThreadsJoined;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/utilities/rest/RestAPI;)V", "Event", "SettingsUpdateType", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserGuildSettings extends StoreV2 {
    private final StoreAnalytics analytics;
    private final Clock clock;
    private final Dispatcher dispatcher;
    private final PublishSubject<Event> eventSubject;
    private final Map<Long, ModelNotificationSettings> guildSettings;
    private final Persister<Map<Long, ModelNotificationSettings>> guildSettingsCache;
    private Map<Long, ? extends ModelNotificationSettings> guildSettingsSnapshot;
    private final HashSet<Long> guildsToHideMutedChannelsIn;
    private final Persister<Set<Long>> guildsToHideMutedChannelsInCache;
    private Set<Long> guildsToHideMutedChannelsInSnapshot;
    private final ObservationDeck observationDeck;
    private Subscription recomputeSettingsSubscription;
    private final RestAPI restApi;
    private final StoreChannels storeChannels;
    private final StoreThreadsJoined storeThreadsJoined;

    /* compiled from: StoreUserGuildSettings.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0001\u0004B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0001\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/stores/StoreUserGuildSettings$Event;", "", HookHelper.constructorName, "()V", "SettingsUpdated", "Lcom/discord/stores/StoreUserGuildSettings$Event$SettingsUpdated;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class Event {

        /* compiled from: StoreUserGuildSettings.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/stores/StoreUserGuildSettings$Event$SettingsUpdated;", "Lcom/discord/stores/StoreUserGuildSettings$Event;", "Lcom/discord/stores/StoreUserGuildSettings$SettingsUpdateType;", "component1", "()Lcom/discord/stores/StoreUserGuildSettings$SettingsUpdateType;", "type", "copy", "(Lcom/discord/stores/StoreUserGuildSettings$SettingsUpdateType;)Lcom/discord/stores/StoreUserGuildSettings$Event$SettingsUpdated;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StoreUserGuildSettings$SettingsUpdateType;", "getType", HookHelper.constructorName, "(Lcom/discord/stores/StoreUserGuildSettings$SettingsUpdateType;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class SettingsUpdated extends Event {
            private final SettingsUpdateType type;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public SettingsUpdated(SettingsUpdateType settingsUpdateType) {
                super(null);
                m.checkNotNullParameter(settingsUpdateType, "type");
                this.type = settingsUpdateType;
            }

            public static /* synthetic */ SettingsUpdated copy$default(SettingsUpdated settingsUpdated, SettingsUpdateType settingsUpdateType, int i, Object obj) {
                if ((i & 1) != 0) {
                    settingsUpdateType = settingsUpdated.type;
                }
                return settingsUpdated.copy(settingsUpdateType);
            }

            public final SettingsUpdateType component1() {
                return this.type;
            }

            public final SettingsUpdated copy(SettingsUpdateType settingsUpdateType) {
                m.checkNotNullParameter(settingsUpdateType, "type");
                return new SettingsUpdated(settingsUpdateType);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof SettingsUpdated) && m.areEqual(this.type, ((SettingsUpdated) obj).type);
                }
                return true;
            }

            public final SettingsUpdateType getType() {
                return this.type;
            }

            public int hashCode() {
                SettingsUpdateType settingsUpdateType = this.type;
                if (settingsUpdateType != null) {
                    return settingsUpdateType.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("SettingsUpdated(type=");
                R.append(this.type);
                R.append(")");
                return R.toString();
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreUserGuildSettings.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/discord/stores/StoreUserGuildSettings$SettingsUpdateType;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "GUILD", AutocompleteSelectionTypes.CHANNEL, "THREAD", "CATEGORY", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum SettingsUpdateType {
        GUILD,
        CHANNEL,
        THREAD,
        CATEGORY
    }

    public /* synthetic */ StoreUserGuildSettings(Dispatcher dispatcher, Clock clock, StoreAnalytics storeAnalytics, StoreChannels storeChannels, StoreThreadsJoined storeThreadsJoined, ObservationDeck observationDeck, RestAPI restAPI, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(dispatcher, clock, storeAnalytics, storeChannels, storeThreadsJoined, (i & 32) != 0 ? ObservationDeckProvider.get() : observationDeck, (i & 64) != 0 ? RestAPI.Companion.getApi() : restAPI);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final SettingsUpdateType getSettingsUpdateType(Channel channel) {
        return ChannelUtils.C(channel) ? SettingsUpdateType.THREAD : ChannelUtils.B(channel) ? SettingsUpdateType.CHANNEL : SettingsUpdateType.CATEGORY;
    }

    @StoreThread
    private final void handleGuildSettings(List<? extends ModelNotificationSettings> list) {
        Subscription subscription = this.recomputeSettingsSubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        for (ModelNotificationSettings modelNotificationSettings : list) {
            ModelNotificationSettings copyAndRecomputeTransientFields = modelNotificationSettings.copyAndRecomputeTransientFields(this.clock);
            Map<Long, ModelNotificationSettings> map = this.guildSettings;
            m.checkNotNullExpressionValue(copyAndRecomputeTransientFields, "computedSettings");
            if (!m.areEqual(copyAndRecomputeTransientFields, map.get(Long.valueOf(copyAndRecomputeTransientFields.getGuildId())))) {
                this.guildSettings.put(Long.valueOf(copyAndRecomputeTransientFields.getGuildId()), copyAndRecomputeTransientFields);
                markChanged();
            }
        }
        long j = Long.MAX_VALUE;
        for (ModelNotificationSettings modelNotificationSettings2 : this.guildSettings.values()) {
            j = Math.min(j, modelNotificationSettings2.getNextMuteEndTimeMs(this.clock));
        }
        if (j < RecyclerView.FOREVER_NS) {
            Observable<Long> e02 = Observable.e0(j - this.clock.currentTimeMillis(), TimeUnit.MILLISECONDS, this.dispatcher.getScheduler());
            m.checkNotNullExpressionValue(e02, "Observable.timer(\n      …patcher.scheduler\n      )");
            ObservableExtensionsKt.appSubscribe(e02, StoreUserGuildSettings.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new StoreUserGuildSettings$handleGuildSettings$1(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreUserGuildSettings$handleGuildSettings$2(this));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void recomputeMuteConfigs() {
        handleGuildSettingUpdated$app_productionGoogleRelease(new ArrayList(this.guildSettings.values()));
    }

    public static /* synthetic */ void setChannelMuted$default(StoreUserGuildSettings storeUserGuildSettings, Context context, long j, boolean z2, ModelMuteConfig modelMuteConfig, int i, Object obj) {
        if ((i & 8) != 0) {
            modelMuteConfig = null;
        }
        storeUserGuildSettings.setChannelMuted(context, j, z2, modelMuteConfig);
    }

    public static /* synthetic */ void setGuildMuted$default(StoreUserGuildSettings storeUserGuildSettings, Context context, long j, boolean z2, ModelMuteConfig modelMuteConfig, int i, Object obj) {
        if ((i & 8) != 0) {
            modelMuteConfig = null;
        }
        storeUserGuildSettings.setGuildMuted(context, j, z2, modelMuteConfig);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateThreadMemberSettings(Context context, long j, long j2, RestAPIParams.ThreadMemberSettings threadMemberSettings, int i) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(ObservableExtensionsKt.restSubscribeOn$default(this.restApi.updateThreadMemberSettings(j, threadMemberSettings), false, 1, null)), StoreUserGuildSettings.class, (r18 & 2) != 0 ? null : context, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreUserGuildSettings$updateThreadMemberSettings$1(this, j, j2, i));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateUserGuildSettings(Context context, long j, RestAPIParams.UserGuildSettings userGuildSettings, SettingsUpdateType settingsUpdateType) {
        Set<Long> keySet;
        Map<Long, RestAPIParams.UserGuildSettings.ChannelOverride> channelOverrides = userGuildSettings.getChannelOverrides();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(ObservableExtensionsKt.restSubscribeOn$default(this.restApi.updateUserGuildSettings(j, userGuildSettings), false, 1, null)), StoreUserGuildSettings.class, (r18 & 2) != 0 ? null : context, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreUserGuildSettings$updateUserGuildSettings$1(this, (channelOverrides == null || (keySet = channelOverrides.keySet()) == null) ? null : (Long) u.firstOrNull(keySet), settingsUpdateType));
    }

    public final Map<Long, ModelNotificationSettings> getGuildSettings() {
        return this.guildSettingsSnapshot;
    }

    @StoreThread
    public final Map<Long, ModelNotificationSettings> getGuildSettingsInternal$app_productionGoogleRelease() {
        return this.guildSettings;
    }

    public final Set<Long> getGuildsToHideMutedChannelsIn() {
        return this.guildsToHideMutedChannelsInSnapshot;
    }

    @StoreThread
    public final void handleConnectionOpen$app_productionGoogleRelease(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        ModelPayload.VersionedUserGuildSettings userGuildSettings = modelPayload.getUserGuildSettings();
        m.checkNotNullExpressionValue(userGuildSettings, "payload.userGuildSettings");
        if (!userGuildSettings.isPartial()) {
            this.guildSettings.clear();
        }
        ModelPayload.VersionedUserGuildSettings userGuildSettings2 = modelPayload.getUserGuildSettings();
        m.checkNotNullExpressionValue(userGuildSettings2, "payload.userGuildSettings");
        List<ModelNotificationSettings> entries = userGuildSettings2.getEntries();
        m.checkNotNullExpressionValue(entries, "payload.userGuildSettings.entries");
        handleGuildSettings(entries);
        markChanged();
    }

    @StoreThread
    public final void handleGuildSettingUpdated$app_productionGoogleRelease(List<? extends ModelNotificationSettings> list) {
        m.checkNotNullParameter(list, "guildSettingsList");
        handleGuildSettings(list);
    }

    @Override // com.discord.stores.Store
    public void init(Context context) {
        m.checkNotNullParameter(context, "context");
        super.init(context);
        this.guildsToHideMutedChannelsIn.addAll(this.guildsToHideMutedChannelsInCache.get());
        this.guildSettings.putAll(this.guildSettingsCache.get());
        markChanged();
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    public final Observable<Map<Long, ModelNotificationSettings>> observeGuildSettings() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreUserGuildSettings$observeGuildSettings$1(this), 14, null);
    }

    public final Observable<Boolean> observeHideMutedChannels(final long j) {
        Observable<Boolean> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreUserGuildSettings$observeHideMutedChannels$1(this), 14, null).F(new b<Set<? extends Long>, Boolean>() { // from class: com.discord.stores.StoreUserGuildSettings$observeHideMutedChannels$2
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Boolean call(Set<? extends Long> set) {
                return call2((Set<Long>) set);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Boolean call2(Set<Long> set) {
                return Boolean.valueOf(set.contains(Long.valueOf(j)));
            }
        }).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<List<Long>> observeMutedGuildIds() {
        Observable<List<Long>> q = observeGuildSettings().F(StoreUserGuildSettings$observeMutedGuildIds$1.INSTANCE).q();
        m.checkNotNullExpressionValue(q, "observeGuildSettings()\n …  .distinctUntilChanged()");
        return q;
    }

    public final void setChannelFrequency(Context context, Channel channel, int i) {
        int i2;
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(channel, "channel");
        SettingsUpdateType settingsUpdateType = getSettingsUpdateType(channel);
        if (ChannelUtils.C(channel)) {
            if (i == ModelNotificationSettings.FREQUENCY_NOTHING) {
                i2 = 8;
            } else {
                i2 = i == ModelNotificationSettings.FREQUENCY_MENTIONS ? 4 : 2;
            }
            StoreThreadsJoined.JoinedThread joinedThread = this.storeThreadsJoined.getJoinedThread(channel.h());
            updateThreadMemberSettings(context, channel.h(), channel.r(), new RestAPIParams.ThreadMemberSettings(Integer.valueOf(i2), null, null, 6, null), joinedThread != null ? joinedThread.getFlags() : 0);
            return;
        }
        updateUserGuildSettings(context, channel.f(), new RestAPIParams.UserGuildSettings(channel.h(), new RestAPIParams.UserGuildSettings.ChannelOverride(Integer.valueOf(i))), settingsUpdateType);
    }

    public final void setChannelMuted(Context context, long j, boolean z2, ModelMuteConfig modelMuteConfig) {
        m.checkNotNullParameter(context, "context");
        this.dispatcher.schedule(new StoreUserGuildSettings$setChannelMuted$1(this, j, context, z2, modelMuteConfig));
    }

    public final void setChannelNotificationsDefault(Context context, Channel channel) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(channel, "channel");
        if (!ChannelUtils.C(channel)) {
            updateUserGuildSettings(context, channel.f(), new RestAPIParams.UserGuildSettings(channel.h(), new RestAPIParams.UserGuildSettings.ChannelOverride(Boolean.FALSE, null, Integer.valueOf(ModelNotificationSettings.FREQUENCY_UNSET))), getSettingsUpdateType(channel));
        }
    }

    public final void setGuildFrequency(Context context, Guild guild, int i) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(guild, "guild");
        updateUserGuildSettings(context, guild.getId(), new RestAPIParams.UserGuildSettings(null, null, null, null, null, Integer.valueOf(i), null, 95, null), SettingsUpdateType.GUILD);
    }

    public final void setGuildMuted(Context context, long j, boolean z2, ModelMuteConfig modelMuteConfig) {
        m.checkNotNullParameter(context, "context");
        updateUserGuildSettings(context, j, new RestAPIParams.UserGuildSettings(null, null, Boolean.valueOf(z2), modelMuteConfig, null, null, null, 115, null), SettingsUpdateType.GUILD);
    }

    public final void setGuildToggles(Context context, Guild guild, Boolean bool, Boolean bool2, Boolean bool3, Boolean bool4) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(guild, "guild");
        updateUserGuildSettings(context, guild.getId(), new RestAPIParams.UserGuildSettings(bool, bool2, bool3, null, bool4, null, null, 104, null), SettingsUpdateType.GUILD);
    }

    public final void setHideMutedChannels(long j, boolean z2) {
        this.dispatcher.schedule(new StoreUserGuildSettings$setHideMutedChannels$1(this, z2, j));
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        super.snapshotData();
        this.guildSettingsSnapshot = new HashMap(this.guildSettings);
        Persister.set$default(this.guildSettingsCache, this.guildSettings, false, 2, null);
        this.guildsToHideMutedChannelsInSnapshot = new HashSet(this.guildsToHideMutedChannelsIn);
        this.guildsToHideMutedChannelsInCache.set(this.guildsToHideMutedChannelsIn, true);
    }

    public final Observable<ModelNotificationSettings> observeGuildSettings(final long j) {
        Observable<ModelNotificationSettings> q = observeGuildSettings().F(new b<Map<Long, ? extends ModelNotificationSettings>, ModelNotificationSettings>() { // from class: com.discord.stores.StoreUserGuildSettings$observeGuildSettings$2
            public final ModelNotificationSettings call(Map<Long, ? extends ModelNotificationSettings> map) {
                ModelNotificationSettings modelNotificationSettings = map.get(Long.valueOf(j));
                return modelNotificationSettings != null ? modelNotificationSettings : new ModelNotificationSettings();
            }
        }).q();
        m.checkNotNullExpressionValue(q, "observeGuildSettings()\n …  .distinctUntilChanged()");
        return q;
    }

    public StoreUserGuildSettings(Dispatcher dispatcher, Clock clock, StoreAnalytics storeAnalytics, StoreChannels storeChannels, StoreThreadsJoined storeThreadsJoined, ObservationDeck observationDeck, RestAPI restAPI) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(storeAnalytics, "analytics");
        m.checkNotNullParameter(storeChannels, "storeChannels");
        m.checkNotNullParameter(storeThreadsJoined, "storeThreadsJoined");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(restAPI, "restApi");
        this.dispatcher = dispatcher;
        this.clock = clock;
        this.analytics = storeAnalytics;
        this.storeChannels = storeChannels;
        this.storeThreadsJoined = storeThreadsJoined;
        this.observationDeck = observationDeck;
        this.restApi = restAPI;
        this.guildSettings = new HashMap();
        this.guildsToHideMutedChannelsIn = new HashSet<>();
        this.guildSettingsSnapshot = h0.emptyMap();
        this.guildsToHideMutedChannelsInSnapshot = n0.emptySet();
        this.guildSettingsCache = new Persister<>("STORE_SETTINGS_USER_GUILD_V5", new HashMap());
        this.guildsToHideMutedChannelsInCache = new Persister<>("STORE_SHOW_HIDE_MUTED_CHANNELS_V2", new HashSet());
        this.eventSubject = PublishSubject.k0();
    }
}
