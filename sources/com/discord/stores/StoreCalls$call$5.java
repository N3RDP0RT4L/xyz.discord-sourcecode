package com.discord.stores;

import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.voice.state.VoiceState;
import com.discord.models.user.User;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreCalls.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u000b\u001a\u00020\b2z\u0010\u0007\u001av\u0012\u0006\u0012\u0004\u0018\u00010\u0001\u0012,\u0012*\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u0005 \u0006*\u0014\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00020\u0002 \u0006*:\u0012\u0006\u0012\u0004\u0018\u00010\u0001\u0012,\u0012*\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u0005 \u0006*\u0014\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00020\u0002\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\t\u0010\n"}, d2 = {"Lkotlin/Pair;", "Lcom/discord/api/channel/Channel;", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/api/voice/state/VoiceState;", "kotlin.jvm.PlatformType", "<name for destructuring parameter 0>", "", "invoke", "(Lkotlin/Pair;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreCalls$call$5 extends o implements Function1<Pair<? extends Channel, ? extends Map<Long, ? extends VoiceState>>, Unit> {
    public final /* synthetic */ StoreCalls$call$1 $doCall$1;
    public final /* synthetic */ StoreCalls$call$2 $doCallIfCallable$2;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreCalls$call$5(StoreCalls$call$2 storeCalls$call$2, StoreCalls$call$1 storeCalls$call$1) {
        super(1);
        this.$doCallIfCallable$2 = storeCalls$call$2;
        this.$doCall$1 = storeCalls$call$1;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Pair<? extends Channel, ? extends Map<Long, ? extends VoiceState>> pair) {
        invoke2((Pair<Channel, ? extends Map<Long, VoiceState>>) pair);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Pair<Channel, ? extends Map<Long, VoiceState>> pair) {
        Channel component1 = pair.component1();
        Map<Long, VoiceState> component2 = pair.component2();
        User a = component1 != null ? ChannelUtils.a(component1) : null;
        if (a != null) {
            this.$doCallIfCallable$2.invoke(a.getId());
        } else if (component1 != null) {
            this.$doCall$1.invoke(component2.isEmpty());
        }
    }
}
