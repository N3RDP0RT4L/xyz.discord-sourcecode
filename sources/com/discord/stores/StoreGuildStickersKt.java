package com.discord.stores;

import kotlin.Metadata;
/* compiled from: StoreGuildStickers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0002*&\u0010\u0004\"\u000e\u0012\u0004\u0012\u0002`\u0001\u0012\u0004\u0012\u00020\u00020\u00002\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0001\u0012\u0004\u0012\u00020\u00020\u0000¨\u0006\u0005"}, d2 = {"", "Lcom/discord/primitives/StickerId;", "Lcom/discord/api/sticker/Sticker;", "", "StickerMap", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildStickersKt {
}
