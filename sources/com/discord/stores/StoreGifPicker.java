package com.discord.stores;

import andhook.lib.HookHelper;
import androidx.exifinterface.media.ExifInterface;
import com.discord.models.gifpicker.domain.ModelGifCategory;
import com.discord.models.gifpicker.dto.GifCategoryDto;
import com.discord.models.gifpicker.dto.ModelGif;
import com.discord.models.gifpicker.dto.TrendingGifCategoriesResponseDto;
import com.discord.models.gifpicker.dto.TrendingGifPreviewDto;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$1;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$2;
import d0.t.h0;
import d0.t.n;
import d0.t.o;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreGifPicker.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b#\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 _2\u00020\u0001:\u0002`_B+\u0012\u0006\u0010?\u001a\u00020>\u0012\u0006\u0010T\u001a\u00020S\u0012\b\b\u0002\u0010Y\u001a\u00020X\u0012\b\b\u0002\u0010G\u001a\u00020F¢\u0006\u0004\b]\u0010^J\u0015\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0015\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0002H\u0002¢\u0006\u0004\b\u0007\u0010\u0005J!\u0010\n\u001a\u0014\u0012\u0004\u0012\u00020\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\u00020\bH\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0015\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\u0002H\u0002¢\u0006\u0004\b\f\u0010\u0005J!\u0010\r\u001a\u0014\u0012\u0004\u0012\u00020\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00020\bH\u0002¢\u0006\u0004\b\r\u0010\u000bJ\u000f\u0010\u000e\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ'\u0010\u0011\u001a\u001a\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00020\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\u00020\b0\u0010H\u0002¢\u0006\u0004\b\u0011\u0010\u0012J'\u0010\u0013\u001a\u001a\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00020\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00020\b0\u0010H\u0002¢\u0006\u0004\b\u0013\u0010\u0012J\u000f\u0010\u0015\u001a\u00020\u0014H\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0017\u0010\u0019\u001a\u00020\u00142\u0006\u0010\u0018\u001a\u00020\u0017H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u000f\u0010\u001b\u001a\u00020\u0014H\u0002¢\u0006\u0004\b\u001b\u0010\u0016J\u001d\u0010\u001d\u001a\u00020\u00142\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00060\u0002H\u0002¢\u0006\u0004\b\u001d\u0010\u001eJ\u000f\u0010\u001f\u001a\u00020\u0014H\u0002¢\u0006\u0004\b\u001f\u0010\u0016J\u001d\u0010!\u001a\u00020\u00142\f\u0010 \u001a\b\u0012\u0004\u0012\u00020\t0\u0002H\u0002¢\u0006\u0004\b!\u0010\u001eJ\u000f\u0010\"\u001a\u00020\u0014H\u0002¢\u0006\u0004\b\"\u0010\u0016J\u000f\u0010#\u001a\u00020\u0014H\u0002¢\u0006\u0004\b#\u0010\u0016J\u0017\u0010%\u001a\u00020\u00142\u0006\u0010$\u001a\u00020\u0017H\u0003¢\u0006\u0004\b%\u0010\u001aJ\u001d\u0010&\u001a\u00020\u00142\f\u0010 \u001a\b\u0012\u0004\u0012\u00020\t0\u0002H\u0003¢\u0006\u0004\b&\u0010\u001eJ\u001d\u0010(\u001a\u00020\u00142\f\u0010'\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0003¢\u0006\u0004\b(\u0010\u001eJ\u001d\u0010*\u001a\u00020\u00142\f\u0010)\u001a\b\u0012\u0004\u0012\u00020\u00060\u0002H\u0003¢\u0006\u0004\b*\u0010\u001eJ\u0017\u0010,\u001a\u00020\u00142\u0006\u0010+\u001a\u00020\u0006H\u0002¢\u0006\u0004\b,\u0010-J%\u0010.\u001a\u00020\u00142\u0006\u0010+\u001a\u00020\u00062\f\u0010 \u001a\b\u0012\u0004\u0012\u00020\t0\u0002H\u0003¢\u0006\u0004\b.\u0010/J\u0017\u00100\u001a\u00020\u00142\u0006\u0010+\u001a\u00020\u0006H\u0002¢\u0006\u0004\b0\u0010-J%\u00102\u001a\u00020\u00142\u0006\u0010+\u001a\u00020\u00062\f\u00101\u001a\b\u0012\u0004\u0012\u00020\u00060\u0002H\u0003¢\u0006\u0004\b2\u0010/J\u0019\u00103\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00020\u0010¢\u0006\u0004\b3\u0010\u0012J\u0019\u00104\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00020\u0010¢\u0006\u0004\b4\u0010\u0012J\u0013\u00105\u001a\b\u0012\u0004\u0012\u00020\u00060\u0010¢\u0006\u0004\b5\u0010\u0012J\u0019\u00106\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\u00020\u0010¢\u0006\u0004\b6\u0010\u0012J!\u0010\u0013\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00020\u00102\u0006\u0010+\u001a\u00020\u0006¢\u0006\u0004\b\u0013\u00107J!\u00108\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\u00020\u00102\u0006\u0010+\u001a\u00020\u0006¢\u0006\u0004\b8\u00107J\r\u00109\u001a\u00020\u0014¢\u0006\u0004\b9\u0010\u0016J\u000f\u0010:\u001a\u00020\u0014H\u0016¢\u0006\u0004\b:\u0010\u0016R\u0016\u0010<\u001a\u00020;8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b<\u0010=R\u0016\u0010?\u001a\u00020>8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b?\u0010@R(\u0010A\u001a\u0014\u0012\u0004\u0012\u00020\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00020\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bA\u0010BR\u001c\u0010C\u001a\b\u0012\u0004\u0012\u00020\u00060\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bC\u0010DR\u0016\u0010E\u001a\u00020;8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bE\u0010=R\u0016\u0010G\u001a\u00020F8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bG\u0010HR\u001c\u0010I\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bI\u0010DR(\u0010J\u001a\u0014\u0012\u0004\u0012\u00020\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\u00020\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bJ\u0010BR\u0016\u0010K\u001a\u00020\u00068\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bK\u0010LR(\u0010N\u001a\u0014\u0012\u0004\u0012\u00020\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00020M8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bN\u0010OR\u001c\u0010Q\u001a\b\u0012\u0004\u0012\u00020\u00030P8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bQ\u0010DR\u001c\u0010R\u001a\b\u0012\u0004\u0012\u00020\t0\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bR\u0010DR\u0016\u0010T\u001a\u00020S8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bT\u0010UR\u001c\u0010V\u001a\b\u0012\u0004\u0012\u00020\u00060P8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bV\u0010DR\u001c\u0010W\u001a\b\u0012\u0004\u0012\u00020\t0P8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bW\u0010DR\u0016\u0010Y\u001a\u00020X8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bY\u0010ZR\u0016\u0010[\u001a\u00020;8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b[\u0010=R(\u0010\\\u001a\u0014\u0012\u0004\u0012\u00020\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\u00020M8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\\\u0010O¨\u0006a"}, d2 = {"Lcom/discord/stores/StoreGifPicker;", "Lcom/discord/stores/StoreV2;", "", "Lcom/discord/models/gifpicker/domain/ModelGifCategory;", "getGifCategories", "()Ljava/util/List;", "", "getGifTrendingSearchTerms", "", "Lcom/discord/models/gifpicker/dto/ModelGif;", "getSearchHistory", "()Ljava/util/Map;", "getTrendingCategoryGifs", "getSuggestedSearchTermsHistory", "getTrendingGifCategoryPreviewUrl", "()Ljava/lang/String;", "Lrx/Observable;", "observeSearchHistory", "()Lrx/Observable;", "observeSuggestedSearchTerms", "", "handleFetchGifCategoriesError", "()V", "Lcom/discord/models/gifpicker/dto/TrendingGifCategoriesResponseDto;", "trendingGifsResponseRaw", "handleFetchGifCategoriesOnNext", "(Lcom/discord/models/gifpicker/dto/TrendingGifCategoriesResponseDto;)V", "handleFetchTrendingSearchTermsError", "trendingSearchTerms", "handleFetchTrendingSearchTermsOnNext", "(Ljava/util/List;)V", "handleFetchTrendingGifsError", "gifs", "handleFetchTrendingGifsOnNext", "fetchTrendingCategoryGifs", "fetchGifTrendingSearchTerms", "trendingGifCategoriesResponseDto", "handleTrendingCategoriesResponse", "updateTrendingCategoryGifs", "categories", "updateGifCategories", "trendingGifSearchTerms", "updateTrendingSearchTerms", "query", "fetchGifsForSearchQuery", "(Ljava/lang/String;)V", "handleGifSearchResults", "(Ljava/lang/String;Ljava/util/List;)V", "fetchSuggestedSearchTerms", "suggestedSearchTerms", "handleSuggestedSearchTerms", "observeGifCategories", "observeGifTrendingSearchTerms", "observeTrendingGifCategoryPreviewUrl", "observeTrendingCategoryGifs", "(Ljava/lang/String;)Lrx/Observable;", "observeGifsForSearchQuery", "fetchGifCategories", "snapshotData", "", "isFetchingTrendingSearchTerms", "Z", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "suggestedSearchTermsSnapshot", "Ljava/util/Map;", "gifTrendingSearchTermsSnapshot", "Ljava/util/List;", "isFetchingGifCategories", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "gifCategoriesSnapshot", "gifSearchHistorySnapshot", "trendingGifCategoryPreviewUrl", "Ljava/lang/String;", "Lcom/discord/stores/StoreGifPicker$CacheHistory;", "gifSuggestedSearchTermsHistory", "Lcom/discord/stores/StoreGifPicker$CacheHistory;", "", "gifCategories", "trendingCategoryGifsSnapshot", "Lcom/discord/stores/StoreUserSettingsSystem;", "storeUserSettingsSystem", "Lcom/discord/stores/StoreUserSettingsSystem;", "gifTrendingSearchTerms", "trendingCategoryGifs", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "isFetchingTrendingCategoryGifs", "gifSearchHistory", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreUserSettingsSystem;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/updates/ObservationDeck;)V", "Companion", "CacheHistory", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGifPicker extends StoreV2 {
    public static final Companion Companion = new Companion(null);
    private static final List<ModelGif> searchResultsLoadingList = n.emptyList();
    private static final List<String> searchTermsLoadingList = n.emptyList();
    private final Dispatcher dispatcher;
    private final List<ModelGifCategory> gifCategories;
    private List<ModelGifCategory> gifCategoriesSnapshot;
    private final CacheHistory<String, List<ModelGif>> gifSearchHistory;
    private Map<String, ? extends List<ModelGif>> gifSearchHistorySnapshot;
    private final CacheHistory<String, List<String>> gifSuggestedSearchTermsHistory;
    private final List<String> gifTrendingSearchTerms;
    private List<String> gifTrendingSearchTermsSnapshot;
    private boolean isFetchingGifCategories;
    private boolean isFetchingTrendingCategoryGifs;
    private boolean isFetchingTrendingSearchTerms;
    private final ObservationDeck observationDeck;
    private final RestAPI restAPI;
    private final StoreUserSettingsSystem storeUserSettingsSystem;
    private Map<String, ? extends List<String>> suggestedSearchTermsSnapshot;
    private final List<ModelGif> trendingCategoryGifs;
    private List<ModelGif> trendingCategoryGifsSnapshot;
    private String trendingGifCategoryPreviewUrl;

    /* compiled from: StoreGifPicker.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010'\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\b\u0002\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u00022\u001e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0003j\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001`\u0004B\u0007¢\u0006\u0004\b\n\u0010\u000bJ%\u0010\b\u001a\u00020\u00072\u0014\u0010\u0006\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u0005H\u0014¢\u0006\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/stores/StoreGifPicker$CacheHistory;", "K", ExifInterface.GPS_MEASUREMENT_INTERRUPTED, "Ljava/util/LinkedHashMap;", "Lkotlin/collections/LinkedHashMap;", "", "eldest", "", "removeEldestEntry", "(Ljava/util/Map$Entry;)Z", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class CacheHistory<K, V> extends LinkedHashMap<K, V> {
        @Override // java.util.LinkedHashMap, java.util.HashMap, java.util.AbstractMap, java.util.Map
        public final /* bridge */ Set<Map.Entry<K, V>> entrySet() {
            return getEntries();
        }

        public /* bridge */ Set getEntries() {
            return super.entrySet();
        }

        public /* bridge */ Set getKeys() {
            return super.keySet();
        }

        public /* bridge */ int getSize() {
            return super.size();
        }

        public /* bridge */ Collection getValues() {
            return super.values();
        }

        @Override // java.util.LinkedHashMap, java.util.HashMap, java.util.AbstractMap, java.util.Map
        public final /* bridge */ Set<K> keySet() {
            return getKeys();
        }

        @Override // java.util.LinkedHashMap
        public boolean removeEldestEntry(Map.Entry<K, V> entry) {
            return size() > 20;
        }

        @Override // java.util.HashMap, java.util.AbstractMap, java.util.Map
        public final /* bridge */ int size() {
            return getSize();
        }

        @Override // java.util.LinkedHashMap, java.util.HashMap, java.util.AbstractMap, java.util.Map
        public final /* bridge */ Collection<V> values() {
            return getValues();
        }
    }

    /* compiled from: StoreGifPicker.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fR\u001f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007R\u001f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0005\u001a\u0004\b\n\u0010\u0007¨\u0006\r"}, d2 = {"Lcom/discord/stores/StoreGifPicker$Companion;", "", "", "", "searchTermsLoadingList", "Ljava/util/List;", "getSearchTermsLoadingList", "()Ljava/util/List;", "Lcom/discord/models/gifpicker/dto/ModelGif;", "searchResultsLoadingList", "getSearchResultsLoadingList", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public final List<ModelGif> getSearchResultsLoadingList() {
            return StoreGifPicker.searchResultsLoadingList;
        }

        public final List<String> getSearchTermsLoadingList() {
            return StoreGifPicker.searchTermsLoadingList;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ StoreGifPicker(Dispatcher dispatcher, StoreUserSettingsSystem storeUserSettingsSystem, RestAPI restAPI, ObservationDeck observationDeck, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(dispatcher, storeUserSettingsSystem, (i & 4) != 0 ? RestAPI.Companion.getApi() : restAPI, (i & 8) != 0 ? ObservationDeckProvider.get() : observationDeck);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void fetchGifTrendingSearchTerms() {
        this.isFetchingTrendingSearchTerms = true;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.getGifTrendingSearchTerms("tenor", this.storeUserSettingsSystem.getLocale(), 5), false, 1, null), StoreGifPicker.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new StoreGifPicker$fetchGifTrendingSearchTerms$1(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreGifPicker$fetchGifTrendingSearchTerms$2(this));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void fetchGifsForSearchQuery(String str) {
        Observable<R> F = this.restAPI.getGifSearchResults(str, "tenor", this.storeUserSettingsSystem.getLocale(), "tinygif", 50).F(StoreGifPicker$fetchGifsForSearchQuery$1.INSTANCE);
        m.checkNotNullExpressionValue(F, "restAPI.getGifSearchResu…to)\n          }\n        }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(F, false, 1, null), StoreGifPicker.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new StoreGifPicker$fetchGifsForSearchQuery$2(this, str), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreGifPicker$fetchGifsForSearchQuery$3(this, str));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void fetchSuggestedSearchTerms(String str) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.getGifSuggestedSearchTerms("tenor", str, this.storeUserSettingsSystem.getLocale(), 5), false, 1, null), StoreGifPicker.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new StoreGifPicker$fetchSuggestedSearchTerms$1(this, str), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreGifPicker$fetchSuggestedSearchTerms$2(this, str));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void fetchTrendingCategoryGifs() {
        this.isFetchingTrendingCategoryGifs = true;
        Observable<R> F = this.restAPI.getTrendingGifCategory("tenor", this.storeUserSettingsSystem.getLocale(), "tinygif", 50).F(StoreGifPicker$fetchTrendingCategoryGifs$1.INSTANCE);
        m.checkNotNullExpressionValue(F, "restAPI.getTrendingGifCa…to)\n          }\n        }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(F, false, 1, null), StoreGifPicker.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new StoreGifPicker$fetchTrendingCategoryGifs$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreGifPicker$fetchTrendingCategoryGifs$3(this));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final List<ModelGifCategory> getGifCategories() {
        return this.gifCategoriesSnapshot;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final List<String> getGifTrendingSearchTerms() {
        return this.gifTrendingSearchTermsSnapshot;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Map<String, List<ModelGif>> getSearchHistory() {
        return this.gifSearchHistorySnapshot;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Map<String, List<String>> getSuggestedSearchTermsHistory() {
        return this.gifSuggestedSearchTermsHistory;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final List<ModelGif> getTrendingCategoryGifs() {
        return this.trendingCategoryGifsSnapshot;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final String getTrendingGifCategoryPreviewUrl() {
        return this.trendingGifCategoryPreviewUrl;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleFetchGifCategoriesError() {
        this.dispatcher.schedule(new StoreGifPicker$handleFetchGifCategoriesError$1(this));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleFetchGifCategoriesOnNext(TrendingGifCategoriesResponseDto trendingGifCategoriesResponseDto) {
        this.dispatcher.schedule(new StoreGifPicker$handleFetchGifCategoriesOnNext$1(this, trendingGifCategoriesResponseDto));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleFetchTrendingGifsError() {
        this.dispatcher.schedule(new StoreGifPicker$handleFetchTrendingGifsError$1(this));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleFetchTrendingGifsOnNext(List<ModelGif> list) {
        this.dispatcher.schedule(new StoreGifPicker$handleFetchTrendingGifsOnNext$1(this, list));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleFetchTrendingSearchTermsError() {
        this.dispatcher.schedule(new StoreGifPicker$handleFetchTrendingSearchTermsError$1(this));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleFetchTrendingSearchTermsOnNext(List<String> list) {
        this.dispatcher.schedule(new StoreGifPicker$handleFetchTrendingSearchTermsOnNext$1(this, list));
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleGifSearchResults(String str, List<ModelGif> list) {
        this.gifSearchHistory.put(str, list);
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleSuggestedSearchTerms(String str, List<String> list) {
        this.gifSuggestedSearchTermsHistory.put(str, list);
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleTrendingCategoriesResponse(TrendingGifCategoriesResponseDto trendingGifCategoriesResponseDto) {
        List<GifCategoryDto> categories = trendingGifCategoriesResponseDto.getCategories();
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(categories, 10));
        for (GifCategoryDto gifCategoryDto : categories) {
            arrayList.add(new ModelGifCategory(gifCategoryDto.getName(), gifCategoryDto.getSrc()));
        }
        updateGifCategories(arrayList);
        List<TrendingGifPreviewDto> gifs = trendingGifCategoriesResponseDto.getGifs();
        this.trendingGifCategoryPreviewUrl = gifs.isEmpty() ^ true ? gifs.get(0).getSrc() : "";
        markChanged();
    }

    private final Observable<Map<String, List<ModelGif>>> observeSearchHistory() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, true, null, null, new StoreGifPicker$observeSearchHistory$1(this), 12, null);
    }

    private final Observable<Map<String, List<String>>> observeSuggestedSearchTerms() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, true, null, null, new StoreGifPicker$observeSuggestedSearchTerms$1(this), 12, null);
    }

    @StoreThread
    private final void updateGifCategories(List<ModelGifCategory> list) {
        this.gifCategories.clear();
        this.gifCategories.addAll(list);
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void updateTrendingCategoryGifs(List<ModelGif> list) {
        this.trendingCategoryGifs.clear();
        this.trendingCategoryGifs.addAll(list);
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void updateTrendingSearchTerms(List<String> list) {
        this.gifTrendingSearchTerms.clear();
        this.gifTrendingSearchTerms.addAll(list);
        markChanged();
    }

    public final void fetchGifCategories() {
        this.isFetchingGifCategories = true;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.getTrendingGifCategories("tenor", this.storeUserSettingsSystem.getLocale(), "tinygif"), false, 1, null), StoreGifPicker.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new StoreGifPicker$fetchGifCategories$1(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreGifPicker$fetchGifCategories$2(this));
    }

    public final Observable<List<ModelGifCategory>> observeGifCategories() {
        this.dispatcher.schedule(new StoreGifPicker$observeGifCategories$1(this));
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, true, null, null, new StoreGifPicker$observeGifCategories$2(this), 12, null);
    }

    public final Observable<List<String>> observeGifTrendingSearchTerms() {
        this.dispatcher.schedule(new StoreGifPicker$observeGifTrendingSearchTerms$1(this));
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, true, null, null, new StoreGifPicker$observeGifTrendingSearchTerms$2(this), 12, null);
    }

    public final Observable<List<ModelGif>> observeGifsForSearchQuery(final String str) {
        m.checkNotNullParameter(str, "query");
        this.dispatcher.schedule(new StoreGifPicker$observeGifsForSearchQuery$1(this, str));
        Observable<R> F = observeSearchHistory().F(new b<Map<String, ? extends List<? extends ModelGif>>, List<? extends ModelGif>>() { // from class: com.discord.stores.StoreGifPicker$observeGifsForSearchQuery$2
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ List<? extends ModelGif> call(Map<String, ? extends List<? extends ModelGif>> map) {
                return call2((Map<String, ? extends List<ModelGif>>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final List<ModelGif> call2(Map<String, ? extends List<ModelGif>> map) {
                return map.get(str);
            }
        });
        m.checkNotNullExpressionValue(F, "observeSearchHistory().m…gifSearchHistory[query] }");
        Observable<List<ModelGif>> F2 = F.x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
        m.checkNotNullExpressionValue(F2, "filter { it != null }.map { it!! }");
        return F2;
    }

    public final Observable<List<ModelGif>> observeTrendingCategoryGifs() {
        this.dispatcher.schedule(new StoreGifPicker$observeTrendingCategoryGifs$1(this));
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, true, null, null, new StoreGifPicker$observeTrendingCategoryGifs$2(this), 12, null);
    }

    public final Observable<String> observeTrendingGifCategoryPreviewUrl() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, true, null, null, new StoreGifPicker$observeTrendingGifCategoryPreviewUrl$1(this), 12, null);
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        super.snapshotData();
        this.gifCategoriesSnapshot = new ArrayList(this.gifCategories);
        this.gifTrendingSearchTermsSnapshot = new ArrayList(this.gifTrendingSearchTerms);
        this.gifSearchHistorySnapshot = new HashMap(this.gifSearchHistory);
        this.suggestedSearchTermsSnapshot = new HashMap(this.gifSuggestedSearchTermsHistory);
        this.trendingCategoryGifsSnapshot = new ArrayList(this.trendingCategoryGifs);
    }

    public final Observable<List<String>> observeSuggestedSearchTerms(final String str) {
        m.checkNotNullParameter(str, "query");
        this.dispatcher.schedule(new StoreGifPicker$observeSuggestedSearchTerms$2(this, str));
        Observable<R> F = observeSuggestedSearchTerms().F(new b<Map<String, ? extends List<? extends String>>, List<? extends String>>() { // from class: com.discord.stores.StoreGifPicker$observeSuggestedSearchTerms$3
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ List<? extends String> call(Map<String, ? extends List<? extends String>> map) {
                return call2((Map<String, ? extends List<String>>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final List<String> call2(Map<String, ? extends List<String>> map) {
                return map.get(str);
            }
        });
        m.checkNotNullExpressionValue(F, "observeSuggestedSearchTe…archTermsHistory[query] }");
        Observable<List<String>> F2 = F.x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
        m.checkNotNullExpressionValue(F2, "filter { it != null }.map { it!! }");
        return F2;
    }

    public StoreGifPicker(Dispatcher dispatcher, StoreUserSettingsSystem storeUserSettingsSystem, RestAPI restAPI, ObservationDeck observationDeck) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(storeUserSettingsSystem, "storeUserSettingsSystem");
        m.checkNotNullParameter(restAPI, "restAPI");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.dispatcher = dispatcher;
        this.storeUserSettingsSystem = storeUserSettingsSystem;
        this.restAPI = restAPI;
        this.observationDeck = observationDeck;
        this.gifCategoriesSnapshot = n.emptyList();
        this.gifTrendingSearchTermsSnapshot = n.emptyList();
        this.gifSearchHistorySnapshot = h0.emptyMap();
        this.suggestedSearchTermsSnapshot = h0.emptyMap();
        this.trendingCategoryGifsSnapshot = n.emptyList();
        this.gifCategories = new ArrayList();
        this.gifTrendingSearchTerms = new ArrayList();
        this.trendingCategoryGifs = new ArrayList();
        this.trendingGifCategoryPreviewUrl = "";
        this.gifSearchHistory = new CacheHistory<>();
        this.gifSuggestedSearchTermsHistory = new CacheHistory<>();
    }
}
