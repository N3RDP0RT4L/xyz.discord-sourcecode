package com.discord.stores;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import com.discord.api.channel.Channel;
import com.discord.stores.StoreSlowMode;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.time.Clock;
import d0.z.d.m;
import j0.k.b;
import j0.l.a.c0;
import j0.l.a.i2;
import j0.l.a.r;
import j0.l.e.k;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Action0;
import rx.subjects.BehaviorSubject;
/* compiled from: StoreSlowMode.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 ,2\u00020\u0001:\u0002,-B\u0017\u0012\u0006\u0010'\u001a\u00020&\u0012\u0006\u0010\u001f\u001a\u00020\u001e¢\u0006\u0004\b*\u0010+J+\u0010\t\u001a\u00020\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0006H\u0003¢\u0006\u0004\b\t\u0010\nJ)\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\r\u0010\u000eJ+\u0010\u000f\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u000b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\u000f\u0010\u000eJ+\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\u000e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\u0010\u0010\u0011J\u001b\u0010\u0012\u001a\u00020\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0007¢\u0006\u0004\b\u0012\u0010\u0013J\u001b\u0010\u0015\u001a\u00020\b2\n\u0010\u0014\u001a\u00060\u0002j\u0002`\u0003H\u0007¢\u0006\u0004\b\u0015\u0010\u0013J+\u0010\u0016\u001a\u00020\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0006H\u0007¢\u0006\u0004\b\u0016\u0010\nRÔ\u0003\u0010\u001c\u001a¿\u0003\u0012×\u0001\u0012Ô\u0001\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00030\u0002j\u0002`\u0003\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u001a0\u0002j\u0002`\u001a \u0019*j\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00030\u0002j\u0002`\u0003\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u001a0\u0002j\u0002`\u001a\u0018\u00010\u0018j4\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00030\u0002j\u0002`\u0003\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u001a0\u0002j\u0002`\u001a\u0018\u0001`\u001b0\u0018j2\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00030\u0002j\u0002`\u0003\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u001a0\u0002j\u0002`\u001a`\u001b \u0019*Þ\u0001\u0012×\u0001\u0012Ô\u0001\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00030\u0002j\u0002`\u0003\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u001a0\u0002j\u0002`\u001a \u0019*j\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00030\u0002j\u0002`\u0003\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u001a0\u0002j\u0002`\u001a\u0018\u00010\u0018j4\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00030\u0002j\u0002`\u0003\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u001a0\u0002j\u0002`\u001a\u0018\u0001`\u001b0\u0018j2\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00030\u0002j\u0002`\u0003\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u001a0\u0002j\u0002`\u001a`\u001b\u0018\u00010\u00170\u00178\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR\u0016\u0010\u001f\u001a\u00020\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 RF\u0010!\u001a2\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\u0018j\u0018\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b`\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"RÔ\u0003\u0010#\u001a¿\u0003\u0012×\u0001\u0012Ô\u0001\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00030\u0002j\u0002`\u0003\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u001a0\u0002j\u0002`\u001a \u0019*j\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00030\u0002j\u0002`\u0003\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u001a0\u0002j\u0002`\u001a\u0018\u00010\u0018j4\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00030\u0002j\u0002`\u0003\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u001a0\u0002j\u0002`\u001a\u0018\u0001`\u001b0\u0018j2\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00030\u0002j\u0002`\u0003\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u001a0\u0002j\u0002`\u001a`\u001b \u0019*Þ\u0001\u0012×\u0001\u0012Ô\u0001\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00030\u0002j\u0002`\u0003\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u001a0\u0002j\u0002`\u001a \u0019*j\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00030\u0002j\u0002`\u0003\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u001a0\u0002j\u0002`\u001a\u0018\u00010\u0018j4\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00030\u0002j\u0002`\u0003\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u001a0\u0002j\u0002`\u001a\u0018\u0001`\u001b0\u0018j2\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00030\u0002j\u0002`\u0003\u0012\u0016\u0012\u0014 \u0019*\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u001a0\u0002j\u0002`\u001a`\u001b\u0018\u00010\u00170\u00178\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010\u001dRB\u0010$\u001a.\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\b\u0012\u00060\u0002j\u0002`\u001a0\u0018j\u0016\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\b\u0012\u00060\u0002j\u0002`\u001a`\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010\"RF\u0010%\u001a2\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\u0018j\u0018\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b`\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b%\u0010\"R\u0016\u0010'\u001a\u00020&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(RB\u0010)\u001a.\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\b\u0012\u00060\u0002j\u0002`\u001a0\u0018j\u0016\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\b\u0012\u00060\u0002j\u0002`\u001a`\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b)\u0010\"¨\u0006."}, d2 = {"Lcom/discord/stores/StoreSlowMode;", "Lcom/discord/stores/Store;", "", "Lcom/discord/primitives/ChannelId;", "channelId", "cooldownMs", "Lcom/discord/stores/StoreSlowMode$Type;", "type", "", "onCooldownInternal", "(JJLcom/discord/stores/StoreSlowMode$Type;)V", "Lrx/Observable;", "", "getChannelCooldownObservable", "(JLcom/discord/stores/StoreSlowMode$Type;)Lrx/Observable;", "removeChannelCooldownObservable", "observeCooldownSecs", "(Ljava/lang/Long;Lcom/discord/stores/StoreSlowMode$Type;)Lrx/Observable;", "onMessageSent", "(J)V", "parentChannelId", "onThreadCreated", "onCooldown", "Lrx/subjects/BehaviorSubject;", "Ljava/util/HashMap;", "kotlin.jvm.PlatformType", "Lcom/discord/primitives/Timestamp;", "Lkotlin/collections/HashMap;", "threadCreateNextSendTimesSubject", "Lrx/subjects/BehaviorSubject;", "Lcom/discord/stores/StoreStream;", "stream", "Lcom/discord/stores/StoreStream;", "channelMessageSendCooldownObservables", "Ljava/util/HashMap;", "messageSendNextSendTimesSubject", "threadCreateNextSendTimes", "channelThreadCreateCooldownObservables", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "messageSendNextSendTimes", HookHelper.constructorName, "(Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreStream;)V", "Companion", "Type", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreSlowMode extends Store {
    @Deprecated
    private static final long COOLDOWN_BUFFER_MS = 1000;
    private static final Companion Companion = new Companion(null);
    private final HashMap<Long, Observable<Integer>> channelMessageSendCooldownObservables = new HashMap<>();
    private final HashMap<Long, Observable<Integer>> channelThreadCreateCooldownObservables = new HashMap<>();
    private final Clock clock;
    private final HashMap<Long, Long> messageSendNextSendTimes;
    private final BehaviorSubject<HashMap<Long, Long>> messageSendNextSendTimesSubject;
    private final StoreStream stream;
    private final HashMap<Long, Long> threadCreateNextSendTimes;
    private final BehaviorSubject<HashMap<Long, Long>> threadCreateNextSendTimesSubject;

    /* compiled from: StoreSlowMode.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0005\b\u0082\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/stores/StoreSlowMode$Companion;", "", "", "COOLDOWN_BUFFER_MS", "J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreSlowMode.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/stores/StoreSlowMode$Type;", "", HookHelper.constructorName, "()V", "MessageSend", "ThreadCreate", "Lcom/discord/stores/StoreSlowMode$Type$MessageSend;", "Lcom/discord/stores/StoreSlowMode$Type$ThreadCreate;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class Type {

        /* compiled from: StoreSlowMode.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreSlowMode$Type$MessageSend;", "Lcom/discord/stores/StoreSlowMode$Type;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class MessageSend extends Type {
            public static final MessageSend INSTANCE = new MessageSend();

            private MessageSend() {
                super(null);
            }
        }

        /* compiled from: StoreSlowMode.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreSlowMode$Type$ThreadCreate;", "Lcom/discord/stores/StoreSlowMode$Type;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class ThreadCreate extends Type {
            public static final ThreadCreate INSTANCE = new ThreadCreate();

            private ThreadCreate() {
                super(null);
            }
        }

        private Type() {
        }

        public /* synthetic */ Type(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public StoreSlowMode(Clock clock, StoreStream storeStream) {
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(storeStream, "stream");
        this.clock = clock;
        this.stream = storeStream;
        HashMap<Long, Long> hashMap = new HashMap<>();
        this.messageSendNextSendTimes = hashMap;
        this.messageSendNextSendTimesSubject = BehaviorSubject.l0(new HashMap(hashMap));
        HashMap<Long, Long> hashMap2 = new HashMap<>();
        this.threadCreateNextSendTimes = hashMap2;
        this.threadCreateNextSendTimesSubject = BehaviorSubject.l0(new HashMap(hashMap2));
    }

    private final synchronized Observable<Integer> getChannelCooldownObservable(final long j, final Type type) {
        Observable<Integer> observable;
        Type.MessageSend messageSend = Type.MessageSend.INSTANCE;
        if (m.areEqual(type, messageSend)) {
            observable = this.channelMessageSendCooldownObservables.get(Long.valueOf(j));
        } else {
            observable = this.channelThreadCreateCooldownObservables.get(Long.valueOf(j));
        }
        if (observable != null) {
            return observable;
        }
        Observable<Integer> h02 = Observable.h0(new c0(this.stream.getPermissions$app_productionGoogleRelease().observePermissionsForChannel(j).F(new b<Long, Boolean>() { // from class: com.discord.stores.StoreSlowMode$getChannelCooldownObservable$newObservable$1
            public final Boolean call(Long l) {
                return Boolean.valueOf(PermissionUtils.INSTANCE.hasBypassSlowmodePermissions(l, StoreSlowMode.Type.this));
            }
        }).q().Y(new b<Boolean, Observable<? extends Long>>() { // from class: com.discord.stores.StoreSlowMode$getChannelCooldownObservable$newObservable$2
            public final Observable<? extends Long> call(Boolean bool) {
                m.checkNotNullExpressionValue(bool, "shouldOverrideCooldown");
                if (bool.booleanValue()) {
                    return new k(0L);
                }
                return (Observable<R>) (m.areEqual(type, StoreSlowMode.Type.MessageSend.INSTANCE) ? StoreSlowMode.this.messageSendNextSendTimesSubject : StoreSlowMode.this.threadCreateNextSendTimesSubject).F(new b<HashMap<Long, Long>, Long>() { // from class: com.discord.stores.StoreSlowMode$getChannelCooldownObservable$newObservable$2.1
                    public final Long call(HashMap<Long, Long> hashMap) {
                        return hashMap.get(Long.valueOf(j));
                    }
                }).Y(new b<Long, Observable<? extends Long>>() { // from class: com.discord.stores.StoreSlowMode$getChannelCooldownObservable$newObservable$2.2

                    /* compiled from: StoreSlowMode.kt */
                    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Long;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                    /* renamed from: com.discord.stores.StoreSlowMode$getChannelCooldownObservable$newObservable$2$2$2  reason: invalid class name and collision with other inner class name */
                    /* loaded from: classes.dex */
                    public static final class C02132<T, R> implements b<Long, Boolean> {
                        public static final C02132 INSTANCE = new C02132();

                        public final Boolean call(Long l) {
                            return Boolean.valueOf(l.longValue() >= 0);
                        }
                    }

                    public final Observable<? extends Long> call(final Long l) {
                        Clock clock;
                        clock = StoreSlowMode.this.clock;
                        final long currentTimeMillis = clock.currentTimeMillis();
                        if (l == null || l.longValue() <= currentTimeMillis) {
                            return new k(0L);
                        }
                        Observable<R> F = Observable.D(0L, 1L, TimeUnit.SECONDS).F(new b<Long, Long>() { // from class: com.discord.stores.StoreSlowMode.getChannelCooldownObservable.newObservable.2.2.1
                            public final Long call(Long l2) {
                                return Long.valueOf(l.longValue() - (currentTimeMillis + (l2.longValue() * 1000)));
                            }
                        });
                        return Observable.h0(new r(F.j, new i2(C02132.INSTANCE)));
                    }
                });
            }
        }).F(StoreSlowMode$getChannelCooldownObservable$newObservable$3.INSTANCE).v(new Action0() { // from class: com.discord.stores.StoreSlowMode$getChannelCooldownObservable$newObservable$4
            @Override // rx.functions.Action0
            public final void call() {
                StoreSlowMode.this.removeChannelCooldownObservable(j, type);
            }
        }).M(1)));
        if (m.areEqual(type, messageSend)) {
            HashMap<Long, Observable<Integer>> hashMap = this.channelMessageSendCooldownObservables;
            Long valueOf = Long.valueOf(j);
            m.checkNotNullExpressionValue(h02, "newObservable");
            hashMap.put(valueOf, h02);
        } else {
            HashMap<Long, Observable<Integer>> hashMap2 = this.channelThreadCreateCooldownObservables;
            Long valueOf2 = Long.valueOf(j);
            m.checkNotNullExpressionValue(h02, "newObservable");
            hashMap2.put(valueOf2, h02);
        }
        return h02;
    }

    @StoreThread
    private final void onCooldownInternal(long j, long j2, Type type) {
        if (m.areEqual(type, Type.MessageSend.INSTANCE)) {
            this.messageSendNextSendTimes.put(Long.valueOf(j), Long.valueOf(this.clock.currentTimeMillis() + j2));
            this.messageSendNextSendTimesSubject.onNext(new HashMap<>(this.messageSendNextSendTimes));
            return;
        }
        this.threadCreateNextSendTimes.put(Long.valueOf(j), Long.valueOf(this.clock.currentTimeMillis() + j2));
        this.threadCreateNextSendTimesSubject.onNext(new HashMap<>(this.threadCreateNextSendTimes));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized Observable<Integer> removeChannelCooldownObservable(long j, Type type) {
        Observable<Integer> observable;
        if (m.areEqual(type, Type.MessageSend.INSTANCE)) {
            observable = this.channelMessageSendCooldownObservables.remove(Long.valueOf(j));
        } else {
            observable = this.channelThreadCreateCooldownObservables.remove(Long.valueOf(j));
        }
        return observable;
    }

    public final Observable<Integer> observeCooldownSecs(Long l, Type type) {
        m.checkNotNullParameter(type, "type");
        if (l != null) {
            return getChannelCooldownObservable(l.longValue(), type);
        }
        k kVar = new k(0);
        m.checkNotNullExpressionValue(kVar, "Observable.just(0)");
        return kVar;
    }

    @StoreThread
    public final void onCooldown(long j, long j2, Type type) {
        m.checkNotNullParameter(type, "type");
        onCooldownInternal(j, j2 + 1000, type);
    }

    @StoreThread
    public final void onMessageSent(long j) {
        Channel findChannelByIdInternal$app_productionGoogleRelease = this.stream.getChannels$app_productionGoogleRelease().findChannelByIdInternal$app_productionGoogleRelease(j);
        int u = findChannelByIdInternal$app_productionGoogleRelease != null ? findChannelByIdInternal$app_productionGoogleRelease.u() : 0;
        if (u > 0) {
            onCooldownInternal(j, u * 1000, Type.MessageSend.INSTANCE);
        }
    }

    @StoreThread
    public final void onThreadCreated(long j) {
        Channel findChannelByIdInternal$app_productionGoogleRelease = this.stream.getChannels$app_productionGoogleRelease().findChannelByIdInternal$app_productionGoogleRelease(j);
        int u = findChannelByIdInternal$app_productionGoogleRelease != null ? findChannelByIdInternal$app_productionGoogleRelease.u() : 0;
        if (u > 0) {
            onCooldownInternal(j, u * 1000, Type.ThreadCreate.INSTANCE);
        }
    }
}
