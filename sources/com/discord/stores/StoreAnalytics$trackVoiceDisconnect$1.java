package com.discord.stores;

import com.discord.api.channel.Channel;
import com.discord.stores.StoreMediaSettings;
import com.discord.utilities.analytics.AnalyticsTracker;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreAnalytics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAnalytics$trackVoiceDisconnect$1 extends o implements Function0<Unit> {
    public final /* synthetic */ Map $properties;
    public final /* synthetic */ StoreAnalytics this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreAnalytics$trackVoiceDisconnect$1(StoreAnalytics storeAnalytics, Map map) {
        super(0);
        this.this$0 = storeAnalytics;
        this.$properties = map;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreStream storeStream;
        StoreStream storeStream2;
        StoreStream storeStream3;
        StoreStream storeStream4;
        storeStream = this.this$0.stores;
        long lastSelectedVoiceChannelId = storeStream.getVoiceChannelSelected$app_productionGoogleRelease().getLastSelectedVoiceChannelId();
        storeStream2 = this.this$0.stores;
        Channel findChannelByIdInternal$app_productionGoogleRelease = storeStream2.getChannels$app_productionGoogleRelease().findChannelByIdInternal$app_productionGoogleRelease(lastSelectedVoiceChannelId);
        storeStream3 = this.this$0.stores;
        StoreMediaSettings mediaSettings$app_productionGoogleRelease = storeStream3.getMediaSettings$app_productionGoogleRelease();
        AnalyticsTracker analyticsTracker = AnalyticsTracker.INSTANCE;
        Map<String, ? extends Object> map = this.$properties;
        StoreMediaSettings.VoiceConfiguration voiceConfigurationBlocking = mediaSettings$app_productionGoogleRelease.getVoiceConfigurationBlocking();
        storeStream4 = this.this$0.stores;
        analyticsTracker.voiceDisconnect(map, voiceConfigurationBlocking, findChannelByIdInternal$app_productionGoogleRelease, storeStream4.getRtcRegion$app_productionGoogleRelease().getPreferredRegion());
    }
}
