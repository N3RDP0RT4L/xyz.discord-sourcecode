package com.discord.stores;

import android.content.Context;
import com.discord.stores.StoreDynamicLink;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreDynamicLink.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;", "dynamicLinkData", "", "invoke", "(Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreDynamicLink$storeLinkIfExists$3 extends o implements Function1<StoreDynamicLink.DynamicLinkData, Unit> {
    public final /* synthetic */ Context $context;
    public final /* synthetic */ StoreDynamicLink this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreDynamicLink$storeLinkIfExists$3(StoreDynamicLink storeDynamicLink, Context context) {
        super(1);
        this.this$0 = storeDynamicLink;
        this.$context = context;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(StoreDynamicLink.DynamicLinkData dynamicLinkData) {
        invoke2(dynamicLinkData);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(StoreDynamicLink.DynamicLinkData dynamicLinkData) {
        this.this$0.handleDataReceived(dynamicLinkData, this.$context);
    }
}
