package com.discord.stores;

import com.discord.models.commands.Application;
import d0.t.n;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreApplicationCommands.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreApplicationCommands$requestApplications$1 extends o implements Function0<Unit> {
    public final /* synthetic */ Long $guildId;
    public final /* synthetic */ StoreApplicationCommands this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreApplicationCommands$requestApplications$1(StoreApplicationCommands storeApplicationCommands, Long l) {
        super(0);
        this.this$0 = storeApplicationCommands;
        this.$guildId = l;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        String generateNonce;
        Map map;
        BuiltInCommandsProvider builtInCommandsProvider;
        BuiltInCommandsProvider builtInCommandsProvider2;
        StoreGatewayConnection storeGatewayConnection;
        generateNonce = this.this$0.generateNonce();
        this.this$0.applicationNonce = generateNonce;
        this.this$0.applications = n.emptyList();
        map = this.this$0.applicationCommandIndexes;
        map.clear();
        this.this$0.markChanged(StoreApplicationCommands.Companion.getGuildApplicationsUpdate());
        Long l = this.$guildId;
        if (l == null || l.longValue() <= 0) {
            StoreApplicationCommands storeApplicationCommands = this.this$0;
            builtInCommandsProvider = storeApplicationCommands.builtInCommandsProvider;
            builtInCommandsProvider2 = this.this$0.builtInCommandsProvider;
            storeApplicationCommands.handleGuildApplicationsUpdate(n.listOf((Object[]) new Application[]{builtInCommandsProvider.getFrecencyApplication(), builtInCommandsProvider2.getBuiltInApplication()}));
            return;
        }
        storeGatewayConnection = this.this$0.storeGatewayConnection;
        storeGatewayConnection.requestApplicationCommands(this.$guildId.longValue(), generateNonce, true, (r20 & 8) != 0 ? null : null, (r20 & 16) != 0 ? null : null, 0, (r20 & 64) != 0 ? null : null);
    }
}
