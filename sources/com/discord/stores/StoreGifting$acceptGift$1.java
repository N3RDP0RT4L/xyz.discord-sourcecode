package com.discord.stores;

import com.discord.models.domain.ModelGift;
import com.discord.restapi.RestAPIAbortCodes;
import com.discord.stores.StoreGifting;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import j0.p.a;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import rx.Observable;
/* compiled from: StoreGifting.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGifting$acceptGift$1 extends o implements Function0<Unit> {
    public final /* synthetic */ ModelGift $gift;
    public final /* synthetic */ StoreGifting this$0;

    /* compiled from: StoreGifting.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "error", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreGifting$acceptGift$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<Error, Unit> {

        /* compiled from: StoreGifting.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreGifting$acceptGift$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C02011 extends o implements Function0<Unit> {
            public final /* synthetic */ Error $error;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C02011(Error error) {
                super(0);
                this.$error = error;
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                ModelGift copy;
                ModelGift copy2;
                if (this.$error.getType() == Error.Type.DISCORD_BAD_REQUEST) {
                    this.$error.setShowErrorToasts(false);
                    Error.Response response = this.$error.getResponse();
                    m.checkNotNullExpressionValue(response, "error.response");
                    switch (response.getCode()) {
                        case RestAPIAbortCodes.GIFTING_ALREADY_USED_ERROR_CODE /* 50050 */:
                            StoreGifting$acceptGift$1 storeGifting$acceptGift$1 = StoreGifting$acceptGift$1.this;
                            StoreGifting storeGifting = storeGifting$acceptGift$1.this$0;
                            String code = storeGifting$acceptGift$1.$gift.getCode();
                            ModelGift modelGift = StoreGifting$acceptGift$1.this.$gift;
                            copy = modelGift.copy((r26 & 1) != 0 ? modelGift.skuId : 0L, (r26 & 2) != 0 ? modelGift.redeemed : false, (r26 & 4) != 0 ? modelGift.expiresAt : null, (r26 & 8) != 0 ? modelGift.code : null, (r26 & 16) != 0 ? modelGift.uses : modelGift.getMaxUses(), (r26 & 32) != 0 ? modelGift.storeListing : null, (r26 & 64) != 0 ? modelGift.maxUses : 0, (r26 & 128) != 0 ? modelGift.user : null, (r26 & 256) != 0 ? modelGift.subscriptionPlanId : null, (r26 & 512) != 0 ? modelGift.subscriptionPlan : null, (r26 & 1024) != 0 ? modelGift.giftStyle : null);
                            storeGifting.setGifts(code, new StoreGifting.GiftState.Resolved(copy));
                            return;
                        case RestAPIAbortCodes.GIFTING_ALREADY_OWNED_ERROR_CODE /* 50051 */:
                            StoreGifting$acceptGift$1 storeGifting$acceptGift$12 = StoreGifting$acceptGift$1.this;
                            StoreGifting storeGifting2 = storeGifting$acceptGift$12.this$0;
                            String code2 = storeGifting$acceptGift$12.$gift.getCode();
                            copy2 = r5.copy((r26 & 1) != 0 ? r5.skuId : 0L, (r26 & 2) != 0 ? r5.redeemed : true, (r26 & 4) != 0 ? r5.expiresAt : null, (r26 & 8) != 0 ? r5.code : null, (r26 & 16) != 0 ? r5.uses : 0, (r26 & 32) != 0 ? r5.storeListing : null, (r26 & 64) != 0 ? r5.maxUses : 0, (r26 & 128) != 0 ? r5.user : null, (r26 & 256) != 0 ? r5.subscriptionPlanId : null, (r26 & 512) != 0 ? r5.subscriptionPlan : null, (r26 & 1024) != 0 ? StoreGifting$acceptGift$1.this.$gift.giftStyle : null);
                            storeGifting2.setGifts(code2, new StoreGifting.GiftState.Resolved(copy2));
                            return;
                        default:
                            StoreGifting$acceptGift$1 storeGifting$acceptGift$13 = StoreGifting$acceptGift$1.this;
                            StoreGifting storeGifting3 = storeGifting$acceptGift$13.this$0;
                            String code3 = storeGifting$acceptGift$13.$gift.getCode();
                            ModelGift modelGift2 = StoreGifting$acceptGift$1.this.$gift;
                            Error.Response response2 = this.$error.getResponse();
                            m.checkNotNullExpressionValue(response2, "error.response");
                            storeGifting3.setGifts(code3, new StoreGifting.GiftState.RedeemedFailed(modelGift2, false, Integer.valueOf(response2.getCode())));
                            return;
                    }
                } else {
                    StoreGifting$acceptGift$1 storeGifting$acceptGift$14 = StoreGifting$acceptGift$1.this;
                    storeGifting$acceptGift$14.this$0.setGifts(storeGifting$acceptGift$14.$gift.getCode(), new StoreGifting.GiftState.RedeemedFailed(StoreGifting$acceptGift$1.this.$gift, true, null));
                }
            }
        }

        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Error error) {
            invoke2(error);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Error error) {
            m.checkNotNullParameter(error, "error");
            StoreGifting$acceptGift$1.this.this$0.getDispatcher().schedule(new C02011(error));
        }
    }

    /* compiled from: StoreGifting.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Ljava/lang/Void;", "it", "", "invoke", "(Ljava/lang/Void;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreGifting$acceptGift$1$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2 extends o implements Function1<Void, Unit> {

        /* compiled from: StoreGifting.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreGifting$acceptGift$1$2$1  reason: invalid class name */
        /* loaded from: classes.dex */
        public static final class AnonymousClass1 extends o implements Function0<Unit> {
            public AnonymousClass1() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                ModelGift copy;
                StoreGifting$acceptGift$1 storeGifting$acceptGift$1 = StoreGifting$acceptGift$1.this;
                StoreGifting storeGifting = storeGifting$acceptGift$1.this$0;
                String code = storeGifting$acceptGift$1.$gift.getCode();
                copy = r5.copy((r26 & 1) != 0 ? r5.skuId : 0L, (r26 & 2) != 0 ? r5.redeemed : true, (r26 & 4) != 0 ? r5.expiresAt : null, (r26 & 8) != 0 ? r5.code : null, (r26 & 16) != 0 ? r5.uses : 0, (r26 & 32) != 0 ? r5.storeListing : null, (r26 & 64) != 0 ? r5.maxUses : 0, (r26 & 128) != 0 ? r5.user : null, (r26 & 256) != 0 ? r5.subscriptionPlanId : null, (r26 & 512) != 0 ? r5.subscriptionPlan : null, (r26 & 1024) != 0 ? StoreGifting$acceptGift$1.this.$gift.giftStyle : null);
                storeGifting.setGifts(code, new StoreGifting.GiftState.Resolved(copy));
            }
        }

        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Void r1) {
            invoke2(r1);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Void r2) {
            StoreGifting$acceptGift$1.this.this$0.getDispatcher().schedule(new AnonymousClass1());
            StoreStream.Companion.getLibrary().fetchApplications();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGifting$acceptGift$1(StoreGifting storeGifting, ModelGift modelGift) {
        super(0);
        this.this$0 = storeGifting;
        this.$gift = modelGift;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        this.this$0.setGifts(this.$gift.getCode(), new StoreGifting.GiftState.Redeeming(this.$gift));
        Observable<Void> X = RestAPI.Companion.getApi().acceptGift(this.$gift.getCode()).X(a.c());
        m.checkNotNullExpressionValue(X, "RestAPI\n        .api\n   …scribeOn(Schedulers.io())");
        ObservableExtensionsKt.appSubscribe(X, this.this$0.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new AnonymousClass1(), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2());
    }
}
