package com.discord.stores;

import com.discord.api.user.UserProfile;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import java.util.HashSet;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import rx.Observable;
/* compiled from: StoreUserProfile.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserProfile$fetchProfile$1 extends o implements Function0<Unit> {
    public final /* synthetic */ Long $guildId;
    public final /* synthetic */ Function1 $onFetchSuccess;
    public final /* synthetic */ long $userId;
    public final /* synthetic */ boolean $withMutualGuilds;
    public final /* synthetic */ StoreUserProfile this$0;

    /* compiled from: StoreUserProfile.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/user/UserProfile;", "userProfile", "", "invoke", "(Lcom/discord/api/user/UserProfile;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreUserProfile$fetchProfile$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<UserProfile, Unit> {

        /* compiled from: StoreUserProfile.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreUserProfile$fetchProfile$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C02171 extends o implements Function0<Unit> {
            public final /* synthetic */ UserProfile $userProfile;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C02171(UserProfile userProfile) {
                super(0);
                this.$userProfile = userProfile;
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                StoreStream storeStream;
                if (this.$userProfile != null) {
                    storeStream = StoreUserProfile$fetchProfile$1.this.this$0.storeStream;
                    storeStream.handleUserProfile(this.$userProfile, StoreUserProfile$fetchProfile$1.this.$guildId);
                    Function1 function1 = StoreUserProfile$fetchProfile$1.this.$onFetchSuccess;
                    if (function1 != null) {
                        Unit unit = (Unit) function1.invoke(this.$userProfile);
                    }
                }
            }
        }

        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(UserProfile userProfile) {
            invoke2(userProfile);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(UserProfile userProfile) {
            Dispatcher dispatcher;
            dispatcher = StoreUserProfile$fetchProfile$1.this.this$0.dispatcher;
            dispatcher.schedule(new C02171(userProfile));
        }
    }

    /* compiled from: StoreUserProfile.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreUserProfile$fetchProfile$1$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2 extends o implements Function1<Error, Unit> {

        /* compiled from: StoreUserProfile.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreUserProfile$fetchProfile$1$2$1  reason: invalid class name */
        /* loaded from: classes.dex */
        public static final class AnonymousClass1 extends o implements Function0<Unit> {
            public AnonymousClass1() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                StoreUserProfile$fetchProfile$1 storeUserProfile$fetchProfile$1 = StoreUserProfile$fetchProfile$1.this;
                storeUserProfile$fetchProfile$1.this$0.handleFailure(storeUserProfile$fetchProfile$1.$userId);
            }
        }

        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Error error) {
            invoke2(error);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Error error) {
            Dispatcher dispatcher;
            m.checkNotNullParameter(error, "it");
            dispatcher = StoreUserProfile$fetchProfile$1.this.this$0.dispatcher;
            dispatcher.schedule(new AnonymousClass1());
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreUserProfile$fetchProfile$1(StoreUserProfile storeUserProfile, long j, Long l, boolean z2, Function1 function1) {
        super(0);
        this.this$0 = storeUserProfile;
        this.$userId = j;
        this.$guildId = l;
        this.$withMutualGuilds = z2;
        this.$onFetchSuccess = function1;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        HashSet hashSet;
        HashSet hashSet2;
        RestAPI restAPI;
        hashSet = this.this$0.profilesLoading;
        if (!hashSet.contains(Long.valueOf(this.$userId))) {
            hashSet2 = this.this$0.profilesLoading;
            hashSet2.add(Long.valueOf(this.$userId));
            restAPI = this.this$0.restAPI;
            Observable q = ObservableExtensionsKt.restSubscribeOn$default(restAPI.userProfileGet(this.$userId, this.$withMutualGuilds, this.$guildId), false, 1, null).q();
            m.checkNotNullExpressionValue(q, "restAPI\n          .userP…  .distinctUntilChanged()");
            ObservableExtensionsKt.appSubscribe(q, this.this$0.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new AnonymousClass2(), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
        }
    }
}
