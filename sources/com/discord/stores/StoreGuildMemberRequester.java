package com.discord.stores;

import andhook.lib.HookHelper;
import com.discord.api.channel.Channel;
import com.discord.api.guildmember.GuildMembersChunk;
import com.discord.api.user.User;
import com.discord.models.member.GuildMember;
import com.discord.models.message.Message;
import com.discord.utilities.lazy.requester.GuildMemberRequestManager;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import kotlin.Metadata;
/* compiled from: StoreGuildMemberRequester.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010*\u001a\u00020)\u0012\u0006\u0010&\u001a\u00020%¢\u0006\u0004\b4\u00105J)\u0010\t\u001a\u00020\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u0003¢\u0006\u0004\b\t\u0010\nJ'\u0010\u000e\u001a\u00020\r2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\f\u001a\u00060\u0002j\u0002`\u000bH\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ-\u0010\u0012\u001a\u00020\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0010\u0010\u0011\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u000b0\u0010H\u0003¢\u0006\u0004\b\u0012\u0010\u0013J%\u0010\u0014\u001a\u00020\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\f\u001a\u00060\u0002j\u0002`\u000b¢\u0006\u0004\b\u0014\u0010\u0015J\r\u0010\u0016\u001a\u00020\b¢\u0006\u0004\b\u0016\u0010\u0017J\u0017\u0010\u0019\u001a\u00020\b2\u0006\u0010\u0018\u001a\u00020\rH\u0007¢\u0006\u0004\b\u0019\u0010\u001aJ\u000f\u0010\u001b\u001a\u00020\bH\u0007¢\u0006\u0004\b\u001b\u0010\u0017J)\u0010\u001f\u001a\u00020\b2\n\u0010\u001d\u001a\u00060\u0002j\u0002`\u001c2\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u001eH\u0007¢\u0006\u0004\b\u001f\u0010 J\u0017\u0010#\u001a\u00020\b2\u0006\u0010\"\u001a\u00020!H\u0007¢\u0006\u0004\b#\u0010$R\u0016\u0010&\u001a\u00020%8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b&\u0010'R\u0016\u0010\u0018\u001a\u00020\r8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0018\u0010(R\u0016\u0010*\u001a\u00020)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010+R\u0016\u0010-\u001a\u00020,8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b-\u0010.R6\u00102\u001a\"\u0012\b\u0012\u00060\u0002j\u0002`\u001c\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`1\u0012\u0004\u0012\u00020\u0006000/8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b2\u00103¨\u00066"}, d2 = {"Lcom/discord/stores/StoreGuildMemberRequester;", "Lcom/discord/stores/Store;", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "Lcom/discord/models/message/Message;", "messages", "", "requestForMessages", "(JLjava/lang/Iterable;)V", "Lcom/discord/primitives/UserId;", "userId", "", "guildMemberExists", "(JJ)Z", "", "userIds", "sendRequests", "(JLjava/util/List;)V", "queueRequest", "(JJ)V", "performQueuedRequests", "()V", "isConnected", "handleConnectionReady", "(Z)V", "handleConnectionOpen", "Lcom/discord/primitives/ChannelId;", "channelId", "", "handleLoadMessages", "(JLjava/util/Collection;)V", "Lcom/discord/api/guildmember/GuildMembersChunk;", "chunk", "handleGuildMembersChunk", "(Lcom/discord/api/guildmember/GuildMembersChunk;)V", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Z", "Lcom/discord/stores/StoreStream;", "collector", "Lcom/discord/stores/StoreStream;", "Lcom/discord/utilities/lazy/requester/GuildMemberRequestManager;", "requestManager", "Lcom/discord/utilities/lazy/requester/GuildMemberRequestManager;", "Ljava/util/TreeMap;", "", "Lcom/discord/primitives/MessageId;", "channelPendingMessages", "Ljava/util/TreeMap;", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildMemberRequester extends Store {
    private final StoreStream collector;
    private final Dispatcher dispatcher;
    private boolean isConnected;
    private final GuildMemberRequestManager requestManager = new GuildMemberRequestManager(new StoreGuildMemberRequester$requestManager$1(this), new StoreGuildMemberRequester$requestManager$2(this));
    private final TreeMap<Long, Map<Long, Message>> channelPendingMessages = new TreeMap<>();

    public StoreGuildMemberRequester(StoreStream storeStream, Dispatcher dispatcher) {
        m.checkNotNullParameter(storeStream, "collector");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        this.collector = storeStream;
        this.dispatcher = dispatcher;
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final boolean guildMemberExists(long j, long j2) {
        Map<Long, GuildMember> map = this.collector.getGuilds$app_productionGoogleRelease().getGuildMembersComputedInternal$app_productionGoogleRelease().get(Long.valueOf(j));
        return (map != null ? map.get(Long.valueOf(j2)) : null) != null;
    }

    @StoreThread
    private final void requestForMessages(long j, Iterable<Message> iterable) {
        for (Message message : iterable) {
            User author = message.getAuthor();
            if (author != null) {
                this.requestManager.queueRequest(j, author.i());
            }
            List<User> mentions = message.getMentions();
            if (mentions != null) {
                for (User user : mentions) {
                    this.requestManager.queueRequest(j, user.i());
                }
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void sendRequests(long j, List<Long> list) {
        StoreGatewayConnection.requestGuildMembers$default(this.collector.getGatewaySocket$app_productionGoogleRelease(), j, null, list, null, 2, null);
    }

    @StoreThread
    public final void handleConnectionOpen() {
        this.isConnected = true;
        this.requestManager.reset();
        for (Map.Entry<Long, Map<Long, Message>> entry : this.channelPendingMessages.entrySet()) {
            handleLoadMessages(entry.getKey().longValue(), entry.getValue().values());
        }
        this.channelPendingMessages.clear();
    }

    @StoreThread
    public final void handleConnectionReady(boolean z2) {
        this.isConnected = z2;
        if (z2) {
            this.requestManager.requestUnacknowledged();
        }
    }

    @StoreThread
    public final void handleGuildMembersChunk(GuildMembersChunk guildMembersChunk) {
        m.checkNotNullParameter(guildMembersChunk, "chunk");
        long a = guildMembersChunk.a();
        List<com.discord.api.guildmember.GuildMember> b2 = guildMembersChunk.b();
        ArrayList<User> arrayList = new ArrayList(o.collectionSizeOrDefault(b2, 10));
        for (com.discord.api.guildmember.GuildMember guildMember : b2) {
            arrayList.add(guildMember.m());
        }
        for (User user : arrayList) {
            this.requestManager.acknowledge(a, user.i());
        }
        List<Long> c = guildMembersChunk.c();
        if (c != null) {
            for (Number number : c) {
                this.requestManager.acknowledge(a, number.longValue());
            }
        }
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [java.lang.Object] */
    @StoreThread
    public final void handleLoadMessages(long j, Collection<Message> collection) {
        m.checkNotNullParameter(collection, "messages");
        if (this.isConnected) {
            Channel findChannelByIdInternal$app_productionGoogleRelease = this.collector.getChannels$app_productionGoogleRelease().findChannelByIdInternal$app_productionGoogleRelease(j);
            if (findChannelByIdInternal$app_productionGoogleRelease != null) {
                Long valueOf = Long.valueOf(findChannelByIdInternal$app_productionGoogleRelease.f());
                if (!(valueOf.longValue() > 0)) {
                    valueOf = null;
                }
                if (valueOf != null) {
                    requestForMessages(valueOf.longValue(), collection);
                    this.requestManager.flush();
                    return;
                }
                return;
            }
            return;
        }
        Map<Long, Message> map = this.channelPendingMessages.get(Long.valueOf(j));
        if (map == null) {
            map = new HashMap<>(collection.size());
        }
        for (?? r1 : collection) {
            map.put(Long.valueOf(((Message) r1).getId()), r1);
        }
        this.channelPendingMessages.put(Long.valueOf(j), map);
    }

    public final void performQueuedRequests() {
        this.dispatcher.schedule(new StoreGuildMemberRequester$performQueuedRequests$1(this));
    }

    public final void queueRequest(long j, long j2) {
        this.dispatcher.schedule(new StoreGuildMemberRequester$queueRequest$1(this, j, j2));
    }
}
