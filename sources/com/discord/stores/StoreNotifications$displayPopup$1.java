package com.discord.stores;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import com.discord.api.channel.Channel;
import com.discord.models.message.Message;
import com.discord.utilities.intent.IntentUtils;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreNotifications.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "view", "", "invoke", "(Landroid/view/View;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreNotifications$displayPopup$1 extends o implements Function1<View, Unit> {
    public final /* synthetic */ Channel $channel;
    public final /* synthetic */ Message $message;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreNotifications$displayPopup$1(Channel channel, Message message) {
        super(1);
        this.$channel = channel;
        this.$message = message;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(View view) {
        invoke2(view);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(View view) {
        m.checkNotNullParameter(view, "view");
        Intent selectChannel = IntentUtils.RouteBuilders.selectChannel(this.$channel.h(), this.$channel.f(), Long.valueOf(this.$message.getId()));
        selectChannel.putExtra("com.discord.intent.ORIGIN_SOURCE", "com.discord.intent.ORIGIN_NOTIF_INAPP");
        IntentUtils intentUtils = IntentUtils.INSTANCE;
        Context context = view.getContext();
        m.checkNotNullExpressionValue(context, "view.context");
        intentUtils.consumeExternalRoutingIntent(selectChannel, context);
    }
}
