package com.discord.stores;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.Context;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.app.AppLog;
import com.discord.models.domain.ModelPayload;
import com.discord.models.guild.Guild;
import com.discord.restapi.RestAPIInterface;
import com.discord.restapi.RestAPIParams;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.discord.widgets.guilds.join.GuildJoinHelperKt;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.BehaviorSubject;
/* compiled from: StoreLurking.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0092\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0016\n\u0002\u0018\u0002\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 Y2\u00020\u0001:\u0003YZ[B\u001f\u0012\u0006\u0010N\u001a\u00020M\u0012\u0006\u0010Q\u001a\u00020P\u0012\u0006\u0010F\u001a\u00020E¢\u0006\u0004\bW\u0010XJ{\u0010\u0011\u001a\u00020\u000b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u000e\u0010\u0006\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00052\b\b\u0002\u0010\b\u001a\u00020\u00072(\b\u0002\u0010\f\u001a\"\u0012\u0004\u0012\u00020\n\u0012\f\u0012\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0005\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u000b0\t2\u000e\b\u0002\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000b0\r2\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u000fH\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u001b\u0010\u0013\u001a\u00020\u000b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0003¢\u0006\u0004\b\u0013\u0010\u0014J;\u0010\u0017\u001a\u00020\u000b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u000e\b\u0002\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u000b0\r2\u000e\b\u0002\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u000b0\rH\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u001d\u0010\u001b\u001a\u0012\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\u001a0\u0019¢\u0006\u0004\b\u001b\u0010\u001cJ\u0017\u0010\u001d\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\u001a¢\u0006\u0004\b\u001d\u0010\u001eJ\u0017\u0010\"\u001a\u00020\u00072\u0006\u0010\u001f\u001a\u00020\nH\u0001¢\u0006\u0004\b \u0010!J\u0017\u0010\"\u001a\u00020\u00072\u0006\u0010\u001f\u001a\u00020#H\u0001¢\u0006\u0004\b \u0010$J\u001b\u0010\"\u001a\u00020\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0001¢\u0006\u0004\b \u0010%J\u001f\u0010&\u001a\b\u0012\u0004\u0012\u00020\u00070\u00192\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b&\u0010'J\u0017\u0010,\u001a\u00020\u000b2\u0006\u0010)\u001a\u00020(H\u0001¢\u0006\u0004\b*\u0010+J\u0017\u0010/\u001a\u00020\u000b2\u0006\u0010\u001f\u001a\u00020#H\u0001¢\u0006\u0004\b-\u0010.J\u0017\u00101\u001a\u00020\u000b2\u0006\u0010\u001f\u001a\u00020#H\u0001¢\u0006\u0004\b0\u0010.J\u001b\u00103\u001a\u00020\u000b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0001¢\u0006\u0004\b2\u0010\u0014J\u001b\u00105\u001a\u00020\u000b2\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u0005H\u0001¢\u0006\u0004\b4\u0010\u0014Jm\u00106\u001a\u00020\u000b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u000e\u0010\u0006\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00052\b\b\u0002\u0010\b\u001a\u00020\u00072(\b\u0002\u0010\f\u001a\"\u0012\u0004\u0012\u00020\n\u0012\f\u0012\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0005\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u000b0\t2\u000e\b\u0002\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000b0\r¢\u0006\u0004\b6\u00107J5\u00108\u001a\u00020\u000b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u000e\u0010\u0006\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00052\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u000f¢\u0006\u0004\b8\u00109J\u0019\u0010;\u001a\u00020\u000b2\n\u0010:\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b;\u0010\u0014J5\u0010<\u001a\u00020\u000b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u000b0\r2\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u000b0\r¢\u0006\u0004\b<\u0010\u0018J!\u0010=\u001a\u00020\u000b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b=\u0010>Rz\u0010C\u001af\u0012,\u0012*\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020A B*\u0014\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020A\u0018\u00010@0@ B*2\u0012,\u0012*\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020A B*\u0014\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020A\u0018\u00010@0@\u0018\u00010?0?8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bC\u0010DR\u0016\u0010F\u001a\u00020E8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bF\u0010GR\u0018\u0010I\u001a\u0004\u0018\u00010H8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bI\u0010JR&\u0010K\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020A0@8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bK\u0010LR\u0016\u0010N\u001a\u00020M8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bN\u0010OR\u0016\u0010Q\u001a\u00020P8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bQ\u0010RR\u001e\u0010U\u001a\n\u0018\u00010Sj\u0004\u0018\u0001`T8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bU\u0010V¨\u0006\\"}, d2 = {"Lcom/discord/stores/StoreLurking;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/ChannelId;", "channelId", "", "navigateToGuild", "Lkotlin/Function3;", "Lcom/discord/models/guild/Guild;", "", "onGuildLurked", "Lkotlin/Function0;", "onErrorLurking", "Landroid/content/Context;", "context", "startLurkingInternal", "(JLjava/lang/Long;ZLkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function0;Landroid/content/Context;)V", "stopLurkingInternal", "(J)V", "onSuccess", "onFailure", "postLeaveGuild", "(JLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V", "Lrx/Observable;", "", "getLurkingGuildIds", "()Lrx/Observable;", "getLurkingGuildIdsSync", "()Ljava/util/Set;", "guild", "isLurking$app_productionGoogleRelease", "(Lcom/discord/models/guild/Guild;)Z", "isLurking", "Lcom/discord/api/guild/Guild;", "(Lcom/discord/api/guild/Guild;)Z", "(J)Z", "isLurkingObs", "(J)Lrx/Observable;", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen$app_productionGoogleRelease", "(Lcom/discord/models/domain/ModelPayload;)V", "handleConnectionOpen", "handleGuildAdd$app_productionGoogleRelease", "(Lcom/discord/api/guild/Guild;)V", "handleGuildAdd", "handleGuildRemove$app_productionGoogleRelease", "handleGuildRemove", "handleGuildSelected$app_productionGoogleRelease", "handleGuildSelected", "handleVoiceChannelSelected$app_productionGoogleRelease", "handleVoiceChannelSelected", "startLurking", "(JLjava/lang/Long;ZLkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function0;)V", "startLurkingAndNavigate", "(JLjava/lang/Long;Landroid/content/Context;)V", "currentlyLurkedGuildId", "removeOldLurkingGuilds", "stopLurking", "postJoinGuildAsMember", "(JLandroid/content/Context;)V", "Lrx/subjects/BehaviorSubject;", "", "Lcom/discord/stores/StoreLurking$LurkContext;", "kotlin.jvm.PlatformType", "lurkedGuildsSubject", "Lrx/subjects/BehaviorSubject;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/stores/StoreLurking$LurkRequest;", "lurkRequest", "Lcom/discord/stores/StoreLurking$LurkRequest;", "lurkedGuilds", "Ljava/util/Map;", "Lcom/discord/stores/StoreStream;", "stream", "Lcom/discord/stores/StoreStream;", "Lcom/discord/stores/StoreGuilds;", "guildsStore", "Lcom/discord/stores/StoreGuilds;", "", "Lcom/discord/primitives/SessionId;", "sessionId", "Ljava/lang/String;", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/Dispatcher;)V", "Companion", "LurkContext", "LurkRequest", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreLurking {
    public static final Companion Companion = new Companion(null);
    private final Dispatcher dispatcher;
    private final StoreGuilds guildsStore;
    private LurkRequest lurkRequest;
    private final Map<Long, LurkContext> lurkedGuilds = new LinkedHashMap();
    private final BehaviorSubject<Map<Long, LurkContext>> lurkedGuildsSubject = BehaviorSubject.l0(new LinkedHashMap());
    private String sessionId;
    private final StoreStream stream;

    /* compiled from: StoreLurking.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fJ5\u0010\t\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0010\u0010\b\u001a\f\u0012\b\u0012\u00060\u0004j\u0002`\u00050\u0007H\u0002¢\u0006\u0004\b\t\u0010\n¨\u0006\r"}, d2 = {"Lcom/discord/stores/StoreLurking$Companion;", "", "", "isJoined", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "lurkingGuildIds", "isLurking", "(ZJLjava/util/Set;)Z", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final boolean isLurking(boolean z2, long j, Set<Long> set) {
            return !z2 && set.contains(Long.valueOf(j));
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreLurking.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\f\b\u0082\b\u0018\u00002\u00020\u0001B\u001b\u0012\n\u0010\t\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\n\u001a\u00020\u0006¢\u0006\u0004\b\u001a\u0010\u001bJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ(\u0010\u000b\u001a\u00020\u00002\f\b\u0002\u0010\t\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\n\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0014\u001a\u00020\u00062\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u001d\u0010\t\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0016\u001a\u0004\b\u0017\u0010\u0005R\u0019\u0010\n\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0018\u001a\u0004\b\u0019\u0010\b¨\u0006\u001c"}, d2 = {"Lcom/discord/stores/StoreLurking$LurkContext;", "", "", "Lcom/discord/primitives/GuildId;", "component1", "()J", "", "component2", "()Z", "guildId", "shouldNavigate", "copy", "(JZ)Lcom/discord/stores/StoreLurking$LurkContext;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "J", "getGuildId", "Z", "getShouldNavigate", HookHelper.constructorName, "(JZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class LurkContext {
        private final long guildId;
        private final boolean shouldNavigate;

        public LurkContext(long j, boolean z2) {
            this.guildId = j;
            this.shouldNavigate = z2;
        }

        public static /* synthetic */ LurkContext copy$default(LurkContext lurkContext, long j, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                j = lurkContext.guildId;
            }
            if ((i & 2) != 0) {
                z2 = lurkContext.shouldNavigate;
            }
            return lurkContext.copy(j, z2);
        }

        public final long component1() {
            return this.guildId;
        }

        public final boolean component2() {
            return this.shouldNavigate;
        }

        public final LurkContext copy(long j, boolean z2) {
            return new LurkContext(j, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof LurkContext)) {
                return false;
            }
            LurkContext lurkContext = (LurkContext) obj;
            return this.guildId == lurkContext.guildId && this.shouldNavigate == lurkContext.shouldNavigate;
        }

        public final long getGuildId() {
            return this.guildId;
        }

        public final boolean getShouldNavigate() {
            return this.shouldNavigate;
        }

        public int hashCode() {
            int a = b.a(this.guildId) * 31;
            boolean z2 = this.shouldNavigate;
            if (z2) {
                z2 = true;
            }
            int i = z2 ? 1 : 0;
            int i2 = z2 ? 1 : 0;
            return a + i;
        }

        public String toString() {
            StringBuilder R = a.R("LurkContext(guildId=");
            R.append(this.guildId);
            R.append(", shouldNavigate=");
            return a.M(R, this.shouldNavigate, ")");
        }
    }

    /* compiled from: StoreLurking.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0082\b\u0018\u00002\u00020\u0001B%\u0012\n\u0010\t\u001a\u00060\u0002j\u0002`\u0003\u0012\u0010\b\u0002\u0010\n\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0006¢\u0006\u0004\b\u001b\u0010\u001cJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0018\u0010\u0007\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ0\u0010\u000b\u001a\u00020\u00002\f\b\u0002\u0010\t\u001a\u00060\u0002j\u0002`\u00032\u0010\b\u0002\u0010\n\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0006HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R!\u0010\n\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0017\u001a\u0004\b\u0018\u0010\bR\u001d\u0010\t\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0019\u001a\u0004\b\u001a\u0010\u0005¨\u0006\u001d"}, d2 = {"Lcom/discord/stores/StoreLurking$LurkRequest;", "", "", "Lcom/discord/primitives/GuildId;", "component1", "()J", "Lcom/discord/primitives/ChannelId;", "component2", "()Ljava/lang/Long;", "guildId", "channelId", "copy", "(JLjava/lang/Long;)Lcom/discord/stores/StoreLurking$LurkRequest;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Long;", "getChannelId", "J", "getGuildId", HookHelper.constructorName, "(JLjava/lang/Long;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class LurkRequest {
        private final Long channelId;
        private final long guildId;

        public LurkRequest(long j, Long l) {
            this.guildId = j;
            this.channelId = l;
        }

        public static /* synthetic */ LurkRequest copy$default(LurkRequest lurkRequest, long j, Long l, int i, Object obj) {
            if ((i & 1) != 0) {
                j = lurkRequest.guildId;
            }
            if ((i & 2) != 0) {
                l = lurkRequest.channelId;
            }
            return lurkRequest.copy(j, l);
        }

        public final long component1() {
            return this.guildId;
        }

        public final Long component2() {
            return this.channelId;
        }

        public final LurkRequest copy(long j, Long l) {
            return new LurkRequest(j, l);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof LurkRequest)) {
                return false;
            }
            LurkRequest lurkRequest = (LurkRequest) obj;
            return this.guildId == lurkRequest.guildId && m.areEqual(this.channelId, lurkRequest.channelId);
        }

        public final Long getChannelId() {
            return this.channelId;
        }

        public final long getGuildId() {
            return this.guildId;
        }

        public int hashCode() {
            int a = b.a(this.guildId) * 31;
            Long l = this.channelId;
            return a + (l != null ? l.hashCode() : 0);
        }

        public String toString() {
            StringBuilder R = a.R("LurkRequest(guildId=");
            R.append(this.guildId);
            R.append(", channelId=");
            return a.F(R, this.channelId, ")");
        }

        public /* synthetic */ LurkRequest(long j, Long l, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(j, (i & 2) != 0 ? null : l);
        }
    }

    public StoreLurking(StoreStream storeStream, StoreGuilds storeGuilds, Dispatcher dispatcher) {
        m.checkNotNullParameter(storeStream, "stream");
        m.checkNotNullParameter(storeGuilds, "guildsStore");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        this.stream = storeStream;
        this.guildsStore = storeGuilds;
        this.dispatcher = dispatcher;
    }

    private final void postLeaveGuild(long j, Function0<Unit> function0, Function0<Unit> function02) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().leaveGuild(j, new RestAPIParams.LeaveGuildBody(true)), false, 1, null), StoreLurking.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new StoreLurking$postLeaveGuild$3(function02), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreLurking$postLeaveGuild$4(function0));
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void postLeaveGuild$default(StoreLurking storeLurking, long j, Function0 function0, Function0 function02, int i, Object obj) {
        if ((i & 2) != 0) {
            function0 = StoreLurking$postLeaveGuild$1.INSTANCE;
        }
        if ((i & 4) != 0) {
            function02 = StoreLurking$postLeaveGuild$2.INSTANCE;
        }
        storeLurking.postLeaveGuild(j, function0, function02);
    }

    public static /* synthetic */ void startLurking$default(StoreLurking storeLurking, long j, Long l, boolean z2, Function3 function3, Function0 function0, int i, Object obj) {
        boolean z3 = (i & 4) != 0 ? true : z2;
        StoreLurking$startLurking$1 storeLurking$startLurking$1 = function3;
        if ((i & 8) != 0) {
            storeLurking$startLurking$1 = StoreLurking$startLurking$1.INSTANCE;
        }
        Function3 function32 = storeLurking$startLurking$1;
        StoreLurking$startLurking$2 storeLurking$startLurking$2 = function0;
        if ((i & 16) != 0) {
            storeLurking$startLurking$2 = StoreLurking$startLurking$2.INSTANCE;
        }
        storeLurking.startLurking(j, l, z3, function32, storeLurking$startLurking$2);
    }

    public static /* synthetic */ void startLurkingAndNavigate$default(StoreLurking storeLurking, long j, Long l, Context context, int i, Object obj) {
        if ((i & 4) != 0) {
            context = null;
        }
        storeLurking.startLurkingAndNavigate(j, l, context);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void startLurkingInternal(long j, Long l, boolean z2, Function3<? super Guild, ? super Long, ? super Boolean, Unit> function3, Function0<Unit> function0, Context context) {
        if (this.sessionId == null) {
            AppLog appLog = AppLog.g;
            Logger.w$default(appLog, "Cannot lurk " + j + MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR + l + " with no session", null, 2, null);
            function0.invoke();
        } else if (j != 0 || j == -1) {
            Guild guild = this.stream.getGuilds$app_productionGoogleRelease().getGuildsInternal$app_productionGoogleRelease().get(Long.valueOf(j));
            if (guild == null || isLurking$app_productionGoogleRelease(guild)) {
                this.lurkedGuilds.put(Long.valueOf(j), new LurkContext(j, z2));
                this.lurkedGuildsSubject.onNext(this.lurkedGuilds);
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(ObservableExtensionsKt.restSubscribeOn$default(RestAPIInterface.DefaultImpls.joinGuild$default(RestAPI.Companion.getApi(), j, true, this.sessionId, null, new RestAPIParams.InviteCode(null), null, 32, null), false, 1, null)), StoreLurking.class, (r18 & 2) != 0 ? null : context, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new StoreLurking$startLurkingInternal$3(this, j), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreLurking$startLurkingInternal$4(this, j, function3, l));
                return;
            }
            function3.invoke(guild, l, Boolean.TRUE);
        } else {
            AppLog appLog2 = AppLog.g;
            Logger.w$default(appLog2, "Invalid ids for lurking " + j + MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR + l, null, 2, null);
            function0.invoke();
        }
    }

    @StoreThread
    private final void stopLurkingInternal(long j) {
        if (this.lurkedGuilds.remove(Long.valueOf(j)) != null) {
            this.lurkedGuildsSubject.onNext(this.lurkedGuilds);
        }
    }

    public final Observable<Set<Long>> getLurkingGuildIds() {
        Observable F = this.lurkedGuildsSubject.F(StoreLurking$getLurkingGuildIds$1.INSTANCE);
        m.checkNotNullExpressionValue(F, "lurkedGuildsSubject.map { it.keys }");
        return F;
    }

    public final Set<Long> getLurkingGuildIdsSync() {
        return this.lurkedGuilds.keySet();
    }

    @StoreThread
    public final void handleConnectionOpen$app_productionGoogleRelease(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        this.sessionId = modelPayload.getSessionId();
        LurkRequest lurkRequest = this.lurkRequest;
        if (lurkRequest != null) {
            AppLog.i("Processing lurk request from app link");
            startLurkingAndNavigate$default(this, lurkRequest.getGuildId(), lurkRequest.getChannelId(), null, 4, null);
        }
        this.lurkRequest = null;
    }

    @StoreThread
    public final void handleGuildAdd$app_productionGoogleRelease(com.discord.api.guild.Guild guild) {
        LurkContext lurkContext;
        m.checkNotNullParameter(guild, "guild");
        if (!this.lurkedGuilds.keySet().contains(Long.valueOf(guild.r()))) {
            return;
        }
        if (guild.s() == null && (lurkContext = this.lurkedGuilds.get(Long.valueOf(guild.r()))) != null && lurkContext.getShouldNavigate()) {
            this.stream.getGuildSelected$app_productionGoogleRelease().handleGuildSelected(guild.r());
        } else if (guild.s() != null) {
            stopLurkingInternal(guild.r());
        }
    }

    @StoreThread
    public final void handleGuildRemove$app_productionGoogleRelease(com.discord.api.guild.Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        stopLurkingInternal(guild.r());
    }

    @StoreThread
    public final void handleGuildSelected$app_productionGoogleRelease(long j) {
        LurkRequest lurkRequest = this.lurkRequest;
        if ((lurkRequest == null || j != lurkRequest.getGuildId()) && this.sessionId != null) {
            this.lurkRequest = null;
        }
        removeOldLurkingGuilds(j);
    }

    @StoreThread
    public final void handleVoiceChannelSelected$app_productionGoogleRelease(long j) {
        Channel channel = this.stream.getChannels$app_productionGoogleRelease().getChannel(j);
        Set<Long> keySet = this.lurkedGuilds.keySet();
        ArrayList<Number> arrayList = new ArrayList();
        for (Object obj : keySet) {
            if (channel == null || ((Number) obj).longValue() != channel.f()) {
                arrayList.add(obj);
            }
        }
        for (Number number : arrayList) {
            long longValue = number.longValue();
            if (longValue != this.stream.getGuildSelected$app_productionGoogleRelease().getSelectedGuildIdInternal$app_productionGoogleRelease()) {
                postLeaveGuild$default(this, longValue, null, null, 6, null);
            }
        }
    }

    @StoreThread
    public final boolean isLurking$app_productionGoogleRelease(Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        return Companion.isLurking(guild.getJoinedAt() != null, guild.getId(), this.lurkedGuilds.keySet());
    }

    public final Observable<Boolean> isLurkingObs(long j) {
        Observable<Boolean> q = Observable.j(getLurkingGuildIds(), this.stream.getGuilds$app_productionGoogleRelease().observeGuild(j), StoreLurking$isLurkingObs$1.INSTANCE).q();
        m.checkNotNullExpressionValue(q, "Observable\n          .co…  .distinctUntilChanged()");
        return q;
    }

    public final void postJoinGuildAsMember(long j, Context context) {
        m.checkNotNullParameter(context, "context");
        GuildJoinHelperKt.joinGuild$default(context, j, false, this.sessionId, null, null, StoreLurking.class, null, null, null, StoreLurking$postJoinGuildAsMember$1.INSTANCE, 944, null);
    }

    public final void removeOldLurkingGuilds(long j) {
        Channel channel = this.stream.getChannels$app_productionGoogleRelease().getChannel(this.stream.getVoiceChannelSelected$app_productionGoogleRelease().getSelectedVoiceChannelId());
        Long valueOf = channel != null ? Long.valueOf(channel.f()) : null;
        Set<Long> keySet = this.lurkedGuilds.keySet();
        ArrayList<Number> arrayList = new ArrayList();
        for (Object obj : keySet) {
            long longValue = ((Number) obj).longValue();
            if (longValue != j && (valueOf == null || longValue != valueOf.longValue())) {
                arrayList.add(obj);
            }
        }
        ArrayList<Guild> arrayList2 = new ArrayList();
        for (Number number : arrayList) {
            Guild guild = this.guildsStore.getGuildsInternal$app_productionGoogleRelease().get(Long.valueOf(number.longValue()));
            if (guild != null) {
                arrayList2.add(guild);
            }
        }
        for (Guild guild2 : arrayList2) {
            postLeaveGuild$default(this, guild2.getId(), null, null, 6, null);
        }
    }

    public final void startLurking(long j, Long l, boolean z2, Function3<? super Guild, ? super Long, ? super Boolean, Unit> function3, Function0<Unit> function0) {
        m.checkNotNullParameter(function3, "onGuildLurked");
        m.checkNotNullParameter(function0, "onErrorLurking");
        this.dispatcher.schedule(new StoreLurking$startLurking$3(this, j, l, z2, function3, function0));
    }

    public final void startLurkingAndNavigate(long j, Long l, Context context) {
        this.dispatcher.schedule(new StoreLurking$startLurkingAndNavigate$1(this, j, l, context));
    }

    public final void stopLurking(long j, Function0<Unit> function0, Function0<Unit> function02) {
        m.checkNotNullParameter(function0, "onSuccess");
        m.checkNotNullParameter(function02, "onFailure");
        postLeaveGuild(j, function0, function02);
    }

    @StoreThread
    public final boolean isLurking$app_productionGoogleRelease(com.discord.api.guild.Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        return Companion.isLurking(guild.s() != null, guild.r(), this.lurkedGuilds.keySet());
    }

    @StoreThread
    public final boolean isLurking$app_productionGoogleRelease(long j) {
        return Companion.isLurking(false, j, this.lurkedGuilds.keySet());
    }
}
