package com.discord.stores;

import d0.f0.q;
import d0.g0.w;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.functions.Function2;
/* compiled from: StoreCollapsedChannelCategories.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010#\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\n\u001a\u001c\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0004j\u0002`\u00070\u00060\u00032\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0000H\n¢\u0006\u0004\b\b\u0010\t"}, d2 = {"", "key", "value", "Lkotlin/Pair;", "", "Lcom/discord/primitives/GuildId;", "", "Lcom/discord/primitives/ChannelId;", "invoke", "(Ljava/lang/String;Ljava/lang/String;)Lkotlin/Pair;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreCollapsedChannelCategories$Companion$fromCache$1 extends o implements Function2<String, String, Pair<? extends Long, ? extends Set<Long>>> {
    public static final StoreCollapsedChannelCategories$Companion$fromCache$1 INSTANCE = new StoreCollapsedChannelCategories$Companion$fromCache$1();

    public StoreCollapsedChannelCategories$Companion$fromCache$1() {
        super(2);
    }

    public final Pair<Long, Set<Long>> invoke(String str, String str2) {
        m.checkNotNullParameter(str, "key");
        m.checkNotNullParameter(str2, "value");
        long parseLong = Long.parseLong(str);
        return d0.o.to(Long.valueOf(parseLong), q.toHashSet(q.map(w.splitToSequence$default(str2, new String[]{","}, false, 0, 6, null), StoreCollapsedChannelCategories$Companion$fromCache$1$guildChannelIdsCollapsed$1.INSTANCE)));
    }
}
