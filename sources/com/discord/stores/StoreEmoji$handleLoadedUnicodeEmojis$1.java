package com.discord.stores;

import com.discord.models.domain.emoji.ModelEmojiUnicode;
import d0.g0.t;
import d0.g0.w;
import d0.z.d.m;
import d0.z.d.o;
import java.util.HashMap;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreEmoji.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/emoji/ModelEmojiUnicode;", "emoji", "", "invoke", "(Lcom/discord/models/domain/emoji/ModelEmojiUnicode;)V", "indexEmoji"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreEmoji$handleLoadedUnicodeEmojis$1 extends o implements Function1<ModelEmojiUnicode, Unit> {
    public final /* synthetic */ HashMap $unicodeEmojiSurrogateMap;
    public final /* synthetic */ HashMap $unicodeEmojisNamesMap;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreEmoji$handleLoadedUnicodeEmojis$1(HashMap hashMap, HashMap hashMap2) {
        super(1);
        this.$unicodeEmojiSurrogateMap = hashMap;
        this.$unicodeEmojisNamesMap = hashMap2;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ModelEmojiUnicode modelEmojiUnicode) {
        invoke2(modelEmojiUnicode);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ModelEmojiUnicode modelEmojiUnicode) {
        m.checkNotNullParameter(modelEmojiUnicode, "emoji");
        HashMap hashMap = this.$unicodeEmojiSurrogateMap;
        String surrogates = modelEmojiUnicode.getSurrogates();
        m.checkNotNullExpressionValue(surrogates, "emoji.surrogates");
        hashMap.put(surrogates, modelEmojiUnicode);
        List<String> names = modelEmojiUnicode.getNames();
        m.checkNotNullExpressionValue(names, "emoji\n          .names");
        for (String str : names) {
            HashMap hashMap2 = this.$unicodeEmojisNamesMap;
            m.checkNotNullExpressionValue(str, "emojiName");
            hashMap2.put(str, modelEmojiUnicode);
        }
        String surrogates2 = modelEmojiUnicode.getSurrogates();
        m.checkNotNullExpressionValue(surrogates2, "emoji.surrogates");
        if (w.indexOf$default((CharSequence) surrogates2, (char) 8205, 0, false, 6, (Object) null) < 0) {
            String surrogates3 = modelEmojiUnicode.getSurrogates();
            m.checkNotNullExpressionValue(surrogates3, "emoji.surrogates");
            String replace$default = t.replace$default(surrogates3, "️", "", false, 4, (Object) null);
            if (!m.areEqual(replace$default, modelEmojiUnicode.getSurrogates())) {
                this.$unicodeEmojiSurrogateMap.put(replace$default, modelEmojiUnicode);
            }
        }
    }
}
