package com.discord.stores;

import com.discord.stores.StoreMediaSettings;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreMediaEngine.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "voiceConfig", "", "invoke", "(Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMediaEngine$setupMediaEngineSettingsSubscription$1 extends o implements Function1<StoreMediaSettings.VoiceConfiguration, Unit> {
    public final /* synthetic */ StoreMediaEngine this$0;

    /* compiled from: StoreMediaEngine.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreMediaEngine$setupMediaEngineSettingsSubscription$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ StoreMediaSettings.VoiceConfiguration $voiceConfig;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(StoreMediaSettings.VoiceConfiguration voiceConfiguration) {
            super(0);
            this.$voiceConfig = voiceConfiguration;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            StoreMediaEngine$setupMediaEngineSettingsSubscription$1.this.this$0.handleVoiceConfigChanged(this.$voiceConfig);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreMediaEngine$setupMediaEngineSettingsSubscription$1(StoreMediaEngine storeMediaEngine) {
        super(1);
        this.this$0 = storeMediaEngine;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(StoreMediaSettings.VoiceConfiguration voiceConfiguration) {
        invoke2(voiceConfiguration);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(StoreMediaSettings.VoiceConfiguration voiceConfiguration) {
        Dispatcher dispatcher;
        m.checkNotNullParameter(voiceConfiguration, "voiceConfig");
        dispatcher = this.this$0.dispatcher;
        dispatcher.schedule(new AnonymousClass1(voiceConfiguration));
    }
}
