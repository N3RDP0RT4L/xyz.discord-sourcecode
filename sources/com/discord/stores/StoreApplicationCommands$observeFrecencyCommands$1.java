package com.discord.stores;

import com.discord.models.commands.ApplicationCommand;
import com.discord.models.experiments.domain.Experiment;
import d0.t.n;
import d0.t.u;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreApplicationCommands.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"", "Lcom/discord/models/commands/ApplicationCommand;", "invoke", "()Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreApplicationCommands$observeFrecencyCommands$1 extends o implements Function0<List<? extends ApplicationCommand>> {
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ StoreApplicationCommands this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreApplicationCommands$observeFrecencyCommands$1(StoreApplicationCommands storeApplicationCommands, long j) {
        super(0);
        this.this$0 = storeApplicationCommands;
        this.$guildId = j;
    }

    @Override // kotlin.jvm.functions.Function0
    public final List<? extends ApplicationCommand> invoke() {
        StoreExperiments storeExperiments;
        storeExperiments = this.this$0.storeExperiments;
        Experiment userExperiment = storeExperiments.getUserExperiment("2021-09_android_app_commands_frecency", false);
        if (userExperiment == null || userExperiment.getBucket() != 1) {
            return n.emptyList();
        }
        return u.take(this.this$0.getFrecencyCommands(this.$guildId), 5);
    }
}
