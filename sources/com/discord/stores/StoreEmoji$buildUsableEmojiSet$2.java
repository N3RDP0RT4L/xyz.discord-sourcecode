package com.discord.stores;

import com.discord.stores.StoreEmoji;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreEmoji.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000b\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"", "isExternalEmoji", "invoke", "(Z)Z", "isExternalEmojiRestricted"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreEmoji$buildUsableEmojiSet$2 extends o implements Function1<Boolean, Boolean> {
    public final /* synthetic */ StoreEmoji.EmojiContext $emojiContext;
    public final /* synthetic */ boolean $hasExternalEmojiPermission;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreEmoji$buildUsableEmojiSet$2(boolean z2, StoreEmoji.EmojiContext emojiContext) {
        super(1);
        this.$hasExternalEmojiPermission = z2;
        this.$emojiContext = emojiContext;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Boolean invoke(Boolean bool) {
        return Boolean.valueOf(invoke(bool.booleanValue()));
    }

    public final boolean invoke(boolean z2) {
        StoreEmoji.EmojiContext emojiContext = this.$emojiContext;
        return ((emojiContext instanceof StoreEmoji.EmojiContext.Chat) || (emojiContext instanceof StoreEmoji.EmojiContext.Guild)) && z2 && !this.$hasExternalEmojiPermission;
    }
}
