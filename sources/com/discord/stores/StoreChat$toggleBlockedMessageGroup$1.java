package com.discord.stores;

import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreChat.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreChat$toggleBlockedMessageGroup$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $messageId;
    public final /* synthetic */ StoreChat this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreChat$toggleBlockedMessageGroup$1(StoreChat storeChat, long j) {
        super(0);
        this.this$0 = storeChat;
        this.$messageId = j;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        List list;
        List list2;
        List list3;
        list = this.this$0.expandedBlockedMessageGroups;
        if (list.contains(Long.valueOf(this.$messageId))) {
            list3 = this.this$0.expandedBlockedMessageGroups;
            list3.remove(Long.valueOf(this.$messageId));
        } else {
            list2 = this.this$0.expandedBlockedMessageGroups;
            list2.add(Long.valueOf(this.$messageId));
        }
        this.this$0.markChanged();
    }
}
