package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.utilities.frecency.FrecencyTracker;
import com.discord.utilities.media.MediaFrecencyTracker;
import d0.t.o;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
/* compiled from: StoreStickers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\b\u001a\u001e\u0012\b\u0012\u00060\u0004j\u0002`\u0005 \u0001*\u000e\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Lcom/discord/utilities/media/MediaFrecencyTracker;", "kotlin.jvm.PlatformType", "it", "", "", "Lcom/discord/primitives/StickerId;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/utilities/media/MediaFrecencyTracker;)Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStickers$observeFrequentlyUsedStickerIds$1<T, R> implements b<MediaFrecencyTracker, List<? extends Long>> {
    public static final StoreStickers$observeFrequentlyUsedStickerIds$1 INSTANCE = new StoreStickers$observeFrequentlyUsedStickerIds$1();

    public final List<Long> call(MediaFrecencyTracker mediaFrecencyTracker) {
        Collection<String> sortedKeys$default = FrecencyTracker.getSortedKeys$default(mediaFrecencyTracker, 0L, 1, null);
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(sortedKeys$default, 10));
        for (String str : sortedKeys$default) {
            arrayList.add(Long.valueOf(Long.parseLong(str)));
        }
        return arrayList;
    }
}
