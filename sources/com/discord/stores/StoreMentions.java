package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guild.Guild;
import com.discord.api.guildmember.GuildMember;
import com.discord.api.permission.Permission;
import com.discord.api.thread.ThreadMembersUpdate;
import com.discord.api.user.User;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.models.domain.ModelMessageDelete;
import com.discord.models.domain.ModelNotificationSettings;
import com.discord.models.domain.ModelPayload;
import com.discord.models.domain.ModelReadState;
import com.discord.models.domain.ModelUserRelationship;
import com.discord.models.message.Message;
import com.discord.models.thread.dto.ModelThreadListSync;
import com.discord.stores.StoreMessageAck;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.persister.Persister;
import d0.t.h0;
import d0.t.o;
import d0.t.r;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreMentions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0088\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001BA\u0012\u0006\u0010j\u001a\u00020i\u0012\u0006\u0010s\u001a\u00020r\u0012\u0006\u0010Q\u001a\u00020P\u0012\u0006\u0010T\u001a\u00020S\u0012\u0006\u0010c\u001a\u00020b\u0012\u0006\u0010Z\u001a\u00020Y\u0012\b\b\u0002\u0010W\u001a\u00020V¢\u0006\u0004\bx\u0010yJ\u001b\u0010\u0006\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u001f\u0010\n\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\t0\bH\u0003¢\u0006\u0004\b\n\u0010\u000bJ]\u0010\u0016\u001a\u00020\u00142\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\b\u0010\r\u001a\u0004\u0018\u00010\f2\b\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\u000e\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u00102\u0012\u0010\u0013\u001a\u000e\u0012\b\u0012\u00060\u0002j\u0002`\u0012\u0018\u00010\u00102\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014H\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0013\u0010\u0016\u001a\u00020\u0014*\u00020\u0018H\u0003¢\u0006\u0004\b\u0016\u0010\u0019J\u0013\u0010\u0016\u001a\u00020\u0014*\u00020\u001aH\u0003¢\u0006\u0004\b\u0016\u0010\u001bJ\u001b\u0010\u001c\u001a\u00020\u00142\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0003¢\u0006\u0004\b\u001c\u0010\u001dJ\u001d\u0010\u001e\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\t0\b¢\u0006\u0004\b\u001e\u0010\u000bJ\u0013\u0010 \u001a\b\u0012\u0004\u0012\u00020\t0\u001f¢\u0006\u0004\b \u0010!J#\u0010\"\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\t0\b0\u001f¢\u0006\u0004\b\"\u0010!J\u0017\u0010%\u001a\u00020\u00052\u0006\u0010$\u001a\u00020#H\u0007¢\u0006\u0004\b%\u0010&J\u001b\u0010(\u001a\u00020\u00052\n\u0010'\u001a\u00060\u0002j\u0002`\u0003H\u0007¢\u0006\u0004\b(\u0010\u0007J\u0017\u0010+\u001a\u00020\u00052\u0006\u0010*\u001a\u00020)H\u0007¢\u0006\u0004\b+\u0010,J\u0017\u0010.\u001a\u00020\u00052\u0006\u0010$\u001a\u00020-H\u0007¢\u0006\u0004\b.\u0010/J\u0017\u00102\u001a\u00020\u00052\u0006\u00101\u001a\u000200H\u0007¢\u0006\u0004\b2\u00103J\u0017\u00105\u001a\u00020\u00052\u0006\u00104\u001a\u00020\u001aH\u0007¢\u0006\u0004\b5\u00106J\u0017\u00109\u001a\u00020\u00052\u0006\u00108\u001a\u000207H\u0007¢\u0006\u0004\b9\u0010:J\u0017\u0010=\u001a\u00020\u00052\u0006\u0010<\u001a\u00020;H\u0007¢\u0006\u0004\b=\u0010>J\u0017\u0010A\u001a\u00020\u00052\u0006\u0010@\u001a\u00020?H\u0007¢\u0006\u0004\bA\u0010BJ\u0017\u0010C\u001a\u00020\u00052\u0006\u0010*\u001a\u00020)H\u0007¢\u0006\u0004\bC\u0010,J\u0017\u0010E\u001a\u00020\u00052\u0006\u0010$\u001a\u00020DH\u0007¢\u0006\u0004\bE\u0010FJ\u000f\u0010G\u001a\u00020\u0005H\u0017¢\u0006\u0004\bG\u0010HJ)\u0010L\u001a\u00020\t2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\f\u0010I\u001a\b\u0012\u0004\u0012\u00020\u00180\u0010H\u0001¢\u0006\u0004\bJ\u0010KR,\u0010N\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\t0\b0M8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bN\u0010OR\u0016\u0010Q\u001a\u00020P8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bQ\u0010RR\u0016\u0010T\u001a\u00020S8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bT\u0010UR\u0016\u0010W\u001a\u00020V8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bW\u0010XR\u0016\u0010Z\u001a\u00020Y8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bZ\u0010[R&\u0010^\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`]\u0012\u0004\u0012\u00020\u00180\\8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b^\u0010_R\u001a\u0010'\u001a\u00060\u0002j\u0002`\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b'\u0010`R,\u0010a\u001a\u0018\u0012\u0004\u0012\u00020\u0002\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00120\u00100\\8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\ba\u0010_R\u0016\u0010c\u001a\u00020b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bc\u0010dR\u001a\u0010f\u001a\u00060\u0002j\u0002`e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bf\u0010`R&\u0010g\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\t0\b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bg\u0010hR\u0016\u0010j\u001a\u00020i8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bj\u0010kR*\u0010m\u001a\u0016\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\b\u0012\u00060\u0002j\u0002`l0\\8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bm\u0010_R&\u0010n\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\t0\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bn\u0010hR\u001c\u0010p\u001a\b\u0012\u0004\u0012\u0002000o8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bp\u0010qR\u0016\u0010s\u001a\u00020r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bs\u0010tR \u0010v\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030u8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bv\u0010w¨\u0006z"}, d2 = {"Lcom/discord/stores/StoreMentions;", "Lcom/discord/stores/StoreV2;", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "removeAllMessagesForChannel", "(J)V", "", "", "computeMentionCounts", "()Ljava/util/Map;", "Lcom/discord/api/user/User;", "author", "Lcom/discord/api/utcdatetime/UtcDateTime;", "editedTimestamp", "", "mentions", "Lcom/discord/primitives/RoleId;", "mentionRoles", "", "mentionEveryone", "hasMention", "(JLcom/discord/api/user/User;Lcom/discord/api/utcdatetime/UtcDateTime;Ljava/util/List;Ljava/util/List;Ljava/lang/Boolean;)Z", "Lcom/discord/models/message/Message;", "(Lcom/discord/models/message/Message;)Z", "Lcom/discord/api/message/Message;", "(Lcom/discord/api/message/Message;)Z", "isMentionableChannel", "(J)Z", "getMentionCounts", "Lrx/Observable;", "observeTotalMentions", "()Lrx/Observable;", "observeMentionCounts", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "selectedChannelId", "handleChannelSelected", "Lcom/discord/api/channel/Channel;", "channel", "handleChannelOrThreadCreateOrUpdate", "(Lcom/discord/api/channel/Channel;)V", "Lcom/discord/models/thread/dto/ModelThreadListSync;", "handleThreadListSync", "(Lcom/discord/models/thread/dto/ModelThreadListSync;)V", "Lcom/discord/models/domain/ModelReadState;", "readState", "handleMessageAck", "(Lcom/discord/models/domain/ModelReadState;)V", "message", "handleMessageCreateOrUpdate", "(Lcom/discord/api/message/Message;)V", "Lcom/discord/models/domain/ModelMessageDelete;", "messageDeleteBulk", "handleMessageDeleted", "(Lcom/discord/models/domain/ModelMessageDelete;)V", "Lcom/discord/api/guild/Guild;", "guild", "handleGuildAdd", "(Lcom/discord/api/guild/Guild;)V", "Lcom/discord/api/guildmember/GuildMember;", "member", "handleGuildMemberAdd", "(Lcom/discord/api/guildmember/GuildMember;)V", "handleChannelOrThreadDelete", "Lcom/discord/api/thread/ThreadMembersUpdate;", "handleThreadMembersUpdate", "(Lcom/discord/api/thread/ThreadMembersUpdate;)V", "snapshotData", "()V", "messagesAfter", "processMarkUnread$app_productionGoogleRelease", "(JLjava/util/List;)I", "processMarkUnread", "Lcom/discord/utilities/persister/Persister;", "countsCache", "Lcom/discord/utilities/persister/Persister;", "Lcom/discord/stores/StoreMessageAck;", "storeMessageAck", "Lcom/discord/stores/StoreMessageAck;", "Lcom/discord/stores/StoreUserGuildSettings;", "storeUserGuildSettings", "Lcom/discord/stores/StoreUserGuildSettings;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/stores/StoreThreadsJoined;", "storeThreadsJoined", "Lcom/discord/stores/StoreThreadsJoined;", "Ljava/util/HashMap;", "Lcom/discord/primitives/MessageId;", "mentionedMessages", "Ljava/util/HashMap;", "J", "myRoleIds", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreChannels;", "Lcom/discord/primitives/UserId;", "myId", "countsEmpty", "Ljava/util/Map;", "Lcom/discord/stores/StoreUserRelationships;", "storeUserRelationships", "Lcom/discord/stores/StoreUserRelationships;", "Lcom/discord/primitives/GuildId;", "channelGuildIds", "countsSnapshot", "Ljava/util/ArrayList;", "serverInitReadStates", "Ljava/util/ArrayList;", "Lcom/discord/stores/StorePermissions;", "storePermissions", "Lcom/discord/stores/StorePermissions;", "Ljava/util/HashSet;", "privateChannels", "Ljava/util/HashSet;", HookHelper.constructorName, "(Lcom/discord/stores/StoreUserRelationships;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreMessageAck;Lcom/discord/stores/StoreUserGuildSettings;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreThreadsJoined;Lcom/discord/stores/updates/ObservationDeck;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMentions extends StoreV2 {
    private final HashMap<Long, Long> channelGuildIds;
    private final Persister<Map<Long, Integer>> countsCache;
    private final Map<Long, Integer> countsEmpty;
    private Map<Long, Integer> countsSnapshot;
    private final HashMap<Long, Message> mentionedMessages;
    private long myId;
    private final HashMap<Long, List<Long>> myRoleIds;
    private final ObservationDeck observationDeck;
    private final HashSet<Long> privateChannels;
    private long selectedChannelId;
    private final ArrayList<ModelReadState> serverInitReadStates;
    private final StoreChannels storeChannels;
    private final StoreMessageAck storeMessageAck;
    private final StorePermissions storePermissions;
    private final StoreThreadsJoined storeThreadsJoined;
    private final StoreUserGuildSettings storeUserGuildSettings;
    private final StoreUserRelationships storeUserRelationships;

    public /* synthetic */ StoreMentions(StoreUserRelationships storeUserRelationships, StorePermissions storePermissions, StoreMessageAck storeMessageAck, StoreUserGuildSettings storeUserGuildSettings, StoreChannels storeChannels, StoreThreadsJoined storeThreadsJoined, ObservationDeck observationDeck, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(storeUserRelationships, storePermissions, storeMessageAck, storeUserGuildSettings, storeChannels, storeThreadsJoined, (i & 64) != 0 ? ObservationDeckProvider.get() : observationDeck);
    }

    @StoreThread
    private final Map<Long, Integer> computeMentionCounts() {
        StoreMessageAck.Ack ack;
        if (this.serverInitReadStates.isEmpty() && this.mentionedMessages.isEmpty()) {
            return this.countsEmpty;
        }
        HashMap hashMap = new HashMap();
        Map<Long, StoreMessageAck.Ack> allInternal = this.storeMessageAck.getAllInternal();
        Iterator<ModelReadState> it = this.serverInitReadStates.iterator();
        while (it.hasNext()) {
            ModelReadState next = it.next();
            long component1 = next.component1();
            long component2 = next.component2();
            int component3 = next.component3();
            if (component3 >= 1 && isMentionableChannel(component1) && (ack = allInternal.get(Long.valueOf(component1))) != null && ack.component1() <= component2) {
                Long valueOf = Long.valueOf(component1);
                Integer num = (Integer) hashMap.get(Long.valueOf(component1));
                if (num == null) {
                    num = 0;
                }
                hashMap.put(valueOf, Integer.valueOf(num.intValue() + component3));
            }
        }
        for (Message message : this.mentionedMessages.values()) {
            long channelId = message.getChannelId();
            User author = message.getAuthor();
            if (!ModelUserRelationship.isType(this.storeUserRelationships.getRelationships().get(author != null ? Long.valueOf(author.i()) : null), 2) && isMentionableChannel(channelId)) {
                StoreMessageAck.Ack ack2 = allInternal.get(Long.valueOf(channelId));
                if (ack2 == null || ack2.component1() < message.getId()) {
                    Long valueOf2 = Long.valueOf(channelId);
                    Integer num2 = (Integer) hashMap.get(Long.valueOf(channelId));
                    if (num2 == null) {
                        num2 = 0;
                    }
                    hashMap.put(valueOf2, Integer.valueOf(num2.intValue() + 1));
                }
            }
        }
        return hashMap;
    }

    @StoreThread
    private final boolean hasMention(long j, User user, UtcDateTime utcDateTime, List<User> list, List<Long> list2, Boolean bool) {
        List<Long> list3;
        boolean z2;
        boolean z3;
        ModelNotificationSettings modelNotificationSettings;
        ModelNotificationSettings.ChannelOverride channelOverride;
        Long l = this.channelGuildIds.get(Long.valueOf(j));
        if (l == null) {
            l = 0L;
        }
        m.checkNotNullExpressionValue(l, "channelGuildIds[channelId] ?: 0");
        long longValue = l.longValue();
        if (user == null || user.i() != this.myId) {
            if ((utcDateTime != null ? utcDateTime.g() : 0L) <= 0) {
                Channel channel = this.storeChannels.getChannel(j);
                if (!(channel == null || !ChannelUtils.C(channel) || this.storeThreadsJoined.hasJoined(j))) {
                    return false;
                }
                Map<Long, ModelNotificationSettings> guildSettingsInternal$app_productionGoogleRelease = this.storeUserGuildSettings.getGuildSettingsInternal$app_productionGoogleRelease();
                if (this.privateChannels.contains(Long.valueOf(j)) && ((modelNotificationSettings = guildSettingsInternal$app_productionGoogleRelease.get(0L)) == null || (channelOverride = modelNotificationSettings.getChannelOverride(j)) == null || !channelOverride.isMuted())) {
                    return true;
                }
                if (list != null) {
                    if (!list.isEmpty()) {
                        for (User user2 : list) {
                            if (user2.i() == this.myId) {
                                z3 = true;
                                continue;
                            } else {
                                z3 = false;
                                continue;
                            }
                            if (z3) {
                                z2 = true;
                                break;
                            }
                        }
                    }
                    z2 = false;
                    if (z2) {
                        return true;
                    }
                }
                ModelNotificationSettings modelNotificationSettings2 = guildSettingsInternal$app_productionGoogleRelease.get(Long.valueOf(longValue));
                if (!((modelNotificationSettings2 != null && modelNotificationSettings2.isSuppressRoles()) || (list3 = this.myRoleIds.get(Long.valueOf(longValue))) == null || list2 == null)) {
                    Iterator<T> it = list2.iterator();
                    if (it.hasNext()) {
                        return list3.contains(Long.valueOf(((Number) it.next()).longValue()));
                    }
                }
                if (!m.areEqual(bool, Boolean.TRUE)) {
                    return false;
                }
                ModelNotificationSettings modelNotificationSettings3 = guildSettingsInternal$app_productionGoogleRelease.get(Long.valueOf(longValue));
                return modelNotificationSettings3 == null || !modelNotificationSettings3.isSuppressEveryone();
            }
        }
        return false;
    }

    @StoreThread
    private final boolean isMentionableChannel(long j) {
        StoreMessageAck.Ack ack = this.storeMessageAck.getAllInternal().get(Long.valueOf(j));
        boolean isLockedAck = ack != null ? ack.isLockedAck() : false;
        if (j == this.selectedChannelId && !isLockedAck) {
            return false;
        }
        return this.privateChannels.contains(Long.valueOf(j)) || PermissionUtils.can(Permission.VIEW_CHANNEL, this.storePermissions.getPermissionsByChannel().get(Long.valueOf(j)));
    }

    @StoreThread
    private final void removeAllMessagesForChannel(long j) {
        HashMap<Long, Message> hashMap = this.mentionedMessages;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry<Long, Message> entry : hashMap.entrySet()) {
            if (entry.getValue().getChannelId() == j) {
                linkedHashMap.put(entry.getKey(), entry.getValue());
            }
        }
        for (Number number : linkedHashMap.keySet()) {
            this.mentionedMessages.remove(Long.valueOf(number.longValue()));
            markChanged();
        }
    }

    public final Map<Long, Integer> getMentionCounts() {
        return this.countsSnapshot;
    }

    @StoreThread
    public final void handleChannelOrThreadCreateOrUpdate(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        this.channelGuildIds.put(Long.valueOf(channel.h()), Long.valueOf(channel.f()));
        if (ChannelUtils.x(channel) && !ChannelUtils.v(channel)) {
            this.privateChannels.add(Long.valueOf(channel.h()));
        }
    }

    @StoreThread
    public final void handleChannelOrThreadDelete(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        this.channelGuildIds.remove(Long.valueOf(channel.h()));
        this.privateChannels.remove(Long.valueOf(channel.h()));
        removeAllMessagesForChannel(channel.h());
    }

    @StoreThread
    public final void handleChannelSelected(long j) {
        this.selectedChannelId = j;
        markChanged();
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        this.privateChannels.clear();
        this.mentionedMessages.clear();
        this.channelGuildIds.clear();
        this.myRoleIds.clear();
        this.myId = modelPayload.getMe().i();
        for (Guild guild : modelPayload.getGuilds()) {
            m.checkNotNullExpressionValue(guild, "guild");
            handleGuildAdd(guild);
        }
        HashSet<Long> hashSet = this.privateChannels;
        List<Channel> privateChannels = modelPayload.getPrivateChannels();
        ArrayList<Channel> Y = a.Y(privateChannels, "payload.privateChannels");
        for (Object obj : privateChannels) {
            Channel channel = (Channel) obj;
            m.checkNotNullExpressionValue(channel, "it");
            if (!ChannelUtils.v(channel)) {
                Y.add(obj);
            }
        }
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(Y, 10));
        for (Channel channel2 : Y) {
            arrayList.add(Long.valueOf(channel2.h()));
        }
        hashSet.addAll(arrayList);
        ModelPayload.VersionedReadStates readState = modelPayload.getReadState();
        m.checkNotNullExpressionValue(readState, "payload.readState");
        if (!readState.isPartial()) {
            this.serverInitReadStates.clear();
        } else {
            ModelPayload.VersionedReadStates readState2 = modelPayload.getReadState();
            m.checkNotNullExpressionValue(readState2, "payload.readState");
            List<ModelReadState> entries = readState2.getEntries();
            m.checkNotNullExpressionValue(entries, "payload.readState.entries");
            ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(entries, 10));
            for (ModelReadState modelReadState : entries) {
                arrayList2.add(Long.valueOf(modelReadState.getChannelId()));
            }
            r.removeAll((List) this.serverInitReadStates, (Function1) new StoreMentions$handleConnectionOpen$3(u.toHashSet(arrayList2)));
        }
        ArrayList<ModelReadState> arrayList3 = this.serverInitReadStates;
        ModelPayload.VersionedReadStates readState3 = modelPayload.getReadState();
        m.checkNotNullExpressionValue(readState3, "payload.readState");
        List<ModelReadState> entries2 = readState3.getEntries();
        m.checkNotNullExpressionValue(entries2, "payload.readState.entries");
        ArrayList arrayList4 = new ArrayList();
        for (Object obj2 : entries2) {
            if (((ModelReadState) obj2).isMentioned()) {
                arrayList4.add(obj2);
            }
        }
        arrayList3.addAll(arrayList4);
        r.removeAll((List) this.serverInitReadStates, (Function1) new StoreMentions$handleConnectionOpen$5(this));
        markChanged();
    }

    @StoreThread
    public final void handleGuildAdd(Guild guild) {
        Object obj;
        boolean z2;
        m.checkNotNullParameter(guild, "guild");
        List<GuildMember> v = guild.v();
        if (v != null) {
            Iterator<T> it = v.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (((GuildMember) obj).m().i() == this.myId) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    break;
                }
            }
            GuildMember guildMember = (GuildMember) obj;
            if (guildMember != null) {
                this.myRoleIds.put(Long.valueOf(guild.r()), guildMember.l());
            }
        }
        List<Channel> g = guild.g();
        if (g != null) {
            for (Channel channel : g) {
                this.channelGuildIds.put(Long.valueOf(channel.h()), Long.valueOf(guild.r()));
            }
        }
        List<Channel> N = guild.N();
        if (N != null) {
            for (Channel channel2 : N) {
                this.channelGuildIds.put(Long.valueOf(channel2.h()), Long.valueOf(guild.r()));
            }
        }
    }

    @StoreThread
    public final void handleGuildMemberAdd(GuildMember guildMember) {
        m.checkNotNullParameter(guildMember, "member");
        long i = guildMember.m().i();
        if (i == this.myId) {
            this.myRoleIds.put(Long.valueOf(i), guildMember.l());
        }
    }

    @StoreThread
    public final void handleMessageAck(ModelReadState modelReadState) {
        m.checkNotNullParameter(modelReadState, "readState");
        r.removeAll((List) this.serverInitReadStates, (Function1) new StoreMentions$handleMessageAck$1(modelReadState));
        this.serverInitReadStates.add(modelReadState);
        markChanged();
    }

    @StoreThread
    public final void handleMessageCreateOrUpdate(com.discord.api.message.Message message) {
        m.checkNotNullParameter(message, "message");
        if (hasMention(message)) {
            this.mentionedMessages.put(Long.valueOf(message.o()), new Message(message));
            markChanged();
        }
    }

    @StoreThread
    public final void handleMessageDeleted(ModelMessageDelete modelMessageDelete) {
        m.checkNotNullParameter(modelMessageDelete, "messageDeleteBulk");
        if (this.privateChannels.contains(Long.valueOf(modelMessageDelete.getChannelId()))) {
            List<Long> messageIds = modelMessageDelete.getMessageIds();
            m.checkNotNullExpressionValue(messageIds, "messageDeleteBulk.messageIds");
            for (Long l : messageIds) {
                HashMap<Long, Message> hashMap = this.mentionedMessages;
                m.checkNotNullExpressionValue(l, "it");
                hashMap.remove(l);
            }
            markChanged();
        }
    }

    @StoreThread
    public final void handleThreadListSync(ModelThreadListSync modelThreadListSync) {
        m.checkNotNullParameter(modelThreadListSync, "payload");
        for (Channel channel : modelThreadListSync.getThreads()) {
            this.channelGuildIds.put(Long.valueOf(channel.h()), Long.valueOf(modelThreadListSync.getGuildId()));
        }
    }

    @StoreThread
    public final void handleThreadMembersUpdate(ThreadMembersUpdate threadMembersUpdate) {
        m.checkNotNullParameter(threadMembersUpdate, "payload");
        List<Long> d = threadMembersUpdate.d();
        if (d != null && d.contains(Long.valueOf(this.myId))) {
            removeAllMessagesForChannel(threadMembersUpdate.c());
        }
    }

    public final Observable<Map<Long, Integer>> observeMentionCounts() {
        Observable<Map<Long, Integer>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreMentions$observeMentionCounts$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    public final Observable<Integer> observeTotalMentions() {
        Observable F = observeMentionCounts().F(StoreMentions$observeTotalMentions$1.INSTANCE);
        m.checkNotNullExpressionValue(F, "observeMentionCounts()\n … .map { it.values.sum() }");
        return F;
    }

    @StoreThread
    public final int processMarkUnread$app_productionGoogleRelease(long j, List<Message> list) {
        m.checkNotNullParameter(list, "messagesAfter");
        int i = 0;
        for (Message message : list) {
            if (!this.mentionedMessages.containsKey(Long.valueOf(message.getId())) && hasMention(message)) {
                this.mentionedMessages.put(Long.valueOf(message.getId()), message);
                i++;
            }
        }
        r.removeAll((List) this.serverInitReadStates, (Function1) new StoreMentions$processMarkUnread$1(j));
        markChanged();
        return i;
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        super.snapshotData();
        Map<Long, Integer> computeMentionCounts = computeMentionCounts();
        Persister.set$default(this.countsCache, computeMentionCounts, false, 2, null);
        this.countsSnapshot = computeMentionCounts;
    }

    public StoreMentions(StoreUserRelationships storeUserRelationships, StorePermissions storePermissions, StoreMessageAck storeMessageAck, StoreUserGuildSettings storeUserGuildSettings, StoreChannels storeChannels, StoreThreadsJoined storeThreadsJoined, ObservationDeck observationDeck) {
        m.checkNotNullParameter(storeUserRelationships, "storeUserRelationships");
        m.checkNotNullParameter(storePermissions, "storePermissions");
        m.checkNotNullParameter(storeMessageAck, "storeMessageAck");
        m.checkNotNullParameter(storeUserGuildSettings, "storeUserGuildSettings");
        m.checkNotNullParameter(storeChannels, "storeChannels");
        m.checkNotNullParameter(storeThreadsJoined, "storeThreadsJoined");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.storeUserRelationships = storeUserRelationships;
        this.storePermissions = storePermissions;
        this.storeMessageAck = storeMessageAck;
        this.storeUserGuildSettings = storeUserGuildSettings;
        this.storeChannels = storeChannels;
        this.storeThreadsJoined = storeThreadsJoined;
        this.observationDeck = observationDeck;
        HashMap hashMap = new HashMap();
        this.countsEmpty = hashMap;
        this.countsCache = new Persister<>("CHANNEL_MENTION_COUNTS_V6", hashMap);
        this.countsSnapshot = h0.emptyMap();
        this.serverInitReadStates = new ArrayList<>();
        this.mentionedMessages = new HashMap<>();
        this.privateChannels = new HashSet<>();
        this.myRoleIds = new HashMap<>();
        this.channelGuildIds = new HashMap<>();
    }

    @StoreThread
    private final boolean hasMention(Message message) {
        return hasMention(message.getChannelId(), message.getAuthor(), message.getEditedTimestamp(), message.getMentions(), message.getMentionRoles(), message.getMentionEveryone());
    }

    @StoreThread
    private final boolean hasMention(com.discord.api.message.Message message) {
        return hasMention(message.g(), message.e(), message.j(), message.t(), message.s(), message.r());
    }
}
