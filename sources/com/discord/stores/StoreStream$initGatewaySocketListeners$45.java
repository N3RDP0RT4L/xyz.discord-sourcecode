package com.discord.stores;

import com.discord.api.presence.Presence;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreStream.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/api/presence/Presence;", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Lcom/discord/api/presence/Presence;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStream$initGatewaySocketListeners$45 extends o implements Function1<Presence, Unit> {
    public final /* synthetic */ StoreStream this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreStream$initGatewaySocketListeners$45(StoreStream storeStream) {
        super(1);
        this.this$0 = storeStream;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Presence presence) {
        invoke2(presence);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Presence presence) {
        StoreStream storeStream = this.this$0;
        Long d = presence.d();
        long longValue = d != null ? d.longValue() : 0L;
        m.checkNotNullExpressionValue(presence, "it");
        storeStream.handlePresenceUpdate(longValue, presence);
    }
}
