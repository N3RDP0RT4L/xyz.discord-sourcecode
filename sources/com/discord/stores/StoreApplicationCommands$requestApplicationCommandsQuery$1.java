package com.discord.stores;

import d0.t.n;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreApplicationCommands.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreApplicationCommands$requestApplicationCommandsQuery$1 extends o implements Function0<Unit> {
    public final /* synthetic */ Long $guildId;
    public final /* synthetic */ String $query;
    public final /* synthetic */ StoreApplicationCommands this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreApplicationCommands$requestApplicationCommandsQuery$1(StoreApplicationCommands storeApplicationCommands, Long l, String str) {
        super(0);
        this.this$0 = storeApplicationCommands;
        this.$guildId = l;
        this.$query = str;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        String generateNonce;
        Long l;
        StoreGatewayConnection storeGatewayConnection;
        List list;
        generateNonce = this.this$0.generateNonce();
        this.this$0.queryNonce = generateNonce;
        l = this.this$0.queryGuildId;
        if (!m.areEqual(l, this.$guildId)) {
            list = this.this$0.queryCommands;
            list.clear();
            this.this$0.markChanged(StoreApplicationCommands.Companion.getQueryCommandsUpdate());
        }
        this.this$0.queryGuildId = this.$guildId;
        this.this$0.query = this.$query;
        Long l2 = this.$guildId;
        if (l2 == null || l2.longValue() <= 0) {
            this.this$0.handleQueryCommandsUpdate(n.emptyList());
            return;
        }
        storeGatewayConnection = this.this$0.storeGatewayConnection;
        storeGatewayConnection.requestApplicationCommands(this.$guildId.longValue(), generateNonce, false, (r20 & 8) != 0 ? null : this.$query, (r20 & 16) != 0 ? null : null, 20, (r20 & 64) != 0 ? null : null);
    }
}
