package com.discord.stores;

import com.discord.stores.StoreAnalytics;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreAnalytics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/stores/StoreAnalytics$ScreenViewed;", "p1", "", "invoke", "(Lcom/discord/stores/StoreAnalytics$ScreenViewed;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final /* synthetic */ class StoreAnalytics$init$4 extends k implements Function1<StoreAnalytics.ScreenViewed, Unit> {
    public StoreAnalytics$init$4(StoreAnalytics storeAnalytics) {
        super(1, storeAnalytics, StoreAnalytics.class, "onScreenViewed", "onScreenViewed(Lcom/discord/stores/StoreAnalytics$ScreenViewed;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(StoreAnalytics.ScreenViewed screenViewed) {
        invoke2(screenViewed);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(StoreAnalytics.ScreenViewed screenViewed) {
        m.checkNotNullParameter(screenViewed, "p1");
        ((StoreAnalytics) this.receiver).onScreenViewed(screenViewed);
    }
}
