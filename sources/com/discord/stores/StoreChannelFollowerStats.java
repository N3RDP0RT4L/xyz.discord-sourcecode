package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelChannelFollowerStats;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.time.ClockFactory;
import d0.z.d.m;
import java.util.HashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.objectweb.asm.Opcodes;
import rx.Observable;
/* compiled from: StoreChannelFollowerStats.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\b\u0007\u0018\u0000 &2\u00020\u0001:\u0003'&(B\u0017\u0012\u0006\u0010 \u001a\u00020\u001f\u0012\u0006\u0010\u001d\u001a\u00020\u001c¢\u0006\u0004\b$\u0010%J\u001b\u0010\u0006\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u001b\u0010\t\u001a\u00020\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0003¢\u0006\u0004\b\t\u0010\nJ\u001b\u0010\u000b\u001a\u00020\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0003¢\u0006\u0004\b\u000b\u0010\nJ#\u0010\u000e\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\r\u001a\u00020\fH\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u001b\u0010\u0010\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0003¢\u0006\u0004\b\u0010\u0010\u0007J\u001b\u0010\u0011\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0003¢\u0006\u0004\b\u0011\u0010\u0007J!\u0010\u0013\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\f0\u00122\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0019\u0010\u0015\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0015\u0010\u0007J\u000f\u0010\u0016\u001a\u00020\u0005H\u0017¢\u0006\u0004\b\u0016\u0010\u0017R&\u0010\u001a\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00190\u00188\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001a\u0010\u001bR\u0016\u0010\u001d\u001a\u00020\u001c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u0016\u0010 \u001a\u00020\u001f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b \u0010!R&\u0010#\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00190\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010\u001b¨\u0006)"}, d2 = {"Lcom/discord/stores/StoreChannelFollowerStats;", "Lcom/discord/stores/StoreV2;", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "fetchIfNonexistingOrStale", "(J)V", "", "isStale", "(J)Z", "isExisting", "Lcom/discord/models/domain/ModelChannelFollowerStats;", "channelFollowerStats", "handleChannelFollowerStatsFetchSuccess", "(JLcom/discord/models/domain/ModelChannelFollowerStats;)V", "handleChannelFollowerStatsFetchStart", "handleChannelFollowerStatsFetchFailed", "Lrx/Observable;", "observeChannelFollowerStats", "(J)Lrx/Observable;", "fetchChannelFollowerStats", "snapshotData", "()V", "", "Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;", "channelFollowerStatsStateSnapshot", "Ljava/util/Map;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "", "channelFollowerStatsState", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V", "Companion", "ChannelFollowerStatData", "FetchState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreChannelFollowerStats extends StoreV2 {
    public static final Companion Companion = new Companion(null);
    private static final int STALE_TIME_DIFF_MS = 3600000;
    private final Map<Long, ChannelFollowerStatData> channelFollowerStatsState = new HashMap();
    private Map<Long, ChannelFollowerStatData> channelFollowerStatsStateSnapshot = new HashMap();
    private final Dispatcher dispatcher;
    private final ObservationDeck observationDeck;

    /* compiled from: StoreChannelFollowerStats.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\b\b\u0002\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0018\u001a\u0004\b\u0019\u0010\u0007¨\u0006\u001c"}, d2 = {"Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;", "", "Lcom/discord/stores/StoreChannelFollowerStats$FetchState;", "component1", "()Lcom/discord/stores/StoreChannelFollowerStats$FetchState;", "Lcom/discord/models/domain/ModelChannelFollowerStats;", "component2", "()Lcom/discord/models/domain/ModelChannelFollowerStats;", "fetchState", "data", "copy", "(Lcom/discord/stores/StoreChannelFollowerStats$FetchState;Lcom/discord/models/domain/ModelChannelFollowerStats;)Lcom/discord/stores/StoreChannelFollowerStats$ChannelFollowerStatData;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StoreChannelFollowerStats$FetchState;", "getFetchState", "Lcom/discord/models/domain/ModelChannelFollowerStats;", "getData", HookHelper.constructorName, "(Lcom/discord/stores/StoreChannelFollowerStats$FetchState;Lcom/discord/models/domain/ModelChannelFollowerStats;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ChannelFollowerStatData {
        private final ModelChannelFollowerStats data;
        private final FetchState fetchState;

        public ChannelFollowerStatData(FetchState fetchState, ModelChannelFollowerStats modelChannelFollowerStats) {
            m.checkNotNullParameter(fetchState, "fetchState");
            m.checkNotNullParameter(modelChannelFollowerStats, "data");
            this.fetchState = fetchState;
            this.data = modelChannelFollowerStats;
        }

        public static /* synthetic */ ChannelFollowerStatData copy$default(ChannelFollowerStatData channelFollowerStatData, FetchState fetchState, ModelChannelFollowerStats modelChannelFollowerStats, int i, Object obj) {
            if ((i & 1) != 0) {
                fetchState = channelFollowerStatData.fetchState;
            }
            if ((i & 2) != 0) {
                modelChannelFollowerStats = channelFollowerStatData.data;
            }
            return channelFollowerStatData.copy(fetchState, modelChannelFollowerStats);
        }

        public final FetchState component1() {
            return this.fetchState;
        }

        public final ModelChannelFollowerStats component2() {
            return this.data;
        }

        public final ChannelFollowerStatData copy(FetchState fetchState, ModelChannelFollowerStats modelChannelFollowerStats) {
            m.checkNotNullParameter(fetchState, "fetchState");
            m.checkNotNullParameter(modelChannelFollowerStats, "data");
            return new ChannelFollowerStatData(fetchState, modelChannelFollowerStats);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ChannelFollowerStatData)) {
                return false;
            }
            ChannelFollowerStatData channelFollowerStatData = (ChannelFollowerStatData) obj;
            return m.areEqual(this.fetchState, channelFollowerStatData.fetchState) && m.areEqual(this.data, channelFollowerStatData.data);
        }

        public final ModelChannelFollowerStats getData() {
            return this.data;
        }

        public final FetchState getFetchState() {
            return this.fetchState;
        }

        public int hashCode() {
            FetchState fetchState = this.fetchState;
            int i = 0;
            int hashCode = (fetchState != null ? fetchState.hashCode() : 0) * 31;
            ModelChannelFollowerStats modelChannelFollowerStats = this.data;
            if (modelChannelFollowerStats != null) {
                i = modelChannelFollowerStats.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("ChannelFollowerStatData(fetchState=");
            R.append(this.fetchState);
            R.append(", data=");
            R.append(this.data);
            R.append(")");
            return R.toString();
        }

        public /* synthetic */ ChannelFollowerStatData(FetchState fetchState, ModelChannelFollowerStats modelChannelFollowerStats, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(fetchState, (i & 2) != 0 ? new ModelChannelFollowerStats(0L, null, null, null, null, null, null, Opcodes.LAND, null) : modelChannelFollowerStats);
        }
    }

    /* compiled from: StoreChannelFollowerStats.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/stores/StoreChannelFollowerStats$Companion;", "", "", "STALE_TIME_DIFF_MS", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreChannelFollowerStats.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/stores/StoreChannelFollowerStats$FetchState;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "FETCHING", "FAILED", "SUCCEEDED", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum FetchState {
        FETCHING,
        FAILED,
        SUCCEEDED
    }

    public StoreChannelFollowerStats(Dispatcher dispatcher, ObservationDeck observationDeck) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
    }

    @StoreThread
    private final void fetchIfNonexistingOrStale(long j) {
        this.dispatcher.schedule(new StoreChannelFollowerStats$fetchIfNonexistingOrStale$1(this, j));
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleChannelFollowerStatsFetchFailed(long j) {
        this.channelFollowerStatsState.put(Long.valueOf(j), new ChannelFollowerStatData(FetchState.FAILED, null, 2, null));
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleChannelFollowerStatsFetchStart(long j) {
        this.channelFollowerStatsState.put(Long.valueOf(j), new ChannelFollowerStatData(FetchState.FETCHING, null, 2, null));
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleChannelFollowerStatsFetchSuccess(long j, ModelChannelFollowerStats modelChannelFollowerStats) {
        this.channelFollowerStatsState.put(Long.valueOf(j), new ChannelFollowerStatData(FetchState.SUCCEEDED, modelChannelFollowerStats));
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final boolean isExisting(long j) {
        ChannelFollowerStatData channelFollowerStatData = this.channelFollowerStatsState.get(Long.valueOf(j));
        return (channelFollowerStatData == null || channelFollowerStatData.getFetchState() == FetchState.FAILED) ? false : true;
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final boolean isStale(long j) {
        ModelChannelFollowerStats data;
        ChannelFollowerStatData channelFollowerStatData = this.channelFollowerStatsState.get(Long.valueOf(j));
        return ((channelFollowerStatData == null || (data = channelFollowerStatData.getData()) == null) ? 0L : data.getLastFetched() + ((long) 3600000)) <= ClockFactory.get().currentTimeMillis();
    }

    public final void fetchChannelFollowerStats(long j) {
        fetchIfNonexistingOrStale(j);
    }

    public final Observable<ModelChannelFollowerStats> observeChannelFollowerStats(long j) {
        Observable<ModelChannelFollowerStats> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreChannelFollowerStats$observeChannelFollowerStats$1(this, j), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck\n        …  .distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        this.channelFollowerStatsStateSnapshot = new HashMap(this.channelFollowerStatsState);
    }
}
