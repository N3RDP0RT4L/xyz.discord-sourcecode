package com.discord.stores;

import com.discord.models.domain.ModelGuildBoostSlot;
import com.discord.stores.StoreGuildBoost;
import d0.t.h0;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreGuildBoost.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildBoost$updateGuildBoostSlot$1 extends o implements Function0<Unit> {
    public final /* synthetic */ ModelGuildBoostSlot $newSlot;
    public final /* synthetic */ StoreGuildBoost this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGuildBoost$updateGuildBoostSlot$1(StoreGuildBoost storeGuildBoost, ModelGuildBoostSlot modelGuildBoostSlot) {
        super(0);
        this.this$0 = storeGuildBoost;
        this.$newSlot = modelGuildBoostSlot;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreGuildBoost.State state;
        state = this.this$0.state;
        if (state instanceof StoreGuildBoost.State.Loaded) {
            Map plus = h0.plus(((StoreGuildBoost.State.Loaded) state).getBoostSlotMap(), d0.o.to(Long.valueOf(this.$newSlot.getId()), this.$newSlot));
            this.this$0.state = new StoreGuildBoost.State.Loaded(plus);
            this.this$0.markChanged();
        }
    }
}
