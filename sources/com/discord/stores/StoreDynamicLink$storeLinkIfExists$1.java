package com.discord.stores;

import android.net.Uri;
import androidx.core.app.NotificationCompat;
import com.discord.stores.StoreDynamicLink;
import j0.k.b;
import kotlin.Metadata;
/* compiled from: StoreDynamicLink.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u0004\u0018\u00010\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Landroid/net/Uri;", "kotlin.jvm.PlatformType", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;", NotificationCompat.CATEGORY_CALL, "(Landroid/net/Uri;)Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreDynamicLink$storeLinkIfExists$1<T, R> implements b<Uri, StoreDynamicLink.DynamicLinkData> {
    public static final StoreDynamicLink$storeLinkIfExists$1 INSTANCE = new StoreDynamicLink$storeLinkIfExists$1();

    /* JADX WARN: Can't wrap try/catch for region: R(22:5|(1:7)(1:8)|9|(1:20)(1:19)|21|(4:23|(1:25)|26|(17:30|(2:31|(2:33|(1:81))(2:82|35))|36|38|(1:48)|(4:50|(1:52)|53|(13:57|(2:58|(2:60|(1:83))(2:84|62))|63|77|65|66|79|68|69|75|71|72|73))|64|77|65|66|79|68|69|75|71|72|73))|37|38|(3:40|44|46)|48|(0)|64|77|65|66|79|68|69|75|71|72|73) */
    /* JADX WARN: Code restructure failed: missing block: B:67:0x011b, code lost:
        r4 = null;
     */
    /* JADX WARN: Code restructure failed: missing block: B:70:0x0124, code lost:
        r5 = null;
     */
    /* JADX WARN: Removed duplicated region for block: B:50:0x00d8  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final com.discord.stores.StoreDynamicLink.DynamicLinkData call(android.net.Uri r10) {
        /*
            Method dump skipped, instructions count: 309
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreDynamicLink$storeLinkIfExists$1.call(android.net.Uri):com.discord.stores.StoreDynamicLink$DynamicLinkData");
    }
}
