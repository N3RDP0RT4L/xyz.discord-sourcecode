package com.discord.stores;

import com.discord.models.domain.ModelVoiceRegion;
import com.discord.stores.StoreGuildVoiceRegions;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import java.util.HashMap;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreGuildVoiceRegions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildVoiceRegions$fetchIfNonexisting$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ StoreGuildVoiceRegions this$0;

    /* compiled from: StoreGuildVoiceRegions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/models/domain/ModelVoiceRegion;", "voiceRegions", "", "invoke", "(Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreGuildVoiceRegions$fetchIfNonexisting$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<List<? extends ModelVoiceRegion>, Unit> {

        /* compiled from: StoreGuildVoiceRegions.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreGuildVoiceRegions$fetchIfNonexisting$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C02061 extends o implements Function0<Unit> {
            public final /* synthetic */ List $voiceRegions;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C02061(List list) {
                super(0);
                this.$voiceRegions = list;
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                StoreGuildVoiceRegions$fetchIfNonexisting$1 storeGuildVoiceRegions$fetchIfNonexisting$1 = StoreGuildVoiceRegions$fetchIfNonexisting$1.this;
                storeGuildVoiceRegions$fetchIfNonexisting$1.this$0.handleGuildVoiceRegions(storeGuildVoiceRegions$fetchIfNonexisting$1.$guildId, this.$voiceRegions);
            }
        }

        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(List<? extends ModelVoiceRegion> list) {
            invoke2(list);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(List<? extends ModelVoiceRegion> list) {
            Dispatcher dispatcher;
            m.checkNotNullParameter(list, "voiceRegions");
            dispatcher = StoreGuildVoiceRegions$fetchIfNonexisting$1.this.this$0.dispatcher;
            dispatcher.schedule(new C02061(list));
        }
    }

    /* compiled from: StoreGuildVoiceRegions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreGuildVoiceRegions$fetchIfNonexisting$1$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2 extends o implements Function1<Error, Unit> {

        /* compiled from: StoreGuildVoiceRegions.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreGuildVoiceRegions$fetchIfNonexisting$1$2$1  reason: invalid class name */
        /* loaded from: classes.dex */
        public static final class AnonymousClass1 extends o implements Function0<Unit> {
            public AnonymousClass1() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                StoreGuildVoiceRegions$fetchIfNonexisting$1 storeGuildVoiceRegions$fetchIfNonexisting$1 = StoreGuildVoiceRegions$fetchIfNonexisting$1.this;
                storeGuildVoiceRegions$fetchIfNonexisting$1.this$0.handleGuildVoiceRegionsFetchFailed(storeGuildVoiceRegions$fetchIfNonexisting$1.$guildId);
            }
        }

        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Error error) {
            invoke2(error);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Error error) {
            Dispatcher dispatcher;
            m.checkNotNullParameter(error, "it");
            dispatcher = StoreGuildVoiceRegions$fetchIfNonexisting$1.this.this$0.dispatcher;
            dispatcher.schedule(new AnonymousClass1());
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGuildVoiceRegions$fetchIfNonexisting$1(StoreGuildVoiceRegions storeGuildVoiceRegions, long j) {
        super(0);
        this.this$0 = storeGuildVoiceRegions;
        this.$guildId = j;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        HashMap hashMap;
        hashMap = this.this$0.guildVoiceRegionsState;
        StoreGuildVoiceRegions.State state = (StoreGuildVoiceRegions.State) hashMap.get(Long.valueOf(this.$guildId));
        if (!(state instanceof StoreGuildVoiceRegions.State.Loaded) && !(state instanceof StoreGuildVoiceRegions.State.Fetching)) {
            this.this$0.handleGuildVoiceRegionsFetchStart(this.$guildId);
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn(RestAPI.Companion.getApi().getGuildVoiceRegions(this.$guildId), false), this.this$0.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new AnonymousClass2(), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
        }
    }
}
