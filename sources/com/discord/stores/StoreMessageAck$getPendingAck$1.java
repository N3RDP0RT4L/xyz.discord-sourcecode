package com.discord.stores;

import com.discord.stores.StoreMessageAck;
import com.discord.utilities.message.MessageUtils;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function2;
/* compiled from: StoreMessageAck.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\b\u001a\u00020\u00052\b\u0010\u0001\u001a\u0004\u0018\u00010\u00002\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Lcom/discord/stores/StoreMessageAck$Ack;", "currentAck", "", "Lcom/discord/primitives/MessageId;", "mostRecentMessageId", "", "invoke", "(Lcom/discord/stores/StoreMessageAck$Ack;J)Z", "isUpdateRequired"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessageAck$getPendingAck$1 extends o implements Function2<StoreMessageAck.Ack, Long, Boolean> {
    public final /* synthetic */ boolean $clearLock;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreMessageAck$getPendingAck$1(boolean z2) {
        super(2);
        this.$clearLock = z2;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Boolean invoke(StoreMessageAck.Ack ack, Long l) {
        return Boolean.valueOf(invoke(ack, l.longValue()));
    }

    public final boolean invoke(StoreMessageAck.Ack ack, long j) {
        boolean isNewer = MessageUtils.isNewer(ack != null ? Long.valueOf(ack.getMessageId()) : null, Long.valueOf(j));
        boolean z2 = ack == null || !ack.isLockedAck();
        if (isNewer) {
            return this.$clearLock || z2;
        }
        return false;
    }
}
