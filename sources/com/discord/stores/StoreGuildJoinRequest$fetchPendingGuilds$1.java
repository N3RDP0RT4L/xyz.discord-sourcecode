package com.discord.stores;

import com.discord.api.guild.Guild;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreGuildJoinRequest.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildJoinRequest$fetchPendingGuilds$1 extends o implements Function0<Unit> {
    public final /* synthetic */ StoreGuildJoinRequest this$0;

    /* compiled from: StoreGuildJoinRequest.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/api/guild/Guild;", "results", "", "invoke", "(Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreGuildJoinRequest$fetchPendingGuilds$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<List<? extends Guild>, Unit> {

        /* compiled from: StoreGuildJoinRequest.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreGuildJoinRequest$fetchPendingGuilds$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C02021 extends o implements Function0<Unit> {
            public final /* synthetic */ List $results;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C02021(List list) {
                super(0);
                this.$results = list;
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                HashMap hashMap;
                for (Guild guild : this.$results) {
                    hashMap = StoreGuildJoinRequest$fetchPendingGuilds$1.this.this$0.pendingGuilds;
                    hashMap.put(Long.valueOf(guild.r()), new com.discord.models.guild.Guild(guild));
                }
                StoreGuildJoinRequest$fetchPendingGuilds$1.this.this$0.markChanged();
            }
        }

        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(List<? extends Guild> list) {
            invoke2((List<Guild>) list);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(List<Guild> list) {
            Dispatcher dispatcher;
            m.checkNotNullParameter(list, "results");
            dispatcher = StoreGuildJoinRequest$fetchPendingGuilds$1.this.this$0.dispatcher;
            dispatcher.schedule(new C02021(list));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGuildJoinRequest$fetchPendingGuilds$1(StoreGuildJoinRequest storeGuildJoinRequest) {
        super(0);
        this.this$0 = storeGuildJoinRequest;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreGuilds storeGuilds;
        HashMap hashMap;
        HashMap hashMap2;
        storeGuilds = this.this$0.guildsStore;
        Map<Long, com.discord.models.guild.Guild> guilds = storeGuilds.getGuilds();
        hashMap = this.this$0.guildJoinRequests;
        Set keySet = hashMap.keySet();
        m.checkNotNullExpressionValue(keySet, "guildJoinRequests.keys");
        ArrayList arrayList = new ArrayList();
        Iterator it = keySet.iterator();
        while (true) {
            boolean z2 = false;
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            Long l = (Long) next;
            m.checkNotNullExpressionValue(l, "it");
            if (!guilds.containsKey(l)) {
                hashMap2 = this.this$0.pendingGuilds;
                if (!hashMap2.containsKey(l)) {
                    z2 = true;
                }
            }
            if (z2) {
                arrayList.add(next);
            }
        }
        if (!arrayList.isEmpty()) {
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().getUserJoinRequestGuilds(), false, 1, null), this.this$0.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
        }
    }
}
