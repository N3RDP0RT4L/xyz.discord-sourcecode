package com.discord.stores;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guild.Guild;
import com.discord.models.domain.ModelMessageDelete;
import com.discord.models.domain.ModelPayload;
import com.discord.models.message.Message;
import com.discord.models.thread.dto.ModelThreadListSync;
import com.discord.stores.StoreMessagesLoader;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import d0.d0.f;
import d0.t.h0;
import d0.t.r;
import d0.z.d.m;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreThreadMessages.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0092\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 J2\u00020\u0001:\u0002JKB!\u0012\u0006\u0010>\u001a\u00020=\u0012\u0006\u0010A\u001a\u00020@\u0012\b\b\u0002\u0010F\u001a\u00020E¢\u0006\u0004\bH\u0010IJ\u001b\u0010\u0006\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u001b\u0010\n\u001a\u00020\u00052\n\u0010\t\u001a\u00060\u0002j\u0002`\bH\u0003¢\u0006\u0004\b\n\u0010\u0007J\u0017\u0010\r\u001a\u00020\u00052\u0006\u0010\f\u001a\u00020\u000bH\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0010\u001a\u00020\u000fH\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0014\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u000fH\u0003¢\u0006\u0004\b\u0014\u0010\u0015J#\u0010\u0018\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\b\u0012\u0004\u0012\u00020\u00110\u00170\u0016¢\u0006\u0004\b\u0018\u0010\u0019J\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u001b2\n\u0010\u001a\u001a\u00060\u0002j\u0002`\b¢\u0006\u0004\b\u001c\u0010\u001dJ\u0017\u0010 \u001a\u00020\u00052\u0006\u0010\u001f\u001a\u00020\u001eH\u0007¢\u0006\u0004\b \u0010!J\u0017\u0010\"\u001a\u00020\u00052\u0006\u0010\f\u001a\u00020\u000bH\u0007¢\u0006\u0004\b\"\u0010\u000eJ\u001b\u0010#\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0007¢\u0006\u0004\b#\u0010\u0007J\u0017\u0010$\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u000fH\u0007¢\u0006\u0004\b$\u0010\u0015J\u0017\u0010'\u001a\u00020\u00052\u0006\u0010&\u001a\u00020%H\u0007¢\u0006\u0004\b'\u0010(J\u0017\u0010*\u001a\u00020\u00052\u0006\u0010)\u001a\u00020\u000fH\u0007¢\u0006\u0004\b*\u0010\u0015J\u0017\u0010+\u001a\u00020\u00052\u0006\u0010)\u001a\u00020\u000fH\u0007¢\u0006\u0004\b+\u0010\u0015J\u0017\u0010.\u001a\u00020\u00052\u0006\u0010-\u001a\u00020,H\u0007¢\u0006\u0004\b.\u0010/J\u0017\u00100\u001a\u00020\u00052\u0006\u0010-\u001a\u00020,H\u0007¢\u0006\u0004\b0\u0010/J\u0017\u00103\u001a\u00020\u00052\u0006\u00102\u001a\u000201H\u0007¢\u0006\u0004\b3\u00104J\u0017\u00107\u001a\u00020\u00052\u0006\u00106\u001a\u000205H\u0007¢\u0006\u0004\b7\u00108J\u000f\u00109\u001a\u00020\u0005H\u0017¢\u0006\u0004\b9\u0010:R&\u0010;\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\b\u0012\u0004\u0012\u00020\u00110\u00178\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b;\u0010<R\u0016\u0010>\u001a\u00020=8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b>\u0010?R\u0016\u0010A\u001a\u00020@8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bA\u0010BR&\u0010D\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\b\u0012\u0004\u0012\u00020\u00110C8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bD\u0010<R\u0016\u0010F\u001a\u00020E8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bF\u0010G¨\u0006L"}, d2 = {"Lcom/discord/stores/StoreThreadMessages;", "Lcom/discord/stores/StoreV2;", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "deleteForGuild", "(J)V", "Lcom/discord/primitives/ChannelId;", "parentChannelId", "deleteForParentChannel", "Lcom/discord/api/guild/Guild;", "guild", "updateFromGuild", "(Lcom/discord/api/guild/Guild;)V", "Lcom/discord/api/channel/Channel;", "thread", "Lcom/discord/stores/StoreThreadMessages$ThreadState;", "getOrAddState", "(Lcom/discord/api/channel/Channel;)Lcom/discord/stores/StoreThreadMessages$ThreadState;", "updateFromThread", "(Lcom/discord/api/channel/Channel;)V", "Lrx/Observable;", "", "observeThreadCountAndLatestMessage", "()Lrx/Observable;", "channelId", "", "getThreadMessageCount", "(J)Ljava/lang/Integer;", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "handleGuildCreate", "handleGuildDelete", "handleThreadCreateOrUpdate", "Lcom/discord/models/thread/dto/ModelThreadListSync;", "threadListSync", "handleThreadListSync", "(Lcom/discord/models/thread/dto/ModelThreadListSync;)V", "channel", "handleChannelDelete", "handleThreadDelete", "Lcom/discord/api/message/Message;", "message", "handleMessageCreate", "(Lcom/discord/api/message/Message;)V", "handleMessageUpdate", "Lcom/discord/models/domain/ModelMessageDelete;", "messageDeleteBulk", "handleMessageDelete", "(Lcom/discord/models/domain/ModelMessageDelete;)V", "Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;", "chunk", "handleMessagesLoaded", "(Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;)V", "snapshotData", "()V", "threadsSnapshot", "Ljava/util/Map;", "Lcom/discord/stores/StoreMessages;", "storeMessages", "Lcom/discord/stores/StoreMessages;", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreChannels;", "", "threads", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", HookHelper.constructorName, "(Lcom/discord/stores/StoreMessages;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/updates/ObservationDeck;)V", "Companion", "ThreadState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreThreadMessages extends StoreV2 {
    public static final Companion Companion = new Companion(null);
    public static final int MAX_THREAD_MESSAGE_COUNT = 50;
    private final ObservationDeck observationDeck;
    private final StoreChannels storeChannels;
    private final StoreMessages storeMessages;
    private final Map<Long, ThreadState> threads;
    private Map<Long, ThreadState> threadsSnapshot;

    /* compiled from: StoreThreadMessages.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/stores/StoreThreadMessages$Companion;", "", "", "MAX_THREAD_MESSAGE_COUNT", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreThreadMessages.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0011\b\u0086\b\u0018\u00002\u00020\u0001B=\u0012\n\u0010\u000f\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010\u0010\u001a\u00060\u0002j\u0002`\u0006\u0012\n\u0010\u0011\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\u0012\u001a\u00020\t\u0012\b\u0010\u0013\u001a\u0004\u0018\u00010\f¢\u0006\u0004\b*\u0010+J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\u0007\u001a\u00060\u0002j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0005J\u0014\u0010\b\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\b\u0010\u0005J\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0012\u0010\r\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJP\u0010\u0014\u001a\u00020\u00002\f\b\u0002\u0010\u000f\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010\u0010\u001a\u00060\u0002j\u0002`\u00062\f\b\u0002\u0010\u0011\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u0012\u001a\u00020\t2\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\fHÆ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0017\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0019\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\u0019\u0010\u000bJ\u001a\u0010\u001c\u001a\u00020\u001b2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR$\u0010\u0013\u001a\u0004\u0018\u00010\f8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0013\u0010\u001e\u001a\u0004\b\u001f\u0010\u000e\"\u0004\b \u0010!R\u001d\u0010\u000f\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\"\u001a\u0004\b#\u0010\u0005R\"\u0010\u0012\u001a\u00020\t8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0012\u0010$\u001a\u0004\b%\u0010\u000b\"\u0004\b&\u0010'R\u001d\u0010\u0010\u001a\u00060\u0002j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\"\u001a\u0004\b(\u0010\u0005R\u001d\u0010\u0011\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\"\u001a\u0004\b)\u0010\u0005¨\u0006,"}, d2 = {"Lcom/discord/stores/StoreThreadMessages$ThreadState;", "", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "Lcom/discord/primitives/GuildId;", "component2", "component3", "", "component4", "()I", "Lcom/discord/models/message/Message;", "component5", "()Lcom/discord/models/message/Message;", "threadId", "guildId", "parentId", "count", "mostRecentMessage", "copy", "(JJJILcom/discord/models/message/Message;)Lcom/discord/stores/StoreThreadMessages$ThreadState;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/message/Message;", "getMostRecentMessage", "setMostRecentMessage", "(Lcom/discord/models/message/Message;)V", "J", "getThreadId", "I", "getCount", "setCount", "(I)V", "getGuildId", "getParentId", HookHelper.constructorName, "(JJJILcom/discord/models/message/Message;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ThreadState {
        private int count;
        private final long guildId;
        private Message mostRecentMessage;
        private final long parentId;
        private final long threadId;

        public ThreadState(long j, long j2, long j3, int i, Message message) {
            this.threadId = j;
            this.guildId = j2;
            this.parentId = j3;
            this.count = i;
            this.mostRecentMessage = message;
        }

        public static /* synthetic */ ThreadState copy$default(ThreadState threadState, long j, long j2, long j3, int i, Message message, int i2, Object obj) {
            return threadState.copy((i2 & 1) != 0 ? threadState.threadId : j, (i2 & 2) != 0 ? threadState.guildId : j2, (i2 & 4) != 0 ? threadState.parentId : j3, (i2 & 8) != 0 ? threadState.count : i, (i2 & 16) != 0 ? threadState.mostRecentMessage : message);
        }

        public final long component1() {
            return this.threadId;
        }

        public final long component2() {
            return this.guildId;
        }

        public final long component3() {
            return this.parentId;
        }

        public final int component4() {
            return this.count;
        }

        public final Message component5() {
            return this.mostRecentMessage;
        }

        public final ThreadState copy(long j, long j2, long j3, int i, Message message) {
            return new ThreadState(j, j2, j3, i, message);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ThreadState)) {
                return false;
            }
            ThreadState threadState = (ThreadState) obj;
            return this.threadId == threadState.threadId && this.guildId == threadState.guildId && this.parentId == threadState.parentId && this.count == threadState.count && m.areEqual(this.mostRecentMessage, threadState.mostRecentMessage);
        }

        public final int getCount() {
            return this.count;
        }

        public final long getGuildId() {
            return this.guildId;
        }

        public final Message getMostRecentMessage() {
            return this.mostRecentMessage;
        }

        public final long getParentId() {
            return this.parentId;
        }

        public final long getThreadId() {
            return this.threadId;
        }

        public int hashCode() {
            int a = (((b.a(this.parentId) + ((b.a(this.guildId) + (b.a(this.threadId) * 31)) * 31)) * 31) + this.count) * 31;
            Message message = this.mostRecentMessage;
            return a + (message != null ? message.hashCode() : 0);
        }

        public final void setCount(int i) {
            this.count = i;
        }

        public final void setMostRecentMessage(Message message) {
            this.mostRecentMessage = message;
        }

        public String toString() {
            StringBuilder R = a.R("ThreadState(threadId=");
            R.append(this.threadId);
            R.append(", guildId=");
            R.append(this.guildId);
            R.append(", parentId=");
            R.append(this.parentId);
            R.append(", count=");
            R.append(this.count);
            R.append(", mostRecentMessage=");
            R.append(this.mostRecentMessage);
            R.append(")");
            return R.toString();
        }
    }

    public /* synthetic */ StoreThreadMessages(StoreMessages storeMessages, StoreChannels storeChannels, ObservationDeck observationDeck, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(storeMessages, storeChannels, (i & 4) != 0 ? ObservationDeckProvider.get() : observationDeck);
    }

    @StoreThread
    private final void deleteForGuild(long j) {
        if (r.removeAll(this.threads.values(), new StoreThreadMessages$deleteForGuild$removed$1(j))) {
            markChanged();
        }
    }

    @StoreThread
    private final void deleteForParentChannel(long j) {
        if (r.removeAll(this.threads.values(), new StoreThreadMessages$deleteForParentChannel$removed$1(j))) {
            markChanged();
        }
    }

    @StoreThread
    private final ThreadState getOrAddState(Channel channel) {
        ThreadState threadState = (ThreadState) a.c(channel, this.threads);
        if (threadState == null) {
            long h = channel.h();
            long f = channel.f();
            long r = channel.r();
            Integer l = channel.l();
            threadState = new ThreadState(h, f, r, l != null ? l.intValue() : 0, null);
            this.threads.put(Long.valueOf(channel.h()), threadState);
            markChanged();
        }
        return threadState;
    }

    @StoreThread
    private final void updateFromGuild(Guild guild) {
        List<Channel> N = guild.N();
        if (N != null) {
            for (Channel channel : N) {
                updateFromThread(channel);
            }
        }
    }

    @StoreThread
    private final void updateFromThread(Channel channel) {
        ThreadState orAddState = getOrAddState(channel);
        Integer l = channel.l();
        if (l != null) {
            this.threads.put(Long.valueOf(channel.h()), ThreadState.copy$default(orAddState, 0L, 0L, 0L, l.intValue(), null, 23, null));
            markChanged();
        }
        if (channel.i() != 0) {
            Message mostRecentMessage = orAddState.getMostRecentMessage();
            if (mostRecentMessage == null || mostRecentMessage.getId() != channel.i()) {
                this.threads.put(Long.valueOf(channel.h()), ThreadState.copy$default(orAddState, 0L, 0L, 0L, 0, null, 15, null));
                markChanged();
            }
        }
    }

    public final Integer getThreadMessageCount(long j) {
        ThreadState threadState = this.threadsSnapshot.get(Long.valueOf(j));
        if (threadState != null) {
            return Integer.valueOf(threadState.getCount());
        }
        return null;
    }

    @StoreThread
    public final void handleChannelDelete(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        deleteForParentChannel(channel.h());
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        List<Guild> guilds = modelPayload.getGuilds();
        m.checkNotNullExpressionValue(guilds, "payload.guilds");
        for (Guild guild : guilds) {
            updateFromGuild(guild);
        }
    }

    @StoreThread
    public final void handleGuildCreate(Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        updateFromGuild(guild);
    }

    @StoreThread
    public final void handleGuildDelete(long j) {
        deleteForGuild(j);
    }

    @StoreThread
    public final void handleMessageCreate(com.discord.api.message.Message message) {
        m.checkNotNullParameter(message, "message");
        Channel channel = this.storeChannels.getChannel(message.g());
        if (channel != null && ChannelUtils.C(channel)) {
            ThreadState orAddState = getOrAddState(channel);
            this.threads.put(Long.valueOf(channel.h()), ThreadState.copy$default(orAddState, 0L, 0L, 0L, f.coerceAtMost(orAddState.getCount() + 1, 50), new Message(message), 7, null));
            markChanged();
        }
    }

    @StoreThread
    public final void handleMessageDelete(ModelMessageDelete modelMessageDelete) {
        Message mostRecentMessage;
        m.checkNotNullParameter(modelMessageDelete, "messageDeleteBulk");
        ThreadState threadState = this.threads.get(Long.valueOf(modelMessageDelete.getChannelId()));
        Long valueOf = (threadState == null || (mostRecentMessage = threadState.getMostRecentMessage()) == null) ? null : Long.valueOf(mostRecentMessage.getId());
        if (valueOf != null) {
            valueOf.longValue();
            for (Long l : modelMessageDelete.getMessageIds()) {
                if (!(!m.areEqual(valueOf, l))) {
                    this.threads.put(Long.valueOf(modelMessageDelete.getChannelId()), ThreadState.copy$default(threadState, 0L, 0L, 0L, 0, null, 15, null));
                    markChanged();
                    return;
                }
            }
        }
    }

    @StoreThread
    public final void handleMessageUpdate(com.discord.api.message.Message message) {
        Message mostRecentMessage;
        m.checkNotNullParameter(message, "message");
        ThreadState threadState = this.threads.get(Long.valueOf(message.g()));
        if (threadState != null && (mostRecentMessage = threadState.getMostRecentMessage()) != null && mostRecentMessage.getId() == message.o()) {
            this.threads.put(Long.valueOf(message.g()), ThreadState.copy$default(threadState, 0L, 0L, 0L, 0, this.storeMessages.getMessage(message.g(), message.o()), 15, null));
            markChanged();
        }
    }

    @StoreThread
    public final void handleMessagesLoaded(StoreMessagesLoader.ChannelChunk channelChunk) {
        Channel channel;
        boolean z2;
        Channel channel2;
        m.checkNotNullParameter(channelChunk, "chunk");
        boolean z3 = false;
        boolean z4 = false;
        for (Message message : channelChunk.getMessages()) {
            if (message.hasThread() && !this.threads.containsKey(Long.valueOf(message.getId())) && (channel2 = this.storeChannels.getThreadsByIdInternal$app_productionGoogleRelease().get(Long.valueOf(message.getId()))) != null) {
                updateFromThread(channel2);
                z4 = true;
            }
        }
        if (z4) {
            markChanged();
        }
        if (channelChunk.isPresent() && (channel = this.storeChannels.getThreadsByIdInternal$app_productionGoogleRelease().get(Long.valueOf(channelChunk.getChannelId()))) != null && ChannelUtils.C(channel)) {
            ThreadState orAddState = getOrAddState(channel);
            if (channelChunk.getMessages().isEmpty()) {
                this.threads.put(Long.valueOf(channel.h()), ThreadState.copy$default(orAddState, 0L, 0L, 0L, 0, null, 7, null));
            } else {
                Message message2 = this.storeMessages.getMessage(channelChunk.getChannelId(), channelChunk.getMessages().get(0).getId());
                int size = channelChunk.getMessages().size();
                List<Message> messages = channelChunk.getMessages();
                if (!(messages instanceof Collection) || !messages.isEmpty()) {
                    Iterator<T> it = messages.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        Integer type = ((Message) it.next()).getType();
                        if (type != null && type.intValue() == 21) {
                            z2 = true;
                            continue;
                        } else {
                            z2 = false;
                            continue;
                        }
                        if (z2) {
                            z3 = true;
                            break;
                        }
                    }
                }
                if (z3) {
                    size--;
                }
                Integer type2 = message2 != null ? message2.getType() : null;
                this.threads.put(Long.valueOf(channel.h()), ThreadState.copy$default(orAddState, 0L, 0L, 0L, f.coerceAtMost(size, 50), (type2 != null && type2.intValue() == 21) ? null : message2, 7, null));
            }
            markChanged();
        }
    }

    @StoreThread
    public final void handleThreadCreateOrUpdate(Channel channel) {
        m.checkNotNullParameter(channel, "thread");
        updateFromThread(channel);
    }

    @StoreThread
    public final void handleThreadDelete(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        if (this.threads.containsKey(Long.valueOf(channel.h()))) {
            this.threads.remove(Long.valueOf(channel.h()));
            markChanged();
        }
    }

    @StoreThread
    public final void handleThreadListSync(ModelThreadListSync modelThreadListSync) {
        m.checkNotNullParameter(modelThreadListSync, "threadListSync");
        for (Channel channel : modelThreadListSync.getThreads()) {
            updateFromThread(channel);
        }
        List<com.discord.api.message.Message> mostRecentMessages = modelThreadListSync.getMostRecentMessages();
        if (mostRecentMessages != null) {
            for (com.discord.api.message.Message message : mostRecentMessages) {
                ThreadState threadState = this.threads.get(Long.valueOf(message.g()));
                if (threadState != null) {
                    this.threads.put(Long.valueOf(message.g()), ThreadState.copy$default(threadState, 0L, 0L, 0L, 0, new Message(message), 15, null));
                }
            }
        }
    }

    public final Observable<Map<Long, ThreadState>> observeThreadCountAndLatestMessage() {
        Observable<Map<Long, ThreadState>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreThreadMessages$observeThreadCountAndLatestMessage$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        this.threadsSnapshot = new HashMap(this.threads);
    }

    public StoreThreadMessages(StoreMessages storeMessages, StoreChannels storeChannels, ObservationDeck observationDeck) {
        m.checkNotNullParameter(storeMessages, "storeMessages");
        m.checkNotNullParameter(storeChannels, "storeChannels");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.storeMessages = storeMessages;
        this.storeChannels = storeChannels;
        this.observationDeck = observationDeck;
        this.threads = new HashMap();
        this.threadsSnapshot = h0.emptyMap();
    }
}
