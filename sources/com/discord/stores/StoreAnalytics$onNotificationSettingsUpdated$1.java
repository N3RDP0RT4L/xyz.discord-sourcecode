package com.discord.stores;

import com.discord.api.channel.Channel;
import com.discord.models.domain.ModelNotificationSettings;
import com.discord.utilities.analytics.AnalyticsTracker;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreAnalytics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAnalytics$onNotificationSettingsUpdated$1 extends o implements Function0<Unit> {
    public final /* synthetic */ Long $channelId;
    public final /* synthetic */ ModelNotificationSettings $notifSettings;
    public final /* synthetic */ StoreAnalytics this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreAnalytics$onNotificationSettingsUpdated$1(StoreAnalytics storeAnalytics, Long l, ModelNotificationSettings modelNotificationSettings) {
        super(0);
        this.this$0 = storeAnalytics;
        this.$channelId = l;
        this.$notifSettings = modelNotificationSettings;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreStream storeStream;
        boolean z2;
        Long l = this.$channelId;
        Channel channel = null;
        if (l != null) {
            long longValue = l.longValue();
            List<ModelNotificationSettings.ChannelOverride> channelOverrides = this.$notifSettings.getChannelOverrides();
            m.checkNotNullExpressionValue(channelOverrides, "notifSettings.channelOverrides");
            boolean z3 = true;
            if (!(channelOverrides instanceof Collection) || !channelOverrides.isEmpty()) {
                for (ModelNotificationSettings.ChannelOverride channelOverride : channelOverrides) {
                    m.checkNotNullExpressionValue(channelOverride, "it");
                    if (channelOverride.getChannelId() == longValue) {
                        z2 = true;
                        continue;
                    } else {
                        z2 = false;
                        continue;
                    }
                    if (z2) {
                        break;
                    }
                }
            }
            z3 = false;
            if (!z3) {
                l = null;
            }
            if (l != null) {
                long longValue2 = l.longValue();
                storeStream = this.this$0.stores;
                channel = storeStream.getChannels$app_productionGoogleRelease().findChannelByIdInternal$app_productionGoogleRelease(longValue2);
            }
        }
        AnalyticsTracker.INSTANCE.notificationSettingsUpdated(this.$notifSettings, channel);
    }
}
