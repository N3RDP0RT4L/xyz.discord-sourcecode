package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreLurking;
import d0.z.d.m;
import java.util.Set;
import kotlin.Metadata;
import rx.functions.Func2;
/* compiled from: StoreLurking.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\n\u001a\n \u0003*\u0004\u0018\u00010\u00070\u00072\"\u0010\u0004\u001a\u001e\u0012\b\u0012\u00060\u0001j\u0002`\u0002 \u0003*\u000e\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0018\u00010\u00000\u00002\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\n¢\u0006\u0004\b\b\u0010\t"}, d2 = {"", "", "Lcom/discord/primitives/GuildId;", "kotlin.jvm.PlatformType", "lurkingGuildIds", "Lcom/discord/models/guild/Guild;", "guild", "", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Set;Lcom/discord/models/guild/Guild;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreLurking$isLurkingObs$1<T1, T2, R> implements Func2<Set<? extends Long>, Guild, Boolean> {
    public static final StoreLurking$isLurkingObs$1 INSTANCE = new StoreLurking$isLurkingObs$1();

    @Override // rx.functions.Func2
    public /* bridge */ /* synthetic */ Boolean call(Set<? extends Long> set, Guild guild) {
        return call2((Set<Long>) set, guild);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final Boolean call2(Set<Long> set, Guild guild) {
        boolean z2 = false;
        if (guild != null) {
            StoreLurking.Companion companion = StoreLurking.Companion;
            if (guild.getJoinedAt() != null) {
                z2 = true;
            }
            long id2 = guild.getId();
            m.checkNotNullExpressionValue(set, "lurkingGuildIds");
            z2 = companion.isLurking(z2, id2, set);
        }
        return Boolean.valueOf(z2);
    }
}
