package com.discord.stores;

import com.discord.api.stageinstance.RecommendedStageInstance;
import com.discord.stores.StoreRequestedStageChannels;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreRequestedStageChannels.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreRequestedStageChannels$fetchStageChannels$1 extends o implements Function0<Unit> {
    public final /* synthetic */ Set $channelIds;
    public final /* synthetic */ StoreRequestedStageChannels this$0;

    /* compiled from: StoreRequestedStageChannels.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreRequestedStageChannels$fetchStageChannels$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<Error, Unit> {
        public final /* synthetic */ Set $channelIdsToRequest;

        /* compiled from: StoreRequestedStageChannels.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreRequestedStageChannels$fetchStageChannels$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C02121 extends o implements Function0<Unit> {
            public C02121() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                AnonymousClass1 r0 = AnonymousClass1.this;
                StoreRequestedStageChannels$fetchStageChannels$1.this.this$0.updateStatus(r0.$channelIdsToRequest, StoreRequestedStageChannels.FetchStatus.ERROR);
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Set set) {
            super(1);
            this.$channelIdsToRequest = set;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Error error) {
            invoke2(error);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Error error) {
            Dispatcher dispatcher;
            m.checkNotNullParameter(error, "it");
            dispatcher = StoreRequestedStageChannels$fetchStageChannels$1.this.this$0.dispatcher;
            dispatcher.schedule(new C02121());
        }
    }

    /* compiled from: StoreRequestedStageChannels.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/api/stageinstance/RecommendedStageInstance;", "stageInstances", "", "invoke", "(Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreRequestedStageChannels$fetchStageChannels$1$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2 extends o implements Function1<List<? extends RecommendedStageInstance>, Unit> {
        public final /* synthetic */ Set $channelIdsToRequest;

        /* compiled from: StoreRequestedStageChannels.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreRequestedStageChannels$fetchStageChannels$1$2$1  reason: invalid class name */
        /* loaded from: classes.dex */
        public static final class AnonymousClass1 extends o implements Function0<Unit> {
            public final /* synthetic */ List $stageInstances;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public AnonymousClass1(List list) {
                super(0);
                this.$stageInstances = list;
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                AnonymousClass2 r0 = AnonymousClass2.this;
                StoreRequestedStageChannels$fetchStageChannels$1.this.this$0.onLoaded(r0.$channelIdsToRequest, this.$stageInstances);
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass2(Set set) {
            super(1);
            this.$channelIdsToRequest = set;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(List<? extends RecommendedStageInstance> list) {
            invoke2((List<RecommendedStageInstance>) list);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(List<RecommendedStageInstance> list) {
            Dispatcher dispatcher;
            m.checkNotNullParameter(list, "stageInstances");
            dispatcher = StoreRequestedStageChannels$fetchStageChannels$1.this.this$0.dispatcher;
            dispatcher.schedule(new AnonymousClass1(list));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreRequestedStageChannels$fetchStageChannels$1(StoreRequestedStageChannels storeRequestedStageChannels, Set set) {
        super(0);
        this.this$0 = storeRequestedStageChannels;
        this.$channelIds = set;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        RestAPI restAPI;
        Map map;
        Set set = this.$channelIds;
        ArrayList arrayList = new ArrayList();
        Iterator it = set.iterator();
        while (true) {
            boolean z2 = false;
            if (it.hasNext()) {
                Object next = it.next();
                long longValue = ((Number) next).longValue();
                map = this.this$0.requestedStageChannelsSnapshot;
                StoreRequestedStageChannels.StageInstanceState stageInstanceState = (StoreRequestedStageChannels.StageInstanceState) map.get(Long.valueOf(longValue));
                if (stageInstanceState == null || !stageInstanceState.isLoading()) {
                    z2 = true;
                }
                if (z2) {
                    arrayList.add(next);
                }
            } else {
                Set<Long> set2 = u.toSet(arrayList);
                this.this$0.updateStatus(set2, StoreRequestedStageChannels.FetchStatus.LOADING);
                restAPI = this.this$0.restAPI;
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(restAPI.getStageInstancesForChannels(set2), false, 1, null), this.this$0.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new AnonymousClass1(set2), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2(set2));
                return;
            }
        }
    }
}
