package com.discord.stores;

import kotlin.Metadata;
/* compiled from: StoreEmojiCustom.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010%\n\u0002\b\u0002*(\b\u0002\u0010\u0004\"\u000e\u0012\u0004\u0012\u0002`\u0001\u0012\u0004\u0012\u00020\u00020\u00002\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0001\u0012\u0004\u0012\u00020\u00020\u0000*(\b\u0002\u0010\u0006\"\u000e\u0012\u0004\u0012\u0002`\u0001\u0012\u0004\u0012\u00020\u00020\u00052\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0001\u0012\u0004\u0012\u00020\u00020\u0005¨\u0006\u0007"}, d2 = {"", "Lcom/discord/primitives/EmojiId;", "Lcom/discord/models/domain/emoji/ModelEmojiCustom;", "", "EmojiMap", "", "EmojiMutableMap", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreEmojiCustomKt {
}
