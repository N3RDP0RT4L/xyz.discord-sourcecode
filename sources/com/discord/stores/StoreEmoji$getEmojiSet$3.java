package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.stores.StoreMediaFavorites;
import com.discord.utilities.Quad;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import rx.functions.Func4;
/* compiled from: StoreEmoji.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u000e\u001aº\u0001\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00000\u0000\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00000\u0000\u0012 \u0012\u001e\u0012\b\u0012\u00060\u0005j\u0002`\u0006 \u0001*\u000e\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0018\u00010\u00040\u0004\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\t \u0001*\n\u0012\u0004\u0012\u00020\t\u0018\u00010\b0\b \u0001*\\\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00000\u0000\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00000\u0000\u0012 \u0012\u001e\u0012\b\u0012\u00060\u0005j\u0002`\u0006 \u0001*\u000e\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0018\u00010\u00040\u0004\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\t \u0001*\n\u0012\u0004\u0012\u00020\t\u0018\u00010\b0\b\u0018\u00010\u000b0\u000b2\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0003\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\"\u0010\u0007\u001a\u001e\u0012\b\u0012\u00060\u0005j\u0002`\u0006 \u0001*\u000e\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0018\u00010\u00040\u00042\u001a\u0010\n\u001a\u0016\u0012\u0004\u0012\u00020\t \u0001*\n\u0012\u0004\u0012\u00020\t\u0018\u00010\b0\bH\n¢\u0006\u0004\b\f\u0010\r"}, d2 = {"", "kotlin.jvm.PlatformType", "isPremium", "hasExternalEmojiPermission", "", "", "Lcom/discord/primitives/GuildId;", "sortedGuildIds", "", "Lcom/discord/stores/StoreMediaFavorites$Favorite;", "favorites", "Lcom/discord/utilities/Quad;", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;Ljava/util/Set;)Lcom/discord/utilities/Quad;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreEmoji$getEmojiSet$3<T1, T2, T3, T4, R> implements Func4<Boolean, Boolean, List<? extends Long>, Set<? extends StoreMediaFavorites.Favorite>, Quad<? extends Boolean, ? extends Boolean, ? extends List<? extends Long>, ? extends Set<? extends StoreMediaFavorites.Favorite>>> {
    public static final StoreEmoji$getEmojiSet$3 INSTANCE = new StoreEmoji$getEmojiSet$3();

    @Override // rx.functions.Func4
    public /* bridge */ /* synthetic */ Quad<? extends Boolean, ? extends Boolean, ? extends List<? extends Long>, ? extends Set<? extends StoreMediaFavorites.Favorite>> call(Boolean bool, Boolean bool2, List<? extends Long> list, Set<? extends StoreMediaFavorites.Favorite> set) {
        return call2(bool, bool2, (List<Long>) list, set);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final Quad<Boolean, Boolean, List<Long>, Set<StoreMediaFavorites.Favorite>> call2(Boolean bool, Boolean bool2, List<Long> list, Set<? extends StoreMediaFavorites.Favorite> set) {
        return new Quad<>(bool, bool2, list, set);
    }
}
