package com.discord.stores;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.user.NsfwAllowance;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.stores.StoreSearch;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.search.history.MGPreferenceSearchHistoryCache;
import com.discord.utilities.search.history.SearchHistoryCache;
import com.discord.utilities.search.network.SearchFetcher;
import com.discord.utilities.search.query.node.QueryNode;
import com.discord.utilities.search.strings.SearchStringProvider;
import com.discord.utilities.search.suggestion.SearchSuggestionEngine;
import com.discord.widgets.chat.AutocompleteSelectionTypes;
import d0.t.n;
import d0.z.d.m;
import j0.l.e.k;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import rx.Observable;
import rx.Subscription;
import rx.subjects.BehaviorSubject;
import rx.subjects.SerializedSubject;
/* compiled from: StoreSearch.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Ì\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u001e\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0002bcBG\u0012\u0006\u0010?\u001a\u00020>\u0012\u0006\u0010N\u001a\u00020M\u0012\u0006\u00105\u001a\u000204\u0012\u0006\u0010;\u001a\u00020:\u0012\u0006\u0010T\u001a\u00020S\u0012\u0006\u0010G\u001a\u00020F\u0012\u0006\u0010W\u001a\u00020V\u0012\u0006\u0010D\u001a\u00020C¢\u0006\u0004\bY\u0010ZBA\b\u0016\u0012\u0006\u0010;\u001a\u00020:\u0012\u0006\u0010T\u001a\u00020S\u0012\u0006\u0010G\u001a\u00020F\u0012\u0006\u0010W\u001a\u00020V\u0012\u0006\u0010\\\u001a\u00020[\u0012\u0006\u0010^\u001a\u00020]\u0012\u0006\u0010`\u001a\u00020_¢\u0006\u0004\bY\u0010aJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0019\u0010\u000b\u001a\u00020\u00062\b\u0010\n\u001a\u0004\u0018\u00010\tH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0019\u0010\r\u001a\u00020\u00062\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0011\u001a\u00020\u00062\u0006\u0010\u0010\u001a\u00020\u000fH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u0013\u0010\u0014\u001a\u00020\u0013*\u00020\u0002H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u0013\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0016¢\u0006\u0004\b\u0017\u0010\u0018J!\u0010\u001c\u001a\u00020\u00062\n\u0010\u001b\u001a\u00060\u0019j\u0002`\u001a2\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u001c\u0010\u001dJ!\u0010 \u001a\u00020\u00062\n\u0010\u001f\u001a\u00060\u0019j\u0002`\u001e2\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b \u0010\u001dJ\r\u0010!\u001a\u00020\u0006¢\u0006\u0004\b!\u0010\"J\u001d\u0010%\u001a\u00020\u00062\u0006\u0010$\u001a\u00020#2\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b%\u0010&J\u0019\u0010)\u001a\u00020\u00062\n\u0010(\u001a\u00060\u0019j\u0002`'¢\u0006\u0004\b)\u0010*J\u001f\u0010.\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020-0,0+0\u0016¢\u0006\u0004\b.\u0010\u0018J\r\u0010/\u001a\u00020\u0006¢\u0006\u0004\b/\u0010\"J%\u00103\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\f\u00100\u001a\b\u0012\u0004\u0012\u00020-0,H\u0000¢\u0006\u0004\b1\u00102R\u0019\u00105\u001a\u0002048\u0006@\u0006¢\u0006\f\n\u0004\b5\u00106\u001a\u0004\b7\u00108R\u0018\u0010\n\u001a\u0004\u0018\u00010\t8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\n\u00109R\u0016\u0010;\u001a\u00020:8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b;\u0010<R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0003\u0010=R\u0019\u0010?\u001a\u00020>8\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010@\u001a\u0004\bA\u0010BR\u0016\u0010D\u001a\u00020C8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bD\u0010ER\u0016\u0010G\u001a\u00020F8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bG\u0010HR2\u0010K\u001a\u001e\u0012\f\u0012\n J*\u0004\u0018\u00010\u000f0\u000f\u0012\f\u0012\n J*\u0004\u0018\u00010\u000f0\u000f0I8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bK\u0010LR\u0019\u0010N\u001a\u00020M8\u0006@\u0006¢\u0006\f\n\u0004\bN\u0010O\u001a\u0004\bP\u0010QR2\u0010R\u001a\u001e\u0012\f\u0012\n J*\u0004\u0018\u00010\u00020\u0002\u0012\f\u0012\n J*\u0004\u0018\u00010\u00020\u00020I8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bR\u0010LR\u0016\u0010T\u001a\u00020S8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bT\u0010UR\u0016\u0010W\u001a\u00020V8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bW\u0010X¨\u0006d"}, d2 = {"Lcom/discord/stores/StoreSearch;", "", "Lcom/discord/stores/StoreSearch$SearchTarget;", "searchTarget", "Lcom/discord/utilities/search/strings/SearchStringProvider;", "searchStringProvider", "", "init", "(Lcom/discord/stores/StoreSearch$SearchTarget;Lcom/discord/utilities/search/strings/SearchStringProvider;)V", "Lrx/Subscription;", Traits.Payment.Type.SUBSCRIPTION, "handleSubscription", "(Lrx/Subscription;)V", "updateTarget", "(Lcom/discord/stores/StoreSearch$SearchTarget;)V", "Lcom/discord/stores/StoreSearch$DisplayState;", "displayState", "onStateChanged", "(Lcom/discord/stores/StoreSearch$DisplayState;)V", "", "includeNsfw", "(Lcom/discord/stores/StoreSearch$SearchTarget;)Z", "Lrx/Observable;", "getDisplayState", "()Lrx/Observable;", "", "Lcom/discord/primitives/ChannelId;", "channelId", "initForChannel", "(JLcom/discord/utilities/search/strings/SearchStringProvider;)V", "Lcom/discord/primitives/GuildId;", "guildId", "initForGuild", "clear", "()V", "", "queryString", "loadInitial", "(Ljava/lang/String;Lcom/discord/utilities/search/strings/SearchStringProvider;)V", "Lcom/discord/primitives/MessageId;", "oldestMessageId", "loadMore", "(J)V", "", "", "Lcom/discord/utilities/search/query/node/QueryNode;", "getHistory", "clearHistory", "query", "persistQuery$app_productionGoogleRelease", "(Lcom/discord/stores/StoreSearch$SearchTarget;Ljava/util/List;)V", "persistQuery", "Lcom/discord/stores/StoreSearchQuery;", "storeSearchQuery", "Lcom/discord/stores/StoreSearchQuery;", "getStoreSearchQuery", "()Lcom/discord/stores/StoreSearchQuery;", "Lrx/Subscription;", "Lcom/discord/stores/StoreStream;", "stream", "Lcom/discord/stores/StoreStream;", "Lcom/discord/stores/StoreSearch$SearchTarget;", "Lcom/discord/stores/StoreSearchData;", "storeSearchData", "Lcom/discord/stores/StoreSearchData;", "getStoreSearchData", "()Lcom/discord/stores/StoreSearchData;", "Lcom/discord/utilities/search/history/SearchHistoryCache;", "historyCache", "Lcom/discord/utilities/search/history/SearchHistoryCache;", "Lcom/discord/stores/StoreGuildsNsfw;", "storeGuildsNsfw", "Lcom/discord/stores/StoreGuildsNsfw;", "Lrx/subjects/SerializedSubject;", "kotlin.jvm.PlatformType", "displayStateSubject", "Lrx/subjects/SerializedSubject;", "Lcom/discord/stores/StoreSearchInput;", "storeSearchInput", "Lcom/discord/stores/StoreSearchInput;", "getStoreSearchInput", "()Lcom/discord/stores/StoreSearchInput;", "searchTargetSubject", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreUser;", HookHelper.constructorName, "(Lcom/discord/stores/StoreSearchData;Lcom/discord/stores/StoreSearchInput;Lcom/discord/stores/StoreSearchQuery;Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreGuildsNsfw;Lcom/discord/stores/StoreUser;Lcom/discord/utilities/search/history/SearchHistoryCache;)V", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreGuildsNsfw;Lcom/discord/stores/StoreUser;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;)V", "DisplayState", "SearchTarget", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreSearch {
    private final Dispatcher dispatcher;
    private final SerializedSubject<DisplayState, DisplayState> displayStateSubject;
    private final SearchHistoryCache historyCache;
    private SearchTarget searchTarget;
    private final SerializedSubject<SearchTarget, SearchTarget> searchTargetSubject;
    private final StoreGuildsNsfw storeGuildsNsfw;
    private final StoreSearchData storeSearchData;
    private final StoreSearchInput storeSearchInput;
    private final StoreSearchQuery storeSearchQuery;
    private final StoreUser storeUser;
    private final StoreStream stream;
    private Subscription subscription;

    /* compiled from: StoreSearch.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/stores/StoreSearch$DisplayState;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "SUGGESTIONS", "RESULTS", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum DisplayState {
        SUGGESTIONS,
        RESULTS
    }

    /* compiled from: StoreSearch.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u001cB\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0016\u001a\u0004\b\u0017\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0018\u001a\u0004\b\u0019\u0010\u0004¨\u0006\u001d"}, d2 = {"Lcom/discord/stores/StoreSearch$SearchTarget;", "", "Lcom/discord/stores/StoreSearch$SearchTarget$Type;", "component1", "()Lcom/discord/stores/StoreSearch$SearchTarget$Type;", "", "component2", "()J", "type", ModelAuditLogEntry.CHANGE_KEY_ID, "copy", "(Lcom/discord/stores/StoreSearch$SearchTarget$Type;J)Lcom/discord/stores/StoreSearch$SearchTarget;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getId", "Lcom/discord/stores/StoreSearch$SearchTarget$Type;", "getType", HookHelper.constructorName, "(Lcom/discord/stores/StoreSearch$SearchTarget$Type;J)V", "Type", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class SearchTarget {

        /* renamed from: id  reason: collision with root package name */
        private final long f2784id;
        private final Type type;

        /* compiled from: StoreSearch.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/stores/StoreSearch$SearchTarget$Type;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "GUILD", AutocompleteSelectionTypes.CHANNEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public enum Type {
            GUILD,
            CHANNEL
        }

        public SearchTarget(Type type, long j) {
            m.checkNotNullParameter(type, "type");
            this.type = type;
            this.f2784id = j;
        }

        public static /* synthetic */ SearchTarget copy$default(SearchTarget searchTarget, Type type, long j, int i, Object obj) {
            if ((i & 1) != 0) {
                type = searchTarget.type;
            }
            if ((i & 2) != 0) {
                j = searchTarget.f2784id;
            }
            return searchTarget.copy(type, j);
        }

        public final Type component1() {
            return this.type;
        }

        public final long component2() {
            return this.f2784id;
        }

        public final SearchTarget copy(Type type, long j) {
            m.checkNotNullParameter(type, "type");
            return new SearchTarget(type, j);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof SearchTarget)) {
                return false;
            }
            SearchTarget searchTarget = (SearchTarget) obj;
            return m.areEqual(this.type, searchTarget.type) && this.f2784id == searchTarget.f2784id;
        }

        public final long getId() {
            return this.f2784id;
        }

        public final Type getType() {
            return this.type;
        }

        public int hashCode() {
            Type type = this.type;
            return b.a(this.f2784id) + ((type != null ? type.hashCode() : 0) * 31);
        }

        public String toString() {
            StringBuilder R = a.R("SearchTarget(type=");
            R.append(this.type);
            R.append(", id=");
            return a.B(R, this.f2784id, ")");
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            SearchTarget.Type.values();
            int[] iArr = new int[2];
            $EnumSwitchMapping$0 = iArr;
            iArr[SearchTarget.Type.GUILD.ordinal()] = 1;
            iArr[SearchTarget.Type.CHANNEL.ordinal()] = 2;
        }
    }

    public StoreSearch(StoreSearchData storeSearchData, StoreSearchInput storeSearchInput, StoreSearchQuery storeSearchQuery, StoreStream storeStream, Dispatcher dispatcher, StoreGuildsNsfw storeGuildsNsfw, StoreUser storeUser, SearchHistoryCache searchHistoryCache) {
        m.checkNotNullParameter(storeSearchData, "storeSearchData");
        m.checkNotNullParameter(storeSearchInput, "storeSearchInput");
        m.checkNotNullParameter(storeSearchQuery, "storeSearchQuery");
        m.checkNotNullParameter(storeStream, "stream");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(storeGuildsNsfw, "storeGuildsNsfw");
        m.checkNotNullParameter(storeUser, "storeUser");
        m.checkNotNullParameter(searchHistoryCache, "historyCache");
        this.storeSearchData = storeSearchData;
        this.storeSearchInput = storeSearchInput;
        this.storeSearchQuery = storeSearchQuery;
        this.stream = storeStream;
        this.dispatcher = dispatcher;
        this.storeGuildsNsfw = storeGuildsNsfw;
        this.storeUser = storeUser;
        this.historyCache = searchHistoryCache;
        this.displayStateSubject = new SerializedSubject<>(BehaviorSubject.l0(DisplayState.SUGGESTIONS));
        this.searchTargetSubject = new SerializedSubject<>(BehaviorSubject.k0());
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void handleSubscription(Subscription subscription) {
        Subscription subscription2 = this.subscription;
        if (subscription2 != null) {
            subscription2.unsubscribe();
        }
        this.subscription = subscription;
    }

    private final boolean includeNsfw(SearchTarget searchTarget) {
        if (this.storeUser.getMe().getNsfwAllowance() == NsfwAllowance.DISALLOWED) {
            return false;
        }
        int ordinal = searchTarget.getType().ordinal();
        if (ordinal == 0) {
            return this.storeGuildsNsfw.isGuildNsfwGateAgreed(searchTarget.getId());
        }
        if (ordinal == 1) {
            return true;
        }
        throw new NoWhenBranchMatchedException();
    }

    private final synchronized void init(SearchTarget searchTarget, SearchStringProvider searchStringProvider) {
        if (!m.areEqual(this.searchTarget, searchTarget)) {
            updateTarget(searchTarget);
            this.storeSearchData.init(searchTarget);
            this.storeSearchInput.init(searchStringProvider);
            Observable H = Observable.H(this.storeSearchQuery.getState().x(StoreSearch$init$1.INSTANCE).F(StoreSearch$init$2.INSTANCE), this.storeSearchInput.getCurrentParsedInput().F(StoreSearch$init$3.INSTANCE));
            m.checkNotNullExpressionValue(H, "Observable\n        .merg…splayState.SUGGESTIONS })");
            ObservableExtensionsKt.appSubscribe(H, getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new StoreSearch$init$4(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreSearch$init$5(this));
            ObservableExtensionsKt.appSubscribe(this.storeSearchQuery.getState(), getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreSearch$init$6(this));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onStateChanged(DisplayState displayState) {
        this.displayStateSubject.k.onNext(displayState);
    }

    private final synchronized void updateTarget(SearchTarget searchTarget) {
        this.searchTarget = searchTarget;
        this.searchTargetSubject.k.onNext(searchTarget);
    }

    public final synchronized void clear() {
        updateTarget(null);
        handleSubscription(null);
        onStateChanged(DisplayState.SUGGESTIONS);
        this.storeSearchQuery.clear();
        this.storeSearchInput.clear();
        this.storeSearchData.clear();
    }

    public final void clearHistory() {
        SearchHistoryCache searchHistoryCache = this.historyCache;
        SearchTarget searchTarget = this.searchTarget;
        if (searchTarget != null) {
            searchHistoryCache.clear(searchTarget);
        }
    }

    public final Observable<DisplayState> getDisplayState() {
        Observable<DisplayState> q = ObservableExtensionsKt.computationLatest(this.displayStateSubject).q();
        m.checkNotNullExpressionValue(q, "displayStateSubject\n    …  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Collection<List<QueryNode>>> getHistory() {
        Observable Y = this.searchTargetSubject.Y(new j0.k.b<SearchTarget, Observable<? extends Collection<? extends List<? extends QueryNode>>>>() { // from class: com.discord.stores.StoreSearch$getHistory$1
            public final Observable<? extends Collection<List<QueryNode>>> call(StoreSearch.SearchTarget searchTarget) {
                SearchHistoryCache searchHistoryCache;
                if (searchTarget == null) {
                    return new k(n.emptyList());
                }
                searchHistoryCache = StoreSearch.this.historyCache;
                return searchHistoryCache.getHistory(searchTarget);
            }
        });
        m.checkNotNullExpressionValue(Y, "searchTargetSubject\n    …())\n          }\n        }");
        return Y;
    }

    public final StoreSearchData getStoreSearchData() {
        return this.storeSearchData;
    }

    public final StoreSearchInput getStoreSearchInput() {
        return this.storeSearchInput;
    }

    public final StoreSearchQuery getStoreSearchQuery() {
        return this.storeSearchQuery;
    }

    public final void initForChannel(long j, SearchStringProvider searchStringProvider) {
        m.checkNotNullParameter(searchStringProvider, "searchStringProvider");
        init(new SearchTarget(SearchTarget.Type.CHANNEL, j), searchStringProvider);
        SearchSuggestionEngine.INSTANCE.setTargetGuildId(null);
    }

    public final void initForGuild(long j, SearchStringProvider searchStringProvider) {
        m.checkNotNullParameter(searchStringProvider, "searchStringProvider");
        init(new SearchTarget(SearchTarget.Type.GUILD, j), searchStringProvider);
        SearchSuggestionEngine.INSTANCE.setTargetGuildId(Long.valueOf(j));
    }

    public final void loadInitial(String str, SearchStringProvider searchStringProvider) {
        m.checkNotNullParameter(str, "queryString");
        m.checkNotNullParameter(searchStringProvider, "searchStringProvider");
        SearchTarget searchTarget = this.searchTarget;
        if (searchTarget != null) {
            this.storeSearchQuery.parseAndQuery(this, searchTarget, str, searchStringProvider, includeNsfw(searchTarget));
        }
    }

    public final void loadMore(long j) {
        SearchTarget searchTarget = this.searchTarget;
        if (searchTarget != null) {
            this.storeSearchQuery.loadMore(searchTarget, j);
        }
    }

    public final void persistQuery$app_productionGoogleRelease(SearchTarget searchTarget, List<? extends QueryNode> list) {
        m.checkNotNullParameter(searchTarget, "searchTarget");
        m.checkNotNullParameter(list, "query");
        this.historyCache.persistQuery(searchTarget, list);
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public StoreSearch(StoreStream storeStream, Dispatcher dispatcher, StoreGuildsNsfw storeGuildsNsfw, StoreUser storeUser, ObservationDeck observationDeck, StoreChannels storeChannels, StoreGuilds storeGuilds) {
        this(new StoreSearchData(observationDeck, storeChannels, storeUser, storeGuilds), new StoreSearchInput(), new StoreSearchQuery(new SearchFetcher()), storeStream, dispatcher, storeGuildsNsfw, storeUser, new MGPreferenceSearchHistoryCache());
        m.checkNotNullParameter(storeStream, "stream");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(storeGuildsNsfw, "storeGuildsNsfw");
        m.checkNotNullParameter(storeUser, "storeUser");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(storeChannels, "storeChannels");
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
    }
}
