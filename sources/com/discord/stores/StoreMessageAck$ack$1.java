package com.discord.stores;

import com.discord.api.channel.Channel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreMessageAck.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessageAck$ack$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ boolean $clearLock;
    public final /* synthetic */ boolean $isLockedAck;
    public final /* synthetic */ StoreMessageAck this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreMessageAck$ack$1(StoreMessageAck storeMessageAck, long j, boolean z2, boolean z3) {
        super(0);
        this.this$0 = storeMessageAck;
        this.$channelId = j;
        this.$isLockedAck = z2;
        this.$clearLock = z3;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreStream storeStream;
        StoreStream storeStream2;
        storeStream = this.this$0.stream;
        Channel findChannelByIdInternal$app_productionGoogleRelease = storeStream.getChannels$app_productionGoogleRelease().findChannelByIdInternal$app_productionGoogleRelease(this.$channelId);
        if (findChannelByIdInternal$app_productionGoogleRelease == null) {
            return;
        }
        if (findChannelByIdInternal$app_productionGoogleRelease.A() != 4) {
            this.this$0.internalAck(findChannelByIdInternal$app_productionGoogleRelease, this.$isLockedAck, this.$clearLock);
            return;
        }
        storeStream2 = this.this$0.stream;
        for (Channel channel : storeStream2.getChannels$app_productionGoogleRelease().findChannelsByCategoryInternal$app_productionGoogleRelease(findChannelByIdInternal$app_productionGoogleRelease.f(), findChannelByIdInternal$app_productionGoogleRelease.h())) {
            this.this$0.internalAck(channel, this.$isLockedAck, this.$clearLock);
        }
    }
}
