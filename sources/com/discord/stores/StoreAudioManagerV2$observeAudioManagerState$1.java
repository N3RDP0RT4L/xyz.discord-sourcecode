package com.discord.stores;

import com.discord.stores.StoreAudioManagerV2;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreAudioManagerV2.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/stores/StoreAudioManagerV2$State;", "invoke", "()Lcom/discord/stores/StoreAudioManagerV2$State;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAudioManagerV2$observeAudioManagerState$1 extends o implements Function0<StoreAudioManagerV2.State> {
    public final /* synthetic */ StoreAudioManagerV2 this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreAudioManagerV2$observeAudioManagerState$1(StoreAudioManagerV2 storeAudioManagerV2) {
        super(0);
        this.this$0 = storeAudioManagerV2;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final StoreAudioManagerV2.State invoke() {
        StoreAudioManagerV2.State audioManagerState;
        audioManagerState = this.this$0.getAudioManagerState();
        return audioManagerState;
    }
}
