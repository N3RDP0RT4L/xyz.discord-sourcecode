package com.discord.stores;

import andhook.lib.HookHelper;
import com.discord.models.domain.ModelGuildIntegration;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.d0.f;
import d0.t.g0;
import d0.t.o;
import d0.z.d.m;
import j0.k.b;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import rx.Observable;
import rx.Subscription;
import rx.subjects.BehaviorSubject;
/* compiled from: StoreGuildIntegrations.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010$\u001a\u00020#¢\u0006\u0004\b-\u0010.J\u001b\u0010\u0006\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u000f\u0010\b\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\b\u0010\tJ)\u0010\r\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\nH\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u001b\u0010\u000f\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0002¢\u0006\u0004\b\u000f\u0010\u0007J1\u0010\u0013\u001a\u001a\u0012\u0016\u0012\u0014\u0012\b\u0012\u00060\u0002j\u0002`\u0012\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u00110\u00102\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0013\u0010\u0014J-\u0010\u0013\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u000b0\u00102\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0015\u001a\u00060\u0002j\u0002`\u0012¢\u0006\u0004\b\u0013\u0010\u0016J\u0019\u0010\u0017\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0017\u0010\u0007J\r\u0010\u0018\u001a\u00020\u0005¢\u0006\u0004\b\u0018\u0010\tJ\u0019\u0010\u001b\u001a\u00020\u00052\b\u0010\u001a\u001a\u0004\u0018\u00010\u0019H\u0007¢\u0006\u0004\b\u001b\u0010\u001cR6\u0010\u001e\u001a\"\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0012\u0012\u0004\u0012\u00020\u000b0\u00110\u001d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR\u0018\u0010!\u001a\u0004\u0018\u00010 8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b!\u0010\"R\u0016\u0010$\u001a\u00020#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%R»\u0001\u0010(\u001a¦\u0001\u0012L\u0012J\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0012\u0012\u0004\u0012\u00020\u000b0\u0011 '*$\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0012\u0012\u0004\u0012\u00020\u000b0\u0011\u0018\u00010\u00110\u0011 '*R\u0012L\u0012J\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0012\u0012\u0004\u0012\u00020\u000b0\u0011 '*$\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0012\u0012\u0004\u0012\u00020\u000b0\u0011\u0018\u00010\u00110\u0011\u0018\u00010&0&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u0010)R\u0016\u0010+\u001a\u00020*8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b+\u0010,¨\u0006/"}, d2 = {"Lcom/discord/stores/StoreGuildIntegrations;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "handleIntegrationScreenOpened", "(J)V", "handleIntegrationScreenClosed", "()V", "", "Lcom/discord/models/domain/ModelGuildIntegration;", "integrations", "handleIntegrationsLoaded", "(JLjava/util/List;)V", "requestGuildIntegrations", "Lrx/Observable;", "", "Lcom/discord/primitives/IntegrationId;", "get", "(J)Lrx/Observable;", "integrationId", "(JJ)Lrx/Observable;", "onIntegrationScreenOpened", "onIntegrationScreenClosed", "Lcom/discord/models/domain/ModelGuildIntegration$Update;", "update", "handleUpdate", "(Lcom/discord/models/domain/ModelGuildIntegration$Update;)V", "Ljava/util/HashMap;", "allIntegrations", "Ljava/util/HashMap;", "Lrx/Subscription;", "closeIntegrationScreenSubscription", "Lrx/Subscription;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lrx/subjects/BehaviorSubject;", "kotlin.jvm.PlatformType", "integrationsSubject", "Lrx/subjects/BehaviorSubject;", "", "isOnIntegrationsScreen", "Z", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildIntegrations {
    private Subscription closeIntegrationScreenSubscription;
    private final Dispatcher dispatcher;
    private boolean isOnIntegrationsScreen;
    private final HashMap<Long, Map<Long, ModelGuildIntegration>> allIntegrations = new HashMap<>();
    private final BehaviorSubject<Map<Long, Map<Long, ModelGuildIntegration>>> integrationsSubject = BehaviorSubject.l0(new HashMap());

    public StoreGuildIntegrations(Dispatcher dispatcher) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        this.dispatcher = dispatcher;
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleIntegrationScreenClosed() {
        this.isOnIntegrationsScreen = false;
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleIntegrationScreenOpened(long j) {
        if (!this.isOnIntegrationsScreen) {
            this.isOnIntegrationsScreen = true;
            requestGuildIntegrations(j);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleIntegrationsLoaded(long j, List<? extends ModelGuildIntegration> list) {
        HashMap<Long, Map<Long, ModelGuildIntegration>> hashMap = this.allIntegrations;
        Long valueOf = Long.valueOf(j);
        LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(o.collectionSizeOrDefault(list, 10)), 16));
        for (Object obj : list) {
            linkedHashMap.put(Long.valueOf(((ModelGuildIntegration) obj).getId()), obj);
        }
        hashMap.put(valueOf, linkedHashMap);
        this.integrationsSubject.onNext(new HashMap(this.allIntegrations));
    }

    private final void requestGuildIntegrations(long j) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().getGuildIntegrations(j), false, 1, null), StoreGuildIntegrations.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreGuildIntegrations$requestGuildIntegrations$1(this, j));
    }

    public final Observable<Map<Long, ModelGuildIntegration>> get(final long j) {
        Observable<Map<Long, ModelGuildIntegration>> q = this.integrationsSubject.F(new b<Map<Long, ? extends Map<Long, ? extends ModelGuildIntegration>>, Map<Long, ? extends ModelGuildIntegration>>() { // from class: com.discord.stores.StoreGuildIntegrations$get$1
            public final Map<Long, ModelGuildIntegration> call(Map<Long, ? extends Map<Long, ? extends ModelGuildIntegration>> map) {
                return (Map) map.get(Long.valueOf(j));
            }
        }).q();
        m.checkNotNullExpressionValue(q, "integrationsSubject\n    …  .distinctUntilChanged()");
        return q;
    }

    @StoreThread
    public final void handleUpdate(ModelGuildIntegration.Update update) {
        if (this.isOnIntegrationsScreen && update != null) {
            requestGuildIntegrations(update.getGuildId());
        }
    }

    public final synchronized void onIntegrationScreenClosed() {
        Subscription subscription = this.closeIntegrationScreenSubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        Observable<Long> d02 = Observable.d0(1000L, TimeUnit.MILLISECONDS);
        m.checkNotNullExpressionValue(d02, "Observable\n        .time…S, TimeUnit.MILLISECONDS)");
        ObservableExtensionsKt.appSubscribe(d02, getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new StoreGuildIntegrations$onIntegrationScreenClosed$2(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreGuildIntegrations$onIntegrationScreenClosed$1(this));
    }

    public final synchronized void onIntegrationScreenOpened(long j) {
        Subscription subscription = this.closeIntegrationScreenSubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        this.dispatcher.schedule(new StoreGuildIntegrations$onIntegrationScreenOpened$1(this, j));
    }

    public final Observable<ModelGuildIntegration> get(long j, final long j2) {
        Observable<ModelGuildIntegration> q = get(j).F(new b<Map<Long, ? extends ModelGuildIntegration>, ModelGuildIntegration>() { // from class: com.discord.stores.StoreGuildIntegrations$get$2
            public final ModelGuildIntegration call(Map<Long, ? extends ModelGuildIntegration> map) {
                if (map != null) {
                    return map.get(Long.valueOf(j2));
                }
                return null;
            }
        }).q();
        m.checkNotNullExpressionValue(q, "get(guildId)\n          .…  .distinctUntilChanged()");
        return q;
    }
}
