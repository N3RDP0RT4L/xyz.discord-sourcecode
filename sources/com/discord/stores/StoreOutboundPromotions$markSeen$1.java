package com.discord.stores;

import android.content.SharedPreferences;
import com.discord.api.premium.OutboundPromotion;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.stores.StoreOutboundPromotions;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreOutboundPromotions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreOutboundPromotions$markSeen$1 extends o implements Function0<Unit> {
    public final /* synthetic */ StoreOutboundPromotions this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreOutboundPromotions$markSeen$1(StoreOutboundPromotions storeOutboundPromotions) {
        super(0);
        this.this$0 = storeOutboundPromotions;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreOutboundPromotions.State state;
        List<OutboundPromotion> validActivePromotions;
        int unseenCount;
        state = this.this$0.state;
        Object obj = null;
        if (!(state instanceof StoreOutboundPromotions.State.Loaded)) {
            state = null;
        }
        StoreOutboundPromotions.State.Loaded loaded = (StoreOutboundPromotions.State.Loaded) state;
        if (loaded != null && (validActivePromotions = loaded.getValidActivePromotions()) != null) {
            Iterator<T> it = validActivePromotions.iterator();
            if (it.hasNext()) {
                obj = it.next();
                if (it.hasNext()) {
                    UtcDateTime h = ((OutboundPromotion) obj).h();
                    do {
                        Object next = it.next();
                        UtcDateTime h2 = ((OutboundPromotion) next).h();
                        if (h.compareTo(h2) < 0) {
                            obj = next;
                            h = h2;
                        }
                    } while (it.hasNext());
                }
            }
            OutboundPromotion outboundPromotion = (OutboundPromotion) obj;
            if (outboundPromotion != null) {
                SharedPreferences.Editor edit = this.this$0.getPrefs().edit();
                m.checkNotNullExpressionValue(edit, "editor");
                edit.putLong("LATEST_SEEN_PROMO_DATE", outboundPromotion.h().g());
                edit.apply();
                StoreOutboundPromotions storeOutboundPromotions = this.this$0;
                unseenCount = storeOutboundPromotions.getUnseenCount(validActivePromotions);
                storeOutboundPromotions.state = new StoreOutboundPromotions.State.Loaded(validActivePromotions, unseenCount);
                this.this$0.markChanged();
            }
        }
    }
}
