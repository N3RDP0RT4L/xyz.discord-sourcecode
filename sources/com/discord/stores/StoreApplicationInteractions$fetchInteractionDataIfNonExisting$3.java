package com.discord.stores;

import com.discord.api.commands.ApplicationCommandData;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreApplicationInteractions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/commands/ApplicationCommandData;", "interactionDetail", "", "invoke", "(Lcom/discord/api/commands/ApplicationCommandData;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreApplicationInteractions$fetchInteractionDataIfNonExisting$3 extends o implements Function1<ApplicationCommandData, Unit> {
    public final /* synthetic */ long $interactionId;
    public final /* synthetic */ StoreApplicationInteractions this$0;

    /* compiled from: StoreApplicationInteractions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreApplicationInteractions$fetchInteractionDataIfNonExisting$3$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ ApplicationCommandData $interactionDetail;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(ApplicationCommandData applicationCommandData) {
            super(0);
            this.$interactionDetail = applicationCommandData;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            StoreApplicationInteractions$fetchInteractionDataIfNonExisting$3 storeApplicationInteractions$fetchInteractionDataIfNonExisting$3 = StoreApplicationInteractions$fetchInteractionDataIfNonExisting$3.this;
            storeApplicationInteractions$fetchInteractionDataIfNonExisting$3.this$0.handleInteractionDataFetchSuccess(storeApplicationInteractions$fetchInteractionDataIfNonExisting$3.$interactionId, this.$interactionDetail);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreApplicationInteractions$fetchInteractionDataIfNonExisting$3(StoreApplicationInteractions storeApplicationInteractions, long j) {
        super(1);
        this.this$0 = storeApplicationInteractions;
        this.$interactionId = j;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ApplicationCommandData applicationCommandData) {
        invoke2(applicationCommandData);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ApplicationCommandData applicationCommandData) {
        Dispatcher dispatcher;
        m.checkNotNullParameter(applicationCommandData, "interactionDetail");
        dispatcher = this.this$0.dispatcher;
        dispatcher.schedule(new AnonymousClass1(applicationCommandData));
    }
}
