package com.discord.stores;

import com.discord.api.message.reaction.MessageReactionEmoji;
import com.discord.stores.StoreMessageReactions;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreMessageReactions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessageReactions$forceRetryFetchReactions$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ MessageReactionEmoji $emoji;
    public final /* synthetic */ long $messageId;
    public final /* synthetic */ StoreMessageReactions this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreMessageReactions$forceRetryFetchReactions$1(StoreMessageReactions storeMessageReactions, long j, MessageReactionEmoji messageReactionEmoji, long j2) {
        super(0);
        this.this$0 = storeMessageReactions;
        this.$messageId = j;
        this.$emoji = messageReactionEmoji;
        this.$channelId = j2;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        Map map;
        map = this.this$0.reactions;
        Map map2 = (Map) map.get(Long.valueOf(this.$messageId));
        if (map2 != null) {
            StoreMessageReactions.EmojiResults emojiResults = (StoreMessageReactions.EmojiResults) map2.remove(this.$emoji.c());
        }
        this.this$0.fetchReactions(this.$channelId, this.$messageId, this.$emoji);
        this.this$0.markChanged();
    }
}
