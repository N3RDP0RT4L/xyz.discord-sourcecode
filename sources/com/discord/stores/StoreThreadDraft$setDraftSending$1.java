package com.discord.stores;

import com.discord.stores.StoreThreadDraft;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import rx.subjects.BehaviorSubject;
/* compiled from: StoreThreadDraft.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreThreadDraft$setDraftSending$1 extends o implements Function0<Unit> {
    public final /* synthetic */ StoreThreadDraft this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreThreadDraft$setDraftSending$1(StoreThreadDraft storeThreadDraft) {
        super(0);
        this.this$0 = storeThreadDraft;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        BehaviorSubject behaviorSubject;
        BehaviorSubject behaviorSubject2;
        behaviorSubject = this.this$0.draftStateSubject;
        behaviorSubject2 = this.this$0.draftStateSubject;
        m.checkNotNullExpressionValue(behaviorSubject2, "draftStateSubject");
        behaviorSubject.onNext(StoreThreadDraft.ThreadDraftState.copy$default((StoreThreadDraft.ThreadDraftState) behaviorSubject2.n0(), false, null, null, true, false, 23, null));
    }
}
