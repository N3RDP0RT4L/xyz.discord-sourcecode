package com.discord.stores;

import com.discord.stores.StoreNux;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreNux.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreNux$updateNux$1 extends o implements Function0<Unit> {
    public final /* synthetic */ Function1 $updateFunction;
    public final /* synthetic */ StoreNux this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreNux$updateNux$1(StoreNux storeNux, Function1 function1) {
        super(0);
        this.this$0 = storeNux;
        this.$updateFunction = function1;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreNux.NuxState nuxState;
        StoreNux.NuxState nuxState2;
        StoreNux storeNux = this.this$0;
        Function1 function1 = this.$updateFunction;
        nuxState = storeNux.nuxState;
        storeNux.nuxState = (StoreNux.NuxState) function1.invoke(nuxState);
        StoreNux storeNux2 = this.this$0;
        nuxState2 = storeNux2.nuxState;
        storeNux2.publishNuxUpdated(nuxState2);
    }
}
