package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.models.domain.ModelInvite;
import com.discord.stores.StoreInstantInvites;
import j0.k.b;
import kotlin.Metadata;
/* compiled from: StoreInviteSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/stores/StoreInstantInvites$InviteState;", "inviteState", "Lcom/discord/models/domain/ModelInvite;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/stores/StoreInstantInvites$InviteState;)Lcom/discord/models/domain/ModelInvite;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreInviteSettings$getInvite$2<T, R> implements b<StoreInstantInvites.InviteState, ModelInvite> {
    public static final StoreInviteSettings$getInvite$2 INSTANCE = new StoreInviteSettings$getInvite$2();

    public final ModelInvite call(StoreInstantInvites.InviteState inviteState) {
        if (inviteState instanceof StoreInstantInvites.InviteState.Resolved) {
            return ((StoreInstantInvites.InviteState.Resolved) inviteState).getInvite();
        }
        return null;
    }
}
