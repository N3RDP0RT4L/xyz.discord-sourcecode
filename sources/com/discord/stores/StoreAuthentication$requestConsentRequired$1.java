package com.discord.stores;

import com.discord.models.domain.ModelLocationMetadata;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreAuthentication.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/ModelLocationMetadata;", "it", "", "invoke", "(Lcom/discord/models/domain/ModelLocationMetadata;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAuthentication$requestConsentRequired$1 extends o implements Function1<ModelLocationMetadata, Unit> {
    public final /* synthetic */ StoreAuthentication this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreAuthentication$requestConsentRequired$1(StoreAuthentication storeAuthentication) {
        super(1);
        this.this$0 = storeAuthentication;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ModelLocationMetadata modelLocationMetadata) {
        invoke2(modelLocationMetadata);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ModelLocationMetadata modelLocationMetadata) {
        m.checkNotNullParameter(modelLocationMetadata, "it");
        this.this$0.setConsentRequired(modelLocationMetadata.getConsentRequired());
        String countryCode = modelLocationMetadata.getCountryCode();
        if (countryCode != null) {
            StoreStream.Companion.getPhone().updateDefaultCountryCode(countryCode);
        }
    }
}
