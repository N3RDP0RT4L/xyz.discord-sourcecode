package com.discord.stores;

import com.discord.api.channel.Channel;
import com.discord.utilities.analytics.AnalyticsTracker;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreAnalytics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAnalytics$trackShowCallFeedbackSheet$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ StoreAnalytics this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreAnalytics$trackShowCallFeedbackSheet$1(StoreAnalytics storeAnalytics, long j) {
        super(0);
        this.this$0 = storeAnalytics;
        this.$channelId = j;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreStream storeStream;
        storeStream = this.this$0.stores;
        Channel findChannelByIdInternal$app_productionGoogleRelease = storeStream.getChannels$app_productionGoogleRelease().findChannelByIdInternal$app_productionGoogleRelease(this.$channelId);
        if (findChannelByIdInternal$app_productionGoogleRelease != null) {
            AnalyticsTracker.INSTANCE.openCallFeedbackSheet(findChannelByIdInternal$app_productionGoogleRelease.f(), findChannelByIdInternal$app_productionGoogleRelease.h(), findChannelByIdInternal$app_productionGoogleRelease.A());
        }
    }
}
