package com.discord.stores;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import com.discord.analytics.generated.events.network_action.TrackNetworkActionUserRegister;
import com.discord.analytics.generated.traits.TrackNetworkMetadataReceiver;
import com.discord.api.auth.RegisterResponse;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreAuthentication;
import com.discord.stores.StoreInviteSettings;
import com.discord.stores.StoreStream;
import com.discord.stores.utilities.RestCallStateKt;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import d0.z.d.k;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func2;
/* compiled from: StoreAuthentication.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\b\u001a*\u0012\u000e\b\u0001\u0012\n \u0002*\u0004\u0018\u00010\u00050\u0005 \u0002*\u0014\u0012\u000e\b\u0001\u0012\n \u0002*\u0004\u0018\u00010\u00050\u0005\u0018\u00010\u00040\u00042\u0018\u0010\u0003\u001a\u0014 \u0002*\n\u0018\u00010\u0000j\u0004\u0018\u0001`\u00010\u0000j\u0002`\u0001H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "Lcom/discord/primitives/FingerPrint;", "kotlin.jvm.PlatformType", "fingerprint", "Lrx/Observable;", "Lcom/discord/api/auth/RegisterResponse;", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/String;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAuthentication$register$1<T, R> implements b<String, Observable<? extends RegisterResponse>> {
    public final /* synthetic */ String $captchaKey;
    public final /* synthetic */ boolean $consent;
    public final /* synthetic */ String $dateOfBirth;
    public final /* synthetic */ String $email;
    public final /* synthetic */ String $password;
    public final /* synthetic */ String $phoneToken;
    public final /* synthetic */ String $username;
    public final /* synthetic */ StoreAuthentication this$0;

    /* compiled from: StoreAuthentication.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\b\u0010\u0001\u001a\u0004\u0018\u00010\u00002\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/stores/StoreInviteSettings$InviteCode;", "p1", "", "p2", "Lcom/discord/stores/StoreAuthentication$AuthRequestParams;", "invoke", "(Lcom/discord/stores/StoreInviteSettings$InviteCode;Ljava/lang/String;)Lcom/discord/stores/StoreAuthentication$AuthRequestParams;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreAuthentication$register$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class AnonymousClass1 extends k implements Function2<StoreInviteSettings.InviteCode, String, StoreAuthentication.AuthRequestParams> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public AnonymousClass1() {
            super(2, StoreAuthentication.AuthRequestParams.class, HookHelper.constructorName, "<init>(Lcom/discord/stores/StoreInviteSettings$InviteCode;Ljava/lang/String;)V", 0);
        }

        public final StoreAuthentication.AuthRequestParams invoke(StoreInviteSettings.InviteCode inviteCode, String str) {
            return new StoreAuthentication.AuthRequestParams(inviteCode, str);
        }
    }

    public StoreAuthentication$register$1(StoreAuthentication storeAuthentication, String str, String str2, String str3, String str4, String str5, boolean z2, String str6) {
        this.this$0 = storeAuthentication;
        this.$username = str;
        this.$email = str2;
        this.$phoneToken = str3;
        this.$password = str4;
        this.$captchaKey = str5;
        this.$consent = z2;
        this.$dateOfBirth = str6;
    }

    public final Observable<? extends RegisterResponse> call(final String str) {
        StoreStream.Companion companion = StoreStream.Companion;
        Observable<StoreInviteSettings.InviteCode> inviteCode = companion.getInviteSettings().getInviteCode();
        Observable<String> observeDynamicLinkGuildTemplateCode = companion.getGuildTemplates().observeDynamicLinkGuildTemplateCode();
        final AnonymousClass1 r2 = AnonymousClass1.INSTANCE;
        Object obj = r2;
        if (r2 != null) {
            obj = new Func2() { // from class: com.discord.stores.StoreAuthentication$sam$rx_functions_Func2$0
                @Override // rx.functions.Func2
                public final /* synthetic */ Object call(Object obj2, Object obj3) {
                    return Function2.this.invoke(obj2, obj3);
                }
            };
        }
        Observable j = Observable.j(inviteCode, observeDynamicLinkGuildTemplateCode, (Func2) obj);
        m.checkNotNullExpressionValue(j, "Observable.combineLatest…RequestParams\n          )");
        Observable<R> z2 = ObservableExtensionsKt.takeSingleUntilTimeout$default(j, 500L, false, 2, null).z(new b<StoreAuthentication.AuthRequestParams, Observable<? extends RegisterResponse>>() { // from class: com.discord.stores.StoreAuthentication$register$1.2

            /* compiled from: StoreAuthentication.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/auth/RegisterResponse;", "it", "Lcom/discord/analytics/generated/traits/TrackNetworkMetadataReceiver;", "invoke", "(Lcom/discord/api/auth/RegisterResponse;)Lcom/discord/analytics/generated/traits/TrackNetworkMetadataReceiver;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.stores.StoreAuthentication$register$1$2$1  reason: invalid class name */
            /* loaded from: classes.dex */
            public static final class AnonymousClass1 extends o implements Function1<RegisterResponse, TrackNetworkMetadataReceiver> {
                public final /* synthetic */ String $inviteCode;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public AnonymousClass1(String str) {
                    super(1);
                    this.$inviteCode = str;
                }

                public final TrackNetworkMetadataReceiver invoke(RegisterResponse registerResponse) {
                    return new TrackNetworkActionUserRegister(this.$inviteCode, Boolean.valueOf(StoreAuthentication$register$1.this.$consent), Boolean.FALSE);
                }
            }

            public final Observable<? extends RegisterResponse> call(StoreAuthentication.AuthRequestParams authRequestParams) {
                StoreInviteSettings.InviteCode inviteCode2 = authRequestParams.getInviteCode();
                String inviteCode3 = inviteCode2 != null ? inviteCode2.getInviteCode() : null;
                if (inviteCode3 == null || inviteCode3.length() == 0) {
                    StoreStream.Companion.getNux().setPostRegister(true);
                } else {
                    StoreStream.Companion.getNux().setPostRegisterWithInvite(true);
                }
                RestAPI api = RestAPI.Companion.getApi();
                String str2 = str;
                StoreAuthentication$register$1 storeAuthentication$register$1 = StoreAuthentication$register$1.this;
                String str3 = storeAuthentication$register$1.$username;
                String str4 = storeAuthentication$register$1.$email;
                String str5 = storeAuthentication$register$1.$phoneToken;
                String str6 = storeAuthentication$register$1.$password;
                String str7 = storeAuthentication$register$1.$captchaKey;
                String guildTemplateCode = authRequestParams.getGuildTemplateCode();
                StoreAuthentication$register$1 storeAuthentication$register$12 = StoreAuthentication$register$1.this;
                return RestCallStateKt.logNetworkAction(api.postAuthRegister(new RestAPIParams.AuthRegister(str2, str3, str4, str5, str6, str7, inviteCode3, guildTemplateCode, storeAuthentication$register$12.$consent, storeAuthentication$register$12.$dateOfBirth)), new AnonymousClass1(inviteCode3));
            }
        });
        m.checkNotNullExpressionValue(z2, "Observable.combineLatest…        }\n              }");
        return ObservableExtensionsKt.takeSingleUntilTimeout$default(ObservableExtensionsKt.restSubscribeOn$default(z2, false, 1, null), 0L, false, 3, null).t(new Action1<RegisterResponse>() { // from class: com.discord.stores.StoreAuthentication$register$1.3
            public final void call(RegisterResponse registerResponse) {
                StoreStream.Companion companion2 = StoreStream.Companion;
                companion2.getUserSettingsSystem().setIsSyncThemeEnabled(false);
                StoreAuthentication$register$1.this.this$0.setFingerprint(null, true);
                StoreAuthentication$register$1.this.this$0.setAuthed(registerResponse.a());
                AnalyticsTracker.INSTANCE.appFirstLogin();
                companion2.getNotifications().setEnabledInApp(true, false);
                companion2.getNux().setFirstOpen(true);
            }
        });
    }
}
