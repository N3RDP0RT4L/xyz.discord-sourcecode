package com.discord.stores;

import andhook.lib.HookHelper;
import com.discord.api.guild.Guild;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.guildscheduledevent.GuildScheduledEventStatus;
import com.discord.api.guildscheduledevent.GuildScheduledEventUserUpdate;
import com.discord.models.domain.ModelPayload;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.Clock;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventUser;
import d0.d0.f;
import d0.t.h0;
import d0.t.n;
import d0.t.n0;
import d0.t.o;
import d0.z.d.m;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreGuildScheduledEvents.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Ü\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u000b\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010#\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010!\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 \u0081\u00012\u00020\u0001:\u0002\u0081\u0001B8\u0012\u0006\u0010c\u001a\u00020b\u0012\u0006\u0010x\u001a\u00020w\u0012\u0006\u0010{\u001a\u00020z\u0012\u0006\u0010l\u001a\u00020k\u0012\u0006\u0010t\u001a\u00020s\u0012\u0006\u0010f\u001a\u00020e¢\u0006\u0005\b\u007f\u0010\u0080\u0001J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J'\u0010\n\u001a\u00020\u00022\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u00062\n\u0010\t\u001a\u00060\u0005j\u0002`\bH\u0003¢\u0006\u0004\b\n\u0010\u000bJ'\u0010\f\u001a\u00020\u00022\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u00062\n\u0010\t\u001a\u00060\u0005j\u0002`\bH\u0003¢\u0006\u0004\b\f\u0010\u000bJ3\u0010\u000f\u001a\u00020\u00022\n\u0010\u000e\u001a\u00060\u0005j\u0002`\r2\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u00062\n\u0010\t\u001a\u00060\u0005j\u0002`\bH\u0003¢\u0006\u0004\b\u000f\u0010\u0010J3\u0010\u0011\u001a\u00020\u00022\n\u0010\u000e\u001a\u00060\u0005j\u0002`\r2\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u00062\n\u0010\t\u001a\u00060\u0005j\u0002`\bH\u0003¢\u0006\u0004\b\u0011\u0010\u0010J\u0017\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u0012H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J#\u0010\u0016\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u00122\n\u0010\u000e\u001a\u00060\u0005j\u0002`\rH\u0003¢\u0006\u0004\b\u0016\u0010\u0017J#\u0010\u0018\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u00122\n\u0010\u000e\u001a\u00060\u0005j\u0002`\rH\u0003¢\u0006\u0004\b\u0018\u0010\u0017J\u0017\u0010\u001a\u001a\u00020\u00122\u0006\u0010\u0019\u001a\u00020\u0012H\u0002¢\u0006\u0004\b\u001a\u0010\u001bJ/\u0010 \u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u001f0\u001e2\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u00062\b\b\u0002\u0010\u001d\u001a\u00020\u001c¢\u0006\u0004\b \u0010!J\u0019\u0010\"\u001a\u00020\u00022\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006¢\u0006\u0004\b\"\u0010#J\u0019\u0010$\u001a\u00020\u00022\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006¢\u0006\u0004\b$\u0010#J%\u0010&\u001a\u00020\u00022\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u00062\n\u0010%\u001a\u00060\u0005j\u0002`\b¢\u0006\u0004\b&\u0010\u000bJ)\u0010'\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00120\u001e2\b\u0010%\u001a\u0004\u0018\u00010\u00052\b\u0010\u0007\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b'\u0010(J)\u0010)\u001a\b\u0012\u0004\u0012\u00020\u00120\u001f2\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u00062\b\b\u0002\u0010\u001d\u001a\u00020\u001c¢\u0006\u0004\b)\u0010*J#\u0010,\u001a\u0018\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u001f0+¢\u0006\u0004\b,\u0010-J#\u0010/\u001a\f\u0012\b\u0012\u00060\u0005j\u0002`\b0.2\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006¢\u0006\u0004\b/\u00100J)\u00102\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\r\u0012\u0004\u0012\u0002010+2\n\u0010%\u001a\u00060\u0005j\u0002`\b¢\u0006\u0004\b2\u00103J\r\u00104\u001a\u00020\u001c¢\u0006\u0004\b4\u00105J\r\u00106\u001a\u00020\u001c¢\u0006\u0004\b6\u00105J%\u00107\u001a\u00020\u001c2\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u00062\n\u0010\t\u001a\u00060\u0005j\u0002`\b¢\u0006\u0004\b7\u00108J1\u0010;\u001a\u00020\u00022 \u0010:\u001a\u001c\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\b\u0012\u00060\u0005j\u0002`\b090\u001fH\u0007¢\u0006\u0004\b;\u0010<J\u0015\u0010=\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u0012¢\u0006\u0004\b=\u0010\u0015J\u0015\u0010>\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u0012¢\u0006\u0004\b>\u0010\u0015J/\u0010A\u001a\u0004\u0018\u00010\u00122\u000e\u0010\u0007\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00062\u000e\u0010@\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`?¢\u0006\u0004\bA\u0010BJ\u0017\u0010E\u001a\u00020\u00022\u0006\u0010D\u001a\u00020CH\u0007¢\u0006\u0004\bE\u0010FJ\u0017\u0010I\u001a\u00020\u00022\u0006\u0010H\u001a\u00020GH\u0007¢\u0006\u0004\bI\u0010JJ\u001b\u0010K\u001a\u00020\u00022\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006H\u0007¢\u0006\u0004\bK\u0010#J\u0017\u0010N\u001a\u00020\u00022\u0006\u0010M\u001a\u00020LH\u0007¢\u0006\u0004\bN\u0010OJ\u0017\u0010P\u001a\u00020\u00022\u0006\u0010M\u001a\u00020LH\u0007¢\u0006\u0004\bP\u0010OJ\u0017\u0010Q\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u0012H\u0007¢\u0006\u0004\bQ\u0010\u0015J\u0017\u0010R\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u0012H\u0007¢\u0006\u0004\bR\u0010\u0015J\u0017\u0010S\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u0012H\u0007¢\u0006\u0004\bS\u0010\u0015J-\u0010\u001a\u001a\u0004\u0018\u00010\u00122\n\u0010%\u001a\u00060\u0005j\u0002`\b2\u0010\b\u0002\u0010\u0007\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006¢\u0006\u0004\b\u001a\u0010TJ\u000f\u0010U\u001a\u00020\u0002H\u0016¢\u0006\u0004\bU\u0010\u0004R:\u0010X\u001a&\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u00050Vj\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u0005`W8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bX\u0010YRN\u0010[\u001a:\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0005j\u0002`\b0Z0Vj\u001c\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0005j\u0002`\b0Z`W8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b[\u0010YR\u0016\u0010\\\u001a\u00020\u001c8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\\\u0010]R&\u0010`\u001a\u0012\u0012\u0004\u0012\u00020\u00050^j\b\u0012\u0004\u0012\u00020\u0005`_8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b`\u0010aR\u0016\u0010c\u001a\u00020b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bc\u0010dR\u0016\u0010f\u001a\u00020e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bf\u0010gR,\u0010h\u001a\u0018\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u001f0+8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bh\u0010iR\u0016\u0010j\u001a\u00020\u001c8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bj\u0010]R\u0016\u0010l\u001a\u00020k8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bl\u0010mR&\u0010n\u001a\u0012\u0012\u0004\u0012\u00020\u00050^j\b\u0012\u0004\u0012\u00020\u0005`_8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bn\u0010aRF\u0010p\u001a2\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120o0Vj\u0018\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120o`W8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bp\u0010YR.\u0010q\u001a\u001a\u0012\b\u0012\u00060\u0005j\u0002`\u00060^j\f\u0012\b\u0012\u00060\u0005j\u0002`\u0006`_8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bq\u0010aRn\u0010r\u001aZ\u0012\b\u0012\u00060\u0005j\u0002`\b\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\r\u0012\u0004\u0012\u0002010V0Vj6\u0012\b\u0012\u00060\u0005j\u0002`\b\u0012(\u0012&\u0012\b\u0012\u00060\u0005j\u0002`\r\u0012\u0004\u0012\u0002010Vj\u0012\u0012\b\u0012\u00060\u0005j\u0002`\r\u0012\u0004\u0012\u000201`W`W8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\br\u0010YR\u0016\u0010t\u001a\u00020s8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bt\u0010uRN\u0010v\u001a:\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0005j\u0002`\b0Z0Vj\u001c\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0005j\u0002`\b0Z`W8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bv\u0010YR\u0016\u0010x\u001a\u00020w8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bx\u0010yR\u0016\u0010{\u001a\u00020z8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b{\u0010|R0\u0010}\u001a\u001c\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0005j\u0002`\b0.0+8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b}\u0010iR6\u0010~\u001a\"\u0012\b\u0012\u00060\u0005j\u0002`\b\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\r\u0012\u0004\u0012\u0002010+0+8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b~\u0010i¨\u0006\u0082\u0001"}, d2 = {"Lcom/discord/stores/StoreGuildScheduledEvents;", "Lcom/discord/stores/StoreV2;", "", "handleFetchRsvpUsersFailure", "()V", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/GuildScheduledEventId;", "guildScheduledEventId", "removeMeRsvpForEvent", "(JJ)V", "addMeRsvpForEvent", "Lcom/discord/primitives/UserId;", "userId", "removeUserRsvpForEvent", "(JJJ)V", "addUserRsvpForEvent", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "guildScheduledEvent", "processGuildScheduledEventCreateOrUpdate", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;)V", "processRsvpDelete", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;J)V", "processRsvpCreate", "event", "findEventFromStore", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;)Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "", "filterInaccessible", "Lrx/Observable;", "", "observeGuildScheduledEvents", "(JZ)Lrx/Observable;", "fetchMeGuildScheduledEvents", "(J)V", "fetchGuildScheduledEventUserCounts", "eventId", "fetchGuildScheduledEventUsers", "observeGuildScheduledEvent", "(Ljava/lang/Long;Ljava/lang/Long;)Lrx/Observable;", "getGuildScheduledEvents", "(JZ)Ljava/util/List;", "", "getAllGuildScheduledEvents", "()Ljava/util/Map;", "", "getMeGuildScheduledEventIds", "(J)Ljava/util/Set;", "Lcom/discord/widgets/guildscheduledevent/GuildScheduledEventUser;", "getGuildScheduledEventUsers", "(J)Ljava/util/Map;", "getIsFetchingGuildScheduledEventUsers", "()Z", "getIsGuildScheduledEventUsersError", "isMeRsvpedToEvent", "(JJ)Z", "Lkotlin/Pair;", "events", "addMeRsvpsForEvent", "(Ljava/util/List;)V", "toggleMeRsvpForEvent", "addGuildScheduledEventFromInvite", "Lcom/discord/primitives/ChannelId;", "channelId", "getActiveEventForChannel", "(Ljava/lang/Long;Ljava/lang/Long;)Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "Lcom/discord/api/guild/Guild;", "guild", "handleGuildCreate", "(Lcom/discord/api/guild/Guild;)V", "handleGuildRemove", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventUserUpdate;", "guildScheduledEventUserUpdate", "handleGuildScheduledEventUserAdd", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEventUserUpdate;)V", "handleGuildScheduledEventUserRemove", "handleGuildScheduledEventCreate", "handleGuildScheduledEventUpdate", "handleGuildScheduledEventDelete", "(JLjava/lang/Long;)Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "snapshotData", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "guildScheduledEventsFetchTimestamps", "Ljava/util/HashMap;", "", "guildScheduledEventUsersFetches", "isGuildScheduledEventUsersError", "Z", "Ljava/util/HashSet;", "Lkotlin/collections/HashSet;", "rsvpsAwaitingNetwork", "Ljava/util/HashSet;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "guildScheduledEventsSnapshot", "Ljava/util/Map;", "isFetchingGuildScheduledEventUsers", "Lcom/discord/stores/StoreUser;", "userStore", "Lcom/discord/stores/StoreUser;", "rsvpsAwaitingSnapshot", "", "guildScheduledEvents", "meGuildScheduledEventsFetches", "guildScheduledEventUsers", "Lcom/discord/stores/StoreGuilds;", "guildsStore", "Lcom/discord/stores/StoreGuilds;", "meGuildScheduledEventIds", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/stores/StorePermissions;", "permissionsStore", "Lcom/discord/stores/StorePermissions;", "meGuildScheduledEventIdsSnapshot", "guildScheduledEventUsersSnapshot", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/utilities/time/Clock;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildScheduledEvents extends StoreV2 {
    public static final Companion Companion = new Companion(null);
    public static final long FETCH_GUILD_EVENTS_THRESHOLD = 1800000;
    private final Clock clock;
    private final Dispatcher dispatcher;
    private final StoreGuilds guildsStore;
    private boolean isFetchingGuildScheduledEventUsers;
    private boolean isGuildScheduledEventUsersError;
    private final ObservationDeck observationDeck;
    private final StorePermissions permissionsStore;
    private final StoreUser userStore;
    private final HashMap<Long, List<GuildScheduledEvent>> guildScheduledEvents = new HashMap<>();
    private Map<Long, ? extends List<GuildScheduledEvent>> guildScheduledEventsSnapshot = h0.emptyMap();
    private final HashMap<Long, Long> guildScheduledEventsFetchTimestamps = new HashMap<>();
    private final HashMap<Long, Set<Long>> meGuildScheduledEventIds = new HashMap<>();
    private Map<Long, ? extends Set<Long>> meGuildScheduledEventIdsSnapshot = h0.emptyMap();
    private final HashSet<Long> meGuildScheduledEventsFetches = new HashSet<>();
    private final HashSet<Long> rsvpsAwaitingNetwork = new HashSet<>();
    private HashSet<Long> rsvpsAwaitingSnapshot = new HashSet<>();
    private final HashMap<Long, HashMap<Long, GuildScheduledEventUser>> guildScheduledEventUsers = new HashMap<>();
    private Map<Long, ? extends Map<Long, GuildScheduledEventUser>> guildScheduledEventUsersSnapshot = h0.emptyMap();
    private final HashMap<Long, Set<Long>> guildScheduledEventUsersFetches = new HashMap<>();

    /* compiled from: StoreGuildScheduledEvents.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/stores/StoreGuildScheduledEvents$Companion;", "", "", "FETCH_GUILD_EVENTS_THRESHOLD", "J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public StoreGuildScheduledEvents(Dispatcher dispatcher, ObservationDeck observationDeck, StorePermissions storePermissions, StoreUser storeUser, StoreGuilds storeGuilds, Clock clock) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(storePermissions, "permissionsStore");
        m.checkNotNullParameter(storeUser, "userStore");
        m.checkNotNullParameter(storeGuilds, "guildsStore");
        m.checkNotNullParameter(clock, "clock");
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
        this.permissionsStore = storePermissions;
        this.userStore = storeUser;
        this.guildsStore = storeGuilds;
        this.clock = clock;
    }

    @StoreThread
    private final void addMeRsvpForEvent(long j, long j2) {
        if (this.meGuildScheduledEventIds.containsKey(Long.valueOf(j))) {
            Set<Long> set = this.meGuildScheduledEventIds.get(Long.valueOf(j));
            if (set != null) {
                set.add(Long.valueOf(j2));
                return;
            }
            return;
        }
        this.meGuildScheduledEventIds.put(Long.valueOf(j), n0.mutableSetOf(Long.valueOf(j2)));
    }

    @StoreThread
    private final void addUserRsvpForEvent(long j, long j2, long j3) {
        GuildMember member;
        if (j == this.userStore.getMe().getId() && !isMeRsvpedToEvent(j2, j3)) {
            addMeRsvpForEvent(j2, j3);
        }
        HashMap<Long, GuildScheduledEventUser> hashMap = this.guildScheduledEventUsers.get(Long.valueOf(j3));
        if (hashMap == null) {
            hashMap = new HashMap<>();
        }
        User user = this.userStore.getUsers(d0.t.m.listOf(Long.valueOf(j)), false).get(Long.valueOf(j));
        if (!(user == null || (member = this.guildsStore.getMember(j2, j)) == null)) {
            hashMap.put(Long.valueOf(j), new GuildScheduledEventUser(user, member, j3));
        }
        this.guildScheduledEventUsers.put(Long.valueOf(j3), hashMap);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final GuildScheduledEvent findEventFromStore(GuildScheduledEvent guildScheduledEvent) {
        GuildScheduledEvent findEventFromStore = findEventFromStore(guildScheduledEvent.i(), Long.valueOf(guildScheduledEvent.h()));
        return findEventFromStore != null ? findEventFromStore : guildScheduledEvent;
    }

    public static /* synthetic */ GuildScheduledEvent findEventFromStore$default(StoreGuildScheduledEvents storeGuildScheduledEvents, long j, Long l, int i, Object obj) {
        if ((i & 2) != 0) {
            l = null;
        }
        return storeGuildScheduledEvents.findEventFromStore(j, l);
    }

    public static /* synthetic */ List getGuildScheduledEvents$default(StoreGuildScheduledEvents storeGuildScheduledEvents, long j, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = true;
        }
        return storeGuildScheduledEvents.getGuildScheduledEvents(j, z2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleFetchRsvpUsersFailure() {
        this.dispatcher.schedule(new StoreGuildScheduledEvents$handleFetchRsvpUsersFailure$1(this));
    }

    public static /* synthetic */ Observable observeGuildScheduledEvents$default(StoreGuildScheduledEvents storeGuildScheduledEvents, long j, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = true;
        }
        return storeGuildScheduledEvents.observeGuildScheduledEvents(j, z2);
    }

    private final void processGuildScheduledEventCreateOrUpdate(GuildScheduledEvent guildScheduledEvent) {
        int i;
        int i2;
        GuildScheduledEvent guildScheduledEvent2;
        GuildScheduledEvent guildScheduledEvent3 = guildScheduledEvent;
        long h = guildScheduledEvent.h();
        List<GuildScheduledEvent> list = this.guildScheduledEvents.get(Long.valueOf(h));
        if (list != null) {
            Iterator<GuildScheduledEvent> it = list.iterator();
            int i3 = 0;
            while (it.hasNext()) {
                if (it.next().i() == guildScheduledEvent.i()) {
                    i = i3;
                    break;
                }
                i3++;
            }
        }
        i = -1;
        if (this.guildScheduledEvents.get(Long.valueOf(h)) == null) {
            this.guildScheduledEvents.put(Long.valueOf(h), n.mutableListOf(guildScheduledEvent3));
        } else if (i == -1) {
            List<GuildScheduledEvent> list2 = this.guildScheduledEvents.get(Long.valueOf(h));
            if (list2 != null) {
                list2.add(guildScheduledEvent3);
            }
        } else {
            List<GuildScheduledEvent> list3 = this.guildScheduledEvents.get(Long.valueOf(h));
            Integer n = (list3 == null || (guildScheduledEvent2 = list3.get(i)) == null) ? null : guildScheduledEvent2.n();
            if (guildScheduledEvent.n() == null) {
                i2 = i;
                guildScheduledEvent3 = GuildScheduledEvent.a(guildScheduledEvent, 0L, 0L, null, null, null, null, null, null, null, null, null, null, null, null, null, n, null, null, 229375);
            } else {
                i2 = i;
            }
            List<GuildScheduledEvent> list4 = this.guildScheduledEvents.get(Long.valueOf(h));
            if (list4 != null) {
                list4.set(i2, guildScheduledEvent3);
            }
        }
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void processRsvpCreate(GuildScheduledEvent guildScheduledEvent, long j) {
        long i = guildScheduledEvent.i();
        if (j != this.userStore.getMe().getId() || !isMeRsvpedToEvent(guildScheduledEvent.h(), i)) {
            this.rsvpsAwaitingSnapshot.add(Long.valueOf(i));
            addUserRsvpForEvent(j, guildScheduledEvent.h(), i);
            Integer n = guildScheduledEvent.n();
            GuildScheduledEvent a = GuildScheduledEvent.a(guildScheduledEvent, 0L, 0L, null, null, null, null, null, null, null, null, null, null, null, null, null, Integer.valueOf((n != null ? n.intValue() : 0) + 1), null, null, 229375);
            List<GuildScheduledEvent> list = this.guildScheduledEvents.get(Long.valueOf(a.h()));
            if (list != null) {
                m.checkNotNullExpressionValue(list, "eventList");
                Iterator<GuildScheduledEvent> it = list.iterator();
                int i2 = 0;
                while (true) {
                    if (!it.hasNext()) {
                        i2 = -1;
                        break;
                    }
                    if (it.next().i() == i) {
                        break;
                    }
                    i2++;
                }
                if (i2 != -1) {
                    list.set(i2, a);
                } else {
                    list.add(a);
                }
            } else {
                this.guildScheduledEvents.put(Long.valueOf(guildScheduledEvent.h()), n.mutableListOf(a));
            }
            markChanged();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void processRsvpDelete(GuildScheduledEvent guildScheduledEvent, long j) {
        long i = guildScheduledEvent.i();
        if (j != this.userStore.getMe().getId() || isMeRsvpedToEvent(guildScheduledEvent.h(), i)) {
            this.rsvpsAwaitingSnapshot.add(Long.valueOf(i));
            removeUserRsvpForEvent(j, guildScheduledEvent.h(), i);
            List<GuildScheduledEvent> list = this.guildScheduledEvents.get(Long.valueOf(guildScheduledEvent.h()));
            if (list != null) {
                m.checkNotNullExpressionValue(list, "eventList");
                Iterator<GuildScheduledEvent> it = list.iterator();
                int i2 = 0;
                while (true) {
                    if (!it.hasNext()) {
                        i2 = -1;
                        break;
                    }
                    if (it.next().i() == i) {
                        break;
                    }
                    i2++;
                }
                Integer valueOf = Integer.valueOf(i2);
                if (!(valueOf.intValue() != -1)) {
                    valueOf = null;
                }
                if (valueOf != null) {
                    int intValue = valueOf.intValue();
                    Integer n = guildScheduledEvent.n();
                    list.set(intValue, GuildScheduledEvent.a(guildScheduledEvent, 0L, 0L, null, null, null, null, null, null, null, null, null, null, null, null, null, Integer.valueOf(f.coerceAtLeast(0, n != null ? n.intValue() - 1 : 0)), null, null, 229375));
                }
            }
            markChanged();
        }
    }

    @StoreThread
    private final void removeMeRsvpForEvent(long j, long j2) {
        Set<Long> set = this.meGuildScheduledEventIds.get(Long.valueOf(j));
        if (set != null) {
            set.remove(Long.valueOf(j2));
        }
    }

    @StoreThread
    private final void removeUserRsvpForEvent(long j, long j2, long j3) {
        if (j == this.userStore.getMe().getId() && isMeRsvpedToEvent(j2, j3)) {
            removeMeRsvpForEvent(j2, j3);
        }
        HashMap<Long, GuildScheduledEventUser> hashMap = this.guildScheduledEventUsers.get(Long.valueOf(j3));
        if (hashMap != null) {
            hashMap.remove(Long.valueOf(j));
        }
    }

    public final void addGuildScheduledEventFromInvite(GuildScheduledEvent guildScheduledEvent) {
        int i;
        m.checkNotNullParameter(guildScheduledEvent, "guildScheduledEvent");
        long h = guildScheduledEvent.h();
        List<GuildScheduledEvent> list = this.guildScheduledEvents.get(Long.valueOf(h));
        if (list != null) {
            Iterator<GuildScheduledEvent> it = list.iterator();
            i = 0;
            while (it.hasNext()) {
                if (it.next().i() == guildScheduledEvent.i()) {
                    break;
                }
                i++;
            }
        }
        i = -1;
        if (i == -1 && !this.guildScheduledEvents.containsKey(Long.valueOf(h))) {
            this.guildScheduledEvents.put(Long.valueOf(h), n.mutableListOf(guildScheduledEvent));
        } else if (i == -1) {
            ((List) h0.getValue(this.guildScheduledEvents, Long.valueOf(h))).add(guildScheduledEvent);
        } else {
            ((List) h0.getValue(this.guildScheduledEvents, Long.valueOf(h))).set(i, guildScheduledEvent);
        }
        if (guildScheduledEvent.o() != null) {
            addMeRsvpForEvent(h, guildScheduledEvent.i());
        }
        markChanged();
    }

    @StoreThread
    public final void addMeRsvpsForEvent(List<Pair<Long, Long>> list) {
        m.checkNotNullParameter(list, "events");
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            Pair pair = (Pair) it.next();
            addMeRsvpForEvent(((Number) pair.getFirst()).longValue(), ((Number) pair.getSecond()).longValue());
        }
        markChanged();
    }

    public final void fetchGuildScheduledEventUserCounts(long j) {
        List<GuildScheduledEvent> list = this.guildScheduledEvents.get(Long.valueOf(j));
        if (!(list == null || list.isEmpty())) {
            Long l = this.guildScheduledEventsFetchTimestamps.get(Long.valueOf(j));
            if (l == null || this.clock.currentTimeMillis() - l.longValue() >= FETCH_GUILD_EVENTS_THRESHOLD) {
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().getGuildScheduledEvents(j, true), false, 1, null), StoreGuildScheduledEvents.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreGuildScheduledEvents$fetchGuildScheduledEventUserCounts$1(this, j));
            }
        }
    }

    public final void fetchGuildScheduledEventUsers(long j, long j2) {
        this.dispatcher.schedule(new StoreGuildScheduledEvents$fetchGuildScheduledEventUsers$1(this, j, j2));
    }

    public final void fetchMeGuildScheduledEvents(long j) {
        List<GuildScheduledEvent> list = this.guildScheduledEvents.get(Long.valueOf(j));
        if (!(list == null || list.isEmpty()) && !this.meGuildScheduledEventsFetches.contains(Long.valueOf(j))) {
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().getMeGuildScheduledEvents(j), false, 1, null), StoreGuildScheduledEvents.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreGuildScheduledEvents$fetchMeGuildScheduledEvents$1(this, j));
        }
    }

    public final GuildScheduledEvent getActiveEventForChannel(Long l, Long l2) {
        boolean z2;
        Object obj = null;
        if (l == null || l2 == null) {
            return null;
        }
        Iterator it = getGuildScheduledEvents$default(this, l.longValue(), false, 2, null).iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            GuildScheduledEvent guildScheduledEvent = (GuildScheduledEvent) next;
            if (!m.areEqual(guildScheduledEvent.b(), l2) || guildScheduledEvent.m() != GuildScheduledEventStatus.ACTIVE) {
                z2 = false;
                continue;
            } else {
                z2 = true;
                continue;
            }
            if (z2) {
                obj = next;
                break;
            }
        }
        return (GuildScheduledEvent) obj;
    }

    public final Map<Long, List<GuildScheduledEvent>> getAllGuildScheduledEvents() {
        return this.guildScheduledEventsSnapshot;
    }

    public final Map<Long, GuildScheduledEventUser> getGuildScheduledEventUsers(long j) {
        HashMap<Long, GuildScheduledEventUser> hashMap = this.guildScheduledEventUsers.get(Long.valueOf(j));
        return hashMap != null ? hashMap : h0.emptyMap();
    }

    /* JADX WARN: Removed duplicated region for block: B:22:0x005d A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:24:0x001d A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.util.List<com.discord.api.guildscheduledevent.GuildScheduledEvent> getGuildScheduledEvents(long r8, boolean r10) {
        /*
            r7 = this;
            com.discord.stores.StorePermissions r0 = r7.permissionsStore
            java.util.Map r0 = r0.getPermissionsByChannel()
            java.util.Map<java.lang.Long, ? extends java.util.List<com.discord.api.guildscheduledevent.GuildScheduledEvent>> r1 = r7.guildScheduledEventsSnapshot
            java.lang.Long r8 = java.lang.Long.valueOf(r8)
            java.lang.Object r8 = r1.get(r8)
            java.util.List r8 = (java.util.List) r8
            if (r8 == 0) goto L61
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            java.util.Iterator r8 = r8.iterator()
        L1d:
            boolean r1 = r8.hasNext()
            if (r1 == 0) goto L65
            java.lang.Object r1 = r8.next()
            r2 = r1
            com.discord.api.guildscheduledevent.GuildScheduledEvent r2 = (com.discord.api.guildscheduledevent.GuildScheduledEvent) r2
            r3 = 0
            r4 = 1
            if (r10 != 0) goto L30
        L2e:
            r3 = 1
            goto L5b
        L30:
            com.discord.api.guildscheduledevent.GuildScheduledEventStatus r5 = r2.m()
            com.discord.api.guildscheduledevent.GuildScheduledEventStatus r6 = com.discord.api.guildscheduledevent.GuildScheduledEventStatus.COMPLETED
            if (r5 != r6) goto L39
            goto L5b
        L39:
            java.lang.Long r5 = r2.b()
            if (r5 != 0) goto L40
            goto L2e
        L40:
            java.lang.Long r2 = r2.b()
            java.lang.Object r2 = r0.get(r2)
            java.lang.Long r2 = (java.lang.Long) r2
            if (r2 == 0) goto L5b
            long r2 = r2.longValue()
            r4 = 1024(0x400, double:5.06E-321)
            java.lang.Long r2 = java.lang.Long.valueOf(r2)
            boolean r2 = com.discord.utilities.permissions.PermissionUtils.can(r4, r2)
            r3 = r2
        L5b:
            if (r3 == 0) goto L1d
            r9.add(r1)
            goto L1d
        L61:
            java.util.List r9 = d0.t.n.emptyList()
        L65:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreGuildScheduledEvents.getGuildScheduledEvents(long, boolean):java.util.List");
    }

    public final boolean getIsFetchingGuildScheduledEventUsers() {
        return this.isFetchingGuildScheduledEventUsers;
    }

    public final boolean getIsGuildScheduledEventUsersError() {
        return this.isGuildScheduledEventUsersError;
    }

    public final Set<Long> getMeGuildScheduledEventIds(long j) {
        Set<Long> set = this.meGuildScheduledEventIdsSnapshot.get(Long.valueOf(j));
        return set != null ? set : n0.emptySet();
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        this.guildScheduledEventsFetchTimestamps.clear();
        this.meGuildScheduledEventsFetches.clear();
        this.guildScheduledEventUsersFetches.clear();
        this.guildScheduledEvents.clear();
        this.meGuildScheduledEventIds.clear();
        List<Guild> guilds = modelPayload.getGuilds();
        m.checkNotNullExpressionValue(guilds, "payload.guilds");
        for (Guild guild : guilds) {
            List<GuildScheduledEvent> o = guild.o();
            if (o != null) {
                for (GuildScheduledEvent guildScheduledEvent : o) {
                    processGuildScheduledEventCreateOrUpdate(guildScheduledEvent);
                }
            }
        }
        markChanged();
    }

    @StoreThread
    public final void handleGuildCreate(Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        List<GuildScheduledEvent> o = guild.o();
        if (o != null) {
            for (GuildScheduledEvent guildScheduledEvent : o) {
                processGuildScheduledEventCreateOrUpdate(guildScheduledEvent);
            }
        }
    }

    @StoreThread
    public final void handleGuildRemove(long j) {
        this.guildScheduledEvents.remove(Long.valueOf(j));
        this.guildScheduledEventsFetchTimestamps.remove(Long.valueOf(j));
        this.meGuildScheduledEventIds.remove(Long.valueOf(j));
        this.meGuildScheduledEventsFetches.remove(Long.valueOf(j));
        this.guildScheduledEventUsersFetches.remove(Long.valueOf(j));
        markChanged();
    }

    @StoreThread
    public final void handleGuildScheduledEventCreate(GuildScheduledEvent guildScheduledEvent) {
        m.checkNotNullParameter(guildScheduledEvent, "guildScheduledEvent");
        processGuildScheduledEventCreateOrUpdate(guildScheduledEvent);
    }

    @StoreThread
    public final void handleGuildScheduledEventDelete(GuildScheduledEvent guildScheduledEvent) {
        List<GuildScheduledEvent> list;
        m.checkNotNullParameter(guildScheduledEvent, "guildScheduledEvent");
        List<GuildScheduledEvent> list2 = this.guildScheduledEvents.get(Long.valueOf(guildScheduledEvent.h()));
        if (list2 != null) {
            Iterator<GuildScheduledEvent> it = list2.iterator();
            int i = 0;
            while (true) {
                if (!it.hasNext()) {
                    i = -1;
                    break;
                }
                if (it.next().i() == guildScheduledEvent.i()) {
                    break;
                }
                i++;
            }
            if (!(i == -1 || (list = this.guildScheduledEvents.get(Long.valueOf(guildScheduledEvent.h()))) == null)) {
                list.remove(i);
            }
            removeMeRsvpForEvent(guildScheduledEvent.h(), guildScheduledEvent.i());
            markChanged();
        }
    }

    @StoreThread
    public final void handleGuildScheduledEventUpdate(GuildScheduledEvent guildScheduledEvent) {
        m.checkNotNullParameter(guildScheduledEvent, "guildScheduledEvent");
        processGuildScheduledEventCreateOrUpdate(guildScheduledEvent);
    }

    @StoreThread
    public final void handleGuildScheduledEventUserAdd(GuildScheduledEventUserUpdate guildScheduledEventUserUpdate) {
        m.checkNotNullParameter(guildScheduledEventUserUpdate, "guildScheduledEventUserUpdate");
        GuildScheduledEvent findEventFromStore$default = findEventFromStore$default(this, guildScheduledEventUserUpdate.a(), null, 2, null);
        if (findEventFromStore$default != null) {
            processRsvpCreate(findEventFromStore$default, guildScheduledEventUserUpdate.b());
        }
    }

    @StoreThread
    public final void handleGuildScheduledEventUserRemove(GuildScheduledEventUserUpdate guildScheduledEventUserUpdate) {
        m.checkNotNullParameter(guildScheduledEventUserUpdate, "guildScheduledEventUserUpdate");
        GuildScheduledEvent findEventFromStore$default = findEventFromStore$default(this, guildScheduledEventUserUpdate.a(), null, 2, null);
        if (findEventFromStore$default != null) {
            processRsvpDelete(findEventFromStore$default, guildScheduledEventUserUpdate.b());
        }
    }

    public final boolean isMeRsvpedToEvent(long j, long j2) {
        Set<Long> set = this.meGuildScheduledEventIdsSnapshot.get(Long.valueOf(j));
        if (set == null) {
            set = n0.emptySet();
        }
        return set.contains(Long.valueOf(j2));
    }

    public final Observable<GuildScheduledEvent> observeGuildScheduledEvent(Long l, Long l2) {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreGuildScheduledEvents$observeGuildScheduledEvent$1(this, l, l2), 14, null);
    }

    public final Observable<List<GuildScheduledEvent>> observeGuildScheduledEvents(long j, boolean z2) {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreGuildScheduledEvents$observeGuildScheduledEvents$1(this, j, z2), 14, null);
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        super.snapshotData();
        this.guildScheduledEventsSnapshot = new HashMap(this.guildScheduledEvents);
        this.meGuildScheduledEventIdsSnapshot = new HashMap(this.meGuildScheduledEventIds);
        this.guildScheduledEventUsersSnapshot = new HashMap(this.guildScheduledEventUsers);
        this.rsvpsAwaitingSnapshot.clear();
    }

    public final void toggleMeRsvpForEvent(GuildScheduledEvent guildScheduledEvent) {
        m.checkNotNullParameter(guildScheduledEvent, "guildScheduledEvent");
        GuildScheduledEvent findEventFromStore = findEventFromStore(guildScheduledEvent);
        long i = findEventFromStore.i();
        if (!this.rsvpsAwaitingNetwork.contains(Long.valueOf(i)) && !this.rsvpsAwaitingSnapshot.contains(Long.valueOf(i))) {
            this.rsvpsAwaitingNetwork.add(Long.valueOf(i));
            this.dispatcher.schedule(new StoreGuildScheduledEvents$toggleMeRsvpForEvent$1(this, guildScheduledEvent, i, findEventFromStore));
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final GuildScheduledEvent findEventFromStore(long j, Long l) {
        boolean z2;
        GuildScheduledEvent guildScheduledEvent;
        Object obj;
        boolean z3;
        GuildScheduledEvent guildScheduledEvent2 = null;
        if (l != null) {
            List<GuildScheduledEvent> list = this.guildScheduledEventsSnapshot.get(Long.valueOf(l.longValue()));
            if (list != null) {
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj = null;
                        break;
                    }
                    obj = it.next();
                    if (j == ((GuildScheduledEvent) obj).i()) {
                        z3 = true;
                        continue;
                    } else {
                        z3 = false;
                        continue;
                    }
                    if (z3) {
                        break;
                    }
                }
                guildScheduledEvent = (GuildScheduledEvent) obj;
            } else {
                guildScheduledEvent = null;
            }
            if (guildScheduledEvent != null) {
                return guildScheduledEvent;
            }
        }
        Iterator it2 = o.flatten(this.guildScheduledEventsSnapshot.values()).iterator();
        while (true) {
            if (!it2.hasNext()) {
                break;
            }
            Object next = it2.next();
            if (j == ((GuildScheduledEvent) next).i()) {
                z2 = true;
                continue;
            } else {
                z2 = false;
                continue;
            }
            if (z2) {
                guildScheduledEvent2 = next;
                break;
            }
        }
        return guildScheduledEvent2;
    }
}
