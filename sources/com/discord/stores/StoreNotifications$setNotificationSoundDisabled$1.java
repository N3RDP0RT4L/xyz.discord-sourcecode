package com.discord.stores;

import com.discord.utilities.fcm.NotificationClient;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreNotifications.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;", "it", "invoke", "(Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;)Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreNotifications$setNotificationSoundDisabled$1 extends o implements Function1<NotificationClient.SettingsV2, NotificationClient.SettingsV2> {
    public final /* synthetic */ boolean $notificationSoundDisabled;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreNotifications$setNotificationSoundDisabled$1(boolean z2) {
        super(1);
        this.$notificationSoundDisabled = z2;
    }

    public final NotificationClient.SettingsV2 invoke(NotificationClient.SettingsV2 settingsV2) {
        NotificationClient.SettingsV2 copy;
        m.checkNotNullParameter(settingsV2, "it");
        copy = settingsV2.copy((r20 & 1) != 0 ? settingsV2.isEnabled : false, (r20 & 2) != 0 ? settingsV2.isEnabledInApp : false, (r20 & 4) != 0 ? settingsV2.isWake : false, (r20 & 8) != 0 ? settingsV2.isDisableBlink : false, (r20 & 16) != 0 ? settingsV2.isDisableSound : this.$notificationSoundDisabled, (r20 & 32) != 0 ? settingsV2.isDisableVibrate : false, (r20 & 64) != 0 ? settingsV2.token : null, (r20 & 128) != 0 ? settingsV2.locale : null, (r20 & 256) != 0 ? settingsV2.sendBlockedChannels : null);
        return copy;
    }
}
