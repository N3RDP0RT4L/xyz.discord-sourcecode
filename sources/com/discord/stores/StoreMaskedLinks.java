package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import androidx.annotation.VisibleForTesting;
import com.discord.BuildConfig;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelUserRelationship;
import com.discord.models.user.User;
import d0.t.n;
import d0.t.n0;
import d0.t.u;
import d0.z.d.m;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.text.Regex;
/* compiled from: StoreMaskedLinks.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010#\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 $2\u00020\u0001:\u0001$B\u001f\u0012\u0006\u0010\u001c\u001a\u00020\u001b\u0012\u0006\u0010\u0019\u001a\u00020\u0018\u0012\u0006\u0010\u001f\u001a\u00020\u001e¢\u0006\u0004\b\"\u0010#J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0013\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00020\u0007¢\u0006\u0004\b\b\u0010\tJ\u001f\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\n\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0005\u0010\u000bJ\u0015\u0010\r\u001a\u00020\f2\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0011\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\u000fH\u0017¢\u0006\u0004\b\u0011\u0010\u0012J\u000f\u0010\u0013\u001a\u00020\fH\u0017¢\u0006\u0004\b\u0013\u0010\u0014R\u001c\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00020\u00158\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0019\u001a\u00020\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001c\u001a\u00020\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR\u0016\u0010\u001f\u001a\u00020\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u001c\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00020\u00078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b!\u0010\u0017¨\u0006%"}, d2 = {"Lcom/discord/stores/StoreMaskedLinks;", "Lcom/discord/stores/StoreV2;", "", "url", "", "isTrustedDomain", "(Ljava/lang/String;)Z", "", "getUserTrustedDomains", "()Ljava/util/Set;", "mask", "(Ljava/lang/String;Ljava/lang/String;)Z", "", "trustDomain", "(Ljava/lang/String;)V", "Landroid/content/Context;", "context", "init", "(Landroid/content/Context;)V", "snapshotData", "()V", "", "userTrustedDomains", "Ljava/util/Set;", "Lcom/discord/stores/StoreChannelsSelected;", "storeChannelsSelected", "Lcom/discord/stores/StoreChannelsSelected;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/stores/StoreUserRelationships;", "storeUserRelationships", "Lcom/discord/stores/StoreUserRelationships;", "userTrustedDomainsSnapshot", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreUserRelationships;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMaskedLinks extends StoreV2 {
    public static final Companion Companion;
    private static final Regex DISCORD_DOMAINS_REGEX = new Regex("(?:^|\\.)(?:discordapp|discord)\\.com$");
    private static final String HOST_SPOTIFY = "https://spotify.com";
    private static final String HOST_SPOTIFY_OPEN = "https://open.spotify.com";
    private static final List<String> TRUSTED_DOMAINS;
    private static final String USER_TRUSTED_DOMAINS_CACHE_KEY = "USER_TRUSTED_DOMAINS_CACHE_KEY";
    private final Dispatcher dispatcher;
    private final StoreChannelsSelected storeChannelsSelected;
    private final StoreUserRelationships storeUserRelationships;
    private Set<String> userTrustedDomainsSnapshot = n0.emptySet();
    private Set<String> userTrustedDomains = new LinkedHashSet();

    /* compiled from: StoreMaskedLinks.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0019\u0010\u0004\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0017\u0010\n\u001a\u00020\u00072\u0006\u0010\u0006\u001a\u00020\u0002H\u0001¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\f\u001a\u00020\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\rR\u0016\u0010\u000e\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0010\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0010\u0010\u000fR\u001c\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00020\u00118\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0014\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0014\u0010\u000f¨\u0006\u0017"}, d2 = {"Lcom/discord/stores/StoreMaskedLinks$Companion;", "", "", "url", "getDomainName", "(Ljava/lang/String;)Ljava/lang/String;", ModelAuditLogEntry.CHANGE_KEY_NAME, "", "isImplicitlyTrustedDomain$app_productionGoogleRelease", "(Ljava/lang/String;)Z", "isImplicitlyTrustedDomain", "Lkotlin/text/Regex;", "DISCORD_DOMAINS_REGEX", "Lkotlin/text/Regex;", "HOST_SPOTIFY", "Ljava/lang/String;", "HOST_SPOTIFY_OPEN", "", "TRUSTED_DOMAINS", "Ljava/util/List;", StoreMaskedLinks.USER_TRUSTED_DOMAINS_CACHE_KEY, HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final String getDomainName(String str) {
            try {
                Uri parse = Uri.parse(str);
                m.checkNotNullExpressionValue(parse, "Uri.parse(url)");
                return parse.getHost();
            } catch (Exception unused) {
                return null;
            }
        }

        @VisibleForTesting
        public final boolean isImplicitlyTrustedDomain$app_productionGoogleRelease(String str) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            return StoreMaskedLinks.TRUSTED_DOMAINS.contains(str) || StoreMaskedLinks.DISCORD_DOMAINS_REGEX.containsMatchIn(str);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    static {
        Companion companion = new Companion(null);
        Companion = companion;
        TRUSTED_DOMAINS = n.listOfNotNull((Object[]) new String[]{companion.getDomainName(BuildConfig.HOST), companion.getDomainName(BuildConfig.HOST_ALTERNATE), companion.getDomainName(BuildConfig.HOST_CDN), companion.getDomainName(BuildConfig.HOST_GIFT), companion.getDomainName(BuildConfig.HOST_INVITE), companion.getDomainName(BuildConfig.HOST_GUILD_TEMPLATE), companion.getDomainName(HOST_SPOTIFY), companion.getDomainName(HOST_SPOTIFY_OPEN)});
    }

    public StoreMaskedLinks(Dispatcher dispatcher, StoreChannelsSelected storeChannelsSelected, StoreUserRelationships storeUserRelationships) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(storeChannelsSelected, "storeChannelsSelected");
        m.checkNotNullParameter(storeUserRelationships, "storeUserRelationships");
        this.dispatcher = dispatcher;
        this.storeChannelsSelected = storeChannelsSelected;
        this.storeUserRelationships = storeUserRelationships;
    }

    public final Set<String> getUserTrustedDomains() {
        return this.userTrustedDomainsSnapshot;
    }

    @Override // com.discord.stores.Store
    @StoreThread
    public void init(Context context) {
        m.checkNotNullParameter(context, "context");
        super.init(context);
        Set<String> stringSet = getPrefs().getStringSet(USER_TRUSTED_DOMAINS_CACHE_KEY, n0.emptySet());
        if (stringSet == null) {
            stringSet = new LinkedHashSet<>();
        }
        this.userTrustedDomains = new HashSet(stringSet);
        markChanged();
    }

    public final boolean isTrustedDomain(String str, String str2) {
        m.checkNotNullParameter(str, "url");
        if (isTrustedDomain(str)) {
            return true;
        }
        boolean z2 = str2 == null || m.areEqual(str2, str);
        Channel selectedChannel = this.storeChannelsSelected.getSelectedChannel();
        Long l = null;
        User a = selectedChannel != null ? ChannelUtils.a(selectedChannel) : null;
        Map<Long, Integer> relationships = this.storeUserRelationships.getRelationships();
        if (a != null) {
            l = Long.valueOf(a.getId());
        }
        if (!ModelUserRelationship.isType(relationships.get(l), 1)) {
            return false;
        }
        return z2;
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        super.snapshotData();
        this.userTrustedDomainsSnapshot = new HashSet(this.userTrustedDomains);
        SharedPreferences.Editor edit = getPrefs().edit();
        m.checkNotNullExpressionValue(edit, "editor");
        edit.putStringSet(USER_TRUSTED_DOMAINS_CACHE_KEY, this.userTrustedDomainsSnapshot);
        edit.apply();
    }

    public final void trustDomain(String str) {
        m.checkNotNullParameter(str, "url");
        this.dispatcher.schedule(new StoreMaskedLinks$trustDomain$1(this, str));
    }

    private final boolean isTrustedDomain(String str) {
        Companion companion = Companion;
        String domainName = companion.getDomainName(str);
        return u.contains(this.userTrustedDomainsSnapshot, domainName) || (domainName != null && companion.isImplicitlyTrustedDomain$app_productionGoogleRelease(domainName));
    }
}
