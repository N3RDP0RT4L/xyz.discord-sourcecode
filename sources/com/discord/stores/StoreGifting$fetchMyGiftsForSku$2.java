package com.discord.stores;

import com.discord.app.AppLog;
import com.discord.stores.StoreGifting;
import com.discord.utilities.error.Error;
import com.discord.utilities.logging.Logger;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreGifting.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "error", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGifting$fetchMyGiftsForSku$2 extends o implements Function1<Error, Unit> {
    public final /* synthetic */ String $comboId;
    public final /* synthetic */ StoreGifting this$0;

    /* compiled from: StoreGifting.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreGifting$fetchMyGiftsForSku$2$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ Error $error;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Error error) {
            super(0);
            this.$error = error;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            int ordinal = this.$error.getType().ordinal();
            if (ordinal == 3) {
                this.$error.setShowErrorToasts(false);
                StoreGifting$fetchMyGiftsForSku$2 storeGifting$fetchMyGiftsForSku$2 = StoreGifting$fetchMyGiftsForSku$2.this;
                storeGifting$fetchMyGiftsForSku$2.this$0.setGifts(storeGifting$fetchMyGiftsForSku$2.$comboId, StoreGifting.GiftState.Invalid.INSTANCE);
            } else if (ordinal != 11) {
                Logger.e$default(AppLog.g, "Fetching Gift Error", new Exception(String.valueOf(this.$error.getType())), null, 4, null);
                StoreGifting$fetchMyGiftsForSku$2 storeGifting$fetchMyGiftsForSku$22 = StoreGifting$fetchMyGiftsForSku$2.this;
                storeGifting$fetchMyGiftsForSku$22.this$0.setGifts(storeGifting$fetchMyGiftsForSku$22.$comboId, StoreGifting.GiftState.LoadFailed.INSTANCE);
            } else {
                StoreGifting$fetchMyGiftsForSku$2 storeGifting$fetchMyGiftsForSku$23 = StoreGifting$fetchMyGiftsForSku$2.this;
                storeGifting$fetchMyGiftsForSku$23.this$0.setGifts(storeGifting$fetchMyGiftsForSku$23.$comboId, StoreGifting.GiftState.LoadFailed.INSTANCE);
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGifting$fetchMyGiftsForSku$2(StoreGifting storeGifting, String str) {
        super(1);
        this.this$0 = storeGifting;
        this.$comboId = str;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Error error) {
        invoke2(error);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Error error) {
        m.checkNotNullParameter(error, "error");
        this.this$0.getDispatcher().schedule(new AnonymousClass1(error));
    }
}
