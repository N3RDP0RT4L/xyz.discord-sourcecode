package com.discord.stores;

import com.discord.api.guild.welcome.GuildWelcomeScreen;
import com.discord.stores.StoreGuildWelcomeScreens;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreGuildWelcomeScreens.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildWelcomeScreens$fetchIfNonexisting$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ StoreGuildWelcomeScreens this$0;

    /* compiled from: StoreGuildWelcomeScreens.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/guild/welcome/GuildWelcomeScreen;", "guildWelcomeScreen", "", "invoke", "(Lcom/discord/api/guild/welcome/GuildWelcomeScreen;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreGuildWelcomeScreens$fetchIfNonexisting$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<GuildWelcomeScreen, Unit> {

        /* compiled from: StoreGuildWelcomeScreens.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreGuildWelcomeScreens$fetchIfNonexisting$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C02071 extends o implements Function0<Unit> {
            public final /* synthetic */ GuildWelcomeScreen $guildWelcomeScreen;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C02071(GuildWelcomeScreen guildWelcomeScreen) {
                super(0);
                this.$guildWelcomeScreen = guildWelcomeScreen;
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                StoreGuildWelcomeScreens$fetchIfNonexisting$1 storeGuildWelcomeScreens$fetchIfNonexisting$1 = StoreGuildWelcomeScreens$fetchIfNonexisting$1.this;
                storeGuildWelcomeScreens$fetchIfNonexisting$1.this$0.handleGuildWelcomeScreen(storeGuildWelcomeScreens$fetchIfNonexisting$1.$guildId, this.$guildWelcomeScreen);
            }
        }

        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(GuildWelcomeScreen guildWelcomeScreen) {
            invoke2(guildWelcomeScreen);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(GuildWelcomeScreen guildWelcomeScreen) {
            Dispatcher dispatcher;
            m.checkNotNullParameter(guildWelcomeScreen, "guildWelcomeScreen");
            dispatcher = StoreGuildWelcomeScreens$fetchIfNonexisting$1.this.this$0.dispatcher;
            dispatcher.schedule(new C02071(guildWelcomeScreen));
        }
    }

    /* compiled from: StoreGuildWelcomeScreens.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreGuildWelcomeScreens$fetchIfNonexisting$1$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2 extends o implements Function1<Error, Unit> {

        /* compiled from: StoreGuildWelcomeScreens.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreGuildWelcomeScreens$fetchIfNonexisting$1$2$1  reason: invalid class name */
        /* loaded from: classes.dex */
        public static final class AnonymousClass1 extends o implements Function0<Unit> {
            public AnonymousClass1() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                StoreGuildWelcomeScreens$fetchIfNonexisting$1 storeGuildWelcomeScreens$fetchIfNonexisting$1 = StoreGuildWelcomeScreens$fetchIfNonexisting$1.this;
                storeGuildWelcomeScreens$fetchIfNonexisting$1.this$0.handleGuildWelcomeScreenFetchFailed(storeGuildWelcomeScreens$fetchIfNonexisting$1.$guildId);
            }
        }

        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Error error) {
            invoke2(error);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Error error) {
            Dispatcher dispatcher;
            m.checkNotNullParameter(error, "it");
            dispatcher = StoreGuildWelcomeScreens$fetchIfNonexisting$1.this.this$0.dispatcher;
            dispatcher.schedule(new AnonymousClass1());
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGuildWelcomeScreens$fetchIfNonexisting$1(StoreGuildWelcomeScreens storeGuildWelcomeScreens, long j) {
        super(0);
        this.this$0 = storeGuildWelcomeScreens;
        this.$guildId = j;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        HashMap hashMap;
        hashMap = this.this$0.guildWelcomeScreensState;
        StoreGuildWelcomeScreens.State state = (StoreGuildWelcomeScreens.State) hashMap.get(Long.valueOf(this.$guildId));
        if (!(state instanceof StoreGuildWelcomeScreens.State.Loaded) && !(state instanceof StoreGuildWelcomeScreens.State.Fetching)) {
            this.this$0.handleGuildWelcomeScreenFetchStart(this.$guildId);
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn(RestAPI.Companion.getApi().getGuildWelcomeScreen(this.$guildId), false), this.this$0.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new AnonymousClass2(), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
        }
    }
}
