package com.discord.stores;

import com.discord.api.message.reaction.MessageReactionEmoji;
import com.discord.stores.StoreMessageReactions;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreMessageReactions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/stores/StoreMessageReactions$EmojiResults;", "invoke", "()Lcom/discord/stores/StoreMessageReactions$EmojiResults;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessageReactions$observeMessageReactions$2 extends o implements Function0<StoreMessageReactions.EmojiResults> {
    public final /* synthetic */ MessageReactionEmoji $emoji;
    public final /* synthetic */ long $messageId;
    public final /* synthetic */ StoreMessageReactions this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreMessageReactions$observeMessageReactions$2(StoreMessageReactions storeMessageReactions, long j, MessageReactionEmoji messageReactionEmoji) {
        super(0);
        this.this$0 = storeMessageReactions;
        this.$messageId = j;
        this.$emoji = messageReactionEmoji;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final StoreMessageReactions.EmojiResults invoke() {
        Map map;
        StoreMessageReactions.EmojiResults emojiResults;
        map = this.this$0.reactionsSnapshot;
        Map map2 = (Map) map.get(Long.valueOf(this.$messageId));
        return (map2 == null || (emojiResults = (StoreMessageReactions.EmojiResults) map2.get(this.$emoji.c())) == null) ? StoreMessageReactions.EmojiResults.Loading.INSTANCE : emojiResults;
    }
}
