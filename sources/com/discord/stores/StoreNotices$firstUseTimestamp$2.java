package com.discord.stores;

import android.content.SharedPreferences;
import com.discord.utilities.time.Clock;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreNotices.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00060\u0000j\u0002`\u0001H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"", "Lcom/discord/primitives/Timestamp;", "invoke", "()J", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreNotices$firstUseTimestamp$2 extends o implements Function0<Long> {
    public final /* synthetic */ StoreNotices this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreNotices$firstUseTimestamp$2(StoreNotices storeNotices) {
        super(0);
        this.this$0 = storeNotices;
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [long, java.lang.Long] */
    /* JADX WARN: Type inference failed for: r4v1, types: [long, java.lang.Long] */
    @Override // kotlin.jvm.functions.Function0
    public final Long invoke() {
        Clock clock;
        ?? r4 = this.this$0.getPrefsSessionDurable().getLong("CACHE_KEY_FIRST_USE", -1L);
        if (r4 != -1) {
            return r4;
        }
        clock = this.this$0.clock;
        ?? currentTimeMillis = clock.currentTimeMillis();
        SharedPreferences.Editor edit = this.this$0.getPrefsSessionDurable().edit();
        m.checkNotNullExpressionValue(edit, "editor");
        edit.putLong("CACHE_KEY_FIRST_USE", currentTimeMillis);
        edit.apply();
        return currentTimeMillis;
    }
}
