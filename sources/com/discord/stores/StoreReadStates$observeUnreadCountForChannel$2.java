package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.models.message.Message;
import com.discord.stores.StoreMessageAck;
import d0.t.s;
import d0.z.d.m;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import rx.functions.Func2;
/* compiled from: StoreReadStates.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\u0010\n\u001a\n \u0003*\u0004\u0018\u00010\u00070\u00072\"\u0010\u0004\u001a\u001e\u0012\b\u0012\u00060\u0001j\u0002`\u0002 \u0003*\u000e\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0018\u00010\u00000\u00002\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\n¢\u0006\u0004\b\b\u0010\t"}, d2 = {"", "Lcom/discord/models/message/Message;", "Lcom/discord/stores/ClientMessage;", "kotlin.jvm.PlatformType", "messages", "Lcom/discord/stores/StoreMessageAck$Ack;", "mostRecentMessageAck", "", NotificationCompat.CATEGORY_CALL, "(Ljava/util/List;Lcom/discord/stores/StoreMessageAck$Ack;)Ljava/lang/Integer;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreReadStates$observeUnreadCountForChannel$2<T1, T2, R> implements Func2<List<? extends Message>, StoreMessageAck.Ack, Integer> {
    public static final StoreReadStates$observeUnreadCountForChannel$2 INSTANCE = new StoreReadStates$observeUnreadCountForChannel$2();

    @Override // rx.functions.Func2
    public /* bridge */ /* synthetic */ Integer call(List<? extends Message> list, StoreMessageAck.Ack ack) {
        return call2((List<Message>) list, ack);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final Integer call2(List<Message> list, StoreMessageAck.Ack ack) {
        int i = 0;
        if (!list.isEmpty()) {
            if ((ack != null ? Long.valueOf(ack.getMessageId()) : null) != null) {
                m.checkNotNullExpressionValue(list, "messages");
                Iterator it = s.asReversed(list).iterator();
                while (it.hasNext() && ((Message) it.next()).getId() != ack.getMessageId()) {
                    i++;
                }
            }
        }
        return Integer.valueOf(i);
    }
}
