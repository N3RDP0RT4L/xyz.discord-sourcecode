package com.discord.stores;

import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreMessageAck;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$3;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$4;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import rx.Observable;
import rx.functions.Action1;
/* compiled from: StoreMessageAck.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/stores/StoreMessageAck$PendingAck;", "pendingAck", "", "invoke", "(Lcom/discord/stores/StoreMessageAck$PendingAck;)V", "postChannelMessagesAck"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessageAck$postPendingAck$1 extends o implements Function1<StoreMessageAck.PendingAck, Unit> {
    public final /* synthetic */ int $mentionCount;
    public final /* synthetic */ StoreMessageAck this$0;

    /* compiled from: StoreMessageAck.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreMessageAck$postPendingAck$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ StoreMessageAck.Ack $ack;
        public final /* synthetic */ long $channelId;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(long j, StoreMessageAck.Ack ack) {
            super(0);
            this.$channelId = j;
            this.$ack = ack;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            StoreMessageAck$postPendingAck$1.this.this$0.updateAcks(this.$channelId, this.$ack);
        }
    }

    /* compiled from: StoreMessageAck.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Ljava/lang/Void;", "it", "", "invoke", "(Ljava/lang/Void;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreMessageAck$postPendingAck$1$3  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass3 extends o implements Function1<Void, Unit> {
        public static final AnonymousClass3 INSTANCE = new AnonymousClass3();

        public AnonymousClass3() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Void r1) {
            invoke2(r1);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Void r1) {
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreMessageAck$postPendingAck$1(StoreMessageAck storeMessageAck, int i) {
        super(1);
        this.this$0 = storeMessageAck;
        this.$mentionCount = i;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(StoreMessageAck.PendingAck pendingAck) {
        invoke2(pendingAck);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(StoreMessageAck.PendingAck pendingAck) {
        Dispatcher dispatcher;
        RestAPI restAPI;
        m.checkNotNullParameter(pendingAck, "pendingAck");
        final long component1 = pendingAck.component1();
        StoreMessageAck.Ack component2 = pendingAck.component2();
        dispatcher = this.this$0.dispatcher;
        dispatcher.schedule(new AnonymousClass1(component1, component2));
        restAPI = this.this$0.restAPI;
        Observable t = ObservableExtensionsKt.restSubscribeOn$default(restAPI.postChannelMessagesAck(component1, Long.valueOf(component2.getMessageId()), new RestAPIParams.ChannelMessagesAck(Boolean.valueOf(component2.isLockedAck()), Integer.valueOf(this.$mentionCount))), false, 1, null).t(new Action1<Void>() { // from class: com.discord.stores.StoreMessageAck$postPendingAck$1.2
            public final void call(Void r3) {
                StoreStream.Companion.getAnalytics().ackMessage(component1);
            }
        });
        m.checkNotNullExpressionValue(t, "restAPI\n          .postC…).ackMessage(channelId) }");
        ObservableExtensionsKt.appSubscribe(t, (r18 & 1) != 0 ? null : null, "REST: ack", (r18 & 4) != 0 ? null : null, AnonymousClass3.INSTANCE, (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
    }
}
