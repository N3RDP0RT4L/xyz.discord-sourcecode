package com.discord.stores;

import com.discord.api.channel.Channel;
import d0.t.h0;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreChannels.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u00030\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "invoke", "()Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreChannels$observeAllChannels$1 extends o implements Function0<Map<Long, ? extends Channel>> {
    public final /* synthetic */ StoreChannels this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreChannels$observeAllChannels$1(StoreChannels storeChannels) {
        super(0);
        this.this$0 = storeChannels;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Map<Long, ? extends Channel> invoke() {
        Map map;
        Map map2;
        map = this.this$0.channelsByIdSnapshot;
        map2 = this.this$0.threadsByIdSnapshot;
        return h0.plus(map, map2);
    }
}
