package com.discord.stores;

import com.discord.stores.StoreGuildMemberVerificationForm;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreGuildMemberVerificationForm.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/stores/StoreGuildMemberVerificationForm$MemberVerificationFormData;", "invoke", "()Lcom/discord/stores/StoreGuildMemberVerificationForm$MemberVerificationFormData;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildMemberVerificationForm$observeMemberVerificationFormData$1 extends o implements Function0<StoreGuildMemberVerificationForm.MemberVerificationFormData> {
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ StoreGuildMemberVerificationForm this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGuildMemberVerificationForm$observeMemberVerificationFormData$1(StoreGuildMemberVerificationForm storeGuildMemberVerificationForm, long j) {
        super(0);
        this.this$0 = storeGuildMemberVerificationForm;
        this.$guildId = j;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final StoreGuildMemberVerificationForm.MemberVerificationFormData invoke() {
        return this.this$0.getMemberVerificationFormData(this.$guildId);
    }
}
