package com.discord.stores;

import androidx.core.app.NotificationCompat;
import d0.t.u;
import j0.k.b;
import java.util.Map;
import kotlin.Metadata;
/* compiled from: StoreMentions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\u0010\b\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032.\u0010\u0005\u001a*\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003 \u0004*\u0014\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "", "Lcom/discord/primitives/ChannelId;", "", "kotlin.jvm.PlatformType", "it", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Map;)Ljava/lang/Integer;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMentions$observeTotalMentions$1<T, R> implements b<Map<Long, ? extends Integer>, Integer> {
    public static final StoreMentions$observeTotalMentions$1 INSTANCE = new StoreMentions$observeTotalMentions$1();

    @Override // j0.k.b
    public /* bridge */ /* synthetic */ Integer call(Map<Long, ? extends Integer> map) {
        return call2((Map<Long, Integer>) map);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final Integer call2(Map<Long, Integer> map) {
        return Integer.valueOf(u.sumOfInt(map.values()));
    }
}
