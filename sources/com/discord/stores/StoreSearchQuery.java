package com.discord.stores;

import andhook.lib.HookHelper;
import com.discord.models.domain.ModelSearchResponse;
import com.discord.models.message.Message;
import com.discord.simpleast.core.parser.Parser;
import com.discord.stores.StoreSearch;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$3;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$4;
import com.discord.utilities.search.network.SearchFetcher;
import com.discord.utilities.search.network.SearchQuery;
import com.discord.utilities.search.network.state.QueryFetchState;
import com.discord.utilities.search.network.state.SearchState;
import com.discord.utilities.search.query.node.QueryNode;
import com.discord.utilities.search.query.parsing.QueryParser;
import com.discord.utilities.search.strings.SearchStringProvider;
import com.discord.utilities.search.validation.SearchData;
import d0.g0.t;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Subscription;
import rx.functions.Func2;
import rx.subjects.BehaviorSubject;
import rx.subjects.SerializedSubject;
/* compiled from: StoreSearchQuery.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 :2\u00020\u0001:\u0001:B\u000f\u0012\u0006\u00106\u001a\u000205¢\u0006\u0004\b8\u00109J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ'\u0010\u000e\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\fH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u000f\u0010\u0010\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\u0010\u0010\u0011J;\u0010\u0015\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\u0010\b\u0002\u0010\u0014\u001a\n\u0018\u00010\u0012j\u0004\u0018\u0001`\u00132\b\b\u0002\u0010\r\u001a\u00020\fH\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0017\u0010\u0019\u001a\u00020\u00062\u0006\u0010\u0018\u001a\u00020\u0017H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u000f\u0010\u001b\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\u001b\u0010\u0011J5\u0010#\u001a\u00020\u00062\u0006\u0010\u001d\u001a\u00020\u001c2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u001f\u001a\u00020\u001e2\u0006\u0010!\u001a\u00020 2\u0006\u0010\"\u001a\u00020\f¢\u0006\u0004\b#\u0010$J!\u0010%\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0014\u001a\u00060\u0012j\u0002`\u0013¢\u0006\u0004\b%\u0010&J\r\u0010'\u001a\u00020\u0006¢\u0006\u0004\b'\u0010\u0011R2\u0010*\u001a\u001e\u0012\f\u0012\n )*\u0004\u0018\u00010\u00170\u0017\u0012\f\u0012\n )*\u0004\u0018\u00010\u00170\u00170(8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010+R\u0019\u0010/\u001a\b\u0012\u0004\u0012\u00020\u00170,8F@\u0006¢\u0006\u0006\u001a\u0004\b-\u0010.R\u0018\u00101\u001a\u0004\u0018\u0001008\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b1\u00102R\u0016\u00103\u001a\u00020\u00178\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b3\u00104R\u0016\u00106\u001a\u0002058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b6\u00107¨\u0006;"}, d2 = {"Lcom/discord/stores/StoreSearchQuery;", "", "Lcom/discord/stores/StoreSearch$SearchTarget;", "searchTarget", "Lcom/discord/utilities/search/network/SearchQuery;", "query", "", "performInitialLoad", "(Lcom/discord/stores/StoreSearch$SearchTarget;Lcom/discord/utilities/search/network/SearchQuery;)V", "searchQuery", "Lcom/discord/models/domain/ModelSearchResponse;", "response", "", "isInitialLoad", "handleResponse", "(Lcom/discord/utilities/search/network/SearchQuery;Lcom/discord/models/domain/ModelSearchResponse;Z)V", "handleError", "()V", "", "Lcom/discord/primitives/MessageId;", "oldestMessageId", "makeQuery", "(Lcom/discord/utilities/search/network/SearchQuery;Lcom/discord/stores/StoreSearch$SearchTarget;Ljava/lang/Long;Z)V", "Lcom/discord/utilities/search/network/state/SearchState;", "searchState", "updateAndPublish", "(Lcom/discord/utilities/search/network/state/SearchState;)V", "unsubscribe", "Lcom/discord/stores/StoreSearch;", "searchStore", "", "queryString", "Lcom/discord/utilities/search/strings/SearchStringProvider;", "searchStringProvider", "includeNsfw", "parseAndQuery", "(Lcom/discord/stores/StoreSearch;Lcom/discord/stores/StoreSearch$SearchTarget;Ljava/lang/String;Lcom/discord/utilities/search/strings/SearchStringProvider;Z)V", "loadMore", "(Lcom/discord/stores/StoreSearch$SearchTarget;J)V", "clear", "Lrx/subjects/SerializedSubject;", "kotlin.jvm.PlatformType", "searchStateSubject", "Lrx/subjects/SerializedSubject;", "Lrx/Observable;", "getState", "()Lrx/Observable;", "state", "Lrx/Subscription;", "querySubscription", "Lrx/Subscription;", "currentSearchState", "Lcom/discord/utilities/search/network/state/SearchState;", "Lcom/discord/utilities/search/network/SearchFetcher;", "searchFetcher", "Lcom/discord/utilities/search/network/SearchFetcher;", HookHelper.constructorName, "(Lcom/discord/utilities/search/network/SearchFetcher;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreSearchQuery {
    public static final Companion Companion = new Companion(null);
    private static final SearchState SEARCH_STATE_NONE = new SearchState(QueryFetchState.NONE, null, null, null, null, false, 0, 126, null);
    private SearchState currentSearchState;
    private Subscription querySubscription;
    private final SearchFetcher searchFetcher;
    private final SerializedSubject<SearchState, SearchState> searchStateSubject;

    /* compiled from: StoreSearchQuery.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/stores/StoreSearchQuery$Companion;", "", "Lcom/discord/utilities/search/network/state/SearchState;", "SEARCH_STATE_NONE", "Lcom/discord/utilities/search/network/state/SearchState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public StoreSearchQuery(SearchFetcher searchFetcher) {
        m.checkNotNullParameter(searchFetcher, "searchFetcher");
        this.searchFetcher = searchFetcher;
        SearchState searchState = SEARCH_STATE_NONE;
        this.searchStateSubject = new SerializedSubject<>(BehaviorSubject.l0(searchState));
        this.currentSearchState = searchState;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleError() {
        updateAndPublish(new SearchState(QueryFetchState.FAILED, null, null, null, null, false, 0, 126, null));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void handleResponse(SearchQuery searchQuery, ModelSearchResponse modelSearchResponse, boolean z2) {
        SearchState searchState;
        int totalResults;
        List arrayList;
        Integer errorCode = modelSearchResponse.getErrorCode();
        if (errorCode == null) {
            if (z2) {
                totalResults = modelSearchResponse.getTotalResults();
                arrayList = new ArrayList();
            } else {
                totalResults = this.currentSearchState.getTotalResults();
                List<Message> hits = this.currentSearchState.getHits();
                if (hits == null || (arrayList = u.toMutableList((Collection) hits)) == null) {
                    arrayList = new ArrayList();
                }
            }
            int i = totalResults;
            List list = arrayList;
            List<com.discord.api.message.Message> hits2 = modelSearchResponse.getHits();
            ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(hits2, 10));
            for (com.discord.api.message.Message message : hits2) {
                arrayList2.add(new Message(message));
            }
            list.addAll(arrayList2);
            searchState = new SearchState(QueryFetchState.COMPLETED, searchQuery, modelSearchResponse.getThreads(), modelSearchResponse.getMembers(), list, i > list.size(), i);
        } else if (errorCode.intValue() == 111000) {
            searchState = new SearchState(QueryFetchState.INDEXING, searchQuery, null, null, null, false, 0, 124, null);
        } else {
            searchState = new SearchState(QueryFetchState.FAILED, searchQuery, null, null, null, false, 0, 124, null);
        }
        updateAndPublish(searchState);
    }

    private final void makeQuery(SearchQuery searchQuery, StoreSearch.SearchTarget searchTarget, Long l, boolean z2) {
        ObservableExtensionsKt.appSubscribe(this.searchFetcher.makeQuery(searchTarget, l, searchQuery), (r18 & 1) != 0 ? null : null, "makeQuery", (r18 & 4) != 0 ? null : new StoreSearchQuery$makeQuery$2(this), new StoreSearchQuery$makeQuery$1(this, searchQuery, z2), (r18 & 16) != 0 ? null : new StoreSearchQuery$makeQuery$3(this), (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
    }

    public static /* synthetic */ void makeQuery$default(StoreSearchQuery storeSearchQuery, SearchQuery searchQuery, StoreSearch.SearchTarget searchTarget, Long l, boolean z2, int i, Object obj) {
        if ((i & 4) != 0) {
            l = null;
        }
        if ((i & 8) != 0) {
            z2 = true;
        }
        storeSearchQuery.makeQuery(searchQuery, searchTarget, l, z2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void performInitialLoad(StoreSearch.SearchTarget searchTarget, SearchQuery searchQuery) {
        unsubscribe();
        makeQuery$default(this, searchQuery, searchTarget, null, false, 12, null);
    }

    private final synchronized void unsubscribe() {
        Subscription subscription = this.querySubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
    }

    private final synchronized void updateAndPublish(SearchState searchState) {
        this.currentSearchState = searchState;
        this.searchStateSubject.k.onNext(searchState);
    }

    public final void clear() {
        unsubscribe();
        updateAndPublish(SEARCH_STATE_NONE);
    }

    public final Observable<SearchState> getState() {
        Observable<SearchState> q = this.searchStateSubject.q();
        m.checkNotNullExpressionValue(q, "searchStateSubject\n        .distinctUntilChanged()");
        return q;
    }

    public final synchronized void loadMore(StoreSearch.SearchTarget searchTarget, long j) {
        m.checkNotNullParameter(searchTarget, "searchTarget");
        SearchQuery searchQuery = this.currentSearchState.getSearchQuery();
        if (searchQuery == null) {
            return;
        }
        if (this.currentSearchState.getQueryFetchState() == QueryFetchState.COMPLETED) {
            if (this.currentSearchState.getHasMore()) {
                unsubscribe();
                updateAndPublish(new SearchState(QueryFetchState.LOADING_MORE, searchQuery, this.currentSearchState.getThreads(), this.currentSearchState.getThreadMembers(), this.currentSearchState.getHits(), false, this.currentSearchState.getTotalResults()));
                makeQuery(searchQuery, searchTarget, Long.valueOf(j), false);
            }
        }
    }

    public final void parseAndQuery(final StoreSearch storeSearch, final StoreSearch.SearchTarget searchTarget, String str, final SearchStringProvider searchStringProvider, final boolean z2) {
        m.checkNotNullParameter(storeSearch, "searchStore");
        m.checkNotNullParameter(searchTarget, "searchTarget");
        m.checkNotNullParameter(str, "queryString");
        m.checkNotNullParameter(searchStringProvider, "searchStringProvider");
        unsubscribe();
        if (!t.isBlank(str)) {
            updateAndPublish(new SearchState(QueryFetchState.IN_PROGRESS, null, null, null, null, false, 0, 126, null));
            Observable Z = Observable.j0(new k(str).F(new b<String, List<QueryNode>>() { // from class: com.discord.stores.StoreSearchQuery$parseAndQuery$1
                public final List<QueryNode> call(String str2) {
                    QueryParser queryParser = new QueryParser(SearchStringProvider.this);
                    m.checkNotNullExpressionValue(str2, "it");
                    return Parser.parse$default(queryParser, str2, null, null, 4, null);
                }
            }), storeSearch.getStoreSearchData().get(), new Func2<List<QueryNode>, SearchData, SearchQuery>() { // from class: com.discord.stores.StoreSearchQuery$parseAndQuery$2
                public final SearchQuery call(List<QueryNode> list, SearchData searchData) {
                    QueryNode.Preprocessor preprocessor = QueryNode.Preprocessor;
                    m.checkNotNullExpressionValue(list, "queryNodes");
                    m.checkNotNullExpressionValue(searchData, "searchData");
                    preprocessor.preprocess(list, searchData);
                    StoreSearch.this.persistQuery$app_productionGoogleRelease(searchTarget, list);
                    return new SearchQuery.Builder().setIncludeNsfw(z2).buildFrom(list, searchData);
                }
            }).k(b.a.d.o.c(StoreSearchQuery$parseAndQuery$3.INSTANCE, null, 1L, TimeUnit.SECONDS)).Z(1);
            m.checkNotNullExpressionValue(Z, "Observable\n            .…   )\n            .take(1)");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.computationBuffered(Z), (r18 & 1) != 0 ? null : null, "parseAndQuery", (r18 & 4) != 0 ? null : null, new StoreSearchQuery$parseAndQuery$4(this, searchTarget), (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
            return;
        }
        updateAndPublish(new SearchState(QueryFetchState.NONE, null, null, null, null, false, 0, 126, null));
    }
}
