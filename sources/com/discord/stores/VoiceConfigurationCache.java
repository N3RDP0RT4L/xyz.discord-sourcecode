package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.SharedPreferences;
import com.discord.rtcconnection.mediaengine.MediaEngineConnection;
import com.discord.stores.StoreMediaSettings;
import com.discord.utilities.cache.SharedPreferenceExtensionsKt;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
/* compiled from: VoiceConfigurationCache.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0017\u001a\u00020\u0016¢\u0006\u0004\b\u0019\u0010\u001aJ\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0006\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\n\u001a\u00020\t2\u0006\u0010\b\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\r\u001a\u00020\f2\u0006\u0010\b\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\r\u0010\u000eJ\r\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b\u0010\u0010\u0011J\u0015\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0012\u001a\u00020\u000f¢\u0006\u0004\b\u0014\u0010\u0015R\u0016\u0010\u0017\u001a\u00020\u00168\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018¨\u0006\u001b"}, d2 = {"Lcom/discord/stores/VoiceConfigurationCache;", "", "Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;", "readNoiseProcessing", "()Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;", "Lcom/discord/stores/StoreMediaSettings$VadUseKrisp;", "readVadUseKrisp", "()Lcom/discord/stores/StoreMediaSettings$VadUseKrisp;", "value", "", "intFromNoiseProcessing", "(Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;)I", "", "booleanFromVadUseKrisp", "(Lcom/discord/stores/StoreMediaSettings$VadUseKrisp;)Z", "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "read", "()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "voiceConfiguration", "", "write", "(Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;)V", "Landroid/content/SharedPreferences;", "sharedPreferences", "Landroid/content/SharedPreferences;", HookHelper.constructorName, "(Landroid/content/SharedPreferences;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class VoiceConfigurationCache {
    private final SharedPreferences sharedPreferences;

    public VoiceConfigurationCache(SharedPreferences sharedPreferences) {
        m.checkNotNullParameter(sharedPreferences, "sharedPreferences");
        this.sharedPreferences = sharedPreferences;
    }

    private final boolean booleanFromVadUseKrisp(StoreMediaSettings.VadUseKrisp vadUseKrisp) {
        return vadUseKrisp != StoreMediaSettings.VadUseKrisp.Disabled;
    }

    private final int intFromNoiseProcessing(StoreMediaSettings.NoiseProcessing noiseProcessing) {
        if (noiseProcessing == StoreMediaSettings.NoiseProcessing.CancellationTemporarilyDisabled) {
            return StoreMediaSettings.NoiseProcessing.Cancellation.ordinal();
        }
        return noiseProcessing.ordinal();
    }

    private final StoreMediaSettings.NoiseProcessing readNoiseProcessing() {
        try {
            return StoreMediaSettings.NoiseProcessing.values()[this.sharedPreferences.getInt("CACHE_KEY_VOICE_SETTINGS_NOISE_PROCESSING", StoreMediaSettings.VoiceConfiguration.Companion.getDEFAULT_NOISE_PROCESSING())];
        } catch (Exception unused) {
            return StoreMediaSettings.NoiseProcessing.Cancellation;
        }
    }

    private final StoreMediaSettings.VadUseKrisp readVadUseKrisp() {
        SharedPreferences sharedPreferences = this.sharedPreferences;
        StoreMediaSettings.VadUseKrisp vadUseKrisp = StoreMediaSettings.VoiceConfiguration.Companion.getDEFAULT_VOICE_CONFIG().getVadUseKrisp();
        StoreMediaSettings.VadUseKrisp vadUseKrisp2 = StoreMediaSettings.VadUseKrisp.Enabled;
        return sharedPreferences.getBoolean("CACHE_KEY_VOICE_SETTINGS_VAD_USE_KRISP", vadUseKrisp == vadUseKrisp2) ? vadUseKrisp2 : StoreMediaSettings.VadUseKrisp.Disabled;
    }

    public final StoreMediaSettings.VoiceConfiguration read() {
        SharedPreferences sharedPreferences = this.sharedPreferences;
        StoreMediaSettings.VoiceConfiguration.Companion companion = StoreMediaSettings.VoiceConfiguration.Companion;
        boolean z2 = sharedPreferences.getBoolean("CACHE_KEY_VOICE_SETTINGS_IS_MUTED", companion.getDEFAULT_VOICE_CONFIG().isSelfMuted());
        boolean z3 = this.sharedPreferences.getBoolean("CACHE_KEY_VOICE_SETTINGS_IS_DEAFENED", companion.getDEFAULT_VOICE_CONFIG().isSelfDeafened());
        boolean z4 = this.sharedPreferences.getBoolean("CACHE_KEY_VOICE_SETTINGS_AUTOMATIC_VAD", companion.getDEFAULT_VOICE_CONFIG().getAutomaticVad());
        StoreMediaSettings.VadUseKrisp readVadUseKrisp = readVadUseKrisp();
        boolean z5 = this.sharedPreferences.getBoolean("CACHE_KEY_VOICE_SETTINGS_AUTOMATIC_GAIN_CONTROL", companion.getDEFAULT_VOICE_CONFIG().getAutomaticGainControl());
        boolean z6 = this.sharedPreferences.getBoolean("CACHE_KEY_VOICE_SETTINGS_ECHO_CANCELLATION", companion.getDEFAULT_VOICE_CONFIG().getEchoCancellation());
        StoreMediaSettings.NoiseProcessing readNoiseProcessing = readNoiseProcessing();
        float f = this.sharedPreferences.getFloat("CACHE_KEY_VOICE_SETTINGS_SENSITIVITY", companion.getDEFAULT_VOICE_CONFIG().getSensitivity());
        MediaEngineConnection.InputMode.a aVar = MediaEngineConnection.InputMode.Companion;
        SharedPreferences sharedPreferences2 = this.sharedPreferences;
        MediaEngineConnection.InputMode inputMode = MediaEngineConnection.InputMode.VOICE_ACTIVITY;
        int i = sharedPreferences2.getInt("CACHE_KEY_VOICE_SETTINGS_INPUT_MODE", inputMode.getNumeral());
        Objects.requireNonNull(aVar);
        return new StoreMediaSettings.VoiceConfiguration(z2, z3, z4, readVadUseKrisp, z5, z6, readNoiseProcessing, f, (i == 1 || i != 2) ? inputMode : MediaEngineConnection.InputMode.PUSH_TO_TALK, this.sharedPreferences.getFloat("CACHE_KEY_VOICE_SETTINGS_OUTPUT_VOLUME", companion.getDEFAULT_VOICE_CONFIG().getOutputVolume()), SharedPreferenceExtensionsKt.getStringEntrySetAsMap$default(this.sharedPreferences, "MUTED_USERS_V2", null, VoiceConfigurationCache$read$1.INSTANCE, 2, null), SharedPreferenceExtensionsKt.getStringEntrySetAsMap$default(this.sharedPreferences, "USER_OUTPUT_VOLUMES_V2", null, VoiceConfigurationCache$read$2.INSTANCE, 2, null), null, this.sharedPreferences.getBoolean("VIDEO_ENABLE_HARDWARE_SCALING", companion.getDEFAULT_VOICE_CONFIG().getEnableVideoHardwareScaling()), this.sharedPreferences.getBoolean("CACHE_KEY_HIDE_VOICE_PARTICIPANTS", companion.getDEFAULT_VOICE_CONFIG().getVoiceParticipantsHidden()), 4096, null);
    }

    public final void write(StoreMediaSettings.VoiceConfiguration voiceConfiguration) {
        m.checkNotNullParameter(voiceConfiguration, "voiceConfiguration");
        SharedPreferences.Editor edit = this.sharedPreferences.edit();
        m.checkNotNullExpressionValue(edit, "editor");
        edit.putBoolean("CACHE_KEY_VOICE_SETTINGS_IS_MUTED", voiceConfiguration.isSelfMuted());
        edit.putBoolean("CACHE_KEY_VOICE_SETTINGS_IS_DEAFENED", voiceConfiguration.isSelfDeafened());
        edit.putBoolean("CACHE_KEY_VOICE_SETTINGS_AUTOMATIC_VAD", voiceConfiguration.getAutomaticVad());
        edit.putBoolean("CACHE_KEY_VOICE_SETTINGS_VAD_USE_KRISP", booleanFromVadUseKrisp(voiceConfiguration.getVadUseKrisp()));
        edit.putBoolean("CACHE_KEY_VOICE_SETTINGS_AUTOMATIC_GAIN_CONTROL", voiceConfiguration.getAutomaticGainControl());
        edit.putInt("CACHE_KEY_VOICE_SETTINGS_NOISE_PROCESSING", intFromNoiseProcessing(voiceConfiguration.getNoiseProcessing()));
        edit.putBoolean("CACHE_KEY_VOICE_SETTINGS_ECHO_CANCELLATION", voiceConfiguration.getEchoCancellation());
        edit.putFloat("CACHE_KEY_VOICE_SETTINGS_SENSITIVITY", voiceConfiguration.getSensitivity());
        edit.putInt("CACHE_KEY_VOICE_SETTINGS_INPUT_MODE", voiceConfiguration.getInputMode().getNumeral());
        edit.putFloat("CACHE_KEY_VOICE_SETTINGS_OUTPUT_VOLUME", voiceConfiguration.getOutputVolume());
        SharedPreferenceExtensionsKt.putStringEntrySetAsMap$default(edit, "MUTED_USERS_V2", voiceConfiguration.getMutedUsers(), null, null, 12, null);
        SharedPreferenceExtensionsKt.putStringEntrySetAsMap$default(edit, "USER_OUTPUT_VOLUMES_V2", voiceConfiguration.getUserOutputVolumes(), null, null, 12, null);
        edit.putBoolean("VIDEO_ENABLE_HARDWARE_SCALING", voiceConfiguration.getEnableVideoHardwareScaling());
        edit.putBoolean("CACHE_KEY_HIDE_VOICE_PARTICIPANTS", voiceConfiguration.getVoiceParticipantsHidden());
        edit.apply();
    }
}
