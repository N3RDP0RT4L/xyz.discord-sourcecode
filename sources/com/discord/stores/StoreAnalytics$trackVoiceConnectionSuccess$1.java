package com.discord.stores;

import com.discord.api.channel.Channel;
import com.discord.rtcconnection.mediaengine.MediaEngine;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreAnalytics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAnalytics$trackVoiceConnectionSuccess$1 extends o implements Function0<Unit> {
    public final /* synthetic */ Map $properties;
    public final /* synthetic */ StoreAnalytics this$0;

    /* compiled from: StoreAnalytics.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/rtcconnection/mediaengine/MediaEngine$AudioInfo;", "audioInfo", "", "invoke", "(Lcom/discord/rtcconnection/mediaengine/MediaEngine$AudioInfo;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreAnalytics$trackVoiceConnectionSuccess$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<MediaEngine.AudioInfo, Unit> {
        public final /* synthetic */ Channel $channel;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Channel channel) {
            super(1);
            this.$channel = channel;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(MediaEngine.AudioInfo audioInfo) {
            invoke2(audioInfo);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(MediaEngine.AudioInfo audioInfo) {
            StoreStream storeStream;
            m.checkNotNullParameter(audioInfo, "audioInfo");
            AnalyticsTracker analyticsTracker = AnalyticsTracker.INSTANCE;
            StoreAnalytics$trackVoiceConnectionSuccess$1 storeAnalytics$trackVoiceConnectionSuccess$1 = StoreAnalytics$trackVoiceConnectionSuccess$1.this;
            Map<String, ? extends Object> map = storeAnalytics$trackVoiceConnectionSuccess$1.$properties;
            Channel channel = this.$channel;
            storeStream = storeAnalytics$trackVoiceConnectionSuccess$1.this$0.stores;
            analyticsTracker.voiceConnectionSuccess(map, audioInfo, channel, storeStream.getRtcRegion$app_productionGoogleRelease().getPreferredRegion());
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreAnalytics$trackVoiceConnectionSuccess$1(StoreAnalytics storeAnalytics, Map map) {
        super(0);
        this.this$0 = storeAnalytics;
        this.$properties = map;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreStream storeStream;
        StoreStream storeStream2;
        StoreStream storeStream3;
        storeStream = this.this$0.stores;
        long selectedVoiceChannelId = storeStream.getVoiceChannelSelected$app_productionGoogleRelease().getSelectedVoiceChannelId();
        storeStream2 = this.this$0.stores;
        Channel findChannelByIdInternal$app_productionGoogleRelease = storeStream2.getChannels$app_productionGoogleRelease().findChannelByIdInternal$app_productionGoogleRelease(selectedVoiceChannelId);
        storeStream3 = this.this$0.stores;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.computationLatest(storeStream3.getMediaEngine$app_productionGoogleRelease().getMediaEngine().e()), this.this$0.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1(findChannelByIdInternal$app_productionGoogleRelease));
    }
}
