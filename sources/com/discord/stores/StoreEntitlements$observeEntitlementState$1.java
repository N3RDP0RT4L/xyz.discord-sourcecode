package com.discord.stores;

import com.discord.stores.StoreEntitlements;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreEntitlements.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/stores/StoreEntitlements$State;", "invoke", "()Lcom/discord/stores/StoreEntitlements$State;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreEntitlements$observeEntitlementState$1 extends o implements Function0<StoreEntitlements.State> {
    public final /* synthetic */ StoreEntitlements this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreEntitlements$observeEntitlementState$1(StoreEntitlements storeEntitlements) {
        super(0);
        this.this$0 = storeEntitlements;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final StoreEntitlements.State invoke() {
        StoreEntitlements.State state;
        state = this.this$0.stateSnapshot;
        return state;
    }
}
