package com.discord.stores;

import com.discord.models.domain.ModelGuildMemberListUpdate;
import com.discord.utilities.lazy.memberlist.MemberListRow;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreChannelMembers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;", "it", "Lcom/discord/utilities/lazy/memberlist/MemberListRow;", "invoke", "(Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;)Lcom/discord/utilities/lazy/memberlist/MemberListRow;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreChannelMembers$handleGuildMemberListUpdate$2 extends o implements Function1<ModelGuildMemberListUpdate.Group, MemberListRow> {
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ StoreChannelMembers this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreChannelMembers$handleGuildMemberListUpdate$2(StoreChannelMembers storeChannelMembers, long j) {
        super(1);
        this.this$0 = storeChannelMembers;
        this.$guildId = j;
    }

    public final MemberListRow invoke(ModelGuildMemberListUpdate.Group group) {
        MemberListRow makeGroup;
        m.checkNotNullParameter(group, "it");
        makeGroup = this.this$0.makeGroup(this.$guildId, group);
        return makeGroup;
    }
}
