package com.discord.stores;

import com.discord.widgets.stage.StageRoles;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreStageChannels.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u0014\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/widgets/stage/StageRoles;", "invoke", "()Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStageChannels$observeStageRolesByChannel$1 extends o implements Function0<Map<Long, ? extends StageRoles>> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ StoreStageChannels this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreStageChannels$observeStageRolesByChannel$1(StoreStageChannels storeStageChannels, long j) {
        super(0);
        this.this$0 = storeStageChannels;
        this.$channelId = j;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Map<Long, ? extends StageRoles> invoke() {
        Map map;
        map = this.this$0.stageRolesByChannelSnapshot;
        return (Map) map.get(Long.valueOf(this.$channelId));
    }
}
