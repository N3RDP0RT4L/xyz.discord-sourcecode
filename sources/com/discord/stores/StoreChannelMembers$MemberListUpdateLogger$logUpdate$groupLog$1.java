package com.discord.stores;

import com.discord.models.domain.ModelGuildMemberListUpdate;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreChannelMembers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;", "it", "", "invoke", "(Lcom/discord/models/domain/ModelGuildMemberListUpdate$Group;)Ljava/lang/CharSequence;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreChannelMembers$MemberListUpdateLogger$logUpdate$groupLog$1 extends o implements Function1<ModelGuildMemberListUpdate.Group, CharSequence> {
    public static final StoreChannelMembers$MemberListUpdateLogger$logUpdate$groupLog$1 INSTANCE = new StoreChannelMembers$MemberListUpdateLogger$logUpdate$groupLog$1();

    public StoreChannelMembers$MemberListUpdateLogger$logUpdate$groupLog$1() {
        super(1);
    }

    public final CharSequence invoke(ModelGuildMemberListUpdate.Group group) {
        m.checkNotNullParameter(group, "it");
        return String.valueOf(group.getCount());
    }
}
