package com.discord.stores;

import com.discord.api.application.Application;
import d0.z.d.o;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreApplication.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u00030\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "", "Lcom/discord/primitives/ApplicationId;", "Lcom/discord/api/application/Application;", "invoke", "()Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreApplication$observeApplications$1 extends o implements Function0<Map<Long, ? extends Application>> {
    public final /* synthetic */ Collection $applicationIds;
    public final /* synthetic */ StoreApplication this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreApplication$observeApplications$1(StoreApplication storeApplication, Collection collection) {
        super(0);
        this.this$0 = storeApplication;
        this.$applicationIds = collection;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Map<Long, ? extends Application> invoke() {
        HashMap hashMap;
        hashMap = this.this$0.applicationsSnapshot;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry entry : hashMap.entrySet()) {
            if (this.$applicationIds.contains(Long.valueOf(((Number) entry.getKey()).longValue()))) {
                linkedHashMap.put(entry.getKey(), entry.getValue());
            }
        }
        return linkedHashMap;
    }
}
