package com.discord.stores;

import andhook.lib.HookHelper;
import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import androidx.annotation.VisibleForTesting;
import b.c.a.a0.d;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.voice.state.VoiceState;
import com.discord.rtcconnection.RtcConnection;
import com.discord.rtcconnection.audio.DiscordAudioManager;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.voice.PerceptualVolumeUtils;
import d0.t.h0;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreAudioManagerV2.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000ª\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0007\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 S2\u00020\u0001:\u0003STUB?\u0012\u0006\u0010I\u001a\u00020H\u0012\u0006\u0010C\u001a\u00020B\u0012\u0006\u0010O\u001a\u00020N\u0012\u0006\u00102\u001a\u000201\u0012\u0006\u0010(\u001a\u00020'\u0012\u0006\u0010@\u001a\u00020?\u0012\u0006\u0010F\u001a\u00020E¢\u0006\u0004\bQ\u0010RJ\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u001d\u0010\t\u001a\u00020\b2\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u0003¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\r\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\u000bH\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0011\u001a\u00020\b2\u0006\u0010\u0010\u001a\u00020\u000fH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u0015\u0010\u0015\u001a\u00020\b2\u0006\u0010\u0014\u001a\u00020\u0013¢\u0006\u0004\b\u0015\u0010\u0016J\u0013\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00020\u0017¢\u0006\u0004\b\u0018\u0010\u0019J\r\u0010\u001a\u001a\u00020\b¢\u0006\u0004\b\u001a\u0010\u001bJ\u0015\u0010\u001c\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\u001c\u0010\u000eJ\u0015\u0010\u001f\u001a\u00020\b2\u0006\u0010\u001e\u001a\u00020\u001d¢\u0006\u0004\b\u001f\u0010 J\u000f\u0010!\u001a\u00020\bH\u0007¢\u0006\u0004\b!\u0010\u001bJ\u0017\u0010$\u001a\u00020\b2\u0006\u0010#\u001a\u00020\"H\u0007¢\u0006\u0004\b$\u0010%J\u000f\u0010&\u001a\u00020\bH\u0016¢\u0006\u0004\b&\u0010\u001bR\u0016\u0010(\u001a\u00020'8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u0010)R\"\u0010#\u001a\u00020\u00028\u0000@\u0000X\u0080\u000e¢\u0006\u0012\n\u0004\b#\u0010*\u001a\u0004\b+\u0010\u0004\"\u0004\b,\u0010-R\u0016\u0010\u0014\u001a\u00020.8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u0014\u0010/R\u0016\u00100\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b0\u0010*R\u0016\u00102\u001a\u0002018\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b2\u00103R\u0016\u00105\u001a\u0002048\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b5\u00106R$\u00108\u001a\u00020\u00062\u0006\u00107\u001a\u00020\u00068\u0000@BX\u0080\u000e¢\u0006\f\n\u0004\b8\u00109\u001a\u0004\b:\u0010;R\u0016\u0010=\u001a\u00020<8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b=\u0010>R\u0016\u0010@\u001a\u00020?8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b@\u0010AR\u0016\u0010C\u001a\u00020B8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bC\u0010DR\u0016\u0010F\u001a\u00020E8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bF\u0010GR\u0016\u0010I\u001a\u00020H8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bI\u0010JR\u0016\u0010L\u001a\u00020K8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bL\u0010MR\u0016\u0010O\u001a\u00020N8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bO\u0010P¨\u0006V"}, d2 = {"Lcom/discord/stores/StoreAudioManagerV2;", "Lcom/discord/stores/StoreV2;", "Lcom/discord/stores/StoreAudioManagerV2$State;", "getAudioManagerState", "()Lcom/discord/stores/StoreAudioManagerV2$State;", "", "Lcom/discord/rtcconnection/audio/DiscordAudioManager$AudioDevice;", "devices", "", "updateAudioDevices", "(Ljava/util/List;)V", "Lcom/discord/rtcconnection/audio/DiscordAudioManager$DeviceTypes;", "device", "updateActiveAudioDevice", "(Lcom/discord/rtcconnection/audio/DiscordAudioManager$DeviceTypes;)V", "", "currentMediaVolumeIndex", "updateCurrentMediaVolume", "(I)V", "Landroid/app/Application;", "context", "init", "(Landroid/app/Application;)V", "Lrx/Observable;", "observeAudioManagerState", "()Lrx/Observable;", "toggleSpeakerOutput", "()V", "selectOutputDevice", "", "ratio", "updateMediaVolume", "(F)V", "handleVoiceStatesUpdated", "Lcom/discord/rtcconnection/RtcConnection$State;", "state", "handleRtcConnectionState", "(Lcom/discord/rtcconnection/RtcConnection$State;)V", "snapshotData", "Lcom/discord/stores/StoreStreamRtcConnection;", "streamRtcConnectionStore", "Lcom/discord/stores/StoreStreamRtcConnection;", "Lcom/discord/stores/StoreAudioManagerV2$State;", "getState$app_productionGoogleRelease", "setState$app_productionGoogleRelease", "(Lcom/discord/stores/StoreAudioManagerV2$State;)V", "Landroid/content/Context;", "Landroid/content/Context;", "stateSnapshot", "Lcom/discord/stores/StoreChannels;", "channelsStore", "Lcom/discord/stores/StoreChannels;", "Landroid/os/Handler;", "audioManagerHandler", "Landroid/os/Handler;", "<set-?>", "lastActiveAudioDevice", "Lcom/discord/rtcconnection/audio/DiscordAudioManager$AudioDevice;", "getLastActiveAudioDevice$app_productionGoogleRelease", "()Lcom/discord/rtcconnection/audio/DiscordAudioManager$AudioDevice;", "", "prevMyVideoOn", "Z", "Lcom/discord/stores/StoreAudioManagerV2$VideoUseDetector;", "videoUseDetector", "Lcom/discord/stores/StoreAudioManagerV2$VideoUseDetector;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/stores/StoreExperiments;", "experimentsStore", "Lcom/discord/stores/StoreExperiments;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Landroid/os/HandlerThread;", "audioManagerThread", "Landroid/os/HandlerThread;", "Lcom/discord/stores/StoreVoiceChannelSelected;", "voiceChannelSelectedStore", "Lcom/discord/stores/StoreVoiceChannelSelected;", HookHelper.constructorName, "(Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreStreamRtcConnection;Lcom/discord/stores/StoreAudioManagerV2$VideoUseDetector;Lcom/discord/stores/StoreExperiments;)V", "Companion", "State", "VideoUseDetector", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAudioManagerV2 extends StoreV2 {
    public static final Companion Companion = new Companion(null);
    private static final State DEFAULT_STATE = new State(DiscordAudioManager.f(), DiscordAudioManager.DeviceTypes.EARPIECE);
    private Handler audioManagerHandler;
    private final StoreChannels channelsStore;
    private Context context;
    private final Dispatcher dispatcher;
    private final StoreExperiments experimentsStore;
    private final ObservationDeck observationDeck;
    private boolean prevMyVideoOn;
    private State state;
    private State stateSnapshot;
    private final StoreStreamRtcConnection streamRtcConnectionStore;
    private final VideoUseDetector videoUseDetector;
    private final StoreVoiceChannelSelected voiceChannelSelectedStore;
    private DiscordAudioManager.AudioDevice lastActiveAudioDevice = DiscordAudioManager.f2756b;
    private final HandlerThread audioManagerThread = new HandlerThread("AudioManagerThread", -1);

    /* compiled from: StoreAudioManagerV2.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\bR\"\u0010\u0003\u001a\u00020\u00028\u0000@\u0001X\u0081\u0004¢\u0006\u0012\n\u0004\b\u0003\u0010\u0004\u0012\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006¨\u0006\n"}, d2 = {"Lcom/discord/stores/StoreAudioManagerV2$Companion;", "", "Lcom/discord/stores/StoreAudioManagerV2$State;", "DEFAULT_STATE", "Lcom/discord/stores/StoreAudioManagerV2$State;", "getDEFAULT_STATE$app_productionGoogleRelease", "()Lcom/discord/stores/StoreAudioManagerV2$State;", "getDEFAULT_STATE$app_productionGoogleRelease$annotations", "()V", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        @VisibleForTesting
        public static /* synthetic */ void getDEFAULT_STATE$app_productionGoogleRelease$annotations() {
        }

        public final State getDEFAULT_STATE$app_productionGoogleRelease() {
            return StoreAudioManagerV2.DEFAULT_STATE;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreAudioManagerV2.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B!\u0012\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\b\b\u0002\u0010\n\u001a\u00020\u0006¢\u0006\u0004\b\u001f\u0010 J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ*\u0010\u000b\u001a\u00020\u00002\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\b\b\u0002\u0010\n\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\"\u0010\n\u001a\u00020\u00068\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\n\u0010\u0017\u001a\u0004\b\u0018\u0010\b\"\u0004\b\u0019\u0010\u001aR(\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\t\u0010\u001b\u001a\u0004\b\u001c\u0010\u0005\"\u0004\b\u001d\u0010\u001e¨\u0006!"}, d2 = {"Lcom/discord/stores/StoreAudioManagerV2$State;", "", "", "Lcom/discord/rtcconnection/audio/DiscordAudioManager$AudioDevice;", "component1", "()Ljava/util/List;", "Lcom/discord/rtcconnection/audio/DiscordAudioManager$DeviceTypes;", "component2", "()Lcom/discord/rtcconnection/audio/DiscordAudioManager$DeviceTypes;", "audioDevices", "activeAudioDevice", "copy", "(Ljava/util/List;Lcom/discord/rtcconnection/audio/DiscordAudioManager$DeviceTypes;)Lcom/discord/stores/StoreAudioManagerV2$State;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/rtcconnection/audio/DiscordAudioManager$DeviceTypes;", "getActiveAudioDevice", "setActiveAudioDevice", "(Lcom/discord/rtcconnection/audio/DiscordAudioManager$DeviceTypes;)V", "Ljava/util/List;", "getAudioDevices", "setAudioDevices", "(Ljava/util/List;)V", HookHelper.constructorName, "(Ljava/util/List;Lcom/discord/rtcconnection/audio/DiscordAudioManager$DeviceTypes;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class State {
        private DiscordAudioManager.DeviceTypes activeAudioDevice;
        private List<DiscordAudioManager.AudioDevice> audioDevices;

        public State() {
            this(null, null, 3, null);
        }

        public State(List<DiscordAudioManager.AudioDevice> list, DiscordAudioManager.DeviceTypes deviceTypes) {
            m.checkNotNullParameter(list, "audioDevices");
            m.checkNotNullParameter(deviceTypes, "activeAudioDevice");
            this.audioDevices = list;
            this.activeAudioDevice = deviceTypes;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ State copy$default(State state, List list, DiscordAudioManager.DeviceTypes deviceTypes, int i, Object obj) {
            if ((i & 1) != 0) {
                list = state.audioDevices;
            }
            if ((i & 2) != 0) {
                deviceTypes = state.activeAudioDevice;
            }
            return state.copy(list, deviceTypes);
        }

        public final List<DiscordAudioManager.AudioDevice> component1() {
            return this.audioDevices;
        }

        public final DiscordAudioManager.DeviceTypes component2() {
            return this.activeAudioDevice;
        }

        public final State copy(List<DiscordAudioManager.AudioDevice> list, DiscordAudioManager.DeviceTypes deviceTypes) {
            m.checkNotNullParameter(list, "audioDevices");
            m.checkNotNullParameter(deviceTypes, "activeAudioDevice");
            return new State(list, deviceTypes);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof State)) {
                return false;
            }
            State state = (State) obj;
            return m.areEqual(this.audioDevices, state.audioDevices) && m.areEqual(this.activeAudioDevice, state.activeAudioDevice);
        }

        public final DiscordAudioManager.DeviceTypes getActiveAudioDevice() {
            return this.activeAudioDevice;
        }

        public final List<DiscordAudioManager.AudioDevice> getAudioDevices() {
            return this.audioDevices;
        }

        public int hashCode() {
            List<DiscordAudioManager.AudioDevice> list = this.audioDevices;
            int i = 0;
            int hashCode = (list != null ? list.hashCode() : 0) * 31;
            DiscordAudioManager.DeviceTypes deviceTypes = this.activeAudioDevice;
            if (deviceTypes != null) {
                i = deviceTypes.hashCode();
            }
            return hashCode + i;
        }

        public final void setActiveAudioDevice(DiscordAudioManager.DeviceTypes deviceTypes) {
            m.checkNotNullParameter(deviceTypes, "<set-?>");
            this.activeAudioDevice = deviceTypes;
        }

        public final void setAudioDevices(List<DiscordAudioManager.AudioDevice> list) {
            m.checkNotNullParameter(list, "<set-?>");
            this.audioDevices = list;
        }

        public String toString() {
            StringBuilder R = a.R("State(audioDevices=");
            R.append(this.audioDevices);
            R.append(", activeAudioDevice=");
            R.append(this.activeAudioDevice);
            R.append(")");
            return R.toString();
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public /* synthetic */ State(java.util.List r1, com.discord.rtcconnection.audio.DiscordAudioManager.DeviceTypes r2, int r3, kotlin.jvm.internal.DefaultConstructorMarker r4) {
            /*
                r0 = this;
                r4 = r3 & 1
                if (r4 == 0) goto La
                com.discord.rtcconnection.audio.DiscordAudioManager r1 = com.discord.rtcconnection.audio.DiscordAudioManager.d
                java.util.List r1 = com.discord.rtcconnection.audio.DiscordAudioManager.f()
            La:
                r3 = r3 & 2
                if (r3 == 0) goto L10
                com.discord.rtcconnection.audio.DiscordAudioManager$DeviceTypes r2 = com.discord.rtcconnection.audio.DiscordAudioManager.DeviceTypes.INVALID
            L10:
                r0.<init>(r1, r2)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreAudioManagerV2.State.<init>(java.util.List, com.discord.rtcconnection.audio.DiscordAudioManager$DeviceTypes, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
        }
    }

    /* compiled from: StoreAudioManagerV2.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u000e\u001a\u00020\r\u0012\u0006\u0010\u0011\u001a\u00020\u0010\u0012\u0006\u0010\u0014\u001a\u00020\u0013¢\u0006\u0004\b\u0016\u0010\u0017J+\u0010\b\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u00070\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0003¢\u0006\u0004\b\b\u0010\tJ\u001b\u0010\u000b\u001a\u00020\n2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0007¢\u0006\u0004\b\u000b\u0010\fR\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0011\u001a\u00020\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0014\u001a\u00020\u00138\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015¨\u0006\u0018"}, d2 = {"Lcom/discord/stores/StoreAudioManagerV2$VideoUseDetector;", "", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/api/voice/state/VoiceState;", "getVoiceStatesForChannel", "(J)Ljava/util/Map;", "", "isMyVideoOn", "(J)Z", "Lcom/discord/stores/StoreChannels;", "channelsStore", "Lcom/discord/stores/StoreChannels;", "Lcom/discord/stores/StoreVoiceStates;", "voiceStatesStore", "Lcom/discord/stores/StoreVoiceStates;", "Lcom/discord/stores/StoreUser;", "usersStore", "Lcom/discord/stores/StoreUser;", HookHelper.constructorName, "(Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreVoiceStates;Lcom/discord/stores/StoreUser;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class VideoUseDetector {
        private final StoreChannels channelsStore;
        private final StoreUser usersStore;
        private final StoreVoiceStates voiceStatesStore;

        public VideoUseDetector(StoreChannels storeChannels, StoreVoiceStates storeVoiceStates, StoreUser storeUser) {
            m.checkNotNullParameter(storeChannels, "channelsStore");
            m.checkNotNullParameter(storeVoiceStates, "voiceStatesStore");
            m.checkNotNullParameter(storeUser, "usersStore");
            this.channelsStore = storeChannels;
            this.voiceStatesStore = storeVoiceStates;
            this.usersStore = storeUser;
        }

        @StoreThread
        private final Map<Long, VoiceState> getVoiceStatesForChannel(long j) {
            Channel findChannelByIdInternal$app_productionGoogleRelease = this.channelsStore.findChannelByIdInternal$app_productionGoogleRelease(j);
            Map<Long, VoiceState> map = this.voiceStatesStore.getInternal$app_productionGoogleRelease().get(findChannelByIdInternal$app_productionGoogleRelease != null ? Long.valueOf(findChannelByIdInternal$app_productionGoogleRelease.f()) : null);
            if (map == null) {
                return h0.emptyMap();
            }
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (Map.Entry<Long, VoiceState> entry : map.entrySet()) {
                Long a = entry.getValue().a();
                if (a != null && a.longValue() == j) {
                    linkedHashMap.put(entry.getKey(), entry.getValue());
                }
            }
            return linkedHashMap;
        }

        @StoreThread
        public final boolean isMyVideoOn(long j) {
            VoiceState voiceState = getVoiceStatesForChannel(j).get(Long.valueOf(this.usersStore.getMeInternal$app_productionGoogleRelease().getId()));
            if (voiceState == null) {
                return false;
            }
            m.checkNotNullParameter(voiceState, "$this$hasVideo");
            return voiceState.i() || voiceState.j();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            DiscordAudioManager.DeviceTypes.values();
            int[] iArr = new int[6];
            $EnumSwitchMapping$0 = iArr;
            iArr[DiscordAudioManager.DeviceTypes.SPEAKERPHONE.ordinal()] = 1;
        }
    }

    static {
        DiscordAudioManager discordAudioManager = DiscordAudioManager.d;
    }

    public StoreAudioManagerV2(ObservationDeck observationDeck, Dispatcher dispatcher, StoreVoiceChannelSelected storeVoiceChannelSelected, StoreChannels storeChannels, StoreStreamRtcConnection storeStreamRtcConnection, VideoUseDetector videoUseDetector, StoreExperiments storeExperiments) {
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(storeVoiceChannelSelected, "voiceChannelSelectedStore");
        m.checkNotNullParameter(storeChannels, "channelsStore");
        m.checkNotNullParameter(storeStreamRtcConnection, "streamRtcConnectionStore");
        m.checkNotNullParameter(videoUseDetector, "videoUseDetector");
        m.checkNotNullParameter(storeExperiments, "experimentsStore");
        this.observationDeck = observationDeck;
        this.dispatcher = dispatcher;
        this.voiceChannelSelectedStore = storeVoiceChannelSelected;
        this.channelsStore = storeChannels;
        this.streamRtcConnectionStore = storeStreamRtcConnection;
        this.videoUseDetector = videoUseDetector;
        this.experimentsStore = storeExperiments;
        State state = new State(null, null, 3, null);
        this.state = state;
        this.stateSnapshot = state;
        DiscordAudioManager discordAudioManager = DiscordAudioManager.d;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final State getAudioManagerState() {
        return this.stateSnapshot;
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void updateActiveAudioDevice(DiscordAudioManager.DeviceTypes deviceTypes) {
        State copy$default = State.copy$default(this.state, null, deviceTypes, 1, null);
        this.state = copy$default;
        if (!(deviceTypes == DiscordAudioManager.DeviceTypes.INVALID || deviceTypes == DiscordAudioManager.DeviceTypes.DEFAULT)) {
            this.lastActiveAudioDevice = copy$default.getAudioDevices().get(deviceTypes.getValue());
        }
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void updateAudioDevices(List<DiscordAudioManager.AudioDevice> list) {
        this.state = State.copy$default(this.state, list, null, 2, null);
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateCurrentMediaVolume(int i) {
        DiscordAudioManager discordAudioManager = DiscordAudioManager.d;
        this.streamRtcConnectionStore.updateStreamVolume(PerceptualVolumeUtils.INSTANCE.perceptualToAmplitude((i / DiscordAudioManager.d().f2758x) * 300.0f, 300.0f));
    }

    public final DiscordAudioManager.AudioDevice getLastActiveAudioDevice$app_productionGoogleRelease() {
        return this.lastActiveAudioDevice;
    }

    public final State getState$app_productionGoogleRelease() {
        return this.state;
    }

    @StoreThread
    public final void handleRtcConnectionState(RtcConnection.State state) {
        m.checkNotNullParameter(state, "state");
        if (m.areEqual(state, RtcConnection.State.f.a)) {
            Handler handler = this.audioManagerHandler;
            if (handler == null) {
                m.throwUninitializedPropertyAccessException("audioManagerHandler");
            }
            handler.post(StoreAudioManagerV2$handleRtcConnectionState$1.INSTANCE);
        } else if ((state instanceof RtcConnection.State.d) && !((RtcConnection.State.d) state).a) {
            Handler handler2 = this.audioManagerHandler;
            if (handler2 == null) {
                m.throwUninitializedPropertyAccessException("audioManagerHandler");
            }
            handler2.post(StoreAudioManagerV2$handleRtcConnectionState$2.INSTANCE);
        }
    }

    @StoreThread
    public final void handleVoiceStatesUpdated() {
        DiscordAudioManager.AudioDevice audioDevice;
        DiscordAudioManager.AudioDevice audioDevice2;
        boolean isMyVideoOn = this.videoUseDetector.isMyVideoOn(this.voiceChannelSelectedStore.getSelectedVoiceChannelId());
        if (isMyVideoOn != this.prevMyVideoOn) {
            if (isMyVideoOn) {
                DiscordAudioManager discordAudioManager = DiscordAudioManager.d;
                DiscordAudioManager d = DiscordAudioManager.d();
                synchronized (d) {
                    if (d.r.get(DiscordAudioManager.DeviceTypes.EARPIECE.getValue()).f2761b) {
                        List<DiscordAudioManager.AudioDevice> list = d.r;
                        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
                        for (DiscordAudioManager.AudioDevice audioDevice3 : list) {
                            if (audioDevice3.a.ordinal() != 4) {
                                audioDevice2 = DiscordAudioManager.AudioDevice.a(audioDevice3, null, false, null, null, 15);
                            } else {
                                audioDevice2 = DiscordAudioManager.AudioDevice.a(audioDevice3, null, false, null, null, 13);
                            }
                            arrayList.add(audioDevice2);
                        }
                        d.r = arrayList;
                        d.f2757s.k.onNext(arrayList);
                        d.l();
                    }
                }
            } else {
                DiscordAudioManager discordAudioManager2 = DiscordAudioManager.d;
                DiscordAudioManager d2 = DiscordAudioManager.d();
                synchronized (d2) {
                    if (d2.r.get(DiscordAudioManager.DeviceTypes.EARPIECE.getValue()).f2761b != d2.h) {
                        List<DiscordAudioManager.AudioDevice> list2 = d2.r;
                        ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(list2, 10));
                        for (DiscordAudioManager.AudioDevice audioDevice4 : list2) {
                            if (audioDevice4.a.ordinal() != 4) {
                                audioDevice = DiscordAudioManager.AudioDevice.a(audioDevice4, null, false, null, null, 15);
                            } else {
                                audioDevice = DiscordAudioManager.AudioDevice.a(audioDevice4, null, d2.h, null, null, 13);
                            }
                            arrayList2.add(audioDevice);
                        }
                        d2.r = arrayList2;
                        d2.f2757s.k.onNext(arrayList2);
                        d2.l();
                    }
                }
            }
            this.prevMyVideoOn = isMyVideoOn;
        }
    }

    public final void init(Application application) {
        m.checkNotNullParameter(application, "context");
        this.context = application;
        if (!this.audioManagerThread.isAlive()) {
            this.audioManagerThread.start();
        }
        Handler handler = new Handler(this.audioManagerThread.getLooper());
        this.audioManagerHandler = handler;
        if (handler == null) {
            m.throwUninitializedPropertyAccessException("audioManagerHandler");
        }
        handler.post(new StoreAudioManagerV2$init$1(this));
    }

    public final Observable<State> observeAudioManagerState() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreAudioManagerV2$observeAudioManagerState$1(this), 14, null);
    }

    public final void selectOutputDevice(final DiscordAudioManager.DeviceTypes deviceTypes) {
        m.checkNotNullParameter(deviceTypes, "device");
        Handler handler = this.audioManagerHandler;
        if (handler == null) {
            m.throwUninitializedPropertyAccessException("audioManagerHandler");
        }
        handler.post(new Runnable() { // from class: com.discord.stores.StoreAudioManagerV2$selectOutputDevice$1
            @Override // java.lang.Runnable
            public final void run() {
                ArrayList arrayList;
                boolean z2;
                boolean z3;
                DiscordAudioManager discordAudioManager = DiscordAudioManager.d;
                DiscordAudioManager d = DiscordAudioManager.d();
                DiscordAudioManager.DeviceTypes deviceTypes2 = DiscordAudioManager.DeviceTypes.this;
                Objects.requireNonNull(d);
                m.checkNotNullParameter(deviceTypes2, "deviceType");
                d.c();
                if (!d.e().f258b) {
                    d.f1("DiscordAudioManager", "Can't setDevice, requires MODIFY_AUDIO_SETTINGS.");
                    return;
                }
                d.b1("DiscordAudioManager", "setDevice(deviceType=" + deviceTypes2 + ')');
                synchronized (d.i) {
                    List<DiscordAudioManager.AudioDevice> list = d.r;
                    arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
                    for (DiscordAudioManager.AudioDevice audioDevice : list) {
                        arrayList.add(DiscordAudioManager.AudioDevice.a(audioDevice, null, false, null, null, 15));
                    }
                }
                DiscordAudioManager.DeviceTypes deviceTypes3 = DiscordAudioManager.DeviceTypes.DEFAULT;
                if (deviceTypes2 == deviceTypes3) {
                    synchronized (d.i) {
                        d.f2760z = deviceTypes3;
                    }
                    d.a(arrayList);
                    return;
                }
                int ordinal = deviceTypes2.ordinal();
                if (ordinal == 0 || ordinal == 1) {
                    z2 = false;
                } else if (ordinal == 2 || ordinal == 3 || ordinal == 4 || ordinal == 5) {
                    z2 = true;
                } else {
                    throw new NoWhenBranchMatchedException();
                }
                if (z2) {
                    synchronized (d.i) {
                        z3 = true ^ d.r.get(deviceTypes2.getValue()).f2761b;
                    }
                    if (!z3) {
                        synchronized (d.i) {
                            d.f2760z = deviceTypes2;
                        }
                        d.b(deviceTypes2);
                    }
                }
            }
        });
    }

    public final void setState$app_productionGoogleRelease(State state) {
        m.checkNotNullParameter(state, "<set-?>");
        this.state = state;
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        super.snapshotData();
        List<DiscordAudioManager.AudioDevice> audioDevices = this.state.getAudioDevices();
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(audioDevices, 10));
        for (DiscordAudioManager.AudioDevice audioDevice : audioDevices) {
            arrayList.add(DiscordAudioManager.AudioDevice.a(audioDevice, null, false, null, null, 15));
        }
        this.stateSnapshot = this.state.copy(arrayList, this.state.getActiveAudioDevice());
    }

    public final void toggleSpeakerOutput() {
        this.dispatcher.schedule(new StoreAudioManagerV2$toggleSpeakerOutput$1(this));
    }

    public final void updateMediaVolume(final float f) {
        Handler handler = this.audioManagerHandler;
        if (handler == null) {
            m.throwUninitializedPropertyAccessException("audioManagerHandler");
        }
        handler.post(new Runnable() { // from class: com.discord.stores.StoreAudioManagerV2$updateMediaVolume$1
            @Override // java.lang.Runnable
            public final void run() {
                DiscordAudioManager discordAudioManager = DiscordAudioManager.d;
                DiscordAudioManager d = DiscordAudioManager.d();
                float f2 = f;
                d.c();
                if (f2 < 0.0f || f2 > 1.0f) {
                    d.f1("DiscordAudioManager", "Unexpected media volume ratio: " + f2);
                    return;
                }
                try {
                    d.e.setStreamVolume(3, d0.a0.a.roundToInt(f2 * d.f2758x), 0);
                } catch (SecurityException e) {
                    d.f1("DiscordAudioManager", "Failed to set stream volume: " + e);
                }
            }
        });
    }
}
