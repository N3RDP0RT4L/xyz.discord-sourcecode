package com.discord.stores;

import andhook.lib.HookHelper;
import com.discord.api.channel.Channel;
import com.discord.api.guild.Guild;
import com.discord.api.stageinstance.StageInstance;
import com.discord.models.domain.ModelPayload;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import d0.t.h0;
import d0.t.n0;
import d0.z.d.m;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreStageInstances.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010$\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010%\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0011\u0012\b\b\u0002\u00106\u001a\u000205¢\u0006\u0004\b9\u0010:J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J+\u0010\f\u001a\u00020\u00042\u000e\u0010\t\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\b2\n\u0010\u000b\u001a\u00060\u0007j\u0002`\nH\u0002¢\u0006\u0004\b\f\u0010\rJ!\u0010\u000f\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u000e2\n\u0010\u000b\u001a\u00060\u0007j\u0002`\n¢\u0006\u0004\b\u000f\u0010\u0010J\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u00022\n\u0010\u000b\u001a\u00060\u0007j\u0002`\n¢\u0006\u0004\b\u0011\u0010\u0012J\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u00022\n\u0010\u000b\u001a\u00060\u0007j\u0002`\n¢\u0006\u0004\b\u0013\u0010\u0012J\u001d\u0010\u0015\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\n\u0012\u0004\u0012\u00020\u00020\u0014¢\u0006\u0004\b\u0015\u0010\u0016J/\u0010\u0017\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0007j\u0002`\n\u0012\u0004\u0012\u00020\u00020\u00140\u000e2\n\u0010\t\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\b\u0017\u0010\u0010J#\u0010\u0018\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0007j\u0002`\n\u0012\u0004\u0012\u00020\u00020\u00140\u000e¢\u0006\u0004\b\u0018\u0010\u0019J-\u0010\u001a\u001a\"\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0007j\u0002`\n\u0012\u0004\u0012\u00020\u00020\u00140\u0014¢\u0006\u0004\b\u001a\u0010\u0016J)\u0010\u001b\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\n\u0012\u0004\u0012\u00020\u00020\u00142\n\u0010\t\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\b\u001b\u0010\u001cJ+\u0010\u001d\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\n\u0012\u0004\u0012\u00020\u00020\u00142\n\u0010\t\u001a\u00060\u0007j\u0002`\bH\u0007¢\u0006\u0004\b\u001d\u0010\u001cJ\u0017\u0010 \u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\u001eH\u0007¢\u0006\u0004\b \u0010!J\u0017\u0010$\u001a\u00020\u00042\u0006\u0010#\u001a\u00020\"H\u0007¢\u0006\u0004\b$\u0010%J\u0017\u0010&\u001a\u00020\u00042\u0006\u0010#\u001a\u00020\"H\u0007¢\u0006\u0004\b&\u0010%J\u0017\u0010)\u001a\u00020\u00042\u0006\u0010(\u001a\u00020'H\u0007¢\u0006\u0004\b)\u0010*J\u0017\u0010+\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b+\u0010\u0006J\u0017\u0010,\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b,\u0010\u0006J\u0017\u0010-\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b-\u0010\u0006J\u000f\u0010.\u001a\u00020\u0004H\u0017¢\u0006\u0004\b.\u0010/R&\u00100\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\n\u0012\u0004\u0012\u00020\u00020\u00148\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b0\u00101R&\u00103\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\n\u0012\u0004\u0012\u00020\u0002028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b3\u00101R6\u00104\u001a\"\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0007j\u0002`\n\u0012\u0004\u0012\u00020\u000202028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b4\u00101R\u0016\u00106\u001a\u0002058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b6\u00107R6\u00108\u001a\"\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0007j\u0002`\n\u0012\u0004\u0012\u00020\u00020\u00140\u00148\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b8\u00101¨\u0006;"}, d2 = {"Lcom/discord/stores/StoreStageInstances;", "Lcom/discord/stores/StoreV2;", "Lcom/discord/api/stageinstance/StageInstance;", "stageInstance", "", "handleStageInstanceCreateOrUpdate", "(Lcom/discord/api/stageinstance/StageInstance;)V", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/ChannelId;", "channelId", "handleDelete", "(Ljava/lang/Long;J)V", "Lrx/Observable;", "observeStageInstanceForChannel", "(J)Lrx/Observable;", "getStageInstanceForChannelInternal", "(J)Lcom/discord/api/stageinstance/StageInstance;", "getStageInstanceForChannel", "", "getStageInstances", "()Ljava/util/Map;", "observeStageInstancesForGuild", "observeStageInstances", "()Lrx/Observable;", "getStageInstancesByGuild", "getStageInstancesForGuild", "(J)Ljava/util/Map;", "getStageInstancesForGuildInternal", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "Lcom/discord/api/guild/Guild;", "guild", "handleGuildAdd", "(Lcom/discord/api/guild/Guild;)V", "handleGuildRemove", "Lcom/discord/api/channel/Channel;", "channel", "handleChannelDelete", "(Lcom/discord/api/channel/Channel;)V", "handleStageInstanceCreate", "handleStageInstanceUpdate", "handleStageInstanceDelete", "snapshotData", "()V", "stageInstancesByChannelSnapshot", "Ljava/util/Map;", "", "stageInstancesByChannel", "stageInstancesByGuild", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "stageInstancesByGuildSnapshot", HookHelper.constructorName, "(Lcom/discord/stores/updates/ObservationDeck;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStageInstances extends StoreV2 {
    private final ObservationDeck observationDeck;
    private final Map<Long, StageInstance> stageInstancesByChannel;
    private Map<Long, StageInstance> stageInstancesByChannelSnapshot;
    private final Map<Long, Map<Long, StageInstance>> stageInstancesByGuild;
    private Map<Long, ? extends Map<Long, StageInstance>> stageInstancesByGuildSnapshot;

    public StoreStageInstances() {
        this(null, 1, null);
    }

    public /* synthetic */ StoreStageInstances(ObservationDeck observationDeck, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? ObservationDeckProvider.get() : observationDeck);
    }

    private final void handleDelete(Long l, long j) {
        this.stageInstancesByChannel.remove(Long.valueOf(j));
        Map<Long, StageInstance> map = this.stageInstancesByGuild.get(l);
        if (map != null) {
            map.remove(Long.valueOf(j));
        }
        markChanged();
    }

    private final void handleStageInstanceCreateOrUpdate(StageInstance stageInstance) {
        this.stageInstancesByChannel.put(Long.valueOf(stageInstance.a()), stageInstance);
        Map<Long, Map<Long, StageInstance>> map = this.stageInstancesByGuild;
        Long valueOf = Long.valueOf(stageInstance.b());
        Map<Long, StageInstance> map2 = map.get(valueOf);
        if (map2 == null) {
            map2 = new LinkedHashMap<>();
            map.put(valueOf, map2);
        }
        map2.put(Long.valueOf(stageInstance.a()), stageInstance);
        markChanged();
    }

    public final StageInstance getStageInstanceForChannel(long j) {
        return this.stageInstancesByChannelSnapshot.get(Long.valueOf(j));
    }

    public final StageInstance getStageInstanceForChannelInternal(long j) {
        return this.stageInstancesByChannel.get(Long.valueOf(j));
    }

    public final Map<Long, StageInstance> getStageInstances() {
        return this.stageInstancesByChannelSnapshot;
    }

    public final Map<Long, Map<Long, StageInstance>> getStageInstancesByGuild() {
        return this.stageInstancesByGuildSnapshot;
    }

    public final Map<Long, StageInstance> getStageInstancesForGuild(long j) {
        Map<Long, StageInstance> map = this.stageInstancesByGuildSnapshot.get(Long.valueOf(j));
        return map != null ? map : h0.emptyMap();
    }

    @StoreThread
    public final Map<Long, StageInstance> getStageInstancesForGuildInternal(long j) {
        Map<Long, StageInstance> map = this.stageInstancesByGuild.get(Long.valueOf(j));
        return map != null ? map : h0.emptyMap();
    }

    @StoreThread
    public final void handleChannelDelete(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        handleDelete(Long.valueOf(channel.f()), channel.h());
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        this.stageInstancesByGuild.clear();
        this.stageInstancesByChannel.clear();
        List<Guild> guilds = modelPayload.getGuilds();
        m.checkNotNullExpressionValue(guilds, "payload.guilds");
        for (Guild guild : guilds) {
            m.checkNotNullExpressionValue(guild, "it");
            handleGuildAdd(guild);
        }
    }

    @StoreThread
    public final void handleGuildAdd(Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        List<StageInstance> J = guild.J();
        if (J != null) {
            for (StageInstance stageInstance : J) {
                handleStageInstanceCreateOrUpdate(stageInstance);
            }
        }
    }

    @StoreThread
    public final void handleGuildRemove(Guild guild) {
        Set<Long> set;
        m.checkNotNullParameter(guild, "guild");
        Map<Long, StageInstance> remove = this.stageInstancesByGuild.remove(Long.valueOf(guild.r()));
        if (remove == null || (set = remove.keySet()) == null) {
            set = n0.emptySet();
        }
        if (!set.isEmpty()) {
            this.stageInstancesByChannel.keySet().removeAll(set);
            markChanged();
        }
    }

    @StoreThread
    public final void handleStageInstanceCreate(StageInstance stageInstance) {
        m.checkNotNullParameter(stageInstance, "stageInstance");
        handleStageInstanceCreateOrUpdate(stageInstance);
    }

    @StoreThread
    public final void handleStageInstanceDelete(StageInstance stageInstance) {
        m.checkNotNullParameter(stageInstance, "stageInstance");
        handleDelete(Long.valueOf(stageInstance.b()), stageInstance.a());
    }

    @StoreThread
    public final void handleStageInstanceUpdate(StageInstance stageInstance) {
        m.checkNotNullParameter(stageInstance, "stageInstance");
        handleStageInstanceCreateOrUpdate(stageInstance);
    }

    public final Observable<StageInstance> observeStageInstanceForChannel(long j) {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreStageInstances$observeStageInstanceForChannel$1(this, j), 14, null);
    }

    public final Observable<Map<Long, StageInstance>> observeStageInstances() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreStageInstances$observeStageInstances$1(this), 14, null);
    }

    public final Observable<Map<Long, StageInstance>> observeStageInstancesForGuild(long j) {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreStageInstances$observeStageInstancesForGuild$1(this, j), 14, null);
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        this.stageInstancesByChannelSnapshot = new HashMap(this.stageInstancesByChannel);
        HashMap hashMap = new HashMap();
        for (Map.Entry<Long, Map<Long, StageInstance>> entry : this.stageInstancesByGuild.entrySet()) {
            hashMap.put(entry.getKey(), new HashMap(entry.getValue()));
        }
        this.stageInstancesByGuildSnapshot = hashMap;
    }

    public StoreStageInstances(ObservationDeck observationDeck) {
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.observationDeck = observationDeck;
        this.stageInstancesByChannel = new LinkedHashMap();
        this.stageInstancesByChannelSnapshot = new HashMap();
        this.stageInstancesByGuild = new LinkedHashMap();
        this.stageInstancesByGuildSnapshot = new HashMap();
    }
}
