package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.user.User;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelGift;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreGifting;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.t.h0;
import d0.t.o;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.BehaviorSubject;
/* compiled from: StoreGifting.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 ?2\u00020\u0001:\u0003?@AB\u000f\u0012\u0006\u00109\u001a\u000208¢\u0006\u0004\b=\u0010>J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\b\u0010\u0006J\u001f\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\tH\u0003¢\u0006\u0004\b\u000b\u0010\fJ+\u0010\u0012\u001a\u00020\u00042\n\u0010\u000f\u001a\u00060\rj\u0002`\u000e2\u000e\u0010\u0011\u001a\n\u0018\u00010\rj\u0004\u0018\u0001`\u0010H\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u001b\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\t0\u00142\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J%\u0010\u001b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u001a0\u00190\u00142\n\u0010\u0018\u001a\u00060\rj\u0002`\u0017¢\u0006\u0004\b\u001b\u0010\u001cJ\u0015\u0010\u001e\u001a\u00020\u00042\u0006\u0010\u001d\u001a\u00020\u001a¢\u0006\u0004\b\u001e\u0010\u001fJ\u0015\u0010 \u001a\u00020\u00042\u0006\u0010\u001d\u001a\u00020\u001a¢\u0006\u0004\b \u0010\u001fJ[\u0010&\u001a\u00020\u00042\n\u0010\u000f\u001a\u00060\rj\u0002`\u000e2\u0010\b\u0002\u0010!\u001a\n\u0018\u00010\rj\u0004\u0018\u0001`\u00102\u0016\b\u0002\u0010#\u001a\u0010\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020\u0004\u0018\u00010\"2\u0016\b\u0002\u0010%\u001a\u0010\u0012\u0004\u0012\u00020$\u0012\u0004\u0012\u00020\u0004\u0018\u00010\"¢\u0006\u0004\b&\u0010'J\u000f\u0010(\u001a\u00020\u0004H\u0007¢\u0006\u0004\b(\u0010)J)\u0010*\u001a\u00020\u00042\n\u0010\u000f\u001a\u00060\rj\u0002`\u000e2\u000e\u0010!\u001a\n\u0018\u00010\rj\u0004\u0018\u0001`\u0010¢\u0006\u0004\b*\u0010\u0013Rj\u0010.\u001aV\u0012$\u0012\"\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\t -*\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\t\u0018\u00010,0, -**\u0012$\u0012\"\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\t -*\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\t\u0018\u00010,0,\u0018\u00010+0+8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b.\u0010/R>\u00102\u001a\u001e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\t00j\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\t`18\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b2\u00103\u001a\u0004\b4\u00105\"\u0004\b6\u00107R\u0019\u00109\u001a\u0002088\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010:\u001a\u0004\b;\u0010<¨\u0006B"}, d2 = {"Lcom/discord/stores/StoreGifting;", "", "", "giftCode", "", "fetchGift", "(Ljava/lang/String;)V", ModelAuditLogEntry.CHANGE_KEY_CODE, "removeGiftCode", "Lcom/discord/stores/StoreGifting$GiftState;", "giftState", "setGifts", "(Ljava/lang/String;Lcom/discord/stores/StoreGifting$GiftState;)V", "", "Lcom/discord/primitives/SkuId;", "skuId", "Lcom/discord/primitives/PlanId;", "subscriptionPlanId", "clearGiftsForSku", "(JLjava/lang/Long;)V", "Lrx/Observable;", "requestGift", "(Ljava/lang/String;)Lrx/Observable;", "Lcom/discord/primitives/UserId;", "userId", "", "Lcom/discord/models/domain/ModelGift;", "getMyResolvedGifts", "(J)Lrx/Observable;", "gift", "acceptGift", "(Lcom/discord/models/domain/ModelGift;)V", "revokeGiftCode", "planId", "Lkotlin/Function1;", "onSuccess", "Lcom/discord/utilities/error/Error;", "onError", "generateGiftCode", "(JLjava/lang/Long;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "handlePreLogout", "()V", "fetchMyGiftsForSku", "Lrx/subjects/BehaviorSubject;", "", "kotlin.jvm.PlatformType", "knownGiftsSubject", "Lrx/subjects/BehaviorSubject;", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "knownGifts", "Ljava/util/HashMap;", "getKnownGifts", "()Ljava/util/HashMap;", "setKnownGifts", "(Ljava/util/HashMap;)V", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "getDispatcher", "()Lcom/discord/stores/Dispatcher;", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;)V", "Companion", "GiftState", "HasGift", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGifting {
    public static final Companion Companion = new Companion(null);
    private final Dispatcher dispatcher;
    private HashMap<String, GiftState> knownGifts = new HashMap<>();
    private final BehaviorSubject<Map<String, GiftState>> knownGiftsSubject = BehaviorSubject.l0(h0.emptyMap());

    /* compiled from: StoreGifting.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bJ)\u0010\b\u001a\u00020\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u000e\u0010\u0006\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0005¢\u0006\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/stores/StoreGifting$Companion;", "", "", "Lcom/discord/primitives/SkuId;", "skuId", "Lcom/discord/primitives/PlanId;", "planId", "", "makeComboId", "(JLjava/lang/Long;)Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public final String makeComboId(long j, Long l) {
            StringBuilder sb = new StringBuilder();
            sb.append(j);
            sb.append(MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR);
            sb.append(l);
            return sb.toString();
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreGifting.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0007\u0004\u0005\u0006\u0007\b\t\nB\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0007\u000b\f\r\u000e\u000f\u0010\u0011¨\u0006\u0012"}, d2 = {"Lcom/discord/stores/StoreGifting$GiftState;", "", HookHelper.constructorName, "()V", "Invalid", "LoadFailed", "Loading", "RedeemedFailed", "Redeeming", "Resolved", "Revoking", "Lcom/discord/stores/StoreGifting$GiftState$Loading;", "Lcom/discord/stores/StoreGifting$GiftState$LoadFailed;", "Lcom/discord/stores/StoreGifting$GiftState$Invalid;", "Lcom/discord/stores/StoreGifting$GiftState$Resolved;", "Lcom/discord/stores/StoreGifting$GiftState$Redeeming;", "Lcom/discord/stores/StoreGifting$GiftState$RedeemedFailed;", "Lcom/discord/stores/StoreGifting$GiftState$Revoking;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class GiftState {

        /* compiled from: StoreGifting.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreGifting$GiftState$Invalid;", "Lcom/discord/stores/StoreGifting$GiftState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Invalid extends GiftState {
            public static final Invalid INSTANCE = new Invalid();

            private Invalid() {
                super(null);
            }
        }

        /* compiled from: StoreGifting.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreGifting$GiftState$LoadFailed;", "Lcom/discord/stores/StoreGifting$GiftState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class LoadFailed extends GiftState {
            public static final LoadFailed INSTANCE = new LoadFailed();

            private LoadFailed() {
                super(null);
            }
        }

        /* compiled from: StoreGifting.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreGifting$GiftState$Loading;", "Lcom/discord/stores/StoreGifting$GiftState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Loading extends GiftState {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        /* compiled from: StoreGifting.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002B!\u0012\u0006\u0010\f\u001a\u00020\u0003\u0012\u0006\u0010\r\u001a\u00020\u0006\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\t¢\u0006\u0004\b \u0010!J\u0010\u0010\u0004\u001a\u00020\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ0\u0010\u000f\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00032\b\b\u0002\u0010\r\u001a\u00020\u00062\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\tHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0018\u001a\u00020\u00062\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u0019\u0010\r\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001a\u001a\u0004\b\u001b\u0010\bR\u001c\u0010\f\u001a\u00020\u00038\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\f\u0010\u001c\u001a\u0004\b\u001d\u0010\u0005R\u001b\u0010\u000e\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001e\u001a\u0004\b\u001f\u0010\u000b¨\u0006\""}, d2 = {"Lcom/discord/stores/StoreGifting$GiftState$RedeemedFailed;", "Lcom/discord/stores/StoreGifting$GiftState;", "Lcom/discord/stores/StoreGifting$HasGift;", "Lcom/discord/models/domain/ModelGift;", "component1", "()Lcom/discord/models/domain/ModelGift;", "", "component2", "()Z", "", "component3", "()Ljava/lang/Integer;", "gift", "canRetry", "errorCode", "copy", "(Lcom/discord/models/domain/ModelGift;ZLjava/lang/Integer;)Lcom/discord/stores/StoreGifting$GiftState$RedeemedFailed;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getCanRetry", "Lcom/discord/models/domain/ModelGift;", "getGift", "Ljava/lang/Integer;", "getErrorCode", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelGift;ZLjava/lang/Integer;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class RedeemedFailed extends GiftState implements HasGift {
            private final boolean canRetry;
            private final Integer errorCode;
            private final ModelGift gift;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public RedeemedFailed(ModelGift modelGift, boolean z2, Integer num) {
                super(null);
                m.checkNotNullParameter(modelGift, "gift");
                this.gift = modelGift;
                this.canRetry = z2;
                this.errorCode = num;
            }

            public static /* synthetic */ RedeemedFailed copy$default(RedeemedFailed redeemedFailed, ModelGift modelGift, boolean z2, Integer num, int i, Object obj) {
                if ((i & 1) != 0) {
                    modelGift = redeemedFailed.getGift();
                }
                if ((i & 2) != 0) {
                    z2 = redeemedFailed.canRetry;
                }
                if ((i & 4) != 0) {
                    num = redeemedFailed.errorCode;
                }
                return redeemedFailed.copy(modelGift, z2, num);
            }

            public final ModelGift component1() {
                return getGift();
            }

            public final boolean component2() {
                return this.canRetry;
            }

            public final Integer component3() {
                return this.errorCode;
            }

            public final RedeemedFailed copy(ModelGift modelGift, boolean z2, Integer num) {
                m.checkNotNullParameter(modelGift, "gift");
                return new RedeemedFailed(modelGift, z2, num);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof RedeemedFailed)) {
                    return false;
                }
                RedeemedFailed redeemedFailed = (RedeemedFailed) obj;
                return m.areEqual(getGift(), redeemedFailed.getGift()) && this.canRetry == redeemedFailed.canRetry && m.areEqual(this.errorCode, redeemedFailed.errorCode);
            }

            public final boolean getCanRetry() {
                return this.canRetry;
            }

            public final Integer getErrorCode() {
                return this.errorCode;
            }

            @Override // com.discord.stores.StoreGifting.HasGift
            public ModelGift getGift() {
                return this.gift;
            }

            public int hashCode() {
                ModelGift gift = getGift();
                int i = 0;
                int hashCode = (gift != null ? gift.hashCode() : 0) * 31;
                boolean z2 = this.canRetry;
                if (z2) {
                    z2 = true;
                }
                int i2 = z2 ? 1 : 0;
                int i3 = z2 ? 1 : 0;
                int i4 = (hashCode + i2) * 31;
                Integer num = this.errorCode;
                if (num != null) {
                    i = num.hashCode();
                }
                return i4 + i;
            }

            public String toString() {
                StringBuilder R = a.R("RedeemedFailed(gift=");
                R.append(getGift());
                R.append(", canRetry=");
                R.append(this.canRetry);
                R.append(", errorCode=");
                return a.E(R, this.errorCode, ")");
            }
        }

        /* compiled from: StoreGifting.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002B\u000f\u0012\u0006\u0010\u0006\u001a\u00020\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0004\u001a\u00020\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u001a\u0010\u0007\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u0003HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001c\u0010\u0006\u001a\u00020\u00038\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/stores/StoreGifting$GiftState$Redeeming;", "Lcom/discord/stores/StoreGifting$GiftState;", "Lcom/discord/stores/StoreGifting$HasGift;", "Lcom/discord/models/domain/ModelGift;", "component1", "()Lcom/discord/models/domain/ModelGift;", "gift", "copy", "(Lcom/discord/models/domain/ModelGift;)Lcom/discord/stores/StoreGifting$GiftState$Redeeming;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/ModelGift;", "getGift", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelGift;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Redeeming extends GiftState implements HasGift {
            private final ModelGift gift;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Redeeming(ModelGift modelGift) {
                super(null);
                m.checkNotNullParameter(modelGift, "gift");
                this.gift = modelGift;
            }

            public static /* synthetic */ Redeeming copy$default(Redeeming redeeming, ModelGift modelGift, int i, Object obj) {
                if ((i & 1) != 0) {
                    modelGift = redeeming.getGift();
                }
                return redeeming.copy(modelGift);
            }

            public final ModelGift component1() {
                return getGift();
            }

            public final Redeeming copy(ModelGift modelGift) {
                m.checkNotNullParameter(modelGift, "gift");
                return new Redeeming(modelGift);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Redeeming) && m.areEqual(getGift(), ((Redeeming) obj).getGift());
                }
                return true;
            }

            @Override // com.discord.stores.StoreGifting.HasGift
            public ModelGift getGift() {
                return this.gift;
            }

            public int hashCode() {
                ModelGift gift = getGift();
                if (gift != null) {
                    return gift.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Redeeming(gift=");
                R.append(getGift());
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: StoreGifting.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002B\u000f\u0012\u0006\u0010\u0006\u001a\u00020\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0004\u001a\u00020\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u001a\u0010\u0007\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u0003HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001c\u0010\u0006\u001a\u00020\u00038\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/stores/StoreGifting$GiftState$Resolved;", "Lcom/discord/stores/StoreGifting$GiftState;", "Lcom/discord/stores/StoreGifting$HasGift;", "Lcom/discord/models/domain/ModelGift;", "component1", "()Lcom/discord/models/domain/ModelGift;", "gift", "copy", "(Lcom/discord/models/domain/ModelGift;)Lcom/discord/stores/StoreGifting$GiftState$Resolved;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/ModelGift;", "getGift", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelGift;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Resolved extends GiftState implements HasGift {
            private final ModelGift gift;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Resolved(ModelGift modelGift) {
                super(null);
                m.checkNotNullParameter(modelGift, "gift");
                this.gift = modelGift;
            }

            public static /* synthetic */ Resolved copy$default(Resolved resolved, ModelGift modelGift, int i, Object obj) {
                if ((i & 1) != 0) {
                    modelGift = resolved.getGift();
                }
                return resolved.copy(modelGift);
            }

            public final ModelGift component1() {
                return getGift();
            }

            public final Resolved copy(ModelGift modelGift) {
                m.checkNotNullParameter(modelGift, "gift");
                return new Resolved(modelGift);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Resolved) && m.areEqual(getGift(), ((Resolved) obj).getGift());
                }
                return true;
            }

            @Override // com.discord.stores.StoreGifting.HasGift
            public ModelGift getGift() {
                return this.gift;
            }

            public int hashCode() {
                ModelGift gift = getGift();
                if (gift != null) {
                    return gift.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Resolved(gift=");
                R.append(getGift());
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: StoreGifting.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u00012\u00020\u0002B\u000f\u0012\u0006\u0010\u0006\u001a\u00020\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0004\u001a\u00020\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u001a\u0010\u0007\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u0003HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001c\u0010\u0006\u001a\u00020\u00038\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/stores/StoreGifting$GiftState$Revoking;", "Lcom/discord/stores/StoreGifting$GiftState;", "Lcom/discord/stores/StoreGifting$HasGift;", "Lcom/discord/models/domain/ModelGift;", "component1", "()Lcom/discord/models/domain/ModelGift;", "gift", "copy", "(Lcom/discord/models/domain/ModelGift;)Lcom/discord/stores/StoreGifting$GiftState$Revoking;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/ModelGift;", "getGift", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelGift;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Revoking extends GiftState implements HasGift {
            private final ModelGift gift;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Revoking(ModelGift modelGift) {
                super(null);
                m.checkNotNullParameter(modelGift, "gift");
                this.gift = modelGift;
            }

            public static /* synthetic */ Revoking copy$default(Revoking revoking, ModelGift modelGift, int i, Object obj) {
                if ((i & 1) != 0) {
                    modelGift = revoking.getGift();
                }
                return revoking.copy(modelGift);
            }

            public final ModelGift component1() {
                return getGift();
            }

            public final Revoking copy(ModelGift modelGift) {
                m.checkNotNullParameter(modelGift, "gift");
                return new Revoking(modelGift);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Revoking) && m.areEqual(getGift(), ((Revoking) obj).getGift());
                }
                return true;
            }

            @Override // com.discord.stores.StoreGifting.HasGift
            public ModelGift getGift() {
                return this.gift;
            }

            public int hashCode() {
                ModelGift gift = getGift();
                if (gift != null) {
                    return gift.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Revoking(gift=");
                R.append(getGift());
                R.append(")");
                return R.toString();
            }
        }

        private GiftState() {
        }

        public /* synthetic */ GiftState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreGifting.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001R\u0016\u0010\u0005\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004¨\u0006\u0006"}, d2 = {"Lcom/discord/stores/StoreGifting$HasGift;", "", "Lcom/discord/models/domain/ModelGift;", "getGift", "()Lcom/discord/models/domain/ModelGift;", "gift", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public interface HasGift {
        ModelGift getGift();
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;
        public static final /* synthetic */ int[] $EnumSwitchMapping$2;

        static {
            Error.Type.values();
            int[] iArr = new int[17];
            $EnumSwitchMapping$0 = iArr;
            Error.Type type = Error.Type.DISCORD_REQUEST_ERROR;
            iArr[type.ordinal()] = 1;
            Error.Type type2 = Error.Type.NETWORK;
            iArr[type2.ordinal()] = 2;
            Error.Type.values();
            int[] iArr2 = new int[17];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[type.ordinal()] = 1;
            iArr2[type2.ordinal()] = 2;
            Error.Type.values();
            int[] iArr3 = new int[17];
            $EnumSwitchMapping$2 = iArr3;
            iArr3[type.ordinal()] = 1;
            iArr3[type2.ordinal()] = 2;
        }
    }

    public StoreGifting(Dispatcher dispatcher) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        this.dispatcher = dispatcher;
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void clearGiftsForSku(long j, Long l) {
        HashMap<String, GiftState> hashMap = this.knownGifts;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry<String, GiftState> entry : hashMap.entrySet()) {
            GiftState value = entry.getValue();
            boolean z2 = true;
            if (value instanceof GiftState.Resolved) {
                GiftState.Resolved resolved = (GiftState.Resolved) value;
                if (resolved.getGift().getSkuId() == j && l != null && !(!m.areEqual(resolved.getGift().getSubscriptionPlanId(), l))) {
                    z2 = false;
                }
            }
            if (z2) {
                linkedHashMap.put(entry.getKey(), entry.getValue());
            }
        }
        this.knownGifts = new HashMap<>(linkedHashMap);
        this.knownGiftsSubject.onNext(new HashMap(this.knownGifts));
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void fetchGift(String str) {
        if (this.knownGifts.containsKey(str) && !(this.knownGifts.get(str) instanceof GiftState.LoadFailed)) {
            if (this.knownGifts.get(str) instanceof GiftState.Resolved) {
                GiftState giftState = this.knownGifts.get(str);
                Objects.requireNonNull(giftState, "null cannot be cast to non-null type com.discord.stores.StoreGifting.GiftState.Resolved");
                if (((GiftState.Resolved) giftState).getGift().isComplete()) {
                    return;
                }
            } else {
                return;
            }
        }
        setGifts(str, GiftState.Loading.INSTANCE);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().resolveGiftCode(str, true, true), false, 1, null), StoreGifting.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new StoreGifting$fetchGift$2(this, str), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreGifting$fetchGift$1(this, str));
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void removeGiftCode(String str) {
        this.knownGifts.remove(str);
        this.knownGiftsSubject.onNext(new HashMap(this.knownGifts));
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void setGifts(String str, GiftState giftState) {
        this.knownGifts.put(str, giftState);
        this.knownGiftsSubject.onNext(new HashMap(this.knownGifts));
    }

    public final void acceptGift(ModelGift modelGift) {
        m.checkNotNullParameter(modelGift, "gift");
        this.dispatcher.schedule(new StoreGifting$acceptGift$1(this, modelGift));
    }

    public final void fetchMyGiftsForSku(long j, Long l) {
        String makeComboId = Companion.makeComboId(j, l);
        if (!this.knownGifts.containsKey(makeComboId) || (this.knownGifts.get(makeComboId) instanceof GiftState.LoadFailed)) {
            this.dispatcher.schedule(new StoreGifting$fetchMyGiftsForSku$1(this, makeComboId));
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().resolveSkuIdGift(j, l), false, 1, null), StoreGifting.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new StoreGifting$fetchMyGiftsForSku$2(this, makeComboId), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreGifting$fetchMyGiftsForSku$3(this, makeComboId, j, l));
        }
    }

    public final void generateGiftCode(long j, Long l, Function1<? super ModelGift, Unit> function1, Function1<? super Error, Unit> function12) {
        String makeComboId = Companion.makeComboId(j, l);
        if (!(this.knownGifts.get(makeComboId) instanceof GiftState.Loading)) {
            this.dispatcher.schedule(new StoreGifting$generateGiftCode$1(this, makeComboId));
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().generateGiftCode(new RestAPIParams.GenerateGiftCode(j, l)), false, 1, null), StoreGifting.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new StoreGifting$generateGiftCode$2(this, makeComboId, function12), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreGifting$generateGiftCode$3(this, makeComboId, function1));
        }
    }

    public final Dispatcher getDispatcher() {
        return this.dispatcher;
    }

    public final HashMap<String, GiftState> getKnownGifts() {
        return this.knownGifts;
    }

    public final Observable<List<ModelGift>> getMyResolvedGifts(final long j) {
        Observable F = this.knownGiftsSubject.F(new b<Map<String, ? extends GiftState>, List<? extends ModelGift>>() { // from class: com.discord.stores.StoreGifting$getMyResolvedGifts$1
            public final List<ModelGift> call(Map<String, ? extends StoreGifting.GiftState> map) {
                User user;
                Collection<? extends StoreGifting.GiftState> values = map.values();
                ArrayList<StoreGifting.GiftState> arrayList = new ArrayList();
                for (T t : values) {
                    StoreGifting.GiftState giftState = (StoreGifting.GiftState) t;
                    if ((giftState instanceof StoreGifting.GiftState.Resolved) && (user = ((StoreGifting.GiftState.Resolved) giftState).getGift().getUser()) != null && user.i() == j) {
                        arrayList.add(t);
                    }
                }
                ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(arrayList, 10));
                for (StoreGifting.GiftState giftState2 : arrayList) {
                    Objects.requireNonNull(giftState2, "null cannot be cast to non-null type com.discord.stores.StoreGifting.GiftState.Resolved");
                    arrayList2.add(((StoreGifting.GiftState.Resolved) giftState2).getGift());
                }
                return arrayList2;
            }
        });
        m.checkNotNullExpressionValue(F, "knownGiftsSubject\n      …              }\n        }");
        return F;
    }

    @StoreThread
    public final void handlePreLogout() {
        this.knownGifts.clear();
    }

    public final Observable<GiftState> requestGift(final String str) {
        m.checkNotNullParameter(str, "giftCode");
        this.dispatcher.schedule(new StoreGifting$requestGift$1(this, str));
        Observable<GiftState> q = this.knownGiftsSubject.F(new b<Map<String, ? extends GiftState>, GiftState>() { // from class: com.discord.stores.StoreGifting$requestGift$2
            public final StoreGifting.GiftState call(Map<String, ? extends StoreGifting.GiftState> map) {
                StoreGifting.GiftState giftState = map.get(str);
                return giftState != null ? giftState : StoreGifting.GiftState.Loading.INSTANCE;
            }
        }).q();
        m.checkNotNullExpressionValue(q, "knownGiftsSubject\n      …  .distinctUntilChanged()");
        return q;
    }

    public final void revokeGiftCode(ModelGift modelGift) {
        m.checkNotNullParameter(modelGift, "gift");
        if (!this.knownGifts.containsKey(modelGift.getCode()) || !(this.knownGifts.get(modelGift.getCode()) instanceof GiftState.Revoking)) {
            this.dispatcher.schedule(new StoreGifting$revokeGiftCode$1(this, modelGift));
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().revokeGiftCode(modelGift.getCode()), false, 1, null), StoreGifting.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new StoreGifting$revokeGiftCode$2(this, modelGift), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreGifting$revokeGiftCode$3(this, modelGift));
        }
    }

    public final void setKnownGifts(HashMap<String, GiftState> hashMap) {
        m.checkNotNullParameter(hashMap, "<set-?>");
        this.knownGifts = hashMap;
    }
}
