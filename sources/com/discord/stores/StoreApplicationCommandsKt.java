package com.discord.stores;

import android.content.res.Resources;
import com.discord.api.commands.ApplicationCommandPermission;
import com.discord.api.commands.ApplicationCommandType;
import com.discord.api.commands.CommandChoice;
import com.discord.models.commands.Application;
import com.discord.models.commands.ApplicationCommand;
import com.discord.models.commands.ApplicationCommandOption;
import com.discord.models.commands.ApplicationSubCommand;
import com.discord.models.commands.RemoteApplicationCommand;
import d0.g0.t;
import d0.t.n;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: StoreApplicationCommands.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a\u001b\u0010\u0004\u001a\u0004\u0018\u00010\u0003*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0001¢\u0006\u0004\b\u0004\u0010\u0005\u001a\u001b\u0010\u0004\u001a\u0004\u0018\u00010\u0003*\u00020\u00062\u0006\u0010\u0002\u001a\u00020\u0001¢\u0006\u0004\b\u0004\u0010\u0007\u001a\u001b\u0010\b\u001a\u0004\u0018\u00010\u0003*\u00020\u00062\u0006\u0010\u0002\u001a\u00020\u0001¢\u0006\u0004\b\b\u0010\u0007\u001a\u001d\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00000\t*\b\u0012\u0004\u0012\u00020\u00000\t¢\u0006\u0004\b\n\u0010\u000b\u001a\u0019\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00000\t*\u00020\u0000H\u0002¢\u0006\u0004\b\f\u0010\r\u001a\u001b\u0010\u0010\u001a\u00020\u000f*\u00020\u00002\u0006\u0010\u000e\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\u0010\u0010\u0011\u001a!\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00000\t*\u00020\u00002\u0006\u0010\u0012\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\u0013\u0010\u0014\u001a\u0011\u0010\u0017\u001a\u00020\u0016*\u00020\u0015¢\u0006\u0004\b\u0017\u0010\u0018\u001a\u001d\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00000\t*\b\u0012\u0004\u0012\u00020\u00190\t¢\u0006\u0004\b\u001a\u0010\u000b\u001a\u0011\u0010\u001b\u001a\u00020\u0000*\u00020\u0019¢\u0006\u0004\b\u001b\u0010\u001c\u001a\u0011\u0010\u001e\u001a\u00020\u0006*\u00020\u001d¢\u0006\u0004\b\u001e\u0010\u001f¨\u0006 "}, d2 = {"Lcom/discord/models/commands/ApplicationCommand;", "Landroid/content/res/Resources;", "resources", "", "getDescriptionText", "(Lcom/discord/models/commands/ApplicationCommand;Landroid/content/res/Resources;)Ljava/lang/String;", "Lcom/discord/models/commands/ApplicationCommandOption;", "(Lcom/discord/models/commands/ApplicationCommandOption;Landroid/content/res/Resources;)Ljava/lang/String;", "getErrorText", "", "flattenSubCommands", "(Ljava/util/List;)Ljava/util/List;", "expandSubCommands", "(Lcom/discord/models/commands/ApplicationCommand;)Ljava/util/List;", "subcommand", "Lcom/discord/models/commands/ApplicationSubCommand;", "expandSubCommand", "(Lcom/discord/models/commands/ApplicationCommand;Lcom/discord/models/commands/ApplicationCommandOption;)Lcom/discord/models/commands/ApplicationSubCommand;", "subcommandGroup", "expandSubCommandGroup", "(Lcom/discord/models/commands/ApplicationCommand;Lcom/discord/models/commands/ApplicationCommandOption;)Ljava/util/List;", "Lcom/discord/api/commands/Application;", "Lcom/discord/models/commands/Application;", "toDomainApplication", "(Lcom/discord/api/commands/Application;)Lcom/discord/models/commands/Application;", "Lcom/discord/api/commands/ApplicationCommand;", "toDomainCommands", "toSlashCommand", "(Lcom/discord/api/commands/ApplicationCommand;)Lcom/discord/models/commands/ApplicationCommand;", "Lcom/discord/api/commands/ApplicationCommandOption;", "toSlashCommandOption", "(Lcom/discord/api/commands/ApplicationCommandOption;)Lcom/discord/models/commands/ApplicationCommandOption;", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreApplicationCommandsKt {
    private static final ApplicationSubCommand expandSubCommand(ApplicationCommand applicationCommand, ApplicationCommandOption applicationCommandOption) {
        String name = applicationCommandOption.getName();
        long applicationId = applicationCommand.getApplicationId();
        String str = applicationCommand.getName() + " " + applicationCommandOption.getName();
        String description = applicationCommandOption.getDescription();
        List<ApplicationCommandOption> options = applicationCommandOption.getOptions();
        if (options == null) {
            options = n.emptyList();
        }
        return new ApplicationSubCommand(applicationCommand, name, null, applicationId, str, description, null, options, applicationCommand.getGuildId(), null, null, applicationCommand.getVersion(), 1604, null);
    }

    private static final List<ApplicationCommand> expandSubCommandGroup(ApplicationCommand applicationCommand, ApplicationCommandOption applicationCommandOption) {
        ArrayList arrayList = new ArrayList();
        List<ApplicationCommandOption> options = applicationCommandOption.getOptions();
        if (options != null) {
            for (ApplicationCommandOption applicationCommandOption2 : options) {
                if (applicationCommandOption2.getType() == ApplicationCommandType.SUBCOMMAND) {
                    String name = applicationCommandOption2.getName();
                    String name2 = applicationCommandOption.getName();
                    long applicationId = applicationCommand.getApplicationId();
                    String str = applicationCommand.getName() + " " + applicationCommandOption.getName() + " " + applicationCommandOption2.getName();
                    String description = applicationCommandOption2.getDescription();
                    List<ApplicationCommandOption> options2 = applicationCommandOption2.getOptions();
                    if (options2 == null) {
                        options2 = n.emptyList();
                    }
                    arrayList.add(new ApplicationSubCommand(applicationCommand, name, name2, applicationId, str, description, null, options2, applicationCommand.getGuildId(), null, null, applicationCommand.getVersion(), 1600, null));
                }
            }
        }
        return arrayList;
    }

    private static final List<ApplicationCommand> expandSubCommands(ApplicationCommand applicationCommand) {
        ArrayList arrayList = new ArrayList();
        boolean z2 = false;
        for (ApplicationCommandOption applicationCommandOption : applicationCommand.getOptions()) {
            if (applicationCommandOption.getType() == ApplicationCommandType.SUBCOMMAND) {
                arrayList.add(expandSubCommand(applicationCommand, applicationCommandOption));
            } else if (applicationCommandOption.getType() == ApplicationCommandType.SUBCOMMAND_GROUP) {
                arrayList.addAll(expandSubCommandGroup(applicationCommand, applicationCommandOption));
            }
            z2 = true;
        }
        if (!z2) {
            arrayList.add(applicationCommand);
        }
        return arrayList;
    }

    public static final List<ApplicationCommand> flattenSubCommands(List<? extends ApplicationCommand> list) {
        m.checkNotNullParameter(list, "$this$flattenSubCommands");
        ArrayList arrayList = new ArrayList();
        for (ApplicationCommand applicationCommand : list) {
            if (applicationCommand instanceof RemoteApplicationCommand) {
                arrayList.addAll(expandSubCommands(applicationCommand));
            } else {
                arrayList.add(applicationCommand);
            }
        }
        return arrayList;
    }

    public static final String getDescriptionText(ApplicationCommand applicationCommand, Resources resources) {
        m.checkNotNullParameter(applicationCommand, "$this$getDescriptionText");
        m.checkNotNullParameter(resources, "resources");
        Integer descriptionRes = applicationCommand.getDescriptionRes();
        if (descriptionRes != null) {
            String string = resources.getString(descriptionRes.intValue());
            m.checkNotNullExpressionValue(string, "resources.getString(it)");
            String replace$default = t.replace$default(string, "¯_(ツ)_/¯", "¯\\_(ツ)_/¯", false, 4, (Object) null);
            if (replace$default != null) {
                return replace$default;
            }
        }
        return applicationCommand.getDescription();
    }

    public static final String getErrorText(ApplicationCommandOption applicationCommandOption, Resources resources) {
        m.checkNotNullParameter(applicationCommandOption, "$this$getErrorText");
        m.checkNotNullParameter(resources, "resources");
        if (applicationCommandOption.getType() == ApplicationCommandType.STRING) {
            List<CommandChoice> choices = applicationCommandOption.getChoices();
            if (!(choices == null || choices.isEmpty())) {
                return resources.getString(R.string.command_validation_choice_error);
            }
        }
        return applicationCommandOption.getType() == ApplicationCommandType.INTEGER ? resources.getString(R.string.command_validation_integer_error) : applicationCommandOption.getType() == ApplicationCommandType.NUMBER ? resources.getString(R.string.command_validation_number_error) : applicationCommandOption.getType() == ApplicationCommandType.BOOLEAN ? resources.getString(R.string.command_validation_boolean_error) : applicationCommandOption.getType() == ApplicationCommandType.USER ? resources.getString(R.string.command_validation_user_error) : applicationCommandOption.getType() == ApplicationCommandType.CHANNEL ? resources.getString(R.string.command_validation_channel_error) : applicationCommandOption.getType() == ApplicationCommandType.ROLE ? resources.getString(R.string.command_validation_role_error) : applicationCommandOption.getType() == ApplicationCommandType.MENTIONABLE ? resources.getString(R.string.command_validation_mentionable_error) : resources.getString(R.string.command_validation_required_error);
    }

    public static final Application toDomainApplication(com.discord.api.commands.Application application) {
        m.checkNotNullParameter(application, "$this$toDomainApplication");
        return new Application(application.d(), application.e(), application.c(), null, application.b(), application.a(), false, 72, null);
    }

    public static final List<ApplicationCommand> toDomainCommands(List<com.discord.api.commands.ApplicationCommand> list) {
        m.checkNotNullParameter(list, "$this$toDomainCommands");
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
        for (com.discord.api.commands.ApplicationCommand applicationCommand : list) {
            arrayList.add(toSlashCommand(applicationCommand));
        }
        return arrayList;
    }

    public static final ApplicationCommand toSlashCommand(com.discord.api.commands.ApplicationCommand applicationCommand) {
        List list;
        m.checkNotNullParameter(applicationCommand, "$this$toSlashCommand");
        String valueOf = String.valueOf(applicationCommand.e());
        long a = applicationCommand.a();
        String f = applicationCommand.f();
        String c = applicationCommand.c();
        List<com.discord.api.commands.ApplicationCommandOption> g = applicationCommand.g();
        if (g != null) {
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(g, 10));
            for (com.discord.api.commands.ApplicationCommandOption applicationCommandOption : g) {
                arrayList.add(toSlashCommandOption(applicationCommandOption));
            }
            list = arrayList;
        } else {
            list = n.emptyList();
        }
        String i = applicationCommand.i();
        List<ApplicationCommandPermission> h = applicationCommand.h();
        Boolean b2 = applicationCommand.b();
        String d = applicationCommand.d();
        return new RemoteApplicationCommand(valueOf, a, f, c, list, d != null ? Long.valueOf(Long.parseLong(d)) : null, i, b2, h, null, 512, null);
    }

    public static final ApplicationCommandOption toSlashCommandOption(com.discord.api.commands.ApplicationCommandOption applicationCommandOption) {
        ArrayList arrayList;
        m.checkNotNullParameter(applicationCommandOption, "$this$toSlashCommandOption");
        ApplicationCommandType k = applicationCommandOption.k();
        String h = applicationCommandOption.h();
        String e = applicationCommandOption.e();
        boolean j = applicationCommandOption.j();
        boolean d = applicationCommandOption.d();
        List<CommandChoice> c = applicationCommandOption.c();
        List<Integer> b2 = applicationCommandOption.b();
        Number g = applicationCommandOption.g();
        Number f = applicationCommandOption.f();
        List<com.discord.api.commands.ApplicationCommandOption> i = applicationCommandOption.i();
        if (i != null) {
            ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(i, 10));
            for (com.discord.api.commands.ApplicationCommandOption applicationCommandOption2 : i) {
                arrayList2.add(toSlashCommandOption(applicationCommandOption2));
            }
            arrayList = arrayList2;
        } else {
            arrayList = null;
        }
        return new ApplicationCommandOption(k, h, e, null, j, d, b2, c, arrayList, applicationCommandOption.a(), g, f, 8, null);
    }

    public static final String getDescriptionText(ApplicationCommandOption applicationCommandOption, Resources resources) {
        String string;
        m.checkNotNullParameter(applicationCommandOption, "$this$getDescriptionText");
        m.checkNotNullParameter(resources, "resources");
        Integer descriptionRes = applicationCommandOption.getDescriptionRes();
        return (descriptionRes == null || (string = resources.getString(descriptionRes.intValue())) == null) ? applicationCommandOption.getDescription() : string;
    }
}
