package com.discord.stores;

import com.discord.models.presence.Presence;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreUserPresence.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u0016\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/presence/Presence;", "Lcom/discord/stores/AppPresence;", "invoke", "()Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserPresence$observeAllPresences$1 extends o implements Function0<Map<Long, ? extends Presence>> {
    public final /* synthetic */ StoreUserPresence this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreUserPresence$observeAllPresences$1(StoreUserPresence storeUserPresence) {
        super(0);
        this.this$0 = storeUserPresence;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Map<Long, ? extends Presence> invoke() {
        return this.this$0.m14getPresences();
    }
}
