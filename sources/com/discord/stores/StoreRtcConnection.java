package com.discord.stores;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.Context;
import android.util.Log;
import androidx.core.app.NotificationCompat;
import b.a.q.w;
import b.d.b.a.a;
import co.discord.media_engine.VideoInputDeviceDescription;
import com.discord.api.channel.Channel;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.stageinstance.StageInstance;
import com.discord.api.voice.server.VoiceServer;
import com.discord.api.voice.state.VoiceState;
import com.discord.app.App;
import com.discord.app.AppLog;
import com.discord.gateway.io.OutgoingPayload;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelPayload;
import com.discord.rtcconnection.RtcConnection;
import com.discord.rtcconnection.VideoMetadata;
import com.discord.rtcconnection.mediaengine.MediaEngineConnection;
import com.discord.stores.StoreMediaSettings;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.collections.ListenerCollection;
import com.discord.utilities.collections.ListenerCollectionSubject;
import com.discord.utilities.debug.DebugPrintBuilder;
import com.discord.utilities.debug.DebugPrintable;
import com.discord.utilities.debug.DebugPrintableCollection;
import com.discord.utilities.io.NetworkUtils;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.networking.NetworkMonitor;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.ssl.SecureSocketsLayerUtils;
import com.discord.utilities.systemlog.SystemLogUtils;
import com.discord.utilities.time.Clock;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import d0.t.h0;
import d0.z.d.m;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLSocketFactory;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
/* compiled from: StoreRtcConnection.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000ø\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0010\u0007\n\u0002\b\n\n\u0002\u0010\u0003\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 Ú\u00012\u00020\u00012\u00020\u0002:\bÚ\u0001Û\u0001Ü\u0001Ý\u0001B]\u0012\b\u0010Ó\u0001\u001a\u00030Ò\u0001\u0012\b\u0010\u0093\u0001\u001a\u00030\u0092\u0001\u0012\b\u0010¯\u0001\u001a\u00030®\u0001\u0012\b\u0010¹\u0001\u001a\u00030¸\u0001\u0012\b\u0010¶\u0001\u001a\u00030µ\u0001\u0012\b\u0010\u0084\u0001\u001a\u00030\u0083\u0001\u0012\n\b\u0002\u0010\u0090\u0001\u001a\u00030\u008f\u0001\u0012\n\b\u0002\u0010\u009e\u0001\u001a\u00030\u009d\u0001¢\u0006\u0006\bØ\u0001\u0010Ù\u0001J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\b\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\b\u0010\u0007J\u0017\u0010\u000b\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\tH\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u000e\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\tH\u0003¢\u0006\u0004\b\u000e\u0010\fJ\u0017\u0010\u0010\u001a\u00020\u00052\u0006\u0010\u000f\u001a\u00020\tH\u0003¢\u0006\u0004\b\u0010\u0010\fJ\u000f\u0010\u0011\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u000f\u0010\u0013\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\u0013\u0010\u0012J\u000f\u0010\u0014\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\u0014\u0010\u0012J\u0017\u0010\u0017\u001a\u00020\u00052\u0006\u0010\u0016\u001a\u00020\u0015H\u0003¢\u0006\u0004\b\u0017\u0010\u0018J\u000f\u0010\u0019\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\u0019\u0010\u0012J\u000f\u0010\u001a\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\u001a\u0010\u0012J)\u0010 \u001a\u00020\u00052\u0006\u0010\u001c\u001a\u00020\u001b2\u0010\b\u0002\u0010\u001f\u001a\n\u0018\u00010\u001dj\u0004\u0018\u0001`\u001eH\u0002¢\u0006\u0004\b \u0010!J'\u0010%\u001a\u00020\u00052\u0016\u0010$\u001a\u0012\u0012\b\u0012\u00060\u001dj\u0002`\u001e\u0012\u0004\u0012\u00020#0\"H\u0003¢\u0006\u0004\b%\u0010&J'\u0010(\u001a\u00020\u00052\u0016\u0010'\u001a\u0012\u0012\b\u0012\u00060\u001dj\u0002`\u001e\u0012\u0004\u0012\u00020\t0\"H\u0003¢\u0006\u0004\b(\u0010&J'\u0010*\u001a\u00020\u00052\u0016\u0010)\u001a\u0012\u0012\b\u0012\u00060\u001dj\u0002`\u001e\u0012\u0004\u0012\u00020\t0\"H\u0003¢\u0006\u0004\b*\u0010&J\u0017\u0010,\u001a\u00020\u00052\u0006\u0010+\u001a\u00020\u0015H\u0002¢\u0006\u0004\b,\u0010\u0018J#\u00100\u001a\u00020\u00052\u0006\u0010-\u001a\u00020\u00152\n\b\u0002\u0010/\u001a\u0004\u0018\u00010.H\u0002¢\u0006\u0004\b0\u00101J\u0017\u00102\u001a\u00020\u00052\u0006\u0010-\u001a\u00020\u0015H\u0002¢\u0006\u0004\b2\u0010\u0018J;\u00104\u001a\u00020\u00052\u0006\u0010-\u001a\u00020\u00152\n\b\u0002\u0010/\u001a\u0004\u0018\u00010.2\u0016\b\u0002\u00103\u001a\u0010\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u0015\u0018\u00010\"H\u0002¢\u0006\u0004\b4\u00105J\u0015\u00108\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010706¢\u0006\u0004\b8\u00109J\u0011\u0010:\u001a\u0004\u0018\u000107H\u0007¢\u0006\u0004\b:\u0010;J\u001f\u0010@\u001a\u00020\u00052\u0006\u0010=\u001a\u00020<2\u0006\u0010?\u001a\u00020>H\u0007¢\u0006\u0004\b@\u0010AJ\u0017\u0010D\u001a\u00020\u00052\u0006\u0010C\u001a\u00020BH\u0016¢\u0006\u0004\bD\u0010EJ\u000f\u0010F\u001a\u00020\u0005H\u0004¢\u0006\u0004\bF\u0010\u0012J\u0017\u0010H\u001a\u00020\u00052\u0006\u0010G\u001a\u00020\tH\u0007¢\u0006\u0004\bH\u0010\fJ\u0017\u0010K\u001a\u00020\u00052\u0006\u0010J\u001a\u00020IH\u0007¢\u0006\u0004\bK\u0010LJ\u0017\u0010O\u001a\u00020\u00052\u0006\u0010N\u001a\u00020MH\u0007¢\u0006\u0004\bO\u0010PJ\u001f\u0010S\u001a\u00020\u00052\u000e\u0010R\u001a\n\u0018\u00010\u001dj\u0004\u0018\u0001`QH\u0007¢\u0006\u0004\bS\u0010TJ\u0017\u0010W\u001a\u00020\u00052\u0006\u0010V\u001a\u00020UH\u0007¢\u0006\u0004\bW\u0010XJ\u0017\u0010[\u001a\u00020\u00052\u0006\u0010Z\u001a\u00020YH\u0016¢\u0006\u0004\b[\u0010\\J#\u0010_\u001a\u00020\u00052\n\u0010]\u001a\u00060\u001dj\u0002`\u001e2\u0006\u0010^\u001a\u00020\tH\u0016¢\u0006\u0004\b_\u0010`JC\u0010g\u001a\u00020\u00052\n\u0010]\u001a\u00060\u001dj\u0002`\u001e2\u000e\u0010c\u001a\n\u0018\u00010aj\u0004\u0018\u0001`b2\u0006\u0010d\u001a\u00020a2\u0006\u0010e\u001a\u00020a2\u0006\u0010f\u001a\u00020aH\u0016¢\u0006\u0004\bg\u0010hJ\u0017\u0010j\u001a\u00020\u00052\u0006\u00103\u001a\u00020iH\u0016¢\u0006\u0004\bj\u0010kJ\u000f\u0010l\u001a\u00020\u0005H\u0016¢\u0006\u0004\bl\u0010\u0012J\u0017\u0010n\u001a\u00020\u00052\u0006\u0010m\u001a\u00020\u001bH\u0016¢\u0006\u0004\bn\u0010oJ#\u0010p\u001a\u00020\u00052\u0006\u0010m\u001a\u00020\u001b2\n\u0010]\u001a\u00060\u001dj\u0002`\u001eH\u0016¢\u0006\u0004\bp\u0010qJ\u000f\u0010r\u001a\u00020\u0005H\u0016¢\u0006\u0004\br\u0010\u0012J\u0017\u0010u\u001a\u00020\u00052\u0006\u0010t\u001a\u00020sH\u0016¢\u0006\u0004\bu\u0010vJ+\u0010|\u001a\u00020\u00052\u0006\u0010x\u001a\u00020w2\u0012\u0010{\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020z0yH\u0016¢\u0006\u0004\b|\u0010}J\u000f\u0010~\u001a\u00020\u0005H\u0016¢\u0006\u0004\b~\u0010\u0012J\u001f\u0010\u0081\u0001\u001a\u00020\u00052\u000b\u0010\u0080\u0001\u001a\u00060\u001dj\u0002`\u007fH\u0016¢\u0006\u0006\b\u0081\u0001\u0010\u0082\u0001R\u001a\u0010\u0084\u0001\u001a\u00030\u0083\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0084\u0001\u0010\u0085\u0001R\"\u0010\u0087\u0001\u001a\u000b\u0018\u00010\u0015j\u0005\u0018\u0001`\u0086\u00018\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u0087\u0001\u0010\u0088\u0001R8\u0010\u008b\u0001\u001a!\u0012\r\u0012\u000b \u008a\u0001*\u0004\u0018\u00010Y0Y\u0012\r\u0012\u000b \u008a\u0001*\u0004\u0018\u00010Y0Y0\u0089\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u008b\u0001\u0010\u008c\u0001R\u0019\u0010\u008d\u0001\u001a\u00020\t8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u008d\u0001\u0010\u008e\u0001R\u001a\u0010\u0090\u0001\u001a\u00030\u008f\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0090\u0001\u0010\u0091\u0001R\u001a\u0010\u0093\u0001\u001a\u00030\u0092\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0093\u0001\u0010\u0094\u0001R\u0017\u0010?\u001a\u00020>8\u0002@\u0002X\u0082.¢\u0006\u0007\n\u0005\b?\u0010\u0095\u0001R,\u0010\u001c\u001a\u0004\u0018\u00010\u001b2\t\u0010\u0096\u0001\u001a\u0004\u0018\u00010\u001b8\u0000@BX\u0080\u000e¢\u0006\u000f\n\u0005\b\u001c\u0010\u0097\u0001\u001a\u0006\b\u0098\u0001\u0010\u0099\u0001R\"\u0010\u009b\u0001\u001a\u000b\u0018\u00010\u001dj\u0005\u0018\u0001`\u009a\u00018\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u009b\u0001\u0010\u009c\u0001R\u001a\u0010\u009e\u0001\u001a\u00030\u009d\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u009e\u0001\u0010\u009f\u0001R\u001e\u0010¡\u0001\u001a\u00070\u001dj\u0003` \u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b¡\u0001\u0010¢\u0001R4\u0010¤\u0001\u001a\u001d\u0012\u0006\u0012\u0004\u0018\u000107 \u008a\u0001*\r\u0012\u0006\u0012\u0004\u0018\u000107\u0018\u00010£\u00010£\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b¤\u0001\u0010¥\u0001R\u001a\u0010§\u0001\u001a\u00030¦\u00018\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b§\u0001\u0010¨\u0001RF\u0010«\u0001\u001a/\u0012\u000f\u0012\r \u008a\u0001*\u0005\u0018\u00010ª\u00010ª\u0001 \u008a\u0001*\u0016\u0012\u000f\u0012\r \u008a\u0001*\u0005\u0018\u00010ª\u00010ª\u0001\u0018\u00010©\u00010©\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b«\u0001\u0010¬\u0001RB\u0010\u00ad\u0001\u001a+\u0012\r\u0012\u000b \u008a\u0001*\u0004\u0018\u00010s0s \u008a\u0001*\u0014\u0012\r\u0012\u000b \u008a\u0001*\u0004\u0018\u00010s0s\u0018\u00010£\u00010£\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u00ad\u0001\u0010¥\u0001R\u001a\u0010¯\u0001\u001a\u00030®\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b¯\u0001\u0010°\u0001R\u001b\u0010´\u0001\u001a\u00070\u001dj\u0003`±\u00018F@\u0006¢\u0006\b\u001a\u0006\b²\u0001\u0010³\u0001R\u001a\u0010¶\u0001\u001a\u00030µ\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b¶\u0001\u0010·\u0001R\u001a\u0010¹\u0001\u001a\u00030¸\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b¹\u0001\u0010º\u0001R&\u0010½\u0001\u001a\n\u0012\u0005\u0012\u00030¼\u00010»\u00018\u0006@\u0006¢\u0006\u0010\n\u0006\b½\u0001\u0010¾\u0001\u001a\u0006\b¿\u0001\u0010À\u0001R\u0019\u0010Á\u0001\u001a\u00020\u00158\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bÁ\u0001\u0010\u0088\u0001R!\u0010Ã\u0001\u001a\n\u0012\u0005\u0012\u00030¼\u00010Â\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bÃ\u0001\u0010Ä\u0001R\u0017\u0010=\u001a\u00020<8\u0002@\u0002X\u0082.¢\u0006\u0007\n\u0005\b=\u0010Å\u0001R#\u0010Æ\u0001\u001a\b\u0012\u0004\u0012\u00020Y068\u0006@\u0006¢\u0006\u000f\n\u0006\bÆ\u0001\u0010Ç\u0001\u001a\u0005\bÈ\u0001\u00109R!\u0010t\u001a\b\u0012\u0004\u0012\u00020s068\u0006@\u0006¢\u0006\u000e\n\u0005\bt\u0010Ç\u0001\u001a\u0005\bÉ\u0001\u00109R\u001b\u0010Ê\u0001\u001a\u0004\u0018\u0001078\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\bÊ\u0001\u0010Ë\u0001R\u0019\u0010N\u001a\u0004\u0018\u00010M8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\bN\u0010Ì\u0001R\u001b\u0010Í\u0001\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\bÍ\u0001\u0010Î\u0001R+\u0010Ð\u0001\u001a\u0010\u0012\f\u0012\n\u0012\u0005\u0012\u00030ª\u00010Ï\u0001068\u0006@\u0006¢\u0006\u000f\n\u0006\bÐ\u0001\u0010Ç\u0001\u001a\u0005\bÑ\u0001\u00109R\u001a\u0010Ó\u0001\u001a\u00030Ò\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bÓ\u0001\u0010Ô\u0001R\u001c\u0010Ö\u0001\u001a\u0005\u0018\u00010Õ\u00018\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\bÖ\u0001\u0010×\u0001¨\u0006Þ\u0001"}, d2 = {"Lcom/discord/stores/StoreRtcConnection;", "Lcom/discord/rtcconnection/RtcConnection$b;", "Lcom/discord/utilities/debug/DebugPrintable;", "Lcom/discord/api/channel/Channel;", "channel", "", "logChannelJoin", "(Lcom/discord/api/channel/Channel;)V", "logChannelLeave", "", "selfMuted", "handleSelfMuted", "(Z)V", "selfDeafened", "handleSelfDeafened", "selfVideo", "handleSelfVideo", "onVoiceStateUpdated", "()V", "checkForVoiceServerUpdate", "createRtcConnection", "", ModelAuditLogEntry.CHANGE_KEY_REASON, "destroyRtcConnection", "(Ljava/lang/String;)V", "handleMediaSessionIdReceived", "updateMetadata", "Lcom/discord/rtcconnection/RtcConnection;", "rtcConnection", "", "Lcom/discord/primitives/UserId;", "targetUserId", "applyVoiceConfiguration", "(Lcom/discord/rtcconnection/RtcConnection;Ljava/lang/Long;)V", "", "", "usersVolume", "handleUsersVolume", "(Ljava/util/Map;)V", "usersMuted", "handleUsersMuted", "usersOffScreen", "handleUsersOffScreen", "message", "recordBreadcrumb", NotificationCompat.CATEGORY_MESSAGE, "", "e", "logi", "(Ljava/lang/String;Ljava/lang/Throwable;)V", "logw", "metadata", "loge", "(Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V", "Lrx/Observable;", "Lcom/discord/rtcconnection/RtcConnection$Metadata;", "observeRtcConnectionMetadata", "()Lrx/Observable;", "getRtcConnectionMetadata", "()Lcom/discord/rtcconnection/RtcConnection$Metadata;", "Landroid/content/Context;", "context", "Lcom/discord/utilities/networking/NetworkMonitor;", "networkMonitor", "init", "(Landroid/content/Context;Lcom/discord/utilities/networking/NetworkMonitor;)V", "Lcom/discord/utilities/debug/DebugPrintBuilder;", "dp", "debugPrint", "(Lcom/discord/utilities/debug/DebugPrintBuilder;)V", "finalize", "connected", "handleConnectionReady", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "Lcom/discord/api/voice/server/VoiceServer;", "voiceServer", "handleVoiceServerUpdate", "(Lcom/discord/api/voice/server/VoiceServer;)V", "Lcom/discord/primitives/ChannelId;", "channelId", "handleVoiceChannelSelected", "(Ljava/lang/Long;)V", "Lcom/discord/api/voice/state/VoiceState;", "voiceState", "handleVoiceStateUpdate", "(Lcom/discord/api/voice/state/VoiceState;)V", "Lcom/discord/rtcconnection/RtcConnection$StateChange;", "stateChange", "onStateChange", "(Lcom/discord/rtcconnection/RtcConnection$StateChange;)V", "userId", "isSpeaking", "onSpeaking", "(JZ)V", "", "Lcom/discord/primitives/StreamId;", "streamId", "audioSsrc", "videoSsrc", "rtxSsrc", "onVideoStream", "(JLjava/lang/Integer;III)V", "Lcom/discord/rtcconnection/VideoMetadata;", "onVideoMetadata", "(Lcom/discord/rtcconnection/VideoMetadata;)V", "onMediaSessionIdReceived", "connection", "onMediaEngineConnectionConnected", "(Lcom/discord/rtcconnection/RtcConnection;)V", "onUserCreated", "(Lcom/discord/rtcconnection/RtcConnection;J)V", "onFatalClose", "Lcom/discord/rtcconnection/RtcConnection$Quality;", "quality", "onQualityUpdate", "(Lcom/discord/rtcconnection/RtcConnection$Quality;)V", "Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;", "event", "", "", "properties", "onAnalyticsEvent", "(Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;Ljava/util/Map;)V", "onFirstFrameSent", "Lcom/discord/primitives/SSRC;", "ssrc", "onFirstFrameReceived", "(J)V", "Lcom/discord/stores/StoreVoiceStates;", "storeVoiceStates", "Lcom/discord/stores/StoreVoiceStates;", "Lcom/discord/primitives/SessionId;", "sessionId", "Ljava/lang/String;", "Lrx/subjects/SerializedSubject;", "kotlin.jvm.PlatformType", "connectionStateSubject", "Lrx/subjects/SerializedSubject;", "hasSelectedVoiceChannel", "Z", "Lcom/discord/utilities/logging/Logger;", "logger", "Lcom/discord/utilities/logging/Logger;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/utilities/networking/NetworkMonitor;", "<set-?>", "Lcom/discord/rtcconnection/RtcConnection;", "getRtcConnection$app_productionGoogleRelease", "()Lcom/discord/rtcconnection/RtcConnection;", "Lcom/discord/primitives/Timestamp;", "joinedChannelTimestamp", "Ljava/lang/Long;", "Lcom/discord/utilities/debug/DebugPrintableCollection;", "dpc", "Lcom/discord/utilities/debug/DebugPrintableCollection;", "Lcom/discord/utilities/debug/DebugPrintableId;", "debugDisplayId", "J", "Lrx/subjects/BehaviorSubject;", "rtcConnectionMetadataSubject", "Lrx/subjects/BehaviorSubject;", "Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;", "currentVoiceState", "Lcom/discord/gateway/io/OutgoingPayload$VoiceStateUpdate;", "Lrx/subjects/PublishSubject;", "Lcom/discord/stores/StoreRtcConnection$SpeakingUserUpdate;", "speakingUsersSubject", "Lrx/subjects/PublishSubject;", "qualitySubject", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "Lcom/discord/primitives/GuildId;", "getConnectedGuildId", "()J", "connectedGuildId", "Lcom/discord/stores/StoreAnalytics;", "storeAnalytics", "Lcom/discord/stores/StoreAnalytics;", "Lcom/discord/stores/StoreRtcRegion;", "storeRtcRegion", "Lcom/discord/stores/StoreRtcRegion;", "Lcom/discord/utilities/collections/ListenerCollection;", "Lcom/discord/stores/StoreRtcConnection$Listener;", "listeners", "Lcom/discord/utilities/collections/ListenerCollection;", "getListeners", "()Lcom/discord/utilities/collections/ListenerCollection;", "loggingTag", "Lcom/discord/utilities/collections/ListenerCollectionSubject;", "listenerSubject", "Lcom/discord/utilities/collections/ListenerCollectionSubject;", "Landroid/content/Context;", "connectionState", "Lrx/Observable;", "getConnectionState", "getQuality", "rtcConnectionMetadata", "Lcom/discord/rtcconnection/RtcConnection$Metadata;", "Lcom/discord/api/voice/server/VoiceServer;", "selectedVoiceChannel", "Lcom/discord/api/channel/Channel;", "", "speakingUpdates", "getSpeakingUpdates", "Lcom/discord/stores/StoreStream;", "stream", "Lcom/discord/stores/StoreStream;", "Lcom/discord/stores/VoicePropsTracker;", "voicePropsTracker", "Lcom/discord/stores/VoicePropsTracker;", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreRtcRegion;Lcom/discord/stores/StoreAnalytics;Lcom/discord/stores/StoreVoiceStates;Lcom/discord/utilities/logging/Logger;Lcom/discord/utilities/debug/DebugPrintableCollection;)V", "Companion", "DefaultListener", "Listener", "SpeakingUserUpdate", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreRtcConnection extends RtcConnection.b implements DebugPrintable {
    public static final Companion Companion = new Companion(null);
    private static final long SPEAKING_UPDATES_BUFFER_MS = 300;
    private static int instanceCounter;
    private final Clock clock;
    private final Observable<RtcConnection.StateChange> connectionState;
    private final SerializedSubject<RtcConnection.StateChange, RtcConnection.StateChange> connectionStateSubject;
    private Context context;
    private OutgoingPayload.VoiceStateUpdate currentVoiceState;
    private final long debugDisplayId;
    private final Dispatcher dispatcher;
    private final DebugPrintableCollection dpc;
    private boolean hasSelectedVoiceChannel;
    private Long joinedChannelTimestamp;
    private final ListenerCollectionSubject<Listener> listenerSubject;
    private final ListenerCollection<Listener> listeners;
    private final Logger logger;
    private final String loggingTag;
    private NetworkMonitor networkMonitor;
    private final Observable<RtcConnection.Quality> quality;
    private final BehaviorSubject<RtcConnection.Quality> qualitySubject;
    private RtcConnection rtcConnection;
    private RtcConnection.Metadata rtcConnectionMetadata;
    private final BehaviorSubject<RtcConnection.Metadata> rtcConnectionMetadataSubject;
    private Channel selectedVoiceChannel;
    private String sessionId;
    private final Observable<List<SpeakingUserUpdate>> speakingUpdates;
    private final PublishSubject<SpeakingUserUpdate> speakingUsersSubject;
    private final StoreAnalytics storeAnalytics;
    private final StoreRtcRegion storeRtcRegion;
    private final StoreVoiceStates storeVoiceStates;
    private final StoreStream stream;
    private VoicePropsTracker voicePropsTracker;
    private VoiceServer voiceServer;

    /* compiled from: StoreRtcConnection.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00058\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/stores/StoreRtcConnection$Companion;", "", "", "SPEAKING_UPDATES_BUFFER_MS", "J", "", "instanceCounter", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreRtcConnection.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\b&\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\f\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0004J\u000f\u0010\u0006\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0006\u0010\u0004J\u001b\u0010\n\u001a\u00020\u00022\n\u0010\t\u001a\u00060\u0007j\u0002`\bH\u0016¢\u0006\u0004\b\n\u0010\u000b¨\u0006\r"}, d2 = {"Lcom/discord/stores/StoreRtcConnection$DefaultListener;", "Lcom/discord/stores/StoreRtcConnection$Listener;", "", "onConnecting", "()V", "onConnected", "onFirstFrameSent", "", "Lcom/discord/primitives/SSRC;", "ssrc", "onFirstFrameReceived", "(J)V", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class DefaultListener implements Listener {
        @Override // com.discord.stores.StoreRtcConnection.Listener
        public void onConnected() {
        }

        @Override // com.discord.stores.StoreRtcConnection.Listener
        public void onConnecting() {
        }

        @Override // com.discord.stores.StoreRtcConnection.Listener
        public void onFirstFrameReceived(long j) {
        }

        @Override // com.discord.stores.StoreRtcConnection.Listener
        public void onFirstFrameSent() {
        }
    }

    /* compiled from: StoreRtcConnection.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001J\u000f\u0010\u0003\u001a\u00020\u0002H&¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H&¢\u0006\u0004\b\u0005\u0010\u0004¨\u0006\u0006"}, d2 = {"Lcom/discord/stores/StoreRtcConnection$Listener;", "", "", "onConnecting", "()V", "onConnected", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public interface Listener {
        void onConnected();

        void onConnecting();

        /* synthetic */ void onFirstFrameReceived(long j);

        /* synthetic */ void onFirstFrameSent();
    }

    /* compiled from: StoreRtcConnection.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B\u001b\u0012\n\u0010\t\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\n\u001a\u00020\u0006¢\u0006\u0004\b\u0019\u0010\u001aJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ(\u0010\u000b\u001a\u00020\u00002\f\b\u0002\u0010\t\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\n\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0014\u001a\u00020\u00062\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\n\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0016\u001a\u0004\b\n\u0010\bR\u001d\u0010\t\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0005¨\u0006\u001b"}, d2 = {"Lcom/discord/stores/StoreRtcConnection$SpeakingUserUpdate;", "", "", "Lcom/discord/primitives/UserId;", "component1", "()J", "", "component2", "()Z", "userId", "isSpeaking", "copy", "(JZ)Lcom/discord/stores/StoreRtcConnection$SpeakingUserUpdate;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "J", "getUserId", HookHelper.constructorName, "(JZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class SpeakingUserUpdate {
        private final boolean isSpeaking;
        private final long userId;

        public SpeakingUserUpdate(long j, boolean z2) {
            this.userId = j;
            this.isSpeaking = z2;
        }

        public static /* synthetic */ SpeakingUserUpdate copy$default(SpeakingUserUpdate speakingUserUpdate, long j, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                j = speakingUserUpdate.userId;
            }
            if ((i & 2) != 0) {
                z2 = speakingUserUpdate.isSpeaking;
            }
            return speakingUserUpdate.copy(j, z2);
        }

        public final long component1() {
            return this.userId;
        }

        public final boolean component2() {
            return this.isSpeaking;
        }

        public final SpeakingUserUpdate copy(long j, boolean z2) {
            return new SpeakingUserUpdate(j, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof SpeakingUserUpdate)) {
                return false;
            }
            SpeakingUserUpdate speakingUserUpdate = (SpeakingUserUpdate) obj;
            return this.userId == speakingUserUpdate.userId && this.isSpeaking == speakingUserUpdate.isSpeaking;
        }

        public final long getUserId() {
            return this.userId;
        }

        public int hashCode() {
            int a = b.a(this.userId) * 31;
            boolean z2 = this.isSpeaking;
            if (z2) {
                z2 = true;
            }
            int i = z2 ? 1 : 0;
            int i2 = z2 ? 1 : 0;
            return a + i;
        }

        public final boolean isSpeaking() {
            return this.isSpeaking;
        }

        public String toString() {
            StringBuilder R = a.R("SpeakingUserUpdate(userId=");
            R.append(this.userId);
            R.append(", isSpeaking=");
            return a.M(R, this.isSpeaking, ")");
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            RtcConnection.AnalyticsEvent.values();
            int[] iArr = new int[5];
            $EnumSwitchMapping$0 = iArr;
            iArr[RtcConnection.AnalyticsEvent.VOICE_CONNECTION_SUCCESS.ordinal()] = 1;
            iArr[RtcConnection.AnalyticsEvent.VOICE_CONNECTION_FAILURE.ordinal()] = 2;
            iArr[RtcConnection.AnalyticsEvent.VOICE_DISCONNECT.ordinal()] = 3;
            iArr[RtcConnection.AnalyticsEvent.VIDEO_STREAM_ENDED.ordinal()] = 4;
            iArr[RtcConnection.AnalyticsEvent.MEDIA_SESSION_JOINED.ordinal()] = 5;
        }
    }

    public /* synthetic */ StoreRtcConnection(StoreStream storeStream, Dispatcher dispatcher, Clock clock, StoreRtcRegion storeRtcRegion, StoreAnalytics storeAnalytics, StoreVoiceStates storeVoiceStates, Logger logger, DebugPrintableCollection debugPrintableCollection, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(storeStream, dispatcher, clock, storeRtcRegion, storeAnalytics, storeVoiceStates, (i & 64) != 0 ? AppLog.g : logger, (i & 128) != 0 ? SystemLogUtils.INSTANCE.getDebugPrintables$app_productionGoogleRelease() : debugPrintableCollection);
    }

    private final void applyVoiceConfiguration(RtcConnection rtcConnection, Long l) {
        StoreMediaSettings.VoiceConfiguration voiceConfigurationBlocking = this.stream.getMediaSettings$app_productionGoogleRelease().getVoiceConfigurationBlocking();
        for (Map.Entry<Long, Float> entry : voiceConfigurationBlocking.getUserOutputVolumes().entrySet()) {
            long longValue = entry.getKey().longValue();
            float floatValue = entry.getValue().floatValue();
            if (l == null || longValue == l.longValue()) {
                rtcConnection.v(longValue, floatValue);
            }
        }
        for (Map.Entry<Long, Boolean> entry2 : voiceConfigurationBlocking.getMutedUsers().entrySet()) {
            long longValue2 = entry2.getKey().longValue();
            boolean booleanValue = entry2.getValue().booleanValue();
            if (l == null || longValue2 == l.longValue()) {
                rtcConnection.Y.put(Long.valueOf(longValue2), Boolean.valueOf(booleanValue));
                MediaEngineConnection mediaEngineConnection = rtcConnection.f2750x;
                if (mediaEngineConnection != null) {
                    mediaEngineConnection.w(longValue2, booleanValue);
                }
            }
        }
        for (Map.Entry<Long, Boolean> entry3 : voiceConfigurationBlocking.getOffScreenUsers().entrySet()) {
            long longValue3 = entry3.getKey().longValue();
            boolean booleanValue2 = entry3.getValue().booleanValue();
            if (l == null || longValue3 == l.longValue()) {
                MediaEngineConnection mediaEngineConnection2 = rtcConnection.f2750x;
                if (mediaEngineConnection2 == null) {
                    Log.e("RtcConnection", "MediaEngine not connected for setLocalVideoOffscreen.");
                } else {
                    mediaEngineConnection2.d(longValue3, booleanValue2);
                }
            }
        }
    }

    public static /* synthetic */ void applyVoiceConfiguration$default(StoreRtcConnection storeRtcConnection, RtcConnection rtcConnection, Long l, int i, Object obj) {
        if ((i & 2) != 0) {
            l = null;
        }
        storeRtcConnection.applyVoiceConfiguration(rtcConnection, l);
    }

    @StoreThread
    private final void checkForVoiceServerUpdate() {
        String str;
        RtcConnection rtcConnection = this.rtcConnection;
        VoiceServer voiceServer = this.voiceServer;
        if (rtcConnection != null && voiceServer != null) {
            if (voiceServer.c() == null || !(!m.areEqual(voiceServer.c(), rtcConnection.O))) {
                if (voiceServer.a() != null) {
                    Long a = voiceServer.a();
                    long j = rtcConnection.P;
                    if (a == null || a.longValue() != j) {
                        return;
                    }
                }
                Objects.requireNonNull(App.Companion);
                SSLSocketFactory sSLSocketFactory = null;
                if (App.access$getIS_LOCAL$cp()) {
                    str = "";
                } else {
                    str = voiceServer.b();
                    sSLSocketFactory = SecureSocketsLayerUtils.createSocketFactory$default(null, 1, null);
                }
                recordBreadcrumb(a.v("Voice server update, connect to server w/ endpoint: ", str));
                rtcConnection.s(new w(rtcConnection, str, voiceServer.d(), sSLSocketFactory));
            }
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:13:0x0036  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0040  */
    @com.discord.stores.StoreThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void createRtcConnection() {
        /*
            Method dump skipped, instructions count: 294
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreRtcConnection.createRtcConnection():void");
    }

    @StoreThread
    private final void destroyRtcConnection(String str) {
        RtcConnection rtcConnection = this.rtcConnection;
        if (rtcConnection != null) {
            recordBreadcrumb(a.v("destroying rtc connection: ", str));
            rtcConnection.e();
            this.rtcConnection = null;
            updateMetadata();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleMediaSessionIdReceived() {
        AppLog appLog = AppLog.g;
        RtcConnection rtcConnection = this.rtcConnection;
        String str = rtcConnection != null ? rtcConnection.K : null;
        Objects.requireNonNull(appLog);
        FirebaseCrashlytics firebaseCrashlytics = FirebaseCrashlytics.getInstance();
        if (str == null) {
            str = "";
        }
        firebaseCrashlytics.setCustomKey("mediaSessionId", str);
        updateMetadata();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleSelfDeafened(boolean z2) {
        this.currentVoiceState = OutgoingPayload.VoiceStateUpdate.copy$default(this.currentVoiceState, null, null, false, z2, false, null, 55, null);
        onVoiceStateUpdated();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleSelfMuted(boolean z2) {
        this.currentVoiceState = OutgoingPayload.VoiceStateUpdate.copy$default(this.currentVoiceState, null, null, z2, false, false, null, 59, null);
        onVoiceStateUpdated();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleSelfVideo(boolean z2) {
        this.currentVoiceState = OutgoingPayload.VoiceStateUpdate.copy$default(this.currentVoiceState, null, null, false, false, z2, null, 47, null);
        onVoiceStateUpdated();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleUsersMuted(Map<Long, Boolean> map) {
        for (Map.Entry<Long, Boolean> entry : map.entrySet()) {
            long longValue = entry.getKey().longValue();
            boolean booleanValue = entry.getValue().booleanValue();
            RtcConnection rtcConnection = this.rtcConnection;
            if (rtcConnection != null) {
                rtcConnection.Y.put(Long.valueOf(longValue), Boolean.valueOf(booleanValue));
                MediaEngineConnection mediaEngineConnection = rtcConnection.f2750x;
                if (mediaEngineConnection != null) {
                    mediaEngineConnection.w(longValue, booleanValue);
                }
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleUsersOffScreen(Map<Long, Boolean> map) {
        for (Map.Entry<Long, Boolean> entry : map.entrySet()) {
            long longValue = entry.getKey().longValue();
            boolean booleanValue = entry.getValue().booleanValue();
            RtcConnection rtcConnection = this.rtcConnection;
            if (rtcConnection != null) {
                MediaEngineConnection mediaEngineConnection = rtcConnection.f2750x;
                if (mediaEngineConnection == null) {
                    Log.e("RtcConnection", "MediaEngine not connected for setLocalVideoOffscreen.");
                } else {
                    mediaEngineConnection.d(longValue, booleanValue);
                }
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleUsersVolume(Map<Long, Float> map) {
        for (Map.Entry<Long, Float> entry : map.entrySet()) {
            long longValue = entry.getKey().longValue();
            float floatValue = entry.getValue().floatValue();
            RtcConnection rtcConnection = this.rtcConnection;
            if (rtcConnection != null) {
                rtcConnection.v(longValue, floatValue);
            }
        }
    }

    @StoreThread
    private final void logChannelJoin(Channel channel) {
        String str;
        long id2 = this.stream.getUsers$app_productionGoogleRelease().getMeInternal$app_productionGoogleRelease().getId();
        RtcConnection rtcConnection = this.rtcConnection;
        if (rtcConnection != null && (str = rtcConnection.m) != null) {
            StageInstance stageInstanceForChannel = this.stream.getStageInstances$app_productionGoogleRelease().getStageInstanceForChannel(channel.h());
            Long valueOf = stageInstanceForChannel != null ? Long.valueOf(stageInstanceForChannel.c()) : null;
            GuildScheduledEvent activeEventForChannel = this.stream.getGuildScheduledEvents$app_productionGoogleRelease().getActiveEventForChannel(Long.valueOf(channel.f()), Long.valueOf(channel.h()));
            Long valueOf2 = activeEventForChannel != null ? Long.valueOf(activeEventForChannel.i()) : null;
            AnalyticsTracker analyticsTracker = AnalyticsTracker.INSTANCE;
            Map<Long, VoiceState> map = (Map) a.u0(channel, this.stream.getVoiceStates$app_productionGoogleRelease().get());
            if (map == null) {
                map = h0.emptyMap();
            }
            Map<Long, VoiceState> map2 = map;
            VideoInputDeviceDescription selectedVideoInputDeviceBlocking = this.stream.getMediaEngine$app_productionGoogleRelease().getSelectedVideoInputDeviceBlocking();
            NetworkUtils networkUtils = NetworkUtils.INSTANCE;
            Context context = this.context;
            if (context == null) {
                m.throwUninitializedPropertyAccessException("context");
            }
            analyticsTracker.voiceChannelJoin(id2, str, channel, map2, selectedVideoInputDeviceBlocking, networkUtils.getNetworkType(context), valueOf, valueOf2);
        }
    }

    @StoreThread
    private final void logChannelLeave(Channel channel) {
        String str;
        long id2 = this.stream.getUsers$app_productionGoogleRelease().getMeInternal$app_productionGoogleRelease().getId();
        RtcConnection rtcConnection = this.rtcConnection;
        if (rtcConnection != null && (str = rtcConnection.m) != null) {
            Long l = null;
            String str2 = rtcConnection != null ? rtcConnection.K : null;
            StageInstance stageInstanceForChannel = this.stream.getStageInstances$app_productionGoogleRelease().getStageInstanceForChannel(channel.h());
            Long valueOf = stageInstanceForChannel != null ? Long.valueOf(stageInstanceForChannel.c()) : null;
            GuildScheduledEvent activeEventForChannel = this.stream.getGuildScheduledEvents$app_productionGoogleRelease().getActiveEventForChannel(Long.valueOf(channel.f()), Long.valueOf(channel.h()));
            Long valueOf2 = activeEventForChannel != null ? Long.valueOf(activeEventForChannel.i()) : null;
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            VoicePropsTracker voicePropsTracker = this.voicePropsTracker;
            if (voicePropsTracker != null) {
                voicePropsTracker.getProps(linkedHashMap);
            }
            this.voicePropsTracker = null;
            AnalyticsTracker analyticsTracker = AnalyticsTracker.INSTANCE;
            Map<Long, VoiceState> map = (Map) a.u0(channel, this.stream.getVoiceStates$app_productionGoogleRelease().get());
            if (map == null) {
                map = h0.emptyMap();
            }
            Map<Long, VoiceState> map2 = map;
            Long l2 = this.joinedChannelTimestamp;
            if (l2 != null) {
                l = Long.valueOf(this.clock.currentTimeMillis() - l2.longValue());
            }
            analyticsTracker.voiceChannelLeave(id2, str, channel, map2, str2, linkedHashMap, l, valueOf, valueOf2);
        }
    }

    private final void loge(String str, Throwable th, Map<String, String> map) {
        this.logger.e(this.loggingTag, str, th, map);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void loge$default(StoreRtcConnection storeRtcConnection, String str, Throwable th, Map map, int i, Object obj) {
        if ((i & 2) != 0) {
            th = null;
        }
        if ((i & 4) != 0) {
            map = null;
        }
        storeRtcConnection.loge(str, th, map);
    }

    private final void logi(String str, Throwable th) {
        this.logger.i(this.loggingTag, str, th);
    }

    public static /* synthetic */ void logi$default(StoreRtcConnection storeRtcConnection, String str, Throwable th, int i, Object obj) {
        if ((i & 2) != 0) {
            th = null;
        }
        storeRtcConnection.logi(str, th);
    }

    private final void logw(String str) {
        Logger.w$default(this.logger, this.loggingTag, str, null, 4, null);
    }

    @StoreThread
    private final void onVoiceStateUpdated() {
        String str;
        if (this.hasSelectedVoiceChannel) {
            StringBuilder R = a.R("Voice state update: ");
            R.append(this.currentVoiceState);
            recordBreadcrumb(R.toString());
        }
        StoreGatewayConnection gatewaySocket = StoreStream.Companion.getGatewaySocket();
        Long guildId = this.currentVoiceState.getGuildId();
        Long channelId = this.currentVoiceState.getChannelId();
        boolean selfMute = this.currentVoiceState.getSelfMute();
        boolean selfDeaf = this.currentVoiceState.getSelfDeaf();
        boolean selfVideo = this.currentVoiceState.getSelfVideo();
        Long channelId2 = this.currentVoiceState.getChannelId();
        if (channelId2 != null) {
            channelId2.longValue();
            str = this.storeRtcRegion.getPreferredRegion();
        } else {
            str = null;
        }
        gatewaySocket.voiceStateUpdate(guildId, channelId, selfMute, selfDeaf, selfVideo, str, this.storeRtcRegion.shouldIncludePreferredRegion(this.currentVoiceState.getGuildId()));
    }

    private final void recordBreadcrumb(String str) {
        this.logger.recordBreadcrumb(str, this.loggingTag);
    }

    @StoreThread
    private final void updateMetadata() {
        RtcConnection rtcConnection = this.rtcConnection;
        RtcConnection.Metadata i = rtcConnection != null ? rtcConnection.i() : null;
        this.rtcConnectionMetadata = i;
        this.rtcConnectionMetadataSubject.onNext(i);
    }

    @Override // com.discord.utilities.debug.DebugPrintable
    public void debugPrint(DebugPrintBuilder debugPrintBuilder) {
        m.checkNotNullParameter(debugPrintBuilder, "dp");
        debugPrintBuilder.appendKeyValue("sessionId", this.sessionId);
        debugPrintBuilder.appendKeyValue("rtcConnection", (DebugPrintable) this.rtcConnection);
    }

    public final void finalize() {
        this.dpc.remove(this.debugDisplayId);
    }

    public final long getConnectedGuildId() {
        Long l;
        RtcConnection rtcConnection = this.rtcConnection;
        if (rtcConnection == null || (l = rtcConnection.O) == null) {
            return 0L;
        }
        return l.longValue();
    }

    public final Observable<RtcConnection.StateChange> getConnectionState() {
        return this.connectionState;
    }

    public final ListenerCollection<Listener> getListeners() {
        return this.listeners;
    }

    public final Observable<RtcConnection.Quality> getQuality() {
        return this.quality;
    }

    public final RtcConnection getRtcConnection$app_productionGoogleRelease() {
        return this.rtcConnection;
    }

    @StoreThread
    public final RtcConnection.Metadata getRtcConnectionMetadata() {
        return this.rtcConnectionMetadata;
    }

    public final Observable<List<SpeakingUserUpdate>> getSpeakingUpdates() {
        return this.speakingUpdates;
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        this.sessionId = modelPayload.getSessionId();
        RtcConnection rtcConnection = this.rtcConnection;
        if (rtcConnection != null) {
            String sessionId = modelPayload.getSessionId();
            m.checkNotNullExpressionValue(sessionId, "payload.sessionId");
            m.checkNotNullParameter(sessionId, "<set-?>");
            rtcConnection.Q = sessionId;
        }
    }

    @StoreThread
    public final void handleConnectionReady(boolean z2) {
        if (z2) {
            Channel channel = this.selectedVoiceChannel;
            handleVoiceChannelSelected(Long.valueOf(channel != null ? channel.h() : 0L));
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:29:0x007e  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x0088  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x0098  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x00ad  */
    @com.discord.stores.StoreThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void handleVoiceChannelSelected(java.lang.Long r13) {
        /*
            r12 = this;
            com.discord.api.channel.Channel r0 = r12.selectedVoiceChannel
            r1 = 0
            if (r0 == 0) goto Le
            long r2 = r0.h()
            java.lang.Long r0 = java.lang.Long.valueOf(r2)
            goto Lf
        Le:
            r0 = r1
        Lf:
            boolean r0 = d0.z.d.m.areEqual(r13, r0)
            r2 = 1
            r0 = r0 ^ r2
            if (r0 == 0) goto L4a
            com.discord.api.channel.Channel r0 = r12.selectedVoiceChannel
            if (r0 == 0) goto L1e
            r12.logChannelLeave(r0)
        L1e:
            r12.joinedChannelTimestamp = r1
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r3 = "Channel ID changed, "
            r0.append(r3)
            r0.append(r13)
            java.lang.String r3 = " != "
            r0.append(r3)
            com.discord.api.channel.Channel r3 = r12.selectedVoiceChannel
            if (r3 == 0) goto L3f
            long r3 = r3.h()
            java.lang.Long r3 = java.lang.Long.valueOf(r3)
            goto L40
        L3f:
            r3 = r1
        L40:
            r0.append(r3)
            java.lang.String r0 = r0.toString()
            r12.destroyRtcConnection(r0)
        L4a:
            if (r13 == 0) goto L5b
            com.discord.stores.StoreStream r0 = r12.stream
            com.discord.stores.StoreChannels r0 = r0.getChannels$app_productionGoogleRelease()
            long r3 = r13.longValue()
            com.discord.api.channel.Channel r13 = r0.findChannelByIdInternal$app_productionGoogleRelease(r3)
            goto L5c
        L5b:
            r13 = r1
        L5c:
            r12.selectedVoiceChannel = r13
            com.discord.gateway.io.OutgoingPayload$VoiceStateUpdate r3 = r12.currentVoiceState
            if (r13 == 0) goto L7b
            long r4 = r13.f()
            java.lang.Long r0 = java.lang.Long.valueOf(r4)
            long r4 = r0.longValue()
            r6 = 0
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 <= 0) goto L76
            r4 = 1
            goto L77
        L76:
            r4 = 0
        L77:
            if (r4 == 0) goto L7b
            r4 = r0
            goto L7c
        L7b:
            r4 = r1
        L7c:
            if (r13 == 0) goto L88
            long r5 = r13.h()
            java.lang.Long r0 = java.lang.Long.valueOf(r5)
            r5 = r0
            goto L89
        L88:
            r5 = r1
        L89:
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 60
            r11 = 0
            com.discord.gateway.io.OutgoingPayload$VoiceStateUpdate r0 = com.discord.gateway.io.OutgoingPayload.VoiceStateUpdate.copy$default(r3, r4, r5, r6, r7, r8, r9, r10, r11)
            r12.currentVoiceState = r0
            if (r13 == 0) goto Lad
            r12.hasSelectedVoiceChannel = r2
            r12.createRtcConnection()
            com.discord.utilities.time.Clock r0 = r12.clock
            long r0 = r0.currentTimeMillis()
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r12.joinedChannelTimestamp = r0
            r12.logChannelJoin(r13)
            goto Laf
        Lad:
            r12.voiceServer = r1
        Laf:
            r12.onVoiceStateUpdated()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreRtcConnection.handleVoiceChannelSelected(java.lang.Long):void");
    }

    @StoreThread
    public final void handleVoiceServerUpdate(VoiceServer voiceServer) {
        m.checkNotNullParameter(voiceServer, "voiceServer");
        recordBreadcrumb("handling voice server update: " + voiceServer);
        this.voiceServer = voiceServer;
        checkForVoiceServerUpdate();
    }

    @StoreThread
    public final void handleVoiceStateUpdate(VoiceState voiceState) {
        m.checkNotNullParameter(voiceState, "voiceState");
        VoicePropsTracker voicePropsTracker = this.voicePropsTracker;
        if (voicePropsTracker != null) {
            voicePropsTracker.handleVoiceStateUpdate(voiceState);
        }
    }

    @StoreThread
    public final void init(Context context, NetworkMonitor networkMonitor) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(networkMonitor, "networkMonitor");
        this.context = context;
        this.networkMonitor = networkMonitor;
        StoreStream.Companion companion = StoreStream.Companion;
        ObservableExtensionsKt.appSubscribe$default(companion.getMediaSettings().isSelfMuted(), StoreRtcConnection.class, (Context) null, (Function1) null, (Function1) null, (Function0) null, (Function0) null, new StoreRtcConnection$init$1(this), 62, (Object) null);
        ObservableExtensionsKt.appSubscribe$default(companion.getMediaSettings().isSelfDeafened(), StoreRtcConnection.class, (Context) null, (Function1) null, (Function1) null, (Function0) null, (Function0) null, new StoreRtcConnection$init$2(this), 62, (Object) null);
        Observable<R> F = companion.getMediaEngine().getSelectedVideoInputDevice().F(StoreRtcConnection$init$3.INSTANCE);
        m.checkNotNullExpressionValue(F, "StoreStream\n        .get…viceDescription != null }");
        ObservableExtensionsKt.appSubscribe$default(F, StoreRtcConnection.class, (Context) null, (Function1) null, (Function1) null, (Function0) null, (Function0) null, new StoreRtcConnection$init$4(this), 62, (Object) null);
        ObservableExtensionsKt.appSubscribe$default(this.stream.getMediaSettings$app_productionGoogleRelease().getUsersVolume(), StoreRtcConnection.class, (Context) null, (Function1) null, (Function1) null, (Function0) null, (Function0) null, new StoreRtcConnection$init$5(this), 62, (Object) null);
        ObservableExtensionsKt.appSubscribe$default(this.stream.getMediaSettings$app_productionGoogleRelease().getUsersMuted(), StoreRtcConnection.class, (Context) null, (Function1) null, (Function1) null, (Function0) null, (Function0) null, new StoreRtcConnection$init$6(this), 62, (Object) null);
        ObservableExtensionsKt.appSubscribe$default(this.stream.getMediaSettings$app_productionGoogleRelease().getUsersOffScreen(), StoreRtcConnection.class, (Context) null, (Function1) null, (Function1) null, (Function0) null, (Function0) null, new StoreRtcConnection$init$7(this), 62, (Object) null);
    }

    public final Observable<RtcConnection.Metadata> observeRtcConnectionMetadata() {
        BehaviorSubject<RtcConnection.Metadata> behaviorSubject = this.rtcConnectionMetadataSubject;
        m.checkNotNullExpressionValue(behaviorSubject, "rtcConnectionMetadataSubject");
        return behaviorSubject;
    }

    @Override // com.discord.rtcconnection.RtcConnection.b, com.discord.rtcconnection.RtcConnection.c
    public void onAnalyticsEvent(RtcConnection.AnalyticsEvent analyticsEvent, Map<String, Object> map) {
        m.checkNotNullParameter(analyticsEvent, "event");
        m.checkNotNullParameter(map, "properties");
        int ordinal = analyticsEvent.ordinal();
        if (ordinal == 0) {
            this.storeAnalytics.trackVoiceConnectionSuccess(map);
        } else if (ordinal == 1) {
            this.storeAnalytics.trackVoiceConnectionFailure(map);
        } else if (ordinal == 2) {
            this.storeAnalytics.trackVoiceDisconnect(map);
        } else if (ordinal == 3) {
            this.dispatcher.schedule(new StoreRtcConnection$onAnalyticsEvent$1(this, map));
        } else if (ordinal == 4) {
            this.storeAnalytics.trackMediaSessionJoined(map);
        }
    }

    @Override // com.discord.rtcconnection.RtcConnection.b, com.discord.rtcconnection.RtcConnection.c
    public void onFatalClose() {
        this.stream.getVoiceChannelSelected$app_productionGoogleRelease().clear();
    }

    @Override // com.discord.rtcconnection.RtcConnection.b
    public void onFirstFrameReceived(long j) {
        this.listenerSubject.notify(new StoreRtcConnection$onFirstFrameReceived$1(j));
    }

    @Override // com.discord.rtcconnection.RtcConnection.b
    public void onFirstFrameSent() {
        this.listenerSubject.notify(StoreRtcConnection$onFirstFrameSent$1.INSTANCE);
    }

    @Override // com.discord.rtcconnection.RtcConnection.b, com.discord.rtcconnection.RtcConnection.c
    public void onMediaEngineConnectionConnected(RtcConnection rtcConnection) {
        m.checkNotNullParameter(rtcConnection, "connection");
        this.dispatcher.schedule(new StoreRtcConnection$onMediaEngineConnectionConnected$1(this, rtcConnection));
    }

    @Override // com.discord.rtcconnection.RtcConnection.b, com.discord.rtcconnection.RtcConnection.c
    public void onMediaSessionIdReceived() {
        this.dispatcher.schedule(new StoreRtcConnection$onMediaSessionIdReceived$1(this));
    }

    @Override // com.discord.rtcconnection.RtcConnection.b, com.discord.rtcconnection.RtcConnection.c
    public void onQualityUpdate(RtcConnection.Quality quality) {
        m.checkNotNullParameter(quality, "quality");
        this.qualitySubject.onNext(quality);
    }

    @Override // com.discord.rtcconnection.RtcConnection.b, com.discord.rtcconnection.RtcConnection.c
    public void onSpeaking(long j, boolean z2) {
        PublishSubject<SpeakingUserUpdate> publishSubject = this.speakingUsersSubject;
        publishSubject.k.onNext(new SpeakingUserUpdate(j, z2));
        this.dispatcher.schedule(new StoreRtcConnection$onSpeaking$1(this, j, z2));
    }

    @Override // com.discord.rtcconnection.RtcConnection.b, com.discord.rtcconnection.RtcConnection.c
    public void onStateChange(RtcConnection.StateChange stateChange) {
        m.checkNotNullParameter(stateChange, "stateChange");
        recordBreadcrumb("connection state change: " + stateChange);
        this.connectionStateSubject.k.onNext(stateChange);
        if (m.areEqual(stateChange.a, RtcConnection.State.f.a)) {
            this.listenerSubject.notify(StoreRtcConnection$onStateChange$1.INSTANCE);
        }
    }

    @Override // com.discord.rtcconnection.RtcConnection.b, com.discord.rtcconnection.RtcConnection.c
    public void onUserCreated(RtcConnection rtcConnection, long j) {
        m.checkNotNullParameter(rtcConnection, "connection");
        applyVoiceConfiguration(rtcConnection, Long.valueOf(j));
    }

    @Override // com.discord.rtcconnection.RtcConnection.b, com.discord.rtcconnection.RtcConnection.c
    public void onVideoMetadata(VideoMetadata videoMetadata) {
        m.checkNotNullParameter(videoMetadata, "metadata");
        this.dispatcher.schedule(new StoreRtcConnection$onVideoMetadata$1(this, videoMetadata));
    }

    @Override // com.discord.rtcconnection.RtcConnection.b, com.discord.rtcconnection.RtcConnection.c
    public void onVideoStream(long j, Integer num, int i, int i2, int i3) {
        this.dispatcher.schedule(new StoreRtcConnection$onVideoStream$1(this, j, num, i, i2, i3));
    }

    public StoreRtcConnection(StoreStream storeStream, Dispatcher dispatcher, Clock clock, StoreRtcRegion storeRtcRegion, StoreAnalytics storeAnalytics, StoreVoiceStates storeVoiceStates, Logger logger, DebugPrintableCollection debugPrintableCollection) {
        m.checkNotNullParameter(storeStream, "stream");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(storeRtcRegion, "storeRtcRegion");
        m.checkNotNullParameter(storeAnalytics, "storeAnalytics");
        m.checkNotNullParameter(storeVoiceStates, "storeVoiceStates");
        m.checkNotNullParameter(logger, "logger");
        m.checkNotNullParameter(debugPrintableCollection, "dpc");
        this.stream = storeStream;
        this.dispatcher = dispatcher;
        this.clock = clock;
        this.storeRtcRegion = storeRtcRegion;
        this.storeAnalytics = storeAnalytics;
        this.storeVoiceStates = storeVoiceStates;
        this.logger = logger;
        this.dpc = debugPrintableCollection;
        ListenerCollectionSubject<Listener> listenerCollectionSubject = new ListenerCollectionSubject<>();
        this.listenerSubject = listenerCollectionSubject;
        this.listeners = listenerCollectionSubject;
        PublishSubject<SpeakingUserUpdate> k0 = PublishSubject.k0();
        this.speakingUsersSubject = k0;
        BehaviorSubject<RtcConnection.Quality> l0 = BehaviorSubject.l0(RtcConnection.Quality.UNKNOWN);
        this.qualitySubject = l0;
        this.currentVoiceState = new OutgoingPayload.VoiceStateUpdate(null, null, false, false, false, null, 32, null);
        SerializedSubject<RtcConnection.StateChange, RtcConnection.StateChange> serializedSubject = new SerializedSubject<>(BehaviorSubject.l0(new RtcConnection.StateChange(new RtcConnection.State.d(false), null)));
        this.connectionStateSubject = serializedSubject;
        this.rtcConnectionMetadataSubject = BehaviorSubject.l0(null);
        Observable<RtcConnection.StateChange> q = ObservableExtensionsKt.computationLatest(serializedSubject).q();
        m.checkNotNullExpressionValue(q, "connectionStateSubject\n …  .distinctUntilChanged()");
        this.connectionState = q;
        m.checkNotNullExpressionValue(l0, "qualitySubject");
        Observable<RtcConnection.Quality> q2 = ObservableExtensionsKt.computationLatest(l0).q();
        m.checkNotNullExpressionValue(q2, "qualitySubject\n         …  .distinctUntilChanged()");
        this.quality = q2;
        Observable<List<SpeakingUserUpdate>> a = k0.a(300L, TimeUnit.MILLISECONDS);
        m.checkNotNullExpressionValue(a, "speakingUsersSubject\n   …S, TimeUnit.MILLISECONDS)");
        Observable<List<SpeakingUserUpdate>> q3 = ObservableExtensionsKt.computationLatest(a).q();
        m.checkNotNullExpressionValue(q3, "speakingUsersSubject\n   …  .distinctUntilChanged()");
        this.speakingUpdates = q3;
        StringBuilder R = a.R("StoreRtcConnection ");
        int i = instanceCounter + 1;
        instanceCounter = i;
        R.append(i);
        String sb = R.toString();
        this.loggingTag = sb;
        this.debugDisplayId = debugPrintableCollection.add(this, sb);
    }
}
