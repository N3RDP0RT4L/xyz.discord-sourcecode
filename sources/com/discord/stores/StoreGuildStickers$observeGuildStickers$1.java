package com.discord.stores;

import com.discord.api.sticker.Sticker;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreGuildStickers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\b\u001a&\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0001j\u0002`\u0003\u0012\u0004\u0012\u00020\u00040\u0000j\u0002`\u00050\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "", "Lcom/discord/primitives/GuildId;", "Lcom/discord/primitives/StickerId;", "Lcom/discord/api/sticker/Sticker;", "Lcom/discord/stores/StickerMap;", "invoke", "()Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildStickers$observeGuildStickers$1 extends o implements Function0<Map<Long, ? extends Map<Long, ? extends Sticker>>> {
    public final /* synthetic */ StoreGuildStickers this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGuildStickers$observeGuildStickers$1(StoreGuildStickers storeGuildStickers) {
        super(0);
        this.this$0 = storeGuildStickers;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Map<Long, ? extends Map<Long, ? extends Sticker>> invoke() {
        Map<Long, ? extends Map<Long, ? extends Sticker>> map;
        map = this.this$0.allGuildStickersSnapshot;
        return map;
    }
}
