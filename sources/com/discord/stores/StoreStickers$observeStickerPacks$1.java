package com.discord.stores;

import com.discord.stores.StoreStickers;
import d0.t.u;
import d0.z.d.o;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreStickers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"", "Lcom/discord/stores/StoreStickers$StickerPackState;", "invoke", "()Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStickers$observeStickerPacks$1 extends o implements Function0<List<? extends StoreStickers.StickerPackState>> {
    public final /* synthetic */ StoreStickers this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreStickers$observeStickerPacks$1(StoreStickers storeStickers) {
        super(0);
        this.this$0 = storeStickers;
    }

    @Override // kotlin.jvm.functions.Function0
    public final List<? extends StoreStickers.StickerPackState> invoke() {
        Map map;
        map = this.this$0.stickerPacks;
        return u.toList(map.values());
    }
}
