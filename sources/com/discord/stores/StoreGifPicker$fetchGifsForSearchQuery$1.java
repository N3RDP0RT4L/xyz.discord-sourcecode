package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.models.gifpicker.dto.GifDto;
import com.discord.models.gifpicker.dto.ModelGif;
import d0.t.o;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
/* compiled from: StoreGifPicker.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0002*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00000\u00002\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "Lcom/discord/models/gifpicker/dto/GifDto;", "kotlin.jvm.PlatformType", "gifDtos", "Lcom/discord/models/gifpicker/dto/ModelGif;", NotificationCompat.CATEGORY_CALL, "(Ljava/util/List;)Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGifPicker$fetchGifsForSearchQuery$1<T, R> implements b<List<? extends GifDto>, List<? extends ModelGif>> {
    public static final StoreGifPicker$fetchGifsForSearchQuery$1 INSTANCE = new StoreGifPicker$fetchGifsForSearchQuery$1();

    @Override // j0.k.b
    public /* bridge */ /* synthetic */ List<? extends ModelGif> call(List<? extends GifDto> list) {
        return call2((List<GifDto>) list);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final List<ModelGif> call2(List<GifDto> list) {
        m.checkNotNullExpressionValue(list, "gifDtos");
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
        for (GifDto gifDto : list) {
            arrayList.add(ModelGif.Companion.createFromGifDto(gifDto));
        }
        return arrayList;
    }
}
