package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.guild.Guild;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelInvite;
import com.discord.stores.StoreInstantInvites;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.t.h0;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreInstantInvites.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001>B#\u0012\u0006\u0010.\u001a\u00020-\u0012\b\b\u0002\u00106\u001a\u000205\u0012\b\b\u0002\u00101\u001a\u000200¢\u0006\u0004\b<\u0010=J+\u0010\u0007\u001a\u001e\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00020\u0002H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u001b\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\t0\u0002H\u0002¢\u0006\u0004\b\n\u0010\bJ\u001d\u0010\u000e\u001a\u00020\r2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u000bH\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\u0012\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u00052\u0006\u0010\u0011\u001a\u00020\tH\u0003¢\u0006\u0004\b\u0012\u0010\u0013J%\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00022\n\u0010\u0014\u001a\u00060\u0003j\u0002`\u0004¢\u0006\u0004\b\u0007\u0010\u0015J\u001d\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\t0\u00162\b\u0010\u0010\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u0017\u0010\u0018J\u001f\u0010\u0019\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\t0\u00020\u0016¢\u0006\u0004\b\u0019\u0010\u001aJ\u0019\u0010\u001b\u001a\u00020\r2\n\u0010\u0014\u001a\u00060\u0003j\u0002`\u0004¢\u0006\u0004\b\u001b\u0010\u001cJQ\u0010%\u001a\u00020\r2\u0006\u0010\u001d\u001a\u00020\u00052\u0010\b\u0002\u0010\u001f\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u001e2\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010!2\u0010\b\u0002\u0010$\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010#¢\u0006\u0004\b%\u0010&J\u0015\u0010(\u001a\u00020\r2\u0006\u0010'\u001a\u00020\u0006¢\u0006\u0004\b(\u0010)J\u0019\u0010*\u001a\u00020\r2\n\u0010\u0014\u001a\u00060\u0003j\u0002`\u0004¢\u0006\u0004\b*\u0010\u001cJ\u000f\u0010+\u001a\u00020\rH\u0016¢\u0006\u0004\b+\u0010,R\u0016\u0010.\u001a\u00020-8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b.\u0010/R\u0016\u00101\u001a\u0002008\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b1\u00102R2\u00103\u001a\u001e\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b3\u00104R\u0016\u00106\u001a\u0002058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b6\u00107R\"\u00109\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\t088\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b9\u00104R\"\u0010:\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\t0\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b:\u00104R2\u0010;\u001a\u001e\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u000608088\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b;\u00104¨\u0006?"}, d2 = {"Lcom/discord/stores/StoreInstantInvites;", "Lcom/discord/stores/StoreV2;", "", "", "Lcom/discord/primitives/GuildId;", "", "Lcom/discord/models/domain/ModelInvite;", "getInvites", "()Ljava/util/Map;", "Lcom/discord/stores/StoreInstantInvites$InviteState;", "getKnownInvites", "", "updatedInvites", "", "onLoadedInvites", "(Ljava/util/List;)V", "inviteKey", "inviteState", "setChatInvites", "(Ljava/lang/String;Lcom/discord/stores/StoreInstantInvites$InviteState;)V", "guildId", "(J)Ljava/util/Map;", "Lrx/Observable;", "observeInvite", "(Ljava/lang/String;)Lrx/Observable;", "observeKnownInvites", "()Lrx/Observable;", "fetchGuildInvites", "(J)V", "inviteCode", "Lcom/discord/primitives/GuildScheduledEventId;", "eventId", ModelAuditLogEntry.CHANGE_KEY_LOCATION, "", "inviteResolved", "Lkotlin/Function0;", "onError", "fetchInviteIfNotLoaded", "(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Boolean;Lkotlin/jvm/functions/Function0;)V", "invite", "onInviteRemoved", "(Lcom/discord/models/domain/ModelInvite;)V", "clearInvites", "snapshotData", "()V", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "invitesSnapshot", "Ljava/util/Map;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "", "knownInvites", "knownInvitesSnapshot", "invites", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/utilities/rest/RestAPI;)V", "InviteState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreInstantInvites extends StoreV2 {
    private final Dispatcher dispatcher;
    private final Map<Long, Map<String, ModelInvite>> invites;
    private Map<Long, ? extends Map<String, ? extends ModelInvite>> invitesSnapshot;
    private final Map<String, InviteState> knownInvites;
    private Map<String, ? extends InviteState> knownInvitesSnapshot;
    private final ObservationDeck observationDeck;
    private final RestAPI restAPI;

    /* compiled from: StoreInstantInvites.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\u0004\u0005\u0006\u0007B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0004\b\t\n\u000b¨\u0006\f"}, d2 = {"Lcom/discord/stores/StoreInstantInvites$InviteState;", "", HookHelper.constructorName, "()V", "Invalid", "LoadFailed", "Loading", "Resolved", "Lcom/discord/stores/StoreInstantInvites$InviteState$Loading;", "Lcom/discord/stores/StoreInstantInvites$InviteState$LoadFailed;", "Lcom/discord/stores/StoreInstantInvites$InviteState$Invalid;", "Lcom/discord/stores/StoreInstantInvites$InviteState$Resolved;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class InviteState {

        /* compiled from: StoreInstantInvites.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreInstantInvites$InviteState$Invalid;", "Lcom/discord/stores/StoreInstantInvites$InviteState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Invalid extends InviteState {
            public static final Invalid INSTANCE = new Invalid();

            private Invalid() {
                super(null);
            }
        }

        /* compiled from: StoreInstantInvites.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreInstantInvites$InviteState$LoadFailed;", "Lcom/discord/stores/StoreInstantInvites$InviteState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class LoadFailed extends InviteState {
            public static final LoadFailed INSTANCE = new LoadFailed();

            private LoadFailed() {
                super(null);
            }
        }

        /* compiled from: StoreInstantInvites.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreInstantInvites$InviteState$Loading;", "Lcom/discord/stores/StoreInstantInvites$InviteState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Loading extends InviteState {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        /* compiled from: StoreInstantInvites.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/stores/StoreInstantInvites$InviteState$Resolved;", "Lcom/discord/stores/StoreInstantInvites$InviteState;", "Lcom/discord/models/domain/ModelInvite;", "component1", "()Lcom/discord/models/domain/ModelInvite;", "invite", "copy", "(Lcom/discord/models/domain/ModelInvite;)Lcom/discord/stores/StoreInstantInvites$InviteState$Resolved;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/ModelInvite;", "getInvite", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelInvite;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Resolved extends InviteState {
            private final ModelInvite invite;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Resolved(ModelInvite modelInvite) {
                super(null);
                m.checkNotNullParameter(modelInvite, "invite");
                this.invite = modelInvite;
            }

            public static /* synthetic */ Resolved copy$default(Resolved resolved, ModelInvite modelInvite, int i, Object obj) {
                if ((i & 1) != 0) {
                    modelInvite = resolved.invite;
                }
                return resolved.copy(modelInvite);
            }

            public final ModelInvite component1() {
                return this.invite;
            }

            public final Resolved copy(ModelInvite modelInvite) {
                m.checkNotNullParameter(modelInvite, "invite");
                return new Resolved(modelInvite);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Resolved) && m.areEqual(this.invite, ((Resolved) obj).invite);
                }
                return true;
            }

            public final ModelInvite getInvite() {
                return this.invite;
            }

            public int hashCode() {
                ModelInvite modelInvite = this.invite;
                if (modelInvite != null) {
                    return modelInvite.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Resolved(invite=");
                R.append(this.invite);
                R.append(")");
                return R.toString();
            }
        }

        private InviteState() {
        }

        public /* synthetic */ InviteState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            Error.Type.values();
            int[] iArr = new int[17];
            $EnumSwitchMapping$0 = iArr;
            iArr[Error.Type.DISCORD_REQUEST_ERROR.ordinal()] = 1;
            iArr[Error.Type.NETWORK.ordinal()] = 2;
        }
    }

    public /* synthetic */ StoreInstantInvites(Dispatcher dispatcher, ObservationDeck observationDeck, RestAPI restAPI, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(dispatcher, (i & 2) != 0 ? ObservationDeckProvider.get() : observationDeck, (i & 4) != 0 ? RestAPI.Companion.getApi() : restAPI);
    }

    private final Map<Long, Map<String, ModelInvite>> getInvites() {
        return this.invitesSnapshot;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Map<String, InviteState> getKnownInvites() {
        return this.knownInvitesSnapshot;
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void onLoadedInvites(List<? extends ModelInvite> list) {
        this.invites.clear();
        for (ModelInvite modelInvite : list) {
            Guild guild = modelInvite.guild;
            Long valueOf = guild != null ? Long.valueOf(guild.r()) : null;
            if (valueOf != null) {
                Map<String, ModelInvite> map = this.invites.get(valueOf);
                if (map == null) {
                    map = new LinkedHashMap<>();
                }
                String str = modelInvite.code;
                m.checkNotNullExpressionValue(str, "invite.code");
                map.put(str, modelInvite);
                this.invites.put(valueOf, map);
            }
        }
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void setChatInvites(String str, InviteState inviteState) {
        this.knownInvites.put(str, inviteState);
        markChanged();
    }

    public final void clearInvites(long j) {
        this.dispatcher.schedule(new StoreInstantInvites$clearInvites$1(this, j));
    }

    public final void fetchGuildInvites(long j) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.getGuildInvites(j), false, 1, null), StoreInstantInvites.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreInstantInvites$fetchGuildInvites$1(this));
    }

    public final void fetchInviteIfNotLoaded(String str, Long l, String str2, Boolean bool, Function0<Unit> function0) {
        m.checkNotNullParameter(str, "inviteCode");
        this.dispatcher.schedule(new StoreInstantInvites$fetchInviteIfNotLoaded$1(this, ModelInvite.getInviteStoreKey(str, l), str, l, bool, str2, function0));
    }

    public final Observable<InviteState> observeInvite(final String str) {
        if (str == null) {
            k kVar = new k(InviteState.LoadFailed.INSTANCE);
            m.checkNotNullExpressionValue(kVar, "Observable.just(InviteState.LoadFailed)");
            return kVar;
        }
        Observable<InviteState> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreInstantInvites$observeInvite$1(this), 14, null).F(new b<Map<String, ? extends InviteState>, InviteState>() { // from class: com.discord.stores.StoreInstantInvites$observeInvite$2
            public final StoreInstantInvites.InviteState call(Map<String, ? extends StoreInstantInvites.InviteState> map) {
                StoreInstantInvites.InviteState inviteState = map.get(str);
                return inviteState != null ? inviteState : StoreInstantInvites.InviteState.Loading.INSTANCE;
            }
        }).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Map<String, InviteState>> observeKnownInvites() {
        Observable<Map<String, InviteState>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreInstantInvites$observeKnownInvites$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    public final void onInviteRemoved(ModelInvite modelInvite) {
        m.checkNotNullParameter(modelInvite, "invite");
        this.dispatcher.schedule(new StoreInstantInvites$onInviteRemoved$1(this, modelInvite));
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        super.snapshotData();
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry<Long, Map<String, ModelInvite>> entry : this.invites.entrySet()) {
            linkedHashMap.put(Long.valueOf(entry.getKey().longValue()), new HashMap(entry.getValue()));
        }
        this.invitesSnapshot = linkedHashMap;
        this.knownInvitesSnapshot = new HashMap(this.knownInvites);
    }

    public final Map<String, ModelInvite> getInvites(long j) {
        Map<String, ModelInvite> map = getInvites().get(Long.valueOf(j));
        return map != null ? map : h0.emptyMap();
    }

    public StoreInstantInvites(Dispatcher dispatcher, ObservationDeck observationDeck, RestAPI restAPI) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(restAPI, "restAPI");
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
        this.restAPI = restAPI;
        this.invites = new LinkedHashMap();
        this.invitesSnapshot = h0.emptyMap();
        this.knownInvites = new LinkedHashMap();
        this.knownInvitesSnapshot = h0.emptyMap();
    }
}
