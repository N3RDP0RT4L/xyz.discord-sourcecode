package com.discord.stores;

import a0.a.a.b;
import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import b.d.b.a.a;
import com.discord.models.message.Message;
import com.discord.models.user.User;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$1;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$2;
import com.discord.utilities.user.UserUtils;
import d0.t.n;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.PublishSubject;
/* compiled from: StoreChat.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010!\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001:\u0003<=>B\u0019\u0012\u0006\u0010/\u001a\u00020.\u0012\b\b\u0002\u00103\u001a\u000202¢\u0006\u0004\b:\u0010;J\u0013\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0017\u0010\t\u001a\f\u0012\b\u0012\u00060\u0007j\u0002`\b0\u0006¢\u0006\u0004\b\t\u0010\nJ\u001d\u0010\u000b\u001a\u0012\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0007j\u0002`\b0\u00060\u0002¢\u0006\u0004\b\u000b\u0010\u0005J\u000f\u0010\r\u001a\u0004\u0018\u00010\f¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u0010\u001a\u0004\u0018\u00010\u000f¢\u0006\u0004\b\u0010\u0010\u0011J\u0013\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\f0\u0002¢\u0006\u0004\b\u0012\u0010\u0005J\u0015\u0010\u0013\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u000f0\u0002¢\u0006\u0004\b\u0013\u0010\u0005J\u0015\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0014\u001a\u00020\f¢\u0006\u0004\b\u0016\u0010\u0017J\u0017\u0010\u0019\u001a\u00020\u00152\b\u0010\u0018\u001a\u0004\u0018\u00010\u000f¢\u0006\u0004\b\u0019\u0010\u001aJ\u0019\u0010\u001c\u001a\u00020\u00152\n\u0010\u001b\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\b\u001c\u0010\u001dJ#\u0010\"\u001a\u00020\u00152\b\u0010\u001f\u001a\u0004\u0018\u00010\u001e2\n\u0010!\u001a\u00060\u0007j\u0002` ¢\u0006\u0004\b\"\u0010#J\u0015\u0010&\u001a\u00020\u00152\u0006\u0010%\u001a\u00020$¢\u0006\u0004\b&\u0010'J\u000f\u0010(\u001a\u00020\u0015H\u0016¢\u0006\u0004\b(\u0010)R \u0010+\u001a\f\u0012\b\u0012\u00060\u0007j\u0002`\b0*8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b+\u0010,R \u0010-\u001a\f\u0012\b\u0012\u00060\u0007j\u0002`\b0\u00068\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b-\u0010,R\u0016\u0010/\u001a\u00020.8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b/\u00100R\u0018\u0010\u0014\u001a\u0004\u0018\u00010\f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0014\u00101R\u0016\u00103\u001a\u0002028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b3\u00104R\u0018\u0010\u0018\u001a\u0004\u0018\u00010\u000f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0018\u00105R:\u00108\u001a&\u0012\f\u0012\n 7*\u0004\u0018\u00010\u00030\u0003 7*\u0012\u0012\f\u0012\n 7*\u0004\u0018\u00010\u00030\u0003\u0018\u000106068\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b8\u00109¨\u0006?"}, d2 = {"Lcom/discord/stores/StoreChat;", "Lcom/discord/stores/StoreV2;", "Lrx/Observable;", "Lcom/discord/stores/StoreChat$Event;", "observeEvents", "()Lrx/Observable;", "", "", "Lcom/discord/primitives/MessageId;", "getExpandedBlockedMessageGroups", "()Ljava/util/List;", "observeExpandedBlockedMessageIds", "Lcom/discord/stores/StoreChat$InteractionState;", "getInteractionState", "()Lcom/discord/stores/StoreChat$InteractionState;", "Lcom/discord/stores/StoreChat$EditingMessage;", "getEditingMessage", "()Lcom/discord/stores/StoreChat$EditingMessage;", "observeInteractionState", "observeEditingMessage", "interactionState", "", "setInteractionState", "(Lcom/discord/stores/StoreChat$InteractionState;)V", "editingMessage", "setEditingMessage", "(Lcom/discord/stores/StoreChat$EditingMessage;)V", "messageId", "toggleBlockedMessageGroup", "(J)V", "Lcom/discord/models/user/User;", "user", "Lcom/discord/primitives/GuildId;", "guildId", "appendMention", "(Lcom/discord/models/user/User;J)V", "", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "replaceChatText", "(Ljava/lang/String;)V", "snapshotData", "()V", "", "expandedBlockedMessageGroups", "Ljava/util/List;", "expandedBlockedMessageGroupsSnapshot", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/stores/StoreChat$InteractionState;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/stores/StoreChat$EditingMessage;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V", "EditingMessage", "Event", "InteractionState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreChat extends StoreV2 {
    private final Dispatcher dispatcher;
    private EditingMessage editingMessage;
    private final PublishSubject<Event> eventSubject;
    private final List<Long> expandedBlockedMessageGroups;
    private List<Long> expandedBlockedMessageGroupsSnapshot;
    private InteractionState interactionState;
    private final ObservationDeck observationDeck;

    /* compiled from: StoreChat.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0016\u001a\u0004\b\u0017\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0018\u001a\u0004\b\u0019\u0010\u0004¨\u0006\u001c"}, d2 = {"Lcom/discord/stores/StoreChat$EditingMessage;", "", "Lcom/discord/models/message/Message;", "component1", "()Lcom/discord/models/message/Message;", "", "component2", "()Ljava/lang/CharSequence;", "message", "content", "copy", "(Lcom/discord/models/message/Message;Ljava/lang/CharSequence;)Lcom/discord/stores/StoreChat$EditingMessage;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/CharSequence;", "getContent", "Lcom/discord/models/message/Message;", "getMessage", HookHelper.constructorName, "(Lcom/discord/models/message/Message;Ljava/lang/CharSequence;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class EditingMessage {
        private final CharSequence content;
        private final Message message;

        public EditingMessage(Message message, CharSequence charSequence) {
            m.checkNotNullParameter(message, "message");
            m.checkNotNullParameter(charSequence, "content");
            this.message = message;
            this.content = charSequence;
        }

        public static /* synthetic */ EditingMessage copy$default(EditingMessage editingMessage, Message message, CharSequence charSequence, int i, Object obj) {
            if ((i & 1) != 0) {
                message = editingMessage.message;
            }
            if ((i & 2) != 0) {
                charSequence = editingMessage.content;
            }
            return editingMessage.copy(message, charSequence);
        }

        public final Message component1() {
            return this.message;
        }

        public final CharSequence component2() {
            return this.content;
        }

        public final EditingMessage copy(Message message, CharSequence charSequence) {
            m.checkNotNullParameter(message, "message");
            m.checkNotNullParameter(charSequence, "content");
            return new EditingMessage(message, charSequence);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof EditingMessage)) {
                return false;
            }
            EditingMessage editingMessage = (EditingMessage) obj;
            return m.areEqual(this.message, editingMessage.message) && m.areEqual(this.content, editingMessage.content);
        }

        public final CharSequence getContent() {
            return this.content;
        }

        public final Message getMessage() {
            return this.message;
        }

        public int hashCode() {
            Message message = this.message;
            int i = 0;
            int hashCode = (message != null ? message.hashCode() : 0) * 31;
            CharSequence charSequence = this.content;
            if (charSequence != null) {
                i = charSequence.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("EditingMessage(message=");
            R.append(this.message);
            R.append(", content=");
            return a.D(R, this.content, ")");
        }
    }

    /* compiled from: StoreChat.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/stores/StoreChat$Event;", "", HookHelper.constructorName, "()V", "AppendChatText", "ReplaceChatText", "Lcom/discord/stores/StoreChat$Event$AppendChatText;", "Lcom/discord/stores/StoreChat$Event$ReplaceChatText;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class Event {

        /* compiled from: StoreChat.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/stores/StoreChat$Event$AppendChatText;", "Lcom/discord/stores/StoreChat$Event;", "", "component1", "()Ljava/lang/String;", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "copy", "(Ljava/lang/String;)Lcom/discord/stores/StoreChat$Event$AppendChatText;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getText", HookHelper.constructorName, "(Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class AppendChatText extends Event {
            private final String text;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public AppendChatText(String str) {
                super(null);
                m.checkNotNullParameter(str, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
                this.text = str;
            }

            public static /* synthetic */ AppendChatText copy$default(AppendChatText appendChatText, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = appendChatText.text;
                }
                return appendChatText.copy(str);
            }

            public final String component1() {
                return this.text;
            }

            public final AppendChatText copy(String str) {
                m.checkNotNullParameter(str, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
                return new AppendChatText(str);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof AppendChatText) && m.areEqual(this.text, ((AppendChatText) obj).text);
                }
                return true;
            }

            public final String getText() {
                return this.text;
            }

            public int hashCode() {
                String str = this.text;
                if (str != null) {
                    return str.hashCode();
                }
                return 0;
            }

            public String toString() {
                return a.H(a.R("AppendChatText(text="), this.text, ")");
            }
        }

        /* compiled from: StoreChat.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/stores/StoreChat$Event$ReplaceChatText;", "Lcom/discord/stores/StoreChat$Event;", "", "component1", "()Ljava/lang/String;", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "copy", "(Ljava/lang/String;)Lcom/discord/stores/StoreChat$Event$ReplaceChatText;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getText", HookHelper.constructorName, "(Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class ReplaceChatText extends Event {
            private final String text;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public ReplaceChatText(String str) {
                super(null);
                m.checkNotNullParameter(str, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
                this.text = str;
            }

            public static /* synthetic */ ReplaceChatText copy$default(ReplaceChatText replaceChatText, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = replaceChatText.text;
                }
                return replaceChatText.copy(str);
            }

            public final String component1() {
                return this.text;
            }

            public final ReplaceChatText copy(String str) {
                m.checkNotNullParameter(str, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
                return new ReplaceChatText(str);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof ReplaceChatText) && m.areEqual(this.text, ((ReplaceChatText) obj).text);
                }
                return true;
            }

            public final String getText() {
                return this.text;
            }

            public int hashCode() {
                String str = this.text;
                if (str != null) {
                    return str.hashCode();
                }
                return 0;
            }

            public String toString() {
                return a.H(a.R("ReplaceChatText(text="), this.text, ")");
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ StoreChat(Dispatcher dispatcher, ObservationDeck observationDeck, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(dispatcher, (i & 2) != 0 ? ObservationDeckProvider.get() : observationDeck);
    }

    public final void appendMention(User user, long j) {
        if (user != null) {
            if (!(user.getUsername().length() == 0)) {
                StoreGatewayConnection.requestGuildMembers$default(StoreStream.Companion.getGatewaySocket(), j, user.getUsername(), null, null, 12, null);
                StringBuilder R = a.R("@");
                R.append(user.getUsername());
                R.append(UserUtils.INSTANCE.getDiscriminatorWithPadding(user));
                String sb = R.toString();
                PublishSubject<Event> publishSubject = this.eventSubject;
                publishSubject.k.onNext(new Event.AppendChatText(sb + ' '));
            }
        }
    }

    public final EditingMessage getEditingMessage() {
        return this.editingMessage;
    }

    public final List<Long> getExpandedBlockedMessageGroups() {
        return this.expandedBlockedMessageGroupsSnapshot;
    }

    public final InteractionState getInteractionState() {
        return this.interactionState;
    }

    public final Observable<EditingMessage> observeEditingMessage() {
        Observable<EditingMessage> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreChat$observeEditingMessage$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    public final Observable<List<Long>> observeExpandedBlockedMessageIds() {
        Observable<List<Long>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreChat$observeExpandedBlockedMessageIds$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    public final Observable<InteractionState> observeInteractionState() {
        Observable F = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreChat$observeInteractionState$1(this), 14, null).x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
        m.checkNotNullExpressionValue(F, "filter { it != null }.map { it!! }");
        Observable<InteractionState> q = F.q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR…  .distinctUntilChanged()");
        return q;
    }

    public final void replaceChatText(String str) {
        m.checkNotNullParameter(str, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.ReplaceChatText(str + ' '));
    }

    public final void setEditingMessage(EditingMessage editingMessage) {
        this.dispatcher.schedule(new StoreChat$setEditingMessage$1(this, editingMessage));
    }

    public final void setInteractionState(InteractionState interactionState) {
        m.checkNotNullParameter(interactionState, "interactionState");
        this.dispatcher.schedule(new StoreChat$setInteractionState$1(this, interactionState));
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        super.snapshotData();
        this.expandedBlockedMessageGroupsSnapshot = new ArrayList(this.expandedBlockedMessageGroups);
    }

    public final void toggleBlockedMessageGroup(long j) {
        this.dispatcher.schedule(new StoreChat$toggleBlockedMessageGroup$1(this, j));
    }

    public StoreChat(Dispatcher dispatcher, ObservationDeck observationDeck) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
        this.expandedBlockedMessageGroups = new ArrayList();
        this.expandedBlockedMessageGroupsSnapshot = n.emptyList();
        this.eventSubject = PublishSubject.k0();
    }

    /* compiled from: StoreChat.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000 +2\u00020\u0001:\u0001+B/\u0012\n\u0010\u0011\u001a\u00060\nj\u0002`\u000b\u0012\n\u0010\u0012\u001a\u00060\nj\u0002`\u000e\u0012\u0006\u0010\u0013\u001a\u00020\u0005\u0012\u0006\u0010\u0014\u001a\u00020\u0002¢\u0006\u0004\b&\u0010'B3\b\u0016\u0012\n\u0010\u0011\u001a\u00060\nj\u0002`\u000b\u0012\n\u0010\u0012\u001a\u00060\nj\u0002`\u000e\u0012\u0006\u0010\u0013\u001a\u00020\u0005\u0012\b\u0010)\u001a\u0004\u0018\u00010(¢\u0006\u0004\b&\u0010*J\u0010\u0010\u0003\u001a\u00020\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\r\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\u0006\u0010\u0007J\r\u0010\b\u001a\u00020\u0005¢\u0006\u0004\b\b\u0010\u0007J\r\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\t\u0010\u0007J\u0014\u0010\f\u001a\u00060\nj\u0002`\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0014\u0010\u000f\u001a\u00060\nj\u0002`\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\rJ\u0010\u0010\u0010\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0007J@\u0010\u0015\u001a\u00020\u00002\f\b\u0002\u0010\u0011\u001a\u00060\nj\u0002`\u000b2\f\b\u0002\u0010\u0012\u001a\u00060\nj\u0002`\u000e2\b\b\u0002\u0010\u0013\u001a\u00020\u00052\b\b\u0002\u0010\u0014\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0018\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u001a\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u001a\u0010\u0004J\u001a\u0010\u001c\u001a\u00020\u00052\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u0019\u0010\u001e\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b\u001e\u0010\u0007R\u0019\u0010\u0013\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u001f\u001a\u0004\b\u0013\u0010\u0007R\u0019\u0010 \u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u001f\u001a\u0004\b \u0010\u0007R\u001d\u0010\u0012\u001a\u00060\nj\u0002`\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010!\u001a\u0004\b\"\u0010\rR\u0016\u0010\u0014\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$R\u001d\u0010\u0011\u001a\u00060\nj\u0002`\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010!\u001a\u0004\b%\u0010\r¨\u0006,"}, d2 = {"Lcom/discord/stores/StoreChat$InteractionState;", "", "", "component4", "()I", "", "isAtTopIgnoringTouch", "()Z", "isAtBottomIgnoringTouch", "isNearBottomIgnoringTouch", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "Lcom/discord/primitives/MessageId;", "component2", "component3", "channelId", "lastMessageId", "isTouchedSinceLastJump", "scrollState", "copy", "(JJZI)Lcom/discord/stores/StoreChat$InteractionState;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "isAtBottom", "Z", "isAtTop", "J", "getLastMessageId", "scrollState$1", "I", "getChannelId", HookHelper.constructorName, "(JJZI)V", "Landroidx/recyclerview/widget/LinearLayoutManager;", "layoutManager", "(JJZLandroidx/recyclerview/widget/LinearLayoutManager;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class InteractionState {
        public static final Companion Companion = new Companion(null);
        private static final int NEAR_EDGE_THRESHOLD = 15;
        private static final int SCROLLED = 0;
        private static final int SCROLLED_BOTTOM = 1;
        private static final int SCROLLED_NEAR_BOTTOM = 4;
        private static final int SCROLLED_TOP = 2;
        private final long channelId;
        private final boolean isAtBottom;
        private final boolean isAtTop;
        private final boolean isTouchedSinceLastJump;
        private final long lastMessageId;
        private final int scrollState$1;

        /* compiled from: StoreChat.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\f\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eR\u001c\u0010\u0006\u001a\u00020\u0003*\u0004\u0018\u00010\u00028B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005R\u0016\u0010\u0007\u001a\u00020\u00038\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0007\u0010\bR\u0016\u0010\t\u001a\u00020\u00038\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\t\u0010\bR\u0016\u0010\n\u001a\u00020\u00038\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\n\u0010\bR\u0016\u0010\u000b\u001a\u00020\u00038\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000b\u0010\bR\u0016\u0010\f\u001a\u00020\u00038\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\f\u0010\b¨\u0006\u000f"}, d2 = {"Lcom/discord/stores/StoreChat$InteractionState$Companion;", "", "Landroidx/recyclerview/widget/LinearLayoutManager;", "", "getScrollState", "(Landroidx/recyclerview/widget/LinearLayoutManager;)I", "scrollState", "NEAR_EDGE_THRESHOLD", "I", "SCROLLED", "SCROLLED_BOTTOM", "SCROLLED_NEAR_BOTTOM", "SCROLLED_TOP", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Companion {
            private Companion() {
            }

            /* JADX INFO: Access modifiers changed from: private */
            public final int getScrollState(LinearLayoutManager linearLayoutManager) {
                boolean z2 = false;
                if (linearLayoutManager == null) {
                    return 0;
                }
                int itemCount = linearLayoutManager.getItemCount() - 1;
                int findLastVisibleItemPosition = linearLayoutManager.findLastVisibleItemPosition();
                int max = Math.max(0, itemCount - 15);
                int findFirstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
                boolean z3 = 1 <= findFirstVisibleItemPosition && 15 >= findFirstVisibleItemPosition;
                int i = findFirstVisibleItemPosition <= 0 ? 1 : 0;
                if (findLastVisibleItemPosition == itemCount || findLastVisibleItemPosition >= max) {
                    z2 = true;
                }
                if (z2) {
                    i |= 2;
                }
                return z3 ? i | 4 : i;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public InteractionState(long j, long j2, boolean z2, int i) {
            this.channelId = j;
            this.lastMessageId = j2;
            this.isTouchedSinceLastJump = z2;
            this.scrollState$1 = i;
            boolean z3 = true;
            this.isAtTop = z2 && isAtTopIgnoringTouch();
            this.isAtBottom = (!z2 || !isAtBottomIgnoringTouch()) ? false : z3;
        }

        private final int component4() {
            return this.scrollState$1;
        }

        public static /* synthetic */ InteractionState copy$default(InteractionState interactionState, long j, long j2, boolean z2, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                j = interactionState.channelId;
            }
            long j3 = j;
            if ((i2 & 2) != 0) {
                j2 = interactionState.lastMessageId;
            }
            long j4 = j2;
            if ((i2 & 4) != 0) {
                z2 = interactionState.isTouchedSinceLastJump;
            }
            boolean z3 = z2;
            if ((i2 & 8) != 0) {
                i = interactionState.scrollState$1;
            }
            return interactionState.copy(j3, j4, z3, i);
        }

        public final long component1() {
            return this.channelId;
        }

        public final long component2() {
            return this.lastMessageId;
        }

        public final boolean component3() {
            return this.isTouchedSinceLastJump;
        }

        public final InteractionState copy(long j, long j2, boolean z2, int i) {
            return new InteractionState(j, j2, z2, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof InteractionState)) {
                return false;
            }
            InteractionState interactionState = (InteractionState) obj;
            return this.channelId == interactionState.channelId && this.lastMessageId == interactionState.lastMessageId && this.isTouchedSinceLastJump == interactionState.isTouchedSinceLastJump && this.scrollState$1 == interactionState.scrollState$1;
        }

        public final long getChannelId() {
            return this.channelId;
        }

        public final long getLastMessageId() {
            return this.lastMessageId;
        }

        public int hashCode() {
            int a = (b.a(this.lastMessageId) + (b.a(this.channelId) * 31)) * 31;
            boolean z2 = this.isTouchedSinceLastJump;
            if (z2) {
                z2 = true;
            }
            int i = z2 ? 1 : 0;
            int i2 = z2 ? 1 : 0;
            return ((a + i) * 31) + this.scrollState$1;
        }

        public final boolean isAtBottom() {
            return this.isAtBottom;
        }

        public final boolean isAtBottomIgnoringTouch() {
            return (this.scrollState$1 & 1) != 0;
        }

        public final boolean isAtTop() {
            return this.isAtTop;
        }

        public final boolean isAtTopIgnoringTouch() {
            return (this.scrollState$1 & 2) != 0;
        }

        public final boolean isNearBottomIgnoringTouch() {
            return (this.scrollState$1 & 4) != 0;
        }

        public final boolean isTouchedSinceLastJump() {
            return this.isTouchedSinceLastJump;
        }

        public String toString() {
            StringBuilder R = a.R("InteractionState(channelId=");
            R.append(this.channelId);
            R.append(", lastMessageId=");
            R.append(this.lastMessageId);
            R.append(", isTouchedSinceLastJump=");
            R.append(this.isTouchedSinceLastJump);
            R.append(", scrollState=");
            return a.A(R, this.scrollState$1, ")");
        }

        public InteractionState(long j, long j2, boolean z2, LinearLayoutManager linearLayoutManager) {
            this(j, j2, z2, Companion.getScrollState(linearLayoutManager));
        }
    }
}
