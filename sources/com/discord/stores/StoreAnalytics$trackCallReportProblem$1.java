package com.discord.stores;

import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.voice.state.VoiceState;
import com.discord.rtcconnection.audio.DiscordAudioManager;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.widgets.voice.feedback.PendingFeedback;
import d0.t.h0;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreAnalytics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAnalytics$trackCallReportProblem$1 extends o implements Function0<Unit> {
    public final /* synthetic */ PendingFeedback.CallFeedback $pendingCallFeedback;
    public final /* synthetic */ StoreAnalytics this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreAnalytics$trackCallReportProblem$1(StoreAnalytics storeAnalytics, PendingFeedback.CallFeedback callFeedback) {
        super(0);
        this.this$0 = storeAnalytics;
        this.$pendingCallFeedback = callFeedback;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreStream storeStream;
        StoreStream storeStream2;
        StoreStream storeStream3;
        StoreStream storeStream4;
        StoreStream storeStream5;
        storeStream = this.this$0.stores;
        long id2 = storeStream.getUsers$app_productionGoogleRelease().getMeInternal$app_productionGoogleRelease().getId();
        storeStream2 = this.this$0.stores;
        Channel findChannelByIdInternal$app_productionGoogleRelease = storeStream2.getChannels$app_productionGoogleRelease().findChannelByIdInternal$app_productionGoogleRelease(this.$pendingCallFeedback.getChannelId());
        if (findChannelByIdInternal$app_productionGoogleRelease != null) {
            storeStream3 = this.this$0.stores;
            StoreMediaSettings mediaSettings$app_productionGoogleRelease = storeStream3.getMediaSettings$app_productionGoogleRelease();
            storeStream4 = this.this$0.stores;
            DiscordAudioManager.AudioDevice lastActiveAudioDevice$app_productionGoogleRelease = storeStream4.getAudioManagerV2$app_productionGoogleRelease().getLastActiveAudioDevice$app_productionGoogleRelease();
            AnalyticsTracker analyticsTracker = AnalyticsTracker.INSTANCE;
            String rtcConnectionId = this.$pendingCallFeedback.getRtcConnectionId();
            storeStream5 = this.this$0.stores;
            Map<Long, VoiceState> map = (Map) a.u0(findChannelByIdInternal$app_productionGoogleRelease, storeStream5.getVoiceStates$app_productionGoogleRelease().get());
            if (map == null) {
                map = h0.emptyMap();
            }
            analyticsTracker.callReportProblem(id2, rtcConnectionId, findChannelByIdInternal$app_productionGoogleRelease, map, this.$pendingCallFeedback.getDurationMs(), this.$pendingCallFeedback.getMediaSessionId(), this.$pendingCallFeedback.getFeedbackRating(), this.$pendingCallFeedback.getReasonCode(), this.$pendingCallFeedback.getReasonDescription(), mediaSettings$app_productionGoogleRelease.getVoiceConfigurationBlocking(), lastActiveAudioDevice$app_productionGoogleRelease, mediaSettings$app_productionGoogleRelease.getVideoHardwareScalingBlocking(), this.$pendingCallFeedback.getIssueDetails());
        }
    }
}
