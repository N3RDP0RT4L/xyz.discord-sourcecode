package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guild.Guild;
import com.discord.api.thread.ThreadMetadata;
import com.discord.models.domain.ModelPayload;
import com.discord.models.thread.dto.ModelThreadListSync;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import d0.d0.f;
import d0.t.g0;
import d0.t.h0;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreThreadsActive.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u00109\u001a\u000208\u0012\b\b\u0002\u00104\u001a\u000203¢\u0006\u0004\b;\u0010<J+\u0010\b\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u00070\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0002¢\u0006\u0004\b\b\u0010\tJ;\u0010\u000b\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u00070\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u000e\u0010\n\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0006H\u0002¢\u0006\u0004\b\u000b\u0010\fJ%\u0010\u000e\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00060\r2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0011\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u001b\u0010\u0015\u001a\u00020\u00122\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0017\u0010\u0018\u001a\u00020\u00122\u0006\u0010\u0017\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\u0018\u0010\u0019J/\u0010\u001c\u001a\"\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u00070\u00050\u0005H\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ)\u0010\u001e\u001a\u0012\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00060\r0\u001d2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u001e\u0010\u001fJ/\u0010 \u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u00070\u00050\u001d2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b \u0010\u001fJ?\u0010!\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u00070\u00050\u001d2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u000e\u0010\n\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0006¢\u0006\u0004\b!\u0010\"J\u0017\u0010%\u001a\u00020\u00122\u0006\u0010$\u001a\u00020#H\u0007¢\u0006\u0004\b%\u0010&J\u0017\u0010'\u001a\u00020\u00122\u0006\u0010\u0011\u001a\u00020\u0010H\u0007¢\u0006\u0004\b'\u0010\u0014J\u001b\u0010(\u001a\u00020\u00122\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0007¢\u0006\u0004\b(\u0010\u0016J\u0017\u0010)\u001a\u00020\u00122\u0006\u0010\u0017\u001a\u00020\u0007H\u0007¢\u0006\u0004\b)\u0010\u0019J\u0017\u0010*\u001a\u00020\u00122\u0006\u0010\u0017\u001a\u00020\u0007H\u0007¢\u0006\u0004\b*\u0010\u0019J\u0017\u0010,\u001a\u00020\u00122\u0006\u0010$\u001a\u00020+H\u0007¢\u0006\u0004\b,\u0010-J\u0017\u0010.\u001a\u00020\u00122\u0006\u0010\u0017\u001a\u00020\u0007H\u0007¢\u0006\u0004\b.\u0010\u0019J\u000f\u0010/\u001a\u00020\u0012H\u0017¢\u0006\u0004\b/\u00100R6\u00101\u001a\"\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u00070\u00050\u00058\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b1\u00102R\u0016\u00104\u001a\u0002038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b4\u00105R6\u00107\u001a\"\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u000706068\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b7\u00102R\u0016\u00109\u001a\u0002088\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b9\u0010:¨\u0006="}, d2 = {"Lcom/discord/stores/StoreThreadsActive;", "Lcom/discord/stores/StoreV2;", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "getActiveThreadsForGuild", "(J)Ljava/util/Map;", "channelId", "getActiveThreadsForChannel", "(JLjava/lang/Long;)Ljava/util/Map;", "", "getChannelsWithActiveThreadsByGuild", "(J)Ljava/util/Set;", "Lcom/discord/api/guild/Guild;", "guild", "", "saveThreads", "(Lcom/discord/api/guild/Guild;)V", "deleteThreads", "(J)V", "channel", "deleteThread", "(Lcom/discord/api/channel/Channel;)V", "getAllActiveThreadsInternal$app_productionGoogleRelease", "()Ljava/util/Map;", "getAllActiveThreadsInternal", "Lrx/Observable;", "observeChannelsWithActiveThreadsByGuild", "(J)Lrx/Observable;", "observeActiveThreadsForGuild", "observeActiveThreadsForChannel", "(JLjava/lang/Long;)Lrx/Observable;", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "handleGuildCreate", "handleGuildDelete", "handleChannelCreateOrUpdate", "handleThreadCreateOrUpdate", "Lcom/discord/models/thread/dto/ModelThreadListSync;", "handleThreadListSync", "(Lcom/discord/models/thread/dto/ModelThreadListSync;)V", "handleThreadDelete", "snapshotData", "()V", "threadsByGuildSnapshot", "Ljava/util/Map;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "", "threadsByGuild", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreChannels;", HookHelper.constructorName, "(Lcom/discord/stores/StoreChannels;Lcom/discord/stores/updates/ObservationDeck;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreThreadsActive extends StoreV2 {
    private final ObservationDeck observationDeck;
    private final StoreChannels storeChannels;
    private final Map<Long, Map<Long, Channel>> threadsByGuild;
    private Map<Long, ? extends Map<Long, Channel>> threadsByGuildSnapshot;

    public /* synthetic */ StoreThreadsActive(StoreChannels storeChannels, ObservationDeck observationDeck, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(storeChannels, (i & 2) != 0 ? ObservationDeckProvider.get() : observationDeck);
    }

    private final void deleteThread(Channel channel) {
        Map map = (Map) a.u0(channel, this.threadsByGuild);
        if (map != null && map.containsKey(Long.valueOf(channel.h()))) {
            map.remove(Long.valueOf(channel.h()));
            markChanged();
        }
    }

    private final void deleteThreads(long j) {
        if (this.threadsByGuild.containsKey(Long.valueOf(j))) {
            this.threadsByGuild.remove(Long.valueOf(j));
            markChanged();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Map<Long, Channel> getActiveThreadsForChannel(long j, Long l) {
        if (l == null) {
            return h0.emptyMap();
        }
        Map<Long, Channel> map = this.threadsByGuildSnapshot.get(Long.valueOf(j));
        if (map == null) {
            map = h0.emptyMap();
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry<Long, Channel> entry : map.entrySet()) {
            if (entry.getValue().r() == l.longValue()) {
                linkedHashMap.put(entry.getKey(), entry.getValue());
            }
        }
        return linkedHashMap;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Map<Long, Channel> getActiveThreadsForGuild(long j) {
        Map<Long, Channel> map = this.threadsByGuildSnapshot.get(Long.valueOf(j));
        return map != null ? map : h0.emptyMap();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Set<Long> getChannelsWithActiveThreadsByGuild(long j) {
        Map<Long, Channel> map = this.threadsByGuildSnapshot.get(Long.valueOf(j));
        if (map == null) {
            map = h0.emptyMap();
        }
        ArrayList arrayList = new ArrayList(map.size());
        for (Map.Entry<Long, Channel> entry : map.entrySet()) {
            arrayList.add(Long.valueOf(entry.getValue().r()));
        }
        return u.toSet(arrayList);
    }

    private final void saveThreads(Guild guild) {
        ThreadMetadata y2;
        List<Channel> threadsForGuildInternal$app_productionGoogleRelease = this.storeChannels.getThreadsForGuildInternal$app_productionGoogleRelease(guild.r());
        if (!threadsForGuildInternal$app_productionGoogleRelease.isEmpty()) {
            Map<Long, Map<Long, Channel>> map = this.threadsByGuild;
            Long valueOf = Long.valueOf(guild.r());
            ArrayList arrayList = new ArrayList();
            for (Object obj : threadsForGuildInternal$app_productionGoogleRelease) {
                Channel channel = (Channel) obj;
                if (ChannelUtils.C(channel) && ((y2 = channel.y()) == null || !y2.b())) {
                    arrayList.add(obj);
                }
            }
            LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(o.collectionSizeOrDefault(arrayList, 10)), 16));
            for (Object obj2 : arrayList) {
                linkedHashMap.put(Long.valueOf(((Channel) obj2).h()), obj2);
            }
            map.put(valueOf, h0.toMutableMap(linkedHashMap));
            markChanged();
        }
    }

    @StoreThread
    public final Map<Long, Map<Long, Channel>> getAllActiveThreadsInternal$app_productionGoogleRelease() {
        return this.threadsByGuild;
    }

    @StoreThread
    public final void handleChannelCreateOrUpdate(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        Map map = (Map) a.u0(channel, this.threadsByGuild);
        if (map != null) {
            Collection values = map.values();
            ArrayList<Channel> arrayList = new ArrayList();
            for (Object obj : values) {
                Channel channel2 = (Channel) obj;
                if (channel2.r() == channel.h() && channel2.o() != channel.o()) {
                    arrayList.add(obj);
                }
            }
            for (Channel channel3 : arrayList) {
                Channel channelInternal$app_productionGoogleRelease = this.storeChannels.getChannelInternal$app_productionGoogleRelease(channel3.f(), channel3.h());
                if (channelInternal$app_productionGoogleRelease != null) {
                    map.put(Long.valueOf(channelInternal$app_productionGoogleRelease.h()), channelInternal$app_productionGoogleRelease);
                }
            }
        }
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        this.threadsByGuild.clear();
        List<Guild> guilds = modelPayload.getGuilds();
        m.checkNotNullExpressionValue(guilds, "payload.guilds");
        for (Guild guild : guilds) {
            m.checkNotNullExpressionValue(guild, "guild");
            saveThreads(guild);
        }
        markChanged();
    }

    @StoreThread
    public final void handleGuildCreate(Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        deleteThreads(guild.r());
        saveThreads(guild);
    }

    @StoreThread
    public final void handleGuildDelete(long j) {
        deleteThreads(j);
    }

    @StoreThread
    public final void handleThreadCreateOrUpdate(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        if (ChannelUtils.C(channel)) {
            ThreadMetadata y2 = channel.y();
            if (y2 == null || !y2.b()) {
                Map<Long, Map<Long, Channel>> map = this.threadsByGuild;
                Long valueOf = Long.valueOf(channel.f());
                Map<Long, Channel> map2 = map.get(valueOf);
                if (map2 == null) {
                    map2 = new HashMap<>();
                    map.put(valueOf, map2);
                }
                map2.put(Long.valueOf(channel.h()), channel);
                markChanged();
                return;
            }
            deleteThread(channel);
        }
    }

    @StoreThread
    public final void handleThreadDelete(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        deleteThread(channel);
    }

    @StoreThread
    public final void handleThreadListSync(ModelThreadListSync modelThreadListSync) {
        m.checkNotNullParameter(modelThreadListSync, "payload");
        Map<Long, Map<Long, Channel>> map = this.threadsByGuild;
        Long valueOf = Long.valueOf(modelThreadListSync.getGuildId());
        Map<Long, Channel> map2 = map.get(valueOf);
        if (map2 == null) {
            map2 = new HashMap<>();
            map.put(valueOf, map2);
        }
        Map<Long, Channel> map3 = map2;
        for (Channel channel : modelThreadListSync.getThreads()) {
            if (ChannelUtils.C(channel)) {
                map3.put(Long.valueOf(channel.h()), channel);
            }
        }
        markChanged();
    }

    public final Observable<Map<Long, Channel>> observeActiveThreadsForChannel(long j, Long l) {
        Observable<Map<Long, Channel>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreThreadsActive$observeActiveThreadsForChannel$1(this, j, l), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Map<Long, Channel>> observeActiveThreadsForGuild(long j) {
        Observable<Map<Long, Channel>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreThreadsActive$observeActiveThreadsForGuild$1(this, j), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Set<Long>> observeChannelsWithActiveThreadsByGuild(long j) {
        Observable<Set<Long>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreThreadsActive$observeChannelsWithActiveThreadsByGuild$1(this, j), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR…  .distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        Map<Long, Map<Long, Channel>> map = this.threadsByGuild;
        LinkedHashMap linkedHashMap = new LinkedHashMap(g0.mapCapacity(map.size()));
        Iterator<T> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            linkedHashMap.put(entry.getKey(), new HashMap((Map) entry.getValue()));
        }
        this.threadsByGuildSnapshot = linkedHashMap;
    }

    public StoreThreadsActive(StoreChannels storeChannels, ObservationDeck observationDeck) {
        m.checkNotNullParameter(storeChannels, "storeChannels");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.storeChannels = storeChannels;
        this.observationDeck = observationDeck;
        this.threadsByGuild = new HashMap();
        this.threadsByGuildSnapshot = h0.emptyMap();
    }
}
