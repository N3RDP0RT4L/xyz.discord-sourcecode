package com.discord.stores;

import andhook.lib.HookHelper;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import com.discord.stores.StoreSearch;
import com.discord.stores.StoreStream;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableWithLeadingEdgeThrottle;
import com.discord.utilities.search.validation.SearchData;
import d0.z.d.m;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.functions.Function4;
import rx.Observable;
import rx.Subscription;
import rx.functions.Func4;
import rx.subjects.BehaviorSubject;
import rx.subjects.Subject;
/* compiled from: StoreSearchData.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\u001e\u001a\u00020\u001d\u0012\u0006\u0010!\u001a\u00020 \u0012\u0006\u0010$\u001a\u00020#\u0012\u0006\u0010'\u001a\u00020&¢\u0006\u0004\b,\u0010-J!\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0002¢\u0006\u0004\b\u0007\u0010\bJ!\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\n\u0010\n\u001a\u00060\u0002j\u0002`\tH\u0002¢\u0006\u0004\b\u000b\u0010\bJ\u0019\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u0015\u0010\u0016\u001a\u00020\u000e2\u0006\u0010\u0015\u001a\u00020\u0014¢\u0006\u0004\b\u0016\u0010\u0017J\u0013\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0004\b\u0018\u0010\u0019J\r\u0010\u001a\u001a\u00020\u000e¢\u0006\u0004\b\u001a\u0010\u001bR\u0018\u0010\r\u001a\u0004\u0018\u00010\f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\r\u0010\u001cR\u0016\u0010\u001e\u001a\u00020\u001d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR\u0016\u0010!\u001a\u00020 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"R\u0016\u0010$\u001a\u00020#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%R\u0016\u0010'\u001a\u00020&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R\"\u0010*\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010+¨\u0006."}, d2 = {"Lcom/discord/stores/StoreSearchData;", "", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lrx/Observable;", "Lcom/discord/utilities/search/validation/SearchData;", "getChannelSearchData", "(J)Lrx/Observable;", "Lcom/discord/primitives/GuildId;", "guildId", "getGuildSearchData", "Lrx/Subscription;", Traits.Payment.Type.SUBSCRIPTION, "", "handleSubscription", "(Lrx/Subscription;)V", "searchData", "handleNewData", "(Lcom/discord/utilities/search/validation/SearchData;)V", "Lcom/discord/stores/StoreSearch$SearchTarget;", "searchTarget", "init", "(Lcom/discord/stores/StoreSearch$SearchTarget;)V", "get", "()Lrx/Observable;", "clear", "()V", "Lrx/Subscription;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreChannels;", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreUser;", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreGuilds;", "Lrx/subjects/Subject;", "searchDataSubject", "Lrx/subjects/Subject;", HookHelper.constructorName, "(Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreSearchData {
    private final ObservationDeck observationDeck;
    private final Subject<SearchData, SearchData> searchDataSubject;
    private final StoreChannels storeChannels;
    private final StoreGuilds storeGuilds;
    private final StoreUser storeUser;
    private Subscription subscription;

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            StoreSearch.SearchTarget.Type.values();
            int[] iArr = new int[2];
            $EnumSwitchMapping$0 = iArr;
            iArr[StoreSearch.SearchTarget.Type.GUILD.ordinal()] = 1;
            iArr[StoreSearch.SearchTarget.Type.CHANNEL.ordinal()] = 2;
        }
    }

    public StoreSearchData(ObservationDeck observationDeck, StoreChannels storeChannels, StoreUser storeUser, StoreGuilds storeGuilds) {
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(storeChannels, "storeChannels");
        m.checkNotNullParameter(storeUser, "storeUser");
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        this.observationDeck = observationDeck;
        this.storeChannels = storeChannels;
        this.storeUser = storeUser;
        this.storeGuilds = storeGuilds;
        BehaviorSubject l0 = BehaviorSubject.l0(new SearchData(null, null, null, null, null, 31, null));
        m.checkNotNullExpressionValue(l0, "BehaviorSubject.create(SearchData())");
        this.searchDataSubject = l0;
    }

    private final Observable<SearchData> getChannelSearchData(long j) {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this.storeChannels, this.storeUser, this.storeGuilds}, false, null, null, new StoreSearchData$getChannelSearchData$1(this, j), 14, null);
    }

    private final Observable<SearchData> getGuildSearchData(long j) {
        StoreStream.Companion companion = StoreStream.Companion;
        Observable<Map<Long, GuildMember>> observeComputed = companion.getGuilds().observeComputed(j);
        Observable<Map<Long, User>> observeAllUsers = companion.getUsers().observeAllUsers();
        Observable F = StoreChannels.observeChannelsForGuild$default(companion.getChannels(), j, null, 2, null).F(StoreSearchData$getGuildSearchData$1.INSTANCE);
        Observable<Map<Long, Long>> observeChannelPermissionsForGuild = companion.getPermissions().observeChannelPermissionsForGuild(j);
        final StoreSearchData$getGuildSearchData$2 storeSearchData$getGuildSearchData$2 = new StoreSearchData$getGuildSearchData$2(new SearchData.Builder());
        Observable<SearchData> combineLatest = ObservableWithLeadingEdgeThrottle.combineLatest(observeComputed, observeAllUsers, F, observeChannelPermissionsForGuild, new Func4() { // from class: com.discord.stores.StoreSearchData$sam$rx_functions_Func4$0
            @Override // rx.functions.Func4
            public final /* synthetic */ Object call(Object obj, Object obj2, Object obj3, Object obj4) {
                return Function4.this.invoke(obj, obj2, obj3, obj4);
            }
        }, 3L, TimeUnit.SECONDS);
        m.checkNotNullExpressionValue(combineLatest, "ObservableWithLeadingEdg…3, TimeUnit.SECONDS\n    )");
        return combineLatest;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleNewData(SearchData searchData) {
        this.searchDataSubject.onNext(searchData);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void handleSubscription(Subscription subscription) {
        Subscription subscription2 = this.subscription;
        if (subscription2 != null) {
            subscription2.unsubscribe();
        }
        this.subscription = subscription;
    }

    public final void clear() {
        handleSubscription(null);
        handleNewData(new SearchData(null, null, null, null, null, 31, null));
    }

    public final Observable<SearchData> get() {
        return this.searchDataSubject;
    }

    public final void init(StoreSearch.SearchTarget searchTarget) {
        Observable<SearchData> observable;
        m.checkNotNullParameter(searchTarget, "searchTarget");
        int ordinal = searchTarget.getType().ordinal();
        if (ordinal == 0) {
            observable = getGuildSearchData(searchTarget.getId());
        } else if (ordinal == 1) {
            observable = getChannelSearchData(searchTarget.getId());
        } else {
            throw new NoWhenBranchMatchedException();
        }
        Observable q = ObservableExtensionsKt.computationBuffered(observable).q();
        m.checkNotNullExpressionValue(q, "searchDataObservable\n   …  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(q, StoreSearchData.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new StoreSearchData$init$1(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreSearchData$init$2(this));
    }
}
