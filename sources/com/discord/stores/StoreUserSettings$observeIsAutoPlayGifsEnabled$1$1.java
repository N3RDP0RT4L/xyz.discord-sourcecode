package com.discord.stores;

import androidx.core.app.NotificationCompat;
import d0.z.d.m;
import kotlin.Metadata;
import rx.functions.Func2;
/* compiled from: StoreUserSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000b\n\u0002\b\u0006\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0003\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "autoPlayGifs", "useReducedMotion", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Boolean;Ljava/lang/Boolean;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserSettings$observeIsAutoPlayGifsEnabled$1$1<T1, T2, R> implements Func2<Boolean, Boolean, Boolean> {
    public static final StoreUserSettings$observeIsAutoPlayGifsEnabled$1$1 INSTANCE = new StoreUserSettings$observeIsAutoPlayGifsEnabled$1$1();

    public final Boolean call(Boolean bool, Boolean bool2) {
        m.checkNotNullExpressionValue(bool, "autoPlayGifs");
        return Boolean.valueOf(bool.booleanValue() && !bool2.booleanValue());
    }
}
