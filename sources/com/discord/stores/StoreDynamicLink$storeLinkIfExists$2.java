package com.discord.stores;

import androidx.core.app.NotificationCompat;
import b.a.e.d;
import com.discord.stores.StoreDynamicLink;
import com.discord.utilities.analytics.AnalyticsTracker;
import kotlin.Metadata;
import rx.functions.Action1;
/* compiled from: StoreDynamicLink.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;", "it", "", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/stores/StoreDynamicLink$DynamicLinkData;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreDynamicLink$storeLinkIfExists$2<T> implements Action1<StoreDynamicLink.DynamicLinkData> {
    public static final StoreDynamicLink$storeLinkIfExists$2 INSTANCE = new StoreDynamicLink$storeLinkIfExists$2();

    public final void call(StoreDynamicLink.DynamicLinkData dynamicLinkData) {
        if (dynamicLinkData != null) {
            AnalyticsTracker analyticsTracker = AnalyticsTracker.INSTANCE;
            String fingerprint = dynamicLinkData.getFingerprint();
            String attemptId = dynamicLinkData.getAttemptId();
            String inviteCode = dynamicLinkData.getInviteCode();
            String guildTemplateCode = dynamicLinkData.getGuildTemplateCode();
            String authToken = dynamicLinkData.getAuthToken();
            d dVar = d.d;
            analyticsTracker.externalDynamicLinkReceived(fingerprint, attemptId, inviteCode, guildTemplateCode, authToken, d.a);
        }
    }
}
