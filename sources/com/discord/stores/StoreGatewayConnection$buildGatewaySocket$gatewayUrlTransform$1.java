package com.discord.stores;

import b.d.b.a.a;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.g0.w;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreGatewayConnection.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000e\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"", "gatewayUrl", "invoke", "(Ljava/lang/String;)Ljava/lang/String;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGatewayConnection$buildGatewaySocket$gatewayUrlTransform$1 extends o implements Function1<String, String> {
    public static final StoreGatewayConnection$buildGatewaySocket$gatewayUrlTransform$1 INSTANCE = new StoreGatewayConnection$buildGatewaySocket$gatewayUrlTransform$1();

    public StoreGatewayConnection$buildGatewaySocket$gatewayUrlTransform$1() {
        super(1);
    }

    public final String invoke(String str) {
        m.checkNotNullParameter(str, "gatewayUrl");
        return a.v("ws://:", w.substringAfterLast$default(str, MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR, null, 2, null));
    }
}
