package com.discord.stores;

import andhook.lib.HookHelper;
import com.discord.api.guild.Guild;
import com.discord.api.guildmember.GuildMember;
import com.discord.api.sticker.GuildStickersUpdate;
import com.discord.api.sticker.Sticker;
import com.discord.api.sticker.StickerType;
import com.discord.models.domain.ModelPayload;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import d0.d0.f;
import d0.t.g0;
import d0.t.h0;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreGuildStickers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0082\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010%\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B7\u0012\u0006\u00106\u001a\u000205\u0012\b\b\u0002\u0010=\u001a\u00020<\u0012\u001c\u0010@\u001a\u0018\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\f0\u0010\u0012\u0004\u0012\u00020\b0?¢\u0006\u0004\bB\u0010CJ)\u0010\t\u001a\u00020\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u0002¢\u0006\u0004\b\t\u0010\nJ1\u0010\u000e\u001a&\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0002j\u0002`\f\u0012\u0004\u0012\u00020\u00060\u000bj\u0002`\r0\u000b¢\u0006\u0004\b\u000e\u0010\u000fJ\u0013\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00060\u0010¢\u0006\u0004\b\u0011\u0010\u0012J-\u0010\u0013\u001a\u0016\u0012\b\u0012\u00060\u0002j\u0002`\f\u0012\u0004\u0012\u00020\u00060\u000bj\u0002`\r2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00062\n\u0010\u0015\u001a\u00060\u0002j\u0002`\f¢\u0006\u0004\b\u0016\u0010\u0017J3\u0010\u0018\u001a&\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0002j\u0002`\f\u0012\u0004\u0012\u00020\u00060\u000bj\u0002`\r0\u000bH\u0007¢\u0006\u0004\b\u0018\u0010\u000fJ3\u0010\u0019\u001a\u001a\u0012\b\u0012\u00060\u0002j\u0002`\f\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u000bj\u0004\u0018\u0001`\r2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0007¢\u0006\u0004\b\u0019\u0010\u0014J7\u0010\u001b\u001a,\u0012(\u0012&\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0002j\u0002`\f\u0012\u0004\u0012\u00020\u00060\u000bj\u0002`\r0\u000b0\u001a¢\u0006\u0004\b\u001b\u0010\u001cJ\u0017\u0010\u001f\u001a\u00020\b2\u0006\u0010\u001e\u001a\u00020\u001dH\u0007¢\u0006\u0004\b\u001f\u0010 J\u0019\u0010#\u001a\u0004\u0018\u00010\b2\u0006\u0010\"\u001a\u00020!H\u0007¢\u0006\u0004\b#\u0010$J\u001b\u0010%\u001a\u00020\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0007¢\u0006\u0004\b%\u0010&J\u0017\u0010)\u001a\u00020\b2\u0006\u0010(\u001a\u00020'H\u0007¢\u0006\u0004\b)\u0010*J\u0017\u0010,\u001a\u00020\b2\b\u0010+\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\b,\u0010-J\u000f\u0010.\u001a\u00020\bH\u0016¢\u0006\u0004\b.\u0010/R\u001a\u00101\u001a\u00060\u0002j\u0002`08\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b1\u00102R:\u00103\u001a&\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0002j\u0002`\f\u0012\u0004\u0012\u00020\u00060\u000bj\u0002`\r0\u000b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b3\u00104R\u0016\u00106\u001a\u0002058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b6\u00107R\u001c\u00108\u001a\b\u0012\u0004\u0012\u00020\u00060\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b8\u00109R:\u0010;\u001a&\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0002j\u0002`\f\u0012\u0004\u0012\u00020\u00060\u000bj\u0002`\r0:8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b;\u00104R\u0016\u0010=\u001a\u00020<8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b=\u0010>R,\u0010@\u001a\u0018\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\f0\u0010\u0012\u0004\u0012\u00020\b0?8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b@\u0010A¨\u0006D"}, d2 = {"Lcom/discord/stores/StoreGuildStickers;", "Lcom/discord/stores/StoreV2;", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "Lcom/discord/api/sticker/Sticker;", "stickers", "", "updateStickers", "(JLjava/util/Collection;)V", "", "Lcom/discord/primitives/StickerId;", "Lcom/discord/stores/StickerMap;", "getAllGuildStickers", "()Ljava/util/Map;", "", "getAllGuildStickersFlattened", "()Ljava/util/List;", "getStickersForGuild", "(J)Ljava/util/Map;", "stickerId", "getGuildSticker", "(J)Lcom/discord/api/sticker/Sticker;", "getAllGuildStickersInternal", "getStickersForGuildInternal", "Lrx/Observable;", "observeGuildStickers", "()Lrx/Observable;", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "Lcom/discord/api/guild/Guild;", "guild", "handleGuildCreateOrUpdate", "(Lcom/discord/api/guild/Guild;)Lkotlin/Unit;", "handleGuildRemove", "(J)V", "Lcom/discord/api/sticker/GuildStickersUpdate;", "stickersUpdate", "handleStickerUpdate", "(Lcom/discord/api/sticker/GuildStickersUpdate;)V", "sticker", "handleFetchedSticker", "(Lcom/discord/api/sticker/Sticker;)V", "snapshotData", "()V", "Lcom/discord/primitives/UserId;", "me", "J", "allGuildStickersSnapshot", "Ljava/util/Map;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "allGuildStickersFlattenedSnapshot", "Ljava/util/List;", "", "allGuildStickers", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lkotlin/Function1;", "onStickersDeleted", "Lkotlin/jvm/functions/Function1;", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;Lkotlin/jvm/functions/Function1;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildStickers extends StoreV2 {
    private final Map<Long, Map<Long, Sticker>> allGuildStickers;
    private List<Sticker> allGuildStickersFlattenedSnapshot;
    private Map<Long, ? extends Map<Long, Sticker>> allGuildStickersSnapshot;
    private final Dispatcher dispatcher;

    /* renamed from: me  reason: collision with root package name */
    private long f2782me;
    private final ObservationDeck observationDeck;
    private final Function1<List<Long>, Unit> onStickersDeleted;

    public /* synthetic */ StoreGuildStickers(Dispatcher dispatcher, ObservationDeck observationDeck, Function1 function1, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(dispatcher, (i & 2) != 0 ? ObservationDeckProvider.get() : observationDeck, function1);
    }

    private final void updateStickers(long j, Collection<Sticker> collection) {
        if (collection.isEmpty()) {
            this.allGuildStickers.remove(Long.valueOf(j));
        } else {
            Map<Long, Map<Long, Sticker>> map = this.allGuildStickers;
            Long valueOf = Long.valueOf(j);
            LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(o.collectionSizeOrDefault(collection, 10)), 16));
            for (Object obj : collection) {
                linkedHashMap.put(Long.valueOf(((Sticker) obj).getId()), obj);
            }
            map.put(valueOf, linkedHashMap);
        }
        markChanged();
    }

    public final Map<Long, Map<Long, Sticker>> getAllGuildStickers() {
        return this.allGuildStickersSnapshot;
    }

    public final List<Sticker> getAllGuildStickersFlattened() {
        return this.allGuildStickersFlattenedSnapshot;
    }

    @StoreThread
    public final Map<Long, Map<Long, Sticker>> getAllGuildStickersInternal() {
        return this.allGuildStickers;
    }

    public final Sticker getGuildSticker(long j) {
        Object obj;
        Iterator<T> it = getAllGuildStickers().values().iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (((Map) obj).containsKey(Long.valueOf(j))) {
                break;
            }
        }
        Map map = (Map) obj;
        if (map != null) {
            return (Sticker) map.get(Long.valueOf(j));
        }
        return null;
    }

    public final Map<Long, Sticker> getStickersForGuild(long j) {
        Map<Long, Sticker> map = getAllGuildStickers().get(Long.valueOf(j));
        return map != null ? map : h0.emptyMap();
    }

    @StoreThread
    public final Map<Long, Sticker> getStickersForGuildInternal(long j) {
        return this.allGuildStickers.get(Long.valueOf(j));
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        this.f2782me = modelPayload.getMe().i();
        for (Guild guild : modelPayload.getGuilds()) {
            m.checkNotNullExpressionValue(guild, "guild");
            handleGuildCreateOrUpdate(guild);
        }
    }

    public final void handleFetchedSticker(Sticker sticker) {
        Long g;
        if (sticker != null && sticker.k() == StickerType.GUILD && (g = sticker.g()) != null) {
            this.dispatcher.schedule(new StoreGuildStickers$handleFetchedSticker$1(this, g.longValue(), sticker));
        }
    }

    @StoreThread
    public final Unit handleGuildCreateOrUpdate(Guild guild) {
        Object obj;
        boolean z2;
        m.checkNotNullParameter(guild, "guild");
        List<GuildMember> v = guild.v();
        if (v == null) {
            return null;
        }
        Iterator<T> it = v.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (((GuildMember) obj).m().i() == this.f2782me) {
                z2 = true;
                continue;
            } else {
                z2 = false;
                continue;
            }
            if (z2) {
                break;
            }
        }
        if (((GuildMember) obj) == null) {
            return null;
        }
        long r = guild.r();
        List<Sticker> K = guild.K();
        if (K == null) {
            K = n.emptyList();
        }
        updateStickers(r, K);
        return Unit.a;
    }

    @StoreThread
    public final void handleGuildRemove(long j) {
        if (this.allGuildStickers.containsKey(Long.valueOf(j))) {
            this.allGuildStickers.remove(Long.valueOf(j));
            markChanged();
        }
    }

    @StoreThread
    public final void handleStickerUpdate(GuildStickersUpdate guildStickersUpdate) {
        Collection collection;
        m.checkNotNullParameter(guildStickersUpdate, "stickersUpdate");
        long b2 = guildStickersUpdate.b();
        List<Sticker> c = guildStickersUpdate.c();
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(c, 10));
        for (Sticker sticker : c) {
            arrayList.add(Long.valueOf(sticker.getId()));
        }
        Set set = u.toSet(arrayList);
        Map<Long, Sticker> map = this.allGuildStickersSnapshot.get(Long.valueOf(b2));
        if (map != null) {
            collection = new ArrayList(map.size());
            for (Map.Entry<Long, Sticker> entry : map.entrySet()) {
                collection.add(Long.valueOf(entry.getKey().longValue()));
            }
        } else {
            collection = n.emptyList();
        }
        ArrayList arrayList2 = new ArrayList();
        for (Object obj : collection) {
            if (!set.contains(Long.valueOf(((Number) obj).longValue()))) {
                arrayList2.add(obj);
            }
        }
        updateStickers(b2, c);
        this.onStickersDeleted.invoke(arrayList2);
    }

    public final Observable<Map<Long, Map<Long, Sticker>>> observeGuildStickers() {
        Observable<Map<Long, Map<Long, Sticker>>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreGuildStickers$observeGuildStickers$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        super.snapshotData();
        ArrayList arrayList = new ArrayList();
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry<Long, Map<Long, Sticker>> entry : this.allGuildStickers.entrySet()) {
            long longValue = entry.getKey().longValue();
            Map<Long, Sticker> value = entry.getValue();
            linkedHashMap.put(Long.valueOf(longValue), h0.toMap(value));
            arrayList.addAll(value.values());
        }
        this.allGuildStickersSnapshot = linkedHashMap;
        this.allGuildStickersFlattenedSnapshot = u.toList(arrayList);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public StoreGuildStickers(Dispatcher dispatcher, ObservationDeck observationDeck, Function1<? super List<Long>, Unit> function1) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(function1, "onStickersDeleted");
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
        this.onStickersDeleted = function1;
        this.allGuildStickers = new LinkedHashMap();
        this.allGuildStickersSnapshot = h0.emptyMap();
        this.allGuildStickersFlattenedSnapshot = n.emptyList();
    }
}
