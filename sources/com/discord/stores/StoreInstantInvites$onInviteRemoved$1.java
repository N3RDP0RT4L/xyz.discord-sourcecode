package com.discord.stores;

import com.discord.api.guild.Guild;
import com.discord.models.domain.ModelInvite;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreInstantInvites.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreInstantInvites$onInviteRemoved$1 extends o implements Function0<Unit> {
    public final /* synthetic */ ModelInvite $invite;
    public final /* synthetic */ StoreInstantInvites this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreInstantInvites$onInviteRemoved$1(StoreInstantInvites storeInstantInvites, ModelInvite modelInvite) {
        super(0);
        this.this$0 = storeInstantInvites;
        this.$invite = modelInvite;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        Map map;
        Guild guild = this.$invite.guild;
        Long valueOf = guild != null ? Long.valueOf(guild.r()) : null;
        String str = this.$invite.code;
        if (valueOf != null) {
            map = this.this$0.invites;
            Map map2 = (Map) map.get(valueOf);
            if (map2 != null && ((ModelInvite) map2.remove(str)) != null) {
                this.this$0.markChanged();
            }
        }
    }
}
