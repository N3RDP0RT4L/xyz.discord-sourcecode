package com.discord.stores;

import com.discord.models.commands.ApplicationCommandLocalSendData;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreApplicationInteractions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreApplicationInteractions$sendApplicationCommand$1 extends o implements Function0<Unit> {
    public final /* synthetic */ List $attachments;
    public final /* synthetic */ ApplicationCommandLocalSendData $localSendData;
    public final /* synthetic */ Function1 $onFail;
    public final /* synthetic */ Function0 $onSuccess;
    public final /* synthetic */ StoreApplicationInteractions this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreApplicationInteractions$sendApplicationCommand$1(StoreApplicationInteractions storeApplicationInteractions, ApplicationCommandLocalSendData applicationCommandLocalSendData, List list, Function0 function0, Function1 function1) {
        super(0);
        this.this$0 = storeApplicationInteractions;
        this.$localSendData = applicationCommandLocalSendData;
        this.$attachments = list;
        this.$onSuccess = function0;
        this.$onFail = function1;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        this.this$0.handleSendApplicationCommandRequest(this.$localSendData, this.$attachments, this.$onSuccess, this.$onFail);
    }
}
