package com.discord.stores;

import com.discord.stores.StoreMFA;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreMFA.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/stores/StoreMFA$State;", "invoke", "()Lcom/discord/stores/StoreMFA$State;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMFA$observeState$1 extends o implements Function0<StoreMFA.State> {
    public final /* synthetic */ StoreMFA this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreMFA$observeState$1(StoreMFA storeMFA) {
        super(0);
        this.this$0 = storeMFA;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final StoreMFA.State invoke() {
        StoreMFA.State state;
        state = this.this$0.state;
        return state;
    }
}
