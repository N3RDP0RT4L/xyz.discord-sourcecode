package com.discord.stores;

import andhook.lib.HookHelper;
import com.discord.api.emoji.GuildEmoji;
import com.discord.api.emoji.GuildEmojisUpdate;
import com.discord.api.guild.Guild;
import com.discord.models.domain.ModelPayload;
import com.discord.models.domain.emoji.ModelEmojiCustom;
import com.discord.models.member.GuildMember;
import com.discord.utilities.persister.Persister;
import d0.d0.f;
import d0.t.g0;
import d0.t.h0;
import d0.t.n;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreEmojiCustom.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u008c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010%\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 B2\u00020\u0001:\u0001BB\u000f\u0012\u0006\u00105\u001a\u000204¢\u0006\u0004\b@\u0010AJ)\u0010\t\u001a\u00020\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u0002¢\u0006\u0004\b\t\u0010\nJ;\u0010\u000f\u001a\u00020\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0010\u0010\r\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\f0\u000b2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u0002¢\u0006\u0004\b\u000f\u0010\u0010J1\u0010\u0014\u001a&\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0002j\u0002`\u0012\u0012\u0004\u0012\u00020\u00060\u0011j\u0002`\u00130\u0011¢\u0006\u0004\b\u0014\u0010\u0015J-\u0010\u0016\u001a\u0016\u0012\b\u0012\u00060\u0002j\u0002`\u0012\u0012\u0004\u0012\u00020\u00060\u0011j\u0002`\u00132\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0016\u0010\u0017J7\u0010\u0019\u001a,\u0012(\u0012&\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0002j\u0002`\u0012\u0012\u0004\u0012\u00020\u00060\u0011j\u0002`\u00130\u00110\u0018¢\u0006\u0004\b\u0019\u0010\u001aJ3\u0010\u001b\u001a&\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0002j\u0002`\u0012\u0012\u0004\u0012\u00020\u00060\u0011j\u0002`\u00130\u0011H\u0007¢\u0006\u0004\b\u001b\u0010\u0015J3\u0010\u001c\u001a\u001a\u0012\b\u0012\u00060\u0002j\u0002`\u0012\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0011j\u0004\u0018\u0001`\u00132\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0007¢\u0006\u0004\b\u001c\u0010\u0017J\u0017\u0010\u001f\u001a\u00020\b2\u0006\u0010\u001e\u001a\u00020\u001dH\u0007¢\u0006\u0004\b\u001f\u0010 J\u0019\u0010#\u001a\u0004\u0018\u00010\b2\u0006\u0010\"\u001a\u00020!H\u0007¢\u0006\u0004\b#\u0010$J\u0017\u0010%\u001a\u00020\b2\u0006\u0010\"\u001a\u00020!H\u0007¢\u0006\u0004\b%\u0010&J\u0017\u0010)\u001a\u00020\b2\u0006\u0010(\u001a\u00020'H\u0007¢\u0006\u0004\b)\u0010*J\u0017\u0010-\u001a\u00020\b2\u0006\u0010,\u001a\u00020+H\u0007¢\u0006\u0004\b-\u0010.J\u000f\u0010/\u001a\u00020\bH\u0016¢\u0006\u0004\b/\u00100R\u001a\u00102\u001a\u00060\u0002j\u0002`18\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b2\u00103R\u0016\u00105\u001a\u0002048\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b5\u00106R:\u00107\u001a&\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0002j\u0002`\u0012\u0012\u0004\u0012\u00020\u00060\u0011j\u0002`\u00130\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b7\u00108R:\u0010:\u001a&\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0002j\u0002`\u0012\u0012\u0004\u0012\u00020\u00060\u0011j\u0002`\u0013098\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b:\u00108RJ\u0010<\u001a6\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012(\u0012&\u0012\b\u0012\u00060\u0002j\u0002`\f\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0002j\u0002`\u0012\u0012\u0004\u0012\u00020\u000609j\u0002`;09098\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b<\u00108R@\u0010>\u001a,\u0012(\u0012&\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0002j\u0002`\u0012\u0012\u0004\u0012\u00020\u00060\u0011j\u0002`\u00130\u00110=8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b>\u0010?¨\u0006C"}, d2 = {"Lcom/discord/stores/StoreEmojiCustom;", "Lcom/discord/stores/StoreV2;", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "Lcom/discord/models/domain/emoji/ModelEmojiCustom;", "emojis", "", "updateGlobalEmojis", "(JLjava/util/Collection;)V", "", "Lcom/discord/primitives/RoleId;", "myRoles", "guildEmojis", "updateAllowedGuildEmojis", "(JLjava/util/List;Ljava/util/Collection;)V", "", "Lcom/discord/primitives/EmojiId;", "Lcom/discord/stores/EmojiMap;", "getAllGuildEmoji", "()Ljava/util/Map;", "getEmojiForGuild", "(J)Ljava/util/Map;", "Lrx/Observable;", "observeAllowedGuildEmoji", "()Lrx/Observable;", "getAllGuildEmojiInternal", "getEmojiForGuildInternal", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "Lcom/discord/api/guild/Guild;", "guild", "handleGuildAdd", "(Lcom/discord/api/guild/Guild;)Lkotlin/Unit;", "handleGuildRemove", "(Lcom/discord/api/guild/Guild;)V", "Lcom/discord/api/guildmember/GuildMember;", "member", "handleGuildMemberAdd", "(Lcom/discord/api/guildmember/GuildMember;)V", "Lcom/discord/api/emoji/GuildEmojisUpdate;", "emojiUpdate", "handleEmojiUpdate", "(Lcom/discord/api/emoji/GuildEmojisUpdate;)V", "snapshotData", "()V", "Lcom/discord/primitives/UserId;", "me", "J", "Lcom/discord/stores/StoreStream;", "stream", "Lcom/discord/stores/StoreStream;", "allGuildEmojiSnapshot", "Ljava/util/Map;", "", "allGuildEmoji", "Lcom/discord/stores/EmojiMutableMap;", "allowedGuildEmojis", "Lcom/discord/utilities/persister/Persister;", "allowedGuildEmojiPersister", "Lcom/discord/utilities/persister/Persister;", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreEmojiCustom extends StoreV2 {
    public static final Companion Companion = new Companion(null);
    private static final long NO_ROLE = 0;

    /* renamed from: me  reason: collision with root package name */
    private long f2781me;
    private final StoreStream stream;
    private final Map<Long, Map<Long, ModelEmojiCustom>> allGuildEmoji = new LinkedHashMap();
    private Map<Long, ? extends Map<Long, ? extends ModelEmojiCustom>> allGuildEmojiSnapshot = h0.emptyMap();
    private final Map<Long, Map<Long, Map<Long, ModelEmojiCustom>>> allowedGuildEmojis = new LinkedHashMap();
    private final Persister<Map<Long, Map<Long, ModelEmojiCustom>>> allowedGuildEmojiPersister = new Persister<>("STORE_EMOJI_AVAILABLE_V5", new HashMap());

    /* compiled from: StoreEmojiCustom.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u001a\u0010\u0004\u001a\u00060\u0002j\u0002`\u00038\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0004\u0010\u0005¨\u0006\b"}, d2 = {"Lcom/discord/stores/StoreEmojiCustom$Companion;", "", "", "Lcom/discord/primitives/RoleId;", "NO_ROLE", "J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public StoreEmojiCustom(StoreStream storeStream) {
        m.checkNotNullParameter(storeStream, "stream");
        this.stream = storeStream;
    }

    private final void updateAllowedGuildEmojis(long j, List<Long> list, Collection<? extends ModelEmojiCustom> collection) {
        Object obj;
        Map<Long, Map<Long, Map<Long, ModelEmojiCustom>>> map = this.allowedGuildEmojis;
        Long valueOf = Long.valueOf(j);
        Map<Long, Map<Long, ModelEmojiCustom>> map2 = map.get(valueOf);
        if (map2 == null) {
            map2 = new HashMap<>();
            map.put(valueOf, map2);
        }
        Map<Long, Map<Long, ModelEmojiCustom>> map3 = map2;
        map3.clear();
        StoreEmojiCustom$updateAllowedGuildEmojis$1 storeEmojiCustom$updateAllowedGuildEmojis$1 = new StoreEmojiCustom$updateAllowedGuildEmojis$1(this, map3);
        for (ModelEmojiCustom modelEmojiCustom : collection) {
            List<Long> roles = modelEmojiCustom.getRoles();
            if (roles.isEmpty()) {
                storeEmojiCustom$updateAllowedGuildEmojis$1.invoke(0L, modelEmojiCustom);
            } else {
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj = null;
                        break;
                    }
                    obj = it.next();
                    if (roles.contains(Long.valueOf(((Number) obj).longValue()))) {
                        break;
                    }
                }
                Long l = (Long) obj;
                if (l != null) {
                    storeEmojiCustom$updateAllowedGuildEmojis$1.invoke(l.longValue(), modelEmojiCustom);
                }
            }
        }
    }

    private final void updateGlobalEmojis(long j, Collection<? extends ModelEmojiCustom> collection) {
        if (collection.isEmpty()) {
            this.allGuildEmoji.remove(Long.valueOf(j));
        } else {
            Map<Long, Map<Long, ModelEmojiCustom>> map = this.allGuildEmoji;
            Long valueOf = Long.valueOf(j);
            LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(o.collectionSizeOrDefault(collection, 10)), 16));
            for (Object obj : collection) {
                linkedHashMap.put(Long.valueOf(((ModelEmojiCustom) obj).getId()), obj);
            }
            map.put(valueOf, linkedHashMap);
        }
        markChanged();
    }

    public final Map<Long, Map<Long, ModelEmojiCustom>> getAllGuildEmoji() {
        return this.allGuildEmojiSnapshot;
    }

    @StoreThread
    public final Map<Long, Map<Long, ModelEmojiCustom>> getAllGuildEmojiInternal() {
        return this.allGuildEmoji;
    }

    public final Map<Long, ModelEmojiCustom> getEmojiForGuild(long j) {
        Map<Long, ModelEmojiCustom> map = getAllGuildEmoji().get(Long.valueOf(j));
        return map != null ? map : h0.emptyMap();
    }

    @StoreThread
    public final Map<Long, ModelEmojiCustom> getEmojiForGuildInternal(long j) {
        return this.allGuildEmoji.get(Long.valueOf(j));
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        this.f2781me = modelPayload.getMe().i();
        for (Guild guild : modelPayload.getGuilds()) {
            m.checkNotNullExpressionValue(guild, "guild");
            handleGuildAdd(guild);
        }
    }

    @StoreThread
    public final void handleEmojiUpdate(GuildEmojisUpdate guildEmojisUpdate) {
        GuildMember guildMember;
        m.checkNotNullParameter(guildEmojisUpdate, "emojiUpdate");
        long c = guildEmojisUpdate.c();
        Map<Long, GuildMember> map = this.stream.getGuilds$app_productionGoogleRelease().getGuildMembersComputedInternal$app_productionGoogleRelease().get(Long.valueOf(c));
        List<GuildEmoji> a = guildEmojisUpdate.a();
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(a, 10));
        for (GuildEmoji guildEmoji : a) {
            arrayList.add(new ModelEmojiCustom(guildEmoji, c));
        }
        if (!(map == null || (guildMember = map.get(Long.valueOf(this.f2781me))) == null)) {
            updateGlobalEmojis(c, arrayList);
            updateAllowedGuildEmojis(c, guildMember.getRoles(), arrayList);
        }
    }

    @StoreThread
    public final Unit handleGuildAdd(Guild guild) {
        Object obj;
        Collection<? extends ModelEmojiCustom> collection;
        boolean z2;
        m.checkNotNullParameter(guild, "guild");
        List<com.discord.api.guildmember.GuildMember> v = guild.v();
        if (v == null) {
            return null;
        }
        Iterator<T> it = v.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (((com.discord.api.guildmember.GuildMember) obj).m().i() == this.f2781me) {
                z2 = true;
                continue;
            } else {
                z2 = false;
                continue;
            }
            if (z2) {
                break;
            }
        }
        com.discord.api.guildmember.GuildMember guildMember = (com.discord.api.guildmember.GuildMember) obj;
        if (guildMember == null) {
            return null;
        }
        long r = guild.r();
        List<GuildEmoji> k = guild.k();
        if (k != null) {
            collection = new ArrayList<>(o.collectionSizeOrDefault(k, 10));
            for (GuildEmoji guildEmoji : k) {
                collection.add(new ModelEmojiCustom(guildEmoji, r));
            }
        } else {
            collection = n.emptyList();
        }
        updateGlobalEmojis(r, collection);
        updateAllowedGuildEmojis(r, guildMember.l(), collection);
        return Unit.a;
    }

    @StoreThread
    public final void handleGuildMemberAdd(com.discord.api.guildmember.GuildMember guildMember) {
        Collection<ModelEmojiCustom> values;
        m.checkNotNullParameter(guildMember, "member");
        if (guildMember.m().i() == this.f2781me) {
            long f = guildMember.f();
            Map<Long, ModelEmojiCustom> map = this.allGuildEmoji.get(Long.valueOf(f));
            if (map != null && (values = map.values()) != null) {
                updateAllowedGuildEmojis(f, guildMember.l(), values);
            }
        }
    }

    @StoreThread
    public final void handleGuildRemove(Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        long r = guild.r();
        if (this.allowedGuildEmojis.containsKey(Long.valueOf(r))) {
            this.allowedGuildEmojis.remove(Long.valueOf(r));
            markChanged();
        }
        if (this.allGuildEmoji.containsKey(Long.valueOf(r))) {
            this.allGuildEmoji.remove(Long.valueOf(r));
            markChanged();
        }
    }

    public final Observable<Map<Long, Map<Long, ModelEmojiCustom>>> observeAllowedGuildEmoji() {
        return this.allowedGuildEmojiPersister.getObservable();
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        super.snapshotData();
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry<Long, Map<Long, ModelEmojiCustom>> entry : this.allGuildEmoji.entrySet()) {
            linkedHashMap.put(Long.valueOf(entry.getKey().longValue()), h0.toMap(entry.getValue()));
        }
        this.allGuildEmojiSnapshot = linkedHashMap;
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        for (Map.Entry<Long, Map<Long, Map<Long, ModelEmojiCustom>>> entry2 : this.allowedGuildEmojis.entrySet()) {
            long longValue = entry2.getKey().longValue();
            LinkedHashMap linkedHashMap3 = new LinkedHashMap();
            for (Map.Entry<Long, Map<Long, ModelEmojiCustom>> entry3 : entry2.getValue().entrySet()) {
                linkedHashMap3.putAll(entry3.getValue());
            }
            linkedHashMap2.put(Long.valueOf(longValue), linkedHashMap3);
        }
        Persister.set$default(this.allowedGuildEmojiPersister, linkedHashMap2, false, 2, null);
    }
}
