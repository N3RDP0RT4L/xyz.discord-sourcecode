package com.discord.stores;

import andhook.lib.HookHelper;
import andhook.lib.xposed.ClassUtils;
import com.discord.BuildConfig;
import com.discord.api.embeddedactivities.EmbeddedActivityInGuild;
import com.discord.api.embeddedactivities.EmbeddedActivityInboundUpdate;
import com.discord.api.guild.Guild;
import com.discord.models.domain.ModelPayload;
import com.discord.models.embeddedactivities.EmbeddedActivity;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import d0.t.h0;
import d0.t.n;
import d0.z.d.m;
import j0.k.b;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreEmbeddedActivities.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010%\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0011\u0012\b\b\u0002\u00103\u001a\u000202¢\u0006\u0004\b5\u00106J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0005\u0010\u0006JE\u0010\u0012\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\u0010\u0010\f\u001a\f\u0012\b\u0012\u00060\u0007j\u0002`\u000b0\n2\n\u0010\u000f\u001a\u00060\rj\u0002`\u000e2\n\u0010\u0011\u001a\u00060\u0007j\u0002`\u0010H\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u001d\u0010\u0017\u001a\u0004\u0018\u00010\u00162\n\u0010\u0015\u001a\u00060\u0007j\u0002`\u0014H\u0002¢\u0006\u0004\b\u0017\u0010\u0018J1\u0010\u001c\u001a&\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0007j\u0002`\u0014\u0012\b\u0012\u00060\u001aj\u0002`\u001b0\u00190\u0019¢\u0006\u0004\b\u001c\u0010\u001dJ7\u0010\u001f\u001a,\u0012(\u0012&\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0007j\u0002`\u0014\u0012\b\u0012\u00060\u001aj\u0002`\u001b0\u00190\u00190\u001e¢\u0006\u0004\b\u001f\u0010 J3\u0010!\u001a\u001c\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0007j\u0002`\u0014\u0012\b\u0012\u00060\u001aj\u0002`\u001b0\u00190\u001e2\n\u0010\t\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\b!\u0010\"J\u0017\u0010%\u001a\u00020\u00042\u0006\u0010$\u001a\u00020#H\u0007¢\u0006\u0004\b%\u0010&J\u0017\u0010'\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b'\u0010\u0006J\u0017\u0010*\u001a\u00020\u00042\u0006\u0010)\u001a\u00020(H\u0007¢\u0006\u0004\b*\u0010+J\u000f\u0010,\u001a\u00020\u0004H\u0017¢\u0006\u0004\b,\u0010-R:\u0010.\u001a&\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0007j\u0002`\u0014\u0012\b\u0012\u00060\u001aj\u0002`\u001b0\u00190\u00198\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b.\u0010/R:\u00101\u001a&\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0007j\u0002`\u0014\u0012\b\u0012\u00060\u001aj\u0002`\u001b00008\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b1\u0010/R\u0016\u00103\u001a\u0002028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b3\u00104¨\u00067"}, d2 = {"Lcom/discord/stores/StoreEmbeddedActivities;", "Lcom/discord/stores/StoreV2;", "Lcom/discord/api/guild/Guild;", "guild", "", "handleEmbeddedActivitiesForGuild", "(Lcom/discord/api/guild/Guild;)V", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "Lcom/discord/primitives/UserId;", "userIds", "Lcom/discord/api/embeddedactivities/EmbeddedActivity;", "Lcom/discord/stores/ApiEmbeddedActivity;", "apiEmbeddedActivity", "Lcom/discord/primitives/GuildId;", "guildId", "handleEmbeddedActivityForChannel", "(JLjava/util/List;Lcom/discord/api/embeddedactivities/EmbeddedActivity;J)V", "Lcom/discord/primitives/ApplicationId;", "applicationId", "", "getUrlForApplication", "(J)Ljava/lang/String;", "", "Lcom/discord/models/embeddedactivities/EmbeddedActivity;", "Lcom/discord/stores/ClientEmbeddedActivity;", "getEmbeddedActivities", "()Ljava/util/Map;", "Lrx/Observable;", "observeEmbeddedActivities", "()Lrx/Observable;", "observeEmbeddedActivitiesForChannel", "(J)Lrx/Observable;", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "handleGuildCreate", "Lcom/discord/api/embeddedactivities/EmbeddedActivityInboundUpdate;", "embeddedActivityInboundUpdate", "handleEmbeddedActivityInboundUpdate", "(Lcom/discord/api/embeddedactivities/EmbeddedActivityInboundUpdate;)V", "snapshotData", "()V", "embeddedActivitiesByChannelSnapshot", "Ljava/util/Map;", "", "embeddedActivitiesByChannel", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", HookHelper.constructorName, "(Lcom/discord/stores/updates/ObservationDeck;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreEmbeddedActivities extends StoreV2 {
    private final Map<Long, Map<Long, EmbeddedActivity>> embeddedActivitiesByChannel;
    private Map<Long, ? extends Map<Long, EmbeddedActivity>> embeddedActivitiesByChannelSnapshot;
    private final ObservationDeck observationDeck;

    public StoreEmbeddedActivities() {
        this(null, 1, null);
    }

    public /* synthetic */ StoreEmbeddedActivities(ObservationDeck observationDeck, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? ObservationDeckProvider.get() : observationDeck);
    }

    private final String getUrlForApplication(long j) {
        if (BuildConfig.EMBEDDED_ACTIVITY_APPLICATION_HOST.length() == 0) {
            return null;
        }
        return "https://" + j + ClassUtils.PACKAGE_SEPARATOR_CHAR + BuildConfig.EMBEDDED_ACTIVITY_APPLICATION_HOST;
    }

    @StoreThread
    private final void handleEmbeddedActivitiesForGuild(Guild guild) {
        List<EmbeddedActivityInGuild> j = guild.j();
        if (j == null) {
            j = n.emptyList();
        }
        for (EmbeddedActivityInGuild embeddedActivityInGuild : j) {
            handleEmbeddedActivityForChannel(embeddedActivityInGuild.a(), embeddedActivityInGuild.c(), embeddedActivityInGuild.b(), guild.r());
        }
    }

    @StoreThread
    private final void handleEmbeddedActivityForChannel(long j, List<Long> list, com.discord.api.embeddedactivities.EmbeddedActivity embeddedActivity, long j2) {
        String urlForApplication = getUrlForApplication(embeddedActivity.a());
        if (urlForApplication != null) {
            EmbeddedActivity fromApiEmbeddedActivity = EmbeddedActivity.Companion.fromApiEmbeddedActivity(embeddedActivity, list, urlForApplication, j2);
            Map<Long, EmbeddedActivity> map = this.embeddedActivitiesByChannel.get(Long.valueOf(j));
            if (map == null) {
                map = new LinkedHashMap<>();
            }
            long applicationId = fromApiEmbeddedActivity.getApplicationId();
            if (!list.isEmpty()) {
                map.put(Long.valueOf(applicationId), fromApiEmbeddedActivity);
            } else {
                map.remove(Long.valueOf(applicationId));
            }
            this.embeddedActivitiesByChannel.put(Long.valueOf(j), map);
            markChanged();
        }
    }

    public final Map<Long, Map<Long, EmbeddedActivity>> getEmbeddedActivities() {
        return this.embeddedActivitiesByChannelSnapshot;
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        this.embeddedActivitiesByChannel.clear();
        List<Guild> guilds = modelPayload.getGuilds();
        m.checkNotNullExpressionValue(guilds, "payload.guilds");
        for (Guild guild : guilds) {
            m.checkNotNullExpressionValue(guild, "guild");
            handleEmbeddedActivitiesForGuild(guild);
        }
        markChanged();
    }

    @StoreThread
    public final void handleEmbeddedActivityInboundUpdate(EmbeddedActivityInboundUpdate embeddedActivityInboundUpdate) {
        m.checkNotNullParameter(embeddedActivityInboundUpdate, "embeddedActivityInboundUpdate");
        handleEmbeddedActivityForChannel(embeddedActivityInboundUpdate.a(), embeddedActivityInboundUpdate.d(), embeddedActivityInboundUpdate.b(), embeddedActivityInboundUpdate.c());
    }

    @StoreThread
    public final void handleGuildCreate(Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        handleEmbeddedActivitiesForGuild(guild);
    }

    public final Observable<Map<Long, Map<Long, EmbeddedActivity>>> observeEmbeddedActivities() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreEmbeddedActivities$observeEmbeddedActivities$1(this), 14, null);
    }

    public final Observable<Map<Long, EmbeddedActivity>> observeEmbeddedActivitiesForChannel(final long j) {
        Observable F = observeEmbeddedActivities().F(new b<Map<Long, ? extends Map<Long, ? extends EmbeddedActivity>>, Map<Long, ? extends EmbeddedActivity>>() { // from class: com.discord.stores.StoreEmbeddedActivities$observeEmbeddedActivitiesForChannel$1
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Map<Long, ? extends EmbeddedActivity> call(Map<Long, ? extends Map<Long, ? extends EmbeddedActivity>> map) {
                return call2((Map<Long, ? extends Map<Long, EmbeddedActivity>>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Map<Long, EmbeddedActivity> call2(Map<Long, ? extends Map<Long, EmbeddedActivity>> map) {
                Map<Long, EmbeddedActivity> map2 = map.get(Long.valueOf(j));
                return map2 != null ? map2 : h0.emptyMap();
            }
        });
        m.checkNotNullExpressionValue(F, "observeEmbeddedActivitie…] ?: emptyMap()\n        }");
        return F;
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        super.snapshotData();
        HashMap hashMap = new HashMap();
        for (Map.Entry<Long, Map<Long, EmbeddedActivity>> entry : this.embeddedActivitiesByChannel.entrySet()) {
            hashMap.put(entry.getKey(), new HashMap(entry.getValue()));
        }
        this.embeddedActivitiesByChannelSnapshot = hashMap;
    }

    public StoreEmbeddedActivities(ObservationDeck observationDeck) {
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.observationDeck = observationDeck;
        this.embeddedActivitiesByChannelSnapshot = h0.emptyMap();
        this.embeddedActivitiesByChannel = new LinkedHashMap();
    }
}
