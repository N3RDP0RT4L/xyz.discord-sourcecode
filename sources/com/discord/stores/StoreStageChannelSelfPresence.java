package com.discord.stores;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import com.discord.api.activity.Activity;
import com.discord.api.activity.ActivityParty;
import com.discord.api.voice.state.VoiceState;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.presence.ActivityUtilsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import j0.k.b;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import rx.Observable;
import rx.functions.Func2;
import rx.subjects.BehaviorSubject;
/* compiled from: StoreStageChannelSelfPresence.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B?\u0012\u0006\u00104\u001a\u000203\u0012\u0006\u00109\u001a\u000208\u0012\u0006\u0010%\u001a\u00020$\u0012\u0006\u0010\u001b\u001a\u00020\u001a\u0012\u0006\u0010*\u001a\u00020)\u0012\u0006\u0010 \u001a\u00020\u001f\u0012\u0006\u0010/\u001a\u00020.¢\u0006\u0004\b=\u0010>J\u000f\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\r\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0004J\u0017\u0010\b\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0006H\u0007¢\u0006\u0004\b\b\u0010\tJ\u000f\u0010\n\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\n\u0010\u0004J\u000f\u0010\u000b\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u000b\u0010\u0004J\u000f\u0010\f\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\f\u0010\u0004J\u000f\u0010\r\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\r\u0010\u0004R$\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00020\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u0019\u0010\u001b\u001a\u00020\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u0019\u0010 \u001a\u00020\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#R\u0019\u0010%\u001a\u00020$8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010&\u001a\u0004\b'\u0010(R\u0019\u0010*\u001a\u00020)8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010+\u001a\u0004\b,\u0010-R\u0019\u0010/\u001a\u00020.8\u0006@\u0006¢\u0006\f\n\u0004\b/\u00100\u001a\u0004\b1\u00102R\u0019\u00104\u001a\u0002038\u0006@\u0006¢\u0006\f\n\u0004\b4\u00105\u001a\u0004\b6\u00107R\u0019\u00109\u001a\u0002088\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010:\u001a\u0004\b;\u0010<¨\u0006?"}, d2 = {"Lcom/discord/stores/StoreStageChannelSelfPresence;", "Lcom/discord/stores/StoreV2;", "", "updateActivity", "()V", "init", "Lcom/discord/api/voice/state/VoiceState;", "voiceState", "handleVoiceStateUpdate", "(Lcom/discord/api/voice/state/VoiceState;)V", "handleStageInstanceCreate", "handleStageInstanceUpdate", "handleStageInstanceDelete", "handleVoiceChannelSelected", "Lcom/discord/api/activity/Activity;", "stageChannelActivity", "Lcom/discord/api/activity/Activity;", "getStageChannelActivity", "()Lcom/discord/api/activity/Activity;", "setStageChannelActivity", "(Lcom/discord/api/activity/Activity;)V", "Lrx/subjects/BehaviorSubject;", "publishStateTrigger", "Lrx/subjects/BehaviorSubject;", "getPublishStateTrigger", "()Lrx/subjects/BehaviorSubject;", "Lcom/discord/stores/StoreStageChannels;", "stageChannels", "Lcom/discord/stores/StoreStageChannels;", "getStageChannels", "()Lcom/discord/stores/StoreStageChannels;", "Lcom/discord/stores/StoreUserSettings;", "userSettings", "Lcom/discord/stores/StoreUserSettings;", "getUserSettings", "()Lcom/discord/stores/StoreUserSettings;", "Lcom/discord/stores/StoreUserPresence;", "userPresence", "Lcom/discord/stores/StoreUserPresence;", "getUserPresence", "()Lcom/discord/stores/StoreUserPresence;", "Lcom/discord/stores/StoreVoiceChannelSelected;", "voiceChannelSelected", "Lcom/discord/stores/StoreVoiceChannelSelected;", "getVoiceChannelSelected", "()Lcom/discord/stores/StoreVoiceChannelSelected;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "getDispatcher", "()Lcom/discord/stores/Dispatcher;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "getObservationDeck", "()Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/stores/StoreUser;", "userStore", "Lcom/discord/stores/StoreUser;", "getUserStore", "()Lcom/discord/stores/StoreUser;", HookHelper.constructorName, "(Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreUserPresence;Lcom/discord/stores/StoreStageChannels;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/Dispatcher;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStageChannelSelfPresence extends StoreV2 {
    private final Dispatcher dispatcher;
    private final ObservationDeck observationDeck;
    private final BehaviorSubject<Unit> publishStateTrigger;
    private Activity stageChannelActivity;
    private final StoreStageChannels stageChannels;
    private final StoreUserPresence userPresence;
    private final StoreUserSettings userSettings;
    private final StoreUser userStore;
    private final StoreVoiceChannelSelected voiceChannelSelected;

    public StoreStageChannelSelfPresence(ObservationDeck observationDeck, StoreUser storeUser, StoreUserPresence storeUserPresence, StoreStageChannels storeStageChannels, StoreVoiceChannelSelected storeVoiceChannelSelected, StoreUserSettings storeUserSettings, Dispatcher dispatcher) {
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(storeUser, "userStore");
        m.checkNotNullParameter(storeUserPresence, "userPresence");
        m.checkNotNullParameter(storeStageChannels, "stageChannels");
        m.checkNotNullParameter(storeVoiceChannelSelected, "voiceChannelSelected");
        m.checkNotNullParameter(storeUserSettings, "userSettings");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        this.observationDeck = observationDeck;
        this.userStore = storeUser;
        this.userPresence = storeUserPresence;
        this.stageChannels = storeStageChannels;
        this.voiceChannelSelected = storeVoiceChannelSelected;
        this.userSettings = storeUserSettings;
        this.dispatcher = dispatcher;
        BehaviorSubject<Unit> k0 = BehaviorSubject.k0();
        m.checkNotNullExpressionValue(k0, "BehaviorSubject.create()");
        this.publishStateTrigger = k0;
    }

    @StoreThread
    private final void updateActivity() {
        ActivityParty i;
        ActivityParty i2;
        Activity createStageChannelListeningActivity = ActivityUtilsKt.createStageChannelListeningActivity();
        String str = null;
        String a = (createStageChannelListeningActivity == null || (i2 = createStageChannelListeningActivity.i()) == null) ? null : i2.a();
        Activity activity = this.stageChannelActivity;
        if (!(!m.areEqual(a, (activity == null || (i = activity.i()) == null) ? null : i.a()))) {
            String h = createStageChannelListeningActivity != null ? createStageChannelListeningActivity.h() : null;
            Activity activity2 = this.stageChannelActivity;
            if (activity2 != null) {
                str = activity2.h();
            }
            if (!(!m.areEqual(h, str))) {
                return;
            }
        }
        this.stageChannelActivity = createStageChannelListeningActivity;
        this.publishStateTrigger.onNext(Unit.a);
    }

    public final Dispatcher getDispatcher() {
        return this.dispatcher;
    }

    public final ObservationDeck getObservationDeck() {
        return this.observationDeck;
    }

    public final BehaviorSubject<Unit> getPublishStateTrigger() {
        return this.publishStateTrigger;
    }

    public final Activity getStageChannelActivity() {
        return this.stageChannelActivity;
    }

    public final StoreStageChannels getStageChannels() {
        return this.stageChannels;
    }

    public final StoreUserPresence getUserPresence() {
        return this.userPresence;
    }

    public final StoreUserSettings getUserSettings() {
        return this.userSettings;
    }

    public final StoreUser getUserStore() {
        return this.userStore;
    }

    public final StoreVoiceChannelSelected getVoiceChannelSelected() {
        return this.voiceChannelSelected;
    }

    @StoreThread
    public final void handleStageInstanceCreate() {
        updateActivity();
    }

    @StoreThread
    public final void handleStageInstanceDelete() {
        updateActivity();
    }

    @StoreThread
    public final void handleStageInstanceUpdate() {
        updateActivity();
    }

    @StoreThread
    public final void handleVoiceChannelSelected() {
        updateActivity();
    }

    @StoreThread
    public final void handleVoiceStateUpdate(VoiceState voiceState) {
        m.checkNotNullParameter(voiceState, "voiceState");
        if (voiceState.m() == this.userStore.getMe().getId()) {
            updateActivity();
        }
    }

    public final void init() {
        Observable Y = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this.stageChannels, this.voiceChannelSelected}, false, null, null, new StoreStageChannelSelfPresence$init$1(this), 14, null).q().Y(new b<Integer, Observable<? extends Boolean>>() { // from class: com.discord.stores.StoreStageChannelSelfPresence$init$2

            /* compiled from: StoreStageChannelSelfPresence.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\u0010\u0007\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0004\u001a\n \u0001*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "kotlin.jvm.PlatformType", "<anonymous parameter 0>", "", "isEnabled", NotificationCompat.CATEGORY_CALL, "(Lkotlin/Unit;Ljava/lang/Boolean;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.stores.StoreStageChannelSelfPresence$init$2$1  reason: invalid class name */
            /* loaded from: classes.dex */
            public static final class AnonymousClass1<T1, T2, R> implements Func2<Unit, Boolean, Boolean> {
                public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                public final Boolean call(Unit unit, Boolean bool) {
                    return bool;
                }
            }

            public final Observable<? extends Boolean> call(Integer num) {
                return Observable.j(ObservableExtensionsKt.leadingEdgeThrottle(StoreStageChannelSelfPresence.this.getPublishStateTrigger(), num.intValue(), TimeUnit.SECONDS), StoreStageChannelSelfPresence.this.getUserSettings().observeIsShowCurrentGameEnabled(), AnonymousClass1.INSTANCE);
            }
        });
        m.checkNotNullExpressionValue(Y, "observationDeck.connectR…bled -> isEnabled }\n    }");
        ObservableExtensionsKt.appSubscribe(Y, StoreStageChannelSelfPresence.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreStageChannelSelfPresence$init$3(this));
    }

    public final void setStageChannelActivity(Activity activity) {
        this.stageChannelActivity = activity;
    }
}
