package com.discord.stores;

import com.discord.rtcconnection.mediaengine.MediaEngine;
import d0.z.d.k;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.subjects.SerializedSubject;
/* compiled from: StoreMediaEngine.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;", "kotlin.jvm.PlatformType", "p1", "", "invoke", "(Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final /* synthetic */ class StoreMediaEngine$enableLocalVoiceStatusListening$1 extends k implements Function1<MediaEngine.LocalVoiceStatus, Unit> {
    public StoreMediaEngine$enableLocalVoiceStatusListening$1(SerializedSubject serializedSubject) {
        super(1, serializedSubject, SerializedSubject.class, "onNext", "onNext(Ljava/lang/Object;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(MediaEngine.LocalVoiceStatus localVoiceStatus) {
        invoke2(localVoiceStatus);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(MediaEngine.LocalVoiceStatus localVoiceStatus) {
        ((SerializedSubject) this.receiver).k.onNext(localVoiceStatus);
    }
}
