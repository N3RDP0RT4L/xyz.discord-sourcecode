package com.discord.stores;

import andhook.lib.HookHelper;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Patterns;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import b.d.b.a.a;
import com.discord.analytics.generated.events.network_action.TrackNetworkActionUserLogin;
import com.discord.analytics.generated.traits.TrackNetworkMetadataReceiver;
import com.discord.api.auth.RegisterResponse;
import com.discord.app.AppLog;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelInvite;
import com.discord.models.domain.auth.ModelLoginResult;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreInviteSettings;
import com.discord.stores.StoreNavigation;
import com.discord.stores.utilities.RestCallStateKt;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.persister.Persister;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$3;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$4;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
/* compiled from: StoreAuthentication.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u008e\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000e\u0018\u0000 s2\u00020\u0001:\u0002tsB\u0017\u0012\u0006\u0010h\u001a\u00020g\u0012\u0006\u0010[\u001a\u00020Z¢\u0006\u0004\bq\u0010rJ\u0019\u0010\u0005\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\f\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\f\u0010\rJ\u0015\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0002H\u0000¢\u0006\u0004\b\u000f\u0010\u0006J\r\u0010\u0011\u001a\u00020\u000e¢\u0006\u0004\b\u0011\u0010\u0012J\u0015\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0002H\u0000¢\u0006\u0004\b\u0013\u0010\u0006J\u0017\u0010\u0016\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u0002H\u0000¢\u0006\u0004\b\u0015\u0010\u0006J\u0017\u0010\u0018\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u0002H\u0000¢\u0006\u0004\b\u0017\u0010\u0006J\r\u0010\u0019\u001a\u00020\u0007¢\u0006\u0004\b\u0019\u0010\tJ\u0015\u0010\u001a\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u0002¢\u0006\u0004\b\u001a\u0010\u0006J\u0015\u0010\u001c\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u001b0\u0002¢\u0006\u0004\b\u001c\u0010\u0006J\u0019\u0010\u001e\u001a\u00020\u00072\b\u0010\u001d\u001a\u0004\u0018\u00010\u0003H\u0007¢\u0006\u0004\b\u001e\u0010\u001fJ\u0017\u0010 \u001a\u00020\u00072\b\u0010\u001d\u001a\u0004\u0018\u00010\u0003¢\u0006\u0004\b \u0010\u001fJ\u0013\u0010\"\u001a\b\u0012\u0004\u0012\u00020!0\u0002¢\u0006\u0004\b\"\u0010\u0006J\u0015\u0010$\u001a\u00020\u00072\u0006\u0010#\u001a\u00020!¢\u0006\u0004\b$\u0010%JA\u0010&\u001a\b\u0012\u0004\u0012\u00020\n0\u00022\u0006\u0010&\u001a\u00020\u00032\u0006\u0010'\u001a\u00020\u00032\b\u0010(\u001a\u0004\u0018\u00010\u00032\u0006\u0010)\u001a\u00020\u000e2\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u0003¢\u0006\u0004\b&\u0010+J#\u0010.\u001a\b\u0012\u0004\u0012\u00020\n0\u00022\u0006\u0010,\u001a\u00020\u00032\u0006\u0010-\u001a\u00020\u0003¢\u0006\u0004\b.\u0010/J\u001d\u00102\u001a\n\u0012\u0006\u0012\u0004\u0018\u0001010\u00022\u0006\u00100\u001a\u00020\u0003¢\u0006\u0004\b2\u00103J\u0017\u00104\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\nH\u0007¢\u0006\u0004\b4\u0010\rJ\r\u00105\u001a\u00020\u0007¢\u0006\u0004\b5\u0010\tJ[\u0010=\u001a\b\u0012\u0004\u0012\u00020<0\u00022\u0006\u00106\u001a\u00020\u00032\b\u00107\u001a\u0004\u0018\u00010\u00032\b\u00108\u001a\u0004\u0018\u00010\u00032\u0006\u0010'\u001a\u00020\u00032\b\u0010(\u001a\u0004\u0018\u00010\u00032\u0006\u00109\u001a\u00020\u000e2\u0010\b\u0002\u0010;\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`:¢\u0006\u0004\b=\u0010>J'\u0010?\u001a\n\u0012\u0006\u0012\u0004\u0018\u0001010\u00022\u0006\u0010&\u001a\u00020\u00032\b\u0010(\u001a\u0004\u0018\u00010\u0003¢\u0006\u0004\b?\u0010/J\u0017\u0010A\u001a\u00020\u00072\b\u0010@\u001a\u0004\u0018\u00010\u0003¢\u0006\u0004\bA\u0010\u001fJ\u001f\u0010D\u001a\u00020\u00072\b\u0010B\u001a\u0004\u0018\u00010\u00032\u0006\u0010C\u001a\u00020\u000e¢\u0006\u0004\bD\u0010EJ\u0017\u0010H\u001a\u00020\u00072\u0006\u0010G\u001a\u00020FH\u0017¢\u0006\u0004\bH\u0010IJ\u0019\u0010K\u001a\u00020\u00072\b\u0010@\u001a\u0004\u0018\u00010\u0003H\u0001¢\u0006\u0004\bJ\u0010\u001fJ\u000f\u0010M\u001a\u00020\u0007H\u0001¢\u0006\u0004\bL\u0010\tR\u001e\u0010O\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00030N8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bO\u0010PR\u0018\u0010Q\u001a\u0004\u0018\u00010\u000e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bQ\u0010RRF\u0010U\u001a2\u0012\u0016\u0012\u0014 T*\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00040\u0003j\u0002`\u0004\u0012\u0016\u0012\u0014 T*\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00040\u0003j\u0002`\u00040S8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bU\u0010VR:\u0010X\u001a&\u0012\f\u0012\n T*\u0004\u0018\u00010\u000e0\u000e T*\u0012\u0012\f\u0012\n T*\u0004\u0018\u00010\u000e0\u000e\u0018\u00010W0W8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bX\u0010YR\u0016\u0010[\u001a\u00020Z8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b[\u0010\\R2\u0010]\u001a\u001e\u0012\f\u0012\n T*\u0004\u0018\u00010!0!\u0012\f\u0012\n T*\u0004\u0018\u00010!0!0S8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b]\u0010VR(\u0010@\u001a\u0004\u0018\u00010\u00032\b\u0010^\u001a\u0004\u0018\u00010\u00038\u0000@BX\u0080\u000e¢\u0006\f\n\u0004\b@\u0010_\u001a\u0004\b`\u0010aR\"\u0010c\u001a\u0004\u0018\u00010b2\b\u0010^\u001a\u0004\u0018\u00010b8B@BX\u0082\u000e¢\u0006\u0006\n\u0004\bc\u0010dR4\u0010B\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00042\u000e\u0010^\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00048\u0000@BX\u0080\u000e¢\u0006\f\n\u0004\bB\u0010_\u001a\u0004\be\u0010aR2\u0010f\u001a\u001e\u0012\f\u0012\n T*\u0004\u0018\u00010\u00030\u0003\u0012\f\u0012\n T*\u0004\u0018\u00010\u00030\u00030S8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bf\u0010VR\u0016\u0010h\u001a\u00020g8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bh\u0010iR$\u0010k\u001a\u00020\u000e2\u0006\u0010j\u001a\u00020\u000e8F@BX\u0086\u000e¢\u0006\f\u001a\u0004\bk\u0010\u0012\"\u0004\bl\u0010mR.\u0010n\u001a\u0004\u0018\u00010\u00032\b\u0010&\u001a\u0004\u0018\u00010\u00038\u0006@BX\u0086\u000e¢\u0006\u0012\n\u0004\bn\u0010_\u001a\u0004\bo\u0010a\"\u0004\bp\u0010\u001f¨\u0006u"}, d2 = {"Lcom/discord/stores/StoreAuthentication;", "Lcom/discord/stores/Store;", "Lrx/Observable;", "", "Lcom/discord/primitives/FingerPrint;", "getFingerprintSnapshotOrGenerate", "()Lrx/Observable;", "", "resetIsConsentRequired", "()V", "Lcom/discord/models/domain/auth/ModelLoginResult;", "loginResult", "dispatchLogin", "(Lcom/discord/models/domain/auth/ModelLoginResult;)V", "", "getPreLogoutSignal$app_productionGoogleRelease", "getPreLogoutSignal", "isAuthed", "()Z", "observeIsAuthed$app_productionGoogleRelease", "observeIsAuthed", "getAuthedToken$app_productionGoogleRelease", "getAuthedToken", "getFingerPrint$app_productionGoogleRelease", "getFingerPrint", "requestConsentRequired", "getAgeGateError", "Lcom/discord/stores/StoreNavigation$AgeGate;", "getShouldShowAgeGate", "error", "handleAgeGateError", "(Ljava/lang/String;)V", "setAgeGateError", "Landroid/net/Uri;", "getOAuthUriObservable", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "setOAuthUriSubject", "(Landroid/net/Uri;)V", "login", "password", "captchaKey", "undelete", "loginSource", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lrx/Observable;", ModelAuditLogEntry.CHANGE_KEY_CODE, "ticket", "authMFA", "(Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;", "token", "Ljava/lang/Void;", "authorizeIP", "(Ljava/lang/String;)Lrx/Observable;", "handleLoginResult", "logout", "username", NotificationCompat.CATEGORY_EMAIL, "phoneToken", "consent", "Lcom/discord/primitives/UtcTimestamp;", "dateOfBirth", "Lcom/discord/api/auth/RegisterResponse;", "register", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lrx/Observable;", "forgotPassword", "authToken", "setAuthed", "fingerprint", "force", "setFingerprint", "(Ljava/lang/String;Z)V", "Landroid/content/Context;", "context", "init", "(Landroid/content/Context;)V", "handleAuthToken$app_productionGoogleRelease", "handleAuthToken", "handlePreLogout$app_productionGoogleRelease", "handlePreLogout", "Lrx/subjects/BehaviorSubject;", "ageGateError", "Lrx/subjects/BehaviorSubject;", "_isConsentRequired", "Ljava/lang/Boolean;", "Lrx/subjects/SerializedSubject;", "kotlin.jvm.PlatformType", "fingerprintSubject", "Lrx/subjects/SerializedSubject;", "Lrx/subjects/PublishSubject;", "userInitiatedAuthEventSubject", "Lrx/subjects/PublishSubject;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "oauthUriSubject", "<set-?>", "Ljava/lang/String;", "getAuthToken$app_productionGoogleRelease", "()Ljava/lang/String;", "Lrx/Subscription;", "isConsentRequiredSubscription", "Lrx/Subscription;", "getFingerprint$app_productionGoogleRelease", "tokenSubject", "Lcom/discord/stores/StoreStream;", "storeStream", "Lcom/discord/stores/StoreStream;", "value", "isConsentRequired", "setConsentRequired", "(Z)V", "savedLogin", "getSavedLogin", "setSavedLogin", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;)V", "Companion", "AuthRequestParams", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAuthentication extends Store {
    private static final String CACHE_KEY_FINGERPRINT = "STORE_AUTHED_FINGERPRINT";
    private static final String CACHE_KEY_LOGIN = "STORE_AUTHED_LOGIN";
    private static final String CACHE_KEY_TOKEN = "STORE_AUTHED_TOKEN";
    public static final Companion Companion = new Companion(null);
    private Boolean _isConsentRequired;
    private BehaviorSubject<String> ageGateError;
    private String authToken;
    private final Dispatcher dispatcher;
    private String fingerprint;
    private Subscription isConsentRequiredSubscription;
    private String savedLogin;
    private final StoreStream storeStream;
    private final PublishSubject<Boolean> userInitiatedAuthEventSubject = PublishSubject.k0();
    private final SerializedSubject<String, String> fingerprintSubject = new SerializedSubject<>(BehaviorSubject.k0());
    private final SerializedSubject<String, String> tokenSubject = new SerializedSubject<>(BehaviorSubject.k0());
    private final SerializedSubject<Uri, Uri> oauthUriSubject = new SerializedSubject<>(BehaviorSubject.l0(Uri.EMPTY));

    /* compiled from: StoreAuthentication.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0082\b\u0018\u00002\u00020\u0001B\u001b\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u0018\u0010\u0019J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J(\u0010\n\u001a\u00020\u00002\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\f\u0010\u0007J\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001b\u0010\t\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0014\u001a\u0004\b\u0015\u0010\u0007R\u001b\u0010\b\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004¨\u0006\u001a"}, d2 = {"Lcom/discord/stores/StoreAuthentication$AuthRequestParams;", "", "Lcom/discord/stores/StoreInviteSettings$InviteCode;", "component1", "()Lcom/discord/stores/StoreInviteSettings$InviteCode;", "", "component2", "()Ljava/lang/String;", "inviteCode", "guildTemplateCode", "copy", "(Lcom/discord/stores/StoreInviteSettings$InviteCode;Ljava/lang/String;)Lcom/discord/stores/StoreAuthentication$AuthRequestParams;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getGuildTemplateCode", "Lcom/discord/stores/StoreInviteSettings$InviteCode;", "getInviteCode", HookHelper.constructorName, "(Lcom/discord/stores/StoreInviteSettings$InviteCode;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class AuthRequestParams {
        private final String guildTemplateCode;
        private final StoreInviteSettings.InviteCode inviteCode;

        public AuthRequestParams(StoreInviteSettings.InviteCode inviteCode, String str) {
            this.inviteCode = inviteCode;
            this.guildTemplateCode = str;
        }

        public static /* synthetic */ AuthRequestParams copy$default(AuthRequestParams authRequestParams, StoreInviteSettings.InviteCode inviteCode, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                inviteCode = authRequestParams.inviteCode;
            }
            if ((i & 2) != 0) {
                str = authRequestParams.guildTemplateCode;
            }
            return authRequestParams.copy(inviteCode, str);
        }

        public final StoreInviteSettings.InviteCode component1() {
            return this.inviteCode;
        }

        public final String component2() {
            return this.guildTemplateCode;
        }

        public final AuthRequestParams copy(StoreInviteSettings.InviteCode inviteCode, String str) {
            return new AuthRequestParams(inviteCode, str);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof AuthRequestParams)) {
                return false;
            }
            AuthRequestParams authRequestParams = (AuthRequestParams) obj;
            return m.areEqual(this.inviteCode, authRequestParams.inviteCode) && m.areEqual(this.guildTemplateCode, authRequestParams.guildTemplateCode);
        }

        public final String getGuildTemplateCode() {
            return this.guildTemplateCode;
        }

        public final StoreInviteSettings.InviteCode getInviteCode() {
            return this.inviteCode;
        }

        public int hashCode() {
            StoreInviteSettings.InviteCode inviteCode = this.inviteCode;
            int i = 0;
            int hashCode = (inviteCode != null ? inviteCode.hashCode() : 0) * 31;
            String str = this.guildTemplateCode;
            if (str != null) {
                i = str.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("AuthRequestParams(inviteCode=");
            R.append(this.inviteCode);
            R.append(", guildTemplateCode=");
            return a.H(R, this.guildTemplateCode, ")");
        }
    }

    /* compiled from: StoreAuthentication.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0019\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0007\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0007\u0010\bR\u0016\u0010\t\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\t\u0010\bR\u0016\u0010\n\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\n\u0010\b¨\u0006\r"}, d2 = {"Lcom/discord/stores/StoreAuthentication$Companion;", "", "Landroid/content/Context;", "context", "", "getDeviceEmail", "(Landroid/content/Context;)Ljava/lang/String;", "CACHE_KEY_FINGERPRINT", "Ljava/lang/String;", "CACHE_KEY_LOGIN", "CACHE_KEY_TOKEN", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        @SuppressLint({"MissingPermission"})
        public final String getDeviceEmail(Context context) {
            Account account;
            if (ContextCompat.checkSelfPermission(context, "android.permission.GET_ACCOUNTS") != 0) {
                return null;
            }
            AccountManager accountManager = AccountManager.get(context);
            m.checkNotNullExpressionValue(accountManager, "AccountManager.get(context)");
            Account[] accounts = accountManager.getAccounts();
            m.checkNotNullExpressionValue(accounts, "AccountManager.get(context).accounts");
            int length = accounts.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    account = null;
                    break;
                }
                account = accounts[i];
                if (Patterns.EMAIL_ADDRESS.matcher(account.type).matches()) {
                    break;
                }
                i++;
            }
            if (account != null) {
                return account.name;
            }
            return null;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public StoreAuthentication(StoreStream storeStream, Dispatcher dispatcher) {
        m.checkNotNullParameter(storeStream, "storeStream");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        this.storeStream = storeStream;
        this.dispatcher = dispatcher;
        BehaviorSubject<String> k0 = BehaviorSubject.k0();
        m.checkNotNullExpressionValue(k0, "BehaviorSubject.create()");
        this.ageGateError = k0;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void dispatchLogin(ModelLoginResult modelLoginResult) {
        this.dispatcher.schedule(new StoreAuthentication$dispatchLogin$1(this, modelLoginResult));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Observable<String> getFingerprintSnapshotOrGenerate() {
        Observable<String> t = this.fingerprintSubject.Z(1).Y(StoreAuthentication$getFingerprintSnapshotOrGenerate$1.INSTANCE).t(new Action1<String>() { // from class: com.discord.stores.StoreAuthentication$getFingerprintSnapshotOrGenerate$2
            public final void call(String str) {
                StoreAuthentication.this.setFingerprint(str, false);
            }
        });
        m.checkNotNullExpressionValue(t, "fingerprintSubject\n     …int(fingerprint, false) }");
        return t;
    }

    public static /* synthetic */ Observable login$default(StoreAuthentication storeAuthentication, String str, String str2, String str3, boolean z2, String str4, int i, Object obj) {
        if ((i & 16) != 0) {
            str4 = null;
        }
        return storeAuthentication.login(str, str2, str3, z2, str4);
    }

    private final synchronized void resetIsConsentRequired() {
        this._isConsentRequired = null;
        Subscription subscription = this.isConsentRequiredSubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        this.isConsentRequiredSubscription = null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void setConsentRequired(boolean z2) {
        this._isConsentRequired = Boolean.valueOf(z2);
    }

    private final void setSavedLogin(String str) {
        this.savedLogin = str;
        AppLog.g(null, str, null);
        getPrefsSessionDurable().edit().putString(CACHE_KEY_LOGIN, str).apply();
    }

    public final Observable<ModelLoginResult> authMFA(String str, String str2) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_CODE);
        m.checkNotNullParameter(str2, "ticket");
        Observable<ModelLoginResult> t = ObservableExtensionsKt.restSubscribeOn$default(RestCallStateKt.logNetworkAction(RestAPI.Companion.getApi().postMFACode(new RestAPIParams.MFALogin(str2, str)), StoreAuthentication$authMFA$1.INSTANCE), false, 1, null).t(new Action1<ModelLoginResult>() { // from class: com.discord.stores.StoreAuthentication$authMFA$2
            public final void call(ModelLoginResult modelLoginResult) {
                StoreAuthentication storeAuthentication = StoreAuthentication.this;
                m.checkNotNullExpressionValue(modelLoginResult, "loginResult");
                storeAuthentication.dispatchLogin(modelLoginResult);
            }
        });
        m.checkNotNullExpressionValue(t, "RestAPI\n        .api\n   …in(loginResult)\n        }");
        return t;
    }

    public final Observable<Void> authorizeIP(String str) {
        m.checkNotNullParameter(str, "token");
        return RestCallStateKt.logNetworkAction(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().authorizeIP(new RestAPIParams.AuthorizeIP(str)), false, 1, null), StoreAuthentication$authorizeIP$1.INSTANCE);
    }

    public final Observable<Void> forgotPassword(String str, String str2) {
        m.checkNotNullParameter(str, "login");
        return ObservableExtensionsKt.restSubscribeOn$default(RestCallStateKt.logNetworkAction(RestAPI.Companion.getApi().forgotPassword(new RestAPIParams.ForgotPassword(str, str2)), StoreAuthentication$forgotPassword$1.INSTANCE), false, 1, null);
    }

    public final Observable<String> getAgeGateError() {
        Observable<String> q = this.ageGateError.q();
        m.checkNotNullExpressionValue(q, "ageGateError.distinctUntilChanged()");
        return q;
    }

    public final String getAuthToken$app_productionGoogleRelease() {
        return this.authToken;
    }

    public final Observable<String> getAuthedToken$app_productionGoogleRelease() {
        Observable<String> q = this.tokenSubject.q();
        m.checkNotNullExpressionValue(q, "tokenSubject.distinctUntilChanged()");
        return q;
    }

    public final Observable<String> getFingerPrint$app_productionGoogleRelease() {
        Observable<String> q = this.fingerprintSubject.q();
        m.checkNotNullExpressionValue(q, "fingerprintSubject.distinctUntilChanged()");
        return q;
    }

    public final String getFingerprint$app_productionGoogleRelease() {
        return this.fingerprint;
    }

    public final Observable<Uri> getOAuthUriObservable() {
        Observable<Uri> q = this.oauthUriSubject.q();
        m.checkNotNullExpressionValue(q, "oauthUriSubject.distinctUntilChanged()");
        return q;
    }

    public final Observable<Boolean> getPreLogoutSignal$app_productionGoogleRelease() {
        Observable<Boolean> x2 = this.userInitiatedAuthEventSubject.x(StoreAuthentication$getPreLogoutSignal$1.INSTANCE);
        m.checkNotNullExpressionValue(x2, "userInitiatedAuthEventSu…LoggedIn -> !isLoggedIn }");
        return x2;
    }

    public final String getSavedLogin() {
        return this.savedLogin;
    }

    public final Observable<StoreNavigation.AgeGate> getShouldShowAgeGate() {
        Observable<StoreNavigation.AgeGate> q = Observable.i(this.storeStream.getUsers$app_productionGoogleRelease().observeMe(true), this.storeStream.getAuthentication$app_productionGoogleRelease().getAgeGateError(), this.storeStream.getChannelsSelected$app_productionGoogleRelease().observeSelectedChannel(), StoreAuthentication$getShouldShowAgeGate$1.INSTANCE).q();
        m.checkNotNullExpressionValue(q, "Observable.combineLatest…  .distinctUntilChanged()");
        return q;
    }

    @StoreThread
    public final void handleAgeGateError(String str) {
        this.ageGateError.onNext(str);
    }

    @StoreThread
    public final void handleAuthToken$app_productionGoogleRelease(String str) {
        this.authToken = str;
        getPrefs().edit().putString(CACHE_KEY_TOKEN, str).apply();
        if (str == null) {
            Persister.Companion.reset();
            SharedPreferences.Editor edit = getPrefs().edit();
            m.checkNotNullExpressionValue(edit, "editor");
            edit.clear();
            edit.apply();
        }
    }

    @StoreThread
    public final void handleLoginResult(ModelLoginResult modelLoginResult) {
        m.checkNotNullParameter(modelLoginResult, "loginResult");
        setFingerprint(null, true);
        setAuthed(modelLoginResult.getToken());
        if (modelLoginResult.getToken() != null) {
            setAgeGateError(null);
            AnalyticsTracker.INSTANCE.appFirstLogin();
        }
        PublishSubject<Boolean> publishSubject = this.userInitiatedAuthEventSubject;
        publishSubject.k.onNext(Boolean.TRUE);
        StoreStream.Companion.getNux().setFirstOpen(true);
    }

    @StoreThread
    public final void handlePreLogout$app_productionGoogleRelease() {
        resetIsConsentRequired();
    }

    @Override // com.discord.stores.Store
    @StoreThread
    public synchronized void init(Context context) {
        m.checkNotNullParameter(context, "context");
        super.init(context);
        String string = getPrefsSessionDurable().getString(CACHE_KEY_FINGERPRINT, null);
        if (string != null) {
            setFingerprint(string, false);
        }
        String string2 = getPrefs().getString(CACHE_KEY_TOKEN, null);
        this.authToken = string2;
        setAuthed(string2);
        handleAgeGateError(null);
        setSavedLogin(getPrefsSessionDurable().getString(CACHE_KEY_LOGIN, Companion.getDeviceEmail(context)));
        Observable<Boolean> q = this.userInitiatedAuthEventSubject.q();
        m.checkNotNullExpressionValue(q, "userInitiatedAuthEventSu…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(ObservableExtensionsKt.computationLatest(q)), getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreAuthentication$init$1(context));
    }

    public final boolean isAuthed() {
        return this.authToken != null;
    }

    public final synchronized boolean isConsentRequired() {
        Boolean bool;
        bool = this._isConsentRequired;
        return bool != null ? bool.booleanValue() : true;
    }

    public final Observable<ModelLoginResult> login(final String str, final String str2, final String str3, final boolean z2, final String str4) {
        m.checkNotNullParameter(str, "login");
        m.checkNotNullParameter(str2, "password");
        setSavedLogin(str);
        Observable<ModelLoginResult> t = getFingerprintSnapshotOrGenerate().z(new b<String, Observable<? extends ModelLoginResult>>() { // from class: com.discord.stores.StoreAuthentication$login$1

            /* compiled from: StoreAuthentication.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/auth/ModelLoginResult;", "it", "Lcom/discord/analytics/generated/traits/TrackNetworkMetadataReceiver;", "invoke", "(Lcom/discord/models/domain/auth/ModelLoginResult;)Lcom/discord/analytics/generated/traits/TrackNetworkMetadataReceiver;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.stores.StoreAuthentication$login$1$1  reason: invalid class name */
            /* loaded from: classes.dex */
            public static final class AnonymousClass1 extends o implements Function1<ModelLoginResult, TrackNetworkMetadataReceiver> {
                public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                public AnonymousClass1() {
                    super(1);
                }

                public final TrackNetworkMetadataReceiver invoke(ModelLoginResult modelLoginResult) {
                    Observable<T> L = ObservableExtensionsKt.takeSingleUntilTimeout$default(StoreStream.Companion.getInviteSettings().getInvite(), 250L, false, 2, null).L(StoreAuthentication$login$1$1$invite$1.INSTANCE);
                    ModelInvite modelInvite = (ModelInvite) new j0.m.a(L).a(L.y());
                    return new TrackNetworkActionUserLogin(modelInvite != null ? modelInvite.code : null, null, 2);
                }
            }

            public final Observable<? extends ModelLoginResult> call(String str5) {
                return RestCallStateKt.logNetworkAction(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().postAuthLogin(new RestAPIParams.AuthLogin(str, str2, str3, Boolean.valueOf(z2), str4)), false, 1, null), AnonymousClass1.INSTANCE);
            }
        }).t(new Action1<ModelLoginResult>() { // from class: com.discord.stores.StoreAuthentication$login$2
            public final void call(ModelLoginResult modelLoginResult) {
                if (!modelLoginResult.getMfa()) {
                    StoreAuthentication storeAuthentication = StoreAuthentication.this;
                    m.checkNotNullExpressionValue(modelLoginResult, "loginResult");
                    storeAuthentication.dispatchLogin(modelLoginResult);
                }
            }
        });
        m.checkNotNullExpressionValue(t, "getFingerprintSnapshotOr…lt)\n          }\n        }");
        return t;
    }

    public final void logout() {
        PublishSubject<Boolean> publishSubject = this.userInitiatedAuthEventSubject;
        publishSubject.k.onNext(Boolean.FALSE);
        Observable z2 = ObservableExtensionsKt.restSubscribeOn$default(RestCallStateKt.logNetworkAction(RestAPI.Companion.getApi().logout(new RestAPIParams.UserDevices(StoreStream.Companion.getNotifications().getPushToken())), StoreAuthentication$logout$1.INSTANCE), false, 1, null).z(new b<Void, Observable<? extends String>>() { // from class: com.discord.stores.StoreAuthentication$logout$2
            public final Observable<? extends String> call(Void r1) {
                Observable<? extends String> fingerprintSnapshotOrGenerate;
                fingerprintSnapshotOrGenerate = StoreAuthentication.this.getFingerprintSnapshotOrGenerate();
                return fingerprintSnapshotOrGenerate;
            }
        });
        m.checkNotNullExpressionValue(z2, "RestAPI\n        .api\n   …intSnapshotOrGenerate() }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.takeSingleUntilTimeout$default(z2, 0L, false, 3, null), (r18 & 1) != 0 ? null : null, "logout", (r18 & 4) != 0 ? null : null, new StoreAuthentication$logout$3(this), (r18 & 16) != 0 ? null : new StoreAuthentication$logout$4(this), (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
    }

    public final Observable<Boolean> observeIsAuthed$app_productionGoogleRelease() {
        Observable F = getAuthedToken$app_productionGoogleRelease().F(StoreAuthentication$observeIsAuthed$1.INSTANCE);
        m.checkNotNullExpressionValue(F, "getAuthedToken()\n      .… -> authedToken != null }");
        return F;
    }

    public final Observable<RegisterResponse> register(String str, String str2, String str3, String str4, String str5, boolean z2, String str6) {
        m.checkNotNullParameter(str, "username");
        m.checkNotNullParameter(str4, "password");
        setSavedLogin(str2);
        Observable z3 = getFingerprintSnapshotOrGenerate().z(new StoreAuthentication$register$1(this, str, str2, str3, str4, str5, z2, str6));
        m.checkNotNullExpressionValue(z3, "getFingerprintSnapshotOr…              }\n        }");
        return z3;
    }

    public final synchronized void requestConsentRequired() {
        if (this._isConsentRequired == null) {
            resetIsConsentRequired();
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().getLocationMetadata(), false, 1, null), getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new StoreAuthentication$requestConsentRequired$3(this), (r18 & 8) != 0 ? null : new StoreAuthentication$requestConsentRequired$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreAuthentication$requestConsentRequired$1(this));
        }
    }

    public final void setAgeGateError(String str) {
        this.dispatcher.schedule(new StoreAuthentication$setAgeGateError$1(this, str));
    }

    public final void setAuthed(String str) {
        this.tokenSubject.k.onNext(str);
    }

    /* JADX WARN: Code restructure failed: missing block: B:5:0x0005, code lost:
        if (r1.fingerprint == null) goto L6;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final synchronized void setFingerprint(java.lang.String r2, boolean r3) {
        /*
            r1 = this;
            monitor-enter(r1)
            if (r3 != 0) goto L7
            java.lang.String r3 = r1.fingerprint     // Catch: java.lang.Throwable -> L44
            if (r3 != 0) goto L2c
        L7:
            android.content.SharedPreferences r3 = r1.getPrefsSessionDurable()     // Catch: java.lang.Throwable -> L44
            android.content.SharedPreferences$Editor r3 = r3.edit()     // Catch: java.lang.Throwable -> L44
            java.lang.String r0 = "STORE_AUTHED_FINGERPRINT"
            android.content.SharedPreferences$Editor r3 = r3.putString(r0, r2)     // Catch: java.lang.Throwable -> L44
            r3.apply()     // Catch: java.lang.Throwable -> L44
            r1.fingerprint = r2     // Catch: java.lang.Throwable -> L44
            rx.subjects.SerializedSubject<java.lang.String, java.lang.String> r3 = r1.fingerprintSubject     // Catch: java.lang.Throwable -> L44
            j0.n.c<T> r3 = r3.k     // Catch: java.lang.Throwable -> L44
            r3.onNext(r2)     // Catch: java.lang.Throwable -> L44
            if (r2 == 0) goto L2c
            com.google.firebase.crashlytics.FirebaseCrashlytics r3 = com.google.firebase.crashlytics.FirebaseCrashlytics.getInstance()     // Catch: java.lang.Throwable -> L44
            java.lang.String r0 = "fingerprint"
            r3.setCustomKey(r0, r2)     // Catch: java.lang.Throwable -> L44
        L2c:
            java.lang.String r3 = r1.fingerprint     // Catch: java.lang.Throwable -> L44
            if (r3 == 0) goto L42
            boolean r3 = d0.z.d.m.areEqual(r3, r2)     // Catch: java.lang.Throwable -> L44
            r3 = r3 ^ 1
            if (r3 == 0) goto L42
            if (r2 == 0) goto L42
            java.lang.String r3 = r1.fingerprint     // Catch: java.lang.Throwable -> L44
            d0.z.d.m.checkNotNull(r3)     // Catch: java.lang.Throwable -> L44
            com.discord.utilities.analytics.AnalyticsTracker.externalFingerprintDropped(r3, r2)     // Catch: java.lang.Throwable -> L44
        L42:
            monitor-exit(r1)
            return
        L44:
            r2 = move-exception
            monitor-exit(r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreAuthentication.setFingerprint(java.lang.String, boolean):void");
    }

    public final void setOAuthUriSubject(Uri uri) {
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        this.oauthUriSubject.k.onNext(uri);
    }
}
