package com.discord.stores;

import andhook.lib.HookHelper;
import android.os.Parcel;
import android.os.Parcelable;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.models.domain.ModelInvite;
import com.discord.models.experiments.domain.Experiment;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreStream;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.HashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func2;
import rx.subjects.BehaviorSubject;
/* compiled from: StoreInviteSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 :2\u00020\u0001:\u0002:;B\u0007¢\u0006\u0004\b9\u0010\u0010J\u0015\u0010\u0004\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u0002¢\u0006\u0004\b\u0004\u0010\u0005J/\u0010\r\u001a\u00020\f2\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\u00062\u0010\b\u0002\u0010\u000b\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\n¢\u0006\u0004\b\r\u0010\u000eJ\r\u0010\u000f\u001a\u00020\f¢\u0006\u0004\b\u000f\u0010\u0010J\u0013\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00110\u0002¢\u0006\u0004\b\u0012\u0010\u0005J%\u0010\u0018\u001a\u0004\u0018\u00010\u00172\n\u0010\u0014\u001a\u00060\tj\u0002`\u00132\b\b\u0002\u0010\u0016\u001a\u00020\u0015¢\u0006\u0004\b\u0018\u0010\u0019J\u0015\u0010\u001b\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u001a0\u0002¢\u0006\u0004\b\u001b\u0010\u0005J\u0019\u0010\u001c\u001a\u00020\f2\n\u0010\u0014\u001a\u00060\tj\u0002`\u0013¢\u0006\u0004\b\u001c\u0010\u001dJ1\u0010$\u001a\u00020\f2\n\u0010\u001f\u001a\u0006\u0012\u0002\b\u00030\u001e2\u0014\u0010!\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u001a\u0012\u0004\u0012\u00020\f0 H\u0000¢\u0006\u0004\b\"\u0010#J\u0015\u0010&\u001a\u00020\f2\u0006\u0010%\u001a\u00020\u0011¢\u0006\u0004\b&\u0010'J/\u0010+\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\tj\u0002`)\u0012\u0004\u0012\u00020*0(0\u00022\n\u0010\u0014\u001a\u00060\tj\u0002`\u0013¢\u0006\u0004\b+\u0010,J+\u0010.\u001a\b\u0012\u0004\u0012\u00020\u001a0\u00022\n\u0010-\u001a\u00060\tj\u0002`)2\b\b\u0002\u0010%\u001a\u00020\u0011H\u0007¢\u0006\u0004\b.\u0010/J!\u00100\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u001a0\u00022\n\u0010\u0014\u001a\u00060\tj\u0002`\u0013¢\u0006\u0004\b0\u0010,J\u0019\u0010\u0012\u001a\u00020\u00112\n\u0010\u0014\u001a\u00060\tj\u0002`\u0013¢\u0006\u0004\b\u0012\u00101R.\u00104\u001a\u001a\u0012\u0006\u0012\u0004\u0018\u00010\u0003 3*\f\u0012\u0006\u0012\u0004\u0018\u00010\u0003\u0018\u000102028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b4\u00105R:\u00106\u001a&\u0012\f\u0012\n 3*\u0004\u0018\u00010\u00110\u0011 3*\u0012\u0012\f\u0012\n 3*\u0004\u0018\u00010\u00110\u0011\u0018\u000102028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b6\u00105R\u0016\u00107\u001a\u00020\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b7\u00108¨\u0006<"}, d2 = {"Lcom/discord/stores/StoreInviteSettings;", "Lcom/discord/stores/Store;", "Lrx/Observable;", "Lcom/discord/stores/StoreInviteSettings$InviteCode;", "getInviteCode", "()Lrx/Observable;", "", "inviteCode", "source", "", "Lcom/discord/primitives/GuildScheduledEventId;", "eventId", "", "setInviteCode", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V", "clearInviteCode", "()V", "Lcom/discord/models/domain/ModelInvite$Settings;", "getInviteSettings", "Lcom/discord/primitives/GuildId;", "guildId", "", "trackExposure", "Lcom/discord/models/experiments/domain/Experiment;", "getInviteGuildExperiment", "(JZ)Lcom/discord/models/experiments/domain/Experiment;", "Lcom/discord/models/domain/ModelInvite;", "getInvite", "handleGuildSelected", "(J)V", "Ljava/lang/Class;", "clazz", "Lkotlin/Function1;", "trackBlock", "trackWithInvite$app_productionGoogleRelease", "(Ljava/lang/Class;Lkotlin/jvm/functions/Function1;)V", "trackWithInvite", "settings", "setInviteSettings", "(Lcom/discord/models/domain/ModelInvite$Settings;)V", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "getInvitableChannels", "(J)Lrx/Observable;", "channelId", "generateInvite", "(JLcom/discord/models/domain/ModelInvite$Settings;)Lrx/Observable;", "generateInviteDefaultChannel", "(J)Lcom/discord/models/domain/ModelInvite$Settings;", "Lrx/subjects/BehaviorSubject;", "kotlin.jvm.PlatformType", "pendingInviteCodeSubject", "Lrx/subjects/BehaviorSubject;", "inviteSettingsSubject", "inviteSettings", "Lcom/discord/models/domain/ModelInvite$Settings;", HookHelper.constructorName, "Companion", "InviteCode", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreInviteSettings extends Store {
    public static final Companion Companion = new Companion(null);
    public static final String LOCATION_DEEPLINK = "Deeplink";
    public static final String LOCATION_JOIN = "Join Guild Modal";
    private ModelInvite.Settings inviteSettings = new ModelInvite.Settings(86400);
    private final BehaviorSubject<InviteCode> pendingInviteCodeSubject = BehaviorSubject.l0(null);
    private final BehaviorSubject<ModelInvite.Settings> inviteSettingsSubject = BehaviorSubject.l0(this.inviteSettings);

    /* compiled from: StoreInviteSettings.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/stores/StoreInviteSettings$Companion;", "", "", "LOCATION_DEEPLINK", "Ljava/lang/String;", "LOCATION_JOIN", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreInviteSettings.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\n\b\u0087\b\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\n\u001a\u00020\u0002\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\u000e\u0010\f\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007¢\u0006\u0004\b$\u0010%J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0018\u0010\b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ6\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u00022\b\b\u0002\u0010\u000b\u001a\u00020\u00022\u0010\b\u0002\u0010\f\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007HÆ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0004J\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0018\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0012J \u0010\u001d\u001a\u00020\u001c2\u0006\u0010\u001a\u001a\u00020\u00192\u0006\u0010\u001b\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u001d\u0010\u001eR!\u0010\f\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001f\u001a\u0004\b \u0010\tR\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010!\u001a\u0004\b\"\u0010\u0004R\u0019\u0010\n\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010!\u001a\u0004\b#\u0010\u0004¨\u0006&"}, d2 = {"Lcom/discord/stores/StoreInviteSettings$InviteCode;", "Landroid/os/Parcelable;", "", "component1", "()Ljava/lang/String;", "component2", "", "Lcom/discord/primitives/GuildScheduledEventId;", "component3", "()Ljava/lang/Long;", "inviteCode", "source", "eventId", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Lcom/discord/stores/StoreInviteSettings$InviteCode;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "describeContents", "Landroid/os/Parcel;", "parcel", "flags", "", "writeToParcel", "(Landroid/os/Parcel;I)V", "Ljava/lang/Long;", "getEventId", "Ljava/lang/String;", "getSource", "getInviteCode", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class InviteCode implements Parcelable {
        public static final Parcelable.Creator<InviteCode> CREATOR = new Creator();
        private final Long eventId;
        private final String inviteCode;
        private final String source;

        @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static class Creator implements Parcelable.Creator<InviteCode> {
            /* JADX WARN: Can't rename method to resolve collision */
            @Override // android.os.Parcelable.Creator
            public final InviteCode createFromParcel(Parcel parcel) {
                m.checkNotNullParameter(parcel, "in");
                return new InviteCode(parcel.readString(), parcel.readString(), parcel.readInt() != 0 ? Long.valueOf(parcel.readLong()) : null);
            }

            /* JADX WARN: Can't rename method to resolve collision */
            @Override // android.os.Parcelable.Creator
            public final InviteCode[] newArray(int i) {
                return new InviteCode[i];
            }
        }

        public InviteCode(String str, String str2, Long l) {
            m.checkNotNullParameter(str, "inviteCode");
            m.checkNotNullParameter(str2, "source");
            this.inviteCode = str;
            this.source = str2;
            this.eventId = l;
        }

        public static /* synthetic */ InviteCode copy$default(InviteCode inviteCode, String str, String str2, Long l, int i, Object obj) {
            if ((i & 1) != 0) {
                str = inviteCode.inviteCode;
            }
            if ((i & 2) != 0) {
                str2 = inviteCode.source;
            }
            if ((i & 4) != 0) {
                l = inviteCode.eventId;
            }
            return inviteCode.copy(str, str2, l);
        }

        public final String component1() {
            return this.inviteCode;
        }

        public final String component2() {
            return this.source;
        }

        public final Long component3() {
            return this.eventId;
        }

        public final InviteCode copy(String str, String str2, Long l) {
            m.checkNotNullParameter(str, "inviteCode");
            m.checkNotNullParameter(str2, "source");
            return new InviteCode(str, str2, l);
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof InviteCode)) {
                return false;
            }
            InviteCode inviteCode = (InviteCode) obj;
            return m.areEqual(this.inviteCode, inviteCode.inviteCode) && m.areEqual(this.source, inviteCode.source) && m.areEqual(this.eventId, inviteCode.eventId);
        }

        public final Long getEventId() {
            return this.eventId;
        }

        public final String getInviteCode() {
            return this.inviteCode;
        }

        public final String getSource() {
            return this.source;
        }

        public int hashCode() {
            String str = this.inviteCode;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.source;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            Long l = this.eventId;
            if (l != null) {
                i = l.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("InviteCode(inviteCode=");
            R.append(this.inviteCode);
            R.append(", source=");
            R.append(this.source);
            R.append(", eventId=");
            return a.F(R, this.eventId, ")");
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            m.checkNotNullParameter(parcel, "parcel");
            parcel.writeString(this.inviteCode);
            parcel.writeString(this.source);
            Long l = this.eventId;
            if (l != null) {
                parcel.writeInt(1);
                parcel.writeLong(l.longValue());
                return;
            }
            parcel.writeInt(0);
        }
    }

    public static /* synthetic */ Observable generateInvite$default(StoreInviteSettings storeInviteSettings, long j, ModelInvite.Settings settings, int i, Object obj) {
        if ((i & 2) != 0) {
            settings = storeInviteSettings.inviteSettings;
        }
        return storeInviteSettings.generateInvite(j, settings);
    }

    public static /* synthetic */ Experiment getInviteGuildExperiment$default(StoreInviteSettings storeInviteSettings, long j, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        return storeInviteSettings.getInviteGuildExperiment(j, z2);
    }

    public static /* synthetic */ void setInviteCode$default(StoreInviteSettings storeInviteSettings, String str, String str2, Long l, int i, Object obj) {
        if ((i & 4) != 0) {
            l = null;
        }
        storeInviteSettings.setInviteCode(str, str2, l);
    }

    public final void clearInviteCode() {
        this.pendingInviteCodeSubject.onNext(null);
    }

    public final Observable<ModelInvite> generateInvite(long j) {
        return generateInvite$default(this, j, null, 2, null);
    }

    public final synchronized Observable<ModelInvite> generateInvite(long j, ModelInvite.Settings settings) {
        m.checkNotNullParameter(settings, "settings");
        return ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().postChannelInvite(j, new RestAPIParams.Invite(settings.getMaxAge(), settings.getMaxUses(), settings.isTemporary(), null)), false, 1, null);
    }

    public final Observable<ModelInvite> generateInviteDefaultChannel(final long j) {
        Observable<ModelInvite> z2 = getInvitableChannels(j).F(StoreInviteSettings$generateInviteDefaultChannel$1.INSTANCE).F(StoreInviteSettings$generateInviteDefaultChannel$2.INSTANCE).F(StoreInviteSettings$generateInviteDefaultChannel$3.INSTANCE).Z(1).z(new b<Long, Observable<? extends ModelInvite>>() { // from class: com.discord.stores.StoreInviteSettings$generateInviteDefaultChannel$4
            public final Observable<? extends ModelInvite> call(Long l) {
                if (l != null) {
                    long longValue = l.longValue();
                    StoreInviteSettings storeInviteSettings = StoreInviteSettings.this;
                    Observable<ModelInvite> generateInvite = storeInviteSettings.generateInvite(longValue, storeInviteSettings.getInviteSettings(j));
                    if (generateInvite != null) {
                        return generateInvite;
                    }
                }
                return new k(null);
            }
        });
        m.checkNotNullExpressionValue(z2, "getInvitableChannels(gui…le.just(null)\n          }");
        return z2;
    }

    public final Observable<Map<Long, Channel>> getInvitableChannels(final long j) {
        StoreStream.Companion companion = StoreStream.Companion;
        Observable<Map<Long, Channel>> j2 = Observable.j(companion.getChannels().observeGuildAndPrivateChannels(), companion.getPermissions().observeAllPermissions(), new Func2<Map<Long, ? extends Channel>, Map<Long, ? extends Map<Long, ? extends Long>>, Map<Long, ? extends Channel>>() { // from class: com.discord.stores.StoreInviteSettings$getInvitableChannels$1
            @Override // rx.functions.Func2
            public /* bridge */ /* synthetic */ Map<Long, ? extends Channel> call(Map<Long, ? extends Channel> map, Map<Long, ? extends Map<Long, ? extends Long>> map2) {
                return call2((Map<Long, Channel>) map, (Map<Long, ? extends Map<Long, Long>>) map2);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Map<Long, Channel> call2(Map<Long, Channel> map, Map<Long, ? extends Map<Long, Long>> map2) {
                HashMap hashMap = new HashMap();
                Map<Long, Long> map3 = map2.get(Long.valueOf(j));
                if (map3 != null) {
                    for (Map.Entry<Long, Long> entry : map3.entrySet()) {
                        long longValue = entry.getKey().longValue();
                        long longValue2 = entry.getValue().longValue();
                        Channel channel = map.get(Long.valueOf(longValue));
                        if (channel != null) {
                            m.checkNotNullParameter(channel, "$this$isInvitableChannel");
                            if ((ChannelUtils.s(channel) || ChannelUtils.t(channel)) && PermissionUtils.INSTANCE.hasAccess(channel, Long.valueOf(longValue2)) && PermissionUtils.can(1L, Long.valueOf(longValue2))) {
                                hashMap.put(Long.valueOf(longValue), channel);
                            }
                        }
                    }
                }
                return hashMap;
            }
        });
        m.checkNotNullExpressionValue(j2, "Observable\n        .comb…  }\n          }\n        }");
        return j2;
    }

    public final Observable<ModelInvite> getInvite() {
        Observable<ModelInvite> F = getInviteCode().z(StoreInviteSettings$getInvite$1.INSTANCE).F(StoreInviteSettings$getInvite$2.INSTANCE);
        m.checkNotNullExpressionValue(F, "getInviteCode()\n      .f…-> null\n        }\n      }");
        return F;
    }

    public final Observable<InviteCode> getInviteCode() {
        Observable<InviteCode> q = this.pendingInviteCodeSubject.q();
        m.checkNotNullExpressionValue(q, "pendingInviteCodeSubject.distinctUntilChanged()");
        return q;
    }

    public final Experiment getInviteGuildExperiment(long j, boolean z2) {
        return StoreStream.Companion.getExperiments().getGuildExperiment("2021-03_android_extend_invite_expiration", j, z2);
    }

    public final Observable<ModelInvite.Settings> getInviteSettings() {
        BehaviorSubject<ModelInvite.Settings> behaviorSubject = this.inviteSettingsSubject;
        m.checkNotNullExpressionValue(behaviorSubject, "inviteSettingsSubject");
        Observable<ModelInvite.Settings> q = ObservableExtensionsKt.computationLatest(behaviorSubject).q();
        m.checkNotNullExpressionValue(q, "inviteSettingsSubject\n  …  .distinctUntilChanged()");
        return q;
    }

    public final void handleGuildSelected(long j) {
        setInviteSettings(getInviteSettings(j));
    }

    public final void setInviteCode(String str, String str2, Long l) {
        m.checkNotNullParameter(str, "inviteCode");
        m.checkNotNullParameter(str2, "source");
        this.pendingInviteCodeSubject.onNext(new InviteCode(str, str2, l));
    }

    public final synchronized void setInviteSettings(ModelInvite.Settings settings) {
        m.checkNotNullParameter(settings, "settings");
        this.inviteSettings = settings;
        this.inviteSettingsSubject.onNext(settings);
    }

    public final void trackWithInvite$app_productionGoogleRelease(Class<?> cls, Function1<? super ModelInvite, Unit> function1) {
        m.checkNotNullParameter(cls, "clazz");
        m.checkNotNullParameter(function1, "trackBlock");
        Observable<ModelInvite> x2 = getInvite().x(StoreInviteSettings$trackWithInvite$1.INSTANCE);
        m.checkNotNullExpressionValue(x2, "getInvite()\n        .filter { it != null }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.computationLatest(ObservableExtensionsKt.takeSingleUntilTimeout$default(x2, 250L, false, 2, null)), cls, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new StoreInviteSettings$trackWithInvite$2(function1), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, function1);
    }

    public final ModelInvite.Settings getInviteSettings(long j) {
        Experiment inviteGuildExperiment$default = getInviteGuildExperiment$default(this, j, false, 2, null);
        if (inviteGuildExperiment$default == null || inviteGuildExperiment$default.getBucket() != 1) {
            return new ModelInvite.Settings(86400);
        }
        return new ModelInvite.Settings(ModelInvite.Settings.SEVEN_DAYS);
    }
}
