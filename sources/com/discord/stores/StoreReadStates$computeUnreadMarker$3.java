package com.discord.stores;

import com.discord.models.application.Unread;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import rx.Observable;
/* compiled from: StoreReadStates.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\b\u001a\u0010\u0012\f\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u00032\n\u0010\u0002\u001a\u00060\u0000j\u0002`\u0001H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "Lcom/discord/primitives/ChannelId;", "channelId", "Lrx/Observable;", "Lcom/discord/models/application/Unread$Marker;", "kotlin.jvm.PlatformType", "invoke", "(J)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreReadStates$computeUnreadMarker$3 extends o implements Function1<Long, Observable<Unread.Marker>> {
    public static final StoreReadStates$computeUnreadMarker$3 INSTANCE = new StoreReadStates$computeUnreadMarker$3();

    public StoreReadStates$computeUnreadMarker$3() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Observable<Unread.Marker> invoke(Long l) {
        return invoke(l.longValue());
    }

    public final Observable<Unread.Marker> invoke(long j) {
        Observable<Unread.Marker> invoke = StoreReadStates$computeUnreadMarker$1.INSTANCE.invoke(j);
        m.checkNotNullExpressionValue(invoke, "getMarker(channelId)");
        return invoke;
    }
}
