package com.discord.stores;

import com.discord.rtcconnection.mediaengine.MediaEngineConnection;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreAnalytics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;", "it", "", "invoke", "(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAnalytics$init$1 extends o implements Function1<MediaEngineConnection.InputMode, Unit> {
    public final /* synthetic */ StoreAnalytics this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreAnalytics$init$1(StoreAnalytics storeAnalytics) {
        super(1);
        this.this$0 = storeAnalytics;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(MediaEngineConnection.InputMode inputMode) {
        invoke2(inputMode);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(MediaEngineConnection.InputMode inputMode) {
        m.checkNotNullParameter(inputMode, "it");
        this.this$0.inputMode = inputMode.toString();
    }
}
