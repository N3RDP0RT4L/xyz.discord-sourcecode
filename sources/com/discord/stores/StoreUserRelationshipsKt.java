package com.discord.stores;

import d0.o;
import d0.t.h0;
import java.util.HashMap;
import kotlin.Metadata;
/* compiled from: StoreUserRelationships.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0003\"2\u0010\u0004\u001a\u001e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00020\u0000j\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u0002`\u00038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0004\u0010\u0005¨\u0006\u0006"}, d2 = {"Ljava/util/HashMap;", "", "", "Lkotlin/collections/HashMap;", "UNLOADED_RELATIONSHIPS_SENTINEL", "Ljava/util/HashMap;", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserRelationshipsKt {
    private static final HashMap<Long, Integer> UNLOADED_RELATIONSHIPS_SENTINEL = h0.hashMapOf(o.to(-1L, -1));
}
