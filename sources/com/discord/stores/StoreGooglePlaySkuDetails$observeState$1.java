package com.discord.stores;

import com.discord.stores.StoreGooglePlaySkuDetails;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreGooglePlaySkuDetails.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/stores/StoreGooglePlaySkuDetails$State;", "invoke", "()Lcom/discord/stores/StoreGooglePlaySkuDetails$State;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGooglePlaySkuDetails$observeState$1 extends o implements Function0<StoreGooglePlaySkuDetails.State> {
    public final /* synthetic */ StoreGooglePlaySkuDetails this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGooglePlaySkuDetails$observeState$1(StoreGooglePlaySkuDetails storeGooglePlaySkuDetails) {
        super(0);
        this.this$0 = storeGooglePlaySkuDetails;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final StoreGooglePlaySkuDetails.State invoke() {
        return this.this$0.getState();
    }
}
