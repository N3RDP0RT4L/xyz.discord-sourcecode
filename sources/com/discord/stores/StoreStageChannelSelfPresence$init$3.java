package com.discord.stores;

import com.discord.api.activity.Activity;
import com.discord.api.activity.ActivityType;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreStageChannelSelfPresence.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "showCurrentActivity", "", "invoke", "(Ljava/lang/Boolean;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStageChannelSelfPresence$init$3 extends o implements Function1<Boolean, Unit> {
    public final /* synthetic */ StoreStageChannelSelfPresence this$0;

    /* compiled from: StoreStageChannelSelfPresence.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreStageChannelSelfPresence$init$3$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ Boolean $showCurrentActivity;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Boolean bool) {
            super(0);
            this.$showCurrentActivity = bool;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            StoreUserPresence userPresence = StoreStageChannelSelfPresence$init$3.this.this$0.getUserPresence();
            ActivityType activityType = ActivityType.LISTENING;
            Activity stageChannelActivity = StoreStageChannelSelfPresence$init$3.this.this$0.getStageChannelActivity();
            Boolean bool = this.$showCurrentActivity;
            m.checkNotNullExpressionValue(bool, "showCurrentActivity");
            if (!bool.booleanValue()) {
                stageChannelActivity = null;
            }
            userPresence.updateActivity(activityType, stageChannelActivity, true);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreStageChannelSelfPresence$init$3(StoreStageChannelSelfPresence storeStageChannelSelfPresence) {
        super(1);
        this.this$0 = storeStageChannelSelfPresence;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Boolean bool) {
        invoke2(bool);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Boolean bool) {
        this.this$0.getDispatcher().schedule(new AnonymousClass1(bool));
    }
}
