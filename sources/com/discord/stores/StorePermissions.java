package com.discord.stores;

import andhook.lib.HookHelper;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.permission.Permission;
import com.discord.api.role.GuildRole;
import com.discord.api.stageinstance.StageInstance;
import com.discord.api.thread.AugmentedThreadMember;
import com.discord.api.thread.ThreadMemberUpdate;
import com.discord.api.thread.ThreadMembersUpdate;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.message.Message;
import com.discord.models.thread.dto.ModelThreadListSync;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.collections.CollectionExtensionsKt;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.persister.Persister;
import d0.d0.f;
import d0.t.g0;
import d0.t.h0;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
import rx.Observable;
/* compiled from: StorePermissions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Ô\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0002\b\u0003\n\u0002\u0010\u001c\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010%\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B7\u0012\u0006\u0010o\u001a\u00020n\u0012\u0006\u0010]\u001a\u00020\\\u0012\u0006\u0010l\u001a\u00020k\u0012\u0006\u0010g\u001a\u00020f\u0012\u0006\u0010Q\u001a\u00020P\u0012\u0006\u0010T\u001a\u00020S¢\u0006\u0004\bq\u0010rJ\u000f\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\u0007\u0010\bJ)\u0010\u000e\u001a\u00020\u00022\n\u0010\u000b\u001a\u00060\tj\u0002`\n2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00050\fH\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ=\u0010\u0015\u001a\u0016\u0012\b\u0012\u00060\tj\u0002`\u0013\u0012\b\u0012\u00060\tj\u0002`\u00140\u00122\n\u0010\u000b\u001a\u00060\tj\u0002`\n2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00050\u0010H\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u000f\u0010\u0017\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0017\u0010\u0004J!\u0010\u0018\u001a\u0016\u0012\b\u0012\u00060\tj\u0002`\u0013\u0012\b\u0012\u00060\tj\u0002`\u00140\u0012¢\u0006\u0004\b\u0018\u0010\u0019J/\u0010\u001c\u001a\u0016\u0012\b\u0012\u00060\tj\u0002`\u0013\u0012\b\u0012\u00060\tj\u0002`\u00140\u00122\n\u0010\u000b\u001a\u00060\tj\u0002`\nH\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ!\u0010\u001d\u001a\u0016\u0012\b\u0012\u00060\tj\u0002`\n\u0012\b\u0012\u00060\tj\u0002`\u00140\u0012¢\u0006\u0004\b\u001d\u0010\u0019J'\u0010\u001f\u001a\u0010\u0012\f\u0012\n\u0018\u00010\tj\u0004\u0018\u0001`\u00140\u001e2\n\u0010\u000b\u001a\u00060\tj\u0002`\n¢\u0006\u0004\b\u001f\u0010 J3\u0010!\u001a\u001c\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\tj\u0002`\u0013\u0012\b\u0012\u00060\tj\u0002`\u00140\u00120\u001e2\n\u0010\u000b\u001a\u00060\tj\u0002`\n¢\u0006\u0004\b!\u0010 J'\u0010#\u001a\u0010\u0012\f\u0012\n\u0018\u00010\tj\u0004\u0018\u0001`\u00140\u001e2\n\u0010\"\u001a\u00060\tj\u0002`\u0013¢\u0006\u0004\b#\u0010 J'\u0010$\u001a\u001c\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\tj\u0002`\u0013\u0012\b\u0012\u00060\tj\u0002`\u00140\u00120\u001e¢\u0006\u0004\b$\u0010%J7\u0010&\u001a,\u0012(\u0012&\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\tj\u0002`\u0013\u0012\b\u0012\u00060\tj\u0002`\u00140\u00120\u00120\u001e¢\u0006\u0004\b&\u0010%J\u000f\u0010'\u001a\u00020\u0002H\u0007¢\u0006\u0004\b'\u0010\u0004J\u000f\u0010(\u001a\u00020\u0002H\u0007¢\u0006\u0004\b(\u0010\u0004J\u0017\u0010+\u001a\u00020\u00022\u0006\u0010*\u001a\u00020)H\u0007¢\u0006\u0004\b+\u0010,J\u0017\u0010-\u001a\u00020\u00022\u0006\u0010*\u001a\u00020)H\u0007¢\u0006\u0004\b-\u0010,J\u001b\u0010.\u001a\u00020\u00022\n\u0010\u000b\u001a\u00060\tj\u0002`\nH\u0007¢\u0006\u0004\b.\u0010/J\u0017\u00102\u001a\u00020\u00022\u0006\u00101\u001a\u000200H\u0007¢\u0006\u0004\b2\u00103J\u0017\u00104\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0005H\u0007¢\u0006\u0004\b4\u0010\bJ\u0017\u00105\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0005H\u0007¢\u0006\u0004\b5\u0010\bJ\u0017\u00108\u001a\u00020\u00022\u0006\u00107\u001a\u000206H\u0007¢\u0006\u0004\b8\u00109J\u0017\u0010<\u001a\u00020\u00022\u0006\u0010;\u001a\u00020:H\u0007¢\u0006\u0004\b<\u0010=J\u0017\u0010@\u001a\u00020\u00022\u0006\u0010?\u001a\u00020>H\u0007¢\u0006\u0004\b@\u0010AJ\u001d\u0010E\u001a\u00020\u00022\f\u0010D\u001a\b\u0012\u0004\u0012\u00020C0BH\u0007¢\u0006\u0004\bE\u0010FJ\u0017\u0010I\u001a\u00020\u00022\u0006\u0010H\u001a\u00020GH\u0007¢\u0006\u0004\bI\u0010JJ\u0017\u0010M\u001a\u00020\u00022\u0006\u0010L\u001a\u00020KH\u0007¢\u0006\u0004\bM\u0010NJ\u000f\u0010O\u001a\u00020\u0002H\u0017¢\u0006\u0004\bO\u0010\u0004R\u0016\u0010Q\u001a\u00020P8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bQ\u0010RR\u0016\u0010T\u001a\u00020S8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bT\u0010UR@\u0010W\u001a,\u0012(\u0012&\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\tj\u0002`\u0013\u0012\b\u0012\u00060\tj\u0002`\u00140\u00120\u00120V8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bW\u0010XR0\u0010Y\u001a\u001c\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\tj\u0002`\n\u0012\b\u0012\u00060\tj\u0002`\u00140\u00120V8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bY\u0010XR:\u0010Z\u001a&\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\tj\u0002`\u0013\u0012\b\u0012\u00060\tj\u0002`\u00140\u00120\u00128\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bZ\u0010[R\u0016\u0010]\u001a\u00020\\8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b]\u0010^R\u001a\u0010b\u001a\u00060\tj\u0002`_8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b`\u0010aR*\u0010d\u001a\u0016\u0012\b\u0012\u00060\tj\u0002`\n\u0012\b\u0012\u00060\tj\u0002`\u00140c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bd\u0010[R*\u0010e\u001a\u0016\u0012\b\u0012\u00060\tj\u0002`\n\u0012\b\u0012\u00060\tj\u0002`\u00140\u00128\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\be\u0010[R\u0016\u0010g\u001a\u00020f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bg\u0010hR*\u0010i\u001a\u0016\u0012\b\u0012\u00060\tj\u0002`\u0013\u0012\b\u0012\u00060\tj\u0002`\u00140\u00128\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bi\u0010[R:\u0010j\u001a&\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\tj\u0002`\u0013\u0012\b\u0012\u00060\tj\u0002`\u00140\u00120c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bj\u0010[R\u0016\u0010l\u001a\u00020k8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bl\u0010mR\u0016\u0010o\u001a\u00020n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bo\u0010p¨\u0006s"}, d2 = {"Lcom/discord/stores/StorePermissions;", "Lcom/discord/stores/StoreV2;", "", "recomputeAllPermissions", "()V", "Lcom/discord/api/channel/Channel;", "channel", "updateChannelPermissions", "(Lcom/discord/api/channel/Channel;)V", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "guildThreads", "updateGuildPermissions", "(JLjava/util/Collection;)V", "", "channels", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/permission/PermissionBit;", "computeChannelPermissions", "(JLjava/lang/Iterable;)Ljava/util/Map;", "restoreFromCache", "getPermissionsByChannel", "()Ljava/util/Map;", "getPermissionsByChannelInternal$app_productionGoogleRelease", "(J)Ljava/util/Map;", "getPermissionsByChannelInternal", "getGuildPermissions", "Lrx/Observable;", "observePermissionsForGuild", "(J)Lrx/Observable;", "observeChannelPermissionsForGuild", "channelId", "observePermissionsForChannel", "observePermissionsForAllChannels", "()Lrx/Observable;", "observeAllPermissions", "init", "handleConnectionOpen", "Lcom/discord/api/guild/Guild;", "guild", "handleGuildAdd", "(Lcom/discord/api/guild/Guild;)V", "handleGuildRemove", "handleGuildRolesChanged", "(J)V", "Lcom/discord/api/guildmember/GuildMember;", "member", "handleGuildMemberAdd", "(Lcom/discord/api/guildmember/GuildMember;)V", "handleChannelOrThreadCreateOrUpdate", "handleChannelOrThreadDelete", "Lcom/discord/models/thread/dto/ModelThreadListSync;", "threadListSync", "handleThreadListSync", "(Lcom/discord/models/thread/dto/ModelThreadListSync;)V", "Lcom/discord/api/thread/ThreadMemberUpdate;", "threadMemberUpdate", "handleThreadMemberUpdate", "(Lcom/discord/api/thread/ThreadMemberUpdate;)V", "Lcom/discord/api/thread/ThreadMembersUpdate;", "threadMembersUpdate", "handleThreadMembersUpdate", "(Lcom/discord/api/thread/ThreadMembersUpdate;)V", "", "Lcom/discord/models/message/Message;", "messages", "handleMessagesLoaded", "(Ljava/util/List;)V", "Lcom/discord/utilities/search/network/state/SearchState;", "searchState", "handleSearchFinish", "(Lcom/discord/utilities/search/network/state/SearchState;)V", "Lcom/discord/api/stageinstance/StageInstance;", "stageInstance", "handleStageInstanceChange", "(Lcom/discord/api/stageinstance/StageInstance;)V", "snapshotData", "Lcom/discord/stores/StoreStageInstances;", "storeStageInstances", "Lcom/discord/stores/StoreStageInstances;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/utilities/persister/Persister;", "permissionsForChannelsCache", "Lcom/discord/utilities/persister/Persister;", "permissionsForGuildsCache", "permissionsForChannelsSnapshot", "Ljava/util/Map;", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreChannels;", "Lcom/discord/primitives/UserId;", "getMeId", "()J", "meId", "", "permissionsForGuilds", "permissionsForGuildsSnapshot", "Lcom/discord/stores/StoreThreadsJoined;", "storeThreadsJoined", "Lcom/discord/stores/StoreThreadsJoined;", "permissionsForChannelsFlattenedSnapshot", "permissionsForChannels", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreGuilds;", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreUser;", HookHelper.constructorName, "(Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreThreadsJoined;Lcom/discord/stores/StoreStageInstances;Lcom/discord/stores/updates/ObservationDeck;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StorePermissions extends StoreV2 {
    private final ObservationDeck observationDeck;
    private final StoreChannels storeChannels;
    private final StoreGuilds storeGuilds;
    private final StoreStageInstances storeStageInstances;
    private final StoreThreadsJoined storeThreadsJoined;
    private final StoreUser storeUser;
    private final Map<Long, Long> permissionsForGuilds = new HashMap();
    private Map<Long, Long> permissionsForGuildsSnapshot = new HashMap();
    private final Persister<Map<Long, Long>> permissionsForGuildsCache = new Persister<>("STORE_GUILD_PERMISSIONS_V5", new HashMap());
    private final Map<Long, Map<Long, Long>> permissionsForChannels = new HashMap();
    private Map<Long, ? extends Map<Long, Long>> permissionsForChannelsSnapshot = new HashMap();
    private final Persister<Map<Long, Map<Long, Long>>> permissionsForChannelsCache = new Persister<>("STORE_CHANNEL_PERMISSIONS_BY_GUILD_V5", new HashMap());
    private Map<Long, Long> permissionsForChannelsFlattenedSnapshot = new HashMap();

    public StorePermissions(StoreUser storeUser, StoreChannels storeChannels, StoreGuilds storeGuilds, StoreThreadsJoined storeThreadsJoined, StoreStageInstances storeStageInstances, ObservationDeck observationDeck) {
        m.checkNotNullParameter(storeUser, "storeUser");
        m.checkNotNullParameter(storeChannels, "storeChannels");
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        m.checkNotNullParameter(storeThreadsJoined, "storeThreadsJoined");
        m.checkNotNullParameter(storeStageInstances, "storeStageInstances");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.storeUser = storeUser;
        this.storeChannels = storeChannels;
        this.storeGuilds = storeGuilds;
        this.storeThreadsJoined = storeThreadsJoined;
        this.storeStageInstances = storeStageInstances;
        this.observationDeck = observationDeck;
    }

    @StoreThread
    private final Map<Long, Long> computeChannelPermissions(long j, Iterable<Channel> iterable) {
        long j2;
        Guild guild = this.storeGuilds.getGuildsInternal$app_productionGoogleRelease().get(Long.valueOf(j));
        if (guild == null) {
            return h0.emptyMap();
        }
        Map<Long, GuildMember> map = this.storeGuilds.getGuildMembersComputedInternal$app_productionGoogleRelease().get(Long.valueOf(j));
        Map<Long, GuildRole> map2 = this.storeGuilds.getGuildRolesInternal$app_productionGoogleRelease().get(Long.valueOf(j));
        Map<Long, StageInstance> stageInstancesForGuildInternal = this.storeStageInstances.getStageInstancesForGuildInternal(j);
        LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(o.collectionSizeOrDefault(iterable, 10)), 16));
        for (Channel channel : iterable) {
            boolean hasJoinedInternal = this.storeThreadsJoined.hasJoinedInternal(channel.h());
            Channel guildChannelInternal$app_productionGoogleRelease = this.storeChannels.getGuildChannelInternal$app_productionGoogleRelease(channel.f(), channel.r());
            if (ChannelUtils.x(channel)) {
                j2 = Permission.ALL;
            } else {
                j2 = PermissionUtils.computePermissions(getMeId(), channel, guildChannelInternal$app_productionGoogleRelease, guild.getOwnerId(), map != null ? map.get(Long.valueOf(getMeId())) : null, map2, stageInstancesForGuildInternal, hasJoinedInternal);
            }
            Pair pair = d0.o.to(Long.valueOf(channel.h()), Long.valueOf(j2));
            linkedHashMap.put(pair.getFirst(), pair.getSecond());
        }
        return linkedHashMap;
    }

    private final long getMeId() {
        return this.storeUser.getMeInternal$app_productionGoogleRelease().getId();
    }

    @StoreThread
    private final void recomputeAllPermissions() {
        this.permissionsForGuilds.clear();
        this.permissionsForChannels.clear();
        Collection<Channel> values = this.storeChannels.getThreadsByIdInternal$app_productionGoogleRelease().values();
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Object obj : values) {
            Long valueOf = Long.valueOf(((Channel) obj).f());
            Object obj2 = linkedHashMap.get(valueOf);
            if (obj2 == null) {
                obj2 = new ArrayList();
                linkedHashMap.put(valueOf, obj2);
            }
            ((List) obj2).add(obj);
        }
        Set<Long> keySet = this.storeChannels.getChannelsByGuildInternal$app_productionGoogleRelease().keySet();
        ArrayList<Number> arrayList = new ArrayList();
        for (Object obj3 : keySet) {
            if (((Number) obj3).longValue() != 0) {
                arrayList.add(obj3);
            }
        }
        for (Number number : arrayList) {
            long longValue = number.longValue();
            List list = (List) linkedHashMap.get(Long.valueOf(longValue));
            if (list == null) {
                list = n.emptyList();
            }
            updateGuildPermissions(longValue, list);
        }
    }

    @StoreThread
    private final void restoreFromCache() {
        this.permissionsForGuilds.putAll(this.permissionsForGuildsCache.get());
        this.permissionsForChannels.putAll(this.permissionsForChannelsCache.get());
    }

    @StoreThread
    private final void updateChannelPermissions(Channel channel) {
        if (getMeId() != 0) {
            long f = channel.f();
            Map<Long, Long> map = this.permissionsForChannels.get(Long.valueOf(f));
            if (map == null) {
                map = h0.emptyMap();
            }
            this.permissionsForChannels.put(Long.valueOf(f), h0.plus(map, computeChannelPermissions(f, d0.t.m.listOf(channel))));
            markChanged();
        }
    }

    @StoreThread
    private final void updateGuildPermissions(long j, Collection<Channel> collection) {
        Map<Long, Channel> map;
        Guild guild;
        if (getMeId() != 0 && (map = this.storeChannels.getChannelsByGuildInternal$app_productionGoogleRelease().get(Long.valueOf(j))) != null && (guild = this.storeGuilds.getGuildsInternal$app_productionGoogleRelease().get(Long.valueOf(j))) != null) {
            Map<Long, GuildMember> map2 = this.storeGuilds.getGuildMembersComputedInternal$app_productionGoogleRelease().get(Long.valueOf(j));
            long computeNonThreadPermissions = PermissionUtils.computeNonThreadPermissions(getMeId(), j, guild.getOwnerId(), map2 != null ? map2.get(Long.valueOf(getMeId())) : null, this.storeGuilds.getGuildRolesInternal$app_productionGoogleRelease().get(Long.valueOf(j)), null);
            Long l = this.permissionsForGuilds.get(Long.valueOf(j));
            if (l == null || l.longValue() != computeNonThreadPermissions) {
                this.permissionsForGuilds.put(Long.valueOf(j), Long.valueOf(computeNonThreadPermissions));
                markChanged();
            }
            Map<Long, Long> computeChannelPermissions = computeChannelPermissions(j, u.plus((Collection) map.values(), (Iterable) collection));
            if (!m.areEqual(this.permissionsForChannels.get(Long.valueOf(j)), computeChannelPermissions)) {
                this.permissionsForChannels.put(Long.valueOf(j), computeChannelPermissions);
                markChanged();
            }
        }
    }

    public final Map<Long, Long> getGuildPermissions() {
        return this.permissionsForGuildsSnapshot;
    }

    public final Map<Long, Long> getPermissionsByChannel() {
        return this.permissionsForChannelsFlattenedSnapshot;
    }

    @StoreThread
    public final Map<Long, Long> getPermissionsByChannelInternal$app_productionGoogleRelease(long j) {
        Map<Long, Long> map = this.permissionsForChannels.get(Long.valueOf(j));
        return map != null ? map : h0.emptyMap();
    }

    @StoreThread
    public final void handleChannelOrThreadCreateOrUpdate(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        updateChannelPermissions(channel);
    }

    @StoreThread
    public final void handleChannelOrThreadDelete(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        long f = channel.f();
        Map<Long, Long> map = this.permissionsForChannels.get(Long.valueOf(f));
        if (map == null) {
            map = h0.emptyMap();
        }
        this.permissionsForChannels.put(Long.valueOf(f), h0.minus(map, Long.valueOf(channel.h())));
        markChanged();
    }

    @StoreThread
    public final void handleConnectionOpen() {
        recomputeAllPermissions();
        markChanged();
    }

    @StoreThread
    public final void handleGuildAdd(com.discord.api.guild.Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        updateGuildPermissions(guild.r(), this.storeChannels.getThreadsForGuildInternal$app_productionGoogleRelease(guild.r()));
    }

    @StoreThread
    public final void handleGuildMemberAdd(com.discord.api.guildmember.GuildMember guildMember) {
        m.checkNotNullParameter(guildMember, "member");
        if (getMeId() != 0 && guildMember.m().i() == getMeId()) {
            updateGuildPermissions(guildMember.f(), this.storeChannels.getThreadsForGuildInternal$app_productionGoogleRelease(guildMember.f()));
        }
    }

    @StoreThread
    public final void handleGuildRemove(com.discord.api.guild.Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        long r = guild.r();
        this.permissionsForChannels.remove(Long.valueOf(r));
        this.permissionsForGuilds.remove(Long.valueOf(r));
        markChanged();
    }

    @StoreThread
    public final void handleGuildRolesChanged(long j) {
        updateGuildPermissions(j, this.storeChannels.getThreadsForGuildInternal$app_productionGoogleRelease(j));
    }

    @StoreThread
    public final void handleMessagesLoaded(List<Message> list) {
        m.checkNotNullParameter(list, "messages");
        if (getMeId() != 0) {
            ArrayList arrayList = new ArrayList();
            for (Message message : list) {
                Channel thread = message.getThread();
                if (thread != null) {
                    arrayList.add(thread);
                }
            }
            Channel channel = (Channel) u.firstOrNull((List<? extends Object>) arrayList);
            Long valueOf = channel != null ? Long.valueOf(channel.f()) : null;
            if (valueOf != null) {
                updateGuildPermissions(valueOf.longValue(), this.storeChannels.getThreadsForGuildInternal$app_productionGoogleRelease(valueOf.longValue()));
            }
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:16:0x004e  */
    /* JADX WARN: Removed duplicated region for block: B:19:? A[RETURN, SYNTHETIC] */
    @com.discord.stores.StoreThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void handleSearchFinish(com.discord.utilities.search.network.state.SearchState r6) {
        /*
            r5 = this;
            java.lang.String r0 = "searchState"
            d0.z.d.m.checkNotNullParameter(r6, r0)
            long r0 = r5.getMeId()
            r2 = 0
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 != 0) goto L11
            return
        L11:
            java.util.List r6 = r6.getHits()
            if (r6 == 0) goto L4b
            java.util.ArrayList r0 = new java.util.ArrayList
            r1 = 10
            int r1 = d0.t.o.collectionSizeOrDefault(r6, r1)
            r0.<init>(r1)
            java.util.Iterator r6 = r6.iterator()
        L26:
            boolean r1 = r6.hasNext()
            if (r1 == 0) goto L3a
            java.lang.Object r1 = r6.next()
            com.discord.models.message.Message r1 = (com.discord.models.message.Message) r1
            com.discord.api.channel.Channel r1 = r1.getThread()
            r0.add(r1)
            goto L26
        L3a:
            java.lang.Object r6 = d0.t.u.firstOrNull(r0)
            com.discord.api.channel.Channel r6 = (com.discord.api.channel.Channel) r6
            if (r6 == 0) goto L4b
            long r0 = r6.f()
            java.lang.Long r6 = java.lang.Long.valueOf(r0)
            goto L4c
        L4b:
            r6 = 0
        L4c:
            if (r6 == 0) goto L5f
            long r0 = r6.longValue()
            com.discord.stores.StoreChannels r2 = r5.storeChannels
            long r3 = r6.longValue()
            java.util.List r6 = r2.getThreadsForGuildInternal$app_productionGoogleRelease(r3)
            r5.updateGuildPermissions(r0, r6)
        L5f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StorePermissions.handleSearchFinish(com.discord.utilities.search.network.state.SearchState):void");
    }

    @StoreThread
    public final void handleStageInstanceChange(StageInstance stageInstance) {
        Channel channelInternal$app_productionGoogleRelease;
        m.checkNotNullParameter(stageInstance, "stageInstance");
        if (getMeId() != 0 && (channelInternal$app_productionGoogleRelease = this.storeChannels.getChannelInternal$app_productionGoogleRelease(stageInstance.b(), stageInstance.a())) != null) {
            updateChannelPermissions(channelInternal$app_productionGoogleRelease);
        }
    }

    @StoreThread
    public final void handleThreadListSync(ModelThreadListSync modelThreadListSync) {
        m.checkNotNullParameter(modelThreadListSync, "threadListSync");
        if (getMeId() != 0) {
            updateGuildPermissions(modelThreadListSync.getGuildId(), this.storeChannels.getThreadsForGuildInternal$app_productionGoogleRelease(modelThreadListSync.getGuildId()));
        }
    }

    @StoreThread
    public final void handleThreadMemberUpdate(ThreadMemberUpdate threadMemberUpdate) {
        m.checkNotNullParameter(threadMemberUpdate, "threadMemberUpdate");
        Channel guildChannelInternal$app_productionGoogleRelease = this.storeChannels.getGuildChannelInternal$app_productionGoogleRelease(threadMemberUpdate.b(), threadMemberUpdate.c());
        if (guildChannelInternal$app_productionGoogleRelease != null) {
            updateChannelPermissions(guildChannelInternal$app_productionGoogleRelease);
        }
    }

    @StoreThread
    public final void handleThreadMembersUpdate(ThreadMembersUpdate threadMembersUpdate) {
        Channel guildChannelInternal$app_productionGoogleRelease;
        Boolean bool;
        boolean z2;
        m.checkNotNullParameter(threadMembersUpdate, "threadMembersUpdate");
        if (!(getMeId() == 0 || (guildChannelInternal$app_productionGoogleRelease = this.storeChannels.getGuildChannelInternal$app_productionGoogleRelease(threadMembersUpdate.b(), threadMembersUpdate.c())) == null)) {
            List<AugmentedThreadMember> a = threadMembersUpdate.a();
            Boolean bool2 = null;
            if (a != null) {
                boolean z3 = true;
                if (!a.isEmpty()) {
                    for (AugmentedThreadMember augmentedThreadMember : a) {
                        if (augmentedThreadMember.g() == getMeId()) {
                            z2 = true;
                            continue;
                        } else {
                            z2 = false;
                            continue;
                        }
                        if (z2) {
                            break;
                        }
                    }
                }
                z3 = false;
                bool = Boolean.valueOf(z3);
            } else {
                bool = null;
            }
            List<Long> d = threadMembersUpdate.d();
            if (d != null) {
                bool2 = Boolean.valueOf(d.contains(Long.valueOf(getMeId())));
            }
            Boolean bool3 = Boolean.TRUE;
            if (m.areEqual(bool, bool3) || m.areEqual(bool2, bool3)) {
                updateChannelPermissions(guildChannelInternal$app_productionGoogleRelease);
            }
        }
    }

    @StoreThread
    public final void init() {
        restoreFromCache();
        markChanged();
    }

    public final Observable<Map<Long, Map<Long, Long>>> observeAllPermissions() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StorePermissions$observeAllPermissions$1(this), 14, null);
    }

    public final Observable<Map<Long, Long>> observeChannelPermissionsForGuild(final long j) {
        Observable<Map<Long, Long>> q = observeAllPermissions().F(new b<Map<Long, ? extends Map<Long, ? extends Long>>, Map<Long, ? extends Long>>() { // from class: com.discord.stores.StorePermissions$observeChannelPermissionsForGuild$1
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Map<Long, ? extends Long> call(Map<Long, ? extends Map<Long, ? extends Long>> map) {
                return call2((Map<Long, ? extends Map<Long, Long>>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Map<Long, Long> call2(Map<Long, ? extends Map<Long, Long>> map) {
                m.checkNotNullExpressionValue(map, ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS);
                Map<Long, Long> map2 = map.get(Long.valueOf(j));
                if (map2 == null) {
                    map2 = h0.emptyMap();
                }
                return map2;
            }
        }).q();
        m.checkNotNullExpressionValue(q, "observeAllPermissions()\n…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Map<Long, Long>> observePermissionsForAllChannels() {
        Observable<Map<Long, Long>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StorePermissions$observePermissionsForAllChannels$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck\n        …  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Long> observePermissionsForChannel(long j) {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StorePermissions$observePermissionsForChannel$1(this, j), 14, null);
    }

    public final Observable<Long> observePermissionsForGuild(long j) {
        Observable<Long> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StorePermissions$observePermissionsForGuild$1(this, j), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck\n        …  .distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        Iterator<T> it = this.permissionsForChannels.values().iterator();
        int i = 0;
        while (it.hasNext()) {
            i += ((Map) it.next()).size();
        }
        HashMap hashMap = new HashMap(i, 0.75f);
        Iterator<T> it2 = this.permissionsForChannels.values().iterator();
        while (it2.hasNext()) {
            hashMap.putAll((Map) it2.next());
        }
        this.permissionsForGuildsSnapshot = CollectionExtensionsKt.snapshot$default(this.permissionsForGuilds, 0, 0.0f, 3, null);
        this.permissionsForChannelsSnapshot = CollectionExtensionsKt.snapshot$default(this.permissionsForChannels, 0, 0.0f, 3, null);
        this.permissionsForChannelsFlattenedSnapshot = hashMap;
        this.permissionsForGuildsCache.set(this.permissionsForGuilds, true);
        this.permissionsForChannelsCache.set(this.permissionsForChannels, true);
    }
}
