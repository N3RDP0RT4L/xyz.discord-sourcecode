package com.discord.stores;

import com.discord.stores.StoreGuildTemplates;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreGuildTemplates.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;", "invoke", "()Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildTemplates$observeGuildTemplate$1 extends o implements Function0<StoreGuildTemplates.GuildTemplateState> {
    public final /* synthetic */ String $guildTemplateCode;
    public final /* synthetic */ StoreGuildTemplates this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGuildTemplates$observeGuildTemplate$1(StoreGuildTemplates storeGuildTemplates, String str) {
        super(0);
        this.this$0 = storeGuildTemplates;
        this.$guildTemplateCode = str;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final StoreGuildTemplates.GuildTemplateState invoke() {
        StoreGuildTemplates.GuildTemplateState guildTemplate = this.this$0.getGuildTemplate(this.$guildTemplateCode);
        return guildTemplate != null ? guildTemplate : StoreGuildTemplates.GuildTemplateState.Loading.INSTANCE;
    }
}
