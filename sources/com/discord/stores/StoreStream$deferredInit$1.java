package com.discord.stores;

import android.app.Application;
import androidx.core.app.NotificationCompat;
import b.a.e.d;
import com.discord.app.AppLog;
import com.discord.rtcconnection.RtcConnection;
import com.discord.stores.StoreMessagesLoader;
import com.discord.stores.StoreRtcConnection;
import com.discord.utilities.networking.NetworkMonitor;
import com.discord.utilities.persister.Persister;
import com.discord.utilities.time.Clock;
import com.discord.utilities.time.TimeElapsed;
import com.discord.utilities.voice.VoiceEngineServiceController;
import d0.z.d.k;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import rx.Observable;
import rx.functions.Func4;
import rx.subjects.BehaviorSubject;
/* compiled from: StoreStream.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStream$deferredInit$1 extends o implements Function0<Unit> {
    public final /* synthetic */ Application $context;
    public final /* synthetic */ StoreStream this$0;

    /* compiled from: StoreStream.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "it", "", "invoke", "(Z)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreStream$deferredInit$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<Boolean, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Boolean bool) {
            invoke(bool.booleanValue());
            return Unit.a;
        }

        public final void invoke(boolean z2) {
            StoreStream$deferredInit$1.this.this$0.handlePreLogout();
        }
    }

    /* compiled from: StoreStream.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "p1", "", "invoke", "(Z)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreStream$deferredInit$1$10  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class AnonymousClass10 extends k implements Function1<Boolean, Unit> {
        public AnonymousClass10(StoreStream storeStream) {
            super(1, storeStream, StoreStream.class, "handleBackgrounded", "handleBackgrounded(Z)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Boolean bool) {
            invoke(bool.booleanValue());
            return Unit.a;
        }

        public final void invoke(boolean z2) {
            ((StoreStream) this.receiver).handleBackgrounded(z2);
        }
    }

    /* compiled from: StoreStream.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000b\n\u0002\b\b\u0010\b\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0003\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0004\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0005\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "kotlin.jvm.PlatformType", "experimentsInitialized", "isAuthed", "channelsSelectedInitialized", "cachesInitialized", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreStream$deferredInit$1$11  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass11<T1, T2, T3, T4, R> implements Func4<Boolean, Boolean, Boolean, Boolean, Boolean> {
        public static final AnonymousClass11 INSTANCE = new AnonymousClass11();

        /* JADX WARN: Code restructure failed: missing block: B:9:0x0025, code lost:
            if (r4.booleanValue() != false) goto L10;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public final java.lang.Boolean call(java.lang.Boolean r2, java.lang.Boolean r3, java.lang.Boolean r4, java.lang.Boolean r5) {
            /*
                r1 = this;
                java.lang.String r0 = "experimentsInitialized"
                d0.z.d.m.checkNotNullExpressionValue(r2, r0)
                boolean r2 = r2.booleanValue()
                if (r2 == 0) goto L29
                java.lang.String r2 = "cachesInitialized"
                d0.z.d.m.checkNotNullExpressionValue(r5, r2)
                boolean r2 = r5.booleanValue()
                if (r2 == 0) goto L29
                boolean r2 = r3.booleanValue()
                if (r2 == 0) goto L27
                java.lang.String r2 = "channelsSelectedInitialized"
                d0.z.d.m.checkNotNullExpressionValue(r4, r2)
                boolean r2 = r4.booleanValue()
                if (r2 == 0) goto L29
            L27:
                r2 = 1
                goto L2a
            L29:
                r2 = 0
            L2a:
                java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
                return r2
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreStream$deferredInit$1.AnonymousClass11.call(java.lang.Boolean, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean):java.lang.Boolean");
        }
    }

    /* compiled from: StoreStream.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "p1", "", "invoke", "(Ljava/lang/Boolean;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreStream$deferredInit$1$12  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class AnonymousClass12 extends k implements Function1<Boolean, Unit> {
        public AnonymousClass12(BehaviorSubject behaviorSubject) {
            super(1, behaviorSubject, BehaviorSubject.class, "onNext", "onNext(Ljava/lang/Object;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Boolean bool) {
            invoke2(bool);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Boolean bool) {
            ((BehaviorSubject) this.receiver).onNext(bool);
        }
    }

    /* compiled from: StoreStream.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "p1", "", "invoke", "(Ljava/lang/String;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreStream$deferredInit$1$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class AnonymousClass2 extends k implements Function1<String, Unit> {
        public AnonymousClass2(StoreStream storeStream) {
            super(1, storeStream, StoreStream.class, "handleAuthToken", "handleAuthToken(Ljava/lang/String;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(String str) {
            invoke2(str);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(String str) {
            ((StoreStream) this.receiver).handleAuthToken(str);
        }
    }

    /* compiled from: StoreStream.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "p1", "", "invoke", "(Ljava/lang/String;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreStream$deferredInit$1$3  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class AnonymousClass3 extends k implements Function1<String, Unit> {
        public AnonymousClass3(StoreStream storeStream) {
            super(1, storeStream, StoreStream.class, "handleFingerprint", "handleFingerprint(Ljava/lang/String;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(String str) {
            invoke2(str);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(String str) {
            ((StoreStream) this.receiver).handleFingerprint(str);
        }
    }

    /* compiled from: StoreStream.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;", "p1", "", "invoke", "(Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreStream$deferredInit$1$4  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class AnonymousClass4 extends k implements Function1<StoreMessagesLoader.ChannelChunk, Unit> {
        public AnonymousClass4(StoreStream storeStream) {
            super(1, storeStream, StoreStream.class, "handleMessagesLoaded", "handleMessagesLoaded(Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreMessagesLoader.ChannelChunk channelChunk) {
            invoke2(channelChunk);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreMessagesLoader.ChannelChunk channelChunk) {
            m.checkNotNullParameter(channelChunk, "p1");
            ((StoreStream) this.receiver).handleMessagesLoaded(channelChunk);
        }
    }

    /* compiled from: StoreStream.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\n\u0010\u0002\u001a\u00060\u0000j\u0002`\u0001¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/primitives/ChannelId;", "p1", "", "invoke", "(J)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreStream$deferredInit$1$5  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class AnonymousClass5 extends k implements Function1<Long, Unit> {
        public AnonymousClass5(StoreStream storeStream) {
            super(1, storeStream, StoreStream.class, "handleChannelSelected", "handleChannelSelected(J)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Long l) {
            invoke(l.longValue());
            return Unit.a;
        }

        public final void invoke(long j) {
            ((StoreStream) this.receiver).handleChannelSelected(j);
        }
    }

    /* compiled from: StoreStream.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\n\u0010\u0002\u001a\u00060\u0000j\u0002`\u0001¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/primitives/ChannelId;", "p1", "", "invoke", "(J)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreStream$deferredInit$1$6  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class AnonymousClass6 extends k implements Function1<Long, Unit> {
        public AnonymousClass6(StoreStream storeStream) {
            super(1, storeStream, StoreStream.class, "handleVoiceChannelSelected", "handleVoiceChannelSelected(J)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Long l) {
            invoke(l.longValue());
            return Unit.a;
        }

        public final void invoke(long j) {
            ((StoreStream) this.receiver).handleVoiceChannelSelected(j);
        }
    }

    /* compiled from: StoreStream.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010\"\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "", "p1", "", "invoke", "(Ljava/util/Set;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreStream$deferredInit$1$7  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class AnonymousClass7 extends k implements Function1<Set<? extends Long>, Unit> {
        public AnonymousClass7(StoreStream storeStream) {
            super(1, storeStream, StoreStream.class, "handleSpeakingUsers", "handleSpeakingUsers(Ljava/util/Set;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Set<? extends Long> set) {
            invoke2((Set<Long>) set);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Set<Long> set) {
            m.checkNotNullParameter(set, "p1");
            ((StoreStream) this.receiver).handleSpeakingUsers(set);
        }
    }

    /* compiled from: StoreStream.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/rtcconnection/RtcConnection$StateChange;", "p1", "", "invoke", "(Lcom/discord/rtcconnection/RtcConnection$StateChange;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreStream$deferredInit$1$8  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class AnonymousClass8 extends k implements Function1<RtcConnection.StateChange, Unit> {
        public AnonymousClass8(StoreStream storeStream) {
            super(1, storeStream, StoreStream.class, "handleRtcConnectionStateChanged", "handleRtcConnectionStateChanged(Lcom/discord/rtcconnection/RtcConnection$StateChange;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(RtcConnection.StateChange stateChange) {
            invoke2(stateChange);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(RtcConnection.StateChange stateChange) {
            m.checkNotNullParameter(stateChange, "p1");
            ((StoreStream) this.receiver).handleRtcConnectionStateChanged(stateChange);
        }
    }

    /* compiled from: StoreStream.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/stores/StoreRtcConnection$SpeakingUserUpdate;", "p1", "", "invoke", "(Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreStream$deferredInit$1$9  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class AnonymousClass9 extends k implements Function1<List<? extends StoreRtcConnection.SpeakingUserUpdate>, Unit> {
        public AnonymousClass9(StoreStream storeStream) {
            super(1, storeStream, StoreStream.class, "handleSpeakingUpdates", "handleSpeakingUpdates(Ljava/util/List;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(List<? extends StoreRtcConnection.SpeakingUserUpdate> list) {
            invoke2((List<StoreRtcConnection.SpeakingUserUpdate>) list);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(List<StoreRtcConnection.SpeakingUserUpdate> list) {
            m.checkNotNullParameter(list, "p1");
            ((StoreStream) this.receiver).handleSpeakingUpdates(list);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreStream$deferredInit$1(StoreStream storeStream, Application application) {
        super(0);
        this.this$0 = storeStream;
        this.$context = application;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        Clock clock;
        Clock clock2;
        clock = this.this$0.clock;
        TimeElapsed timeElapsed = new TimeElapsed(clock, 0L, 2, null);
        NetworkMonitor networkMonitor = new NetworkMonitor(this.$context, AppLog.g);
        this.this$0.startStoreInitializationTimer(networkMonitor);
        this.this$0.getExperiments$app_productionGoogleRelease().init(this.$context);
        this.this$0.getMediaEngine$app_productionGoogleRelease().init(this.$context);
        this.this$0.getAccessibility$app_productionGoogleRelease().init(this.$context);
        this.this$0.getGatewaySocket$app_productionGoogleRelease().init(this.$context, networkMonitor);
        this.this$0.getNavigation$app_productionGoogleRelease().init(this.$context);
        this.this$0.getNux$app_productionGoogleRelease().init(this.$context);
        this.this$0.getChannels$app_productionGoogleRelease().init();
        this.this$0.getUsers$app_productionGoogleRelease().init(this.$context);
        this.this$0.getGuilds$app_productionGoogleRelease().init(this.$context);
        this.this$0.getPermissions$app_productionGoogleRelease().init();
        this.this$0.getGuildsSorted$app_productionGoogleRelease().init(this.$context);
        this.this$0.getGuildsNsfw$app_productionGoogleRelease().init(this.$context);
        this.this$0.getGuildSelected$app_productionGoogleRelease().init(this.$context);
        this.this$0.getChannelsSelected$app_productionGoogleRelease().init();
        this.this$0.getMediaSettings$app_productionGoogleRelease().init();
        this.this$0.getMessages$app_productionGoogleRelease().init(this.$context);
        this.this$0.getMessagesLoader$app_productionGoogleRelease().init(this.$context);
        this.this$0.getMessageAck$app_productionGoogleRelease().init(this.$context);
        this.this$0.getMessagesMostRecent$app_productionGoogleRelease().init(this.$context);
        this.this$0.getNotifications$app_productionGoogleRelease().init(this.$context);
        this.this$0.getRtcConnection$app_productionGoogleRelease().init(this.$context, networkMonitor);
        this.this$0.getReadStates$app_productionGoogleRelease().init(this.$context);
        this.this$0.getVoiceChannelSelected$app_productionGoogleRelease().init(this.$context);
        this.this$0.getVoiceSpeaking$app_productionGoogleRelease().init(this.$context);
        this.this$0.getVoiceParticipants$app_productionGoogleRelease().init(this.$context);
        this.this$0.getConnectivity$app_productionGoogleRelease().init(networkMonitor);
        this.this$0.getClientVersion$app_productionGoogleRelease().init(this.$context);
        this.this$0.getMediaSettings$app_productionGoogleRelease().init(this.$context);
        this.this$0.getAnalytics$app_productionGoogleRelease().init(this.$context);
        this.this$0.getCollapsedChannelCategories$app_productionGoogleRelease().init(this.$context);
        this.this$0.getGuildSettings$app_productionGoogleRelease().init(this.$context);
        this.this$0.getNotices$app_productionGoogleRelease().init(this.$context);
        this.this$0.getUserConnections$app_productionGoogleRelease().init(this.$context);
        this.this$0.getChangeLogStore$app_productionGoogleRelease().init(this.$context);
        this.this$0.getReviewRequestStore$app_productionGoogleRelease().init(this.$context);
        this.this$0.getPresences$app_productionGoogleRelease().init(this.$context);
        this.this$0.getSpotify$app_productionGoogleRelease().init(this.$context);
        this.this$0.streamRtcConnection.init(networkMonitor);
        this.this$0.getGuildsSorted$app_productionGoogleRelease().init(this.$context);
        this.this$0.getExpandedGuildFolders$app_productionGoogleRelease().init(this.$context);
        this.this$0.getUserRelationships$app_productionGoogleRelease().init();
        this.this$0.getMaskedLinks$app_productionGoogleRelease().init(this.$context);
        this.this$0.getRtcRegion$app_productionGoogleRelease().init();
        this.this$0.getStickers$app_productionGoogleRelease().init();
        this.this$0.getGooglePlayPurchases$app_productionGoogleRelease().init(this.$context);
        this.this$0.getPhone$app_productionGoogleRelease().init(this.$context);
        this.this$0.getApplicationInteractions$app_productionGoogleRelease().init(this.$context);
        this.this$0.getStageSelfPresence$app_productionGoogleRelease().init();
        this.this$0.getContactSync$app_productionGoogleRelease().init(this.$context);
        this.this$0.getAudioManagerV2$app_productionGoogleRelease().init(this.$context);
        this.this$0.getOutboundPromotions$app_productionGoogleRelease().init(this.$context);
        StoreStream storeStream = this.this$0;
        storeStream.dispatchSubscribe(storeStream.getAuthentication$app_productionGoogleRelease().getPreLogoutSignal$app_productionGoogleRelease(), "streamPreLogout", new AnonymousClass1());
        StoreStream storeStream2 = this.this$0;
        storeStream2.dispatchSubscribe(storeStream2.getAuthentication$app_productionGoogleRelease().getAuthedToken$app_productionGoogleRelease(), "streamAuthedToken", new AnonymousClass2(this.this$0));
        StoreStream storeStream3 = this.this$0;
        storeStream3.dispatchSubscribe(storeStream3.getAuthentication$app_productionGoogleRelease().getFingerPrint$app_productionGoogleRelease(), "streamAuthedFingerprint", new AnonymousClass3(this.this$0));
        StoreStream storeStream4 = this.this$0;
        storeStream4.dispatchSubscribe(storeStream4.getMessagesLoader$app_productionGoogleRelease().get(), "streamMessagesLoaded", new AnonymousClass4(this.this$0));
        StoreStream storeStream5 = this.this$0;
        storeStream5.dispatchSubscribe(storeStream5.getChannelsSelected$app_productionGoogleRelease().observeId(), "streamChannelSelected", new AnonymousClass5(this.this$0));
        StoreStream storeStream6 = this.this$0;
        storeStream6.dispatchSubscribe(storeStream6.getVoiceChannelSelected$app_productionGoogleRelease().observeSelectedVoiceChannelId(), "streamVoiceChannelSelected", new AnonymousClass6(this.this$0));
        StoreStream storeStream7 = this.this$0;
        storeStream7.dispatchSubscribe(storeStream7.getVoiceSpeaking$app_productionGoogleRelease().observeSpeakingUsers(), "streamUserSpeaking", new AnonymousClass7(this.this$0));
        StoreStream storeStream8 = this.this$0;
        storeStream8.dispatchSubscribe(storeStream8.getRtcConnection$app_productionGoogleRelease().getConnectionState(), "streamRtcConnectionStateChanged", new AnonymousClass8(this.this$0));
        StoreStream storeStream9 = this.this$0;
        storeStream9.dispatchSubscribe(storeStream9.getRtcConnection$app_productionGoogleRelease().getSpeakingUpdates(), "streamRtcSpeakingUpdates", new AnonymousClass9(this.this$0));
        this.this$0.initGatewaySocketListeners();
        this.this$0.dispatchSubscribe(d.d.a(), "streamBackgrounded", new AnonymousClass10(this.this$0));
        StoreStream storeStream10 = this.this$0;
        Observable h = Observable.h(storeStream10.getExperiments$app_productionGoogleRelease().isInitialized().q(), this.this$0.getAuthentication$app_productionGoogleRelease().observeIsAuthed$app_productionGoogleRelease(), this.this$0.getChannelsSelected$app_productionGoogleRelease().observeInitializedForAuthedUser(), Persister.Companion.isPreloaded(), AnonymousClass11.INSTANCE);
        m.checkNotNullExpressionValue(h, "Observable\n        .comb…tedInitialized)\n        }");
        storeStream10.dispatchSubscribe(h, "streamInit", new AnonymousClass12(this.this$0.initialized));
        AppLog.i("[StoreStream] Application stores initialized in: " + timeElapsed.getSeconds() + " seconds.");
        VoiceEngineServiceController.Companion.getINSTANCE().init(this.$context);
        StoreStream storeStream11 = this.this$0;
        Application application = this.$context;
        clock2 = storeStream11.clock;
        storeStream11.maybeLogNotificationPermissionStatus(application, clock2);
    }
}
