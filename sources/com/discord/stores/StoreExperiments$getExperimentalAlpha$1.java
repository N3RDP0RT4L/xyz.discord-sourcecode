package com.discord.stores;

import com.discord.models.user.MeUser;
import com.discord.utilities.user.UserUtils;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreExperiments.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreExperiments$getExperimentalAlpha$1 extends o implements Function0<Boolean> {
    public final /* synthetic */ StoreExperiments this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreExperiments$getExperimentalAlpha$1(StoreExperiments storeExperiments) {
        super(0);
        this.this$0 = storeExperiments;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final Boolean invoke2() {
        StoreUser storeUser;
        StoreGuilds storeGuilds;
        storeUser = this.this$0.storeUser;
        MeUser me2 = storeUser.getMe();
        storeGuilds = this.this$0.storeGuilds;
        return (UserUtils.INSTANCE.isStaff(me2) || storeGuilds.getGuilds().get(197038439483310086L) != null) ? 1 : null;
    }
}
