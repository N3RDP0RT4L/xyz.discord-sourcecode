package com.discord.stores;

import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.t.g0;
import d0.t.h0;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreAnalytics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0010\u0000\n\u0002\b\u0004\u0010\u0006\u001a\u0010\u0012\u0004\u0012\u00020\u0001\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"", "", "", "invoke", "()Ljava/util/Map;", "com/discord/stores/StoreAnalytics$trackChannelOpened$1$1", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAnalytics$trackChannelOpened$$inlined$let$lambda$1 extends o implements Function0<Map<String, ? extends Object>> {
    public final /* synthetic */ SelectedChannelAnalyticsLocation $analyticsLocation$inlined;
    public final /* synthetic */ Channel $channel;
    public final /* synthetic */ long $channelId$inlined;
    public final /* synthetic */ ChannelAnalyticsViewType $channelView$inlined;
    public final /* synthetic */ StoreAnalytics this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreAnalytics$trackChannelOpened$$inlined$let$lambda$1(Channel channel, StoreAnalytics storeAnalytics, long j, ChannelAnalyticsViewType channelAnalyticsViewType, SelectedChannelAnalyticsLocation selectedChannelAnalyticsLocation) {
        super(0);
        this.$channel = channel;
        this.this$0 = storeAnalytics;
        this.$channelId$inlined = j;
        this.$channelView$inlined = channelAnalyticsViewType;
        this.$analyticsLocation$inlined = selectedChannelAnalyticsLocation;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Map<String, ? extends Object> invoke() {
        Map<String, ? extends Object> plus = h0.plus(ChannelUtils.C(this.$channel) ? this.this$0.getThreadSnapshotAnalyticsProperties(this.$channel) : this.this$0.getChannelSnapshotAnalyticsProperties(this.$channel, true), g0.mapOf(d0.o.to("channel_view", this.$channelView$inlined.getAnalyticsValue())));
        SelectedChannelAnalyticsLocation selectedChannelAnalyticsLocation = this.$analyticsLocation$inlined;
        return selectedChannelAnalyticsLocation != null ? h0.plus(plus, g0.mapOf(d0.o.to(ModelAuditLogEntry.CHANGE_KEY_LOCATION, selectedChannelAnalyticsLocation.getAnalyticsValue()))) : plus;
    }
}
