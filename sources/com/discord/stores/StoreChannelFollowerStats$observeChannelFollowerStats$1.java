package com.discord.stores;

import com.discord.models.domain.ModelChannelFollowerStats;
import com.discord.stores.StoreChannelFollowerStats;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreChannelFollowerStats.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/models/domain/ModelChannelFollowerStats;", "invoke", "()Lcom/discord/models/domain/ModelChannelFollowerStats;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreChannelFollowerStats$observeChannelFollowerStats$1 extends o implements Function0<ModelChannelFollowerStats> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ StoreChannelFollowerStats this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreChannelFollowerStats$observeChannelFollowerStats$1(StoreChannelFollowerStats storeChannelFollowerStats, long j) {
        super(0);
        this.this$0 = storeChannelFollowerStats;
        this.$channelId = j;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final ModelChannelFollowerStats invoke() {
        Map map;
        map = this.this$0.channelFollowerStatsStateSnapshot;
        StoreChannelFollowerStats.ChannelFollowerStatData channelFollowerStatData = (StoreChannelFollowerStats.ChannelFollowerStatData) map.get(Long.valueOf(this.$channelId));
        if (channelFollowerStatData != null) {
            return channelFollowerStatData.getData();
        }
        return null;
    }
}
