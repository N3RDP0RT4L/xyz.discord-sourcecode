package com.discord.stores;

import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreStageChannels.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/channel/Channel;", "channel", "", "invoke", "(Lcom/discord/api/channel/Channel;)Ljava/lang/Long;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStageChannels$stageChannelIdsInGuildForDispatch$1 extends o implements Function1<Channel, Long> {
    public static final StoreStageChannels$stageChannelIdsInGuildForDispatch$1 INSTANCE = new StoreStageChannels$stageChannelIdsInGuildForDispatch$1();

    public StoreStageChannels$stageChannelIdsInGuildForDispatch$1() {
        super(1);
    }

    public final Long invoke(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        Long valueOf = Long.valueOf(channel.h());
        valueOf.longValue();
        if (ChannelUtils.z(channel)) {
            return valueOf;
        }
        return null;
    }
}
