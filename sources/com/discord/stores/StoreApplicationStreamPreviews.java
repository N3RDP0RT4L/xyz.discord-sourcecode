package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.Context;
import b.d.b.a.a;
import com.discord.models.domain.ModelApplicationStream;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.streams.StreamContext;
import com.discord.utilities.time.Clock;
import d0.t.h0;
import d0.z.d.m;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscription;
/* compiled from: StoreApplicationStreamPreviews.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0088\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 ;2\u00020\u0001:\u0002;<B+\u0012\u0006\u00100\u001a\u00020/\u0012\u0006\u0010)\u001a\u00020(\u0012\b\b\u0002\u0010#\u001a\u00020\"\u0012\b\b\u0002\u0010&\u001a\u00020%¢\u0006\u0004\b9\u0010:J\u001b\u0010\u0006\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J%\u0010\u0006\u001a\u00020\u00052\b\u0010\t\u001a\u0004\u0018\u00010\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\nJ\u001b\u0010\u000b\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0002¢\u0006\u0004\b\u000b\u0010\u0007J\u001d\u0010\r\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\b0\f¢\u0006\u0004\b\r\u0010\u000eJ\u001d\u0010\u0012\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00112\u0006\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b\u0012\u0010\u0013J!\u0010\u0012\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00112\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0012\u0010\u0014J\u001b\u0010\u0015\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0007¢\u0006\u0004\b\u0015\u0010\u0007J#\u0010\u0017\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0016\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0017\u0010\u0018J#\u0010\u001b\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u001a\u001a\u00020\u0019H\u0007¢\u0006\u0004\b\u001b\u0010\u001cJ\u000f\u0010\u001d\u001a\u00020\u0005H\u0017¢\u0006\u0004\b\u001d\u0010\u001eJ\u0015\u0010\u0006\u001a\u00020\u00052\u0006\u0010 \u001a\u00020\u001f¢\u0006\u0004\b\u0006\u0010!R\u0016\u0010#\u001a\u00020\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$R\u0016\u0010&\u001a\u00020%8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b&\u0010'R\u0016\u0010)\u001a\u00020(8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b)\u0010*R&\u0010,\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\b0+8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b,\u0010-R&\u0010.\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\b0\f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b.\u0010-R\u0016\u00100\u001a\u00020/8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b0\u00101R:\u00105\u001a&\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020302j\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u000203`48\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b5\u00106R:\u00108\u001a&\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020702j\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u000207`48\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b8\u00106¨\u0006="}, d2 = {"Lcom/discord/stores/StoreApplicationStreamPreviews;", "Lcom/discord/stores/StoreV2;", "", "Lcom/discord/primitives/StreamKey;", "streamKey", "", "fetchStreamPreviewIfNotFetching", "(Ljava/lang/String;)V", "Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;", "preview", "(Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;Ljava/lang/String;)V", "fetchStreamPreview", "", "getStreamKeyToPreviewMap", "()Ljava/util/Map;", "Lcom/discord/models/domain/ModelApplicationStream;", "applicationStream", "Lrx/Observable;", "observeStreamPreview", "(Lcom/discord/models/domain/ModelApplicationStream;)Lrx/Observable;", "(Ljava/lang/String;)Lrx/Observable;", "handleFetchStart", "url", "handleFetchSuccess", "(Ljava/lang/String;Ljava/lang/String;)V", "Lcom/discord/utilities/error/Error;", "error", "handleFetchFailed", "(Ljava/lang/String;Lcom/discord/utilities/error/Error;)V", "snapshotData", "()V", "Lcom/discord/utilities/streams/StreamContext;", "streamContext", "(Lcom/discord/utilities/streams/StreamContext;)V", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "", "streamKeyToPreviewMap", "Ljava/util/Map;", "streamKeyToPreviewMapSnapshot", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Ljava/util/HashMap;", "", "Lkotlin/collections/HashMap;", "fetchAttempts", "Ljava/util/HashMap;", "Lrx/Subscription;", "fetchStreamPreviewSubscriptions", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/updates/ObservationDeck;)V", "Companion", "StreamPreview", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreApplicationStreamPreviews extends StoreV2 {
    private static final Companion Companion = new Companion(null);
    @Deprecated
    private static final long READ_PREVIEW_DEFAULT_RETRY_DELAY_MS = 10000;
    private final Clock clock;
    private final Dispatcher dispatcher;
    private final HashMap<String, Integer> fetchAttempts;
    private final HashMap<String, Subscription> fetchStreamPreviewSubscriptions;
    private final ObservationDeck observationDeck;
    private final RestAPI restAPI;
    private final Map<String, StreamPreview> streamKeyToPreviewMap;
    private Map<String, ? extends StreamPreview> streamKeyToPreviewMapSnapshot;

    /* compiled from: StoreApplicationStreamPreviews.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0005\b\u0082\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/stores/StoreApplicationStreamPreviews$Companion;", "", "", "READ_PREVIEW_DEFAULT_RETRY_DELAY_MS", "J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreApplicationStreamPreviews.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;", "", HookHelper.constructorName, "()V", "Fetching", "Resolved", "Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview$Resolved;", "Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview$Fetching;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class StreamPreview {

        /* compiled from: StoreApplicationStreamPreviews.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview$Fetching;", "Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Fetching extends StreamPreview {
            public static final Fetching INSTANCE = new Fetching();

            private Fetching() {
                super(null);
            }
        }

        /* compiled from: StoreApplicationStreamPreviews.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0011\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001c\u0010\u0006\u001a\u00020\u00002\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0005\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview$Resolved;", "Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;", "", "component1", "()Ljava/lang/String;", "url", "copy", "(Ljava/lang/String;)Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview$Resolved;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getUrl", HookHelper.constructorName, "(Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Resolved extends StreamPreview {
            private final String url;

            public Resolved(String str) {
                super(null);
                this.url = str;
            }

            public static /* synthetic */ Resolved copy$default(Resolved resolved, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = resolved.url;
                }
                return resolved.copy(str);
            }

            public final String component1() {
                return this.url;
            }

            public final Resolved copy(String str) {
                return new Resolved(str);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Resolved) && m.areEqual(this.url, ((Resolved) obj).url);
                }
                return true;
            }

            public final String getUrl() {
                return this.url;
            }

            public int hashCode() {
                String str = this.url;
                if (str != null) {
                    return str.hashCode();
                }
                return 0;
            }

            public String toString() {
                return a.H(a.R("Resolved(url="), this.url, ")");
            }
        }

        private StreamPreview() {
        }

        public /* synthetic */ StreamPreview(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ StoreApplicationStreamPreviews(Dispatcher dispatcher, Clock clock, RestAPI restAPI, ObservationDeck observationDeck, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(dispatcher, clock, (i & 4) != 0 ? RestAPI.Companion.getApi() : restAPI, (i & 8) != 0 ? ObservationDeckProvider.get() : observationDeck);
    }

    private final void fetchStreamPreview(String str) {
        this.dispatcher.schedule(new StoreApplicationStreamPreviews$fetchStreamPreview$1(this, str));
        Subscription subscription = this.fetchStreamPreviewSubscriptions.get(str);
        if (subscription != null) {
            subscription.unsubscribe();
        }
        ObservableExtensionsKt.appSubscribe$default(ObservableExtensionsKt.restSubscribeOn(this.restAPI.getStreamPreview(str, this.clock.currentTimeMillis()), false), StoreApplicationStreamPreviews.class, (Context) null, new StoreApplicationStreamPreviews$fetchStreamPreview$2(this, str), new StoreApplicationStreamPreviews$fetchStreamPreview$4(this, str), new StoreApplicationStreamPreviews$fetchStreamPreview$5(this, str), (Function0) null, new StoreApplicationStreamPreviews$fetchStreamPreview$3(this, str), 34, (Object) null);
    }

    public final void fetchStreamPreviewIfNotFetching(StreamContext streamContext) {
        m.checkNotNullParameter(streamContext, "streamContext");
        fetchStreamPreviewIfNotFetching(streamContext.getPreview(), streamContext.getStream().getEncodedStreamKey());
    }

    public final Map<String, StreamPreview> getStreamKeyToPreviewMap() {
        return this.streamKeyToPreviewMapSnapshot;
    }

    @StoreThread
    public final void handleFetchFailed(String str, Error error) {
        m.checkNotNullParameter(str, "streamKey");
        m.checkNotNullParameter(error, "error");
        Long l = null;
        this.streamKeyToPreviewMap.put(str, new StreamPreview.Resolved(null));
        Throwable throwable = error.getThrowable();
        if (!(throwable instanceof HttpException)) {
            throwable = null;
        }
        HttpException httpException = (HttpException) throwable;
        Integer valueOf = httpException != null ? Integer.valueOf(httpException.a()) : null;
        Error.Response response = error.getResponse();
        m.checkNotNullExpressionValue(response, "error.response");
        Long retryAfterMs = response.getRetryAfterMs();
        Integer num = this.fetchAttempts.get(str);
        if (num == null) {
            num = 0;
        }
        m.checkNotNullExpressionValue(num, "fetchAttempts[streamKey] ?: 0");
        int intValue = num.intValue() + 1;
        long j = intValue * 10000;
        if (valueOf == null) {
            l = Long.valueOf(j);
        } else if (valueOf.intValue() == 429) {
            if (retryAfterMs != null) {
                j = retryAfterMs.longValue();
            }
            l = Long.valueOf(j);
        } else if (!(valueOf.intValue() == 401 || valueOf.intValue() == 403)) {
            l = Long.valueOf(j);
        }
        if (l != null) {
            Subscription subscription = this.fetchStreamPreviewSubscriptions.get(str);
            if (subscription != null) {
                subscription.unsubscribe();
            }
            Observable<Long> d02 = Observable.d0(l.longValue(), TimeUnit.MILLISECONDS);
            m.checkNotNullExpressionValue(d02, "Observable.timer(retryAf…s, TimeUnit.MILLISECONDS)");
            ObservableExtensionsKt.appSubscribe$default(d02, StoreApplicationStreamPreviews.class, (Context) null, new StoreApplicationStreamPreviews$handleFetchFailed$1(this, str), (Function1) null, new StoreApplicationStreamPreviews$handleFetchFailed$2(this, str), (Function0) null, new StoreApplicationStreamPreviews$handleFetchFailed$3(this, str), 42, (Object) null);
        }
        this.fetchAttempts.put(str, Integer.valueOf(intValue));
        markChanged();
    }

    @StoreThread
    public final void handleFetchStart(String str) {
        m.checkNotNullParameter(str, "streamKey");
        this.streamKeyToPreviewMap.put(str, StreamPreview.Fetching.INSTANCE);
        markChanged();
    }

    @StoreThread
    public final void handleFetchSuccess(String str, String str2) {
        m.checkNotNullParameter(str, "streamKey");
        m.checkNotNullParameter(str2, "url");
        this.streamKeyToPreviewMap.put(str, new StreamPreview.Resolved(str2));
        this.fetchAttempts.remove(str);
        markChanged();
    }

    public final Observable<StreamPreview> observeStreamPreview(ModelApplicationStream modelApplicationStream) {
        m.checkNotNullParameter(modelApplicationStream, "applicationStream");
        return observeStreamPreview(modelApplicationStream.getEncodedStreamKey());
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        super.snapshotData();
        this.streamKeyToPreviewMapSnapshot = new HashMap(this.streamKeyToPreviewMap);
    }

    public final Observable<StreamPreview> observeStreamPreview(String str) {
        m.checkNotNullParameter(str, "streamKey");
        Observable<StreamPreview> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreApplicationStreamPreviews$observeStreamPreview$1(this, str), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    public StoreApplicationStreamPreviews(Dispatcher dispatcher, Clock clock, RestAPI restAPI, ObservationDeck observationDeck) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(restAPI, "restAPI");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.dispatcher = dispatcher;
        this.clock = clock;
        this.restAPI = restAPI;
        this.observationDeck = observationDeck;
        this.streamKeyToPreviewMap = new HashMap();
        this.streamKeyToPreviewMapSnapshot = h0.emptyMap();
        this.fetchAttempts = new HashMap<>();
        this.fetchStreamPreviewSubscriptions = new HashMap<>();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void fetchStreamPreviewIfNotFetching(String str) {
        fetchStreamPreviewIfNotFetching(this.streamKeyToPreviewMap.get(str), str);
    }

    private final void fetchStreamPreviewIfNotFetching(StreamPreview streamPreview, String str) {
        if (streamPreview == null || !(streamPreview instanceof StreamPreview.Fetching)) {
            fetchStreamPreview(str);
        }
    }
}
