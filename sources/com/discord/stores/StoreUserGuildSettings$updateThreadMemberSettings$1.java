package com.discord.stores;

import com.discord.api.thread.ThreadMember;
import com.discord.stores.StoreUserGuildSettings;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.subjects.PublishSubject;
/* compiled from: StoreUserGuildSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/thread/ThreadMember;", "threadMember", "", "invoke", "(Lcom/discord/api/thread/ThreadMember;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserGuildSettings$updateThreadMemberSettings$1 extends o implements Function1<ThreadMember, Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ int $oldFlags;
    public final /* synthetic */ long $parentChannelId;
    public final /* synthetic */ StoreUserGuildSettings this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreUserGuildSettings$updateThreadMemberSettings$1(StoreUserGuildSettings storeUserGuildSettings, long j, long j2, int i) {
        super(1);
        this.this$0 = storeUserGuildSettings;
        this.$channelId = j;
        this.$parentChannelId = j2;
        this.$oldFlags = i;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ThreadMember threadMember) {
        invoke2(threadMember);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ThreadMember threadMember) {
        StoreAnalytics storeAnalytics;
        PublishSubject publishSubject;
        m.checkNotNullParameter(threadMember, "threadMember");
        storeAnalytics = this.this$0.analytics;
        storeAnalytics.onThreadNotificationSettingsUpdated(this.$channelId, this.$parentChannelId, threadMember.a(), this.$oldFlags);
        publishSubject = this.this$0.eventSubject;
        publishSubject.k.onNext(new StoreUserGuildSettings.Event.SettingsUpdated(StoreUserGuildSettings.SettingsUpdateType.THREAD));
    }
}
