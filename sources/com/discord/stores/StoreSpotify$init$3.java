package com.discord.stores;

import com.discord.models.domain.spotify.ModelSpotifyTrack;
import com.discord.stores.StoreSpotify;
import com.discord.utilities.time.Clock;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import rx.Subscription;
/* compiled from: StoreSpotify.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/spotify/ModelSpotifyTrack;", "track", "", "invoke", "(Lcom/discord/models/domain/spotify/ModelSpotifyTrack;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreSpotify$init$3 extends o implements Function1<ModelSpotifyTrack, Unit> {
    public final /* synthetic */ StoreSpotify this$0;

    /* compiled from: StoreSpotify.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreSpotify$init$3$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ ModelSpotifyTrack $track;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(ModelSpotifyTrack modelSpotifyTrack) {
            super(0);
            this.$track = modelSpotifyTrack;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            StoreSpotify.SpotifyState spotifyState;
            Subscription subscription;
            Clock clock;
            spotifyState = StoreSpotify$init$3.this.this$0.spotifyState;
            if (spotifyState != null) {
                StoreSpotify$init$3.this.this$0.spotifyState = StoreSpotify.SpotifyState.copy$default(spotifyState, this.$track, false, 0, 0L, 14, null);
                subscription = StoreSpotify$init$3.this.this$0.expireStateSub;
                if (subscription != null) {
                    subscription.unsubscribe();
                }
                if (this.$track != null) {
                    long durationMs = this.$track.getDurationMs() + spotifyState.getStart();
                    clock = StoreSpotify$init$3.this.this$0.clock;
                    StoreSpotify$init$3.this.this$0.startStateExpiration(durationMs - clock.currentTimeMillis());
                }
            }
            StoreSpotify$init$3.this.this$0.publishState();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreSpotify$init$3(StoreSpotify storeSpotify) {
        super(1);
        this.this$0 = storeSpotify;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ModelSpotifyTrack modelSpotifyTrack) {
        invoke2(modelSpotifyTrack);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ModelSpotifyTrack modelSpotifyTrack) {
        Dispatcher dispatcher;
        dispatcher = this.this$0.dispatcher;
        dispatcher.schedule(new AnonymousClass1(modelSpotifyTrack));
    }
}
