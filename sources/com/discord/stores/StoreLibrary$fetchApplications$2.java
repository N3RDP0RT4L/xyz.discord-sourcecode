package com.discord.stores;

import com.discord.models.domain.ModelLibraryApplication;
import d0.z.d.m;
import d0.z.d.o;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreLibrary.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/models/domain/ModelLibraryApplication;", "libraryApps", "", "invoke", "(Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreLibrary$fetchApplications$2 extends o implements Function1<List<? extends ModelLibraryApplication>, Unit> {
    public final /* synthetic */ StoreLibrary this$0;

    /* compiled from: StoreLibrary.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreLibrary$fetchApplications$2$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ List $libraryApps;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(List list) {
            super(0);
            this.$libraryApps = list;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            Map map;
            Map map2;
            HashMap hashMap = new HashMap();
            for (ModelLibraryApplication modelLibraryApplication : this.$libraryApps) {
                hashMap.put(Long.valueOf(modelLibraryApplication.getSkuId()), modelLibraryApplication);
            }
            map = StoreLibrary$fetchApplications$2.this.this$0.libraryApplications;
            map.clear();
            map2 = StoreLibrary$fetchApplications$2.this.this$0.libraryApplications;
            map2.putAll(hashMap);
            StoreLibrary$fetchApplications$2.this.this$0.markChanged();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreLibrary$fetchApplications$2(StoreLibrary storeLibrary) {
        super(1);
        this.this$0 = storeLibrary;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(List<? extends ModelLibraryApplication> list) {
        invoke2((List<ModelLibraryApplication>) list);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<ModelLibraryApplication> list) {
        m.checkNotNullParameter(list, "libraryApps");
        this.this$0.getDispatcher().schedule(new AnonymousClass1(list));
    }
}
