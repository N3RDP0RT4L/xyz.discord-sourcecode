package com.discord.stores;

import com.discord.stores.StoreNux;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreNux.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/stores/StoreNux$NuxState;", "it", "invoke", "(Lcom/discord/stores/StoreNux$NuxState;)Lcom/discord/stores/StoreNux$NuxState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreNux$setPostRegisterWithInvite$1 extends o implements Function1<StoreNux.NuxState, StoreNux.NuxState> {
    public final /* synthetic */ boolean $value;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreNux$setPostRegisterWithInvite$1(boolean z2) {
        super(1);
        this.$value = z2;
    }

    public final StoreNux.NuxState invoke(StoreNux.NuxState nuxState) {
        m.checkNotNullParameter(nuxState, "it");
        return StoreNux.NuxState.copy$default(nuxState, false, this.$value, false, false, false, null, 61, null);
    }
}
