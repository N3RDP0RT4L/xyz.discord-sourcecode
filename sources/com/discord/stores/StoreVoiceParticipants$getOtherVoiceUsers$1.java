package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.api.channel.Channel;
import com.discord.api.stageinstance.StageInstance;
import com.discord.api.stageinstance.StageInstancePrivacyLevel;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import com.discord.stores.StoreStream;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import rx.Observable;
import rx.functions.Action1;
/* compiled from: StoreVoiceParticipants.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\t\u001aB\u0012\u001a\b\u0001\u0012\u0016\u0012\u0004\u0012\u00020\u0006 \u0002*\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00050\u0005 \u0002* \u0012\u001a\b\u0001\u0012\u0016\u0012\u0004\u0012\u00020\u0006 \u0002*\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00050\u0005\u0018\u00010\u00040\u00042\u0018\u0010\u0003\u001a\u0014 \u0002*\n\u0018\u00010\u0000j\u0004\u0018\u0001`\u00010\u0000j\u0002`\u0001H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"", "Lcom/discord/primitives/UserId;", "kotlin.jvm.PlatformType", "meId", "Lrx/Observable;", "", "Lcom/discord/models/user/User;", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Long;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreVoiceParticipants$getOtherVoiceUsers$1<T, R> implements b<Long, Observable<? extends Collection<? extends User>>> {
    public final /* synthetic */ Channel $channel;
    public final /* synthetic */ Map $voiceStates;

    /* compiled from: StoreVoiceParticipants.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0003\u0010\n\u001a\u001e\u0012\b\u0012\u00060\u0001j\u0002`\u0002 \u0005*\u000e\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0018\u00010\u00070\u000726\u0010\u0006\u001a2\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\b\u0012\u00060\u0003j\u0002`\u0004 \u0005*\u0018\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\b\u0010\t"}, d2 = {"", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/member/GuildMember;", "Lcom/discord/stores/ClientGuildMember;", "kotlin.jvm.PlatformType", "members", "", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Map;)Ljava/util/Set;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreVoiceParticipants$getOtherVoiceUsers$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1<T, R> implements b<Map<Long, ? extends GuildMember>, Set<? extends Long>> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        @Override // j0.k.b
        public /* bridge */ /* synthetic */ Set<? extends Long> call(Map<Long, ? extends GuildMember> map) {
            return call2((Map<Long, GuildMember>) map);
        }

        /* renamed from: call  reason: avoid collision after fix types in other method */
        public final Set<Long> call2(Map<Long, GuildMember> map) {
            return map.keySet();
        }
    }

    public StoreVoiceParticipants$getOtherVoiceUsers$1(Channel channel, Map map) {
        this.$channel = channel;
        this.$voiceStates = map;
    }

    public final Observable<? extends Collection<User>> call(final Long l) {
        return (Observable<R>) StoreStream.Companion.getGuilds().observeComputed(this.$channel.f()).F(AnonymousClass1.INSTANCE).F(new b<Set<? extends Long>, List<? extends Long>>() { // from class: com.discord.stores.StoreVoiceParticipants$getOtherVoiceUsers$1.2
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ List<? extends Long> call(Set<? extends Long> set) {
                return call2((Set<Long>) set);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final List<Long> call2(Set<Long> set) {
                m.checkNotNullExpressionValue(set, "memberIds");
                ArrayList arrayList = new ArrayList();
                for (T t : set) {
                    long longValue = ((Number) t).longValue();
                    Long l2 = l;
                    if ((l2 == null || longValue != l2.longValue()) && StoreVoiceParticipants$getOtherVoiceUsers$1.this.$voiceStates.containsKey(Long.valueOf(longValue))) {
                        arrayList.add(t);
                    }
                }
                return arrayList;
            }
        }).Y(new b<List<? extends Long>, Observable<? extends Collection<? extends User>>>() { // from class: com.discord.stores.StoreVoiceParticipants$getOtherVoiceUsers$1.3

            /* compiled from: StoreVoiceParticipants.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0002\b\u0003\u0010\t\u001a\u0016\u0012\u0004\u0012\u00020\u0003 \u0004*\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00060\u00062.\u0010\u0005\u001a*\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003 \u0004*\u0014\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/user/User;", "kotlin.jvm.PlatformType", "otherUsers", "", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Map;)Ljava/util/Collection;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.stores.StoreVoiceParticipants$getOtherVoiceUsers$1$3$2  reason: invalid class name */
            /* loaded from: classes.dex */
            public static final class AnonymousClass2<T, R> implements b<Map<Long, ? extends User>, Collection<? extends User>> {
                public static final AnonymousClass2 INSTANCE = new AnonymousClass2();

                public final Collection<User> call(Map<Long, ? extends User> map) {
                    return map.values();
                }
            }

            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Observable<? extends Collection<? extends User>> call(List<? extends Long> list) {
                return call2((List<Long>) list);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Observable<? extends Collection<User>> call2(List<Long> list) {
                StoreUser users = StoreStream.Companion.getUsers();
                m.checkNotNullExpressionValue(list, "otherMemberIds");
                return (Observable<R>) users.observeUsers(list).t(new Action1<Map<Long, ? extends User>>() { // from class: com.discord.stores.StoreVoiceParticipants.getOtherVoiceUsers.1.3.1
                    public final void call(Map<Long, ? extends User> map) {
                        StoreStream.Companion companion = StoreStream.Companion;
                        StageInstance stageInstanceForChannel = companion.getStageInstances().getStageInstanceForChannel(StoreVoiceParticipants$getOtherVoiceUsers$1.this.$channel.h());
                        if ((stageInstanceForChannel != null ? stageInstanceForChannel.e() : null) == StageInstancePrivacyLevel.PUBLIC) {
                            StoreGuildMemberRequester guildMemberRequester = companion.getGuildMemberRequester();
                            for (Number number : StoreVoiceParticipants$getOtherVoiceUsers$1.this.$voiceStates.keySet()) {
                                long longValue = number.longValue();
                                m.checkNotNullExpressionValue(map, "otherUsers");
                                if (!map.containsKey(Long.valueOf(longValue))) {
                                    guildMemberRequester.queueRequest(StoreVoiceParticipants$getOtherVoiceUsers$1.this.$channel.f(), longValue);
                                }
                            }
                            guildMemberRequester.performQueuedRequests();
                        }
                    }
                }).F(AnonymousClass2.INSTANCE);
            }
        });
    }
}
