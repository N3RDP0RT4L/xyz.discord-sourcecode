package com.discord.stores;

import com.discord.api.application.ApplicationAsset;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreApplicationAssets.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreApplicationAssets$fetchIfNonExisting$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $appId;
    public final /* synthetic */ StoreApplicationAssets this$0;

    /* compiled from: StoreApplicationAssets.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/api/application/ApplicationAsset;", "applicationAssets", "", "invoke", "(Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreApplicationAssets$fetchIfNonExisting$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<List<? extends ApplicationAsset>, Unit> {

        /* compiled from: StoreApplicationAssets.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreApplicationAssets$fetchIfNonExisting$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C01941 extends o implements Function0<Unit> {
            public final /* synthetic */ List $applicationAssets;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C01941(List list) {
                super(0);
                this.$applicationAssets = list;
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                HashSet hashSet;
                hashSet = StoreApplicationAssets$fetchIfNonExisting$1.this.this$0.embeddedAppBackgroundsLoading;
                hashSet.remove(Long.valueOf(StoreApplicationAssets$fetchIfNonExisting$1.this.$appId));
                StoreApplicationAssets$fetchIfNonExisting$1 storeApplicationAssets$fetchIfNonExisting$1 = StoreApplicationAssets$fetchIfNonExisting$1.this;
                storeApplicationAssets$fetchIfNonExisting$1.this$0.handleApplicationAssets(storeApplicationAssets$fetchIfNonExisting$1.$appId, this.$applicationAssets);
            }
        }

        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(List<? extends ApplicationAsset> list) {
            invoke2((List<ApplicationAsset>) list);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(List<ApplicationAsset> list) {
            Dispatcher dispatcher;
            m.checkNotNullParameter(list, "applicationAssets");
            dispatcher = StoreApplicationAssets$fetchIfNonExisting$1.this.this$0.dispatcher;
            dispatcher.schedule(new C01941(list));
        }
    }

    /* compiled from: StoreApplicationAssets.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreApplicationAssets$fetchIfNonExisting$1$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2 extends o implements Function1<Error, Unit> {

        /* compiled from: StoreApplicationAssets.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreApplicationAssets$fetchIfNonExisting$1$2$1  reason: invalid class name */
        /* loaded from: classes.dex */
        public static final class AnonymousClass1 extends o implements Function0<Unit> {
            public AnonymousClass1() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                HashSet hashSet;
                hashSet = StoreApplicationAssets$fetchIfNonExisting$1.this.this$0.embeddedAppBackgroundsLoading;
                hashSet.remove(Long.valueOf(StoreApplicationAssets$fetchIfNonExisting$1.this.$appId));
            }
        }

        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Error error) {
            invoke2(error);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Error error) {
            Dispatcher dispatcher;
            m.checkNotNullParameter(error, "it");
            dispatcher = StoreApplicationAssets$fetchIfNonExisting$1.this.this$0.dispatcher;
            dispatcher.schedule(new AnonymousClass1());
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreApplicationAssets$fetchIfNonExisting$1(StoreApplicationAssets storeApplicationAssets, long j) {
        super(0);
        this.this$0 = storeApplicationAssets;
        this.$appId = j;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        Map map;
        HashSet hashSet;
        RestAPI restAPI;
        map = this.this$0.embeddedAppBackgrounds;
        if (!map.containsKey(Long.valueOf(this.$appId))) {
            hashSet = this.this$0.embeddedAppBackgroundsLoading;
            if (!hashSet.contains(Long.valueOf(this.$appId))) {
                restAPI = this.this$0.restApi;
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(restAPI.getApplicationAssets(this.$appId), false, 1, null), this.this$0.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new AnonymousClass2(), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
            }
        }
    }
}
