package com.discord.stores;

import com.discord.stores.StoreMessageState;
import d0.t.h0;
import d0.t.n0;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreMessageState.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessageState$revealSpoilerEmbed$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $messageId;
    public final /* synthetic */ int $spoilerEmbedIndex;
    public final /* synthetic */ StoreMessageState this$0;

    /* compiled from: StoreMessageState.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/stores/StoreMessageState$State;", "currentState", "invoke", "(Lcom/discord/stores/StoreMessageState$State;)Lcom/discord/stores/StoreMessageState$State;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreMessageState$revealSpoilerEmbed$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreMessageState.State, StoreMessageState.State> {
        public AnonymousClass1() {
            super(1);
        }

        public final StoreMessageState.State invoke(StoreMessageState.State state) {
            m.checkNotNullParameter(state, "currentState");
            return StoreMessageState.State.copy$default(state, null, h0.plus(state.getVisibleSpoilerEmbedMap(), d0.o.to(Integer.valueOf(StoreMessageState$revealSpoilerEmbed$1.this.$spoilerEmbedIndex), n0.emptySet())), 1, null);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreMessageState$revealSpoilerEmbed$1(StoreMessageState storeMessageState, long j, int i) {
        super(0);
        this.this$0 = storeMessageState;
        this.$messageId = j;
        this.$spoilerEmbedIndex = i;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        this.this$0.updateState(this.$messageId, new AnonymousClass1());
    }
}
