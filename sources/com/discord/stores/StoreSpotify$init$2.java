package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.models.domain.spotify.ModelSpotifyTrack;
import kotlin.Metadata;
import kotlin.Unit;
import rx.functions.Func2;
/* compiled from: StoreSpotify.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0007\u001a\u0004\u0018\u00010\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "kotlin.jvm.PlatformType", "<anonymous parameter 0>", "Lcom/discord/models/domain/spotify/ModelSpotifyTrack;", "track", NotificationCompat.CATEGORY_CALL, "(Lkotlin/Unit;Lcom/discord/models/domain/spotify/ModelSpotifyTrack;)Lcom/discord/models/domain/spotify/ModelSpotifyTrack;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreSpotify$init$2<T1, T2, R> implements Func2<Unit, ModelSpotifyTrack, ModelSpotifyTrack> {
    public static final StoreSpotify$init$2 INSTANCE = new StoreSpotify$init$2();

    public final ModelSpotifyTrack call(Unit unit, ModelSpotifyTrack modelSpotifyTrack) {
        return modelSpotifyTrack;
    }
}
