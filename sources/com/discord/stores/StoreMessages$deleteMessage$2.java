package com.discord.stores;

import com.discord.models.domain.ModelMessageDelete;
import com.discord.models.message.Message;
import com.discord.utilities.messagesend.MessageQueue;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreMessages.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessages$deleteMessage$2 extends o implements Function0<Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ Message $message;
    public final /* synthetic */ long $messageId;
    public final /* synthetic */ StoreMessages this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreMessages$deleteMessage$2(StoreMessages storeMessages, Message message, long j, long j2) {
        super(0);
        this.this$0 = storeMessages;
        this.$message = message;
        this.$channelId = j;
        this.$messageId = j2;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreStream storeStream;
        MessageQueue orCreateMessageQueue;
        if (this.$message.isLocal()) {
            String nonce = this.$message.getNonce();
            if (nonce != null) {
                orCreateMessageQueue = this.this$0.getOrCreateMessageQueue(this.$channelId);
                orCreateMessageQueue.cancel(nonce);
            }
            this.this$0.handleLocalMessageDelete(this.$message);
            Integer type = this.$message.getType();
            if (type != null && type.intValue() == -2) {
                this.this$0.trackFailedLocalMessageResolved(this.$message, FailedMessageResolutionType.DELETED);
            }
        } else if (this.$message.isEphemeralMessage()) {
            this.this$0.handleMessageDelete(new ModelMessageDelete(this.$channelId, this.$messageId));
        }
        storeStream = this.this$0.stream;
        storeStream.handleLocalMessageDelete(this.$message);
    }
}
