package com.discord.stores;

import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.functions.Function2;
/* compiled from: VoiceConfigurationCache.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\t\u001a\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u00060\u00032\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"", "key", "value", "Lkotlin/Pair;", "", "Lcom/discord/primitives/UserId;", "", "invoke", "(Ljava/lang/String;Ljava/lang/String;)Lkotlin/Pair;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class VoiceConfigurationCache$read$1 extends o implements Function2<String, String, Pair<? extends Long, ? extends Boolean>> {
    public static final VoiceConfigurationCache$read$1 INSTANCE = new VoiceConfigurationCache$read$1();

    public VoiceConfigurationCache$read$1() {
        super(2);
    }

    public final Pair<Long, Boolean> invoke(String str, String str2) {
        m.checkNotNullParameter(str, "key");
        m.checkNotNullParameter(str2, "value");
        return d0.o.to(Long.valueOf(Long.parseLong(str)), Boolean.valueOf(Boolean.parseBoolean(str2)));
    }
}
