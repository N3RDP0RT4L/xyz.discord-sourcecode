package com.discord.stores;

import com.discord.api.directory.DirectoryEntryGuild;
import com.discord.stores.utilities.RestCallState;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreDirectories.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\b\u001a\u00020\u00052 \u0010\u0004\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0002 \u0003*\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u00010\u00010\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Lcom/discord/stores/utilities/RestCallState;", "", "Lcom/discord/api/directory/DirectoryEntryGuild;", "kotlin.jvm.PlatformType", "entriesResponse", "", "invoke", "(Lcom/discord/stores/utilities/RestCallState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreDirectories$fetchDirectoriesForChannel$2 extends o implements Function1<RestCallState<? extends List<? extends DirectoryEntryGuild>>, Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ StoreDirectories this$0;

    /* compiled from: StoreDirectories.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreDirectories$fetchDirectoriesForChannel$2$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ RestCallState $entriesResponse;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(RestCallState restCallState) {
            super(0);
            this.$entriesResponse = restCallState;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            Map map;
            map = StoreDirectories$fetchDirectoriesForChannel$2.this.this$0.directoriesMap;
            map.put(Long.valueOf(StoreDirectories$fetchDirectoriesForChannel$2.this.$channelId), this.$entriesResponse);
            StoreDirectories$fetchDirectoriesForChannel$2.this.this$0.markChanged();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreDirectories$fetchDirectoriesForChannel$2(StoreDirectories storeDirectories, long j) {
        super(1);
        this.this$0 = storeDirectories;
        this.$channelId = j;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(RestCallState<? extends List<? extends DirectoryEntryGuild>> restCallState) {
        invoke2((RestCallState<? extends List<DirectoryEntryGuild>>) restCallState);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(RestCallState<? extends List<DirectoryEntryGuild>> restCallState) {
        Dispatcher dispatcher;
        m.checkNotNullParameter(restCallState, "entriesResponse");
        dispatcher = this.this$0.dispatcher;
        dispatcher.schedule(new AnonymousClass1(restCallState));
    }
}
