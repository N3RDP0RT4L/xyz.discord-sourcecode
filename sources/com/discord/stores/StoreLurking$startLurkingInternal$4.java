package com.discord.stores;

import com.discord.api.guild.Guild;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function3;
import rx.subjects.BehaviorSubject;
/* compiled from: StoreLurking.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/guild/Guild;", "it", "", "invoke", "(Lcom/discord/api/guild/Guild;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreLurking$startLurkingInternal$4 extends o implements Function1<Guild, Unit> {
    public final /* synthetic */ Long $channelId;
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ Function3 $onGuildLurked;
    public final /* synthetic */ StoreLurking this$0;

    /* compiled from: StoreLurking.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreLurking$startLurkingInternal$4$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ Guild $it;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Guild guild) {
            super(0);
            this.$it = guild;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            Map map;
            BehaviorSubject behaviorSubject;
            Map map2;
            if (!StoreLurking$startLurkingInternal$4.this.this$0.isLurking$app_productionGoogleRelease(this.$it)) {
                map = StoreLurking$startLurkingInternal$4.this.this$0.lurkedGuilds;
                map.remove(Long.valueOf(StoreLurking$startLurkingInternal$4.this.$guildId));
                behaviorSubject = StoreLurking$startLurkingInternal$4.this.this$0.lurkedGuildsSubject;
                map2 = StoreLurking$startLurkingInternal$4.this.this$0.lurkedGuilds;
                behaviorSubject.onNext(map2);
            }
            StoreLurking$startLurkingInternal$4.this.$onGuildLurked.invoke(new com.discord.models.guild.Guild(this.$it), StoreLurking$startLurkingInternal$4.this.$channelId, Boolean.FALSE);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreLurking$startLurkingInternal$4(StoreLurking storeLurking, long j, Function3 function3, Long l) {
        super(1);
        this.this$0 = storeLurking;
        this.$guildId = j;
        this.$onGuildLurked = function3;
        this.$channelId = l;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Guild guild) {
        invoke2(guild);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Guild guild) {
        Dispatcher dispatcher;
        m.checkNotNullParameter(guild, "it");
        dispatcher = this.this$0.dispatcher;
        dispatcher.schedule(new AnonymousClass1(guild));
    }
}
