package com.discord.stores;

import com.discord.models.message.Message;
import d0.t.n;
import d0.z.d.o;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StorePinnedMessages.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/models/message/Message;", "invoke", "()Lcom/discord/models/message/Message;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StorePinnedMessages$observePinnedMessage$1 extends o implements Function0<Message> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ long $messageId;
    public final /* synthetic */ StorePinnedMessages this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StorePinnedMessages$observePinnedMessage$1(StorePinnedMessages storePinnedMessages, long j, long j2) {
        super(0);
        this.this$0 = storePinnedMessages;
        this.$channelId = j;
        this.$messageId = j2;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final Message invoke() {
        Map map;
        Message message;
        map = this.this$0.pinnedMessages;
        List list = (List) map.get(Long.valueOf(this.$channelId));
        if (list == null) {
            list = n.emptyList();
        }
        message = StorePinnedMessages.Companion.getMessage(list, this.$messageId);
        return message;
    }
}
