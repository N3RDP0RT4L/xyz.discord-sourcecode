package com.discord.stores;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import com.discord.models.domain.ModelCall;
import com.discord.stores.updates.ObservationDeck;
import d0.z.d.m;
import java.util.HashSet;
import java.util.Set;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: StoreCallsIncoming.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010#\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u001e\u001a\u00020\u001d\u0012\u0006\u0010\u001b\u001a\u00020\u001a\u0012\u0006\u0010%\u001a\u00020$¢\u0006\u0004\b'\u0010(J\u001b\u0010\u0006\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0013\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00020\b¢\u0006\u0004\b\t\u0010\nJ\u0019\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00020\b0\u000b¢\u0006\u0004\b\f\u0010\rJ\u0013\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\u000b¢\u0006\u0004\b\u000f\u0010\rJ\u000f\u0010\u0010\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0010\u0010\u0011J\u0019\u0010\u0012\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0012\u0010\u0007J\u001b\u0010\u0013\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0007¢\u0006\u0004\b\u0013\u0010\u0007J\u0017\u0010\u0016\u001a\u00020\u00052\u0006\u0010\u0015\u001a\u00020\u0014H\u0007¢\u0006\u0004\b\u0016\u0010\u0017J\u0017\u0010\u0019\u001a\u00020\u00052\u0006\u0010\u0018\u001a\u00020\u0014H\u0007¢\u0006\u0004\b\u0019\u0010\u0017R\u0016\u0010\u001b\u001a\u00020\u001a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\u0016\u0010\u001e\u001a\u00020\u001d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR\u001c\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00020 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"R\u001c\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00020\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b#\u0010\"R\u0016\u0010%\u001a\u00020$8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b%\u0010&¨\u0006)"}, d2 = {"Lcom/discord/stores/StoreCallsIncoming;", "Lcom/discord/stores/StoreV2;", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "clearIncomingCall", "(J)V", "", "getIncomingCalls", "()Ljava/util/Set;", "Lrx/Observable;", "observeIncoming", "()Lrx/Observable;", "", "observeHasIncoming", "snapshotData", "()V", "removeIncomingCall", "handleVoiceChannelSelected", "Lcom/discord/models/domain/ModelCall;", NotificationCompat.CATEGORY_CALL, "handleCallCreateOrUpdate", "(Lcom/discord/models/domain/ModelCall;)V", "callDelete", "handleCallDelete", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "", "incomingCalls", "Ljava/util/Set;", "incomingCallSnapshot", "Lcom/discord/stores/StoreUser;", "userStore", "Lcom/discord/stores/StoreUser;", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/StoreUser;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreCallsIncoming extends StoreV2 {
    private final Dispatcher dispatcher;
    private final ObservationDeck observationDeck;
    private final StoreUser userStore;
    private final Set<Long> incomingCalls = new HashSet();
    private Set<Long> incomingCallSnapshot = new HashSet();

    public StoreCallsIncoming(Dispatcher dispatcher, ObservationDeck observationDeck, StoreUser storeUser) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(storeUser, "userStore");
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
        this.userStore = storeUser;
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void clearIncomingCall(long j) {
        if (this.incomingCalls.contains(Long.valueOf(j))) {
            this.incomingCalls.remove(Long.valueOf(j));
            markChanged();
        }
    }

    public final Set<Long> getIncomingCalls() {
        return this.incomingCallSnapshot;
    }

    @StoreThread
    public final void handleCallCreateOrUpdate(ModelCall modelCall) {
        m.checkNotNullParameter(modelCall, NotificationCompat.CATEGORY_CALL);
        long channelId = modelCall.getChannelId();
        if (!modelCall.getRinging().contains(Long.valueOf(this.userStore.getMe().getId()))) {
            clearIncomingCall(channelId);
        } else if (!this.incomingCalls.contains(Long.valueOf(channelId))) {
            this.incomingCalls.add(Long.valueOf(channelId));
            markChanged();
        }
    }

    @StoreThread
    public final void handleCallDelete(ModelCall modelCall) {
        m.checkNotNullParameter(modelCall, "callDelete");
        clearIncomingCall(modelCall.getChannelId());
    }

    @StoreThread
    public final void handleVoiceChannelSelected(long j) {
        clearIncomingCall(j);
    }

    public final Observable<Boolean> observeHasIncoming() {
        Observable<Boolean> q = observeIncoming().F(StoreCallsIncoming$observeHasIncoming$1.INSTANCE).q();
        m.checkNotNullExpressionValue(q, "observeIncoming()\n      …  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Set<Long>> observeIncoming() {
        Observable<Set<Long>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreCallsIncoming$observeIncoming$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck\n        …  .distinctUntilChanged()");
        return q;
    }

    public final void removeIncomingCall(long j) {
        this.dispatcher.schedule(new StoreCallsIncoming$removeIncomingCall$1(this, j));
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        super.snapshotData();
        this.incomingCallSnapshot = new HashSet(this.incomingCalls);
    }
}
