package com.discord.stores;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import b.d.b.a.a;
import co.discord.media_engine.VideoInputDeviceDescription;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guild.GuildFeature;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.message.Message;
import com.discord.api.permission.Permission;
import com.discord.api.permission.PermissionOverwrite;
import com.discord.api.role.GuildRole;
import com.discord.api.thread.ThreadMetadata;
import com.discord.api.voice.state.VoiceState;
import com.discord.app.AppComponent;
import com.discord.app.AppLog;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelInvite;
import com.discord.models.domain.ModelNotificationSettings;
import com.discord.models.domain.ModelPayload;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import com.discord.rtcconnection.RtcConnection;
import com.discord.rtcconnection.audio.DiscordAudioManager;
import com.discord.utilities.KotlinExtensionsKt;
import com.discord.utilities.PermissionOverwriteUtilsKt;
import com.discord.utilities.SnowflakeUtils;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.AnalyticsUtils;
import com.discord.utilities.analytics.SearchType;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.collections.CollectionExtensionsKt;
import com.discord.utilities.intent.RouteHandlers;
import com.discord.utilities.rest.FileUploadAlertType;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.Clock;
import com.discord.widgets.chat.input.emoji.EmojiPickerContextType;
import com.discord.widgets.guilds.invite.GuildInvite;
import com.discord.widgets.voice.feedback.FeedbackIssue;
import com.discord.widgets.voice.feedback.PendingFeedback;
import d0.d0.f;
import d0.o;
import d0.t.g0;
import d0.t.h0;
import d0.t.n;
import d0.z.d.m;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import kotlin.Metadata;
import kotlin.Pair;
import rx.Observable;
import rx.subjects.PublishSubject;
/* compiled from: StoreAnalytics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000ø\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u001e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\"\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0017\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0002\u0094\u0002B'\u0012\b\u0010\u008d\u0002\u001a\u00030\u008c\u0002\u0012\b\u0010\u0087\u0002\u001a\u00030\u0086\u0002\u0012\b\u0010\u008a\u0002\u001a\u00030\u0089\u0002¢\u0006\u0006\b\u0092\u0002\u0010\u0093\u0002J!\u0010\u0007\u001a\u00020\u00062\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u000b\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u000f\u0010\r\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\r\u0010\u000eJ)\u0010\u0015\u001a\u0010\u0012\u0004\u0012\u00020\u0013\u0012\u0006\u0012\u0004\u0018\u00010\u00140\u00122\n\u0010\u0011\u001a\u00060\u000fj\u0002`\u0010H\u0002¢\u0006\u0004\b\u0015\u0010\u0016J3\u0010\u0018\u001a\u0010\u0012\u0004\u0012\u00020\u0013\u0012\u0006\u0012\u0004\u0018\u00010\u00140\u00122\n\u0010\u0011\u001a\u00060\u000fj\u0002`\u00102\b\b\u0002\u0010\u0017\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0018\u0010\u0019J/\u0010\u0018\u001a\u0010\u0012\u0004\u0012\u00020\u0013\u0012\u0006\u0012\u0004\u0018\u00010\u00140\u00122\u0006\u0010\u001b\u001a\u00020\u001a2\b\b\u0002\u0010\u0017\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0018\u0010\u001cJ%\u0010\u001d\u001a\u0010\u0012\u0004\u0012\u00020\u0013\u0012\u0006\u0012\u0004\u0018\u00010\u00140\u00122\u0006\u0010\u001b\u001a\u00020\u001aH\u0002¢\u0006\u0004\b\u001d\u0010\u001eJ'\u0010!\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140\u00122\n\u0010 \u001a\u00060\u000fj\u0002`\u001fH\u0002¢\u0006\u0004\b!\u0010\u0016J#\u0010!\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140\u00122\u0006\u0010#\u001a\u00020\"H\u0002¢\u0006\u0004\b!\u0010$J'\u0010&\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140%2\n\u0010 \u001a\u00060\u000fj\u0002`\u001fH\u0003¢\u0006\u0004\b&\u0010\u0016Ja\u00100\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140%2\n\u0010 \u001a\u00060\u000fj\u0002`\u001f2\u0006\u0010(\u001a\u00020'2\f\u0010*\u001a\b\u0012\u0004\u0012\u00020\u001a0)2\u0006\u0010+\u001a\u00020'2\u0006\u0010,\u001a\u00020'2\n\u0010.\u001a\u00060\u000fj\u0002`-2\u0006\u0010/\u001a\u00020\u0004H\u0002¢\u0006\u0004\b0\u00101J\u001b\u00104\u001a\u0004\u0018\u00010\u00132\b\u00103\u001a\u0004\u0018\u000102H\u0002¢\u0006\u0004\b4\u00105J5\u0010;\u001a\u00020\u00062\u0006\u00107\u001a\u0002062\u0006\u00109\u001a\u0002082\u0014\b\u0002\u0010:\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140\u0012H\u0002¢\u0006\u0004\b;\u0010<J\u0017\u0010?\u001a\u00020\u00062\u0006\u0010>\u001a\u00020=H\u0016¢\u0006\u0004\b?\u0010@J\u0019\u0010B\u001a\u00020\u00062\b\u0010A\u001a\u0004\u0018\u00010\u0013H\u0007¢\u0006\u0004\bB\u0010CJ\u0019\u0010E\u001a\u00020\u00062\b\u0010D\u001a\u0004\u0018\u00010\u0013H\u0007¢\u0006\u0004\bE\u0010CJ\u000f\u0010F\u001a\u00020\u0006H\u0007¢\u0006\u0004\bF\u0010\u000eJ\u0017\u0010H\u001a\u00020\u00062\u0006\u0010G\u001a\u00020\u0004H\u0007¢\u0006\u0004\bH\u0010IJ\u0017\u0010L\u001a\u00020\u00062\u0006\u0010K\u001a\u00020JH\u0007¢\u0006\u0004\bL\u0010MJ\u0019\u0010O\u001a\u00020\u00062\b\u0010N\u001a\u0004\u0018\u00010\u0002H\u0007¢\u0006\u0004\bO\u0010PJ\u0017\u0010Q\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u0004H\u0007¢\u0006\u0004\bQ\u0010IJ+\u0010U\u001a\u00020\u00062\u0006\u0010R\u001a\u00020\u00132\n\u0010T\u001a\u00060\u000fj\u0002`S2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001a¢\u0006\u0004\bU\u0010VJ\u001d\u0010Y\u001a\u00020\u00062\f\u0010X\u001a\b\u0012\u0004\u0012\u00020\u000f0WH\u0007¢\u0006\u0004\bY\u0010ZJ\u001d\u0010^\u001a\u00020\u00062\u000e\u0010]\u001a\n\u0012\u0006\b\u0001\u0012\u00020\\0[¢\u0006\u0004\b^\u0010_J\u0019\u0010`\u001a\u00020\u00062\n\u0010\u0011\u001a\u00060\u000fj\u0002`\u0010¢\u0006\u0004\b`\u0010aJ\u001d\u0010f\u001a\u00020\u00062\u0006\u0010c\u001a\u00020b2\u0006\u0010e\u001a\u00020d¢\u0006\u0004\bf\u0010gJ'\u0010m\u001a\u00020\u00062\b\u0010i\u001a\u0004\u0018\u00010h2\u0006\u0010k\u001a\u00020j2\u0006\u0010l\u001a\u00020\u0013¢\u0006\u0004\bm\u0010nJ1\u0010m\u001a\u00020\u00062\b\u0010i\u001a\u0004\u0018\u00010o2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001a2\u0006\u0010k\u001a\u00020j2\u0006\u0010l\u001a\u00020\u0013¢\u0006\u0004\bm\u0010pJ\u0015\u0010r\u001a\u00020\u00062\u0006\u0010q\u001a\u00020\u0004¢\u0006\u0004\br\u0010IJ!\u0010t\u001a\u00020\u00062\u0006\u0010s\u001a\u00020\u00132\n\u0010 \u001a\u00060\u000fj\u0002`\u001f¢\u0006\u0004\bt\u0010uJ!\u0010w\u001a\u00020\u00062\u0006\u0010s\u001a\u00020\u00132\n\b\u0002\u0010v\u001a\u0004\u0018\u00010\u0013¢\u0006\u0004\bw\u0010xJ%\u0010{\u001a\u00020\u00062\u0006\u0010z\u001a\u00020y2\u000e\u0010\u0011\u001a\n\u0018\u00010\u000fj\u0004\u0018\u0001`\u0010¢\u0006\u0004\b{\u0010|J8\u0010\u0080\u0001\u001a\u00020\u00062\n\u0010\u0011\u001a\u00060\u000fj\u0002`\u00102\n\u0010}\u001a\u00060\u000fj\u0002`\u00102\u0006\u0010~\u001a\u00020'2\u0006\u0010\u007f\u001a\u00020'¢\u0006\u0006\b\u0080\u0001\u0010\u0081\u0001J)\u0010\u0083\u0001\u001a\u00020\u00062\u0007\u0010\u0082\u0001\u001a\u00020\u00132\u0006\u0010l\u001a\u00020\u00132\u0006\u0010\u0011\u001a\u00020\u000f¢\u0006\u0006\b\u0083\u0001\u0010\u0084\u0001J\u001d\u0010\u0085\u0001\u001a\u00020\u00062\n\u0010 \u001a\u00060\u000fj\u0002`\u001fH\u0007¢\u0006\u0005\b\u0085\u0001\u0010aJ4\u0010\u008a\u0001\u001a\u00020\u00062\n\u0010\u0011\u001a\u00060\u000fj\u0002`\u00102\b\u0010\u0087\u0001\u001a\u00030\u0086\u00012\n\u0010\u0089\u0001\u001a\u0005\u0018\u00010\u0088\u0001H\u0007¢\u0006\u0006\b\u008a\u0001\u0010\u008b\u0001J\u001b\u0010\u008c\u0001\u001a\u00020\u00062\n\u0010\u0011\u001a\u00060\u000fj\u0002`\u0010¢\u0006\u0005\b\u008c\u0001\u0010aJ\u001a\u0010\u008f\u0001\u001a\u00020\u00062\b\u0010\u008e\u0001\u001a\u00030\u008d\u0001¢\u0006\u0006\b\u008f\u0001\u0010\u0090\u0001J\u001a\u0010\u0093\u0001\u001a\u00020\u00062\b\u0010\u0092\u0001\u001a\u00030\u0091\u0001¢\u0006\u0006\b\u0093\u0001\u0010\u0094\u0001J$\u0010\u0095\u0001\u001a\u00020\u00062\u0012\u0010:\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140%¢\u0006\u0006\b\u0095\u0001\u0010\u0096\u0001J$\u0010\u0097\u0001\u001a\u00020\u00062\u0012\u0010:\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140%¢\u0006\u0006\b\u0097\u0001\u0010\u0096\u0001J&\u0010\u0098\u0001\u001a\u00020\u00062\u0012\u0010:\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140%H\u0007¢\u0006\u0006\b\u0098\u0001\u0010\u0096\u0001J$\u0010\u0099\u0001\u001a\u00020\u00062\u0012\u0010:\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140%¢\u0006\u0006\b\u0099\u0001\u0010\u0096\u0001J&\u0010\u009a\u0001\u001a\u00020\u00062\u0012\u0010:\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140%H\u0007¢\u0006\u0006\b\u009a\u0001\u0010\u0096\u0001J\u001b\u0010\u009b\u0001\u001a\u00020\u00062\n\u0010 \u001a\u00060\u000fj\u0002`\u001f¢\u0006\u0005\b\u009b\u0001\u0010aJ3\u0010¡\u0001\u001a\u00020\u00062\b\u0010\u009d\u0001\u001a\u00030\u009c\u00012\f\b\u0002\u0010\u009f\u0001\u001a\u0005\u0018\u00010\u009e\u00012\t\b\u0002\u0010 \u0001\u001a\u00020\u0004¢\u0006\u0006\b¡\u0001\u0010¢\u0001J3\u0010£\u0001\u001a\u00020\u00062\b\u0010\u009d\u0001\u001a\u00030\u009c\u00012\f\b\u0002\u0010\u009f\u0001\u001a\u0005\u0018\u00010\u009e\u00012\t\b\u0002\u0010 \u0001\u001a\u00020\u0004¢\u0006\u0006\b£\u0001\u0010¢\u0001JI\u0010¦\u0001\u001a\u00020\u00062\b\u0010\u009d\u0001\u001a\u00030\u009c\u00012\u0007\u0010¤\u0001\u001a\u00020'2\u000b\b\u0002\u0010¥\u0001\u001a\u0004\u0018\u00010'2\f\b\u0002\u0010\u009f\u0001\u001a\u0005\u0018\u00010\u009e\u00012\t\b\u0002\u0010 \u0001\u001a\u00020\u0004¢\u0006\u0006\b¦\u0001\u0010§\u0001J?\u0010ª\u0001\u001a\u00020\u00062\b\u0010\u009d\u0001\u001a\u00030\u009c\u00012\u0007\u0010¤\u0001\u001a\u00020'2\f\b\u0002\u0010\u009f\u0001\u001a\u0005\u0018\u00010\u009e\u00012\f\b\u0002\u0010©\u0001\u001a\u0005\u0018\u00010¨\u0001¢\u0006\u0006\bª\u0001\u0010«\u0001J\u0017\u0010¬\u0001\u001a\u00020\u00062\u0006\u00107\u001a\u00020\u0013¢\u0006\u0005\b¬\u0001\u0010CJP\u0010µ\u0001\u001a\u00020\u00062\b\u0010®\u0001\u001a\u00030\u00ad\u00012\u0007\u0010¯\u0001\u001a\u00020'2\u0007\u0010°\u0001\u001a\u00020'2\u0007\u0010±\u0001\u001a\u00020'2\u0007\u0010²\u0001\u001a\u00020\u00042\u0007\u0010³\u0001\u001a\u00020\u00042\u0007\u0010´\u0001\u001a\u00020\u0004¢\u0006\u0006\bµ\u0001\u0010¶\u0001Jj\u0010¼\u0001\u001a\u00020\u00062\u0007\u0010¯\u0001\u001a\u00020'2\u0007\u0010°\u0001\u001a\u00020'2\u0007\u0010±\u0001\u001a\u00020'2\u0007\u0010²\u0001\u001a\u00020\u00042\u0007\u0010³\u0001\u001a\u00020\u00042\b\u0010¸\u0001\u001a\u00030·\u00012\f\u0010º\u0001\u001a\u00070\u000fj\u0003`¹\u00012\u0007\u0010»\u0001\u001a\u00020'2\n\u0010\u0011\u001a\u00060\u000fj\u0002`\u0010¢\u0006\u0006\b¼\u0001\u0010½\u0001J\u001b\u0010¾\u0001\u001a\u00020\u00062\n\u0010\u0011\u001a\u00060\u000fj\u0002`\u0010¢\u0006\u0005\b¾\u0001\u0010aJ.\u0010Á\u0001\u001a\u00020\u00062\n\u0010\u0011\u001a\u00060\u000fj\u0002`\u00102\u0007\u0010¿\u0001\u001a\u00020\u000f2\u0007\u0010À\u0001\u001a\u00020\u000f¢\u0006\u0006\bÁ\u0001\u0010Â\u0001J\u000f\u0010Ã\u0001\u001a\u00020\u0006¢\u0006\u0005\bÃ\u0001\u0010\u000eJ\u0018\u0010Å\u0001\u001a\u00020\u00062\u0007\u0010Ä\u0001\u001a\u00020\u000f¢\u0006\u0005\bÅ\u0001\u0010aJ4\u0010È\u0001\u001a\u00020\u00062\u0007\u0010¿\u0001\u001a\u00020\u000f2\u0007\u0010À\u0001\u001a\u00020\u000f2\u0007\u0010Æ\u0001\u001a\u00020\u00132\u0007\u0010Ç\u0001\u001a\u00020\u0004¢\u0006\u0006\bÈ\u0001\u0010É\u0001J=\u0010Î\u0001\u001a\u00020\u00062\n\u0010 \u001a\u00060\u000fj\u0002`\u001f2\u000e\u0010Ë\u0001\u001a\t\u0012\u0004\u0012\u00020\u001a0Ê\u00012\u000f\u0010Í\u0001\u001a\n\u0012\u0005\u0012\u00030Ì\u00010Ê\u0001¢\u0006\u0006\bÎ\u0001\u0010Ï\u0001J%\u0010Ñ\u0001\u001a\u00020\u00062\n\u0010\u0011\u001a\u00060\u000fj\u0002`\u00102\u0007\u0010Ð\u0001\u001a\u00020\u0013¢\u0006\u0006\bÑ\u0001\u0010Ò\u0001J\u000f\u0010Ó\u0001\u001a\u00020\u0006¢\u0006\u0005\bÓ\u0001\u0010\u000eJ\u0018\u0010Ô\u0001\u001a\u00020\u00062\u0006\u0010\u001b\u001a\u00020\u001a¢\u0006\u0006\bÔ\u0001\u0010Õ\u0001JA\u0010Ú\u0001\u001a\u00020\u00062\u0006\u0010\u001b\u001a\u00020\u001a2\f\u0010×\u0001\u001a\u00070\u000fj\u0003`Ö\u00012\u0007\u0010´\u0001\u001a\u00020\u00042\u0007\u0010Ø\u0001\u001a\u00020\u00042\u0007\u0010Ù\u0001\u001a\u00020\u0004¢\u0006\u0006\bÚ\u0001\u0010Û\u0001J\u0018\u0010Ü\u0001\u001a\u00020\u00062\u0006\u00109\u001a\u000208¢\u0006\u0006\bÜ\u0001\u0010Ý\u0001J!\u0010ß\u0001\u001a\u00020\u00062\u0006\u00109\u001a\u0002082\u0007\u0010Þ\u0001\u001a\u00020\u0004¢\u0006\u0006\bß\u0001\u0010à\u0001J$\u0010ã\u0001\u001a\u00020\u00062\u0007\u0010á\u0001\u001a\u00020\u00042\t\u0010â\u0001\u001a\u0004\u0018\u00010\u0004¢\u0006\u0006\bã\u0001\u0010ä\u0001J\u000f\u0010å\u0001\u001a\u00020\u0006¢\u0006\u0005\bå\u0001\u0010\u000eJ\u000f\u0010æ\u0001\u001a\u00020\u0006¢\u0006\u0005\bæ\u0001\u0010\u000eJ7\u0010ê\u0001\u001a\u00020\u00062\n\u0010\u0011\u001a\u00060\u000fj\u0002`\u00102\u0007\u0010ç\u0001\u001a\u00020\u00132\u0007\u0010è\u0001\u001a\u00020'2\u0007\u0010é\u0001\u001a\u00020'¢\u0006\u0006\bê\u0001\u0010ë\u0001J_\u0010ð\u0001\u001a\u00020\u00062\n\u0010\u0011\u001a\u00060\u000fj\u0002`\u00102\u0007\u0010ç\u0001\u001a\u00020\u00132\u0007\u0010è\u0001\u001a\u00020'2\u0007\u0010é\u0001\u001a\u00020'2\t\u0010ì\u0001\u001a\u0004\u0018\u00010\u00132\t\u0010í\u0001\u001a\u0004\u0018\u00010\u00132\u0010\u0010ï\u0001\u001a\u000b\u0018\u00010\u000fj\u0005\u0018\u0001`î\u0001¢\u0006\u0006\bð\u0001\u0010ñ\u0001J0\u0010õ\u0001\u001a\u00020\u00062\n\u0010\u0011\u001a\u00060\u000fj\u0002`\u00102\b\u0010ó\u0001\u001a\u00030ò\u00012\b\u0010ô\u0001\u001a\u00030ò\u0001¢\u0006\u0006\bõ\u0001\u0010ö\u0001R\u0017\u0010ù\u0001\u001a\u00030\u009e\u00018F@\u0006¢\u0006\b\u001a\u0006\b÷\u0001\u0010ø\u0001RB\u0010ü\u0001\u001a+\u0012\r\u0012\u000b û\u0001*\u0004\u0018\u00010\t0\t û\u0001*\u0014\u0012\r\u0012\u000b û\u0001*\u0004\u0018\u00010\t0\t\u0018\u00010ú\u00010ú\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bü\u0001\u0010ý\u0001R'\u0010\u0080\u0002\u001a\u0010\u0012\u0004\u0012\u00020\u0013\u0012\u0006\u0012\u0004\u0018\u00010\u00140\u00128B@\u0002X\u0082\u0004¢\u0006\b\u001a\u0006\bþ\u0001\u0010ÿ\u0001R\u001b\u0010\u0081\u0002\u001a\u0004\u0018\u00010\u00138\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u0081\u0002\u0010\u0082\u0002R\u001b\u0010\u0083\u0002\u001a\u0004\u0018\u00010\u001a8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u0083\u0002\u0010\u0084\u0002R\u001b\u0010\u0085\u0002\u001a\u0004\u0018\u00010\u00138\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u0085\u0002\u0010\u0082\u0002R\u001a\u0010\u0087\u0002\u001a\u00030\u0086\u00028\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0087\u0002\u0010\u0088\u0002R\u0019\u0010D\u001a\u0004\u0018\u00010\u00138\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\bD\u0010\u0082\u0002R\u001a\u0010\u008a\u0002\u001a\u00030\u0089\u00028\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u008a\u0002\u0010\u008b\u0002R\u0019\u0010A\u001a\u0004\u0018\u00010\u00138\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\bA\u0010\u0082\u0002R\u001a\u0010\u008d\u0002\u001a\u00030\u008c\u00028\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u008d\u0002\u0010\u008e\u0002R\u001a\u0010\u0090\u0002\u001a\u00030\u008f\u00028\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u0090\u0002\u0010\u0091\u0002¨\u0006\u0095\u0002"}, d2 = {"Lcom/discord/stores/StoreAnalytics;", "Lcom/discord/stores/Store;", "Lco/discord/media_engine/VideoInputDeviceDescription;", "selectedVideoInputDevice", "", "isScreenSharing", "", "handleVideoInputUpdate", "(Lco/discord/media_engine/VideoInputDeviceDescription;Z)V", "Lcom/discord/stores/StoreAnalytics$ScreenViewed;", "screenViewed", "onScreenViewed", "(Lcom/discord/stores/StoreAnalytics$ScreenViewed;)V", "updateTrackingData", "()V", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "", "", "getGuildAndChannelSnapshotAnalyticsProperties", "(J)Ljava/util/Map;", "includeNsfw", "getChannelSnapshotAnalyticsProperties", "(JZ)Ljava/util/Map;", "Lcom/discord/api/channel/Channel;", "channel", "(Lcom/discord/api/channel/Channel;Z)Ljava/util/Map;", "getThreadSnapshotAnalyticsProperties", "(Lcom/discord/api/channel/Channel;)Ljava/util/Map;", "Lcom/discord/primitives/GuildId;", "guildId", "getGuildSnapshotAnalyticsProperties", "Lcom/discord/models/guild/Guild;", "guild", "(Lcom/discord/models/guild/Guild;)Ljava/util/Map;", "", "getGuildAnalyticsPropertiesInternal", "", "guildSize", "", "guildChannels", "numGuildRoles", "guildMemberNumRoles", "Lcom/discord/api/permission/PermissionBit;", "guildPermissions", "isVip", "guildPropertiesMap", "(JILjava/util/Collection;IIJZ)Ljava/util/Map;", "Lcom/discord/widgets/voice/feedback/FeedbackIssue;", "issue", "getStreamFeedbackReasonFromIssue", "(Lcom/discord/widgets/voice/feedback/FeedbackIssue;)Ljava/lang/String;", "Lcom/discord/utilities/analytics/AnalyticsTracker$PremiumUpsellType;", "type", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerContextType;", "emojiPickerContextType", "properties", "emojiPickerUpsellViewed", "(Lcom/discord/utilities/analytics/AnalyticsTracker$PremiumUpsellType;Lcom/discord/widgets/chat/input/emoji/EmojiPickerContextType;Ljava/util/Map;)V", "Landroid/content/Context;", "context", "init", "(Landroid/content/Context;)V", "authToken", "handleAuthToken", "(Ljava/lang/String;)V", "fingerprint", "handleFingerprint", "handlePreLogout", "connected", "handleConnected", "(Z)V", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "videoInputDevice", "handleVideoInputDeviceSelected", "(Lco/discord/media_engine/VideoInputDeviceDescription;)V", "handleIsScreenSharingChanged", "videoLayout", "Lcom/discord/primitives/UserId;", "meId", "trackVideoLayoutToggled", "(Ljava/lang/String;JLcom/discord/api/channel/Channel;)V", "", "speakingUsers", "handleUserSpeaking", "(Ljava/util/Set;)V", "Ljava/lang/Class;", "Lcom/discord/app/AppComponent;", "screen", "appUiViewed", "(Ljava/lang/Class;)V", "ackMessage", "(J)V", "Landroid/content/Intent;", "intent", "Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;", "metadata", "deepLinkReceived", "(Landroid/content/Intent;Lcom/discord/utilities/intent/RouteHandlers$AnalyticsMetadata;)V", "Lcom/discord/models/domain/ModelInvite;", "invite", "Lcom/discord/api/message/Message;", "message", ModelAuditLogEntry.CHANGE_KEY_LOCATION, "inviteSent", "(Lcom/discord/models/domain/ModelInvite;Lcom/discord/api/message/Message;Ljava/lang/String;)V", "Lcom/discord/widgets/guilds/invite/GuildInvite;", "(Lcom/discord/widgets/guilds/invite/GuildInvite;Lcom/discord/api/channel/Channel;Lcom/discord/api/message/Message;Ljava/lang/String;)V", "isActive", "onOverlayVoiceEvent", "pane", "onGuildSettingsPaneViewed", "(Ljava/lang/String;J)V", "locationSection", "onUserSettingsPaneViewed", "(Ljava/lang/String;Ljava/lang/String;)V", "Lcom/discord/models/domain/ModelNotificationSettings;", "notifSettings", "onNotificationSettingsUpdated", "(Lcom/discord/models/domain/ModelNotificationSettings;Ljava/lang/Long;)V", "parentChannelId", "flags", "oldFlags", "onThreadNotificationSettingsUpdated", "(JJII)V", "giftCode", "trackOpenGiftAcceptModal", "(Ljava/lang/String;Ljava/lang/String;J)V", "trackGuildViewed", "Lcom/discord/stores/ChannelAnalyticsViewType;", "channelView", "Lcom/discord/stores/SelectedChannelAnalyticsLocation;", "analyticsLocation", "trackChannelOpened", "(JLcom/discord/stores/ChannelAnalyticsViewType;Lcom/discord/stores/SelectedChannelAnalyticsLocation;)V", "trackShowCallFeedbackSheet", "Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;", "pendingCallFeedback", "trackCallReportProblem", "(Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;)V", "Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;", "pendingStreamFeedback", "trackStreamReportProblem", "(Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;)V", "trackVoiceConnectionSuccess", "(Ljava/util/Map;)V", "trackVoiceConnectionFailure", "trackVoiceDisconnect", "trackMediaSessionJoined", "trackVideoStreamEnded", "trackGuildProfileOpened", "Lcom/discord/utilities/analytics/SearchType;", "searchType", "Lcom/discord/utilities/analytics/Traits$Location;", "locationTrait", "throttle", "trackSearchStarted", "(Lcom/discord/utilities/analytics/SearchType;Lcom/discord/utilities/analytics/Traits$Location;Z)V", "trackSearchResultsEmpty", "totalResultsCount", "lockedResultsCount", "trackSearchResultViewed", "(Lcom/discord/utilities/analytics/SearchType;ILjava/lang/Integer;Lcom/discord/utilities/analytics/Traits$Location;Z)V", "Lcom/discord/utilities/analytics/Traits$Source;", "sourceTrait", "trackSearchResultSelected", "(Lcom/discord/utilities/analytics/SearchType;ILcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$Source;)V", "trackChatInputComponentViewed", "Lcom/discord/utilities/rest/FileUploadAlertType;", "alertType", "numAttachments", "maxAttachmentSize", "totalAttachmentSize", "hasImage", "hasVideo", "isPremium", "trackFileUploadAlertViewed", "(Lcom/discord/utilities/rest/FileUploadAlertType;IIIZZZ)V", "Lcom/discord/stores/FailedMessageResolutionType;", "resolutionType", "Lcom/discord/primitives/Timestamp;", "initialAttemptTimestamp", "numRetries", "trackFailedMessageResolved", "(IIIZZLcom/discord/stores/FailedMessageResolutionType;JIJ)V", "trackApplicationCommandBrowserOpened", "applicationId", "commandId", "trackApplicationCommandSelected", "(JJJ)V", "trackApplicationCommandBrowserScrolled", "targetApplicationId", "trackApplicationCommandBrowserJump", "argumentType", "isRequired", "trackApplicationCommandValidationFailure", "(JJLjava/lang/String;Z)V", "", "channelSuggestions", "Lcom/discord/models/user/User;", "userSuggestions", "inviteSuggestionOpened", "(JLjava/util/List;Ljava/util/List;)V", "tabType", "trackThreadBrowserTabChanged", "(JLjava/lang/String;)V", "appLandingViewed", "openUnicodeEmojiPopout", "(Lcom/discord/api/channel/Channel;)V", "Lcom/discord/primitives/EmojiId;", "emojiId", "joinedSourceGuild", "sourceGuildPrivate", "openCustomEmojiPopout", "(Lcom/discord/api/channel/Channel;JZZZ)V", "emojiPickerUpsellHeaderViewed", "(Lcom/discord/widgets/chat/input/emoji/EmojiPickerContextType;)V", "isAnimated", "emojiPickerUpsellLockedItemClicked", "(Lcom/discord/widgets/chat/input/emoji/EmojiPickerContextType;Z)V", "hasPremiumStreamResolution", "hasPremiumStreamFps", "streamQualityIndicatorViewed", "(ZLjava/lang/Boolean;)V", "emojiAutocompleteUpsellInlineViewed", "emojiAutocompleteUpsellModalViewed", "autocompleteType", "numEmojiResults", "numStickerResults", "trackAutocompleteOpen", "(JLjava/lang/String;II)V", "selectionType", "selection", "Lcom/discord/primitives/StickerId;", "stickerId", "trackAutocompleteSelect", "(JLjava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V", "Lcom/discord/rtcconnection/audio/DiscordAudioManager$DeviceTypes;", "fromAudioOutputMode", "toAudioOutputMode", "trackVoiceAudioOutputModeSelected", "(JLcom/discord/rtcconnection/audio/DiscordAudioManager$DeviceTypes;Lcom/discord/rtcconnection/audio/DiscordAudioManager$DeviceTypes;)V", "getEmojiPickerUpsellLocation", "()Lcom/discord/utilities/analytics/Traits$Location;", "emojiPickerUpsellLocation", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "screenViewedSubject", "Lrx/subjects/PublishSubject;", "getSnapshotAnalyticsProperties", "()Ljava/util/Map;", "snapshotAnalyticsProperties", "inputMode", "Ljava/lang/String;", "selectedVoiceChannel", "Lcom/discord/api/channel/Channel;", "analyticsToken", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "Lcom/discord/stores/StoreStream;", "stores", "Lcom/discord/stores/StoreStream;", "Ljava/util/concurrent/atomic/AtomicBoolean;", "hasTrackedAppUiShown", "Ljava/util/concurrent/atomic/AtomicBoolean;", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;)V", "ScreenViewed", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAnalytics extends Store {
    private String analyticsToken;
    private String authToken;
    private final Clock clock;
    private final Dispatcher dispatcher;
    private String fingerprint;
    private String inputMode;
    private Channel selectedVoiceChannel;
    private final StoreStream stores;
    private AtomicBoolean hasTrackedAppUiShown = new AtomicBoolean(false);
    private final PublishSubject<ScreenViewed> screenViewedSubject = PublishSubject.k0();

    /* compiled from: StoreAnalytics.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0082\b\u0018\u00002\u00020\u0001B#\u0012\u000e\u0010\n\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00030\u0002\u0012\n\u0010\u000b\u001a\u00060\u0006j\u0002`\u0007¢\u0006\u0004\b\u001c\u0010\u001dJ\u0018\u0010\u0004\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\b\u001a\u00060\u0006j\u0002`\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ0\u0010\f\u001a\u00020\u00002\u0010\b\u0002\u0010\n\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00030\u00022\f\b\u0002\u0010\u000b\u001a\u00060\u0006j\u0002`\u0007HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R!\u0010\n\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0018\u001a\u0004\b\u0019\u0010\u0005R\u001d\u0010\u000b\u001a\u00060\u0006j\u0002`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001a\u001a\u0004\b\u001b\u0010\t¨\u0006\u001e"}, d2 = {"Lcom/discord/stores/StoreAnalytics$ScreenViewed;", "", "Ljava/lang/Class;", "Lcom/discord/app/AppComponent;", "component1", "()Ljava/lang/Class;", "", "Lcom/discord/primitives/Timestamp;", "component2", "()J", "screen", "timestamp", "copy", "(Ljava/lang/Class;J)Lcom/discord/stores/StoreAnalytics$ScreenViewed;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Class;", "getScreen", "J", "getTimestamp", HookHelper.constructorName, "(Ljava/lang/Class;J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ScreenViewed {
        private final Class<? extends AppComponent> screen;
        private final long timestamp;

        public ScreenViewed(Class<? extends AppComponent> cls, long j) {
            m.checkNotNullParameter(cls, "screen");
            this.screen = cls;
            this.timestamp = j;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ ScreenViewed copy$default(ScreenViewed screenViewed, Class cls, long j, int i, Object obj) {
            if ((i & 1) != 0) {
                cls = screenViewed.screen;
            }
            if ((i & 2) != 0) {
                j = screenViewed.timestamp;
            }
            return screenViewed.copy(cls, j);
        }

        public final Class<? extends AppComponent> component1() {
            return this.screen;
        }

        public final long component2() {
            return this.timestamp;
        }

        public final ScreenViewed copy(Class<? extends AppComponent> cls, long j) {
            m.checkNotNullParameter(cls, "screen");
            return new ScreenViewed(cls, j);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ScreenViewed)) {
                return false;
            }
            ScreenViewed screenViewed = (ScreenViewed) obj;
            return m.areEqual(this.screen, screenViewed.screen) && this.timestamp == screenViewed.timestamp;
        }

        public final Class<? extends AppComponent> getScreen() {
            return this.screen;
        }

        public final long getTimestamp() {
            return this.timestamp;
        }

        public int hashCode() {
            Class<? extends AppComponent> cls = this.screen;
            return b.a(this.timestamp) + ((cls != null ? cls.hashCode() : 0) * 31);
        }

        public String toString() {
            StringBuilder R = a.R("ScreenViewed(screen=");
            R.append(this.screen);
            R.append(", timestamp=");
            return a.B(R, this.timestamp, ")");
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            FeedbackIssue.values();
            int[] iArr = new int[24];
            $EnumSwitchMapping$0 = iArr;
            iArr[FeedbackIssue.STREAM_REPORT_ENDED_BLACK.ordinal()] = 1;
            iArr[FeedbackIssue.STREAM_REPORT_ENDED_BLURRY.ordinal()] = 2;
            iArr[FeedbackIssue.STREAM_REPORT_ENDED_LAGGING.ordinal()] = 3;
            iArr[FeedbackIssue.STREAM_REPORT_ENDED_OUT_OF_SYNC.ordinal()] = 4;
            iArr[FeedbackIssue.STREAM_REPORT_ENDED_AUDIO_MISSING.ordinal()] = 5;
            iArr[FeedbackIssue.STREAM_REPORT_ENDED_AUDIO_POOR.ordinal()] = 6;
            iArr[FeedbackIssue.STREAM_REPORT_ENDED_STREAM_STOPPED_UNEXPECTEDLY.ordinal()] = 7;
            iArr[FeedbackIssue.OTHER.ordinal()] = 8;
        }
    }

    public StoreAnalytics(StoreStream storeStream, Dispatcher dispatcher, Clock clock) {
        m.checkNotNullParameter(storeStream, "stores");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(clock, "clock");
        this.stores = storeStream;
        this.dispatcher = dispatcher;
        this.clock = clock;
    }

    private final void emojiPickerUpsellViewed(AnalyticsTracker.PremiumUpsellType premiumUpsellType, EmojiPickerContextType emojiPickerContextType, Map<String, ? extends Object> map) {
        String str;
        if (m.areEqual(emojiPickerContextType, EmojiPickerContextType.Global.INSTANCE)) {
            str = "Custom Status Modal";
        } else if (m.areEqual(emojiPickerContextType, EmojiPickerContextType.Chat.INSTANCE)) {
            Channel channel = this.stores.getChannels$app_productionGoogleRelease().getChannel(this.stores.getChannelsSelected$app_productionGoogleRelease().getId());
            str = (channel == null || channel.f() != 0) ? Traits.Location.Page.GUILD_CHANNEL : "DM Channel";
        } else {
            str = "";
        }
        AnalyticsTracker.premiumUpsellViewed$default(AnalyticsTracker.INSTANCE, premiumUpsellType, new Traits.Location((String) KotlinExtensionsKt.getExhaustive(str), Traits.Location.Section.EMOJI_PICKER_POPOUT, Traits.Location.Obj.BUTTON_UPSELL, Traits.Location.ObjType.SEARCH, null, 16, null), h0.plus(map, o.to("has_search_query", Boolean.TRUE)), null, 8, null);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void emojiPickerUpsellViewed$default(StoreAnalytics storeAnalytics, AnalyticsTracker.PremiumUpsellType premiumUpsellType, EmojiPickerContextType emojiPickerContextType, Map map, int i, Object obj) {
        if ((i & 4) != 0) {
            map = h0.emptyMap();
        }
        storeAnalytics.emojiPickerUpsellViewed(premiumUpsellType, emojiPickerContextType, map);
    }

    private final Map<String, Object> getChannelSnapshotAnalyticsProperties(long j, boolean z2) {
        Channel channel = this.stores.getChannels$app_productionGoogleRelease().getChannel(j);
        Map<String, Object> channelSnapshotAnalyticsProperties = channel != null ? getChannelSnapshotAnalyticsProperties(channel, z2) : null;
        return channelSnapshotAnalyticsProperties != null ? channelSnapshotAnalyticsProperties : h0.emptyMap();
    }

    public static /* synthetic */ Map getChannelSnapshotAnalyticsProperties$default(StoreAnalytics storeAnalytics, long j, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        return storeAnalytics.getChannelSnapshotAnalyticsProperties(j, z2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final Map<String, Object> getGuildAnalyticsPropertiesInternal(long j) {
        Collection<Channel> collection;
        GuildMember guildMember;
        List<Long> roles;
        Guild guild = this.stores.getGuilds$app_productionGoogleRelease().getGuildsInternal$app_productionGoogleRelease().get(Long.valueOf(j));
        if (guild == null) {
            return new HashMap();
        }
        int approximateMemberCount = this.stores.getGuildMemberCounts$app_productionGoogleRelease().getApproximateMemberCount(j);
        Map<Long, Channel> channelsForGuildInternal$app_productionGoogleRelease = this.stores.getChannels$app_productionGoogleRelease().getChannelsForGuildInternal$app_productionGoogleRelease(j);
        if (channelsForGuildInternal$app_productionGoogleRelease == null || (collection = channelsForGuildInternal$app_productionGoogleRelease.values()) == null) {
            collection = n.emptyList();
        }
        Collection<Channel> collection2 = collection;
        Map<Long, GuildRole> map = this.stores.getGuilds$app_productionGoogleRelease().getGuildRolesInternal$app_productionGoogleRelease().get(Long.valueOf(j));
        int size = map != null ? map.size() : 0;
        long id2 = this.stores.getUsers$app_productionGoogleRelease().getMeInternal$app_productionGoogleRelease().getId();
        Map<Long, GuildMember> map2 = this.stores.getGuilds$app_productionGoogleRelease().getGuildMembersComputedInternal$app_productionGoogleRelease().get(Long.valueOf(j));
        int size2 = (map2 == null || (guildMember = map2.get(Long.valueOf(id2))) == null || (roles = guildMember.getRoles()) == null) ? 0 : roles.size();
        Long l = this.stores.getPermissions$app_productionGoogleRelease().getGuildPermissions().get(Long.valueOf(j));
        return guildPropertiesMap(guild.getId(), approximateMemberCount, collection2, size, size2, l != null ? l.longValue() : 0L, guild.getFeatures().contains(GuildFeature.VIP_REGIONS));
    }

    private final Map<String, Object> getGuildAndChannelSnapshotAnalyticsProperties(long j) {
        Channel channel = this.stores.getChannels$app_productionGoogleRelease().getChannel(j);
        Map<String, Object> map = null;
        if (channel != null) {
            map = h0.plus(getGuildSnapshotAnalyticsProperties(channel.f()), getChannelSnapshotAnalyticsProperties$default(this, channel, false, 2, (Object) null));
        }
        return map != null ? map : h0.emptyMap();
    }

    private final Map<String, Object> getGuildSnapshotAnalyticsProperties(long j) {
        Guild guild = this.stores.getGuilds$app_productionGoogleRelease().getGuilds().get(Long.valueOf(j));
        Map<String, Object> guildSnapshotAnalyticsProperties = guild != null ? getGuildSnapshotAnalyticsProperties(guild) : null;
        return guildSnapshotAnalyticsProperties != null ? guildSnapshotAnalyticsProperties : h0.emptyMap();
    }

    private final Map<String, Object> getSnapshotAnalyticsProperties() {
        return getGuildAndChannelSnapshotAnalyticsProperties(this.stores.getChannelsSelected$app_productionGoogleRelease().getId());
    }

    private final String getStreamFeedbackReasonFromIssue(FeedbackIssue feedbackIssue) {
        if (feedbackIssue != null) {
            int ordinal = feedbackIssue.ordinal();
            if (ordinal == 0) {
                return "OTHER";
            }
            switch (ordinal) {
                case 10:
                    return "BLACK_SCREEN";
                case 11:
                    return "BLURRY";
                case 12:
                    return "LAGGING";
                case 13:
                    return "OUT_OF_SYNC";
                case 14:
                    return "AUDIO_MISSING";
                case 15:
                    return "AUDIO_POOR";
                case 16:
                    return "STREAM_STOPPED_UNEXPECTEDLY";
            }
        }
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Map<String, Object> getThreadSnapshotAnalyticsProperties(Channel channel) {
        Integer threadMessageCount = this.stores.getThreadMessages$app_productionGoogleRelease().getThreadMessageCount(channel.h());
        boolean z2 = false;
        int intValue = threadMessageCount != null ? threadMessageCount.intValue() : 0;
        Pair[] pairArr = new Pair[9];
        pairArr[0] = o.to(ModelAuditLogEntry.CHANGE_KEY_CHANNEL_ID, Long.valueOf(channel.h()));
        pairArr[1] = o.to(ModelAuditLogEntry.CHANGE_KEY_GUILD_ID, Long.valueOf(channel.f()));
        pairArr[2] = o.to("parent_id", Long.valueOf(channel.r()));
        pairArr[3] = o.to("channel_type", Integer.valueOf(channel.A()));
        pairArr[4] = o.to("thread_approximate_message_count", Integer.valueOf(f.coerceAtMost(intValue, 50)));
        ThreadMetadata y2 = channel.y();
        pairArr[5] = o.to("thread_archived", Boolean.valueOf(y2 != null && y2.b()));
        ThreadMetadata y3 = channel.y();
        if (y3 != null && y3.d()) {
            z2 = true;
        }
        pairArr[6] = o.to("thread_locked", Boolean.valueOf(z2));
        ThreadMetadata y4 = channel.y();
        pairArr[7] = o.to("thread_auto_archive_duration_minutes", y4 != null ? Integer.valueOf(y4.c()) : null);
        pairArr[8] = o.to("thread_approximate_creation_date", Long.valueOf((channel.h() >>> 22) + SnowflakeUtils.DISCORD_EPOCH));
        return h0.mapOf(pairArr);
    }

    private final Map<String, Object> guildPropertiesMap(long j, int i, Collection<Channel> collection, int i2, int i3, long j2, boolean z2) {
        int i4;
        Pair[] pairArr = new Pair[9];
        int i5 = 0;
        pairArr[0] = o.to(ModelAuditLogEntry.CHANGE_KEY_GUILD_ID, Long.valueOf(j));
        pairArr[1] = o.to("guild_size_total", Integer.valueOf(i));
        pairArr[2] = o.to("guild_num_channels", Integer.valueOf(collection.size()));
        if (collection.isEmpty()) {
            i4 = 0;
        } else {
            i4 = 0;
            for (Channel channel : collection) {
                if (ChannelUtils.s(channel) && (i4 = i4 + 1) < 0) {
                    n.throwCountOverflow();
                }
            }
        }
        pairArr[3] = o.to("guild_num_text_channels", Integer.valueOf(i4));
        if (!collection.isEmpty()) {
            for (Channel channel2 : collection) {
                if (ChannelUtils.t(channel2) && (i5 = i5 + 1) < 0) {
                    n.throwCountOverflow();
                }
            }
        }
        pairArr[4] = o.to("guild_num_voice_channels", Integer.valueOf(i5));
        pairArr[5] = o.to("guild_num_roles", Integer.valueOf(i2));
        pairArr[6] = o.to("guild_member_num_roles", Integer.valueOf(i3));
        pairArr[7] = o.to("guild_member_perms", Long.valueOf(j2));
        pairArr[8] = o.to("guild_is_vip", Boolean.valueOf(z2));
        return h0.mutableMapOf(pairArr);
    }

    @StoreThread
    private final void handleVideoInputUpdate(VideoInputDeviceDescription videoInputDeviceDescription, boolean z2) {
        Channel channel = this.selectedVoiceChannel;
        if (channel != null) {
            Map<Long, VoiceState> map = this.stores.getVoiceStates$app_productionGoogleRelease().get().get(Long.valueOf(channel.f()));
            if (map == null) {
                map = h0.emptyMap();
            }
            Map<Long, VoiceState> map2 = map;
            long id2 = this.stores.getUsers$app_productionGoogleRelease().getMeInternal$app_productionGoogleRelease().getId();
            AnalyticsTracker analyticsTracker = AnalyticsTracker.INSTANCE;
            RtcConnection.Metadata rtcConnectionMetadata = this.stores.getRtcConnection$app_productionGoogleRelease().getRtcConnectionMetadata();
            analyticsTracker.videoInputsUpdate(id2, channel, map2, videoInputDeviceDescription, z2, rtcConnectionMetadata != null ? rtcConnectionMetadata.f2753b : null);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onScreenViewed(ScreenViewed screenViewed) {
        if (!this.hasTrackedAppUiShown.getAndSet(true)) {
            this.dispatcher.schedule(new StoreAnalytics$onScreenViewed$1(this, screenViewed));
        }
    }

    public static /* synthetic */ void onUserSettingsPaneViewed$default(StoreAnalytics storeAnalytics, String str, String str2, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = null;
        }
        storeAnalytics.onUserSettingsPaneViewed(str, str2);
    }

    public static /* synthetic */ void trackSearchResultSelected$default(StoreAnalytics storeAnalytics, SearchType searchType, int i, Traits.Location location, Traits.Source source, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            location = null;
        }
        if ((i2 & 8) != 0) {
            source = null;
        }
        storeAnalytics.trackSearchResultSelected(searchType, i, location, source);
    }

    public static /* synthetic */ void trackSearchResultsEmpty$default(StoreAnalytics storeAnalytics, SearchType searchType, Traits.Location location, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            location = null;
        }
        if ((i & 4) != 0) {
            z2 = false;
        }
        storeAnalytics.trackSearchResultsEmpty(searchType, location, z2);
    }

    public static /* synthetic */ void trackSearchStarted$default(StoreAnalytics storeAnalytics, SearchType searchType, Traits.Location location, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            location = null;
        }
        if ((i & 4) != 0) {
            z2 = false;
        }
        storeAnalytics.trackSearchStarted(searchType, location, z2);
    }

    /* JADX WARN: Removed duplicated region for block: B:20:0x0028  */
    /* JADX WARN: Removed duplicated region for block: B:31:0x003e  */
    /* JADX WARN: Removed duplicated region for block: B:32:0x0048  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void updateTrackingData() {
        /*
            r5 = this;
            java.lang.String r0 = r5.authToken
            r1 = 0
            r2 = 1
            if (r0 == 0) goto Lf
            int r0 = r0.length()
            if (r0 != 0) goto Ld
            goto Lf
        Ld:
            r0 = 0
            goto L10
        Lf:
            r0 = 1
        L10:
            r0 = r0 ^ r2
            if (r0 != 0) goto L25
            java.lang.String r3 = r5.fingerprint
            if (r3 == 0) goto L20
            int r3 = r3.length()
            if (r3 != 0) goto L1e
            goto L20
        L1e:
            r3 = 0
            goto L21
        L20:
            r3 = 1
        L21:
            if (r3 != 0) goto L25
            r3 = 1
            goto L26
        L25:
            r3 = 0
        L26:
            if (r0 == 0) goto L3a
            java.lang.String r0 = r5.analyticsToken
            if (r0 == 0) goto L35
            int r0 = r0.length()
            if (r0 != 0) goto L33
            goto L35
        L33:
            r0 = 0
            goto L36
        L35:
            r0 = 1
        L36:
            if (r0 != 0) goto L3a
            r0 = 1
            goto L3b
        L3a:
            r0 = 0
        L3b:
            r4 = 0
            if (r3 == 0) goto L48
            com.discord.utilities.analytics.AnalyticsTracker r0 = com.discord.utilities.analytics.AnalyticsTracker.INSTANCE
            com.discord.utilities.analytics.AnalyticsUtils$Tracker r0 = r0.getTracker()
            r0.setTrackingData(r4, r2)
            goto L60
        L48:
            if (r0 == 0) goto L57
            com.discord.utilities.analytics.AnalyticsTracker r0 = com.discord.utilities.analytics.AnalyticsTracker.INSTANCE
            com.discord.utilities.analytics.AnalyticsUtils$Tracker r0 = r0.getTracker()
            java.lang.String r2 = r5.analyticsToken
            r3 = 2
            com.discord.utilities.analytics.AnalyticsUtils.Tracker.setTrackingData$default(r0, r2, r1, r3, r4)
            goto L60
        L57:
            com.discord.utilities.analytics.AnalyticsTracker r0 = com.discord.utilities.analytics.AnalyticsTracker.INSTANCE
            com.discord.utilities.analytics.AnalyticsUtils$Tracker r0 = r0.getTracker()
            r0.setTrackingData(r4, r1)
        L60:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreAnalytics.updateTrackingData():void");
    }

    public final void ackMessage(long j) {
        this.dispatcher.schedule(new StoreAnalytics$ackMessage$1(this, j));
    }

    public final void appLandingViewed() {
        AnalyticsTracker.appLandingViewed(getPrefsSessionDurable().getLong("CACHE_KEY_LOGOUT_TS", 0L));
    }

    public final void appUiViewed(Class<? extends AppComponent> cls) {
        m.checkNotNullParameter(cls, "screen");
        PublishSubject<ScreenViewed> publishSubject = this.screenViewedSubject;
        publishSubject.k.onNext(new ScreenViewed(cls, this.clock.currentTimeMillis()));
    }

    public final void deepLinkReceived(Intent intent, RouteHandlers.AnalyticsMetadata analyticsMetadata) {
        m.checkNotNullParameter(intent, "intent");
        m.checkNotNullParameter(analyticsMetadata, "metadata");
        Long channelId = analyticsMetadata.getChannelId();
        Map<String, ? extends Object> guildAndChannelSnapshotAnalyticsProperties = channelId != null ? getGuildAndChannelSnapshotAnalyticsProperties(channelId.longValue()) : null;
        if (guildAndChannelSnapshotAnalyticsProperties == null) {
            guildAndChannelSnapshotAnalyticsProperties = h0.emptyMap();
        }
        AnalyticsTracker.INSTANCE.deepLinkReceived(intent, analyticsMetadata, guildAndChannelSnapshotAnalyticsProperties);
    }

    public final void emojiAutocompleteUpsellInlineViewed() {
        AnalyticsTracker.premiumUpsellViewed$default(AnalyticsTracker.INSTANCE, AnalyticsTracker.PremiumUpsellType.EmojiAutocompleteInline, null, h0.emptyMap(), null, 8, null);
    }

    public final void emojiAutocompleteUpsellModalViewed() {
        AnalyticsTracker.premiumUpsellViewed$default(AnalyticsTracker.INSTANCE, AnalyticsTracker.PremiumUpsellType.EmojiAutocompleteModal, null, h0.emptyMap(), null, 8, null);
    }

    public final void emojiPickerUpsellHeaderViewed(EmojiPickerContextType emojiPickerContextType) {
        m.checkNotNullParameter(emojiPickerContextType, "emojiPickerContextType");
        emojiPickerUpsellViewed$default(this, AnalyticsTracker.PremiumUpsellType.EmojiPickerHeaderViewed, emojiPickerContextType, null, 4, null);
    }

    public final void emojiPickerUpsellLockedItemClicked(EmojiPickerContextType emojiPickerContextType, boolean z2) {
        m.checkNotNullParameter(emojiPickerContextType, "emojiPickerContextType");
        emojiPickerUpsellViewed(AnalyticsTracker.PremiumUpsellType.EmojiPickerLockedItemClicked, emojiPickerContextType, h0.mapOf(o.to("is_animated", Boolean.valueOf(z2)), o.to("is_external", Boolean.TRUE)));
    }

    public final Traits.Location getEmojiPickerUpsellLocation() {
        Channel channel = this.stores.getChannels$app_productionGoogleRelease().getChannel(this.stores.getChannelsSelected$app_productionGoogleRelease().getId());
        return new Traits.Location((channel == null || channel.f() != 0) ? Traits.Location.Page.GUILD_CHANNEL : "DM Channel", Traits.Location.Section.EMOJI_PICKER_POPOUT, null, null, null, 28, null);
    }

    @StoreThread
    public final void handleAuthToken(String str) {
        this.authToken = str;
        updateTrackingData();
    }

    @StoreThread
    public final void handleConnected(boolean z2) {
        if (!z2) {
            this.analyticsToken = null;
            updateTrackingData();
        }
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        this.analyticsToken = modelPayload.getAnalyticsToken();
        updateTrackingData();
    }

    @StoreThread
    public final void handleFingerprint(String str) {
        this.fingerprint = str;
        updateTrackingData();
    }

    @StoreThread
    public final void handleIsScreenSharingChanged(boolean z2) {
        handleVideoInputUpdate(this.stores.getMediaEngine$app_productionGoogleRelease().getSelectedVideoInputDeviceBlocking(), z2);
    }

    @StoreThread
    public final void handlePreLogout() {
        SharedPreferences.Editor edit = getPrefsSessionDurable().edit();
        m.checkNotNullExpressionValue(edit, "editor");
        edit.putLong("CACHE_KEY_LOGOUT_TS", this.clock.currentTimeMillis());
        edit.apply();
    }

    @StoreThread
    public final void handleUserSpeaking(Set<Long> set) {
        Channel channel;
        m.checkNotNullParameter(set, "speakingUsers");
        String str = this.inputMode;
        if (str != null && (channel = this.selectedVoiceChannel) != null) {
            long id2 = this.stores.getUsers$app_productionGoogleRelease().getMeInternal$app_productionGoogleRelease().getId();
            AnalyticsTracker analyticsTracker = AnalyticsTracker.INSTANCE;
            analyticsTracker.userSpeaking(id2, set, str, channel);
            analyticsTracker.userListening(id2, set, str, channel);
        }
    }

    @StoreThread
    public final void handleVideoInputDeviceSelected(VideoInputDeviceDescription videoInputDeviceDescription) {
        handleVideoInputUpdate(videoInputDeviceDescription, this.stores.getApplicationStreaming$app_productionGoogleRelease().isScreenSharing());
    }

    @Override // com.discord.stores.Store
    public void init(Context context) {
        m.checkNotNullParameter(context, "context");
        super.init(context);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.computationBuffered(this.stores.getMediaSettings$app_productionGoogleRelease().getInputMode()), StoreAnalytics.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreAnalytics$init$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.computationBuffered(this.stores.getVoiceChannelSelected$app_productionGoogleRelease().observeSelectedChannel()), StoreAnalytics.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreAnalytics$init$2(this));
        Observable y2 = this.screenViewedSubject.Y(StoreAnalytics$init$3.INSTANCE).y();
        m.checkNotNullExpressionValue(y2, "screenViewedSubject\n    …       }\n        .first()");
        ObservableExtensionsKt.appSubscribe(y2, StoreAnalytics.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreAnalytics$init$4(this));
    }

    public final void inviteSent(ModelInvite modelInvite, Message message, String str) {
        GuildScheduledEvent guildScheduledEvent;
        Channel channel;
        com.discord.api.guild.Guild guild;
        m.checkNotNullParameter(message, "message");
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        Pair[] pairArr = new Pair[5];
        pairArr[0] = o.to(ModelAuditLogEntry.CHANGE_KEY_LOCATION, str);
        pairArr[1] = o.to("message_id", Long.valueOf(message.o()));
        Long l = null;
        pairArr[2] = o.to("invite_guild_id", (modelInvite == null || (guild = modelInvite.guild) == null) ? null : Long.valueOf(guild.r()));
        pairArr[3] = o.to("invite_channel_id", (modelInvite == null || (channel = modelInvite.getChannel()) == null) ? null : Long.valueOf(channel.h()));
        if (!(modelInvite == null || (guildScheduledEvent = modelInvite.getGuildScheduledEvent()) == null)) {
            l = Long.valueOf(guildScheduledEvent.i());
        }
        pairArr[4] = o.to("invite_guild_scheduled_event_id", l);
        AnalyticsTracker.INSTANCE.inviteSent(modelInvite, h0.plus(h0.mapOf(pairArr), getGuildAndChannelSnapshotAnalyticsProperties(message.g())));
    }

    public final void inviteSuggestionOpened(long j, List<Channel> list, List<? extends User> list2) {
        m.checkNotNullParameter(list, "channelSuggestions");
        m.checkNotNullParameter(list2, "userSuggestions");
        AnalyticsTracker.INSTANCE.inviteSuggestionOpened(j, list, list2);
    }

    public final void onGuildSettingsPaneViewed(String str, long j) {
        m.checkNotNullParameter(str, "pane");
        this.dispatcher.schedule(new StoreAnalytics$onGuildSettingsPaneViewed$1(this, j, str));
    }

    public final void onNotificationSettingsUpdated(ModelNotificationSettings modelNotificationSettings, Long l) {
        m.checkNotNullParameter(modelNotificationSettings, "notifSettings");
        this.dispatcher.schedule(new StoreAnalytics$onNotificationSettingsUpdated$1(this, l, modelNotificationSettings));
    }

    public final void onOverlayVoiceEvent(boolean z2) {
        this.dispatcher.schedule(new StoreAnalytics$onOverlayVoiceEvent$1(this, z2));
    }

    public final void onThreadNotificationSettingsUpdated(long j, long j2, int i, int i2) {
        this.dispatcher.schedule(new StoreAnalytics$onThreadNotificationSettingsUpdated$1(this, j, j2, i2, i));
    }

    public final void onUserSettingsPaneViewed(String str, String str2) {
        m.checkNotNullParameter(str, "pane");
        AnalyticsTracker.INSTANCE.settingsPaneViewed("user", str, str2 != null ? g0.mapOf(o.to("location_section", str2)) : null);
    }

    public final void openCustomEmojiPopout(Channel channel, long j, boolean z2, boolean z3, boolean z4) {
        m.checkNotNullParameter(channel, "channel");
        AnalyticsTracker.INSTANCE.openCustomEmojiPopout(channel.f(), j, z2, z3, z4, CollectionExtensionsKt.filterNonNullValues(getChannelSnapshotAnalyticsProperties$default(this, channel, false, 2, (Object) null)));
    }

    public final void openUnicodeEmojiPopout(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        AnalyticsTracker.INSTANCE.openUnicodeEmojiPopout(channel.f(), CollectionExtensionsKt.filterNonNullValues(getChannelSnapshotAnalyticsProperties$default(this, channel, false, 2, (Object) null)));
    }

    public final void streamQualityIndicatorViewed(boolean z2, Boolean bool) {
        AnalyticsTracker.premiumUpsellViewed$default(AnalyticsTracker.INSTANCE, AnalyticsTracker.PremiumUpsellType.StreamQualityIndicator, null, CollectionExtensionsKt.filterNonNullValues(h0.mapOf(o.to("has_premium_stream_fps", bool), o.to("has_premium_stream_resolution", Boolean.valueOf(z2)))), null, 8, null);
    }

    public final void trackApplicationCommandBrowserJump(long j) {
        AnalyticsTracker.INSTANCE.applicationCommandBrowserJump(j);
    }

    public final void trackApplicationCommandBrowserOpened(long j) {
        AnalyticsTracker.INSTANCE.applicationCommandBrowserOpened(getGuildAndChannelSnapshotAnalyticsProperties(j));
    }

    public final void trackApplicationCommandBrowserScrolled() {
        AnalyticsTracker.INSTANCE.applicationCommandBrowserScrolled();
    }

    public final void trackApplicationCommandSelected(long j, long j2, long j3) {
        AnalyticsTracker.INSTANCE.applicationCommandSelected(j2, j3, getGuildAndChannelSnapshotAnalyticsProperties(j));
    }

    public final void trackApplicationCommandValidationFailure(long j, long j2, String str, boolean z2) {
        m.checkNotNullParameter(str, "argumentType");
        AnalyticsTracker.INSTANCE.applicationCommandValidationFailure(j, j2, str, z2);
    }

    public final void trackAutocompleteOpen(long j, String str, int i, int i2) {
        m.checkNotNullParameter(str, "autocompleteType");
        AnalyticsTracker.INSTANCE.autocompleteOpen(CollectionExtensionsKt.filterNonNullValues(getGuildAndChannelSnapshotAnalyticsProperties(j)), str, i, i2);
    }

    public final void trackAutocompleteSelect(long j, String str, int i, int i2, String str2, String str3, Long l) {
        m.checkNotNullParameter(str, "autocompleteType");
        AnalyticsTracker.INSTANCE.autocompleteSelect(CollectionExtensionsKt.filterNonNullValues(getGuildAndChannelSnapshotAnalyticsProperties(j)), str, i, i2, str2, str3, l);
    }

    public final void trackCallReportProblem(PendingFeedback.CallFeedback callFeedback) {
        m.checkNotNullParameter(callFeedback, "pendingCallFeedback");
        this.dispatcher.schedule(new StoreAnalytics$trackCallReportProblem$1(this, callFeedback));
    }

    @StoreThread
    public final void trackChannelOpened(long j, ChannelAnalyticsViewType channelAnalyticsViewType, SelectedChannelAnalyticsLocation selectedChannelAnalyticsLocation) {
        m.checkNotNullParameter(channelAnalyticsViewType, "channelView");
        Channel findChannelByIdInternal$app_productionGoogleRelease = this.stores.getChannels$app_productionGoogleRelease().findChannelByIdInternal$app_productionGoogleRelease(j);
        if (findChannelByIdInternal$app_productionGoogleRelease != null) {
            AnalyticsTracker.INSTANCE.channelOpened(j, new StoreAnalytics$trackChannelOpened$$inlined$let$lambda$1(findChannelByIdInternal$app_productionGoogleRelease, this, j, channelAnalyticsViewType, selectedChannelAnalyticsLocation));
        }
    }

    public final void trackChatInputComponentViewed(String str) {
        m.checkNotNullParameter(str, "type");
        AnalyticsTracker.INSTANCE.chatInputComponentViewed(str);
    }

    public final void trackFailedMessageResolved(int i, int i2, int i3, boolean z2, boolean z3, FailedMessageResolutionType failedMessageResolutionType, long j, int i4, long j2) {
        m.checkNotNullParameter(failedMessageResolutionType, "resolutionType");
        AnalyticsTracker.INSTANCE.failedMessageResolved(i, i2, i3, z2, z3, failedMessageResolutionType, j, i4, getGuildAndChannelSnapshotAnalyticsProperties(j2));
    }

    public final void trackFileUploadAlertViewed(FileUploadAlertType fileUploadAlertType, int i, int i2, int i3, boolean z2, boolean z3, boolean z4) {
        m.checkNotNullParameter(fileUploadAlertType, "alertType");
        AnalyticsTracker.INSTANCE.fileUploadAlertViewed(fileUploadAlertType, i, i2, i3, z2, z3, z4, getSnapshotAnalyticsProperties());
    }

    public final void trackGuildProfileOpened(long j) {
        AnalyticsTracker.INSTANCE.openGuildProfileSheet(j);
    }

    @StoreThread
    public final void trackGuildViewed(long j) {
        boolean z2;
        GuildMember guildMember;
        Guild guild = this.stores.getGuilds$app_productionGoogleRelease().getGuildsInternal$app_productionGoogleRelease().get(Long.valueOf(j));
        if (guild != null) {
            long id2 = this.stores.getUsers$app_productionGoogleRelease().getMeInternal$app_productionGoogleRelease().getId();
            Map<Long, GuildMember> map = this.stores.getGuilds$app_productionGoogleRelease().getGuildMembersComputedInternal$app_productionGoogleRelease().get(Long.valueOf(j));
            boolean pending = (map == null || (guildMember = map.get(Long.valueOf(id2))) == null) ? false : guildMember.getPending();
            try {
                z2 = guild.getFeatures().contains(GuildFeature.PREVIEW_ENABLED);
            } catch (Exception e) {
                AppLog.g.e("Guild is missing feature set", e, g0.mapOf(o.to(ModelAuditLogEntry.CHANGE_KEY_GUILD_ID, String.valueOf(guild.getId()))));
                z2 = false;
            }
            AnalyticsTracker.INSTANCE.guildViewed(j, new StoreAnalytics$trackGuildViewed$1(h0.plus(getGuildAnalyticsPropertiesInternal(j), h0.mapOf(o.to("is_pending", Boolean.valueOf(pending)), o.to("preview_enabled", Boolean.valueOf(z2))))));
        }
    }

    public final void trackMediaSessionJoined(Map<String, Object> map) {
        m.checkNotNullParameter(map, "properties");
        this.dispatcher.schedule(new StoreAnalytics$trackMediaSessionJoined$1(this, map));
    }

    public final void trackOpenGiftAcceptModal(String str, String str2, long j) {
        m.checkNotNullParameter(str, "giftCode");
        m.checkNotNullParameter(str2, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        this.dispatcher.schedule(new StoreAnalytics$trackOpenGiftAcceptModal$1(this, str, j, str2));
    }

    public final void trackSearchResultSelected(SearchType searchType, int i, Traits.Location location, Traits.Source source) {
        m.checkNotNullParameter(searchType, "searchType");
        AnalyticsTracker.INSTANCE.searchResultSelected(searchType, location, source, i, getSnapshotAnalyticsProperties());
    }

    public final void trackSearchResultViewed(SearchType searchType, int i, Integer num, Traits.Location location, boolean z2) {
        m.checkNotNullParameter(searchType, "searchType");
        AnalyticsTracker.INSTANCE.searchResultViewed(searchType, i, num, location, getSnapshotAnalyticsProperties(), z2);
    }

    public final void trackSearchResultsEmpty(SearchType searchType, Traits.Location location, boolean z2) {
        m.checkNotNullParameter(searchType, "searchType");
        AnalyticsTracker.INSTANCE.searchResultsEmpty(searchType, location, getSnapshotAnalyticsProperties(), z2);
    }

    public final void trackSearchStarted(SearchType searchType, Traits.Location location, boolean z2) {
        m.checkNotNullParameter(searchType, "searchType");
        AnalyticsTracker.INSTANCE.searchStart(searchType, location, getSnapshotAnalyticsProperties(), z2);
    }

    public final void trackShowCallFeedbackSheet(long j) {
        this.dispatcher.schedule(new StoreAnalytics$trackShowCallFeedbackSheet$1(this, j));
    }

    public final void trackStreamReportProblem(PendingFeedback.StreamFeedback streamFeedback) {
        m.checkNotNullParameter(streamFeedback, "pendingStreamFeedback");
        AnalyticsTracker.INSTANCE.reportStreamProblem(streamFeedback.getStream(), streamFeedback.getFeedbackRating(), getStreamFeedbackReasonFromIssue(streamFeedback.getIssue()), streamFeedback.getMediaSessionId(), streamFeedback.getIssueDetails());
    }

    public final void trackThreadBrowserTabChanged(long j, String str) {
        m.checkNotNullParameter(str, "tabType");
        AnalyticsTracker.INSTANCE.threadBrowserTabChanged(getGuildAndChannelSnapshotAnalyticsProperties(j), str);
    }

    public final void trackVideoLayoutToggled(String str, long j, Channel channel) {
        m.checkNotNullParameter(str, "videoLayout");
        AnalyticsTracker.INSTANCE.videoLayoutToggled(str, j, channel);
    }

    @StoreThread
    public final void trackVideoStreamEnded(Map<String, Object> map) {
        m.checkNotNullParameter(map, "properties");
        Object obj = map.get("sender_user_id");
        Long l = null;
        if (!(obj instanceof Long)) {
            obj = null;
        }
        Long l2 = (Long) obj;
        if (l2 != null) {
            long longValue = l2.longValue();
            Object obj2 = map.get(ModelAuditLogEntry.CHANGE_KEY_CHANNEL_ID);
            if (!(obj2 instanceof Long)) {
                obj2 = null;
            }
            Long l3 = (Long) obj2;
            if (l3 != null) {
                long longValue2 = l3.longValue();
                Object obj3 = map.get(ModelAuditLogEntry.CHANGE_KEY_GUILD_ID);
                if (obj3 instanceof Long) {
                    l = obj3;
                }
                Integer maxViewersForStream = this.stores.getApplicationStreaming$app_productionGoogleRelease().getMaxViewersForStream(longValue, longValue2, l);
                if (maxViewersForStream != null) {
                    map.put("max_viewers", maxViewersForStream);
                }
                AnalyticsTracker.INSTANCE.videoStreamEnded(map);
            }
        }
    }

    public final void trackVoiceAudioOutputModeSelected(long j, DiscordAudioManager.DeviceTypes deviceTypes, DiscordAudioManager.DeviceTypes deviceTypes2) {
        m.checkNotNullParameter(deviceTypes, "fromAudioOutputMode");
        m.checkNotNullParameter(deviceTypes2, "toAudioOutputMode");
        this.dispatcher.schedule(new StoreAnalytics$trackVoiceAudioOutputModeSelected$1(this, j, deviceTypes, deviceTypes2));
    }

    public final void trackVoiceConnectionFailure(Map<String, Object> map) {
        m.checkNotNullParameter(map, "properties");
        this.dispatcher.schedule(new StoreAnalytics$trackVoiceConnectionFailure$1(this, map));
    }

    public final void trackVoiceConnectionSuccess(Map<String, Object> map) {
        m.checkNotNullParameter(map, "properties");
        this.dispatcher.schedule(new StoreAnalytics$trackVoiceConnectionSuccess$1(this, map));
    }

    @StoreThread
    public final void trackVoiceDisconnect(Map<String, Object> map) {
        m.checkNotNullParameter(map, "properties");
        this.dispatcher.schedule(new StoreAnalytics$trackVoiceDisconnect$1(this, map));
    }

    public static /* synthetic */ Map getChannelSnapshotAnalyticsProperties$default(StoreAnalytics storeAnalytics, Channel channel, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        return storeAnalytics.getChannelSnapshotAnalyticsProperties(channel, z2);
    }

    private final Map<String, Object> getGuildSnapshotAnalyticsProperties(Guild guild) {
        GuildMember guildMember;
        List<Long> roles;
        int approximateMemberCount = this.stores.getGuildMemberCounts$app_productionGoogleRelease().getApproximateMemberCount(guild.getId());
        Collection<Channel> values = this.stores.getChannels$app_productionGoogleRelease().getChannelsForGuild(guild.getId()).values();
        Map map = (Map) a.d(guild, this.stores.getGuilds$app_productionGoogleRelease().getRoles());
        int size = map != null ? map.size() : 0;
        long id2 = this.stores.getUsers$app_productionGoogleRelease().getMe().getId();
        Map map2 = (Map) a.d(guild, this.stores.getGuilds$app_productionGoogleRelease().getMembers());
        int size2 = (map2 == null || (guildMember = (GuildMember) map2.get(Long.valueOf(id2))) == null || (roles = guildMember.getRoles()) == null) ? 0 : roles.size();
        Long l = (Long) a.d(guild, this.stores.getPermissions$app_productionGoogleRelease().getGuildPermissions());
        return guildPropertiesMap(guild.getId(), approximateMemberCount, values, size, size2, l != null ? l.longValue() : 0L, guild.getFeatures().contains(GuildFeature.VIP_REGIONS));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Map<String, Object> getChannelSnapshotAnalyticsProperties(Channel channel, boolean z2) {
        PermissionOverwrite permissionOverwrite;
        Object obj;
        boolean z3;
        Long l = (Long) a.c(channel, this.stores.getPermissions$app_productionGoogleRelease().getPermissionsByChannel());
        List<PermissionOverwrite> s2 = channel.s();
        Boolean bool = null;
        if (s2 != null) {
            Iterator<T> it = s2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (((PermissionOverwrite) obj).e() == channel.f()) {
                    z3 = true;
                    continue;
                } else {
                    z3 = false;
                    continue;
                }
                if (z3) {
                    break;
                }
            }
            permissionOverwrite = (PermissionOverwrite) obj;
        } else {
            permissionOverwrite = null;
        }
        if (permissionOverwrite != null) {
            bool = Boolean.valueOf(PermissionOverwriteUtilsKt.denies(permissionOverwrite, Permission.VIEW_CHANNEL));
        }
        return h0.plus(h0.plus(AnalyticsUtils.INSTANCE.getProperties$app_productionGoogleRelease(channel), h0.mapOf(o.to("channel_member_perms", l), o.to("channel_hidden", bool))), z2 ? g0.mapOf(o.to("channel_is_nsfw", Boolean.valueOf(channel.o()))) : h0.emptyMap());
    }

    public final void inviteSent(GuildInvite guildInvite, Channel channel, Message message, String str) {
        m.checkNotNullParameter(message, "message");
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        Pair[] pairArr = new Pair[5];
        pairArr[0] = o.to(ModelAuditLogEntry.CHANGE_KEY_LOCATION, str);
        pairArr[1] = o.to("message_id", Long.valueOf(message.o()));
        Long l = null;
        pairArr[2] = o.to("invite_guild_id", guildInvite != null ? guildInvite.getGuildId() : null);
        pairArr[3] = o.to("invite_channel_id", guildInvite != null ? guildInvite.getChannelId() : null);
        if (guildInvite != null) {
            l = guildInvite.getGuildScheduledEventId();
        }
        pairArr[4] = o.to("invite_guild_scheduled_event_id", l);
        AnalyticsTracker.INSTANCE.inviteSent(guildInvite, channel, h0.plus(h0.mapOf(pairArr), getGuildAndChannelSnapshotAnalyticsProperties(message.g())));
    }
}
