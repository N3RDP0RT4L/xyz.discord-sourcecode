package com.discord.stores;

import kotlin.Metadata;
/* compiled from: StoreMessageReactions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010$\n\u0002\b\u0002*\f\b\u0002\u0010\u0001\"\u00020\u00002\u00020\u0000*D\b\u0002\u0010\u0007\"\u001a\u0012\u0004\u0012\u0002`\u0003\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00020\u00022\"\u0012\b\u0012\u00060\u0006j\u0002`\u0003\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0000j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00020\u0002*D\b\u0002\u0010\t\"\u001a\u0012\u0004\u0012\u0002`\u0003\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\b0\b2\"\u0012\b\u0012\u00060\u0006j\u0002`\u0003\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0000j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\b0\b¨\u0006\n"}, d2 = {"", "EmojiKey", "", "Lcom/discord/primitives/MessageId;", "Lcom/discord/stores/EmojiKey;", "Lcom/discord/stores/StoreMessageReactions$EmojiResults;", "", "MutableReactionsMap", "", "ReactionsMap", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessageReactionsKt {
}
