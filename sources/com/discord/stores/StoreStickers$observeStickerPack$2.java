package com.discord.stores;

import com.discord.stores.StoreStickers;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreStickers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/stores/StoreStickers$StickerPackState;", "invoke", "()Lcom/discord/stores/StoreStickers$StickerPackState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStickers$observeStickerPack$2 extends o implements Function0<StoreStickers.StickerPackState> {
    public final /* synthetic */ long $stickerPackId;
    public final /* synthetic */ StoreStickers this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreStickers$observeStickerPack$2(StoreStickers storeStickers, long j) {
        super(0);
        this.this$0 = storeStickers;
        this.$stickerPackId = j;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final StoreStickers.StickerPackState invoke() {
        Map map;
        map = this.this$0.stickerPacks;
        StoreStickers.StickerPackState stickerPackState = (StoreStickers.StickerPackState) map.get(Long.valueOf(this.$stickerPackId));
        return stickerPackState != null ? stickerPackState : StoreStickers.StickerPackState.Unknown.INSTANCE;
    }
}
