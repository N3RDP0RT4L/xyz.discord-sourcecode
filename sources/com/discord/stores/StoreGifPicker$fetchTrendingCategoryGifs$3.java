package com.discord.stores;

import com.discord.models.gifpicker.dto.ModelGif;
import d0.z.d.k;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreGifPicker.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/models/gifpicker/dto/ModelGif;", "p1", "", "invoke", "(Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final /* synthetic */ class StoreGifPicker$fetchTrendingCategoryGifs$3 extends k implements Function1<List<? extends ModelGif>, Unit> {
    public StoreGifPicker$fetchTrendingCategoryGifs$3(StoreGifPicker storeGifPicker) {
        super(1, storeGifPicker, StoreGifPicker.class, "handleFetchTrendingGifsOnNext", "handleFetchTrendingGifsOnNext(Ljava/util/List;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(List<? extends ModelGif> list) {
        invoke2((List<ModelGif>) list);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<ModelGif> list) {
        m.checkNotNullParameter(list, "p1");
        ((StoreGifPicker) this.receiver).handleFetchTrendingGifsOnNext(list);
    }
}
