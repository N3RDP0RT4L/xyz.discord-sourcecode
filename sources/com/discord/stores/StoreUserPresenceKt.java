package com.discord.stores;

import kotlin.Metadata;
/* compiled from: StoreUserPresence.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\"\u0016\u0010\u0001\u001a\u00020\u00008\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0001\u0010\u0002*\f\b\u0002\u0010\u0004\"\u00020\u00032\u00020\u0003*\f\b\u0002\u0010\u0006\"\u00020\u00052\u00020\u0005¨\u0006\u0007"}, d2 = {"", "ME_GUILD_ID", "J", "Lcom/discord/api/presence/Presence;", "ApiPresence", "Lcom/discord/models/presence/Presence;", "AppPresence", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserPresenceKt {
    private static final long ME_GUILD_ID = Long.MAX_VALUE;
}
