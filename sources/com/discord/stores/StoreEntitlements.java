package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelEntitlement;
import com.discord.restapi.RestAPIInterface;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.t.h0;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreEntitlements.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010%\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0001/B!\u0012\u0006\u0010!\u001a\u00020 \u0012\u0006\u0010\u001c\u001a\u00020\u001b\u0012\b\b\u0002\u0010$\u001a\u00020#¢\u0006\u0004\b-\u0010.J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0013\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00020\u0005¢\u0006\u0004\b\u0006\u0010\u0007J\u000f\u0010\t\u001a\u00020\bH\u0007¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\bH\u0007¢\u0006\u0004\b\u000b\u0010\nJ\u001d\u0010\u000f\u001a\u00020\b2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\fH\u0007¢\u0006\u0004\b\u000f\u0010\u0010J)\u0010\u0015\u001a\u00020\b2\n\u0010\u0013\u001a\u00060\u0011j\u0002`\u00122\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\r0\fH\u0007¢\u0006\u0004\b\u0015\u0010\u0016J\u000f\u0010\u0017\u001a\u00020\bH\u0017¢\u0006\u0004\b\u0017\u0010\nJ\u0019\u0010\u0018\u001a\u00020\b2\n\u0010\u0013\u001a\u00060\u0011j\u0002`\u0012¢\u0006\u0004\b\u0018\u0010\u0019J\r\u0010\u001a\u001a\u00020\b¢\u0006\u0004\b\u001a\u0010\nR\u0016\u0010\u001c\u001a\u00020\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR\u0016\u0010\u001e\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR\u0016\u0010!\u001a\u00020 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"R\u0016\u0010$\u001a\u00020#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%R,\u0010(\u001a\u0018\u0012\b\u0012\u00060\u0011j\u0002`'\u0012\n\u0012\b\u0012\u0004\u0012\u00020\r0\f0&8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b(\u0010)R\u0016\u0010*\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b*\u0010\u001fR,\u0010,\u001a\u0018\u0012\b\u0012\u00060\u0011j\u0002`'\u0012\n\u0012\b\u0012\u0004\u0012\u00020\r0\f0+8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b,\u0010)¨\u00060"}, d2 = {"Lcom/discord/stores/StoreEntitlements;", "Lcom/discord/stores/StoreV2;", "Lcom/discord/stores/StoreEntitlements$State;", "getEntitlementState", "()Lcom/discord/stores/StoreEntitlements$State;", "Lrx/Observable;", "observeEntitlementState", "()Lrx/Observable;", "", "handleFetchingState", "()V", "handleFetchError", "", "Lcom/discord/models/domain/ModelEntitlement;", "giftEntitlements", "handleFetchGiftsSuccess", "(Ljava/util/List;)V", "", "Lcom/discord/primitives/ApplicationId;", "applicationId", "entitlements", "handleFetchEntitlementsSuccess", "(JLjava/util/List;)V", "snapshotData", "fetchMyEntitlementsForApplication", "(J)V", "fetchMyGiftEntitlements", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "state", "Lcom/discord/stores/StoreEntitlements$State;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "", "Lcom/discord/primitives/SkuId;", "giftEntitlementMap", "Ljava/util/Map;", "stateSnapshot", "", "entitlementMap", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/utilities/rest/RestAPI;)V", "State", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreEntitlements extends StoreV2 {
    private final Dispatcher dispatcher;
    private Map<Long, List<ModelEntitlement>> entitlementMap;
    private Map<Long, ? extends List<ModelEntitlement>> giftEntitlementMap;
    private final ObservationDeck observationDeck;
    private final RestAPI restAPI;
    private State state;
    private State stateSnapshot;

    /* compiled from: StoreEntitlements.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0006\u0007\bB\t\b\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u000f\u0010\u0002\u001a\u00020\u0000H\u0016¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\t\n\u000b¨\u0006\f"}, d2 = {"Lcom/discord/stores/StoreEntitlements$State;", "", "deepCopy", "()Lcom/discord/stores/StoreEntitlements$State;", HookHelper.constructorName, "()V", "Failure", "Loaded", "Loading", "Lcom/discord/stores/StoreEntitlements$State$Loading;", "Lcom/discord/stores/StoreEntitlements$State$Failure;", "Lcom/discord/stores/StoreEntitlements$State$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class State {

        /* compiled from: StoreEntitlements.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreEntitlements$State$Failure;", "Lcom/discord/stores/StoreEntitlements$State;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Failure extends State {
            public static final Failure INSTANCE = new Failure();

            private Failure() {
                super(null);
            }
        }

        /* compiled from: StoreEntitlements.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001BC\u0012\u001c\u0010\r\u001a\u0018\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0004\u0012\u001c\u0010\u000e\u001a\u0018\u0012\b\u0012\u00060\u0005j\u0002`\u000b\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0004¢\u0006\u0004\b\u001f\u0010 J\u000f\u0010\u0002\u001a\u00020\u0000H\u0016¢\u0006\u0004\b\u0002\u0010\u0003J&\u0010\t\u001a\u0018\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0004HÆ\u0003¢\u0006\u0004\b\t\u0010\nJ&\u0010\f\u001a\u0018\u0012\b\u0012\u00060\u0005j\u0002`\u000b\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0004HÆ\u0003¢\u0006\u0004\b\f\u0010\nJP\u0010\u000f\u001a\u00020\u00002\u001e\b\u0002\u0010\r\u001a\u0018\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u00042\u001e\b\u0002\u0010\u000e\u001a\u0018\u0012\b\u0012\u00060\u0005j\u0002`\u000b\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0004HÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u001a\u0010\u001a\u001a\u00020\u00192\b\u0010\u0018\u001a\u0004\u0018\u00010\u0017HÖ\u0003¢\u0006\u0004\b\u001a\u0010\u001bR/\u0010\u000e\u001a\u0018\u0012\b\u0012\u00060\u0005j\u0002`\u000b\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u00048\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001c\u001a\u0004\b\u001d\u0010\nR/\u0010\r\u001a\u0018\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u00048\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001c\u001a\u0004\b\u001e\u0010\n¨\u0006!"}, d2 = {"Lcom/discord/stores/StoreEntitlements$State$Loaded;", "Lcom/discord/stores/StoreEntitlements$State;", "deepCopy", "()Lcom/discord/stores/StoreEntitlements$State$Loaded;", "", "", "Lcom/discord/primitives/SkuId;", "", "Lcom/discord/models/domain/ModelEntitlement;", "component1", "()Ljava/util/Map;", "Lcom/discord/primitives/ApplicationId;", "component2", "giftableEntitlements", "ownedEntitlements", "copy", "(Ljava/util/Map;Ljava/util/Map;)Lcom/discord/stores/StoreEntitlements$State$Loaded;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getOwnedEntitlements", "getGiftableEntitlements", HookHelper.constructorName, "(Ljava/util/Map;Ljava/util/Map;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Loaded extends State {
            private final Map<Long, List<ModelEntitlement>> giftableEntitlements;
            private final Map<Long, List<ModelEntitlement>> ownedEntitlements;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Loaded(Map<Long, ? extends List<ModelEntitlement>> map, Map<Long, ? extends List<ModelEntitlement>> map2) {
                super(null);
                m.checkNotNullParameter(map, "giftableEntitlements");
                m.checkNotNullParameter(map2, "ownedEntitlements");
                this.giftableEntitlements = map;
                this.ownedEntitlements = map2;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Loaded copy$default(Loaded loaded, Map map, Map map2, int i, Object obj) {
                if ((i & 1) != 0) {
                    map = loaded.giftableEntitlements;
                }
                if ((i & 2) != 0) {
                    map2 = loaded.ownedEntitlements;
                }
                return loaded.copy(map, map2);
            }

            public final Map<Long, List<ModelEntitlement>> component1() {
                return this.giftableEntitlements;
            }

            public final Map<Long, List<ModelEntitlement>> component2() {
                return this.ownedEntitlements;
            }

            public final Loaded copy(Map<Long, ? extends List<ModelEntitlement>> map, Map<Long, ? extends List<ModelEntitlement>> map2) {
                m.checkNotNullParameter(map, "giftableEntitlements");
                m.checkNotNullParameter(map2, "ownedEntitlements");
                return new Loaded(map, map2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return m.areEqual(this.giftableEntitlements, loaded.giftableEntitlements) && m.areEqual(this.ownedEntitlements, loaded.ownedEntitlements);
            }

            public final Map<Long, List<ModelEntitlement>> getGiftableEntitlements() {
                return this.giftableEntitlements;
            }

            public final Map<Long, List<ModelEntitlement>> getOwnedEntitlements() {
                return this.ownedEntitlements;
            }

            public int hashCode() {
                Map<Long, List<ModelEntitlement>> map = this.giftableEntitlements;
                int i = 0;
                int hashCode = (map != null ? map.hashCode() : 0) * 31;
                Map<Long, List<ModelEntitlement>> map2 = this.ownedEntitlements;
                if (map2 != null) {
                    i = map2.hashCode();
                }
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(giftableEntitlements=");
                R.append(this.giftableEntitlements);
                R.append(", ownedEntitlements=");
                return a.L(R, this.ownedEntitlements, ")");
            }

            @Override // com.discord.stores.StoreEntitlements.State
            public Loaded deepCopy() {
                return copy(new HashMap(this.giftableEntitlements), new HashMap(this.ownedEntitlements));
            }
        }

        /* compiled from: StoreEntitlements.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreEntitlements$State$Loading;", "Lcom/discord/stores/StoreEntitlements$State;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Loading extends State {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        private State() {
        }

        public State deepCopy() {
            return this;
        }

        public /* synthetic */ State(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ StoreEntitlements(Dispatcher dispatcher, ObservationDeck observationDeck, RestAPI restAPI, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(dispatcher, observationDeck, (i & 4) != 0 ? RestAPI.Companion.getApi() : restAPI);
    }

    public final void fetchMyEntitlementsForApplication(long j) {
        this.dispatcher.schedule(new StoreEntitlements$fetchMyEntitlementsForApplication$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPIInterface.DefaultImpls.getMyEntitlements$default(this.restAPI, j, false, 2, null), false, 1, null), StoreEntitlements.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new StoreEntitlements$fetchMyEntitlementsForApplication$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreEntitlements$fetchMyEntitlementsForApplication$3(this, j));
    }

    public final void fetchMyGiftEntitlements() {
        this.dispatcher.schedule(new StoreEntitlements$fetchMyGiftEntitlements$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.getGifts(), false, 1, null), StoreEntitlements.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new StoreEntitlements$fetchMyGiftEntitlements$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreEntitlements$fetchMyGiftEntitlements$3(this));
    }

    public final State getEntitlementState() {
        return this.stateSnapshot;
    }

    @StoreThread
    public final void handleFetchEntitlementsSuccess(long j, List<ModelEntitlement> list) {
        m.checkNotNullParameter(list, "entitlements");
        this.entitlementMap.put(Long.valueOf(j), list);
        this.state = new State.Loaded(this.giftEntitlementMap, this.entitlementMap);
        markChanged();
    }

    @StoreThread
    public final void handleFetchError() {
        this.state = State.Failure.INSTANCE;
        markChanged();
    }

    @StoreThread
    public final void handleFetchGiftsSuccess(List<ModelEntitlement> list) {
        m.checkNotNullParameter(list, "giftEntitlements");
        HashMap hashMap = new HashMap();
        for (ModelEntitlement modelEntitlement : list) {
            List list2 = (List) hashMap.get(Long.valueOf(modelEntitlement.getSkuId()));
            if (list2 == null) {
                list2 = new ArrayList();
            }
            list2.add(modelEntitlement);
            hashMap.put(Long.valueOf(modelEntitlement.getSkuId()), list2);
        }
        this.giftEntitlementMap = hashMap;
        this.state = new State.Loaded(hashMap, this.entitlementMap);
        markChanged();
    }

    @StoreThread
    public final void handleFetchingState() {
        this.state = State.Loading.INSTANCE;
        markChanged();
    }

    public final Observable<State> observeEntitlementState() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreEntitlements$observeEntitlementState$1(this), 14, null);
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        super.snapshotData();
        this.stateSnapshot = this.state.deepCopy();
    }

    public StoreEntitlements(Dispatcher dispatcher, ObservationDeck observationDeck, RestAPI restAPI) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(restAPI, "restAPI");
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
        this.restAPI = restAPI;
        this.giftEntitlementMap = h0.emptyMap();
        this.entitlementMap = new LinkedHashMap();
        State.Loading loading = State.Loading.INSTANCE;
        this.state = loading;
        this.stateSnapshot = loading;
    }
}
