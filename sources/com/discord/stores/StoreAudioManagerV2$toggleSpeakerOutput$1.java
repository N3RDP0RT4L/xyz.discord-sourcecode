package com.discord.stores;

import com.discord.rtcconnection.audio.DiscordAudioManager;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import java.util.ListIterator;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreAudioManagerV2.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAudioManagerV2$toggleSpeakerOutput$1 extends o implements Function0<Unit> {
    public final /* synthetic */ StoreAudioManagerV2 this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreAudioManagerV2$toggleSpeakerOutput$1(StoreAudioManagerV2 storeAudioManagerV2) {
        super(0);
        this.this$0 = storeAudioManagerV2;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        DiscordAudioManager.DeviceTypes deviceTypes;
        boolean z2;
        if (this.this$0.getState$app_productionGoogleRelease().getActiveAudioDevice().ordinal() != 2) {
            this.this$0.selectOutputDevice(DiscordAudioManager.DeviceTypes.SPEAKERPHONE);
            return;
        }
        DiscordAudioManager discordAudioManager = DiscordAudioManager.d;
        List<DiscordAudioManager.DeviceTypes> list = DiscordAudioManager.c;
        ListIterator<DiscordAudioManager.DeviceTypes> listIterator = list.listIterator(list.size());
        while (true) {
            if (!listIterator.hasPrevious()) {
                deviceTypes = null;
                break;
            }
            deviceTypes = listIterator.previous();
            DiscordAudioManager.DeviceTypes deviceTypes2 = deviceTypes;
            if (deviceTypes2 == DiscordAudioManager.DeviceTypes.SPEAKERPHONE || !this.this$0.getState$app_productionGoogleRelease().getAudioDevices().get(deviceTypes2.getValue()).f2761b) {
                z2 = false;
                continue;
            } else {
                z2 = true;
                continue;
            }
            if (z2) {
                break;
            }
        }
        m.checkNotNull(deviceTypes);
        this.this$0.selectOutputDevice(deviceTypes);
    }
}
