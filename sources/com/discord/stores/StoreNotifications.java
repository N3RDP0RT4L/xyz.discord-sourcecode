package com.discord.stores;

import andhook.lib.HookHelper;
import android.app.Application;
import android.content.Context;
import androidx.appcompat.widget.ActivityChooserModel;
import androidx.core.app.FrameMetricsAggregator;
import b.a.d.o;
import b.a.e.d;
import b.d.b.a.a;
import b.i.a.f.e.o.f;
import com.discord.api.channel.Channel;
import com.discord.app.AppActivity;
import com.discord.models.message.Message;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreStream;
import com.discord.utilities.SnowflakeUtils;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.fcm.NotificationClient;
import com.discord.utilities.persister.Persister;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ActivityLifecycleCallbacks;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$3;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$4;
import com.discord.utilities.time.Clock;
import com.discord.widgets.notice.NoticePopupChannel;
import d0.z.d.m;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import s.a.k0;
/* compiled from: StoreNotifications.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0010\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 E2\u00020\u0001:\u0001EB\u0017\u0012\u0006\u0010:\u001a\u000209\u0012\u0006\u0010A\u001a\u00020@¢\u0006\u0004\bC\u0010DJ\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0019\u0010\u0007\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0007\u0010\u0006J\u001f\u0010\f\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000e\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u000f\u0010\u0010\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0010\u0010\u000fJ\u0017\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u0011H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0013\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00160\u0015¢\u0006\u0004\b\u0017\u0010\u0018J\u000f\u0010\u0019\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u0015\u0010\u001d\u001a\u00020\u00042\u0006\u0010\u001c\u001a\u00020\u001b¢\u0006\u0004\b\u001d\u0010\u001eJ\u0015\u0010 \u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\u001b¢\u0006\u0004\b \u0010\u001eJ\u0015\u0010\"\u001a\u00020\u00042\u0006\u0010!\u001a\u00020\u001b¢\u0006\u0004\b\"\u0010\u001eJ\u0015\u0010$\u001a\u00020\u00042\u0006\u0010#\u001a\u00020\u001b¢\u0006\u0004\b$\u0010\u001eJ\u001f\u0010'\u001a\u00020\u00042\u0006\u0010%\u001a\u00020\u001b2\b\b\u0002\u0010&\u001a\u00020\u001b¢\u0006\u0004\b'\u0010(J\u0015\u0010)\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u0011¢\u0006\u0004\b)\u0010\u0014J\u0017\u0010+\u001a\u00020\u00042\b\u0010*\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b+\u0010\u0006J\u001b\u0010/\u001a\u00020\u00042\n\u0010.\u001a\u00060,j\u0002`-H\u0007¢\u0006\u0004\b/\u00100J\u0017\u00102\u001a\u00020\u00042\u0006\u0010\t\u001a\u000201H\u0007¢\u0006\u0004\b2\u00103J\r\u00104\u001a\u00020\u0004¢\u0006\u0004\b4\u0010\u000fR\u001c\u00106\u001a\b\u0012\u0004\u0012\u00020\u0016058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b6\u00107R\u0018\u0010*\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b*\u00108R\u0016\u0010:\u001a\u0002098\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b:\u0010;R\u0018\u0010=\u001a\u0004\u0018\u00010<8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b=\u0010>R\u0018\u0010?\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b?\u00108R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0003\u00108R\u0016\u0010A\u001a\u00020@8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bA\u0010B¨\u0006F"}, d2 = {"Lcom/discord/stores/StoreNotifications;", "Lcom/discord/stores/Store;", "", "pushToken", "", "handleRegistrationToken", "(Ljava/lang/String;)V", "handleRegistrationTokenPersisted", "Lcom/discord/models/message/Message;", "message", "Lcom/discord/api/channel/Channel;", "channel", "displayPopup", "(Lcom/discord/models/message/Message;Lcom/discord/api/channel/Channel;)V", "tryTokenPersist", "()V", "configureNotificationClient", "Landroid/app/Application;", "application", "configureContextSetter", "(Landroid/app/Application;)V", "Lrx/Observable;", "Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;", "getSettings", "()Lrx/Observable;", "getPushToken", "()Ljava/lang/String;", "", "notificationsVibrateDisabled", "setNotificationsVibrateDisabled", "(Z)V", "notificationLightDisabled", "setNotificationLightDisabled", "notificationSoundDisabled", "setNotificationSoundDisabled", "enabled", "setEnabled", "isEnabledInApp", "logToggle", "setEnabledInApp", "(ZZ)V", "init", "authToken", "handleAuthToken", "", "Lcom/discord/primitives/ChannelId;", "channelId", "handleChannelSelected", "(J)V", "Lcom/discord/api/message/Message;", "handleMessageCreate", "(Lcom/discord/api/message/Message;)V", "handlePreLogout", "Lcom/discord/utilities/persister/Persister;", "notificationSettings", "Lcom/discord/utilities/persister/Persister;", "Ljava/lang/String;", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "Landroid/content/Context;", "context", "Landroid/content/Context;", "pushTokenPersisted", "Lcom/discord/stores/StoreStream;", "stream", "Lcom/discord/stores/StoreStream;", HookHelper.constructorName, "(Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreStream;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreNotifications extends Store {
    public static final Companion Companion = new Companion(null);
    private static final long INAPP_MESSAGE_WINDOW_MS = 10000;
    private String authToken;
    private final Clock clock;
    private Context context;
    private final Persister<NotificationClient.SettingsV2> notificationSettings = new Persister<>("STORE_NOTIFICATIONS_SETTINGS_V2", new NotificationClient.SettingsV2(false, false, false, false, false, false, null, null, null, FrameMetricsAggregator.EVERY_DURATION, null));
    private String pushToken;
    private String pushTokenPersisted;
    private final StoreStream stream;

    /* compiled from: StoreNotifications.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/stores/StoreNotifications$Companion;", "", "", "INAPP_MESSAGE_WINDOW_MS", "J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public StoreNotifications(Clock clock, StoreStream storeStream) {
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(storeStream, "stream");
        this.clock = clock;
        this.stream = storeStream;
    }

    private final void configureContextSetter(Application application) {
        application.registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() { // from class: com.discord.stores.StoreNotifications$configureContextSetter$1
            @Override // com.discord.utilities.rx.ActivityLifecycleCallbacks
            public void onActivityCreatedOrResumed(AppActivity appActivity) {
                m.checkNotNullParameter(appActivity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
                super.onActivityCreatedOrResumed(appActivity);
                StoreNotifications.this.context = appActivity;
            }

            @Override // com.discord.utilities.rx.ActivityLifecycleCallbacks
            public void onActivityDestroyed(AppActivity appActivity) {
                m.checkNotNullParameter(appActivity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
                super.onActivityDestroyed(appActivity);
                StoreNotifications.this.context = null;
            }
        });
    }

    private final void configureNotificationClient() {
        NotificationClient.INSTANCE.setRegistrationIdReceived(new StoreNotifications$configureNotificationClient$1(this));
        Observable<NotificationClient.SettingsV2> settings = getSettings();
        StoreStream.Companion companion = StoreStream.Companion;
        Observable j = Observable.j(Observable.h(settings, companion.getAuthentication().getAuthedToken$app_productionGoogleRelease(), companion.getUserSettingsSystem().observeSettings(false), ObservableExtensionsKt.leadingEdgeThrottle(companion.getPermissions().observePermissionsForAllChannels(), 1L, TimeUnit.SECONDS).F(StoreNotifications$configureNotificationClient$completedSettings$1.INSTANCE), StoreNotifications$configureNotificationClient$completedSettings$2.INSTANCE).q(), d.d.a(), StoreNotifications$configureNotificationClient$2.INSTANCE);
        m.checkNotNullExpressionValue(j, "Observable\n        .comb… isBackgrounded\n        }");
        Observable q = ObservableExtensionsKt.computationBuffered(j).q();
        m.checkNotNullExpressionValue(q, "Observable\n        .comb…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(q, (r18 & 1) != 0 ? null : null, "nsClient", (r18 & 4) != 0 ? null : null, StoreNotifications$configureNotificationClient$3.INSTANCE, (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
    }

    private final void displayPopup(Message message, Channel channel) {
        Context context;
        if ((message.getId() >>> 22) + SnowflakeUtils.DISCORD_EPOCH + 10000 > this.clock.currentTimeMillis() && (context = this.context) != null) {
            NoticePopupChannel noticePopupChannel = NoticePopupChannel.INSTANCE;
            StringBuilder R = a.R("{InAppNotif}#");
            R.append(message.getChannelId());
            noticePopupChannel.enqueue(context, R.toString(), message, new StoreNotifications$displayPopup$1(channel, message));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void handleRegistrationToken(String str) {
        this.pushToken = str;
        tryTokenPersist();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void handleRegistrationTokenPersisted(String str) {
        this.pushTokenPersisted = str;
    }

    public static /* synthetic */ void setEnabledInApp$default(StoreNotifications storeNotifications, boolean z2, boolean z3, int i, Object obj) {
        if ((i & 2) != 0) {
            z3 = true;
        }
        storeNotifications.setEnabledInApp(z2, z3);
    }

    private final void tryTokenPersist() {
        String str = this.authToken;
        if (str == null) {
            this.pushTokenPersisted = null;
        }
        if (str != null && !m.areEqual(this.pushToken, this.pushTokenPersisted)) {
            ObservableExtensionsKt.computationBuffered(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().userCreateDevice(new RestAPIParams.UserDevices(this.pushToken)), false, 1, null)).k(o.a.g(null, new StoreNotifications$tryTokenPersist$1(this), null));
        }
    }

    public final synchronized String getPushToken() {
        return this.pushToken;
    }

    public final Observable<NotificationClient.SettingsV2> getSettings() {
        return ObservableExtensionsKt.computationBuffered(this.notificationSettings.getObservable());
    }

    public final synchronized void handleAuthToken(String str) {
        this.authToken = str;
        tryTokenPersist();
    }

    @StoreThread
    public final void handleChannelSelected(long j) {
        f.H0(f.c(k0.a), null, null, new StoreNotifications$handleChannelSelected$1(j, null), 3, null);
    }

    /* JADX WARN: Removed duplicated region for block: B:31:0x00ce  */
    /* JADX WARN: Removed duplicated region for block: B:43:? A[RETURN, SYNTHETIC] */
    @com.discord.stores.StoreThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void handleMessageCreate(com.discord.api.message.Message r13) {
        /*
            Method dump skipped, instructions count: 301
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreNotifications.handleMessageCreate(com.discord.api.message.Message):void");
    }

    public final void handlePreLogout() {
        Persister.set$default(this.notificationSettings, new NotificationClient.SettingsV2(false, false, false, false, false, false, null, null, null, FrameMetricsAggregator.EVERY_DURATION, null), false, 2, null);
    }

    public final void init(Application application) {
        m.checkNotNullParameter(application, "application");
        super.init((Context) application);
        configureContextSetter(application);
        configureNotificationClient();
    }

    public final void setEnabled(boolean z2) {
        this.notificationSettings.getAndSet(true, new StoreNotifications$setEnabled$1(z2));
        AnalyticsTracker.INSTANCE.updateNotifications(z2);
    }

    public final void setEnabledInApp(boolean z2, boolean z3) {
        NotificationClient.SettingsV2 andSet = this.notificationSettings.getAndSet(true, new StoreNotifications$setEnabledInApp$oldValue$1(z2));
        if (z3 && andSet.isEnabledInApp() != z2) {
            AnalyticsTracker.INSTANCE.updateNotificationsInApp(z2);
        }
    }

    public final void setNotificationLightDisabled(boolean z2) {
        this.notificationSettings.getAndSet(true, new StoreNotifications$setNotificationLightDisabled$1(z2));
    }

    public final void setNotificationSoundDisabled(boolean z2) {
        this.notificationSettings.getAndSet(true, new StoreNotifications$setNotificationSoundDisabled$1(z2));
    }

    public final void setNotificationsVibrateDisabled(boolean z2) {
        this.notificationSettings.getAndSet(true, new StoreNotifications$setNotificationsVibrateDisabled$1(z2));
    }
}
