package com.discord.stores;

import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreRtcConnection.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreRtcConnection$onSpeaking$1 extends o implements Function0<Unit> {
    public final /* synthetic */ boolean $isSpeaking;
    public final /* synthetic */ long $userId;
    public final /* synthetic */ StoreRtcConnection this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreRtcConnection$onSpeaking$1(StoreRtcConnection storeRtcConnection, long j, boolean z2) {
        super(0);
        this.this$0 = storeRtcConnection;
        this.$userId = j;
        this.$isSpeaking = z2;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        VoicePropsTracker voicePropsTracker;
        voicePropsTracker = this.this$0.voicePropsTracker;
        if (voicePropsTracker != null) {
            voicePropsTracker.handleOnSpeaking(this.$userId, this.$isSpeaking);
        }
    }
}
