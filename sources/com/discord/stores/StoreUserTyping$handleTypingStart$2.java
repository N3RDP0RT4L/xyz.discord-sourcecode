package com.discord.stores;

import com.discord.utilities.analytics.Traits;
import d0.z.d.m;
import d0.z.d.o;
import java.util.HashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import rx.Subscription;
/* compiled from: StoreUserTyping.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lrx/Subscription;", Traits.Payment.Type.SUBSCRIPTION, "", "invoke", "(Lrx/Subscription;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserTyping$handleTypingStart$2 extends o implements Function1<Subscription, Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ long $userId;
    public final /* synthetic */ StoreUserTyping this$0;

    /* compiled from: StoreUserTyping.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreUserTyping$handleTypingStart$2$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ Subscription $subscription;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Subscription subscription) {
            super(0);
            this.$subscription = subscription;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            HashMap hashMap;
            hashMap = StoreUserTyping$handleTypingStart$2.this.this$0.typingUsersRemoveCallbacks;
            Map map = (Map) hashMap.get(Long.valueOf(StoreUserTyping$handleTypingStart$2.this.$channelId));
            if (map != null) {
                Subscription subscription = (Subscription) map.put(Long.valueOf(StoreUserTyping$handleTypingStart$2.this.$userId), this.$subscription);
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreUserTyping$handleTypingStart$2(StoreUserTyping storeUserTyping, long j, long j2) {
        super(1);
        this.this$0 = storeUserTyping;
        this.$channelId = j;
        this.$userId = j2;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Subscription subscription) {
        invoke2(subscription);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Subscription subscription) {
        Dispatcher dispatcher;
        m.checkNotNullParameter(subscription, Traits.Payment.Type.SUBSCRIPTION);
        dispatcher = this.this$0.dispatcher;
        dispatcher.schedule(new AnonymousClass1(subscription));
    }
}
