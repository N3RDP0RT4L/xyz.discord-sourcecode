package com.discord.stores;

import d0.z.d.m;
import d0.z.d.o;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreNotices.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0007\u001a\u0012\u0012\u0004\u0012\u00020\u0001\u0012\b\u0012\u00060\u0002j\u0002`\u00030\u00002\u0016\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u0001\u0012\b\u0012\u00060\u0002j\u0002`\u00030\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Ljava/util/HashMap;", "", "", "Lcom/discord/primitives/Timestamp;", "cache", "invoke", "(Ljava/util/HashMap;)Ljava/util/HashMap;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreNotices$clearSeen$1 extends o implements Function1<HashMap<String, Long>, HashMap<String, Long>> {
    public final /* synthetic */ String $noticeName;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreNotices$clearSeen$1(String str) {
        super(1);
        this.$noticeName = str;
    }

    public final HashMap<String, Long> invoke(HashMap<String, Long> hashMap) {
        m.checkNotNullParameter(hashMap, "cache");
        hashMap.remove(this.$noticeName);
        return hashMap;
    }
}
