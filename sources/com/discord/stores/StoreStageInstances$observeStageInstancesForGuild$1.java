package com.discord.stores;

import com.discord.api.stageinstance.StageInstance;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreStageInstances.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u00030\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/stageinstance/StageInstance;", "invoke", "()Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStageInstances$observeStageInstancesForGuild$1 extends o implements Function0<Map<Long, ? extends StageInstance>> {
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ StoreStageInstances this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreStageInstances$observeStageInstancesForGuild$1(StoreStageInstances storeStageInstances, long j) {
        super(0);
        this.this$0 = storeStageInstances;
        this.$guildId = j;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Map<Long, ? extends StageInstance> invoke() {
        return this.this$0.getStageInstancesForGuild(this.$guildId);
    }
}
