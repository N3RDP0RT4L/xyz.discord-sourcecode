package com.discord.stores;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import com.discord.simpleast.core.parser.Parser;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$3;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$4;
import com.discord.utilities.search.query.FilterType;
import com.discord.utilities.search.query.node.QueryNode;
import com.discord.utilities.search.query.node.answer.ChannelNode;
import com.discord.utilities.search.query.node.answer.HasAnswerOption;
import com.discord.utilities.search.query.node.answer.HasNode;
import com.discord.utilities.search.query.node.answer.UserNode;
import com.discord.utilities.search.query.node.content.ContentNode;
import com.discord.utilities.search.query.node.filter.FilterNode;
import com.discord.utilities.search.query.parsing.QueryParser;
import com.discord.utilities.search.strings.SearchStringProvider;
import com.discord.utilities.search.suggestion.entries.ChannelSuggestion;
import com.discord.utilities.search.suggestion.entries.UserSuggestion;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import j0.p.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.Observable;
import rx.Subscription;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
/* compiled from: StoreSearchInput.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0086\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010!\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\bI\u0010\u001bJ\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u001d\u0010\u000b\u001a\u00020\n2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u0007H\u0002¢\u0006\u0004\b\u000b\u0010\fJ3\u0010\u0010\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\n2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u000fH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u0015\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u0012¢\u0006\u0004\b\u0014\u0010\u0015J\u0015\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u0016¢\u0006\u0004\b\u0018\u0010\u0019J\r\u0010\u001a\u001a\u00020\u0004¢\u0006\u0004\b\u001a\u0010\u001bJ+\u0010 \u001a\u00020\u00042\u0006\u0010\u001d\u001a\u00020\u001c2\u0006\u0010\u001f\u001a\u00020\u001e2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u000f¢\u0006\u0004\b \u0010!J+\u0010#\u001a\u00020\u00042\u0006\u0010\u001d\u001a\u00020\u001c2\u0006\u0010\"\u001a\u00020\u001e2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u000f¢\u0006\u0004\b#\u0010!J+\u0010'\u001a\u00020\u00042\u0006\u0010%\u001a\u00020$2\u0006\u0010&\u001a\u00020\u001e2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u000f¢\u0006\u0004\b'\u0010(J3\u0010-\u001a\u00020\u00042\u0006\u0010*\u001a\u00020)2\u0006\u0010+\u001a\u00020\u001e2\u0006\u0010,\u001a\u00020\u001e2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u000f¢\u0006\u0004\b-\u0010.J+\u00101\u001a\u00020\u00042\u0006\u00100\u001a\u00020/2\u0006\u0010\u0013\u001a\u00020\u00122\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u000f¢\u0006\u0004\b1\u00102J\u001b\u00103\u001a\u00020\u00042\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\u0004\b3\u00104RJ\u00107\u001a6\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\b 6*\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u00070\u0007\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\b 6*\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u00070\u0007058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b7\u00108R2\u00109\u001a\u001e\u0012\f\u0012\n 6*\u0004\u0018\u00010\u00160\u0016\u0012\f\u0012\n 6*\u0004\u0018\u00010\u00160\u0016058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b9\u00108R%\u0010;\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070:8\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010<\u001a\u0004\b=\u0010>R\u001f\u0010@\u001a\b\u0012\u0004\u0012\u00020?0:8\u0006@\u0006¢\u0006\f\n\u0004\b@\u0010<\u001a\u0004\b@\u0010>R%\u0010A\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070:8\u0006@\u0006¢\u0006\f\n\u0004\bA\u0010<\u001a\u0004\bB\u0010>R\u0018\u0010C\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bC\u0010DR2\u0010E\u001a\u001e\u0012\f\u0012\n 6*\u0004\u0018\u00010?0?\u0012\f\u0012\n 6*\u0004\u0018\u00010?0?058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bE\u00108RR\u0010G\u001a>\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\b 6*\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u00070\u0007 6*\u001e\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\b 6*\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u00070\u0007\u0018\u00010F0F8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bG\u0010H¨\u0006J"}, d2 = {"Lcom/discord/stores/StoreSearchInput;", "", "Lrx/Subscription;", Traits.Payment.Type.SUBSCRIPTION, "", "handleInputSubscription", "(Lrx/Subscription;)V", "", "Lcom/discord/utilities/search/query/node/QueryNode;", "query", "", "getAnswerReplacementStart", "(Ljava/util/List;)I", "replacementIndex", "replacement", "", "replaceAndPublish", "(ILjava/util/List;Ljava/util/List;)V", "Lcom/discord/utilities/search/strings/SearchStringProvider;", "searchStringProvider", "init", "(Lcom/discord/utilities/search/strings/SearchStringProvider;)V", "", "input", "updateInput", "(Ljava/lang/String;)V", "clear", "()V", "Lcom/discord/utilities/search/suggestion/entries/UserSuggestion;", "userSuggestion", "", "fromFilterString", "onFromUserClicked", "(Lcom/discord/utilities/search/suggestion/entries/UserSuggestion;Ljava/lang/CharSequence;Ljava/util/List;)V", "mentionsFilterString", "onMentionsUserClicked", "Lcom/discord/utilities/search/suggestion/entries/ChannelSuggestion;", "channelSuggestion", "inFilterString", "onInChannelClicked", "(Lcom/discord/utilities/search/suggestion/entries/ChannelSuggestion;Ljava/lang/CharSequence;Ljava/util/List;)V", "Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;", "hasAnswerOption", "hasFilterString", "hasAnswerString", "onHasClicked", "(Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/util/List;)V", "Lcom/discord/utilities/search/query/FilterType;", "filterType", "onFilterClicked", "(Lcom/discord/utilities/search/query/FilterType;Lcom/discord/utilities/search/strings/SearchStringProvider;Ljava/util/List;)V", "onQueryClicked", "(Ljava/util/List;)V", "Lrx/subjects/SerializedSubject;", "kotlin.jvm.PlatformType", "astSubject", "Lrx/subjects/SerializedSubject;", "inputSubject", "Lrx/Observable;", "currentParsedInput", "Lrx/Observable;", "getCurrentParsedInput", "()Lrx/Observable;", "", "isInputValid", "forcedInput", "getForcedInput", "inputSubscription", "Lrx/Subscription;", "isInputValidSubject", "Lrx/subjects/PublishSubject;", "forcedInputSubject", "Lrx/subjects/PublishSubject;", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreSearchInput {
    private final SerializedSubject<List<QueryNode>, List<QueryNode>> astSubject;
    private final Observable<List<QueryNode>> currentParsedInput;
    private final Observable<List<QueryNode>> forcedInput;
    private final PublishSubject<List<QueryNode>> forcedInputSubject;
    private final SerializedSubject<String, String> inputSubject = new SerializedSubject<>(BehaviorSubject.l0(""));
    private Subscription inputSubscription;
    private final Observable<Boolean> isInputValid;
    private final SerializedSubject<Boolean, Boolean> isInputValidSubject;

    /* compiled from: StoreSearchInput.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0007\u001a\n \u0002*\u0004\u0018\u00010\u00040\u00042\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "Lcom/discord/utilities/search/query/node/QueryNode;", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Ljava/util/List;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreSearchInput$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1<T, R> implements b<List<? extends QueryNode>, Boolean> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public final Boolean call(List<? extends QueryNode> list) {
            m.checkNotNullExpressionValue(list, "it");
            boolean z2 = true;
            if (!(list instanceof Collection) || !list.isEmpty()) {
                for (QueryNode queryNode : list) {
                    if (!(queryNode instanceof FilterNode)) {
                        break;
                    }
                }
            }
            z2 = false;
            return Boolean.valueOf(z2);
        }
    }

    /* compiled from: StoreSearchInput.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Ljava/lang/Boolean;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreSearchInput$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2 extends o implements Function1<Boolean, Unit> {
        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Boolean bool) {
            invoke2(bool);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Boolean bool) {
            StoreSearchInput.this.isInputValidSubject.k.onNext(bool);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            FilterType.values();
            int[] iArr = new int[4];
            $EnumSwitchMapping$0 = iArr;
            iArr[FilterType.FROM.ordinal()] = 1;
            iArr[FilterType.MENTIONS.ordinal()] = 2;
            iArr[FilterType.IN.ordinal()] = 3;
            iArr[FilterType.HAS.ordinal()] = 4;
        }
    }

    public StoreSearchInput() {
        SerializedSubject<List<QueryNode>, List<QueryNode>> serializedSubject = new SerializedSubject<>(BehaviorSubject.k0());
        this.astSubject = serializedSubject;
        SerializedSubject<Boolean, Boolean> serializedSubject2 = new SerializedSubject<>(BehaviorSubject.l0(Boolean.FALSE));
        this.isInputValidSubject = serializedSubject2;
        PublishSubject<List<QueryNode>> k0 = PublishSubject.k0();
        this.forcedInputSubject = k0;
        this.isInputValid = serializedSubject2;
        this.currentParsedInput = serializedSubject;
        m.checkNotNullExpressionValue(k0, "forcedInputSubject");
        this.forcedInput = k0;
        Observable X = ObservableExtensionsKt.leadingEdgeThrottle(serializedSubject, 50L, TimeUnit.MILLISECONDS).F(AnonymousClass1.INSTANCE).X(a.a());
        m.checkNotNullExpressionValue(X, "astSubject\n        .lead…Schedulers.computation())");
        ObservableExtensionsKt.appSubscribe(X, (r18 & 1) != 0 ? null : null, "validateSearchInput", (r18 & 4) != 0 ? null : null, new AnonymousClass2(), (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
    }

    private final int getAnswerReplacementStart(List<? extends QueryNode> list) {
        if (list.size() <= 1) {
            return 0;
        }
        int lastIndex = n.getLastIndex(list);
        QueryNode queryNode = list.get(lastIndex);
        int i = lastIndex - 1;
        QueryNode queryNode2 = list.get(i);
        if (queryNode instanceof FilterNode) {
            return lastIndex;
        }
        if (!(queryNode instanceof ContentNode) || !(queryNode2 instanceof FilterNode)) {
            return -1;
        }
        return i;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void handleInputSubscription(Subscription subscription) {
        Subscription subscription2 = this.inputSubscription;
        if (subscription2 != null) {
            subscription2.unsubscribe();
        }
        this.inputSubscription = subscription;
    }

    private final void replaceAndPublish(int i, List<? extends QueryNode> list, List<QueryNode> list2) {
        if (i >= 0 && i <= list2.size()) {
            list2.subList(i, list2.size()).clear();
            list2.addAll(i, list);
            this.forcedInputSubject.k.onNext(list2);
        }
    }

    public final void clear() {
        handleInputSubscription(null);
        this.inputSubject.k.onNext("");
    }

    public final Observable<List<QueryNode>> getCurrentParsedInput() {
        return this.currentParsedInput;
    }

    public final Observable<List<QueryNode>> getForcedInput() {
        return this.forcedInput;
    }

    public final void init(SearchStringProvider searchStringProvider) {
        m.checkNotNullParameter(searchStringProvider, "searchStringProvider");
        final QueryParser queryParser = new QueryParser(searchStringProvider);
        Observable X = ObservableExtensionsKt.leadingEdgeThrottle(this.inputSubject, 100L, TimeUnit.MILLISECONDS).q().F(new b<String, List<QueryNode>>() { // from class: com.discord.stores.StoreSearchInput$init$1
            public final List<QueryNode> call(String str) {
                QueryParser queryParser2 = QueryParser.this;
                m.checkNotNullExpressionValue(str, "it");
                return Parser.parse$default(queryParser2, str, null, null, 4, null);
            }
        }).X(a.a());
        m.checkNotNullExpressionValue(X, "inputSubject\n        .le…Schedulers.computation())");
        ObservableExtensionsKt.appSubscribe(X, StoreSearchInput.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new StoreSearchInput$init$2(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreSearchInput$init$3(this.astSubject));
    }

    public final Observable<Boolean> isInputValid() {
        return this.isInputValid;
    }

    public final void onFilterClicked(FilterType filterType, SearchStringProvider searchStringProvider, List<QueryNode> list) {
        FilterNode filterNode;
        int i;
        m.checkNotNullParameter(filterType, "filterType");
        m.checkNotNullParameter(searchStringProvider, "searchStringProvider");
        m.checkNotNullParameter(list, "query");
        ArrayList arrayList = new ArrayList(list);
        int ordinal = filterType.ordinal();
        if (ordinal == 0) {
            filterNode = new FilterNode(FilterType.FROM, searchStringProvider.getFromFilterString());
        } else if (ordinal == 1) {
            filterNode = new FilterNode(FilterType.MENTIONS, searchStringProvider.getMentionsFilterString());
        } else if (ordinal == 2) {
            filterNode = new FilterNode(FilterType.HAS, searchStringProvider.getHasFilterString());
        } else if (ordinal == 3) {
            filterNode = new FilterNode(FilterType.IN, searchStringProvider.getInFilterString());
        } else {
            throw new NoWhenBranchMatchedException();
        }
        if (arrayList.isEmpty()) {
            i = 0;
        } else {
            i = ((QueryNode) u.last((List<? extends Object>) arrayList)) instanceof ContentNode ? n.getLastIndex(list) : list.size();
        }
        replaceAndPublish(i, d0.t.m.listOf(filterNode), list);
    }

    public final void onFromUserClicked(UserSuggestion userSuggestion, CharSequence charSequence, List<QueryNode> list) {
        m.checkNotNullParameter(userSuggestion, "userSuggestion");
        m.checkNotNullParameter(charSequence, "fromFilterString");
        m.checkNotNullParameter(list, "query");
        replaceAndPublish(getAnswerReplacementStart(list), n.listOf((Object[]) new QueryNode[]{new FilterNode(FilterType.FROM, charSequence), new UserNode(userSuggestion.getUserName(), userSuggestion.getDiscriminator())}), list);
    }

    public final void onHasClicked(HasAnswerOption hasAnswerOption, CharSequence charSequence, CharSequence charSequence2, List<QueryNode> list) {
        m.checkNotNullParameter(hasAnswerOption, "hasAnswerOption");
        m.checkNotNullParameter(charSequence, "hasFilterString");
        m.checkNotNullParameter(charSequence2, "hasAnswerString");
        m.checkNotNullParameter(list, "query");
        replaceAndPublish(getAnswerReplacementStart(list), n.listOf((Object[]) new QueryNode[]{new FilterNode(FilterType.HAS, charSequence), new HasNode(hasAnswerOption, charSequence2)}), list);
    }

    public final void onInChannelClicked(ChannelSuggestion channelSuggestion, CharSequence charSequence, List<QueryNode> list) {
        m.checkNotNullParameter(channelSuggestion, "channelSuggestion");
        m.checkNotNullParameter(charSequence, "inFilterString");
        m.checkNotNullParameter(list, "query");
        replaceAndPublish(getAnswerReplacementStart(list), n.listOf((Object[]) new QueryNode[]{new FilterNode(FilterType.IN, charSequence), new ChannelNode(channelSuggestion.getChannelName())}), list);
    }

    public final void onMentionsUserClicked(UserSuggestion userSuggestion, CharSequence charSequence, List<QueryNode> list) {
        m.checkNotNullParameter(userSuggestion, "userSuggestion");
        m.checkNotNullParameter(charSequence, "mentionsFilterString");
        m.checkNotNullParameter(list, "query");
        replaceAndPublish(getAnswerReplacementStart(list), n.listOf((Object[]) new QueryNode[]{new FilterNode(FilterType.MENTIONS, charSequence), new UserNode(userSuggestion.getUserName(), userSuggestion.getDiscriminator())}), list);
    }

    public final void onQueryClicked(List<? extends QueryNode> list) {
        m.checkNotNullParameter(list, "query");
        this.forcedInputSubject.k.onNext(list);
    }

    public final void updateInput(String str) {
        m.checkNotNullParameter(str, "input");
        this.inputSubject.k.onNext(str);
    }
}
