package com.discord.stores;

import com.discord.models.domain.ModelNotificationSettings;
import com.discord.stores.StoreUserGuildSettings;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.subjects.PublishSubject;
/* compiled from: StoreUserGuildSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/ModelNotificationSettings;", "notifSettings", "", "invoke", "(Lcom/discord/models/domain/ModelNotificationSettings;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserGuildSettings$updateUserGuildSettings$1 extends o implements Function1<ModelNotificationSettings, Unit> {
    public final /* synthetic */ Long $channelId;
    public final /* synthetic */ StoreUserGuildSettings.SettingsUpdateType $settingsUpdateType;
    public final /* synthetic */ StoreUserGuildSettings this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreUserGuildSettings$updateUserGuildSettings$1(StoreUserGuildSettings storeUserGuildSettings, Long l, StoreUserGuildSettings.SettingsUpdateType settingsUpdateType) {
        super(1);
        this.this$0 = storeUserGuildSettings;
        this.$channelId = l;
        this.$settingsUpdateType = settingsUpdateType;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ModelNotificationSettings modelNotificationSettings) {
        invoke2(modelNotificationSettings);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ModelNotificationSettings modelNotificationSettings) {
        StoreAnalytics storeAnalytics;
        PublishSubject publishSubject;
        m.checkNotNullParameter(modelNotificationSettings, "notifSettings");
        storeAnalytics = this.this$0.analytics;
        storeAnalytics.onNotificationSettingsUpdated(modelNotificationSettings, this.$channelId);
        publishSubject = this.this$0.eventSubject;
        publishSubject.k.onNext(new StoreUserGuildSettings.Event.SettingsUpdated(this.$settingsUpdateType));
    }
}
