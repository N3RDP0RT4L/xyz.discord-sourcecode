package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.models.message.Message;
import d0.z.d.m;
import j0.k.b;
import java.util.List;
import kotlin.Metadata;
/* compiled from: StoreMessageAck.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\b\u001a\n \u0003*\u0004\u0018\u00010\u00050\u00052\"\u0010\u0004\u001a\u001e\u0012\b\u0012\u00060\u0001j\u0002`\u0002 \u0003*\u000e\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "Lcom/discord/models/message/Message;", "Lcom/discord/stores/ClientMessage;", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Ljava/util/List;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessageAck$markUnread$1<T, R> implements b<List<? extends Message>, Boolean> {
    public static final StoreMessageAck$markUnread$1 INSTANCE = new StoreMessageAck$markUnread$1();

    @Override // j0.k.b
    public /* bridge */ /* synthetic */ Boolean call(List<? extends Message> list) {
        return call2((List<Message>) list);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final Boolean call2(List<Message> list) {
        m.checkNotNullExpressionValue(list, "it");
        return Boolean.valueOf(!list.isEmpty());
    }
}
