package com.discord.stores;

import androidx.core.app.NotificationCompat;
import d0.t.u;
import d0.z.d.m;
import j0.k.b;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
/* compiled from: StoreGuildsSorted.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0003\u0010\b\u001a\u001e\u0012\b\u0012\u00060\u0001j\u0002`\u0002 \u0003*\u000e\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0018\u00010\u00050\u00052\"\u0010\u0004\u001a\u001e\u0012\b\u0012\u00060\u0001j\u0002`\u0002 \u0003*\u000e\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "", "Lcom/discord/primitives/GuildId;", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Ljava/util/List;)Ljava/util/Set;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildsSorted$observeStores$1<T, R> implements b<List<? extends Long>, Set<? extends Long>> {
    public static final StoreGuildsSorted$observeStores$1 INSTANCE = new StoreGuildsSorted$observeStores$1();

    @Override // j0.k.b
    public /* bridge */ /* synthetic */ Set<? extends Long> call(List<? extends Long> list) {
        return call2((List<Long>) list);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final Set<Long> call2(List<Long> list) {
        m.checkNotNullExpressionValue(list, "it");
        return u.toSet(list);
    }
}
