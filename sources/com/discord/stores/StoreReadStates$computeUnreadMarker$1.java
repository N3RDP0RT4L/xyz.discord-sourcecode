package com.discord.stores;

import com.discord.models.application.Unread;
import com.discord.stores.StoreMessageAck;
import com.discord.stores.StoreStream;
import d0.z.d.o;
import j0.k.b;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import rx.Observable;
import rx.functions.Func3;
/* compiled from: StoreReadStates.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\b\u001a&\u0012\f\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004 \u0005*\u0012\u0012\f\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00030\u00032\n\u0010\u0002\u001a\u00060\u0000j\u0002`\u0001H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "Lcom/discord/primitives/ChannelId;", "channelId", "Lrx/Observable;", "Lcom/discord/models/application/Unread$Marker;", "kotlin.jvm.PlatformType", "invoke", "(J)Lrx/Observable;", "getMarker"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreReadStates$computeUnreadMarker$1 extends o implements Function1<Long, Observable<Unread.Marker>> {
    public static final StoreReadStates$computeUnreadMarker$1 INSTANCE = new StoreReadStates$computeUnreadMarker$1();

    public StoreReadStates$computeUnreadMarker$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Observable<Unread.Marker> invoke(Long l) {
        return invoke(l.longValue());
    }

    public final Observable<Unread.Marker> invoke(final long j) {
        StoreStream.Companion companion = StoreStream.Companion;
        return Observable.i(companion.getMessageAck().observeAll().F(new b<Map<Long, ? extends StoreMessageAck.Ack>, StoreMessageAck.Ack>() { // from class: com.discord.stores.StoreReadStates$computeUnreadMarker$1.1
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ StoreMessageAck.Ack call(Map<Long, ? extends StoreMessageAck.Ack> map) {
                return call2((Map<Long, StoreMessageAck.Ack>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final StoreMessageAck.Ack call2(Map<Long, StoreMessageAck.Ack> map) {
                return map.get(Long.valueOf(j));
            }
        }).Z(1), companion.getMessageAck().observeAll().F(new b<Map<Long, ? extends StoreMessageAck.Ack>, StoreMessageAck.Ack>() { // from class: com.discord.stores.StoreReadStates$computeUnreadMarker$1.2
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ StoreMessageAck.Ack call(Map<Long, ? extends StoreMessageAck.Ack> map) {
                return call2((Map<Long, StoreMessageAck.Ack>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final StoreMessageAck.Ack call2(Map<Long, StoreMessageAck.Ack> map) {
                return map.get(Long.valueOf(j));
            }
        }), companion.getMessagesMostRecent().observeRecentMessageIds(j).Z(1), new Func3<StoreMessageAck.Ack, StoreMessageAck.Ack, Long, Unread.Marker>() { // from class: com.discord.stores.StoreReadStates$computeUnreadMarker$1.3
            public final Unread.Marker call(StoreMessageAck.Ack ack, StoreMessageAck.Ack ack2, Long l) {
                if (ack2 != null && ack2.isLockedAck()) {
                    return new Unread.Marker(j, ack2.getMessageId(), l);
                }
                if (ack != null) {
                    return new Unread.Marker(j, ack.getMessageId(), l);
                }
                return new Unread.Marker(j, 0L, l);
            }
        });
    }
}
