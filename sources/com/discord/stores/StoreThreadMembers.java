package com.discord.stores;

import andhook.lib.HookHelper;
import com.discord.api.channel.Channel;
import com.discord.api.guildmember.GuildMember;
import com.discord.api.thread.AugmentedThreadMember;
import com.discord.api.thread.ThreadMembersUpdate;
import com.discord.api.thread.ThreadMetadata;
import com.discord.api.user.User;
import com.discord.stores.updates.ObservationDeck;
import d0.t.g0;
import d0.t.u;
import d0.z.d.m;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: StoreThreadMembers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010%\n\u0002\u0010#\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010 \u001a\u00020\u001f\u0012\u0006\u0010'\u001a\u00020&¢\u0006\u0004\b)\u0010*J)\u0010\b\u001a\u0012\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00070\u00060\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\b\u0010\tJ\u000f\u0010\u000b\u001a\u00020\nH\u0007¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u000f\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\rH\u0007¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u0013\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\u0011H\u0007¢\u0006\u0004\b\u0013\u0010\u0014J\u0017\u0010\u0017\u001a\u00020\n2\u0006\u0010\u0016\u001a\u00020\u0015H\u0007¢\u0006\u0004\b\u0017\u0010\u0018J\u0017\u0010\u0019\u001a\u00020\n2\u0006\u0010\u0016\u001a\u00020\u0015H\u0007¢\u0006\u0004\b\u0019\u0010\u0018J\u000f\u0010\u001a\u001a\u00020\nH\u0017¢\u0006\u0004\b\u001a\u0010\fR0\u0010\u001d\u001a\u001c\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00070\u001c0\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u0016\u0010 \u001a\u00020\u001f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b \u0010!RN\u0010$\u001a:\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00070\u00060\"j\u001c\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00070\u0006`#8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b$\u0010%R\u0016\u0010'\u001a\u00020&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(¨\u0006+"}, d2 = {"Lcom/discord/stores/StoreThreadMembers;", "Lcom/discord/stores/StoreV2;", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lrx/Observable;", "", "Lcom/discord/primitives/UserId;", "observeThreadMembers", "(J)Lrx/Observable;", "", "handleConnectionOpen", "()V", "Lcom/discord/api/thread/ThreadMembersUpdate;", "threadMembersUpdate", "handleThreadMembersUpdate", "(Lcom/discord/api/thread/ThreadMembersUpdate;)V", "Lcom/discord/api/thread/ThreadMemberListUpdate;", "threadMemberListUpdate", "handleThreadMemberListUpdate", "(Lcom/discord/api/thread/ThreadMemberListUpdate;)V", "Lcom/discord/api/channel/Channel;", "channel", "handleThreadCreateOrUpdate", "(Lcom/discord/api/channel/Channel;)V", "handleThreadDelete", "snapshotData", "", "", "memberLists", "Ljava/util/Map;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "memberListsSnapshot", "Ljava/util/HashMap;", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreChannels;", HookHelper.constructorName, "(Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/StoreChannels;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreThreadMembers extends StoreV2 {
    private final Map<Long, Set<Long>> memberLists = new LinkedHashMap();
    private HashMap<Long, Set<Long>> memberListsSnapshot = new HashMap<>();
    private final ObservationDeck observationDeck;
    private final StoreChannels storeChannels;

    public StoreThreadMembers(ObservationDeck observationDeck, StoreChannels storeChannels) {
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(storeChannels, "storeChannels");
        this.observationDeck = observationDeck;
        this.storeChannels = storeChannels;
    }

    @StoreThread
    public final void handleConnectionOpen() {
        this.memberLists.clear();
        markChanged();
    }

    @StoreThread
    public final void handleThreadCreateOrUpdate(Channel channel) {
        ThreadMetadata y2;
        m.checkNotNullParameter(channel, "channel");
        if (this.memberLists.containsKey(Long.valueOf(channel.h())) && (y2 = channel.y()) != null && y2.b()) {
            this.memberLists.remove(Long.valueOf(channel.h()));
            markChanged();
        }
    }

    @StoreThread
    public final void handleThreadDelete(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        if (this.memberLists.containsKey(Long.valueOf(channel.h()))) {
            this.memberLists.remove(Long.valueOf(channel.h()));
            markChanged();
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:14:0x005d, code lost:
        if (r6 != null) goto L16;
     */
    @com.discord.stores.StoreThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void handleThreadMemberListUpdate(com.discord.api.thread.ThreadMemberListUpdate r6) {
        /*
            r5 = this;
            java.lang.String r0 = "threadMemberListUpdate"
            d0.z.d.m.checkNotNullParameter(r6, r0)
            com.discord.stores.StoreChannels r0 = r5.storeChannels
            long r1 = r6.a()
            long r3 = r6.c()
            com.discord.api.channel.Channel r0 = r0.getChannelInternal$app_productionGoogleRelease(r1, r3)
            if (r0 == 0) goto L1f
            long r1 = r0.r()
            java.lang.Long r1 = java.lang.Long.valueOf(r1)
            goto L20
        L1f:
            r1 = 0
        L20:
            if (r1 == 0) goto L6b
            java.util.Map<java.lang.Long, java.util.Set<java.lang.Long>> r1 = r5.memberLists
            long r2 = r0.h()
            java.lang.Long r0 = java.lang.Long.valueOf(r2)
            java.util.List r6 = r6.b()
            if (r6 == 0) goto L60
            java.util.ArrayList r2 = new java.util.ArrayList
            r3 = 10
            int r3 = d0.t.o.collectionSizeOrDefault(r6, r3)
            r2.<init>(r3)
            java.util.Iterator r6 = r6.iterator()
        L41:
            boolean r3 = r6.hasNext()
            if (r3 == 0) goto L59
            java.lang.Object r3 = r6.next()
            com.discord.api.thread.ThreadListMember r3 = (com.discord.api.thread.ThreadListMember) r3
            long r3 = r3.c()
            java.lang.Long r3 = java.lang.Long.valueOf(r3)
            r2.add(r3)
            goto L41
        L59:
            java.util.Set r6 = d0.t.u.toMutableSet(r2)
            if (r6 == 0) goto L60
            goto L65
        L60:
            java.util.LinkedHashSet r6 = new java.util.LinkedHashSet
            r6.<init>()
        L65:
            r1.put(r0, r6)
            r5.markChanged()
        L6b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreThreadMembers.handleThreadMemberListUpdate(com.discord.api.thread.ThreadMemberListUpdate):void");
    }

    @StoreThread
    public final void handleThreadMembersUpdate(ThreadMembersUpdate threadMembersUpdate) {
        User m;
        m.checkNotNullParameter(threadMembersUpdate, "threadMembersUpdate");
        if (this.memberLists.containsKey(Long.valueOf(threadMembersUpdate.c()))) {
            List<AugmentedThreadMember> a = threadMembersUpdate.a();
            if (a != null) {
                for (AugmentedThreadMember augmentedThreadMember : a) {
                    GuildMember c = augmentedThreadMember.c();
                    Long valueOf = (c == null || (m = c.m()) == null) ? null : Long.valueOf(m.i());
                    if (valueOf != null) {
                        Set<Long> set = this.memberLists.get(Long.valueOf(threadMembersUpdate.c()));
                        if (set != null) {
                            set.add(valueOf);
                        }
                        markChanged();
                    }
                }
            }
            List<Long> d = threadMembersUpdate.d();
            if (d != null) {
                for (Number number : d) {
                    long longValue = number.longValue();
                    Set<Long> set2 = this.memberLists.get(Long.valueOf(threadMembersUpdate.c()));
                    if (set2 != null) {
                        set2.remove(Long.valueOf(longValue));
                    }
                    markChanged();
                }
            }
        }
    }

    public final Observable<Set<Long>> observeThreadMembers(long j) {
        Observable<Set<Long>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreThreadMembers$observeThreadMembers$1(this, j), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR…  .distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        super.snapshotData();
        Map<Long, Set<Long>> map = this.memberLists;
        LinkedHashMap linkedHashMap = new LinkedHashMap(g0.mapCapacity(map.size()));
        Iterator<T> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            linkedHashMap.put(entry.getKey(), u.toSet((Iterable) entry.getValue()));
        }
        this.memberListsSnapshot = new HashMap<>(linkedHashMap);
    }
}
