package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.SharedPreferences;
import androidx.appcompat.widget.ActivityChooserModel;
import com.discord.app.AppActivity;
import com.discord.models.domain.Consents;
import com.discord.models.domain.ModelCustomStatusSetting;
import com.discord.models.domain.ModelGuildFolder;
import com.discord.models.domain.ModelPayload;
import com.discord.models.domain.ModelUserSettings;
import com.discord.restapi.RestAPIParams;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.persister.Persister;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.time.TimeUtils;
import d0.d0.f;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import rx.subjects.BehaviorSubject;
import rx.subjects.SerializedSubject;
import xyz.discord.R;
/* compiled from: StoreUserSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u009e\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b(\n\u0002\u0010\u001e\n\u0002\b\u0012\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0012\u0018\u0000 \u009e\u00012\u00020\u0001:\u0002\u009e\u0001B\u001d\u0012\b\u0010\u008e\u0001\u001a\u00030\u008d\u0001\u0012\b\u0010\u0081\u0001\u001a\u00030\u0080\u0001¢\u0006\u0006\b\u009c\u0001\u0010\u009d\u0001J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\b\u0010\u0006J\u000f\u0010\t\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0019\u0010\r\u001a\u00020\u00042\b\u0010\f\u001a\u0004\u0018\u00010\u000bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u000fH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u001d\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00020\u00142\b\b\u0002\u0010\u0013\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u001f\u0010\u001a\u001a\u00020\u00042\b\u0010\u0018\u001a\u0004\u0018\u00010\u00172\u0006\u0010\u0019\u001a\u00020\u0002¢\u0006\u0004\b\u001a\u0010\u001bJ\u001d\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00020\u00142\b\b\u0002\u0010\u0013\u001a\u00020\u0002¢\u0006\u0004\b\u001c\u0010\u0016J\r\u0010\u001d\u001a\u00020\u0002¢\u0006\u0004\b\u001d\u0010\u001eJ\u0015\u0010 \u001a\u00020\u00022\u0006\u0010\u001f\u001a\u00020\u0002¢\u0006\u0004\b \u0010!J\u001d\u0010#\u001a\b\u0012\u0004\u0012\u00020\"0\u00142\b\b\u0002\u0010\u0013\u001a\u00020\u0002¢\u0006\u0004\b#\u0010\u0016J\r\u0010$\u001a\u00020\"¢\u0006\u0004\b$\u0010%J\u001f\u0010'\u001a\u00020\u00042\b\u0010\u0018\u001a\u0004\u0018\u00010\u00172\u0006\u0010&\u001a\u00020\"¢\u0006\u0004\b'\u0010(J\u0013\u0010)\u001a\b\u0012\u0004\u0012\u00020\u00020\u0014¢\u0006\u0004\b)\u0010*J\u001b\u0010+\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00142\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b+\u0010\u0016J\u0013\u0010-\u001a\b\u0012\u0004\u0012\u00020,0\u0014¢\u0006\u0004\b-\u0010*J\u0019\u00100\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020/0.0\u0014¢\u0006\u0004\b0\u0010*J\u0019\u00102\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002010.0\u0014¢\u0006\u0004\b2\u0010*J\u0013\u00103\u001a\b\u0012\u0004\u0012\u00020\"0\u0014¢\u0006\u0004\b3\u0010*J\u0013\u00104\u001a\b\u0012\u0004\u0012\u00020\u00020\u0014¢\u0006\u0004\b4\u0010*J\u0013\u00106\u001a\b\u0012\u0004\u0012\u0002050\u0014¢\u0006\u0004\b6\u0010*J\u0013\u00107\u001a\b\u0012\u0004\u0012\u00020\"0\u0014¢\u0006\u0004\b7\u0010*J\u0013\u00108\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0014¢\u0006\u0004\b8\u0010*J\r\u00109\u001a\u00020\u0002¢\u0006\u0004\b9\u0010\u001eJ\u001f\u0010;\u001a\u00020\u00042\b\u0010\u0018\u001a\u0004\u0018\u00010\u00172\u0006\u0010:\u001a\u00020\u0002¢\u0006\u0004\b;\u0010\u001bJ\r\u0010<\u001a\u00020\u0002¢\u0006\u0004\b<\u0010\u001eJ\u001f\u0010>\u001a\u00020\u00042\b\u0010\u0018\u001a\u0004\u0018\u00010\u00172\u0006\u0010=\u001a\u00020\u0002¢\u0006\u0004\b>\u0010\u001bJ\r\u0010?\u001a\u00020\u0002¢\u0006\u0004\b?\u0010\u001eJ\u001f\u0010A\u001a\u00020\u00042\b\u0010\u0018\u001a\u0004\u0018\u00010\u00172\u0006\u0010@\u001a\u00020\u0002¢\u0006\u0004\bA\u0010\u001bJ\u0013\u0010B\u001a\b\u0012\u0004\u0012\u00020\u00020\u0014¢\u0006\u0004\bB\u0010*J\r\u0010C\u001a\u00020\u0002¢\u0006\u0004\bC\u0010\u001eJ\u0015\u0010E\u001a\u00020\u00042\u0006\u0010D\u001a\u00020\u0002¢\u0006\u0004\bE\u0010\u0006J\r\u0010F\u001a\u00020\u0002¢\u0006\u0004\bF\u0010\u001eJ\u0015\u0010F\u001a\u00020\u00042\u0006\u0010G\u001a\u00020\u0002¢\u0006\u0004\bF\u0010\u0006J\r\u0010H\u001a\u00020\u0002¢\u0006\u0004\bH\u0010\u001eJ\u0015\u0010J\u001a\u00020\u00042\u0006\u0010I\u001a\u00020\u0002¢\u0006\u0004\bJ\u0010\u0006J\r\u0010K\u001a\u00020\u0002¢\u0006\u0004\bK\u0010\u001eJ\u0015\u0010M\u001a\u00020\u00042\u0006\u0010L\u001a\u00020\u0002¢\u0006\u0004\bM\u0010\u0006J\r\u0010N\u001a\u00020\u0002¢\u0006\u0004\bN\u0010\u001eJ\u001f\u0010O\u001a\u00020\u00042\b\u0010\u0018\u001a\u0004\u0018\u00010\u00172\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\bO\u0010\u001bJ\r\u0010P\u001a\u00020\u0002¢\u0006\u0004\bP\u0010\u001eJ\u0015\u0010R\u001a\u00020\u00042\u0006\u0010Q\u001a\u00020\u0002¢\u0006\u0004\bR\u0010\u0006J\u0013\u0010S\u001a\b\u0012\u0004\u0012\u00020\u00020\u0014¢\u0006\u0004\bS\u0010*J\r\u0010T\u001a\u00020\u0002¢\u0006\u0004\bT\u0010\u001eJ\u0015\u0010V\u001a\u00020\u00022\u0006\u0010U\u001a\u00020\u0002¢\u0006\u0004\bV\u0010!J\u0013\u0010W\u001a\b\u0012\u0004\u0012\u00020\u00020\u0014¢\u0006\u0004\bW\u0010*J\u001f\u0010Y\u001a\u00020\u00042\b\u0010\u0018\u001a\u0004\u0018\u00010\u00172\u0006\u0010X\u001a\u00020\u0002¢\u0006\u0004\bY\u0010\u001bJ'\u0010\\\u001a\u00020\u00042\b\u0010\u0018\u001a\u0004\u0018\u00010\u00172\u0006\u0010Z\u001a\u0002012\u0006\u0010[\u001a\u00020\u0002¢\u0006\u0004\b\\\u0010]J7\u0010`\u001a\u00020\u00042\b\u0010\u0018\u001a\u0004\u0018\u00010\u00172\u000e\u0010_\u001a\n\u0012\u0004\u0012\u000201\u0018\u00010^2\u0006\u0010Z\u001a\u0002012\u0006\u0010[\u001a\u00020\u0002¢\u0006\u0004\b`\u0010aJ/\u0010c\u001a\u00020\u00042\b\u0010\u0018\u001a\u0004\u0018\u00010\u00172\u0006\u0010b\u001a\u00020\u00022\u000e\u0010_\u001a\n\u0012\u0004\u0012\u000201\u0018\u00010^¢\u0006\u0004\bc\u0010dJ\u001f\u0010f\u001a\u00020\u00042\b\u0010\u0018\u001a\u0004\u0018\u00010\u00172\u0006\u0010e\u001a\u00020\"¢\u0006\u0004\bf\u0010(J5\u0010j\u001a\u00020\u00042\b\u0010\u0018\u001a\u0004\u0018\u00010\u00172\b\u0010g\u001a\u0004\u0018\u00010\u00022\b\u0010h\u001a\u0004\u0018\u00010\u00022\b\u0010i\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\bj\u0010kJ\u001f\u0010m\u001a\u00020\u00042\b\u0010\u0018\u001a\u0004\u0018\u00010\u00172\u0006\u0010l\u001a\u00020\"¢\u0006\u0004\bm\u0010(J\u001d\u0010n\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00142\b\u0010\f\u001a\u0004\u0018\u00010\u000b¢\u0006\u0004\bn\u0010oJ\r\u0010p\u001a\u00020\u0004¢\u0006\u0004\bp\u0010\nJ\u0017\u0010s\u001a\u00020\u00042\u0006\u0010r\u001a\u00020qH\u0017¢\u0006\u0004\bs\u0010tJ\u0015\u0010w\u001a\u00020\u00042\u0006\u0010v\u001a\u00020u¢\u0006\u0004\bw\u0010xJ\u0017\u0010y\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u000fH\u0007¢\u0006\u0004\by\u0010\u0012R$\u0010z\u001a\u0004\u0018\u00010\u00028\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\bz\u0010{\u001a\u0004\b|\u0010}\"\u0004\b~\u0010\u007fR\u001a\u0010\u0081\u0001\u001a\u00030\u0080\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0081\u0001\u0010\u0082\u0001R \u0010\u0084\u0001\u001a\t\u0012\u0004\u0012\u00020\u00020\u0083\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0084\u0001\u0010\u0085\u0001R\u001c\u0010\u0087\u0001\u001a\u0005\u0018\u00010\u0086\u00018\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u0087\u0001\u0010\u0088\u0001R8\u0010\u008b\u0001\u001a!\u0012\r\u0012\u000b \u008a\u0001*\u0004\u0018\u00010505\u0012\r\u0012\u000b \u008a\u0001*\u0004\u0018\u000105050\u0089\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u008b\u0001\u0010\u008c\u0001R\u001a\u0010\u008e\u0001\u001a\u00030\u008d\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u008e\u0001\u0010\u008f\u0001R&\u0010\u0090\u0001\u001a\u000f\u0012\n\u0012\b\u0012\u0004\u0012\u0002010.0\u0083\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0090\u0001\u0010\u0085\u0001R \u0010\u0091\u0001\u001a\t\u0012\u0004\u0012\u00020\u00020\u0083\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0091\u0001\u0010\u0085\u0001R \u0010\u0092\u0001\u001a\t\u0012\u0004\u0012\u00020\u00020\u0083\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0092\u0001\u0010\u0085\u0001R \u0010\u0093\u0001\u001a\t\u0012\u0004\u0012\u00020\u00020\u0083\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0093\u0001\u0010\u0085\u0001R8\u0010\u0094\u0001\u001a!\u0012\r\u0012\u000b \u008a\u0001*\u0004\u0018\u00010\u000b0\u000b\u0012\r\u0012\u000b \u008a\u0001*\u0004\u0018\u00010\u000b0\u000b0\u0089\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0094\u0001\u0010\u008c\u0001R \u0010\u0095\u0001\u001a\t\u0012\u0004\u0012\u00020\u00020\u0083\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0095\u0001\u0010\u0085\u0001R \u0010\u0096\u0001\u001a\t\u0012\u0004\u0012\u00020\"0\u0083\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0096\u0001\u0010\u0085\u0001R8\u0010\u0097\u0001\u001a!\u0012\r\u0012\u000b \u008a\u0001*\u0004\u0018\u00010\"0\"\u0012\r\u0012\u000b \u008a\u0001*\u0004\u0018\u00010\"0\"0\u0089\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0097\u0001\u0010\u008c\u0001R8\u0010\u0098\u0001\u001a!\u0012\r\u0012\u000b \u008a\u0001*\u0004\u0018\u00010\u00020\u0002\u0012\r\u0012\u000b \u008a\u0001*\u0004\u0018\u00010\u00020\u00020\u0089\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0098\u0001\u0010\u008c\u0001R&\u0010\u0099\u0001\u001a\u000f\u0012\n\u0012\b\u0012\u0004\u0012\u00020/0.0\u0083\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0099\u0001\u0010\u0085\u0001R8\u0010\u009a\u0001\u001a!\u0012\r\u0012\u000b \u008a\u0001*\u0004\u0018\u00010\"0\"\u0012\r\u0012\u000b \u008a\u0001*\u0004\u0018\u00010\"0\"0\u0089\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u009a\u0001\u0010\u008c\u0001R8\u0010\u009b\u0001\u001a!\u0012\r\u0012\u000b \u008a\u0001*\u0004\u0018\u00010\u00020\u0002\u0012\r\u0012\u000b \u008a\u0001*\u0004\u0018\u00010\u00020\u00020\u0089\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u009b\u0001\u0010\u008c\u0001¨\u0006\u009f\u0001"}, d2 = {"Lcom/discord/stores/StoreUserSettings;", "Lcom/discord/stores/Store;", "", "allowAccessibilityDetection", "", "updateAllowAccessibilityDetectionInternal", "(Z)V", "isDeveloperMode", "setDeveloperModeInternal", "expireCustomStatus", "()V", "Lcom/discord/models/domain/ModelCustomStatusSetting;", "customStatus", "updateLocalCustomStatus", "(Lcom/discord/models/domain/ModelCustomStatusSetting;)V", "Lcom/discord/models/domain/ModelUserSettings;", "userSettings", "handleUserSettings", "(Lcom/discord/models/domain/ModelUserSettings;)V", "respectReducedMotion", "Lrx/Observable;", "observeIsAnimatedEmojisEnabled", "(Z)Lrx/Observable;", "Lcom/discord/app/AppActivity;", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "isAnimatedEmojisEnabled", "setIsAnimatedEmojisEnabled", "(Lcom/discord/app/AppActivity;Z)V", "observeIsAutoPlayGifsEnabled", "getIsAutoPlayGifsEnabled", "()Z", "isAutoPlayGifsEnabled", "setIsAutoPlayGifsEnabled", "(Z)Z", "", "observeStickerAnimationSettings", "getStickerAnimationSettings", "()I", "stickerAnimationSettings", "setStickerAnimationSettings", "(Lcom/discord/app/AppActivity;I)V", "observeIsAccessibilityDetectionAllowed", "()Lrx/Observable;", "setIsAccessibilityDetectionAllowed", "Lcom/discord/models/domain/Consents;", "observeConsents", "", "Lcom/discord/models/domain/ModelGuildFolder;", "observeGuildFolders", "", "observeRestrictedGuildIds", "observeExplicitContentFilter", "observeIsDefaultGuildsRestricted", "Lcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;", "observeFriendSourceFlags", "observeFriendDiscoveryFlags", "observeCustomStatus", "getIsEmbedMediaInlined", "isInlineEmbedMediaEnabled", "setIsEmbedMediaInlined", "getIsAttachmentMediaInline", "isAttachmentMediaInline", "setIsAttachmentMediaInline", "getIsRenderEmbedsEnabled", "isRenderEmbedsEnabled", "setIsRenderEmbedsEnabled", "observeIsRenderEmbedsEnabled", "getIsAutoImageCompressionEnabled", "isAutoImageCompressionEnabled", "setIsAutoImageCompressionEnabled", "getIsSyncTextAndImagesEnabled", "isSyncTextAndImagesEnabled", "getIsMobileOverlayEnabled", "isMobileOverlayEnabled", "setIsMobileOverlayEnabled", "getIsShiftEnterToSendEnabled", "isShiftEnterToSendEnabled", "setIsShiftEnterToSendEnabled", "getIsDeveloperMode", "setIsDeveloperMode", "getIsChromeCustomTabsEnabled", "isUseChromeCustomTabsEnabled", "setIsChromeCustomTabsEnabled", "observeIsStickerSuggestionsEnabled", "getIsStickerSuggestionsEnabled", "enabled", "setIsStickerSuggestionsEnabled", "observeIsShowCurrentGameEnabled", "isShowCurrentGameEnabled", "setIsShowCurrentGameEnabled", "guildId", "restricted", "setRestrictedGuildId", "(Lcom/discord/app/AppActivity;JZ)V", "", "restrictedGuilds", "setRestrictedGuildIds", "(Lcom/discord/app/AppActivity;Ljava/util/Collection;JZ)V", "defaultGuildsRestricted", "setDefaultGuildsRestricted", "(Lcom/discord/app/AppActivity;ZLjava/util/Collection;)V", "explicitContentFilter", "setExplicitContentFilter", "all", "mutualGuilds", "mutualFriends", "setFriendSourceFlags", "(Lcom/discord/app/AppActivity;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V", "flags", "setFriendDiscoveryFlags", "updateCustomStatus", "(Lcom/discord/models/domain/ModelCustomStatusSetting;)Lrx/Observable;", "updateContactSyncShown", "Landroid/content/Context;", "context", "init", "(Landroid/content/Context;)V", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "handleUserSettingsUpdate", "contactSyncUpsellShown", "Ljava/lang/Boolean;", "getContactSyncUpsellShown", "()Ljava/lang/Boolean;", "setContactSyncUpsellShown", "(Ljava/lang/Boolean;)V", "Lcom/discord/stores/StoreAccessibility;", "accessibilityStore", "Lcom/discord/stores/StoreAccessibility;", "Lcom/discord/utilities/persister/Persister;", "stickerSuggestionsPublisher", "Lcom/discord/utilities/persister/Persister;", "Lrx/Subscription;", "expireCustomStatusSubscription", "Lrx/Subscription;", "Lrx/subjects/SerializedSubject;", "kotlin.jvm.PlatformType", "friendSourceFlagsSubject", "Lrx/subjects/SerializedSubject;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "restrictedGuildIdsPublisher", "autoPlayGifsPublisher", "showCurrentGame", "allowAccessibilityDetectionPublisher", "customStatusSubject", "allowAnimatedEmojisPublisher", "stickerAnimationSettingsPublisher", "explicitContentFilterSubject", "shouldRenderEmbedsSubject", "guildFoldersPublisher", "friendDiscoveryFlagsSubject", "defaultGuildsRestrictedSubject", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreAccessibility;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserSettings extends Store {
    public static final Companion Companion = new Companion(null);
    private final StoreAccessibility accessibilityStore;
    private final Persister<Boolean> allowAnimatedEmojisPublisher;
    private final Persister<Boolean> autoPlayGifsPublisher;
    private Boolean contactSyncUpsellShown;
    private final Dispatcher dispatcher;
    private Subscription expireCustomStatusSubscription;
    private final Persister<Boolean> showCurrentGame;
    private final Persister<Boolean> stickerSuggestionsPublisher;
    private final SerializedSubject<Boolean, Boolean> shouldRenderEmbedsSubject = new SerializedSubject<>(BehaviorSubject.k0());
    private final SerializedSubject<Integer, Integer> explicitContentFilterSubject = new SerializedSubject<>(BehaviorSubject.k0());
    private final SerializedSubject<Boolean, Boolean> defaultGuildsRestrictedSubject = new SerializedSubject<>(BehaviorSubject.k0());
    private final SerializedSubject<ModelUserSettings.FriendSourceFlags, ModelUserSettings.FriendSourceFlags> friendSourceFlagsSubject = new SerializedSubject<>(BehaviorSubject.k0());
    private final SerializedSubject<Integer, Integer> friendDiscoveryFlagsSubject = new SerializedSubject<>(BehaviorSubject.l0(0));
    private final SerializedSubject<ModelCustomStatusSetting, ModelCustomStatusSetting> customStatusSubject = new SerializedSubject<>(BehaviorSubject.k0());
    private final Persister<List<Long>> restrictedGuildIdsPublisher = new Persister<>("RESTRICTED_GUILD_IDS", new ArrayList());
    private final Persister<List<ModelGuildFolder>> guildFoldersPublisher = new Persister<>("STORE_SETTINGS_FOLDERS_V1", new ArrayList());
    private final Persister<Integer> stickerAnimationSettingsPublisher = new Persister<>("CACHE_KEY_STICKER_ANIMATION_SETTINGS_V1", 0);
    private final Persister<Boolean> allowAccessibilityDetectionPublisher = new Persister<>("STORE_SETTINGS_ALLOW_ACCESSIBILITY_DETECTION", Boolean.FALSE);

    /* compiled from: StoreUserSettings.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bJ'\u0010\b\u001a\u00020\u0007*\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00032\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0002¢\u0006\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/stores/StoreUserSettings$Companion;", "", "Lcom/discord/app/AppActivity;", "Lcom/discord/restapi/RestAPIParams$UserSettings;", "settings", "", "successStringResId", "", "updateUserSettings", "(Lcom/discord/app/AppActivity;Lcom/discord/restapi/RestAPIParams$UserSettings;Ljava/lang/Integer;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final void updateUserSettings(AppActivity appActivity, RestAPIParams.UserSettings userSettings, Integer num) {
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().updateUserSettings(userSettings), false, 1, null), appActivity, null, 2, null), StoreUserSettings.class, (r18 & 2) != 0 ? null : appActivity, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreUserSettings$Companion$updateUserSettings$1(appActivity, num));
        }

        public static /* synthetic */ void updateUserSettings$default(Companion companion, AppActivity appActivity, RestAPIParams.UserSettings userSettings, Integer num, int i, Object obj) {
            if ((i & 2) != 0) {
                num = null;
            }
            companion.updateUserSettings(appActivity, userSettings, num);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public StoreUserSettings(Dispatcher dispatcher, StoreAccessibility storeAccessibility) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(storeAccessibility, "accessibilityStore");
        this.dispatcher = dispatcher;
        this.accessibilityStore = storeAccessibility;
        Boolean bool = Boolean.TRUE;
        this.allowAnimatedEmojisPublisher = new Persister<>("STORE_SETTINGS_ALLOW_ANIMATED_EMOJIS", bool);
        this.showCurrentGame = new Persister<>("STORE_SETTINGS_ALLOW_GAME_STATUS", bool);
        this.stickerSuggestionsPublisher = new Persister<>("CACHE_KEY_STICKER_SUGGESTIONS", bool);
        this.autoPlayGifsPublisher = new Persister<>("STORE_SETTINGS_AUTO_PLAY_GIFS", bool);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void expireCustomStatus() {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(updateCustomStatus(null), false, 1, null), StoreUserSettings.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, StoreUserSettings$expireCustomStatus$1.INSTANCE);
    }

    private final void handleUserSettings(ModelUserSettings modelUserSettings) {
        List<ModelGuildFolder> guildFolders = modelUserSettings.getGuildFolders();
        List<Long> restrictedGuilds = modelUserSettings.getRestrictedGuilds();
        if (getIsSyncTextAndImagesEnabled()) {
            if (modelUserSettings.getInlineEmbedMedia() != null) {
                Boolean inlineEmbedMedia = modelUserSettings.getInlineEmbedMedia();
                m.checkNotNullExpressionValue(inlineEmbedMedia, "userSettings.inlineEmbedMedia");
                setIsEmbedMediaInlined(null, inlineEmbedMedia.booleanValue());
            }
            if (modelUserSettings.getInlineAttachmentMedia() != null) {
                Boolean inlineAttachmentMedia = modelUserSettings.getInlineAttachmentMedia();
                m.checkNotNullExpressionValue(inlineAttachmentMedia, "userSettings.inlineAttachmentMedia");
                setIsAttachmentMediaInline(null, inlineAttachmentMedia.booleanValue());
            }
            if (modelUserSettings.getRenderEmbeds() != null) {
                Boolean renderEmbeds = modelUserSettings.getRenderEmbeds();
                m.checkNotNullExpressionValue(renderEmbeds, "userSettings.renderEmbeds");
                setIsRenderEmbedsEnabled(null, renderEmbeds.booleanValue());
            }
            if (modelUserSettings.getAnimateEmoji() != null) {
                Boolean animateEmoji = modelUserSettings.getAnimateEmoji();
                m.checkNotNullExpressionValue(animateEmoji, "userSettings.animateEmoji");
                setIsAnimatedEmojisEnabled(null, animateEmoji.booleanValue());
            }
            if (modelUserSettings.getAnimateStickers() != null) {
                Integer animateStickers = modelUserSettings.getAnimateStickers();
                m.checkNotNullExpressionValue(animateStickers, "userSettings.animateStickers");
                setStickerAnimationSettings(null, animateStickers.intValue());
            }
        }
        if (guildFolders != null) {
            Persister.set$default(this.guildFoldersPublisher, guildFolders, false, 2, null);
        }
        if (restrictedGuilds != null) {
            Persister.set$default(this.restrictedGuildIdsPublisher, restrictedGuilds, false, 2, null);
        }
        if (modelUserSettings.getDeveloperMode() != null) {
            Boolean developerMode = modelUserSettings.getDeveloperMode();
            m.checkNotNullExpressionValue(developerMode, "userSettings.developerMode");
            setDeveloperModeInternal(developerMode.booleanValue());
        }
        if (modelUserSettings.getShowCurrentGame() != null) {
            Persister<Boolean> persister = this.showCurrentGame;
            Boolean showCurrentGame = modelUserSettings.getShowCurrentGame();
            m.checkNotNullExpressionValue(showCurrentGame, "userSettings.showCurrentGame");
            Persister.set$default(persister, showCurrentGame, false, 2, null);
        }
        if (modelUserSettings.getExplicitContentFilter() != null) {
            SerializedSubject<Integer, Integer> serializedSubject = this.explicitContentFilterSubject;
            serializedSubject.k.onNext(modelUserSettings.getExplicitContentFilter());
        }
        if (modelUserSettings.getFriendSourceFlags() != null) {
            SerializedSubject<ModelUserSettings.FriendSourceFlags, ModelUserSettings.FriendSourceFlags> serializedSubject2 = this.friendSourceFlagsSubject;
            serializedSubject2.k.onNext(modelUserSettings.getFriendSourceFlags());
        }
        if (modelUserSettings.getDefaultGuildsRestricted() != null) {
            SerializedSubject<Boolean, Boolean> serializedSubject3 = this.defaultGuildsRestrictedSubject;
            serializedSubject3.k.onNext(modelUserSettings.getDefaultGuildsRestricted());
        }
        if (modelUserSettings.getCustomStatus() != null) {
            updateLocalCustomStatus(modelUserSettings.getCustomStatus());
        }
        if (modelUserSettings.getFriendDiscoveryFlags() != null) {
            SerializedSubject<Integer, Integer> serializedSubject4 = this.friendDiscoveryFlagsSubject;
            serializedSubject4.k.onNext(modelUserSettings.getFriendDiscoveryFlags());
        }
        if (modelUserSettings.getContactSyncUpsellShown() != null) {
            this.contactSyncUpsellShown = modelUserSettings.getContactSyncUpsellShown();
        }
    }

    public static /* synthetic */ Observable observeIsAnimatedEmojisEnabled$default(StoreUserSettings storeUserSettings, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = true;
        }
        return storeUserSettings.observeIsAnimatedEmojisEnabled(z2);
    }

    public static /* synthetic */ Observable observeIsAutoPlayGifsEnabled$default(StoreUserSettings storeUserSettings, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = true;
        }
        return storeUserSettings.observeIsAutoPlayGifsEnabled(z2);
    }

    public static /* synthetic */ Observable observeStickerAnimationSettings$default(StoreUserSettings storeUserSettings, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = true;
        }
        return storeUserSettings.observeStickerAnimationSettings(z2);
    }

    private final void setDeveloperModeInternal(boolean z2) {
        getPrefs().edit().putBoolean("CACHE_KEY_DEVELOPER_MODE", z2).apply();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateAllowAccessibilityDetectionInternal(boolean z2) {
        Persister.set$default(this.allowAccessibilityDetectionPublisher, Boolean.valueOf(z2), false, 2, null);
        this.dispatcher.schedule(new StoreUserSettings$updateAllowAccessibilityDetectionInternal$1(this, z2));
    }

    private final synchronized void updateLocalCustomStatus(ModelCustomStatusSetting modelCustomStatusSetting) {
        Subscription subscription = this.expireCustomStatusSubscription;
        if (subscription != null) {
            m.checkNotNull(subscription);
            subscription.unsubscribe();
        }
        this.customStatusSubject.k.onNext(modelCustomStatusSetting);
        if (!m.areEqual(modelCustomStatusSetting, ModelCustomStatusSetting.Companion.getCLEAR())) {
            m.checkNotNull(modelCustomStatusSetting);
            if (modelCustomStatusSetting.getExpiresAt() != null) {
                Observable<Long> d02 = Observable.d0(f.coerceAtLeast(TimeUtils.parseUTCDate(modelCustomStatusSetting.getExpiresAt()) - ClockFactory.get().currentTimeMillis(), 0L), TimeUnit.MILLISECONDS);
                m.checkNotNullExpressionValue(d02, "Observable\n            .…l, TimeUnit.MILLISECONDS)");
                ObservableExtensionsKt.appSubscribe(d02, getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new StoreUserSettings$updateLocalCustomStatus$1(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreUserSettings$updateLocalCustomStatus$2(this));
            }
        }
    }

    public final Boolean getContactSyncUpsellShown() {
        return this.contactSyncUpsellShown;
    }

    public final boolean getIsAttachmentMediaInline() {
        return getPrefs().getBoolean("CACHE_KEY_INLINE_ATTACHMENT_MEDIA", true);
    }

    public final boolean getIsAutoImageCompressionEnabled() {
        return getPrefs().getBoolean("CACHE_KEY_IMAGE_COMPRESSION", false);
    }

    public final boolean getIsAutoPlayGifsEnabled() {
        return getPrefs().getBoolean("STORE_SETTINGS_AUTO_PLAY_GIFS", true);
    }

    public final boolean getIsChromeCustomTabsEnabled() {
        return getPrefs().getBoolean("CACHE_KEY_USE_CHROME_CUSTOM_TABS", true);
    }

    public final boolean getIsDeveloperMode() {
        return getPrefs().getBoolean("CACHE_KEY_DEVELOPER_MODE", false);
    }

    public final boolean getIsEmbedMediaInlined() {
        return getPrefs().getBoolean("CACHE_KEY_INLINE_EMBED_MEDIA", true);
    }

    public final boolean getIsMobileOverlayEnabled() {
        return getPrefs().getBoolean("CACHE_KEY_MOBILE_OVERLAY", false);
    }

    public final boolean getIsRenderEmbedsEnabled() {
        return getPrefs().getBoolean("CACHE_KEY_RENDER_EMBEDS", true);
    }

    public final boolean getIsShiftEnterToSendEnabled() {
        return getPrefs().getBoolean("CACHE_KEY_SHIFT_ENTER_TO_SEND", false);
    }

    public final boolean getIsStickerSuggestionsEnabled() {
        return this.stickerSuggestionsPublisher.get().booleanValue();
    }

    public final boolean getIsSyncTextAndImagesEnabled() {
        return getPrefs().getBoolean("CACHE_KEY_SYNC_TEXT_AND_IMAGES", true);
    }

    public final int getStickerAnimationSettings() {
        return this.stickerAnimationSettingsPublisher.get().intValue();
    }

    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        ModelUserSettings userSettings = modelPayload.getUserSettings();
        m.checkNotNullExpressionValue(userSettings, "userSettings");
        handleUserSettings(userSettings);
    }

    @StoreThread
    public final void handleUserSettingsUpdate(ModelUserSettings modelUserSettings) {
        m.checkNotNullParameter(modelUserSettings, "userSettings");
        handleUserSettings(modelUserSettings);
    }

    @Override // com.discord.stores.Store
    @StoreThread
    public void init(Context context) {
        m.checkNotNullParameter(context, "context");
        super.init(context);
        SerializedSubject<Boolean, Boolean> serializedSubject = this.shouldRenderEmbedsSubject;
        serializedSubject.k.onNext(Boolean.valueOf(getIsRenderEmbedsEnabled()));
    }

    public final Observable<Consents> observeConsents() {
        Observable<Consents> L = RestAPI.Companion.getApi().getConsents().L(StoreUserSettings$observeConsents$1.INSTANCE);
        m.checkNotNullExpressionValue(L, "RestAPI\n          .api\n …turn { Consents.DEFAULT }");
        return ObservableExtensionsKt.restSubscribeOn$default(L, false, 1, null);
    }

    public final Observable<ModelCustomStatusSetting> observeCustomStatus() {
        Observable<ModelCustomStatusSetting> q = ObservableExtensionsKt.computationLatest(this.customStatusSubject).q();
        m.checkNotNullExpressionValue(q, "customStatusSubject\n    …  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Integer> observeExplicitContentFilter() {
        Observable<Integer> q = ObservableExtensionsKt.computationLatest(this.explicitContentFilterSubject).q();
        m.checkNotNullExpressionValue(q, "explicitContentFilterSub…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Integer> observeFriendDiscoveryFlags() {
        Observable<Integer> q = ObservableExtensionsKt.computationLatest(this.friendDiscoveryFlagsSubject).q();
        m.checkNotNullExpressionValue(q, "friendDiscoveryFlagsSubj…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<ModelUserSettings.FriendSourceFlags> observeFriendSourceFlags() {
        Observable<ModelUserSettings.FriendSourceFlags> q = ObservableExtensionsKt.computationLatest(this.friendSourceFlagsSubject).q();
        m.checkNotNullExpressionValue(q, "friendSourceFlagsSubject…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<List<ModelGuildFolder>> observeGuildFolders() {
        Observable<List<ModelGuildFolder>> q = ObservableExtensionsKt.computationLatest(this.guildFoldersPublisher.getObservable()).q();
        m.checkNotNullExpressionValue(q, "guildFoldersPublisher\n  …  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Boolean> observeIsAccessibilityDetectionAllowed() {
        Observable<Boolean> q = this.allowAccessibilityDetectionPublisher.getObservable().q();
        m.checkNotNullExpressionValue(q, "allowAccessibilityDetect…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Boolean> observeIsAnimatedEmojisEnabled(boolean z2) {
        Observable<Boolean> observable = this.allowAnimatedEmojisPublisher.getObservable();
        if (z2) {
            observable = Observable.j(observable, this.accessibilityStore.observeReducedMotionEnabled(), StoreUserSettings$observeIsAnimatedEmojisEnabled$1$1.INSTANCE);
        }
        Observable<Boolean> q = observable.q();
        m.checkNotNullExpressionValue(q, "allowAnimatedEmojisPubli…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Boolean> observeIsAutoPlayGifsEnabled(boolean z2) {
        Observable<Boolean> observable = this.autoPlayGifsPublisher.getObservable();
        if (z2) {
            observable = Observable.j(observable, this.accessibilityStore.observeReducedMotionEnabled(), StoreUserSettings$observeIsAutoPlayGifsEnabled$1$1.INSTANCE);
        }
        Observable<Boolean> q = observable.q();
        m.checkNotNullExpressionValue(q, "autoPlayGifsPublisher\n  …  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Boolean> observeIsDefaultGuildsRestricted() {
        Observable<Boolean> q = ObservableExtensionsKt.computationLatest(this.defaultGuildsRestrictedSubject).q();
        m.checkNotNullExpressionValue(q, "defaultGuildsRestrictedS…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Boolean> observeIsRenderEmbedsEnabled() {
        Observable<Boolean> q = ObservableExtensionsKt.computationLatest(this.shouldRenderEmbedsSubject).q();
        m.checkNotNullExpressionValue(q, "shouldRenderEmbedsSubjec…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Boolean> observeIsShowCurrentGameEnabled() {
        return this.showCurrentGame.getObservable();
    }

    public final Observable<Boolean> observeIsStickerSuggestionsEnabled() {
        return this.stickerSuggestionsPublisher.getObservable();
    }

    public final Observable<List<Long>> observeRestrictedGuildIds() {
        Observable<List<Long>> q = ObservableExtensionsKt.computationLatest(this.restrictedGuildIdsPublisher.getObservable()).q();
        m.checkNotNullExpressionValue(q, "restrictedGuildIdsPublis…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Integer> observeStickerAnimationSettings(boolean z2) {
        Observable<Integer> observable = this.stickerAnimationSettingsPublisher.getObservable();
        if (z2) {
            observable = Observable.j(observable, this.accessibilityStore.observeReducedMotionEnabled(), StoreUserSettings$observeStickerAnimationSettings$1$1.INSTANCE);
        }
        Observable<Integer> q = observable.q();
        m.checkNotNullExpressionValue(q, "stickerAnimationSettings…  .distinctUntilChanged()");
        return q;
    }

    public final void setContactSyncUpsellShown(Boolean bool) {
        this.contactSyncUpsellShown = bool;
    }

    public final void setDefaultGuildsRestricted(AppActivity appActivity, boolean z2, Collection<Long> collection) {
        if (appActivity != null) {
            Companion.updateUserSettings$default(Companion, appActivity, RestAPIParams.UserSettings.Companion.createWithRestrictedGuilds(Boolean.valueOf(z2), collection), null, 2, null);
        }
    }

    public final void setExplicitContentFilter(AppActivity appActivity, int i) {
        if (appActivity != null) {
            Companion.updateUserSettings$default(Companion, appActivity, RestAPIParams.UserSettings.Companion.createWithExplicitContentFilter(i), null, 2, null);
        }
    }

    public final void setFriendDiscoveryFlags(AppActivity appActivity, int i) {
        if (appActivity != null) {
            Companion.updateUserSettings$default(Companion, appActivity, RestAPIParams.UserSettings.Companion.createWithFriendDiscoveryFlags(Integer.valueOf(i)), null, 2, null);
        }
    }

    public final void setFriendSourceFlags(AppActivity appActivity, Boolean bool, Boolean bool2, Boolean bool3) {
        if (appActivity != null) {
            Companion.updateUserSettings$default(Companion, appActivity, RestAPIParams.UserSettings.Companion.createWithFriendSourceFlags(bool, bool2, bool3), null, 2, null);
        }
    }

    public final Observable<ModelUserSettings> setIsAccessibilityDetectionAllowed(final boolean z2) {
        Observable<ModelUserSettings> t = RestAPI.Companion.getApi().updateUserSettings(RestAPIParams.UserSettings.Companion.createWithAllowAccessibilityDetection(Boolean.valueOf(z2))).t(new Action1<ModelUserSettings>() { // from class: com.discord.stores.StoreUserSettings$setIsAccessibilityDetectionAllowed$1
            public final void call(ModelUserSettings modelUserSettings) {
                StoreUserSettings.this.updateAllowAccessibilityDetectionInternal(z2);
            }
        });
        m.checkNotNullExpressionValue(t, "RestAPI\n          .api\n …AccessibilityDetection) }");
        return t;
    }

    public final void setIsAnimatedEmojisEnabled(AppActivity appActivity, boolean z2) {
        Persister.set$default(this.allowAnimatedEmojisPublisher, Boolean.valueOf(z2), false, 2, null);
        if (getIsSyncTextAndImagesEnabled() && appActivity != null) {
            Companion.updateUserSettings$default(Companion, appActivity, RestAPIParams.UserSettings.Companion.createWithAllowAnimatedEmojis(Boolean.valueOf(z2)), null, 2, null);
        }
    }

    public final void setIsAttachmentMediaInline(AppActivity appActivity, boolean z2) {
        if (getIsAttachmentMediaInline() != z2) {
            getPrefs().edit().putBoolean("CACHE_KEY_INLINE_ATTACHMENT_MEDIA", z2).apply();
        }
        if (getIsSyncTextAndImagesEnabled() && appActivity != null) {
            Companion.updateUserSettings$default(Companion, appActivity, RestAPIParams.UserSettings.Companion.createWithInlineAttachmentMedia(z2), null, 2, null);
        }
    }

    public final void setIsAutoImageCompressionEnabled(boolean z2) {
        if (getIsAutoImageCompressionEnabled() != z2) {
            getPrefs().edit().putBoolean("CACHE_KEY_IMAGE_COMPRESSION", z2).apply();
        }
    }

    public final boolean setIsAutoPlayGifsEnabled(boolean z2) {
        return ((Boolean) Persister.set$default(this.autoPlayGifsPublisher, Boolean.valueOf(z2), false, 2, null)).booleanValue();
    }

    public final void setIsChromeCustomTabsEnabled(boolean z2) {
        SharedPreferences.Editor edit = getPrefs().edit();
        m.checkNotNullExpressionValue(edit, "editor");
        edit.putBoolean("CACHE_KEY_USE_CHROME_CUSTOM_TABS", z2);
        edit.apply();
    }

    public final void setIsDeveloperMode(AppActivity appActivity, boolean z2) {
        if (getIsDeveloperMode() != z2) {
            if (appActivity != null) {
                Companion.updateUserSettings(appActivity, RestAPIParams.UserSettings.Companion.createWithDeveloperMode(z2), Integer.valueOf((int) R.string.theme_updated));
            }
            setDeveloperModeInternal(z2);
        }
    }

    public final void setIsEmbedMediaInlined(AppActivity appActivity, boolean z2) {
        if (getIsEmbedMediaInlined() != z2) {
            getPrefs().edit().putBoolean("CACHE_KEY_INLINE_EMBED_MEDIA", z2).apply();
        }
        if (getIsSyncTextAndImagesEnabled() && appActivity != null) {
            Companion.updateUserSettings$default(Companion, appActivity, RestAPIParams.UserSettings.Companion.createWithInlineEmbedMedia(z2), null, 2, null);
        }
    }

    public final void setIsMobileOverlayEnabled(boolean z2) {
        SharedPreferences.Editor edit = getPrefs().edit();
        m.checkNotNullExpressionValue(edit, "editor");
        if (getIsMobileOverlayEnabled() != z2) {
            edit.putBoolean("CACHE_KEY_MOBILE_OVERLAY", z2);
            AnalyticsTracker.overlayToggled(z2);
        }
        edit.apply();
    }

    public final void setIsRenderEmbedsEnabled(AppActivity appActivity, boolean z2) {
        if (getIsRenderEmbedsEnabled() != z2) {
            getPrefs().edit().putBoolean("CACHE_KEY_RENDER_EMBEDS", z2).apply();
            SerializedSubject<Boolean, Boolean> serializedSubject = this.shouldRenderEmbedsSubject;
            serializedSubject.k.onNext(Boolean.valueOf(z2));
        }
        if (getIsSyncTextAndImagesEnabled() && appActivity != null) {
            Companion.updateUserSettings$default(Companion, appActivity, RestAPIParams.UserSettings.Companion.createWithRenderEmbeds(z2), null, 2, null);
        }
    }

    public final void setIsShiftEnterToSendEnabled(boolean z2) {
        SharedPreferences.Editor edit = getPrefs().edit();
        m.checkNotNullExpressionValue(edit, "editor");
        if (getIsShiftEnterToSendEnabled() != z2) {
            edit.putBoolean("CACHE_KEY_SHIFT_ENTER_TO_SEND", z2);
        }
        edit.apply();
    }

    public final void setIsShowCurrentGameEnabled(AppActivity appActivity, boolean z2) {
        Persister.set$default(this.showCurrentGame, Boolean.valueOf(z2), false, 2, null);
        if (appActivity != null) {
            Companion.updateUserSettings$default(Companion, appActivity, RestAPIParams.UserSettings.Companion.createWithShowCurrentGame(z2), null, 2, null);
        }
    }

    public final boolean setIsStickerSuggestionsEnabled(boolean z2) {
        return this.stickerSuggestionsPublisher.set(Boolean.valueOf(z2), true).booleanValue();
    }

    public final void setRestrictedGuildId(AppActivity appActivity, long j, boolean z2) {
        setRestrictedGuildIds(appActivity, this.restrictedGuildIdsPublisher.get(), j, z2);
    }

    public final void setRestrictedGuildIds(AppActivity appActivity, Collection<Long> collection, long j, boolean z2) {
        ArrayList arrayList = new ArrayList(collection);
        if (z2 && !arrayList.contains(Long.valueOf(j))) {
            arrayList.add(Long.valueOf(j));
        }
        if (!z2) {
            arrayList.remove(Long.valueOf(j));
        }
        if (appActivity != null) {
            Companion.updateUserSettings$default(Companion, appActivity, RestAPIParams.UserSettings.Companion.createWithRestrictedGuilds(null, arrayList), null, 2, null);
        }
    }

    public final void setStickerAnimationSettings(AppActivity appActivity, int i) {
        Persister.set$default(this.stickerAnimationSettingsPublisher, Integer.valueOf(i), false, 2, null);
        if (getIsSyncTextAndImagesEnabled() && appActivity != null) {
            Companion.updateUserSettings$default(Companion, appActivity, RestAPIParams.UserSettings.Companion.createWithStickerAnimationSettings(Integer.valueOf(i)), null, 2, null);
        }
    }

    public final void updateContactSyncShown() {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().updateUserSettings(RestAPIParams.UserSettings.Companion.createWithContactSyncUpsellShown()), false, 1, null), StoreUserSettings.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, StoreUserSettings$updateContactSyncShown$1.INSTANCE);
    }

    public final Observable<ModelUserSettings> updateCustomStatus(ModelCustomStatusSetting modelCustomStatusSetting) {
        return RestAPI.Companion.getApiSerializeNulls().updateUserSettingsCustomStatus(new RestAPIParams.UserSettingsCustomStatus(modelCustomStatusSetting));
    }

    public final void getIsSyncTextAndImagesEnabled(boolean z2) {
        SharedPreferences.Editor edit = getPrefs().edit();
        m.checkNotNullExpressionValue(edit, "editor");
        if (getIsSyncTextAndImagesEnabled() != z2) {
            edit.putBoolean("CACHE_KEY_SYNC_TEXT_AND_IMAGES", z2);
        }
        edit.apply();
    }
}
