package com.discord.stores;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.message.Message;
import com.discord.models.domain.ModelNotificationSettings;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import com.discord.stores.StoreThreadsJoined;
import d0.t.n;
import d0.z.d.m;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
/* compiled from: NotificationTextUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\n\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\t\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b1\u00102Jc\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0003\u001a\u00020\u00022\u0010\u0010\u0007\u001a\f\u0012\b\u0012\u00060\u0005j\u0002`\u00060\u00042\u0006\u0010\t\u001a\u00020\b2\b\u0010\u000b\u001a\u0004\u0018\u00010\n2\b\u0010\r\u001a\u0004\u0018\u00010\f2\b\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\b\u0010\u0011\u001a\u0004\u0018\u00010\u00102\b\u0010\u0012\u001a\u0004\u0018\u00010\u0010H\u0002¢\u0006\u0004\b\u0014\u0010\u0015JG\u0010\u0017\u001a\u00020\u0013*\u00020\f2\u0006\u0010\u0003\u001a\u00020\u00022\u0010\u0010\u0007\u001a\f\u0012\b\u0012\u00060\u0005j\u0002`\u00060\u00042\u0006\u0010\t\u001a\u00020\b2\b\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u0016\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u0017\u0010\u0018J!\u0010\u001c\u001a\u0004\u0018\u00010\u001b*\u00020\f2\n\u0010\u001a\u001a\u00060\u0005j\u0002`\u0019H\u0002¢\u0006\u0004\b\u001c\u0010\u001dJO\u0010!\u001a\u00020\u00132\b\u0010\u001e\u001a\u0004\u0018\u00010\u001b2\u0006\u0010\u0003\u001a\u00020\u00022\u0010\u0010\u0007\u001a\f\u0012\b\u0012\u00060\u0005j\u0002`\u00060\u00042\u0006\u0010\t\u001a\u00020\b2\b\b\u0002\u0010\u001f\u001a\u00020\u00132\b\b\u0002\u0010 \u001a\u00020\u0013H\u0002¢\u0006\u0004\b!\u0010\"J\u001d\u0010#\u001a\u00020\u0013*\u0004\u0018\u00010\f2\b\u0010\u0016\u001a\u0004\u0018\u00010\u0010¢\u0006\u0004\b#\u0010$J©\u0001\u0010-\u001a\u00020\u00132\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010%\u001a\u00020\b2\u0006\u0010\u0016\u001a\u00020\u00102\u0016\u0010(\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`'\u0012\u0004\u0012\u00020\u001b0&2\b\u0010\u0012\u001a\u0004\u0018\u00010\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000e2&\u0010+\u001a\"\u0012\b\u0012\u00060\u0005j\u0002`)\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`'\u0012\u0004\u0012\u00020*0&0&2\u0016\u0010\r\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`)\u0012\u0004\u0012\u00020\f0&2\u0016\u0010,\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0019\u0012\u0004\u0012\u00020\n0&¢\u0006\u0004\b-\u0010.J)\u0010/\u001a\u0004\u0018\u00010\u001b*\u0004\u0018\u00010\f2\b\u0010\u0016\u001a\u0004\u0018\u00010\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000e¢\u0006\u0004\b/\u00100¨\u00063"}, d2 = {"Lcom/discord/stores/NotificationTextUtils;", "", "Lcom/discord/models/user/User;", "me", "", "", "Lcom/discord/primitives/RoleId;", "myRoleIds", "Lcom/discord/api/message/Message;", "message", "Lcom/discord/stores/StoreThreadsJoined$JoinedThread;", "joinedThread", "Lcom/discord/models/domain/ModelNotificationSettings;", "guildSettings", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/api/channel/Channel;", "thread", "parentChannel", "", "isThreadNotificationAllowed", "(Lcom/discord/models/user/User;Ljava/util/Collection;Lcom/discord/api/message/Message;Lcom/discord/stores/StoreThreadsJoined$JoinedThread;Lcom/discord/models/domain/ModelNotificationSettings;Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;)Z", "channel", "isNotificationAllowed", "(Lcom/discord/models/domain/ModelNotificationSettings;Lcom/discord/models/user/User;Ljava/util/Collection;Lcom/discord/api/message/Message;Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;)Z", "Lcom/discord/primitives/ChannelId;", "channelId", "", "messageNotifications", "(Lcom/discord/models/domain/ModelNotificationSettings;J)Ljava/lang/Integer;", "msgNotifLevel", "isSuppressEveryone", "isSuppressRoles", "shouldNotifyForLevel", "(Ljava/lang/Integer;Lcom/discord/models/user/User;Ljava/util/Collection;Lcom/discord/api/message/Message;ZZ)Z", "isGuildOrCategoryOrChannelMuted", "(Lcom/discord/models/domain/ModelNotificationSettings;Lcom/discord/api/channel/Channel;)Z", NotificationCompat.CATEGORY_MESSAGE, "", "Lcom/discord/primitives/UserId;", "blockedRelationships", "Lcom/discord/primitives/GuildId;", "Lcom/discord/models/member/GuildMember;", "guildMembers", "joinedThreads", "shouldNotifyInAppPopup", "(Lcom/discord/models/user/User;Lcom/discord/api/message/Message;Lcom/discord/api/channel/Channel;Ljava/util/Map;Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Z", "channelMessageNotificationLevel", "(Lcom/discord/models/domain/ModelNotificationSettings;Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;)Ljava/lang/Integer;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class NotificationTextUtils {
    public static final NotificationTextUtils INSTANCE = new NotificationTextUtils();

    private NotificationTextUtils() {
    }

    private final boolean isNotificationAllowed(ModelNotificationSettings modelNotificationSettings, User user, Collection<Long> collection, Message message, Guild guild, Channel channel) {
        if (modelNotificationSettings.isMobilePush() && !isGuildOrCategoryOrChannelMuted(modelNotificationSettings, channel)) {
            return shouldNotifyForLevel(channelMessageNotificationLevel(modelNotificationSettings, channel, guild), user, collection, message, modelNotificationSettings.isSuppressEveryone(), modelNotificationSettings.isSuppressRoles());
        }
        return false;
    }

    /* JADX WARN: Code restructure failed: missing block: B:25:0x005a, code lost:
        if (r4 != true) goto L26;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final boolean isThreadNotificationAllowed(com.discord.models.user.User r4, java.util.Collection<java.lang.Long> r5, com.discord.api.message.Message r6, com.discord.stores.StoreThreadsJoined.JoinedThread r7, com.discord.models.domain.ModelNotificationSettings r8, com.discord.models.guild.Guild r9, com.discord.api.channel.Channel r10, com.discord.api.channel.Channel r11) {
        /*
            r3 = this;
            boolean r10 = r3.isGuildOrCategoryOrChannelMuted(r8, r10)
            r0 = 0
            if (r10 == 0) goto L8
            return r0
        L8:
            boolean r10 = r3.isGuildOrCategoryOrChannelMuted(r8, r11)
            java.lang.Integer r8 = r3.channelMessageNotificationLevel(r8, r11, r9)
            com.discord.utilities.threads.ThreadUtils r9 = com.discord.utilities.threads.ThreadUtils.INSTANCE
            int r7 = r9.computeThreadNotificationSetting(r7, r10, r8)
            r8 = 2
            r9 = 1
            if (r7 == r8) goto L8b
            r8 = 8
            if (r7 == r8) goto L8c
            java.lang.Boolean r7 = r6.r()
            java.lang.Boolean r8 = java.lang.Boolean.TRUE
            boolean r7 = d0.z.d.m.areEqual(r7, r8)
            if (r7 != 0) goto L8b
            java.util.List r7 = r6.t()
            if (r7 == 0) goto L5c
            boolean r8 = r7.isEmpty()
            if (r8 == 0) goto L38
        L36:
            r4 = 0
            goto L5a
        L38:
            java.util.Iterator r7 = r7.iterator()
        L3c:
            boolean r8 = r7.hasNext()
            if (r8 == 0) goto L36
            java.lang.Object r8 = r7.next()
            com.discord.api.user.User r8 = (com.discord.api.user.User) r8
            long r10 = r8.i()
            long r1 = r4.getId()
            int r8 = (r10 > r1 ? 1 : (r10 == r1 ? 0 : -1))
            if (r8 != 0) goto L56
            r8 = 1
            goto L57
        L56:
            r8 = 0
        L57:
            if (r8 == 0) goto L3c
            r4 = 1
        L5a:
            if (r4 == r9) goto L8b
        L5c:
            java.util.List r4 = r6.s()
            if (r4 == 0) goto L8c
            boolean r6 = r4.isEmpty()
            if (r6 == 0) goto L6a
        L68:
            r4 = 0
            goto L89
        L6a:
            java.util.Iterator r4 = r4.iterator()
        L6e:
            boolean r6 = r4.hasNext()
            if (r6 == 0) goto L68
            java.lang.Object r6 = r4.next()
            java.lang.Number r6 = (java.lang.Number) r6
            long r6 = r6.longValue()
            java.lang.Long r6 = java.lang.Long.valueOf(r6)
            boolean r6 = r5.contains(r6)
            if (r6 == 0) goto L6e
            r4 = 1
        L89:
            if (r4 != r9) goto L8c
        L8b:
            r0 = 1
        L8c:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.NotificationTextUtils.isThreadNotificationAllowed(com.discord.models.user.User, java.util.Collection, com.discord.api.message.Message, com.discord.stores.StoreThreadsJoined$JoinedThread, com.discord.models.domain.ModelNotificationSettings, com.discord.models.guild.Guild, com.discord.api.channel.Channel, com.discord.api.channel.Channel):boolean");
    }

    private final Integer messageNotifications(ModelNotificationSettings modelNotificationSettings, long j) {
        ModelNotificationSettings.ChannelOverride channelOverride = modelNotificationSettings.getChannelOverride(j);
        if (channelOverride == null) {
            return null;
        }
        Integer valueOf = Integer.valueOf(channelOverride.getMessageNotifications());
        if (!(valueOf.intValue() == ModelNotificationSettings.FREQUENCY_UNSET)) {
            return valueOf;
        }
        return null;
    }

    /* JADX WARN: Code restructure failed: missing block: B:32:0x0066, code lost:
        if (r8 != true) goto L33;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final boolean shouldNotifyForLevel(java.lang.Integer r8, com.discord.models.user.User r9, java.util.Collection<java.lang.Long> r10, com.discord.api.message.Message r11, boolean r12, boolean r13) {
        /*
            r7 = this;
            int r0 = com.discord.models.domain.ModelNotificationSettings.FREQUENCY_ALL
            r1 = 0
            r2 = 1
            if (r8 != 0) goto L7
            goto L10
        L7:
            int r3 = r8.intValue()
            if (r3 != r0) goto L10
        Ld:
            r1 = 1
            goto L9b
        L10:
            int r0 = com.discord.models.domain.ModelNotificationSettings.FREQUENCY_NOTHING
            if (r8 != 0) goto L15
            goto L1d
        L15:
            int r3 = r8.intValue()
            if (r3 != r0) goto L1d
            goto L9b
        L1d:
            int r0 = com.discord.models.domain.ModelNotificationSettings.FREQUENCY_MENTIONS
            if (r8 != 0) goto L22
            goto Ld
        L22:
            int r8 = r8.intValue()
            if (r8 != r0) goto Ld
            java.lang.Boolean r8 = r11.r()
            java.lang.Boolean r0 = java.lang.Boolean.TRUE
            boolean r8 = d0.z.d.m.areEqual(r8, r0)
            if (r8 == 0) goto L36
            if (r12 == 0) goto Ld
        L36:
            java.util.List r8 = r11.t()
            if (r8 == 0) goto L68
            boolean r12 = r8.isEmpty()
            if (r12 == 0) goto L44
        L42:
            r8 = 0
            goto L66
        L44:
            java.util.Iterator r8 = r8.iterator()
        L48:
            boolean r12 = r8.hasNext()
            if (r12 == 0) goto L42
            java.lang.Object r12 = r8.next()
            com.discord.api.user.User r12 = (com.discord.api.user.User) r12
            long r3 = r12.i()
            long r5 = r9.getId()
            int r12 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r12 != 0) goto L62
            r12 = 1
            goto L63
        L62:
            r12 = 0
        L63:
            if (r12 == 0) goto L48
            r8 = 1
        L66:
            if (r8 == r2) goto Ld
        L68:
            if (r13 != 0) goto L9b
            java.util.List r8 = r11.s()
            if (r8 == 0) goto L9b
            boolean r9 = r8.isEmpty()
            if (r9 == 0) goto L78
        L76:
            r8 = 0
            goto L97
        L78:
            java.util.Iterator r8 = r8.iterator()
        L7c:
            boolean r9 = r8.hasNext()
            if (r9 == 0) goto L76
            java.lang.Object r9 = r8.next()
            java.lang.Number r9 = (java.lang.Number) r9
            long r11 = r9.longValue()
            java.lang.Long r9 = java.lang.Long.valueOf(r11)
            boolean r9 = r10.contains(r9)
            if (r9 == 0) goto L7c
            r8 = 1
        L97:
            if (r8 != r2) goto L9b
            goto Ld
        L9b:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.NotificationTextUtils.shouldNotifyForLevel(java.lang.Integer, com.discord.models.user.User, java.util.Collection, com.discord.api.message.Message, boolean, boolean):boolean");
    }

    public static /* synthetic */ boolean shouldNotifyForLevel$default(NotificationTextUtils notificationTextUtils, Integer num, User user, Collection collection, Message message, boolean z2, boolean z3, int i, Object obj) {
        return notificationTextUtils.shouldNotifyForLevel(num, user, collection, message, (i & 16) != 0 ? false : z2, (i & 32) != 0 ? false : z3);
    }

    public final Integer channelMessageNotificationLevel(ModelNotificationSettings modelNotificationSettings, Channel channel, Guild guild) {
        Integer num = null;
        Integer valueOf = guild != null ? Integer.valueOf(guild.getDefaultMessageNotifications()) : null;
        if (channel == null || modelNotificationSettings == null) {
            return valueOf;
        }
        Integer messageNotifications = messageNotifications(modelNotificationSettings, channel.h());
        if (messageNotifications == null) {
            messageNotifications = messageNotifications(modelNotificationSettings, channel.r());
        }
        if (messageNotifications != null) {
            num = messageNotifications;
        } else {
            Integer valueOf2 = Integer.valueOf(modelNotificationSettings.getMessageNotifications());
            if (valueOf2.intValue() != ModelNotificationSettings.FREQUENCY_UNSET) {
                num = valueOf2;
            }
        }
        return num != null ? num : valueOf;
    }

    public final boolean isGuildOrCategoryOrChannelMuted(ModelNotificationSettings modelNotificationSettings, Channel channel) {
        if (modelNotificationSettings == null) {
            return false;
        }
        if (modelNotificationSettings.isMuted()) {
            return true;
        }
        if (channel == null) {
            return false;
        }
        ModelNotificationSettings.ChannelOverride channelOverride = modelNotificationSettings.getChannelOverride(channel.r());
        if (channelOverride != null && channelOverride.isMuted()) {
            return true;
        }
        ModelNotificationSettings.ChannelOverride channelOverride2 = modelNotificationSettings.getChannelOverride(channel.h());
        return channelOverride2 != null && channelOverride2.isMuted();
    }

    public final boolean shouldNotifyInAppPopup(User user, Message message, Channel channel, Map<Long, Integer> map, Channel channel2, Guild guild, Map<Long, ? extends Map<Long, GuildMember>> map2, Map<Long, ? extends ModelNotificationSettings> map3, Map<Long, StoreThreadsJoined.JoinedThread> map4) {
        com.discord.api.user.User e;
        List<Long> list;
        m.checkNotNullParameter(user, "me");
        m.checkNotNullParameter(message, NotificationCompat.CATEGORY_MESSAGE);
        m.checkNotNullParameter(channel, "channel");
        m.checkNotNullParameter(map, "blockedRelationships");
        m.checkNotNullParameter(map2, "guildMembers");
        m.checkNotNullParameter(map3, "guildSettings");
        m.checkNotNullParameter(map4, "joinedThreads");
        if (ChannelUtils.v(channel) || (e = message.e()) == null || e.i() == 0 || e.i() == user.getId() || map.containsKey(Long.valueOf(e.i()))) {
            return false;
        }
        Integer E = message.E();
        if (E != null && E.intValue() == 3) {
            return false;
        }
        Map map5 = (Map) a.u0(channel, map2);
        Integer num = null;
        GuildMember guildMember = map5 != null ? (GuildMember) a.e(user, map5) : null;
        if (guildMember == null || (list = guildMember.getRoles()) == null) {
            list = n.emptyList();
        }
        List<Long> list2 = list;
        ModelNotificationSettings modelNotificationSettings = (ModelNotificationSettings) a.u0(channel, map3);
        if (ChannelUtils.C(channel)) {
            return isThreadNotificationAllowed(user, list2, message, (StoreThreadsJoined.JoinedThread) a.c(channel, map4), modelNotificationSettings, guild, channel, channel2);
        }
        if (modelNotificationSettings != null) {
            return isNotificationAllowed(modelNotificationSettings, user, list2, message, guild, channel);
        }
        if (guild != null) {
            num = Integer.valueOf(guild.getDefaultMessageNotifications());
        }
        return shouldNotifyForLevel$default(this, num, user, list2, message, false, false, 48, null);
    }
}
