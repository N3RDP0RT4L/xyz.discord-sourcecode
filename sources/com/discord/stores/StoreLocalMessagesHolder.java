package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.SharedPreferences;
import b.a.b.a;
import b.i.d.e;
import com.discord.app.AppLog;
import com.discord.models.message.Message;
import com.discord.utilities.cache.SharedPreferencesProvider;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.message.MessageUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import d0.t.h0;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import kotlin.Metadata;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;
/* compiled from: StoreLocalMessagesHolder.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010#\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0000\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b5\u0010\bJ\u0019\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0003¢\u0006\u0004\b\u0007\u0010\bJ%\u0010\u000e\u001a\u001a\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00020\u000b\u0012\n\u0012\b\u0012\u0004\u0012\u00020\r0\f0\n0\t¢\u0006\u0004\b\u000e\u0010\u000fJ\u0019\u0010\u0011\u001a\u00020\u00042\b\b\u0002\u0010\u0010\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0011\u0010\u0006J\u0015\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\r0\fH\u0007¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0014\u001a\u00020\rH\u0007¢\u0006\u0004\b\u0015\u0010\u0016J%\u0010\u001b\u001a\u0004\u0018\u00010\r2\n\u0010\u0018\u001a\u00060\u000bj\u0002`\u00172\u0006\u0010\u001a\u001a\u00020\u0019H\u0007¢\u0006\u0004\b\u001b\u0010\u001cJ\u0017\u0010\u001d\u001a\u00020\u00042\u0006\u0010\u0014\u001a\u00020\rH\u0007¢\u0006\u0004\b\u001d\u0010\u0016J#\u0010\u001d\u001a\u00020\u00042\n\u0010\u0018\u001a\u00060\u000bj\u0002`\u00172\u0006\u0010\u001a\u001a\u00020\u0019H\u0007¢\u0006\u0004\b\u001d\u0010\u001eJ\u000f\u0010\u001f\u001a\u00020\u0004H\u0007¢\u0006\u0004\b\u001f\u0010\bR\u001c\u0010!\u001a\b\u0012\u0004\u0012\u00020\u000b0 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"RF\u0010$\u001a2\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00020\u000b\u0012\n\u0012\b\u0012\u0004\u0012\u00020\r0\f0\n\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00020\u000b\u0012\n\u0012\b\u0012\u0004\u0012\u00020\r0\f0\n0#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%R.\u0010(\u001a\u001a\u0012\u0004\u0012\u00020\u000b\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\r0'0&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u0010)R\u0016\u0010\u0010\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0010\u0010*R\u001e\u0010-\u001a\n ,*\u0004\u0018\u00010+0+8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b-\u0010.R(\u0010/\u001a\u0014\u0012\u0004\u0012\u00020\u000b\u0012\n\u0012\b\u0012\u0004\u0012\u00020\r0\f0\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b/\u00100R\u0016\u00102\u001a\u0002018\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b2\u00103R(\u00104\u001a\u0014\u0012\u0004\u0012\u00020\u000b\u0012\n\u0012\b\u0012\u0004\u0012\u00020\r0\f0\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b4\u00100¨\u00066"}, d2 = {"Lcom/discord/stores/StoreLocalMessagesHolder;", "", "", "force", "", "publishIfUpdated", "(Z)V", "messageCacheTryPersist", "()V", "Lrx/Observable;", "", "", "", "Lcom/discord/models/message/Message;", "getMessagesPublisher", "()Lrx/Observable;", "cacheEnabled", "init", "getFlattenedMessages", "()Ljava/util/List;", "message", "addMessage", "(Lcom/discord/models/message/Message;)V", "Lcom/discord/primitives/ChannelId;", "channelId", "", "nonce", "getMessage", "(JLjava/lang/String;)Lcom/discord/models/message/Message;", "deleteMessage", "(JLjava/lang/String;)V", "clearCache", "", "updatedChannels", "Ljava/util/Set;", "Lrx/subjects/Subject;", "messagesPublisher", "Lrx/subjects/Subject;", "Ljava/util/HashMap;", "Ljava/util/TreeMap;", "messages", "Ljava/util/HashMap;", "Z", "Lcom/google/gson/Gson;", "kotlin.jvm.PlatformType", "gson", "Lcom/google/gson/Gson;", "messagesSnapshot", "Ljava/util/Map;", "Landroid/content/SharedPreferences;", "sharedPreferences", "Landroid/content/SharedPreferences;", "cacheSnapshot", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreLocalMessagesHolder {
    private boolean cacheEnabled;
    private final Gson gson;
    private SharedPreferences sharedPreferences;
    private final HashMap<Long, TreeMap<Long, Message>> messages = new HashMap<>();
    private final Subject<Map<Long, List<Message>>, Map<Long, List<Message>>> messagesPublisher = new SerializedSubject(BehaviorSubject.k0());
    private Map<Long, ? extends List<Message>> messagesSnapshot = h0.emptyMap();
    private Map<Long, ? extends List<Message>> cacheSnapshot = h0.emptyMap();
    private final Set<Long> updatedChannels = new HashSet();

    public StoreLocalMessagesHolder() {
        e eVar = new e();
        a.a(eVar);
        this.gson = eVar.a();
    }

    public static /* synthetic */ void init$default(StoreLocalMessagesHolder storeLocalMessagesHolder, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = true;
        }
        storeLocalMessagesHolder.init(z2);
    }

    @StoreThread
    private final void messageCacheTryPersist() {
        if (this.cacheEnabled) {
            HashMap hashMap = new HashMap();
            for (Map.Entry<Long, TreeMap<Long, Message>> entry : this.messages.entrySet()) {
                Long key = entry.getKey();
                Collection<Message> values = entry.getValue().values();
                m.checkNotNullExpressionValue(values, "entry.value.values");
                hashMap.put(key, u.toList(values));
            }
            if (!m.areEqual(this.cacheSnapshot, hashMap)) {
                this.cacheSnapshot = hashMap;
                String m = this.gson.m(hashMap);
                SharedPreferences sharedPreferences = this.sharedPreferences;
                if (sharedPreferences == null) {
                    m.throwUninitializedPropertyAccessException("sharedPreferences");
                }
                sharedPreferences.edit().putString("STORE_LOCAL_MESSAGES_CACHE_V11", m).apply();
            }
        }
    }

    private final void publishIfUpdated(boolean z2) {
        Collection<Message> collection;
        if (!this.updatedChannels.isEmpty() || z2) {
            HashMap hashMap = new HashMap(this.messagesSnapshot);
            for (Long l : this.updatedChannels) {
                long longValue = l.longValue();
                Long valueOf = Long.valueOf(longValue);
                TreeMap<Long, Message> treeMap = this.messages.get(Long.valueOf(longValue));
                if (treeMap == null || (collection = treeMap.values()) == null) {
                    collection = n.emptyList();
                }
                hashMap.put(valueOf, new ArrayList(collection));
            }
            this.updatedChannels.clear();
            this.messagesSnapshot = hashMap;
            this.messagesPublisher.onNext(hashMap);
            messageCacheTryPersist();
        }
    }

    public static /* synthetic */ void publishIfUpdated$default(StoreLocalMessagesHolder storeLocalMessagesHolder, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = false;
        }
        storeLocalMessagesHolder.publishIfUpdated(z2);
    }

    @StoreThread
    public final void addMessage(Message message) {
        m.checkNotNullParameter(message, "message");
        long channelId = message.getChannelId();
        TreeMap<Long, Message> treeMap = this.messages.get(Long.valueOf(channelId));
        if (treeMap == null) {
            treeMap = new TreeMap<>();
        }
        treeMap.put(Long.valueOf(message.getId()), message);
        this.messages.put(Long.valueOf(channelId), treeMap);
        this.updatedChannels.add(Long.valueOf(channelId));
        publishIfUpdated$default(this, false, 1, null);
    }

    @StoreThread
    public final void clearCache() {
        for (Map.Entry<Long, TreeMap<Long, Message>> entry : this.messages.entrySet()) {
            Long key = entry.getKey();
            Set<Long> set = this.updatedChannels;
            m.checkNotNullExpressionValue(key, "channelId");
            set.add(key);
            entry.getValue().clear();
        }
        publishIfUpdated$default(this, false, 1, null);
    }

    @StoreThread
    public final void deleteMessage(Message message) {
        m.checkNotNullParameter(message, "message");
        long id2 = message.getId();
        long channelId = message.getChannelId();
        TreeMap<Long, Message> treeMap = this.messages.get(Long.valueOf(channelId));
        if (treeMap != null) {
            m.checkNotNullExpressionValue(treeMap, "messages[channelId] ?: return");
            if (treeMap.containsKey(Long.valueOf(id2))) {
                treeMap.remove(Long.valueOf(id2));
                this.updatedChannels.add(Long.valueOf(channelId));
                if (treeMap.isEmpty()) {
                    this.messages.remove(Long.valueOf(channelId));
                }
            }
            publishIfUpdated$default(this, false, 1, null);
        }
    }

    @StoreThread
    public final List<Message> getFlattenedMessages() {
        return o.flatten(this.messagesSnapshot.values());
    }

    @StoreThread
    public final Message getMessage(long j, String str) {
        Collection<Message> values;
        m.checkNotNullParameter(str, "nonce");
        TreeMap<Long, Message> treeMap = this.messages.get(Long.valueOf(j));
        Object obj = null;
        if (treeMap == null || (values = treeMap.values()) == null) {
            return null;
        }
        Iterator<T> it = values.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            if (m.areEqual(((Message) next).getNonce(), str)) {
                obj = next;
                break;
            }
        }
        return (Message) obj;
    }

    public final Observable<Map<Long, List<Message>>> getMessagesPublisher() {
        return this.messagesPublisher;
    }

    @StoreThread
    public final void init(boolean z2) {
        Map map;
        if (z2) {
            try {
                SharedPreferences sharedPreferences = SharedPreferencesProvider.INSTANCE.get();
                this.sharedPreferences = sharedPreferences;
                if (sharedPreferences == null) {
                    m.throwUninitializedPropertyAccessException("sharedPreferences");
                }
                String string = sharedPreferences.getString("STORE_LOCAL_MESSAGES_CACHE_V11", null);
                Type type = new TypeToken<Map<Long, ? extends List<? extends Message>>>() { // from class: com.discord.stores.StoreLocalMessagesHolder$init$type$1
                }.getType();
                if (string != null) {
                    map = (Map) this.gson.g(string, type);
                } else {
                    map = h0.emptyMap();
                }
                for (Map.Entry entry : map.entrySet()) {
                    long longValue = ((Number) entry.getKey()).longValue();
                    this.messages.put(Long.valueOf(longValue), new TreeMap<>(MessageUtils.getSORT_BY_IDS_COMPARATOR()));
                    TreeMap<Long, Message> treeMap = this.messages.get(Long.valueOf(longValue));
                    if (treeMap == null) {
                        treeMap = new TreeMap<>();
                    }
                    for (Message message : (List) entry.getValue()) {
                        treeMap.put(Long.valueOf(message.getId()), message);
                    }
                    this.messages.put(Long.valueOf(longValue), treeMap);
                    this.updatedChannels.add(Long.valueOf(longValue));
                }
            } catch (Exception e) {
                clearCache();
                Logger.e$default(AppLog.g, "Error restoring cached local messages", e, null, 4, null);
            }
        }
        this.cacheEnabled = z2;
        publishIfUpdated(true);
    }

    @StoreThread
    public final void deleteMessage(long j, String str) {
        Object obj;
        m.checkNotNullParameter(str, "nonce");
        TreeMap<Long, Message> treeMap = this.messages.get(Long.valueOf(j));
        if (treeMap != null) {
            m.checkNotNullExpressionValue(treeMap, "messages[channelId] ?: return");
            Collection<Message> values = treeMap.values();
            m.checkNotNullExpressionValue(values, "messagesForChannel.values");
            Iterator<T> it = values.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (m.areEqual(((Message) obj).getNonce(), str)) {
                    break;
                }
            }
            Message message = (Message) obj;
            if (message != null) {
                m.checkNotNullExpressionValue(message, "messagesForChannel.value…once == nonce } ?: return");
                deleteMessage(message);
            }
        }
    }
}
