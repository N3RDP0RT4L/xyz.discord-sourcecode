package com.discord.stores;

import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreCalls.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "ring", "", "invoke", "(Z)V", "doCall"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreCalls$call$1 extends o implements Function1<Boolean, Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ StoreCalls this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreCalls$call$1(StoreCalls storeCalls, long j) {
        super(1);
        this.this$0 = storeCalls;
        this.$channelId = j;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Boolean bool) {
        invoke(bool.booleanValue());
        return Unit.a;
    }

    public final void invoke(boolean z2) {
        StoreStream storeStream;
        storeStream = this.this$0.stream;
        storeStream.getVoiceChannelSelected$app_productionGoogleRelease().selectVoiceChannel(this.$channelId);
        if (z2) {
            StoreCalls.ring$default(this.this$0, this.$channelId, null, 2, null);
        }
    }
}
