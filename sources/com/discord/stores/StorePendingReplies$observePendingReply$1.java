package com.discord.stores;

import com.discord.stores.StorePendingReplies;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StorePendingReplies.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/stores/StorePendingReplies$PendingReply;", "invoke", "()Lcom/discord/stores/StorePendingReplies$PendingReply;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StorePendingReplies$observePendingReply$1 extends o implements Function0<StorePendingReplies.PendingReply> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ StorePendingReplies this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StorePendingReplies$observePendingReply$1(StorePendingReplies storePendingReplies, long j) {
        super(0);
        this.this$0 = storePendingReplies;
        this.$channelId = j;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final StorePendingReplies.PendingReply invoke() {
        return this.this$0.getPendingReply(this.$channelId);
    }
}
