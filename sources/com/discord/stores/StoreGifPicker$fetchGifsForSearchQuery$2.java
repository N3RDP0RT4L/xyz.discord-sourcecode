package com.discord.stores;

import com.discord.utilities.error.Error;
import d0.t.n;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreGifPicker.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGifPicker$fetchGifsForSearchQuery$2 extends o implements Function1<Error, Unit> {
    public final /* synthetic */ String $query;
    public final /* synthetic */ StoreGifPicker this$0;

    /* compiled from: StoreGifPicker.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreGifPicker$fetchGifsForSearchQuery$2$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public AnonymousClass1() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            StoreGifPicker$fetchGifsForSearchQuery$2 storeGifPicker$fetchGifsForSearchQuery$2 = StoreGifPicker$fetchGifsForSearchQuery$2.this;
            storeGifPicker$fetchGifsForSearchQuery$2.this$0.handleGifSearchResults(storeGifPicker$fetchGifsForSearchQuery$2.$query, n.emptyList());
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGifPicker$fetchGifsForSearchQuery$2(StoreGifPicker storeGifPicker, String str) {
        super(1);
        this.this$0 = storeGifPicker;
        this.$query = str;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Error error) {
        invoke2(error);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Error error) {
        Dispatcher dispatcher;
        m.checkNotNullParameter(error, "it");
        dispatcher = this.this$0.dispatcher;
        dispatcher.schedule(new AnonymousClass1());
    }
}
