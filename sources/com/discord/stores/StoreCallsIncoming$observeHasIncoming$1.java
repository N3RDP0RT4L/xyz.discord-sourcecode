package com.discord.stores;

import androidx.core.app.NotificationCompat;
import d0.z.d.m;
import j0.k.b;
import java.util.Set;
import kotlin.Metadata;
/* compiled from: StoreCallsIncoming.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0007\u001a\n \u0002*\u0004\u0018\u00010\u00040\u00042\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "", "kotlin.jvm.PlatformType", "incomingCalls", "", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Set;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreCallsIncoming$observeHasIncoming$1<T, R> implements b<Set<? extends Long>, Boolean> {
    public static final StoreCallsIncoming$observeHasIncoming$1 INSTANCE = new StoreCallsIncoming$observeHasIncoming$1();

    @Override // j0.k.b
    public /* bridge */ /* synthetic */ Boolean call(Set<? extends Long> set) {
        return call2((Set<Long>) set);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final Boolean call2(Set<Long> set) {
        m.checkNotNullExpressionValue(set, "incomingCalls");
        return Boolean.valueOf(!set.isEmpty());
    }
}
