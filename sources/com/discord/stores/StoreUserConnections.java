package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.Context;
import com.discord.api.connectedaccounts.ConnectedAccount;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelPayload;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.stores.utilities.RestCallStateKt;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.t.n;
import d0.z.d.g;
import d0.z.d.g0.a;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.function.UnaryOperator;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreUserConnections.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0001:B!\u0012\u0006\u0010,\u001a\u00020+\u0012\u0006\u00101\u001a\u000200\u0012\b\b\u0002\u00106\u001a\u000205¢\u0006\u0004\b8\u00109J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0007¢\u0006\u0004\b\t\u0010\nJ\u001d\u0010\u000e\u001a\u00020\u00042\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000bH\u0007¢\u0006\u0004\b\u000e\u0010\u000fJ\r\u0010\u0011\u001a\u00020\u0010¢\u0006\u0004\b\u0011\u0010\u0012J\u0013\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00100\u0013¢\u0006\u0004\b\u0014\u0010\u0015J-\u0010\u001b\u001a\u00020\u00042\u0006\u0010\u0016\u001a\u00020\f2\u0006\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u00172\u0006\u0010\u001a\u001a\u00020\u0017¢\u0006\u0004\b\u001b\u0010\u001cJ\u001d\u0010 \u001a\u00020\u00042\u0006\u0010\u001e\u001a\u00020\u001d2\u0006\u0010\u001f\u001a\u00020\u001d¢\u0006\u0004\b \u0010!J%\u0010#\u001a\u00020\u00042\u0006\u0010\u001e\u001a\u00020\u001d2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\"\u001a\u00020\u001d¢\u0006\u0004\b#\u0010$J\r\u0010%\u001a\u00020\u0004¢\u0006\u0004\b%\u0010&J\u000f\u0010'\u001a\u00020\u0004H\u0017¢\u0006\u0004\b'\u0010&R\u0016\u0010(\u001a\u00020\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b(\u0010)R\u0016\u0010*\u001a\u00020\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b*\u0010)R\u0019\u0010,\u001a\u00020+8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010-\u001a\u0004\b.\u0010/R\u0019\u00101\u001a\u0002008\u0006@\u0006¢\u0006\f\n\u0004\b1\u00102\u001a\u0004\b3\u00104R\u0016\u00106\u001a\u0002058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b6\u00107¨\u0006;"}, d2 = {"Lcom/discord/stores/StoreUserConnections;", "Lcom/discord/stores/StoreV2;", "Landroid/content/Context;", "context", "", "init", "(Landroid/content/Context;)V", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "", "Lcom/discord/api/connectedaccounts/ConnectedAccount;", "accounts", "handleUserConnections", "(Ljava/util/List;)V", "Lcom/discord/stores/StoreUserConnections$State;", "getConnectedAccounts", "()Lcom/discord/stores/StoreUserConnections$State;", "Lrx/Observable;", "observeConnectedAccounts", "()Lrx/Observable;", "connectedAccount", "", "syncFriends", "showActivity", "isVisible", "updateUserConnection", "(Lcom/discord/api/connectedaccounts/ConnectedAccount;ZZZ)V", "", "platformName", "connectionId", "deleteUserConnection", "(Ljava/lang/String;Ljava/lang/String;)V", ModelAuditLogEntry.CHANGE_KEY_LOCATION, "authorizeConnection", "(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V", "fetchConnectedAccounts", "()V", "snapshotData", "state", "Lcom/discord/stores/StoreUserConnections$State;", "stateSnapshot", "Lcom/discord/stores/StoreStream;", "stream", "Lcom/discord/stores/StoreStream;", "getStream", "()Lcom/discord/stores/StoreStream;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "getDispatcher", "()Lcom/discord/stores/Dispatcher;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V", "State", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserConnections extends StoreV2 {
    private final Dispatcher dispatcher;
    private final ObservationDeck observationDeck;
    private State state;
    private State stateSnapshot;
    private final StoreStream stream;

    /* compiled from: StoreUserConnections.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010(\n\u0002\b\u0003\n\u0002\u0010*\n\u0002\b\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002()B\u0017\b\u0002\u0012\f\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001¢\u0006\u0004\b&\u0010'J\u0018\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0096\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u001e\u0010\t\u001a\u00020\u00042\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00020\u0007H\u0096\u0001¢\u0006\u0004\b\t\u0010\nJ\u0018\u0010\r\u001a\u00020\u00022\u0006\u0010\f\u001a\u00020\u000bH\u0096\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0018\u0010\u000f\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u0002H\u0096\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u0004H\u0096\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0016\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00020\u0013H\u0096\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0018\u0010\u0016\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u0002H\u0096\u0001¢\u0006\u0004\b\u0016\u0010\u0010J\u0016\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00020\u0017H\u0096\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u001e\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00020\u00172\u0006\u0010\f\u001a\u00020\u000bH\u0096\u0001¢\u0006\u0004\b\u0018\u0010\u001aJ&\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u001b\u001a\u00020\u000b2\u0006\u0010\u001c\u001a\u00020\u000bH\u0096\u0001¢\u0006\u0004\b\u001d\u0010\u001eR\u0016\u0010!\u001a\u00020\u000b8\u0016@\u0016X\u0096\u0005¢\u0006\u0006\u001a\u0004\b\u001f\u0010 R\"\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00020\u00018\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%\u0082\u0001\u0002*+¨\u0006,"}, d2 = {"Lcom/discord/stores/StoreUserConnections$State;", "", "Lcom/discord/api/connectedaccounts/ConnectedAccount;", "element", "", "contains", "(Lcom/discord/api/connectedaccounts/ConnectedAccount;)Z", "", "elements", "containsAll", "(Ljava/util/Collection;)Z", "", "index", "get", "(I)Lcom/discord/api/connectedaccounts/ConnectedAccount;", "indexOf", "(Lcom/discord/api/connectedaccounts/ConnectedAccount;)I", "isEmpty", "()Z", "", "iterator", "()Ljava/util/Iterator;", "lastIndexOf", "", "listIterator", "()Ljava/util/ListIterator;", "(I)Ljava/util/ListIterator;", "fromIndex", "toIndex", "subList", "(II)Ljava/util/List;", "getSize", "()I", "size", "connectedAccounts", "Ljava/util/List;", "getConnectedAccounts", "()Ljava/util/List;", HookHelper.constructorName, "(Ljava/util/List;)V", "ConnectedAccounts", "Loading", "Lcom/discord/stores/StoreUserConnections$State$Loading;", "Lcom/discord/stores/StoreUserConnections$State$ConnectedAccounts;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class State implements List<ConnectedAccount>, a {
        private final List<ConnectedAccount> connectedAccounts;

        /* compiled from: StoreUserConnections.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J \u0010\u0007\u001a\u00020\u00002\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\"\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/stores/StoreUserConnections$State$ConnectedAccounts;", "Lcom/discord/stores/StoreUserConnections$State;", "", "Lcom/discord/api/connectedaccounts/ConnectedAccount;", "component1", "()Ljava/util/List;", "connectedAccounts", "copy", "(Ljava/util/List;)Lcom/discord/stores/StoreUserConnections$State$ConnectedAccounts;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getConnectedAccounts", HookHelper.constructorName, "(Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class ConnectedAccounts extends State {
            private final List<ConnectedAccount> connectedAccounts;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public ConnectedAccounts(List<ConnectedAccount> list) {
                super(list, null);
                m.checkNotNullParameter(list, "connectedAccounts");
                this.connectedAccounts = list;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ ConnectedAccounts copy$default(ConnectedAccounts connectedAccounts, List list, int i, Object obj) {
                if ((i & 1) != 0) {
                    list = connectedAccounts.getConnectedAccounts();
                }
                return connectedAccounts.copy(list);
            }

            public final List<ConnectedAccount> component1() {
                return getConnectedAccounts();
            }

            public final ConnectedAccounts copy(List<ConnectedAccount> list) {
                m.checkNotNullParameter(list, "connectedAccounts");
                return new ConnectedAccounts(list);
            }

            @Override // java.util.List, java.util.Collection
            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof ConnectedAccounts) && m.areEqual(getConnectedAccounts(), ((ConnectedAccounts) obj).getConnectedAccounts());
                }
                return true;
            }

            @Override // com.discord.stores.StoreUserConnections.State
            public List<ConnectedAccount> getConnectedAccounts() {
                return this.connectedAccounts;
            }

            @Override // java.util.List, java.util.Collection
            public int hashCode() {
                List<ConnectedAccount> connectedAccounts = getConnectedAccounts();
                if (connectedAccounts != null) {
                    return connectedAccounts.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = b.d.b.a.a.R("ConnectedAccounts(connectedAccounts=");
                R.append(getConnectedAccounts());
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: StoreUserConnections.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreUserConnections$State$Loading;", "Lcom/discord/stores/StoreUserConnections$State;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Loading extends State {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(n.emptyList(), null);
            }
        }

        private State(List<ConnectedAccount> list) {
            this.connectedAccounts = list;
        }

        /* renamed from: add  reason: avoid collision after fix types in other method */
        public void add2(int i, ConnectedAccount connectedAccount) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.List
        public /* synthetic */ void add(int i, ConnectedAccount connectedAccount) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        public boolean add(ConnectedAccount connectedAccount) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.List, java.util.Collection
        public /* synthetic */ boolean add(Object obj) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.List
        public boolean addAll(int i, Collection<? extends ConnectedAccount> collection) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.List, java.util.Collection
        public boolean addAll(Collection<? extends ConnectedAccount> collection) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.List, java.util.Collection
        public void clear() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        public boolean contains(ConnectedAccount connectedAccount) {
            m.checkNotNullParameter(connectedAccount, "element");
            return this.connectedAccounts.contains(connectedAccount);
        }

        @Override // java.util.List, java.util.Collection
        public final /* bridge */ boolean contains(Object obj) {
            if (obj instanceof ConnectedAccount) {
                return contains((ConnectedAccount) obj);
            }
            return false;
        }

        @Override // java.util.List, java.util.Collection
        public boolean containsAll(Collection<? extends Object> collection) {
            m.checkNotNullParameter(collection, "elements");
            return this.connectedAccounts.containsAll(collection);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // java.util.List
        public ConnectedAccount get(int i) {
            ConnectedAccount connectedAccount = this.connectedAccounts.get(i);
            m.checkNotNullExpressionValue(connectedAccount, "get(...)");
            return connectedAccount;
        }

        public List<ConnectedAccount> getConnectedAccounts() {
            return this.connectedAccounts;
        }

        public int getSize() {
            return this.connectedAccounts.size();
        }

        public int indexOf(ConnectedAccount connectedAccount) {
            m.checkNotNullParameter(connectedAccount, "element");
            return this.connectedAccounts.indexOf(connectedAccount);
        }

        @Override // java.util.List
        public final /* bridge */ int indexOf(Object obj) {
            if (obj instanceof ConnectedAccount) {
                return indexOf((ConnectedAccount) obj);
            }
            return -1;
        }

        @Override // java.util.List, java.util.Collection
        public boolean isEmpty() {
            return this.connectedAccounts.isEmpty();
        }

        @Override // java.util.List, java.util.Collection, java.lang.Iterable
        public Iterator<ConnectedAccount> iterator() {
            return this.connectedAccounts.iterator();
        }

        public int lastIndexOf(ConnectedAccount connectedAccount) {
            m.checkNotNullParameter(connectedAccount, "element");
            return this.connectedAccounts.lastIndexOf(connectedAccount);
        }

        @Override // java.util.List
        public final /* bridge */ int lastIndexOf(Object obj) {
            if (obj instanceof ConnectedAccount) {
                return lastIndexOf((ConnectedAccount) obj);
            }
            return -1;
        }

        @Override // java.util.List
        public ListIterator<ConnectedAccount> listIterator() {
            return this.connectedAccounts.listIterator();
        }

        @Override // java.util.List
        public ListIterator<ConnectedAccount> listIterator(int i) {
            return this.connectedAccounts.listIterator(i);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // java.util.List
        public ConnectedAccount remove(int i) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.List, java.util.Collection
        public boolean remove(Object obj) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.List, java.util.Collection
        public boolean removeAll(Collection<? extends Object> collection) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.List
        public void replaceAll(UnaryOperator<ConnectedAccount> unaryOperator) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.List, java.util.Collection
        public boolean retainAll(Collection<? extends Object> collection) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        /* renamed from: set  reason: avoid collision after fix types in other method */
        public ConnectedAccount set2(int i, ConnectedAccount connectedAccount) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.List
        public /* synthetic */ ConnectedAccount set(int i, ConnectedAccount connectedAccount) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.List, java.util.Collection
        public final /* bridge */ int size() {
            return getSize();
        }

        @Override // java.util.List
        public void sort(Comparator<? super ConnectedAccount> comparator) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.List
        public List<ConnectedAccount> subList(int i, int i2) {
            return this.connectedAccounts.subList(i, i2);
        }

        @Override // java.util.List, java.util.Collection
        public Object[] toArray() {
            return g.toArray(this);
        }

        @Override // java.util.List, java.util.Collection
        public <T> T[] toArray(T[] tArr) {
            return (T[]) g.toArray(this, tArr);
        }

        public /* synthetic */ State(List list, DefaultConstructorMarker defaultConstructorMarker) {
            this(list);
        }
    }

    public /* synthetic */ StoreUserConnections(StoreStream storeStream, Dispatcher dispatcher, ObservationDeck observationDeck, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(storeStream, dispatcher, (i & 4) != 0 ? ObservationDeckProvider.get() : observationDeck);
    }

    public final void authorizeConnection(String str, Context context, String str2) {
        m.checkNotNullParameter(str, "platformName");
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(str2, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        AnalyticsTracker.INSTANCE.trackConnectedAccountInitiated(str, str2);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().authorizeConnection(str), false, 1, null), StoreUserConnections.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreUserConnections$authorizeConnection$1(context));
    }

    public final void deleteUserConnection(String str, String str2) {
        m.checkNotNullParameter(str, "platformName");
        m.checkNotNullParameter(str2, "connectionId");
        ObservableExtensionsKt.appSubscribe(RestCallStateKt.logNetworkAction(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().deleteConnection(str, str2), false, 1, null), StoreUserConnections$deleteUserConnection$1.INSTANCE), StoreUserConnections.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreUserConnections$deleteUserConnection$2(this));
    }

    public final void fetchConnectedAccounts() {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().getConnections(), false, 1, null), StoreUserConnections.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreUserConnections$fetchConnectedAccounts$1(this));
    }

    public final State getConnectedAccounts() {
        return this.stateSnapshot;
    }

    public final Dispatcher getDispatcher() {
        return this.dispatcher;
    }

    public final StoreStream getStream() {
        return this.stream;
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        List<ConnectedAccount> connectedAccounts = modelPayload.getConnectedAccounts();
        m.checkNotNullExpressionValue(connectedAccounts, "payload.connectedAccounts");
        handleUserConnections(connectedAccounts);
    }

    @StoreThread
    public final void handleUserConnections(List<ConnectedAccount> list) {
        m.checkNotNullParameter(list, "accounts");
        this.state = new State.ConnectedAccounts(list);
        markChanged();
    }

    @Override // com.discord.stores.Store
    public void init(Context context) {
        m.checkNotNullParameter(context, "context");
        super.init(context);
        ObservableExtensionsKt.appSubscribe(this.stream.getGatewaySocket$app_productionGoogleRelease().getUserConnectionUpdate(), StoreUserConnections.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreUserConnections$init$1(this));
    }

    public final Observable<State> observeConnectedAccounts() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreUserConnections$observeConnectedAccounts$1(this), 14, null);
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        super.snapshotData();
        State state = this.state;
        State state2 = State.Loading.INSTANCE;
        if (!m.areEqual(state, state2)) {
            if (state instanceof State.ConnectedAccounts) {
                state2 = new State.ConnectedAccounts(new ArrayList(state.getConnectedAccounts()));
            } else {
                throw new NoWhenBranchMatchedException();
            }
        }
        this.stateSnapshot = state2;
    }

    public final void updateUserConnection(ConnectedAccount connectedAccount, boolean z2, boolean z3, boolean z4) {
        m.checkNotNullParameter(connectedAccount, "connectedAccount");
        ObservableExtensionsKt.appSubscribe(RestCallStateKt.logNetworkAction(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().updateConnection(connectedAccount.g(), connectedAccount.b(), RestAPIParams.ConnectedAccount.Companion.create(connectedAccount, z2, z3, z4 ? 1 : 0)), false, 1, null), new StoreUserConnections$updateUserConnection$1(connectedAccount, z2)), StoreUserConnections.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreUserConnections$updateUserConnection$2(this));
    }

    public StoreUserConnections(StoreStream storeStream, Dispatcher dispatcher, ObservationDeck observationDeck) {
        m.checkNotNullParameter(storeStream, "stream");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.stream = storeStream;
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
        State.Loading loading = State.Loading.INSTANCE;
        this.state = loading;
        this.stateSnapshot = loading;
    }
}
