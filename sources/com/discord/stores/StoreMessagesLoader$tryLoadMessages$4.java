package com.discord.stores;

import com.discord.models.message.Message;
import com.discord.stores.StoreMessagesLoader;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function4;
/* compiled from: StoreMessagesLoader.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\f\u001a\u00020\t2\n\u0010\u0002\u001a\u00060\u0000j\u0002`\u00012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\u0006H\n¢\u0006\u0004\b\n\u0010\u000b"}, d2 = {"", "Lcom/discord/primitives/ChannelId;", "channelId", "", "Lcom/discord/models/message/Message;", "messages", "", "shouldRequestOlder", "shouldRequestNewer", "", "invoke", "(JLjava/util/List;ZZ)V", "loadPagedMessages"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessagesLoader$tryLoadMessages$4 extends o implements Function4<Long, List<? extends Message>, Boolean, Boolean, Unit> {
    public final /* synthetic */ StoreMessagesLoader$tryLoadMessages$3 $loadMessages$3;
    public final /* synthetic */ StoreMessagesLoader this$0;

    /* compiled from: StoreMessagesLoader.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;", "it", "invoke", "(Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;)Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreMessagesLoader$tryLoadMessages$4$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreMessagesLoader.ChannelLoadedState, StoreMessagesLoader.ChannelLoadedState> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public AnonymousClass1() {
            super(1);
        }

        public final StoreMessagesLoader.ChannelLoadedState invoke(StoreMessagesLoader.ChannelLoadedState channelLoadedState) {
            m.checkNotNullParameter(channelLoadedState, "it");
            return StoreMessagesLoader.ChannelLoadedState.copy$default(channelLoadedState, false, false, false, false, null, 27, null);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreMessagesLoader$tryLoadMessages$4(StoreMessagesLoader storeMessagesLoader, StoreMessagesLoader$tryLoadMessages$3 storeMessagesLoader$tryLoadMessages$3) {
        super(4);
        this.this$0 = storeMessagesLoader;
        this.$loadMessages$3 = storeMessagesLoader$tryLoadMessages$3;
    }

    @Override // kotlin.jvm.functions.Function4
    public /* bridge */ /* synthetic */ Unit invoke(Long l, List<? extends Message> list, Boolean bool, Boolean bool2) {
        invoke(l.longValue(), (List<Message>) list, bool.booleanValue(), bool2.booleanValue());
        return Unit.a;
    }

    public final synchronized void invoke(long j, List<Message> list, boolean z2, boolean z3) {
        m.checkNotNullParameter(list, "messages");
        if (list.isEmpty()) {
            this.this$0.channelLoadedStateUpdate(j, AnonymousClass1.INSTANCE);
            return;
        }
        if (z2) {
            this.$loadMessages$3.invoke(j, (r13 & 2) != 0 ? null : null, (r13 & 4) != 0 ? null : Long.valueOf(list.get(0).getId()), (r13 & 8) != 0 ? null : null);
        } else if (z3) {
            this.$loadMessages$3.invoke(j, (r13 & 2) != 0 ? null : null, (r13 & 4) != 0 ? null : null, (r13 & 8) != 0 ? null : Long.valueOf(list.get(list.size() - 1).getId()));
        }
    }
}
