package com.discord.stores;

import com.discord.models.application.Unread;
import com.discord.utilities.persister.Persister;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreReadStates.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/models/application/Unread;", "kotlin.jvm.PlatformType", "newValue", "", "invoke", "(Lcom/discord/models/application/Unread;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreReadStates$clearMarker$2 extends o implements Function1<Unread, Unit> {
    public final /* synthetic */ StoreReadStates this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreReadStates$clearMarker$2(StoreReadStates storeReadStates) {
        super(1);
        this.this$0 = storeReadStates;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Unread unread) {
        invoke2(unread);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Unread unread) {
        Persister persister;
        persister = this.this$0.unreadMessageMarker;
        m.checkNotNullExpressionValue(unread, "newValue");
        Persister.set$default(persister, unread, false, 2, null);
    }
}
