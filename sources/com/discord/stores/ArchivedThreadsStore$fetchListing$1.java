package com.discord.stores;

import com.discord.api.channel.Channel;
import com.discord.api.thread.ThreadListing;
import com.discord.restapi.utils.RetryWithDelay;
import com.discord.stores.ArchivedThreadsStore;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.error.Error;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import rx.Subscription;
/* compiled from: ArchivedThreadsStore.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ArchivedThreadsStore$fetchListing$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ boolean $reload;
    public final /* synthetic */ ArchivedThreadsStore.ThreadListingType $threadListingType;
    public final /* synthetic */ ArchivedThreadsStore this$0;

    /* compiled from: ArchivedThreadsStore.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lrx/Subscription;", Traits.Payment.Type.SUBSCRIPTION, "", "invoke", "(Lrx/Subscription;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.ArchivedThreadsStore$fetchListing$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<Subscription, Unit> {
        public final /* synthetic */ Pair $key;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Pair pair) {
            super(1);
            this.$key = pair;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Subscription subscription) {
            invoke2(subscription);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Subscription subscription) {
            Map map;
            m.checkNotNullParameter(subscription, Traits.Payment.Type.SUBSCRIPTION);
            map = ArchivedThreadsStore$fetchListing$1.this.this$0.fetchSubscriptions;
            map.put(this.$key, subscription);
        }
    }

    /* compiled from: ArchivedThreadsStore.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.ArchivedThreadsStore$fetchListing$1$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2 extends o implements Function1<Error, Unit> {
        public final /* synthetic */ Pair $key;

        /* compiled from: ArchivedThreadsStore.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.ArchivedThreadsStore$fetchListing$1$2$1  reason: invalid class name */
        /* loaded from: classes.dex */
        public static final class AnonymousClass1 extends o implements Function0<Unit> {
            public AnonymousClass1() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                Map map;
                map = ArchivedThreadsStore$fetchListing$1.this.this$0.listings;
                map.put(AnonymousClass2.this.$key, ArchivedThreadsStore.ThreadListingState.Error.INSTANCE);
                ArchivedThreadsStore$fetchListing$1.this.this$0.markChanged();
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass2(Pair pair) {
            super(1);
            this.$key = pair;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Error error) {
            invoke2(error);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Error error) {
            Dispatcher dispatcher;
            m.checkNotNullParameter(error, "it");
            dispatcher = ArchivedThreadsStore$fetchListing$1.this.this$0.dispatcher;
            dispatcher.schedule(new AnonymousClass1());
        }
    }

    /* compiled from: ArchivedThreadsStore.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/thread/ThreadListing;", "result", "", "invoke", "(Lcom/discord/api/thread/ThreadListing;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.ArchivedThreadsStore$fetchListing$1$3  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass3 extends o implements Function1<ThreadListing, Unit> {
        public final /* synthetic */ List $currentThreads;
        public final /* synthetic */ Pair $key;

        /* compiled from: ArchivedThreadsStore.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.ArchivedThreadsStore$fetchListing$1$3$1  reason: invalid class name */
        /* loaded from: classes.dex */
        public static final class AnonymousClass1 extends o implements Function0<Unit> {
            public final /* synthetic */ ThreadListing $result;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public AnonymousClass1(ThreadListing threadListing) {
                super(0);
                this.$result = threadListing;
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                Map map;
                StoreStream storeStream;
                for (Channel channel : this.$result.b()) {
                    storeStream = ArchivedThreadsStore$fetchListing$1.this.this$0.storeStream;
                    storeStream.handleThreadCreateOrUpdate(channel);
                }
                map = ArchivedThreadsStore$fetchListing$1.this.this$0.listings;
                AnonymousClass3 r1 = AnonymousClass3.this;
                map.put(r1.$key, new ArchivedThreadsStore.ThreadListingState.Listing(u.plus((Collection) r1.$currentThreads, (Iterable) this.$result.b()), this.$result.a(), false));
                ArchivedThreadsStore$fetchListing$1.this.this$0.markChanged();
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass3(Pair pair, List list) {
            super(1);
            this.$key = pair;
            this.$currentThreads = list;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(ThreadListing threadListing) {
            invoke2(threadListing);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(ThreadListing threadListing) {
            Dispatcher dispatcher;
            m.checkNotNullParameter(threadListing, "result");
            dispatcher = ArchivedThreadsStore$fetchListing$1.this.this$0.dispatcher;
            dispatcher.schedule(new AnonymousClass1(threadListing));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ArchivedThreadsStore$fetchListing$1(ArchivedThreadsStore archivedThreadsStore, long j, ArchivedThreadsStore.ThreadListingType threadListingType, boolean z2) {
        super(0);
        this.this$0 = archivedThreadsStore;
        this.$channelId = j;
        this.$threadListingType = threadListingType;
        this.$reload = z2;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        Map map;
        Map map2;
        ArchivedThreadsStore.ThreadListingState.Listing listing;
        Map map3;
        Pair pair = new Pair(Long.valueOf(this.$channelId), this.$threadListingType);
        map = this.this$0.listings;
        ArchivedThreadsStore.ThreadListingState threadListingState = (ArchivedThreadsStore.ThreadListingState) map.get(pair);
        boolean z2 = threadListingState instanceof ArchivedThreadsStore.ThreadListingState.Listing;
        List<Channel> emptyList = (!z2 || this.$reload) ? n.emptyList() : ((ArchivedThreadsStore.ThreadListingState.Listing) threadListingState).getThreads();
        map2 = this.this$0.listings;
        if (z2) {
            listing = ArchivedThreadsStore.ThreadListingState.Listing.copy$default((ArchivedThreadsStore.ThreadListingState.Listing) threadListingState, null, false, true, 3, null);
        } else {
            listing = new ArchivedThreadsStore.ThreadListingState.Listing(emptyList, true, true);
        }
        map2.put(pair, listing);
        this.this$0.markChanged();
        map3 = this.this$0.fetchSubscriptions;
        Subscription subscription = (Subscription) map3.get(pair);
        if (subscription != null) {
            subscription.unsubscribe();
        }
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RetryWithDelay.restRetry$default(RetryWithDelay.INSTANCE, this.$threadListingType.fetchNext(this.$channelId, emptyList), 0L, null, null, 7, null), false, 1, null), this.this$0.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new AnonymousClass1(pair), (r18 & 8) != 0 ? null : new AnonymousClass2(pair), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass3(pair, emptyList));
    }
}
