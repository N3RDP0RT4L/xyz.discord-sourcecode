package com.discord.stores;

import com.discord.api.directory.DirectoryEntryEvent;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.stores.utilities.RestCallState;
import com.discord.stores.utilities.Success;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreDirectories.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0012\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/stores/utilities/RestCallState;", "", "Lcom/discord/api/directory/DirectoryEntryEvent;", "response", "", "invoke", "(Lcom/discord/stores/utilities/RestCallState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreDirectories$fetchGuildScheduledEventsForChannel$1 extends o implements Function1<RestCallState<? extends List<? extends DirectoryEntryEvent>>, Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ StoreDirectories this$0;

    /* compiled from: StoreDirectories.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreDirectories$fetchGuildScheduledEventsForChannel$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ RestCallState $response;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(RestCallState restCallState) {
            super(0);
            this.$response = restCallState;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            Map map;
            StoreGuildScheduledEvents storeGuildScheduledEvents;
            map = StoreDirectories$fetchGuildScheduledEventsForChannel$1.this.this$0.directoryGuildScheduledEventsMap;
            map.put(Long.valueOf(StoreDirectories$fetchGuildScheduledEventsForChannel$1.this.$channelId), this.$response);
            RestCallState restCallState = this.$response;
            if (restCallState instanceof Success) {
                ArrayList arrayList = new ArrayList();
                for (DirectoryEntryEvent directoryEntryEvent : (Iterable) ((Success) restCallState).invoke()) {
                    GuildScheduledEvent a = directoryEntryEvent.a();
                    Pair pair = null;
                    if (!(a.o() != null)) {
                        a = null;
                    }
                    if (a != null) {
                        pair = d0.o.to(Long.valueOf(a.h()), Long.valueOf(a.i()));
                    }
                    if (pair != null) {
                        arrayList.add(pair);
                    }
                }
                storeGuildScheduledEvents = StoreDirectories$fetchGuildScheduledEventsForChannel$1.this.this$0.guildScheduledEventsStore;
                storeGuildScheduledEvents.addMeRsvpsForEvent(arrayList);
            }
            StoreDirectories$fetchGuildScheduledEventsForChannel$1.this.this$0.markChanged();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreDirectories$fetchGuildScheduledEventsForChannel$1(StoreDirectories storeDirectories, long j) {
        super(1);
        this.this$0 = storeDirectories;
        this.$channelId = j;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(RestCallState<? extends List<? extends DirectoryEntryEvent>> restCallState) {
        invoke2((RestCallState<? extends List<DirectoryEntryEvent>>) restCallState);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(RestCallState<? extends List<DirectoryEntryEvent>> restCallState) {
        Dispatcher dispatcher;
        m.checkNotNullParameter(restCallState, "response");
        dispatcher = this.this$0.dispatcher;
        dispatcher.schedule(new AnonymousClass1(restCallState));
    }
}
