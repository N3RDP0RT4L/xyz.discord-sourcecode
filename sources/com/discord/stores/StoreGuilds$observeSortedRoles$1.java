package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.api.role.GuildRole;
import com.discord.utilities.guilds.RoleUtils;
import d0.t.u;
import j0.k.b;
import j0.l.e.k;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: StoreGuilds.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0003\u0010\n\u001aB\u0012\u001a\b\u0001\u0012\u0016\u0012\u0004\u0012\u00020\u0003 \u0004*\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00070\u0007 \u0004* \u0012\u001a\b\u0001\u0012\u0016\u0012\u0004\u0012\u00020\u0003 \u0004*\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00070\u0007\u0018\u00010\u00060\u00062.\u0010\u0005\u001a*\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003 \u0004*\u0014\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\b\u0010\t"}, d2 = {"", "", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "kotlin.jvm.PlatformType", "roles", "Lrx/Observable;", "", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Map;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuilds$observeSortedRoles$1<T, R> implements b<Map<Long, ? extends GuildRole>, Observable<? extends List<? extends GuildRole>>> {
    public static final StoreGuilds$observeSortedRoles$1 INSTANCE = new StoreGuilds$observeSortedRoles$1();

    @Override // j0.k.b
    public /* bridge */ /* synthetic */ Observable<? extends List<? extends GuildRole>> call(Map<Long, ? extends GuildRole> map) {
        return call2((Map<Long, GuildRole>) map);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final Observable<? extends List<GuildRole>> call2(Map<Long, GuildRole> map) {
        return new k(u.sortedWith(map.values(), RoleUtils.getROLE_COMPARATOR()));
    }
}
