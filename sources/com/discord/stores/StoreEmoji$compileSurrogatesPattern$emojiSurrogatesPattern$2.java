package com.discord.stores;

import d0.z.d.m;
import d0.z.d.o;
import java.util.regex.Pattern;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreEmoji.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\r\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "it", "", "invoke", "(Ljava/lang/String;)Ljava/lang/CharSequence;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreEmoji$compileSurrogatesPattern$emojiSurrogatesPattern$2 extends o implements Function1<String, CharSequence> {
    public static final StoreEmoji$compileSurrogatesPattern$emojiSurrogatesPattern$2 INSTANCE = new StoreEmoji$compileSurrogatesPattern$emojiSurrogatesPattern$2();

    public StoreEmoji$compileSurrogatesPattern$emojiSurrogatesPattern$2() {
        super(1);
    }

    public final CharSequence invoke(String str) {
        m.checkNotNullParameter(str, "it");
        String quote = Pattern.quote(str);
        m.checkNotNullExpressionValue(quote, "Pattern.quote(it)");
        return quote;
    }
}
