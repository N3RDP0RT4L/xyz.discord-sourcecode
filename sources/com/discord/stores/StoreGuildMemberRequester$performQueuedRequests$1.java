package com.discord.stores;

import com.discord.utilities.lazy.requester.GuildMemberRequestManager;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreGuildMemberRequester.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildMemberRequester$performQueuedRequests$1 extends o implements Function0<Unit> {
    public final /* synthetic */ StoreGuildMemberRequester this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGuildMemberRequester$performQueuedRequests$1(StoreGuildMemberRequester storeGuildMemberRequester) {
        super(0);
        this.this$0 = storeGuildMemberRequester;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        GuildMemberRequestManager guildMemberRequestManager;
        guildMemberRequestManager = this.this$0.requestManager;
        guildMemberRequestManager.flush();
    }
}
