package com.discord.stores;

import android.content.Context;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.models.domain.ModelMuteConfig;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreThreadsJoined;
import com.discord.stores.StoreUserGuildSettings;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreUserGuildSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserGuildSettings$setChannelMuted$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ Context $context;
    public final /* synthetic */ ModelMuteConfig $muteConfig;
    public final /* synthetic */ boolean $muted;
    public final /* synthetic */ StoreUserGuildSettings this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreUserGuildSettings$setChannelMuted$1(StoreUserGuildSettings storeUserGuildSettings, long j, Context context, boolean z2, ModelMuteConfig modelMuteConfig) {
        super(0);
        this.this$0 = storeUserGuildSettings;
        this.$channelId = j;
        this.$context = context;
        this.$muted = z2;
        this.$muteConfig = modelMuteConfig;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreChannels storeChannels;
        StoreUserGuildSettings.SettingsUpdateType settingsUpdateType;
        StoreThreadsJoined storeThreadsJoined;
        storeChannels = this.this$0.storeChannels;
        Channel findChannelByIdInternal$app_productionGoogleRelease = storeChannels.findChannelByIdInternal$app_productionGoogleRelease(this.$channelId);
        if (findChannelByIdInternal$app_productionGoogleRelease != null) {
            settingsUpdateType = this.this$0.getSettingsUpdateType(findChannelByIdInternal$app_productionGoogleRelease);
            if (ChannelUtils.C(findChannelByIdInternal$app_productionGoogleRelease)) {
                storeThreadsJoined = this.this$0.storeThreadsJoined;
                StoreThreadsJoined.JoinedThread joinedThread = storeThreadsJoined.getJoinedThread(findChannelByIdInternal$app_productionGoogleRelease.h());
                this.this$0.updateThreadMemberSettings(this.$context, findChannelByIdInternal$app_productionGoogleRelease.h(), findChannelByIdInternal$app_productionGoogleRelease.r(), new RestAPIParams.ThreadMemberSettings(null, Boolean.valueOf(this.$muted), this.$muteConfig, 1, null), joinedThread != null ? joinedThread.getFlags() : 0);
                return;
            }
            this.this$0.updateUserGuildSettings(this.$context, findChannelByIdInternal$app_productionGoogleRelease.f(), new RestAPIParams.UserGuildSettings(findChannelByIdInternal$app_productionGoogleRelease.h(), new RestAPIParams.UserGuildSettings.ChannelOverride(Boolean.valueOf(this.$muted), this.$muteConfig, null, 4, null)), settingsUpdateType);
        }
    }
}
