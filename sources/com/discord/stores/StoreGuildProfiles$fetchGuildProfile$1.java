package com.discord.stores;

import com.discord.api.guild.preview.GuildPreview;
import com.discord.stores.StoreGuildProfiles;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreGuildProfiles.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildProfiles$fetchGuildProfile$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ StoreGuildProfiles this$0;

    /* compiled from: StoreGuildProfiles.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/guild/preview/GuildPreview;", "guildPreview", "", "invoke", "(Lcom/discord/api/guild/preview/GuildPreview;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreGuildProfiles$fetchGuildProfile$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<GuildPreview, Unit> {

        /* compiled from: StoreGuildProfiles.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreGuildProfiles$fetchGuildProfile$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C02041 extends o implements Function0<Unit> {
            public final /* synthetic */ GuildPreview $guildPreview;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C02041(GuildPreview guildPreview) {
                super(0);
                this.$guildPreview = guildPreview;
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                StoreGuildProfiles$fetchGuildProfile$1.this.this$0.handleGuildProfileFetchSuccess(this.$guildPreview);
            }
        }

        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(GuildPreview guildPreview) {
            invoke2(guildPreview);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(GuildPreview guildPreview) {
            Dispatcher dispatcher;
            m.checkNotNullParameter(guildPreview, "guildPreview");
            dispatcher = StoreGuildProfiles$fetchGuildProfile$1.this.this$0.dispatcher;
            dispatcher.schedule(new C02041(guildPreview));
        }
    }

    /* compiled from: StoreGuildProfiles.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreGuildProfiles$fetchGuildProfile$1$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2 extends o implements Function1<Error, Unit> {

        /* compiled from: StoreGuildProfiles.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreGuildProfiles$fetchGuildProfile$1$2$1  reason: invalid class name */
        /* loaded from: classes.dex */
        public static final class AnonymousClass1 extends o implements Function0<Unit> {
            public AnonymousClass1() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                StoreGuildProfiles$fetchGuildProfile$1 storeGuildProfiles$fetchGuildProfile$1 = StoreGuildProfiles$fetchGuildProfile$1.this;
                storeGuildProfiles$fetchGuildProfile$1.this$0.handleGuildProfileFetchFailed(storeGuildProfiles$fetchGuildProfile$1.$guildId);
            }
        }

        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Error error) {
            invoke2(error);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Error error) {
            Dispatcher dispatcher;
            m.checkNotNullParameter(error, "it");
            dispatcher = StoreGuildProfiles$fetchGuildProfile$1.this.this$0.dispatcher;
            dispatcher.schedule(new AnonymousClass1());
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGuildProfiles$fetchGuildProfile$1(StoreGuildProfiles storeGuildProfiles, long j) {
        super(0);
        this.this$0 = storeGuildProfiles;
        this.$guildId = j;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        Map map;
        map = this.this$0.guildProfilesState;
        StoreGuildProfiles.GuildProfileData guildProfileData = (StoreGuildProfiles.GuildProfileData) map.get(Long.valueOf(this.$guildId));
        if ((guildProfileData != null ? guildProfileData.getFetchState() : null) != StoreGuildProfiles.FetchStates.FETCHING) {
            this.this$0.handleGuildProfileFetchStart(this.$guildId);
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().getGuildPreview(this.$guildId), false, 1, null), this.this$0.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new AnonymousClass2(), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
        }
    }
}
