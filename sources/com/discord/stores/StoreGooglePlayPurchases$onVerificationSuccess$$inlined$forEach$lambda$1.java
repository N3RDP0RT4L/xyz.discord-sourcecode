package com.discord.stores;

import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreGooglePlayPurchases;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import rx.subjects.PublishSubject;
/* compiled from: StoreGooglePlayPurchases.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002¨\u0006\u0003"}, d2 = {"", "invoke", "()V", "com/discord/stores/StoreGooglePlayPurchases$onVerificationSuccess$1$1", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGooglePlayPurchases$onVerificationSuccess$$inlined$forEach$lambda$1 extends o implements Function0<Unit> {
    public final /* synthetic */ String $sku;
    public final /* synthetic */ StoreGooglePlayPurchases.VerificationResult $verificationResult$inlined;
    public final /* synthetic */ StoreGooglePlayPurchases this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGooglePlayPurchases$onVerificationSuccess$$inlined$forEach$lambda$1(String str, StoreGooglePlayPurchases storeGooglePlayPurchases, StoreGooglePlayPurchases.VerificationResult verificationResult) {
        super(0);
        this.$sku = str;
        this.this$0 = storeGooglePlayPurchases;
        this.$verificationResult$inlined = verificationResult;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        PublishSubject publishSubject;
        publishSubject = this.this$0.eventSubject;
        String str = this.$sku;
        m.checkNotNullExpressionValue(str, "sku");
        RestAPIParams.VerifyPurchaseResponse apiResponse = this.$verificationResult$inlined.getApiResponse();
        String str2 = null;
        Long valueOf = apiResponse != null ? Long.valueOf(apiResponse.getVerifiedSkuId()) : null;
        RestAPIParams.VerifyPurchaseResponse apiResponse2 = this.$verificationResult$inlined.getApiResponse();
        Long subscriptionPlanId = apiResponse2 != null ? apiResponse2.getSubscriptionPlanId() : null;
        RestAPIParams.VerifyPurchaseResponse apiResponse3 = this.$verificationResult$inlined.getApiResponse();
        if (apiResponse3 != null) {
            str2 = apiResponse3.getGiftCode();
        }
        publishSubject.k.onNext(new StoreGooglePlayPurchases.Event.PurchaseQuerySuccess(str, valueOf, subscriptionPlanId, str2));
        this.this$0.queryState = StoreGooglePlayPurchases.QueryState.NotInProgress.INSTANCE;
        this.this$0.markChanged();
    }
}
