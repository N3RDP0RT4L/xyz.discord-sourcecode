package com.discord.stores;

import com.discord.stores.StoreGuildTemplates;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreGuildTemplates.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildTemplates$maybeInitTemplateState$1 extends o implements Function0<Unit> {
    public final /* synthetic */ String $guildTemplateCode;
    public final /* synthetic */ StoreGuildTemplates this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGuildTemplates$maybeInitTemplateState$1(StoreGuildTemplates storeGuildTemplates, String str) {
        super(0);
        this.this$0 = storeGuildTemplates;
        this.$guildTemplateCode = str;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        Map map;
        Map map2;
        Map map3;
        map = this.this$0.guildTemplatesByCode;
        if (map.containsKey(this.$guildTemplateCode)) {
            map3 = this.this$0.guildTemplatesByCode;
            if (!(map3.get(this.$guildTemplateCode) instanceof StoreGuildTemplates.GuildTemplateState.LoadFailed)) {
                return;
            }
        }
        map2 = this.this$0.guildTemplatesByCode;
        map2.put(this.$guildTemplateCode, StoreGuildTemplates.GuildTemplateState.Loading.INSTANCE);
        this.this$0.markChanged();
        this.this$0.requestGuildTemplate(this.$guildTemplateCode);
    }
}
