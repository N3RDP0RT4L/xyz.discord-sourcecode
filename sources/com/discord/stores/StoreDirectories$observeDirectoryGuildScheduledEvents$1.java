package com.discord.stores;

import com.discord.api.directory.DirectoryEntryEvent;
import com.discord.stores.utilities.RestCallState;
import d0.z.d.o;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreDirectories.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\b\u001a\u001e\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00030\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/stores/utilities/RestCallState;", "", "Lcom/discord/api/directory/DirectoryEntryEvent;", "invoke", "()Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreDirectories$observeDirectoryGuildScheduledEvents$1 extends o implements Function0<Map<Long, ? extends RestCallState<? extends List<? extends DirectoryEntryEvent>>>> {
    public final /* synthetic */ StoreDirectories this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreDirectories$observeDirectoryGuildScheduledEvents$1(StoreDirectories storeDirectories) {
        super(0);
        this.this$0 = storeDirectories;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Map<Long, ? extends RestCallState<? extends List<? extends DirectoryEntryEvent>>> invoke() {
        Map<Long, ? extends RestCallState<? extends List<? extends DirectoryEntryEvent>>> map;
        map = this.this$0.directoryGuildScheduledEventsMapSnapshot;
        return map;
    }
}
