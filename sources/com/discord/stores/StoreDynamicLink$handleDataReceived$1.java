package com.discord.stores;

import com.discord.stores.StoreDynamicLink;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreDynamicLink.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreDynamicLink$handleDataReceived$1 extends o implements Function0<Unit> {
    public final /* synthetic */ StoreDynamicLink.DynamicLinkData $data;
    public final /* synthetic */ StoreDynamicLink this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreDynamicLink$handleDataReceived$1(StoreDynamicLink storeDynamicLink, StoreDynamicLink.DynamicLinkData dynamicLinkData) {
        super(0);
        this.this$0 = storeDynamicLink;
        this.$data = dynamicLinkData;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreStream storeStream;
        StoreStream storeStream2;
        storeStream = this.this$0.stream;
        StoreAuthentication authentication$app_productionGoogleRelease = storeStream.getAuthentication$app_productionGoogleRelease();
        StoreDynamicLink.DynamicLinkData dynamicLinkData = this.$data;
        String str = null;
        authentication$app_productionGoogleRelease.setFingerprint(dynamicLinkData != null ? dynamicLinkData.getFingerprint() : null, false);
        StoreDynamicLink.DynamicLinkData dynamicLinkData2 = this.$data;
        if (dynamicLinkData2 != null) {
            str = dynamicLinkData2.getAuthToken();
        }
        if (str != null) {
            storeStream2 = this.this$0.stream;
            storeStream2.getAuthentication$app_productionGoogleRelease().setAuthed(this.$data.getAuthToken());
        }
    }
}
