package com.discord.stores;

import com.discord.models.domain.ModelCall;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreCalls.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/ModelCall;", "it", "", "invoke", "(Lcom/discord/models/domain/ModelCall;)Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreCalls$findCall$1 extends o implements Function1<ModelCall, Boolean> {
    public static final StoreCalls$findCall$1 INSTANCE = new StoreCalls$findCall$1();

    public StoreCalls$findCall$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Boolean invoke(ModelCall modelCall) {
        return Boolean.valueOf(invoke2(modelCall));
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final boolean invoke2(ModelCall modelCall) {
        return modelCall != null;
    }
}
