package com.discord.stores;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.Context;
import b.a.d.o;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.app.AppLog;
import com.discord.models.message.Message;
import com.discord.stores.StoreChat;
import com.discord.stores.StoreMessagesLoader;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$1;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$2;
import d0.t.n0;
import d0.z.d.m;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Subscription;
import rx.subjects.BehaviorSubject;
import rx.subjects.SerializedSubject;
/* compiled from: StoreMessagesLoader.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000¢\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u0000 k2\u00020\u0001:\u0003lmkB\u0017\u0012\u0006\u0010d\u001a\u00020c\u0012\u0006\u0010O\u001a\u00020N¢\u0006\u0004\bi\u0010jJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J!\u0010\u000b\u001a\u00020\u00042\u0010\u0010\n\u001a\f\u0012\b\u0012\u00060\bj\u0002`\t0\u0007H\u0002¢\u0006\u0004\b\u000b\u0010\fJO\u0010\u0015\u001a\u00020\u00042\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r2\n\u0010\u0010\u001a\u00060\bj\u0002`\t2\f\b\u0002\u0010\u0012\u001a\u00060\bj\u0002`\u00112\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\bH\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u001b\u0010\u0017\u001a\u00020\u00042\n\u0010\u0010\u001a\u00060\bj\u0002`\tH\u0002¢\u0006\u0004\b\u0017\u0010\u0018J[\u0010 \u001a\u00020\u00042\b\b\u0002\u0010\u0019\u001a\u00020\b2\b\b\u0002\u0010\u001b\u001a\u00020\u001a2\b\b\u0002\u0010\u001c\u001a\u00020\u001a2\b\b\u0002\u0010\u001d\u001a\u00020\u001a2\u0010\b\u0002\u0010\u001e\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t2\u0010\b\u0002\u0010\u001f\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\u0011H\u0002¢\u0006\u0004\b \u0010!J/\u0010%\u001a\u00020\u00042\n\u0010\u0010\u001a\u00060\bj\u0002`\t2\u0012\u0010$\u001a\u000e\u0012\u0004\u0012\u00020#\u0012\u0004\u0012\u00020#0\"H\u0002¢\u0006\u0004\b%\u0010&J\u000f\u0010'\u001a\u00020\u0004H\u0002¢\u0006\u0004\b'\u0010(J\u0017\u0010+\u001a\u00020\u00042\u0006\u0010*\u001a\u00020)H\u0002¢\u0006\u0004\b+\u0010,J\u0013\u0010/\u001a\b\u0012\u0004\u0012\u00020.0-¢\u0006\u0004\b/\u00100J\u001f\u00101\u001a\b\u0012\u0004\u0012\u00020#0-2\n\u0010\u0010\u001a\u00060\bj\u0002`\t¢\u0006\u0004\b1\u00102J\u0017\u00103\u001a\f\u0012\b\u0012\u00060\bj\u0002`\u00110-¢\u0006\u0004\b3\u00100J\u0013\u00104\u001a\b\u0012\u0004\u0012\u00020\u001a0-¢\u0006\u0004\b4\u00100J\u0017\u00107\u001a\u00020\u00042\u0006\u00106\u001a\u000205H\u0016¢\u0006\u0004\b7\u00108J\r\u00109\u001a\u00020\u0004¢\u0006\u0004\b9\u0010(J\r\u0010:\u001a\u00020\u0004¢\u0006\u0004\b:\u0010(J%\u0010;\u001a\u00020\u00042\n\u0010\u0010\u001a\u00060\bj\u0002`\t2\n\u0010\u0012\u001a\u00060\bj\u0002`\u0011¢\u0006\u0004\b;\u0010<J\u0015\u0010>\u001a\u00020\u00042\u0006\u0010=\u001a\u00020\u001a¢\u0006\u0004\b>\u0010?J\u0017\u0010A\u001a\u00020\u00042\b\u0010@\u001a\u0004\u0018\u00010)¢\u0006\u0004\bA\u0010,J\u0015\u0010C\u001a\u00020\u00042\u0006\u0010B\u001a\u00020\u001a¢\u0006\u0004\bC\u0010?J\u001b\u0010E\u001a\u00020\u00042\n\u0010D\u001a\u00060\bj\u0002`\tH\u0007¢\u0006\u0004\bE\u0010\u0018R\u0018\u0010G\u001a\u0004\u0018\u00010F8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bG\u0010HRr\u0010L\u001a^\u0012,\u0012*\u0012\b\u0012\u00060\bj\u0002`\t\u0012\u0004\u0012\u00020# K*\u0014\u0012\b\u0012\u00060\bj\u0002`\t\u0012\u0004\u0012\u00020#\u0018\u00010J0J\u0012,\u0012*\u0012\b\u0012\u00060\bj\u0002`\t\u0012\u0004\u0012\u00020# K*\u0014\u0012\b\u0012\u00060\bj\u0002`\t\u0012\u0004\u0012\u00020#\u0018\u00010J0J0I8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bL\u0010MR\u0016\u0010O\u001a\u00020N8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bO\u0010PR2\u0010Q\u001a\u001e\u0012\f\u0012\n\u0018\u00010\bj\u0004\u0018\u0001`\u0011\u0012\f\u0012\n\u0018\u00010\bj\u0004\u0018\u0001`\u00110I8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bQ\u0010MR \u0010\n\u001a\f\u0012\b\u0012\u00060\bj\u0002`\t0\u00078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\n\u0010RR\u0016\u0010B\u001a\u00020\u001a8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bB\u0010SR\u0016\u0010T\u001a\u00020\u001a8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bT\u0010SR\u0018\u0010U\u001a\u0004\u0018\u00010F8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bU\u0010HR\u0016\u0010W\u001a\u00020V8\u0002@\u0002X\u0082D¢\u0006\u0006\n\u0004\bW\u0010XR2\u0010Y\u001a\u001e\u0012\f\u0012\n K*\u0004\u0018\u00010\u001a0\u001a\u0012\f\u0012\n K*\u0004\u0018\u00010\u001a0\u001a0I8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bY\u0010MR\u0016\u0010Z\u001a\u00020\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bZ\u0010[R\u0016\u0010\\\u001a\u00020\u001a8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\\\u0010SR:\u0010_\u001a&\u0012\b\u0012\u00060\bj\u0002`\t\u0012\u0004\u0012\u00020#0]j\u0012\u0012\b\u0012\u00060\bj\u0002`\t\u0012\u0004\u0012\u00020#`^8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b_\u0010`R\u0016\u0010a\u001a\u00020V8\u0002@\u0002X\u0082D¢\u0006\u0006\n\u0004\ba\u0010XR\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0003\u0010bR\u001a\u0010D\u001a\u00060\bj\u0002`\t8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bD\u0010[R\u0016\u0010d\u001a\u00020c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bd\u0010eR2\u0010f\u001a\u001e\u0012\f\u0012\n K*\u0004\u0018\u00010.0.\u0012\f\u0012\n K*\u0004\u0018\u00010.0.0I8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bf\u0010MR\u0016\u0010g\u001a\u00020V8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bg\u0010XR\u0016\u0010h\u001a\u00020\b8\u0002@\u0002X\u0082D¢\u0006\u0006\n\u0004\bh\u0010[¨\u0006n"}, d2 = {"Lcom/discord/stores/StoreMessagesLoader;", "Lcom/discord/stores/Store;", "Lcom/discord/stores/StoreChat$InteractionState;", "interactionState", "", "handleChatInteraction", "(Lcom/discord/stores/StoreChat$InteractionState;)V", "", "", "Lcom/discord/primitives/ChannelId;", "detachedChannels", "handleChatDetached", "(Ljava/util/Set;)V", "", "Lcom/discord/models/message/Message;", "messages", "channelId", "Lcom/discord/primitives/MessageId;", "messageId", "before", "after", "handleLoadedMessages", "(Ljava/util/List;JJLjava/lang/Long;Ljava/lang/Long;)V", "handleLoadMessagesError", "(J)V", "delay", "", "force", "resetRetry", "resetDelay", "targetChannelId", "targetMessageId", "tryLoadMessages", "(JZZZLjava/lang/Long;Ljava/lang/Long;)V", "Lkotlin/Function1;", "Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;", "updater", "channelLoadedStateUpdate", "(JLkotlin/jvm/functions/Function1;)V", "channelLoadedStatesReset", "()V", "", "message", "log", "(Ljava/lang/String;)V", "Lrx/Observable;", "Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;", "get", "()Lrx/Observable;", "getMessagesLoadedState", "(J)Lrx/Observable;", "getScrollTo", "observeChannelMessagesLoading", "Landroid/content/Context;", "context", "init", "(Landroid/content/Context;)V", "clearScrollTo", "requestNewestMessages", "jumpToMessage", "(JJ)V", "connected", "handleConnected", "(Z)V", "authToken", "handleAuthToken", "backgrounded", "handleBackgrounded", "selectedChannelId", "handleChannelSelected", "Lrx/Subscription;", "delayLoadingMessagesSubscription", "Lrx/Subscription;", "Lrx/subjects/SerializedSubject;", "", "kotlin.jvm.PlatformType", "channelLoadedStateSubject", "Lrx/subjects/SerializedSubject;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "scrollToSubject", "Ljava/util/Set;", "Z", "hasConnected", "loadingMessagesSubscription", "", "loadingMessagesRetryJitter", "I", "channelMessagesLoadingSubject", "loadingMessagesRetryDelayMillis", "J", "authed", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "channelLoadedStates", "Ljava/util/HashMap;", "messageRequestSize", "Lcom/discord/stores/StoreChat$InteractionState;", "Lcom/discord/stores/StoreStream;", "stream", "Lcom/discord/stores/StoreStream;", "channelMessageChunksSubject", "loadingMessagesRetryMax", "loadingMessagesRetryDelayDefault", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;)V", "Companion", "ChannelChunk", "ChannelLoadedState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessagesLoader extends Store {
    public static final Companion Companion = new Companion(null);
    public static final long SCROLL_TO_LAST_UNREAD = 0;
    public static final long SCROLL_TO_LATEST = 1;
    private boolean authed;
    private final SerializedSubject<Map<Long, ChannelLoadedState>, Map<Long, ChannelLoadedState>> channelLoadedStateSubject;
    private final HashMap<Long, ChannelLoadedState> channelLoadedStates;
    private Subscription delayLoadingMessagesSubscription;
    private final Dispatcher dispatcher;
    private boolean hasConnected;
    private StoreChat.InteractionState interactionState;
    private Subscription loadingMessagesSubscription;
    private long selectedChannelId;
    private final StoreStream stream;
    private final int messageRequestSize = 50;
    private final SerializedSubject<ChannelChunk, ChannelChunk> channelMessageChunksSubject = new SerializedSubject<>(BehaviorSubject.k0());
    private final SerializedSubject<Long, Long> scrollToSubject = new SerializedSubject<>(BehaviorSubject.l0(null));
    private final SerializedSubject<Boolean, Boolean> channelMessagesLoadingSubject = new SerializedSubject<>(BehaviorSubject.l0(Boolean.FALSE));
    private Set<Long> detachedChannels = n0.emptySet();
    private boolean backgrounded = true;
    private final long loadingMessagesRetryDelayDefault = 1500;
    private final int loadingMessagesRetryJitter = 2000;
    private long loadingMessagesRetryDelayMillis = 1500;
    private int loadingMessagesRetryMax = 30000;

    /* compiled from: StoreMessagesLoader.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\r\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001BA\u0012\n\u0010\u0010\u001a\u00060\u0002j\u0002`\u0003\u0012\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0006\u0010\u0012\u001a\u00020\n\u0012\u0006\u0010\u0013\u001a\u00020\n\u0012\u0006\u0010\u0014\u001a\u00020\n\u0012\u0006\u0010\u0015\u001a\u00020\n¢\u0006\u0004\b&\u0010'J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0016\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\r\u0010\fJ\u0010\u0010\u000e\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000e\u0010\fJ\u0010\u0010\u000f\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000f\u0010\fJV\u0010\u0016\u001a\u00020\u00002\f\b\u0002\u0010\u0010\u001a\u00060\u0002j\u0002`\u00032\u000e\b\u0002\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\b\b\u0002\u0010\u0012\u001a\u00020\n2\b\b\u0002\u0010\u0013\u001a\u00020\n2\b\b\u0002\u0010\u0014\u001a\u00020\n2\b\b\u0002\u0010\u0015\u001a\u00020\nHÆ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u001c\u001a\u00020\u001bHÖ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u001a\u0010\u001f\u001a\u00020\n2\b\u0010\u001e\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001f\u0010 R\u001d\u0010\u0010\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010!\u001a\u0004\b\"\u0010\u0005R\u0019\u0010\u0014\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010#\u001a\u0004\b\u0014\u0010\fR\u0019\u0010\u0013\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010#\u001a\u0004\b\u0013\u0010\fR\u0019\u0010\u0015\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010#\u001a\u0004\b\u0015\u0010\fR\u0019\u0010\u0012\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010#\u001a\u0004\b\u0012\u0010\fR\u001f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00070\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010$\u001a\u0004\b%\u0010\t¨\u0006("}, d2 = {"Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;", "", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "", "Lcom/discord/models/message/Message;", "component2", "()Ljava/util/List;", "", "component3", "()Z", "component4", "component5", "component6", "channelId", "messages", "isInitial", "isPresent", "isAppendingTop", "isJump", "copy", "(JLjava/util/List;ZZZZ)Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "J", "getChannelId", "Z", "Ljava/util/List;", "getMessages", HookHelper.constructorName, "(JLjava/util/List;ZZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ChannelChunk {
        private final long channelId;
        private final boolean isAppendingTop;
        private final boolean isInitial;
        private final boolean isJump;
        private final boolean isPresent;
        private final List<Message> messages;

        public ChannelChunk(long j, List<Message> list, boolean z2, boolean z3, boolean z4, boolean z5) {
            m.checkNotNullParameter(list, "messages");
            this.channelId = j;
            this.messages = list;
            this.isInitial = z2;
            this.isPresent = z3;
            this.isAppendingTop = z4;
            this.isJump = z5;
        }

        public final long component1() {
            return this.channelId;
        }

        public final List<Message> component2() {
            return this.messages;
        }

        public final boolean component3() {
            return this.isInitial;
        }

        public final boolean component4() {
            return this.isPresent;
        }

        public final boolean component5() {
            return this.isAppendingTop;
        }

        public final boolean component6() {
            return this.isJump;
        }

        public final ChannelChunk copy(long j, List<Message> list, boolean z2, boolean z3, boolean z4, boolean z5) {
            m.checkNotNullParameter(list, "messages");
            return new ChannelChunk(j, list, z2, z3, z4, z5);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ChannelChunk)) {
                return false;
            }
            ChannelChunk channelChunk = (ChannelChunk) obj;
            return this.channelId == channelChunk.channelId && m.areEqual(this.messages, channelChunk.messages) && this.isInitial == channelChunk.isInitial && this.isPresent == channelChunk.isPresent && this.isAppendingTop == channelChunk.isAppendingTop && this.isJump == channelChunk.isJump;
        }

        public final long getChannelId() {
            return this.channelId;
        }

        public final List<Message> getMessages() {
            return this.messages;
        }

        public int hashCode() {
            int a = b.a(this.channelId) * 31;
            List<Message> list = this.messages;
            int hashCode = (a + (list != null ? list.hashCode() : 0)) * 31;
            boolean z2 = this.isInitial;
            int i = 1;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (hashCode + i2) * 31;
            boolean z3 = this.isPresent;
            if (z3) {
                z3 = true;
            }
            int i5 = z3 ? 1 : 0;
            int i6 = z3 ? 1 : 0;
            int i7 = (i4 + i5) * 31;
            boolean z4 = this.isAppendingTop;
            if (z4) {
                z4 = true;
            }
            int i8 = z4 ? 1 : 0;
            int i9 = z4 ? 1 : 0;
            int i10 = (i7 + i8) * 31;
            boolean z5 = this.isJump;
            if (!z5) {
                i = z5 ? 1 : 0;
            }
            return i10 + i;
        }

        public final boolean isAppendingTop() {
            return this.isAppendingTop;
        }

        public final boolean isInitial() {
            return this.isInitial;
        }

        public final boolean isJump() {
            return this.isJump;
        }

        public final boolean isPresent() {
            return this.isPresent;
        }

        public String toString() {
            StringBuilder R = a.R("ChannelChunk(channelId=");
            R.append(this.channelId);
            R.append(", messages=");
            R.append(this.messages);
            R.append(", isInitial=");
            R.append(this.isInitial);
            R.append(", isPresent=");
            R.append(this.isPresent);
            R.append(", isAppendingTop=");
            R.append(this.isAppendingTop);
            R.append(", isJump=");
            return a.M(R, this.isJump, ")");
        }
    }

    /* compiled from: StoreMessagesLoader.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001BA\u0012\b\b\u0002\u0010\f\u001a\u00020\u0002\u0012\b\b\u0002\u0010\r\u001a\u00020\u0002\u0012\b\b\u0002\u0010\u000e\u001a\u00020\u0002\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u0002\u0012\u0010\b\u0002\u0010\u0010\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0004J\u0018\u0010\n\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJJ\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\b\b\u0002\u0010\r\u001a\u00020\u00022\b\b\u0002\u0010\u000e\u001a\u00020\u00022\b\b\u0002\u0010\u000f\u001a\u00020\u00022\u0010\b\u0002\u0010\u0010\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\tHÆ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0017\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u001a\u0010\u001a\u001a\u00020\u00022\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001a\u0010\u001bR\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001c\u001a\u0004\b\u000f\u0010\u0004R\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001c\u001a\u0004\b\u000e\u0010\u0004R\u0019\u0010\r\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001c\u001a\u0004\b\r\u0010\u0004R!\u0010\u0010\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001d\u001a\u0004\b\u001e\u0010\u000bR\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001c\u001a\u0004\b\f\u0010\u0004¨\u0006!"}, d2 = {"Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;", "", "", "component1", "()Z", "component2", "component3", "component4", "", "Lcom/discord/primitives/MessageId;", "component5", "()Ljava/lang/Long;", "isInitialMessagesLoaded", "isOldestMessagesLoaded", "isLoadingMessages", "isTouchedSinceLastJump", "newestSentByUserMessageId", "copy", "(ZZZZLjava/lang/Long;)Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Ljava/lang/Long;", "getNewestSentByUserMessageId", HookHelper.constructorName, "(ZZZZLjava/lang/Long;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ChannelLoadedState {
        private final boolean isInitialMessagesLoaded;
        private final boolean isLoadingMessages;
        private final boolean isOldestMessagesLoaded;
        private final boolean isTouchedSinceLastJump;
        private final Long newestSentByUserMessageId;

        public ChannelLoadedState() {
            this(false, false, false, false, null, 31, null);
        }

        public ChannelLoadedState(boolean z2, boolean z3, boolean z4, boolean z5, Long l) {
            this.isInitialMessagesLoaded = z2;
            this.isOldestMessagesLoaded = z3;
            this.isLoadingMessages = z4;
            this.isTouchedSinceLastJump = z5;
            this.newestSentByUserMessageId = l;
        }

        public static /* synthetic */ ChannelLoadedState copy$default(ChannelLoadedState channelLoadedState, boolean z2, boolean z3, boolean z4, boolean z5, Long l, int i, Object obj) {
            if ((i & 1) != 0) {
                z2 = channelLoadedState.isInitialMessagesLoaded;
            }
            if ((i & 2) != 0) {
                z3 = channelLoadedState.isOldestMessagesLoaded;
            }
            boolean z6 = z3;
            if ((i & 4) != 0) {
                z4 = channelLoadedState.isLoadingMessages;
            }
            boolean z7 = z4;
            if ((i & 8) != 0) {
                z5 = channelLoadedState.isTouchedSinceLastJump;
            }
            boolean z8 = z5;
            if ((i & 16) != 0) {
                l = channelLoadedState.newestSentByUserMessageId;
            }
            return channelLoadedState.copy(z2, z6, z7, z8, l);
        }

        public final boolean component1() {
            return this.isInitialMessagesLoaded;
        }

        public final boolean component2() {
            return this.isOldestMessagesLoaded;
        }

        public final boolean component3() {
            return this.isLoadingMessages;
        }

        public final boolean component4() {
            return this.isTouchedSinceLastJump;
        }

        public final Long component5() {
            return this.newestSentByUserMessageId;
        }

        public final ChannelLoadedState copy(boolean z2, boolean z3, boolean z4, boolean z5, Long l) {
            return new ChannelLoadedState(z2, z3, z4, z5, l);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ChannelLoadedState)) {
                return false;
            }
            ChannelLoadedState channelLoadedState = (ChannelLoadedState) obj;
            return this.isInitialMessagesLoaded == channelLoadedState.isInitialMessagesLoaded && this.isOldestMessagesLoaded == channelLoadedState.isOldestMessagesLoaded && this.isLoadingMessages == channelLoadedState.isLoadingMessages && this.isTouchedSinceLastJump == channelLoadedState.isTouchedSinceLastJump && m.areEqual(this.newestSentByUserMessageId, channelLoadedState.newestSentByUserMessageId);
        }

        public final Long getNewestSentByUserMessageId() {
            return this.newestSentByUserMessageId;
        }

        public int hashCode() {
            boolean z2 = this.isInitialMessagesLoaded;
            int i = 1;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = i2 * 31;
            boolean z3 = this.isOldestMessagesLoaded;
            if (z3) {
                z3 = true;
            }
            int i5 = z3 ? 1 : 0;
            int i6 = z3 ? 1 : 0;
            int i7 = (i4 + i5) * 31;
            boolean z4 = this.isLoadingMessages;
            if (z4) {
                z4 = true;
            }
            int i8 = z4 ? 1 : 0;
            int i9 = z4 ? 1 : 0;
            int i10 = (i7 + i8) * 31;
            boolean z5 = this.isTouchedSinceLastJump;
            if (!z5) {
                i = z5 ? 1 : 0;
            }
            int i11 = (i10 + i) * 31;
            Long l = this.newestSentByUserMessageId;
            return i11 + (l != null ? l.hashCode() : 0);
        }

        public final boolean isInitialMessagesLoaded() {
            return this.isInitialMessagesLoaded;
        }

        public final boolean isLoadingMessages() {
            return this.isLoadingMessages;
        }

        public final boolean isOldestMessagesLoaded() {
            return this.isOldestMessagesLoaded;
        }

        public final boolean isTouchedSinceLastJump() {
            return this.isTouchedSinceLastJump;
        }

        public String toString() {
            StringBuilder R = a.R("ChannelLoadedState(isInitialMessagesLoaded=");
            R.append(this.isInitialMessagesLoaded);
            R.append(", isOldestMessagesLoaded=");
            R.append(this.isOldestMessagesLoaded);
            R.append(", isLoadingMessages=");
            R.append(this.isLoadingMessages);
            R.append(", isTouchedSinceLastJump=");
            R.append(this.isTouchedSinceLastJump);
            R.append(", newestSentByUserMessageId=");
            return a.F(R, this.newestSentByUserMessageId, ")");
        }

        public /* synthetic */ ChannelLoadedState(boolean z2, boolean z3, boolean z4, boolean z5, Long l, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? false : z2, (i & 2) != 0 ? false : z3, (i & 4) != 0 ? false : z4, (i & 8) == 0 ? z5 : false, (i & 16) != 0 ? null : l);
        }
    }

    /* compiled from: StoreMessagesLoader.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0019\u0010\u0006\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0006\u0010\u0007R\u001a\u0010\b\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\b\u0010\tR\u001a\u0010\n\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\n\u0010\t¨\u0006\r"}, d2 = {"Lcom/discord/stores/StoreMessagesLoader$Companion;", "", "", "Lcom/discord/primitives/MessageId;", "targetMessageId", "", "isScrollToAction", "(J)Z", "SCROLL_TO_LAST_UNREAD", "J", "SCROLL_TO_LATEST", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public final boolean isScrollToAction(long j) {
            return j == 0 || j == 1;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public StoreMessagesLoader(StoreStream storeStream, Dispatcher dispatcher) {
        m.checkNotNullParameter(storeStream, "stream");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        this.stream = storeStream;
        this.dispatcher = dispatcher;
        HashMap<Long, ChannelLoadedState> hashMap = new HashMap<>();
        this.channelLoadedStates = hashMap;
        this.channelLoadedStateSubject = new SerializedSubject<>(BehaviorSubject.l0(new HashMap(hashMap)));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void channelLoadedStateUpdate(long j, Function1<? super ChannelLoadedState, ChannelLoadedState> function1) {
        ChannelLoadedState channelLoadedState = this.channelLoadedStates.get(Long.valueOf(j));
        if (channelLoadedState == null) {
            channelLoadedState = new ChannelLoadedState(false, false, false, false, null, 31, null);
        }
        m.checkNotNullExpressionValue(channelLoadedState, "channelLoadedStates[chan…] ?: ChannelLoadedState()");
        this.channelLoadedStates.put(Long.valueOf(j), function1.invoke(channelLoadedState));
        SerializedSubject<Map<Long, ChannelLoadedState>, Map<Long, ChannelLoadedState>> serializedSubject = this.channelLoadedStateSubject;
        serializedSubject.k.onNext(new HashMap(this.channelLoadedStates));
    }

    private final void channelLoadedStatesReset() {
        this.channelLoadedStates.clear();
        SerializedSubject<Map<Long, ChannelLoadedState>, Map<Long, ChannelLoadedState>> serializedSubject = this.channelLoadedStateSubject;
        serializedSubject.k.onNext(new HashMap(this.channelLoadedStates));
        Subscription subscription = this.loadingMessagesSubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        this.loadingMessagesRetryDelayMillis = this.loadingMessagesRetryDelayDefault;
        log("Disconnected, resetting all message loaded states.");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void handleChatDetached(Set<Long> set) {
        this.detachedChannels = set;
        log("Loaded detached channel state: " + set);
        tryLoadMessages$default(this, 0L, false, true, false, null, null, 59, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void handleChatInteraction(StoreChat.InteractionState interactionState) {
        channelLoadedStateUpdate(interactionState.getChannelId(), new StoreMessagesLoader$handleChatInteraction$1$1(interactionState));
        this.interactionState = interactionState;
        tryLoadMessages$default(this, 0L, false, true, false, null, null, 59, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void handleLoadMessagesError(long j) {
        channelLoadedStateUpdate(j, StoreMessagesLoader$handleLoadMessagesError$1.INSTANCE);
        long j2 = this.loadingMessagesRetryDelayMillis;
        if (j2 < this.loadingMessagesRetryMax) {
            this.loadingMessagesRetryDelayMillis = (j2 * 2) + new Random().nextInt(this.loadingMessagesRetryJitter);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Failed to load messages for channel [" + j + "], ");
        sb.append("retrying in " + this.loadingMessagesRetryDelayMillis + "ms");
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "StringBuilder()\n        …}ms\")\n        .toString()");
        log(sb2);
        tryLoadMessages$default(this, this.loadingMessagesRetryDelayMillis, false, false, false, null, null, 58, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Removed duplicated region for block: B:36:0x0082 A[Catch: all -> 0x0125, TryCatch #0 {, blocks: (B:4:0x0007, B:14:0x001d, B:26:0x003a, B:28:0x0070, B:36:0x0082, B:38:0x0090, B:40:0x009f, B:41:0x00a3, B:43:0x00a9, B:50:0x00bf, B:52:0x00c3, B:53:0x00d1, B:56:0x00e4, B:57:0x00e7), top: B:62:0x0007 }] */
    /* JADX WARN: Removed duplicated region for block: B:37:0x008e  */
    /* JADX WARN: Removed duplicated region for block: B:40:0x009f A[Catch: all -> 0x0125, TryCatch #0 {, blocks: (B:4:0x0007, B:14:0x001d, B:26:0x003a, B:28:0x0070, B:36:0x0082, B:38:0x0090, B:40:0x009f, B:41:0x00a3, B:43:0x00a9, B:50:0x00bf, B:52:0x00c3, B:53:0x00d1, B:56:0x00e4, B:57:0x00e7), top: B:62:0x0007 }] */
    /* JADX WARN: Removed duplicated region for block: B:55:0x00e2 A[ADDED_TO_REGION] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final synchronized void handleLoadedMessages(java.util.List<com.discord.models.message.Message> r19, long r20, long r22, java.lang.Long r24, java.lang.Long r25) {
        /*
            Method dump skipped, instructions count: 296
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreMessagesLoader.handleLoadedMessages(java.util.List, long, long, java.lang.Long, java.lang.Long):void");
    }

    private final void log(String str) {
        AppLog.i("[MessageLoader] " + str);
    }

    private final synchronized void tryLoadMessages(long j, boolean z2, boolean z3, boolean z4, Long l, Long l2) {
        long longValue = l != null ? l.longValue() : this.selectedChannelId;
        Subscription subscription = this.delayLoadingMessagesSubscription;
        if (subscription != null && z4) {
            if (subscription != null) {
                subscription.unsubscribe();
            }
            this.delayLoadingMessagesSubscription = null;
        }
        if (this.delayLoadingMessagesSubscription != null && !z2) {
            return;
        }
        if (j > 0) {
            Observable<Long> d02 = Observable.d0(j, TimeUnit.MILLISECONDS);
            m.checkNotNullExpressionValue(d02, "Observable\n          .ti…y, TimeUnit.MILLISECONDS)");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.computationBuffered(d02), getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new StoreMessagesLoader$tryLoadMessages$2(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreMessagesLoader$tryLoadMessages$1(this));
            return;
        }
        if (z3) {
            this.loadingMessagesRetryDelayMillis = this.loadingMessagesRetryDelayDefault;
        }
        ChannelLoadedState channelLoadedState = this.channelLoadedStates.get(Long.valueOf(longValue));
        if (channelLoadedState == null || !channelLoadedState.isLoadingMessages() || z2) {
            Subscription subscription2 = this.loadingMessagesSubscription;
            if (subscription2 != null) {
                subscription2.unsubscribe();
            }
            if (longValue > 0 && !this.backgrounded && this.authed) {
                StoreMessagesLoader$tryLoadMessages$3 storeMessagesLoader$tryLoadMessages$3 = new StoreMessagesLoader$tryLoadMessages$3(this);
                StoreMessagesLoader$tryLoadMessages$4 storeMessagesLoader$tryLoadMessages$4 = new StoreMessagesLoader$tryLoadMessages$4(this, storeMessagesLoader$tryLoadMessages$3);
                if (l2 != null) {
                    StoreMessagesLoader$tryLoadMessages$3.invoke$default(storeMessagesLoader$tryLoadMessages$3, longValue, l2, null, null, 12, null);
                } else {
                    if (channelLoadedState != null && channelLoadedState.isInitialMessagesLoaded()) {
                        this.channelMessagesLoadingSubject.k.onNext(Boolean.FALSE);
                        StoreChat.InteractionState interactionState = this.interactionState;
                        if (interactionState != null) {
                            boolean z5 = false;
                            boolean z6 = interactionState.isAtTop() && !channelLoadedState.isOldestMessagesLoaded();
                            if (interactionState.isAtBottom() && this.detachedChannels.contains(Long.valueOf(longValue))) {
                                z5 = true;
                            }
                            if (interactionState.getChannelId() == longValue && (z6 || z5)) {
                                channelLoadedStateUpdate(longValue, StoreMessagesLoader$tryLoadMessages$5$1.INSTANCE);
                                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.computationBuffered(ObservableExtensionsKt.takeSingleUntilTimeout$default(this.stream.getMessages$app_productionGoogleRelease().observeMessagesForChannel(longValue), 0L, false, 3, null)), interactionState.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new StoreMessagesLoader$tryLoadMessages$$inlined$apply$lambda$2(this, channelLoadedState, longValue, storeMessagesLoader$tryLoadMessages$4), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreMessagesLoader$tryLoadMessages$$inlined$apply$lambda$1(z6, z5, this, channelLoadedState, longValue, storeMessagesLoader$tryLoadMessages$4));
                            }
                        }
                    }
                    StoreMessagesLoader$tryLoadMessages$3.invoke$default(storeMessagesLoader$tryLoadMessages$3, longValue, 0L, null, null, 12, null);
                }
            }
        }
    }

    public static /* synthetic */ void tryLoadMessages$default(StoreMessagesLoader storeMessagesLoader, long j, boolean z2, boolean z3, boolean z4, Long l, Long l2, int i, Object obj) {
        long j2 = (i & 1) != 0 ? 0L : j;
        boolean z5 = false;
        boolean z6 = (i & 2) != 0 ? false : z2;
        boolean z7 = (i & 4) != 0 ? false : z3;
        if ((i & 8) == 0) {
            z5 = z4;
        }
        Long l3 = null;
        Long l4 = (i & 16) != 0 ? null : l;
        if ((i & 32) == 0) {
            l3 = l2;
        }
        storeMessagesLoader.tryLoadMessages(j2, z6, z7, z5, l4, l3);
    }

    public final void clearScrollTo() {
        this.scrollToSubject.k.onNext(null);
    }

    public final Observable<ChannelChunk> get() {
        return ObservableExtensionsKt.computationBuffered(this.channelMessageChunksSubject);
    }

    public final Observable<ChannelLoadedState> getMessagesLoadedState(final long j) {
        Observable<R> F = this.channelLoadedStateSubject.F(new j0.k.b<Map<Long, ? extends ChannelLoadedState>, ChannelLoadedState>() { // from class: com.discord.stores.StoreMessagesLoader$getMessagesLoadedState$1
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ StoreMessagesLoader.ChannelLoadedState call(Map<Long, ? extends StoreMessagesLoader.ChannelLoadedState> map) {
                return call2((Map<Long, StoreMessagesLoader.ChannelLoadedState>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final StoreMessagesLoader.ChannelLoadedState call2(Map<Long, StoreMessagesLoader.ChannelLoadedState> map) {
                StoreMessagesLoader.ChannelLoadedState channelLoadedState = map.get(Long.valueOf(j));
                return channelLoadedState != null ? channelLoadedState : new StoreMessagesLoader.ChannelLoadedState(false, false, false, false, null, 31, null);
            }
        });
        m.checkNotNullExpressionValue(F, "channelLoadedStateSubjec…?: ChannelLoadedState() }");
        Observable<ChannelLoadedState> q = ObservableExtensionsKt.computationLatest(F).q();
        m.checkNotNullExpressionValue(q, "channelLoadedStateSubjec…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Long> getScrollTo() {
        Observable<R> F = this.scrollToSubject.x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
        m.checkNotNullExpressionValue(F, "filter { it != null }.map { it!! }");
        return ObservableExtensionsKt.computationLatest(F);
    }

    public final synchronized void handleAuthToken(String str) {
        this.authed = str != null;
        tryLoadMessages$default(this, 0L, false, true, false, null, null, 59, null);
    }

    public final synchronized void handleBackgrounded(boolean z2) {
        this.backgrounded = z2;
        tryLoadMessages$default(this, 0L, false, true, false, null, null, 59, null);
    }

    @StoreThread
    public final synchronized void handleChannelSelected(long j) {
        channelLoadedStateUpdate(this.selectedChannelId, StoreMessagesLoader$handleChannelSelected$1.INSTANCE);
        this.selectedChannelId = j;
        tryLoadMessages$default(this, 0L, true, true, false, null, null, 57, null);
    }

    public final synchronized void handleConnected(boolean z2) {
        if (z2) {
            this.hasConnected = true;
        } else if (this.hasConnected) {
            channelLoadedStatesReset();
        }
        tryLoadMessages$default(this, 0L, false, true, this.hasConnected, null, null, 51, null);
    }

    @Override // com.discord.stores.Store
    public void init(Context context) {
        m.checkNotNullParameter(context, "context");
        super.init(context);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.computationLatest(this.stream.getChat$app_productionGoogleRelease().observeInteractionState()), StoreMessagesLoader.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreMessagesLoader$init$1(this));
        ObservableExtensionsKt.appSubscribe(this.stream.getMessages$app_productionGoogleRelease().getAllDetached(), StoreMessagesLoader.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreMessagesLoader$init$2(this));
    }

    public final synchronized void jumpToMessage(final long j, final long j2) {
        if (j2 > 0) {
            StoreMessagesLoader$jumpToMessage$1 storeMessagesLoader$jumpToMessage$1 = new StoreMessagesLoader$jumpToMessage$1(this, j, j2);
            StoreMessagesLoader$jumpToMessage$2 storeMessagesLoader$jumpToMessage$2 = new StoreMessagesLoader$jumpToMessage$2(this, j);
            Observable Y = this.stream.getChannelsSelected$app_productionGoogleRelease().observeId().k(o.c(new StoreMessagesLoader$jumpToMessage$3(j), -1L, 1000L, TimeUnit.MILLISECONDS)).Y(new j0.k.b<Long, Observable<? extends Message>>() { // from class: com.discord.stores.StoreMessagesLoader$jumpToMessage$4
                public final Observable<? extends Message> call(Long l) {
                    StoreStream storeStream;
                    storeStream = StoreMessagesLoader.this.stream;
                    return storeStream.getMessages$app_productionGoogleRelease().observeMessagesForChannel(j, j2);
                }
            });
            m.checkNotNullExpressionValue(Y, "stream\n        .channels…lId, messageId)\n        }");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.computationLatest(ObservableExtensionsKt.takeSingleUntilTimeout$default(Y, 0L, false, 3, null)), getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreMessagesLoader$jumpToMessage$5(this, storeMessagesLoader$jumpToMessage$1));
            Observable z2 = StoreConnectionOpen.observeConnectionOpen$default(this.stream.getConnectionOpen$app_productionGoogleRelease(), false, 1, null).Z(1).z(new j0.k.b<Boolean, Observable<? extends Channel>>() { // from class: com.discord.stores.StoreMessagesLoader$jumpToMessage$6
                public final Observable<? extends Channel> call(Boolean bool) {
                    StoreStream storeStream;
                    m.checkNotNullExpressionValue(bool, "isConnected");
                    int i = bool.booleanValue() ? 1 : 3;
                    storeStream = StoreMessagesLoader.this.stream;
                    Observable<R> F = storeStream.getChannels$app_productionGoogleRelease().observeChannel(j).x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
                    m.checkNotNullExpressionValue(F, "filter { it != null }.map { it!! }");
                    return ObservableExtensionsKt.takeSingleUntilTimeout$default(F, 1000 * i, false, 2, null);
                }
            });
            m.checkNotNullExpressionValue(z2, "stream\n        .connecti…mes.ONE_SECOND)\n        }");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.computationLatest(z2), getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : StoreMessagesLoader$jumpToMessage$7.INSTANCE, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreMessagesLoader$jumpToMessage$8(storeMessagesLoader$jumpToMessage$2));
        }
    }

    public final Observable<Boolean> observeChannelMessagesLoading() {
        Observable<Boolean> q = ObservableExtensionsKt.computationBuffered(this.channelMessagesLoadingSubject).q();
        m.checkNotNullExpressionValue(q, "channelMessagesLoadingSu…  .distinctUntilChanged()");
        return q;
    }

    public final synchronized void requestNewestMessages() {
        ChannelLoadedState channelLoadedState;
        if (this.detachedChannels.contains(Long.valueOf(this.selectedChannelId)) || (channelLoadedState = this.channelLoadedStates.get(Long.valueOf(this.selectedChannelId))) == null || !channelLoadedState.isInitialMessagesLoaded()) {
            channelLoadedStateUpdate(this.selectedChannelId, StoreMessagesLoader$requestNewestMessages$1.INSTANCE);
            tryLoadMessages$default(this, 0L, true, false, false, null, 1L, 25, null);
            return;
        }
        this.scrollToSubject.k.onNext(1L);
    }
}
