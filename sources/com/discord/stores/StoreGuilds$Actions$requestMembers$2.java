package com.discord.stores;

import androidx.core.app.NotificationCompat;
import d0.z.d.m;
import j0.k.b;
import kotlin.Metadata;
/* compiled from: StoreGuilds.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\u0010\u0006\u001a\n \u0003*\u0004\u0018\u00010\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "token", "", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/String;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuilds$Actions$requestMembers$2<T, R> implements b<String, Boolean> {
    public static final StoreGuilds$Actions$requestMembers$2 INSTANCE = new StoreGuilds$Actions$requestMembers$2();

    public final Boolean call(String str) {
        boolean z2 = false;
        if (str != null) {
            int length = str.length() - 1;
            int i = 0;
            boolean z3 = false;
            while (i <= length) {
                boolean z4 = m.compare(str.charAt(!z3 ? i : length), 32) <= 0;
                if (!z3) {
                    if (!z4) {
                        z3 = true;
                    } else {
                        i++;
                    }
                } else if (!z4) {
                    break;
                } else {
                    length--;
                }
            }
            if (str.subSequence(i, length + 1).toString().length() > 0) {
                z2 = true;
            }
        }
        return Boolean.valueOf(z2);
    }
}
