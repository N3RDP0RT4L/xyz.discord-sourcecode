package com.discord.stores;

import com.discord.stores.StoreUserNotes;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreUserNotes.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/stores/StoreUserNotes$UserNoteState;", "invoke", "()Lcom/discord/stores/StoreUserNotes$UserNoteState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserNotes$observeUserNote$1 extends o implements Function0<StoreUserNotes.UserNoteState> {
    public final /* synthetic */ long $userId;
    public final /* synthetic */ StoreUserNotes this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreUserNotes$observeUserNote$1(StoreUserNotes storeUserNotes, long j) {
        super(0);
        this.this$0 = storeUserNotes;
        this.$userId = j;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final StoreUserNotes.UserNoteState invoke() {
        StoreUserNotes.UserNoteState userNoteState = this.this$0.getNotesByUserId().get(Long.valueOf(this.$userId));
        return userNoteState != null ? userNoteState : StoreUserNotes.UserNoteState.Loading.INSTANCE;
    }
}
