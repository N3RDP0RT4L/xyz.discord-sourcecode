package com.discord.stores;

import androidx.exifinterface.media.ExifInterface;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: StoreEmoji.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0000\n\u0002\u0010%\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u0004\"\u0004\b\u0000\u0010\u0000*\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00028\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {ExifInterface.GPS_DIRECTION_TRUE, "", "", "key", "", "invoke", "(Ljava/util/Map;Ljava/lang/Object;)V", "increment"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreEmoji$buildUsableEmojiSet$4 extends o implements Function2<Map<T, Integer>, T, Unit> {
    public static final StoreEmoji$buildUsableEmojiSet$4 INSTANCE = new StoreEmoji$buildUsableEmojiSet$4();

    public StoreEmoji$buildUsableEmojiSet$4() {
        super(2);
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2) {
        invoke((Map<Map, Integer>) obj, (Map) obj2);
        return Unit.a;
    }

    public final <T> void invoke(Map<T, Integer> map, T t) {
        m.checkNotNullParameter(map, "$this$increment");
        Integer num = map.get(t);
        int i = 1;
        if (num != null) {
            i = 1 + num.intValue();
        }
        map.put(t, Integer.valueOf(i));
    }
}
