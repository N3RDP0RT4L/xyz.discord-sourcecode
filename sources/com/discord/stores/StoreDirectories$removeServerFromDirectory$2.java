package com.discord.stores;

import com.discord.api.directory.DirectoryEntryGuild;
import com.discord.api.guild.preview.GuildPreview;
import com.discord.stores.utilities.RestCallState;
import com.discord.stores.utilities.Success;
import d0.t.n;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreDirectories.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/stores/utilities/RestCallState;", "Lcom/discord/api/directory/DirectoryEntryGuild;", "request", "", "invoke", "(Lcom/discord/stores/utilities/RestCallState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreDirectories$removeServerFromDirectory$2 extends o implements Function1<RestCallState<? extends DirectoryEntryGuild>, Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ StoreDirectories this$0;

    /* compiled from: StoreDirectories.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreDirectories$removeServerFromDirectory$2$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public AnonymousClass1() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            Map map;
            Map map2;
            map = StoreDirectories$removeServerFromDirectory$2.this.this$0.directoriesMap;
            Long valueOf = Long.valueOf(StoreDirectories$removeServerFromDirectory$2.this.$channelId);
            map2 = StoreDirectories$removeServerFromDirectory$2.this.this$0.directoriesMap;
            RestCallState restCallState = (RestCallState) map2.get(Long.valueOf(StoreDirectories$removeServerFromDirectory$2.this.$channelId));
            List list = restCallState != null ? (List) restCallState.invoke() : null;
            if (list == null) {
                list = n.emptyList();
            }
            ArrayList arrayList = new ArrayList();
            for (Object obj : list) {
                GuildPreview e = ((DirectoryEntryGuild) obj).e();
                if (e == null || e.h() != StoreDirectories$removeServerFromDirectory$2.this.$guildId) {
                    arrayList.add(obj);
                }
            }
            map.put(valueOf, new Success(arrayList));
            StoreDirectories$removeServerFromDirectory$2.this.this$0.markChanged();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreDirectories$removeServerFromDirectory$2(StoreDirectories storeDirectories, long j, long j2) {
        super(1);
        this.this$0 = storeDirectories;
        this.$channelId = j;
        this.$guildId = j2;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(RestCallState<? extends DirectoryEntryGuild> restCallState) {
        invoke2((RestCallState<DirectoryEntryGuild>) restCallState);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(RestCallState<DirectoryEntryGuild> restCallState) {
        Dispatcher dispatcher;
        m.checkNotNullParameter(restCallState, "request");
        if (restCallState instanceof Success) {
            dispatcher = this.this$0.dispatcher;
            dispatcher.schedule(new AnonymousClass1());
        }
    }
}
