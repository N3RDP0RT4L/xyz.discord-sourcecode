package com.discord.stores;

import a0.a.a.b;
import andhook.lib.HookHelper;
import androidx.appcompat.widget.ActivityChooserModel;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.api.activity.Activity;
import com.discord.api.activity.ActivityEmoji;
import com.discord.api.activity.ActivityType;
import com.discord.api.guild.Guild;
import com.discord.api.presence.ClientStatus;
import com.discord.api.presence.ClientStatuses;
import com.discord.api.thread.AugmentedThreadMember;
import com.discord.api.thread.ThreadListMember;
import com.discord.api.thread.ThreadMemberListUpdate;
import com.discord.api.thread.ThreadMembersUpdate;
import com.discord.api.user.User;
import com.discord.models.domain.ModelCustomStatusSetting;
import com.discord.models.domain.ModelPayload;
import com.discord.models.domain.ModelSession;
import com.discord.models.domain.ModelUserSettings;
import com.discord.models.domain.emoji.ModelEmojiCustom;
import com.discord.models.domain.emoji.ModelEmojiUnicode;
import com.discord.models.presence.Presence;
import com.discord.models.user.MeUser;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.collections.SnowflakePartitionMap;
import com.discord.utilities.presence.ActivityUtilsKt;
import com.discord.utilities.time.Clock;
import com.discord.utilities.time.TimeUtils;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreUserPresence.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000ô\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u0000 \u0085\u00012\u00020\u0001:\u0004\u0085\u0001\u0086\u0001B!\u0012\u0006\u0010b\u001a\u00020a\u0012\u0006\u0010p\u001a\u00020o\u0012\u0006\u0010x\u001a\u00020w¢\u0006\u0006\b\u0083\u0001\u0010\u0084\u0001J1\u0010\n\u001a\u00020\t2\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u000e\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0019\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\r\u001a\u00020\fH\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u001b\u0010\u0014\u001a\u00020\t2\n\u0010\u0013\u001a\u00060\u0011j\u0002`\u0012H\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u001b\u0010\u0018\u001a\u00020\t2\n\u0010\u0017\u001a\u00060\u0011j\u0002`\u0016H\u0003¢\u0006\u0004\b\u0018\u0010\u0015J-\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00042\u0006\u0010\u0019\u001a\u00020\u000e2\u000e\u0010\u001a\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u0004H\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ-\u0010 \u001a\b\u0012\u0004\u0012\u00020\u000e0\u001f2\u0006\u0010\u001e\u001a\u00020\u001d2\u000e\u0010\u001a\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u0004H\u0002¢\u0006\u0004\b \u0010!J!\u0010%\u001a\u0016\u0012\b\u0012\u00060\u0011j\u0002`\u0016\u0012\b\u0012\u00060#j\u0002`$0\"¢\u0006\u0004\b%\u0010&J'\u0010(\u001a\u001c\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0011j\u0002`\u0016\u0012\b\u0012\u00060#j\u0002`$0\"0'¢\u0006\u0004\b(\u0010)J9\u0010,\u001a\u001c\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0011j\u0002`\u0016\u0012\b\u0012\u00060#j\u0002`$0\"0'2\u0010\u0010+\u001a\f\u0012\b\u0012\u00060\u0011j\u0002`\u00160*¢\u0006\u0004\b,\u0010-J'\u0010.\u001a\u0010\u0012\f\u0012\n\u0018\u00010#j\u0004\u0018\u0001`$0'2\n\u0010\u0017\u001a\u00060\u0011j\u0002`\u0016¢\u0006\u0004\b.\u0010/J\u0017\u00100\u001a\f\u0012\b\u0012\u00060#j\u0002`$0'¢\u0006\u0004\b0\u0010)J-\u00103\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u000e0'2\n\u0010\u0017\u001a\u00060\u0011j\u0002`\u00162\n\u00102\u001a\u00060\u0011j\u0002`1¢\u0006\u0004\b3\u00104J'\u00105\u001a\u0004\u0018\u00010\u000e2\n\u0010\u0017\u001a\u00060\u0011j\u0002`\u00162\n\u00102\u001a\u00060\u0011j\u0002`1¢\u0006\u0004\b5\u00106J\u0017\u00109\u001a\u00020\t2\u0006\u00108\u001a\u000207H\u0007¢\u0006\u0004\b9\u0010:J\u0017\u0010=\u001a\u00020\t2\u0006\u0010<\u001a\u00020;H\u0007¢\u0006\u0004\b=\u0010>J\u0017\u0010?\u001a\u00020\t2\u0006\u0010<\u001a\u00020;H\u0007¢\u0006\u0004\b?\u0010>J'\u0010@\u001a\u00020\t2\n\u0010\u0013\u001a\u00060\u0011j\u0002`\u00122\n\u0010\u0017\u001a\u00060\u0011j\u0002`\u0016H\u0007¢\u0006\u0004\b@\u0010AJ\u001d\u0010B\u001a\u00020\t2\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H\u0007¢\u0006\u0004\bB\u0010CJ\u0017\u0010D\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\bD\u0010EJ'\u0010I\u001a\u00020\t2\n\u0010\u0013\u001a\u00060\u0011j\u0002`\u00122\n\u0010H\u001a\u00060Fj\u0002`GH\u0007¢\u0006\u0004\bI\u0010JJI\u0010I\u001a\u00020\t2\n\u0010\u0013\u001a\u00060\u0011j\u0002`\u00122\n\u0010\u0017\u001a\u00060\u0011j\u0002`\u00162\u0006\u0010L\u001a\u00020K2\b\u0010N\u001a\u0004\u0018\u00010M2\u000e\u0010O\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u0004H\u0007¢\u0006\u0004\bI\u0010PJ!\u0010R\u001a\u00020\t2\u0010\u0010Q\u001a\f\u0012\b\u0012\u00060Fj\u0002`G0\u0004H\u0007¢\u0006\u0004\bR\u0010CJ\u0017\u0010U\u001a\u00020\t2\u0006\u0010T\u001a\u00020SH\u0007¢\u0006\u0004\bU\u0010VJ\u0017\u0010Y\u001a\u00020\t2\u0006\u0010X\u001a\u00020WH\u0007¢\u0006\u0004\bY\u0010ZJ+\u0010]\u001a\u00020\t2\u0006\u0010\u001e\u001a\u00020\u001d2\b\u0010[\u001a\u0004\u0018\u00010\u000e2\b\b\u0002\u0010\\\u001a\u00020\u0007H\u0007¢\u0006\u0004\b]\u0010^J\u000f\u0010_\u001a\u00020\tH\u0017¢\u0006\u0004\b_\u0010`R\u0016\u0010b\u001a\u00020a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bb\u0010cRZ\u0010h\u001aF\u0012\b\u0012\u00060\u0011j\u0002`\u0016\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0011j\u0002`\u0012\u0012\u0004\u0012\u00020f0e0dj\"\u0012\b\u0012\u00060\u0011j\u0002`\u0016\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0011j\u0002`\u0012\u0012\u0004\u0012\u00020f0e`g8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bh\u0010iR,\u0010k\u001a\u00060#j\u0002`$2\n\u0010j\u001a\u00060#j\u0002`$8\u0000@BX\u0080\u000e¢\u0006\f\n\u0004\bk\u0010l\u001a\u0004\bm\u0010nR\u0019\u0010p\u001a\u00020o8\u0006@\u0006¢\u0006\f\n\u0004\bp\u0010q\u001a\u0004\br\u0010sR\u0018\u0010u\u001a\u0004\u0018\u00010t8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bu\u0010vR\u0019\u0010x\u001a\u00020w8\u0006@\u0006¢\u0006\f\n\u0004\bx\u0010y\u001a\u0004\bz\u0010{R#\u0010}\u001a\f\u0012\b\u0012\u00060#j\u0002`$0|8\u0006@\u0006¢\u0006\f\n\u0004\b}\u0010~\u001a\u0004\b%\u0010\u007fR\u001c\u0010\u0080\u0001\u001a\u00060#j\u0002`$8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u0080\u0001\u0010lR-\u0010\u0081\u0001\u001a\u0016\u0012\b\u0012\u00060\u0011j\u0002`\u0016\u0012\b\u0012\u00060#j\u0002`$0\"8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u0081\u0001\u0010\u0082\u0001¨\u0006\u0087\u0001"}, d2 = {"Lcom/discord/stores/StoreUserPresence;", "Lcom/discord/stores/StoreV2;", "Lcom/discord/models/domain/ModelUserSettings;", "userSettings", "", "Lcom/discord/models/domain/ModelSession;", "sessions", "", "sendGateway", "", "updateSelfPresence", "(Lcom/discord/models/domain/ModelUserSettings;Ljava/util/List;Z)V", "Lcom/discord/models/domain/ModelCustomStatusSetting;", "customStatusSetting", "Lcom/discord/api/activity/Activity;", "getCustomStatusActivityFromSetting", "(Lcom/discord/models/domain/ModelCustomStatusSetting;)Lcom/discord/api/activity/Activity;", "", "Lcom/discord/primitives/GuildId;", "guildId", "clearPresences", "(J)V", "Lcom/discord/primitives/UserId;", "userId", "flattenPresence", "newActivity", "oldActivities", "replaceActivityInList", "(Lcom/discord/api/activity/Activity;Ljava/util/List;)Ljava/util/List;", "Lcom/discord/api/activity/ActivityType;", "type", "", "removeActivityInList", "(Lcom/discord/api/activity/ActivityType;Ljava/util/List;)Ljava/util/List;", "", "Lcom/discord/models/presence/Presence;", "Lcom/discord/stores/AppPresence;", "getPresences", "()Ljava/util/Map;", "Lrx/Observable;", "observeAllPresences", "()Lrx/Observable;", "", "userIds", "observePresencesForUsers", "(Ljava/util/Collection;)Lrx/Observable;", "observePresenceForUser", "(J)Lrx/Observable;", "observeLocalPresence", "Lcom/discord/primitives/ApplicationId;", "applicationId", "observeApplicationActivity", "(JJ)Lrx/Observable;", "getApplicationActivity", "(JJ)Lcom/discord/api/activity/Activity;", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "Lcom/discord/api/guild/Guild;", "guild", "handleGuildAdd", "(Lcom/discord/api/guild/Guild;)V", "handleGuildRemove", "handleGuildMemberRemove", "(JJ)V", "handleSessionsReplace", "(Ljava/util/List;)V", "handleUserSettingsUpdate", "(Lcom/discord/models/domain/ModelUserSettings;)V", "Lcom/discord/api/presence/Presence;", "Lcom/discord/stores/ApiPresence;", "presence", "handlePresenceUpdate", "(JLcom/discord/api/presence/Presence;)V", "Lcom/discord/api/presence/ClientStatus;", "status", "Lcom/discord/api/presence/ClientStatuses;", "clientStatuses", "activities", "(JJLcom/discord/api/presence/ClientStatus;Lcom/discord/api/presence/ClientStatuses;Ljava/util/List;)V", "presencesList", "handlePresenceReplace", "Lcom/discord/api/thread/ThreadMemberListUpdate;", "threadMemberListUpdate", "handleThreadMemberListUpdate", "(Lcom/discord/api/thread/ThreadMemberListUpdate;)V", "Lcom/discord/api/thread/ThreadMembersUpdate;", "threadMembersUpdate", "handleThreadMembersUpdate", "(Lcom/discord/api/thread/ThreadMembersUpdate;)V", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "forceDirty", "updateActivity", "(Lcom/discord/api/activity/ActivityType;Lcom/discord/api/activity/Activity;Z)V", "snapshotData", "()V", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "Ljava/util/HashMap;", "", "Lcom/discord/stores/StoreUserPresence$TimestampedPresence;", "Lkotlin/collections/HashMap;", "userGuildPresences", "Ljava/util/HashMap;", "<set-?>", "localPresence", "Lcom/discord/models/presence/Presence;", "getLocalPresence$app_productionGoogleRelease", "()Lcom/discord/models/presence/Presence;", "Lcom/discord/stores/StoreStream;", "stream", "Lcom/discord/stores/StoreStream;", "getStream", "()Lcom/discord/stores/StoreStream;", "Lcom/discord/models/user/MeUser;", "meUser", "Lcom/discord/models/user/MeUser;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "getObservationDeck", "()Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;", "presences", "Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;", "()Lcom/discord/utilities/collections/SnowflakePartitionMap$CopiablePartitionMap;", "localPresenceSnapshot", "presencesSnapshot", "Ljava/util/Map;", HookHelper.constructorName, "(Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreStream;Lcom/discord/stores/updates/ObservationDeck;)V", "Companion", "TimestampedPresence", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserPresence extends StoreV2 {
    public static final Companion Companion = new Companion(null);
    private static final StoreUserPresence$Companion$LocalPresenceUpdateSource$1 LocalPresenceUpdateSource = new ObservationDeck.UpdateSource() { // from class: com.discord.stores.StoreUserPresence$Companion$LocalPresenceUpdateSource$1
    };
    private final Clock clock;
    private Presence localPresence;
    private Presence localPresenceSnapshot;
    private MeUser meUser;
    private final ObservationDeck observationDeck;
    private final StoreStream stream;
    private final SnowflakePartitionMap.CopiablePartitionMap<Presence> presences = new SnowflakePartitionMap.CopiablePartitionMap<>(0, 1, null);
    private final HashMap<Long, Map<Long, TimestampedPresence>> userGuildPresences = new HashMap<>();
    private Map<Long, Presence> presencesSnapshot = new HashMap();

    /* compiled from: StoreUserPresence.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0006*\u0001\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/stores/StoreUserPresence$Companion;", "", "com/discord/stores/StoreUserPresence$Companion$LocalPresenceUpdateSource$1", "LocalPresenceUpdateSource", "Lcom/discord/stores/StoreUserPresence$Companion$LocalPresenceUpdateSource$1;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreUserPresence.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0082\b\u0018\u00002\u00020\u0001B\u001b\u0012\n\u0010\t\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\n\u001a\u00020\u0006¢\u0006\u0004\b\u001b\u0010\u001cJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ(\u0010\u000b\u001a\u00020\u00002\f\b\u0002\u0010\t\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\n\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u0019\u0010\n\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0017\u001a\u0004\b\u0018\u0010\bR\u001d\u0010\t\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0019\u001a\u0004\b\u001a\u0010\u0005¨\u0006\u001d"}, d2 = {"Lcom/discord/stores/StoreUserPresence$TimestampedPresence;", "", "Lcom/discord/models/presence/Presence;", "Lcom/discord/stores/AppPresence;", "component1", "()Lcom/discord/models/presence/Presence;", "", "component2", "()J", "presence", "timestamp", "copy", "(Lcom/discord/models/presence/Presence;J)Lcom/discord/stores/StoreUserPresence$TimestampedPresence;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getTimestamp", "Lcom/discord/models/presence/Presence;", "getPresence", HookHelper.constructorName, "(Lcom/discord/models/presence/Presence;J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class TimestampedPresence {
        private final Presence presence;
        private final long timestamp;

        public TimestampedPresence(Presence presence, long j) {
            m.checkNotNullParameter(presence, "presence");
            this.presence = presence;
            this.timestamp = j;
        }

        public static /* synthetic */ TimestampedPresence copy$default(TimestampedPresence timestampedPresence, Presence presence, long j, int i, Object obj) {
            if ((i & 1) != 0) {
                presence = timestampedPresence.presence;
            }
            if ((i & 2) != 0) {
                j = timestampedPresence.timestamp;
            }
            return timestampedPresence.copy(presence, j);
        }

        public final Presence component1() {
            return this.presence;
        }

        public final long component2() {
            return this.timestamp;
        }

        public final TimestampedPresence copy(Presence presence, long j) {
            m.checkNotNullParameter(presence, "presence");
            return new TimestampedPresence(presence, j);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof TimestampedPresence)) {
                return false;
            }
            TimestampedPresence timestampedPresence = (TimestampedPresence) obj;
            return m.areEqual(this.presence, timestampedPresence.presence) && this.timestamp == timestampedPresence.timestamp;
        }

        public final Presence getPresence() {
            return this.presence;
        }

        public final long getTimestamp() {
            return this.timestamp;
        }

        public int hashCode() {
            Presence presence = this.presence;
            return b.a(this.timestamp) + ((presence != null ? presence.hashCode() : 0) * 31);
        }

        public String toString() {
            StringBuilder R = a.R("TimestampedPresence(presence=");
            R.append(this.presence);
            R.append(", timestamp=");
            return a.B(R, this.timestamp, ")");
        }
    }

    public StoreUserPresence(Clock clock, StoreStream storeStream, ObservationDeck observationDeck) {
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(storeStream, "stream");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.clock = clock;
        this.stream = storeStream;
        this.observationDeck = observationDeck;
        ClientStatus clientStatus = ClientStatus.ONLINE;
        ClientStatus clientStatus2 = ClientStatus.OFFLINE;
        Presence presence = new Presence(clientStatus, new ClientStatuses(clientStatus2, clientStatus2, clientStatus), null);
        this.localPresence = presence;
        this.localPresenceSnapshot = Presence.copy$default(presence, null, null, null, 7, null);
    }

    @StoreThread
    private final void clearPresences(long j) {
        HashMap<Long, Map<Long, TimestampedPresence>> hashMap = this.userGuildPresences;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry<Long, Map<Long, TimestampedPresence>> entry : hashMap.entrySet()) {
            if (entry.getValue().remove(Long.valueOf(j)) != null) {
                linkedHashMap.put(entry.getKey(), entry.getValue());
            }
        }
        for (Map.Entry entry2 : linkedHashMap.entrySet()) {
            flattenPresence(((Number) entry2.getKey()).longValue());
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:33:0x007b  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x0080  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x0085  */
    /* JADX WARN: Removed duplicated region for block: B:40:0x009e  */
    @com.discord.stores.StoreThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void flattenPresence(long r10) {
        /*
            r9 = this;
            java.util.HashMap<java.lang.Long, java.util.Map<java.lang.Long, com.discord.stores.StoreUserPresence$TimestampedPresence>> r0 = r9.userGuildPresences
            java.lang.Long r1 = java.lang.Long.valueOf(r10)
            java.lang.Object r0 = r0.get(r1)
            java.util.Map r0 = (java.util.Map) r0
            r1 = 0
            if (r0 == 0) goto L53
            java.util.Collection r0 = r0.values()
            if (r0 == 0) goto L53
            java.util.Iterator r0 = r0.iterator()
            boolean r2 = r0.hasNext()
            if (r2 != 0) goto L21
            r2 = r1
            goto L4a
        L21:
            java.lang.Object r2 = r0.next()
            boolean r3 = r0.hasNext()
            if (r3 != 0) goto L2c
            goto L4a
        L2c:
            r3 = r2
            com.discord.stores.StoreUserPresence$TimestampedPresence r3 = (com.discord.stores.StoreUserPresence.TimestampedPresence) r3
            long r3 = r3.getTimestamp()
        L33:
            java.lang.Object r5 = r0.next()
            r6 = r5
            com.discord.stores.StoreUserPresence$TimestampedPresence r6 = (com.discord.stores.StoreUserPresence.TimestampedPresence) r6
            long r6 = r6.getTimestamp()
            int r8 = (r3 > r6 ? 1 : (r3 == r6 ? 0 : -1))
            if (r8 >= 0) goto L44
            r2 = r5
            r3 = r6
        L44:
            boolean r5 = r0.hasNext()
            if (r5 != 0) goto L33
        L4a:
            com.discord.stores.StoreUserPresence$TimestampedPresence r2 = (com.discord.stores.StoreUserPresence.TimestampedPresence) r2
            if (r2 == 0) goto L53
            com.discord.models.presence.Presence r0 = r2.getPresence()
            goto L54
        L53:
            r0 = r1
        L54:
            if (r0 == 0) goto L5d
            com.discord.api.presence.ClientStatus r2 = r0.getStatus()
            if (r2 == 0) goto L5d
            goto L5f
        L5d:
            com.discord.api.presence.ClientStatus r2 = com.discord.api.presence.ClientStatus.OFFLINE
        L5f:
            if (r0 == 0) goto L78
            java.util.List r3 = r0.getActivities()
            if (r3 == 0) goto L78
            com.discord.utilities.presence.PresenceUtils r4 = com.discord.utilities.presence.PresenceUtils.INSTANCE
            java.util.Comparator r4 = r4.getACTIVITY_COMPARATOR$app_productionGoogleRelease()
            java.util.List r3 = d0.t.u.sortedWith(r3, r4)
            if (r3 == 0) goto L78
            java.util.List r3 = d0.t.u.reversed(r3)
            goto L79
        L78:
            r3 = r1
        L79:
            if (r0 == 0) goto L80
            com.discord.api.presence.ClientStatuses r0 = r0.getClientStatuses()
            goto L81
        L80:
            r0 = r1
        L81:
            com.discord.api.presence.ClientStatus r4 = com.discord.api.presence.ClientStatus.OFFLINE
            if (r2 != r4) goto L9e
            java.util.HashMap<java.lang.Long, java.util.Map<java.lang.Long, com.discord.stores.StoreUserPresence$TimestampedPresence>> r0 = r9.userGuildPresences
            java.lang.Long r1 = java.lang.Long.valueOf(r10)
            r0.remove(r1)
            com.discord.utilities.collections.SnowflakePartitionMap$CopiablePartitionMap<com.discord.models.presence.Presence> r0 = r9.presences
            java.lang.Long r10 = java.lang.Long.valueOf(r10)
            java.lang.Object r10 = r0.remove(r10)
            if (r10 == 0) goto Le3
            r9.markChanged()
            goto Le3
        L9e:
            com.discord.utilities.collections.SnowflakePartitionMap$CopiablePartitionMap<com.discord.models.presence.Presence> r5 = r9.presences
            java.lang.Long r6 = java.lang.Long.valueOf(r10)
            java.lang.Object r5 = r5.get(r6)
            com.discord.models.presence.Presence r5 = (com.discord.models.presence.Presence) r5
            if (r5 == 0) goto Lb0
            com.discord.api.presence.ClientStatus r1 = r5.getStatus()
        Lb0:
            if (r1 != r2) goto Lca
            java.util.List r1 = r5.getActivities()
            boolean r1 = d0.z.d.m.areEqual(r1, r3)
            r1 = r1 ^ 1
            if (r1 != 0) goto Lca
            com.discord.api.presence.ClientStatuses r1 = r5.getClientStatuses()
            boolean r1 = d0.z.d.m.areEqual(r1, r0)
            r1 = r1 ^ 1
            if (r1 == 0) goto Le3
        Lca:
            com.discord.utilities.collections.SnowflakePartitionMap$CopiablePartitionMap<com.discord.models.presence.Presence> r1 = r9.presences
            java.lang.Long r10 = java.lang.Long.valueOf(r10)
            com.discord.models.presence.Presence r11 = new com.discord.models.presence.Presence
            if (r0 == 0) goto Ld5
            goto Lda
        Ld5:
            com.discord.api.presence.ClientStatuses r0 = new com.discord.api.presence.ClientStatuses
            r0.<init>(r4, r4, r4)
        Lda:
            r11.<init>(r2, r0, r3)
            r1.put(r10, r11)
            r9.markChanged()
        Le3:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreUserPresence.flattenPresence(long):void");
    }

    @StoreThread
    private final Activity getCustomStatusActivityFromSetting(ModelCustomStatusSetting modelCustomStatusSetting) {
        ActivityEmoji activityEmoji = null;
        if (modelCustomStatusSetting == ModelCustomStatusSetting.Companion.getCLEAR()) {
            return null;
        }
        if (modelCustomStatusSetting.getExpiresAt() != null && TimeUtils.parseUTCDate(modelCustomStatusSetting.getExpiresAt()) - this.clock.currentTimeMillis() <= 0) {
            return null;
        }
        if (modelCustomStatusSetting.getEmojiId() != null) {
            StoreEmoji emojis$app_productionGoogleRelease = this.stream.getEmojis$app_productionGoogleRelease();
            Long emojiId = modelCustomStatusSetting.getEmojiId();
            m.checkNotNull(emojiId);
            ModelEmojiCustom customEmojiInternal = emojis$app_productionGoogleRelease.getCustomEmojiInternal(emojiId.longValue());
            if (customEmojiInternal != null) {
                activityEmoji = new ActivityEmoji(String.valueOf(customEmojiInternal.getId()), customEmojiInternal.getName(), customEmojiInternal.isAnimated());
            }
        } else if (modelCustomStatusSetting.getEmojiName() != null) {
            Map<String, ModelEmojiUnicode> unicodeEmojiSurrogateMap = this.stream.getEmojis$app_productionGoogleRelease().getUnicodeEmojiSurrogateMap();
            String emojiName = modelCustomStatusSetting.getEmojiName();
            m.checkNotNull(emojiName);
            ModelEmojiUnicode modelEmojiUnicode = unicodeEmojiSurrogateMap.get(emojiName);
            if (modelEmojiUnicode != null) {
                activityEmoji = new ActivityEmoji(null, modelEmojiUnicode.getSurrogates(), false);
            }
        }
        return ActivityUtilsKt.createCustomStatusActivity(modelCustomStatusSetting.getText(), activityEmoji, this.clock.currentTimeMillis());
    }

    private final List<Activity> removeActivityInList(ActivityType activityType, List<Activity> list) {
        List<Activity> list2;
        int i;
        if (list == null || (list2 = u.toMutableList((Collection) list)) == null) {
            list2 = new ArrayList<>();
        }
        if (list != null) {
            i = 0;
            for (Activity activity : list) {
                if (activity.p() == activityType) {
                    break;
                }
                i++;
            }
        }
        i = -1;
        if (i != -1) {
            list2.remove(i);
        }
        return list2;
    }

    private final List<Activity> replaceActivityInList(Activity activity, List<Activity> list) {
        List<Activity> removeActivityInList = removeActivityInList(activity.p(), list);
        removeActivityInList.add(activity);
        return removeActivityInList;
    }

    public static /* synthetic */ void updateActivity$default(StoreUserPresence storeUserPresence, ActivityType activityType, Activity activity, boolean z2, int i, Object obj) {
        if ((i & 4) != 0) {
            z2 = false;
        }
        storeUserPresence.updateActivity(activityType, activity, z2);
    }

    /* JADX WARN: Code restructure failed: missing block: B:20:0x0042, code lost:
        if (r11 != null) goto L24;
     */
    /* JADX WARN: Removed duplicated region for block: B:27:0x005c  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x0061  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x0064  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x0082  */
    @com.discord.stores.StoreThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void updateSelfPresence(com.discord.models.domain.ModelUserSettings r10, java.util.List<? extends com.discord.models.domain.ModelSession> r11, boolean r12) {
        /*
            r9 = this;
            com.discord.models.user.MeUser r0 = r9.meUser
            if (r0 == 0) goto Lca
            long r4 = r0.getId()
            if (r10 == 0) goto L11
            com.discord.api.presence.ClientStatus r0 = r10.getStatus()
            if (r0 == 0) goto L11
            goto L17
        L11:
            com.discord.models.presence.Presence r0 = r9.localPresence
            com.discord.api.presence.ClientStatus r0 = r0.getStatus()
        L17:
            r6 = r0
            java.lang.String r0 = "userSettings?.status\n   …  ?: localPresence.status"
            d0.z.d.m.checkNotNullExpressionValue(r6, r0)
            r0 = 0
            if (r11 == 0) goto L45
            java.util.Iterator r11 = r11.iterator()
        L25:
            boolean r1 = r11.hasNext()
            if (r1 == 0) goto L39
            java.lang.Object r1 = r11.next()
            r2 = r1
            com.discord.models.domain.ModelSession r2 = (com.discord.models.domain.ModelSession) r2
            boolean r2 = r2.isActive()
            if (r2 == 0) goto L25
            goto L3a
        L39:
            r1 = r0
        L3a:
            com.discord.models.domain.ModelSession r1 = (com.discord.models.domain.ModelSession) r1
            if (r1 == 0) goto L45
            java.util.List r11 = r1.getActivities()
            if (r11 == 0) goto L45
            goto L57
        L45:
            com.discord.utilities.collections.SnowflakePartitionMap$CopiablePartitionMap<com.discord.models.presence.Presence> r11 = r9.presences
            java.lang.Long r1 = java.lang.Long.valueOf(r4)
            java.lang.Object r11 = r11.get(r1)
            com.discord.models.presence.Presence r11 = (com.discord.models.presence.Presence) r11
            if (r11 == 0) goto L59
            java.util.List r11 = r11.getActivities()
        L57:
            r8 = r11
            goto L5a
        L59:
            r8 = r0
        L5a:
            if (r10 == 0) goto L61
            com.discord.models.domain.ModelCustomStatusSetting r10 = r10.getCustomStatus()
            goto L62
        L61:
            r10 = r0
        L62:
            if (r10 == 0) goto L82
            com.discord.api.activity.Activity r10 = r9.getCustomStatusActivityFromSetting(r10)
            if (r10 == 0) goto L75
            com.discord.models.presence.Presence r11 = r9.localPresence
            java.util.List r11 = r11.getActivities()
            java.util.List r10 = r9.replaceActivityInList(r10, r11)
            goto L88
        L75:
            com.discord.api.activity.ActivityType r10 = com.discord.api.activity.ActivityType.CUSTOM_STATUS
            com.discord.models.presence.Presence r11 = r9.localPresence
            java.util.List r11 = r11.getActivities()
            java.util.List r10 = r9.removeActivityInList(r10, r11)
            goto L88
        L82:
            com.discord.models.presence.Presence r10 = r9.localPresence
            java.util.List r10 = r10.getActivities()
        L88:
            com.discord.models.presence.Presence r11 = r9.localPresence
            com.discord.api.presence.ClientStatus r11 = r11.getStatus()
            r1 = 1
            if (r6 != r11) goto L9e
            com.discord.models.presence.Presence r11 = r9.localPresence
            java.util.List r11 = r11.getActivities()
            boolean r11 = d0.z.d.m.areEqual(r10, r11)
            r11 = r11 ^ r1
            if (r11 == 0) goto Laf
        L9e:
            com.discord.models.presence.Presence r11 = new com.discord.models.presence.Presence
            r11.<init>(r6, r0, r10)
            r9.localPresence = r11
            com.discord.stores.updates.ObservationDeck$UpdateSource[] r10 = new com.discord.stores.updates.ObservationDeck.UpdateSource[r1]
            r11 = 0
            com.discord.stores.StoreUserPresence$Companion$LocalPresenceUpdateSource$1 r0 = com.discord.stores.StoreUserPresence.LocalPresenceUpdateSource
            r10[r11] = r0
            r9.markChanged(r10)
        Laf:
            r2 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r7 = 0
            r1 = r9
            r1.handlePresenceUpdate(r2, r4, r6, r7, r8)
            if (r12 != 0) goto Lca
            com.discord.utilities.presence.PresenceUtils r10 = com.discord.utilities.presence.PresenceUtils.INSTANCE
            com.discord.models.presence.Presence r11 = r9.localPresence
            com.discord.api.activity.Activity r10 = r10.getCustomStatusActivity(r11)
            if (r10 != 0) goto Lca
            com.discord.stores.StoreUserPresence$Companion$LocalPresenceUpdateSource$1 r10 = com.discord.stores.StoreUserPresence.LocalPresenceUpdateSource
            r9.markUnchanged(r10)
        Lca:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreUserPresence.updateSelfPresence(com.discord.models.domain.ModelUserSettings, java.util.List, boolean):void");
    }

    public final Activity getApplicationActivity(long j, long j2) {
        List<Activity> activities;
        boolean z2;
        Presence presence = this.presencesSnapshot.get(Long.valueOf(j));
        Object obj = null;
        if (presence == null || (activities = presence.getActivities()) == null) {
            return null;
        }
        Iterator<T> it = activities.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            Long a = ((Activity) next).a();
            if (a != null && a.longValue() == j2) {
                z2 = true;
                continue;
            } else {
                z2 = false;
                continue;
            }
            if (z2) {
                obj = next;
                break;
            }
        }
        return (Activity) obj;
    }

    public final Presence getLocalPresence$app_productionGoogleRelease() {
        return this.localPresence;
    }

    public final ObservationDeck getObservationDeck() {
        return this.observationDeck;
    }

    public final SnowflakePartitionMap.CopiablePartitionMap<Presence> getPresences() {
        return this.presences;
    }

    public final StoreStream getStream() {
        return this.stream;
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        this.userGuildPresences.clear();
        this.presences.clear();
        User me2 = modelPayload.getMe();
        m.checkNotNullExpressionValue(me2, "payload.me");
        this.meUser = new MeUser(me2);
        List<Guild> guilds = modelPayload.getGuilds();
        m.checkNotNullExpressionValue(guilds, "payload.guilds");
        for (Guild guild : guilds) {
            handleGuildAdd(guild);
        }
        List<com.discord.api.presence.Presence> presences = modelPayload.getPresences();
        m.checkNotNullExpressionValue(presences, "payload.presences");
        for (com.discord.api.presence.Presence presence : presences) {
            User f = presence.f();
            if (f != null) {
                handlePresenceUpdate(RecyclerView.FOREVER_NS, f.i(), presence.e(), presence.c(), presence.b());
            }
        }
        updateSelfPresence(modelPayload.getUserSettings(), modelPayload.getSessions(), false);
        markChanged();
    }

    @StoreThread
    public final void handleGuildAdd(Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        List<com.discord.api.presence.Presence> D = guild.D();
        if (D != null) {
            for (com.discord.api.presence.Presence presence : D) {
                User f = presence.f();
                if (f != null) {
                    handlePresenceUpdate(guild.r(), f.i(), presence.e(), presence.c(), presence.b());
                }
            }
        }
    }

    @StoreThread
    public final void handleGuildMemberRemove(long j, long j2) {
        Map<Long, TimestampedPresence> map = this.userGuildPresences.get(Long.valueOf(j2));
        if ((map != null ? map.remove(Long.valueOf(j)) : null) != null) {
            flattenPresence(j2);
        }
    }

    @StoreThread
    public final void handleGuildRemove(Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        clearPresences(guild.r());
    }

    @StoreThread
    public final void handlePresenceReplace(List<com.discord.api.presence.Presence> list) {
        m.checkNotNullParameter(list, "presencesList");
        clearPresences(RecyclerView.FOREVER_NS);
        for (com.discord.api.presence.Presence presence : list) {
            User f = presence.f();
            if (f != null) {
                long i = f.i();
                ClientStatus e = presence.e();
                ClientStatuses c = presence.c();
                if (c == null) {
                    ClientStatus clientStatus = ClientStatus.OFFLINE;
                    c = new ClientStatuses(clientStatus, clientStatus, clientStatus);
                }
                handlePresenceUpdate(RecyclerView.FOREVER_NS, i, e, c, presence.b());
            }
        }
    }

    @StoreThread
    public final void handlePresenceUpdate(long j, com.discord.api.presence.Presence presence) {
        m.checkNotNullParameter(presence, "presence");
        User f = presence.f();
        if (f != null) {
            handlePresenceUpdate(j, f.i(), presence.e(), presence.c(), presence.b());
        }
    }

    @StoreThread
    public final void handleSessionsReplace(List<? extends ModelSession> list) {
        m.checkNotNullParameter(list, "sessions");
        updateSelfPresence(null, list, true);
    }

    @StoreThread
    public final void handleThreadMemberListUpdate(ThreadMemberListUpdate threadMemberListUpdate) {
        m.checkNotNullParameter(threadMemberListUpdate, "threadMemberListUpdate");
        List<ThreadListMember> b2 = threadMemberListUpdate.b();
        if (b2 != null) {
            for (ThreadListMember threadListMember : b2) {
                com.discord.api.presence.Presence b3 = threadListMember.b();
                if (b3 != null) {
                    handlePresenceUpdate(threadMemberListUpdate.a(), threadListMember.c(), b3.e(), b3.c(), b3.b());
                }
            }
        }
    }

    @StoreThread
    public final void handleThreadMembersUpdate(ThreadMembersUpdate threadMembersUpdate) {
        m.checkNotNullParameter(threadMembersUpdate, "threadMembersUpdate");
        List<AugmentedThreadMember> a = threadMembersUpdate.a();
        if (a != null) {
            for (AugmentedThreadMember augmentedThreadMember : a) {
                com.discord.api.presence.Presence f = augmentedThreadMember.f();
                if (f != null) {
                    handlePresenceUpdate(threadMembersUpdate.b(), augmentedThreadMember.g(), f.e(), f.c(), f.b());
                }
            }
        }
    }

    @StoreThread
    public final void handleUserSettingsUpdate(ModelUserSettings modelUserSettings) {
        m.checkNotNullParameter(modelUserSettings, "userSettings");
        updateSelfPresence(modelUserSettings, null, true);
    }

    public final Observable<Map<Long, Presence>> observeAllPresences() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreUserPresence$observeAllPresences$1(this), 14, null);
    }

    public final Observable<Activity> observeApplicationActivity(long j, final long j2) {
        Observable<Activity> q = observePresenceForUser(j).F(new j0.k.b<Presence, Activity>() { // from class: com.discord.stores.StoreUserPresence$observeApplicationActivity$1
            public final Activity call(Presence presence) {
                List<Activity> activities;
                boolean z2;
                Activity activity = null;
                if (presence == null || (activities = presence.getActivities()) == null) {
                    return null;
                }
                Iterator<T> it = activities.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    T next = it.next();
                    Long a = ((Activity) next).a();
                    if (a != null && a.longValue() == j2) {
                        z2 = true;
                        continue;
                    } else {
                        z2 = false;
                        continue;
                    }
                    if (z2) {
                        activity = next;
                        break;
                    }
                }
                return activity;
            }
        }).q();
        m.checkNotNullExpressionValue(q, "observePresenceForUser(u…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Presence> observeLocalPresence() {
        Observable<Presence> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{LocalPresenceUpdateSource}, false, null, null, new StoreUserPresence$observeLocalPresence$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck\n      .c…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Presence> observePresenceForUser(final long j) {
        Observable<Presence> q = observeAllPresences().F(new j0.k.b<Map<Long, ? extends Presence>, Presence>() { // from class: com.discord.stores.StoreUserPresence$observePresenceForUser$1
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Presence call(Map<Long, ? extends Presence> map) {
                return call2((Map<Long, Presence>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Presence call2(Map<Long, Presence> map) {
                return map.get(Long.valueOf(j));
            }
        }).q();
        m.checkNotNullExpressionValue(q, "observeAllPresences()\n  …  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Map<Long, Presence>> observePresencesForUsers(final Collection<Long> collection) {
        m.checkNotNullParameter(collection, "userIds");
        Observable<Map<Long, Presence>> q = observeAllPresences().F(new j0.k.b<Map<Long, ? extends Presence>, Map<Long, ? extends Presence>>() { // from class: com.discord.stores.StoreUserPresence$observePresencesForUsers$1
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Map<Long, ? extends Presence> call(Map<Long, ? extends Presence> map) {
                return call2((Map<Long, Presence>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Map<Long, Presence> call2(Map<Long, Presence> map) {
                m.checkNotNullExpressionValue(map, "it");
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                for (Map.Entry<Long, Presence> entry : map.entrySet()) {
                    if (collection.contains(Long.valueOf(entry.getKey().longValue()))) {
                        linkedHashMap.put(entry.getKey(), entry.getValue());
                    }
                }
                return linkedHashMap;
            }
        }).q();
        m.checkNotNullExpressionValue(q, "observeAllPresences()\n  …  .distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        this.presencesSnapshot = this.presences.fastCopy();
        Presence presence = this.localPresence;
        List<Activity> activities = presence.getActivities();
        this.localPresenceSnapshot = Presence.copy$default(presence, null, null, activities != null ? u.toList(activities) : null, 3, null);
        if (getUpdateSources().contains(LocalPresenceUpdateSource)) {
            StoreGatewayConnection.presenceUpdate$default(StoreStream.Companion.getGatewaySocket(), this.localPresence.getStatus(), null, this.localPresence.getActivities(), null, 10, null);
        }
    }

    @StoreThread
    public final void updateActivity(ActivityType activityType, Activity activity, boolean z2) {
        List<Activity> list;
        Activity activity2;
        Object obj;
        boolean z3;
        m.checkNotNullParameter(activityType, "type");
        if (!z2) {
            List<Activity> activities = this.localPresence.getActivities();
            if (activities != null) {
                Iterator<T> it = activities.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj = null;
                        break;
                    }
                    obj = it.next();
                    if (((Activity) obj).p() == (activity != null ? activity.p() : null)) {
                        z3 = true;
                        continue;
                    } else {
                        z3 = false;
                        continue;
                    }
                    if (z3) {
                        break;
                    }
                }
                activity2 = (Activity) obj;
            } else {
                activity2 = null;
            }
            if (!(!m.areEqual(activity, activity2))) {
                return;
            }
        }
        if (activity != null) {
            list = replaceActivityInList(activity, this.localPresence.getActivities());
        } else {
            list = removeActivityInList(activityType, this.localPresence.getActivities());
        }
        ArrayList arrayList = new ArrayList();
        for (Object obj2 : list) {
            if (((Activity) obj2).p() != ActivityType.UNKNOWN) {
                arrayList.add(obj2);
            }
        }
        this.localPresence = new Presence(this.localPresence.getStatus(), null, arrayList);
        markChanged(LocalPresenceUpdateSource);
        MeUser meUser = this.meUser;
        if (meUser != null) {
            long id2 = meUser.getId();
            if (!m.areEqual(this.presences.get(Long.valueOf(id2)), this.localPresence)) {
                this.presences.put(Long.valueOf(id2), this.localPresence);
                markChanged();
            }
        }
    }

    /* renamed from: getPresences  reason: collision with other method in class */
    public final Map<Long, Presence> m14getPresences() {
        return this.presencesSnapshot;
    }

    @StoreThread
    public final void handlePresenceUpdate(long j, long j2, ClientStatus clientStatus, ClientStatuses clientStatuses, List<Activity> list) {
        m.checkNotNullParameter(clientStatus, "status");
        if (j == 0) {
            j = Long.MAX_VALUE;
        }
        MeUser meUser = this.meUser;
        if (meUser == null || meUser.getId() != j2 || j == RecyclerView.FOREVER_NS) {
            HashMap<Long, Map<Long, TimestampedPresence>> hashMap = this.userGuildPresences;
            Long valueOf = Long.valueOf(j2);
            Map<Long, TimestampedPresence> map = hashMap.get(valueOf);
            if (map == null) {
                map = new HashMap<>();
                hashMap.put(valueOf, map);
            }
            map.put(Long.valueOf(j), new TimestampedPresence(new Presence(clientStatus, clientStatuses, list), this.clock.currentTimeMillis()));
            flattenPresence(j2);
        }
    }
}
