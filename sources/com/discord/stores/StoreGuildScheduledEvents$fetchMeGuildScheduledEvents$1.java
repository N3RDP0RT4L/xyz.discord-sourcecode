package com.discord.stores;

import com.discord.api.guildscheduledevent.GuildScheduledEventMeUser;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreGuildScheduledEvents.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/api/guildscheduledevent/GuildScheduledEventMeUser;", "eventUsers", "", "invoke", "(Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildScheduledEvents$fetchMeGuildScheduledEvents$1 extends o implements Function1<List<? extends GuildScheduledEventMeUser>, Unit> {
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ StoreGuildScheduledEvents this$0;

    /* compiled from: StoreGuildScheduledEvents.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreGuildScheduledEvents$fetchMeGuildScheduledEvents$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ List $eventUsers;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(List list) {
            super(0);
            this.$eventUsers = list;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            HashMap hashMap;
            HashSet hashSet;
            List<GuildScheduledEventMeUser> list = this.$eventUsers;
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(list, 10));
            for (GuildScheduledEventMeUser guildScheduledEventMeUser : list) {
                arrayList.add(Long.valueOf(guildScheduledEventMeUser.a()));
            }
            hashMap = StoreGuildScheduledEvents$fetchMeGuildScheduledEvents$1.this.this$0.meGuildScheduledEventIds;
            hashMap.put(Long.valueOf(StoreGuildScheduledEvents$fetchMeGuildScheduledEvents$1.this.$guildId), u.toMutableSet(arrayList));
            hashSet = StoreGuildScheduledEvents$fetchMeGuildScheduledEvents$1.this.this$0.meGuildScheduledEventsFetches;
            hashSet.add(Long.valueOf(StoreGuildScheduledEvents$fetchMeGuildScheduledEvents$1.this.$guildId));
            StoreGuildScheduledEvents$fetchMeGuildScheduledEvents$1.this.this$0.markChanged();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGuildScheduledEvents$fetchMeGuildScheduledEvents$1(StoreGuildScheduledEvents storeGuildScheduledEvents, long j) {
        super(1);
        this.this$0 = storeGuildScheduledEvents;
        this.$guildId = j;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(List<? extends GuildScheduledEventMeUser> list) {
        invoke2((List<GuildScheduledEventMeUser>) list);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<GuildScheduledEventMeUser> list) {
        Dispatcher dispatcher;
        m.checkNotNullParameter(list, "eventUsers");
        dispatcher = this.this$0.dispatcher;
        dispatcher.schedule(new AnonymousClass1(list));
    }
}
