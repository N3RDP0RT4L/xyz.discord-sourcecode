package com.discord.stores;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.Context;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.message.Message;
import com.discord.api.user.User;
import com.discord.models.domain.ModelReadState;
import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StoreChat;
import com.discord.stores.StoreMessageAck;
import com.discord.stores.StoreMessagesLoader;
import com.discord.stores.StoreThreadsActiveJoined;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.SnowflakeUtils;
import com.discord.utilities.collections.CollectionExtensionsKt;
import com.discord.utilities.message.MessageUtils;
import com.discord.utilities.persister.Persister;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$3;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$4;
import com.discord.utilities.threads.ThreadUtils;
import d0.t.n0;
import d0.t.o0;
import d0.z.d.m;
import j0.l.e.k;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Subscription;
import rx.functions.Func3;
import rx.functions.Func4;
/* compiled from: StoreMessageAck.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Ê\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010%\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0003klmB'\u0012\u0006\u0010Z\u001a\u00020Y\u0012\u0006\u0010]\u001a\u00020\\\u0012\u0006\u0010`\u001a\u00020_\u0012\u0006\u0010c\u001a\u00020b¢\u0006\u0004\bi\u0010jJ'\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\b\u0010\tJ5\u0010\u000f\u001a(\u0012$\u0012\"\u0012\u000e\u0012\f\u0012\b\u0012\u00060\rj\u0002`\u000e0\f\u0012\u000e\u0012\f\u0012\b\u0012\u00060\rj\u0002`\u000e0\f0\u000b0\nH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u000f\u0010\u0011\u001a\u00020\u0007H\u0003¢\u0006\u0004\b\u0011\u0010\u0012J3\u0010\u0015\u001a\u00020\u00072\u0010\u0010\u0013\u001a\f\u0012\b\u0012\u00060\rj\u0002`\u000e0\f2\u0010\u0010\u0014\u001a\f\u0012\b\u0012\u00060\rj\u0002`\u000e0\fH\u0003¢\u0006\u0004\b\u0015\u0010\u0016J#\u0010\u001a\u001a\u00020\u00072\n\u0010\u0017\u001a\u00060\rj\u0002`\u000e2\u0006\u0010\u0019\u001a\u00020\u0018H\u0003¢\u0006\u0004\b\u001a\u0010\u001bJ\u001b\u0010\u001d\u001a\u00020\u001c2\n\u0010\u0017\u001a\u00060\rj\u0002`\u000eH\u0003¢\u0006\u0004\b\u001d\u0010\u001eJ!\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u001c0\n2\n\u0010\u0017\u001a\u00060\rj\u0002`\u000eH\u0002¢\u0006\u0004\b\u001f\u0010 J\u0017\u0010\"\u001a\u00020\u00042\u0006\u0010!\u001a\u00020\u001cH\u0002¢\u0006\u0004\b\"\u0010#J3\u0010%\u001a\b\u0012\u0004\u0012\u00020$0\n*\f\u0012\b\u0012\u00060\rj\u0002`\u000e0\n2\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0004H\u0002¢\u0006\u0004\b%\u0010&J#\u0010)\u001a\u00020\u0007*\b\u0012\u0004\u0012\u00020$0\n2\b\b\u0002\u0010(\u001a\u00020'H\u0002¢\u0006\u0004\b)\u0010*J\u001d\u0010,\u001a\u0012\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u0004\u0012\u00020\u00180+¢\u0006\u0004\b,\u0010-J\u001f\u0010.\u001a\u0012\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u0004\u0012\u00020\u00180+H\u0007¢\u0006\u0004\b.\u0010-J#\u0010/\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u0004\u0012\u00020\u00180+0\n¢\u0006\u0004\b/\u0010\u0010J\u001b\u00100\u001a\u0004\u0018\u00010\u00182\n\u0010\u0017\u001a\u00060\rj\u0002`\u000e¢\u0006\u0004\b0\u00101J!\u00102\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00180\n2\n\u0010\u0017\u001a\u00060\rj\u0002`\u000e¢\u0006\u0004\b2\u0010 J3\u00109\u001a\u00020\u00072\b\u00104\u001a\u0004\u0018\u0001032\n\u00106\u001a\u00060\rj\u0002`52\u000e\b\u0002\u00108\u001a\b\u0012\u0004\u0012\u00020\u000707¢\u0006\u0004\b9\u0010:J+\u0010;\u001a\u00020\u00072\n\u0010\u0017\u001a\u00060\rj\u0002`\u000e2\u0006\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0006\u001a\u00020\u0004¢\u0006\u0004\b;\u0010<J%\u0010?\u001a\u00020\u00072\n\u0010\u0017\u001a\u00060\rj\u0002`\u000e2\n\u0010>\u001a\u00060\rj\u0002`=¢\u0006\u0004\b?\u0010@J\u0017\u0010A\u001a\u00020\u00072\u0006\u00104\u001a\u000203H\u0016¢\u0006\u0004\bA\u0010BJ\u0017\u0010E\u001a\u00020\u00072\u0006\u0010D\u001a\u00020CH\u0007¢\u0006\u0004\bE\u0010FJ\u000f\u0010G\u001a\u00020\u0007H\u0007¢\u0006\u0004\bG\u0010\u0012J\u000f\u0010H\u001a\u00020\u0007H\u0007¢\u0006\u0004\bH\u0010\u0012J\u0017\u0010K\u001a\u00020\u00072\u0006\u0010J\u001a\u00020IH\u0007¢\u0006\u0004\bK\u0010LJ\u0017\u0010O\u001a\u00020\u00072\u0006\u0010N\u001a\u00020MH\u0007¢\u0006\u0004\bO\u0010PJ\u000f\u0010Q\u001a\u00020\u0007H\u0007¢\u0006\u0004\bQ\u0010\u0012J\u000f\u0010R\u001a\u00020\u0007H\u0016¢\u0006\u0004\bR\u0010\u0012R,\u0010T\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u0004\u0012\u00020\u00180+0S8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bT\u0010UR\u0018\u0010W\u001a\u0004\u0018\u00010V8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bW\u0010XR\u0016\u0010Z\u001a\u00020Y8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bZ\u0010[R\u0016\u0010]\u001a\u00020\\8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b]\u0010^R\u0016\u0010`\u001a\u00020_8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b`\u0010aR\u0016\u0010c\u001a\u00020b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bc\u0010dR&\u0010e\u001a\u0012\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u0004\u0012\u00020\u00180+8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\be\u0010fR&\u0010h\u001a\u0012\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u0004\u0012\u00020\u00180g8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bh\u0010f¨\u0006n"}, d2 = {"Lcom/discord/stores/StoreMessageAck;", "Lcom/discord/stores/StoreV2;", "Lcom/discord/api/channel/Channel;", "channel", "", "isLockedAck", "clearLock", "", "internalAck", "(Lcom/discord/api/channel/Channel;ZZ)V", "Lrx/Observable;", "Lkotlin/Pair;", "", "", "Lcom/discord/primitives/ChannelId;", "observeActiveJoinedThreadIdsWithPrevious", "()Lrx/Observable;", "pruneAcks", "()V", "oldThreadIds", "newThreadIds", "updateThreadAcks", "(Ljava/util/Set;Ljava/util/Set;)V", "channelId", "Lcom/discord/stores/StoreMessageAck$Ack;", "ackNewer", "updateAcks", "(JLcom/discord/stores/StoreMessageAck$Ack;)V", "Lcom/discord/stores/StoreMessageAck$ThreadState;", "getThreadStateInternal", "(J)Lcom/discord/stores/StoreMessageAck$ThreadState;", "observeThreadState", "(J)Lrx/Observable;", "threadState", "shouldAck", "(Lcom/discord/stores/StoreMessageAck$ThreadState;)Z", "Lcom/discord/stores/StoreMessageAck$PendingAck;", "getPendingAck", "(Lrx/Observable;ZZ)Lrx/Observable;", "", "mentionCount", "postPendingAck", "(Lrx/Observable;I)V", "", "getAll", "()Ljava/util/Map;", "getAllInternal", "observeAll", "getForChannel", "(J)Lcom/discord/stores/StoreMessageAck$Ack;", "observeForChannel", "Landroid/content/Context;", "context", "Lcom/discord/primitives/GuildId;", "guildId", "Lkotlin/Function0;", "onSuccess", "ackGuild", "(Landroid/content/Context;JLkotlin/jvm/functions/Function0;)V", "ack", "(JZZ)V", "Lcom/discord/primitives/MessageId;", "messageId", "markUnread", "(JJ)V", "init", "(Landroid/content/Context;)V", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "handlePreLogout", "handleGuildCreate", "Lcom/discord/api/message/Message;", "message", "handleMessageCreate", "(Lcom/discord/api/message/Message;)V", "Lcom/discord/models/domain/ModelReadState;", "readState", "handleMessageAck", "(Lcom/discord/models/domain/ModelReadState;)V", "handleChannelSelected", "snapshotData", "Lcom/discord/utilities/persister/Persister;", "acksPersister", "Lcom/discord/utilities/persister/Persister;", "Lrx/Subscription;", "threadSyncSubscription", "Lrx/Subscription;", "Lcom/discord/stores/StoreStream;", "stream", "Lcom/discord/stores/StoreStream;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "acksSnapshot", "Ljava/util/Map;", "", "acks", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/rest/RestAPI;)V", "Ack", "PendingAck", "ThreadState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessageAck extends StoreV2 {
    private final Map<Long, Ack> acks;
    private final Persister<Map<Long, Ack>> acksPersister;
    private Map<Long, Ack> acksSnapshot;
    private final Dispatcher dispatcher;
    private final ObservationDeck observationDeck;
    private final RestAPI restAPI;
    private final StoreStream stream;
    private Subscription threadSyncSubscription;

    /* compiled from: StoreMessageAck.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001B#\u0012\n\u0010\n\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\u000b\u001a\u00020\u0006\u0012\u0006\u0010\f\u001a\u00020\u0006¢\u0006\u0004\b\u001c\u0010\u001dB#\b\u0016\u0012\b\u0010\u001f\u001a\u0004\u0018\u00010\u001e\u0012\u0006\u0010\u000b\u001a\u00020\u0006\u0012\u0006\u0010\f\u001a\u00020\u0006¢\u0006\u0004\b\u001c\u0010 J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\t\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\t\u0010\bJ2\u0010\r\u001a\u00020\u00002\f\b\u0002\u0010\n\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u000b\u001a\u00020\u00062\b\b\u0002\u0010\f\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0016\u001a\u00020\u00062\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u0019\u0010\f\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0018\u001a\u0004\b\f\u0010\bR\u0019\u0010\u000b\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0018\u001a\u0004\b\u0019\u0010\bR\u001d\u0010\n\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u001a\u001a\u0004\b\u001b\u0010\u0005¨\u0006!"}, d2 = {"Lcom/discord/stores/StoreMessageAck$Ack;", "", "", "Lcom/discord/primitives/MessageId;", "component1", "()J", "", "component2", "()Z", "component3", "messageId", "viewed", "isLockedAck", "copy", "(JZZ)Lcom/discord/stores/StoreMessageAck$Ack;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getViewed", "J", "getMessageId", HookHelper.constructorName, "(JZZ)V", "Lcom/discord/models/domain/ModelReadState;", "readState", "(Lcom/discord/models/domain/ModelReadState;ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Ack {
        private final boolean isLockedAck;
        private final long messageId;
        private final boolean viewed;

        public Ack(long j, boolean z2, boolean z3) {
            this.messageId = j;
            this.viewed = z2;
            this.isLockedAck = z3;
        }

        public static /* synthetic */ Ack copy$default(Ack ack, long j, boolean z2, boolean z3, int i, Object obj) {
            if ((i & 1) != 0) {
                j = ack.messageId;
            }
            if ((i & 2) != 0) {
                z2 = ack.viewed;
            }
            if ((i & 4) != 0) {
                z3 = ack.isLockedAck;
            }
            return ack.copy(j, z2, z3);
        }

        public final long component1() {
            return this.messageId;
        }

        public final boolean component2() {
            return this.viewed;
        }

        public final boolean component3() {
            return this.isLockedAck;
        }

        public final Ack copy(long j, boolean z2, boolean z3) {
            return new Ack(j, z2, z3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Ack)) {
                return false;
            }
            Ack ack = (Ack) obj;
            return this.messageId == ack.messageId && this.viewed == ack.viewed && this.isLockedAck == ack.isLockedAck;
        }

        public final long getMessageId() {
            return this.messageId;
        }

        public final boolean getViewed() {
            return this.viewed;
        }

        public int hashCode() {
            int a = b.a(this.messageId) * 31;
            boolean z2 = this.viewed;
            int i = 1;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (a + i2) * 31;
            boolean z3 = this.isLockedAck;
            if (!z3) {
                i = z3 ? 1 : 0;
            }
            return i4 + i;
        }

        public final boolean isLockedAck() {
            return this.isLockedAck;
        }

        public String toString() {
            StringBuilder R = a.R("Ack(messageId=");
            R.append(this.messageId);
            R.append(", viewed=");
            R.append(this.viewed);
            R.append(", isLockedAck=");
            return a.M(R, this.isLockedAck, ")");
        }

        public Ack(ModelReadState modelReadState, boolean z2, boolean z3) {
            this(modelReadState != null ? modelReadState.getLastMessageId() : 0L, z2, z3);
        }
    }

    /* compiled from: StoreMessageAck.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\n\b\u0082\b\u0018\u0000 \u001d2\u00020\u0001:\u0001\u001dB\u001b\u0012\n\u0010\t\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\n\u001a\u00020\u0006¢\u0006\u0004\b\u001b\u0010\u001cJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ(\u0010\u000b\u001a\u00020\u00002\f\b\u0002\u0010\t\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\n\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u0019\u0010\n\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0017\u001a\u0004\b\u0018\u0010\bR\u001d\u0010\t\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0019\u001a\u0004\b\u001a\u0010\u0005¨\u0006\u001e"}, d2 = {"Lcom/discord/stores/StoreMessageAck$PendingAck;", "", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "Lcom/discord/stores/StoreMessageAck$Ack;", "component2", "()Lcom/discord/stores/StoreMessageAck$Ack;", "channelId", "ack", "copy", "(JLcom/discord/stores/StoreMessageAck$Ack;)Lcom/discord/stores/StoreMessageAck$PendingAck;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StoreMessageAck$Ack;", "getAck", "J", "getChannelId", HookHelper.constructorName, "(JLcom/discord/stores/StoreMessageAck$Ack;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class PendingAck {
        public static final Companion Companion = new Companion(null);
        private static final PendingAck EMPTY = new PendingAck(0, new Ack(0L, false, false));
        private final Ack ack;
        private final long channelId;

        /* compiled from: StoreMessageAck.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/stores/StoreMessageAck$PendingAck$Companion;", "", "Lcom/discord/stores/StoreMessageAck$PendingAck;", "EMPTY", "Lcom/discord/stores/StoreMessageAck$PendingAck;", "getEMPTY", "()Lcom/discord/stores/StoreMessageAck$PendingAck;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Companion {
            private Companion() {
            }

            public final PendingAck getEMPTY() {
                return PendingAck.EMPTY;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public PendingAck(long j, Ack ack) {
            m.checkNotNullParameter(ack, "ack");
            this.channelId = j;
            this.ack = ack;
        }

        public static /* synthetic */ PendingAck copy$default(PendingAck pendingAck, long j, Ack ack, int i, Object obj) {
            if ((i & 1) != 0) {
                j = pendingAck.channelId;
            }
            if ((i & 2) != 0) {
                ack = pendingAck.ack;
            }
            return pendingAck.copy(j, ack);
        }

        public final long component1() {
            return this.channelId;
        }

        public final Ack component2() {
            return this.ack;
        }

        public final PendingAck copy(long j, Ack ack) {
            m.checkNotNullParameter(ack, "ack");
            return new PendingAck(j, ack);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof PendingAck)) {
                return false;
            }
            PendingAck pendingAck = (PendingAck) obj;
            return this.channelId == pendingAck.channelId && m.areEqual(this.ack, pendingAck.ack);
        }

        public final Ack getAck() {
            return this.ack;
        }

        public final long getChannelId() {
            return this.channelId;
        }

        public int hashCode() {
            int a = b.a(this.channelId) * 31;
            Ack ack = this.ack;
            return a + (ack != null ? ack.hashCode() : 0);
        }

        public String toString() {
            StringBuilder R = a.R("PendingAck(channelId=");
            R.append(this.channelId);
            R.append(", ack=");
            R.append(this.ack);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: StoreMessageAck.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/stores/StoreMessageAck$ThreadState;", "", HookHelper.constructorName, "()V", "NotThread", "Thread", "Lcom/discord/stores/StoreMessageAck$ThreadState$NotThread;", "Lcom/discord/stores/StoreMessageAck$ThreadState$Thread;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class ThreadState {

        /* compiled from: StoreMessageAck.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreMessageAck$ThreadState$NotThread;", "Lcom/discord/stores/StoreMessageAck$ThreadState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class NotThread extends ThreadState {
            public static final NotThread INSTANCE = new NotThread();

            private NotThread() {
                super(null);
            }
        }

        /* compiled from: StoreMessageAck.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0010\u001a\u00020\u00022\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0012\u001a\u0004\b\u0005\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/stores/StoreMessageAck$ThreadState$Thread;", "Lcom/discord/stores/StoreMessageAck$ThreadState;", "", "component1", "()Z", "isActiveJoined", "copy", "(Z)Lcom/discord/stores/StoreMessageAck$ThreadState$Thread;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", HookHelper.constructorName, "(Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Thread extends ThreadState {
            private final boolean isActiveJoined;

            public Thread(boolean z2) {
                super(null);
                this.isActiveJoined = z2;
            }

            public static /* synthetic */ Thread copy$default(Thread thread, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    z2 = thread.isActiveJoined;
                }
                return thread.copy(z2);
            }

            public final boolean component1() {
                return this.isActiveJoined;
            }

            public final Thread copy(boolean z2) {
                return new Thread(z2);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Thread) && this.isActiveJoined == ((Thread) obj).isActiveJoined;
                }
                return true;
            }

            public int hashCode() {
                boolean z2 = this.isActiveJoined;
                if (z2) {
                    return 1;
                }
                return z2 ? 1 : 0;
            }

            public final boolean isActiveJoined() {
                return this.isActiveJoined;
            }

            public String toString() {
                return a.M(a.R("Thread(isActiveJoined="), this.isActiveJoined, ")");
            }
        }

        private ThreadState() {
        }

        public /* synthetic */ ThreadState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public StoreMessageAck(StoreStream storeStream, ObservationDeck observationDeck, Dispatcher dispatcher, RestAPI restAPI) {
        m.checkNotNullParameter(storeStream, "stream");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(restAPI, "restAPI");
        this.stream = storeStream;
        this.observationDeck = observationDeck;
        this.dispatcher = dispatcher;
        this.restAPI = restAPI;
        Persister<Map<Long, Ack>> persister = new Persister<>("MOST_RECENT_ACKS_V3", new HashMap());
        this.acksPersister = persister;
        this.acksSnapshot = persister.get();
        this.acks = new HashMap(this.acksSnapshot);
    }

    public static /* synthetic */ void ack$default(StoreMessageAck storeMessageAck, long j, boolean z2, boolean z3, int i, Object obj) {
        if ((i & 4) != 0) {
            z3 = false;
        }
        storeMessageAck.ack(j, z2, z3);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void ackGuild$default(StoreMessageAck storeMessageAck, Context context, long j, Function0 function0, int i, Object obj) {
        if ((i & 4) != 0) {
            function0 = StoreMessageAck$ackGuild$1.INSTANCE;
        }
        storeMessageAck.ackGuild(context, j, function0);
    }

    private final Observable<PendingAck> getPendingAck(Observable<Long> observable, final boolean z2, boolean z3) {
        final StoreMessageAck$getPendingAck$1 storeMessageAck$getPendingAck$1 = new StoreMessageAck$getPendingAck$1(z3);
        Observable Y = observable.Y(new j0.k.b<Long, Observable<? extends PendingAck>>() { // from class: com.discord.stores.StoreMessageAck$getPendingAck$2
            public final Observable<? extends StoreMessageAck.PendingAck> call(final Long l) {
                StoreStream storeStream;
                StoreStream storeStream2;
                Observable observeThreadState;
                if (l.longValue() <= 0) {
                    return new k(StoreMessageAck.PendingAck.Companion.getEMPTY());
                }
                storeStream = StoreMessageAck.this.stream;
                StoreMessageAck messageAck$app_productionGoogleRelease = storeStream.getMessageAck$app_productionGoogleRelease();
                m.checkNotNullExpressionValue(l, "channelId");
                Observable<StoreMessageAck.Ack> observeForChannel = messageAck$app_productionGoogleRelease.observeForChannel(l.longValue());
                storeStream2 = StoreMessageAck.this.stream;
                Observable<Long> observeRecentMessageIds = storeStream2.getMessagesMostRecent$app_productionGoogleRelease().observeRecentMessageIds(l.longValue());
                observeThreadState = StoreMessageAck.this.observeThreadState(l.longValue());
                return Observable.i(observeForChannel, observeRecentMessageIds, observeThreadState, new Func3<StoreMessageAck.Ack, Long, StoreMessageAck.ThreadState, StoreMessageAck.PendingAck>() { // from class: com.discord.stores.StoreMessageAck$getPendingAck$2.1
                    /* JADX WARN: Code restructure failed: missing block: B:7:0x0026, code lost:
                        if (r8.invoke(r6, r7.longValue()) != false) goto L8;
                     */
                    /*
                        Code decompiled incorrectly, please refer to instructions dump.
                        To view partially-correct add '--show-bad-code' argument
                    */
                    public final com.discord.stores.StoreMessageAck.PendingAck call(com.discord.stores.StoreMessageAck.Ack r6, java.lang.Long r7, com.discord.stores.StoreMessageAck.ThreadState r8) {
                        /*
                            r5 = this;
                            com.discord.stores.StoreMessageAck$getPendingAck$2 r0 = com.discord.stores.StoreMessageAck$getPendingAck$2.this
                            com.discord.stores.StoreMessageAck r0 = com.discord.stores.StoreMessageAck.this
                            java.lang.String r1 = "threadState"
                            d0.z.d.m.checkNotNullExpressionValue(r8, r1)
                            boolean r8 = com.discord.stores.StoreMessageAck.access$shouldAck(r0, r8)
                            if (r8 == 0) goto L4a
                            com.discord.stores.StoreMessageAck$getPendingAck$2 r8 = com.discord.stores.StoreMessageAck$getPendingAck$2.this
                            boolean r0 = r2
                            java.lang.String r1 = "mostRecentMessageId"
                            if (r0 != 0) goto L28
                            com.discord.stores.StoreMessageAck$getPendingAck$1 r8 = r3
                            d0.z.d.m.checkNotNullExpressionValue(r7, r1)
                            long r2 = r7.longValue()
                            boolean r6 = r8.invoke(r6, r2)
                            if (r6 == 0) goto L4a
                        L28:
                            com.discord.stores.StoreMessageAck$PendingAck r6 = new com.discord.stores.StoreMessageAck$PendingAck
                            java.lang.Long r8 = r2
                            java.lang.String r0 = "channelId"
                            d0.z.d.m.checkNotNullExpressionValue(r8, r0)
                            long r2 = r8.longValue()
                            com.discord.stores.StoreMessageAck$Ack r8 = new com.discord.stores.StoreMessageAck$Ack
                            d0.z.d.m.checkNotNullExpressionValue(r7, r1)
                            long r0 = r7.longValue()
                            r7 = 1
                            com.discord.stores.StoreMessageAck$getPendingAck$2 r4 = com.discord.stores.StoreMessageAck$getPendingAck$2.this
                            boolean r4 = r2
                            r8.<init>(r0, r7, r4)
                            r6.<init>(r2, r8)
                            goto L50
                        L4a:
                            com.discord.stores.StoreMessageAck$PendingAck$Companion r6 = com.discord.stores.StoreMessageAck.PendingAck.Companion
                            com.discord.stores.StoreMessageAck$PendingAck r6 = r6.getEMPTY()
                        L50:
                            return r6
                        */
                        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreMessageAck$getPendingAck$2.AnonymousClass1.call(com.discord.stores.StoreMessageAck$Ack, java.lang.Long, com.discord.stores.StoreMessageAck$ThreadState):com.discord.stores.StoreMessageAck$PendingAck");
                    }
                });
            }
        });
        m.checkNotNullExpressionValue(Y, "switchMap { channelId ->…gAck.EMPTY)\n      }\n    }");
        return Y;
    }

    @StoreThread
    private final ThreadState getThreadStateInternal(long j) {
        Map<Long, Channel> threadsByIdInternal$app_productionGoogleRelease = this.stream.getChannels$app_productionGoogleRelease().getThreadsByIdInternal$app_productionGoogleRelease();
        Map<Long, StoreThreadsActiveJoined.ActiveJoinedThread> activeJoinedThreadsInternal$app_productionGoogleRelease = this.stream.getThreadsActiveJoined$app_productionGoogleRelease().getActiveJoinedThreadsInternal$app_productionGoogleRelease();
        if (!threadsByIdInternal$app_productionGoogleRelease.containsKey(Long.valueOf(j))) {
            return ThreadState.NotThread.INSTANCE;
        }
        return new ThreadState.Thread(activeJoinedThreadsInternal$app_productionGoogleRelease.containsKey(Long.valueOf(j)));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void internalAck(Channel channel, boolean z2, boolean z3) {
        k kVar = new k(Long.valueOf(channel.h()));
        m.checkNotNullExpressionValue(kVar, "Observable.just(channel.id)");
        postPendingAck$default(this, ObservableExtensionsKt.takeSingleUntilTimeout$default(getPendingAck(kVar, z2, z3), 0L, false, 1, null), 0, 1, null);
    }

    private final Observable<Pair<Set<Long>, Set<Long>>> observeActiveJoinedThreadIdsWithPrevious() {
        Observable<Pair<Set<Long>, Set<Long>>> P = this.stream.getThreadsActiveJoined$app_productionGoogleRelease().observeAllActiveJoinedThreadsById().F(StoreMessageAck$observeActiveJoinedThreadIdsWithPrevious$1.INSTANCE).q().P(new Pair(n0.emptySet(), n0.emptySet()), StoreMessageAck$observeActiveJoinedThreadIdsWithPrevious$2.INSTANCE);
        m.checkNotNullExpressionValue(P, "stream\n        .threadsA…oinedThreadIds)\n        }");
        return P;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Observable<ThreadState> observeThreadState(final long j) {
        Observable<ThreadState> q = this.stream.getChannels$app_productionGoogleRelease().observeChannel(j).Y(new j0.k.b<Channel, Observable<? extends ThreadState>>() { // from class: com.discord.stores.StoreMessageAck$observeThreadState$1
            public final Observable<? extends StoreMessageAck.ThreadState> call(Channel channel) {
                StoreStream storeStream;
                if (channel == null || !ChannelUtils.C(channel)) {
                    return new k(StoreMessageAck.ThreadState.NotThread.INSTANCE);
                }
                storeStream = StoreMessageAck.this.stream;
                return (Observable<R>) storeStream.getThreadsActiveJoined$app_productionGoogleRelease().observeAllActiveJoinedThreadsById().F(new j0.k.b<Map<Long, ? extends StoreThreadsActiveJoined.ActiveJoinedThread>, StoreMessageAck.ThreadState.Thread>() { // from class: com.discord.stores.StoreMessageAck$observeThreadState$1.1
                    @Override // j0.k.b
                    public /* bridge */ /* synthetic */ StoreMessageAck.ThreadState.Thread call(Map<Long, ? extends StoreThreadsActiveJoined.ActiveJoinedThread> map) {
                        return call2((Map<Long, StoreThreadsActiveJoined.ActiveJoinedThread>) map);
                    }

                    /* renamed from: call  reason: avoid collision after fix types in other method */
                    public final StoreMessageAck.ThreadState.Thread call2(Map<Long, StoreThreadsActiveJoined.ActiveJoinedThread> map) {
                        m.checkNotNullExpressionValue(map, "activeJoinedThreads");
                        return new StoreMessageAck.ThreadState.Thread(map.containsKey(Long.valueOf(j)));
                    }
                });
            }
        }).q();
        m.checkNotNullExpressionValue(q, "stream\n          .channe…  .distinctUntilChanged()");
        return q;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void postPendingAck(Observable<PendingAck> observable, int i) {
        StoreMessageAck$postPendingAck$1 storeMessageAck$postPendingAck$1 = new StoreMessageAck$postPendingAck$1(this, i);
        Observable<PendingAck> x2 = observable.x(StoreMessageAck$postPendingAck$2.INSTANCE);
        m.checkNotNullExpressionValue(x2, "filter { it != PendingAck.EMPTY }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.computationLatest(x2), observable.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreMessageAck$postPendingAck$3(storeMessageAck$postPendingAck$1));
    }

    public static /* synthetic */ void postPendingAck$default(StoreMessageAck storeMessageAck, Observable observable, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = 0;
        }
        storeMessageAck.postPendingAck(observable, i);
    }

    @StoreThread
    private final void pruneAcks() {
        if (!(!this.stream.getGuilds$app_productionGoogleRelease().getUnavailableGuildsInternal$app_productionGoogleRelease().isEmpty())) {
            Map<Long, Channel> channelsByIdInternal$app_productionGoogleRelease = this.stream.getChannels$app_productionGoogleRelease().getChannelsByIdInternal$app_productionGoogleRelease();
            Map<Long, StoreThreadsActiveJoined.ActiveJoinedThread> activeJoinedThreadsInternal$app_productionGoogleRelease = this.stream.getThreadsActiveJoined$app_productionGoogleRelease().getActiveJoinedThreadsInternal$app_productionGoogleRelease();
            Iterator<Long> it = this.acks.keySet().iterator();
            while (it.hasNext()) {
                long longValue = it.next().longValue();
                if (!channelsByIdInternal$app_productionGoogleRelease.containsKey(Long.valueOf(longValue)) && !activeJoinedThreadsInternal$app_productionGoogleRelease.containsKey(Long.valueOf(longValue))) {
                    it.remove();
                    markChanged();
                }
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final boolean shouldAck(ThreadState threadState) {
        if (threadState instanceof ThreadState.NotThread) {
            return true;
        }
        if (threadState instanceof ThreadState.Thread) {
            return ((ThreadState.Thread) threadState).isActiveJoined();
        }
        throw new NoWhenBranchMatchedException();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void updateAcks(long j, Ack ack) {
        if (shouldAck(getThreadStateInternal(j))) {
            Ack ack2 = this.acks.get(Long.valueOf(j));
            if (ack2 == null || MessageUtils.compareMessages(Long.valueOf(ack2.getMessageId()), Long.valueOf(ack.getMessageId())) != 0 || (ack.isLockedAck() && !ack2.isLockedAck())) {
                this.acks.put(Long.valueOf(j), ack);
                markChanged();
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void updateThreadAcks(Set<Long> set, Set<Long> set2) {
        StoreThreadsActiveJoined.ActiveJoinedThread activeJoinedThread;
        if (this.stream.getGuilds$app_productionGoogleRelease().getUnavailableGuildsInternal$app_productionGoogleRelease().isEmpty()) {
            for (Number number : o0.minus((Set) set, (Iterable) set2)) {
                long longValue = number.longValue();
                if (this.acks.containsKey(Long.valueOf(longValue)) && this.acks.remove(Long.valueOf(longValue)) != null) {
                    markChanged();
                }
            }
        }
        Map<Long, Long> guildsJoinedAtInternal$app_productionGoogleRelease = this.stream.getGuilds$app_productionGoogleRelease().getGuildsJoinedAtInternal$app_productionGoogleRelease();
        Map<Long, StoreThreadsActiveJoined.ActiveJoinedThread> activeJoinedThreadsInternal$app_productionGoogleRelease = this.stream.getThreadsActiveJoined$app_productionGoogleRelease().getActiveJoinedThreadsInternal$app_productionGoogleRelease();
        for (Number number2 : o0.minus((Set) set2, (Iterable) set)) {
            long longValue2 = number2.longValue();
            if (!this.acks.containsKey(Long.valueOf(longValue2)) && (activeJoinedThread = activeJoinedThreadsInternal$app_productionGoogleRelease.get(Long.valueOf(longValue2))) != null) {
                this.acks.put(Long.valueOf(longValue2), new Ack((ThreadUtils.getThreadAckMessageTimestamp$default(ThreadUtils.INSTANCE, activeJoinedThread.getChannel(), guildsJoinedAtInternal$app_productionGoogleRelease.get(Long.valueOf(activeJoinedThread.getChannel().f())), Long.valueOf(activeJoinedThread.getJoinTimestamp().g()), null, 4, null) - SnowflakeUtils.DISCORD_EPOCH) << 22, false, false));
                markChanged();
            }
        }
    }

    public final void ack(long j, boolean z2, boolean z3) {
        this.dispatcher.schedule(new StoreMessageAck$ack$1(this, j, z2, z3));
    }

    public final void ackGuild(Context context, long j, Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "onSuccess");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.ackGuild(j), false, 1, null), (r18 & 1) != 0 ? null : context, "REST: ackGuild", (r18 & 4) != 0 ? null : null, new StoreMessageAck$ackGuild$2(function0), (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
    }

    public final Map<Long, Ack> getAll() {
        return this.acksSnapshot;
    }

    @StoreThread
    public final Map<Long, Ack> getAllInternal() {
        return this.acks;
    }

    public final Ack getForChannel(long j) {
        return getAll().get(Long.valueOf(j));
    }

    @StoreThread
    public final void handleChannelSelected() {
        for (Map.Entry<Long, Ack> entry : this.acks.entrySet()) {
            this.acks.put(Long.valueOf(entry.getKey().longValue()), Ack.copy$default(entry.getValue(), 0L, false, false, 3, null));
        }
        markChanged();
    }

    /* JADX WARN: Removed duplicated region for block: B:67:0x00d8 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:69:0x0082 A[SYNTHETIC] */
    @com.discord.stores.StoreThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void handleConnectionOpen(com.discord.models.domain.ModelPayload r13) {
        /*
            Method dump skipped, instructions count: 417
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreMessageAck.handleConnectionOpen(com.discord.models.domain.ModelPayload):void");
    }

    @StoreThread
    public final void handleGuildCreate() {
        pruneAcks();
    }

    @StoreThread
    public final void handleMessageAck(ModelReadState modelReadState) {
        m.checkNotNullParameter(modelReadState, "readState");
        updateAcks(modelReadState.getChannelId(), new Ack(modelReadState, false, false));
    }

    @StoreThread
    public final void handleMessageCreate(Message message) {
        m.checkNotNullParameter(message, "message");
        User e = message.e();
        if (e != null && e.i() == this.stream.getUsers$app_productionGoogleRelease().getMeInternal$app_productionGoogleRelease().getId()) {
            updateAcks(message.g(), new Ack(message.o(), false, false));
        }
    }

    @StoreThread
    public final void handlePreLogout() {
        Subscription subscription = this.threadSyncSubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        this.threadSyncSubscription = null;
    }

    @Override // com.discord.stores.Store
    public void init(Context context) {
        m.checkNotNullParameter(context, "context");
        super.init(context);
        Observable<R> Y = StoreStream.Companion.getChannelsSelected().observeResolvedSelectedChannel().q().Y(new j0.k.b<StoreChannelsSelected.ResolvedSelectedChannel, Observable<? extends Long>>() { // from class: com.discord.stores.StoreMessageAck$init$1

            /* compiled from: StoreMessageAck.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.stores.StoreMessageAck$init$1$1  reason: invalid class name */
            /* loaded from: classes.dex */
            public static final class AnonymousClass1<T, R> implements j0.k.b<StoreMessagesLoader.ChannelLoadedState, Boolean> {
                public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                public final Boolean call(StoreMessagesLoader.ChannelLoadedState channelLoadedState) {
                    return Boolean.valueOf(channelLoadedState.isInitialMessagesLoaded() && !channelLoadedState.isLoadingMessages());
                }
            }

            public final Observable<? extends Long> call(final StoreChannelsSelected.ResolvedSelectedChannel resolvedSelectedChannel) {
                StoreStream storeStream;
                StoreStream storeStream2;
                StoreStream storeStream3;
                StoreStream storeStream4;
                storeStream = StoreMessageAck.this.stream;
                Observable<Set<Long>> o = storeStream.getMessages$app_productionGoogleRelease().getAllDetached().o(50L, TimeUnit.MILLISECONDS);
                storeStream2 = StoreMessageAck.this.stream;
                Observable<R> q = storeStream2.getMessagesLoader$app_productionGoogleRelease().getMessagesLoadedState(resolvedSelectedChannel.getId()).F(AnonymousClass1.INSTANCE).q();
                storeStream3 = StoreMessageAck.this.stream;
                Observable<StoreChat.InteractionState> x2 = storeStream3.getChat$app_productionGoogleRelease().observeInteractionState().x(new j0.k.b<StoreChat.InteractionState, Boolean>() { // from class: com.discord.stores.StoreMessageAck$init$1.2
                    public final Boolean call(StoreChat.InteractionState interactionState) {
                        return Boolean.valueOf(interactionState.getChannelId() == StoreChannelsSelected.ResolvedSelectedChannel.this.getId());
                    }
                });
                storeStream4 = StoreMessageAck.this.stream;
                return Observable.h(o, q, x2, storeStream4.getMessageAck$app_productionGoogleRelease().observeForChannel(resolvedSelectedChannel.getId()), new Func4<Set<? extends Long>, Boolean, StoreChat.InteractionState, StoreMessageAck.Ack, Long>() { // from class: com.discord.stores.StoreMessageAck$init$1.3
                    @Override // rx.functions.Func4
                    public /* bridge */ /* synthetic */ Long call(Set<? extends Long> set, Boolean bool, StoreChat.InteractionState interactionState, StoreMessageAck.Ack ack) {
                        return call2((Set<Long>) set, bool, interactionState, ack);
                    }

                    /* renamed from: call  reason: avoid collision after fix types in other method */
                    public final Long call2(Set<Long> set, Boolean bool, StoreChat.InteractionState interactionState, StoreMessageAck.Ack ack) {
                        long j;
                        Channel maybeChannel = StoreChannelsSelected.ResolvedSelectedChannel.this.getMaybeChannel();
                        boolean z2 = true;
                        if (maybeChannel == null || !ChannelUtils.o(maybeChannel)) {
                            z2 = false;
                        }
                        if (interactionState.isAtBottomIgnoringTouch()) {
                            m.checkNotNullExpressionValue(bool, "isLoadingSettled");
                            if (bool.booleanValue() && !set.contains(Long.valueOf(interactionState.getChannelId())) && !z2 && (ack == null || !ack.isLockedAck())) {
                                j = interactionState.getChannelId();
                                return Long.valueOf(j);
                            }
                        }
                        j = 0;
                        return Long.valueOf(j);
                    }
                });
            }
        });
        m.checkNotNullExpressionValue(Y, "StoreStream\n        .get… 0L\n          }\n        }");
        Observable<Long> o = ObservableExtensionsKt.computationLatest(Y).o(500L, TimeUnit.MILLISECONDS);
        m.checkNotNullExpressionValue(o, "StoreStream\n        .get…0, TimeUnit.MILLISECONDS)");
        postPendingAck$default(this, getPendingAck(o, false, false), 0, 1, null);
    }

    public final void markUnread(long j, long j2) {
        Observable j3 = Observable.j(this.stream.getMessages$app_productionGoogleRelease().observeMessagesForChannel(j).x(StoreMessageAck$markUnread$1.INSTANCE), observeThreadState(j), StoreMessageAck$markUnread$2.INSTANCE);
        m.checkNotNullExpressionValue(j3, "Observable\n        .comb…lMessages, threadState) }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.takeSingleUntilTimeout(j3, 10L, false), StoreMessageAck.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreMessageAck$markUnread$3(this, j2, j));
    }

    public final Observable<Map<Long, Ack>> observeAll() {
        Observable<Map<Long, Ack>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreMessageAck$observeAll$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Ack> observeForChannel(long j) {
        Observable<Ack> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreMessageAck$observeForChannel$1(this, j), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR…  .distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        super.snapshotData();
        HashMap snapshot$default = CollectionExtensionsKt.snapshot$default(this.acks, 0, 0.0f, 3, null);
        this.acksSnapshot = snapshot$default;
        Persister.set$default(this.acksPersister, snapshot$default, false, 2, null);
    }
}
