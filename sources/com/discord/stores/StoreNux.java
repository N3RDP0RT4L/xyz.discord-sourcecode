package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.BehaviorSubject;
/* compiled from: StoreNux.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001)B\u000f\u0012\u0006\u0010 \u001a\u00020\u001f¢\u0006\u0004\b'\u0010(J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0013\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00020\u0007¢\u0006\u0004\b\b\u0010\tJ\u0015\u0010\f\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\f\u0010\rJ\u0015\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\u000e\u0010\rJ\u0015\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\u000f\u0010\rJ\u0015\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\u0010\u0010\rJ\u001d\u0010\u0013\u001a\u00020\u00042\u000e\u0010\u000b\u001a\n\u0018\u00010\u0011j\u0004\u0018\u0001`\u0012¢\u0006\u0004\b\u0013\u0010\u0014J!\u0010\u0017\u001a\u00020\u00042\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00020\u0015¢\u0006\u0004\b\u0017\u0010\u0018J\r\u0010\u0019\u001a\u00020\u0004¢\u0006\u0004\b\u0019\u0010\u001aJ\u001b\u0010\u001c\u001a\u00020\u00042\n\u0010\u001b\u001a\u00060\u0011j\u0002`\u0012H\u0007¢\u0006\u0004\b\u001c\u0010\u001dJ\u001b\u0010\u001e\u001a\u00020\u00042\n\u0010\u001b\u001a\u00060\u0011j\u0002`\u0012H\u0007¢\u0006\u0004\b\u001e\u0010\u001dR\u0016\u0010 \u001a\u00020\u001f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b \u0010!R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0003\u0010\"R:\u0010%\u001a&\u0012\f\u0012\n $*\u0004\u0018\u00010\u00020\u0002 $*\u0012\u0012\f\u0012\n $*\u0004\u0018\u00010\u00020\u0002\u0018\u00010#0#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b%\u0010&¨\u0006*"}, d2 = {"Lcom/discord/stores/StoreNux;", "Lcom/discord/stores/Store;", "Lcom/discord/stores/StoreNux$NuxState;", "nuxState", "", "publishNuxUpdated", "(Lcom/discord/stores/StoreNux$NuxState;)V", "Lrx/Observable;", "getNuxState", "()Lrx/Observable;", "", "value", "setFirstOpen", "(Z)V", "setPostRegister", "setPostRegisterWithInvite", "setContactSyncCompleted", "", "Lcom/discord/primitives/GuildId;", "setPremiumGuildHintGuildId", "(Ljava/lang/Long;)V", "Lkotlin/Function1;", "updateFunction", "updateNux", "(Lkotlin/jvm/functions/Function1;)V", "clearNux", "()V", "guildId", "handleGuildSelected", "(J)V", "handleSamplePremiumGuildSelected", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/stores/StoreNux$NuxState;", "Lrx/subjects/BehaviorSubject;", "kotlin.jvm.PlatformType", "nuxStateSubject", "Lrx/subjects/BehaviorSubject;", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;)V", "NuxState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreNux extends Store {
    private final Dispatcher dispatcher;
    private NuxState nuxState;
    private final BehaviorSubject<NuxState> nuxStateSubject;

    /* compiled from: StoreNux.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001BK\u0012\b\b\u0002\u0010\r\u001a\u00020\u0002\u0012\b\b\u0002\u0010\u000e\u001a\u00020\u0002\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u0002\u0012\b\b\u0002\u0010\u0010\u001a\u00020\u0002\u0012\b\b\u0002\u0010\u0011\u001a\u00020\u0002\u0012\u0010\b\u0002\u0010\u0012\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\n¢\u0006\u0004\b&\u0010'J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0004J\u0010\u0010\b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\b\u0010\u0004J\u0018\u0010\u000b\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJT\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\r\u001a\u00020\u00022\b\b\u0002\u0010\u000e\u001a\u00020\u00022\b\b\u0002\u0010\u000f\u001a\u00020\u00022\b\b\u0002\u0010\u0010\u001a\u00020\u00022\b\b\u0002\u0010\u0011\u001a\u00020\u00022\u0010\b\u0002\u0010\u0012\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\nHÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u001a\u0010\u001c\u001a\u00020\u00022\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001e\u001a\u0004\b\u001f\u0010\u0004R\u0019\u0010\u0011\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001e\u001a\u0004\b \u0010\u0004R!\u0010\u0012\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010!\u001a\u0004\b\"\u0010\fR\u0019\u0010\u0010\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001e\u001a\u0004\b#\u0010\u0004R\u0019\u0010\r\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001e\u001a\u0004\b$\u0010\u0004R\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001e\u001a\u0004\b%\u0010\u0004¨\u0006("}, d2 = {"Lcom/discord/stores/StoreNux$NuxState;", "", "", "component1", "()Z", "component2", "component3", "component4", "component5", "", "Lcom/discord/primitives/GuildId;", "component6", "()Ljava/lang/Long;", "postRegister", "postRegisterWithInvite", "contactSyncCompleted", "firstOpen", "addGuildHint", "guildBoostHintGuildId", "copy", "(ZZZZZLjava/lang/Long;)Lcom/discord/stores/StoreNux$NuxState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getContactSyncCompleted", "getAddGuildHint", "Ljava/lang/Long;", "getGuildBoostHintGuildId", "getFirstOpen", "getPostRegister", "getPostRegisterWithInvite", HookHelper.constructorName, "(ZZZZZLjava/lang/Long;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class NuxState {
        private final boolean addGuildHint;
        private final boolean contactSyncCompleted;
        private final boolean firstOpen;
        private final Long guildBoostHintGuildId;
        private final boolean postRegister;
        private final boolean postRegisterWithInvite;

        public NuxState() {
            this(false, false, false, false, false, null, 63, null);
        }

        public NuxState(boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, Long l) {
            this.postRegister = z2;
            this.postRegisterWithInvite = z3;
            this.contactSyncCompleted = z4;
            this.firstOpen = z5;
            this.addGuildHint = z6;
            this.guildBoostHintGuildId = l;
        }

        public static /* synthetic */ NuxState copy$default(NuxState nuxState, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, Long l, int i, Object obj) {
            if ((i & 1) != 0) {
                z2 = nuxState.postRegister;
            }
            if ((i & 2) != 0) {
                z3 = nuxState.postRegisterWithInvite;
            }
            boolean z7 = z3;
            if ((i & 4) != 0) {
                z4 = nuxState.contactSyncCompleted;
            }
            boolean z8 = z4;
            if ((i & 8) != 0) {
                z5 = nuxState.firstOpen;
            }
            boolean z9 = z5;
            if ((i & 16) != 0) {
                z6 = nuxState.addGuildHint;
            }
            boolean z10 = z6;
            if ((i & 32) != 0) {
                l = nuxState.guildBoostHintGuildId;
            }
            return nuxState.copy(z2, z7, z8, z9, z10, l);
        }

        public final boolean component1() {
            return this.postRegister;
        }

        public final boolean component2() {
            return this.postRegisterWithInvite;
        }

        public final boolean component3() {
            return this.contactSyncCompleted;
        }

        public final boolean component4() {
            return this.firstOpen;
        }

        public final boolean component5() {
            return this.addGuildHint;
        }

        public final Long component6() {
            return this.guildBoostHintGuildId;
        }

        public final NuxState copy(boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, Long l) {
            return new NuxState(z2, z3, z4, z5, z6, l);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof NuxState)) {
                return false;
            }
            NuxState nuxState = (NuxState) obj;
            return this.postRegister == nuxState.postRegister && this.postRegisterWithInvite == nuxState.postRegisterWithInvite && this.contactSyncCompleted == nuxState.contactSyncCompleted && this.firstOpen == nuxState.firstOpen && this.addGuildHint == nuxState.addGuildHint && m.areEqual(this.guildBoostHintGuildId, nuxState.guildBoostHintGuildId);
        }

        public final boolean getAddGuildHint() {
            return this.addGuildHint;
        }

        public final boolean getContactSyncCompleted() {
            return this.contactSyncCompleted;
        }

        public final boolean getFirstOpen() {
            return this.firstOpen;
        }

        public final Long getGuildBoostHintGuildId() {
            return this.guildBoostHintGuildId;
        }

        public final boolean getPostRegister() {
            return this.postRegister;
        }

        public final boolean getPostRegisterWithInvite() {
            return this.postRegisterWithInvite;
        }

        public int hashCode() {
            boolean z2 = this.postRegister;
            int i = 1;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = i2 * 31;
            boolean z3 = this.postRegisterWithInvite;
            if (z3) {
                z3 = true;
            }
            int i5 = z3 ? 1 : 0;
            int i6 = z3 ? 1 : 0;
            int i7 = (i4 + i5) * 31;
            boolean z4 = this.contactSyncCompleted;
            if (z4) {
                z4 = true;
            }
            int i8 = z4 ? 1 : 0;
            int i9 = z4 ? 1 : 0;
            int i10 = (i7 + i8) * 31;
            boolean z5 = this.firstOpen;
            if (z5) {
                z5 = true;
            }
            int i11 = z5 ? 1 : 0;
            int i12 = z5 ? 1 : 0;
            int i13 = (i10 + i11) * 31;
            boolean z6 = this.addGuildHint;
            if (!z6) {
                i = z6 ? 1 : 0;
            }
            int i14 = (i13 + i) * 31;
            Long l = this.guildBoostHintGuildId;
            return i14 + (l != null ? l.hashCode() : 0);
        }

        public String toString() {
            StringBuilder R = a.R("NuxState(postRegister=");
            R.append(this.postRegister);
            R.append(", postRegisterWithInvite=");
            R.append(this.postRegisterWithInvite);
            R.append(", contactSyncCompleted=");
            R.append(this.contactSyncCompleted);
            R.append(", firstOpen=");
            R.append(this.firstOpen);
            R.append(", addGuildHint=");
            R.append(this.addGuildHint);
            R.append(", guildBoostHintGuildId=");
            return a.F(R, this.guildBoostHintGuildId, ")");
        }

        public /* synthetic */ NuxState(boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, Long l, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? false : z2, (i & 2) != 0 ? false : z3, (i & 4) != 0 ? false : z4, (i & 8) != 0 ? false : z5, (i & 16) == 0 ? z6 : false, (i & 32) != 0 ? null : l);
        }
    }

    public StoreNux(Dispatcher dispatcher) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        this.dispatcher = dispatcher;
        NuxState nuxState = new NuxState(false, false, false, false, false, null, 63, null);
        this.nuxState = nuxState;
        this.nuxStateSubject = BehaviorSubject.l0(nuxState);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void publishNuxUpdated(NuxState nuxState) {
        this.nuxStateSubject.onNext(nuxState);
    }

    public final void clearNux() {
        this.dispatcher.schedule(new StoreNux$clearNux$1(this));
    }

    public final Observable<NuxState> getNuxState() {
        Observable<NuxState> q = this.nuxStateSubject.q();
        m.checkNotNullExpressionValue(q, "nuxStateSubject.distinctUntilChanged()");
        return q;
    }

    @StoreThread
    public final void handleGuildSelected(long j) {
        Long guildBoostHintGuildId = this.nuxState.getGuildBoostHintGuildId();
        if (guildBoostHintGuildId == null || guildBoostHintGuildId.longValue() != j) {
            NuxState copy$default = NuxState.copy$default(this.nuxState, false, false, false, false, false, null, 31, null);
            this.nuxState = copy$default;
            publishNuxUpdated(copy$default);
        }
    }

    @StoreThread
    public final void handleSamplePremiumGuildSelected(long j) {
        NuxState copy$default = NuxState.copy$default(this.nuxState, false, false, false, false, false, Long.valueOf(j), 31, null);
        this.nuxState = copy$default;
        publishNuxUpdated(copy$default);
    }

    public final void setContactSyncCompleted(boolean z2) {
        updateNux(new StoreNux$setContactSyncCompleted$1(z2));
    }

    public final void setFirstOpen(boolean z2) {
        updateNux(new StoreNux$setFirstOpen$1(z2));
    }

    public final void setPostRegister(boolean z2) {
        updateNux(new StoreNux$setPostRegister$1(z2));
    }

    public final void setPostRegisterWithInvite(boolean z2) {
        updateNux(new StoreNux$setPostRegisterWithInvite$1(z2));
    }

    public final void setPremiumGuildHintGuildId(Long l) {
        updateNux(new StoreNux$setPremiumGuildHintGuildId$1(l));
    }

    public final void updateNux(Function1<? super NuxState, NuxState> function1) {
        m.checkNotNullParameter(function1, "updateFunction");
        this.dispatcher.schedule(new StoreNux$updateNux$1(this, function1));
    }
}
