package com.discord.stores;

import com.discord.utilities.lazy.memberlist.ChannelMemberList;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreChannelMembers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;", "invoke", "()Lcom/discord/utilities/lazy/memberlist/ChannelMemberList;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreChannelMembers$observeChannelMemberList$1 extends o implements Function0<ChannelMemberList> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ StoreChannelMembers this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreChannelMembers$observeChannelMemberList$1(StoreChannelMembers storeChannelMembers, long j, long j2) {
        super(0);
        this.this$0 = storeChannelMembers;
        this.$guildId = j;
        this.$channelId = j2;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final ChannelMemberList invoke() {
        return this.this$0.getChannelMemberList(this.$guildId, this.$channelId);
    }
}
