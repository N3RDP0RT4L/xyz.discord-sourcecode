package com.discord.stores;

import com.discord.models.guild.Guild;
import d0.z.d.o;
import java.util.LinkedHashMap;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreGuildsSorted.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a&\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u00030\u0000j\u0012\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003`\u0004H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Ljava/util/LinkedHashMap;", "", "Lcom/discord/primitives/GuildId;", "Lcom/discord/models/guild/Guild;", "Lkotlin/collections/LinkedHashMap;", "invoke", "()Ljava/util/LinkedHashMap;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildsSorted$observeOrderedGuilds$1 extends o implements Function0<LinkedHashMap<Long, Guild>> {
    public final /* synthetic */ StoreGuildsSorted this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGuildsSorted$observeOrderedGuilds$1(StoreGuildsSorted storeGuildsSorted) {
        super(0);
        this.this$0 = storeGuildsSorted;
    }

    @Override // kotlin.jvm.functions.Function0
    public final LinkedHashMap<Long, Guild> invoke() {
        return this.this$0.getOrderedGuilds();
    }
}
