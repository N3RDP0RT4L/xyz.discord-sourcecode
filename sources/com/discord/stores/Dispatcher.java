package com.discord.stores;

import andhook.lib.HookHelper;
import com.discord.app.AppLog;
import com.discord.utilities.logging.Logger;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Iterator;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Scheduler;
import rx.functions.Action0;
/* compiled from: Dispatcher.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\u0014\u001a\u00020\u0013\u0012\b\b\u0002\u0010\u0019\u001a\u00020\u0018¢\u0006\u0004\b\u001f\u0010 J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u001b\u0010\u0007\u001a\u00020\u00022\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00020\u0005¢\u0006\u0004\b\u0007\u0010\bJ#\u0010\f\u001a\u00020\u00022\u0012\u0010\u000b\u001a\n\u0012\u0006\b\u0001\u0012\u00020\n0\t\"\u00020\nH\u0007¢\u0006\u0004\b\f\u0010\rJ#\u0010\u000e\u001a\u00020\u00022\u0012\u0010\u000b\u001a\n\u0012\u0006\b\u0001\u0012\u00020\n0\t\"\u00020\nH\u0007¢\u0006\u0004\b\u000e\u0010\rJ\u000f\u0010\u000f\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u000f\u0010\u0004R\u0018\u0010\u0011\u001a\u0004\u0018\u00010\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0014\u001a\u00020\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0019\u001a\u00020\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR&\u0010\u001d\u001a\u0012\u0012\u0004\u0012\u00020\n0\u001bj\b\u0012\u0004\u0012\u00020\n`\u001c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u001e¨\u0006!"}, d2 = {"Lcom/discord/stores/Dispatcher;", "", "", "assertCleanDispatch", "()V", "Lkotlin/Function0;", "action", "schedule", "(Lkotlin/jvm/functions/Function0;)V", "", "Lcom/discord/stores/DispatchHandler;", "dispatchHandlerArgs", "registerDispatchHandlers", "([Lcom/discord/stores/DispatchHandler;)V", "unregisterDispatchHandlers", "onDispatchEnded", "Ljava/lang/Thread;", "dispatcherThread", "Ljava/lang/Thread;", "Lrx/Scheduler;", "scheduler", "Lrx/Scheduler;", "getScheduler", "()Lrx/Scheduler;", "", "assertCleanDispatches", "Z", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "dispatchHandlers", "Ljava/util/ArrayList;", HookHelper.constructorName, "(Lrx/Scheduler;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class Dispatcher {
    private final boolean assertCleanDispatches;
    private final ArrayList<DispatchHandler> dispatchHandlers;
    private Thread dispatcherThread;
    private final Scheduler scheduler;

    public Dispatcher(Scheduler scheduler, boolean z2) {
        m.checkNotNullParameter(scheduler, "scheduler");
        this.scheduler = scheduler;
        this.assertCleanDispatches = z2;
        this.dispatchHandlers = new ArrayList<>();
    }

    private final void assertCleanDispatch() {
        if (Thread.currentThread() == this.dispatcherThread) {
            AppLog.g.w("dirty dispatch", new RuntimeException());
        }
    }

    public final Scheduler getScheduler() {
        return this.scheduler;
    }

    @StoreThread
    public final void onDispatchEnded() {
        Iterator<DispatchHandler> it = this.dispatchHandlers.iterator();
        while (it.hasNext()) {
            it.next().onDispatchEnded();
        }
    }

    @StoreThread
    public final void registerDispatchHandlers(DispatchHandler... dispatchHandlerArr) {
        m.checkNotNullParameter(dispatchHandlerArr, "dispatchHandlerArgs");
        for (DispatchHandler dispatchHandler : dispatchHandlerArr) {
            this.dispatchHandlers.add(dispatchHandler);
        }
    }

    public final void schedule(final Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "action");
        if (this.dispatchHandlers.isEmpty()) {
            AppLog appLog = AppLog.g;
            String name = Dispatcher.class.getName();
            m.checkNotNullExpressionValue(name, "javaClass.name");
            Logger.e$default(appLog, name, "scheduled an action without registering DispatchHandlers", null, null, 12, null);
        }
        if (this.assertCleanDispatches) {
            assertCleanDispatch();
        }
        this.scheduler.a().a(new Action0() { // from class: com.discord.stores.Dispatcher$schedule$1
            @Override // rx.functions.Action0
            public final void call() {
                Thread thread;
                thread = Dispatcher.this.dispatcherThread;
                if (thread == null) {
                    Dispatcher.this.dispatcherThread = Thread.currentThread();
                }
                function0.invoke();
                Dispatcher.this.onDispatchEnded();
            }
        });
    }

    @StoreThread
    public final void unregisterDispatchHandlers(DispatchHandler... dispatchHandlerArr) {
        m.checkNotNullParameter(dispatchHandlerArr, "dispatchHandlerArgs");
        for (DispatchHandler dispatchHandler : dispatchHandlerArr) {
            this.dispatchHandlers.remove(dispatchHandler);
        }
    }

    public /* synthetic */ Dispatcher(Scheduler scheduler, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(scheduler, (i & 2) != 0 ? false : z2);
    }
}
