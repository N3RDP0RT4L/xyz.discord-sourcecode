package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.commands.ApplicationCommandAutocompleteResult;
import com.discord.api.commands.GuildApplicationCommands;
import com.discord.api.permission.Permission;
import com.discord.models.commands.Application;
import com.discord.models.commands.ApplicationCommand;
import com.discord.models.domain.ModelPayload;
import com.discord.models.domain.NonceGenerator;
import com.discord.models.user.User;
import com.discord.stores.CommandAutocompleteState;
import com.discord.stores.DiscoverCommands;
import com.discord.stores.LoadState;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.chat.input.models.ApplicationCommandData;
import d0.d0.f;
import d0.g0.t;
import d0.g0.w;
import d0.t.g0;
import d0.t.h0;
import d0.t.n;
import d0.t.o;
import d0.t.s;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Ref$IntRef;
import rx.Observable;
import rx.functions.Func2;
/* compiled from: StoreApplicationCommands.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0094\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010$\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0010%\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010!\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 ¸\u00012\u00020\u0001:\u0004¸\u0001¹\u0001B{\u0012\b\u0010\u0096\u0001\u001a\u00030\u0095\u0001\u0012\b\u0010¨\u0001\u001a\u00030§\u0001\u0012\b\u0010\u008d\u0001\u001a\u00030\u008c\u0001\u0012\b\u0010¥\u0001\u001a\u00030¤\u0001\u0012\b\u0010¯\u0001\u001a\u00030®\u0001\u0012\b\u0010\u0082\u0001\u001a\u00030\u0081\u0001\u0012\b\u0010\u0087\u0001\u001a\u00030\u0086\u0001\u0012\b\b\u0002\u0010z\u001a\u00020y\u0012\n\b\u0002\u0010³\u0001\u001a\u00030²\u0001\u0012\b\b\u0002\u0010\u007f\u001a\u00020~\u0012\n\b\u0002\u0010¡\u0001\u001a\u00030 \u0001¢\u0006\u0006\b¶\u0001\u0010·\u0001JE\u0010\u000b\u001a\u00020\n2\u000e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00052\u0010\b\u0002\u0010\b\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00072\b\b\u0002\u0010\t\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u000f\u0010\u000e\u001a\u00020\rH\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ%\u0010\u0014\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\r2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011H\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u001d\u0010\u0017\u001a\u00020\n2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00160\u0011H\u0003¢\u0006\u0004\b\u0017\u0010\u0018J\u001d\u0010\u0019\u001a\u00020\n2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011H\u0003¢\u0006\u0004\b\u0019\u0010\u0018J\u001d\u0010\u001a\u001a\u00020\n2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011H\u0003¢\u0006\u0004\b\u001a\u0010\u0018J\u001b\u0010\u001b\u001a\u00020\n2\n\u0010\b\u001a\u00060\u0002j\u0002`\u0007H\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ\u001f\u0010 \u001a\u00020\u001f2\u000e\u0010\u001e\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u001dH\u0002¢\u0006\u0004\b \u0010!J'\u0010&\u001a\u00020\n2\u0006\u0010\"\u001a\u00020\r2\u0006\u0010#\u001a\u00020\r2\u0006\u0010%\u001a\u00020$H\u0003¢\u0006\u0004\b&\u0010'J\r\u0010)\u001a\u00020(¢\u0006\u0004\b)\u0010*J\u0013\u0010+\u001a\b\u0012\u0004\u0012\u00020\u00160\u0011¢\u0006\u0004\b+\u0010,J\u0019\u0010.\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00160-¢\u0006\u0004\b.\u0010/J\u0013\u00100\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011¢\u0006\u0004\b0\u0010,J%\u00101\u001a\u001a\u0012\u0004\u0012\u00020\r\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020$0-0-¢\u0006\u0004\b1\u0010/J\u001f\u00102\u001a\b\u0012\u0004\u0012\u00020\u00120\u00112\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b2\u00103J\u001f\u00107\u001a\b\u0012\u0004\u0012\u00020(062\n\u00105\u001a\u00060\u0002j\u0002`4¢\u0006\u0004\b7\u00108J%\u00109\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u0011062\n\u00105\u001a\u00060\u0002j\u0002`4¢\u0006\u0004\b9\u00108J%\u0010:\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00160\u0011062\n\u00105\u001a\u00060\u0002j\u0002`4¢\u0006\u0004\b:\u00108J+\u0010;\u001a \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\r\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020$0-0-06¢\u0006\u0004\b;\u0010<J%\u0010=\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u0011062\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b=\u00108J\u0015\u0010?\u001a\u00020\n2\u0006\u0010>\u001a\u00020\u001f¢\u0006\u0004\b?\u0010@J\u0017\u0010C\u001a\u00020\n2\u0006\u0010B\u001a\u00020AH\u0007¢\u0006\u0004\bC\u0010DJ\r\u0010E\u001a\u00020\n¢\u0006\u0004\bE\u0010FJ\r\u0010G\u001a\u00020\n¢\u0006\u0004\bG\u0010FJ\u0019\u0010H\u001a\u00020\n2\n\u0010\b\u001a\u00060\u0002j\u0002`\u0007¢\u0006\u0004\bH\u0010\u001cJ7\u0010J\u001a\u00020\n2\u000e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\u000e\u0010\b\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00072\b\b\u0002\u0010I\u001a\u00020\u001f¢\u0006\u0004\bJ\u0010KJ\u0015\u0010N\u001a\u00020\n2\u0006\u0010M\u001a\u00020L¢\u0006\u0004\bN\u0010OJ\u001d\u0010P\u001a\u00020\n2\u000e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003¢\u0006\u0004\bP\u0010QJ\u0019\u0010R\u001a\u00020\n2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\bR\u0010\u001cJ%\u0010T\u001a\u00020\n2\u000e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\u0006\u0010S\u001a\u00020\r¢\u0006\u0004\bT\u0010UJ1\u0010X\u001a\u00020\n2\u000e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\n\u00105\u001a\u00060\u0002j\u0002`42\u0006\u0010W\u001a\u00020V¢\u0006\u0004\bX\u0010YJ\u0017\u0010\\\u001a\u00020\n2\u0006\u0010[\u001a\u00020ZH\u0007¢\u0006\u0004\b\\\u0010]J\r\u0010^\u001a\u00020\n¢\u0006\u0004\b^\u0010FJ\r\u0010_\u001a\u00020\n¢\u0006\u0004\b_\u0010FJ\u0017\u0010b\u001a\u00020\n2\u0006\u0010a\u001a\u00020`H\u0007¢\u0006\u0004\bb\u0010cJ\u000f\u0010d\u001a\u00020\nH\u0016¢\u0006\u0004\bd\u0010FJ\u0019\u0010e\u001a\u00020\u001f2\n\u0010\b\u001a\u00060\u0002j\u0002`\u0007¢\u0006\u0004\be\u0010fJ\u001f\u0010g\u001a\u00020\n2\u0006\u0010\"\u001a\u00020\r2\u0006\u0010#\u001a\u00020\rH\u0007¢\u0006\u0004\bg\u0010hJ\u001f\u0010i\u001a\u00020\n2\u0006\u0010\"\u001a\u00020\r2\u0006\u0010#\u001a\u00020\rH\u0007¢\u0006\u0004\bi\u0010hR\u0016\u0010j\u001a\u00020(8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bj\u0010kR\u001c\u0010l\u001a\b\u0012\u0004\u0012\u00020\u00160\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bl\u0010mR\u001c\u0010n\u001a\b\u0012\u0004\u0012\u00020\u00160\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bn\u0010mR\u0018\u0010o\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bo\u0010pR\u001e\u0010q\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bq\u0010pR,\u0010s\u001a\u0018\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u00110r8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bs\u0010tR\u001e\u0010u\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bu\u0010pR\u0018\u0010S\u001a\u0004\u0018\u00010\r8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bS\u0010vR\"\u0010x\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020w0r8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bx\u0010tR\u0016\u0010z\u001a\u00020y8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bz\u0010{R\u0016\u0010|\u001a\u00020\u00058\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b|\u0010}R\u0017\u0010\u007f\u001a\u00020~8\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b\u007f\u0010\u0080\u0001R\u001a\u0010\u0082\u0001\u001a\u00030\u0081\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0082\u0001\u0010\u0083\u0001R\u0019\u0010\u0084\u0001\u001a\u00020\u001f8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u0084\u0001\u0010\u0085\u0001R\u001a\u0010\u0087\u0001\u001a\u00030\u0086\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0087\u0001\u0010\u0088\u0001R\u0019\u0010\u0089\u0001\u001a\u00020\u001f8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u0089\u0001\u0010\u0085\u0001R\u001a\u0010\u008a\u0001\u001a\u0004\u0018\u00010\r8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u008a\u0001\u0010vR$\u0010\u008b\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00160-8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u008b\u0001\u0010tR\u001a\u0010\u008d\u0001\u001a\u00030\u008c\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u008d\u0001\u0010\u008e\u0001R(\u0010\u008f\u0001\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0007\u0012\u0004\u0012\u00020\u00050r8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u008f\u0001\u0010tR\u001e\u0010\u0090\u0001\u001a\b\u0012\u0004\u0012\u00020\u00120\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u0090\u0001\u0010mR.\u0010\u0091\u0001\u001a\u0018\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u00110-8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u0091\u0001\u0010tR0\u0010\u0092\u0001\u001a\u001a\u0012\u0004\u0012\u00020\r\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020$0r0r8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u0092\u0001\u0010tR\u001a\u0010\u0093\u0001\u001a\u0004\u0018\u00010\r8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u0093\u0001\u0010vR\u0018\u0010\u0094\u0001\u001a\u00020(8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u0094\u0001\u0010kR\u001a\u0010\u0096\u0001\u001a\u00030\u0095\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0096\u0001\u0010\u0097\u0001R\u0018\u0010\u0098\u0001\u001a\u00020\u00058\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u0098\u0001\u0010}R \u0010\u0099\u0001\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00078\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u0099\u0001\u0010pR \u0010\u009a\u0001\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u009a\u0001\u0010pR\u001f\u0010\u009c\u0001\u001a\t\u0012\u0004\u0012\u00020\u00120\u009b\u00018\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b\u009c\u0001\u0010mR\u001b\u0010\u009d\u0001\u001a\u0004\u0018\u00010L8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u009d\u0001\u0010\u009e\u0001R0\u0010\u009f\u0001\u001a\u001a\u0012\u0004\u0012\u00020\r\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020$0-0-8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u009f\u0001\u0010tR\u001a\u0010¡\u0001\u001a\u00030 \u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b¡\u0001\u0010¢\u0001R\u001a\u0010£\u0001\u001a\u0004\u0018\u00010\r8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b£\u0001\u0010vR\u001a\u0010¥\u0001\u001a\u00030¤\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b¥\u0001\u0010¦\u0001R\u001a\u0010¨\u0001\u001a\u00030§\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b¨\u0001\u0010©\u0001R\u0017\u0010>\u001a\u00020\u001f8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b>\u0010\u0085\u0001R\u0018\u0010ª\u0001\u001a\u00020\u00058\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\bª\u0001\u0010}R\u001a\u0010«\u0001\u001a\u0004\u0018\u00010\r8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b«\u0001\u0010vR%\u0010\u00ad\u0001\u001a\u000f\u0012\u0004\u0012\u00020\r\u0012\u0005\u0012\u00030¬\u00010r8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u00ad\u0001\u0010tR\u001a\u0010¯\u0001\u001a\u00030®\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b¯\u0001\u0010°\u0001R\u0019\u0010±\u0001\u001a\u00020\u001f8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b±\u0001\u0010\u0085\u0001R\u001a\u0010³\u0001\u001a\u00030²\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b³\u0001\u0010´\u0001R\u0018\u0010µ\u0001\u001a\u00020\u00058\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\bµ\u0001\u0010}¨\u0006º\u0001"}, d2 = {"Lcom/discord/stores/StoreApplicationCommands;", "Lcom/discord/stores/StoreV2;", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "offset", "Lcom/discord/primitives/ApplicationId;", "applicationId", "limit", "", "requestApplicationCommands", "(Ljava/lang/Long;ILjava/lang/Long;I)V", "", "generateNonce", "()Ljava/lang/String;", "nonce", "", "Lcom/discord/models/commands/ApplicationCommand;", "commands", "handleFrecencyCommandsUpdate", "(Ljava/lang/String;Ljava/util/List;)V", "Lcom/discord/models/commands/Application;", "handleGuildApplicationsUpdate", "(Ljava/util/List;)V", "handleQueryCommandsUpdate", "handleDiscoverCommandsUpdate", "getApplicationCommandsViaRest", "(J)V", "Lcom/discord/api/permission/PermissionBit;", "channelPermissions", "", "shouldReturnApplicationCommands", "(Ljava/lang/Long;)Z", "commandOptionName", "queryString", "Lcom/discord/stores/CommandAutocompleteState;", "state", "setAutocompleteState", "(Ljava/lang/String;Ljava/lang/String;Lcom/discord/stores/CommandAutocompleteState;)V", "Lcom/discord/stores/DiscoverCommands;", "getDiscoverCommands", "()Lcom/discord/stores/DiscoverCommands;", "getApplications", "()Ljava/util/List;", "", "getApplicationMap", "()Ljava/util/Map;", "getQueryCommands", "getAutocompleteOptionResults", "getFrecencyCommands", "(J)Ljava/util/List;", "Lcom/discord/primitives/ChannelId;", "channelId", "Lrx/Observable;", "observeDiscoverCommands", "(J)Lrx/Observable;", "observeQueryCommands", "observeGuildApplications", "observeAutocompleteResults", "()Lrx/Observable;", "observeFrecencyCommands", "connectionReady", "handleConnectionReady", "(Z)V", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "requestLoadMoreUp", "()V", "requestLoadMoreDown", "requestDiscoverCommands", "forDmDiscover", "requestInitialApplicationCommands", "(Ljava/lang/Long;Ljava/lang/Long;Z)V", "Lcom/discord/models/user/User;", "botUser", "handleDmUserApplication", "(Lcom/discord/models/user/User;)V", "requestApplications", "(Ljava/lang/Long;)V", "requestFrecencyCommands", "query", "requestApplicationCommandsQuery", "(Ljava/lang/Long;Ljava/lang/String;)V", "Lcom/discord/widgets/chat/input/models/ApplicationCommandData;", "data", "requestApplicationCommandAutocompleteData", "(Ljava/lang/Long;JLcom/discord/widgets/chat/input/models/ApplicationCommandData;)V", "Lcom/discord/api/commands/ApplicationCommandAutocompleteResult;", "autocompleteResult", "handleApplicationCommandAutocompleteResult", "(Lcom/discord/api/commands/ApplicationCommandAutocompleteResult;)V", "clearAutocompleteResults", "clearQueryCommands", "Lcom/discord/api/commands/GuildApplicationCommands;", "commandsGateway", "handleApplicationCommandsUpdate", "(Lcom/discord/api/commands/GuildApplicationCommands;)V", "snapshotData", "hasFetchedApplicationCommands", "(J)Z", "setAutocompleteLoading", "(Ljava/lang/String;Ljava/lang/String;)V", "setAutocompleteFailed", "discoverCommands", "Lcom/discord/stores/DiscoverCommands;", "applications", "Ljava/util/List;", "applicationsSnapshot", "jumpedApplicationId", "Ljava/lang/Long;", "queryGuildId", "", "frecencyCommands", "Ljava/util/Map;", "pendingGatewayGuildId", "Ljava/lang/String;", "Lcom/discord/stores/CommandOptionAutocompleteQuery;", "autocompleteNonceData", "Lcom/discord/utilities/rest/RestAPI;", "restApi", "Lcom/discord/utilities/rest/RestAPI;", "numRemoteCommands", "I", "Lcom/discord/stores/BuiltInCommandsProvider;", "builtInCommandsProvider", "Lcom/discord/stores/BuiltInCommandsProvider;", "Lcom/discord/stores/StoreExperiments;", "storeExperiments", "Lcom/discord/stores/StoreExperiments;", "loadDirectionDown", "Z", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "loadDirectionUp", "sessionId", "applicationsMapSnapshot", "Lcom/discord/stores/StoreApplicationCommandFrecency;", "storeApplicationCommandFrecency", "Lcom/discord/stores/StoreApplicationCommandFrecency;", "applicationCommandIndexes", "queryCommandsSnapshot", "frecencyCommandsSnapshot", "autocompleteOptionResults", "queryNonce", "discoverCommandsSnapshot", "Lcom/discord/stores/StoreGatewayConnection;", "storeGatewayConnection", "Lcom/discord/stores/StoreGatewayConnection;", "currentStartOffset", "discoverApplicationId", "discoverGuildId", "", "queryCommands", "pendingBotUser", "Lcom/discord/models/user/User;", "autocompleteOptionResultsSnapshot", "Lcom/discord/models/domain/NonceGenerator;", "nonceGenerator", "Lcom/discord/models/domain/NonceGenerator;", "applicationNonce", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreGuilds;", "Lcom/discord/stores/StorePermissions;", "storePermissions", "Lcom/discord/stores/StorePermissions;", "currentEndOffset", "discoverCommandsNonce", "Lcom/discord/stores/StoreApplicationCommands$FrecencyRequest;", "frecencyRequests", "Lcom/discord/stores/StoreUser;", "storeUsers", "Lcom/discord/stores/StoreUser;", "isLoadingDiscoveryCommands", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "jumpedSequenceId", HookHelper.constructorName, "(Lcom/discord/stores/StoreGatewayConnection;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreApplicationCommandFrecency;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreExperiments;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/BuiltInCommandsProvider;Lcom/discord/models/domain/NonceGenerator;)V", "Companion", "FrecencyRequest", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreApplicationCommands extends StoreV2 {
    public static final int COMMANDS_LIMIT_PER_REQUEST = 20;
    public static final long TYPE_APPLICATION_COMMAND = 2;
    public static final long TYPE_APPLICATION_COMMAND_AUTOCOMPLETE = 4;
    private Map<Long, Integer> applicationCommandIndexes;
    private String applicationNonce;
    private List<Application> applications;
    private Map<Long, Application> applicationsMapSnapshot;
    private List<Application> applicationsSnapshot;
    private Map<String, CommandOptionAutocompleteQuery> autocompleteNonceData;
    private Map<String, Map<String, CommandAutocompleteState>> autocompleteOptionResults;
    private Map<String, ? extends Map<String, ? extends CommandAutocompleteState>> autocompleteOptionResultsSnapshot;
    private final BuiltInCommandsProvider builtInCommandsProvider;
    private boolean connectionReady;
    private int currentEndOffset;
    private int currentStartOffset;
    private Long discoverApplicationId;
    private DiscoverCommands discoverCommands;
    private String discoverCommandsNonce;
    private DiscoverCommands discoverCommandsSnapshot;
    private Long discoverGuildId;
    private final Dispatcher dispatcher;
    private Map<Long, List<ApplicationCommand>> frecencyCommands;
    private Map<Long, ? extends List<? extends ApplicationCommand>> frecencyCommandsSnapshot;
    private Map<String, FrecencyRequest> frecencyRequests;
    private boolean isLoadingDiscoveryCommands;
    private Long jumpedApplicationId;
    private int jumpedSequenceId;
    private boolean loadDirectionDown;
    private boolean loadDirectionUp;
    private final NonceGenerator nonceGenerator;
    private int numRemoteCommands;
    private final ObservationDeck observationDeck;
    private User pendingBotUser;
    private Long pendingGatewayGuildId;
    private String query;
    private final List<ApplicationCommand> queryCommands;
    private List<? extends ApplicationCommand> queryCommandsSnapshot;
    private Long queryGuildId;
    private String queryNonce;
    private final RestAPI restApi;
    private String sessionId;
    private final StoreApplicationCommandFrecency storeApplicationCommandFrecency;
    private final StoreExperiments storeExperiments;
    private final StoreGatewayConnection storeGatewayConnection;
    private final StoreGuilds storeGuilds;
    private final StorePermissions storePermissions;
    private final StoreUser storeUsers;
    public static final Companion Companion = new Companion(null);
    private static final ObservationDeck.UpdateSource DiscoverCommandsUpdate = new ObservationDeck.UpdateSource() { // from class: com.discord.stores.StoreApplicationCommands$Companion$DiscoverCommandsUpdate$1
    };
    private static final ObservationDeck.UpdateSource QueryCommandsUpdate = new ObservationDeck.UpdateSource() { // from class: com.discord.stores.StoreApplicationCommands$Companion$QueryCommandsUpdate$1
    };
    private static final ObservationDeck.UpdateSource GuildApplicationsUpdate = new ObservationDeck.UpdateSource() { // from class: com.discord.stores.StoreApplicationCommands$Companion$GuildApplicationsUpdate$1
    };
    private static final ObservationDeck.UpdateSource AutocompleteResultsUpdate = new ObservationDeck.UpdateSource() { // from class: com.discord.stores.StoreApplicationCommands$Companion$AutocompleteResultsUpdate$1
    };
    private static final ObservationDeck.UpdateSource FrecencyCommandsUpdate = new ObservationDeck.UpdateSource() { // from class: com.discord.stores.StoreApplicationCommands$Companion$FrecencyCommandsUpdate$1
    };

    /* compiled from: StoreApplicationCommands.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0016\u0010\u0017R\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0004\u001a\u0004\b\b\u0010\u0006R\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0004\u001a\u0004\b\n\u0010\u0006R\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0004\u001a\u0004\b\f\u0010\u0006R\u0019\u0010\r\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u0004\u001a\u0004\b\u000e\u0010\u0006R\u0016\u0010\u0010\u001a\u00020\u000f8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0013\u001a\u00020\u00128\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0015\u001a\u00020\u00128\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0015\u0010\u0014¨\u0006\u0018"}, d2 = {"Lcom/discord/stores/StoreApplicationCommands$Companion;", "", "Lcom/discord/stores/updates/ObservationDeck$UpdateSource;", "GuildApplicationsUpdate", "Lcom/discord/stores/updates/ObservationDeck$UpdateSource;", "getGuildApplicationsUpdate", "()Lcom/discord/stores/updates/ObservationDeck$UpdateSource;", "QueryCommandsUpdate", "getQueryCommandsUpdate", "FrecencyCommandsUpdate", "getFrecencyCommandsUpdate", "AutocompleteResultsUpdate", "getAutocompleteResultsUpdate", "DiscoverCommandsUpdate", "getDiscoverCommandsUpdate", "", "COMMANDS_LIMIT_PER_REQUEST", "I", "", "TYPE_APPLICATION_COMMAND", "J", "TYPE_APPLICATION_COMMAND_AUTOCOMPLETE", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public final ObservationDeck.UpdateSource getAutocompleteResultsUpdate() {
            return StoreApplicationCommands.AutocompleteResultsUpdate;
        }

        public final ObservationDeck.UpdateSource getDiscoverCommandsUpdate() {
            return StoreApplicationCommands.DiscoverCommandsUpdate;
        }

        public final ObservationDeck.UpdateSource getFrecencyCommandsUpdate() {
            return StoreApplicationCommands.FrecencyCommandsUpdate;
        }

        public final ObservationDeck.UpdateSource getGuildApplicationsUpdate() {
            return StoreApplicationCommands.GuildApplicationsUpdate;
        }

        public final ObservationDeck.UpdateSource getQueryCommandsUpdate() {
            return StoreApplicationCommands.QueryCommandsUpdate;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreApplicationCommands.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\u000e\u0010\n\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\u0004\b\u001b\u0010\u001cJ\u0018\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0016\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ2\u0010\f\u001a\u00020\u00002\u0010\b\u0002\u0010\n\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\u000e\b\u0002\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u001f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00070\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0017\u001a\u0004\b\u0018\u0010\tR!\u0010\n\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0019\u001a\u0004\b\u001a\u0010\u0005¨\u0006\u001d"}, d2 = {"Lcom/discord/stores/StoreApplicationCommands$FrecencyRequest;", "", "", "Lcom/discord/primitives/GuildId;", "component1", "()Ljava/lang/Long;", "", "", "component2", "()Ljava/util/List;", "guildId", "applicationCommandIds", "copy", "(Ljava/lang/Long;Ljava/util/List;)Lcom/discord/stores/StoreApplicationCommands$FrecencyRequest;", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getApplicationCommandIds", "Ljava/lang/Long;", "getGuildId", HookHelper.constructorName, "(Ljava/lang/Long;Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class FrecencyRequest {
        private final List<String> applicationCommandIds;
        private final Long guildId;

        public FrecencyRequest(Long l, List<String> list) {
            m.checkNotNullParameter(list, "applicationCommandIds");
            this.guildId = l;
            this.applicationCommandIds = list;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ FrecencyRequest copy$default(FrecencyRequest frecencyRequest, Long l, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                l = frecencyRequest.guildId;
            }
            if ((i & 2) != 0) {
                list = frecencyRequest.applicationCommandIds;
            }
            return frecencyRequest.copy(l, list);
        }

        public final Long component1() {
            return this.guildId;
        }

        public final List<String> component2() {
            return this.applicationCommandIds;
        }

        public final FrecencyRequest copy(Long l, List<String> list) {
            m.checkNotNullParameter(list, "applicationCommandIds");
            return new FrecencyRequest(l, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof FrecencyRequest)) {
                return false;
            }
            FrecencyRequest frecencyRequest = (FrecencyRequest) obj;
            return m.areEqual(this.guildId, frecencyRequest.guildId) && m.areEqual(this.applicationCommandIds, frecencyRequest.applicationCommandIds);
        }

        public final List<String> getApplicationCommandIds() {
            return this.applicationCommandIds;
        }

        public final Long getGuildId() {
            return this.guildId;
        }

        public int hashCode() {
            Long l = this.guildId;
            int i = 0;
            int hashCode = (l != null ? l.hashCode() : 0) * 31;
            List<String> list = this.applicationCommandIds;
            if (list != null) {
                i = list.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("FrecencyRequest(guildId=");
            R.append(this.guildId);
            R.append(", applicationCommandIds=");
            return a.K(R, this.applicationCommandIds, ")");
        }
    }

    public /* synthetic */ StoreApplicationCommands(StoreGatewayConnection storeGatewayConnection, StorePermissions storePermissions, StoreApplicationCommandFrecency storeApplicationCommandFrecency, StoreGuilds storeGuilds, StoreUser storeUser, StoreExperiments storeExperiments, Dispatcher dispatcher, RestAPI restAPI, ObservationDeck observationDeck, BuiltInCommandsProvider builtInCommandsProvider, NonceGenerator nonceGenerator, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(storeGatewayConnection, storePermissions, storeApplicationCommandFrecency, storeGuilds, storeUser, storeExperiments, dispatcher, (i & 128) != 0 ? RestAPI.Companion.getApi() : restAPI, (i & 256) != 0 ? ObservationDeckProvider.get() : observationDeck, (i & 512) != 0 ? new BuiltInCommands() : builtInCommandsProvider, (i & 1024) != 0 ? new NonceGenerator() : nonceGenerator);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final String generateNonce() {
        return String.valueOf(this.nonceGenerator.nonce());
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void getApplicationCommandsViaRest(long j) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(this.restApi.getApplicationCommands(j), false, 1, null), StoreApplicationCommands.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new StoreApplicationCommands$getApplicationCommandsViaRest$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreApplicationCommands$getApplicationCommandsViaRest$1(this, j));
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleDiscoverCommandsUpdate(List<? extends ApplicationCommand> list) {
        LoadState loadState;
        this.isLoadingDiscoveryCommands = false;
        List<ApplicationCommand> mutableList = u.toMutableList((Collection) this.discoverCommands.getCommands());
        int size = mutableList.size();
        Long l = this.jumpedApplicationId;
        if (l != null) {
            mutableList = u.toMutableList((Collection) list);
            Integer num = this.applicationCommandIndexes.get(l);
            this.currentStartOffset = num != null ? num.intValue() : 0;
        } else if (this.loadDirectionUp) {
            ArrayList arrayList = new ArrayList();
            for (Object obj : list) {
                ApplicationCommand applicationCommand = (ApplicationCommand) obj;
                ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(mutableList, 10));
                for (ApplicationCommand applicationCommand2 : mutableList) {
                    arrayList2.add(applicationCommand2.getId());
                }
                if (!arrayList2.contains(applicationCommand.getId())) {
                    arrayList.add(obj);
                }
            }
            for (ApplicationCommand applicationCommand3 : s.asReversed(arrayList)) {
                mutableList.add(0, applicationCommand3);
            }
            this.currentStartOffset -= mutableList.size() - size;
        } else if (this.loadDirectionDown) {
            ArrayList arrayList3 = new ArrayList();
            for (Object obj2 : list) {
                ApplicationCommand applicationCommand4 = (ApplicationCommand) obj2;
                ArrayList arrayList4 = new ArrayList(o.collectionSizeOrDefault(mutableList, 10));
                for (ApplicationCommand applicationCommand5 : mutableList) {
                    arrayList4.add(applicationCommand5.getId());
                }
                if (!arrayList4.contains(applicationCommand4.getId())) {
                    arrayList3.add(obj2);
                }
            }
            mutableList = u.toMutableList((Collection) u.plus((Collection) mutableList, (Iterable) arrayList3));
        } else {
            mutableList = u.toMutableList((Collection) list);
            this.currentStartOffset = 0;
        }
        List list2 = mutableList;
        if ((list2.size() + this.currentStartOffset) - 1 == this.numRemoteCommands - 1) {
            list2.addAll(this.builtInCommandsProvider.getBuiltInCommands());
        }
        this.currentEndOffset = (list2.size() + this.currentStartOffset) - 1;
        boolean z2 = this.currentStartOffset > 0 && (list2.isEmpty() ^ true);
        boolean z3 = this.currentEndOffset < this.numRemoteCommands - 1;
        if (this.loadDirectionUp) {
            loadState = LoadState.JustLoadedUp.INSTANCE;
        } else {
            loadState = this.loadDirectionDown ? LoadState.JustLoadedDown.INSTANCE : LoadState.JustLoadedAll.INSTANCE;
        }
        this.discoverCommands = new DiscoverCommands(list2, this.currentStartOffset, this.currentEndOffset, z2, z3, this.jumpedSequenceId, this.jumpedApplicationId, loadState);
        markChanged(DiscoverCommandsUpdate);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleFrecencyCommandsUpdate(String str, List<? extends ApplicationCommand> list) {
        int intValue;
        boolean z2;
        FrecencyRequest frecencyRequest = this.frecencyRequests.get(str);
        if (frecencyRequest != null) {
            this.frecencyRequests.remove(str);
            List<ApplicationCommand> flattenSubCommands = StoreApplicationCommandsKt.flattenSubCommands(list);
            Map<Long, List<ApplicationCommand>> map = this.frecencyCommands;
            Long guildId = frecencyRequest.getGuildId();
            Long valueOf = Long.valueOf(guildId != null ? guildId.longValue() : 0L);
            List<String> applicationCommandIds = frecencyRequest.getApplicationCommandIds();
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(applicationCommandIds, 10));
            for (String str2 : applicationCommandIds) {
                Integer intOrNull = d0.g0.s.toIntOrNull(str2);
                Object obj = null;
                if (intOrNull == null || (intValue = intOrNull.intValue()) >= 0) {
                    Iterator<T> it = flattenSubCommands.iterator();
                    while (true) {
                        if (it.hasNext()) {
                            Object next = it.next();
                            if (m.areEqual(((ApplicationCommand) next).getId(), str2)) {
                                obj = next;
                                break;
                            }
                        }
                    }
                } else {
                    Iterator<T> it2 = this.builtInCommandsProvider.getBuiltInCommands().iterator();
                    while (true) {
                        if (it2.hasNext()) {
                            Object next2 = it2.next();
                            Integer intOrNull2 = d0.g0.s.toIntOrNull(((ApplicationCommand) next2).getId());
                            if (intOrNull2 != null && intOrNull2.intValue() == intValue) {
                                z2 = true;
                                continue;
                            } else {
                                z2 = false;
                                continue;
                            }
                            if (z2) {
                                obj = next2;
                                break;
                            }
                        }
                    }
                }
                arrayList.add((ApplicationCommand) obj);
            }
            map.put(valueOf, u.filterNotNull(arrayList));
            markChanged(FrecencyCommandsUpdate);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleGuildApplicationsUpdate(List<Application> list) {
        this.applications = list;
        this.numRemoteCommands = 0;
        int i = 0;
        for (Application application : list) {
            this.applicationCommandIndexes.put(Long.valueOf(application.getId()), Integer.valueOf(i));
            i += application.getCommandCount();
            if (!application.getBuiltIn()) {
                this.numRemoteCommands = application.getCommandCount() + this.numRemoteCommands;
            }
        }
        markChanged(GuildApplicationsUpdate);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleQueryCommandsUpdate(List<? extends ApplicationCommand> list) {
        this.queryCommands.clear();
        this.queryCommands.addAll(list);
        String str = this.query;
        if (str != null) {
            List<ApplicationCommand> list2 = this.queryCommands;
            List<ApplicationCommand> builtInCommands = this.builtInCommandsProvider.getBuiltInCommands();
            ArrayList arrayList = new ArrayList();
            for (Object obj : builtInCommands) {
                if (t.startsWith(((ApplicationCommand) obj).getName(), str, true)) {
                    arrayList.add(obj);
                }
            }
            list2.addAll(arrayList);
        }
        markChanged(QueryCommandsUpdate);
    }

    private final void requestApplicationCommands(Long l, int i, Long l2, int i2) {
        this.dispatcher.schedule(new StoreApplicationCommands$requestApplicationCommands$1(this, l, i, i2, l2));
    }

    public static /* synthetic */ void requestApplicationCommands$default(StoreApplicationCommands storeApplicationCommands, Long l, int i, Long l2, int i2, int i3, Object obj) {
        if ((i3 & 2) != 0) {
            i = 0;
        }
        if ((i3 & 4) != 0) {
            l2 = null;
        }
        if ((i3 & 8) != 0) {
            i2 = 20;
        }
        storeApplicationCommands.requestApplicationCommands(l, i, l2, i2);
    }

    public static /* synthetic */ void requestInitialApplicationCommands$default(StoreApplicationCommands storeApplicationCommands, Long l, Long l2, boolean z2, int i, Object obj) {
        if ((i & 4) != 0) {
            z2 = false;
        }
        storeApplicationCommands.requestInitialApplicationCommands(l, l2, z2);
    }

    @StoreThread
    private final void setAutocompleteState(String str, String str2, CommandAutocompleteState commandAutocompleteState) {
        Map<String, CommandAutocompleteState> map = this.autocompleteOptionResults.get(str);
        if (map == null) {
            map = h0.emptyMap();
        }
        if (!(map.get(str2) instanceof CommandAutocompleteState.Choices)) {
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            Map<String, CommandAutocompleteState> map2 = this.autocompleteOptionResults.get(str);
            if (map2 == null) {
                map2 = h0.emptyMap();
            }
            linkedHashMap.putAll(map2);
            linkedHashMap.put(str2, commandAutocompleteState);
            this.autocompleteOptionResults.put(str, linkedHashMap);
            markChanged(AutocompleteResultsUpdate);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final boolean shouldReturnApplicationCommands(Long l) {
        Long l2 = this.discoverGuildId;
        return l2 == null || (l2 != null && l2.longValue() == 0) || PermissionUtils.can(Permission.USE_APPLICATION_COMMANDS, l);
    }

    public final void clearAutocompleteResults() {
        this.dispatcher.schedule(new StoreApplicationCommands$clearAutocompleteResults$1(this));
    }

    public final void clearQueryCommands() {
        this.dispatcher.schedule(new StoreApplicationCommands$clearQueryCommands$1(this));
    }

    public final Map<Long, Application> getApplicationMap() {
        return this.applicationsMapSnapshot;
    }

    public final List<Application> getApplications() {
        return this.applicationsSnapshot;
    }

    public final Map<String, Map<String, CommandAutocompleteState>> getAutocompleteOptionResults() {
        return this.autocompleteOptionResultsSnapshot;
    }

    public final DiscoverCommands getDiscoverCommands() {
        return this.discoverCommandsSnapshot;
    }

    public final List<ApplicationCommand> getFrecencyCommands(long j) {
        List<ApplicationCommand> list = (List) this.frecencyCommandsSnapshot.get(Long.valueOf(j));
        return list != null ? list : n.emptyList();
    }

    public final List<ApplicationCommand> getQueryCommands() {
        return this.queryCommandsSnapshot;
    }

    @StoreThread
    public final void handleApplicationCommandAutocompleteResult(ApplicationCommandAutocompleteResult applicationCommandAutocompleteResult) {
        m.checkNotNullParameter(applicationCommandAutocompleteResult, "autocompleteResult");
        CommandOptionAutocompleteQuery commandOptionAutocompleteQuery = this.autocompleteNonceData.get(applicationCommandAutocompleteResult.b());
        if (commandOptionAutocompleteQuery != null) {
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            Map<String, CommandAutocompleteState> map = this.autocompleteOptionResults.get(commandOptionAutocompleteQuery.getCommandOptionName());
            if (map == null) {
                map = h0.emptyMap();
            }
            linkedHashMap.putAll(map);
            linkedHashMap.put(commandOptionAutocompleteQuery.getQueryString(), new CommandAutocompleteState.Choices(applicationCommandAutocompleteResult.a()));
            this.autocompleteOptionResults.put(commandOptionAutocompleteQuery.getCommandOptionName(), linkedHashMap);
            markChanged(AutocompleteResultsUpdate);
        }
    }

    @StoreThread
    public final void handleApplicationCommandsUpdate(GuildApplicationCommands guildApplicationCommands) {
        m.checkNotNullParameter(guildApplicationCommands, "commandsGateway");
        if (this.frecencyRequests.containsKey(guildApplicationCommands.c())) {
            String c = guildApplicationCommands.c();
            List<com.discord.api.commands.ApplicationCommand> a = guildApplicationCommands.a();
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(a, 10));
            for (com.discord.api.commands.ApplicationCommand applicationCommand : a) {
                arrayList.add(StoreApplicationCommandsKt.toSlashCommand(applicationCommand));
            }
            handleFrecencyCommandsUpdate(c, arrayList);
            return;
        }
        String c2 = guildApplicationCommands.c();
        if (m.areEqual(c2, this.applicationNonce)) {
            List listOf = d0.t.m.listOf(this.builtInCommandsProvider.getFrecencyApplication());
            List<com.discord.api.commands.Application> b2 = guildApplicationCommands.b();
            ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(b2, 10));
            for (com.discord.api.commands.Application application : b2) {
                arrayList2.add(StoreApplicationCommandsKt.toDomainApplication(application));
            }
            handleGuildApplicationsUpdate(u.plus((Collection<? extends Application>) u.plus((Collection) listOf, (Iterable) u.sortedWith(arrayList2, new Comparator() { // from class: com.discord.stores.StoreApplicationCommands$handleApplicationCommandsUpdate$$inlined$sortedBy$1
                @Override // java.util.Comparator
                public final int compare(T t, T t2) {
                    return d0.u.a.compareValues(((Application) t).getName(), ((Application) t2).getName());
                }
            })), this.builtInCommandsProvider.getBuiltInApplication()));
        } else if (m.areEqual(c2, this.queryNonce)) {
            List<com.discord.api.commands.ApplicationCommand> a2 = guildApplicationCommands.a();
            ArrayList arrayList3 = new ArrayList(o.collectionSizeOrDefault(a2, 10));
            for (com.discord.api.commands.ApplicationCommand applicationCommand2 : a2) {
                arrayList3.add(StoreApplicationCommandsKt.toSlashCommand(applicationCommand2));
            }
            handleQueryCommandsUpdate(arrayList3);
        } else if (m.areEqual(c2, this.discoverCommandsNonce)) {
            List<com.discord.api.commands.ApplicationCommand> a3 = guildApplicationCommands.a();
            ArrayList arrayList4 = new ArrayList(o.collectionSizeOrDefault(a3, 10));
            for (com.discord.api.commands.ApplicationCommand applicationCommand3 : a3) {
                arrayList4.add(StoreApplicationCommandsKt.toSlashCommand(applicationCommand3));
            }
            handleDiscoverCommandsUpdate(arrayList4);
        }
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        this.sessionId = modelPayload.getSessionId();
    }

    public final void handleConnectionReady(boolean z2) {
        this.connectionReady = z2;
        if (z2) {
            Long l = this.pendingGatewayGuildId;
            requestFrecencyCommands(l != null ? l.longValue() : 0L);
            Long l2 = this.pendingGatewayGuildId;
            if (l2 != null) {
                requestApplications(l2);
                this.pendingGatewayGuildId = null;
            }
            User user = this.pendingBotUser;
            if (user != null) {
                handleDmUserApplication(user);
                this.pendingBotUser = null;
            }
        }
    }

    public final void handleDmUserApplication(User user) {
        m.checkNotNullParameter(user, "botUser");
        if (user.isBot()) {
            if (!this.connectionReady) {
                this.pendingBotUser = user;
            } else {
                this.dispatcher.schedule(new StoreApplicationCommands$handleDmUserApplication$1(this, user));
            }
        }
    }

    public final boolean hasFetchedApplicationCommands(long j) {
        Integer num = this.applicationCommandIndexes.get(Long.valueOf(j));
        if (num == null) {
            return false;
        }
        int intValue = num.intValue();
        return this.currentStartOffset <= intValue && this.currentEndOffset >= intValue;
    }

    public final Observable<Map<String, Map<String, CommandAutocompleteState>>> observeAutocompleteResults() {
        Observable<Map<String, Map<String, CommandAutocompleteState>>> T = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{AutocompleteResultsUpdate}, false, null, null, new StoreApplicationCommands$observeAutocompleteResults$1(this), 14, null).T(h0.emptyMap());
        m.checkNotNullExpressionValue(T, "observationDeck.connectR…  }.startWith(emptyMap())");
        return T;
    }

    public final Observable<DiscoverCommands> observeDiscoverCommands(long j) {
        Observable<DiscoverCommands> j2 = Observable.j(ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{DiscoverCommandsUpdate}, false, null, null, new StoreApplicationCommands$observeDiscoverCommands$1(this), 14, null).q(), this.storePermissions.observePermissionsForChannel(j), new Func2<DiscoverCommands, Long, DiscoverCommands>() { // from class: com.discord.stores.StoreApplicationCommands$observeDiscoverCommands$2
            public final DiscoverCommands call(DiscoverCommands discoverCommands, Long l) {
                boolean shouldReturnApplicationCommands;
                BuiltInCommandsProvider builtInCommandsProvider;
                shouldReturnApplicationCommands = StoreApplicationCommands.this.shouldReturnApplicationCommands(l);
                if (shouldReturnApplicationCommands) {
                    return discoverCommands;
                }
                builtInCommandsProvider = StoreApplicationCommands.this.builtInCommandsProvider;
                List<ApplicationCommand> builtInCommands = builtInCommandsProvider.getBuiltInCommands();
                return new DiscoverCommands(builtInCommands, 0, builtInCommands.size(), false, false, 0, null, null, 128, null);
            }
        });
        m.checkNotNullExpressionValue(j2, "Observable\n        .comb…  )\n          }\n        }");
        return j2;
    }

    public final Observable<List<ApplicationCommand>> observeFrecencyCommands(long j) {
        Observable<List<ApplicationCommand>> T = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{FrecencyCommandsUpdate, this.storeExperiments}, false, null, null, new StoreApplicationCommands$observeFrecencyCommands$1(this, j), 14, null).T(n.emptyList());
        m.checkNotNullExpressionValue(T, "observationDeck.connectR…st<ApplicationCommand>())");
        return T;
    }

    public final Observable<List<Application>> observeGuildApplications(long j) {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{GuildApplicationsUpdate, this.storePermissions}, false, null, null, new StoreApplicationCommands$observeGuildApplications$1(this, j), 14, null);
    }

    public final Observable<List<ApplicationCommand>> observeQueryCommands(long j) {
        Observable<List<ApplicationCommand>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{QueryCommandsUpdate, this.storePermissions}, false, null, null, new StoreApplicationCommands$observeQueryCommands$1(this, j), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    public final void requestApplicationCommandAutocompleteData(Long l, long j, ApplicationCommandData applicationCommandData) {
        m.checkNotNullParameter(applicationCommandData, "data");
        this.dispatcher.schedule(new StoreApplicationCommands$requestApplicationCommandAutocompleteData$1(this, applicationCommandData, j, l));
    }

    public final void requestApplicationCommandsQuery(Long l, String str) {
        m.checkNotNullParameter(str, "query");
        this.dispatcher.schedule(new StoreApplicationCommands$requestApplicationCommandsQuery$1(this, l, str));
    }

    public final void requestApplications(Long l) {
        if (!this.connectionReady) {
            this.pendingGatewayGuildId = l;
        } else {
            this.dispatcher.schedule(new StoreApplicationCommands$requestApplications$1(this, l));
        }
    }

    public final void requestDiscoverCommands(long j) {
        Long l = this.discoverGuildId;
        if (l != null) {
            l.longValue();
            requestInitialApplicationCommands$default(this, l, Long.valueOf(j), false, 4, null);
        }
    }

    public final void requestFrecencyCommands(long j) {
        boolean requestApplicationCommands;
        List<String> topCommandIds = this.storeApplicationCommandFrecency.getTopCommandIds(Long.valueOf(j));
        String generateNonce = generateNonce();
        FrecencyRequest frecencyRequest = new FrecencyRequest(Long.valueOf(j), topCommandIds);
        if (!this.frecencyRequests.containsValue(frecencyRequest)) {
            this.frecencyRequests.put(generateNonce, frecencyRequest);
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(topCommandIds, 10));
            for (String str : topCommandIds) {
                if (w.contains$default((CharSequence) str, (CharSequence) " ", false, 2, (Object) null)) {
                    str = (String) w.split$default((CharSequence) str, new String[]{" "}, false, 0, 6, (Object) null).get(0);
                }
                arrayList.add(str);
            }
            ArrayList arrayList2 = new ArrayList();
            Iterator it = arrayList.iterator();
            while (true) {
                boolean z2 = true;
                if (!it.hasNext()) {
                    break;
                }
                Object next = it.next();
                Long longOrNull = d0.g0.s.toLongOrNull((String) next);
                if ((longOrNull != null ? longOrNull.longValue() : 0L) <= 0) {
                    z2 = false;
                }
                if (z2) {
                    arrayList2.add(next);
                }
            }
            if (!arrayList2.isEmpty()) {
                requestApplicationCommands = this.storeGatewayConnection.requestApplicationCommands(j, generateNonce, false, (r20 & 8) != 0 ? null : null, (r20 & 16) != 0 ? null : null, 20, (r20 & 64) != 0 ? null : u.distinct(arrayList2));
                if (!requestApplicationCommands) {
                    this.dispatcher.schedule(new StoreApplicationCommands$requestFrecencyCommands$1(this, generateNonce));
                    return;
                }
                return;
            }
            this.dispatcher.schedule(new StoreApplicationCommands$requestFrecencyCommands$2(this, generateNonce));
        }
    }

    public final void requestInitialApplicationCommands(Long l, Long l2, boolean z2) {
        DiscoverCommands copy;
        Ref$IntRef ref$IntRef = new Ref$IntRef();
        int i = 0;
        ref$IntRef.element = 0;
        this.loadDirectionUp = false;
        this.loadDirectionDown = false;
        if (!z2) {
            this.jumpedApplicationId = l2;
        }
        if (this.jumpedApplicationId != null) {
            this.jumpedSequenceId++;
        }
        if (l2 != null) {
            long longValue = l2.longValue();
            copy = r8.copy((r18 & 1) != 0 ? r8.commands : n.emptyList(), (r18 & 2) != 0 ? r8.currentStartOffset : 0, (r18 & 4) != 0 ? r8.currentEndOffset : 0, (r18 & 8) != 0 ? r8.hasMoreBefore : false, (r18 & 16) != 0 ? r8.hasMoreAfter : false, (r18 & 32) != 0 ? r8.jumpedSequenceId : 0, (r18 & 64) != 0 ? r8.jumpedApplicationId : null, (r18 & 128) != 0 ? this.discoverCommands.loadState : null);
            this.discoverCommands = copy;
            if (this.applicationCommandIndexes.containsKey(Long.valueOf(longValue))) {
                Integer num = this.applicationCommandIndexes.get(Long.valueOf(longValue));
                if (num != null) {
                    i = num.intValue();
                }
                ref$IntRef.element = i;
                this.currentStartOffset = i;
                this.currentEndOffset = i;
            }
            if (longValue == this.builtInCommandsProvider.getBuiltInApplication().getId()) {
                this.dispatcher.schedule(new StoreApplicationCommands$requestInitialApplicationCommands$$inlined$let$lambda$1(this, ref$IntRef));
                return;
            }
        }
        requestApplicationCommands$default(this, l, ref$IntRef.element, l2, 0, 8, null);
    }

    public final void requestLoadMoreDown() {
        DiscoverCommands copy;
        Long l = this.discoverGuildId;
        if (l != null) {
            requestApplicationCommands$default(this, Long.valueOf(l.longValue()), this.currentEndOffset + 1, null, 20, 4, null);
            this.loadDirectionUp = false;
            this.loadDirectionDown = true;
            this.jumpedApplicationId = null;
            copy = r1.copy((r18 & 1) != 0 ? r1.commands : null, (r18 & 2) != 0 ? r1.currentStartOffset : 0, (r18 & 4) != 0 ? r1.currentEndOffset : 0, (r18 & 8) != 0 ? r1.hasMoreBefore : false, (r18 & 16) != 0 ? r1.hasMoreAfter : false, (r18 & 32) != 0 ? r1.jumpedSequenceId : 0, (r18 & 64) != 0 ? r1.jumpedApplicationId : null, (r18 & 128) != 0 ? this.discoverCommands.loadState : LoadState.LoadingDown.INSTANCE);
            this.discoverCommands = copy;
        }
    }

    public final void requestLoadMoreUp() {
        int i;
        int i2;
        DiscoverCommands copy;
        Long l = this.discoverGuildId;
        if (l != null) {
            long longValue = l.longValue();
            int i3 = this.currentStartOffset;
            int i4 = i3 - 20;
            if (i4 < 0) {
                i = i3;
                i2 = 0;
            } else {
                i2 = i4;
                i = 20;
            }
            requestApplicationCommands$default(this, Long.valueOf(longValue), i2, null, i, 4, null);
            this.loadDirectionUp = true;
            this.loadDirectionDown = false;
            this.jumpedApplicationId = null;
            copy = r1.copy((r18 & 1) != 0 ? r1.commands : null, (r18 & 2) != 0 ? r1.currentStartOffset : 0, (r18 & 4) != 0 ? r1.currentEndOffset : 0, (r18 & 8) != 0 ? r1.hasMoreBefore : false, (r18 & 16) != 0 ? r1.hasMoreAfter : false, (r18 & 32) != 0 ? r1.jumpedSequenceId : 0, (r18 & 64) != 0 ? r1.jumpedApplicationId : null, (r18 & 128) != 0 ? this.discoverCommands.loadState : LoadState.LoadingUp.INSTANCE);
            this.discoverCommands = copy;
        }
    }

    @StoreThread
    public final void setAutocompleteFailed(String str, String str2) {
        m.checkNotNullParameter(str, "commandOptionName");
        m.checkNotNullParameter(str2, "queryString");
        setAutocompleteState(str, str2, CommandAutocompleteState.Failed.INSTANCE);
    }

    @StoreThread
    public final void setAutocompleteLoading(String str, String str2) {
        m.checkNotNullParameter(str, "commandOptionName");
        m.checkNotNullParameter(str2, "queryString");
        setAutocompleteState(str, str2, CommandAutocompleteState.Loading.INSTANCE);
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        DiscoverCommands copy;
        if (getUpdateSources().contains(DiscoverCommandsUpdate)) {
            DiscoverCommands discoverCommands = this.discoverCommands;
            copy = discoverCommands.copy((r18 & 1) != 0 ? discoverCommands.commands : StoreApplicationCommandsKt.flattenSubCommands(discoverCommands.getCommands()), (r18 & 2) != 0 ? discoverCommands.currentStartOffset : 0, (r18 & 4) != 0 ? discoverCommands.currentEndOffset : 0, (r18 & 8) != 0 ? discoverCommands.hasMoreBefore : false, (r18 & 16) != 0 ? discoverCommands.hasMoreAfter : false, (r18 & 32) != 0 ? discoverCommands.jumpedSequenceId : 0, (r18 & 64) != 0 ? discoverCommands.jumpedApplicationId : null, (r18 & 128) != 0 ? discoverCommands.loadState : null);
            this.discoverCommandsSnapshot = copy;
        }
        if (getUpdateSources().contains(QueryCommandsUpdate)) {
            this.queryCommandsSnapshot = new ArrayList(StoreApplicationCommandsKt.flattenSubCommands(this.queryCommands));
        }
        if (getUpdateSources().contains(GuildApplicationsUpdate)) {
            ArrayList arrayList = new ArrayList(this.applications);
            this.applicationsSnapshot = arrayList;
            LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(o.collectionSizeOrDefault(arrayList, 10)), 16));
            for (Object obj : arrayList) {
                linkedHashMap.put(Long.valueOf(((Application) obj).getId()), obj);
            }
            this.applicationsMapSnapshot = linkedHashMap;
        }
        if (getUpdateSources().contains(AutocompleteResultsUpdate)) {
            this.autocompleteOptionResultsSnapshot = new HashMap(this.autocompleteOptionResults);
        }
        if (getUpdateSources().contains(FrecencyCommandsUpdate)) {
            LinkedHashMap linkedHashMap2 = new LinkedHashMap();
            linkedHashMap2.putAll(this.frecencyCommands);
            this.frecencyCommandsSnapshot = linkedHashMap2;
        }
    }

    public StoreApplicationCommands(StoreGatewayConnection storeGatewayConnection, StorePermissions storePermissions, StoreApplicationCommandFrecency storeApplicationCommandFrecency, StoreGuilds storeGuilds, StoreUser storeUser, StoreExperiments storeExperiments, Dispatcher dispatcher, RestAPI restAPI, ObservationDeck observationDeck, BuiltInCommandsProvider builtInCommandsProvider, NonceGenerator nonceGenerator) {
        m.checkNotNullParameter(storeGatewayConnection, "storeGatewayConnection");
        m.checkNotNullParameter(storePermissions, "storePermissions");
        m.checkNotNullParameter(storeApplicationCommandFrecency, "storeApplicationCommandFrecency");
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        m.checkNotNullParameter(storeUser, "storeUsers");
        m.checkNotNullParameter(storeExperiments, "storeExperiments");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(restAPI, "restApi");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(builtInCommandsProvider, "builtInCommandsProvider");
        m.checkNotNullParameter(nonceGenerator, "nonceGenerator");
        this.storeGatewayConnection = storeGatewayConnection;
        this.storePermissions = storePermissions;
        this.storeApplicationCommandFrecency = storeApplicationCommandFrecency;
        this.storeGuilds = storeGuilds;
        this.storeUsers = storeUser;
        this.storeExperiments = storeExperiments;
        this.dispatcher = dispatcher;
        this.restApi = restAPI;
        this.observationDeck = observationDeck;
        this.builtInCommandsProvider = builtInCommandsProvider;
        this.nonceGenerator = nonceGenerator;
        DiscoverCommands.Companion companion = DiscoverCommands.Companion;
        this.discoverCommands = companion.getDefaultModelDiscoveryCommands();
        this.discoverCommandsSnapshot = companion.getDefaultModelDiscoveryCommands();
        this.applicationCommandIndexes = new LinkedHashMap();
        this.applications = n.emptyList();
        this.applicationsSnapshot = n.emptyList();
        this.applicationsMapSnapshot = h0.emptyMap();
        this.queryCommands = new ArrayList();
        this.queryCommandsSnapshot = n.emptyList();
        this.autocompleteNonceData = new LinkedHashMap();
        this.autocompleteOptionResults = new LinkedHashMap();
        this.autocompleteOptionResultsSnapshot = h0.emptyMap();
        this.frecencyRequests = new LinkedHashMap();
        this.frecencyCommands = new LinkedHashMap();
        this.frecencyCommandsSnapshot = h0.emptyMap();
    }
}
