package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.stores.StoreThreadsActiveJoined;
import j0.k.b;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
/* compiled from: StoreMessageAck.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0003\u0010\t\u001a\u001e\u0012\b\u0012\u00060\u0001j\u0002`\u0002 \u0004*\u000e\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0018\u00010\u00060\u00062.\u0010\u0005\u001a*\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003 \u0004*\u0014\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/stores/StoreThreadsActiveJoined$ActiveJoinedThread;", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Map;)Ljava/util/Set;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessageAck$observeActiveJoinedThreadIdsWithPrevious$1<T, R> implements b<Map<Long, ? extends StoreThreadsActiveJoined.ActiveJoinedThread>, Set<? extends Long>> {
    public static final StoreMessageAck$observeActiveJoinedThreadIdsWithPrevious$1 INSTANCE = new StoreMessageAck$observeActiveJoinedThreadIdsWithPrevious$1();

    @Override // j0.k.b
    public /* bridge */ /* synthetic */ Set<? extends Long> call(Map<Long, ? extends StoreThreadsActiveJoined.ActiveJoinedThread> map) {
        return call2((Map<Long, StoreThreadsActiveJoined.ActiveJoinedThread>) map);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final Set<Long> call2(Map<Long, StoreThreadsActiveJoined.ActiveJoinedThread> map) {
        return map.keySet();
    }
}
