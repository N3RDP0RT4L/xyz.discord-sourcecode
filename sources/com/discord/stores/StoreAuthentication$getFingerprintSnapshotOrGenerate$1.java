package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.api.fingerprint.FingerprintResponse;
import com.discord.restapi.RestAPIParams;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import j0.k.b;
import j0.l.e.k;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: StoreAuthentication.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a>\u0012\u0018\b\u0001\u0012\u0014 \u0002*\n\u0018\u00010\u0000j\u0004\u0018\u0001`\u00010\u0000j\u0002`\u0001 \u0002*\u001e\u0012\u0018\b\u0001\u0012\u0014 \u0002*\n\u0018\u00010\u0000j\u0004\u0018\u0001`\u00010\u0000j\u0002`\u0001\u0018\u00010\u00040\u00042\u0018\u0010\u0003\u001a\u0014 \u0002*\n\u0018\u00010\u0000j\u0004\u0018\u0001`\u00010\u0000j\u0002`\u0001H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "Lcom/discord/primitives/FingerPrint;", "kotlin.jvm.PlatformType", "fingerprint", "Lrx/Observable;", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/String;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAuthentication$getFingerprintSnapshotOrGenerate$1<T, R> implements b<String, Observable<? extends String>> {
    public static final StoreAuthentication$getFingerprintSnapshotOrGenerate$1 INSTANCE = new StoreAuthentication$getFingerprintSnapshotOrGenerate$1();

    /* compiled from: StoreAuthentication.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u0014 \u0001*\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00040\u0003j\u0002`\u00042\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/api/fingerprint/FingerprintResponse;", "kotlin.jvm.PlatformType", "it", "", "Lcom/discord/primitives/FingerPrint;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/api/fingerprint/FingerprintResponse;)Ljava/lang/String;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreAuthentication$getFingerprintSnapshotOrGenerate$1$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2<T, R> implements b<FingerprintResponse, String> {
        public static final AnonymousClass2 INSTANCE = new AnonymousClass2();

        public final String call(FingerprintResponse fingerprintResponse) {
            return fingerprintResponse.a();
        }
    }

    public final Observable<? extends String> call(String str) {
        if (str != null) {
            return new k(str);
        }
        return ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().postAuthFingerprint(new RestAPIParams.EmptyBody()), false, 1, null).F(AnonymousClass2.INSTANCE);
    }
}
