package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.models.domain.ModelInvite;
import com.discord.stores.StoreInstantInvites;
import com.discord.stores.StoreInviteSettings;
import j0.k.b;
import j0.l.e.k;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: StoreInviteSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0007\u001a\u001e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0003 \u0004*\u000e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0003\u0018\u00010\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/stores/StoreInviteSettings$InviteCode;", "inviteCode", "Lrx/Observable;", "Lcom/discord/stores/StoreInstantInvites$InviteState;", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/stores/StoreInviteSettings$InviteCode;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreInviteSettings$getInvite$1<T, R> implements b<StoreInviteSettings.InviteCode, Observable<? extends StoreInstantInvites.InviteState>> {
    public static final StoreInviteSettings$getInvite$1 INSTANCE = new StoreInviteSettings$getInvite$1();

    public final Observable<? extends StoreInstantInvites.InviteState> call(StoreInviteSettings.InviteCode inviteCode) {
        if (inviteCode == null) {
            return new k(null);
        }
        return StoreStream.Companion.getInstantInvites().observeInvite(ModelInvite.getInviteStoreKey(inviteCode.getInviteCode(), inviteCode.getEventId()));
    }
}
