package com.discord.stores;

import b.d.b.a.a;
import com.discord.models.message.Message;
import com.discord.stores.StoreMessageAck;
import com.discord.utilities.SnowflakeUtils;
import d0.z.d.m;
import d0.z.d.o;
import j0.l.e.k;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreMessageAck.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\n\u001a\u00020\u00072n\u0010\u0006\u001aj\u0012 \u0012\u001e\u0012\b\u0012\u00060\u0002j\u0002`\u0003 \u0004*\u000e\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0018\u00010\u00010\u0001\u0012\f\u0012\n \u0004*\u0004\u0018\u00010\u00050\u0005 \u0004*4\u0012 \u0012\u001e\u0012\b\u0012\u00060\u0002j\u0002`\u0003 \u0004*\u000e\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0018\u00010\u00010\u0001\u0012\f\u0012\n \u0004*\u0004\u0018\u00010\u00050\u0005\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\b\u0010\t"}, d2 = {"Lkotlin/Pair;", "", "Lcom/discord/models/message/Message;", "Lcom/discord/stores/ClientMessage;", "kotlin.jvm.PlatformType", "Lcom/discord/stores/StoreMessageAck$ThreadState;", "<name for destructuring parameter 0>", "", "invoke", "(Lkotlin/Pair;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessageAck$markUnread$3 extends o implements Function1<Pair<? extends List<? extends Message>, ? extends StoreMessageAck.ThreadState>, Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ long $messageId;
    public final /* synthetic */ StoreMessageAck this$0;

    /* compiled from: StoreMessageAck.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreMessageAck$markUnread$3$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ List $channelMessages;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(List list) {
            super(0);
            this.$channelMessages = list;
        }

        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r2v20 */
        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            Message message;
            List list = this.$channelMessages;
            ArrayList Y = a.Y(list, "channelMessages");
            Iterator it = list.iterator();
            while (true) {
                boolean z2 = true;
                if (!it.hasNext()) {
                    break;
                }
                Object next = it.next();
                if (((Message) next).getId() >= StoreMessageAck$markUnread$3.this.$messageId) {
                    z2 = false;
                }
                if (z2) {
                    Y.add(next);
                }
            }
            Iterator it2 = Y.iterator();
            if (!it2.hasNext()) {
                message = null;
            } else {
                Object next2 = it2.next();
                if (!it2.hasNext()) {
                    message = next2;
                } else {
                    long id2 = ((Message) next2).getId();
                    do {
                        Object next3 = it2.next();
                        long id3 = ((Message) next3).getId();
                        if (id2 < id3) {
                            next2 = next3;
                            id2 = id3;
                        }
                    } while (it2.hasNext());
                    message = next2;
                }
            }
            Message message2 = message;
            List list2 = this.$channelMessages;
            ArrayList Y2 = a.Y(list2, "channelMessages");
            for (Object obj : list2) {
                if (((Message) obj).getId() >= StoreMessageAck$markUnread$3.this.$messageId) {
                    Y2.add(obj);
                }
            }
            StoreMessageAck.Ack ack = new StoreMessageAck.Ack(message2 != null ? message2.getId() : (((StoreMessageAck$markUnread$3.this.$messageId >>> 22) - 1) - SnowflakeUtils.DISCORD_EPOCH) << 22, false, true);
            StoreMessageAck$markUnread$3 storeMessageAck$markUnread$3 = StoreMessageAck$markUnread$3.this;
            storeMessageAck$markUnread$3.this$0.updateAcks(storeMessageAck$markUnread$3.$channelId, ack);
            int processMarkUnread$app_productionGoogleRelease = StoreStream.Companion.getMentions().processMarkUnread$app_productionGoogleRelease(StoreMessageAck$markUnread$3.this.$channelId, Y2);
            StoreMessageAck storeMessageAck = StoreMessageAck$markUnread$3.this.this$0;
            k kVar = new k(new StoreMessageAck.PendingAck(StoreMessageAck$markUnread$3.this.$channelId, ack));
            m.checkNotNullExpressionValue(kVar, "Observable.just(PendingAck(channelId, ack))");
            storeMessageAck.postPendingAck(kVar, processMarkUnread$app_productionGoogleRelease);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreMessageAck$markUnread$3(StoreMessageAck storeMessageAck, long j, long j2) {
        super(1);
        this.this$0 = storeMessageAck;
        this.$messageId = j;
        this.$channelId = j2;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Pair<? extends List<? extends Message>, ? extends StoreMessageAck.ThreadState> pair) {
        invoke2((Pair<? extends List<Message>, ? extends StoreMessageAck.ThreadState>) pair);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Pair<? extends List<Message>, ? extends StoreMessageAck.ThreadState> pair) {
        boolean shouldAck;
        Dispatcher dispatcher;
        List<Message> component1 = pair.component1();
        StoreMessageAck.ThreadState component2 = pair.component2();
        StoreMessageAck storeMessageAck = this.this$0;
        m.checkNotNullExpressionValue(component2, "threadState");
        shouldAck = storeMessageAck.shouldAck(component2);
        if (shouldAck) {
            dispatcher = this.this$0.dispatcher;
            dispatcher.schedule(new AnonymousClass1(component1));
        }
    }
}
