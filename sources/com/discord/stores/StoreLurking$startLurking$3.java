package com.discord.stores;

import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function3;
/* compiled from: StoreLurking.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreLurking$startLurking$3 extends o implements Function0<Unit> {
    public final /* synthetic */ Long $channelId;
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ boolean $navigateToGuild;
    public final /* synthetic */ Function0 $onErrorLurking;
    public final /* synthetic */ Function3 $onGuildLurked;
    public final /* synthetic */ StoreLurking this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreLurking$startLurking$3(StoreLurking storeLurking, long j, Long l, boolean z2, Function3 function3, Function0 function0) {
        super(0);
        this.this$0 = storeLurking;
        this.$guildId = j;
        this.$channelId = l;
        this.$navigateToGuild = z2;
        this.$onGuildLurked = function3;
        this.$onErrorLurking = function0;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        this.this$0.startLurkingInternal(this.$guildId, this.$channelId, (r17 & 4) != 0 ? false : this.$navigateToGuild, (r17 & 8) != 0 ? StoreLurking$startLurkingInternal$1.INSTANCE : this.$onGuildLurked, (r17 & 16) != 0 ? StoreLurking$startLurkingInternal$2.INSTANCE : this.$onErrorLurking, (r17 & 32) != 0 ? null : null);
    }
}
