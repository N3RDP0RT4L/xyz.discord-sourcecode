package com.discord.stores;

import com.discord.stores.StoreGooglePlayPurchases;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreGooglePlayPurchases.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGooglePlayPurchases$downgradePurchase$1 extends o implements Function0<Unit> {
    public final /* synthetic */ StoreGooglePlayPurchases this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGooglePlayPurchases$downgradePurchase$1(StoreGooglePlayPurchases storeGooglePlayPurchases) {
        super(0);
        this.this$0 = storeGooglePlayPurchases;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreGooglePlayPurchases.State state;
        state = this.this$0.storeState;
        if (state instanceof StoreGooglePlayPurchases.State.Loaded) {
            StoreGooglePlayPurchases.State.Loaded loaded = (StoreGooglePlayPurchases.State.Loaded) state;
            if (loaded.getPendingDowngrade() != null) {
                this.this$0.queryState = StoreGooglePlayPurchases.QueryState.InProgress.INSTANCE;
                this.this$0.markChanged();
                this.this$0.doDowngrade(loaded.getPendingDowngrade());
            }
        }
    }
}
