package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelPayload;
import com.discord.models.domain.ModelUserRelationship;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.persister.Persister;
import d0.d0.f;
import d0.t.g0;
import d0.t.h0;
import d0.t.o;
import d0.z.d.m;
import j0.k.b;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreUserRelationships.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u001e\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u00014B\u0011\u0012\b\b\u0002\u00100\u001a\u00020/¢\u0006\u0004\b2\u00103J\u000f\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\r\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\u0006\u0010\u0007J\u000f\u0010\b\u001a\u00020\u0005H\u0007¢\u0006\u0004\b\b\u0010\u0007J!\u0010\u000e\u001a\u0016\u0012\b\u0012\u00060\nj\u0002`\u000b\u0012\b\u0012\u00060\fj\u0002`\r0\t¢\u0006\u0004\b\u000e\u0010\u000fJ\r\u0010\u0011\u001a\u00020\u0010¢\u0006\u0004\b\u0011\u0010\u0012J\u0013\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00100\u0013¢\u0006\u0004\b\u0014\u0010\u0015J'\u0010\u0016\u001a\u001c\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\nj\u0002`\u000b\u0012\b\u0012\u00060\fj\u0002`\r0\t0\u0013¢\u0006\u0004\b\u0016\u0010\u0015J9\u0010\u0016\u001a\u001c\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\nj\u0002`\u000b\u0012\b\u0012\u00060\fj\u0002`\r0\t0\u00132\u0010\u0010\u0018\u001a\f\u0012\b\u0012\u00060\nj\u0002`\u000b0\u0017¢\u0006\u0004\b\u0016\u0010\u0019J'\u0010\u0016\u001a\u0010\u0012\f\u0012\n\u0018\u00010\fj\u0004\u0018\u0001`\r0\u00132\n\u0010\u001a\u001a\u00060\nj\u0002`\u000b¢\u0006\u0004\b\u0016\u0010\u001bJ/\u0010\u001d\u001a\u001c\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\nj\u0002`\u000b\u0012\b\u0012\u00060\fj\u0002`\r0\t0\u00132\u0006\u0010\u001c\u001a\u00020\f¢\u0006\u0004\b\u001d\u0010\u001eJ\u0017\u0010!\u001a\u00020\u00052\u0006\u0010 \u001a\u00020\u001fH\u0007¢\u0006\u0004\b!\u0010\"J\u0017\u0010%\u001a\u00020\u00052\u0006\u0010$\u001a\u00020#H\u0007¢\u0006\u0004\b%\u0010&J\u0017\u0010'\u001a\u00020\u00052\u0006\u0010$\u001a\u00020#H\u0007¢\u0006\u0004\b'\u0010&J\u000f\u0010(\u001a\u00020\u0005H\u0017¢\u0006\u0004\b(\u0010\u0007R0\u0010*\u001a\u001c\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\nj\u0002`\u000b\u0012\b\u0012\u00060\fj\u0002`\r0\t0)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010+R\u0016\u0010,\u001a\u00020\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b,\u0010-R\u0016\u0010.\u001a\u00020\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b.\u0010-R\u0016\u00100\u001a\u00020/8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b0\u00101¨\u00065"}, d2 = {"Lcom/discord/stores/StoreUserRelationships;", "Lcom/discord/stores/StoreV2;", "Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;", "ensureRelationshipLoaded", "()Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;", "", "init", "()V", "handlePreLogout", "", "", "Lcom/discord/primitives/UserId;", "", "Lcom/discord/primitives/RelationshipType;", "getRelationships", "()Ljava/util/Map;", "Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState;", "getRelationshipsState", "()Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState;", "Lrx/Observable;", "observeUserRelationshipsState", "()Lrx/Observable;", "observe", "", "userIds", "(Ljava/util/Collection;)Lrx/Observable;", "userId", "(J)Lrx/Observable;", "relationshipType", "observeForType", "(I)Lrx/Observable;", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "Lcom/discord/models/domain/ModelUserRelationship;", "relationship", "handleRelationshipAdd", "(Lcom/discord/models/domain/ModelUserRelationship;)V", "handleRelationshipRemove", "snapshotData", "Lcom/discord/utilities/persister/Persister;", "relationshipsCache", "Lcom/discord/utilities/persister/Persister;", "relationshipsState", "Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState;", "relationshipsStateSnapshot", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", HookHelper.constructorName, "(Lcom/discord/stores/updates/ObservationDeck;)V", "UserRelationshipsState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserRelationships extends StoreV2 {
    private final ObservationDeck observationDeck;
    private final Persister<Map<Long, Integer>> relationshipsCache;
    private UserRelationshipsState relationshipsState;
    private UserRelationshipsState relationshipsStateSnapshot;

    /* compiled from: StoreUserRelationships.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState;", "", HookHelper.constructorName, "()V", "Loaded", "Unloaded", "Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Unloaded;", "Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class UserRelationshipsState {

        /* compiled from: StoreUserRelationships.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B#\u0012\u001a\u0010\t\u001a\u0016\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\b\u0012\u00060\u0005j\u0002`\u00060\u0002¢\u0006\u0004\b\u0018\u0010\u0019J$\u0010\u0007\u001a\u0016\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\b\u0012\u00060\u0005j\u0002`\u00060\u0002HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ.\u0010\n\u001a\u00020\u00002\u001c\b\u0002\u0010\t\u001a\u0016\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\b\u0012\u00060\u0005j\u0002`\u00060\u0002HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0011HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R-\u0010\t\u001a\u0016\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\b\u0012\u00060\u0005j\u0002`\u00060\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0016\u001a\u0004\b\u0017\u0010\b¨\u0006\u001a"}, d2 = {"Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;", "Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState;", "", "", "Lcom/discord/primitives/UserId;", "", "Lcom/discord/primitives/RelationshipType;", "component1", "()Ljava/util/Map;", "relationships", "copy", "(Ljava/util/Map;)Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Loaded;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getRelationships", HookHelper.constructorName, "(Ljava/util/Map;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Loaded extends UserRelationshipsState {
            private final Map<Long, Integer> relationships;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loaded(Map<Long, Integer> map) {
                super(null);
                m.checkNotNullParameter(map, "relationships");
                this.relationships = map;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Loaded copy$default(Loaded loaded, Map map, int i, Object obj) {
                if ((i & 1) != 0) {
                    map = loaded.relationships;
                }
                return loaded.copy(map);
            }

            public final Map<Long, Integer> component1() {
                return this.relationships;
            }

            public final Loaded copy(Map<Long, Integer> map) {
                m.checkNotNullParameter(map, "relationships");
                return new Loaded(map);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Loaded) && m.areEqual(this.relationships, ((Loaded) obj).relationships);
                }
                return true;
            }

            public final Map<Long, Integer> getRelationships() {
                return this.relationships;
            }

            public int hashCode() {
                Map<Long, Integer> map = this.relationships;
                if (map != null) {
                    return map.hashCode();
                }
                return 0;
            }

            public String toString() {
                return a.L(a.R("Loaded(relationships="), this.relationships, ")");
            }
        }

        /* compiled from: StoreUserRelationships.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState$Unloaded;", "Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Unloaded extends UserRelationshipsState {
            public static final Unloaded INSTANCE = new Unloaded();

            private Unloaded() {
                super(null);
            }
        }

        private UserRelationshipsState() {
        }

        public /* synthetic */ UserRelationshipsState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public StoreUserRelationships() {
        this(null, 1, null);
    }

    public /* synthetic */ StoreUserRelationships(ObservationDeck observationDeck, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? ObservationDeckProvider.get() : observationDeck);
    }

    @StoreThread
    private final UserRelationshipsState.Loaded ensureRelationshipLoaded() {
        UserRelationshipsState userRelationshipsState = this.relationshipsState;
        if (!(userRelationshipsState instanceof UserRelationshipsState.Loaded)) {
            userRelationshipsState = null;
        }
        UserRelationshipsState.Loaded loaded = (UserRelationshipsState.Loaded) userRelationshipsState;
        if (loaded == null) {
            loaded = new UserRelationshipsState.Loaded(h0.emptyMap());
        }
        this.relationshipsState = loaded;
        return loaded;
    }

    public final Map<Long, Integer> getRelationships() {
        Map<Long, Integer> relationships;
        UserRelationshipsState userRelationshipsState = this.relationshipsStateSnapshot;
        if (!(userRelationshipsState instanceof UserRelationshipsState.Loaded)) {
            userRelationshipsState = null;
        }
        UserRelationshipsState.Loaded loaded = (UserRelationshipsState.Loaded) userRelationshipsState;
        return (loaded == null || (relationships = loaded.getRelationships()) == null) ? h0.emptyMap() : relationships;
    }

    public final UserRelationshipsState getRelationshipsState() {
        return this.relationshipsStateSnapshot;
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        List<ModelUserRelationship> relationships = modelPayload.getRelationships();
        m.checkNotNullExpressionValue(relationships, "payload\n        .relationships");
        LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(o.collectionSizeOrDefault(relationships, 10)), 16));
        for (ModelUserRelationship modelUserRelationship : relationships) {
            m.checkNotNullExpressionValue(modelUserRelationship, "it");
            Long valueOf = Long.valueOf(modelUserRelationship.getId());
            m.checkNotNullExpressionValue(modelUserRelationship, "it");
            linkedHashMap.put(valueOf, Integer.valueOf(modelUserRelationship.getType()));
        }
        this.relationshipsState = new UserRelationshipsState.Loaded(linkedHashMap);
        markChanged();
    }

    @StoreThread
    public final void handlePreLogout() {
        this.relationshipsState = UserRelationshipsState.Unloaded.INSTANCE;
        markChanged();
    }

    @StoreThread
    public final void handleRelationshipAdd(ModelUserRelationship modelUserRelationship) {
        m.checkNotNullParameter(modelUserRelationship, "relationship");
        Map mutableMap = h0.toMutableMap(ensureRelationshipLoaded().getRelationships());
        Integer num = (Integer) mutableMap.get(Long.valueOf(modelUserRelationship.getId()));
        int type = modelUserRelationship.getType();
        if (num == null || num.intValue() != type) {
            mutableMap.put(Long.valueOf(modelUserRelationship.getId()), Integer.valueOf(modelUserRelationship.getType()));
            this.relationshipsState = new UserRelationshipsState.Loaded(mutableMap);
            markChanged();
        }
    }

    @StoreThread
    public final void handleRelationshipRemove(ModelUserRelationship modelUserRelationship) {
        m.checkNotNullParameter(modelUserRelationship, "relationship");
        Map mutableMap = h0.toMutableMap(ensureRelationshipLoaded().getRelationships());
        if (mutableMap.remove(Long.valueOf(modelUserRelationship.getId())) != null) {
            this.relationshipsState = new UserRelationshipsState.Loaded(mutableMap);
            markChanged();
        }
    }

    public final void init() {
        HashMap hashMap;
        Map<Long, Integer> map = this.relationshipsCache.get();
        hashMap = StoreUserRelationshipsKt.UNLOADED_RELATIONSHIPS_SENTINEL;
        if (!m.areEqual(map, hashMap)) {
            this.relationshipsState = new UserRelationshipsState.Loaded(map);
            markChanged();
        }
    }

    public final Observable<Map<Long, Integer>> observe() {
        Observable<Map<Long, Integer>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreUserRelationships$observe$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    public final Observable<Map<Long, Integer>> observeForType(final int i) {
        Observable<Map<Long, Integer>> q = observe().F(new b<Map<Long, ? extends Integer>, Map<Long, ? extends Integer>>() { // from class: com.discord.stores.StoreUserRelationships$observeForType$1
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Map<Long, ? extends Integer> call(Map<Long, ? extends Integer> map) {
                return call2((Map<Long, Integer>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Map<Long, Integer> call2(Map<Long, Integer> map) {
                m.checkNotNullExpressionValue(map, "relationships");
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                for (Map.Entry<Long, Integer> entry : map.entrySet()) {
                    if (entry.getValue().intValue() == i) {
                        linkedHashMap.put(entry.getKey(), entry.getValue());
                    }
                }
                return linkedHashMap;
            }
        }).q();
        m.checkNotNullExpressionValue(q, "observe()\n          .map…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<UserRelationshipsState> observeUserRelationshipsState() {
        Observable<UserRelationshipsState> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreUserRelationships$observeUserRelationshipsState$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        UserRelationshipsState userRelationshipsState;
        super.snapshotData();
        UserRelationshipsState userRelationshipsState2 = this.relationshipsState;
        if (userRelationshipsState2 instanceof UserRelationshipsState.Loaded) {
            HashMap hashMap = new HashMap(((UserRelationshipsState.Loaded) userRelationshipsState2).getRelationships());
            Persister.set$default(this.relationshipsCache, hashMap, false, 2, null);
            userRelationshipsState = new UserRelationshipsState.Loaded(hashMap);
        } else {
            Persister.clear$default(this.relationshipsCache, false, 1, null);
            userRelationshipsState = UserRelationshipsState.Unloaded.INSTANCE;
        }
        this.relationshipsStateSnapshot = userRelationshipsState;
    }

    public StoreUserRelationships(ObservationDeck observationDeck) {
        HashMap hashMap;
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.observationDeck = observationDeck;
        UserRelationshipsState.Unloaded unloaded = UserRelationshipsState.Unloaded.INSTANCE;
        this.relationshipsState = unloaded;
        this.relationshipsStateSnapshot = unloaded;
        hashMap = StoreUserRelationshipsKt.UNLOADED_RELATIONSHIPS_SENTINEL;
        this.relationshipsCache = new Persister<>("STORE_USER_RELATIONSHIPS_V9", hashMap);
    }

    public final Observable<Map<Long, Integer>> observe(final Collection<Long> collection) {
        m.checkNotNullParameter(collection, "userIds");
        Observable<Map<Long, Integer>> q = observe().F(new b<Map<Long, ? extends Integer>, Map<Long, ? extends Integer>>() { // from class: com.discord.stores.StoreUserRelationships$observe$2
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Map<Long, ? extends Integer> call(Map<Long, ? extends Integer> map) {
                return call2((Map<Long, Integer>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Map<Long, Integer> call2(Map<Long, Integer> map) {
                m.checkNotNullExpressionValue(map, "it");
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                for (Map.Entry<Long, Integer> entry : map.entrySet()) {
                    if (collection.contains(Long.valueOf(entry.getKey().longValue()))) {
                        linkedHashMap.put(entry.getKey(), entry.getValue());
                    }
                }
                return linkedHashMap;
            }
        }).q();
        m.checkNotNullExpressionValue(q, "observe()\n          .map…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Integer> observe(final long j) {
        Observable F = observe().F(new b<Map<Long, ? extends Integer>, Integer>() { // from class: com.discord.stores.StoreUserRelationships$observe$3
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Integer call(Map<Long, ? extends Integer> map) {
                return call2((Map<Long, Integer>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Integer call2(Map<Long, Integer> map) {
                return map.get(Long.valueOf(j));
            }
        });
        m.checkNotNullExpressionValue(F, "observe()\n          .map…> relationships[userId] }");
        return F;
    }
}
