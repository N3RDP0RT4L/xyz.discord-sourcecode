package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.core.app.NotificationCompat;
import b.a.d.a0;
import b.a.d.b0;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.models.application.Unread;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelNotificationSettings;
import com.discord.models.message.Message;
import com.discord.stores.StoreMessageAck;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreThreadsActiveJoined;
import com.discord.utilities.SnowflakeUtils;
import com.discord.utilities.message.MessageUtils;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.persister.Persister;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$3;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$4;
import com.discord.utilities.rx.ObservableWithLeadingEdgeThrottle;
import com.discord.utilities.time.Clock;
import d0.z.d.m;
import j0.k.a;
import j0.k.b;
import j0.l.a.k;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.functions.Function7;
import rx.Observable;
import rx.functions.Action0;
import rx.functions.Func7;
import rx.subjects.BehaviorSubject;
import rx.subjects.SerializedSubject;
/* compiled from: StoreReadStates.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0090\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u00108\u001a\u000207¢\u0006\u0004\b@\u0010AJ\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0004J\u000f\u0010\u0006\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0006\u0010\u0004Jã\u0001\u0010\u001b\u001a\"\u0012\u000e\u0012\f\u0012\b\u0012\u00060\bj\u0002`\t0\u001a\u0012\u000e\u0012\f\u0012\b\u0012\u00060\bj\u0002`\u000e0\u001a0\u00192\u001a\u0010\u000b\u001a\u0016\u0012\b\u0012\u00060\bj\u0002`\t\u0012\b\u0012\u00060\bj\u0002`\n0\u00072\u0016\u0010\r\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`\t\u0012\u0004\u0012\u00020\f0\u00072\u001a\u0010\u0010\u001a\u0016\u0012\b\u0012\u00060\bj\u0002`\u000e\u0012\b\u0012\u00060\bj\u0002`\u000f0\u00072\u0016\u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`\u000e\u0012\u0004\u0012\u00020\u00110\u00072\u0016\u0010\u0014\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`\t\u0012\u0004\u0012\u00020\u00130\u00072\u001a\u0010\u0016\u001a\u0016\u0012\b\u0012\u00060\bj\u0002`\t\u0012\b\u0012\u00060\bj\u0002`\u00150\u00072\u0016\u0010\u0018\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`\t\u0012\u0004\u0012\u00020\u00170\u0007H\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ1\u0010\u001f\u001a\u00020\u001e2\b\u0010\u001d\u001a\u0004\u0018\u00010\f2\u0016\u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`\u000e\u0012\u0004\u0012\u00020\u00110\u0007H\u0002¢\u0006\u0004\b\u001f\u0010 J\u001d\u0010$\u001a\b\u0012\u0004\u0012\u00020#0\"2\u0006\u0010!\u001a\u00020\bH\u0007¢\u0006\u0004\b$\u0010%J\u001d\u0010&\u001a\u0012\u0012\u000e\u0012\f\u0012\b\u0012\u00060\bj\u0002`\t0\u001a0\"¢\u0006\u0004\b&\u0010'J\u001d\u0010(\u001a\u0012\u0012\u000e\u0012\f\u0012\b\u0012\u00060\bj\u0002`\u000e0\u001a0\"¢\u0006\u0004\b(\u0010'J\u001f\u0010*\u001a\b\u0012\u0004\u0012\u00020\u001e0\"2\n\u0010)\u001a\u00060\bj\u0002`\u000e¢\u0006\u0004\b*\u0010%J\u0017\u0010+\u001a\u00020\u00022\b\u0010!\u001a\u0004\u0018\u00010\b¢\u0006\u0004\b+\u0010,J\u0017\u0010/\u001a\u00020\u00022\u0006\u0010.\u001a\u00020-H\u0016¢\u0006\u0004\b/\u00100J\u001f\u00102\u001a\b\u0012\u0004\u0012\u0002010\"2\n\u0010!\u001a\u00060\bj\u0002`\t¢\u0006\u0004\b2\u0010%RZ\u00105\u001aF\u0012 \u0012\u001e\u0012\b\u0012\u00060\bj\u0002`\t 4*\u000e\u0012\b\u0012\u00060\bj\u0002`\t\u0018\u00010\u001a0\u001a\u0012 \u0012\u001e\u0012\b\u0012\u00060\bj\u0002`\t 4*\u000e\u0012\b\u0012\u00060\bj\u0002`\t\u0018\u00010\u001a0\u001a038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b5\u00106R\u0016\u00108\u001a\u0002078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b8\u00109RZ\u0010:\u001aF\u0012 \u0012\u001e\u0012\b\u0012\u00060\bj\u0002`\u000e 4*\u000e\u0012\b\u0012\u00060\bj\u0002`\u000e\u0018\u00010\u001a0\u001a\u0012 \u0012\u001e\u0012\b\u0012\u00060\bj\u0002`\u000e 4*\u000e\u0012\b\u0012\u00060\bj\u0002`\u000e\u0018\u00010\u001a0\u001a038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b:\u00106R\u0019\u0010<\u001a\b\u0012\u0004\u0012\u00020#0\"8F@\u0006¢\u0006\u0006\u001a\u0004\b;\u0010'R\u001c\u0010>\u001a\b\u0012\u0004\u0012\u00020#0=8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b>\u0010?R2\u0010+\u001a\u001e\u0012\f\u0012\n 4*\u0004\u0018\u00010\u001e0\u001e\u0012\f\u0012\n 4*\u0004\u0018\u00010\u001e0\u001e038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b+\u00106¨\u0006B"}, d2 = {"Lcom/discord/stores/StoreReadStates;", "Lcom/discord/stores/Store;", "", "computeUnreadChannelIds", "()V", "computeUnreadMarker", "clearMarker", "", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/permission/PermissionBit;", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS, "Lcom/discord/api/channel/Channel;", "channels", "Lcom/discord/primitives/GuildId;", "Lcom/discord/primitives/Timestamp;", "guildJoinedAt", "Lcom/discord/models/domain/ModelNotificationSettings;", "guildSettings", "Lcom/discord/stores/StoreMessageAck$Ack;", "acks", "Lcom/discord/primitives/MessageId;", "mostRecent", "Lcom/discord/stores/StoreThreadsActiveJoined$ActiveJoinedThread;", "activeJoinedThreads", "Lkotlin/Pair;", "", "computeUnreadIds", "(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Lkotlin/Pair;", "channel", "", "isChannelMuted", "(Lcom/discord/api/channel/Channel;Ljava/util/Map;)Z", "channelId", "Lrx/Observable;", "Lcom/discord/models/application/Unread;", "getUnreadMarker", "(J)Lrx/Observable;", "getUnreadChannelIds", "()Lrx/Observable;", "getUnreadGuildIds", "guildId", "getIsUnread", "markAsRead", "(Ljava/lang/Long;)V", "Landroid/content/Context;", "context", "init", "(Landroid/content/Context;)V", "", "observeUnreadCountForChannel", "Lrx/subjects/SerializedSubject;", "kotlin.jvm.PlatformType", "unreadChannelIds", "Lrx/subjects/SerializedSubject;", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "unreadGuildIds", "getUnreadMarkerForSelectedChannel", "unreadMarkerForSelectedChannel", "Lcom/discord/utilities/persister/Persister;", "unreadMessageMarker", "Lcom/discord/utilities/persister/Persister;", HookHelper.constructorName, "(Lcom/discord/utilities/time/Clock;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreReadStates extends Store {
    private final Clock clock;
    private final SerializedSubject<Set<Long>, Set<Long>> unreadChannelIds = new SerializedSubject<>(BehaviorSubject.l0(new HashSet()));
    private final SerializedSubject<Set<Long>, Set<Long>> unreadGuildIds = new SerializedSubject<>(BehaviorSubject.l0(new HashSet()));
    private final SerializedSubject<Boolean, Boolean> markAsRead = new SerializedSubject<>(BehaviorSubject.l0(Boolean.FALSE));
    private final Persister<Unread> unreadMessageMarker = new Persister<>("UNREAD_MESSAGE_MARKER_V3", new Unread(null, 0, 3, null));

    public StoreReadStates(Clock clock) {
        m.checkNotNullParameter(clock, "clock");
        this.clock = clock;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void clearMarker() {
        Observable Z = this.unreadMessageMarker.getObservable().F(StoreReadStates$clearMarker$1.INSTANCE).Z(1);
        m.checkNotNullExpressionValue(Z, "unreadMessageMarker\n    …ount() }\n        .take(1)");
        ObservableExtensionsKt.appSubscribe(Z, (r18 & 1) != 0 ? null : null, "unreadMessageMarker", (r18 & 4) != 0 ? null : null, new StoreReadStates$clearMarker$2(this), (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
    }

    private final void computeUnreadChannelIds() {
        StoreStream.Companion companion = StoreStream.Companion;
        Observable<Map<Long, Long>> observePermissionsForAllChannels = companion.getPermissions().observePermissionsForAllChannels();
        Observable<Map<Long, Channel>> observeGuildAndPrivateChannels = companion.getChannels().observeGuildAndPrivateChannels();
        Observable<Map<Long, Long>> observeJoinedAt = companion.getGuilds().observeJoinedAt();
        Observable<Map<Long, ModelNotificationSettings>> observeGuildSettings = companion.getUserGuildSettings().observeGuildSettings();
        Observable<Map<Long, StoreMessageAck.Ack>> observeAll = companion.getMessageAck().observeAll();
        Observable<Map<Long, Long>> observeRecentMessageIds = companion.getMessagesMostRecent().observeRecentMessageIds();
        TimeUnit timeUnit = TimeUnit.SECONDS;
        Observable leadingEdgeThrottle = ObservableExtensionsKt.leadingEdgeThrottle(observeRecentMessageIds, 3L, timeUnit);
        Observable<Map<Long, StoreThreadsActiveJoined.ActiveJoinedThread>> observeAllActiveJoinedThreadsById = companion.getThreadsActiveJoined().observeAllActiveJoinedThreadsById();
        final StoreReadStates$computeUnreadChannelIds$1 storeReadStates$computeUnreadChannelIds$1 = new StoreReadStates$computeUnreadChannelIds$1(this);
        Observable combineLatest = ObservableWithLeadingEdgeThrottle.combineLatest(observePermissionsForAllChannels, observeGuildAndPrivateChannels, observeJoinedAt, observeGuildSettings, observeAll, leadingEdgeThrottle, observeAllActiveJoinedThreadsById, new Func7() { // from class: com.discord.stores.StoreReadStates$sam$rx_functions_Func7$0
            @Override // rx.functions.Func7
            public final /* synthetic */ Object call(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7) {
                return Function7.this.invoke(obj, obj2, obj3, obj4, obj5, obj6, obj7);
            }
        }, 1L, timeUnit);
        m.checkNotNullExpressionValue(combineLatest, "ObservableWithLeadingEdg…imeUnit.SECONDS\n        )");
        Observable q = ObservableExtensionsKt.computationLatest(combineLatest).q();
        m.checkNotNullExpressionValue(q, "ObservableWithLeadingEdg…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(q, (r18 & 1) != 0 ? null : null, "computeUnreadChannelIds", (r18 & 4) != 0 ? null : null, new StoreReadStates$computeUnreadChannelIds$2(this), (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Pair<Set<Long>, Set<Long>> computeUnreadIds(Map<Long, Long> map, Map<Long, Channel> map2, Map<Long, Long> map3, Map<Long, ? extends ModelNotificationSettings> map4, Map<Long, StoreMessageAck.Ack> map5, Map<Long, Long> map6, Map<Long, StoreThreadsActiveJoined.ActiveJoinedThread> map7) {
        long j;
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        for (Map.Entry<Long, Long> entry : map6.entrySet()) {
            long longValue = entry.getKey().longValue();
            long longValue2 = entry.getValue().longValue();
            StoreThreadsActiveJoined.ActiveJoinedThread activeJoinedThread = map7.get(Long.valueOf(longValue));
            Channel channel = map2.get(Long.valueOf(longValue));
            if (channel == null) {
                StoreThreadsActiveJoined.ActiveJoinedThread activeJoinedThread2 = map7.get(Long.valueOf(longValue));
                channel = activeJoinedThread2 != null ? activeJoinedThread2.getChannel() : null;
            }
            if (channel != null && !ChannelUtils.x(channel) && (!ChannelUtils.C(channel) || activeJoinedThread != null)) {
                if (PermissionUtils.INSTANCE.hasAccess(channel, map.get(Long.valueOf(longValue))) && !isChannelMuted(channel, map4)) {
                    StoreMessageAck.Ack ack = map5.get(Long.valueOf(longValue));
                    if (ack != null || !ChannelUtils.C(channel)) {
                        if (ack != null) {
                            j = ack.getMessageId();
                        } else {
                            Long l = (Long) a.u0(channel, map3);
                            j = ((l != null ? l.longValue() : this.clock.currentTimeMillis()) - SnowflakeUtils.DISCORD_EPOCH) << 22;
                        }
                        if (MessageUtils.isNewer(Long.valueOf(j), Long.valueOf(longValue2))) {
                            hashSet.add(Long.valueOf(longValue));
                            Channel channel2 = map2.get(Long.valueOf(channel.r()));
                            boolean isChannelMuted = isChannelMuted(channel2, map4);
                            if (!isChannelMuted && ChannelUtils.C(channel) && channel2 != null) {
                                isChannelMuted = isChannelMuted(map2.get(Long.valueOf(channel2.r())), map4);
                            }
                            if (!isChannelMuted) {
                                hashSet2.add(Long.valueOf(channel.f()));
                            }
                        }
                    }
                }
            }
        }
        return new Pair<>(hashSet, hashSet2);
    }

    private final void computeUnreadMarker() {
        StoreReadStates$computeUnreadMarker$1 storeReadStates$computeUnreadMarker$1 = StoreReadStates$computeUnreadMarker$1.INSTANCE;
        Observable<Long> observeId = StoreStream.Companion.getChannelsSelected().observeId();
        StoreReadStates$computeUnreadMarker$2 storeReadStates$computeUnreadMarker$2 = StoreReadStates$computeUnreadMarker$2.INSTANCE;
        Unread.Marker marker = new Unread.Marker();
        StoreReadStates$computeUnreadMarker$3 storeReadStates$computeUnreadMarker$3 = StoreReadStates$computeUnreadMarker$3.INSTANCE;
        m.checkNotNullParameter(storeReadStates$computeUnreadMarker$2, "observableCondition");
        m.checkNotNullParameter(storeReadStates$computeUnreadMarker$3, "defaultObservableFunc");
        b0 b0Var = new b0(marker);
        m.checkNotNullParameter(storeReadStates$computeUnreadMarker$2, "observableCondition");
        m.checkNotNullParameter(b0Var, "switchedObservableFunc");
        m.checkNotNullParameter(storeReadStates$computeUnreadMarker$3, "defaultObservableFunc");
        Observable Y = observeId.k(new a0(storeReadStates$computeUnreadMarker$2, b0Var, storeReadStates$computeUnreadMarker$3)).Y(new b<Unread.Marker, Observable<? extends Unread>>() { // from class: com.discord.stores.StoreReadStates$computeUnreadMarker$4

            /* compiled from: StoreReadStates.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000b\n\u0002\b\u0005\u0010\u0005\u001a\n \u0002*\u0004\u0018\u00010\u00000\u00002\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "markAsRead", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Boolean;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.stores.StoreReadStates$computeUnreadMarker$4$2  reason: invalid class name */
            /* loaded from: classes.dex */
            public static final class AnonymousClass2<T, R> implements b<Boolean, Boolean> {
                public static final AnonymousClass2 INSTANCE = new AnonymousClass2();

                public final Boolean call(Boolean bool) {
                    return bool;
                }
            }

            public final Observable<? extends Unread> call(final Unread.Marker marker2) {
                SerializedSubject serializedSubject;
                m.checkNotNullParameter(marker2, "marker");
                Observable<R> F = StoreStream.Companion.getMessages().observeMessagesForChannel(marker2.getChannelId()).F(new b<List<? extends Message>, Unread>() { // from class: com.discord.stores.StoreReadStates$computeUnreadMarker$4.1
                    @Override // j0.k.b
                    public /* bridge */ /* synthetic */ Unread call(List<? extends Message> list) {
                        return call2((List<Message>) list);
                    }

                    /* renamed from: call  reason: avoid collision after fix types in other method */
                    public final Unread call2(List<Message> list) {
                        Unread.Marker marker3 = Unread.Marker.this;
                        m.checkNotNullExpressionValue(list, "messages");
                        return new Unread(marker3, list);
                    }
                });
                serializedSubject = StoreReadStates.this.markAsRead;
                Observable<R> a02 = F.a0((Observable<R>) serializedSubject.x(AnonymousClass2.INSTANCE));
                Action0 action0 = new Action0() { // from class: com.discord.stores.StoreReadStates$computeUnreadMarker$4.3
                    @Override // rx.functions.Action0
                    public final void call() {
                        StoreReadStates.this.clearMarker();
                    }
                };
                a.C0399a aVar = j0.k.a.a;
                return Observable.h0(new k(a02, new j0.l.e.a(aVar, aVar, action0)));
            }
        });
        m.checkNotNullExpressionValue(Y, "getChannelsSelected()\n  …clearMarker() }\n        }");
        ObservableExtensionsKt.appSubscribe(Y, (r18 & 1) != 0 ? null : null, "unreadMessageMarker", (r18 & 4) != 0 ? null : null, new StoreReadStates$computeUnreadMarker$5(this), (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
    }

    private final boolean isChannelMuted(Channel channel, Map<Long, ? extends ModelNotificationSettings> map) {
        if (channel == null) {
            return false;
        }
        long h = channel.h();
        ModelNotificationSettings modelNotificationSettings = (ModelNotificationSettings) b.d.b.a.a.u0(channel, map);
        ModelNotificationSettings.ChannelOverride channelOverride = modelNotificationSettings != null ? modelNotificationSettings.getChannelOverride(h) : null;
        return channelOverride != null && channelOverride.isMuted();
    }

    public final Observable<Boolean> getIsUnread(final long j) {
        Observable<Boolean> q = getUnreadGuildIds().F(new b<Set<? extends Long>, Boolean>() { // from class: com.discord.stores.StoreReadStates$getIsUnread$1
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Boolean call(Set<? extends Long> set) {
                return call2((Set<Long>) set);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Boolean call2(Set<Long> set) {
                return Boolean.valueOf(set.contains(Long.valueOf(j)));
            }
        }).q();
        m.checkNotNullExpressionValue(q, "getUnreadGuildIds()\n    …  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Set<Long>> getUnreadChannelIds() {
        return this.unreadChannelIds;
    }

    public final Observable<Set<Long>> getUnreadGuildIds() {
        return this.unreadGuildIds;
    }

    public final Observable<Unread> getUnreadMarker(final long j) {
        Observable<Unread> q = getUnreadMarkerForSelectedChannel().x(new b<Unread, Boolean>() { // from class: com.discord.stores.StoreReadStates$getUnreadMarker$1
            public final Boolean call(Unread unread) {
                m.checkNotNullParameter(unread, "marker");
                return Boolean.valueOf(unread.getMarker().getChannelId() == j);
            }
        }).q();
        m.checkNotNullExpressionValue(q, "unreadMarkerForSelectedC…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Unread> getUnreadMarkerForSelectedChannel() {
        Observable<Unread> q = ObservableExtensionsKt.computationLatest(this.unreadMessageMarker.getObservable()).q();
        m.checkNotNullExpressionValue(q, "unreadMessageMarker\n    …  .distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.Store
    public void init(Context context) {
        m.checkNotNullParameter(context, "context");
        super.init(context);
        computeUnreadChannelIds();
        computeUnreadMarker();
    }

    public final void markAsRead(Long l) {
        if (l != null) {
            SerializedSubject<Boolean, Boolean> serializedSubject = this.markAsRead;
            serializedSubject.k.onNext(Boolean.TRUE);
            SerializedSubject<Boolean, Boolean> serializedSubject2 = this.markAsRead;
            serializedSubject2.k.onNext(Boolean.FALSE);
            StoreStream.Companion.getMessageAck().ack(l.longValue(), false, false);
        }
    }

    public final Observable<Integer> observeUnreadCountForChannel(final long j) {
        StoreStream.Companion companion = StoreStream.Companion;
        Observable<Integer> j2 = Observable.j(companion.getMessages().observeMessagesForChannel(j), companion.getMessageAck().observeAll().F(new b<Map<Long, ? extends StoreMessageAck.Ack>, StoreMessageAck.Ack>() { // from class: com.discord.stores.StoreReadStates$observeUnreadCountForChannel$1
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ StoreMessageAck.Ack call(Map<Long, ? extends StoreMessageAck.Ack> map) {
                return call2((Map<Long, StoreMessageAck.Ack>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final StoreMessageAck.Ack call2(Map<Long, StoreMessageAck.Ack> map) {
                return map.get(Long.valueOf(j));
            }
        }), StoreReadStates$observeUnreadCountForChannel$2.INSTANCE);
        m.checkNotNullExpressionValue(j2, "Observable.combineLatest…\n      messageCount\n    }");
        return j2;
    }
}
