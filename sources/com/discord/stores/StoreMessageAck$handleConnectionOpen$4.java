package com.discord.stores;

import d0.z.d.m;
import d0.z.d.o;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreMessageAck.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\b\u001a\u00020\u00052&\u0010\u0004\u001a\"\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\u0001\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\u00010\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Lkotlin/Pair;", "", "", "Lcom/discord/primitives/ChannelId;", "<name for destructuring parameter 0>", "", "invoke", "(Lkotlin/Pair;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessageAck$handleConnectionOpen$4 extends o implements Function1<Pair<? extends Set<? extends Long>, ? extends Set<? extends Long>>, Unit> {
    public final /* synthetic */ StoreMessageAck this$0;

    /* compiled from: StoreMessageAck.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreMessageAck$handleConnectionOpen$4$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ Set $newThreadIds;
        public final /* synthetic */ Set $oldThreadIds;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Set set, Set set2) {
            super(0);
            this.$oldThreadIds = set;
            this.$newThreadIds = set2;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            StoreMessageAck$handleConnectionOpen$4.this.this$0.updateThreadAcks(this.$oldThreadIds, this.$newThreadIds);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreMessageAck$handleConnectionOpen$4(StoreMessageAck storeMessageAck) {
        super(1);
        this.this$0 = storeMessageAck;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Pair<? extends Set<? extends Long>, ? extends Set<? extends Long>> pair) {
        invoke2((Pair<? extends Set<Long>, ? extends Set<Long>>) pair);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Pair<? extends Set<Long>, ? extends Set<Long>> pair) {
        Dispatcher dispatcher;
        m.checkNotNullParameter(pair, "<name for destructuring parameter 0>");
        dispatcher = this.this$0.dispatcher;
        dispatcher.schedule(new AnonymousClass1(pair.component1(), pair.component2()));
    }
}
