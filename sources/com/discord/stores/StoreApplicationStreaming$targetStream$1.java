package com.discord.stores;

import com.discord.models.domain.ModelApplicationStream;
import com.discord.stores.StoreApplicationStreaming;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreApplicationStreaming.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreApplicationStreaming$targetStream$1 extends o implements Function0<Unit> {
    public final /* synthetic */ boolean $force;
    public final /* synthetic */ String $streamKey;
    public final /* synthetic */ StoreApplicationStreaming this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreApplicationStreaming$targetStream$1(StoreApplicationStreaming storeApplicationStreaming, String str, boolean z2) {
        super(0);
        this.this$0 = storeApplicationStreaming;
        this.$streamKey = str;
        this.$force = z2;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreApplicationStreaming.ActiveApplicationStream activeApplicationStream;
        StoreStream storeStream;
        StoreApplicationStreaming.ActiveApplicationStream.State state;
        ModelApplicationStream stream;
        activeApplicationStream = this.this$0.activeApplicationStream;
        boolean z2 = m.areEqual((activeApplicationStream == null || (stream = activeApplicationStream.getStream()) == null) ? null : stream.getEncodedStreamKey(), this.$streamKey) && activeApplicationStream.getState().isStreamActive();
        if (this.$force || !z2) {
            if (!(activeApplicationStream == null || (state = activeApplicationStream.getState()) == null || !state.isStreamActive())) {
                this.this$0.stopStreamInternal(activeApplicationStream.getStream().getEncodedStreamKey());
            }
            storeStream = this.this$0.storeStream;
            storeStream.handleStreamTargeted(this.$streamKey);
        }
    }
}
