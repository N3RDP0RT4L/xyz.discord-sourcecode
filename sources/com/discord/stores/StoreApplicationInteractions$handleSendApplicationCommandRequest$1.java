package com.discord.stores;

import com.discord.models.commands.ApplicationCommandLocalSendData;
import com.discord.utilities.messagesend.MessageResult;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreApplicationInteractions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/messagesend/MessageResult;", "result", "", "invoke", "(Lcom/discord/utilities/messagesend/MessageResult;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreApplicationInteractions$handleSendApplicationCommandRequest$1 extends o implements Function1<MessageResult, Unit> {
    public final /* synthetic */ ApplicationCommandLocalSendData $localSendData;
    public final /* synthetic */ Function1 $onFail;
    public final /* synthetic */ Function0 $onSuccess;
    public final /* synthetic */ StoreApplicationInteractions this$0;

    /* compiled from: StoreApplicationInteractions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreApplicationInteractions$handleSendApplicationCommandRequest$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ MessageResult $result;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(MessageResult messageResult) {
            super(0);
            this.$result = messageResult;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            StoreApplicationInteractions$handleSendApplicationCommandRequest$1 storeApplicationInteractions$handleSendApplicationCommandRequest$1 = StoreApplicationInteractions$handleSendApplicationCommandRequest$1.this;
            storeApplicationInteractions$handleSendApplicationCommandRequest$1.this$0.handleApplicationCommandResult(this.$result, storeApplicationInteractions$handleSendApplicationCommandRequest$1.$localSendData, storeApplicationInteractions$handleSendApplicationCommandRequest$1.$onSuccess, storeApplicationInteractions$handleSendApplicationCommandRequest$1.$onFail);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreApplicationInteractions$handleSendApplicationCommandRequest$1(StoreApplicationInteractions storeApplicationInteractions, ApplicationCommandLocalSendData applicationCommandLocalSendData, Function0 function0, Function1 function1) {
        super(1);
        this.this$0 = storeApplicationInteractions;
        this.$localSendData = applicationCommandLocalSendData;
        this.$onSuccess = function0;
        this.$onFail = function1;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(MessageResult messageResult) {
        invoke2(messageResult);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(MessageResult messageResult) {
        Dispatcher dispatcher;
        m.checkNotNullParameter(messageResult, "result");
        dispatcher = this.this$0.dispatcher;
        dispatcher.schedule(new AnonymousClass1(messageResult));
    }
}
