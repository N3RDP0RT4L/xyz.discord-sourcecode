package com.discord.stores;

import com.discord.rtcconnection.RtcConnection;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreStreamRtcConnection.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStreamRtcConnection$updateStreamVolume$1 extends o implements Function0<Unit> {
    public final /* synthetic */ float $volume;
    public final /* synthetic */ StoreStreamRtcConnection this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreStreamRtcConnection$updateStreamVolume$1(StoreStreamRtcConnection storeStreamRtcConnection, float f) {
        super(0);
        this.this$0 = storeStreamRtcConnection;
        this.$volume = f;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        Long l;
        RtcConnection rtcConnection;
        this.this$0.streamVolume = this.$volume;
        l = this.this$0.streamOwner;
        if (l != null) {
            long longValue = l.longValue();
            rtcConnection = this.this$0.rtcConnection;
            if (rtcConnection != null) {
                rtcConnection.v(longValue, this.$volume);
            }
        }
        this.this$0.markChanged();
    }
}
