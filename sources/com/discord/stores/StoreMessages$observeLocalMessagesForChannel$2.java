package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.models.message.Message;
import java.util.List;
import kotlin.Metadata;
import rx.functions.Func2;
/* compiled from: StoreMessages.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\b\u001a\n \u0002*\u0004\u0018\u00010\u00050\u00052\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u00002\u001a\u0010\u0004\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "Lcom/discord/models/message/Message;", "kotlin.jvm.PlatformType", "messages1", "messages2", "", NotificationCompat.CATEGORY_CALL, "(Ljava/util/List;Ljava/util/List;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessages$observeLocalMessagesForChannel$2<T1, T2, R> implements Func2<List<? extends Message>, List<? extends Message>, Boolean> {
    public static final StoreMessages$observeLocalMessagesForChannel$2 INSTANCE = new StoreMessages$observeLocalMessagesForChannel$2();

    @Override // rx.functions.Func2
    public /* bridge */ /* synthetic */ Boolean call(List<? extends Message> list, List<? extends Message> list2) {
        return call2((List<Message>) list, (List<Message>) list2);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final Boolean call2(List<Message> list, List<Message> list2) {
        return Boolean.valueOf(list == list2);
    }
}
