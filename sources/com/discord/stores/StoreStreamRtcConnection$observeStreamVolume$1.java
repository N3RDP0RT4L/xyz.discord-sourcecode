package com.discord.stores;

import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreStreamRtcConnection.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0007\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()F", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStreamRtcConnection$observeStreamVolume$1 extends o implements Function0<Float> {
    public final /* synthetic */ StoreStreamRtcConnection this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreStreamRtcConnection$observeStreamVolume$1(StoreStreamRtcConnection storeStreamRtcConnection) {
        super(0);
        this.this$0 = storeStreamRtcConnection;
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [java.lang.Float, float] */
    @Override // kotlin.jvm.functions.Function0
    public final Float invoke() {
        return this.this$0.getStreamVolume();
    }
}
