package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.SharedPreferences;
import com.discord.stores.StoreNotices;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.time.Clock;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: StoreReviewRequest.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\u0018\u0000 \u001d2\u00020\u0001:\u0001\u001dB\u0017\u0012\u0006\u0010\u0010\u001a\u00020\u000f\u0012\u0006\u0010\u0016\u001a\u00020\u0015¢\u0006\u0004\b\u001b\u0010\u001cJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0007¢\u0006\u0004\b\t\u0010\nJ\r\u0010\u000b\u001a\u00020\u0004¢\u0006\u0004\b\u000b\u0010\fJ\r\u0010\r\u001a\u00020\u0004¢\u0006\u0004\b\r\u0010\fJ\r\u0010\u000e\u001a\u00020\u0004¢\u0006\u0004\b\u000e\u0010\fR\u0016\u0010\u0010\u001a\u00020\u000f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0016\u001a\u00020\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0019\u001a\u00020\u00188\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0019\u0010\u001a¨\u0006\u001e"}, d2 = {"Lcom/discord/stores/StoreReviewRequest;", "Lcom/discord/stores/Store;", "Landroid/content/Context;", "context", "", "init", "(Landroid/content/Context;)V", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "onReviewRequestShown", "()V", "onUserAcceptedRequest", "onUserDismissedRequest", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "", "hasUserAcceptedReviewRequest", "Z", "Lcom/discord/stores/StoreStream;", "stream", "Lcom/discord/stores/StoreStream;", "", "requestedReviewRevision", "I", HookHelper.constructorName, "(Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreStream;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreReviewRequest extends Store {
    public static final Companion Companion = new Companion(null);
    private static final int MINIMUM_GUILD_MEMBER_COUNT = 5;
    private static final long MINIMUM_INSTALL_AGE = 864000000;
    private static final int REVIEW_REQUEST_REVISION = 693;
    private final Clock clock;
    private boolean hasUserAcceptedReviewRequest;
    private int requestedReviewRevision = -1;
    private final StoreStream stream;

    /* compiled from: StoreReviewRequest.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007R\u0016\u0010\b\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\b\u0010\u0004¨\u0006\u000b"}, d2 = {"Lcom/discord/stores/StoreReviewRequest$Companion;", "", "", "MINIMUM_GUILD_MEMBER_COUNT", "I", "", "MINIMUM_INSTALL_AGE", "J", "REVIEW_REQUEST_REVISION", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public StoreReviewRequest(Clock clock, StoreStream storeStream) {
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(storeStream, "stream");
        this.clock = clock;
        this.stream = storeStream;
    }

    /* JADX WARN: Code restructure failed: missing block: B:16:0x0033, code lost:
        if (r0 == true) goto L18;
     */
    @com.discord.stores.StoreThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void handleConnectionOpen(com.discord.models.domain.ModelPayload r10) {
        /*
            r9 = this;
            java.lang.String r0 = "payload"
            d0.z.d.m.checkNotNullParameter(r10, r0)
            java.util.List r0 = r10.getGuilds()
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L36
            boolean r3 = r0.isEmpty()
            if (r3 == 0) goto L16
        L14:
            r0 = 0
            goto L33
        L16:
            java.util.Iterator r0 = r0.iterator()
        L1a:
            boolean r3 = r0.hasNext()
            if (r3 == 0) goto L14
            java.lang.Object r3 = r0.next()
            com.discord.api.guild.Guild r3 = (com.discord.api.guild.Guild) r3
            int r3 = r3.u()
            r4 = 5
            if (r3 < r4) goto L2f
            r3 = 1
            goto L30
        L2f:
            r3 = 0
        L30:
            if (r3 == 0) goto L1a
            r0 = 1
        L33:
            if (r0 != r1) goto L36
            goto L37
        L36:
            r1 = 0
        L37:
            com.discord.api.user.User r10 = r10.getMe()
            java.lang.Boolean r10 = r10.s()
            if (r10 == 0) goto L45
            boolean r2 = r10.booleanValue()
        L45:
            if (r1 == 0) goto L89
            if (r2 != 0) goto L4a
            goto L89
        L4a:
            boolean r10 = r9.hasUserAcceptedReviewRequest
            if (r10 == 0) goto L4f
            return
        L4f:
            int r10 = r9.requestedReviewRevision
            r0 = 693(0x2b5, float:9.71E-43)
            if (r10 < r0) goto L56
            return
        L56:
            com.discord.stores.StoreStream r10 = r9.stream
            com.discord.stores.StoreNotices r10 = r10.getNotices$app_productionGoogleRelease()
            long r0 = r10.getFirstUseTimestamp()
            r2 = 864000000(0x337f9800, double:4.26872718E-315)
            long r0 = r0 + r2
            com.discord.utilities.time.Clock r10 = r9.clock
            long r2 = r10.currentTimeMillis()
            int r10 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r10 > 0) goto L6f
            return
        L6f:
            com.discord.stores.StoreNotices$Dialog$Type r0 = com.discord.stores.StoreNotices.Dialog.Type.REQUEST_RATING_MODAL
            r1 = 0
            r6 = 1
            r4 = 0
            r2 = 31536000000(0x757b12c00, double:1.55808542072E-313)
            r7 = 5
            r8 = 0
            com.discord.stores.StoreNotices$PassiveNotice r10 = com.discord.stores.StoreNotices.Dialog.Type.buildPassiveNotice$default(r0, r1, r2, r4, r6, r7, r8)
            com.discord.stores.StoreStream r0 = r9.stream
            com.discord.stores.StoreNotices r0 = r0.getNotices$app_productionGoogleRelease()
            r0.requestToShow(r10)
        L89:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreReviewRequest.handleConnectionOpen(com.discord.models.domain.ModelPayload):void");
    }

    @Override // com.discord.stores.Store
    public void init(Context context) {
        m.checkNotNullParameter(context, "context");
        super.init(context);
        this.requestedReviewRevision = getPrefsSessionDurable().getInt("CACHE_KEY_VIEWED_REVIEW_REQUEST_REVISION", this.requestedReviewRevision);
        this.hasUserAcceptedReviewRequest = getPrefsSessionDurable().getBoolean("CACHE_KEY_HAS_ACCEPTED_REVIEW_REQUEST", this.hasUserAcceptedReviewRequest);
    }

    public final void onReviewRequestShown() {
        this.requestedReviewRevision = REVIEW_REQUEST_REVISION;
        SharedPreferences.Editor edit = getPrefsSessionDurable().edit();
        m.checkNotNullExpressionValue(edit, "editor");
        edit.putInt("CACHE_KEY_VIEWED_REVIEW_REQUEST_REVISION", REVIEW_REQUEST_REVISION);
        edit.apply();
        this.stream.getNotices$app_productionGoogleRelease().markSeen(StoreNotices.Dialog.Type.REQUEST_RATING_MODAL);
        AnalyticsTracker.INSTANCE.reviewRequestTriggered();
    }

    public final void onUserAcceptedRequest() {
        this.hasUserAcceptedReviewRequest = true;
        SharedPreferences.Editor edit = getPrefsSessionDurable().edit();
        m.checkNotNullExpressionValue(edit, "editor");
        edit.putBoolean("CACHE_KEY_HAS_ACCEPTED_REVIEW_REQUEST", true);
        edit.apply();
        AnalyticsTracker.INSTANCE.reviewRequestAccepted();
    }

    public final void onUserDismissedRequest() {
        AnalyticsTracker.INSTANCE.reviewRequestDismissed();
    }
}
