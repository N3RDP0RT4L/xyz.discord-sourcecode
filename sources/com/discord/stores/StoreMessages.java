package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import androidx.appcompat.widget.ActivityChooserModel;
import androidx.work.Operation;
import androidx.work.WorkManager;
import com.discord.api.activity.Activity;
import com.discord.api.application.Application;
import com.discord.api.interaction.InteractionStateUpdate;
import com.discord.api.message.LocalAttachment;
import com.discord.api.message.MessageReference;
import com.discord.api.message.activity.MessageActivity;
import com.discord.api.message.allowedmentions.MessageAllowedMentions;
import com.discord.api.message.reaction.MessageReactionUpdate;
import com.discord.api.sticker.BaseSticker;
import com.discord.api.user.User;
import com.discord.app.AppLog;
import com.discord.models.commands.ApplicationCommandLocalSendData;
import com.discord.models.domain.ModelMessageDelete;
import com.discord.models.domain.ModelPayload;
import com.discord.models.message.Message;
import com.discord.models.user.CoreUser;
import com.discord.stores.StoreMessagesLoader;
import com.discord.utilities.attachments.AttachmentUtilsKt;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.message.LocalMessageCreatorsKt;
import com.discord.utilities.messagesend.MessageQueue;
import com.discord.utilities.messagesend.MessageRequest;
import com.discord.utilities.messagesend.MessageResult;
import com.discord.utilities.rest.ProcessedMessageContent;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$3;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$4;
import com.discord.utilities.time.Clock;
import com.discord.utilities.user.UserUtils;
import com.lytefast.flexinput.model.Attachment;
import d0.t.n;
import d0.t.o;
import d0.z.d.m;
import j0.k.b;
import j0.l.a.q;
import j0.l.a.r;
import j0.l.a.x0;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Ref$ObjectRef;
import rx.Emitter;
import rx.Observable;
import rx.functions.Action1;
import rx.subjects.BehaviorSubject;
/* compiled from: StoreMessages.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000®\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 ®\u00012\u00020\u0001:\u0002®\u0001B'\u0012\b\u0010\u0097\u0001\u001a\u00030\u0096\u0001\u0012\b\u0010\u009d\u0001\u001a\u00030\u009c\u0001\u0012\b\u0010 \u0001\u001a\u00030\u009f\u0001¢\u0006\u0006\b¬\u0001\u0010\u00ad\u0001J+\u0010\t\u001a\u0012\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0007j\u0002`\b0\u00060\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0002¢\u0006\u0004\b\t\u0010\nJ+\u0010\u000b\u001a\u0012\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0007j\u0002`\b0\u00060\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0002¢\u0006\u0004\b\u000b\u0010\nJ;\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0018\u0010\u0010\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r\u0012\u0004\u0012\u00020\u000f0\fH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J'\u0010\u0017\u001a\u00020\u00162\u000e\u0010\u0013\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\b2\u0006\u0010\u0015\u001a\u00020\u0014H\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u000f\u0010\u0019\u001a\u00020\u0016H\u0003¢\u0006\u0004\b\u0019\u0010\u001aJ\u000f\u0010\u001b\u001a\u00020\u0016H\u0003¢\u0006\u0004\b\u001b\u0010\u001aJ\u001b\u0010\u001d\u001a\u00020\u00162\n\u0010\u001c\u001a\u00060\u0007j\u0002`\bH\u0003¢\u0006\u0004\b\u001d\u0010\u001eJ-\u0010!\u001a\u00020\u00162\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0010\u0010 \u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u001f0\u0006H\u0002¢\u0006\u0004\b!\u0010\"J\u001b\u0010#\u001a\u00020\u00162\n\u0010\u0013\u001a\u00060\u0007j\u0002`\bH\u0003¢\u0006\u0004\b#\u0010\u001eJ#\u0010#\u001a\u00020\u00162\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010$\u001a\u00020\u0014H\u0003¢\u0006\u0004\b#\u0010%J+\u0010+\u001a\u00020\u00162\u0006\u0010'\u001a\u00020&2\b\b\u0002\u0010)\u001a\u00020(2\b\b\u0002\u0010*\u001a\u00020(H\u0003¢\u0006\u0004\b+\u0010,J\u001b\u0010-\u001a\u00020\u00162\n\u0010\u0013\u001a\u00060\u0007j\u0002`\bH\u0003¢\u0006\u0004\b-\u0010\u001eJ\u001b\u0010.\u001a\u00020\u00162\n\u0010\u0013\u001a\u00060\u0007j\u0002`\bH\u0003¢\u0006\u0004\b.\u0010\u001eJ\u001b\u00100\u001a\u00020/2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0003¢\u0006\u0004\b0\u00101J#\u00104\u001a\u00020\u00162\n\u0010\u0013\u001a\u00060\u0007j\u0002`\b2\u0006\u00103\u001a\u000202H\u0002¢\u0006\u0004\b4\u00105J\u0013\u00107\u001a\b\u0012\u0004\u0012\u00020(06¢\u0006\u0004\b7\u00108J)\u00109\u001a\u0012\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0007j\u0002`\b0\u00060\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b9\u0010\nJ\u001f\u0010:\u001a\b\u0012\u0004\u0012\u00020(0\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b:\u0010\nJ3\u00109\u001a\u0010\u0012\f\u0012\n\u0018\u00010\u0007j\u0004\u0018\u0001`\b0\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010;\u001a\u00060\u0002j\u0002`\u001f¢\u0006\u0004\b9\u0010<J)\u0010=\u001a\u0004\u0018\u00010\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010;\u001a\u00060\u0002j\u0002`\u001fH\u0007¢\u0006\u0004\b=\u0010>JÑ\u0001\u0010V\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010@\u001a\u00020?2\u0006\u0010A\u001a\u00020\u00142\u000e\u0010B\u001a\n\u0012\u0004\u0012\u00020?\u0018\u00010\u00062\u0012\u0010D\u001a\u000e\u0012\b\u0012\u0006\u0012\u0002\b\u00030C\u0018\u00010\u00062\u0010\b\u0002\u0010F\u001a\n\u0012\u0004\u0012\u00020E\u0018\u00010\u00062\n\b\u0002\u0010H\u001a\u0004\u0018\u00010G2\n\b\u0002\u0010J\u001a\u0004\u0018\u00010I2\n\b\u0002\u0010L\u001a\u0004\u0018\u00010K2\n\b\u0002\u0010N\u001a\u0004\u0018\u00010M2\n\b\u0002\u0010P\u001a\u0004\u0018\u00010O2\n\b\u0002\u0010Q\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010R\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010T\u001a\u0004\u0018\u00010S2\n\b\u0002\u0010U\u001a\u0004\u0018\u00010\u0014¢\u0006\u0004\bV\u0010WJ=\u0010V\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00052\n\u0010\u001c\u001a\u00060\u0007j\u0002`\b2\u0006\u0010Y\u001a\u00020X2\u0014\b\u0002\u0010D\u001a\u000e\u0012\b\u0012\u0006\u0012\u0002\b\u00030C\u0018\u00010\u0006¢\u0006\u0004\bV\u0010ZJ+\u0010[\u001a\u00020\u00162\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\b\u0010$\u001a\u0004\u0018\u00010\u00142\u0006\u0010U\u001a\u00020\u0014¢\u0006\u0004\b[\u0010\\J#\u0010]\u001a\u00020\u00162\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\b\u0010$\u001a\u0004\u0018\u00010\u0014¢\u0006\u0004\b]\u0010%J\u001d\u0010^\u001a\u00020\u00162\u000e\u0010\u001c\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\b¢\u0006\u0004\b^\u0010\u001eJ#\u0010_\u001a\u00020\u00162\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010$\u001a\u00020\u0014H\u0007¢\u0006\u0004\b_\u0010%J7\u0010`\u001a\u00020\u00162\n\u0010;\u001a\u00060\u0002j\u0002`\u001f2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010A\u001a\u00020\u00142\b\u0010J\u001a\u0004\u0018\u00010I¢\u0006\u0004\b`\u0010aJ5\u0010c\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00052\n\u0010\u001c\u001a\u00060\u0007j\u0002`\b2\b\b\u0002\u0010b\u001a\u00020(2\n\b\u0002\u0010U\u001a\u0004\u0018\u00010\u0014¢\u0006\u0004\bc\u0010dJ!\u0010f\u001a\u00020\u00162\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010e\u001a\u00020\u0014¢\u0006\u0004\bf\u0010%J\u0017\u0010i\u001a\u00020\u00162\u0006\u0010h\u001a\u00020gH\u0016¢\u0006\u0004\bi\u0010jJ\u0017\u0010l\u001a\u00020\u00162\u0006\u0010k\u001a\u00020(H\u0007¢\u0006\u0004\bl\u0010mJ\u0015\u0010p\u001a\u00020\u00162\u0006\u0010o\u001a\u00020n¢\u0006\u0004\bp\u0010qJ\u0019\u0010r\u001a\u00020\u00162\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\br\u0010sJ\u001f\u0010w\u001a\u00020\u00162\u0010\u0010v\u001a\f\u0012\b\u0012\u00060tj\u0002`u0\u0006¢\u0006\u0004\bw\u0010xJ\r\u0010y\u001a\u00020\u0016¢\u0006\u0004\by\u0010\u001aJ\u0019\u0010z\u001a\u00020\u00162\n\u0010\u001c\u001a\u00060tj\u0002`u¢\u0006\u0004\bz\u0010{J\u0015\u0010!\u001a\u00020\u00162\u0006\u0010}\u001a\u00020|¢\u0006\u0004\b!\u0010~J\u0019\u0010\u0081\u0001\u001a\u00020\u00162\u0007\u0010\u0080\u0001\u001a\u00020\u007f¢\u0006\u0006\b\u0081\u0001\u0010\u0082\u0001J)\u0010\u0086\u0001\u001a\u00020\u00162\u000e\u0010\u0084\u0001\u001a\t\u0012\u0005\u0012\u00030\u0083\u00010\u00062\u0007\u0010\u0085\u0001\u001a\u00020(¢\u0006\u0006\b\u0086\u0001\u0010\u0087\u0001J\u001a\u0010\u0089\u0001\u001a\u00020\u00162\b\u0010\u0088\u0001\u001a\u00030\u0083\u0001¢\u0006\u0006\b\u0089\u0001\u0010\u008a\u0001J\u001a\u0010\u008b\u0001\u001a\u00020\u00162\b\u0010\u0088\u0001\u001a\u00030\u0083\u0001¢\u0006\u0006\b\u008b\u0001\u0010\u008a\u0001J\u001a\u0010\u008c\u0001\u001a\u00020\u00162\u0006\u0010'\u001a\u00020&H\u0007¢\u0006\u0006\b\u008c\u0001\u0010\u008d\u0001J\u001a\u0010\u008e\u0001\u001a\u00020\u00162\u0006\u0010'\u001a\u00020&H\u0007¢\u0006\u0006\b\u008e\u0001\u0010\u008d\u0001R\u001a\u0010\u0090\u0001\u001a\u00030\u008f\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0090\u0001\u0010\u0091\u0001R\u0017\u0010h\u001a\u00020g8\u0002@\u0002X\u0082.¢\u0006\u0007\n\u0005\bh\u0010\u0092\u0001R\u001a\u0010\u0094\u0001\u001a\u00030\u0093\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0094\u0001\u0010\u0095\u0001R\u001a\u0010\u0097\u0001\u001a\u00030\u0096\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0097\u0001\u0010\u0098\u0001R*\u0010\u009a\u0001\u001a\u0013\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020/0\u0099\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u009a\u0001\u0010\u009b\u0001R\u001a\u0010\u009d\u0001\u001a\u00030\u009c\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u009d\u0001\u0010\u009e\u0001R\u001a\u0010 \u0001\u001a\u00030\u009f\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b \u0001\u0010¡\u0001R'\u0010¥\u0001\u001a\u0013\u0012\u000f\u0012\r\u0012\b\u0012\u00060\u0002j\u0002`\u00030¢\u00010\u00058F@\u0006¢\u0006\b\u001a\u0006\b£\u0001\u0010¤\u0001R$\u0010¨\u0001\u001a\r §\u0001*\u0005\u0018\u00010¦\u00010¦\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b¨\u0001\u0010©\u0001R@\u0010ª\u0001\u001a)\u0012\r\u0012\u000b §\u0001*\u0004\u0018\u00010(0( §\u0001*\u0013\u0012\r\u0012\u000b §\u0001*\u0004\u0018\u00010(0(\u0018\u000106068\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bª\u0001\u0010«\u0001¨\u0006¯\u0001"}, d2 = {"Lcom/discord/stores/StoreMessages;", "Lcom/discord/stores/Store;", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lrx/Observable;", "", "Lcom/discord/models/message/Message;", "Lcom/discord/stores/ClientMessage;", "observeSyncedMessagesForChannel", "(J)Lrx/Observable;", "observeLocalMessagesForChannel", "Lkotlin/Function1;", "Lrx/Emitter;", "Lcom/discord/utilities/messagesend/MessageResult;", "Lcom/discord/utilities/messagesend/MessageRequest;", "createRequest", "enqueueRequest", "(JLkotlin/jvm/functions/Function1;)Lrx/Observable;", "localMessage", "", "errorMessage", "", "handleSendMessageValidationError", "(Lcom/discord/models/message/Message;Ljava/lang/String;)V", "resendAllLocalMessages", "()V", "markLocalCaptchaRequiredMessagesFailed", "message", "handleLocalMessageCreate", "(Lcom/discord/models/message/Message;)V", "Lcom/discord/primitives/MessageId;", "messageIds", "handleMessageDelete", "(JLjava/util/List;)V", "handleLocalMessageDelete", "nonce", "(JLjava/lang/String;)V", "Lcom/discord/api/interaction/InteractionStateUpdate;", "interactionUpdate", "", "isFailed", "isLoading", "handleInteractionStateUpdate", "(Lcom/discord/api/interaction/InteractionStateUpdate;ZZ)V", "handleSendMessageFailure", "handleSendMessageCaptchaRequired", "Lcom/discord/utilities/messagesend/MessageQueue;", "getOrCreateMessageQueue", "(J)Lcom/discord/utilities/messagesend/MessageQueue;", "Lcom/discord/stores/FailedMessageResolutionType;", "failedMessageResolutionType", "trackFailedLocalMessageResolved", "(Lcom/discord/models/message/Message;Lcom/discord/stores/FailedMessageResolutionType;)V", "Lrx/subjects/BehaviorSubject;", "observeInitResendFinished", "()Lrx/subjects/BehaviorSubject;", "observeMessagesForChannel", "observeIsDetached", "messageId", "(JJ)Lrx/Observable;", "getMessage", "(JJ)Lcom/discord/models/message/Message;", "Lcom/discord/models/user/User;", "author", "content", "mentions", "Lcom/lytefast/flexinput/model/Attachment;", "attachments", "Lcom/discord/api/sticker/BaseSticker;", "stickers", "Lcom/discord/api/message/MessageReference;", "messageReference", "Lcom/discord/api/message/allowedmentions/MessageAllowedMentions;", "allowedMentions", "Lcom/discord/api/application/Application;", "application", "Lcom/discord/api/activity/Activity;", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "Lcom/discord/api/message/activity/MessageActivity;", "messageActivity", "lastManualAttemptTimestamp", "initialAttemptTimestamp", "", "numRetries", "captchaKey", "sendMessage", "(JLcom/discord/models/user/User;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/discord/api/message/MessageReference;Lcom/discord/api/message/allowedmentions/MessageAllowedMentions;Lcom/discord/api/application/Application;Lcom/discord/api/activity/Activity;Lcom/discord/api/message/activity/MessageActivity;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/String;)Lrx/Observable;", "Lcom/discord/models/commands/ApplicationCommandLocalSendData;", "applicationCommandLocalSendData", "(Lcom/discord/models/message/Message;Lcom/discord/models/commands/ApplicationCommandLocalSendData;Ljava/util/List;)Lrx/Observable;", "resendMessageWithCaptcha", "(JLjava/lang/String;Ljava/lang/String;)V", "markMessageFailed", "deleteMessage", "deleteLocalMessage", "editMessage", "(JJLjava/lang/String;Lcom/discord/api/message/allowedmentions/MessageAllowedMentions;)V", "isAutoAttempt", "resendMessage", "(Lcom/discord/models/message/Message;ZLjava/lang/String;)Lrx/Observable;", "requestId", "cancelMessageSend", "Landroid/content/Context;", "context", "init", "(Landroid/content/Context;)V", "connected", "handleConnected", "(Z)V", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "handleChannelSelected", "(J)V", "Lcom/discord/api/message/Message;", "Lcom/discord/stores/ApiMessage;", "messagesList", "handleMessageCreate", "(Ljava/util/List;)V", "handlePreLogout", "handleMessageUpdate", "(Lcom/discord/api/message/Message;)V", "Lcom/discord/models/domain/ModelMessageDelete;", "messageDelete", "(Lcom/discord/models/domain/ModelMessageDelete;)V", "Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;", "chunk", "handleMessagesLoaded", "(Lcom/discord/stores/StoreMessagesLoader$ChannelChunk;)V", "Lcom/discord/api/message/reaction/MessageReactionUpdate;", "updates", "add", "handleReactionUpdate", "(Ljava/util/List;Z)V", "update", "handleReactionsRemoveEmoji", "(Lcom/discord/api/message/reaction/MessageReactionUpdate;)V", "handleReactionsRemoveAll", "handleInteractionCreate", "(Lcom/discord/api/interaction/InteractionStateUpdate;)V", "handleInteractionFailure", "Lcom/discord/stores/StoreMessagesHolder;", "holder", "Lcom/discord/stores/StoreMessagesHolder;", "Landroid/content/Context;", "Lcom/discord/stores/StoreLocalMessagesHolder;", "localMessagesHolder", "Lcom/discord/stores/StoreLocalMessagesHolder;", "Lcom/discord/stores/StoreStream;", "stream", "Lcom/discord/stores/StoreStream;", "Ljava/util/HashMap;", "messageQueues", "Ljava/util/HashMap;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "", "getAllDetached", "()Lrx/Observable;", "allDetached", "Ljava/util/concurrent/ExecutorService;", "kotlin.jvm.PlatformType", "queueExecutor", "Ljava/util/concurrent/ExecutorService;", "initResendFinished", "Lrx/subjects/BehaviorSubject;", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessages extends Store {
    private static final long BACKGROUND_SENDING_DELAY_MS = 120000;
    public static final Companion Companion = new Companion(null);
    private final Clock clock;
    private Context context;
    private final Dispatcher dispatcher;
    private final StoreStream stream;
    private final StoreMessagesHolder holder = new StoreMessagesHolder();
    private final StoreLocalMessagesHolder localMessagesHolder = new StoreLocalMessagesHolder();
    private final ExecutorService queueExecutor = Executors.newSingleThreadExecutor();
    private final HashMap<Long, MessageQueue> messageQueues = new HashMap<>();
    private final BehaviorSubject<Boolean> initResendFinished = BehaviorSubject.l0(Boolean.FALSE);

    /* compiled from: StoreMessages.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/stores/StoreMessages$Companion;", "", "Landroid/content/Context;", "context", "Landroidx/work/Operation;", "cancelBackgroundSendingWork", "(Landroid/content/Context;)Landroidx/work/Operation;", "", "BACKGROUND_SENDING_DELAY_MS", "J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Operation cancelBackgroundSendingWork(Context context) {
            Operation cancelUniqueWork = WorkManager.getInstance(context).cancelUniqueWork("BACKGROUND_MESSAGE_SENDING");
            m.checkNotNullExpressionValue(cancelUniqueWork, "WorkManager.getInstance(…dWorker.UNIQUE_WORK_NAME)");
            return cancelUniqueWork;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public StoreMessages(StoreStream storeStream, Dispatcher dispatcher, Clock clock) {
        m.checkNotNullParameter(storeStream, "stream");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(clock, "clock");
        this.stream = storeStream;
        this.dispatcher = dispatcher;
        this.clock = clock;
    }

    public static final /* synthetic */ Context access$getContext$p(StoreMessages storeMessages) {
        Context context = storeMessages.context;
        if (context == null) {
            m.throwUninitializedPropertyAccessException("context");
        }
        return context;
    }

    private final Observable<MessageResult> enqueueRequest(final long j, final Function1<? super Emitter<MessageResult>, ? extends MessageRequest> function1) {
        Observable<MessageResult> X = Observable.n(new Action1<Emitter<MessageResult>>() { // from class: com.discord.stores.StoreMessages$enqueueRequest$1
            public final void call(Emitter<MessageResult> emitter) {
                MessageQueue orCreateMessageQueue;
                orCreateMessageQueue = StoreMessages.this.getOrCreateMessageQueue(j);
                Function1 function12 = function1;
                m.checkNotNullExpressionValue(emitter, "emitter");
                orCreateMessageQueue.enqueue((MessageRequest) function12.invoke(emitter));
            }
        }, Emitter.BackpressureMode.ERROR).X(this.dispatcher.getScheduler());
        m.checkNotNullExpressionValue(X, "Observable.create<Messag…eOn(dispatcher.scheduler)");
        return X;
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final MessageQueue getOrCreateMessageQueue(long j) {
        MessageQueue messageQueue = this.messageQueues.get(Long.valueOf(j));
        if (messageQueue == null) {
            Context context = this.context;
            if (context == null) {
                m.throwUninitializedPropertyAccessException("context");
            }
            ContentResolver contentResolver = context.getContentResolver();
            m.checkNotNullExpressionValue(contentResolver, "context.contentResolver");
            ExecutorService executorService = this.queueExecutor;
            m.checkNotNullExpressionValue(executorService, "queueExecutor");
            messageQueue = new MessageQueue(contentResolver, executorService, this.clock);
            this.messageQueues.put(Long.valueOf(j), messageQueue);
        }
        return messageQueue;
    }

    @StoreThread
    private final void handleInteractionStateUpdate(InteractionStateUpdate interactionStateUpdate, boolean z2, boolean z3) {
        Message message;
        ApplicationCommandLocalSendData applicationCommandLocalSendData = this.stream.getApplicationInteractions$app_productionGoogleRelease().getApplicationCommandLocalSendDataSet$app_productionGoogleRelease().get(interactionStateUpdate.b());
        if (applicationCommandLocalSendData != null && (message = this.localMessagesHolder.getMessage(applicationCommandLocalSendData.getChannelId(), applicationCommandLocalSendData.getNonceString())) != null) {
            this.localMessagesHolder.addMessage(LocalMessageCreatorsKt.createLocalApplicationCommandMessage(message, interactionStateUpdate.a(), z2, z3, this.clock));
        }
    }

    public static /* synthetic */ void handleInteractionStateUpdate$default(StoreMessages storeMessages, InteractionStateUpdate interactionStateUpdate, boolean z2, boolean z3, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        if ((i & 4) != 0) {
            z3 = false;
        }
        storeMessages.handleInteractionStateUpdate(interactionStateUpdate, z2, z3);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleLocalMessageCreate(Message message) {
        this.localMessagesHolder.addMessage(message);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleLocalMessageDelete(Message message) {
        this.localMessagesHolder.deleteMessage(message);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleSendMessageCaptchaRequired(Message message) {
        this.localMessagesHolder.addMessage(Message.copy$default(message, 0L, 0L, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, -6, null, null, null, null, null, null, null, null, null, null, null, null, null, false, null, null, null, null, null, null, -131073, 63, null));
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleSendMessageFailure(Message message) {
        deleteMessage(message);
        String content = message.getContent();
        if (content == null) {
            content = "";
        }
        String str = content;
        long channelId = message.getChannelId();
        User author = message.getAuthor();
        m.checkNotNull(author);
        handleLocalMessageCreate(LocalMessageCreatorsKt.createLocalMessage$default(str, channelId, author, message.getMentions(), true, message.getHasLocalUploads(), message.getApplication(), message.getActivity(), this.clock, message.getLocalAttachments(), message.getLastManualAttemptTimestamp(), message.getInitialAttemptTimestamp(), message.getNumRetries(), message.getStickers(), message.getMessageReference(), message.getAllowedMentions(), null, 65536, null));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleSendMessageValidationError(Message message, String str) {
        Logger.e$default(AppLog.g, "ValidationError", str, null, null, 12, null);
        deleteMessage(message);
    }

    @StoreThread
    private final void markLocalCaptchaRequiredMessagesFailed() {
        List<Message> flattenedMessages = this.localMessagesHolder.getFlattenedMessages();
        ArrayList<Message> arrayList = new ArrayList();
        for (Object obj : flattenedMessages) {
            Integer type = ((Message) obj).getType();
            if (type != null && type.intValue() == -6) {
                arrayList.add(obj);
            }
        }
        for (Message message : arrayList) {
            markMessageFailed(message.getChannelId(), message.getNonce());
        }
    }

    private final Observable<List<Message>> observeLocalMessagesForChannel(final long j) {
        Observable<R> F = this.localMessagesHolder.getMessagesPublisher().F(new b<Map<Long, ? extends List<? extends Message>>, List<? extends Message>>() { // from class: com.discord.stores.StoreMessages$observeLocalMessagesForChannel$1
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ List<? extends Message> call(Map<Long, ? extends List<? extends Message>> map) {
                return call2((Map<Long, ? extends List<Message>>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final List<Message> call2(Map<Long, ? extends List<Message>> map) {
                List<Message> list = map.get(Long.valueOf(j));
                return list != null ? list : n.emptyList();
            }
        });
        m.checkNotNullExpressionValue(F, "localMessagesHolder\n    …annelId] ?: emptyList() }");
        Observable<List<Message>> r = ObservableExtensionsKt.computationBuffered(F).r(StoreMessages$observeLocalMessagesForChannel$2.INSTANCE);
        m.checkNotNullExpressionValue(r, "localMessagesHolder\n    …messages1 === messages2 }");
        return r;
    }

    private final Observable<List<Message>> observeSyncedMessagesForChannel(final long j) {
        Observable<R> F = this.holder.getMessagesPublisher().F(new b<Map<Long, List<Message>>, List<? extends Message>>() { // from class: com.discord.stores.StoreMessages$observeSyncedMessagesForChannel$1
            public final List<Message> call(Map<Long, List<Message>> map) {
                List<Message> list = map.get(Long.valueOf(j));
                return list != null ? list : n.emptyList();
            }
        });
        m.checkNotNullExpressionValue(F, "holder\n          .messag…annelId] ?: emptyList() }");
        Observable<List<Message>> r = ObservableExtensionsKt.computationBuffered(F).r(StoreMessages$observeSyncedMessagesForChannel$2.INSTANCE);
        m.checkNotNullExpressionValue(r, "holder\n          .messag…messages1 === messages2 }");
        return r;
    }

    @StoreThread
    private final void resendAllLocalMessages() {
        List<Message> flattenedMessages = this.localMessagesHolder.getFlattenedMessages();
        ArrayList<Message> arrayList = new ArrayList();
        for (Object obj : flattenedMessages) {
            Integer type = ((Message) obj).getType();
            if (type != null && type.intValue() == -1) {
                arrayList.add(obj);
            }
        }
        ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(arrayList, 10));
        for (Message message : arrayList) {
            arrayList2.add(resendMessage$default(this, message, true, null, 4, null));
        }
        Observable h02 = Observable.h0(new r(Observable.h0(new q(arrayList2)).j, x0.a.a));
        m.checkNotNullExpressionValue(h02, "Observable\n        .mergeDelayError(observables)");
        ObservableExtensionsKt.appSubscribe(h02, StoreMessages.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : new StoreMessages$resendAllLocalMessages$1(this), (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, StoreMessages$resendAllLocalMessages$2.INSTANCE);
    }

    public static /* synthetic */ Observable resendMessage$default(StoreMessages storeMessages, Message message, boolean z2, String str, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        if ((i & 4) != 0) {
            str = null;
        }
        return storeMessages.resendMessage(message, z2, str);
    }

    public static /* synthetic */ Observable sendMessage$default(StoreMessages storeMessages, long j, com.discord.models.user.User user, String str, List list, List list2, List list3, MessageReference messageReference, MessageAllowedMentions messageAllowedMentions, Application application, Activity activity, MessageActivity messageActivity, Long l, Long l2, Integer num, String str2, int i, Object obj) {
        return storeMessages.sendMessage(j, user, str, list, list2, (i & 32) != 0 ? null : list3, (i & 64) != 0 ? null : messageReference, (i & 128) != 0 ? null : messageAllowedMentions, (i & 256) != 0 ? null : application, (i & 512) != 0 ? null : activity, (i & 1024) != 0 ? null : messageActivity, (i & 2048) != 0 ? null : l, (i & 4096) != 0 ? null : l2, (i & 8192) != 0 ? null : num, (i & 16384) != 0 ? null : str2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Removed duplicated region for block: B:21:0x0046  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x007e  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x00c0  */
    /* JADX WARN: Removed duplicated region for block: B:47:0x00c3  */
    /* JADX WARN: Removed duplicated region for block: B:52:0x00d5  */
    /* JADX WARN: Removed duplicated region for block: B:53:0x00dc  */
    /* JADX WARN: Removed duplicated region for block: B:56:0x00e9  */
    /* JADX WARN: Removed duplicated region for block: B:57:0x00ef  */
    /* JADX WARN: Removed duplicated region for block: B:60:0x00f6  */
    /* JADX WARN: Removed duplicated region for block: B:61:0x00fb  */
    /* JADX WARN: Removed duplicated region for block: B:64:0x0104  */
    /* JADX WARN: Removed duplicated region for block: B:65:0x010a  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void trackFailedLocalMessageResolved(com.discord.models.message.Message r19, com.discord.stores.FailedMessageResolutionType r20) {
        /*
            Method dump skipped, instructions count: 277
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreMessages.trackFailedLocalMessageResolved(com.discord.models.message.Message, com.discord.stores.FailedMessageResolutionType):void");
    }

    public final void cancelMessageSend(long j, String str) {
        m.checkNotNullParameter(str, "requestId");
        getOrCreateMessageQueue(j).cancel(str);
    }

    @StoreThread
    public final void deleteLocalMessage(long j, String str) {
        m.checkNotNullParameter(str, "nonce");
        getOrCreateMessageQueue(j).cancel(str);
        handleLocalMessageDelete(j, str);
    }

    public final void deleteMessage(Message message) {
        if (message != null) {
            long id2 = message.getId();
            long channelId = message.getChannelId();
            if (message.isLocal() || message.isEphemeralMessage()) {
                this.dispatcher.schedule(new StoreMessages$deleteMessage$2(this, message, channelId, id2));
            } else {
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().deleteMessage(channelId, id2), false, 1, null), (r18 & 1) != 0 ? null : null, "deleteMessage", (r18 & 4) != 0 ? null : null, StoreMessages$deleteMessage$1.INSTANCE, (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
            }
        }
    }

    public final void editMessage(long j, long j2, String str, MessageAllowedMentions messageAllowedMentions) {
        m.checkNotNullParameter(str, "content");
        getOrCreateMessageQueue(j2).enqueue(new MessageRequest.Edit(j2, str, j, messageAllowedMentions, this.clock.currentTimeMillis()));
    }

    public final Observable<Set<Long>> getAllDetached() {
        Observable<Set<Long>> detachedChannelsSubject = this.holder.getDetachedChannelsSubject();
        m.checkNotNullExpressionValue(detachedChannelsSubject, "holder\n          .detachedChannelsSubject");
        return ObservableExtensionsKt.computationLatest(detachedChannelsSubject);
    }

    @StoreThread
    public final Message getMessage(long j, long j2) {
        TreeMap<Long, Message> messagesForChannel = this.holder.getMessagesForChannel(Long.valueOf(j));
        if (messagesForChannel != null) {
            return messagesForChannel.get(Long.valueOf(j2));
        }
        return null;
    }

    public final void handleChannelSelected(long j) {
        this.holder.setSelectedChannelId(j);
    }

    @StoreThread
    public final void handleConnected(boolean z2) {
        if (!z2) {
            this.holder.invalidate();
            return;
        }
        Collection<MessageQueue> values = this.messageQueues.values();
        m.checkNotNullExpressionValue(values, "messageQueues.values");
        for (MessageQueue messageQueue : values) {
            messageQueue.handleConnected();
        }
    }

    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        this.holder.setMyUserId(modelPayload.getMe().i());
    }

    @StoreThread
    public final void handleInteractionCreate(InteractionStateUpdate interactionStateUpdate) {
        m.checkNotNullParameter(interactionStateUpdate, "interactionUpdate");
        handleInteractionStateUpdate$default(this, interactionStateUpdate, false, true, 2, null);
    }

    @StoreThread
    public final void handleInteractionFailure(InteractionStateUpdate interactionStateUpdate) {
        m.checkNotNullParameter(interactionStateUpdate, "interactionUpdate");
        handleInteractionStateUpdate$default(this, interactionStateUpdate, true, false, 4, null);
    }

    public final void handleMessageCreate(List<com.discord.api.message.Message> list) {
        m.checkNotNullParameter(list, "messagesList");
        for (com.discord.api.message.Message message : list) {
            String v = message.v();
            if (v != null) {
                this.localMessagesHolder.deleteMessage(message.g(), v);
            }
        }
        StoreMessagesHolder storeMessagesHolder = this.holder;
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
        for (com.discord.api.message.Message message2 : list) {
            arrayList.add(new Message(message2));
        }
        storeMessagesHolder.addMessages(arrayList);
    }

    public final void handleMessageDelete(ModelMessageDelete modelMessageDelete) {
        m.checkNotNullParameter(modelMessageDelete, "messageDelete");
        long channelId = modelMessageDelete.getChannelId();
        List<Long> messageIds = modelMessageDelete.getMessageIds();
        m.checkNotNullExpressionValue(messageIds, "messageDelete.messageIds");
        handleMessageDelete(channelId, messageIds);
    }

    public final void handleMessageUpdate(com.discord.api.message.Message message) {
        m.checkNotNullParameter(message, "message");
        this.holder.updateMessages(message);
    }

    public final void handleMessagesLoaded(StoreMessagesLoader.ChannelChunk channelChunk) {
        m.checkNotNullParameter(channelChunk, "chunk");
        this.holder.loadMessageChunks(d0.t.m.listOf(channelChunk));
    }

    public final void handlePreLogout() {
        this.localMessagesHolder.clearCache();
    }

    public final void handleReactionUpdate(List<MessageReactionUpdate> list, boolean z2) {
        m.checkNotNullParameter(list, "updates");
        this.holder.updateReactions(list, z2);
    }

    public final void handleReactionsRemoveAll(MessageReactionUpdate messageReactionUpdate) {
        m.checkNotNullParameter(messageReactionUpdate, "update");
        this.holder.removeAllReactions(messageReactionUpdate);
    }

    public final void handleReactionsRemoveEmoji(MessageReactionUpdate messageReactionUpdate) {
        m.checkNotNullParameter(messageReactionUpdate, "update");
        this.holder.removeEmojiReactions(messageReactionUpdate);
    }

    @Override // com.discord.stores.Store
    public void init(Context context) {
        m.checkNotNullParameter(context, "context");
        super.init(context);
        this.context = context;
        this.holder.init(true);
        StoreLocalMessagesHolder.init$default(this.localMessagesHolder, false, 1, null);
        markLocalCaptchaRequiredMessagesFailed();
        resendAllLocalMessages();
    }

    public final void markMessageFailed(long j, String str) {
        Message message;
        if (str != null && (message = this.localMessagesHolder.getMessage(j, str)) != null) {
            this.dispatcher.schedule(new StoreMessages$markMessageFailed$1(this, message));
        }
    }

    public final BehaviorSubject<Boolean> observeInitResendFinished() {
        BehaviorSubject<Boolean> behaviorSubject = this.initResendFinished;
        m.checkNotNullExpressionValue(behaviorSubject, "initResendFinished");
        return behaviorSubject;
    }

    public final Observable<Boolean> observeIsDetached(final long j) {
        Observable<Boolean> q = getAllDetached().F(new b<Set<? extends Long>, Boolean>() { // from class: com.discord.stores.StoreMessages$observeIsDetached$1
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Boolean call(Set<? extends Long> set) {
                return call2((Set<Long>) set);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Boolean call2(Set<Long> set) {
                return Boolean.valueOf(set.contains(Long.valueOf(j)));
            }
        }).q();
        m.checkNotNullExpressionValue(q, "allDetached\n          .m…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<List<Message>> observeMessagesForChannel(long j) {
        Observable<List<Message>> i = Observable.i(observeSyncedMessagesForChannel(j), observeLocalMessagesForChannel(j), observeIsDetached(j), StoreMessages$observeMessagesForChannel$1.INSTANCE);
        m.checkNotNullExpressionValue(i, "Observable.combineLatest…ges + localMessages\n    }");
        return i;
    }

    public final Observable<MessageResult> resendMessage(Message message, boolean z2, String str) {
        ArrayList arrayList;
        ArrayList arrayList2;
        Integer type;
        Integer type2;
        Integer type3;
        m.checkNotNullParameter(message, "message");
        if ((!z2 || (((type2 = message.getType()) != null && type2.intValue() == -1) || ((type3 = message.getType()) != null && type3.intValue() == -6))) && (z2 || ((type = message.getType()) != null && type.intValue() == -2))) {
            this.dispatcher.schedule(new StoreMessages$resendMessage$1(this, message));
            Integer numRetries = message.getNumRetries();
            int intValue = numRetries != null ? numRetries.intValue() : 0;
            long channelId = message.getChannelId();
            User author = message.getAuthor();
            m.checkNotNull(author);
            CoreUser coreUser = new CoreUser(author);
            String content = message.getContent();
            if (content == null) {
                content = "";
            }
            List<User> mentions = message.getMentions();
            if (mentions != null) {
                arrayList = new ArrayList(o.collectionSizeOrDefault(mentions, 10));
                for (User user : mentions) {
                    arrayList.add(new CoreUser(user));
                }
            } else {
                arrayList = null;
            }
            List<LocalAttachment> localAttachments = message.getLocalAttachments();
            if (localAttachments != null) {
                arrayList2 = new ArrayList(o.collectionSizeOrDefault(localAttachments, 10));
                for (LocalAttachment localAttachment : localAttachments) {
                    Uri parse = Uri.parse(localAttachment.c());
                    long b2 = localAttachment.b();
                    m.checkNotNullExpressionValue(parse, "contentUri");
                    arrayList2.add(new Attachment(b2, parse, localAttachment.a(), null, false, 16, null));
                }
            } else {
                arrayList2 = null;
            }
            return sendMessage$default(this, channelId, coreUser, content, arrayList, arrayList2, null, message.getMessageReference(), message.getAllowedMentions(), null, null, null, z2 ? message.getLastManualAttemptTimestamp() : null, message.getInitialAttemptTimestamp(), Integer.valueOf(intValue + 1), str, 1824, null);
        }
        throw new IllegalArgumentException("Incorrect " + z2 + " auto attempt and message type " + message.getType());
    }

    public final void resendMessageWithCaptcha(long j, String str, String str2) {
        Message message;
        m.checkNotNullParameter(str2, "captchaKey");
        if (str != null && (message = this.localMessagesHolder.getMessage(j, str)) != null) {
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(resendMessage(message, true, str2)), StoreMessages.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, StoreMessages$resendMessageWithCaptcha$1.INSTANCE);
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v5, types: [java.util.List, T] */
    public final Observable<MessageResult> sendMessage(long j, com.discord.models.user.User user, String str, List<? extends com.discord.models.user.User> list, List<? extends Attachment<?>> list2, List<? extends BaseSticker> list3, MessageReference messageReference, MessageAllowedMentions messageAllowedMentions, Application application, Activity activity, MessageActivity messageActivity, Long l, Long l2, Integer num, String str2) {
        long j2;
        List list4;
        ArrayList arrayList;
        long j3;
        String str3 = str;
        m.checkNotNullParameter(user, "author");
        m.checkNotNullParameter(str3, "content");
        Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        ref$ObjectRef.element = list2;
        int i = 0;
        if (!(list2 == 0 || list2.isEmpty())) {
            ProcessedMessageContent.Companion companion = ProcessedMessageContent.Companion;
            Context context = this.context;
            if (context == null) {
                m.throwUninitializedPropertyAccessException("context");
            }
            ProcessedMessageContent fromAttachments = companion.fromAttachments(list2, str3, context);
            List<Attachment<?>> invalidAttachments = fromAttachments.getInvalidAttachments();
            if (!invalidAttachments.isEmpty()) {
                User synthesizeApiUser = UserUtils.INSTANCE.synthesizeApiUser(user);
                Clock clock = this.clock;
                ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(invalidAttachments, 10));
                Iterator<T> it = invalidAttachments.iterator();
                while (it.hasNext()) {
                    arrayList2.add(AttachmentUtilsKt.toLocalAttachment((Attachment) it.next()));
                }
                this.dispatcher.schedule(new StoreMessages$sendMessage$1(this, LocalMessageCreatorsKt.createInvalidAttachmentsMessage(j, synthesizeApiUser, clock, arrayList2)));
            }
            ref$ObjectRef.element = fromAttachments.getValidAttachments();
            str3 = fromAttachments.getContent();
            List list5 = (List) ref$ObjectRef.element;
            if (list5 == null || list5.isEmpty()) {
                if (str3.length() == 0) {
                    Observable<MessageResult> n = Observable.n(StoreMessages$sendMessage$2.INSTANCE, Emitter.BackpressureMode.ERROR);
                    m.checkNotNullExpressionValue(n, "Observable.create({ emit…r.BackpressureMode.ERROR)");
                    return n;
                }
            }
        }
        if (l != null) {
            j2 = l.longValue();
        } else {
            j2 = this.clock.currentTimeMillis();
        }
        long j4 = j2;
        User synthesizeApiUser2 = UserUtils.INSTANCE.synthesizeApiUser(user);
        if (list != null) {
            list4 = new ArrayList(o.collectionSizeOrDefault(list, 10));
            for (com.discord.models.user.User user2 : list) {
                list4.add(UserUtils.INSTANCE.synthesizeApiUser(user2));
            }
        } else {
            list4 = n.emptyList();
        }
        List list6 = list4;
        List list7 = (List) ref$ObjectRef.element;
        boolean z2 = !(list7 == null || list7.isEmpty());
        Clock clock2 = this.clock;
        List<Attachment> list8 = (List) ref$ObjectRef.element;
        if (list8 != null) {
            ArrayList arrayList3 = new ArrayList(o.collectionSizeOrDefault(list8, 10));
            for (Attachment attachment : list8) {
                arrayList3.add(AttachmentUtilsKt.toLocalAttachment(attachment));
            }
            arrayList = arrayList3;
        } else {
            arrayList = null;
        }
        Long valueOf = Long.valueOf(j4);
        if (l2 != null) {
            j3 = l2.longValue();
        } else {
            j3 = this.clock.currentTimeMillis();
        }
        Long valueOf2 = Long.valueOf(j3);
        if (num != null) {
            i = num.intValue();
        }
        Message createLocalMessage = LocalMessageCreatorsKt.createLocalMessage(str3, j, synthesizeApiUser2, list6, false, z2, application, messageActivity, clock2, arrayList, valueOf, valueOf2, Integer.valueOf(i), list3, messageReference, messageAllowedMentions, str2);
        if (messageActivity == null) {
            this.dispatcher.schedule(new StoreMessages$sendMessage$3(this, createLocalMessage));
        }
        this.dispatcher.schedule(new StoreMessages$sendMessage$4(this));
        return enqueueRequest(j, new StoreMessages$sendMessage$createRequest$1(this, createLocalMessage, ref$ObjectRef, activity, j4));
    }

    @StoreThread
    private final void handleLocalMessageDelete(long j, String str) {
        this.localMessagesHolder.deleteMessage(j, str);
    }

    private final void handleMessageDelete(long j, List<Long> list) {
        this.holder.deleteMessages(j, list);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ Observable sendMessage$default(StoreMessages storeMessages, Message message, ApplicationCommandLocalSendData applicationCommandLocalSendData, List list, int i, Object obj) {
        if ((i & 4) != 0) {
            list = null;
        }
        return storeMessages.sendMessage(message, applicationCommandLocalSendData, list);
    }

    public final Observable<Message> observeMessagesForChannel(long j, final long j2) {
        Observable<Message> q = observeMessagesForChannel(j).F(new b<List<? extends Message>, Message>() { // from class: com.discord.stores.StoreMessages$observeMessagesForChannel$2
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Message call(List<? extends Message> list) {
                return call2((List<Message>) list);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Message call2(List<Message> list) {
                T t;
                boolean z2;
                m.checkNotNullExpressionValue(list, "messages");
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (((Message) t).getId() == j2) {
                        z2 = true;
                        continue;
                    } else {
                        z2 = false;
                        continue;
                    }
                    if (z2) {
                        break;
                    }
                }
                return (Message) t;
            }
        }).q();
        m.checkNotNullExpressionValue(q, "observeMessagesForChanne…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<MessageResult> sendMessage(Message message, ApplicationCommandLocalSendData applicationCommandLocalSendData, List<? extends Attachment<?>> list) {
        m.checkNotNullParameter(message, "message");
        m.checkNotNullParameter(applicationCommandLocalSendData, "applicationCommandLocalSendData");
        this.dispatcher.schedule(new StoreMessages$sendMessage$5(this, message));
        return enqueueRequest(message.getChannelId(), new StoreMessages$sendMessage$createRequest$2(this, message, applicationCommandLocalSendData, list));
    }
}
