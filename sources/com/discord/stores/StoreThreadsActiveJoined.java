package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.guild.Guild;
import com.discord.api.thread.ThreadMemberUpdate;
import com.discord.api.thread.ThreadMembersUpdate;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.models.thread.dto.ModelThreadListSync;
import com.discord.stores.StoreThreadsJoined;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import d0.d0.f;
import d0.t.g0;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreThreadsActiveJoined.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0082\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\b\b\u0018\u00002\u00020\u0001:\u0001FB!\u0012\u0006\u0010=\u001a\u00020<\u0012\u0006\u0010:\u001a\u000209\u0012\b\b\u0002\u00107\u001a\u000206¢\u0006\u0004\bD\u0010EJ!\u0010\u0006\u001a\u00020\u00052\u0010\b\u0002\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J'\u0010\n\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\t\u001a\u00060\u0002j\u0002`\bH\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u001f\u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\b\u0012\u0004\u0012\u00020\r0\fH\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ!\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00120\u00112\n\u0010\t\u001a\u00060\u0002j\u0002`\bH\u0001¢\u0006\u0004\b\u0013\u0010\u0014J#\u0010\u0017\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\b\u0012\u0004\u0012\u00020\r0\f0\u0016¢\u0006\u0004\b\u0017\u0010\u0018J#\u0010\u0019\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\b\u0012\u0004\u0012\u00020\u00120\f0\u0016¢\u0006\u0004\b\u0019\u0010\u0018J?\u0010\u001a\u001a(\u0012$\u0012\"\u0012\b\u0012\u00060\u0002j\u0002`\b\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\b\u0012\u0004\u0012\u00020\r0\f0\f0\u00162\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u001a\u0010\u001bJ/\u0010\u001c\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\b\u0012\u0004\u0012\u00020\u00120\f0\u00162\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u001c\u0010\u001bJ;\u0010\u001d\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\b\u0012\u0004\u0012\u00020\r0\f0\u00162\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\t\u001a\u00060\u0002j\u0002`\b¢\u0006\u0004\b\u001d\u0010\u001eJ\u000f\u0010\u001f\u001a\u00020\u0005H\u0007¢\u0006\u0004\b\u001f\u0010 J\u0017\u0010#\u001a\u00020\u00052\u0006\u0010\"\u001a\u00020!H\u0007¢\u0006\u0004\b#\u0010$J\u001b\u0010%\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0007¢\u0006\u0004\b%\u0010&J\u0017\u0010)\u001a\u00020\u00052\u0006\u0010(\u001a\u00020'H\u0007¢\u0006\u0004\b)\u0010*J\u0017\u0010,\u001a\u00020\u00052\u0006\u0010+\u001a\u00020\u0012H\u0007¢\u0006\u0004\b,\u0010-J\u0017\u0010.\u001a\u00020\u00052\u0006\u0010+\u001a\u00020\u0012H\u0007¢\u0006\u0004\b.\u0010-J\u0017\u00100\u001a\u00020\u00052\u0006\u0010(\u001a\u00020/H\u0007¢\u0006\u0004\b0\u00101J\u0017\u00103\u001a\u00020\u00052\u0006\u0010(\u001a\u000202H\u0007¢\u0006\u0004\b3\u00104J\u000f\u00105\u001a\u00020\u0005H\u0017¢\u0006\u0004\b5\u0010 R\u0016\u00107\u001a\u0002068\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b7\u00108R\u0016\u0010:\u001a\u0002098\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b:\u0010;R\u0016\u0010=\u001a\u00020<8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b=\u0010>R&\u0010@\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\b\u0012\u0004\u0012\u00020\r0?8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b@\u0010AR&\u0010B\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\b\u0012\u0004\u0012\u00020\r0\f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bB\u0010ARF\u0010C\u001a2\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012$\u0012\"\u0012\b\u0012\u00060\u0002j\u0002`\b\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\b\u0012\u0004\u0012\u00020\r0\f0\f0\f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bC\u0010A¨\u0006G"}, d2 = {"Lcom/discord/stores/StoreThreadsActiveJoined;", "Lcom/discord/stores/StoreV2;", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "saveThreads", "(Ljava/lang/Long;)V", "Lcom/discord/primitives/ChannelId;", "channelId", "updateThread", "(JJ)V", "", "Lcom/discord/stores/StoreThreadsActiveJoined$ActiveJoinedThread;", "getActiveJoinedThreadsInternal$app_productionGoogleRelease", "()Ljava/util/Map;", "getActiveJoinedThreadsInternal", "", "Lcom/discord/api/channel/Channel;", "getActiveJoinedThreadsForChannelInternal$app_productionGoogleRelease", "(J)Ljava/util/List;", "getActiveJoinedThreadsForChannelInternal", "Lrx/Observable;", "observeAllActiveJoinedThreadsById", "()Lrx/Observable;", "observeAllActiveJoinedThreadsChannelsById", "observeActiveJoinedThreadsForGuild", "(J)Lrx/Observable;", "observeActiveJoinedThreadsChannelsForGuild", "observeActiveJoinedThreadsForChannel", "(JJ)Lrx/Observable;", "handleConnectionOpen", "()V", "Lcom/discord/api/guild/Guild;", "guild", "handleGuildCreate", "(Lcom/discord/api/guild/Guild;)V", "handleGuildDelete", "(J)V", "Lcom/discord/models/thread/dto/ModelThreadListSync;", "payload", "handleThreadListSync", "(Lcom/discord/models/thread/dto/ModelThreadListSync;)V", "channel", "handleChannelCreateOrUpdate", "(Lcom/discord/api/channel/Channel;)V", "handleThreadCreateOrUpdateOrDelete", "Lcom/discord/api/thread/ThreadMemberUpdate;", "handleThreadMemberUpdate", "(Lcom/discord/api/thread/ThreadMemberUpdate;)V", "Lcom/discord/api/thread/ThreadMembersUpdate;", "handleThreadMembersUpdate", "(Lcom/discord/api/thread/ThreadMembersUpdate;)V", "snapshotData", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/stores/StoreThreadsJoined;", "storeThreadsJoined", "Lcom/discord/stores/StoreThreadsJoined;", "Lcom/discord/stores/StoreThreadsActive;", "storeThreadsActive", "Lcom/discord/stores/StoreThreadsActive;", "", "activeJoinedThreads", "Ljava/util/Map;", "activeJoinedThreadsByThreadIdSnapshot", "activeJoinedThreadsHierarchicalSnapshot", HookHelper.constructorName, "(Lcom/discord/stores/StoreThreadsActive;Lcom/discord/stores/StoreThreadsJoined;Lcom/discord/stores/updates/ObservationDeck;)V", "ActiveJoinedThread", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreThreadsActiveJoined extends StoreV2 {
    private final Map<Long, ActiveJoinedThread> activeJoinedThreads;
    private Map<Long, ActiveJoinedThread> activeJoinedThreadsByThreadIdSnapshot;
    private Map<Long, ? extends Map<Long, ? extends Map<Long, ActiveJoinedThread>>> activeJoinedThreadsHierarchicalSnapshot;
    private final ObservationDeck observationDeck;
    private final StoreThreadsActive storeThreadsActive;
    private final StoreThreadsJoined storeThreadsJoined;

    /* compiled from: StoreThreadsActiveJoined.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0016\u001a\u0004\b\u0017\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0018\u001a\u0004\b\u0019\u0010\u0004¨\u0006\u001c"}, d2 = {"Lcom/discord/stores/StoreThreadsActiveJoined$ActiveJoinedThread;", "", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "Lcom/discord/api/utcdatetime/UtcDateTime;", "component2", "()Lcom/discord/api/utcdatetime/UtcDateTime;", "channel", "joinTimestamp", "copy", "(Lcom/discord/api/channel/Channel;Lcom/discord/api/utcdatetime/UtcDateTime;)Lcom/discord/stores/StoreThreadsActiveJoined$ActiveJoinedThread;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/utcdatetime/UtcDateTime;", "getJoinTimestamp", "Lcom/discord/api/channel/Channel;", "getChannel", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Lcom/discord/api/utcdatetime/UtcDateTime;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ActiveJoinedThread {
        private final Channel channel;
        private final UtcDateTime joinTimestamp;

        public ActiveJoinedThread(Channel channel, UtcDateTime utcDateTime) {
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(utcDateTime, "joinTimestamp");
            this.channel = channel;
            this.joinTimestamp = utcDateTime;
        }

        public static /* synthetic */ ActiveJoinedThread copy$default(ActiveJoinedThread activeJoinedThread, Channel channel, UtcDateTime utcDateTime, int i, Object obj) {
            if ((i & 1) != 0) {
                channel = activeJoinedThread.channel;
            }
            if ((i & 2) != 0) {
                utcDateTime = activeJoinedThread.joinTimestamp;
            }
            return activeJoinedThread.copy(channel, utcDateTime);
        }

        public final Channel component1() {
            return this.channel;
        }

        public final UtcDateTime component2() {
            return this.joinTimestamp;
        }

        public final ActiveJoinedThread copy(Channel channel, UtcDateTime utcDateTime) {
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(utcDateTime, "joinTimestamp");
            return new ActiveJoinedThread(channel, utcDateTime);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ActiveJoinedThread)) {
                return false;
            }
            ActiveJoinedThread activeJoinedThread = (ActiveJoinedThread) obj;
            return m.areEqual(this.channel, activeJoinedThread.channel) && m.areEqual(this.joinTimestamp, activeJoinedThread.joinTimestamp);
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final UtcDateTime getJoinTimestamp() {
            return this.joinTimestamp;
        }

        public int hashCode() {
            Channel channel = this.channel;
            int i = 0;
            int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
            UtcDateTime utcDateTime = this.joinTimestamp;
            if (utcDateTime != null) {
                i = utcDateTime.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("ActiveJoinedThread(channel=");
            R.append(this.channel);
            R.append(", joinTimestamp=");
            R.append(this.joinTimestamp);
            R.append(")");
            return R.toString();
        }
    }

    public /* synthetic */ StoreThreadsActiveJoined(StoreThreadsActive storeThreadsActive, StoreThreadsJoined storeThreadsJoined, ObservationDeck observationDeck, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(storeThreadsActive, storeThreadsJoined, (i & 4) != 0 ? ObservationDeckProvider.get() : observationDeck);
    }

    @StoreThread
    private final void saveThreads(Long l) {
        Channel channel;
        Map<Long, Map<Long, Channel>> allActiveThreadsInternal$app_productionGoogleRelease = this.storeThreadsActive.getAllActiveThreadsInternal$app_productionGoogleRelease();
        for (StoreThreadsJoined.JoinedThread joinedThread : this.storeThreadsJoined.getAllJoinedThreadsInternal$app_productionGoogleRelease().values()) {
            if (l == null || joinedThread.getGuildId() == l.longValue()) {
                Map<Long, Channel> map = allActiveThreadsInternal$app_productionGoogleRelease.get(Long.valueOf(joinedThread.getGuildId()));
                if (!(map == null || (channel = map.get(Long.valueOf(joinedThread.getThreadId()))) == null)) {
                    this.activeJoinedThreads.put(Long.valueOf(channel.h()), new ActiveJoinedThread(channel, joinedThread.getJoinTimestamp()));
                    markChanged();
                }
            }
        }
    }

    public static /* synthetic */ void saveThreads$default(StoreThreadsActiveJoined storeThreadsActiveJoined, Long l, int i, Object obj) {
        if ((i & 1) != 0) {
            l = null;
        }
        storeThreadsActiveJoined.saveThreads(l);
    }

    @StoreThread
    private final void updateThread(long j, long j2) {
        Map<Long, Channel> map = this.storeThreadsActive.getAllActiveThreadsInternal$app_productionGoogleRelease().get(Long.valueOf(j));
        Channel channel = map != null ? map.get(Long.valueOf(j2)) : null;
        StoreThreadsJoined.JoinedThread joinedThread = this.storeThreadsJoined.getAllJoinedThreadsInternal$app_productionGoogleRelease().get(Long.valueOf(j2));
        if (channel != null && joinedThread != null) {
            this.activeJoinedThreads.put(Long.valueOf(j2), new ActiveJoinedThread(channel, joinedThread.getJoinTimestamp()));
            markChanged();
        } else if (this.activeJoinedThreads.containsKey(Long.valueOf(j2))) {
            this.activeJoinedThreads.remove(Long.valueOf(j2));
            markChanged();
        }
    }

    @StoreThread
    public final List<Channel> getActiveJoinedThreadsForChannelInternal$app_productionGoogleRelease(long j) {
        Collection<ActiveJoinedThread> values = this.activeJoinedThreads.values();
        ArrayList<ActiveJoinedThread> arrayList = new ArrayList();
        for (Object obj : values) {
            if (((ActiveJoinedThread) obj).getChannel().r() == j) {
                arrayList.add(obj);
            }
        }
        ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(arrayList, 10));
        for (ActiveJoinedThread activeJoinedThread : arrayList) {
            arrayList2.add(activeJoinedThread.getChannel());
        }
        return arrayList2;
    }

    @StoreThread
    public final Map<Long, ActiveJoinedThread> getActiveJoinedThreadsInternal$app_productionGoogleRelease() {
        return this.activeJoinedThreads;
    }

    @StoreThread
    public final void handleChannelCreateOrUpdate(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        for (Channel channel2 : getActiveJoinedThreadsForChannelInternal$app_productionGoogleRelease(channel.h())) {
            updateThread(channel2.f(), channel2.h());
        }
    }

    @StoreThread
    public final void handleConnectionOpen() {
        this.activeJoinedThreads.clear();
        saveThreads$default(this, null, 1, null);
        markChanged();
    }

    @StoreThread
    public final void handleGuildCreate(Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        saveThreads(Long.valueOf(guild.r()));
    }

    @StoreThread
    public final void handleGuildDelete(long j) {
        Iterator<ActiveJoinedThread> it = this.activeJoinedThreads.values().iterator();
        while (it.hasNext()) {
            if (it.next().getChannel().f() == j) {
                it.remove();
                markChanged();
            }
        }
    }

    @StoreThread
    public final void handleThreadCreateOrUpdateOrDelete(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        updateThread(channel.f(), channel.h());
    }

    @StoreThread
    public final void handleThreadListSync(ModelThreadListSync modelThreadListSync) {
        m.checkNotNullParameter(modelThreadListSync, "payload");
        handleGuildDelete(modelThreadListSync.getGuildId());
        saveThreads(Long.valueOf(modelThreadListSync.getGuildId()));
    }

    @StoreThread
    public final void handleThreadMemberUpdate(ThreadMemberUpdate threadMemberUpdate) {
        m.checkNotNullParameter(threadMemberUpdate, "payload");
        updateThread(threadMemberUpdate.b(), threadMemberUpdate.c());
    }

    @StoreThread
    public final void handleThreadMembersUpdate(ThreadMembersUpdate threadMembersUpdate) {
        m.checkNotNullParameter(threadMembersUpdate, "payload");
        updateThread(threadMembersUpdate.b(), threadMembersUpdate.c());
    }

    public final Observable<Map<Long, Channel>> observeActiveJoinedThreadsChannelsForGuild(long j) {
        Observable<Map<Long, Channel>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreThreadsActiveJoined$observeActiveJoinedThreadsChannelsForGuild$1(this, j), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck\n        …  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Map<Long, ActiveJoinedThread>> observeActiveJoinedThreadsForChannel(long j, long j2) {
        Observable<Map<Long, ActiveJoinedThread>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreThreadsActiveJoined$observeActiveJoinedThreadsForChannel$1(this, j, j2), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck\n        …  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Map<Long, Map<Long, ActiveJoinedThread>>> observeActiveJoinedThreadsForGuild(long j) {
        Observable<Map<Long, Map<Long, ActiveJoinedThread>>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreThreadsActiveJoined$observeActiveJoinedThreadsForGuild$1(this, j), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck\n        …  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Map<Long, ActiveJoinedThread>> observeAllActiveJoinedThreadsById() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreThreadsActiveJoined$observeAllActiveJoinedThreadsById$1(this), 14, null);
    }

    public final Observable<Map<Long, Channel>> observeAllActiveJoinedThreadsChannelsById() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreThreadsActiveJoined$observeAllActiveJoinedThreadsChannelsById$1(this), 14, null);
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        this.activeJoinedThreadsByThreadIdSnapshot = new HashMap(this.activeJoinedThreads);
        Collection<ActiveJoinedThread> values = this.activeJoinedThreads.values();
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Object obj : values) {
            Long valueOf = Long.valueOf(((ActiveJoinedThread) obj).getChannel().f());
            Object obj2 = linkedHashMap.get(valueOf);
            if (obj2 == null) {
                obj2 = new ArrayList();
                linkedHashMap.put(valueOf, obj2);
            }
            ((List) obj2).add(obj);
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap(g0.mapCapacity(linkedHashMap.size()));
        for (Map.Entry entry : linkedHashMap.entrySet()) {
            Object key = entry.getKey();
            LinkedHashMap linkedHashMap3 = new LinkedHashMap();
            for (Object obj3 : (List) entry.getValue()) {
                Long valueOf2 = Long.valueOf(((ActiveJoinedThread) obj3).getChannel().r());
                Object obj4 = linkedHashMap3.get(valueOf2);
                if (obj4 == null) {
                    obj4 = new ArrayList();
                    linkedHashMap3.put(valueOf2, obj4);
                }
                ((List) obj4).add(obj3);
            }
            LinkedHashMap linkedHashMap4 = new LinkedHashMap(g0.mapCapacity(linkedHashMap3.size()));
            for (Map.Entry entry2 : linkedHashMap3.entrySet()) {
                Object key2 = entry2.getKey();
                List list = (List) entry2.getValue();
                LinkedHashMap linkedHashMap5 = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(o.collectionSizeOrDefault(list, 10)), 16));
                for (Object obj5 : list) {
                    linkedHashMap5.put(Long.valueOf(((ActiveJoinedThread) obj5).getChannel().h()), obj5);
                }
                linkedHashMap4.put(key2, linkedHashMap5);
            }
            linkedHashMap2.put(key, linkedHashMap4);
        }
        this.activeJoinedThreadsHierarchicalSnapshot = linkedHashMap2;
    }

    public StoreThreadsActiveJoined(StoreThreadsActive storeThreadsActive, StoreThreadsJoined storeThreadsJoined, ObservationDeck observationDeck) {
        m.checkNotNullParameter(storeThreadsActive, "storeThreadsActive");
        m.checkNotNullParameter(storeThreadsJoined, "storeThreadsJoined");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.storeThreadsActive = storeThreadsActive;
        this.storeThreadsJoined = storeThreadsJoined;
        this.observationDeck = observationDeck;
        this.activeJoinedThreads = new HashMap();
        this.activeJoinedThreadsByThreadIdSnapshot = new HashMap();
        this.activeJoinedThreadsHierarchicalSnapshot = new HashMap();
    }
}
