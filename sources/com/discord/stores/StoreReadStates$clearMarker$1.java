package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.models.application.Unread;
import j0.k.b;
import kotlin.Metadata;
/* compiled from: StoreReadStates.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0005\u0010\u0005\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/application/Unread;", "kotlin.jvm.PlatformType", "unread", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/application/Unread;)Lcom/discord/models/application/Unread;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreReadStates$clearMarker$1<T, R> implements b<Unread, Unread> {
    public static final StoreReadStates$clearMarker$1 INSTANCE = new StoreReadStates$clearMarker$1();

    public final Unread call(Unread unread) {
        return unread.createWithEmptyCount();
    }
}
