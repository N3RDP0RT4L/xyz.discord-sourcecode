package com.discord.stores;

import com.discord.api.user.UserProfile;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreUserProfile.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/api/user/UserProfile;", "invoke", "()Lcom/discord/api/user/UserProfile;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserProfile$observeUserProfile$1 extends o implements Function0<UserProfile> {
    public final /* synthetic */ long $userId;
    public final /* synthetic */ StoreUserProfile this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreUserProfile$observeUserProfile$1(StoreUserProfile storeUserProfile, long j) {
        super(0);
        this.this$0 = storeUserProfile;
        this.$userId = j;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final UserProfile invoke() {
        return this.this$0.getUserProfile(this.$userId);
    }
}
