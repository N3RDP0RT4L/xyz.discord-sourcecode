package com.discord.stores;

import com.discord.widgets.chat.input.expression.ExpressionTrayTab;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreExpressionPickerNavigation.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;", "invoke", "()Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreExpressionPickerNavigation$observeSelectedTab$1 extends o implements Function0<ExpressionTrayTab> {
    public final /* synthetic */ StoreExpressionPickerNavigation this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreExpressionPickerNavigation$observeSelectedTab$1(StoreExpressionPickerNavigation storeExpressionPickerNavigation) {
        super(0);
        this.this$0 = storeExpressionPickerNavigation;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final ExpressionTrayTab invoke() {
        ExpressionTrayTab expressionTrayTab;
        expressionTrayTab = this.this$0.selectedTab;
        return expressionTrayTab;
    }
}
