package com.discord.stores;

import com.discord.widgets.stage.StageRoles;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreStageChannelSelfPresence.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\b\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()I", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStageChannelSelfPresence$init$1 extends o implements Function0<Integer> {
    public final /* synthetic */ StoreStageChannelSelfPresence this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreStageChannelSelfPresence$init$1(StoreStageChannelSelfPresence storeStageChannelSelfPresence) {
        super(0);
        this.this$0 = storeStageChannelSelfPresence;
    }

    /* JADX WARN: Type inference failed for: r0v6, types: [int, java.lang.Integer] */
    @Override // kotlin.jvm.functions.Function0
    public final Integer invoke() {
        Map<Long, StageRoles> channelRolesInternal = this.this$0.getStageChannels().getChannelRolesInternal(this.this$0.getVoiceChannelSelected().getSelectedVoiceChannelId());
        return ((channelRolesInternal != null ? channelRolesInternal.size() : 0) / 100) + 1;
    }
}
