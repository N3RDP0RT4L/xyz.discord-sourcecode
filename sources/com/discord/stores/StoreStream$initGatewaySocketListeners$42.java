package com.discord.stores;

import com.discord.models.domain.ModelNotificationSettings;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreStream.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/ModelNotificationSettings;", "p1", "", "invoke", "(Lcom/discord/models/domain/ModelNotificationSettings;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final /* synthetic */ class StoreStream$initGatewaySocketListeners$42 extends k implements Function1<ModelNotificationSettings, Unit> {
    public StoreStream$initGatewaySocketListeners$42(StoreStream storeStream) {
        super(1, storeStream, StoreStream.class, "handleGuildSettingUpdated", "handleGuildSettingUpdated(Lcom/discord/models/domain/ModelNotificationSettings;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ModelNotificationSettings modelNotificationSettings) {
        invoke2(modelNotificationSettings);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ModelNotificationSettings modelNotificationSettings) {
        m.checkNotNullParameter(modelNotificationSettings, "p1");
        ((StoreStream) this.receiver).handleGuildSettingUpdated(modelNotificationSettings);
    }
}
