package com.discord.stores;

import com.discord.models.experiments.dto.UnauthenticatedUserExperimentsDto;
import com.discord.models.experiments.dto.UserExperimentDto;
import d0.d0.f;
import d0.t.g0;
import d0.z.d.o;
import java.util.LinkedHashMap;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreExperiments.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/models/experiments/dto/UnauthenticatedUserExperimentsDto;", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Lcom/discord/models/experiments/dto/UnauthenticatedUserExperimentsDto;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreExperiments$tryInitializeExperiments$1 extends o implements Function1<UnauthenticatedUserExperimentsDto, Unit> {
    public final /* synthetic */ StoreExperiments this$0;

    /* compiled from: StoreExperiments.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreExperiments$tryInitializeExperiments$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ UnauthenticatedUserExperimentsDto $it;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(UnauthenticatedUserExperimentsDto unauthenticatedUserExperimentsDto) {
            super(0);
            this.$it = unauthenticatedUserExperimentsDto;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            StoreAuthentication storeAuthentication;
            storeAuthentication = StoreExperiments$tryInitializeExperiments$1.this.this$0.storeAuthentication;
            storeAuthentication.setFingerprint(this.$it.getFingerprint(), false);
            StoreExperiments storeExperiments = StoreExperiments$tryInitializeExperiments$1.this.this$0;
            List<UserExperimentDto> assignments = this.$it.getAssignments();
            LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(d0.t.o.collectionSizeOrDefault(assignments, 10)), 16));
            for (Object obj : assignments) {
                linkedHashMap.put(Long.valueOf(((UserExperimentDto) obj).getNameHash()), obj);
            }
            storeExperiments.handleLoadedUserExperiments(linkedHashMap, true);
            StoreExperiments$tryInitializeExperiments$1.this.this$0.setInitialized();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreExperiments$tryInitializeExperiments$1(StoreExperiments storeExperiments) {
        super(1);
        this.this$0 = storeExperiments;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(UnauthenticatedUserExperimentsDto unauthenticatedUserExperimentsDto) {
        invoke2(unauthenticatedUserExperimentsDto);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(UnauthenticatedUserExperimentsDto unauthenticatedUserExperimentsDto) {
        Dispatcher dispatcher;
        dispatcher = this.this$0.dispatcher;
        dispatcher.schedule(new AnonymousClass1(unauthenticatedUserExperimentsDto));
    }
}
