package com.discord.stores;

import com.discord.stores.StoreGifPicker;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreGifPicker.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGifPicker$observeGifsForSearchQuery$1 extends o implements Function0<Unit> {
    public final /* synthetic */ String $query;
    public final /* synthetic */ StoreGifPicker this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGifPicker$observeGifsForSearchQuery$1(StoreGifPicker storeGifPicker, String str) {
        super(0);
        this.this$0 = storeGifPicker;
        this.$query = str;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreGifPicker.CacheHistory cacheHistory;
        StoreGifPicker.CacheHistory cacheHistory2;
        cacheHistory = this.this$0.gifSearchHistory;
        if (!cacheHistory.containsKey(this.$query)) {
            cacheHistory2 = this.this$0.gifSearchHistory;
            cacheHistory2.put(this.$query, StoreGifPicker.Companion.getSearchResultsLoadingList());
            this.this$0.fetchGifsForSearchQuery(this.$query);
        }
    }
}
