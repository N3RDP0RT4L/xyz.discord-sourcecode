package com.discord.stores;

import andhook.lib.HookHelper;
import com.discord.api.commands.ApplicationCommandType;
import com.discord.models.commands.Application;
import com.discord.models.commands.ApplicationCommand;
import com.discord.models.commands.ApplicationCommandOption;
import com.discord.models.commands.BuiltInCommand;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.t.m;
import d0.t.n;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: StoreApplicationCommands.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\n\u0018\u0000 \u001c2\u00020\u0001:\u0001\u001cB\u0007¢\u0006\u0004\b\u001a\u0010\u001bJ;\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\b\u0010\u0007\u001a\u0004\u0018\u00010\u00062\b\u0010\b\u001a\u0004\u0018\u00010\u00062\u0006\u0010\t\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000b\u0010\fJ;\u0010\u000e\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\b\u0010\u0007\u001a\u0004\u0018\u00010\u00062\b\u0010\b\u001a\u0004\u0018\u00010\u00062\u0006\u0010\r\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000e\u0010\fJ\u000f\u0010\u0010\u001a\u00020\u000fH\u0016¢\u0006\u0004\b\u0010\u0010\u0011J\u000f\u0010\u0012\u001a\u00020\u000fH\u0016¢\u0006\u0004\b\u0012\u0010\u0011J\u0015\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\n0\u0013H\u0016¢\u0006\u0004\b\u0014\u0010\u0015R\u001c\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\n0\u00138\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0018\u001a\u00020\u000f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019¨\u0006\u001d"}, d2 = {"Lcom/discord/stores/BuiltInCommands;", "Lcom/discord/stores/BuiltInCommandsProvider;", "", ModelAuditLogEntry.CHANGE_KEY_ID, "", ModelAuditLogEntry.CHANGE_KEY_NAME, "", "descriptionRes", "optionDescriptionRes", "replacement", "Lcom/discord/models/commands/ApplicationCommand;", "createAppendToEndSlashCommand", "(JLjava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)Lcom/discord/models/commands/ApplicationCommand;", "wrapper", "createWrappedSlashCommand", "Lcom/discord/models/commands/Application;", "getFrecencyApplication", "()Lcom/discord/models/commands/Application;", "getBuiltInApplication", "", "getBuiltInCommands", "()Ljava/util/List;", "builtInCommands", "Ljava/util/List;", "builtInApplication", "Lcom/discord/models/commands/Application;", HookHelper.constructorName, "()V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class BuiltInCommands implements BuiltInCommandsProvider {
    public static final long BUILT_IN_APP_ID = -1;
    public static final Companion Companion = new Companion(null);
    public static final long FRECENCY_APP_ID = -2;
    private final Application builtInApplication;
    private final List<ApplicationCommand> builtInCommands;

    /* compiled from: StoreApplicationCommands.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/stores/BuiltInCommands$Companion;", "", "", "BUILT_IN_APP_ID", "J", "FRECENCY_APP_ID", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public BuiltInCommands() {
        List<ApplicationCommand> listOf = n.listOf((Object[]) new ApplicationCommand[]{createAppendToEndSlashCommand(-1L, "shrug", Integer.valueOf((int) R.string.command_shrug_description), Integer.valueOf((int) R.string.command_shrug_message_description), "¯\\\\_(ツ)\\_/¯"), createAppendToEndSlashCommand(-2L, "tableflip", Integer.valueOf((int) R.string.command_tableflip_description), Integer.valueOf((int) R.string.command_tableflip_message_description), "(╯°□°）╯︵ ┻━┻"), createAppendToEndSlashCommand(-3L, "unflip", Integer.valueOf((int) R.string.command_tableunflip_description), Integer.valueOf((int) R.string.command_tableunflip_message_description), "┬─┬ ノ( ゜-゜ノ)"), createWrappedSlashCommand(-5L, "me", Integer.valueOf((int) R.string.command_me_description), Integer.valueOf((int) R.string.command_me_message_description), "_"), createWrappedSlashCommand(-6L, "spoiler", Integer.valueOf((int) R.string.command_spoiler_description), Integer.valueOf((int) R.string.command_spoiler_message_description), "||")});
        this.builtInCommands = listOf;
        this.builtInApplication = new Application(-1L, "Built-In", null, Integer.valueOf((int) R.drawable.ic_slash_command_24dp), listOf.size(), null, true, 36, null);
    }

    private final ApplicationCommand createAppendToEndSlashCommand(long j, String str, Integer num, Integer num2, String str2) {
        return new BuiltInCommand(String.valueOf(j), -1L, str, num, m.listOf(new ApplicationCommandOption(ApplicationCommandType.STRING, "message", null, num2, false, false, null, null, null, false, null, null, 4036, null)), new BuiltInCommands$createAppendToEndSlashCommand$1(str2));
    }

    private final ApplicationCommand createWrappedSlashCommand(long j, String str, Integer num, Integer num2, String str2) {
        return new BuiltInCommand(String.valueOf(j), -1L, str, num, m.listOf(new ApplicationCommandOption(ApplicationCommandType.STRING, "message", null, num2, true, false, null, null, null, false, null, null, 4036, null)), new BuiltInCommands$createWrappedSlashCommand$1(str2));
    }

    @Override // com.discord.stores.BuiltInCommandsProvider
    public Application getBuiltInApplication() {
        return this.builtInApplication;
    }

    @Override // com.discord.stores.BuiltInCommandsProvider
    public List<ApplicationCommand> getBuiltInCommands() {
        return this.builtInCommands;
    }

    @Override // com.discord.stores.BuiltInCommandsProvider
    public Application getFrecencyApplication() {
        return new Application(-2L, "Frequently Used", null, Integer.valueOf((int) R.drawable.ic_recent_24dp), 0, null, true, 36, null);
    }
}
