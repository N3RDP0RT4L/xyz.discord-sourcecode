package com.discord.stores;

import androidx.core.app.NotificationCompat;
import d0.z.d.m;
import j0.k.b;
import kotlin.Metadata;
import rx.Observable;
import rx.functions.Action1;
/* compiled from: StoreGuilds.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\b\u001a>\u0012\u0018\b\u0001\u0012\u0014 \u0005*\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00040\u0003j\u0002`\u0004 \u0005*\u001e\u0012\u0018\b\u0001\u0012\u0014 \u0005*\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00040\u0003j\u0002`\u0004\u0018\u00010\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "query", "Lrx/Observable;", "", "Lcom/discord/primitives/GuildId;", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/String;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuilds$Actions$requestMembers$3<T, R> implements b<String, Observable<? extends Long>> {
    public static final StoreGuilds$Actions$requestMembers$3 INSTANCE = new StoreGuilds$Actions$requestMembers$3();

    public final Observable<? extends Long> call(final String str) {
        return StoreStream.Companion.getGuildSelected().observeSelectedGuildId().t(new Action1<Long>() { // from class: com.discord.stores.StoreGuilds$Actions$requestMembers$3.1
            public final void call(Long l) {
                StoreGatewayConnection gatewaySocket = StoreStream.Companion.getGatewaySocket();
                m.checkNotNullExpressionValue(l, "selectedGuildId");
                StoreGatewayConnection.requestGuildMembers$default(gatewaySocket, l.longValue(), str, null, null, 12, null);
            }
        });
    }
}
