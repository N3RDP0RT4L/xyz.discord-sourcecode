package com.discord.stores;

import com.discord.api.channel.Channel;
import com.discord.models.message.Message;
import d0.d0.f;
import d0.t.g0;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreChannels.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "", "Lcom/discord/api/channel/Channel;", "invoke", "()Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreChannels$observeThreadsFromMessages$1 extends o implements Function0<Map<Long, ? extends Channel>> {
    public final /* synthetic */ List $messages;
    public final /* synthetic */ StoreChannels this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreChannels$observeThreadsFromMessages$1(StoreChannels storeChannels, List list) {
        super(0);
        this.this$0 = storeChannels;
        this.$messages = list;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Map<Long, ? extends Channel> invoke() {
        List list = this.$messages;
        ArrayList<Message> arrayList = new ArrayList();
        for (Object obj : list) {
            if (((Message) obj).hasThread()) {
                arrayList.add(obj);
            }
        }
        ArrayList arrayList2 = new ArrayList();
        for (Message message : arrayList) {
            Channel channel = this.this$0.getChannel(message.getId());
            if (channel != null) {
                arrayList2.add(channel);
            }
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(d0.t.o.collectionSizeOrDefault(arrayList2, 10)), 16));
        for (Object obj2 : arrayList2) {
            linkedHashMap.put(Long.valueOf(((Channel) obj2).h()), obj2);
        }
        return linkedHashMap;
    }
}
