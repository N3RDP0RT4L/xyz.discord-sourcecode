package com.discord.stores;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.message.reaction.MessageReactionEmoji;
import com.discord.api.message.reaction.MessageReactionUpdate;
import com.discord.models.user.User;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.z.d.m;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreMessageReactions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0086\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0001=B!\u0012\u0006\u00103\u001a\u000202\u0012\u0006\u00100\u001a\u00020/\u0012\b\b\u0002\u00109\u001a\u000208¢\u0006\u0004\b;\u0010<J=\u0010\r\u001a\u00020\f2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u00072\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0003¢\u0006\u0004\b\r\u0010\u000eJ/\u0010\u000f\u001a\u00020\f2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u0007H\u0003¢\u0006\u0004\b\u000f\u0010\u0010J/\u0010\u0011\u001a\u00020\f2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u0007H\u0003¢\u0006\u0004\b\u0011\u0010\u0010J'\u0010\u0015\u001a\u00020\u00142\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u00052\n\u0010\b\u001a\u00060\u0012j\u0002`\u0013H\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u0017\u0010\u0017\u001a\u00020\u00122\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\u0017\u0010\u0018J3\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00140\u00192\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\u001a\u0010\u001bJ-\u0010\u001c\u001a\u00020\f2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\u001c\u0010\u0010J9\u0010\u001f\u001a\u00020\f2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u00072\n\u0010\u001e\u001a\u00060\u0002j\u0002`\u001d¢\u0006\u0004\b\u001f\u0010 J\u000f\u0010!\u001a\u00020\fH\u0007¢\u0006\u0004\b!\u0010\"J\u0017\u0010%\u001a\u00020\f2\u0006\u0010$\u001a\u00020#H\u0007¢\u0006\u0004\b%\u0010&J\u0017\u0010'\u001a\u00020\f2\u0006\u0010$\u001a\u00020#H\u0007¢\u0006\u0004\b'\u0010&J\u0017\u0010(\u001a\u00020\f2\u0006\u0010$\u001a\u00020#H\u0007¢\u0006\u0004\b(\u0010&J\u0017\u0010)\u001a\u00020\f2\u0006\u0010$\u001a\u00020#H\u0007¢\u0006\u0004\b)\u0010&J\u000f\u0010*\u001a\u00020\fH\u0017¢\u0006\u0004\b*\u0010\"R:\u0010-\u001a&\u0012\b\u0012\u00060\u0002j\u0002`\u0005\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0012j\u0002`\u0013\u0012\u0004\u0012\u00020\u00140+0+j\u0002`,8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b-\u0010.R\u0016\u00100\u001a\u00020/8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b0\u00101R\u0016\u00103\u001a\u0002028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b3\u00104R:\u00107\u001a&\u0012\b\u0012\u00060\u0002j\u0002`\u0005\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0012j\u0002`\u0013\u0012\u0004\u0012\u00020\u00140505j\u0002`68\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b7\u0010.R\u0016\u00109\u001a\u0002088\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b9\u0010:¨\u0006>"}, d2 = {"Lcom/discord/stores/StoreMessageReactions;", "Lcom/discord/stores/StoreV2;", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/primitives/MessageId;", "messageId", "Lcom/discord/api/message/reaction/MessageReactionEmoji;", "emoji", "", "Lcom/discord/models/user/User;", "reactionUsers", "", "handleReactionUsers", "(JJLcom/discord/api/message/reaction/MessageReactionEmoji;Ljava/util/List;)V", "handleLoadReactionUsersFailure", "(JJLcom/discord/api/message/reaction/MessageReactionEmoji;)V", "fetchReactions", "", "Lcom/discord/stores/EmojiKey;", "Lcom/discord/stores/StoreMessageReactions$EmojiResults;", "ensureReactionResults", "(JLjava/lang/String;)Lcom/discord/stores/StoreMessageReactions$EmojiResults;", "getReactionEmojiRequestParam", "(Lcom/discord/api/message/reaction/MessageReactionEmoji;)Ljava/lang/String;", "Lrx/Observable;", "observeMessageReactions", "(JJLcom/discord/api/message/reaction/MessageReactionEmoji;)Lrx/Observable;", "forceRetryFetchReactions", "Lcom/discord/primitives/UserId;", "userId", "deleteEmoji", "(JJLcom/discord/api/message/reaction/MessageReactionEmoji;J)V", "handleConnectionOpen", "()V", "Lcom/discord/api/message/reaction/MessageReactionUpdate;", "update", "handleReactionAdd", "(Lcom/discord/api/message/reaction/MessageReactionUpdate;)V", "handleReactionRemove", "handleReactionRemoveEmoji", "handleReactionRemoveAll", "snapshotData", "", "Lcom/discord/stores/MutableReactionsMap;", "reactions", "Ljava/util/Map;", "Lcom/discord/stores/StoreUser;", "userStore", "Lcom/discord/stores/StoreUser;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "", "Lcom/discord/stores/ReactionsMap;", "reactionsSnapshot", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreUser;Lcom/discord/stores/updates/ObservationDeck;)V", "EmojiResults", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessageReactions extends StoreV2 {
    private final Dispatcher dispatcher;
    private final ObservationDeck observationDeck;
    private final Map<Long, Map<String, EmojiResults>> reactions;
    private Map<Long, ? extends Map<String, ? extends EmojiResults>> reactionsSnapshot;
    private final StoreUser userStore;

    /* compiled from: StoreMessageReactions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/stores/StoreMessageReactions$EmojiResults;", "", HookHelper.constructorName, "()V", "Failure", "Loading", "Users", "Lcom/discord/stores/StoreMessageReactions$EmojiResults$Users;", "Lcom/discord/stores/StoreMessageReactions$EmojiResults$Failure;", "Lcom/discord/stores/StoreMessageReactions$EmojiResults$Loading;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class EmojiResults {

        /* compiled from: StoreMessageReactions.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B'\u0012\n\u0010\u000b\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010\f\u001a\u00060\u0002j\u0002`\u0006\u0012\u0006\u0010\r\u001a\u00020\b¢\u0006\u0004\b \u0010!J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\u0007\u001a\u00060\u0002j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0005J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ6\u0010\u000e\u001a\u00020\u00002\f\b\u0002\u0010\u000b\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010\f\u001a\u00060\u0002j\u0002`\u00062\b\b\u0002\u0010\r\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0019\u001a\u00020\u00182\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016HÖ\u0003¢\u0006\u0004\b\u0019\u0010\u001aR\u001d\u0010\u000b\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001b\u001a\u0004\b\u001c\u0010\u0005R\u0019\u0010\r\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001d\u001a\u0004\b\u001e\u0010\nR\u001d\u0010\f\u001a\u00060\u0002j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001b\u001a\u0004\b\u001f\u0010\u0005¨\u0006\""}, d2 = {"Lcom/discord/stores/StoreMessageReactions$EmojiResults$Failure;", "Lcom/discord/stores/StoreMessageReactions$EmojiResults;", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "Lcom/discord/primitives/MessageId;", "component2", "Lcom/discord/api/message/reaction/MessageReactionEmoji;", "component3", "()Lcom/discord/api/message/reaction/MessageReactionEmoji;", "channelId", "messageId", "emoji", "copy", "(JJLcom/discord/api/message/reaction/MessageReactionEmoji;)Lcom/discord/stores/StoreMessageReactions$EmojiResults$Failure;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getChannelId", "Lcom/discord/api/message/reaction/MessageReactionEmoji;", "getEmoji", "getMessageId", HookHelper.constructorName, "(JJLcom/discord/api/message/reaction/MessageReactionEmoji;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Failure extends EmojiResults {
            private final long channelId;
            private final MessageReactionEmoji emoji;
            private final long messageId;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Failure(long j, long j2, MessageReactionEmoji messageReactionEmoji) {
                super(null);
                m.checkNotNullParameter(messageReactionEmoji, "emoji");
                this.channelId = j;
                this.messageId = j2;
                this.emoji = messageReactionEmoji;
            }

            public static /* synthetic */ Failure copy$default(Failure failure, long j, long j2, MessageReactionEmoji messageReactionEmoji, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = failure.channelId;
                }
                long j3 = j;
                if ((i & 2) != 0) {
                    j2 = failure.messageId;
                }
                long j4 = j2;
                if ((i & 4) != 0) {
                    messageReactionEmoji = failure.emoji;
                }
                return failure.copy(j3, j4, messageReactionEmoji);
            }

            public final long component1() {
                return this.channelId;
            }

            public final long component2() {
                return this.messageId;
            }

            public final MessageReactionEmoji component3() {
                return this.emoji;
            }

            public final Failure copy(long j, long j2, MessageReactionEmoji messageReactionEmoji) {
                m.checkNotNullParameter(messageReactionEmoji, "emoji");
                return new Failure(j, j2, messageReactionEmoji);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Failure)) {
                    return false;
                }
                Failure failure = (Failure) obj;
                return this.channelId == failure.channelId && this.messageId == failure.messageId && m.areEqual(this.emoji, failure.emoji);
            }

            public final long getChannelId() {
                return this.channelId;
            }

            public final MessageReactionEmoji getEmoji() {
                return this.emoji;
            }

            public final long getMessageId() {
                return this.messageId;
            }

            public int hashCode() {
                int a = (b.a(this.messageId) + (b.a(this.channelId) * 31)) * 31;
                MessageReactionEmoji messageReactionEmoji = this.emoji;
                return a + (messageReactionEmoji != null ? messageReactionEmoji.hashCode() : 0);
            }

            public String toString() {
                StringBuilder R = a.R("Failure(channelId=");
                R.append(this.channelId);
                R.append(", messageId=");
                R.append(this.messageId);
                R.append(", emoji=");
                R.append(this.emoji);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: StoreMessageReactions.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreMessageReactions$EmojiResults$Loading;", "Lcom/discord/stores/StoreMessageReactions$EmojiResults;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Loading extends EmojiResults {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        /* compiled from: StoreMessageReactions.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001BS\u0012*\u0010\u0013\u001a&\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u00070\u0004j\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u0007`\b\u0012\n\u0010\u0014\u001a\u00060\u0005j\u0002`\u000b\u0012\n\u0010\u0015\u001a\u00060\u0005j\u0002`\u000e\u0012\u0006\u0010\u0016\u001a\u00020\u0010¢\u0006\u0004\b+\u0010,J\r\u0010\u0002\u001a\u00020\u0000¢\u0006\u0004\b\u0002\u0010\u0003J4\u0010\t\u001a&\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u00070\u0004j\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u0007`\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0014\u0010\f\u001a\u00060\u0005j\u0002`\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0014\u0010\u000f\u001a\u00060\u0005j\u0002`\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\rJ\u0010\u0010\u0011\u001a\u00020\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012Jd\u0010\u0017\u001a\u00020\u00002,\b\u0002\u0010\u0013\u001a&\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u00070\u0004j\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u0007`\b2\f\b\u0002\u0010\u0014\u001a\u00060\u0005j\u0002`\u000b2\f\b\u0002\u0010\u0015\u001a\u00060\u0005j\u0002`\u000e2\b\b\u0002\u0010\u0016\u001a\u00020\u0010HÆ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u001a\u001a\u00020\u0019HÖ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u001d\u001a\u00020\u001cHÖ\u0001¢\u0006\u0004\b\u001d\u0010\u001eJ\u001a\u0010\"\u001a\u00020!2\b\u0010 \u001a\u0004\u0018\u00010\u001fHÖ\u0003¢\u0006\u0004\b\"\u0010#R\u001d\u0010\u0015\u001a\u00060\u0005j\u0002`\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010$\u001a\u0004\b%\u0010\rR\u001d\u0010\u0014\u001a\u00060\u0005j\u0002`\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010$\u001a\u0004\b&\u0010\rR=\u0010\u0013\u001a&\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u00070\u0004j\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u0007`\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010'\u001a\u0004\b(\u0010\nR\u0019\u0010\u0016\u001a\u00020\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010)\u001a\u0004\b*\u0010\u0012¨\u0006-"}, d2 = {"Lcom/discord/stores/StoreMessageReactions$EmojiResults$Users;", "Lcom/discord/stores/StoreMessageReactions$EmojiResults;", "deepCopy", "()Lcom/discord/stores/StoreMessageReactions$EmojiResults$Users;", "Ljava/util/LinkedHashMap;", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/user/User;", "Lkotlin/collections/LinkedHashMap;", "component1", "()Ljava/util/LinkedHashMap;", "Lcom/discord/primitives/ChannelId;", "component2", "()J", "Lcom/discord/primitives/MessageId;", "component3", "Lcom/discord/api/message/reaction/MessageReactionEmoji;", "component4", "()Lcom/discord/api/message/reaction/MessageReactionEmoji;", "users", "channelId", "messageId", "emoji", "copy", "(Ljava/util/LinkedHashMap;JJLcom/discord/api/message/reaction/MessageReactionEmoji;)Lcom/discord/stores/StoreMessageReactions$EmojiResults$Users;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getMessageId", "getChannelId", "Ljava/util/LinkedHashMap;", "getUsers", "Lcom/discord/api/message/reaction/MessageReactionEmoji;", "getEmoji", HookHelper.constructorName, "(Ljava/util/LinkedHashMap;JJLcom/discord/api/message/reaction/MessageReactionEmoji;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Users extends EmojiResults {
            private final long channelId;
            private final MessageReactionEmoji emoji;
            private final long messageId;
            private final LinkedHashMap<Long, User> users;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Users(LinkedHashMap<Long, User> linkedHashMap, long j, long j2, MessageReactionEmoji messageReactionEmoji) {
                super(null);
                m.checkNotNullParameter(linkedHashMap, "users");
                m.checkNotNullParameter(messageReactionEmoji, "emoji");
                this.users = linkedHashMap;
                this.channelId = j;
                this.messageId = j2;
                this.emoji = messageReactionEmoji;
            }

            public static /* synthetic */ Users copy$default(Users users, LinkedHashMap linkedHashMap, long j, long j2, MessageReactionEmoji messageReactionEmoji, int i, Object obj) {
                LinkedHashMap<Long, User> linkedHashMap2 = linkedHashMap;
                if ((i & 1) != 0) {
                    linkedHashMap2 = users.users;
                }
                if ((i & 2) != 0) {
                    j = users.channelId;
                }
                long j3 = j;
                if ((i & 4) != 0) {
                    j2 = users.messageId;
                }
                long j4 = j2;
                if ((i & 8) != 0) {
                    messageReactionEmoji = users.emoji;
                }
                return users.copy(linkedHashMap2, j3, j4, messageReactionEmoji);
            }

            public final LinkedHashMap<Long, User> component1() {
                return this.users;
            }

            public final long component2() {
                return this.channelId;
            }

            public final long component3() {
                return this.messageId;
            }

            public final MessageReactionEmoji component4() {
                return this.emoji;
            }

            public final Users copy(LinkedHashMap<Long, User> linkedHashMap, long j, long j2, MessageReactionEmoji messageReactionEmoji) {
                m.checkNotNullParameter(linkedHashMap, "users");
                m.checkNotNullParameter(messageReactionEmoji, "emoji");
                return new Users(linkedHashMap, j, j2, messageReactionEmoji);
            }

            public final Users deepCopy() {
                return new Users(new LinkedHashMap(this.users), this.channelId, this.messageId, this.emoji);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Users)) {
                    return false;
                }
                Users users = (Users) obj;
                return m.areEqual(this.users, users.users) && this.channelId == users.channelId && this.messageId == users.messageId && m.areEqual(this.emoji, users.emoji);
            }

            public final long getChannelId() {
                return this.channelId;
            }

            public final MessageReactionEmoji getEmoji() {
                return this.emoji;
            }

            public final long getMessageId() {
                return this.messageId;
            }

            public final LinkedHashMap<Long, User> getUsers() {
                return this.users;
            }

            public int hashCode() {
                LinkedHashMap<Long, User> linkedHashMap = this.users;
                int i = 0;
                int hashCode = linkedHashMap != null ? linkedHashMap.hashCode() : 0;
                int a = (b.a(this.messageId) + ((b.a(this.channelId) + (hashCode * 31)) * 31)) * 31;
                MessageReactionEmoji messageReactionEmoji = this.emoji;
                if (messageReactionEmoji != null) {
                    i = messageReactionEmoji.hashCode();
                }
                return a + i;
            }

            public String toString() {
                StringBuilder R = a.R("Users(users=");
                R.append(this.users);
                R.append(", channelId=");
                R.append(this.channelId);
                R.append(", messageId=");
                R.append(this.messageId);
                R.append(", emoji=");
                R.append(this.emoji);
                R.append(")");
                return R.toString();
            }
        }

        private EmojiResults() {
        }

        public /* synthetic */ EmojiResults(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ StoreMessageReactions(Dispatcher dispatcher, StoreUser storeUser, ObservationDeck observationDeck, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(dispatcher, storeUser, (i & 4) != 0 ? ObservationDeckProvider.get() : observationDeck);
    }

    @StoreThread
    private final EmojiResults ensureReactionResults(long j, String str) {
        Map<String, EmojiResults> map = this.reactions.get(Long.valueOf(j));
        if (map == null) {
            map = new HashMap<>();
        }
        EmojiResults emojiResults = map.get(str);
        if (emojiResults == null) {
            emojiResults = EmojiResults.Loading.INSTANCE;
        }
        map.put(str, emojiResults);
        this.reactions.put(Long.valueOf(j), map);
        return emojiResults;
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void fetchReactions(long j, long j2, MessageReactionEmoji messageReactionEmoji) {
        EmojiResults ensureReactionResults = ensureReactionResults(j2, messageReactionEmoji.c());
        if (!(ensureReactionResults instanceof EmojiResults.Users) || !(!((EmojiResults.Users) ensureReactionResults).getUsers().isEmpty())) {
            String reactionEmojiRequestParam = getReactionEmojiRequestParam(messageReactionEmoji);
            Map<String, EmojiResults> map = this.reactions.get(Long.valueOf(j2));
            if (map != null) {
                map.put(messageReactionEmoji.c(), EmojiResults.Loading.INSTANCE);
            }
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().getReactionUsers(j, j2, reactionEmojiRequestParam, 100), false, 1, null), StoreMessageReactions.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new StoreMessageReactions$fetchReactions$2(this, j, j2, messageReactionEmoji), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreMessageReactions$fetchReactions$1(this, j, j2, messageReactionEmoji));
        }
    }

    private final String getReactionEmojiRequestParam(MessageReactionEmoji messageReactionEmoji) {
        if (messageReactionEmoji.e()) {
            return messageReactionEmoji.d() + MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR + messageReactionEmoji.b();
        }
        String d = messageReactionEmoji.d();
        return d != null ? d : "";
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleLoadReactionUsersFailure(long j, long j2, MessageReactionEmoji messageReactionEmoji) {
        ensureReactionResults(j2, messageReactionEmoji.c());
        Map<String, EmojiResults> map = this.reactions.get(Long.valueOf(j2));
        if (map != null) {
            map.put(messageReactionEmoji.c(), new EmojiResults.Failure(j, j2, messageReactionEmoji));
        }
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleReactionUsers(long j, long j2, MessageReactionEmoji messageReactionEmoji, List<? extends User> list) {
        ensureReactionResults(j2, messageReactionEmoji.c());
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Object obj : list) {
            linkedHashMap.put(Long.valueOf(((User) obj).getId()), obj);
        }
        Map<String, EmojiResults> map = this.reactions.get(Long.valueOf(j2));
        if (map != null) {
            map.put(messageReactionEmoji.c(), new EmojiResults.Users(linkedHashMap, j, j2, messageReactionEmoji));
        }
        markChanged();
    }

    public final void deleteEmoji(long j, long j2, MessageReactionEmoji messageReactionEmoji, long j3) {
        m.checkNotNullParameter(messageReactionEmoji, "emoji");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().removeReaction(j, j2, getReactionEmojiRequestParam(messageReactionEmoji), j3), false, 1, null), StoreMessageReactions.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, StoreMessageReactions$deleteEmoji$1.INSTANCE);
    }

    public final void forceRetryFetchReactions(long j, long j2, MessageReactionEmoji messageReactionEmoji) {
        m.checkNotNullParameter(messageReactionEmoji, "emoji");
        this.dispatcher.schedule(new StoreMessageReactions$forceRetryFetchReactions$1(this, j2, messageReactionEmoji, j));
    }

    @StoreThread
    public final void handleConnectionOpen() {
        if (!this.reactions.isEmpty()) {
            this.reactions.clear();
            markChanged();
        }
    }

    @StoreThread
    public final void handleReactionAdd(MessageReactionUpdate messageReactionUpdate) {
        m.checkNotNullParameter(messageReactionUpdate, "update");
        User user = this.userStore.getUsersInternal$app_productionGoogleRelease().get(Long.valueOf(messageReactionUpdate.d()));
        if (user != null) {
            EmojiResults ensureReactionResults = ensureReactionResults(messageReactionUpdate.c(), messageReactionUpdate.b().c());
            if (!(ensureReactionResults instanceof EmojiResults.Users)) {
                ensureReactionResults = null;
            }
            EmojiResults.Users users = (EmojiResults.Users) ensureReactionResults;
            if (users != null) {
                users.getUsers().put(Long.valueOf(messageReactionUpdate.d()), user);
                markChanged();
            }
        }
    }

    @StoreThread
    public final void handleReactionRemove(MessageReactionUpdate messageReactionUpdate) {
        m.checkNotNullParameter(messageReactionUpdate, "update");
        EmojiResults ensureReactionResults = ensureReactionResults(messageReactionUpdate.c(), messageReactionUpdate.b().c());
        if (!(ensureReactionResults instanceof EmojiResults.Users)) {
            ensureReactionResults = null;
        }
        EmojiResults.Users users = (EmojiResults.Users) ensureReactionResults;
        if (users != null) {
            users.getUsers().remove(Long.valueOf(messageReactionUpdate.d()));
            markChanged();
        }
    }

    @StoreThread
    public final void handleReactionRemoveAll(MessageReactionUpdate messageReactionUpdate) {
        m.checkNotNullParameter(messageReactionUpdate, "update");
        this.reactions.remove(Long.valueOf(messageReactionUpdate.c()));
        markChanged();
    }

    @StoreThread
    public final void handleReactionRemoveEmoji(MessageReactionUpdate messageReactionUpdate) {
        m.checkNotNullParameter(messageReactionUpdate, "update");
        long c = messageReactionUpdate.c();
        String c2 = messageReactionUpdate.b().c();
        ensureReactionResults(c, c2);
        Map<String, EmojiResults> map = this.reactions.get(Long.valueOf(c));
        if (map != null) {
            map.remove(c2);
        }
        markChanged();
    }

    public final Observable<EmojiResults> observeMessageReactions(long j, long j2, MessageReactionEmoji messageReactionEmoji) {
        m.checkNotNullParameter(messageReactionEmoji, "emoji");
        this.dispatcher.schedule(new StoreMessageReactions$observeMessageReactions$1(this, j, j2, messageReactionEmoji));
        Observable<EmojiResults> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreMessageReactions$observeMessageReactions$2(this, j2, messageReactionEmoji), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        super.snapshotData();
        HashMap hashMap = new HashMap(this.reactions);
        for (Map.Entry<Long, Map<String, EmojiResults>> entry : this.reactions.entrySet()) {
            long longValue = entry.getKey().longValue();
            HashMap hashMap2 = new HashMap(entry.getValue());
            hashMap.put(Long.valueOf(longValue), hashMap2);
            for (Map.Entry entry2 : hashMap2.entrySet()) {
                String str = (String) entry2.getKey();
                Object obj = (EmojiResults) entry2.getValue();
                if (obj instanceof EmojiResults.Users) {
                    obj = ((EmojiResults.Users) obj).deepCopy();
                }
                hashMap2.put(str, obj);
            }
        }
        this.reactionsSnapshot = hashMap;
    }

    public StoreMessageReactions(Dispatcher dispatcher, StoreUser storeUser, ObservationDeck observationDeck) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(storeUser, "userStore");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.dispatcher = dispatcher;
        this.userStore = storeUser;
        this.observationDeck = observationDeck;
        this.reactions = new HashMap();
        this.reactionsSnapshot = new HashMap();
    }
}
