package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.api.guild.GuildVerificationLevel;
import com.discord.models.guild.Guild;
import j0.k.b;
import kotlin.Metadata;
/* compiled from: StoreGuilds.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0006\u001a\n \u0003*\u0004\u0018\u00010\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/api/guild/GuildVerificationLevel;", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/guild/Guild;)Lcom/discord/api/guild/GuildVerificationLevel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuilds$observeVerificationLevel$1<T, R> implements b<Guild, GuildVerificationLevel> {
    public static final StoreGuilds$observeVerificationLevel$1 INSTANCE = new StoreGuilds$observeVerificationLevel$1();

    public final GuildVerificationLevel call(Guild guild) {
        GuildVerificationLevel verificationLevel;
        return (guild == null || (verificationLevel = guild.getVerificationLevel()) == null) ? GuildVerificationLevel.NONE : verificationLevel;
    }
}
