package com.discord.stores;

import com.discord.models.commands.ApplicationCommandLocalSendData;
import com.discord.models.message.Message;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreApplicationInteractions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreApplicationInteractions$resendApplicationCommand$1 extends o implements Function0<Unit> {
    public final /* synthetic */ Message $message;
    public final /* synthetic */ String $nonce;
    public final /* synthetic */ ApplicationCommandLocalSendData $updatedSendData;
    public final /* synthetic */ StoreApplicationInteractions this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreApplicationInteractions$resendApplicationCommand$1(StoreApplicationInteractions storeApplicationInteractions, Message message, String str, ApplicationCommandLocalSendData applicationCommandLocalSendData) {
        super(0);
        this.this$0 = storeApplicationInteractions;
        this.$message = message;
        this.$nonce = str;
        this.$updatedSendData = applicationCommandLocalSendData;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreMessages storeMessages;
        storeMessages = this.this$0.storeMessages;
        storeMessages.deleteLocalMessage(this.$message.getChannelId(), this.$nonce);
        StoreApplicationInteractions.handleSendApplicationCommandRequest$default(this.this$0, this.$updatedSendData, null, null, null, 14, null);
        this.this$0.removeApplicationCommandSendData(this.$nonce);
    }
}
