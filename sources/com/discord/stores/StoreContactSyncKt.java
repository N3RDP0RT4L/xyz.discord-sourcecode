package com.discord.stores;

import kotlin.Metadata;
/* compiled from: StoreContactSync.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0004\"\u0016\u0010\u0001\u001a\u00020\u00008\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0001\u0010\u0002\"\u0016\u0010\u0003\u001a\u00020\u00008\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0002\"\u0016\u0010\u0004\u001a\u00020\u00008\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0004\u0010\u0002\"\u0016\u0010\u0005\u001a\u00020\u00008\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0002\"\u0016\u0010\u0007\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0007\u0010\b\"\u0016\u0010\t\u001a\u00020\u00008\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\t\u0010\u0002¨\u0006\n"}, d2 = {"", "FRIENDS_UPSELL_DISMISS_KEY", "Ljava/lang/String;", "NOTICE_NAME", "UPSELL_DISMISS_KEY", "LAST_UPLOAD_TIME_CACHE_KEY", "", "EXISTING_ACCOUNT_AGE_THRESHOLD", "J", "DISMISS_STATE_CACHE_KEY", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreContactSyncKt {
    private static final String DISMISS_STATE_CACHE_KEY = "CONTACT_SYNC_DISMISS_STATE";
    private static final long EXISTING_ACCOUNT_AGE_THRESHOLD = 604800000;
    private static final String FRIENDS_UPSELL_DISMISS_KEY = "CONTACT_SYNC_DISMISS_FRIENDS_UPSELL";
    private static final String LAST_UPLOAD_TIME_CACHE_KEY = "CONTACT_SYNC_LAST_UPLOAD_TIME";
    private static final String NOTICE_NAME = "CONTACT_SYNC_UPSELL";
    private static final String UPSELL_DISMISS_KEY = "CONTACT_SYNC_DISMISS_UPSELL";
}
