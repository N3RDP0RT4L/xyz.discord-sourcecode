package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.api.message.Message;
import com.discord.stores.StoreMessagesLoader;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function4;
import rx.Observable;
import rx.Subscription;
import rx.subjects.SerializedSubject;
/* compiled from: StoreMessagesLoader.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\n\u001a\u00020\u00072\n\u0010\u0002\u001a\u00060\u0000j\u0002`\u00012\u0010\b\u0002\u0010\u0004\u001a\n\u0018\u00010\u0000j\u0004\u0018\u0001`\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00002\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\b\u0010\t"}, d2 = {"", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/primitives/MessageId;", "messageId", "before", "after", "", "invoke", "(JLjava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;)V", "loadMessages"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMessagesLoader$tryLoadMessages$3 extends o implements Function4<Long, Long, Long, Long, Unit> {
    public final /* synthetic */ StoreMessagesLoader this$0;

    /* compiled from: StoreMessagesLoader.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;", "it", "invoke", "(Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;)Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreMessagesLoader$tryLoadMessages$3$1 */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreMessagesLoader.ChannelLoadedState, StoreMessagesLoader.ChannelLoadedState> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public AnonymousClass1() {
            super(1);
        }

        public final StoreMessagesLoader.ChannelLoadedState invoke(StoreMessagesLoader.ChannelLoadedState channelLoadedState) {
            m.checkNotNullParameter(channelLoadedState, "it");
            return StoreMessagesLoader.ChannelLoadedState.copy$default(channelLoadedState, false, false, true, false, null, 27, null);
        }
    }

    /* compiled from: StoreMessagesLoader.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0002*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00000\u00002\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "Lcom/discord/api/message/Message;", "kotlin.jvm.PlatformType", "messages", "Lcom/discord/models/message/Message;", NotificationCompat.CATEGORY_CALL, "(Ljava/util/List;)Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreMessagesLoader$tryLoadMessages$3$2 */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2<T, R> implements b<List<? extends Message>, List<? extends com.discord.models.message.Message>> {
        public static final AnonymousClass2 INSTANCE = new AnonymousClass2();

        @Override // j0.k.b
        public /* bridge */ /* synthetic */ List<? extends com.discord.models.message.Message> call(List<? extends Message> list) {
            return call2((List<Message>) list);
        }

        /* renamed from: call */
        public final List<com.discord.models.message.Message> call2(List<Message> list) {
            m.checkNotNullExpressionValue(list, "messages");
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(list, 10));
            for (Message message : list) {
                arrayList.add(new com.discord.models.message.Message(message));
            }
            return arrayList;
        }
    }

    /* compiled from: StoreMessagesLoader.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "Lcom/discord/models/message/Message;", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreMessagesLoader$tryLoadMessages$3$3 */
    /* loaded from: classes.dex */
    public static final class AnonymousClass3 extends o implements Function1<List<? extends com.discord.models.message.Message>, Unit> {
        public final /* synthetic */ Long $after;
        public final /* synthetic */ Long $before;
        public final /* synthetic */ long $channelId;
        public final /* synthetic */ Long $messageId;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass3(long j, Long l, Long l2, Long l3) {
            super(1);
            StoreMessagesLoader$tryLoadMessages$3.this = r1;
            this.$channelId = j;
            this.$messageId = l;
            this.$before = l2;
            this.$after = l3;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(List<? extends com.discord.models.message.Message> list) {
            invoke2((List<com.discord.models.message.Message>) list);
            return Unit.a;
        }

        /* renamed from: invoke */
        public final void invoke2(List<com.discord.models.message.Message> list) {
            StoreMessagesLoader storeMessagesLoader = StoreMessagesLoader$tryLoadMessages$3.this.this$0;
            m.checkNotNullExpressionValue(list, "it");
            long j = this.$channelId;
            Long l = this.$messageId;
            storeMessagesLoader.handleLoadedMessages(list, j, l != null ? l.longValue() : 0L, this.$before, this.$after);
        }
    }

    /* compiled from: StoreMessagesLoader.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreMessagesLoader$tryLoadMessages$3$4 */
    /* loaded from: classes.dex */
    public static final class AnonymousClass4 extends o implements Function1<Error, Unit> {
        public final /* synthetic */ long $channelId;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass4(long j) {
            super(1);
            StoreMessagesLoader$tryLoadMessages$3.this = r1;
            this.$channelId = j;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Error error) {
            invoke2(error);
            return Unit.a;
        }

        /* renamed from: invoke */
        public final void invoke2(Error error) {
            m.checkNotNullParameter(error, "it");
            StoreMessagesLoader$tryLoadMessages$3.this.this$0.handleLoadMessagesError(this.$channelId);
        }
    }

    /* compiled from: StoreMessagesLoader.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lrx/Subscription;", "it", "", "invoke", "(Lrx/Subscription;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreMessagesLoader$tryLoadMessages$3$5 */
    /* loaded from: classes.dex */
    public static final class AnonymousClass5 extends o implements Function1<Subscription, Unit> {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass5() {
            super(1);
            StoreMessagesLoader$tryLoadMessages$3.this = r1;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Subscription subscription) {
            invoke2(subscription);
            return Unit.a;
        }

        /* renamed from: invoke */
        public final void invoke2(Subscription subscription) {
            m.checkNotNullParameter(subscription, "it");
            StoreMessagesLoader$tryLoadMessages$3.this.this$0.loadingMessagesSubscription = subscription;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreMessagesLoader$tryLoadMessages$3(StoreMessagesLoader storeMessagesLoader) {
        super(4);
        this.this$0 = storeMessagesLoader;
    }

    @Override // kotlin.jvm.functions.Function4
    public /* bridge */ /* synthetic */ Unit invoke(Long l, Long l2, Long l3, Long l4) {
        invoke(l.longValue(), l2, l3, l4);
        return Unit.a;
    }

    public final synchronized void invoke(long j, Long l, Long l2, Long l3) {
        Observable<List<Message>> observable;
        SerializedSubject serializedSubject;
        int i;
        int i2;
        this.this$0.channelLoadedStateUpdate(j, AnonymousClass1.INSTANCE);
        if (l != null && l.longValue() == 1) {
            RestAPI api = RestAPI.Companion.getApi();
            i = this.this$0.messageRequestSize;
            observable = api.getChannelMessages(j, l2, l3, Integer.valueOf(i));
            serializedSubject = this.this$0.channelMessagesLoadingSubject;
            serializedSubject.k.onNext(Boolean.TRUE);
            Observable F = ObservableExtensionsKt.restSubscribeOn(observable, false).F(AnonymousClass2.INSTANCE);
            m.checkNotNullExpressionValue(F, "messagesRequest\n        …messages.map(::Message) }");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.computationLatest(F), this.this$0.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new AnonymousClass5(), (r18 & 8) != 0 ? null : new AnonymousClass4(j), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass3(j, l, l2, l3));
        }
        if (l != null && l.longValue() == 0) {
            RestAPI api2 = RestAPI.Companion.getApi();
            i = this.this$0.messageRequestSize;
            observable = api2.getChannelMessages(j, l2, l3, Integer.valueOf(i));
            serializedSubject = this.this$0.channelMessagesLoadingSubject;
            serializedSubject.k.onNext(Boolean.TRUE);
            Observable F2 = ObservableExtensionsKt.restSubscribeOn(observable, false).F(AnonymousClass2.INSTANCE);
            m.checkNotNullExpressionValue(F2, "messagesRequest\n        …messages.map(::Message) }");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.computationLatest(F2), this.this$0.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new AnonymousClass5(), (r18 & 8) != 0 ? null : new AnonymousClass4(j), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass3(j, l, l2, l3));
        }
        RestAPI api3 = RestAPI.Companion.getApi();
        i2 = this.this$0.messageRequestSize;
        observable = api3.getChannelMessagesAround(j, i2, l.longValue());
        serializedSubject = this.this$0.channelMessagesLoadingSubject;
        serializedSubject.k.onNext(Boolean.TRUE);
        Observable F22 = ObservableExtensionsKt.restSubscribeOn(observable, false).F(AnonymousClass2.INSTANCE);
        m.checkNotNullExpressionValue(F22, "messagesRequest\n        …messages.map(::Message) }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.computationLatest(F22), this.this$0.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new AnonymousClass5(), (r18 & 8) != 0 ? null : new AnonymousClass4(j), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass3(j, l, l2, l3));
    }
}
