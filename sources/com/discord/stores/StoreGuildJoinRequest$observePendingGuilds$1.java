package com.discord.stores;

import com.discord.models.guild.Guild;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreGuildJoinRequest.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u00030\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "", "Lcom/discord/primitives/GuildId;", "Lcom/discord/models/guild/Guild;", "invoke", "()Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildJoinRequest$observePendingGuilds$1 extends o implements Function0<Map<Long, ? extends Guild>> {
    public final /* synthetic */ StoreGuildJoinRequest this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGuildJoinRequest$observePendingGuilds$1(StoreGuildJoinRequest storeGuildJoinRequest) {
        super(0);
        this.this$0 = storeGuildJoinRequest;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Map<Long, ? extends Guild> invoke() {
        Map<Long, ? extends Guild> map;
        map = this.this$0.pendingGuildsSnapshot;
        return map;
    }
}
