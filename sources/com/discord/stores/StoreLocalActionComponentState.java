package com.discord.stores;

import andhook.lib.HookHelper;
import com.discord.api.botuikit.SelectItem;
import com.discord.stores.StoreApplicationInteractions;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import d0.t.h0;
import d0.z.d.m;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreLocalActionComponentState.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\u001e\u001a\u00020\u001d\u0012\b\b\u0002\u0010*\u001a\u00020)¢\u0006\u0004\b,\u0010-J3\u0010\t\u001a(\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u001a\u0012\u0018\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u00020\u0002¢\u0006\u0004\b\t\u0010\nJ9\u0010\f\u001a.\u0012*\u0012(\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u001a\u0012\u0018\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u00020\u00020\u000b¢\u0006\u0004\b\f\u0010\rJ3\u0010\u0012\u001a\u00020\u00112\n\u0010\u000e\u001a\u00060\u0003j\u0002`\u00042\n\u0010\u000f\u001a\u00060\u0005j\u0002`\u00062\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\u0004\b\u0012\u0010\u0013J+\u0010\u0014\u001a\u00020\u00112\n\u0010\u000e\u001a\u00060\u0003j\u0002`\u00042\u000e\u0010\u000f\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006H\u0007¢\u0006\u0004\b\u0014\u0010\u0015J\u000f\u0010\u0016\u001a\u00020\u0011H\u0017¢\u0006\u0004\b\u0016\u0010\u0017RH\u0010\u0018\u001a(\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u001a\u0012\u0018\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u00020\u00028\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\n\"\u0004\b\u001b\u0010\u001cR\u0019\u0010\u001e\u001a\u00020\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010!R%\u0010%\u001a\u000e\u0012\u0004\u0012\u00020#\u0012\u0004\u0012\u00020$0\"8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u0019\u001a\u0004\b&\u0010\nR?\u0010'\u001a(\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u001a\u0012\u0018\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u00020\"8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010\u0019\u001a\u0004\b(\u0010\nR\u0016\u0010*\u001a\u00020)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010+¨\u0006."}, d2 = {"Lcom/discord/stores/StoreLocalActionComponentState;", "Lcom/discord/stores/StoreV2;", "", "", "Lcom/discord/primitives/MessageId;", "", "Lcom/discord/widgets/botuikit/ComponentIndex;", "", "Lcom/discord/api/botuikit/SelectItem;", "getSelectComponentSelectionsData", "()Ljava/util/Map;", "Lrx/Observable;", "observeSelectComponentSelections", "()Lrx/Observable;", "messageId", "componentIndex", "selectedItems", "", "setSelectComponentSelection", "(JILjava/util/List;)V", "clearState", "(JLjava/lang/Integer;)V", "snapshotData", "()V", "selectComponentSelectionsSnapshot", "Ljava/util/Map;", "getSelectComponentSelectionsSnapshot", "setSelectComponentSelectionsSnapshot", "(Ljava/util/Map;)V", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "getDispatcher", "()Lcom/discord/stores/Dispatcher;", "", "", "Lcom/discord/stores/StoreApplicationInteractions$ComponentLocation;", "componentInteractions", "getComponentInteractions", "selectComponentSelections", "getSelectComponentSelections", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreLocalActionComponentState extends StoreV2 {
    private final Map<String, StoreApplicationInteractions.ComponentLocation> componentInteractions;
    private final Dispatcher dispatcher;
    private final ObservationDeck observationDeck;
    private final Map<Long, Map<Integer, List<SelectItem>>> selectComponentSelections;
    private Map<Long, ? extends Map<Integer, ? extends List<SelectItem>>> selectComponentSelectionsSnapshot;

    public /* synthetic */ StoreLocalActionComponentState(Dispatcher dispatcher, ObservationDeck observationDeck, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(dispatcher, (i & 2) != 0 ? ObservationDeckProvider.get() : observationDeck);
    }

    @StoreThread
    public final void clearState(long j, Integer num) {
        Map<Integer, List<SelectItem>> map;
        Map<Integer, List<SelectItem>> mutableMap;
        if (this.selectComponentSelections.containsKey(Long.valueOf(j)) && (map = this.selectComponentSelections.get(Long.valueOf(j))) != null && (mutableMap = h0.toMutableMap(map)) != null) {
            if (num != null && mutableMap.containsKey(num)) {
                mutableMap.remove(num);
                this.selectComponentSelections.put(Long.valueOf(j), mutableMap);
                markChanged();
            } else if (num == null) {
                this.selectComponentSelections.remove(Long.valueOf(j));
                markChanged();
            }
        }
    }

    public final Map<String, StoreApplicationInteractions.ComponentLocation> getComponentInteractions() {
        return this.componentInteractions;
    }

    public final Dispatcher getDispatcher() {
        return this.dispatcher;
    }

    public final Map<Long, Map<Integer, List<SelectItem>>> getSelectComponentSelections() {
        return this.selectComponentSelections;
    }

    public final Map<Long, Map<Integer, List<SelectItem>>> getSelectComponentSelectionsData() {
        return this.selectComponentSelectionsSnapshot;
    }

    public final Map<Long, Map<Integer, List<SelectItem>>> getSelectComponentSelectionsSnapshot() {
        return this.selectComponentSelectionsSnapshot;
    }

    public final Observable<Map<Long, Map<Integer, List<SelectItem>>>> observeSelectComponentSelections() {
        Observable<Map<Long, Map<Integer, List<SelectItem>>>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreLocalActionComponentState$observeSelectComponentSelections$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    public final void setSelectComponentSelection(long j, int i, List<SelectItem> list) {
        m.checkNotNullParameter(list, "selectedItems");
        this.dispatcher.schedule(new StoreLocalActionComponentState$setSelectComponentSelection$1(this, j, list, i));
    }

    public final void setSelectComponentSelectionsSnapshot(Map<Long, ? extends Map<Integer, ? extends List<SelectItem>>> map) {
        m.checkNotNullParameter(map, "<set-?>");
        this.selectComponentSelectionsSnapshot = map;
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        super.snapshotData();
        this.selectComponentSelectionsSnapshot = new HashMap(this.selectComponentSelections);
    }

    public StoreLocalActionComponentState(Dispatcher dispatcher, ObservationDeck observationDeck) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
        this.componentInteractions = new LinkedHashMap();
        this.selectComponentSelections = new LinkedHashMap();
        this.selectComponentSelectionsSnapshot = h0.emptyMap();
    }
}
