package com.discord.stores;

import andhook.lib.HookHelper;
import com.discord.models.domain.ModelRtcLatencyRegion;
import com.discord.utilities.persister.Persister;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.Clock;
import d0.z.d.m;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreRtcRegion.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000t\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 72\u00020\u0001:\u00017B1\u0012\u0006\u00103\u001a\u000202\u0012\u0006\u0010#\u001a\u00020\"\u0012\u0006\u0010*\u001a\u00020)\u0012\u0006\u00100\u001a\u00020/\u0012\b\b\u0002\u0010&\u001a\u00020%¢\u0006\u0004\b5\u00106J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u001d\u0010\f\u001a\u00020\u00022\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0002¢\u0006\u0004\b\f\u0010\rJ+\u0010\u0012\u001a\u00020\u00112\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\t2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000e0\tH\u0002¢\u0006\u0004\b\u0012\u0010\u0013J%\u0010\u0017\u001a\u00020\u00112\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u000e0\t2\u0006\u0010\u0016\u001a\u00020\u0015H\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\r\u0010\u0019\u001a\u00020\u0002¢\u0006\u0004\b\u0019\u0010\u0004J\u0011\u0010\u001a\u001a\u0004\u0018\u00010\u000eH\u0007¢\u0006\u0004\b\u001a\u0010\u001bJ\u001f\u0010\u001e\u001a\u00020\u00112\u000e\u0010\u001d\u001a\n\u0018\u00010\u0015j\u0004\u0018\u0001`\u001cH\u0007¢\u0006\u0004\b\u001e\u0010\u001fJ\u000f\u0010!\u001a\u00020\u0002H\u0001¢\u0006\u0004\b \u0010\u0004R\u0016\u0010#\u001a\u00020\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$R\u0016\u0010&\u001a\u00020%8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b&\u0010'R\u0016\u0010\u0006\u001a\u00020\u00058\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0006\u0010(R\u0016\u0010*\u001a\u00020)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010+R\u001c\u0010-\u001a\b\u0012\u0004\u0012\u00020\u00050,8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b-\u0010.R\u0016\u00100\u001a\u00020/8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b0\u00101R\u0016\u00103\u001a\u0002028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b3\u00104¨\u00068"}, d2 = {"Lcom/discord/stores/StoreRtcRegion;", "", "", "fetchRtcLatencyTestRegionsIps", "()V", "Lcom/discord/stores/RtcLatencyTestResult;", "lastTestResult", "updateLastTestResult", "(Lcom/discord/stores/RtcLatencyTestResult;)V", "", "Lcom/discord/models/domain/ModelRtcLatencyRegion;", "regionsWithIps", "maybePerformLatencyTest", "(Ljava/util/List;)V", "", "list1", "list2", "", "areStringListsEqual", "(Ljava/util/List;Ljava/util/List;)Z", "newGeoRankedRegions", "", "timeNowMs", "shouldPerformLatencyTest", "(Ljava/util/List;J)Z", "init", "getPreferredRegion", "()Ljava/lang/String;", "Lcom/discord/primitives/GuildId;", "guildId", "shouldIncludePreferredRegion", "(Ljava/lang/Long;)Z", "handleConnectionOpen$app_productionGoogleRelease", "handleConnectionOpen", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "Lcom/discord/stores/RtcLatencyTestResult;", "Lcom/discord/stores/StoreMediaEngine;", "storeMediaEngine", "Lcom/discord/stores/StoreMediaEngine;", "Lcom/discord/utilities/persister/Persister;", "lastTestResultCache", "Lcom/discord/utilities/persister/Persister;", "Lcom/discord/stores/StoreExperiments;", "storeExperiments", "Lcom/discord/stores/StoreExperiments;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreMediaEngine;Lcom/discord/stores/StoreExperiments;Lcom/discord/utilities/rest/RestAPI;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreRtcRegion {
    public static final Companion Companion = new Companion(null);
    private static final int LATENCY_TEST_CACHE_TTL_MS = 86400000;
    private static final int MAX_LATENCY_TEST_CONN_OPEN_JITTER_MS = 30000;
    private static final int MIN_LATENCY_TEST_CONN_OPEN_JITTER_MS = 1000;
    private final Clock clock;
    private final Dispatcher dispatcher;
    private RtcLatencyTestResult lastTestResult;
    private final Persister<RtcLatencyTestResult> lastTestResultCache;
    private final RestAPI restAPI;
    private final StoreExperiments storeExperiments;
    private final StoreMediaEngine storeMediaEngine;

    /* compiled from: StoreRtcRegion.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004¨\u0006\t"}, d2 = {"Lcom/discord/stores/StoreRtcRegion$Companion;", "", "", "LATENCY_TEST_CACHE_TTL_MS", "I", "MAX_LATENCY_TEST_CONN_OPEN_JITTER_MS", "MIN_LATENCY_TEST_CONN_OPEN_JITTER_MS", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public StoreRtcRegion(Dispatcher dispatcher, Clock clock, StoreMediaEngine storeMediaEngine, StoreExperiments storeExperiments, RestAPI restAPI) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(storeMediaEngine, "storeMediaEngine");
        m.checkNotNullParameter(storeExperiments, "storeExperiments");
        m.checkNotNullParameter(restAPI, "restAPI");
        this.dispatcher = dispatcher;
        this.clock = clock;
        this.storeMediaEngine = storeMediaEngine;
        this.storeExperiments = storeExperiments;
        this.restAPI = restAPI;
        this.lastTestResultCache = new Persister<>("CACHE_KEY_LATENCY_TEST_RESULT", new RtcLatencyTestResult(null, null, 0L, 7, null));
        this.lastTestResult = new RtcLatencyTestResult(null, null, 0L, 7, null);
    }

    private final boolean areStringListsEqual(List<String> list, List<String> list2) {
        if (list.size() != list2.size()) {
            return false;
        }
        int i = 0;
        for (String str : list) {
            if (!m.areEqual(list2.get(i), str)) {
                return false;
            }
            i++;
        }
        return true;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void fetchRtcLatencyTestRegionsIps() {
        if (this.storeMediaEngine.hasNativeEngineEverInitialized()) {
            ObservableExtensionsKt.appSubscribe(this.restAPI.getRtcLatencyTestRegionsIps(), StoreRtcRegion.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreRtcRegion$fetchRtcLatencyTestRegionsIps$1(this));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void maybePerformLatencyTest(List<ModelRtcLatencyRegion> list) {
        this.dispatcher.schedule(new StoreRtcRegion$maybePerformLatencyTest$1(this, list));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final boolean shouldPerformLatencyTest(List<String> list, long j) {
        return this.lastTestResult.getLatencyRankedRegions().isEmpty() || !areStringListsEqual(list, this.lastTestResult.getGeoRankedRegions()) || j - this.lastTestResult.getLastTestTimestampMs() >= ((long) LATENCY_TEST_CACHE_TTL_MS);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateLastTestResult(RtcLatencyTestResult rtcLatencyTestResult) {
        this.lastTestResult = rtcLatencyTestResult;
        this.lastTestResultCache.set(rtcLatencyTestResult, true);
    }

    @StoreThread
    public final String getPreferredRegion() {
        if (!this.lastTestResult.getLatencyRankedRegions().isEmpty()) {
            return this.lastTestResult.getLatencyRankedRegions().get(0);
        }
        return null;
    }

    @StoreThread
    public final void handleConnectionOpen$app_productionGoogleRelease() {
        Observable<Long> d02 = Observable.d0(new Random().nextInt(29000) + 1000, TimeUnit.MILLISECONDS);
        m.checkNotNullExpressionValue(d02, "Observable.timer(fetchJi…), TimeUnit.MILLISECONDS)");
        ObservableExtensionsKt.appSubscribe(d02, StoreRtcRegion.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreRtcRegion$handleConnectionOpen$1(this));
    }

    public final void init() {
        this.lastTestResult = this.lastTestResultCache.get();
    }

    @StoreThread
    public final boolean shouldIncludePreferredRegion(Long l) {
        return getPreferredRegion() != null;
    }

    public /* synthetic */ StoreRtcRegion(Dispatcher dispatcher, Clock clock, StoreMediaEngine storeMediaEngine, StoreExperiments storeExperiments, RestAPI restAPI, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(dispatcher, clock, storeMediaEngine, storeExperiments, (i & 16) != 0 ? RestAPI.Companion.getApi() : restAPI);
    }
}
