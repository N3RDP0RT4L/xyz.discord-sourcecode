package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.Context;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelPayload;
import com.discord.models.experiments.domain.Experiment;
import com.discord.models.experiments.domain.ExperimentHash;
import com.discord.models.experiments.dto.GuildExperimentDto;
import com.discord.models.experiments.dto.UserExperimentDto;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.experiments.ExperimentRegistry;
import com.discord.utilities.experiments.ExperimentUtils;
import com.discord.utilities.experiments.RegisteredExperiment;
import com.discord.utilities.persister.Persister;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.Clock;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import d0.t.h0;
import d0.z.d.m;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreExperiments.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000¾\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 \u007f2\u00020\u0001:\u0001\u007fBA\u0012\u0006\u0010d\u001a\u00020c\u0012\u0006\u0010y\u001a\u00020x\u0012\u0006\u0010i\u001a\u00020h\u0012\u0006\u0010Z\u001a\u00020Y\u0012\u0006\u0010q\u001a\u00020p\u0012\u0006\u0010`\u001a\u00020_\u0012\b\b\u0002\u0010]\u001a\u00020\\¢\u0006\u0004\b}\u0010~J+\u0010\t\u001a\u00020\b2\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00022\u0006\u0010\u0007\u001a\u00020\u0006H\u0003¢\u0006\u0004\b\t\u0010\nJ%\u0010\r\u001a\u00020\b2\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\u0006\u0010\u0007\u001a\u00020\u0006H\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u000f\u001a\u00020\bH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u000f\u0010\u0011\u001a\u00020\bH\u0002¢\u0006\u0004\b\u0011\u0010\u0010J\u000f\u0010\u0012\u001a\u00020\bH\u0003¢\u0006\u0004\b\u0012\u0010\u0010J\u000f\u0010\u0013\u001a\u00020\bH\u0003¢\u0006\u0004\b\u0013\u0010\u0010J\u000f\u0010\u0014\u001a\u00020\bH\u0003¢\u0006\u0004\b\u0014\u0010\u0010J+\u0010\u001b\u001a\u00020\b2\u0006\u0010\u0016\u001a\u00020\u00152\n\u0010\u0018\u001a\u00060\u0003j\u0002`\u00172\u0006\u0010\u001a\u001a\u00020\u0019H\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ/\u0010\"\u001a\u00020\b2\u0006\u0010\u001d\u001a\u00020\u00152\u0006\u0010\u001f\u001a\u00020\u001e2\u0006\u0010 \u001a\u00020\u001e2\u0006\u0010!\u001a\u00020\u001eH\u0002¢\u0006\u0004\b\"\u0010#J3\u0010$\u001a\u00020\b2\u0006\u0010\u001d\u001a\u00020\u00152\n\u0010\u0018\u001a\u00060\u0003j\u0002`\u00172\u0006\u0010\u001f\u001a\u00020\u001e2\u0006\u0010!\u001a\u00020\u001eH\u0002¢\u0006\u0004\b$\u0010%J#\u0010)\u001a\u00020\u00062\u0006\u0010&\u001a\u00020\u00152\n\u0010(\u001a\u00060\u0003j\u0002`'H\u0002¢\u0006\u0004\b)\u0010*J\u0017\u0010+\u001a\u00020\b2\u0006\u0010&\u001a\u00020\u0015H\u0002¢\u0006\u0004\b+\u0010,J\u000f\u0010-\u001a\u00020\bH\u0002¢\u0006\u0004\b-\u0010\u0010J\u001f\u0010.\u001a\u0012\u0012\u0004\u0012\u00020\u0015\u0012\b\u0012\u00060\u0003j\u0002`'0\u0002H\u0002¢\u0006\u0004\b.\u0010/J\u001f\u00100\u001a\u00020\b2\u0006\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u001f\u001a\u00020\u001eH\u0003¢\u0006\u0004\b0\u00101J\u0017\u00102\u001a\u00020\b2\u0006\u0010\u0016\u001a\u00020\u0015H\u0003¢\u0006\u0004\b2\u0010,J\u0013\u00104\u001a\b\u0012\u0004\u0012\u00020\u000603¢\u0006\u0004\b4\u00105J\u001f\u00107\u001a\u0004\u0018\u00010\u00192\u0006\u0010\u001d\u001a\u00020\u00152\u0006\u00106\u001a\u00020\u0006¢\u0006\u0004\b7\u00108J%\u00109\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u0019032\u0006\u0010\u001d\u001a\u00020\u00152\u0006\u00106\u001a\u00020\u0006¢\u0006\u0004\b9\u0010:J+\u0010;\u001a\u0004\u0018\u00010\u00192\u0006\u0010\u0016\u001a\u00020\u00152\n\u0010\u0018\u001a\u00060\u0003j\u0002`\u00172\u0006\u00106\u001a\u00020\u0006¢\u0006\u0004\b;\u0010<J1\u0010=\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u0019032\u0006\u0010\u0016\u001a\u00020\u00152\n\u0010\u0018\u001a\u00060\u0003j\u0002`\u00172\u0006\u00106\u001a\u00020\u0006¢\u0006\u0004\b=\u0010>J\u001f\u0010?\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u001e0\u000203¢\u0006\u0004\b?\u00105J\u0013\u0010@\u001a\b\u0012\u0004\u0012\u00020\u000603¢\u0006\u0004\b@\u00105J\u001d\u0010A\u001a\u00020\b2\u0006\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u001f\u001a\u00020\u001e¢\u0006\u0004\bA\u00101J\u0015\u0010B\u001a\u00020\b2\u0006\u0010\u0016\u001a\u00020\u0015¢\u0006\u0004\bB\u0010,J\u0017\u0010E\u001a\u00020\b2\u0006\u0010D\u001a\u00020CH\u0017¢\u0006\u0004\bE\u0010FJ\u0017\u0010I\u001a\u00020\b2\u0006\u0010H\u001a\u00020GH\u0007¢\u0006\u0004\bI\u0010JJ\u0019\u0010L\u001a\u00020\b2\b\u0010K\u001a\u0004\u0018\u00010\u0015H\u0007¢\u0006\u0004\bL\u0010,J\u0019\u0010N\u001a\u00020\b2\b\u0010M\u001a\u0004\u0018\u00010\u0015H\u0007¢\u0006\u0004\bN\u0010,J\u000f\u0010O\u001a\u00020\bH\u0017¢\u0006\u0004\bO\u0010\u0010J%\u0010R\u001a\u0004\u0018\u00010\u00192\u0006\u0010\u0016\u001a\u00020\u00152\n\u0010\u0018\u001a\u00060\u0003j\u0002`\u0017H\u0000¢\u0006\u0004\bP\u0010QR\"\u0010S\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u001e0\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bS\u0010TR2\u0010W\u001a\u001e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u00190Uj\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u0019`V8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bW\u0010XR\u0016\u0010Z\u001a\u00020Y8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bZ\u0010[R\u0016\u0010]\u001a\u00020\\8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b]\u0010^R\u0016\u0010`\u001a\u00020_8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b`\u0010aR\u0018\u0010M\u001a\u0004\u0018\u00010\u00158\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bM\u0010bR\u0016\u0010d\u001a\u00020c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bd\u0010eR2\u0010f\u001a\u001e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\f0Uj\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\f`V8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bf\u0010XR2\u0010g\u001a\u001e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u001e0Uj\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u001e`V8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bg\u0010XR\u0016\u0010i\u001a\u00020h8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bi\u0010jR(\u0010l\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00020k8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bl\u0010mR\"\u0010o\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0n0k8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bo\u0010mR\u0016\u0010q\u001a\u00020p8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bq\u0010rR\u0018\u0010K\u001a\u0004\u0018\u00010\u00158\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bK\u0010bR:\u0010s\u001a&\u0012\u0004\u0012\u00020\u0015\u0012\b\u0012\u00060\u0003j\u0002`'0Uj\u0012\u0012\u0004\u0012\u00020\u0015\u0012\b\u0012\u00060\u0003j\u0002`'`V8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bs\u0010XR\"\u0010t\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\f0\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bt\u0010TR\"\u0010u\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bu\u0010TR\u0016\u0010v\u001a\u00020\u00068\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bv\u0010wR\u0016\u0010y\u001a\u00020x8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\by\u0010zR2\u0010{\u001a\u001e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040Uj\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004`V8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b{\u0010XR(\u0010|\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u001e0\u00020k8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b|\u0010m¨\u0006\u0080\u0001"}, d2 = {"Lcom/discord/stores/StoreExperiments;", "Lcom/discord/stores/StoreV2;", "", "", "Lcom/discord/models/experiments/dto/UserExperimentDto;", "experiments", "", "doCache", "", "handleLoadedUserExperiments", "(Ljava/util/Map;Z)V", "", "Lcom/discord/models/experiments/dto/GuildExperimentDto;", "handleLoadedGuildExperiments", "(Ljava/util/Collection;Z)V", "cacheUserExperiments", "()V", "cacheGuildExperiments", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_RESET, "tryInitializeExperiments", "setInitialized", "", "experimentName", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/models/experiments/domain/Experiment;", "experiment", "memoizeGuildExperiment", "(Ljava/lang/String;JLcom/discord/models/experiments/domain/Experiment;)V", ModelAuditLogEntry.CHANGE_KEY_NAME, "", "bucket", "population", "revision", "trackExposureToUserExperiment", "(Ljava/lang/String;III)V", "trackExposureToGuildExperiment", "(Ljava/lang/String;JII)V", "exposureKey", "Lcom/discord/primitives/Timestamp;", "nowMs", "wasExperimentExposureTrackedRecently", "(Ljava/lang/String;J)Z", "didTrackExposureToExperiment", "(Ljava/lang/String;)V", "cacheExperimentTrackedExposureTimestamps", "loadCachedExperimentTrackedExposureTimestamps", "()Ljava/util/Map;", "handleSetOverride", "(Ljava/lang/String;I)V", "handleClearOverride", "Lrx/Observable;", "isInitialized", "()Lrx/Observable;", "trackExposure", "getUserExperiment", "(Ljava/lang/String;Z)Lcom/discord/models/experiments/domain/Experiment;", "observeUserExperiment", "(Ljava/lang/String;Z)Lrx/Observable;", "getGuildExperiment", "(Ljava/lang/String;JZ)Lcom/discord/models/experiments/domain/Experiment;", "observeGuildExperiment", "(Ljava/lang/String;JZ)Lrx/Observable;", "observeOverrides", "getExperimentalAlpha", "setOverride", "clearOverride", "Landroid/content/Context;", "context", "init", "(Landroid/content/Context;)V", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "authToken", "handleAuthToken", "fingerprint", "handleFingerprint", "snapshotData", "getMemoizedGuildExperiment$app_productionGoogleRelease", "(Ljava/lang/String;J)Lcom/discord/models/experiments/domain/Experiment;", "getMemoizedGuildExperiment", "experimentOverridesSnapshot", "Ljava/util/Map;", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "memoizedGuildExperiments", "Ljava/util/HashMap;", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreGuilds;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/stores/StoreGuildMemberCounts;", "storeGuildMemberCounts", "Lcom/discord/stores/StoreGuildMemberCounts;", "Ljava/lang/String;", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "guildExperiments", "experimentOverrides", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreUser;", "Lcom/discord/utilities/persister/Persister;", "userExperimentsCache", "Lcom/discord/utilities/persister/Persister;", "", "guildExperimentsCache", "Lcom/discord/stores/StoreAuthentication;", "storeAuthentication", "Lcom/discord/stores/StoreAuthentication;", "experimentTrackedExposureTimestamps", "guildExperimentsSnapshot", "userExperimentsSnapshot", "initialized", "Z", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "userExperiments", "experimentOverridesCache", HookHelper.constructorName, "(Lcom/discord/utilities/time/Clock;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreAuthentication;Lcom/discord/stores/StoreGuildMemberCounts;Lcom/discord/stores/updates/ObservationDeck;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreExperiments extends StoreV2 {
    private static final long DISCORD_TESTERS_GUILD_ID = 197038439483310086L;
    private static final String EXPERIMENT_OVERRIDES_CACHE_KEY = "EXPERIMENT_OVERRIDES_CACHE_KEY";
    private static final String EXPERIMENT_TRACKED_EXPOSURE_TIMESTAMPS_CACHE_KEY = "EXPERIMENT_TRIGGER_TIMESTAMPS_CACHE_KEY";
    private static final String GUILD_EXPERIMENTS_CACHE_KEY = "GUILD_EXPERIMENTS_CACHE_KEY";
    private static final String UNINITIALIZED = "UNINITIALIZED";
    private static final String USER_EXPERIMENTS_CACHE_KEY = "USER_EXPERIMENTS_CACHE_KEY";
    private String authToken;
    private final Clock clock;
    private final Dispatcher dispatcher;
    private final HashMap<String, Integer> experimentOverrides;
    private final Persister<Map<String, Integer>> experimentOverridesCache;
    private Map<String, Integer> experimentOverridesSnapshot;
    private final HashMap<String, Long> experimentTrackedExposureTimestamps;
    private String fingerprint;
    private final HashMap<Long, GuildExperimentDto> guildExperiments;
    private final Persister<List<GuildExperimentDto>> guildExperimentsCache;
    private Map<Long, GuildExperimentDto> guildExperimentsSnapshot;
    private boolean initialized;
    private final HashMap<String, Experiment> memoizedGuildExperiments;
    private final ObservationDeck observationDeck;
    private final StoreAuthentication storeAuthentication;
    private final StoreGuildMemberCounts storeGuildMemberCounts;
    private final StoreGuilds storeGuilds;
    private final StoreUser storeUser;
    private final HashMap<Long, UserExperimentDto> userExperiments;
    private final Persister<Map<Long, UserExperimentDto>> userExperimentsCache;
    private Map<Long, UserExperimentDto> userExperimentsSnapshot;
    public static final Companion Companion = new Companion(null);
    private static final StoreExperiments$Companion$InitializedUpdateSource$1 InitializedUpdateSource = new ObservationDeck.UpdateSource() { // from class: com.discord.stores.StoreExperiments$Companion$InitializedUpdateSource$1
    };
    private static final StoreExperiments$Companion$ExperimentOverridesUpdateSource$1 ExperimentOverridesUpdateSource = new ObservationDeck.UpdateSource() { // from class: com.discord.stores.StoreExperiments$Companion$ExperimentOverridesUpdateSource$1
    };

    /* compiled from: StoreExperiments.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\b\u0004\n\u0002\b\b*\u0002\n\u000e\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0004\u001a\u00060\u0002j\u0002`\u00038\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0004\u0010\u0005R\u0016\u0010\u0007\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0007\u0010\bR\u0016\u0010\t\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\t\u0010\bR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u0016\u0010\r\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\r\u0010\bR\u0016\u0010\u000f\u001a\u00020\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0011\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0011\u0010\bR\u0016\u0010\u0012\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0012\u0010\b¨\u0006\u0015"}, d2 = {"Lcom/discord/stores/StoreExperiments$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "DISCORD_TESTERS_GUILD_ID", "J", "", StoreExperiments.EXPERIMENT_OVERRIDES_CACHE_KEY, "Ljava/lang/String;", "EXPERIMENT_TRACKED_EXPOSURE_TIMESTAMPS_CACHE_KEY", "com/discord/stores/StoreExperiments$Companion$ExperimentOverridesUpdateSource$1", "ExperimentOverridesUpdateSource", "Lcom/discord/stores/StoreExperiments$Companion$ExperimentOverridesUpdateSource$1;", StoreExperiments.GUILD_EXPERIMENTS_CACHE_KEY, "com/discord/stores/StoreExperiments$Companion$InitializedUpdateSource$1", "InitializedUpdateSource", "Lcom/discord/stores/StoreExperiments$Companion$InitializedUpdateSource$1;", StoreExperiments.UNINITIALIZED, StoreExperiments.USER_EXPERIMENTS_CACHE_KEY, HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ StoreExperiments(Clock clock, Dispatcher dispatcher, StoreUser storeUser, StoreGuilds storeGuilds, StoreAuthentication storeAuthentication, StoreGuildMemberCounts storeGuildMemberCounts, ObservationDeck observationDeck, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(clock, dispatcher, storeUser, storeGuilds, storeAuthentication, storeGuildMemberCounts, (i & 64) != 0 ? ObservationDeckProvider.get() : observationDeck);
    }

    private final void cacheExperimentTrackedExposureTimestamps() {
        getPrefs().edit().putString(EXPERIMENT_TRACKED_EXPOSURE_TIMESTAMPS_CACHE_KEY, new Gson().m(this.experimentTrackedExposureTimestamps)).apply();
    }

    private final void cacheGuildExperiments() {
        boolean z2;
        Collection<GuildExperimentDto> values = this.guildExperiments.values();
        m.checkNotNullExpressionValue(values, "guildExperiments.values");
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = values.iterator();
        while (true) {
            boolean z3 = true;
            if (it.hasNext()) {
                Object next = it.next();
                GuildExperimentDto guildExperimentDto = (GuildExperimentDto) next;
                Collection<RegisteredExperiment> values2 = ExperimentRegistry.INSTANCE.getRegisteredExperiments().values();
                m.checkNotNullExpressionValue(values2, "ExperimentRegistry.registeredExperiments.values");
                if (!(values2 instanceof Collection) || !values2.isEmpty()) {
                    for (RegisteredExperiment registeredExperiment : values2) {
                        if (ExperimentHash.INSTANCE.from(registeredExperiment.getName()) == guildExperimentDto.getExperimentIdHash()) {
                            z2 = true;
                            continue;
                        } else {
                            z2 = false;
                            continue;
                        }
                        if (z2) {
                            break;
                        }
                    }
                }
                z3 = false;
                if (z3) {
                    arrayList.add(next);
                }
            } else {
                this.guildExperimentsCache.set(arrayList, true);
                return;
            }
        }
    }

    private final void cacheUserExperiments() {
        boolean z2;
        HashMap<Long, UserExperimentDto> hashMap = this.userExperiments;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        Iterator<Map.Entry<Long, UserExperimentDto>> it = hashMap.entrySet().iterator();
        while (true) {
            boolean z3 = true;
            if (it.hasNext()) {
                Map.Entry<Long, UserExperimentDto> next = it.next();
                Collection<RegisteredExperiment> values = ExperimentRegistry.INSTANCE.getRegisteredExperiments().values();
                m.checkNotNullExpressionValue(values, "ExperimentRegistry.registeredExperiments.values");
                if (!(values instanceof Collection) || !values.isEmpty()) {
                    for (RegisteredExperiment registeredExperiment : values) {
                        if (ExperimentHash.INSTANCE.from(registeredExperiment.getName()) == next.getValue().getNameHash()) {
                            z2 = true;
                            continue;
                        } else {
                            z2 = false;
                            continue;
                        }
                        if (z2) {
                            break;
                        }
                    }
                }
                z3 = false;
                if (z3) {
                    linkedHashMap.put(next.getKey(), next.getValue());
                }
            } else {
                this.userExperimentsCache.set(linkedHashMap, true);
                return;
            }
        }
    }

    private final void didTrackExposureToExperiment(String str) {
        this.experimentTrackedExposureTimestamps.put(str, Long.valueOf(this.clock.currentTimeMillis()));
        cacheExperimentTrackedExposureTimestamps();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleClearOverride(String str) {
        this.experimentOverrides.remove(str);
        markChanged(ExperimentOverridesUpdateSource);
    }

    @StoreThread
    private final void handleLoadedGuildExperiments(Collection<GuildExperimentDto> collection, boolean z2) {
        this.guildExperiments.clear();
        this.memoizedGuildExperiments.clear();
        AbstractMap abstractMap = this.guildExperiments;
        for (Object obj : collection) {
            abstractMap.put(Long.valueOf(((GuildExperimentDto) obj).getExperimentIdHash()), obj);
        }
        if (z2) {
            cacheGuildExperiments();
        }
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleLoadedUserExperiments(Map<Long, UserExperimentDto> map, boolean z2) {
        this.userExperiments.clear();
        this.userExperiments.putAll(map);
        if (z2) {
            cacheUserExperiments();
        }
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleSetOverride(String str, int i) {
        this.experimentOverrides.put(str, Integer.valueOf(i));
        markChanged(ExperimentOverridesUpdateSource);
    }

    private final Map<String, Long> loadCachedExperimentTrackedExposureTimestamps() {
        String string = getPrefs().getString(EXPERIMENT_TRACKED_EXPOSURE_TIMESTAMPS_CACHE_KEY, null);
        if (string == null) {
            return h0.emptyMap();
        }
        Object g = new Gson().g(string, new TypeToken<Map<String, ? extends Long>>() { // from class: com.discord.stores.StoreExperiments$loadCachedExperimentTrackedExposureTimestamps$typeToken$1
        }.getType());
        m.checkNotNullExpressionValue(g, "Gson().fromJson(json, typeToken)");
        return (Map) g;
    }

    private final synchronized void memoizeGuildExperiment(String str, long j, Experiment experiment) {
        this.memoizedGuildExperiments.put(str + MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR + j, experiment);
    }

    @StoreThread
    private final void reset() {
        if (this.authToken == null) {
            this.userExperiments.clear();
            this.guildExperiments.clear();
            this.userExperimentsCache.set(new HashMap(), true);
            this.guildExperimentsCache.set(new ArrayList(), true);
            this.initialized = false;
            markChanged();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void setInitialized() {
        this.initialized = true;
        markChanged(InitializedUpdateSource);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void trackExposureToGuildExperiment(String str, long j, int i, int i2) {
        String str2 = str + MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR + j;
        if (!wasExperimentExposureTrackedRecently(str2, this.clock.currentTimeMillis())) {
            AnalyticsTracker.guildExperimentTriggered(str, i2, i, j);
            didTrackExposureToExperiment(str2);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void trackExposureToUserExperiment(String str, int i, int i2, int i3) {
        if (!wasExperimentExposureTrackedRecently(str, this.clock.currentTimeMillis())) {
            AnalyticsTracker.userExperimentTriggered(str, i3, i2, i);
            didTrackExposureToExperiment(str);
        }
    }

    @StoreThread
    private final void tryInitializeExperiments() {
        if (!this.initialized && !m.areEqual(this.authToken, UNINITIALIZED) && !m.areEqual(this.fingerprint, UNINITIALIZED)) {
            this.initialized = true;
            if (this.authToken != null) {
                setInitialized();
                return;
            }
            Observable c02 = ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().getExperiments(), false, 1, null).c0(2000L, TimeUnit.MILLISECONDS);
            m.checkNotNullExpressionValue(c02, "RestAPI\n          .api\n …0, TimeUnit.MILLISECONDS)");
            ObservableExtensionsKt.appSubscribe(c02, StoreExperiments.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new StoreExperiments$tryInitializeExperiments$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreExperiments$tryInitializeExperiments$1(this));
        }
    }

    private final boolean wasExperimentExposureTrackedRecently(String str, long j) {
        Long l = this.experimentTrackedExposureTimestamps.get(str);
        if (l == null) {
            l = Long.MIN_VALUE;
        }
        m.checkNotNullExpressionValue(l, "experimentTrackedExposur…y] ?: Timestamp.MIN_VALUE");
        return j - 604800000 < l.longValue();
    }

    public final void clearOverride(String str) {
        m.checkNotNullParameter(str, "experimentName");
        this.dispatcher.schedule(new StoreExperiments$clearOverride$1(this, str));
    }

    public final Observable<Boolean> getExperimentalAlpha() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this.storeUser, this.storeGuilds}, false, null, null, new StoreExperiments$getExperimentalAlpha$1(this), 14, null);
    }

    public final Experiment getGuildExperiment(String str, long j, boolean z2) {
        m.checkNotNullParameter(str, "experimentName");
        long from = ExperimentHash.INSTANCE.from(str);
        Integer num = this.experimentOverridesSnapshot.get(str);
        GuildExperimentDto guildExperimentDto = this.guildExperimentsSnapshot.get(Long.valueOf(from));
        if (num != null) {
            return new Experiment(guildExperimentDto != null ? guildExperimentDto.getRevision() : 0, num.intValue(), 0, true, StoreExperiments$getGuildExperiment$1.INSTANCE);
        }
        Experiment memoizedGuildExperiment$app_productionGoogleRelease = getMemoizedGuildExperiment$app_productionGoogleRelease(j + MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR + str, j);
        if (memoizedGuildExperiment$app_productionGoogleRelease != null) {
            return memoizedGuildExperiment$app_productionGoogleRelease;
        }
        if (guildExperimentDto == null) {
            return null;
        }
        int computeGuildExperimentBucket = ExperimentUtils.INSTANCE.computeGuildExperimentBucket(str, j, this.storeGuildMemberCounts.getApproximateMemberCount(j), this.storeGuilds.getGuild(j), guildExperimentDto);
        int revision = guildExperimentDto.getRevision();
        Experiment experiment = new Experiment(revision, computeGuildExperimentBucket, 0, false, new StoreExperiments$getGuildExperiment$experiment$1(this, str, j, computeGuildExperimentBucket, revision));
        if (z2) {
            experiment.getTrackExposure().invoke();
        }
        memoizeGuildExperiment(str, j, experiment);
        return experiment;
    }

    public final synchronized Experiment getMemoizedGuildExperiment$app_productionGoogleRelease(String str, long j) {
        m.checkNotNullParameter(str, "experimentName");
        return this.memoizedGuildExperiments.get(str + MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR + j);
    }

    public final Experiment getUserExperiment(String str, boolean z2) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        long from = ExperimentHash.INSTANCE.from(str);
        Integer num = this.experimentOverridesSnapshot.get(str);
        UserExperimentDto userExperimentDto = this.userExperimentsSnapshot.get(Long.valueOf(from));
        if (num != null) {
            return new Experiment(userExperimentDto != null ? userExperimentDto.getRevision() : 0, num.intValue(), userExperimentDto != null ? userExperimentDto.getPopulation() : 0, true, StoreExperiments$getUserExperiment$1.INSTANCE);
        } else if (userExperimentDto == null) {
            return null;
        } else {
            int bucket = userExperimentDto.getBucket();
            int population = userExperimentDto.getPopulation();
            int revision = userExperimentDto.getRevision();
            Experiment experiment = new Experiment(revision, bucket, population, false, new StoreExperiments$getUserExperiment$experiment$1(this, str, bucket, population, revision));
            if (z2) {
                experiment.getTrackExposure().invoke();
            }
            return experiment;
        }
    }

    @StoreThread
    public final void handleAuthToken(String str) {
        this.authToken = str;
        reset();
        tryInitializeExperiments();
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        Map<Long, UserExperimentDto> experiments = modelPayload.getExperiments();
        m.checkNotNullExpressionValue(experiments, "payload.experiments");
        handleLoadedUserExperiments(experiments, true);
        List<GuildExperimentDto> guildExperiments = modelPayload.getGuildExperiments();
        if (guildExperiments != null) {
            m.checkNotNullExpressionValue(guildExperiments, "it");
            handleLoadedGuildExperiments(guildExperiments, true);
        }
    }

    @StoreThread
    public final void handleFingerprint(String str) {
        this.fingerprint = str;
        tryInitializeExperiments();
    }

    @Override // com.discord.stores.Store
    @StoreThread
    public void init(Context context) {
        m.checkNotNullParameter(context, "context");
        super.init(context);
        this.experimentTrackedExposureTimestamps.putAll(loadCachedExperimentTrackedExposureTimestamps());
        this.experimentOverrides.putAll(this.experimentOverridesCache.get());
        handleLoadedUserExperiments(this.userExperimentsCache.get(), false);
        handleLoadedGuildExperiments(this.guildExperimentsCache.get(), false);
        markChanged(this, ExperimentOverridesUpdateSource);
    }

    public final Observable<Boolean> isInitialized() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{InitializedUpdateSource}, false, null, null, new StoreExperiments$isInitialized$1(this), 14, null);
    }

    public final Observable<Experiment> observeGuildExperiment(String str, long j, boolean z2) {
        m.checkNotNullParameter(str, "experimentName");
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreExperiments$observeGuildExperiment$1(this, str, j, z2), 14, null);
    }

    public final Observable<Map<String, Integer>> observeOverrides() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{ExperimentOverridesUpdateSource}, false, null, null, new StoreExperiments$observeOverrides$1(this), 14, null);
    }

    public final Observable<Experiment> observeUserExperiment(String str, boolean z2) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        Observable<Experiment> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreExperiments$observeUserExperiment$1(this, str, z2), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR…  .distinctUntilChanged()");
        return q;
    }

    public final void setOverride(String str, int i) {
        m.checkNotNullParameter(str, "experimentName");
        this.dispatcher.schedule(new StoreExperiments$setOverride$1(this, i, str));
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        super.snapshotData();
        this.userExperimentsSnapshot = new HashMap(this.userExperiments);
        this.guildExperimentsSnapshot = new HashMap(this.guildExperiments);
        if (getUpdateSources().contains(ExperimentOverridesUpdateSource)) {
            this.experimentOverridesSnapshot = new HashMap(this.experimentOverrides);
            this.experimentOverridesCache.set(this.experimentOverrides, true);
        }
    }

    public StoreExperiments(Clock clock, Dispatcher dispatcher, StoreUser storeUser, StoreGuilds storeGuilds, StoreAuthentication storeAuthentication, StoreGuildMemberCounts storeGuildMemberCounts, ObservationDeck observationDeck) {
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(storeUser, "storeUser");
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        m.checkNotNullParameter(storeAuthentication, "storeAuthentication");
        m.checkNotNullParameter(storeGuildMemberCounts, "storeGuildMemberCounts");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.clock = clock;
        this.dispatcher = dispatcher;
        this.storeUser = storeUser;
        this.storeGuilds = storeGuilds;
        this.storeAuthentication = storeAuthentication;
        this.storeGuildMemberCounts = storeGuildMemberCounts;
        this.observationDeck = observationDeck;
        this.authToken = UNINITIALIZED;
        this.fingerprint = UNINITIALIZED;
        this.userExperimentsSnapshot = h0.emptyMap();
        this.guildExperimentsSnapshot = h0.emptyMap();
        this.experimentOverridesSnapshot = h0.emptyMap();
        this.userExperiments = new HashMap<>();
        this.guildExperiments = new HashMap<>();
        this.experimentOverrides = new HashMap<>();
        this.memoizedGuildExperiments = new HashMap<>();
        this.experimentTrackedExposureTimestamps = new HashMap<>();
        this.userExperimentsCache = new Persister<>(USER_EXPERIMENTS_CACHE_KEY, new HashMap());
        this.guildExperimentsCache = new Persister<>(GUILD_EXPERIMENTS_CACHE_KEY, new ArrayList());
        this.experimentOverridesCache = new Persister<>(EXPERIMENT_OVERRIDES_CACHE_KEY, new HashMap());
    }
}
