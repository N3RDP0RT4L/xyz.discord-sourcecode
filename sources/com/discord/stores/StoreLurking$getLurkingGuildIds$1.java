package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.stores.StoreLurking;
import j0.k.b;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
/* compiled from: StoreLurking.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0010%\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0003\u0010\t\u001a\u001e\u0012\b\u0012\u00060\u0001j\u0002`\u0002 \u0004*\u000e\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0018\u00010\u00060\u00062.\u0010\u0005\u001a*\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003 \u0004*\u0014\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"", "", "Lcom/discord/primitives/GuildId;", "Lcom/discord/stores/StoreLurking$LurkContext;", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Map;)Ljava/util/Set;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreLurking$getLurkingGuildIds$1<T, R> implements b<Map<Long, StoreLurking.LurkContext>, Set<? extends Long>> {
    public static final StoreLurking$getLurkingGuildIds$1 INSTANCE = new StoreLurking$getLurkingGuildIds$1();

    public final Set<Long> call(Map<Long, StoreLurking.LurkContext> map) {
        return map.keySet();
    }
}
