package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.permission.Permission;
import com.discord.api.role.GuildRole;
import com.discord.api.stageinstance.StageInstance;
import com.discord.api.voice.state.StageRequestToSpeakState;
import com.discord.api.voice.state.VoiceState;
import com.discord.models.guild.Guild;
import com.discord.models.guild.UserGuildMember;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.widgets.stage.StageChannelNotifications;
import com.discord.widgets.stage.StageRoles;
import com.discord.widgets.stage.model.StageChannel;
import d0.d0.f;
import d0.f0.n;
import d0.f0.q;
import d0.t.g0;
import d0.t.h0;
import d0.t.n0;
import d0.t.o;
import d0.t.o0;
import d0.t.r;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.sequences.Sequence;
import rx.Observable;
/* compiled from: StoreStageChannels.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000â\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010#\n\u0002\b\u0007\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \u0088\u00012\u00020\u0001:\u0004\u0088\u0001\u0089\u0001BG\u0012\u0006\u0010w\u001a\u00020v\u0012\b\u0010\u0081\u0001\u001a\u00030\u0080\u0001\u0012\b\u0010\u0084\u0001\u001a\u00030\u0083\u0001\u0012\u0006\u0010d\u001a\u00020c\u0012\u0006\u0010~\u001a\u00020}\u0012\u0006\u0010i\u001a\u00020h\u0012\b\b\u0002\u0010z\u001a\u00020y¢\u0006\u0006\b\u0086\u0001\u0010\u0087\u0001J;\u0010\r\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\b\u0010\b\u001a\u0004\u0018\u00010\u00072\u0006\u0010\t\u001a\u00020\u0007H\u0002ø\u0001\u0000ø\u0001\u0001¢\u0006\u0004\b\u000b\u0010\fJ1\u0010\u0011\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010\u000e\u001a\u00020\u0007H\u0002ø\u0001\u0000ø\u0001\u0001¢\u0006\u0004\b\u000f\u0010\u0010J;\u0010\u0016\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010\u0012\u001a\u00020\u00072\b\b\u0002\u0010\u0013\u001a\u00020\u0007H\u0002ø\u0001\u0000ø\u0001\u0001¢\u0006\u0004\b\u0014\u0010\u0015J!\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00040\u00192\n\u0010\u0018\u001a\u00060\u0004j\u0002`\u0017H\u0002¢\u0006\u0004\b\u001a\u0010\u001bJd\u0010#\u001a\u00020\u00072\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010\u0003\u001a\u00020\u00022\u001a\b\u0002\u0010\u001e\u001a\u0014\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u001d\u0018\u00010\u001c2\u001a\b\u0002\u0010\u000e\u001a\u0014\u0012\b\u0012\u00060\u0004j\u0002`\u001f\u0012\u0004\u0012\u00020 \u0018\u00010\u001cH\u0002ø\u0001\u0000ø\u0001\u0002ø\u0001\u0001¢\u0006\u0004\b!\u0010\"J]\u0010'\u001a\u0004\u0018\u00010&2\u0006\u0010\u0003\u001a\u00020\u00022\n\b\u0002\u0010%\u001a\u0004\u0018\u00010$2\u001a\b\u0002\u0010\u001e\u001a\u0014\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u001d\u0018\u00010\u001c2\u001a\b\u0002\u0010\u000e\u001a\u0014\u0012\b\u0012\u00060\u0004j\u0002`\u001f\u0012\u0004\u0012\u00020 \u0018\u00010\u001cH\u0002¢\u0006\u0004\b'\u0010(J\u000f\u0010)\u001a\u00020\nH\u0002¢\u0006\u0004\b)\u0010*J0\u0010/\u001a\u0004\u0018\u00010\u00072\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\n\u0010,\u001a\u00060\u0004j\u0002`+ø\u0001\u0000ø\u0001\u0002ø\u0001\u0001¢\u0006\u0004\b-\u0010.J$\u00102\u001a\u0004\u0018\u00010\u00072\n\u0010,\u001a\u00060\u0004j\u0002`+ø\u0001\u0000ø\u0001\u0002ø\u0001\u0001¢\u0006\u0004\b0\u00101J0\u00104\u001a\u0004\u0018\u00010\u00072\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\n\u0010,\u001a\u00060\u0004j\u0002`+ø\u0001\u0000ø\u0001\u0002ø\u0001\u0001¢\u0006\u0004\b3\u0010.J$\u00106\u001a\u0004\u0018\u00010\u00072\n\u0010,\u001a\u00060\u0004j\u0002`+ø\u0001\u0000ø\u0001\u0002ø\u0001\u0001¢\u0006\u0004\b5\u00101J.\u00107\u001a\u0014\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u001c2\n\u0010,\u001a\u00060\u0004j\u0002`+ø\u0001\u0000¢\u0006\u0004\b7\u00108J.\u00109\u001a\u0014\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u001c2\n\u0010,\u001a\u00060\u0004j\u0002`+ø\u0001\u0000¢\u0006\u0004\b9\u00108J\u0085\u0001\u0010?\u001a\u0012\u0012\b\u0012\u00060\u0004j\u0002`+\u0012\u0004\u0012\u00020>0\u001c2\n\u0010\u0018\u001a\u00060\u0004j\u0002`\u00172\u0018\b\u0002\u0010:\u001a\u0012\u0012\b\u0012\u00060\u0004j\u0002`+\u0012\u0004\u0012\u00020\u00020\u001c2\f\b\u0002\u0010;\u001a\u00060\u0004j\u0002`\u00052\u0018\b\u0002\u0010=\u001a\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020<0\u001c2\u0018\b\u0002\u0010\u001e\u001a\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u001d0\u001c¢\u0006\u0004\b?\u0010@J#\u0010B\u001a\u00020A2\u0006\u0010\u0003\u001a\u00020\u00022\f\b\u0002\u0010;\u001a\u00060\u0004j\u0002`\u0005¢\u0006\u0004\bB\u0010CJ/\u0010E\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0004j\u0002`+\u0012\u0004\u0012\u00020>0\u001c0D2\n\u0010\u0018\u001a\u00060\u0004j\u0002`\u0017¢\u0006\u0004\bE\u0010FJ#\u0010G\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0004j\u0002`+\u0012\u0004\u0012\u00020>0\u001c0D¢\u0006\u0004\bG\u0010HJ6\u0010I\u001a(\u0012$\u0012\"\u0012\b\u0012\u00060\u0004j\u0002`+\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u00070\u001c0\u001c0Dø\u0001\u0000¢\u0006\u0004\bI\u0010HJ0\u0010J\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00070D2\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\n\u0010,\u001a\u00060\u0004j\u0002`+ø\u0001\u0000¢\u0006\u0004\bJ\u0010KJ$\u0010L\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00070D2\n\u0010,\u001a\u00060\u0004j\u0002`+ø\u0001\u0000¢\u0006\u0004\bL\u0010FJ4\u0010M\u001a\u001a\u0012\u0016\u0012\u0014\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u001c0D2\n\u0010,\u001a\u00060\u0004j\u0002`+ø\u0001\u0000¢\u0006\u0004\bM\u0010FJ\u001f\u0010O\u001a\b\u0012\u0004\u0012\u00020N0D2\n\u0010,\u001a\u00060\u0004j\u0002`+¢\u0006\u0004\bO\u0010FJ+\u0010P\u001a\b\u0012\u0004\u0012\u00020N0D2\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\n\u0010,\u001a\u00060\u0004j\u0002`+¢\u0006\u0004\bP\u0010KJ\u000f\u0010Q\u001a\u00020\nH\u0007¢\u0006\u0004\bQ\u0010*J3\u0010S\u001a\u00020\n2\n\u0010\u0018\u001a\u00060\u0004j\u0002`\u00172\n\u0010R\u001a\u00060\u0004j\u0002`+2\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u0005H\u0007¢\u0006\u0004\bS\u0010TJ\u0017\u0010V\u001a\u00020\n2\u0006\u0010%\u001a\u00020UH\u0007¢\u0006\u0004\bV\u0010WJ\u0017\u0010X\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\bX\u0010YJ\u0017\u0010\\\u001a\u00020\n2\u0006\u0010[\u001a\u00020ZH\u0007¢\u0006\u0004\b\\\u0010]J'\u0010^\u001a\u00020\n2\n\u0010\u0018\u001a\u00060\u0004j\u0002`\u00172\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u0005H\u0007¢\u0006\u0004\b^\u0010_J\u001b\u0010`\u001a\u00020\n2\n\u0010\u0018\u001a\u00060\u0004j\u0002`\u0017H\u0007¢\u0006\u0004\b`\u0010aJ\u000f\u0010b\u001a\u00020\nH\u0017¢\u0006\u0004\bb\u0010*R\u0016\u0010d\u001a\u00020c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bd\u0010eR\u001c\u0010%\u001a\u0004\u0018\u00010$*\u00020\u00028B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\bf\u0010gR\u0016\u0010i\u001a\u00020h8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bi\u0010jR \u0010l\u001a\f\u0012\b\u0012\u00060\u0004j\u0002`+0k8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bl\u0010mR9\u0010n\u001a\"\u0012\b\u0012\u00060\u0004j\u0002`+\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u00070\u001c0\u001c8\u0002@\u0002X\u0082\u000eø\u0001\u0000¢\u0006\u0006\n\u0004\bn\u0010oR*\u0010r\u001a\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u001f\u0012\u0004\u0012\u00020 0\u001c*\u00020$8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\bp\u0010qR6\u0010u\u001a\"\u0012\b\u0012\u00060\u0004j\u0002`\u0017\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0004j\u0002`+\u0012\u0004\u0012\u00020t0s0s8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bu\u0010oR\u0016\u0010w\u001a\u00020v8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bw\u0010xR\u0016\u0010z\u001a\u00020y8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bz\u0010{R9\u0010|\u001a\"\u0012\b\u0012\u00060\u0004j\u0002`+\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u00070s0s8\u0002@\u0002X\u0082\u0004ø\u0001\u0000¢\u0006\u0006\n\u0004\b|\u0010oR\u0016\u0010~\u001a\u00020}8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b~\u0010\u007fR\u001a\u0010\u0081\u0001\u001a\u00030\u0080\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0081\u0001\u0010\u0082\u0001R\u001a\u0010\u0084\u0001\u001a\u00030\u0083\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0084\u0001\u0010\u0085\u0001\u0082\u0002\u000f\n\u0002\b\u0019\n\u0005\b¡\u001e0\u0001\n\u0002\b!¨\u0006\u008a\u0001"}, d2 = {"Lcom/discord/stores/StoreStageChannels;", "Lcom/discord/stores/StoreV2;", "Lcom/discord/api/channel/Channel;", "channel", "", "Lcom/discord/primitives/UserId;", "userId", "Lcom/discord/widgets/stage/StageRoles;", "oldRole", "newRole", "", "handleUserRoleChange-uOJZ9lM", "(Lcom/discord/api/channel/Channel;JLcom/discord/widgets/stage/StageRoles;I)V", "handleUserRoleChange", "roles", "handleUserJoinedStageOrGainedRole-oRmTEFA", "(Lcom/discord/api/channel/Channel;JI)V", "handleUserJoinedStageOrGainedRole", "oldRoles", "newRoles", "handleUserLeftStageOrLostRoles-GM3QuyE", "(Lcom/discord/api/channel/Channel;JII)V", "handleUserLeftStageOrLostRoles", "Lcom/discord/primitives/GuildId;", "guildId", "Lkotlin/sequences/Sequence;", "stageChannelIdsInGuildForDispatch", "(J)Lkotlin/sequences/Sequence;", "", "Lcom/discord/api/voice/state/VoiceState;", "voiceStates", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "computeUserRolesInDispatch-t27eFtU", "(JLcom/discord/api/channel/Channel;Ljava/util/Map;Ljava/util/Map;)I", "computeUserRolesInDispatch", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/stores/StageChannelRoleContext;", "roleContext", "(Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/stores/StageChannelRoleContext;", "markStageEventDirty", "()V", "Lcom/discord/primitives/ChannelId;", "channelId", "getUserRoles-uOBN1zc", "(JJ)Lcom/discord/widgets/stage/StageRoles;", "getUserRoles", "getMyRoles-visDeB4", "(J)Lcom/discord/widgets/stage/StageRoles;", "getMyRoles", "getUserRolesInternal-uOBN1zc", "getUserRolesInternal", "getMyRolesInternal-visDeB4", "getMyRolesInternal", "getChannelRolesInternal", "(J)Ljava/util/Map;", "getChannelRoles", "channelsInGuild", "myId", "Lcom/discord/models/user/User;", "users", "Lcom/discord/widgets/stage/model/StageChannel;", "getStageChannelsInGuild", "(JLjava/util/Map;JLjava/util/Map;Ljava/util/Map;)Ljava/util/Map;", "", "getOtherModeratorsCountInChannel", "(Lcom/discord/api/channel/Channel;J)I", "Lrx/Observable;", "observeGuildStageChannels", "(J)Lrx/Observable;", "observeStageChannels", "()Lrx/Observable;", "observeRoles", "observeUserRoles", "(JJ)Lrx/Observable;", "observeMyRoles", "observeStageRolesByChannel", "Lcom/discord/api/voice/state/StageRequestToSpeakState;", "observeMyRequestToSpeakState", "observeUserRequestToSpeakState", "handleConnectionOpen", "fromChannelId", "handleVoiceStatesUpdated", "(JJJ)V", "Lcom/discord/api/guild/Guild;", "handleGuildRemove", "(Lcom/discord/api/guild/Guild;)V", "handleChannelDelete", "(Lcom/discord/api/channel/Channel;)V", "Lcom/discord/api/guildmember/GuildMember;", "member", "handleGuildMemberAdd", "(Lcom/discord/api/guildmember/GuildMember;)V", "handleGuildMemberRemove", "(JJ)V", "handleGuildRoleCreateOrUpdate", "(J)V", "snapshotData", "Lcom/discord/stores/StoreVoiceStates;", "voiceStatesStore", "Lcom/discord/stores/StoreVoiceStates;", "getGuild", "(Lcom/discord/api/channel/Channel;)Lcom/discord/models/guild/Guild;", "Lcom/discord/stores/StoreStageInstances;", "stageInstancesStore", "Lcom/discord/stores/StoreStageInstances;", "", "dirtyChannelIds", "Ljava/util/Set;", "stageRolesByChannelSnapshot", "Ljava/util/Map;", "getRolesMap", "(Lcom/discord/models/guild/Guild;)Ljava/util/Map;", "rolesMap", "", "Lcom/discord/stores/StoreStageChannels$StageEventActivationState;", "stageEventStates", "Lcom/discord/stores/StoreUser;", "userStore", "Lcom/discord/stores/StoreUser;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "stageRolesByChannel", "Lcom/discord/stores/StorePermissions;", "permissionsStore", "Lcom/discord/stores/StorePermissions;", "Lcom/discord/stores/StoreGuilds;", "guildsStore", "Lcom/discord/stores/StoreGuilds;", "Lcom/discord/stores/StoreChannels;", "channelsStore", "Lcom/discord/stores/StoreChannels;", HookHelper.constructorName, "(Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreVoiceStates;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreStageInstances;Lcom/discord/stores/updates/ObservationDeck;)V", "Companion", "StageEventActivationState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStageChannels extends StoreV2 {
    public static final long CAN_MODERATE_STAGE_CHANNELS = 20971536;
    public static final Companion Companion = new Companion(null);
    private static final StoreStageChannels$Companion$EventStateUpdateSource$1 EventStateUpdateSource = new ObservationDeck.UpdateSource() { // from class: com.discord.stores.StoreStageChannels$Companion$EventStateUpdateSource$1
    };
    private final StoreChannels channelsStore;
    private final Set<Long> dirtyChannelIds;
    private final StoreGuilds guildsStore;
    private final ObservationDeck observationDeck;
    private final StorePermissions permissionsStore;
    private final Map<Long, Map<Long, StageEventActivationState>> stageEventStates;
    private final StoreStageInstances stageInstancesStore;
    private final Map<Long, Map<Long, StageRoles>> stageRolesByChannel;
    private Map<Long, ? extends Map<Long, StageRoles>> stageRolesByChannelSnapshot;
    private final StoreUser userStore;
    private final StoreVoiceStates voiceStatesStore;

    /* compiled from: StoreStageChannels.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0017\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\b\u0006*\u0001\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/stores/StoreStageChannels$Companion;", "", "", "CAN_MODERATE_STAGE_CHANNELS", "J", "com/discord/stores/StoreStageChannels$Companion$EventStateUpdateSource$1", "EventStateUpdateSource", "Lcom/discord/stores/StoreStageChannels$Companion$EventStateUpdateSource$1;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreStageChannels.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0082\b\u0018\u00002\u00020\u0001B\u001b\u0012\u0012\b\u0002\u0010\u0007\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u001a\u0010\u0005\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0006J$\u0010\b\u001a\u00020\u00002\u0012\b\u0002\u0010\u0007\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R#\u0010\u0007\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0014\u001a\u0004\b\u0015\u0010\u0006¨\u0006\u0018"}, d2 = {"Lcom/discord/stores/StoreStageChannels$StageEventActivationState;", "", "", "", "Lcom/discord/primitives/UserId;", "component1", "()Ljava/util/Set;", "moderators", "copy", "(Ljava/util/Set;)Lcom/discord/stores/StoreStageChannels$StageEventActivationState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Set;", "getModerators", HookHelper.constructorName, "(Ljava/util/Set;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class StageEventActivationState {
        private final Set<Long> moderators;

        public StageEventActivationState() {
            this(null, 1, null);
        }

        public StageEventActivationState(Set<Long> set) {
            m.checkNotNullParameter(set, "moderators");
            this.moderators = set;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ StageEventActivationState copy$default(StageEventActivationState stageEventActivationState, Set set, int i, Object obj) {
            if ((i & 1) != 0) {
                set = stageEventActivationState.moderators;
            }
            return stageEventActivationState.copy(set);
        }

        public final Set<Long> component1() {
            return this.moderators;
        }

        public final StageEventActivationState copy(Set<Long> set) {
            m.checkNotNullParameter(set, "moderators");
            return new StageEventActivationState(set);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof StageEventActivationState) && m.areEqual(this.moderators, ((StageEventActivationState) obj).moderators);
            }
            return true;
        }

        public final Set<Long> getModerators() {
            return this.moderators;
        }

        public int hashCode() {
            Set<Long> set = this.moderators;
            if (set != null) {
                return set.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder R = a.R("StageEventActivationState(moderators=");
            R.append(this.moderators);
            R.append(")");
            return R.toString();
        }

        public /* synthetic */ StageEventActivationState(Set set, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? n0.emptySet() : set);
        }
    }

    public /* synthetic */ StoreStageChannels(StoreUser storeUser, StoreGuilds storeGuilds, StoreChannels storeChannels, StoreVoiceStates storeVoiceStates, StorePermissions storePermissions, StoreStageInstances storeStageInstances, ObservationDeck observationDeck, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(storeUser, storeGuilds, storeChannels, storeVoiceStates, storePermissions, storeStageInstances, (i & 64) != 0 ? ObservationDeckProvider.get() : observationDeck);
    }

    /* renamed from: computeUserRolesInDispatch-t27eFtU  reason: not valid java name */
    private final int m4computeUserRolesInDispatcht27eFtU(long j, Channel channel, Map<Long, VoiceState> map, Map<Long, GuildRole> map2) {
        Guild guild = (Guild) a.u0(channel, this.guildsStore.getGuildsInternal$app_productionGoogleRelease());
        if (guild == null) {
            return StageRoles.Companion.m31getAUDIENCE1LxfuJo();
        }
        Map map3 = (Map) a.u0(channel, this.guildsStore.getGuildMembersComputedInternal$app_productionGoogleRelease());
        if (map3 == null) {
            return StageRoles.Companion.m31getAUDIENCE1LxfuJo();
        }
        GuildMember guildMember = (GuildMember) map3.get(Long.valueOf(j));
        if (guildMember == null) {
            return StageRoles.Companion.m31getAUDIENCE1LxfuJo();
        }
        if (map2 == null) {
            map2 = (Map) a.u0(channel, this.guildsStore.getGuildRolesInternal$app_productionGoogleRelease());
        }
        if (map == null) {
            map = (Map) a.u0(channel, this.voiceStatesStore.getInternal$app_productionGoogleRelease());
        }
        if (map == null) {
            return StageRoles.Companion.m31getAUDIENCE1LxfuJo();
        }
        StageChannelRoleContext roleContext = roleContext(channel, guild, map, map2);
        return roleContext != null ? roleContext.m3getRoleK6mKVE(j, guildMember) : StageRoles.Companion.m31getAUDIENCE1LxfuJo();
    }

    /* renamed from: computeUserRolesInDispatch-t27eFtU$default  reason: not valid java name */
    public static /* synthetic */ int m5computeUserRolesInDispatcht27eFtU$default(StoreStageChannels storeStageChannels, long j, Channel channel, Map map, Map map2, int i, Object obj) {
        return storeStageChannels.m4computeUserRolesInDispatcht27eFtU(j, channel, (i & 4) != 0 ? null : map, (i & 8) != 0 ? null : map2);
    }

    private final Guild getGuild(Channel channel) {
        return (Guild) a.u0(channel, this.guildsStore.getGuilds());
    }

    public static /* synthetic */ int getOtherModeratorsCountInChannel$default(StoreStageChannels storeStageChannels, Channel channel, long j, int i, Object obj) {
        if ((i & 2) != 0) {
            j = storeStageChannels.userStore.getMe().getId();
        }
        return storeStageChannels.getOtherModeratorsCountInChannel(channel, j);
    }

    private final Map<Long, GuildRole> getRolesMap(Guild guild) {
        Map<Long, GuildRole> map = (Map) a.d(guild, this.guildsStore.getRoles());
        return map != null ? map : h0.emptyMap();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ Map getStageChannelsInGuild$default(StoreStageChannels storeStageChannels, long j, Map map, long j2, Map map2, Map map3, int i, Object obj) {
        Map<Long, VoiceState> map4;
        Map<Long, Channel> channelsForGuild = (i & 2) != 0 ? storeStageChannels.channelsStore.getChannelsForGuild(j) : map;
        long id2 = (i & 4) != 0 ? storeStageChannels.userStore.getMe().getId() : j2;
        Map<Long, User> users = (i & 8) != 0 ? storeStageChannels.userStore.getUsers() : map2;
        if ((i & 16) != 0) {
            Map<Long, VoiceState> map5 = storeStageChannels.voiceStatesStore.get().get(Long.valueOf(j));
            if (map5 == null) {
                map5 = h0.emptyMap();
            }
            map4 = map5;
        } else {
            map4 = map3;
        }
        return storeStageChannels.getStageChannelsInGuild(j, channelsForGuild, id2, users, map4);
    }

    /* renamed from: handleUserJoinedStageOrGainedRole-oRmTEFA  reason: not valid java name */
    private final void m6handleUserJoinedStageOrGainedRoleoRmTEFA(Channel channel, long j, int i) {
        if (StageRoles.m26isModeratorimpl(i) || StageRoles.m27isSpeakerimpl(i)) {
            Map<Long, StageEventActivationState> map = (Map) a.u0(channel, this.stageEventStates);
            if (map == null) {
                map = new LinkedHashMap<>();
                this.stageEventStates.put(Long.valueOf(channel.f()), map);
            }
            StageEventActivationState stageEventActivationState = (StageEventActivationState) a.c(channel, map);
            boolean z2 = true;
            if (stageEventActivationState == null) {
                stageEventActivationState = new StageEventActivationState(null, 1, null);
                map.put(Long.valueOf(channel.h()), stageEventActivationState);
            }
            if (!StageRoles.m26isModeratorimpl(i) || stageEventActivationState.getModerators().contains(Long.valueOf(j))) {
                z2 = false;
            }
            if (z2) {
                map.put(Long.valueOf(channel.h()), stageEventActivationState.copy(z2 ? o0.plus(stageEventActivationState.getModerators(), Long.valueOf(j)) : stageEventActivationState.getModerators()));
                markStageEventDirty();
            }
        }
    }

    /* renamed from: handleUserLeftStageOrLostRoles-GM3QuyE  reason: not valid java name */
    private final void m7handleUserLeftStageOrLostRolesGM3QuyE(Channel channel, long j, int i, int i2) {
        Map map;
        StageEventActivationState stageEventActivationState;
        if ((StageRoles.m26isModeratorimpl(i) || StageRoles.m27isSpeakerimpl(i)) && (map = (Map) a.u0(channel, this.stageEventStates)) != null && (stageEventActivationState = (StageEventActivationState) a.c(channel, map)) != null) {
            boolean z2 = !StageRoles.m26isModeratorimpl(i2) && StageRoles.m26isModeratorimpl(i) && stageEventActivationState.getModerators().contains(Long.valueOf(j));
            if (z2) {
                map.put(Long.valueOf(channel.h()), stageEventActivationState.copy(z2 ? o0.minus(stageEventActivationState.getModerators(), Long.valueOf(j)) : stageEventActivationState.getModerators()));
                markStageEventDirty();
            }
        }
    }

    /* renamed from: handleUserLeftStageOrLostRoles-GM3QuyE$default  reason: not valid java name */
    public static /* synthetic */ void m8handleUserLeftStageOrLostRolesGM3QuyE$default(StoreStageChannels storeStageChannels, Channel channel, long j, int i, int i2, int i3, Object obj) {
        if ((i3 & 8) != 0) {
            i2 = StageRoles.Companion.m31getAUDIENCE1LxfuJo();
        }
        storeStageChannels.m7handleUserLeftStageOrLostRolesGM3QuyE(channel, j, i, i2);
    }

    /* renamed from: handleUserRoleChange-uOJZ9lM  reason: not valid java name */
    private final void m9handleUserRoleChangeuOJZ9lM(Channel channel, long j, StageRoles stageRoles, int i) {
        if ((StageRoles.m26isModeratorimpl(i) && (stageRoles == null || !StageRoles.m26isModeratorimpl(stageRoles.m29unboximpl()))) || (StageRoles.m27isSpeakerimpl(i) && (stageRoles == null || !StageRoles.m27isSpeakerimpl(stageRoles.m29unboximpl())))) {
            m6handleUserJoinedStageOrGainedRoleoRmTEFA(channel, j, i);
        }
        if ((stageRoles != null && StageRoles.m26isModeratorimpl(stageRoles.m29unboximpl()) && !StageRoles.m26isModeratorimpl(i)) || (stageRoles != null && StageRoles.m27isSpeakerimpl(stageRoles.m29unboximpl()) && !StageRoles.m27isSpeakerimpl(i))) {
            m7handleUserLeftStageOrLostRolesGM3QuyE(channel, j, stageRoles.m29unboximpl(), i);
        }
        if (j != this.userStore.getMe().getId()) {
            return;
        }
        if (stageRoles != null && !StageRoles.m25isInvitedToSpeakimpl(stageRoles.m29unboximpl()) && StageRoles.m25isInvitedToSpeakimpl(i)) {
            StageChannelNotifications.Companion.getINSTANCE().onInvitedToSpeak(channel.h());
        } else if (stageRoles != null && StageRoles.m25isInvitedToSpeakimpl(stageRoles.m29unboximpl()) && !StageRoles.m25isInvitedToSpeakimpl(i)) {
            StageChannelNotifications.Companion.getINSTANCE().onInviteToSpeakRescinded();
        }
    }

    private final void markStageEventDirty() {
        markChanged(EventStateUpdateSource);
    }

    private final StageChannelRoleContext roleContext(Channel channel, Guild guild, Map<Long, VoiceState> map, Map<Long, GuildRole> map2) {
        if (guild == null) {
            guild = getGuild(channel);
        }
        if (guild == null) {
            return null;
        }
        if (map == null) {
            map = this.voiceStatesStore.getForChannel(guild.getId(), channel.h());
        }
        if (map2 == null) {
            map2 = getRolesMap(guild);
        }
        return new StageChannelRoleContext(guild, channel, map2, map);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ StageChannelRoleContext roleContext$default(StoreStageChannels storeStageChannels, Channel channel, Guild guild, Map map, Map map2, int i, Object obj) {
        if ((i & 2) != 0) {
            guild = null;
        }
        if ((i & 4) != 0) {
            map = null;
        }
        if ((i & 8) != 0) {
            map2 = null;
        }
        return storeStageChannels.roleContext(channel, guild, map, map2);
    }

    private final Sequence<Long> stageChannelIdsInGuildForDispatch(long j) {
        Collection<Channel> values;
        Sequence asSequence;
        Sequence<Long> mapNotNull;
        Map<Long, Channel> channelsForGuildInternal$app_productionGoogleRelease = this.channelsStore.getChannelsForGuildInternal$app_productionGoogleRelease(j);
        return (channelsForGuildInternal$app_productionGoogleRelease == null || (values = channelsForGuildInternal$app_productionGoogleRelease.values()) == null || (asSequence = u.asSequence(values)) == null || (mapNotNull = q.mapNotNull(asSequence, StoreStageChannels$stageChannelIdsInGuildForDispatch$1.INSTANCE)) == null) ? n.emptySequence() : mapNotNull;
    }

    public final Map<Long, StageRoles> getChannelRoles(long j) {
        return this.stageRolesByChannelSnapshot.get(Long.valueOf(j));
    }

    public final Map<Long, StageRoles> getChannelRolesInternal(long j) {
        return this.stageRolesByChannel.get(Long.valueOf(j));
    }

    /* renamed from: getMyRoles-visDeB4  reason: not valid java name */
    public final StageRoles m10getMyRolesvisDeB4(long j) {
        return m12getUserRolesuOBN1zc(this.userStore.getMe().getId(), j);
    }

    /* renamed from: getMyRolesInternal-visDeB4  reason: not valid java name */
    public final StageRoles m11getMyRolesInternalvisDeB4(long j) {
        return m13getUserRolesInternaluOBN1zc(this.userStore.getMe().getId(), j);
    }

    public final int getOtherModeratorsCountInChannel(Channel channel, long j) {
        StageEventActivationState stageEventActivationState;
        m.checkNotNullParameter(channel, "channel");
        Map map = (Map) a.u0(channel, this.stageEventStates);
        if (map == null || (stageEventActivationState = (StageEventActivationState) a.c(channel, map)) == null) {
            return 0;
        }
        int size = stageEventActivationState.getModerators().size();
        return stageEventActivationState.getModerators().contains(Long.valueOf(j)) ? size - 1 : size;
    }

    public final Map<Long, StageChannel> getStageChannelsInGuild(long j, Map<Long, Channel> map, long j2, Map<Long, ? extends User> map2, Map<Long, VoiceState> map3) {
        StoreStageChannels storeStageChannels = this;
        Map<Long, ? extends User> map4 = map2;
        m.checkNotNullParameter(map, "channelsInGuild");
        m.checkNotNullParameter(map4, "users");
        m.checkNotNullParameter(map3, "voiceStates");
        Collection<Channel> values = map.values();
        ArrayList arrayList = new ArrayList();
        for (Object obj : values) {
            if (ChannelUtils.z((Channel) obj)) {
                arrayList.add(obj);
            }
        }
        int i = 10;
        ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(arrayList, 10));
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            Channel channel = (Channel) it.next();
            Map map5 = (Map) a.c(channel, storeStageChannels.stageRolesByChannelSnapshot);
            if (map5 == null) {
                map5 = h0.emptyMap();
            }
            Set<Number> keySet = map5.keySet();
            ArrayList arrayList3 = new ArrayList();
            for (Number number : keySet) {
                User user = map4.get(Long.valueOf(number.longValue()));
                if (user != null) {
                    arrayList3.add(user);
                }
            }
            ArrayList<User> arrayList4 = new ArrayList();
            for (Object obj2 : arrayList3) {
                StageRoles stageRoles = storeStageChannels.m12getUserRolesuOBN1zc(((User) obj2).getId(), channel.h());
                if (stageRoles != null && StageRoles.m27isSpeakerimpl(stageRoles.m29unboximpl())) {
                    arrayList4.add(obj2);
                }
            }
            ArrayList arrayList5 = new ArrayList(o.collectionSizeOrDefault(arrayList4, 10));
            for (User user2 : arrayList4) {
                arrayList5.add(Long.valueOf(user2.getId()));
            }
            Set set = u.toSet(arrayList5);
            int size = arrayList3.size() - set.size();
            Long l = (Long) a.c(channel, storeStageChannels.permissionsStore.getPermissionsByChannel());
            boolean z2 = PermissionUtils.can(Permission.CONNECT, l) && PermissionUtils.can(Permission.VIEW_CHANNEL, l);
            Map<Long, GuildMember> map6 = storeStageChannels.guildsStore.getMembers().get(Long.valueOf(j));
            ArrayList<User> arrayList6 = new ArrayList();
            for (Object obj3 : arrayList3) {
                if (set.contains(Long.valueOf(((User) obj3).getId()))) {
                    arrayList6.add(obj3);
                }
            }
            ArrayList arrayList7 = new ArrayList(o.collectionSizeOrDefault(arrayList6, 10));
            for (User user3 : arrayList6) {
                arrayList7.add(new UserGuildMember(user3, map6 != null ? (GuildMember) a.e(user3, map6) : null));
            }
            ArrayList arrayList8 = arrayList2;
            it = it;
            StageRoles stageRoles2 = storeStageChannels.m10getMyRolesvisDeB4(channel.h());
            StageInstance stageInstanceForChannel = storeStageChannels.stageInstancesStore.getStageInstanceForChannel(channel.h());
            VoiceState voiceState = map3.get(Long.valueOf(j2));
            Long a = voiceState != null ? voiceState.a() : null;
            arrayList8.add(new StageChannel(channel, arrayList3, stageRoles2, set, arrayList7, size, stageInstanceForChannel, z2, a != null && a.longValue() == channel.h(), null));
            i = 10;
            map4 = map2;
            arrayList2 = arrayList8;
            storeStageChannels = this;
        }
        ArrayList arrayList9 = arrayList2;
        LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(o.collectionSizeOrDefault(arrayList9, i)), 16));
        for (Object obj4 : arrayList9) {
            linkedHashMap.put(Long.valueOf(((StageChannel) obj4).getChannel().h()), obj4);
        }
        return linkedHashMap;
    }

    /* renamed from: getUserRoles-uOBN1zc  reason: not valid java name */
    public final StageRoles m12getUserRolesuOBN1zc(long j, long j2) {
        Map<Long, StageRoles> map = this.stageRolesByChannelSnapshot.get(Long.valueOf(j2));
        if (map == null) {
            return null;
        }
        StageRoles stageRoles = map.get(Long.valueOf(j));
        return StageRoles.m19boximpl(stageRoles != null ? stageRoles.m29unboximpl() : StageRoles.Companion.m31getAUDIENCE1LxfuJo());
    }

    /* renamed from: getUserRolesInternal-uOBN1zc  reason: not valid java name */
    public final StageRoles m13getUserRolesInternaluOBN1zc(long j, long j2) {
        Map<Long, StageRoles> map = this.stageRolesByChannel.get(Long.valueOf(j2));
        if (map == null) {
            return null;
        }
        StageRoles stageRoles = map.get(Long.valueOf(j));
        return StageRoles.m19boximpl(stageRoles != null ? stageRoles.m29unboximpl() : StageRoles.Companion.m31getAUDIENCE1LxfuJo());
    }

    @StoreThread
    public final void handleChannelDelete(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        if (ChannelUtils.z(channel)) {
            if (this.stageRolesByChannel.remove(Long.valueOf(channel.h())) != null) {
                this.dirtyChannelIds.add(Long.valueOf(channel.h()));
                markChanged();
            }
            Map map = (Map) a.u0(channel, this.stageEventStates);
            if (map != null && ((StageEventActivationState) map.remove(Long.valueOf(channel.h()))) != null) {
                markStageEventDirty();
            }
        }
    }

    @StoreThread
    public final void handleConnectionOpen() {
        this.stageRolesByChannel.clear();
        this.stageEventStates.clear();
    }

    @StoreThread
    public final void handleGuildMemberAdd(com.discord.api.guildmember.GuildMember guildMember) {
        StageRoles stageRoles;
        m.checkNotNullParameter(guildMember, "member");
        User user = this.userStore.getUsersInternal$app_productionGoogleRelease().get(Long.valueOf(guildMember.m().i()));
        if (user == null || !user.isBot()) {
            for (Long l : stageChannelIdsInGuildForDispatch(guildMember.f())) {
                long longValue = l.longValue();
                Map<Long, StageRoles> map = this.stageRolesByChannel.get(Long.valueOf(longValue));
                if (!(map == null || (stageRoles = map.get(Long.valueOf(guildMember.m().i()))) == null)) {
                    int i = stageRoles.m29unboximpl();
                    Channel channel = this.channelsStore.getChannelsByIdInternal$app_productionGoogleRelease().get(Long.valueOf(longValue));
                    if (channel != null) {
                        int i2 = m5computeUserRolesInDispatcht27eFtU$default(this, guildMember.m().i(), channel, null, null, 12, null);
                        if (!StageRoles.m22equalsimpl0(i2, i)) {
                            map.put(Long.valueOf(guildMember.m().i()), StageRoles.m19boximpl(i2));
                            m9handleUserRoleChangeuOJZ9lM(channel, guildMember.m().i(), StageRoles.m19boximpl(i), i2);
                            this.dirtyChannelIds.add(Long.valueOf(longValue));
                            markChanged();
                        }
                    }
                }
            }
        }
    }

    @StoreThread
    public final void handleGuildMemberRemove(long j, long j2) {
        StageRoles stageRoles;
        Channel channel;
        User user = this.userStore.getUsersInternal$app_productionGoogleRelease().get(Long.valueOf(j2));
        if (user == null || !user.isBot()) {
            for (Long l : stageChannelIdsInGuildForDispatch(j)) {
                long longValue = l.longValue();
                Map<Long, StageRoles> map = this.stageRolesByChannel.get(Long.valueOf(longValue));
                if (!(map == null || (stageRoles = map.get(Long.valueOf(j2))) == null || (channel = this.channelsStore.getChannelsByIdInternal$app_productionGoogleRelease().get(Long.valueOf(longValue))) == null)) {
                    m8handleUserLeftStageOrLostRolesGM3QuyE$default(this, channel, j2, stageRoles.m29unboximpl(), 0, 8, null);
                    this.dirtyChannelIds.add(Long.valueOf(longValue));
                    markChanged();
                }
            }
        }
    }

    @StoreThread
    public final void handleGuildRemove(com.discord.api.guild.Guild guild) {
        m.checkNotNullParameter(guild, "guild");
        if (this.stageEventStates.remove(Long.valueOf(guild.r())) != null) {
            markStageEventDirty();
        }
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        for (Number number : this.channelsStore.getChannelsForGuild(guild.r()).keySet()) {
            long longValue = number.longValue();
            if (this.stageRolesByChannel.remove(Long.valueOf(longValue)) != null) {
                linkedHashSet.add(Long.valueOf(longValue));
            }
        }
        if (!linkedHashSet.isEmpty()) {
            r.addAll(this.dirtyChannelIds, linkedHashSet);
            markChanged();
        }
    }

    @StoreThread
    public final void handleGuildRoleCreateOrUpdate(long j) {
        for (Long l : stageChannelIdsInGuildForDispatch(j)) {
            long longValue = l.longValue();
            Map<Long, StageRoles> map = this.stageRolesByChannel.get(Long.valueOf(longValue));
            if (map != null) {
                for (Number number : u.toList(map.keySet())) {
                    handleVoiceStatesUpdated(j, longValue, number.longValue());
                }
            }
        }
    }

    @StoreThread
    public final void handleVoiceStatesUpdated(long j, long j2, long j3) {
        boolean z2;
        Long a;
        Long a2;
        Map<Long, VoiceState> map = this.voiceStatesStore.getInternal$app_productionGoogleRelease().get(Long.valueOf(j));
        StageRoles stageRoles = null;
        VoiceState voiceState = map != null ? map.get(Long.valueOf(j3)) : null;
        long longValue = (voiceState == null || (a2 = voiceState.a()) == null) ? j2 : a2.longValue();
        Channel channelInternal$app_productionGoogleRelease = this.channelsStore.getChannelInternal$app_productionGoogleRelease(j, longValue);
        Channel channelInternal$app_productionGoogleRelease2 = this.channelsStore.getChannelInternal$app_productionGoogleRelease(j, j2);
        if (channelInternal$app_productionGoogleRelease2 != null && ChannelUtils.z(channelInternal$app_productionGoogleRelease2) && (voiceState == null || (a = voiceState.a()) == null || a.longValue() != j2)) {
            Map<Long, StageRoles> map2 = this.stageRolesByChannel.get(Long.valueOf(j2));
            if (map2 != null) {
                stageRoles = map2.remove(Long.valueOf(j3));
            }
            if (stageRoles != null) {
                z2 = true;
                m8handleUserLeftStageOrLostRolesGM3QuyE$default(this, channelInternal$app_productionGoogleRelease2, j3, stageRoles.m29unboximpl(), 0, 8, null);
                this.dirtyChannelIds.add(Long.valueOf(j2));
                markChanged();
                if (voiceState != null && channelInternal$app_productionGoogleRelease != null && ChannelUtils.z(channelInternal$app_productionGoogleRelease) == z2) {
                    Map<Long, StageRoles> map3 = this.stageRolesByChannel.get(Long.valueOf(longValue));
                    if (map3 == null) {
                        map3 = new LinkedHashMap<>();
                        this.stageRolesByChannel.put(Long.valueOf(longValue), map3);
                    }
                    Map<Long, StageRoles> map4 = map3;
                    StageRoles stageRoles2 = map4.get(Long.valueOf(j3));
                    int i = m5computeUserRolesInDispatcht27eFtU$default(this, j3, channelInternal$app_productionGoogleRelease, map, null, 8, null);
                    if (StageRoles.m21equalsimpl(i, stageRoles2) ^ z2) {
                        m9handleUserRoleChangeuOJZ9lM(channelInternal$app_productionGoogleRelease, j3, stageRoles2, i);
                        map4.put(Long.valueOf(j3), StageRoles.m19boximpl(i));
                        this.dirtyChannelIds.add(Long.valueOf(longValue));
                        markChanged();
                        return;
                    }
                    return;
                }
                return;
            }
        }
        z2 = true;
        if (voiceState != null) {
        }
    }

    public final Observable<Map<Long, StageChannel>> observeGuildStageChannels(long j) {
        Observable<Map<Long, StageChannel>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this, this.channelsStore, this.userStore, this.voiceStatesStore, this.stageInstancesStore}, false, null, null, new StoreStageChannels$observeGuildStageChannels$1(this, j), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    public final Observable<StageRequestToSpeakState> observeMyRequestToSpeakState(long j) {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this, this.channelsStore, this.guildsStore, this.userStore, this.voiceStatesStore}, false, null, null, new StoreStageChannels$observeMyRequestToSpeakState$1(this, j), 14, null);
    }

    public final Observable<StageRoles> observeMyRoles(long j) {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this, this.channelsStore}, false, null, null, new StoreStageChannels$observeMyRoles$1(this, j), 14, null);
    }

    public final Observable<Map<Long, Map<Long, StageRoles>>> observeRoles() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreStageChannels$observeRoles$1(this), 14, null);
    }

    public final Observable<Map<Long, StageChannel>> observeStageChannels() {
        Observable<Map<Long, StageChannel>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this, this.channelsStore, this.guildsStore, this.userStore, this.voiceStatesStore, this.stageInstancesStore}, false, null, null, new StoreStageChannels$observeStageChannels$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    public final Observable<Map<Long, StageRoles>> observeStageRolesByChannel(long j) {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreStageChannels$observeStageRolesByChannel$1(this, j), 14, null);
    }

    public final Observable<StageRequestToSpeakState> observeUserRequestToSpeakState(long j, long j2) {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this, this.channelsStore, this.guildsStore, this.voiceStatesStore}, false, null, null, new StoreStageChannels$observeUserRequestToSpeakState$1(this, j2, j), 14, null);
    }

    public final Observable<StageRoles> observeUserRoles(long j, long j2) {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this, this.channelsStore}, false, null, null, new StoreStageChannels$observeUserRoles$1(this, j2, j), 14, null);
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        Map<Long, Map<Long, StageRoles>> map = this.stageRolesByChannel;
        Map<Long, ? extends Map<Long, StageRoles>> map2 = this.stageRolesByChannelSnapshot;
        Set<Long> set = this.dirtyChannelIds;
        if (!set.isEmpty()) {
            HashMap hashMap = new HashMap(map.size());
            for (Map.Entry<Long, Map<Long, StageRoles>> entry : map.entrySet()) {
                Long key = entry.getKey();
                Map<Long, StageRoles> value = entry.getValue();
                if (set.contains(key)) {
                    key.longValue();
                    hashMap.put(key, new HashMap(value));
                } else {
                    Map<Long, StageRoles> map3 = map2.get(key);
                    if (map3 != null) {
                        hashMap.put(key, map3);
                    }
                }
            }
            map2 = hashMap;
        }
        this.stageRolesByChannelSnapshot = map2;
        this.dirtyChannelIds.clear();
    }

    public StoreStageChannels(StoreUser storeUser, StoreGuilds storeGuilds, StoreChannels storeChannels, StoreVoiceStates storeVoiceStates, StorePermissions storePermissions, StoreStageInstances storeStageInstances, ObservationDeck observationDeck) {
        m.checkNotNullParameter(storeUser, "userStore");
        m.checkNotNullParameter(storeGuilds, "guildsStore");
        m.checkNotNullParameter(storeChannels, "channelsStore");
        m.checkNotNullParameter(storeVoiceStates, "voiceStatesStore");
        m.checkNotNullParameter(storePermissions, "permissionsStore");
        m.checkNotNullParameter(storeStageInstances, "stageInstancesStore");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.userStore = storeUser;
        this.guildsStore = storeGuilds;
        this.channelsStore = storeChannels;
        this.voiceStatesStore = storeVoiceStates;
        this.permissionsStore = storePermissions;
        this.stageInstancesStore = storeStageInstances;
        this.observationDeck = observationDeck;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        this.stageRolesByChannel = linkedHashMap;
        this.stageRolesByChannelSnapshot = new HashMap(linkedHashMap);
        this.stageEventStates = new LinkedHashMap();
        this.dirtyChannelIds = new LinkedHashSet();
    }
}
