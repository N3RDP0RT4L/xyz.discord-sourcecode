package com.discord.stores;

import com.discord.gateway.GatewaySocket;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreGatewayConnection.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/gateway/GatewaySocket;", "it", "", "invoke", "(Lcom/discord/gateway/GatewaySocket;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGatewayConnection$requestGuildMembers$1 extends o implements Function1<GatewaySocket, Unit> {
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ Integer $limit;
    public final /* synthetic */ String $query;
    public final /* synthetic */ List $userIds;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGatewayConnection$requestGuildMembers$1(long j, String str, List list, Integer num) {
        super(1);
        this.$guildId = j;
        this.$query = str;
        this.$userIds = list;
        this.$limit = num;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(GatewaySocket gatewaySocket) {
        invoke2(gatewaySocket);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(GatewaySocket gatewaySocket) {
        m.checkNotNullParameter(gatewaySocket, "it");
        gatewaySocket.requestGuildMembers(d0.t.m.listOf(Long.valueOf(this.$guildId)), this.$query, this.$userIds, this.$limit);
    }
}
