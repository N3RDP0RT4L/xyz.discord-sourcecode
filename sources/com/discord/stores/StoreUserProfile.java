package com.discord.stores;

import andhook.lib.HookHelper;
import com.discord.api.user.User;
import com.discord.api.user.UserProfile;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.rest.RestAPI;
import d0.t.h0;
import d0.z.d.m;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.objectweb.asm.Opcodes;
import rx.Observable;
/* compiled from: StoreUserProfile.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000~\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 72\u00020\u0001:\u00017B+\u0012\u0006\u00100\u001a\u00020/\u0012\b\b\u0002\u0010*\u001a\u00020)\u0012\u0006\u0010-\u001a\u00020,\u0012\b\b\u0002\u0010!\u001a\u00020 ¢\u0006\u0004\b5\u00106J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u001b\u0010\n\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\bH\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0019\u0010\r\u001a\u00020\f2\n\u0010\t\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\b\r\u0010\u000eJ\u001f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\f0\u000f2\n\u0010\t\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\b\u0010\u0010\u0011JM\u0010\u0018\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\u0010\b\u0002\u0010\u0013\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\u00122\b\b\u0002\u0010\u0015\u001a\u00020\u00142\u0016\b\u0002\u0010\u0017\u001a\u0010\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0016¢\u0006\u0004\b\u0018\u0010\u0019J\u0015\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u001a\u0010\u0006J\u0017\u0010\u001c\u001a\u00020\u00042\u0006\u0010\u001b\u001a\u00020\fH\u0007¢\u0006\u0004\b\u001c\u0010\u001dJ\u000f\u0010\u001e\u001a\u00020\u0004H\u0017¢\u0006\u0004\b\u001e\u0010\u001fR\u0016\u0010!\u001a\u00020 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"R \u0010$\u001a\f\u0012\b\u0012\u00060\u0007j\u0002`\b0#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%R&\u0010'\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020\f0&8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b'\u0010(R\u0016\u0010*\u001a\u00020)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010+R\u0016\u0010-\u001a\u00020,8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b-\u0010.R\u0016\u00100\u001a\u00020/8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b0\u00101R&\u00103\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020\f028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b3\u00104¨\u00068"}, d2 = {"Lcom/discord/stores/StoreUserProfile;", "Lcom/discord/stores/StoreV2;", "Lcom/discord/api/user/User;", "user", "", "handleUser", "(Lcom/discord/api/user/User;)V", "", "Lcom/discord/primitives/UserId;", "userId", "handleFailure", "(J)V", "Lcom/discord/api/user/UserProfile;", "getUserProfile", "(J)Lcom/discord/api/user/UserProfile;", "Lrx/Observable;", "observeUserProfile", "(J)Lrx/Observable;", "Lcom/discord/primitives/GuildId;", "guildId", "", "withMutualGuilds", "Lkotlin/Function1;", "onFetchSuccess", "fetchProfile", "(JLjava/lang/Long;ZLkotlin/jvm/functions/Function1;)V", "updateUser", "userProfile", "handleUserProfile", "(Lcom/discord/api/user/UserProfile;)V", "snapshotData", "()V", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "Ljava/util/HashSet;", "profilesLoading", "Ljava/util/HashSet;", "", "profilesSnapshot", "Ljava/util/Map;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/stores/StoreStream;", "storeStream", "Lcom/discord/stores/StoreStream;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Ljava/util/HashMap;", "profiles", "Ljava/util/HashMap;", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/StoreStream;Lcom/discord/utilities/rest/RestAPI;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserProfile extends StoreV2 {
    public static final Companion Companion = new Companion(null);
    private static final UserProfile EMPTY_PROFILE = new UserProfile(null, null, null, null, null, null, null, Opcodes.LAND);
    private final Dispatcher dispatcher;
    private final ObservationDeck observationDeck;
    private final HashMap<Long, UserProfile> profiles;
    private final HashSet<Long> profilesLoading;
    private Map<Long, UserProfile> profilesSnapshot;
    private final RestAPI restAPI;
    private final StoreStream storeStream;

    /* compiled from: StoreUserProfile.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/stores/StoreUserProfile$Companion;", "", "Lcom/discord/api/user/UserProfile;", "EMPTY_PROFILE", "Lcom/discord/api/user/UserProfile;", "getEMPTY_PROFILE", "()Lcom/discord/api/user/UserProfile;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public final UserProfile getEMPTY_PROFILE() {
            return StoreUserProfile.EMPTY_PROFILE;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ StoreUserProfile(Dispatcher dispatcher, ObservationDeck observationDeck, StoreStream storeStream, RestAPI restAPI, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(dispatcher, (i & 2) != 0 ? ObservationDeckProvider.get() : observationDeck, storeStream, (i & 8) != 0 ? RestAPI.Companion.getApi() : restAPI);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleFailure(long j) {
        this.profilesLoading.remove(Long.valueOf(j));
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleUser(User user) {
        UserProfile userProfile = this.profiles.get(Long.valueOf(user.i()));
        if (userProfile != null) {
            m.checkNotNullExpressionValue(userProfile, "profiles[user.id] ?: return");
            this.profiles.put(Long.valueOf(user.i()), new UserProfile(userProfile.b(), userProfile.d(), user, userProfile.f(), userProfile.e(), null, userProfile.a()));
            markChanged();
        }
    }

    public final void fetchProfile(long j, Long l, boolean z2, Function1<? super UserProfile, Unit> function1) {
        this.dispatcher.schedule(new StoreUserProfile$fetchProfile$1(this, j, l, z2, function1));
    }

    public final UserProfile getUserProfile(long j) {
        UserProfile userProfile = this.profilesSnapshot.get(Long.valueOf(j));
        return userProfile != null ? userProfile : EMPTY_PROFILE;
    }

    @StoreThread
    public final void handleUserProfile(UserProfile userProfile) {
        m.checkNotNullParameter(userProfile, "userProfile");
        long i = userProfile.g().i();
        this.profilesLoading.remove(Long.valueOf(i));
        this.profiles.put(Long.valueOf(i), userProfile);
        markChanged();
    }

    public final Observable<UserProfile> observeUserProfile(long j) {
        Observable<UserProfile> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreUserProfile$observeUserProfile$1(this, j), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        super.snapshotData();
        this.profilesSnapshot = new HashMap(this.profiles);
    }

    public final void updateUser(User user) {
        m.checkNotNullParameter(user, "user");
        this.dispatcher.schedule(new StoreUserProfile$updateUser$1(this, user));
    }

    public StoreUserProfile(Dispatcher dispatcher, ObservationDeck observationDeck, StoreStream storeStream, RestAPI restAPI) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(storeStream, "storeStream");
        m.checkNotNullParameter(restAPI, "restAPI");
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
        this.storeStream = storeStream;
        this.restAPI = restAPI;
        this.profilesLoading = new HashSet<>();
        this.profiles = new HashMap<>();
        this.profilesSnapshot = h0.emptyMap();
    }
}
