package com.discord.stores.updates;

import andhook.lib.HookHelper;
import androidx.exifinterface.media.ExifInterface;
import b.d.b.a.a;
import com.discord.app.AppLog;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.logging.Logger;
import d0.o;
import d0.t.h0;
import d0.t.k;
import d0.t.u;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Ref$ObjectRef;
import rx.Emitter;
import rx.Observable;
import rx.functions.Action0;
import rx.functions.Action1;
/* compiled from: ObservationDeck.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 12\u00020\u0001:\u00041234B\u001b\u0012\b\b\u0002\u0010'\u001a\u00020&\u0012\b\b\u0002\u0010-\u001a\u00020,¢\u0006\u0004\b/\u00100J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J%\u0010\f\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00072\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0002¢\u0006\u0004\b\f\u0010\rJG\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00040\u00152\u0012\u0010\u000f\u001a\n\u0012\u0006\b\u0001\u0012\u00020\n0\u000e\"\u00020\n2\b\b\u0002\u0010\u0011\u001a\u00020\u00102\b\b\u0002\u0010\u0013\u001a\u00020\u00122\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0016\u0010\u0017J[\u0010\u0016\u001a\b\u0012\u0004\u0012\u00028\u00000\u0015\"\u0004\b\u0000\u0010\u00182\u0012\u0010\u000f\u001a\n\u0012\u0006\b\u0001\u0012\u00020\n0\u000e\"\u00020\n2\b\b\u0002\u0010\u0011\u001a\u00020\u00102\b\b\u0002\u0010\u0013\u001a\u00020\u00122\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00022\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00028\u00000\u0019¢\u0006\u0004\b\u0016\u0010\u001bJE\u0010\u001e\u001a\u00020\u001d2\u0012\u0010\u000f\u001a\n\u0012\u0006\b\u0001\u0012\u00020\n0\u000e\"\u00020\n2\b\b\u0002\u0010\u0011\u001a\u00020\u00102\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00022\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00040\u0019¢\u0006\u0004\b\u001e\u0010\u001fJ\u001f\u0010\u001e\u001a\u00020\u001d2\u0006\u0010 \u001a\u00020\u001d2\b\b\u0002\u0010\u0011\u001a\u00020\u0010¢\u0006\u0004\b\u001e\u0010!J\u0015\u0010\"\u001a\u00020\u00042\u0006\u0010 \u001a\u00020\u001d¢\u0006\u0004\b\"\u0010#J\u001b\u0010$\u001a\u00020\u00042\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\t¢\u0006\u0004\b$\u0010%R\u0016\u0010'\u001a\u00020&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R\u001c\u0010*\u001a\b\u0012\u0004\u0012\u00020\u001d0)8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b*\u0010+R\u0016\u0010-\u001a\u00020,8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b-\u0010.¨\u00065"}, d2 = {"Lcom/discord/stores/updates/ObservationDeck;", "", "", "message", "", "logBreadcrumb", "(Ljava/lang/String;)V", "", "throwable", "", "Lcom/discord/stores/updates/ObservationDeck$UpdateSource;", "updates", "logNotifyError", "(Ljava/lang/Throwable;Ljava/util/Set;)V", "", "updateSources", "", "updateOnConnect", "Lrx/Emitter$BackpressureMode;", "backpressureMode", "observerName", "Lrx/Observable;", "connectRx", "([Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;)Lrx/Observable;", ExifInterface.GPS_DIRECTION_TRUE, "Lkotlin/Function0;", "generator", "([Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLrx/Emitter$BackpressureMode;Ljava/lang/String;Lkotlin/jvm/functions/Function0;)Lrx/Observable;", "onUpdate", "Lcom/discord/stores/updates/ObservationDeck$Observer;", "connect", "([Lcom/discord/stores/updates/ObservationDeck$UpdateSource;ZLjava/lang/String;Lkotlin/jvm/functions/Function0;)Lcom/discord/stores/updates/ObservationDeck$Observer;", "observer", "(Lcom/discord/stores/updates/ObservationDeck$Observer;Z)Lcom/discord/stores/updates/ObservationDeck$Observer;", "disconnect", "(Lcom/discord/stores/updates/ObservationDeck$Observer;)V", "notify", "(Ljava/util/Set;)V", "Lcom/discord/utilities/logging/Logger;", "logger", "Lcom/discord/utilities/logging/Logger;", "", "observers", "Ljava/util/List;", "Lcom/discord/stores/updates/ObservationDeck$LogLevel;", "logLevel", "Lcom/discord/stores/updates/ObservationDeck$LogLevel;", HookHelper.constructorName, "(Lcom/discord/utilities/logging/Logger;Lcom/discord/stores/updates/ObservationDeck$LogLevel;)V", "Companion", "LogLevel", "Observer", "UpdateSource", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ObservationDeck {
    private static final String LOG_CATEGORY = "ObservationDeck";
    private final LogLevel logLevel;
    private final Logger logger;
    private List<Observer> observers;
    public static final Companion Companion = new Companion(null);
    private static final Function0<Unit> ON_UPDATE_EMPTY = ObservationDeck$Companion$ON_UPDATE_EMPTY$1.INSTANCE;

    /* compiled from: ObservationDeck.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u001c\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/stores/updates/ObservationDeck$Companion;", "", "", "LOG_CATEGORY", "Ljava/lang/String;", "Lkotlin/Function0;", "", "ON_UPDATE_EMPTY", "Lkotlin/jvm/functions/Function0;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: ObservationDeck.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/stores/updates/ObservationDeck$LogLevel;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "NONE", "ERROR", "VERBOSE", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum LogLevel {
        NONE,
        ERROR,
        VERBOSE
    }

    /* compiled from: ObservationDeck.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0005\b&\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u001a\u0010\u0004J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\r\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\u0006\u0010\u0007R\u0018\u0010\t\u001a\u0004\u0018\u00010\u00058&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\u0007R$\u0010\f\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\n8\u0006@BX\u0086\u000e¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\f\u0010\u000eR\"\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00020\u000f8&@&X¦\u000e¢\u0006\f\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R\u001c\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00160\u00158&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0017\u0010\u0018¨\u0006\u001b"}, d2 = {"Lcom/discord/stores/updates/ObservationDeck$Observer;", "", "", "markStale", "()V", "", "toDebugLogString", "()Ljava/lang/String;", "getName", ModelAuditLogEntry.CHANGE_KEY_NAME, "", "<set-?>", "isStale", "Z", "()Z", "Lkotlin/Function0;", "getOnUpdate", "()Lkotlin/jvm/functions/Function0;", "setOnUpdate", "(Lkotlin/jvm/functions/Function0;)V", "onUpdate", "", "Lcom/discord/stores/updates/ObservationDeck$UpdateSource;", "getObservingUpdates", "()Ljava/util/Set;", "observingUpdates", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class Observer {
        private boolean isStale;

        public abstract String getName();

        public abstract Set<UpdateSource> getObservingUpdates();

        public abstract Function0<Unit> getOnUpdate();

        public final boolean isStale() {
            return this.isStale;
        }

        public final void markStale() {
            this.isStale = true;
            setOnUpdate(ObservationDeck.ON_UPDATE_EMPTY);
        }

        public abstract void setOnUpdate(Function0<Unit> function0);

        public final String toDebugLogString() {
            StringBuilder sb = new StringBuilder();
            StringBuilder R = a.R("Observer name: ");
            String name = getName();
            if (name == null) {
                name = "Unknown";
            }
            R.append(name);
            R.append('\n');
            sb.append(R.toString());
            sb.append(u.joinToString$default(getObservingUpdates(), ", ", null, null, 0, null, null, 62, null));
            String sb2 = sb.toString();
            m.checkNotNullExpressionValue(sb2, "stringBuilder.toString()");
            return sb2;
        }
    }

    /* compiled from: ObservationDeck.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\n\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\bf\u0018\u00002\u00020\u0001¨\u0006\u0002"}, d2 = {"Lcom/discord/stores/updates/ObservationDeck$UpdateSource;", "", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public interface UpdateSource {
    }

    public ObservationDeck() {
        this(null, null, 3, null);
    }

    public ObservationDeck(Logger logger, LogLevel logLevel) {
        m.checkNotNullParameter(logger, "logger");
        m.checkNotNullParameter(logLevel, "logLevel");
        this.logger = logger;
        this.logLevel = logLevel;
        this.observers = new ArrayList();
    }

    public static /* synthetic */ Observer connect$default(ObservationDeck observationDeck, UpdateSource[] updateSourceArr, boolean z2, String str, Function0 function0, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = true;
        }
        if ((i & 4) != 0) {
            str = null;
        }
        return observationDeck.connect(updateSourceArr, z2, str, function0);
    }

    public static /* synthetic */ Observable connectRx$default(ObservationDeck observationDeck, UpdateSource[] updateSourceArr, boolean z2, Emitter.BackpressureMode backpressureMode, String str, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = true;
        }
        if ((i & 4) != 0) {
            backpressureMode = Emitter.BackpressureMode.LATEST;
        }
        if ((i & 8) != 0) {
            str = null;
        }
        return observationDeck.connectRx(updateSourceArr, z2, backpressureMode, str);
    }

    private final void logBreadcrumb(String str) {
        if (this.logLevel == LogLevel.VERBOSE) {
            this.logger.recordBreadcrumb(str, LOG_CATEGORY);
        }
    }

    private final void logNotifyError(Throwable th, Set<? extends UpdateSource> set) {
        Logger logger = this.logger;
        Pair[] pairArr = new Pair[2];
        pairArr[0] = o.to("Update Sources", u.joinToString$default(set, ", ", null, null, 0, null, null, 62, null));
        List<Observer> list = this.observers;
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = list.iterator();
        while (true) {
            boolean z2 = true;
            if (it.hasNext()) {
                Object next = it.next();
                Observer observer = (Observer) next;
                if (!(set instanceof Collection) || !set.isEmpty()) {
                    for (UpdateSource updateSource : set) {
                        if (observer.getObservingUpdates().contains(updateSource)) {
                            break;
                        }
                    }
                }
                z2 = false;
                if (z2) {
                    arrayList.add(next);
                }
            } else {
                pairArr[1] = o.to("Observers", u.joinToString$default(arrayList, "\n", null, null, 0, null, ObservationDeck$logNotifyError$2.INSTANCE, 30, null));
                logger.e("ObservationDeck notify error", th, h0.mapOf(pairArr));
                return;
            }
        }
    }

    public final synchronized Observer connect(UpdateSource[] updateSourceArr, boolean z2, String str, Function0<Unit> function0) {
        Observer observationDeck$connect$observer$1;
        m.checkNotNullParameter(updateSourceArr, "updateSources");
        m.checkNotNullParameter(function0, "onUpdate");
        observationDeck$connect$observer$1 = new Observer(updateSourceArr, function0, str) { // from class: com.discord.stores.updates.ObservationDeck$connect$observer$1
            public final /* synthetic */ String $observerName;
            public final /* synthetic */ Function0 $onUpdate;
            public final /* synthetic */ ObservationDeck.UpdateSource[] $updateSources;
            private final String name;
            private final Set<ObservationDeck.UpdateSource> observingUpdates;
            private Function0<Unit> onUpdate;

            {
                this.$updateSources = updateSourceArr;
                this.$onUpdate = function0;
                this.$observerName = str;
                this.observingUpdates = k.toSet(updateSourceArr);
                this.onUpdate = function0;
                this.name = str == null ? String.valueOf(hashCode()) : str;
            }

            @Override // com.discord.stores.updates.ObservationDeck.Observer
            public String getName() {
                return this.name;
            }

            @Override // com.discord.stores.updates.ObservationDeck.Observer
            public Set<ObservationDeck.UpdateSource> getObservingUpdates() {
                return this.observingUpdates;
            }

            @Override // com.discord.stores.updates.ObservationDeck.Observer
            public Function0<Unit> getOnUpdate() {
                return this.onUpdate;
            }

            @Override // com.discord.stores.updates.ObservationDeck.Observer
            public void setOnUpdate(Function0<Unit> function02) {
                m.checkNotNullParameter(function02, "<set-?>");
                this.onUpdate = function02;
            }
        };
        connect(observationDeck$connect$observer$1, z2);
        return observationDeck$connect$observer$1;
    }

    public final synchronized Observable<Unit> connectRx(final UpdateSource[] updateSourceArr, final boolean z2, Emitter.BackpressureMode backpressureMode, final String str) {
        Observable<Unit> v;
        m.checkNotNullParameter(updateSourceArr, "updateSources");
        m.checkNotNullParameter(backpressureMode, "backpressureMode");
        final Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        ref$ObjectRef.element = null;
        v = Observable.n(new Action1<Emitter<Unit>>() { // from class: com.discord.stores.updates.ObservationDeck$connectRx$1

            /* compiled from: ObservationDeck.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.stores.updates.ObservationDeck$connectRx$1$1  reason: invalid class name */
            /* loaded from: classes.dex */
            public static final class AnonymousClass1 extends d0.z.d.o implements Function0<Unit> {
                public final /* synthetic */ Emitter $emitter;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public AnonymousClass1(Emitter emitter) {
                    super(0);
                    this.$emitter = emitter;
                }

                @Override // kotlin.jvm.functions.Function0
                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2() {
                    this.$emitter.onNext(Unit.a);
                }
            }

            public final void call(Emitter<Unit> emitter) {
                Ref$ObjectRef ref$ObjectRef2 = ref$ObjectRef;
                ObservationDeck observationDeck = ObservationDeck.this;
                boolean z3 = z2;
                String str2 = str;
                AnonymousClass1 r4 = new AnonymousClass1(emitter);
                ObservationDeck.UpdateSource[] updateSourceArr2 = updateSourceArr;
                ref$ObjectRef2.element = (T) observationDeck.connect((ObservationDeck.UpdateSource[]) Arrays.copyOf(updateSourceArr2, updateSourceArr2.length), z3, str2, r4);
            }
        }, backpressureMode).v(new Action0() { // from class: com.discord.stores.updates.ObservationDeck$connectRx$2
            @Override // rx.functions.Action0
            public final void call() {
                ObservationDeck.Observer observer = (ObservationDeck.Observer) ref$ObjectRef.element;
                if (observer != null) {
                    ObservationDeck.this.disconnect(observer);
                }
            }
        });
        m.checkNotNullExpressionValue(v, "Observable.create<Unit>(…rver?.let(::disconnect) }");
        return v;
    }

    public final synchronized void disconnect(Observer observer) {
        m.checkNotNullParameter(observer, "observer");
        logBreadcrumb("disconnect START. observer: " + observer.getName());
        observer.markStale();
        logBreadcrumb("disconnect END. observer: " + observer.getName());
    }

    public final synchronized void notify(Set<? extends UpdateSource> set) {
        boolean z2;
        m.checkNotNullParameter(set, "updates");
        logBreadcrumb("notify START");
        int i = 0;
        while (i < this.observers.size()) {
            Observer observer = this.observers.get(i);
            if (observer.isStale()) {
                logBreadcrumb("removing observer: " + observer.getName());
                this.observers.remove(i);
                i += -1;
            } else {
                if (!(set instanceof Collection) || !set.isEmpty()) {
                    for (UpdateSource updateSource : set) {
                        if (observer.getObservingUpdates().contains(updateSource)) {
                            z2 = true;
                            break;
                        }
                    }
                }
                z2 = false;
                if (z2) {
                    observer.getOnUpdate().invoke();
                }
            }
            i++;
        }
        logBreadcrumb("notify END");
    }

    public static /* synthetic */ Observer connect$default(ObservationDeck observationDeck, Observer observer, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = true;
        }
        return observationDeck.connect(observer, z2);
    }

    public /* synthetic */ ObservationDeck(Logger logger, LogLevel logLevel, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? AppLog.g : logger, (i & 2) != 0 ? LogLevel.NONE : logLevel);
    }

    public static /* synthetic */ Observable connectRx$default(ObservationDeck observationDeck, UpdateSource[] updateSourceArr, boolean z2, Emitter.BackpressureMode backpressureMode, String str, Function0 function0, int i, Object obj) {
        boolean z3 = (i & 2) != 0 ? true : z2;
        if ((i & 4) != 0) {
            backpressureMode = Emitter.BackpressureMode.LATEST;
        }
        Emitter.BackpressureMode backpressureMode2 = backpressureMode;
        if ((i & 8) != 0) {
            str = null;
        }
        return observationDeck.connectRx(updateSourceArr, z3, backpressureMode2, str, function0);
    }

    public final synchronized Observer connect(Observer observer, boolean z2) {
        m.checkNotNullParameter(observer, "observer");
        logBreadcrumb("connect START. observer: " + observer.getName());
        this.observers.add(observer);
        if (z2) {
            observer.getOnUpdate().invoke();
        }
        logBreadcrumb("connect END. observer: " + observer.getName() + " -- isStale: " + observer.isStale());
        return observer;
    }

    public final synchronized <T> Observable<T> connectRx(UpdateSource[] updateSourceArr, boolean z2, Emitter.BackpressureMode backpressureMode, String str, final Function0<? extends T> function0) {
        Observable<T> observable;
        m.checkNotNullParameter(updateSourceArr, "updateSources");
        m.checkNotNullParameter(backpressureMode, "backpressureMode");
        m.checkNotNullParameter(function0, "generator");
        observable = (Observable<T>) connectRx((UpdateSource[]) Arrays.copyOf(updateSourceArr, updateSourceArr.length), z2, backpressureMode, str).F(new b<Unit, T>() { // from class: com.discord.stores.updates.ObservationDeck$connectRx$3
            public final T call(Unit unit) {
                return (T) Function0.this.invoke();
            }
        });
        m.checkNotNullExpressionValue(observable, "connectRx(\n        *upda…    ).map { generator() }");
        return observable;
    }
}
