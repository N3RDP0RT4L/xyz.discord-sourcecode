package com.discord.stores.updates;

import com.discord.stores.updates.ObservationDeck;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: ObservationDeck.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/stores/updates/ObservationDeck$Observer;", "observer", "", "invoke", "(Lcom/discord/stores/updates/ObservationDeck$Observer;)Ljava/lang/CharSequence;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ObservationDeck$logNotifyError$2 extends o implements Function1<ObservationDeck.Observer, CharSequence> {
    public static final ObservationDeck$logNotifyError$2 INSTANCE = new ObservationDeck$logNotifyError$2();

    public ObservationDeck$logNotifyError$2() {
        super(1);
    }

    public final CharSequence invoke(ObservationDeck.Observer observer) {
        m.checkNotNullParameter(observer, "observer");
        return observer.toDebugLogString();
    }
}
