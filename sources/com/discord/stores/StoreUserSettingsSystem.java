package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import b.d.b.a.a;
import com.discord.models.domain.ModelPayload;
import com.discord.models.domain.ModelUserSettings;
import com.discord.models.domain.auth.ModelLoginResult;
import com.discord.models.domain.auth.ModelUserSettingsBootstrap;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.cache.SharedPreferenceExtensionsKt;
import d0.z.d.m;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreUserSettingsSystem.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 C2\u00020\u0001:\u0002CDB#\u0012\u0012\u0010<\u001a\u000e\u0012\u0004\u0012\u00020$\u0012\u0004\u0012\u00020\u00040;\u0012\u0006\u0010?\u001a\u00020>¢\u0006\u0004\bA\u0010BJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u0019\u0010\t\u001a\u00020\u00042\b\u0010\b\u001a\u0004\u0018\u00010\u0007H\u0003¢\u0006\u0004\b\t\u0010\nJ\u0019\u0010\f\u001a\u00020\u00042\b\u0010\u000b\u001a\u0004\u0018\u00010\u0007H\u0002¢\u0006\u0004\b\f\u0010\nJ\u001b\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f2\u0006\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b\u0011\u0010\u0012J\r\u0010\u0013\u001a\u00020\u0007¢\u0006\u0004\b\u0013\u0010\u0014J/\u0010\u0018\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\u0015\u001a\u00020\r2\u0010\b\u0002\u0010\u0017\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0016¢\u0006\u0004\b\u0018\u0010\u0019J\r\u0010\u001a\u001a\u00020\r¢\u0006\u0004\b\u001a\u0010\u001bJ\u0015\u0010\u001d\u001a\u00020\u00042\u0006\u0010\u001c\u001a\u00020\r¢\u0006\u0004\b\u001d\u0010\u001eJ\r\u0010\u001f\u001a\u00020\u0007¢\u0006\u0004\b\u001f\u0010\u0014J1\u0010 \u001a\u00020\u00042\b\u0010\u000b\u001a\u0004\u0018\u00010\u00072\u0006\u0010\u0015\u001a\u00020\r2\u0010\b\u0002\u0010\u0017\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0016¢\u0006\u0004\b \u0010\u0019J\r\u0010!\u001a\u00020\r¢\u0006\u0004\b!\u0010\u001bJ\u0015\u0010#\u001a\u00020\u00042\u0006\u0010\"\u001a\u00020\r¢\u0006\u0004\b#\u0010\u001eJ\r\u0010%\u001a\u00020$¢\u0006\u0004\b%\u0010&J\u0015\u0010(\u001a\u00020\u00042\u0006\u0010'\u001a\u00020$¢\u0006\u0004\b(\u0010)J\u0017\u0010,\u001a\u00020\u00042\u0006\u0010+\u001a\u00020*H\u0017¢\u0006\u0004\b,\u0010-J\u000f\u0010.\u001a\u00020\u0004H\u0007¢\u0006\u0004\b.\u0010/J\u0017\u00102\u001a\u00020\u00042\u0006\u00101\u001a\u000200H\u0007¢\u0006\u0004\b2\u00103J\u0017\u00106\u001a\u00020\u00042\u0006\u00105\u001a\u000204H\u0007¢\u0006\u0004\b6\u00107J\u0017\u00108\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b8\u0010\u0006R\u0016\u00109\u001a\u00020\u00108\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b9\u0010:R\"\u0010<\u001a\u000e\u0012\u0004\u0012\u00020$\u0012\u0004\u0012\u00020\u00040;8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b<\u0010=R\u0016\u0010?\u001a\u00020>8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b?\u0010@¨\u0006E"}, d2 = {"Lcom/discord/stores/StoreUserSettingsSystem;", "Lcom/discord/stores/StoreV2;", "Lcom/discord/models/domain/ModelUserSettings;", "userSettings", "", "handleUserSettings", "(Lcom/discord/models/domain/ModelUserSettings;)V", "", "theme", "handleUserSettingsThemeUpdate", "(Ljava/lang/String;)V", "locale", "handleUserSettingsLocaleUpdate", "", "sample", "Lrx/Observable;", "Lcom/discord/stores/StoreUserSettingsSystem$Settings;", "observeSettings", "(Z)Lrx/Observable;", "getTheme", "()Ljava/lang/String;", "apiSync", "Lkotlin/Function0;", "onRequestApiSync", "setTheme", "(Ljava/lang/String;ZLkotlin/jvm/functions/Function0;)V", "getIsThemeSyncEnabled", "()Z", "isSyncThemeEnabled", "setIsSyncThemeEnabled", "(Z)V", "getLocale", "setLocale", "getIsLocaleSyncEnabled", "isLocaleSyncEnabled", "setIsLocaleSyncEnabled", "", "getFontScale", "()I", "fontScale", "setFontScale", "(I)V", "Landroid/content/Context;", "context", "init", "(Landroid/content/Context;)V", "handlePreLogout", "()V", "Lcom/discord/models/domain/auth/ModelLoginResult;", "loginResult", "handleLoginResult", "(Lcom/discord/models/domain/auth/ModelLoginResult;)V", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "handleUserSettingsUpdate", "settings", "Lcom/discord/stores/StoreUserSettingsSystem$Settings;", "Lkotlin/Function1;", "onFontScaleUpdated", "Lkotlin/jvm/functions/Function1;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", HookHelper.constructorName, "(Lkotlin/jvm/functions/Function1;Lcom/discord/stores/Dispatcher;)V", "Companion", "Settings", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserSettingsSystem extends StoreV2 {
    public static final Companion Companion = new Companion(null);
    private static final boolean DEFAULT_IS_CLIENT_SYNC_ENABLED = true;
    private final Dispatcher dispatcher;
    private final Function1<Integer, Unit> onFontScaleUpdated;
    private Settings settings;

    /* compiled from: StoreUserSettingsSystem.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/stores/StoreUserSettingsSystem$Companion;", "", "", "DEFAULT_IS_CLIENT_SYNC_ENABLED", "Z", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreUserSettingsSystem.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\n\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\u000b\u001a\u00020\u0006¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ0\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\t\u001a\u00020\u00022\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\u000b\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000e\u0010\u0004J\u0010\u0010\u000f\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u000f\u0010\bJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u0013\u0010\u0015\u001a\u00020\u00028F@\u0006¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0004R\u001b\u0010\n\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0016\u001a\u0004\b\u0018\u0010\u0004R\u0019\u0010\u000b\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0019\u001a\u0004\b\u001a\u0010\b¨\u0006\u001d"}, d2 = {"Lcom/discord/stores/StoreUserSettingsSystem$Settings;", "", "", "component1", "()Ljava/lang/String;", "component2", "", "component3", "()I", "theme", "_locale", "fontScale", "copy", "(Ljava/lang/String;Ljava/lang/String;I)Lcom/discord/stores/StoreUserSettingsSystem$Settings;", "toString", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "getLocale", "locale", "Ljava/lang/String;", "get_locale", "getTheme", "I", "getFontScale", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Settings {
        private final String _locale;
        private final int fontScale;
        private final String theme;

        public Settings(String str, String str2, int i) {
            m.checkNotNullParameter(str, "theme");
            this.theme = str;
            this._locale = str2;
            this.fontScale = i;
        }

        public static /* synthetic */ Settings copy$default(Settings settings, String str, String str2, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                str = settings.theme;
            }
            if ((i2 & 2) != 0) {
                str2 = settings._locale;
            }
            if ((i2 & 4) != 0) {
                i = settings.fontScale;
            }
            return settings.copy(str, str2, i);
        }

        public final String component1() {
            return this.theme;
        }

        public final String component2() {
            return this._locale;
        }

        public final int component3() {
            return this.fontScale;
        }

        public final Settings copy(String str, String str2, int i) {
            m.checkNotNullParameter(str, "theme");
            return new Settings(str, str2, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Settings)) {
                return false;
            }
            Settings settings = (Settings) obj;
            return m.areEqual(this.theme, settings.theme) && m.areEqual(this._locale, settings._locale) && this.fontScale == settings.fontScale;
        }

        public final int getFontScale() {
            return this.fontScale;
        }

        public final String getLocale() {
            String str = this._locale;
            if (str != null) {
                return str;
            }
            String defaultLocale = ModelUserSettings.getDefaultLocale();
            m.checkNotNullExpressionValue(defaultLocale, "ModelUserSettings.getDefaultLocale()");
            return defaultLocale;
        }

        public final String getTheme() {
            return this.theme;
        }

        public final String get_locale() {
            return this._locale;
        }

        public int hashCode() {
            String str = this.theme;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this._locale;
            if (str2 != null) {
                i = str2.hashCode();
            }
            return ((hashCode + i) * 31) + this.fontScale;
        }

        public String toString() {
            StringBuilder R = a.R("Settings(theme=");
            R.append(this.theme);
            R.append(", _locale=");
            R.append(this._locale);
            R.append(", fontScale=");
            return a.A(R, this.fontScale, ")");
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public StoreUserSettingsSystem(Function1<? super Integer, Unit> function1, Dispatcher dispatcher) {
        m.checkNotNullParameter(function1, "onFontScaleUpdated");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        this.onFontScaleUpdated = function1;
        this.dispatcher = dispatcher;
    }

    public static final /* synthetic */ Settings access$getSettings$p(StoreUserSettingsSystem storeUserSettingsSystem) {
        Settings settings = storeUserSettingsSystem.settings;
        if (settings == null) {
            m.throwUninitializedPropertyAccessException("settings");
        }
        return settings;
    }

    @StoreThread
    private final void handleUserSettings(ModelUserSettings modelUserSettings) {
        handleUserSettingsThemeUpdate(modelUserSettings.getTheme());
        handleUserSettingsLocaleUpdate(modelUserSettings.getLocale());
    }

    private final void handleUserSettingsLocaleUpdate(String str) {
        if (str != null && getIsLocaleSyncEnabled()) {
            setLocale$default(this, str, false, null, 4, null);
        }
    }

    @StoreThread
    private final void handleUserSettingsThemeUpdate(String str) {
        if (str != null && getIsThemeSyncEnabled()) {
            setTheme$default(this, str, false, null, 4, null);
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void setLocale$default(StoreUserSettingsSystem storeUserSettingsSystem, String str, boolean z2, Function0 function0, int i, Object obj) {
        if ((i & 4) != 0) {
            function0 = null;
        }
        storeUserSettingsSystem.setLocale(str, z2, function0);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void setTheme$default(StoreUserSettingsSystem storeUserSettingsSystem, String str, boolean z2, Function0 function0, int i, Object obj) {
        if ((i & 4) != 0) {
            function0 = null;
        }
        storeUserSettingsSystem.setTheme(str, z2, function0);
    }

    public final int getFontScale() {
        Settings settings = this.settings;
        if (settings == null) {
            m.throwUninitializedPropertyAccessException("settings");
        }
        return settings.getFontScale();
    }

    public final boolean getIsLocaleSyncEnabled() {
        return getPrefs().getBoolean("CACHE_KEY_LOCALE_SYNC", true);
    }

    public final boolean getIsThemeSyncEnabled() {
        return getPrefsSessionDurable().getBoolean("CACHE_KEY_THEME_SYNC", true);
    }

    public final String getLocale() {
        Settings settings = this.settings;
        if (settings == null) {
            m.throwUninitializedPropertyAccessException("settings");
        }
        return settings.getLocale();
    }

    public final String getTheme() {
        Settings settings = this.settings;
        if (settings == null) {
            m.throwUninitializedPropertyAccessException("settings");
        }
        return settings.getTheme();
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        ModelUserSettings userSettings = modelPayload.getUserSettings();
        m.checkNotNullExpressionValue(userSettings, "payload.userSettings");
        handleUserSettings(userSettings);
    }

    @StoreThread
    public final void handleLoginResult(ModelLoginResult modelLoginResult) {
        m.checkNotNullParameter(modelLoginResult, "loginResult");
        ModelUserSettingsBootstrap userSettings = modelLoginResult.getUserSettings();
        String str = null;
        handleUserSettingsThemeUpdate(userSettings != null ? userSettings.getTheme() : null);
        ModelUserSettingsBootstrap userSettings2 = modelLoginResult.getUserSettings();
        if (userSettings2 != null) {
            str = userSettings2.getLocale();
        }
        handleUserSettingsLocaleUpdate(str);
    }

    @StoreThread
    public final void handlePreLogout() {
        setIsSyncThemeEnabled(true);
        setIsLocaleSyncEnabled(true);
        setLocale$default(this, null, false, null, 4, null);
        setFontScale(-1);
    }

    @StoreThread
    public final void handleUserSettingsUpdate(ModelUserSettings modelUserSettings) {
        m.checkNotNullParameter(modelUserSettings, "userSettings");
        handleUserSettings(modelUserSettings);
    }

    @Override // com.discord.stores.Store
    @StoreThread
    public void init(Context context) {
        m.checkNotNullParameter(context, "context");
        super.init(context);
        Resources resources = context.getResources();
        m.checkNotNullExpressionValue(resources, "context.resources");
        this.settings = new Settings(SharedPreferenceExtensionsKt.getStringNonNull(getPrefsSessionDurable(), "CACHE_KEY_THEME", (resources.getConfiguration().uiMode & 48) != 32 ? ModelUserSettings.THEME_LIGHT : ModelUserSettings.THEME_DARK), getPrefs().getString("CACHE_KEY_LOCALE", null), getPrefs().getInt("CACHE_KEY_FONT_SCALE", -1));
    }

    public final Observable<Settings> observeSettings(boolean z2) {
        Observable connectRx$default = ObservationDeck.connectRx$default(ObservationDeckProvider.get(), new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreUserSettingsSystem$observeSettings$1(this), 14, null);
        if (z2) {
            connectRx$default = connectRx$default.O(1500L, TimeUnit.MILLISECONDS);
        }
        Observable<Settings> q = connectRx$default.q();
        m.checkNotNullExpressionValue(q, "ObservationDeckProvider\n…  .distinctUntilChanged()");
        return q;
    }

    public final void setFontScale(int i) {
        SharedPreferences.Editor edit = getPrefs().edit();
        m.checkNotNullExpressionValue(edit, "editor");
        edit.putInt("CACHE_KEY_FONT_SCALE", i);
        edit.apply();
        this.dispatcher.schedule(new StoreUserSettingsSystem$setFontScale$2(this, i));
    }

    public final void setIsLocaleSyncEnabled(boolean z2) {
        SharedPreferences.Editor edit = getPrefs().edit();
        m.checkNotNullExpressionValue(edit, "editor");
        edit.putBoolean("CACHE_KEY_LOCALE_SYNC", z2);
        edit.apply();
    }

    public final void setIsSyncThemeEnabled(boolean z2) {
        SharedPreferences.Editor edit = getPrefsSessionDurable().edit();
        m.checkNotNullExpressionValue(edit, "editor");
        edit.putBoolean("CACHE_KEY_THEME_SYNC", z2);
        edit.apply();
    }

    public final void setLocale(String str, boolean z2, Function0<Unit> function0) {
        if (!getIsLocaleSyncEnabled() || !z2 || !(function0 == null || function0.invoke() == null)) {
            Settings settings = this.settings;
            if (settings == null) {
                m.throwUninitializedPropertyAccessException("settings");
            }
            this.settings = Settings.copy$default(settings, null, str, 0, 5, null);
            SharedPreferences.Editor edit = getPrefs().edit();
            m.checkNotNullExpressionValue(edit, "editor");
            edit.putString("CACHE_KEY_LOCALE", str);
            edit.apply();
            markChanged();
            return;
        }
        throw new IllegalArgumentException("API callback required.");
    }

    public final void setTheme(String str, boolean z2, Function0<Unit> function0) {
        m.checkNotNullParameter(str, "theme");
        if (!getIsThemeSyncEnabled() || !z2 || !(function0 == null || function0.invoke() == null)) {
            if (m.areEqual(getTheme(), ModelUserSettings.THEME_PURE_EVIL) && m.areEqual(str, ModelUserSettings.THEME_DARK)) {
                str = ModelUserSettings.THEME_PURE_EVIL;
            }
            Settings settings = this.settings;
            if (settings == null) {
                m.throwUninitializedPropertyAccessException("settings");
            }
            this.settings = Settings.copy$default(settings, str, null, 0, 6, null);
            SharedPreferences.Editor edit = getPrefsSessionDurable().edit();
            m.checkNotNullExpressionValue(edit, "editor");
            edit.putString("CACHE_KEY_THEME", str);
            edit.apply();
            markChanged();
            return;
        }
        throw new IllegalArgumentException("API callback required.");
    }
}
