package com.discord.stores;

import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.message.MessageReference;
import com.discord.models.message.Message;
import com.discord.stores.StorePendingReplies;
import com.discord.utilities.collections.SnowflakePartitionMap;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StorePendingReplies.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StorePendingReplies$onCreatePendingReply$1 extends o implements Function0<Unit> {
    public final /* synthetic */ Channel $channel;
    public final /* synthetic */ Message $message;
    public final /* synthetic */ boolean $shouldMention;
    public final /* synthetic */ boolean $showMentionToggle;
    public final /* synthetic */ StorePendingReplies this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StorePendingReplies$onCreatePendingReply$1(StorePendingReplies storePendingReplies, Channel channel, Message message, boolean z2, boolean z3) {
        super(0);
        this.this$0 = storePendingReplies;
        this.$channel = channel;
        this.$message = message;
        this.$shouldMention = z2;
        this.$showMentionToggle = z3;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        SnowflakePartitionMap.CopiablePartitionMap copiablePartitionMap;
        Long valueOf = ChannelUtils.x(this.$channel) ? null : Long.valueOf(this.$channel.f());
        copiablePartitionMap = this.this$0.pendingReplies;
        copiablePartitionMap.put(Long.valueOf(this.$channel.h()), new StorePendingReplies.PendingReply(new MessageReference(valueOf, Long.valueOf(this.$channel.h()), Long.valueOf(this.$message.getId())), this.$message, this.$shouldMention, this.$showMentionToggle));
        this.this$0.markChanged();
    }
}
