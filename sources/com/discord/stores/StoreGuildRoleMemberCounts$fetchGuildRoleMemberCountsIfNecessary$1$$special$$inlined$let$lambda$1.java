package com.discord.stores;

import d0.z.d.o;
import java.util.HashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreGuildRoleMemberCounts.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002¨\u0006\u0003"}, d2 = {"", "invoke", "()V", "com/discord/stores/StoreGuildRoleMemberCounts$fetchGuildRoleMemberCountsIfNecessary$1$1$1", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildRoleMemberCounts$fetchGuildRoleMemberCountsIfNecessary$1$$special$$inlined$let$lambda$1 extends o implements Function0<Unit> {
    public final /* synthetic */ Map $it;
    public final /* synthetic */ StoreGuildRoleMemberCounts$fetchGuildRoleMemberCountsIfNecessary$1 this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGuildRoleMemberCounts$fetchGuildRoleMemberCountsIfNecessary$1$$special$$inlined$let$lambda$1(Map map, StoreGuildRoleMemberCounts$fetchGuildRoleMemberCountsIfNecessary$1 storeGuildRoleMemberCounts$fetchGuildRoleMemberCountsIfNecessary$1) {
        super(0);
        this.$it = map;
        this.this$0 = storeGuildRoleMemberCounts$fetchGuildRoleMemberCountsIfNecessary$1;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        HashMap hashMap;
        hashMap = this.this$0.this$0.guildRoleMemberCounts;
        hashMap.put(Long.valueOf(this.this$0.$guildId), this.$it);
        this.this$0.this$0.markChanged();
    }
}
