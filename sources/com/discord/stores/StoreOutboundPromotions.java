package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.SharedPreferences;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.api.premium.ClaimedOutboundPromotion;
import com.discord.api.premium.OutboundPromotion;
import com.discord.api.user.User;
import com.discord.models.domain.ModelEntitlement;
import com.discord.models.domain.ModelPayload;
import com.discord.models.user.MeUser;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.user.UserUtils;
import com.discord.widgets.settings.premium.OutboundPromosPreviewFeatureFlag;
import d0.t.n;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Action1;
/* compiled from: StoreOutboundPromotions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 12\u00020\u0001:\u000212B+\u0012\u0006\u0010!\u001a\u00020 \u0012\u0006\u0010)\u001a\u00020(\u0012\b\b\u0002\u0010&\u001a\u00020%\u0012\b\b\u0002\u0010,\u001a\u00020+¢\u0006\u0004\b/\u00100J\u001d\u0010\u0006\u001a\u00020\u00052\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\r\u0010\t\u001a\u00020\b¢\u0006\u0004\b\t\u0010\nJ\u0013\u0010\f\u001a\b\u0012\u0004\u0012\u00020\b0\u000b¢\u0006\u0004\b\f\u0010\rJ\r\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\u0006\u0010\u000eJ\u0013\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00050\u000b¢\u0006\u0004\b\u000f\u0010\rJ\u0017\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0011\u001a\u00020\u0010H\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\r\u0010\u0016\u001a\u00020\u0012¢\u0006\u0004\b\u0016\u0010\u0017J\u0019\u0010\u0019\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00180\u00020\u000b¢\u0006\u0004\b\u0019\u0010\rJ\u001f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00180\u000b2\n\u0010\u001c\u001a\u00060\u001aj\u0002`\u001b¢\u0006\u0004\b\u001d\u0010\u001eJ\u000f\u0010\u001f\u001a\u00020\u0012H\u0016¢\u0006\u0004\b\u001f\u0010\u0017R\u0016\u0010!\u001a\u00020 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"R\u0016\u0010#\u001a\u00020\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b#\u0010$R\u0016\u0010&\u001a\u00020%8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b&\u0010'R\u0016\u0010)\u001a\u00020(8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b)\u0010*R\u0016\u0010,\u001a\u00020+8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b,\u0010-R\u0016\u0010.\u001a\u00020\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b.\u0010$¨\u00063"}, d2 = {"Lcom/discord/stores/StoreOutboundPromotions;", "Lcom/discord/stores/StoreV2;", "", "Lcom/discord/api/premium/OutboundPromotion;", "promos", "", "getUnseenCount", "(Ljava/util/List;)I", "Lcom/discord/stores/StoreOutboundPromotions$State;", "getState", "()Lcom/discord/stores/StoreOutboundPromotions$State;", "Lrx/Observable;", "observeState", "()Lrx/Observable;", "()I", "observeUnseenCount", "Lcom/discord/models/domain/ModelPayload;", "readyPayload", "", "handleConnectionOpen$app_productionGoogleRelease", "(Lcom/discord/models/domain/ModelPayload;)V", "handleConnectionOpen", "markSeen", "()V", "Lcom/discord/api/premium/ClaimedOutboundPromotion;", "fetchClaimedOutboundPromotions", "", "Lcom/discord/primitives/PromoId;", "promotionId", "claimOutboundPromotion", "(J)Lrx/Observable;", "snapshotData", "Lcom/discord/widgets/settings/premium/OutboundPromosPreviewFeatureFlag;", "previewFeatureFlag", "Lcom/discord/widgets/settings/premium/OutboundPromosPreviewFeatureFlag;", "state", "Lcom/discord/stores/StoreOutboundPromotions$State;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "stateSnapshot", HookHelper.constructorName, "(Lcom/discord/widgets/settings/premium/OutboundPromosPreviewFeatureFlag;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/utilities/rest/RestAPI;)V", "Companion", "State", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreOutboundPromotions extends StoreV2 {
    private static final String CONSUMED_ENTITLEMENT_PROMO_ID = "CONSUMED_ENTITLEMENT_CODE";
    public static final Companion Companion = new Companion(null);
    private static final String LATEST_SEEN_PROMO_DATE = "LATEST_SEEN_PROMO_DATE";
    private static final long NO_PROMO_ID = -1;
    private static final long NO_SEEN_PROMO_DATE = -1;
    private final Dispatcher dispatcher;
    private final ObservationDeck observationDeck;
    private final OutboundPromosPreviewFeatureFlag previewFeatureFlag;
    private final RestAPI restAPI;
    private State state;
    private State stateSnapshot;

    /* compiled from: StoreOutboundPromotions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0007\u0010\bR\u0016\u0010\t\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\t\u0010\b¨\u0006\f"}, d2 = {"Lcom/discord/stores/StoreOutboundPromotions$Companion;", "", "", "CONSUMED_ENTITLEMENT_PROMO_ID", "Ljava/lang/String;", StoreOutboundPromotions.LATEST_SEEN_PROMO_DATE, "", "NO_PROMO_ID", "J", "NO_SEEN_PROMO_DATE", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreOutboundPromotions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0006\u0007\bB\t\b\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u000f\u0010\u0002\u001a\u00020\u0000H\u0016¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\t\n\u000b¨\u0006\f"}, d2 = {"Lcom/discord/stores/StoreOutboundPromotions$State;", "", "deepCopy", "()Lcom/discord/stores/StoreOutboundPromotions$State;", HookHelper.constructorName, "()V", "Failed", "Loaded", "Loading", "Lcom/discord/stores/StoreOutboundPromotions$State$Failed;", "Lcom/discord/stores/StoreOutboundPromotions$State$Loading;", "Lcom/discord/stores/StoreOutboundPromotions$State$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class State {

        /* compiled from: StoreOutboundPromotions.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreOutboundPromotions$State$Failed;", "Lcom/discord/stores/StoreOutboundPromotions$State;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Failed extends State {
            public static final Failed INSTANCE = new Failed();

            private Failed() {
                super(null);
            }
        }

        /* compiled from: StoreOutboundPromotions.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0006\u0010\f\u001a\u00020\b¢\u0006\u0004\b\u001c\u0010\u001dJ\u000f\u0010\u0002\u001a\u00020\u0000H\u0016¢\u0006\u0004\b\u0002\u0010\u0003J\u0016\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ*\u0010\r\u001a\u00020\u00002\u000e\b\u0002\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\b\b\u0002\u0010\f\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u0012\u0010\nJ\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u001f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0018\u001a\u0004\b\u0019\u0010\u0007R\u0019\u0010\f\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001a\u001a\u0004\b\u001b\u0010\n¨\u0006\u001e"}, d2 = {"Lcom/discord/stores/StoreOutboundPromotions$State$Loaded;", "Lcom/discord/stores/StoreOutboundPromotions$State;", "deepCopy", "()Lcom/discord/stores/StoreOutboundPromotions$State$Loaded;", "", "Lcom/discord/api/premium/OutboundPromotion;", "component1", "()Ljava/util/List;", "", "component2", "()I", "validActivePromotions", "unseenCount", "copy", "(Ljava/util/List;I)Lcom/discord/stores/StoreOutboundPromotions$State$Loaded;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getValidActivePromotions", "I", "getUnseenCount", HookHelper.constructorName, "(Ljava/util/List;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Loaded extends State {
            private final int unseenCount;
            private final List<OutboundPromotion> validActivePromotions;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loaded(List<OutboundPromotion> list, int i) {
                super(null);
                m.checkNotNullParameter(list, "validActivePromotions");
                this.validActivePromotions = list;
                this.unseenCount = i;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Loaded copy$default(Loaded loaded, List list, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    list = loaded.validActivePromotions;
                }
                if ((i2 & 2) != 0) {
                    i = loaded.unseenCount;
                }
                return loaded.copy(list, i);
            }

            public final List<OutboundPromotion> component1() {
                return this.validActivePromotions;
            }

            public final int component2() {
                return this.unseenCount;
            }

            public final Loaded copy(List<OutboundPromotion> list, int i) {
                m.checkNotNullParameter(list, "validActivePromotions");
                return new Loaded(list, i);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return m.areEqual(this.validActivePromotions, loaded.validActivePromotions) && this.unseenCount == loaded.unseenCount;
            }

            public final int getUnseenCount() {
                return this.unseenCount;
            }

            public final List<OutboundPromotion> getValidActivePromotions() {
                return this.validActivePromotions;
            }

            public int hashCode() {
                List<OutboundPromotion> list = this.validActivePromotions;
                return ((list != null ? list.hashCode() : 0) * 31) + this.unseenCount;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(validActivePromotions=");
                R.append(this.validActivePromotions);
                R.append(", unseenCount=");
                return a.A(R, this.unseenCount, ")");
            }

            @Override // com.discord.stores.StoreOutboundPromotions.State
            public Loaded deepCopy() {
                return copy$default(this, new ArrayList(this.validActivePromotions), 0, 2, null);
            }
        }

        /* compiled from: StoreOutboundPromotions.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreOutboundPromotions$State$Loading;", "Lcom/discord/stores/StoreOutboundPromotions$State;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Loading extends State {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        private State() {
        }

        public State deepCopy() {
            return this;
        }

        public /* synthetic */ State(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ StoreOutboundPromotions(OutboundPromosPreviewFeatureFlag outboundPromosPreviewFeatureFlag, Dispatcher dispatcher, ObservationDeck observationDeck, RestAPI restAPI, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(outboundPromosPreviewFeatureFlag, dispatcher, (i & 4) != 0 ? ObservationDeckProvider.get() : observationDeck, (i & 8) != 0 ? RestAPI.Companion.getApi() : restAPI);
    }

    public final Observable<ClaimedOutboundPromotion> claimOutboundPromotion(long j) {
        return this.restAPI.claimOutboundPromotion(j);
    }

    public final Observable<List<ClaimedOutboundPromotion>> fetchClaimedOutboundPromotions() {
        return this.restAPI.getClaimedOutboundPromotions();
    }

    public final State getState() {
        return this.stateSnapshot;
    }

    public final int getUnseenCount() {
        State state = getState();
        if (!(state instanceof State.Loaded)) {
            state = null;
        }
        State.Loaded loaded = (State.Loaded) state;
        if (loaded != null) {
            return loaded.getUnseenCount();
        }
        return 0;
    }

    @StoreThread
    public final void handleConnectionOpen$app_productionGoogleRelease(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "readyPayload");
        UserUtils userUtils = UserUtils.INSTANCE;
        User me2 = modelPayload.getMe();
        m.checkNotNullExpressionValue(me2, "readyPayload.me");
        if (userUtils.isPremium(new MeUser(me2))) {
            this.state = State.Loading.INSTANCE;
            markChanged();
            RestAPI restAPI = this.restAPI;
            Observable z2 = ObservableExtensionsKt.restSubscribeOn$default(this.previewFeatureFlag.isEnabled() ? restAPI.getAllPreviewPromotions() : restAPI.getAllActiveOutboundPromotions(), false, 1, null).z(new b<List<? extends OutboundPromotion>, Observable<? extends List<? extends OutboundPromotion>>>() { // from class: com.discord.stores.StoreOutboundPromotions$handleConnectionOpen$2

                /* compiled from: StoreOutboundPromotions.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\b\u001a\u0014 \u0002*\n\u0018\u00010\u0004j\u0004\u0018\u0001`\u00050\u0004j\u0002`\u00052\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "Lcom/discord/models/domain/ModelEntitlement;", "kotlin.jvm.PlatformType", "entitlements", "", "Lcom/discord/primitives/PromoId;", NotificationCompat.CATEGORY_CALL, "(Ljava/util/List;)Ljava/lang/Long;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.stores.StoreOutboundPromotions$handleConnectionOpen$2$2  reason: invalid class name */
                /* loaded from: classes.dex */
                public static final class AnonymousClass2<T, R> implements b<List<? extends ModelEntitlement>, Long> {
                    public static final AnonymousClass2 INSTANCE = new AnonymousClass2();

                    @Override // j0.k.b
                    public /* bridge */ /* synthetic */ Long call(List<? extends ModelEntitlement> list) {
                        return call2((List<ModelEntitlement>) list);
                    }

                    /* renamed from: call  reason: avoid collision after fix types in other method */
                    public final Long call2(List<ModelEntitlement> list) {
                        T t;
                        Long promotionId;
                        boolean z2;
                        m.checkNotNullExpressionValue(list, "entitlements");
                        Iterator<T> it = list.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                t = null;
                                break;
                            }
                            t = it.next();
                            ModelEntitlement modelEntitlement = (ModelEntitlement) t;
                            if (!m.areEqual(modelEntitlement.getConsumed(), Boolean.TRUE) || modelEntitlement.getPromotionId() == null) {
                                z2 = false;
                                continue;
                            } else {
                                z2 = true;
                                continue;
                            }
                            if (z2) {
                                break;
                            }
                        }
                        ModelEntitlement modelEntitlement2 = (ModelEntitlement) t;
                        return Long.valueOf((modelEntitlement2 == null || (promotionId = modelEntitlement2.getPromotionId()) == null) ? -1L : promotionId.longValue());
                    }
                }

                @Override // j0.k.b
                public /* bridge */ /* synthetic */ Observable<? extends List<? extends OutboundPromotion>> call(List<? extends OutboundPromotion> list) {
                    return call2((List<OutboundPromotion>) list);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final Observable<? extends List<OutboundPromotion>> call2(final List<OutboundPromotion> list) {
                    RestAPI restAPI2;
                    if (StoreOutboundPromotions.this.getPrefs().contains("CONSUMED_ENTITLEMENT_CODE")) {
                        return Observable.C(new Callable<List<? extends OutboundPromotion>>() { // from class: com.discord.stores.StoreOutboundPromotions$handleConnectionOpen$2.1
                            @Override // java.util.concurrent.Callable
                            public final List<? extends OutboundPromotion> call() {
                                long j = StoreOutboundPromotions.this.getPrefs().getLong("CONSUMED_ENTITLEMENT_CODE", -1L);
                                List list2 = list;
                                ArrayList Y = a.Y(list2, "activePromos");
                                for (T t : list2) {
                                    if (((OutboundPromotion) t).b() != j) {
                                        Y.add(t);
                                    }
                                }
                                return Y;
                            }
                        });
                    }
                    m.checkNotNullExpressionValue(list, "activePromos");
                    if (!(!list.isEmpty())) {
                        return new k(list);
                    }
                    restAPI2 = StoreOutboundPromotions.this.restAPI;
                    return (Observable<R>) ObservableExtensionsKt.restSubscribeOn$default(restAPI2.getMyEntitlements(521842831262875670L, false), false, 1, null).F(AnonymousClass2.INSTANCE).t(new Action1<Long>() { // from class: com.discord.stores.StoreOutboundPromotions$handleConnectionOpen$2.3
                        public final void call(Long l) {
                            SharedPreferences.Editor edit = StoreOutboundPromotions.this.getPrefs().edit();
                            m.checkNotNullExpressionValue(edit, "editor");
                            m.checkNotNullExpressionValue(l, "consumedPromoId");
                            edit.putLong("CONSUMED_ENTITLEMENT_CODE", l.longValue());
                            edit.apply();
                        }
                    }).F(new b<Long, List<? extends OutboundPromotion>>() { // from class: com.discord.stores.StoreOutboundPromotions$handleConnectionOpen$2.4
                        public final List<OutboundPromotion> call(Long l) {
                            List list2 = list;
                            ArrayList Y = a.Y(list2, "activePromos");
                            for (T t : list2) {
                                if (l == null || ((OutboundPromotion) t).b() != l.longValue()) {
                                    Y.add(t);
                                }
                            }
                            return Y;
                        }
                    });
                }
            });
            m.checkNotNullExpressionValue(z2, "restAPI.run {\n        if…            }\n          }");
            ObservableExtensionsKt.appSubscribe(z2, StoreOutboundPromotions.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new StoreOutboundPromotions$handleConnectionOpen$3(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreOutboundPromotions$handleConnectionOpen$4(this));
            return;
        }
        this.state = new State.Loaded(n.emptyList(), 0);
        markChanged();
    }

    public final void markSeen() {
        this.dispatcher.schedule(new StoreOutboundPromotions$markSeen$1(this));
    }

    public final Observable<State> observeState() {
        Observable<State> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreOutboundPromotions$observeState$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Integer> observeUnseenCount() {
        Observable<Integer> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreOutboundPromotions$observeUnseenCount$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR…  .distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        super.snapshotData();
        this.stateSnapshot = this.state.deepCopy();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final int getUnseenCount(List<OutboundPromotion> list) {
        long j = getPrefs().getLong(LATEST_SEEN_PROMO_DATE, -1L);
        if ((list instanceof Collection) && list.isEmpty()) {
            return 0;
        }
        Iterator<T> it = list.iterator();
        int i = 0;
        while (it.hasNext()) {
            if ((((OutboundPromotion) it.next()).h().g() > j) && (i = i + 1) < 0) {
                n.throwCountOverflow();
            }
        }
        return i;
    }

    public StoreOutboundPromotions(OutboundPromosPreviewFeatureFlag outboundPromosPreviewFeatureFlag, Dispatcher dispatcher, ObservationDeck observationDeck, RestAPI restAPI) {
        m.checkNotNullParameter(outboundPromosPreviewFeatureFlag, "previewFeatureFlag");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(restAPI, "restAPI");
        this.previewFeatureFlag = outboundPromosPreviewFeatureFlag;
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
        this.restAPI = restAPI;
        State.Loading loading = State.Loading.INSTANCE;
        this.state = loading;
        this.stateSnapshot = loading;
    }
}
