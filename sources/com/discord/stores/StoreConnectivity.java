package com.discord.stores;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.stores.StoreV2;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.networking.NetworkMonitor;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.Clock;
import d0.d0.f;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.s;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
/* compiled from: StoreConnectivity.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 A2\u00020\u0001:\u0003ABCB\u001f\u0012\u0006\u0010=\u001a\u00020<\u0012\u0006\u00105\u001a\u000204\u0012\u0006\u00102\u001a\u000201¢\u0006\u0004\b?\u0010@J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u000f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\u0014\u001a\u00020\r2\u0006\u0010\u0003\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u0012H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u000f\u0010\u0016\u001a\u00020\rH\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0013\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00110\u0018¢\u0006\u0004\b\u0019\u0010\u001aJ\u0017\u0010\u001d\u001a\u00020\u00042\u0006\u0010\u001c\u001a\u00020\u001bH\u0007¢\u0006\u0004\b\u001d\u0010\u001eJ\u0017\u0010 \u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\u0007H\u0007¢\u0006\u0004\b \u0010\nJ\u0017\u0010\"\u001a\u00020\u00042\u0006\u0010!\u001a\u00020\u0007H\u0007¢\u0006\u0004\b\"\u0010\nJ\u0017\u0010$\u001a\u00020\u00042\u0006\u0010#\u001a\u00020\u0007H\u0007¢\u0006\u0004\b$\u0010\nR\u0016\u0010%\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b%\u0010&R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\b\u0010'R\u0016\u0010(\u001a\u00020\r8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b(\u0010)R+\u0010\u0003\u001a\u00020\u00112\u0006\u0010*\u001a\u00020\u00118B@BX\u0082\u008e\u0002¢\u0006\u0012\n\u0004\b+\u0010,\u001a\u0004\b-\u0010.\"\u0004\b/\u00100R\u0016\u00102\u001a\u0002018\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b2\u00103R\u0016\u00105\u001a\u0002048\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b5\u00106R\u0016\u00107\u001a\u00020\u00078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b7\u0010'R\u0013\u00108\u001a\u00020\u00078F@\u0006¢\u0006\u0006\u001a\u0004\b8\u00109R\u0018\u0010:\u001a\u0004\u0018\u00010\r8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b:\u0010;R\u0016\u0010=\u001a\u00020<8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b=\u0010>¨\u0006D"}, d2 = {"Lcom/discord/stores/StoreConnectivity;", "Lcom/discord/stores/StoreV2;", "Lcom/discord/utilities/networking/NetworkMonitor$State;", "state", "", "handleDeviceNetworkStateUpdated", "(Lcom/discord/utilities/networking/NetworkMonitor$State;)V", "", "channelMessagesLoading", "handleChannelMessagesLoading", "(Z)V", "updateConnectivityState", "()V", "", "defaultDelay", "getStateDelay", "(J)J", "Lcom/discord/stores/StoreConnectivity$DelayedState;", "Lcom/discord/stores/StoreConnectivity$State;", "nextState", "getNextStateDelayInitial", "(Lcom/discord/stores/StoreConnectivity$DelayedState;Lcom/discord/stores/StoreConnectivity$State;)J", "getStateActiveMillis", "()J", "Lrx/Observable;", "observeState", "()Lrx/Observable;", "Lcom/discord/utilities/networking/NetworkMonitor;", "networkMonitor", "init", "(Lcom/discord/utilities/networking/NetworkMonitor;)V", "backgrounded", "handleBackgrounded", "connected", "handleConnected", "connectionReady", "handleConnectionReady", "deviceNetworkState", "Lcom/discord/utilities/networking/NetworkMonitor$State;", "Z", "stateTriggeredDelay", "J", "<set-?>", "state$delegate", "Lcom/discord/stores/StoreV2$MarkChangedDelegate;", "getState", "()Lcom/discord/stores/StoreConnectivity$DelayedState;", "setState", "(Lcom/discord/stores/StoreConnectivity$DelayedState;)V", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "isReadyPayloadReceived", "isConnected", "()Z", "stateTriggeredTimeMillis", "Ljava/lang/Long;", "Lcom/discord/stores/StoreStream;", "stream", "Lcom/discord/stores/StoreStream;", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/time/Clock;)V", "Companion", "DelayedState", "State", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreConnectivity extends StoreV2 {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a0.mutableProperty1(new s(StoreConnectivity.class, "state", "getState()Lcom/discord/stores/StoreConnectivity$DelayedState;", 0))};
    private static final Companion Companion = new Companion(null);
    @Deprecated
    public static final long DELAY_EXTENDED = 10000;
    @Deprecated
    public static final long DELAY_INTERVAL = 100;
    @Deprecated
    public static final long DELAY_NOMINAL = 1000;
    private boolean channelMessagesLoading;
    private final Clock clock;
    private final Dispatcher dispatcher;
    private boolean isReadyPayloadReceived;
    private Long stateTriggeredTimeMillis;
    private final StoreStream stream;
    private final StoreV2.MarkChangedDelegate state$delegate = new StoreV2.MarkChangedDelegate(new DelayedState(State.CONNECTING, 10000), null, 2, null);
    private long stateTriggeredDelay = 10000;
    private NetworkMonitor.State deviceNetworkState = NetworkMonitor.State.OFFLINE;

    /* compiled from: StoreConnectivity.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0007\b\u0082\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004¨\u0006\t"}, d2 = {"Lcom/discord/stores/StoreConnectivity$Companion;", "", "", "DELAY_EXTENDED", "J", "DELAY_INTERVAL", "DELAY_NOMINAL", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreConnectivity.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\b\b\u0002\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0018\u001a\u0004\b\u0019\u0010\u0007¨\u0006\u001c"}, d2 = {"Lcom/discord/stores/StoreConnectivity$DelayedState;", "", "Lcom/discord/stores/StoreConnectivity$State;", "component1", "()Lcom/discord/stores/StoreConnectivity$State;", "", "component2", "()J", "state", "delay", "copy", "(Lcom/discord/stores/StoreConnectivity$State;J)Lcom/discord/stores/StoreConnectivity$DelayedState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StoreConnectivity$State;", "getState", "J", "getDelay", HookHelper.constructorName, "(Lcom/discord/stores/StoreConnectivity$State;J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class DelayedState {
        private final long delay;
        private final State state;

        public DelayedState(State state, long j) {
            m.checkNotNullParameter(state, "state");
            this.state = state;
            this.delay = j;
        }

        public static /* synthetic */ DelayedState copy$default(DelayedState delayedState, State state, long j, int i, Object obj) {
            if ((i & 1) != 0) {
                state = delayedState.state;
            }
            if ((i & 2) != 0) {
                j = delayedState.delay;
            }
            return delayedState.copy(state, j);
        }

        public final State component1() {
            return this.state;
        }

        public final long component2() {
            return this.delay;
        }

        public final DelayedState copy(State state, long j) {
            m.checkNotNullParameter(state, "state");
            return new DelayedState(state, j);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof DelayedState)) {
                return false;
            }
            DelayedState delayedState = (DelayedState) obj;
            return m.areEqual(this.state, delayedState.state) && this.delay == delayedState.delay;
        }

        public final long getDelay() {
            return this.delay;
        }

        public final State getState() {
            return this.state;
        }

        public int hashCode() {
            State state = this.state;
            return b.a(this.delay) + ((state != null ? state.hashCode() : 0) * 31);
        }

        public String toString() {
            StringBuilder R = a.R("DelayedState(state=");
            R.append(this.state);
            R.append(", delay=");
            return a.B(R, this.delay, ")");
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public /* synthetic */ DelayedState(com.discord.stores.StoreConnectivity.State r1, long r2, int r4, kotlin.jvm.internal.DefaultConstructorMarker r5) {
            /*
                r0 = this;
                r4 = r4 & 2
                if (r4 == 0) goto L9
                com.discord.stores.StoreConnectivity.access$Companion()
                r2 = 1000(0x3e8, double:4.94E-321)
            L9:
                r0.<init>(r1, r2)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreConnectivity.DelayedState.<init>(com.discord.stores.StoreConnectivity$State, long, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
        }
    }

    /* compiled from: StoreConnectivity.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/discord/stores/StoreConnectivity$State;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "ONLINE", "OFFLINE", "OFFLINE_AIRPLANE_MODE", "CONNECTING", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum State {
        ONLINE,
        OFFLINE,
        OFFLINE_AIRPLANE_MODE,
        CONNECTING
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            State.values();
            int[] iArr = new int[4];
            $EnumSwitchMapping$0 = iArr;
            iArr[State.ONLINE.ordinal()] = 1;
            iArr[State.OFFLINE.ordinal()] = 2;
            iArr[State.OFFLINE_AIRPLANE_MODE.ordinal()] = 3;
            iArr[State.CONNECTING.ordinal()] = 4;
        }
    }

    public StoreConnectivity(StoreStream storeStream, Dispatcher dispatcher, Clock clock) {
        m.checkNotNullParameter(storeStream, "stream");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(clock, "clock");
        this.stream = storeStream;
        this.dispatcher = dispatcher;
        this.clock = clock;
    }

    private final long getNextStateDelayInitial(DelayedState delayedState, State state) {
        int ordinal = state.ordinal();
        if (ordinal == 0) {
            return 1000L;
        }
        boolean z2 = true;
        if (ordinal == 1 || ordinal == 2) {
            return 1000L;
        }
        if (ordinal == 3) {
            State state2 = delayedState.getState();
            if (!(state2 == State.OFFLINE || state2 == State.OFFLINE_AIRPLANE_MODE)) {
                z2 = false;
            }
            return (!z2 || getStateActiveMillis() <= 1000) ? 10000L : 1000L;
        }
        throw new NoWhenBranchMatchedException();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final DelayedState getState() {
        return (DelayedState) this.state$delegate.getValue(this, $$delegatedProperties[0]);
    }

    private final long getStateActiveMillis() {
        Long l = this.stateTriggeredTimeMillis;
        if (l == null) {
            return 0L;
        }
        return this.clock.currentTimeMillis() - l.longValue();
    }

    private final long getStateDelay(long j) {
        return f.coerceAtLeast(d0.a0.a.roundToInt(((float) (j - getStateActiveMillis())) / ((float) 100)) * 100, 0L);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleChannelMessagesLoading(boolean z2) {
        this.dispatcher.schedule(new StoreConnectivity$handleChannelMessagesLoading$1(this, z2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleDeviceNetworkStateUpdated(NetworkMonitor.State state) {
        this.dispatcher.schedule(new StoreConnectivity$handleDeviceNetworkStateUpdated$1(this, state));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void setState(DelayedState delayedState) {
        this.state$delegate.setValue(this, $$delegatedProperties[0], delayedState);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateConnectivityState() {
        State state;
        NetworkMonitor.State state2 = this.deviceNetworkState;
        if (state2 == NetworkMonitor.State.OFFLINE) {
            state = State.OFFLINE;
        } else if (state2 == NetworkMonitor.State.OFFLINE_AIRPLANE_MODE) {
            state = State.OFFLINE_AIRPLANE_MODE;
        } else if (!this.isReadyPayloadReceived || this.channelMessagesLoading) {
            state = State.CONNECTING;
        } else {
            state = State.ONLINE;
        }
        if (getState().getState() != state || this.stateTriggeredTimeMillis == null) {
            this.stateTriggeredTimeMillis = Long.valueOf(this.clock.currentTimeMillis());
            this.stateTriggeredDelay = getNextStateDelayInitial(getState(), state);
        }
        setState(new DelayedState(state, getStateDelay(this.stateTriggeredDelay)));
    }

    @StoreThread
    public final void handleBackgrounded(boolean z2) {
        if (z2) {
            this.stateTriggeredDelay -= getStateActiveMillis();
        }
        if (this.stateTriggeredTimeMillis != null) {
            this.stateTriggeredTimeMillis = Long.valueOf(this.clock.currentTimeMillis());
        }
        updateConnectivityState();
    }

    @StoreThread
    public final void handleConnected(boolean z2) {
        if (!z2) {
            this.isReadyPayloadReceived = false;
            updateConnectivityState();
        }
    }

    @StoreThread
    public final void handleConnectionReady(boolean z2) {
        this.isReadyPayloadReceived = z2;
        updateConnectivityState();
    }

    @StoreThread
    public final void init(NetworkMonitor networkMonitor) {
        m.checkNotNullParameter(networkMonitor, "networkMonitor");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.computationLatest(networkMonitor.observeState()), StoreConnectivity.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreConnectivity$init$1(this));
        ObservableExtensionsKt.appSubscribe(this.stream.getMessagesLoader$app_productionGoogleRelease().observeChannelMessagesLoading(), StoreConnectivity.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreConnectivity$init$2(this));
    }

    public final boolean isConnected() {
        return getState().getState() == State.ONLINE;
    }

    public final Observable<DelayedState> observeState() {
        Observable<DelayedState> q = ObservationDeck.connectRx$default(ObservationDeckProvider.get(), new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreConnectivity$observeState$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "ObservationDeckProvider\n…  .distinctUntilChanged()");
        return q;
    }
}
