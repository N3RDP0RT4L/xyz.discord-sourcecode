package com.discord.stores;

import com.discord.api.guild.Guild;
import com.discord.models.domain.ModelPayload;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreGuildSelected.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\n\u0010\u0002\u001a\u00060\u0000j\u0002`\u0001H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/primitives/GuildId;", "guildId", "", "invoke", "(J)Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildSelected$handleConnectionOpen$1 extends o implements Function1<Long, Boolean> {
    public final /* synthetic */ ModelPayload $payload;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGuildSelected$handleConnectionOpen$1(ModelPayload modelPayload) {
        super(1);
        this.$payload = modelPayload;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Boolean invoke(Long l) {
        return Boolean.valueOf(invoke(l.longValue()));
    }

    public final boolean invoke(long j) {
        boolean z2;
        boolean z3;
        if (j != 0) {
            List<Guild> guilds = this.$payload.getGuilds();
            m.checkNotNullExpressionValue(guilds, "payload.guilds");
            if (!(guilds instanceof Collection) || !guilds.isEmpty()) {
                for (Guild guild : guilds) {
                    if (j == guild.r()) {
                        z3 = true;
                        continue;
                    } else {
                        z3 = false;
                        continue;
                    }
                    if (z3) {
                        z2 = true;
                        break;
                    }
                }
            }
            z2 = false;
            if (!z2) {
                return true;
            }
        }
        return false;
    }
}
