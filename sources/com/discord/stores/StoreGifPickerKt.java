package com.discord.stores;

import kotlin.Metadata;
/* compiled from: StoreGifPicker.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0005\"\u0016\u0010\u0001\u001a\u00020\u00008\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0001\u0010\u0002\"\u0016\u0010\u0003\u001a\u00020\u00008\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0002\"\u0016\u0010\u0005\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0006\"\u0016\u0010\u0007\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0007\u0010\u0006\"\u0016\u0010\b\u001a\u00020\u00008\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\b\u0010\u0002¨\u0006\t"}, d2 = {"", "DEFAULT_GIF_SEARCH_PAGE_SIZE", "I", "DEFAULT_TRENDING_SEARCH_TERM_LIMIT", "", "TENOR_PROVIDER", "Ljava/lang/String;", "TINYGIF_MEDIA_FORMAT", "MAX_CACHE_ENTRIES", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGifPickerKt {
    private static final int DEFAULT_GIF_SEARCH_PAGE_SIZE = 50;
    private static final int DEFAULT_TRENDING_SEARCH_TERM_LIMIT = 5;
    private static final int MAX_CACHE_ENTRIES = 20;
    private static final String TENOR_PROVIDER = "tenor";
    private static final String TINYGIF_MEDIA_FORMAT = "tinygif";
}
