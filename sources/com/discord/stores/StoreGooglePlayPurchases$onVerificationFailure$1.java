package com.discord.stores;

import com.discord.stores.StoreGooglePlayPurchases;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import rx.subjects.PublishSubject;
/* compiled from: StoreGooglePlayPurchases.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGooglePlayPurchases$onVerificationFailure$1 extends o implements Function0<Unit> {
    public final /* synthetic */ StoreGooglePlayPurchases.VerificationResult $verificationResult;
    public final /* synthetic */ StoreGooglePlayPurchases this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGooglePlayPurchases$onVerificationFailure$1(StoreGooglePlayPurchases storeGooglePlayPurchases, StoreGooglePlayPurchases.VerificationResult verificationResult) {
        super(0);
        this.this$0 = storeGooglePlayPurchases;
        this.$verificationResult = verificationResult;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        PublishSubject publishSubject;
        ArrayList<String> b2 = this.$verificationResult.getPurchase().b();
        m.checkNotNullExpressionValue(b2, "verificationResult.purchase.skus");
        for (String str : b2) {
            publishSubject = this.this$0.eventSubject;
            m.checkNotNullExpressionValue(str, "sku");
            publishSubject.k.onNext(new StoreGooglePlayPurchases.Event.PurchaseQueryFailure(str));
        }
        this.this$0.queryState = StoreGooglePlayPurchases.QueryState.NotInProgress.INSTANCE;
        this.this$0.markChanged();
    }
}
