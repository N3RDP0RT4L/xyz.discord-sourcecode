package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.Context;
import com.discord.api.directory.DirectoryEntryEvent;
import com.discord.api.directory.DirectoryEntryGuild;
import com.discord.api.directory.DirectoryEntryType;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.utilities.RestCallState;
import com.discord.stores.utilities.RestCallStateKt;
import com.discord.utilities.features.GrowthTeamFeatures;
import com.discord.utilities.persister.Persister;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.t.h0;
import d0.t.n0;
import d0.t.o0;
import d0.z.d.m;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreDirectories.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0098\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 N2\u00020\u0001:\u0001NB1\u0012\u0006\u00103\u001a\u000202\u0012\u0006\u0010F\u001a\u00020E\u0012\u0006\u00100\u001a\u00020/\u0012\u0006\u0010J\u001a\u00020I\u0012\b\b\u0002\u00106\u001a\u000205¢\u0006\u0004\bL\u0010MJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J'\u0010\r\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b\u0018\u00010\n2\n\u0010\t\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\b\r\u0010\u000eJ-\u0010\u0011\u001a\u0016\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00100\u000f\u0018\u00010\n2\n\u0010\t\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\b\u0011\u0010\u000eJ'\u0010\u0013\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u000b\u0018\u00010\n2\n\u0010\t\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\b\u0013\u0010\u000eJ-\u0010\u0015\u001a\u0016\u0012\u0012\u0012\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b\u0018\u00010\n0\u00142\n\u0010\t\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\b\u0015\u0010\u0016J/\u0010\u0017\u001a$\u0012 \u0012\u001e\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\n0\u000f0\u0014¢\u0006\u0004\b\u0017\u0010\u0018J\u0013\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00190\u0014¢\u0006\u0004\b\u001a\u0010\u0018J\r\u0010\u001b\u001a\u00020\u0019¢\u0006\u0004\b\u001b\u0010\u001cJ/\u0010\u001d\u001a$\u0012 \u0012\u001e\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u000b0\n0\u000f0\u0014¢\u0006\u0004\b\u001d\u0010\u0018J\u0019\u0010\u001e\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\b\u001e\u0010\u001fJ\u0019\u0010 \u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\b \u0010\u001fJ%\u0010#\u001a\u00020\u00042\n\u0010\"\u001a\u00060\u0007j\u0002`!2\n\u0010\t\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\b#\u0010$J!\u0010&\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\u0006\u0010%\u001a\u00020\f¢\u0006\u0004\b&\u0010'J%\u0010(\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\n\u0010\"\u001a\u00060\u0007j\u0002`!¢\u0006\u0004\b(\u0010$J\u000f\u0010)\u001a\u00020\u0004H\u0016¢\u0006\u0004\b)\u0010*J\r\u0010+\u001a\u00020\u0004¢\u0006\u0004\b+\u0010*J\r\u0010,\u001a\u00020\u0004¢\u0006\u0004\b,\u0010*J\u0019\u0010-\u001a\u00020\u00192\n\u0010\"\u001a\u00060\u0007j\u0002`!¢\u0006\u0004\b-\u0010.R\u0016\u00100\u001a\u00020/8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b0\u00101R\u0016\u00103\u001a\u0002028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b3\u00104R\u0016\u00106\u001a\u0002058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b6\u00107R\u001c\u00109\u001a\b\u0012\u0004\u0012\u00020\u0019088\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b9\u0010:R2\u0010<\u001a\u001e\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\n0;8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b<\u0010=R&\u0010?\u001a\u0012\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0007j\u0002`!0>088\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b?\u0010:R2\u0010@\u001a\u001e\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\n0\u000f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b@\u0010=R2\u0010A\u001a\u001e\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u000b0\n0\u000f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bA\u0010=R8\u0010B\u001a$\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0016\u0012\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00100\u000f0\n0\u000f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bB\u0010=R2\u0010C\u001a\u001e\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u000b0\n0;8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bC\u0010=R8\u0010D\u001a$\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0016\u0012\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00100\u000f0\n0;8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bD\u0010=R\u0016\u0010F\u001a\u00020E8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bF\u0010GR\u001c\u0010H\u001a\b\u0012\u0004\u0012\u00020\u0019088\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bH\u0010:R\u0016\u0010J\u001a\u00020I8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bJ\u0010K¨\u0006O"}, d2 = {"Lcom/discord/stores/StoreDirectories;", "Lcom/discord/stores/StoreV2;", "Landroid/content/Context;", "context", "", "init", "(Landroid/content/Context;)V", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/stores/utilities/RestCallState;", "", "Lcom/discord/api/directory/DirectoryEntryGuild;", "getDirectoriesForChannel", "(J)Lcom/discord/stores/utilities/RestCallState;", "", "", "getEntryCountsForChannel", "Lcom/discord/api/directory/DirectoryEntryEvent;", "getGuildScheduledEventsForChannel", "Lrx/Observable;", "observeDirectoriesForChannel", "(J)Lrx/Observable;", "observeDirectories", "()Lrx/Observable;", "", "observeDiscordHubClicked", "getGuildScheduledEventsHeaderDismissed", "()Z", "observeDirectoryGuildScheduledEvents", "fetchDirectoriesForChannel", "(J)V", "fetchEntryCountsForChannel", "Lcom/discord/primitives/GuildId;", "guildId", "fetchGuildScheduledEventsForChannel", "(JJ)V", "directoryEntry", "addServerToDirectory", "(JLcom/discord/api/directory/DirectoryEntryGuild;)V", "removeServerFromDirectory", "snapshotData", "()V", "markDiscordHubClicked", "markGuildScheduledEventsHeaderDismissed", "getAndSetSeenNamePrompt", "(J)Z", "Lcom/discord/stores/StoreGuilds;", "guildStore", "Lcom/discord/stores/StoreGuilds;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/utilities/rest/RestAPI;", "restApi", "Lcom/discord/utilities/rest/RestAPI;", "Lcom/discord/utilities/persister/Persister;", "discordHubClickedPersister", "Lcom/discord/utilities/persister/Persister;", "", "directoriesMap", "Ljava/util/Map;", "", "hubNamePromptPersister", "directoriesMapSnapshot", "directoryGuildScheduledEventsMapSnapshot", "entryCountMapSnapshot", "directoryGuildScheduledEventsMap", "entryCountMap", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "guildScheduledEventsHeaderDismissed", "Lcom/discord/stores/StoreGuildScheduledEvents;", "guildScheduledEventsStore", "Lcom/discord/stores/StoreGuildScheduledEvents;", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreGuildScheduledEvents;Lcom/discord/utilities/rest/RestAPI;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreDirectories extends StoreV2 {
    public static final Companion Companion = new Companion(null);
    private static final String DISCORD_HUB_VERIFICATION_CLICKED_KEY = "hub_verification_clicked_key";
    private static final String GUILD_SCHEDULED_EVENTS_HEADER_DISMISSED = "guild_scheduled_events_header_dismissed";
    private static final String HUB_NAME_PROMPT = "hub_name_prompt";
    private Map<Long, RestCallState<List<DirectoryEntryGuild>>> directoriesMap;
    private Map<Long, ? extends RestCallState<? extends List<DirectoryEntryGuild>>> directoriesMapSnapshot;
    private Map<Long, RestCallState<List<DirectoryEntryEvent>>> directoryGuildScheduledEventsMap;
    private Map<Long, ? extends RestCallState<? extends List<DirectoryEntryEvent>>> directoryGuildScheduledEventsMapSnapshot;
    private final Persister<Boolean> discordHubClickedPersister;
    private final Dispatcher dispatcher;
    private Map<Long, RestCallState<Map<Integer, Integer>>> entryCountMap;
    private Map<Long, ? extends RestCallState<? extends Map<Integer, Integer>>> entryCountMapSnapshot;
    private final Persister<Boolean> guildScheduledEventsHeaderDismissed;
    private final StoreGuildScheduledEvents guildScheduledEventsStore;
    private final StoreGuilds guildStore;
    private final Persister<Set<Long>> hubNamePromptPersister;
    private final ObservationDeck observationDeck;
    private final RestAPI restApi;

    /* compiled from: StoreDirectories.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004¨\u0006\t"}, d2 = {"Lcom/discord/stores/StoreDirectories$Companion;", "", "", "DISCORD_HUB_VERIFICATION_CLICKED_KEY", "Ljava/lang/String;", "GUILD_SCHEDULED_EVENTS_HEADER_DISMISSED", "HUB_NAME_PROMPT", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ StoreDirectories(Dispatcher dispatcher, ObservationDeck observationDeck, StoreGuilds storeGuilds, StoreGuildScheduledEvents storeGuildScheduledEvents, RestAPI restAPI, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(dispatcher, observationDeck, storeGuilds, storeGuildScheduledEvents, (i & 16) != 0 ? RestAPI.Companion.getApi() : restAPI);
    }

    public final void addServerToDirectory(long j, DirectoryEntryGuild directoryEntryGuild) {
        m.checkNotNullParameter(directoryEntryGuild, "directoryEntry");
        this.dispatcher.schedule(new StoreDirectories$addServerToDirectory$1(this, j, directoryEntryGuild));
    }

    public final void fetchDirectoriesForChannel(long j) {
        Observable<R> F = this.restApi.getDirectoryEntries(j).F(StoreDirectories$fetchDirectoriesForChannel$1.INSTANCE);
        m.checkNotNullExpressionValue(F, "restApi\n        .getDire…imateMemberCount ?: 0 } }");
        RestCallStateKt.executeRequest(F, new StoreDirectories$fetchDirectoriesForChannel$2(this, j));
    }

    public final void fetchEntryCountsForChannel(long j) {
        RestCallStateKt.executeRequest(this.restApi.getEntryCounts(j), new StoreDirectories$fetchEntryCountsForChannel$1(this, j));
    }

    public final void fetchGuildScheduledEventsForChannel(long j, long j2) {
        if (GrowthTeamFeatures.INSTANCE.hubEventsEnabled(j, false)) {
            RestCallStateKt.executeRequest(this.restApi.getDirectoryGuildScheduledEvents(j2, DirectoryEntryType.GuildScheduledEvent.getKey()), new StoreDirectories$fetchGuildScheduledEventsForChannel$1(this, j2));
        }
    }

    public final boolean getAndSetSeenNamePrompt(long j) {
        boolean contains = this.hubNamePromptPersister.get().contains(Long.valueOf(j));
        Persister<Set<Long>> persister = this.hubNamePromptPersister;
        persister.set(o0.plus(persister.get(), Long.valueOf(j)), true);
        return contains;
    }

    public final RestCallState<List<DirectoryEntryGuild>> getDirectoriesForChannel(long j) {
        return (RestCallState) this.directoriesMapSnapshot.get(Long.valueOf(j));
    }

    public final RestCallState<Map<Integer, Integer>> getEntryCountsForChannel(long j) {
        return (RestCallState) this.entryCountMapSnapshot.get(Long.valueOf(j));
    }

    public final RestCallState<List<DirectoryEntryEvent>> getGuildScheduledEventsForChannel(long j) {
        return (RestCallState) this.directoryGuildScheduledEventsMapSnapshot.get(Long.valueOf(j));
    }

    public final boolean getGuildScheduledEventsHeaderDismissed() {
        return this.guildScheduledEventsHeaderDismissed.get().booleanValue();
    }

    @Override // com.discord.stores.Store
    public void init(Context context) {
        m.checkNotNullParameter(context, "context");
        super.init(context);
        Observable q = ObservableExtensionsKt.computationLatest(ObservableExtensionsKt.leadingEdgeThrottle(this.guildStore.observeGuilds(), 1L, TimeUnit.SECONDS)).F(StoreDirectories$init$1.INSTANCE).q();
        m.checkNotNullExpressionValue(q, "guildStore\n        .obse…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(q, StoreDirectories.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreDirectories$init$2(this));
    }

    public final void markDiscordHubClicked() {
        this.discordHubClickedPersister.set(Boolean.TRUE, true);
    }

    public final void markGuildScheduledEventsHeaderDismissed() {
        this.guildScheduledEventsHeaderDismissed.set(Boolean.TRUE, true);
    }

    public final Observable<Map<Long, RestCallState<List<DirectoryEntryGuild>>>> observeDirectories() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreDirectories$observeDirectories$1(this), 14, null);
    }

    public final Observable<RestCallState<List<DirectoryEntryGuild>>> observeDirectoriesForChannel(long j) {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreDirectories$observeDirectoriesForChannel$1(this, j), 14, null);
    }

    public final Observable<Map<Long, RestCallState<List<DirectoryEntryEvent>>>> observeDirectoryGuildScheduledEvents() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreDirectories$observeDirectoryGuildScheduledEvents$1(this), 14, null);
    }

    public final Observable<Boolean> observeDiscordHubClicked() {
        Observable<Boolean> q = this.discordHubClickedPersister.getObservable().q();
        m.checkNotNullExpressionValue(q, "discordHubClickedPersist…  .distinctUntilChanged()");
        return q;
    }

    public final void removeServerFromDirectory(long j, long j2) {
        RestCallStateKt.executeRequest(RestCallStateKt.logNetworkAction(this.restApi.removeServerFromHub(j, j2), new StoreDirectories$removeServerFromDirectory$1(j, j2)), new StoreDirectories$removeServerFromDirectory$2(this, j, j2));
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        this.directoriesMapSnapshot = new HashMap(this.directoriesMap);
        this.entryCountMapSnapshot = new HashMap(this.entryCountMap);
        this.directoryGuildScheduledEventsMapSnapshot = new HashMap(this.directoryGuildScheduledEventsMap);
    }

    public StoreDirectories(Dispatcher dispatcher, ObservationDeck observationDeck, StoreGuilds storeGuilds, StoreGuildScheduledEvents storeGuildScheduledEvents, RestAPI restAPI) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(storeGuilds, "guildStore");
        m.checkNotNullParameter(storeGuildScheduledEvents, "guildScheduledEventsStore");
        m.checkNotNullParameter(restAPI, "restApi");
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
        this.guildStore = storeGuilds;
        this.guildScheduledEventsStore = storeGuildScheduledEvents;
        this.restApi = restAPI;
        this.directoriesMapSnapshot = h0.emptyMap();
        this.directoriesMap = new LinkedHashMap();
        this.entryCountMapSnapshot = h0.emptyMap();
        this.entryCountMap = new LinkedHashMap();
        this.directoryGuildScheduledEventsMapSnapshot = h0.emptyMap();
        this.directoryGuildScheduledEventsMap = new LinkedHashMap();
        Boolean bool = Boolean.FALSE;
        this.discordHubClickedPersister = new Persister<>(DISCORD_HUB_VERIFICATION_CLICKED_KEY, bool);
        this.guildScheduledEventsHeaderDismissed = new Persister<>(GUILD_SCHEDULED_EVENTS_HEADER_DISMISSED, bool);
        this.hubNamePromptPersister = new Persister<>(HUB_NAME_PROMPT, n0.emptySet());
    }
}
