package com.discord.stores;

import com.discord.api.channel.Channel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreVoiceChannelSelected.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/api/channel/Channel;", "invoke", "()Lcom/discord/api/channel/Channel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreVoiceChannelSelected$observeSelectedChannel$1 extends o implements Function0<Channel> {
    public final /* synthetic */ StoreVoiceChannelSelected this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreVoiceChannelSelected$observeSelectedChannel$1(StoreVoiceChannelSelected storeVoiceChannelSelected) {
        super(0);
        this.this$0 = storeVoiceChannelSelected;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final Channel invoke() {
        StoreStream storeStream;
        long j;
        storeStream = this.this$0.stream;
        StoreChannels channels$app_productionGoogleRelease = storeStream.getChannels$app_productionGoogleRelease();
        j = this.this$0.selectedVoiceChannelId;
        return channels$app_productionGoogleRelease.findChannelByIdInternal$app_productionGoogleRelease(j);
    }
}
