package com.discord.stores;

import com.discord.models.domain.emoji.ModelEmojiCustom;
import d0.z.d.m;
import d0.z.d.o;
import java.util.HashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: StoreEmojiCustom.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\b\u001a\u00020\u00052\n\u0010\u0002\u001a\u00060\u0000j\u0002`\u00012\u0006\u0010\u0004\u001a\u00020\u0003H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "Lcom/discord/primitives/RoleId;", "roleId", "Lcom/discord/models/domain/emoji/ModelEmojiCustom;", "emoji", "", "invoke", "(JLcom/discord/models/domain/emoji/ModelEmojiCustom;)V", "mergeEmoji"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreEmojiCustom$updateAllowedGuildEmojis$1 extends o implements Function2<Long, ModelEmojiCustom, Unit> {
    public final /* synthetic */ Map $allowed;
    public final /* synthetic */ StoreEmojiCustom this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreEmojiCustom$updateAllowedGuildEmojis$1(StoreEmojiCustom storeEmojiCustom, Map map) {
        super(2);
        this.this$0 = storeEmojiCustom;
        this.$allowed = map;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(Long l, ModelEmojiCustom modelEmojiCustom) {
        invoke(l.longValue(), modelEmojiCustom);
        return Unit.a;
    }

    public final void invoke(long j, ModelEmojiCustom modelEmojiCustom) {
        m.checkNotNullParameter(modelEmojiCustom, "emoji");
        Map map = this.$allowed;
        Long valueOf = Long.valueOf(j);
        Object obj = map.get(valueOf);
        if (obj == null) {
            obj = new HashMap();
            map.put(valueOf, obj);
        }
        Map map2 = (Map) obj;
        long id2 = modelEmojiCustom.getId();
        if (!m.areEqual(modelEmojiCustom, (ModelEmojiCustom) map2.get(Long.valueOf(id2)))) {
            map2.put(Long.valueOf(id2), modelEmojiCustom);
            this.this$0.markChanged();
        }
    }
}
