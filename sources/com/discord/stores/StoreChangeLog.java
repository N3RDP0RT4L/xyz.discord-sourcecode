package com.discord.stores;

import andhook.lib.HookHelper;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import b.i.a.f.e.o.f;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.stores.StoreNotices;
import com.discord.utilities.SnowflakeUtils;
import com.discord.utilities.string.StringUtilsKt;
import com.discord.utilities.time.Clock;
import com.discord.widgets.changelog.WidgetChangeLog;
import com.discord.widgets.changelog.WidgetChangeLogSpecial;
import d0.g0.t;
import d0.g0.w;
import d0.z.d.m;
import kotlin.Metadata;
import s.a.k0;
import s.a.x0;
import xyz.discord.R;
/* compiled from: StoreChangeLog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B7\u0012\u0006\u0010>\u001a\u00020=\u0012\u0006\u0010,\u001a\u00020+\u0012\u0006\u0010&\u001a\u00020%\u0012\u0006\u0010;\u001a\u00020:\u0012\u0006\u00108\u001a\u000207\u0012\u0006\u0010)\u001a\u00020(¢\u0006\u0004\b@\u0010AJ5\u0010\f\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u00072\b\u0010\n\u001a\u0004\u0018\u00010\tH\u0002¢\u0006\u0004\b\f\u0010\rJ\u001b\u0010\u000e\u001a\u00020\u000b2\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u0005H\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ'\u0010\u0012\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u00072\u0006\u0010\u0011\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u000f\u0010\u0015\u001a\u00020\u0014H\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0017\u0010\u001a\u001a\u00020\u00192\u0006\u0010\u0018\u001a\u00020\u0017H\u0007¢\u0006\u0004\b\u001a\u0010\u001bJ\u000f\u0010\u001c\u001a\u00020\u0019H\u0007¢\u0006\u0004\b\u001c\u0010\u001dJ\u000f\u0010\u001e\u001a\u00020\u0019H\u0007¢\u0006\u0004\b\u001e\u0010\u001dJ\u0015\u0010 \u001a\u00020\u00192\u0006\u0010\u001f\u001a\u00020\u0007¢\u0006\u0004\b \u0010!J\u001f\u0010#\u001a\u00020\u00192\u0006\u0010\u0003\u001a\u00020\u00022\b\b\u0002\u0010\"\u001a\u00020\u000b¢\u0006\u0004\b#\u0010$R\u0016\u0010&\u001a\u00020%8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b&\u0010'R\u0016\u0010)\u001a\u00020(8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b)\u0010*R\u0016\u0010,\u001a\u00020+8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b,\u0010-R(\u00102\u001a\u0004\u0018\u00010\u00072\b\u0010.\u001a\u0004\u0018\u00010\u00078B@BX\u0082\u000e¢\u0006\f\u001a\u0004\b/\u00100\"\u0004\b1\u0010!R\"\u0010\u0018\u001a\u00020\u00178\u0006@\u0006X\u0086.¢\u0006\u0012\n\u0004\b\u0018\u00103\u001a\u0004\b4\u00105\"\u0004\b6\u0010\u001bR\u0016\u00108\u001a\u0002078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b8\u00109R\u0016\u0010;\u001a\u00020:8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b;\u0010<R\u0016\u0010>\u001a\u00020=8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b>\u0010?¨\u0006B"}, d2 = {"Lcom/discord/stores/StoreChangeLog;", "Lcom/discord/stores/Store;", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/UserId;", "userId", "", "targetLanguage", "", "experimentBucket", "", "shouldShowChangelog", "(Landroid/content/Context;JLjava/lang/String;Ljava/lang/Integer;)Z", "isTooYoung", "(J)Z", ModelAuditLogEntry.CHANGE_KEY_NAME, "fallback", "getChangelogExperimentString", "(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;", "Lcom/discord/stores/StoreNotices$Notice;", "createChangeLogNotice", "()Lcom/discord/stores/StoreNotices$Notice;", "Landroid/app/Application;", "app", "", "init", "(Landroid/app/Application;)V", "handleConnectionOpen", "()V", "handlePostConnectionOpen", "currentVersion", "markSeen", "(Ljava/lang/String;)V", "fromSettings", "openChangeLog", "(Landroid/content/Context;Z)V", "Lcom/discord/stores/StoreUser;", "users", "Lcom/discord/stores/StoreUser;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/stores/StoreNotices;", "notices", "Lcom/discord/stores/StoreNotices;", "value", "getLastSeenChangeLogVersion", "()Ljava/lang/String;", "setLastSeenChangeLogVersion", "lastSeenChangeLogVersion", "Landroid/app/Application;", "getApp", "()Landroid/app/Application;", "setApp", "Lcom/discord/stores/StoreExperiments;", "experiments", "Lcom/discord/stores/StoreExperiments;", "Lcom/discord/stores/StoreUserSettingsSystem;", "userSettingsSystem", "Lcom/discord/stores/StoreUserSettingsSystem;", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", HookHelper.constructorName, "(Lcom/discord/utilities/time/Clock;Lcom/discord/stores/StoreNotices;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreUserSettingsSystem;Lcom/discord/stores/StoreExperiments;Lcom/discord/stores/Dispatcher;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreChangeLog extends Store {
    public Application app;
    private final Clock clock;
    private final Dispatcher dispatcher;
    private final StoreExperiments experiments;
    private final StoreNotices notices;
    private final StoreUserSettingsSystem userSettingsSystem;
    private final StoreUser users;

    public StoreChangeLog(Clock clock, StoreNotices storeNotices, StoreUser storeUser, StoreUserSettingsSystem storeUserSettingsSystem, StoreExperiments storeExperiments, Dispatcher dispatcher) {
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(storeNotices, "notices");
        m.checkNotNullParameter(storeUser, "users");
        m.checkNotNullParameter(storeUserSettingsSystem, "userSettingsSystem");
        m.checkNotNullParameter(storeExperiments, "experiments");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        this.clock = clock;
        this.notices = storeNotices;
        this.users = storeUser;
        this.userSettingsSystem = storeUserSettingsSystem;
        this.experiments = storeExperiments;
        this.dispatcher = dispatcher;
    }

    private final StoreNotices.Notice createChangeLogNotice() {
        return new StoreNotices.Notice("CHANGE_LOG", null, 1337L, 0, true, null, 0L, false, 604800000L, new StoreChangeLog$createChangeLogNotice$1(this), 226, null);
    }

    private final String getChangelogExperimentString(Context context, String str, String str2) {
        int identifier = context.getResources().getIdentifier(str, "string", context.getPackageName());
        if (identifier <= 0) {
            return str2;
        }
        String string = context.getString(identifier);
        m.checkNotNullExpressionValue(string, "context.getString(id)");
        return string;
    }

    private final String getLastSeenChangeLogVersion() {
        return getPrefs().getString("CACHE_KEY_VIEWED_CHANGE_LOG_VERSION", "");
    }

    private final boolean isTooYoung(long j) {
        long firstUseTimestamp = this.notices.getFirstUseTimestamp() + 432000000;
        long currentTimeMillis = this.clock.currentTimeMillis();
        return currentTimeMillis < ((j >>> 22) + SnowflakeUtils.DISCORD_EPOCH) + 432000000 || currentTimeMillis < firstUseTimestamp;
    }

    public static /* synthetic */ void openChangeLog$default(StoreChangeLog storeChangeLog, Context context, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        storeChangeLog.openChangeLog(context, z2);
    }

    private final void setLastSeenChangeLogVersion(String str) {
        SharedPreferences.Editor edit = getPrefs().edit();
        m.checkNotNullExpressionValue(edit, "editor");
        edit.putString("CACHE_KEY_VIEWED_CHANGE_LOG_VERSION", str);
        edit.apply();
    }

    private final boolean shouldShowChangelog(Context context, long j, String str, Integer num) {
        String str2;
        int identifier = (num != null && num.intValue() == 1) ? context.getResources().getIdentifier("change_log_md_experiment_body", "string", context.getPackageName()) : R.string.change_log_md_body;
        CharSequence stringByLocale = StringUtilsKt.getStringByLocale(context, identifier, "en");
        CharSequence stringByLocale2 = StringUtilsKt.getStringByLocale(context, identifier, str);
        if ((!m.areEqual(str, "en")) && m.areEqual(stringByLocale, stringByLocale2)) {
            return false;
        }
        if (num != null && num.intValue() == 1) {
            String string = context.getString(R.string.change_log_md_date);
            m.checkNotNullExpressionValue(string, "context.getString(R.string.change_log_md_date)");
            str2 = getChangelogExperimentString(context, "change_log_md_experiment_date", string);
        } else {
            str2 = context.getString(R.string.change_log_md_date);
            m.checkNotNullExpressionValue(str2, "context.getString(R.string.change_log_md_date)");
        }
        String lastSeenChangeLogVersion = getLastSeenChangeLogVersion();
        if (!(lastSeenChangeLogVersion == null || t.isBlank(lastSeenChangeLogVersion)) && !isTooYoung(j)) {
            return !m.areEqual(str2, getLastSeenChangeLogVersion());
        }
        markSeen(str2);
        return false;
    }

    public final Application getApp() {
        Application application = this.app;
        if (application == null) {
            m.throwUninitializedPropertyAccessException("app");
        }
        return application;
    }

    @StoreThread
    public final void handleConnectionOpen() {
        f.H0(x0.j, k0.a, null, new StoreChangeLog$handleConnectionOpen$1(this, null), 2, null);
    }

    @StoreThread
    public final void handlePostConnectionOpen() {
        String str = (String) w.split$default((CharSequence) this.userSettingsSystem.getLocale(), new String[]{"-"}, false, 0, 6, (Object) null).get(0);
        long id2 = this.users.getMe().getId();
        Application application = this.app;
        if (application == null) {
            m.throwUninitializedPropertyAccessException("app");
        }
        if (shouldShowChangelog(application, id2, str, null)) {
            this.notices.requestToShow(createChangeLogNotice());
        }
    }

    @StoreThread
    public final void init(Application application) {
        m.checkNotNullParameter(application, "app");
        init((Context) application);
        this.app = application;
    }

    public final void markSeen(String str) {
        m.checkNotNullParameter(str, "currentVersion");
        setLastSeenChangeLogVersion(str);
        StoreNotices.markSeen$default(this.notices, "CHANGE_LOG", 0L, 2, null);
    }

    public final void openChangeLog(Context context, boolean z2) {
        WidgetChangeLogSpecial.Companion.ExitStyle exitStyle;
        m.checkNotNullParameter(context, "context");
        String string = context.getString(R.string.change_log_md_date);
        m.checkNotNullExpressionValue(string, "context.getString(R.string.change_log_md_date)");
        String changelogExperimentString = getChangelogExperimentString(context, "change_log_md_experiment_date", string);
        String string2 = context.getString(R.string.change_log_md_revision);
        m.checkNotNullExpressionValue(string2, "context.getString(R.string.change_log_md_revision)");
        String changelogExperimentString2 = getChangelogExperimentString(context, "change_log_md_revision", string2);
        String string3 = context.getString(R.string.change_log_md_video);
        m.checkNotNullExpressionValue(string3, "context.getString(R.string.change_log_md_video)");
        String changelogExperimentString3 = getChangelogExperimentString(context, "change_log_md_experiment_video", string3);
        String string4 = context.getString(R.string.change_log_md_body);
        m.checkNotNullExpressionValue(string4, "context.getString(R.string.change_log_md_body)");
        String changelogExperimentString4 = getChangelogExperimentString(context, "change_log_md_experiment_body", string4);
        String string5 = context.getString(R.string.back);
        m.checkNotNullExpressionValue(string5, "context.getString(R.string.back)");
        String changelogExperimentString5 = getChangelogExperimentString(context, "change_log_md_experiment_template", string5);
        if (z2) {
            exitStyle = WidgetChangeLogSpecial.Companion.ExitStyle.BACK;
        } else {
            exitStyle = WidgetChangeLogSpecial.Companion.ExitStyle.CLOSE;
        }
        if (m.areEqual(changelogExperimentString5, "special") && m.areEqual((Object) null, (Object) 1)) {
            WidgetChangeLogSpecial.Companion.launch(context, changelogExperimentString, changelogExperimentString2, changelogExperimentString3, changelogExperimentString4, exitStyle, true);
        } else if (m.areEqual((Object) null, (Object) 1)) {
            WidgetChangeLog.Companion.launch(context, changelogExperimentString, changelogExperimentString2, changelogExperimentString3, changelogExperimentString4);
        } else {
            WidgetChangeLog.Companion companion = WidgetChangeLog.Companion;
            String string6 = context.getString(R.string.change_log_md_date);
            m.checkNotNullExpressionValue(string6, "context.getString(R.string.change_log_md_date)");
            String string7 = context.getString(R.string.change_log_md_revision);
            m.checkNotNullExpressionValue(string7, "context.getString(R.string.change_log_md_revision)");
            String string8 = context.getString(R.string.change_log_md_video);
            m.checkNotNullExpressionValue(string8, "context.getString(R.string.change_log_md_video)");
            String string9 = context.getString(R.string.change_log_md_body);
            m.checkNotNullExpressionValue(string9, "context.getString(R.string.change_log_md_body)");
            companion.launch(context, string6, string7, string8, string9);
        }
    }

    public final void setApp(Application application) {
        m.checkNotNullParameter(application, "<set-?>");
        this.app = application;
    }
}
