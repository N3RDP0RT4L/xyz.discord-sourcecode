package com.discord.stores;

import andhook.lib.HookHelper;
import com.discord.stores.StoreV2;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.s;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.reflect.KProperty;
import rx.Observable;
/* compiled from: StoreConnectionOpen.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\r\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u0013\u0010\tJ\u001d\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00020\u00042\b\b\u0002\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\b\u001a\u00020\u0007H\u0007¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\u000b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u000b\u0010\fR+\u0010\u0010\u001a\u00020\u00022\u0006\u0010\r\u001a\u00020\u00028B@BX\u0082\u008e\u0002¢\u0006\u0012\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\f¨\u0006\u0014"}, d2 = {"Lcom/discord/stores/StoreConnectionOpen;", "Lcom/discord/stores/StoreV2;", "", "delayForUI", "Lrx/Observable;", "observeConnectionOpen", "(Z)Lrx/Observable;", "", "handleConnectionOpen", "()V", "connected", "handleConnected", "(Z)V", "<set-?>", "isConnectionOpen$delegate", "Lcom/discord/stores/StoreV2$MarkChangedDelegate;", "isConnectionOpen", "()Z", "setConnectionOpen", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreConnectionOpen extends StoreV2 {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a0.mutableProperty1(new s(StoreConnectionOpen.class, "isConnectionOpen", "isConnectionOpen()Z", 0))};
    private final StoreV2.MarkChangedDelegate isConnectionOpen$delegate = new StoreV2.MarkChangedDelegate(Boolean.FALSE, null, 2, null);

    /* JADX INFO: Access modifiers changed from: private */
    public final boolean isConnectionOpen() {
        return ((Boolean) this.isConnectionOpen$delegate.getValue(this, $$delegatedProperties[0])).booleanValue();
    }

    public static /* synthetic */ Observable observeConnectionOpen$default(StoreConnectionOpen storeConnectionOpen, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = true;
        }
        return storeConnectionOpen.observeConnectionOpen(z2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void setConnectionOpen(boolean z2) {
        this.isConnectionOpen$delegate.setValue(this, $$delegatedProperties[0], Boolean.valueOf(z2));
    }

    @StoreThread
    public final void handleConnected(boolean z2) {
        if (!z2) {
            setConnectionOpen(false);
        }
    }

    @StoreThread
    public final void handleConnectionOpen() {
        setConnectionOpen(true);
    }

    public final Observable<Boolean> observeConnectionOpen(boolean z2) {
        Observable<Boolean> connectRx$default = ObservationDeck.connectRx$default(ObservationDeckProvider.get(), new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreConnectionOpen$observeConnectionOpen$1(this), 14, null);
        if (z2) {
            connectRx$default = connectRx$default.p(100L, TimeUnit.MILLISECONDS);
        }
        m.checkNotNullExpressionValue(connectRx$default, "ObservationDeckProvider\n…            }\n          }");
        return connectRx$default;
    }
}
