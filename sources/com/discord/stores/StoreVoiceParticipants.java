package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.Context;
import b.c.a.a0.d;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.voice.state.StageRequestToSpeakState;
import com.discord.api.voice.state.VoiceState;
import com.discord.models.domain.ModelApplicationStream;
import com.discord.models.member.GuildMember;
import com.discord.models.user.MeUser;
import com.discord.models.user.User;
import com.discord.stores.StoreMediaSettings;
import com.discord.stores.StoreVideoStreams;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.streams.StreamContext;
import com.discord.utilities.streams.StreamContextService;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import j0.l.e.k;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import rx.Observable;
import rx.functions.FuncN;
import rx.subjects.BehaviorSubject;
/* compiled from: StoreVoiceParticipants.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u009c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001:\u0001=B\u000f\u0012\u0006\u00107\u001a\u000206¢\u0006\u0004\b;\u0010<J;\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n0\t2\u0006\u0010\u0003\u001a\u00020\u00022\u0016\u0010\b\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u00070\u0004H\u0002¢\u0006\u0004\b\f\u0010\rJ9\u0010\u0011\u001a\u001a\u0012\u0016\u0012\u0014\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0006\u0012\u0004\u0018\u00010\u00100\u00040\t2\u0010\u0010\u000f\u001a\f\u0012\b\u0012\u00060\u0005j\u0002`\u00060\u000eH\u0002¢\u0006\u0004\b\u0011\u0010\u0012Jç\u0001\u0010$\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020#0\u00042\u0006\u0010\u0014\u001a\u00020\u00132\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u000b0\n2\u0016\u0010\b\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u00070\u00042\u0010\u0010\u0017\u001a\f\u0012\b\u0012\u00060\u0005j\u0002`\u00060\u00162\u0010\u0010\u0018\u001a\f\u0012\b\u0012\u00060\u0005j\u0002`\u00060\n2\u0018\u0010\u001a\u001a\u0014\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0006\u0012\u0004\u0018\u00010\u00190\u00042\u0016\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u001b0\u00042 \u0010\u001f\u001a\u001c\u0012\b\u0012\u00060\u001dj\u0002`\u001e\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0005j\u0002`\u00060\u000e0\u00042\u0006\u0010!\u001a\u00020 2\u0018\u0010\"\u001a\u0014\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0006\u0012\u0004\u0018\u00010\u00100\u0004H\u0002¢\u0006\u0004\b$\u0010%J/\u0010(\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020#0\u00040\t2\n\u0010'\u001a\u00060\u0005j\u0002`&¢\u0006\u0004\b(\u0010)J\u001d\u0010,\u001a\u00020+2\u000e\u0010*\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006¢\u0006\u0004\b,\u0010-J\u0017\u00100\u001a\u00020+2\u0006\u0010/\u001a\u00020.H\u0016¢\u0006\u0004\b0\u00101R:\u00104\u001a&\u0012\f\u0012\n 3*\u0004\u0018\u00010\u00050\u0005 3*\u0012\u0012\f\u0012\n 3*\u0004\u0018\u00010\u00050\u0005\u0018\u000102028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b4\u00105R\u0019\u00107\u001a\u0002068\u0006@\u0006¢\u0006\f\n\u0004\b7\u00108\u001a\u0004\b9\u0010:¨\u0006>"}, d2 = {"Lcom/discord/stores/StoreVoiceParticipants;", "Lcom/discord/stores/Store;", "Lcom/discord/api/channel/Channel;", "channel", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/api/voice/state/VoiceState;", "voiceStates", "Lrx/Observable;", "", "Lcom/discord/models/user/User;", "getOtherVoiceUsers", "(Lcom/discord/api/channel/Channel;Ljava/util/Map;)Lrx/Observable;", "", "userIds", "Lcom/discord/utilities/streams/StreamContext;", "getStreamContextsForUsers", "(Ljava/util/List;)Lrx/Observable;", "Lcom/discord/models/user/MeUser;", "meUser", "otherUsers", "", "speakingUsers", "ringingUsers", "Lcom/discord/stores/StoreVideoStreams$UserStreams;", "videoStreams", "Lcom/discord/models/member/GuildMember;", "guildMembers", "", "Lcom/discord/primitives/StreamKey;", "streamSpectators", "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "voiceConfiguration", "streamContexts", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "create", "(Lcom/discord/models/user/MeUser;Ljava/util/Collection;Ljava/util/Map;Ljava/util/Set;Ljava/util/Collection;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Ljava/util/Map;)Ljava/util/Map;", "Lcom/discord/primitives/ChannelId;", "channelId", "get", "(J)Lrx/Observable;", "userId", "", "selectParticipant", "(Ljava/lang/Long;)V", "Landroid/content/Context;", "context", "init", "(Landroid/content/Context;)V", "Lrx/subjects/BehaviorSubject;", "kotlin.jvm.PlatformType", "selectedParticipantSubject", "Lrx/subjects/BehaviorSubject;", "Lcom/discord/stores/StoreStream;", "stream", "Lcom/discord/stores/StoreStream;", "getStream", "()Lcom/discord/stores/StoreStream;", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;)V", "VoiceUser", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreVoiceParticipants extends Store {
    private final BehaviorSubject<Long> selectedParticipantSubject = BehaviorSubject.l0(0L);
    private final StoreStream stream;

    /* compiled from: StoreVoiceParticipants.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0010\b\n\u0002\b\u0017\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001Bq\u0012\u0006\u0010\u001e\u001a\u00020\b\u0012\b\u0010\u001f\u001a\u0004\u0018\u00010\u000b\u0012\u0006\u0010 \u001a\u00020\u0005\u0012\b\u0010!\u001a\u0004\u0018\u00010\u000f\u0012\u0006\u0010\"\u001a\u00020\u0005\u0012\b\u0010#\u001a\u0004\u0018\u00010\u0013\u0012\u000e\u0010$\u001a\n\u0018\u00010\u0016j\u0004\u0018\u0001`\u0017\u0012\b\u0010%\u001a\u0004\u0018\u00010\u001a\u0012\u0006\u0010&\u001a\u00020\u0005\u0012\b\u0010'\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010(\u001a\u00020\u0005¢\u0006\u0004\bZ\u0010[J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÂ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u000e\u0010\u0007J\u0012\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0007J\u0012\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0018\u0010\u0018\u001a\n\u0018\u00010\u0016j\u0004\u0018\u0001`\u0017HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0019J\u0012\u0010\u001b\u001a\u0004\u0018\u00010\u001aHÆ\u0003¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001d\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u001d\u0010\u0007J\u0090\u0001\u0010)\u001a\u00020\u00002\b\b\u0002\u0010\u001e\u001a\u00020\b2\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u000b2\b\b\u0002\u0010 \u001a\u00020\u00052\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u000f2\b\b\u0002\u0010\"\u001a\u00020\u00052\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\u00132\u0010\b\u0002\u0010$\u001a\n\u0018\u00010\u0016j\u0004\u0018\u0001`\u00172\n\b\u0002\u0010%\u001a\u0004\u0018\u00010\u001a2\b\b\u0002\u0010&\u001a\u00020\u00052\n\b\u0002\u0010'\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010(\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b)\u0010*J\u0010\u0010+\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b+\u0010\u0019J\u0010\u0010-\u001a\u00020,HÖ\u0001¢\u0006\u0004\b-\u0010.J\u001a\u00100\u001a\u00020\u00052\b\u0010/\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b0\u00101R\u0016\u00102\u001a\u00020\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b2\u00103R\u0019\u00104\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b4\u00103\u001a\u0004\b4\u0010\u0007R\u001b\u0010!\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b!\u00105\u001a\u0004\b6\u0010\u0011R\u0018\u0010'\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u00107R\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00108\u001a\u0004\b9\u0010\rR\u0019\u0010:\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b:\u00103\u001a\u0004\b:\u0010\u0007R\u0019\u0010;\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b;\u00103\u001a\u0004\b;\u0010\u0007R\u0013\u0010<\u001a\u00020\u00058F@\u0006¢\u0006\u0006\u001a\u0004\b<\u0010\u0007R\u001b\u0010#\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010=\u001a\u0004\b>\u0010\u0015R\u0019\u0010?\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b?\u00103\u001a\u0004\b?\u0010\u0007R\u0019\u0010\"\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00103\u001a\u0004\b\"\u0010\u0007R\u0019\u0010 \u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b \u00103\u001a\u0004\b \u0010\u0007R\u0019\u0010@\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b@\u00103\u001a\u0004\b@\u0010\u0007R\u0016\u0010A\u001a\u00020\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bA\u00103R\u0019\u0010\u001e\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010B\u001a\u0004\bC\u0010\nR\u0019\u0010&\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b&\u00103\u001a\u0004\b&\u0010\u0007R\u0016\u0010(\u001a\u00020\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u00103R\u001b\u0010E\u001a\u0004\u0018\u00010D8\u0006@\u0006¢\u0006\f\n\u0004\bE\u0010F\u001a\u0004\bG\u0010HR!\u0010$\u001a\n\u0018\u00010\u0016j\u0004\u0018\u0001`\u00178\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010I\u001a\u0004\bJ\u0010\u0019R\u0013\u0010L\u001a\u00020\u00168F@\u0006¢\u0006\u0006\u001a\u0004\bK\u0010\u0019R\u001b\u0010M\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\bM\u0010I\u001a\u0004\bN\u0010\u0019R!\u0010P\u001a\n\u0018\u00010,j\u0004\u0018\u0001`O8\u0006@\u0006¢\u0006\f\n\u0004\bP\u0010Q\u001a\u0004\bR\u0010SR\u0016\u0010T\u001a\u00020\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bT\u00103R\u001b\u0010%\u001a\u0004\u0018\u00010\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010U\u001a\u0004\bV\u0010\u001cR!\u0010W\u001a\n\u0018\u00010,j\u0004\u0018\u0001`O8\u0006@\u0006¢\u0006\f\n\u0004\bW\u0010Q\u001a\u0004\bX\u0010SR\u0016\u0010Y\u001a\u00020\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bY\u00103¨\u0006\\"}, d2 = {"Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "", "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "component10", "()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "", "component11", "()Z", "Lcom/discord/models/user/User;", "component1", "()Lcom/discord/models/user/User;", "Lcom/discord/api/voice/state/VoiceState;", "component2", "()Lcom/discord/api/voice/state/VoiceState;", "component3", "Lcom/discord/stores/StoreVideoStreams$UserStreams;", "component4", "()Lcom/discord/stores/StoreVideoStreams$UserStreams;", "component5", "Lcom/discord/models/member/GuildMember;", "component6", "()Lcom/discord/models/member/GuildMember;", "", "Lcom/discord/primitives/StreamKey;", "component7", "()Ljava/lang/String;", "Lcom/discord/utilities/streams/StreamContext;", "component8", "()Lcom/discord/utilities/streams/StreamContext;", "component9", "user", "voiceState", "isRinging", "streams", "isMe", "guildMember", "watchingStream", "streamContext", "isBooster", "voiceConfiguration", "_isSpeaking", "copy", "(Lcom/discord/models/user/User;Lcom/discord/api/voice/state/VoiceState;ZLcom/discord/stores/StoreVideoStreams$UserStreams;ZLcom/discord/models/member/GuildMember;Ljava/lang/String;Lcom/discord/utilities/streams/StreamContext;ZLcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Z)Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "isServerDeafened", "Z", "isDeafened", "Lcom/discord/stores/StoreVideoStreams$UserStreams;", "getStreams", "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "Lcom/discord/api/voice/state/VoiceState;", "getVoiceState", "isRequestingToSpeak", "isSpeaking", "isConnected", "Lcom/discord/models/member/GuildMember;", "getGuildMember", "isInvitedToSpeak", "isMuted", "isSelfDeafened", "Lcom/discord/models/user/User;", "getUser", "Lcom/discord/models/domain/ModelApplicationStream;", "applicationStream", "Lcom/discord/models/domain/ModelApplicationStream;", "getApplicationStream", "()Lcom/discord/models/domain/ModelApplicationStream;", "Ljava/lang/String;", "getWatchingStream", "getDisplayName", "displayName", "nickname", "getNickname", "Lcom/discord/primitives/StreamId;", "applicationStreamId", "Ljava/lang/Integer;", "getApplicationStreamId", "()Ljava/lang/Integer;", "isSelfMuted", "Lcom/discord/utilities/streams/StreamContext;", "getStreamContext", "callStreamId", "getCallStreamId", "isServerMuted", HookHelper.constructorName, "(Lcom/discord/models/user/User;Lcom/discord/api/voice/state/VoiceState;ZLcom/discord/stores/StoreVideoStreams$UserStreams;ZLcom/discord/models/member/GuildMember;Ljava/lang/String;Lcom/discord/utilities/streams/StreamContext;ZLcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class VoiceUser {
        private final boolean _isSpeaking;
        private final ModelApplicationStream applicationStream;
        private final Integer applicationStreamId;
        private final Integer callStreamId;
        private final GuildMember guildMember;
        private final boolean isBooster;
        private final boolean isDeafened;
        private final boolean isInvitedToSpeak;
        private final boolean isMe;
        private final boolean isMuted;
        private final boolean isRequestingToSpeak;
        private final boolean isRinging;
        private final boolean isSelfDeafened;
        private final boolean isSelfMuted;
        private final boolean isServerDeafened;
        private final boolean isServerMuted;
        private final boolean isSpeaking;
        private final String nickname;
        private final StreamContext streamContext;
        private final StoreVideoStreams.UserStreams streams;
        private final User user;
        private final StoreMediaSettings.VoiceConfiguration voiceConfiguration;
        private final VoiceState voiceState;
        private final String watchingStream;

        public VoiceUser(User user, VoiceState voiceState, boolean z2, StoreVideoStreams.UserStreams userStreams, boolean z3, GuildMember guildMember, String str, StreamContext streamContext, boolean z4, StoreMediaSettings.VoiceConfiguration voiceConfiguration, boolean z5) {
            boolean z6;
            boolean z7;
            m.checkNotNullParameter(user, "user");
            this.user = user;
            this.voiceState = voiceState;
            this.isRinging = z2;
            this.streams = userStreams;
            this.isMe = z3;
            this.guildMember = guildMember;
            this.watchingStream = str;
            this.streamContext = streamContext;
            this.isBooster = z4;
            this.voiceConfiguration = voiceConfiguration;
            this._isSpeaking = z5;
            boolean z8 = true;
            this.isSpeaking = voiceState != null && z5 && !voiceState.e() && !voiceState.b();
            ModelApplicationStream modelApplicationStream = null;
            this.nickname = guildMember != null ? guildMember.getNick() : null;
            this.callStreamId = userStreams != null ? userStreams.getCallStreamId() : null;
            this.applicationStreamId = userStreams != null ? userStreams.getApplicationStreamId() : null;
            this.applicationStream = streamContext != null ? streamContext.getStream() : modelApplicationStream;
            boolean e = voiceState != null ? voiceState.e() : false;
            this.isServerMuted = e;
            if (z3) {
                if (voiceConfiguration != null) {
                    z6 = voiceConfiguration.isSelfMuted();
                }
                z6 = false;
            } else {
                if (voiceState != null) {
                    z6 = voiceState.h();
                }
                z6 = false;
            }
            this.isSelfMuted = z6;
            this.isMuted = e || z6;
            boolean b2 = voiceState != null ? voiceState.b() : false;
            this.isServerDeafened = b2;
            if (z3) {
                if (voiceConfiguration != null) {
                    z7 = voiceConfiguration.isSelfDeafened();
                }
                z7 = false;
            } else {
                if (voiceState != null) {
                    z7 = voiceState.g();
                }
                z7 = false;
            }
            this.isSelfDeafened = z7;
            this.isDeafened = b2 || z7;
            this.isRequestingToSpeak = d.y0(voiceState).isRequestingToSpeak();
            this.isInvitedToSpeak = d.y0(voiceState) != StageRequestToSpeakState.REQUESTED_TO_SPEAK_AND_AWAITING_USER_ACK ? false : z8;
        }

        private final StoreMediaSettings.VoiceConfiguration component10() {
            return this.voiceConfiguration;
        }

        private final boolean component11() {
            return this._isSpeaking;
        }

        public final User component1() {
            return this.user;
        }

        public final VoiceState component2() {
            return this.voiceState;
        }

        public final boolean component3() {
            return this.isRinging;
        }

        public final StoreVideoStreams.UserStreams component4() {
            return this.streams;
        }

        public final boolean component5() {
            return this.isMe;
        }

        public final GuildMember component6() {
            return this.guildMember;
        }

        public final String component7() {
            return this.watchingStream;
        }

        public final StreamContext component8() {
            return this.streamContext;
        }

        public final boolean component9() {
            return this.isBooster;
        }

        public final VoiceUser copy(User user, VoiceState voiceState, boolean z2, StoreVideoStreams.UserStreams userStreams, boolean z3, GuildMember guildMember, String str, StreamContext streamContext, boolean z4, StoreMediaSettings.VoiceConfiguration voiceConfiguration, boolean z5) {
            m.checkNotNullParameter(user, "user");
            return new VoiceUser(user, voiceState, z2, userStreams, z3, guildMember, str, streamContext, z4, voiceConfiguration, z5);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof VoiceUser)) {
                return false;
            }
            VoiceUser voiceUser = (VoiceUser) obj;
            return m.areEqual(this.user, voiceUser.user) && m.areEqual(this.voiceState, voiceUser.voiceState) && this.isRinging == voiceUser.isRinging && m.areEqual(this.streams, voiceUser.streams) && this.isMe == voiceUser.isMe && m.areEqual(this.guildMember, voiceUser.guildMember) && m.areEqual(this.watchingStream, voiceUser.watchingStream) && m.areEqual(this.streamContext, voiceUser.streamContext) && this.isBooster == voiceUser.isBooster && m.areEqual(this.voiceConfiguration, voiceUser.voiceConfiguration) && this._isSpeaking == voiceUser._isSpeaking;
        }

        public final ModelApplicationStream getApplicationStream() {
            return this.applicationStream;
        }

        public final Integer getApplicationStreamId() {
            return this.applicationStreamId;
        }

        public final Integer getCallStreamId() {
            return this.callStreamId;
        }

        public final String getDisplayName() {
            String str = this.nickname;
            return str != null ? str : this.user.getUsername();
        }

        public final GuildMember getGuildMember() {
            return this.guildMember;
        }

        public final String getNickname() {
            return this.nickname;
        }

        public final StreamContext getStreamContext() {
            return this.streamContext;
        }

        public final StoreVideoStreams.UserStreams getStreams() {
            return this.streams;
        }

        public final User getUser() {
            return this.user;
        }

        public final VoiceState getVoiceState() {
            return this.voiceState;
        }

        public final String getWatchingStream() {
            return this.watchingStream;
        }

        public int hashCode() {
            User user = this.user;
            int i = 0;
            int hashCode = (user != null ? user.hashCode() : 0) * 31;
            VoiceState voiceState = this.voiceState;
            int hashCode2 = (hashCode + (voiceState != null ? voiceState.hashCode() : 0)) * 31;
            boolean z2 = this.isRinging;
            int i2 = 1;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            int i5 = (hashCode2 + i3) * 31;
            StoreVideoStreams.UserStreams userStreams = this.streams;
            int hashCode3 = (i5 + (userStreams != null ? userStreams.hashCode() : 0)) * 31;
            boolean z3 = this.isMe;
            if (z3) {
                z3 = true;
            }
            int i6 = z3 ? 1 : 0;
            int i7 = z3 ? 1 : 0;
            int i8 = (hashCode3 + i6) * 31;
            GuildMember guildMember = this.guildMember;
            int hashCode4 = (i8 + (guildMember != null ? guildMember.hashCode() : 0)) * 31;
            String str = this.watchingStream;
            int hashCode5 = (hashCode4 + (str != null ? str.hashCode() : 0)) * 31;
            StreamContext streamContext = this.streamContext;
            int hashCode6 = (hashCode5 + (streamContext != null ? streamContext.hashCode() : 0)) * 31;
            boolean z4 = this.isBooster;
            if (z4) {
                z4 = true;
            }
            int i9 = z4 ? 1 : 0;
            int i10 = z4 ? 1 : 0;
            int i11 = (hashCode6 + i9) * 31;
            StoreMediaSettings.VoiceConfiguration voiceConfiguration = this.voiceConfiguration;
            if (voiceConfiguration != null) {
                i = voiceConfiguration.hashCode();
            }
            int i12 = (i11 + i) * 31;
            boolean z5 = this._isSpeaking;
            if (!z5) {
                i2 = z5 ? 1 : 0;
            }
            return i12 + i2;
        }

        public final boolean isBooster() {
            return this.isBooster;
        }

        public final boolean isConnected() {
            return this.voiceState != null;
        }

        public final boolean isDeafened() {
            return this.isDeafened;
        }

        public final boolean isInvitedToSpeak() {
            return this.isInvitedToSpeak;
        }

        public final boolean isMe() {
            return this.isMe;
        }

        public final boolean isMuted() {
            return this.isMuted;
        }

        public final boolean isRequestingToSpeak() {
            return this.isRequestingToSpeak;
        }

        public final boolean isRinging() {
            return this.isRinging;
        }

        public final boolean isSpeaking() {
            return this.isSpeaking;
        }

        public String toString() {
            StringBuilder R = a.R("VoiceUser(user=");
            R.append(this.user);
            R.append(", voiceState=");
            R.append(this.voiceState);
            R.append(", isRinging=");
            R.append(this.isRinging);
            R.append(", streams=");
            R.append(this.streams);
            R.append(", isMe=");
            R.append(this.isMe);
            R.append(", guildMember=");
            R.append(this.guildMember);
            R.append(", watchingStream=");
            R.append(this.watchingStream);
            R.append(", streamContext=");
            R.append(this.streamContext);
            R.append(", isBooster=");
            R.append(this.isBooster);
            R.append(", voiceConfiguration=");
            R.append(this.voiceConfiguration);
            R.append(", _isSpeaking=");
            return a.M(R, this._isSpeaking, ")");
        }
    }

    public StoreVoiceParticipants(StoreStream storeStream) {
        m.checkNotNullParameter(storeStream, "stream");
        this.stream = storeStream;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Map<Long, VoiceUser> create(MeUser meUser, Collection<? extends User> collection, Map<Long, VoiceState> map, Set<Long> set, Collection<Long> collection2, Map<Long, StoreVideoStreams.UserStreams> map2, Map<Long, GuildMember> map3, Map<String, ? extends List<Long>> map4, StoreMediaSettings.VoiceConfiguration voiceConfiguration, Map<Long, StreamContext> map5) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        for (Map.Entry<String, ? extends List<Long>> entry : map4.entrySet()) {
            if (entry.getValue().contains(Long.valueOf(meUser.getId()))) {
                linkedHashMap2.put(entry.getKey(), entry.getValue());
            }
        }
        ArrayList arrayList = new ArrayList(linkedHashMap2.size());
        for (Map.Entry entry2 : linkedHashMap2.entrySet()) {
            arrayList.add((String) entry2.getKey());
        }
        String str = (String) u.firstOrNull((List<? extends Object>) arrayList);
        VoiceState voiceState = map.get(Long.valueOf(meUser.getId()));
        boolean contains = collection2.contains(Long.valueOf(meUser.getId()));
        StoreVideoStreams.UserStreams userStreams = map2.get(Long.valueOf(meUser.getId()));
        GuildMember guildMember = map3.get(Long.valueOf(meUser.getId()));
        GuildMember guildMember2 = map3.get(Long.valueOf(meUser.getId()));
        linkedHashMap.put(Long.valueOf(meUser.getId()), new VoiceUser(meUser, voiceState, contains, userStreams, true, guildMember, str, null, (guildMember2 != null ? guildMember2.getPremiumSince() : null) != null, voiceConfiguration, set.contains(Long.valueOf(meUser.getId()))));
        ArrayList<VoiceUser> arrayList2 = new ArrayList(o.collectionSizeOrDefault(collection, 10));
        for (User user : collection) {
            LinkedHashMap linkedHashMap3 = new LinkedHashMap();
            for (Map.Entry<String, ? extends List<Long>> entry3 : map4.entrySet()) {
                if (entry3.getValue().contains(Long.valueOf(user.getId()))) {
                    linkedHashMap3.put(entry3.getKey(), entry3.getValue());
                }
            }
            ArrayList arrayList3 = new ArrayList(linkedHashMap3.size());
            for (Map.Entry entry4 : linkedHashMap3.entrySet()) {
                arrayList3.add((String) entry4.getKey());
            }
            String str2 = (String) u.firstOrNull((List<? extends Object>) arrayList3);
            VoiceState voiceState2 = (VoiceState) a.e(user, map);
            boolean contains2 = collection2.contains(Long.valueOf(user.getId()));
            StoreVideoStreams.UserStreams userStreams2 = (StoreVideoStreams.UserStreams) a.e(user, map2);
            GuildMember guildMember3 = (GuildMember) a.e(user, map3);
            StreamContext streamContext = (StreamContext) a.e(user, map5);
            GuildMember guildMember4 = (GuildMember) a.e(user, map3);
            arrayList2.add(new VoiceUser(user, voiceState2, contains2, userStreams2, false, guildMember3, str2, streamContext, (guildMember4 != null ? guildMember4.getPremiumSince() : null) != null, voiceConfiguration, set.contains(Long.valueOf(user.getId()))));
        }
        for (VoiceUser voiceUser : arrayList2) {
            linkedHashMap.put(Long.valueOf(voiceUser.getUser().getId()), voiceUser);
        }
        return linkedHashMap;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Observable<Collection<User>> getOtherVoiceUsers(Channel channel, Map<Long, VoiceState> map) {
        if (ChannelUtils.x(channel)) {
            k kVar = new k(ChannelUtils.g(channel));
            m.checkNotNullExpressionValue(kVar, "Observable.just(channel.getRecipients())");
            return kVar;
        } else if (ChannelUtils.t(channel)) {
            Observable Y = StoreStream.Companion.getUsers().observeMeId().Y(new StoreVoiceParticipants$getOtherVoiceUsers$1(channel, map));
            m.checkNotNullExpressionValue(Y, "StoreStream\n            …          }\n            }");
            return Y;
        } else {
            k kVar2 = new k(n.emptyList());
            m.checkNotNullExpressionValue(kVar2, "Observable.just(emptyList())");
            return kVar2;
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Observable<Map<Long, StreamContext>> getStreamContextsForUsers(final List<Long> list) {
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
        Iterator it = list.iterator();
        while (it.hasNext()) {
            long longValue = ((Number) it.next()).longValue();
            it = it;
            arrayList.add(new StreamContextService(null, null, null, null, null, null, null, null, 255, null).getForUser(longValue, true));
        }
        Observable<Map<Long, StreamContext>> b2 = Observable.b(arrayList, new FuncN<Map<Long, ? extends StreamContext>>() { // from class: com.discord.stores.StoreVoiceParticipants$getStreamContextsForUsers$1
            @Override // rx.functions.FuncN
            public final Map<Long, ? extends StreamContext> call(Object[] objArr) {
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                int i = 0;
                for (Object obj : list) {
                    i++;
                    if (i < 0) {
                        n.throwIndexOverflow();
                    }
                    linkedHashMap.put(Long.valueOf(((Number) obj).longValue()), (StreamContext) objArr[i]);
                }
                return linkedHashMap;
            }
        });
        m.checkNotNullExpressionValue(b2, "Observable\n        .comb…}\n          map\n        }");
        return b2;
    }

    public final Observable<Map<Long, VoiceUser>> get(long j) {
        Observable<R> Y = StoreStream.Companion.getChannels().observeChannel(j).Y(new StoreVoiceParticipants$get$1(this, j));
        m.checkNotNullExpressionValue(Y, "StoreStream\n        .get…  }\n          }\n        }");
        Observable<Map<Long, VoiceUser>> q = ObservableExtensionsKt.computationLatest(Y).q();
        m.checkNotNullExpressionValue(q, "StoreStream\n        .get…  .distinctUntilChanged()");
        return q;
    }

    public final StoreStream getStream() {
        return this.stream;
    }

    @Override // com.discord.stores.Store
    public void init(Context context) {
        m.checkNotNullParameter(context, "context");
        super.init(context);
        ObservableExtensionsKt.appSubscribe(this.stream.getVoiceChannelSelected$app_productionGoogleRelease().observeSelectedVoiceChannelId(), StoreVoiceParticipants.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreVoiceParticipants$init$1(this));
    }

    public final void selectParticipant(Long l) {
        this.selectedParticipantSubject.onNext(l);
    }
}
