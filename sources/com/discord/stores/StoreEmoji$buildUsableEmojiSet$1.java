package com.discord.stores;

import com.discord.stores.StoreEmoji;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreEmoji.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\n\u0010\u0002\u001a\u00060\u0000j\u0002`\u0001H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/primitives/GuildId;", "guildId", "", "invoke", "(J)Z", "isExternalEmoji"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreEmoji$buildUsableEmojiSet$1 extends o implements Function1<Long, Boolean> {
    public final /* synthetic */ StoreEmoji.EmojiContext $emojiContext;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreEmoji$buildUsableEmojiSet$1(StoreEmoji.EmojiContext emojiContext) {
        super(1);
        this.$emojiContext = emojiContext;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Boolean invoke(Long l) {
        return Boolean.valueOf(invoke(l.longValue()));
    }

    public final boolean invoke(long j) {
        StoreEmoji.EmojiContext emojiContext = this.$emojiContext;
        if (emojiContext instanceof StoreEmoji.EmojiContext.Chat) {
            if (((StoreEmoji.EmojiContext.Chat) emojiContext).getGuildId() == j) {
                return false;
            }
        } else if (!(emojiContext instanceof StoreEmoji.EmojiContext.Global)) {
            if (emojiContext instanceof StoreEmoji.EmojiContext.GuildProfile) {
                if (((StoreEmoji.EmojiContext.GuildProfile) emojiContext).getGuildId() == j) {
                    return false;
                }
            } else if (!(emojiContext instanceof StoreEmoji.EmojiContext.Guild)) {
                throw new NoWhenBranchMatchedException();
            } else if (((StoreEmoji.EmojiContext.Guild) emojiContext).getGuildId() == j) {
                return false;
            }
        }
        return true;
    }
}
