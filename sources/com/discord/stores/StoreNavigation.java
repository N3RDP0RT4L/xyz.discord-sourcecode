package com.discord.stores;

import andhook.lib.HookHelper;
import android.app.Application;
import android.net.Uri;
import androidx.appcompat.widget.ActivityChooserModel;
import androidx.fragment.app.FragmentActivity;
import b.d.b.a.a;
import com.discord.app.AppActivity;
import com.discord.models.requiredaction.RequiredAction;
import com.discord.panels.PanelState;
import com.discord.stores.StoreInviteSettings;
import com.discord.stores.StoreNotices;
import com.discord.stores.StoreNux;
import com.discord.stores.StoreStream;
import com.discord.utilities.rx.ActivityLifecycleCallbacks;
import com.discord.utilities.rx.ObservableCombineLatestOverloadsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.widgets.home.PanelLayout;
import com.discord.widgets.tabs.NavigationTab;
import d0.t.n;
import d0.z.d.m;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import org.objectweb.asm.Opcodes;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subjects.SerializedSubject;
/* compiled from: StoreNavigation.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001:\u0003/01B\u000f\u0012\u0006\u0010'\u001a\u00020&¢\u0006\u0004\b-\u0010.J\u0013\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0013\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0006\u0010\u0005J\u0015\u0010\t\u001a\u00020\b2\u0006\u0010\u0007\u001a\u00020\u0003¢\u0006\u0004\b\t\u0010\nJ\u0015\u0010\u000b\u001a\u00020\b2\u0006\u0010\u0007\u001a\u00020\u0003¢\u0006\u0004\b\u000b\u0010\nJ\u0013\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u0002¢\u0006\u0004\b\r\u0010\u0005J#\u0010\u0011\u001a\u00020\b2\u0006\u0010\u000e\u001a\u00020\f2\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u000fH\u0007¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0014\u001a\u00020\b2\u0006\u0010\u0013\u001a\u00020\fH\u0007¢\u0006\u0004\b\u0014\u0010\u0015J)\u0010\u001c\u001a\u00020\b2\u0006\u0010\u0017\u001a\u00020\u00162\u0012\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a0\u0018¢\u0006\u0004\b\u001c\u0010\u001dJ\u0015\u0010 \u001a\u00020\b2\u0006\u0010\u001f\u001a\u00020\u001e¢\u0006\u0004\b \u0010!R:\u0010$\u001a&\u0012\f\u0012\n #*\u0004\u0018\u00010\u00030\u0003 #*\u0012\u0012\f\u0012\n #*\u0004\u0018\u00010\u00030\u0003\u0018\u00010\"0\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%R\u0016\u0010'\u001a\u00020&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R2\u0010*\u001a\u001e\u0012\f\u0012\n #*\u0004\u0018\u00010\f0\f\u0012\f\u0012\n #*\u0004\u0018\u00010\f0\f0)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010+R:\u0010,\u001a&\u0012\f\u0012\n #*\u0004\u0018\u00010\u00030\u0003 #*\u0012\u0012\f\u0012\n #*\u0004\u0018\u00010\u00030\u0003\u0018\u00010\"0\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b,\u0010%¨\u00062"}, d2 = {"Lcom/discord/stores/StoreNavigation;", "", "Lrx/Observable;", "Lcom/discord/panels/PanelState;", "observeLeftPanelState", "()Lrx/Observable;", "observeRightPanelState", "panelState", "", "setLeftPanelState", "(Lcom/discord/panels/PanelState;)V", "setRightPanelState", "Lcom/discord/stores/StoreNavigation$PanelAction;", "getNavigationPanelAction", "actionType", "Lcom/discord/widgets/home/PanelLayout;", "panelLayout", "setNavigationPanelAction", "(Lcom/discord/stores/StoreNavigation$PanelAction;Lcom/discord/widgets/home/PanelLayout;)V", "panelAction", "handleHomeTabSelected", "(Lcom/discord/stores/StoreNavigation$PanelAction;)V", "", "noticeName", "Lkotlin/Function1;", "Landroidx/fragment/app/FragmentActivity;", "", "showAction", "launchNotice", "(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V", "Landroid/app/Application;", "application", "init", "(Landroid/app/Application;)V", "Lrx/subjects/BehaviorSubject;", "kotlin.jvm.PlatformType", "leftPanelStateSubject", "Lrx/subjects/BehaviorSubject;", "Lcom/discord/stores/StoreStream;", "stream", "Lcom/discord/stores/StoreStream;", "Lrx/subjects/SerializedSubject;", "navigationPanelActionSubject", "Lrx/subjects/SerializedSubject;", "rightPanelStateSubject", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;)V", "ActivityNavigationLifecycleCallbacks", "AgeGate", "PanelAction", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreNavigation {
    private final BehaviorSubject<PanelState> leftPanelStateSubject;
    private final SerializedSubject<PanelAction, PanelAction> navigationPanelActionSubject = new SerializedSubject<>(BehaviorSubject.l0(PanelAction.NOOP));
    private final BehaviorSubject<PanelState> rightPanelStateSubject;
    private final StoreStream stream;

    /* compiled from: StoreNavigation.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0012\n\u0002\u0018\u0002\n\u0002\b\u000e\b\u0002\u0018\u00002\u00020\u0001:\u0001/B\u000f\u0012\u0006\u0010#\u001a\u00020\"¢\u0006\u0004\b-\u0010.J\u0015\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u001b\u0010\t\u001a\u00020\b*\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\f\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\f\u0010\rR+\u0010\u0010\u001a\u0014\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000f0\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R+\u0010\u0014\u001a\u0014\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000f0\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0011\u001a\u0004\b\u0015\u0010\u0013R+\u0010\u0016\u001a\u0014\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000f0\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0011\u001a\u0004\b\u0017\u0010\u0013R+\u0010\u0018\u001a\u0014\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000f0\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0011\u001a\u0004\b\u0019\u0010\u0013R+\u0010\u001a\u001a\u0014\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000f0\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0011\u001a\u0004\b\u001b\u0010\u0013R+\u0010\u001c\u001a\u0014\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000f0\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u0011\u001a\u0004\b\u001d\u0010\u0013R+\u0010\u001e\u001a\u0014\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000f0\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u0011\u001a\u0004\b\u001f\u0010\u0013R+\u0010 \u001a\u0014\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000f0\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0011\u001a\u0004\b!\u0010\u0013R\u0016\u0010#\u001a\u00020\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$R+\u0010%\u001a\u0014\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000f0\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010\u0011\u001a\u0004\b&\u0010\u0013R+\u0010'\u001a\u0014\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000f0\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010\u0011\u001a\u0004\b(\u0010\u0013R+\u0010)\u001a\u0014\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000f0\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010\u0011\u001a\u0004\b*\u0010\u0013R+\u0010+\u001a\u0014\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000f0\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010\u0011\u001a\u0004\b,\u0010\u0013¨\u00060"}, d2 = {"Lcom/discord/stores/StoreNavigation$ActivityNavigationLifecycleCallbacks;", "Lcom/discord/utilities/rx/ActivityLifecycleCallbacks;", "Lrx/Observable;", "Lcom/discord/stores/StoreNavigation$ActivityNavigationLifecycleCallbacks$ModelGlobalNavigation;", "getGlobalNavigationData", "()Lrx/Observable;", "Lcom/discord/app/AppActivity;", "model", "", "handleGlobalNavigationData", "(Lcom/discord/app/AppActivity;Lcom/discord/stores/StoreNavigation$ActivityNavigationLifecycleCallbacks$ModelGlobalNavigation;)V", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "onActivityCreatedOrResumed", "(Lcom/discord/app/AppActivity;)V", "Lkotlin/Function2;", "", "noticeHandler", "Lkotlin/jvm/functions/Function2;", "getNoticeHandler", "()Lkotlin/jvm/functions/Function2;", "tosNavHandler", "getTosNavHandler", "clientOutdatedNavHandler", "getClientOutdatedNavHandler", "inviteCodeNavHandler", "getInviteCodeNavHandler", "nuxStateNavHandler", "getNuxStateNavHandler", "guildTemplateCodeNavHandler", "getGuildTemplateCodeNavHandler", "oAuthStateHandler", "getOAuthStateHandler", "clientInitializedNavHandler", "getClientInitializedNavHandler", "Lcom/discord/stores/StoreStream;", "stream", "Lcom/discord/stores/StoreStream;", "authNavHandler", "getAuthNavHandler", "ageGateNavHandler", "getAgeGateNavHandler", "verificationNavHandler", "getVerificationNavHandler", "callNavHandler", "getCallNavHandler", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;)V", "ModelGlobalNavigation", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ActivityNavigationLifecycleCallbacks extends ActivityLifecycleCallbacks {
        private final StoreStream stream;
        private final Function2<AppActivity, ModelGlobalNavigation, Boolean> clientInitializedNavHandler = StoreNavigation$ActivityNavigationLifecycleCallbacks$clientInitializedNavHandler$1.INSTANCE;
        private final Function2<AppActivity, ModelGlobalNavigation, Boolean> clientOutdatedNavHandler = StoreNavigation$ActivityNavigationLifecycleCallbacks$clientOutdatedNavHandler$1.INSTANCE;
        private final Function2<AppActivity, ModelGlobalNavigation, Boolean> authNavHandler = StoreNavigation$ActivityNavigationLifecycleCallbacks$authNavHandler$1.INSTANCE;
        private final Function2<AppActivity, ModelGlobalNavigation, Boolean> tosNavHandler = StoreNavigation$ActivityNavigationLifecycleCallbacks$tosNavHandler$1.INSTANCE;
        private final Function2<AppActivity, ModelGlobalNavigation, Boolean> ageGateNavHandler = StoreNavigation$ActivityNavigationLifecycleCallbacks$ageGateNavHandler$1.INSTANCE;
        private final Function2<AppActivity, ModelGlobalNavigation, Boolean> verificationNavHandler = StoreNavigation$ActivityNavigationLifecycleCallbacks$verificationNavHandler$1.INSTANCE;
        private final Function2<AppActivity, ModelGlobalNavigation, Boolean> callNavHandler = StoreNavigation$ActivityNavigationLifecycleCallbacks$callNavHandler$1.INSTANCE;
        private final Function2<AppActivity, ModelGlobalNavigation, Boolean> inviteCodeNavHandler = new StoreNavigation$ActivityNavigationLifecycleCallbacks$inviteCodeNavHandler$1(this);
        private final Function2<AppActivity, ModelGlobalNavigation, Boolean> guildTemplateCodeNavHandler = new StoreNavigation$ActivityNavigationLifecycleCallbacks$guildTemplateCodeNavHandler$1(this);
        private final Function2<AppActivity, ModelGlobalNavigation, Boolean> nuxStateNavHandler = new StoreNavigation$ActivityNavigationLifecycleCallbacks$nuxStateNavHandler$1(this);
        private final Function2<AppActivity, ModelGlobalNavigation, Boolean> oAuthStateHandler = new StoreNavigation$ActivityNavigationLifecycleCallbacks$oAuthStateHandler$1(this);
        private final Function2<AppActivity, ModelGlobalNavigation, Boolean> noticeHandler = StoreNavigation$ActivityNavigationLifecycleCallbacks$noticeHandler$1.INSTANCE;

        /* compiled from: StoreNavigation.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0010\b\n\u0002\b\u001e\b\u0082\b\u0018\u00002\u00020\u0001B\u0081\u0001\u0012\u0006\u0010\"\u001a\u00020\u0002\u0012\u0006\u0010#\u001a\u00020\u0005\u0012\b\u0010$\u001a\u0004\u0018\u00010\b\u0012\u0006\u0010%\u001a\u00020\u0005\u0012\u0006\u0010&\u001a\u00020\u0005\u0012\u0006\u0010'\u001a\u00020\r\u0012\u0006\u0010(\u001a\u00020\u0005\u0012\b\u0010)\u001a\u0004\u0018\u00010\u0011\u0012\b\u0010*\u001a\u0004\u0018\u00010\b\u0012\b\u0010+\u001a\u0004\u0018\u00010\u0015\u0012\b\u0010,\u001a\u0004\u0018\u00010\u0018\u0012\u0006\u0010-\u001a\u00020\u001b\u0012\u0006\u0010.\u001a\u00020\u001e\u0012\u0006\u0010/\u001a\u00020\u0005¢\u0006\u0004\bO\u0010PJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u000b\u0010\u0007J\u0010\u0010\f\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\f\u0010\u0007J\u0010\u0010\u000e\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0007J\u0012\u0010\u0012\u001a\u0004\u0018\u00010\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u0012\u0010\u0014\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\u0014\u0010\nJ\u0012\u0010\u0016\u001a\u0004\u0018\u00010\u0015HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0012\u0010\u0019\u001a\u0004\u0018\u00010\u0018HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u001c\u001a\u00020\u001bHÆ\u0003¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001f\u001a\u00020\u001eHÆ\u0003¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010!\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b!\u0010\u0007J¦\u0001\u00100\u001a\u00020\u00002\b\b\u0002\u0010\"\u001a\u00020\u00022\b\b\u0002\u0010#\u001a\u00020\u00052\n\b\u0002\u0010$\u001a\u0004\u0018\u00010\b2\b\b\u0002\u0010%\u001a\u00020\u00052\b\b\u0002\u0010&\u001a\u00020\u00052\b\b\u0002\u0010'\u001a\u00020\r2\b\b\u0002\u0010(\u001a\u00020\u00052\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u00112\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010+\u001a\u0004\u0018\u00010\u00152\n\b\u0002\u0010,\u001a\u0004\u0018\u00010\u00182\b\b\u0002\u0010-\u001a\u00020\u001b2\b\b\u0002\u0010.\u001a\u00020\u001e2\b\b\u0002\u0010/\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b0\u00101J\u0010\u00102\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b2\u0010\nJ\u0010\u00104\u001a\u000203HÖ\u0001¢\u0006\u0004\b4\u00105J\u001a\u00107\u001a\u00020\u00052\b\u00106\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b7\u00108R\u001b\u0010)\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b)\u00109\u001a\u0004\b:\u0010\u0013R\u0019\u0010-\u001a\u00020\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010;\u001a\u0004\b<\u0010\u001dR\u0019\u0010(\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010=\u001a\u0004\b>\u0010\u0007R\u001b\u0010,\u001a\u0004\u0018\u00010\u00188\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010?\u001a\u0004\b@\u0010\u001aR\u0019\u0010\"\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010A\u001a\u0004\bB\u0010\u0004R\u001b\u0010+\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010C\u001a\u0004\bD\u0010\u0017R\u0019\u0010/\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010=\u001a\u0004\bE\u0010\u0007R\u0019\u0010&\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010=\u001a\u0004\bF\u0010\u0007R\u0019\u0010'\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010G\u001a\u0004\bH\u0010\u000fR\u001b\u0010*\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010I\u001a\u0004\bJ\u0010\nR\u0019\u0010%\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010=\u001a\u0004\b%\u0010\u0007R\u0019\u0010#\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010=\u001a\u0004\bK\u0010\u0007R\u001b\u0010$\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010I\u001a\u0004\bL\u0010\nR\u0019\u0010.\u001a\u00020\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010M\u001a\u0004\bN\u0010 ¨\u0006Q"}, d2 = {"Lcom/discord/stores/StoreNavigation$ActivityNavigationLifecycleCallbacks$ModelGlobalNavigation;", "", "Lcom/discord/stores/StoreNux$NuxState;", "component1", "()Lcom/discord/stores/StoreNux$NuxState;", "", "component2", "()Z", "", "component3", "()Ljava/lang/String;", "component4", "component5", "Lcom/discord/models/requiredaction/RequiredAction;", "component6", "()Lcom/discord/models/requiredaction/RequiredAction;", "component7", "Lcom/discord/stores/StoreInviteSettings$InviteCode;", "component8", "()Lcom/discord/stores/StoreInviteSettings$InviteCode;", "component9", "Lcom/discord/stores/StoreNotices$Notice;", "component10", "()Lcom/discord/stores/StoreNotices$Notice;", "Lcom/discord/stores/StoreNavigation$AgeGate;", "component11", "()Lcom/discord/stores/StoreNavigation$AgeGate;", "Landroid/net/Uri;", "component12", "()Landroid/net/Uri;", "Lcom/discord/widgets/tabs/NavigationTab;", "component13", "()Lcom/discord/widgets/tabs/NavigationTab;", "component14", "nuxState", "initialized", "authToken", "isInitializedForAuthedUser", "incomingCall", "userRequiredAction", "clientOutdated", "inviteCode", "guildTemplateCode", "notice", "shouldShowAgeGate", "oAuthUri", "navigationTab", "userHasPhone", "copy", "(Lcom/discord/stores/StoreNux$NuxState;ZLjava/lang/String;ZZLcom/discord/models/requiredaction/RequiredAction;ZLcom/discord/stores/StoreInviteSettings$InviteCode;Ljava/lang/String;Lcom/discord/stores/StoreNotices$Notice;Lcom/discord/stores/StoreNavigation$AgeGate;Landroid/net/Uri;Lcom/discord/widgets/tabs/NavigationTab;Z)Lcom/discord/stores/StoreNavigation$ActivityNavigationLifecycleCallbacks$ModelGlobalNavigation;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StoreInviteSettings$InviteCode;", "getInviteCode", "Landroid/net/Uri;", "getOAuthUri", "Z", "getClientOutdated", "Lcom/discord/stores/StoreNavigation$AgeGate;", "getShouldShowAgeGate", "Lcom/discord/stores/StoreNux$NuxState;", "getNuxState", "Lcom/discord/stores/StoreNotices$Notice;", "getNotice", "getUserHasPhone", "getIncomingCall", "Lcom/discord/models/requiredaction/RequiredAction;", "getUserRequiredAction", "Ljava/lang/String;", "getGuildTemplateCode", "getInitialized", "getAuthToken", "Lcom/discord/widgets/tabs/NavigationTab;", "getNavigationTab", HookHelper.constructorName, "(Lcom/discord/stores/StoreNux$NuxState;ZLjava/lang/String;ZZLcom/discord/models/requiredaction/RequiredAction;ZLcom/discord/stores/StoreInviteSettings$InviteCode;Ljava/lang/String;Lcom/discord/stores/StoreNotices$Notice;Lcom/discord/stores/StoreNavigation$AgeGate;Landroid/net/Uri;Lcom/discord/widgets/tabs/NavigationTab;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class ModelGlobalNavigation {
            private final String authToken;
            private final boolean clientOutdated;
            private final String guildTemplateCode;
            private final boolean incomingCall;
            private final boolean initialized;
            private final StoreInviteSettings.InviteCode inviteCode;
            private final boolean isInitializedForAuthedUser;
            private final NavigationTab navigationTab;
            private final StoreNotices.Notice notice;
            private final StoreNux.NuxState nuxState;
            private final Uri oAuthUri;
            private final AgeGate shouldShowAgeGate;
            private final boolean userHasPhone;
            private final RequiredAction userRequiredAction;

            public ModelGlobalNavigation(StoreNux.NuxState nuxState, boolean z2, String str, boolean z3, boolean z4, RequiredAction requiredAction, boolean z5, StoreInviteSettings.InviteCode inviteCode, String str2, StoreNotices.Notice notice, AgeGate ageGate, Uri uri, NavigationTab navigationTab, boolean z6) {
                m.checkNotNullParameter(nuxState, "nuxState");
                m.checkNotNullParameter(requiredAction, "userRequiredAction");
                m.checkNotNullParameter(uri, "oAuthUri");
                m.checkNotNullParameter(navigationTab, "navigationTab");
                this.nuxState = nuxState;
                this.initialized = z2;
                this.authToken = str;
                this.isInitializedForAuthedUser = z3;
                this.incomingCall = z4;
                this.userRequiredAction = requiredAction;
                this.clientOutdated = z5;
                this.inviteCode = inviteCode;
                this.guildTemplateCode = str2;
                this.notice = notice;
                this.shouldShowAgeGate = ageGate;
                this.oAuthUri = uri;
                this.navigationTab = navigationTab;
                this.userHasPhone = z6;
            }

            public final StoreNux.NuxState component1() {
                return this.nuxState;
            }

            public final StoreNotices.Notice component10() {
                return this.notice;
            }

            public final AgeGate component11() {
                return this.shouldShowAgeGate;
            }

            public final Uri component12() {
                return this.oAuthUri;
            }

            public final NavigationTab component13() {
                return this.navigationTab;
            }

            public final boolean component14() {
                return this.userHasPhone;
            }

            public final boolean component2() {
                return this.initialized;
            }

            public final String component3() {
                return this.authToken;
            }

            public final boolean component4() {
                return this.isInitializedForAuthedUser;
            }

            public final boolean component5() {
                return this.incomingCall;
            }

            public final RequiredAction component6() {
                return this.userRequiredAction;
            }

            public final boolean component7() {
                return this.clientOutdated;
            }

            public final StoreInviteSettings.InviteCode component8() {
                return this.inviteCode;
            }

            public final String component9() {
                return this.guildTemplateCode;
            }

            public final ModelGlobalNavigation copy(StoreNux.NuxState nuxState, boolean z2, String str, boolean z3, boolean z4, RequiredAction requiredAction, boolean z5, StoreInviteSettings.InviteCode inviteCode, String str2, StoreNotices.Notice notice, AgeGate ageGate, Uri uri, NavigationTab navigationTab, boolean z6) {
                m.checkNotNullParameter(nuxState, "nuxState");
                m.checkNotNullParameter(requiredAction, "userRequiredAction");
                m.checkNotNullParameter(uri, "oAuthUri");
                m.checkNotNullParameter(navigationTab, "navigationTab");
                return new ModelGlobalNavigation(nuxState, z2, str, z3, z4, requiredAction, z5, inviteCode, str2, notice, ageGate, uri, navigationTab, z6);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof ModelGlobalNavigation)) {
                    return false;
                }
                ModelGlobalNavigation modelGlobalNavigation = (ModelGlobalNavigation) obj;
                return m.areEqual(this.nuxState, modelGlobalNavigation.nuxState) && this.initialized == modelGlobalNavigation.initialized && m.areEqual(this.authToken, modelGlobalNavigation.authToken) && this.isInitializedForAuthedUser == modelGlobalNavigation.isInitializedForAuthedUser && this.incomingCall == modelGlobalNavigation.incomingCall && m.areEqual(this.userRequiredAction, modelGlobalNavigation.userRequiredAction) && this.clientOutdated == modelGlobalNavigation.clientOutdated && m.areEqual(this.inviteCode, modelGlobalNavigation.inviteCode) && m.areEqual(this.guildTemplateCode, modelGlobalNavigation.guildTemplateCode) && m.areEqual(this.notice, modelGlobalNavigation.notice) && m.areEqual(this.shouldShowAgeGate, modelGlobalNavigation.shouldShowAgeGate) && m.areEqual(this.oAuthUri, modelGlobalNavigation.oAuthUri) && m.areEqual(this.navigationTab, modelGlobalNavigation.navigationTab) && this.userHasPhone == modelGlobalNavigation.userHasPhone;
            }

            public final String getAuthToken() {
                return this.authToken;
            }

            public final boolean getClientOutdated() {
                return this.clientOutdated;
            }

            public final String getGuildTemplateCode() {
                return this.guildTemplateCode;
            }

            public final boolean getIncomingCall() {
                return this.incomingCall;
            }

            public final boolean getInitialized() {
                return this.initialized;
            }

            public final StoreInviteSettings.InviteCode getInviteCode() {
                return this.inviteCode;
            }

            public final NavigationTab getNavigationTab() {
                return this.navigationTab;
            }

            public final StoreNotices.Notice getNotice() {
                return this.notice;
            }

            public final StoreNux.NuxState getNuxState() {
                return this.nuxState;
            }

            public final Uri getOAuthUri() {
                return this.oAuthUri;
            }

            public final AgeGate getShouldShowAgeGate() {
                return this.shouldShowAgeGate;
            }

            public final boolean getUserHasPhone() {
                return this.userHasPhone;
            }

            public final RequiredAction getUserRequiredAction() {
                return this.userRequiredAction;
            }

            public int hashCode() {
                StoreNux.NuxState nuxState = this.nuxState;
                int i = 0;
                int hashCode = (nuxState != null ? nuxState.hashCode() : 0) * 31;
                boolean z2 = this.initialized;
                int i2 = 1;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                int i5 = (hashCode + i3) * 31;
                String str = this.authToken;
                int hashCode2 = (i5 + (str != null ? str.hashCode() : 0)) * 31;
                boolean z3 = this.isInitializedForAuthedUser;
                if (z3) {
                    z3 = true;
                }
                int i6 = z3 ? 1 : 0;
                int i7 = z3 ? 1 : 0;
                int i8 = (hashCode2 + i6) * 31;
                boolean z4 = this.incomingCall;
                if (z4) {
                    z4 = true;
                }
                int i9 = z4 ? 1 : 0;
                int i10 = z4 ? 1 : 0;
                int i11 = (i8 + i9) * 31;
                RequiredAction requiredAction = this.userRequiredAction;
                int hashCode3 = (i11 + (requiredAction != null ? requiredAction.hashCode() : 0)) * 31;
                boolean z5 = this.clientOutdated;
                if (z5) {
                    z5 = true;
                }
                int i12 = z5 ? 1 : 0;
                int i13 = z5 ? 1 : 0;
                int i14 = (hashCode3 + i12) * 31;
                StoreInviteSettings.InviteCode inviteCode = this.inviteCode;
                int hashCode4 = (i14 + (inviteCode != null ? inviteCode.hashCode() : 0)) * 31;
                String str2 = this.guildTemplateCode;
                int hashCode5 = (hashCode4 + (str2 != null ? str2.hashCode() : 0)) * 31;
                StoreNotices.Notice notice = this.notice;
                int hashCode6 = (hashCode5 + (notice != null ? notice.hashCode() : 0)) * 31;
                AgeGate ageGate = this.shouldShowAgeGate;
                int hashCode7 = (hashCode6 + (ageGate != null ? ageGate.hashCode() : 0)) * 31;
                Uri uri = this.oAuthUri;
                int hashCode8 = (hashCode7 + (uri != null ? uri.hashCode() : 0)) * 31;
                NavigationTab navigationTab = this.navigationTab;
                if (navigationTab != null) {
                    i = navigationTab.hashCode();
                }
                int i15 = (hashCode8 + i) * 31;
                boolean z6 = this.userHasPhone;
                if (!z6) {
                    i2 = z6 ? 1 : 0;
                }
                return i15 + i2;
            }

            public final boolean isInitializedForAuthedUser() {
                return this.isInitializedForAuthedUser;
            }

            public String toString() {
                StringBuilder R = a.R("ModelGlobalNavigation(nuxState=");
                R.append(this.nuxState);
                R.append(", initialized=");
                R.append(this.initialized);
                R.append(", authToken=");
                R.append(this.authToken);
                R.append(", isInitializedForAuthedUser=");
                R.append(this.isInitializedForAuthedUser);
                R.append(", incomingCall=");
                R.append(this.incomingCall);
                R.append(", userRequiredAction=");
                R.append(this.userRequiredAction);
                R.append(", clientOutdated=");
                R.append(this.clientOutdated);
                R.append(", inviteCode=");
                R.append(this.inviteCode);
                R.append(", guildTemplateCode=");
                R.append(this.guildTemplateCode);
                R.append(", notice=");
                R.append(this.notice);
                R.append(", shouldShowAgeGate=");
                R.append(this.shouldShowAgeGate);
                R.append(", oAuthUri=");
                R.append(this.oAuthUri);
                R.append(", navigationTab=");
                R.append(this.navigationTab);
                R.append(", userHasPhone=");
                return a.M(R, this.userHasPhone, ")");
            }
        }

        public ActivityNavigationLifecycleCallbacks(StoreStream storeStream) {
            m.checkNotNullParameter(storeStream, "stream");
            this.stream = storeStream;
        }

        private final Observable<ModelGlobalNavigation> getGlobalNavigationData() {
            Observable<StoreNux.NuxState> nuxState = this.stream.getNux$app_productionGoogleRelease().getNuxState();
            Observable<Boolean> isInitializedObservable = StoreStream.Companion.isInitializedObservable();
            Observable<String> authedToken$app_productionGoogleRelease = this.stream.getAuthentication$app_productionGoogleRelease().getAuthedToken$app_productionGoogleRelease();
            Observable<Boolean> observeInitializedForAuthedUser = this.stream.getChannelsSelected$app_productionGoogleRelease().observeInitializedForAuthedUser();
            Observable<Boolean> observeHasIncoming = this.stream.getCallsIncoming$app_productionGoogleRelease().observeHasIncoming();
            Observable<RequiredAction> observeUserRequiredAction = this.stream.getUserRequiredAction$app_productionGoogleRelease().observeUserRequiredAction();
            Observable<Boolean> clientOutdated = this.stream.getClientVersion$app_productionGoogleRelease().getClientOutdated();
            Observable<StoreInviteSettings.InviteCode> inviteCode = this.stream.getGuildInvite$app_productionGoogleRelease().getInviteCode();
            Observable<String> observeDynamicLinkGuildTemplateCode = this.stream.getGuildTemplates$app_productionGoogleRelease().observeDynamicLinkGuildTemplateCode();
            Observable<StoreNotices.Notice> notices = this.stream.getNotices$app_productionGoogleRelease().getNotices();
            Observable<AgeGate> shouldShowAgeGate = this.stream.getAuthentication$app_productionGoogleRelease().getShouldShowAgeGate();
            Observable<Uri> oAuthUriObservable = this.stream.getAuthentication$app_productionGoogleRelease().getOAuthUriObservable();
            Observable<NavigationTab> observeSelectedTab = this.stream.getTabsNavigation$app_productionGoogleRelease().observeSelectedTab();
            Observable<R> F = this.stream.getUsers$app_productionGoogleRelease().observeMe(true).F(StoreNavigation$ActivityNavigationLifecycleCallbacks$getGlobalNavigationData$1.INSTANCE);
            m.checkNotNullExpressionValue(F, "stream\n              .us….hasPhone\n              }");
            Observable<ModelGlobalNavigation> q = ObservableExtensionsKt.computationLatest(ObservableExtensionsKt.leadingEdgeThrottle(ObservableCombineLatestOverloadsKt.combineLatest(nuxState, isInitializedObservable, authedToken$app_productionGoogleRelease, observeInitializedForAuthedUser, observeHasIncoming, observeUserRequiredAction, clientOutdated, inviteCode, observeDynamicLinkGuildTemplateCode, notices, shouldShowAgeGate, oAuthUriObservable, observeSelectedTab, F, StoreNavigation$ActivityNavigationLifecycleCallbacks$getGlobalNavigationData$2.INSTANCE), 500L, TimeUnit.MILLISECONDS)).q();
            m.checkNotNullExpressionValue(q, "combineLatest(\n         …  .distinctUntilChanged()");
            return q;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final void handleGlobalNavigationData(AppActivity appActivity, ModelGlobalNavigation modelGlobalNavigation) {
            Iterator it = n.listOf((Object[]) new Function2[]{this.clientInitializedNavHandler, this.clientOutdatedNavHandler, this.authNavHandler, this.tosNavHandler, this.ageGateNavHandler, this.verificationNavHandler, this.callNavHandler, this.inviteCodeNavHandler, this.guildTemplateCodeNavHandler, this.nuxStateNavHandler, this.oAuthStateHandler, this.noticeHandler}).iterator();
            while (it.hasNext() && !((Boolean) ((Function2) it.next()).invoke(appActivity, modelGlobalNavigation)).booleanValue()) {
            }
        }

        public final Function2<AppActivity, ModelGlobalNavigation, Boolean> getAgeGateNavHandler() {
            return this.ageGateNavHandler;
        }

        public final Function2<AppActivity, ModelGlobalNavigation, Boolean> getAuthNavHandler() {
            return this.authNavHandler;
        }

        public final Function2<AppActivity, ModelGlobalNavigation, Boolean> getCallNavHandler() {
            return this.callNavHandler;
        }

        public final Function2<AppActivity, ModelGlobalNavigation, Boolean> getClientInitializedNavHandler() {
            return this.clientInitializedNavHandler;
        }

        public final Function2<AppActivity, ModelGlobalNavigation, Boolean> getClientOutdatedNavHandler() {
            return this.clientOutdatedNavHandler;
        }

        public final Function2<AppActivity, ModelGlobalNavigation, Boolean> getGuildTemplateCodeNavHandler() {
            return this.guildTemplateCodeNavHandler;
        }

        public final Function2<AppActivity, ModelGlobalNavigation, Boolean> getInviteCodeNavHandler() {
            return this.inviteCodeNavHandler;
        }

        public final Function2<AppActivity, ModelGlobalNavigation, Boolean> getNoticeHandler() {
            return this.noticeHandler;
        }

        public final Function2<AppActivity, ModelGlobalNavigation, Boolean> getNuxStateNavHandler() {
            return this.nuxStateNavHandler;
        }

        public final Function2<AppActivity, ModelGlobalNavigation, Boolean> getOAuthStateHandler() {
            return this.oAuthStateHandler;
        }

        public final Function2<AppActivity, ModelGlobalNavigation, Boolean> getTosNavHandler() {
            return this.tosNavHandler;
        }

        public final Function2<AppActivity, ModelGlobalNavigation, Boolean> getVerificationNavHandler() {
            return this.verificationNavHandler;
        }

        /* JADX WARN: Removed duplicated region for block: B:11:0x0034  */
        /* JADX WARN: Removed duplicated region for block: B:13:0x0038  */
        @Override // com.discord.utilities.rx.ActivityLifecycleCallbacks
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public void onActivityCreatedOrResumed(com.discord.app.AppActivity r14) {
            /*
                r13 = this;
                java.lang.String r0 = "activity"
                d0.z.d.m.checkNotNullParameter(r14, r0)
                super.onActivityCreatedOrResumed(r14)
                boolean r0 = r14.isTaskRoot()
                if (r0 != 0) goto L31
                android.content.Intent r0 = r14.getIntent()
                java.lang.String r1 = "android.intent.category.LAUNCHER"
                boolean r0 = r0.hasCategory(r1)
                if (r0 == 0) goto L31
                android.content.Intent r0 = r14.getIntent()
                java.lang.String r1 = "activity.intent"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                java.lang.String r0 = r0.getAction()
                java.lang.String r1 = "android.intent.action.MAIN"
                boolean r0 = d0.z.d.m.areEqual(r0, r1)
                if (r0 == 0) goto L31
                r0 = 1
                goto L32
            L31:
                r0 = 0
            L32:
                if (r0 == 0) goto L38
                r14.finish()
                return
            L38:
                rx.Observable r0 = r13.getGlobalNavigationData()
                r1 = 2
                r2 = 0
                rx.Observable r3 = com.discord.utilities.rx.ObservableExtensionsKt.ui$default(r0, r14, r2, r1, r2)
                java.lang.Class<com.discord.stores.StoreNavigation$ActivityNavigationLifecycleCallbacks> r4 = com.discord.stores.StoreNavigation.ActivityNavigationLifecycleCallbacks.class
                r5 = 0
                r6 = 0
                r7 = 0
                r8 = 0
                r9 = 0
                com.discord.stores.StoreNavigation$ActivityNavigationLifecycleCallbacks$onActivityCreatedOrResumed$1 r10 = new com.discord.stores.StoreNavigation$ActivityNavigationLifecycleCallbacks$onActivityCreatedOrResumed$1
                r10.<init>(r13, r14)
                r11 = 62
                r12 = 0
                com.discord.utilities.rx.ObservableExtensionsKt.appSubscribe$default(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreNavigation.ActivityNavigationLifecycleCallbacks.onActivityCreatedOrResumed(com.discord.app.AppActivity):void");
        }
    }

    /* compiled from: StoreNavigation.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/stores/StoreNavigation$AgeGate;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "REGISTER_AGE_GATE", "NSFW_CHANNEL_AGE_GATE", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum AgeGate {
        REGISTER_AGE_GATE,
        NSFW_CHANNEL_AGE_GATE
    }

    /* compiled from: StoreNavigation.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/discord/stores/StoreNavigation$PanelAction;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "NOOP", "OPEN", "CLOSE", "UNLOCK_LEFT", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum PanelAction {
        NOOP,
        OPEN,
        CLOSE,
        UNLOCK_LEFT
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            PanelAction.values();
            int[] iArr = new int[4];
            $EnumSwitchMapping$0 = iArr;
            iArr[PanelAction.OPEN.ordinal()] = 1;
            iArr[PanelAction.CLOSE.ordinal()] = 2;
        }
    }

    public StoreNavigation(StoreStream storeStream) {
        m.checkNotNullParameter(storeStream, "stream");
        this.stream = storeStream;
        PanelState.a aVar = PanelState.a.a;
        this.leftPanelStateSubject = BehaviorSubject.l0(aVar);
        this.rightPanelStateSubject = BehaviorSubject.l0(aVar);
    }

    public static /* synthetic */ void setNavigationPanelAction$default(StoreNavigation storeNavigation, PanelAction panelAction, PanelLayout panelLayout, int i, Object obj) {
        if ((i & 2) != 0) {
            panelLayout = null;
        }
        storeNavigation.setNavigationPanelAction(panelAction, panelLayout);
    }

    public final Observable<PanelAction> getNavigationPanelAction() {
        Observable<PanelAction> q = ObservableExtensionsKt.computationLatest(this.navigationPanelActionSubject).q();
        m.checkNotNullExpressionValue(q, "navigationPanelActionSub…  .distinctUntilChanged()");
        return q;
    }

    @StoreThread
    public final void handleHomeTabSelected(PanelAction panelAction) {
        m.checkNotNullParameter(panelAction, "panelAction");
        setNavigationPanelAction$default(this, panelAction, null, 2, null);
    }

    public final void init(Application application) {
        m.checkNotNullParameter(application, "application");
        application.registerActivityLifecycleCallbacks(new ActivityNavigationLifecycleCallbacks(this.stream));
    }

    public final void launchNotice(String str, Function1<? super FragmentActivity, Boolean> function1) {
        m.checkNotNullParameter(str, "noticeName");
        m.checkNotNullParameter(function1, "showAction");
        StoreNotices.Notice notice = new StoreNotices.Notice(str, null, 0L, 0, false, null, 0L, false, 0L, new StoreNavigation$launchNotice$notice$1(str, function1), Opcodes.I2F, null);
        StoreStream.Companion companion = StoreStream.Companion;
        companion.getNotices().markInAppSeen();
        companion.getNotices().requestToShow(notice);
    }

    public final Observable<PanelState> observeLeftPanelState() {
        Observable<PanelState> q = this.leftPanelStateSubject.q();
        m.checkNotNullExpressionValue(q, "leftPanelStateSubject.distinctUntilChanged()");
        return q;
    }

    public final Observable<PanelState> observeRightPanelState() {
        Observable<PanelState> q = this.rightPanelStateSubject.q();
        m.checkNotNullExpressionValue(q, "rightPanelStateSubject.distinctUntilChanged()");
        return q;
    }

    public final void setLeftPanelState(PanelState panelState) {
        m.checkNotNullParameter(panelState, "panelState");
        this.leftPanelStateSubject.onNext(panelState);
    }

    public final void setNavigationPanelAction(PanelAction panelAction) {
        setNavigationPanelAction$default(this, panelAction, null, 2, null);
    }

    public final void setNavigationPanelAction(PanelAction panelAction, PanelLayout panelLayout) {
        m.checkNotNullParameter(panelAction, "actionType");
        if (panelLayout == null) {
            this.navigationPanelActionSubject.k.onNext(panelAction);
            return;
        }
        int ordinal = panelAction.ordinal();
        if (ordinal == 1) {
            panelLayout.openStartPanel();
        } else if (ordinal == 2) {
            panelLayout.closePanels();
        }
        SerializedSubject<PanelAction, PanelAction> serializedSubject = this.navigationPanelActionSubject;
        serializedSubject.k.onNext(PanelAction.NOOP);
    }

    public final void setRightPanelState(PanelState panelState) {
        m.checkNotNullParameter(panelState, "panelState");
        this.rightPanelStateSubject.onNext(panelState);
    }
}
