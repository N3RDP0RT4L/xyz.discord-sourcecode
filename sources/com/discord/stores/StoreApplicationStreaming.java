package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.guild.Guild;
import com.discord.api.voice.state.VoiceState;
import com.discord.app.AppLog;
import com.discord.models.domain.ModelApplicationStream;
import com.discord.models.domain.ModelPayload;
import com.discord.models.domain.StreamCreateOrUpdate;
import com.discord.models.domain.StreamDelete;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import d0.t.h0;
import d0.t.n;
import d0.z.d.m;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.collections.ArrayDeque;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreApplicationStreaming.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Ð\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0002wxB9\u0012\u0006\u0010f\u001a\u00020e\u0012\u0006\u0010Z\u001a\u00020Y\u0012\u0006\u0010s\u001a\u00020r\u0012\u0006\u0010m\u001a\u00020l\u0012\u0006\u0010p\u001a\u00020o\u0012\b\b\u0002\u0010T\u001a\u00020S¢\u0006\u0004\bu\u0010vJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ5\u0010\u0011\u001a\u00020\u00042\n\u0010\n\u001a\u00060\u0002j\u0002`\t2\u0006\u0010\f\u001a\u00020\u000b2\u0010\u0010\u0010\u001a\f\u0012\b\u0012\u00060\u000ej\u0002`\u000f0\rH\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0019\u0010\u0015\u001a\u00020\u000b2\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013H\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u001b\u0010\u0017\u001a\u00020\u00042\n\u0010\n\u001a\u00060\u0002j\u0002`\tH\u0002¢\u0006\u0004\b\u0017\u0010\u0006J\u0019\u0010\u0018\u001a\u00020\u00042\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013H\u0003¢\u0006\u0004\b\u0018\u0010\u0019J\u0017\u0010\u001c\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\tH\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u001d\u0010\u001f\u001a\u0012\u0012\b\u0012\u00060\u000ej\u0002`\u000f\u0012\u0004\u0012\u00020\u001e0\u001d¢\u0006\u0004\b\u001f\u0010 J#\u0010\"\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u000ej\u0002`\u000f\u0012\u0004\u0012\u00020\u001e0\u001d0!¢\u0006\u0004\b\"\u0010#J/\u0010&\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u000ej\u0002`\u000f\u0012\u0004\u0012\u00020\u001e0\u001d0!2\n\u0010%\u001a\u00060\u000ej\u0002`$¢\u0006\u0004\b&\u0010'J!\u0010)\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u001e0!2\n\u0010(\u001a\u00060\u000ej\u0002`\u000f¢\u0006\u0004\b)\u0010'J-\u0010*\u001a\"\u0012\u001e\u0012\u001c\u0012\b\u0012\u00060\u0002j\u0002`\t\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u000ej\u0002`\u000f0\r0\u001d0!¢\u0006\u0004\b*\u0010#J\u0015\u0010+\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00130!¢\u0006\u0004\b+\u0010#J\u001b\u0010,\u001a\u00020\u000b2\n\u0010(\u001a\u00060\u000ej\u0002`\u000fH\u0007¢\u0006\u0004\b,\u0010-J9\u00101\u001a\u0004\u0018\u0001002\n\u0010(\u001a\u00060\u000ej\u0002`\u000f2\n\u0010/\u001a\u00060\u000ej\u0002`.2\u000e\u0010%\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`$H\u0007¢\u0006\u0004\b1\u00102J\u0017\u00105\u001a\u00020\u00042\u0006\u00104\u001a\u000203H\u0007¢\u0006\u0004\b5\u00106J%\u00109\u001a\u00020\u00042\u0006\u00108\u001a\u0002072\f\b\u0002\u0010%\u001a\u00060\u000ej\u0002`$H\u0007¢\u0006\u0004\b9\u0010:J\u001b\u0010;\u001a\u00020\u00042\n\u0010\n\u001a\u00060\u0002j\u0002`\tH\u0007¢\u0006\u0004\b;\u0010\u0006J\u001b\u0010<\u001a\u00020\u00042\n\u0010\n\u001a\u00060\u0002j\u0002`\tH\u0007¢\u0006\u0004\b<\u0010\u0006J\u0017\u0010?\u001a\u00020\u00042\u0006\u0010>\u001a\u00020=H\u0007¢\u0006\u0004\b?\u0010@J\u0017\u0010B\u001a\u00020\u00042\u0006\u0010A\u001a\u00020=H\u0007¢\u0006\u0004\bB\u0010@J\u0017\u0010E\u001a\u00020\u00042\u0006\u0010D\u001a\u00020CH\u0007¢\u0006\u0004\bE\u0010FJ\u001b\u0010G\u001a\u00020\u00042\n\u0010/\u001a\u00060\u000ej\u0002`.H\u0007¢\u0006\u0004\bG\u0010HJ\u001b\u0010I\u001a\u00020\u00042\n\u0010\n\u001a\u00060\u0002j\u0002`\tH\u0007¢\u0006\u0004\bI\u0010\u0006J\u000f\u0010J\u001a\u00020\u0004H\u0017¢\u0006\u0004\bJ\u0010\bJ#\u0010L\u001a\u00020\u00042\n\u0010\n\u001a\u00060\u0002j\u0002`\t2\b\b\u0002\u0010K\u001a\u00020\u000b¢\u0006\u0004\bL\u0010MJ5\u0010O\u001a\u00020\u00042\n\u0010/\u001a\u00060\u000ej\u0002`.2\u000e\u0010%\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`$2\n\b\u0002\u0010N\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\bO\u0010PJ\u0019\u0010Q\u001a\u00020\u00042\n\u0010\n\u001a\u00060\u0002j\u0002`\t¢\u0006\u0004\bQ\u0010\u0006J\u000f\u0010\u0015\u001a\u00020\u000bH\u0007¢\u0006\u0004\b\u0015\u0010RR\u0016\u0010T\u001a\u00020S8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bT\u0010UR\u001c\u0010W\u001a\b\u0012\u0004\u0012\u00020\u00020V8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bW\u0010XR\u0016\u0010Z\u001a\u00020Y8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bZ\u0010[R\u0018\u0010\u0014\u001a\u0004\u0018\u00010\u00138\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0014\u0010\\R:\u0010_\u001a&\u0012\b\u0012\u00060\u000ej\u0002`\u000f\u0012\u0004\u0012\u00020\u001e0]j\u0012\u0012\b\u0012\u00060\u000ej\u0002`\u000f\u0012\u0004\u0012\u00020\u001e`^8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b_\u0010`R\u0016\u0010b\u001a\u00020a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bb\u0010cRN\u0010d\u001a:\u0012\b\u0012\u00060\u0002j\u0002`\t\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u000ej\u0002`\u000f0\r0]j\u001c\u0012\b\u0012\u00060\u0002j\u0002`\t\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u000ej\u0002`\u000f0\r`^8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bd\u0010`R\u0016\u0010f\u001a\u00020e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bf\u0010gR0\u0010h\u001a\u001c\u0012\b\u0012\u00060\u0002j\u0002`\t\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u000ej\u0002`\u000f0\r0\u001d8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bh\u0010iR&\u0010j\u001a\u0012\u0012\b\u0012\u00060\u000ej\u0002`\u000f\u0012\u0004\u0012\u00020\u001e0\u001d8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bj\u0010iR\u0018\u0010L\u001a\u0004\u0018\u00010\u001e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bL\u0010kR\u0016\u0010m\u001a\u00020l8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bm\u0010nR\u0016\u0010p\u001a\u00020o8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bp\u0010qR\u0016\u0010s\u001a\u00020r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bs\u0010t¨\u0006y"}, d2 = {"Lcom/discord/stores/StoreApplicationStreaming;", "Lcom/discord/stores/StoreV2;", "", "message", "", "addBreadCrumb", "(Ljava/lang/String;)V", "dumpBreadcrumbs", "()V", "Lcom/discord/primitives/StreamKey;", "streamKey", "", "paused", "", "", "Lcom/discord/primitives/UserId;", "viewerIds", "handleStreamCreateOrUpdate", "(Ljava/lang/String;ZLjava/util/List;)V", "Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;", "activeApplicationStream", "isScreenSharing", "(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;)Z", "stopStreamInternal", "updateActiveApplicationStream", "(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;)V", "getActiveApplicationStreamKeyInternal$app_productionGoogleRelease", "()Ljava/lang/String;", "getActiveApplicationStreamKeyInternal", "", "Lcom/discord/models/domain/ModelApplicationStream;", "getStreamsByUser", "()Ljava/util/Map;", "Lrx/Observable;", "observeStreamsByUser", "()Lrx/Observable;", "Lcom/discord/primitives/GuildId;", "guildId", "observeStreamsForGuild", "(J)Lrx/Observable;", "userId", "observeStreamsForUser", "observeStreamSpectators", "observeActiveStream", "isUserStreaming", "(J)Z", "Lcom/discord/primitives/ChannelId;", "channelId", "", "getMaxViewersForStream", "(JJLjava/lang/Long;)Ljava/lang/Integer;", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "Lcom/discord/api/voice/state/VoiceState;", "voiceState", "handleVoiceStateUpdate", "(Lcom/discord/api/voice/state/VoiceState;J)V", "handleStreamWatch", "handleStreamCreateRequest", "Lcom/discord/models/domain/StreamCreateOrUpdate;", "streamCreate", "handleStreamCreate", "(Lcom/discord/models/domain/StreamCreateOrUpdate;)V", "streamUpdate", "handleStreamUpdate", "Lcom/discord/models/domain/StreamDelete;", "streamDelete", "handleStreamDelete", "(Lcom/discord/models/domain/StreamDelete;)V", "handleVoiceChannelSelected", "(J)V", "handleStreamTargeted", "snapshotData", "force", "targetStream", "(Ljava/lang/String;Z)V", "preferredRegion", "createStream", "(JLjava/lang/Long;Ljava/lang/String;)V", "stopStream", "()Z", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lkotlin/collections/ArrayDeque;", "breadCrumbs", "Lkotlin/collections/ArrayDeque;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "streamsByUser", "Ljava/util/HashMap;", "Lcom/discord/stores/StoreApplicationStreaming$StreamViewerTracker;", "streamViewerTracker", "Lcom/discord/stores/StoreApplicationStreaming$StreamViewerTracker;", "streamSpectators", "Lcom/discord/stores/StoreStream;", "storeStream", "Lcom/discord/stores/StoreStream;", "streamSpectatorsSnapshot", "Ljava/util/Map;", "streamsByUserSnapshot", "Lcom/discord/models/domain/ModelApplicationStream;", "Lcom/discord/stores/StoreVoiceChannelSelected;", "voiceChannelSelectedStore", "Lcom/discord/stores/StoreVoiceChannelSelected;", "Lcom/discord/stores/StoreRtcConnection;", "rtcConnectionStore", "Lcom/discord/stores/StoreRtcConnection;", "Lcom/discord/stores/StoreUser;", "userStore", "Lcom/discord/stores/StoreUser;", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreRtcConnection;Lcom/discord/stores/updates/ObservationDeck;)V", "ActiveApplicationStream", "StreamViewerTracker", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreApplicationStreaming extends StoreV2 {
    private ActiveApplicationStream activeApplicationStream;
    private final ArrayDeque<String> breadCrumbs;
    private final Dispatcher dispatcher;
    private final ObservationDeck observationDeck;
    private final StoreRtcConnection rtcConnectionStore;
    private final StoreStream storeStream;
    private final HashMap<String, List<Long>> streamSpectators;
    private Map<String, ? extends List<Long>> streamSpectatorsSnapshot;
    private final StreamViewerTracker streamViewerTracker;
    private final HashMap<Long, ModelApplicationStream> streamsByUser;
    private Map<Long, ? extends ModelApplicationStream> streamsByUserSnapshot;
    private ModelApplicationStream targetStream;
    private final StoreUser userStore;
    private final StoreVoiceChannelSelected voiceChannelSelectedStore;

    /* compiled from: StoreApplicationStreaming.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u001cB\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0018\u001a\u0004\b\u0019\u0010\u0007¨\u0006\u001d"}, d2 = {"Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;", "", "Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;", "component1", "()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;", "Lcom/discord/models/domain/ModelApplicationStream;", "component2", "()Lcom/discord/models/domain/ModelApplicationStream;", "state", "stream", "copy", "(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;Lcom/discord/models/domain/ModelApplicationStream;)Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;", "getState", "Lcom/discord/models/domain/ModelApplicationStream;", "getStream", HookHelper.constructorName, "(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;Lcom/discord/models/domain/ModelApplicationStream;)V", "State", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ActiveApplicationStream {
        private final State state;
        private final ModelApplicationStream stream;

        /* compiled from: StoreApplicationStreaming.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\u0001\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0007B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\r¨\u0006\u000e"}, d2 = {"Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;", "", "", "isStreamActive", "()Z", HookHelper.constructorName, "(Ljava/lang/String;I)V", "Companion", "CONNECTING", "ACTIVE", "RECONNECTING", "ENDED", "PAUSED", "DENIED_FULL", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public enum State {
            CONNECTING,
            ACTIVE,
            RECONNECTING,
            ENDED,
            PAUSED,
            DENIED_FULL;
            
            private static final Companion Companion = new Companion(null);
            @Deprecated
            private static final List<State> ACTIVE_STATES = n.listOf((Object[]) new State[]{CONNECTING, ACTIVE, RECONNECTING, PAUSED});

            /* compiled from: StoreApplicationStreaming.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0082\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tR\u001f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State$Companion;", "", "", "Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;", "ACTIVE_STATES", "Ljava/util/List;", "getACTIVE_STATES", "()Ljava/util/List;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes.dex */
            public static final class Companion {
                private Companion() {
                }

                public final List<State> getACTIVE_STATES() {
                    return State.ACTIVE_STATES;
                }

                public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                    this();
                }
            }

            public final boolean isStreamActive() {
                return ACTIVE_STATES.contains(this);
            }
        }

        public ActiveApplicationStream(State state, ModelApplicationStream modelApplicationStream) {
            m.checkNotNullParameter(state, "state");
            m.checkNotNullParameter(modelApplicationStream, "stream");
            this.state = state;
            this.stream = modelApplicationStream;
        }

        public static /* synthetic */ ActiveApplicationStream copy$default(ActiveApplicationStream activeApplicationStream, State state, ModelApplicationStream modelApplicationStream, int i, Object obj) {
            if ((i & 1) != 0) {
                state = activeApplicationStream.state;
            }
            if ((i & 2) != 0) {
                modelApplicationStream = activeApplicationStream.stream;
            }
            return activeApplicationStream.copy(state, modelApplicationStream);
        }

        public final State component1() {
            return this.state;
        }

        public final ModelApplicationStream component2() {
            return this.stream;
        }

        public final ActiveApplicationStream copy(State state, ModelApplicationStream modelApplicationStream) {
            m.checkNotNullParameter(state, "state");
            m.checkNotNullParameter(modelApplicationStream, "stream");
            return new ActiveApplicationStream(state, modelApplicationStream);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ActiveApplicationStream)) {
                return false;
            }
            ActiveApplicationStream activeApplicationStream = (ActiveApplicationStream) obj;
            return m.areEqual(this.state, activeApplicationStream.state) && m.areEqual(this.stream, activeApplicationStream.stream);
        }

        public final State getState() {
            return this.state;
        }

        public final ModelApplicationStream getStream() {
            return this.stream;
        }

        public int hashCode() {
            State state = this.state;
            int i = 0;
            int hashCode = (state != null ? state.hashCode() : 0) * 31;
            ModelApplicationStream modelApplicationStream = this.stream;
            if (modelApplicationStream != null) {
                i = modelApplicationStream.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("ActiveApplicationStream(state=");
            R.append(this.state);
            R.append(", stream=");
            R.append(this.stream);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: StoreApplicationStreaming.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0010%\n\u0002\b\u0004\b\u0002\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u0013\u0010\rJ!\u0010\b\u001a\u00020\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\b\u0010\tJ\u001b\u0010\n\u001a\u0004\u0018\u00010\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\n\u0010\u000bJ\r\u0010\f\u001a\u00020\u0007¢\u0006\u0004\b\f\u0010\rJ\u0019\u0010\u000e\u001a\u00020\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u000e\u0010\u000fR&\u0010\u0011\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00050\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012¨\u0006\u0014"}, d2 = {"Lcom/discord/stores/StoreApplicationStreaming$StreamViewerTracker;", "", "", "Lcom/discord/primitives/StreamKey;", "streamKey", "", "viewerCount", "", "onStreamUpdated", "(Ljava/lang/String;I)V", "getMaxViewers", "(Ljava/lang/String;)Ljava/lang/Integer;", "clear", "()V", "remove", "(Ljava/lang/String;)V", "", "maxViewersByStream", "Ljava/util/Map;", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class StreamViewerTracker {
        private final Map<String, Integer> maxViewersByStream = new HashMap();

        public final void clear() {
            this.maxViewersByStream.clear();
        }

        public final Integer getMaxViewers(String str) {
            m.checkNotNullParameter(str, "streamKey");
            return this.maxViewersByStream.get(str);
        }

        public final void onStreamUpdated(String str, int i) {
            m.checkNotNullParameter(str, "streamKey");
            Integer num = this.maxViewersByStream.get(str);
            this.maxViewersByStream.put(str, Integer.valueOf(Math.max(num != null ? num.intValue() : 0, i)));
        }

        public final void remove(String str) {
            m.checkNotNullParameter(str, "streamKey");
            this.maxViewersByStream.remove(str);
        }
    }

    public /* synthetic */ StoreApplicationStreaming(StoreStream storeStream, Dispatcher dispatcher, StoreUser storeUser, StoreVoiceChannelSelected storeVoiceChannelSelected, StoreRtcConnection storeRtcConnection, ObservationDeck observationDeck, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(storeStream, dispatcher, storeUser, storeVoiceChannelSelected, storeRtcConnection, (i & 32) != 0 ? ObservationDeckProvider.get() : observationDeck);
    }

    private final synchronized void addBreadCrumb(String str) {
        ArrayDeque<String> arrayDeque = this.breadCrumbs;
        arrayDeque.addLast(str + ", on thread: " + Thread.currentThread());
        ArrayDeque<String> arrayDeque2 = this.breadCrumbs;
        if (arrayDeque2.size() > 50) {
            arrayDeque2.removeFirst();
        }
    }

    public static /* synthetic */ void createStream$default(StoreApplicationStreaming storeApplicationStreaming, long j, Long l, String str, int i, Object obj) {
        if ((i & 4) != 0) {
            str = null;
        }
        storeApplicationStreaming.createStream(j, l, str);
    }

    private final synchronized void dumpBreadcrumbs() {
        for (String str : this.breadCrumbs) {
            AppLog.g.recordBreadcrumb(str, "StoreApplicationStreaming");
        }
    }

    @StoreThread
    private final void handleStreamCreateOrUpdate(String str, boolean z2, List<Long> list) {
        ActiveApplicationStream.State state;
        if (z2) {
            state = ActiveApplicationStream.State.PAUSED;
        } else {
            state = ActiveApplicationStream.State.ACTIVE;
        }
        updateActiveApplicationStream(new ActiveApplicationStream(state, ModelApplicationStream.Companion.decodeStreamKey(str)));
        this.streamSpectators.put(str, list);
        this.streamViewerTracker.onStreamUpdated(str, list.size());
        markChanged();
    }

    public static /* synthetic */ void handleVoiceStateUpdate$default(StoreApplicationStreaming storeApplicationStreaming, VoiceState voiceState, long j, int i, Object obj) {
        if ((i & 2) != 0) {
            j = voiceState.c();
        }
        storeApplicationStreaming.handleVoiceStateUpdate(voiceState, j);
    }

    public final void stopStreamInternal(String str) {
        this.storeStream.handleStreamDelete(new StreamDelete(str, StreamDelete.Reason.USER_REQUESTED, false), true);
    }

    public static /* synthetic */ void targetStream$default(StoreApplicationStreaming storeApplicationStreaming, String str, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        storeApplicationStreaming.targetStream(str, z2);
    }

    @StoreThread
    private final void updateActiveApplicationStream(ActiveApplicationStream activeApplicationStream) {
        boolean isScreenSharing = isScreenSharing(this.activeApplicationStream);
        boolean isScreenSharing2 = isScreenSharing(activeApplicationStream);
        if (isScreenSharing != isScreenSharing2) {
            this.storeStream.handleIsScreenSharingChanged(isScreenSharing2);
        }
        this.activeApplicationStream = activeApplicationStream;
    }

    public final void createStream(long j, Long l, String str) {
        this.dispatcher.schedule(new StoreApplicationStreaming$createStream$1(this, j, l, str));
    }

    @StoreThread
    public final String getActiveApplicationStreamKeyInternal$app_productionGoogleRelease() {
        ModelApplicationStream stream;
        ActiveApplicationStream activeApplicationStream = this.activeApplicationStream;
        if (activeApplicationStream == null || (stream = activeApplicationStream.getStream()) == null) {
            return null;
        }
        return stream.getEncodedStreamKey();
    }

    @StoreThread
    public final Integer getMaxViewersForStream(long j, long j2, Long l) {
        ModelApplicationStream modelApplicationStream;
        if (l != null) {
            modelApplicationStream = new ModelApplicationStream.GuildStream(l.longValue(), j2, j);
        } else {
            modelApplicationStream = new ModelApplicationStream.CallStream(j2, j);
        }
        return this.streamViewerTracker.getMaxViewers(modelApplicationStream.getEncodedStreamKey());
    }

    public final Map<Long, ModelApplicationStream> getStreamsByUser() {
        return this.streamsByUserSnapshot;
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        addBreadCrumb("Connection open, clearing streams by user.");
        this.streamsByUser.clear();
        this.streamViewerTracker.clear();
        ActiveApplicationStream activeApplicationStream = this.activeApplicationStream;
        if (!(activeApplicationStream == null || activeApplicationStream.getState() == ActiveApplicationStream.State.ENDED)) {
            targetStream(activeApplicationStream.getStream().getEncodedStreamKey(), true);
        }
        List<Guild> guilds = modelPayload.getGuilds();
        m.checkNotNullExpressionValue(guilds, "payload.guilds");
        for (Guild guild : guilds) {
            List<VoiceState> R = guild.R();
            if (R != null) {
                for (VoiceState voiceState : R) {
                    handleVoiceStateUpdate(voiceState, guild.r());
                }
            }
        }
        markChanged();
    }

    @StoreThread
    public final void handleStreamCreate(StreamCreateOrUpdate streamCreateOrUpdate) {
        m.checkNotNullParameter(streamCreateOrUpdate, "streamCreate");
        this.streamViewerTracker.remove(streamCreateOrUpdate.getStreamKey());
        handleStreamCreateOrUpdate(streamCreateOrUpdate.getStreamKey(), streamCreateOrUpdate.getPaused(), streamCreateOrUpdate.getViewerIds());
    }

    @StoreThread
    public final void handleStreamCreateRequest(String str) {
        m.checkNotNullParameter(str, "streamKey");
        updateActiveApplicationStream(new ActiveApplicationStream(ActiveApplicationStream.State.CONNECTING, ModelApplicationStream.Companion.decodeStreamKey(str)));
        markChanged();
    }

    @StoreThread
    public final void handleStreamDelete(StreamDelete streamDelete) {
        ModelApplicationStream modelApplicationStream;
        ActiveApplicationStream activeApplicationStream;
        m.checkNotNullParameter(streamDelete, "streamDelete");
        this.streamSpectators.remove(streamDelete.getStreamKey());
        if (streamDelete.getReason() == StreamDelete.Reason.STREAM_FULL) {
            updateActiveApplicationStream(new ActiveApplicationStream(ActiveApplicationStream.State.DENIED_FULL, ModelApplicationStream.Companion.decodeStreamKey(streamDelete.getStreamKey())));
            markChanged();
            return;
        }
        ActiveApplicationStream activeApplicationStream2 = this.activeApplicationStream;
        if (activeApplicationStream2 == null || (modelApplicationStream = activeApplicationStream2.getStream()) == null) {
            modelApplicationStream = this.targetStream;
        }
        ActiveApplicationStream activeApplicationStream3 = null;
        if (m.areEqual(modelApplicationStream != null ? modelApplicationStream.getEncodedStreamKey() : null, streamDelete.getStreamKey())) {
            if (streamDelete.getUnavailable()) {
                ActiveApplicationStream activeApplicationStream4 = this.activeApplicationStream;
                if (activeApplicationStream4 != null) {
                    activeApplicationStream3 = ActiveApplicationStream.copy$default(activeApplicationStream4, ActiveApplicationStream.State.RECONNECTING, null, 2, null);
                }
            } else if (!(streamDelete.getReason() == StreamDelete.Reason.USER_REQUESTED || (activeApplicationStream = this.activeApplicationStream) == null)) {
                activeApplicationStream3 = ActiveApplicationStream.copy$default(activeApplicationStream, ActiveApplicationStream.State.ENDED, null, 2, null);
            }
            updateActiveApplicationStream(activeApplicationStream3);
            markChanged();
        }
    }

    @StoreThread
    public final void handleStreamTargeted(String str) {
        m.checkNotNullParameter(str, "streamKey");
        ModelApplicationStream decodeStreamKey = ModelApplicationStream.Companion.decodeStreamKey(str);
        ActiveApplicationStream activeApplicationStream = this.activeApplicationStream;
        if (m.areEqual(decodeStreamKey, activeApplicationStream != null ? activeApplicationStream.getStream() : null) && activeApplicationStream.getState().isStreamActive()) {
            return;
        }
        if (this.voiceChannelSelectedStore.getSelectedVoiceChannelId() == decodeStreamKey.getChannelId()) {
            this.storeStream.streamWatch(decodeStreamKey.getEncodedStreamKey());
            this.targetStream = null;
            return;
        }
        this.targetStream = decodeStreamKey;
    }

    @StoreThread
    public final void handleStreamUpdate(StreamCreateOrUpdate streamCreateOrUpdate) {
        m.checkNotNullParameter(streamCreateOrUpdate, "streamUpdate");
        handleStreamCreateOrUpdate(streamCreateOrUpdate.getStreamKey(), streamCreateOrUpdate.getPaused(), streamCreateOrUpdate.getViewerIds());
    }

    @StoreThread
    public final void handleStreamWatch(String str) {
        m.checkNotNullParameter(str, "streamKey");
        updateActiveApplicationStream(new ActiveApplicationStream(ActiveApplicationStream.State.CONNECTING, ModelApplicationStream.Companion.decodeStreamKey(str)));
        markChanged();
    }

    @StoreThread
    public final void handleVoiceChannelSelected(long j) {
        ModelApplicationStream stream;
        ModelApplicationStream modelApplicationStream = this.targetStream;
        if (modelApplicationStream == null || j != modelApplicationStream.getChannelId()) {
            ActiveApplicationStream activeApplicationStream = this.activeApplicationStream;
            if (activeApplicationStream == null || (stream = activeApplicationStream.getStream()) == null || stream.getChannelId() != j) {
                updateActiveApplicationStream(null);
                this.targetStream = null;
                markChanged();
                return;
            }
            return;
        }
        StoreStream storeStream = this.storeStream;
        ModelApplicationStream modelApplicationStream2 = this.targetStream;
        m.checkNotNull(modelApplicationStream2);
        storeStream.streamWatch(modelApplicationStream2.getEncodedStreamKey());
        this.targetStream = null;
    }

    @StoreThread
    public final void handleVoiceStateUpdate(VoiceState voiceState, long j) {
        m.checkNotNullParameter(voiceState, "voiceState");
        addBreadCrumb("Handling a voice state update for " + voiceState.m());
        Long a = voiceState.a();
        long m = voiceState.m();
        boolean i = voiceState.i();
        boolean z2 = true;
        boolean z3 = j != 0;
        if (a == null || a.longValue() == 0) {
            z2 = false;
        }
        if (i && z3 && z2) {
            HashMap<Long, ModelApplicationStream> hashMap = this.streamsByUser;
            Long valueOf = Long.valueOf(m);
            m.checkNotNull(a);
            hashMap.put(valueOf, new ModelApplicationStream.GuildStream(j, a.longValue(), m));
            markChanged();
        } else if (i && z2) {
            HashMap<Long, ModelApplicationStream> hashMap2 = this.streamsByUser;
            Long valueOf2 = Long.valueOf(m);
            m.checkNotNull(a);
            hashMap2.put(valueOf2, new ModelApplicationStream.CallStream(a.longValue(), m));
            markChanged();
        } else if (this.streamsByUser.containsKey(Long.valueOf(m))) {
            this.streamsByUser.remove(Long.valueOf(m));
            markChanged();
        }
        ActiveApplicationStream activeApplicationStream = this.activeApplicationStream;
        if (activeApplicationStream != null && m == this.userStore.getMeInternal$app_productionGoogleRelease().getId()) {
            long channelId = activeApplicationStream.getStream().getChannelId();
            Long a2 = voiceState.a();
            if (a2 == null || channelId != a2.longValue()) {
                updateActiveApplicationStream(null);
                markChanged();
            }
        }
        ModelApplicationStream modelApplicationStream = this.streamsByUser.get(Long.valueOf(m));
        if (i && modelApplicationStream != null && activeApplicationStream != null && m.areEqual(modelApplicationStream.getEncodedStreamKey(), activeApplicationStream.getStream().getEncodedStreamKey()) && activeApplicationStream.getState() == ActiveApplicationStream.State.ENDED) {
            handleStreamTargeted(modelApplicationStream.getEncodedStreamKey());
        }
    }

    @StoreThread
    public final boolean isScreenSharing() {
        return isScreenSharing(this.activeApplicationStream);
    }

    @StoreThread
    public final boolean isUserStreaming(long j) {
        addBreadCrumb("Asking if " + j + " is streaming");
        return this.streamsByUser.containsKey(Long.valueOf(j));
    }

    public final Observable<ActiveApplicationStream> observeActiveStream() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreApplicationStreaming$observeActiveStream$1(this), 14, null);
    }

    public final Observable<Map<String, List<Long>>> observeStreamSpectators() {
        Observable<Map<String, List<Long>>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreApplicationStreaming$observeStreamSpectators$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck\n        …  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Map<Long, ModelApplicationStream>> observeStreamsByUser() {
        Observable<Map<Long, ModelApplicationStream>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreApplicationStreaming$observeStreamsByUser$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck\n        …  .distinctUntilChanged()");
        return q;
    }

    public final Observable<Map<Long, ModelApplicationStream>> observeStreamsForGuild(long j) {
        Observable<Map<Long, ModelApplicationStream>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreApplicationStreaming$observeStreamsForGuild$1(this, j), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck\n        …  .distinctUntilChanged()");
        return q;
    }

    public final Observable<ModelApplicationStream> observeStreamsForUser(long j) {
        Observable<ModelApplicationStream> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreApplicationStreaming$observeStreamsForUser$1(this, j), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck\n        …  .distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        super.snapshotData();
        StringBuilder R = a.R("Snapshotting 'streamsByUser' of size, ");
        R.append(this.streamsByUser.size());
        addBreadCrumb(R.toString());
        try {
            this.streamsByUserSnapshot = new HashMap(this.streamsByUser);
            this.streamSpectatorsSnapshot = new HashMap(this.streamSpectators);
        } catch (Exception e) {
            dumpBreadcrumbs();
            throw e;
        }
    }

    public final void stopStream(String str) {
        m.checkNotNullParameter(str, "streamKey");
        this.dispatcher.schedule(new StoreApplicationStreaming$stopStream$1(this, str));
    }

    public final void targetStream(String str, boolean z2) {
        m.checkNotNullParameter(str, "streamKey");
        this.dispatcher.schedule(new StoreApplicationStreaming$targetStream$1(this, str, z2));
    }

    public StoreApplicationStreaming(StoreStream storeStream, Dispatcher dispatcher, StoreUser storeUser, StoreVoiceChannelSelected storeVoiceChannelSelected, StoreRtcConnection storeRtcConnection, ObservationDeck observationDeck) {
        m.checkNotNullParameter(storeStream, "storeStream");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(storeUser, "userStore");
        m.checkNotNullParameter(storeVoiceChannelSelected, "voiceChannelSelectedStore");
        m.checkNotNullParameter(storeRtcConnection, "rtcConnectionStore");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.storeStream = storeStream;
        this.dispatcher = dispatcher;
        this.userStore = storeUser;
        this.voiceChannelSelectedStore = storeVoiceChannelSelected;
        this.rtcConnectionStore = storeRtcConnection;
        this.observationDeck = observationDeck;
        this.breadCrumbs = new ArrayDeque<>();
        this.streamsByUser = new HashMap<>();
        this.streamsByUserSnapshot = h0.emptyMap();
        this.streamSpectators = new HashMap<>();
        this.streamSpectatorsSnapshot = h0.emptyMap();
        this.streamViewerTracker = new StreamViewerTracker();
        addBreadCrumb("Initializing the store.");
    }

    private final boolean isScreenSharing(ActiveApplicationStream activeApplicationStream) {
        return activeApplicationStream != null && activeApplicationStream.getStream().getOwnerId() == this.userStore.getMeInternal$app_productionGoogleRelease().getId();
    }
}
