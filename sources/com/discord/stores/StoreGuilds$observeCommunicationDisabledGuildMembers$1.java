package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.models.member.GuildMember;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import rx.functions.Func2;
/* compiled from: StoreGuilds.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0003\u0010\r\u001a\u001e\u0012\b\u0012\u00060\u0007j\u0002`\b \u0003*\u000e\u0012\b\u0012\u00060\u0007j\u0002`\b\u0018\u00010\n0\n2\"\u0010\u0004\u001a\u001e\u0012\b\u0012\u00060\u0001j\u0002`\u0002 \u0003*\u000e\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0018\u00010\u00000\u00002V\u0010\t\u001aR\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0001j\u0002`\u0006\u0012\b\u0012\u00060\u0007j\u0002`\b0\u0005 \u0003*(\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0001j\u0002`\u0006\u0012\b\u0012\u00060\u0007j\u0002`\b0\u0005\u0018\u00010\u00050\u0005H\n¢\u0006\u0004\b\u000b\u0010\f"}, d2 = {"", "", "Lcom/discord/primitives/GuildId;", "kotlin.jvm.PlatformType", "guildIds", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/member/GuildMember;", "Lcom/discord/stores/ClientGuildMember;", "members", "", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Set;Ljava/util/Map;)Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuilds$observeCommunicationDisabledGuildMembers$1<T1, T2, R> implements Func2<Set<? extends Long>, Map<Long, ? extends Map<Long, ? extends GuildMember>>, List<? extends GuildMember>> {
    public static final StoreGuilds$observeCommunicationDisabledGuildMembers$1 INSTANCE = new StoreGuilds$observeCommunicationDisabledGuildMembers$1();

    @Override // rx.functions.Func2
    public /* bridge */ /* synthetic */ List<? extends GuildMember> call(Set<? extends Long> set, Map<Long, ? extends Map<Long, ? extends GuildMember>> map) {
        return call2((Set<Long>) set, (Map<Long, ? extends Map<Long, GuildMember>>) map);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final List<GuildMember> call2(Set<Long> set, Map<Long, ? extends Map<Long, GuildMember>> map) {
        ArrayList arrayList;
        Collection<GuildMember> values;
        m.checkNotNullExpressionValue(set, "guildIds");
        ArrayList arrayList2 = new ArrayList();
        for (Number number : set) {
            Map<Long, GuildMember> map2 = map.get(Long.valueOf(number.longValue()));
            if (map2 == null || (values = map2.values()) == null) {
                arrayList = null;
            } else {
                arrayList = new ArrayList();
                for (Object obj : values) {
                    if (((GuildMember) obj).isCommunicationDisabled()) {
                        arrayList.add(obj);
                    }
                }
            }
            if (arrayList != null) {
                arrayList2.add(arrayList);
            }
        }
        return o.flatten(arrayList2);
    }
}
