package com.discord.stores;

import com.discord.rtcconnection.audio.DiscordAudioManager;
import kotlin.Metadata;
/* compiled from: StoreAudioManagerV2.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "run", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAudioManagerV2$handleRtcConnectionState$2 implements Runnable {
    public static final StoreAudioManagerV2$handleRtcConnectionState$2 INSTANCE = new StoreAudioManagerV2$handleRtcConnectionState$2();

    @Override // java.lang.Runnable
    public final void run() {
        DiscordAudioManager discordAudioManager = DiscordAudioManager.d;
        DiscordAudioManager.d().h(false);
    }
}
