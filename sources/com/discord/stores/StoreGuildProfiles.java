package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.guild.preview.GuildPreview;
import com.discord.stores.updates.ObservationDeck;
import d0.z.d.m;
import java.util.HashMap;
import java.util.Map;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: StoreGuildProfiles.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010%\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0002\"#B\u0017\u0012\u0006\u0010\u0019\u001a\u00020\u0018\u0012\u0006\u0010\u001c\u001a\u00020\u001b¢\u0006\u0004\b \u0010!J\u001b\u0010\u0006\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u001b\u0010\b\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0003¢\u0006\u0004\b\b\u0010\u0007J\u0017\u0010\u000b\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\tH\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u001b\u0010\r\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0003¢\u0006\u0004\b\r\u0010\u0007J\u001b\u0010\u000e\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0003¢\u0006\u0004\b\u000e\u0010\u0007J!\u0010\u0011\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00100\u000f2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u000f\u0010\u0013\u001a\u00020\u0005H\u0017¢\u0006\u0004\b\u0013\u0010\u0014R&\u0010\u0016\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00100\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0019\u001a\u00020\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001c\u001a\u00020\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR&\u0010\u001f\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00100\u001e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001f\u0010\u0017¨\u0006$"}, d2 = {"Lcom/discord/stores/StoreGuildProfiles;", "Lcom/discord/stores/StoreV2;", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "fetchGuildProfile", "(J)V", "fetchIfNonexisting", "Lcom/discord/api/guild/preview/GuildPreview;", "guildPreview", "handleGuildProfileFetchSuccess", "(Lcom/discord/api/guild/preview/GuildPreview;)V", "handleGuildProfileFetchStart", "handleGuildProfileFetchFailed", "Lrx/Observable;", "Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;", "observeGuildProfile", "(J)Lrx/Observable;", "snapshotData", "()V", "", "guildProfilesState", "Ljava/util/Map;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "", "guildProfilesStateSnapshot", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V", "FetchStates", "GuildProfileData", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildProfiles extends StoreV2 {
    private final Dispatcher dispatcher;
    private final Map<Long, GuildProfileData> guildProfilesState = new HashMap();
    private Map<Long, GuildProfileData> guildProfilesStateSnapshot = new HashMap();
    private final ObservationDeck observationDeck;

    /* compiled from: StoreGuildProfiles.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/stores/StoreGuildProfiles$FetchStates;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "FETCHING", "FAILED", "SUCCEEDED", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum FetchStates {
        FETCHING,
        FAILED,
        SUCCEEDED
    }

    /* compiled from: StoreGuildProfiles.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J&\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u001b\u0010\t\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0016\u001a\u0004\b\u0017\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0018\u001a\u0004\b\u0019\u0010\u0004¨\u0006\u001c"}, d2 = {"Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;", "", "Lcom/discord/stores/StoreGuildProfiles$FetchStates;", "component1", "()Lcom/discord/stores/StoreGuildProfiles$FetchStates;", "Lcom/discord/api/guild/preview/GuildPreview;", "component2", "()Lcom/discord/api/guild/preview/GuildPreview;", "fetchState", "data", "copy", "(Lcom/discord/stores/StoreGuildProfiles$FetchStates;Lcom/discord/api/guild/preview/GuildPreview;)Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/guild/preview/GuildPreview;", "getData", "Lcom/discord/stores/StoreGuildProfiles$FetchStates;", "getFetchState", HookHelper.constructorName, "(Lcom/discord/stores/StoreGuildProfiles$FetchStates;Lcom/discord/api/guild/preview/GuildPreview;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class GuildProfileData {
        private final GuildPreview data;
        private final FetchStates fetchState;

        public GuildProfileData(FetchStates fetchStates, GuildPreview guildPreview) {
            m.checkNotNullParameter(fetchStates, "fetchState");
            this.fetchState = fetchStates;
            this.data = guildPreview;
        }

        public static /* synthetic */ GuildProfileData copy$default(GuildProfileData guildProfileData, FetchStates fetchStates, GuildPreview guildPreview, int i, Object obj) {
            if ((i & 1) != 0) {
                fetchStates = guildProfileData.fetchState;
            }
            if ((i & 2) != 0) {
                guildPreview = guildProfileData.data;
            }
            return guildProfileData.copy(fetchStates, guildPreview);
        }

        public final FetchStates component1() {
            return this.fetchState;
        }

        public final GuildPreview component2() {
            return this.data;
        }

        public final GuildProfileData copy(FetchStates fetchStates, GuildPreview guildPreview) {
            m.checkNotNullParameter(fetchStates, "fetchState");
            return new GuildProfileData(fetchStates, guildPreview);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof GuildProfileData)) {
                return false;
            }
            GuildProfileData guildProfileData = (GuildProfileData) obj;
            return m.areEqual(this.fetchState, guildProfileData.fetchState) && m.areEqual(this.data, guildProfileData.data);
        }

        public final GuildPreview getData() {
            return this.data;
        }

        public final FetchStates getFetchState() {
            return this.fetchState;
        }

        public int hashCode() {
            FetchStates fetchStates = this.fetchState;
            int i = 0;
            int hashCode = (fetchStates != null ? fetchStates.hashCode() : 0) * 31;
            GuildPreview guildPreview = this.data;
            if (guildPreview != null) {
                i = guildPreview.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("GuildProfileData(fetchState=");
            R.append(this.fetchState);
            R.append(", data=");
            R.append(this.data);
            R.append(")");
            return R.toString();
        }
    }

    public StoreGuildProfiles(Dispatcher dispatcher, ObservationDeck observationDeck) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
    }

    @StoreThread
    private final void fetchGuildProfile(long j) {
        this.dispatcher.schedule(new StoreGuildProfiles$fetchGuildProfile$1(this, j));
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void fetchIfNonexisting(long j) {
        GuildProfileData guildProfileData = this.guildProfilesState.get(Long.valueOf(j));
        if (guildProfileData == null || guildProfileData.getFetchState() == FetchStates.FAILED) {
            fetchGuildProfile(j);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleGuildProfileFetchFailed(long j) {
        this.guildProfilesState.put(Long.valueOf(j), new GuildProfileData(FetchStates.FAILED, null));
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleGuildProfileFetchStart(long j) {
        this.guildProfilesState.put(Long.valueOf(j), new GuildProfileData(FetchStates.FETCHING, null));
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleGuildProfileFetchSuccess(GuildPreview guildPreview) {
        this.guildProfilesState.put(Long.valueOf(guildPreview.h()), new GuildProfileData(FetchStates.SUCCEEDED, guildPreview));
        markChanged();
    }

    public final Observable<GuildProfileData> observeGuildProfile(long j) {
        this.dispatcher.schedule(new StoreGuildProfiles$observeGuildProfile$1(this, j));
        Observable<GuildProfileData> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreGuildProfiles$observeGuildProfile$2(this, j), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck\n        …  .distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        this.guildProfilesStateSnapshot = new HashMap(this.guildProfilesState);
    }
}
