package com.discord.stores;

import com.discord.models.domain.ModelMemberVerificationForm;
import com.discord.stores.StoreGuildMemberVerificationForm;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreGuildMemberVerificationForm.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildMemberVerificationForm$fetchMemberVerificationForm$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ StoreGuildMemberVerificationForm this$0;

    /* compiled from: StoreGuildMemberVerificationForm.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/ModelMemberVerificationForm;", "memberVerificationForm", "", "invoke", "(Lcom/discord/models/domain/ModelMemberVerificationForm;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreGuildMemberVerificationForm$fetchMemberVerificationForm$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<ModelMemberVerificationForm, Unit> {

        /* compiled from: StoreGuildMemberVerificationForm.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreGuildMemberVerificationForm$fetchMemberVerificationForm$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C02031 extends o implements Function0<Unit> {
            public final /* synthetic */ ModelMemberVerificationForm $memberVerificationForm;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C02031(ModelMemberVerificationForm modelMemberVerificationForm) {
                super(0);
                this.$memberVerificationForm = modelMemberVerificationForm;
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                StoreGuildMemberVerificationForm$fetchMemberVerificationForm$1 storeGuildMemberVerificationForm$fetchMemberVerificationForm$1 = StoreGuildMemberVerificationForm$fetchMemberVerificationForm$1.this;
                storeGuildMemberVerificationForm$fetchMemberVerificationForm$1.this$0.handleMemberVerificationFormFetchSuccess(storeGuildMemberVerificationForm$fetchMemberVerificationForm$1.$guildId, this.$memberVerificationForm);
            }
        }

        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(ModelMemberVerificationForm modelMemberVerificationForm) {
            invoke2(modelMemberVerificationForm);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(ModelMemberVerificationForm modelMemberVerificationForm) {
            Dispatcher dispatcher;
            m.checkNotNullParameter(modelMemberVerificationForm, "memberVerificationForm");
            dispatcher = StoreGuildMemberVerificationForm$fetchMemberVerificationForm$1.this.this$0.dispatcher;
            dispatcher.schedule(new C02031(modelMemberVerificationForm));
        }
    }

    /* compiled from: StoreGuildMemberVerificationForm.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreGuildMemberVerificationForm$fetchMemberVerificationForm$1$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2 extends o implements Function1<Error, Unit> {

        /* compiled from: StoreGuildMemberVerificationForm.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreGuildMemberVerificationForm$fetchMemberVerificationForm$1$2$1  reason: invalid class name */
        /* loaded from: classes.dex */
        public static final class AnonymousClass1 extends o implements Function0<Unit> {
            public AnonymousClass1() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                StoreGuildMemberVerificationForm$fetchMemberVerificationForm$1 storeGuildMemberVerificationForm$fetchMemberVerificationForm$1 = StoreGuildMemberVerificationForm$fetchMemberVerificationForm$1.this;
                storeGuildMemberVerificationForm$fetchMemberVerificationForm$1.this$0.handleMemberVerificationFormFetchFailed(storeGuildMemberVerificationForm$fetchMemberVerificationForm$1.$guildId);
            }
        }

        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Error error) {
            invoke2(error);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Error error) {
            Dispatcher dispatcher;
            m.checkNotNullParameter(error, "it");
            dispatcher = StoreGuildMemberVerificationForm$fetchMemberVerificationForm$1.this.this$0.dispatcher;
            dispatcher.schedule(new AnonymousClass1());
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGuildMemberVerificationForm$fetchMemberVerificationForm$1(StoreGuildMemberVerificationForm storeGuildMemberVerificationForm, long j) {
        super(0);
        this.this$0 = storeGuildMemberVerificationForm;
        this.$guildId = j;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        HashMap hashMap;
        RestAPI restAPI;
        hashMap = this.this$0.memberVerificationFormState;
        StoreGuildMemberVerificationForm.MemberVerificationFormData memberVerificationFormData = (StoreGuildMemberVerificationForm.MemberVerificationFormData) hashMap.get(Long.valueOf(this.$guildId));
        if ((memberVerificationFormData != null ? memberVerificationFormData.getFetchState() : null) != StoreGuildMemberVerificationForm.FetchStates.FETCHING) {
            this.this$0.handleMemberVerificationFormFetchStart(this.$guildId);
            restAPI = this.this$0.restAPI;
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(restAPI.getGuildMemberVerificationForm(this.$guildId), false, 1, null), this.this$0.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new AnonymousClass2(), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
        }
    }
}
