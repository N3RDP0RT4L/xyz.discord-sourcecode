package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.SharedPreferences;
import com.discord.BuildConfig;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subjects.SerializedSubject;
/* compiled from: StoreClientVersion.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u0018\u0010\u0019J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0013\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\r\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000bH\u0016¢\u0006\u0004\b\r\u0010\u000eR\u0016\u0010\u000f\u001a\u00020\u00028\u0002@\u0002X\u0082D¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010R2\u0010\u0013\u001a\u001e\u0012\f\u0012\n \u0012*\u0004\u0018\u00010\b0\b\u0012\f\u0012\n \u0012*\u0004\u0018\u00010\b0\b0\u00118\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0003\u0010\u0010R\u0016\u0010\u0016\u001a\u00020\u00158\u0002@\u0002X\u0082D¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017¨\u0006\u001a"}, d2 = {"Lcom/discord/stores/StoreClientVersion;", "Lcom/discord/stores/Store;", "", "clientMinVersion", "", "setClientMinVersion", "(I)V", "Lrx/Observable;", "", "getClientOutdated", "()Lrx/Observable;", "Landroid/content/Context;", "context", "init", "(Landroid/content/Context;)V", "clientVersion", "I", "Lrx/subjects/SerializedSubject;", "kotlin.jvm.PlatformType", "clientOutdatedSubject", "Lrx/subjects/SerializedSubject;", "", "clientMinVersionKey", "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreClientVersion extends Store {
    private int clientMinVersion;
    private final int clientVersion = BuildConfig.VERSION_CODE;
    private final String clientMinVersionKey = "CLIENT_OUTDATED_KEY";
    private final SerializedSubject<Boolean, Boolean> clientOutdatedSubject = new SerializedSubject<>(BehaviorSubject.l0(Boolean.FALSE));

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void setClientMinVersion(int i) {
        if (this.clientMinVersion != i) {
            this.clientMinVersion = i;
            this.clientOutdatedSubject.k.onNext(Boolean.valueOf(this.clientVersion < i));
            SharedPreferences.Editor edit = getPrefs().edit();
            m.checkNotNullExpressionValue(edit, "editor");
            edit.putInt(this.clientMinVersionKey, i);
            edit.apply();
        }
    }

    public final Observable<Boolean> getClientOutdated() {
        Observable<Boolean> q = ObservableExtensionsKt.computationLatest(this.clientOutdatedSubject).q();
        m.checkNotNullExpressionValue(q, "clientOutdatedSubject\n  …  .distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.Store
    public synchronized void init(Context context) {
        m.checkNotNullParameter(context, "context");
        super.init(context);
        int i = getPrefs().getInt(this.clientMinVersionKey, 0);
        this.clientMinVersion = i;
        setClientMinVersion(i);
        Observable<R> z2 = Observable.D(0L, 1L, TimeUnit.HOURS).z(StoreClientVersion$init$1.INSTANCE);
        m.checkNotNullExpressionValue(z2, "Observable\n        .inte…ClientVersion()\n        }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.computationBuffered(z2), getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreClientVersion$init$2(this));
    }
}
