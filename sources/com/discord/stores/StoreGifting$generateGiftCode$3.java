package com.discord.stores;

import com.discord.models.domain.ModelGift;
import com.discord.stores.StoreGifting;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreGifting.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/ModelGift;", "newGift", "", "invoke", "(Lcom/discord/models/domain/ModelGift;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGifting$generateGiftCode$3 extends o implements Function1<ModelGift, Unit> {
    public final /* synthetic */ String $comboId;
    public final /* synthetic */ Function1 $onSuccess;
    public final /* synthetic */ StoreGifting this$0;

    /* compiled from: StoreGifting.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreGifting$generateGiftCode$3$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ ModelGift $newGift;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(ModelGift modelGift) {
            super(0);
            this.$newGift = modelGift;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            StoreGifting$generateGiftCode$3 storeGifting$generateGiftCode$3 = StoreGifting$generateGiftCode$3.this;
            storeGifting$generateGiftCode$3.this$0.removeGiftCode(storeGifting$generateGiftCode$3.$comboId);
            StoreGifting$generateGiftCode$3.this.this$0.setGifts(this.$newGift.getCode(), new StoreGifting.GiftState.Resolved(this.$newGift));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreGifting$generateGiftCode$3(StoreGifting storeGifting, String str, Function1 function1) {
        super(1);
        this.this$0 = storeGifting;
        this.$comboId = str;
        this.$onSuccess = function1;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ModelGift modelGift) {
        invoke2(modelGift);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ModelGift modelGift) {
        m.checkNotNullParameter(modelGift, "newGift");
        this.this$0.getDispatcher().schedule(new AnonymousClass1(modelGift));
        Function1 function1 = this.$onSuccess;
        if (function1 != null) {
            Unit unit = (Unit) function1.invoke(modelGift);
        }
    }
}
