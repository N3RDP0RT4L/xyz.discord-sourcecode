package com.discord.stores;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.Context;
import android.content.SharedPreferences;
import androidx.media.AudioAttributesCompat;
import b.d.b.a.a;
import b.i.d.e;
import com.discord.api.commands.ApplicationCommandData;
import com.discord.api.interaction.Interaction;
import com.discord.api.interaction.InteractionStateUpdate;
import com.discord.api.user.User;
import com.discord.models.commands.Application;
import com.discord.models.commands.ApplicationCommandLocalSendData;
import com.discord.models.domain.ModelPayload;
import com.discord.models.domain.NonceGenerator;
import com.discord.models.message.Message;
import com.discord.nullserializable.NullSerializable;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.cache.SharedPreferencesProvider;
import com.discord.utilities.error.Error;
import com.discord.utilities.message.LocalMessageCreatorsKt;
import com.discord.utilities.messagesend.MessageResult;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.Clock;
import com.discord.utilities.user.UserUtils;
import com.google.gson.Gson;
import com.lytefast.flexinput.model.Attachment;
import d0.g0.t;
import d0.o;
import d0.t.g0;
import d0.t.h0;
import d0.z.d.m;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreApplicationInteractions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Ì\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010$\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 ¶\u00012\u00020\u0001:\b¶\u0001·\u0001¸\u0001¹\u0001Bu\u0012\b\u0010\u009a\u0001\u001a\u00030\u0099\u0001\u0012\b\u0010²\u0001\u001a\u00030±\u0001\u0012\b\u0010\u0092\u0001\u001a\u00030\u0091\u0001\u0012\b\u0010 \u0001\u001a\u00030\u009f\u0001\u0012\b\u0010\u0089\u0001\u001a\u00030\u0088\u0001\u0012\b\u0010\u0081\u0001\u001a\u00030\u0080\u0001\u0012\n\b\u0002\u0010©\u0001\u001a\u00030¨\u0001\u0012\n\b\u0002\u0010\u009d\u0001\u001a\u00030\u009c\u0001\u0012\n\b\u0002\u0010\u008c\u0001\u001a\u00030\u008b\u0001\u0012\n\b\u0002\u0010\u008f\u0001\u001a\u00030\u008e\u0001¢\u0006\u0006\b´\u0001\u0010µ\u0001J/\u0010\u000b\u001a\u00020\n2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u00062\u0006\u0010\t\u001a\u00020\bH\u0003¢\u0006\u0004\b\u000b\u0010\fJW\u0010\u0017\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\r2\u0014\b\u0002\u0010\u0011\u001a\u000e\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u0010\u0018\u00010\u000f2\u0010\b\u0002\u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010\u00122\u0016\b\u0002\u0010\u0016\u001a\u0010\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\n\u0018\u00010\u0014H\u0003¢\u0006\u0004\b\u0017\u0010\u0018J3\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001a0\u00192\u0006\u0010\u000e\u001a\u00020\r2\u0014\b\u0002\u0010\u0011\u001a\u000e\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u0010\u0018\u00010\u000fH\u0002¢\u0006\u0004\b\u001b\u0010\u001cJI\u0010\u001e\u001a\u00020\n2\u0006\u0010\u001d\u001a\u00020\u001a2\u0006\u0010\u000e\u001a\u00020\r2\u0010\b\u0002\u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010\u00122\u0016\b\u0002\u0010\u0016\u001a\u0010\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\n\u0018\u00010\u0014H\u0003¢\u0006\u0004\b\u001e\u0010\u001fJ\u000f\u0010 \u001a\u00020\nH\u0003¢\u0006\u0004\b \u0010!J#\u0010#\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\r2\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\u0002H\u0003¢\u0006\u0004\b#\u0010$J#\u0010%\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\r2\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\u0002H\u0003¢\u0006\u0004\b%\u0010$J+\u0010*\u001a\u00020)2\u0006\u0010\u000e\u001a\u00020\r2\b\b\u0002\u0010'\u001a\u00020&2\b\b\u0002\u0010(\u001a\u00020&H\u0002¢\u0006\u0004\b*\u0010+J\u0017\u0010.\u001a\u00020\n2\u0006\u0010-\u001a\u00020,H\u0002¢\u0006\u0004\b.\u0010/J\u0017\u00100\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\rH\u0003¢\u0006\u0004\b0\u00101J\u0017\u00104\u001a\u00020\n2\u0006\u00103\u001a\u000202H\u0003¢\u0006\u0004\b4\u00105J\u000f\u00106\u001a\u00020\nH\u0003¢\u0006\u0004\b6\u0010!J\u0017\u00107\u001a\u00020\n2\u0006\u0010\"\u001a\u00020\u0002H\u0003¢\u0006\u0004\b7\u00108J\u001f\u0010;\u001a\u00020\n2\u0006\u0010\"\u001a\u00020\u00022\u0006\u0010:\u001a\u000209H\u0003¢\u0006\u0004\b;\u0010<J\u0017\u0010=\u001a\u00020\n2\u0006\u0010\"\u001a\u00020\u0002H\u0003¢\u0006\u0004\b=\u00108J\u000f\u0010>\u001a\u00020\nH\u0003¢\u0006\u0004\b>\u0010!J\u000f\u0010?\u001a\u00020\nH\u0002¢\u0006\u0004\b?\u0010!J\u0013\u0010B\u001a\u00020A*\u00020@H\u0002¢\u0006\u0004\bB\u0010CJ\u0017\u0010F\u001a\u00020\n2\u0006\u0010E\u001a\u00020DH\u0017¢\u0006\u0004\bF\u0010GJ\u0017\u0010I\u001a\u00020\n2\u0006\u0010H\u001a\u00020&H\u0007¢\u0006\u0004\bI\u0010JJ\u0017\u0010M\u001a\u00020\n2\u0006\u0010L\u001a\u00020KH\u0007¢\u0006\u0004\bM\u0010NJ\u000f\u0010O\u001a\u00020\nH\u0007¢\u0006\u0004\bO\u0010!Js\u0010W\u001a\u00020\n2\n\u0010Q\u001a\u00060\u0002j\u0002`P2\u000e\u0010S\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`R2\b\u0010T\u001a\u0004\u0018\u0001022\u0006\u0010V\u001a\u00020U2\u0014\b\u0002\u0010\u0011\u001a\u000e\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u0010\u0018\u00010\u000f2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\n0\u00122\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\n0\u0014¢\u0006\u0004\bW\u0010XJ_\u0010]\u001a\u00020\n2\n\u0010Z\u001a\u00060\u0002j\u0002`Y2\u000e\u0010S\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`R2\n\u0010Q\u001a\u00060\u0002j\u0002`P2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0007\u001a\u00060\u0005j\u0002`\u00062\u0006\u0010V\u001a\u00020[2\b\u0010\\\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b]\u0010^J\u0017\u0010`\u001a\u00020\n2\u0006\u0010_\u001a\u00020)H\u0007¢\u0006\u0004\b`\u0010aJ\u0015\u0010b\u001a\u00020\n2\u0006\u0010_\u001a\u00020)¢\u0006\u0004\bb\u0010aJ\u0017\u0010e\u001a\u00020\n2\u0006\u0010d\u001a\u00020cH\u0007¢\u0006\u0004\be\u0010fJ\u0017\u0010g\u001a\u00020\n2\u0006\u0010d\u001a\u00020cH\u0007¢\u0006\u0004\bg\u0010fJ\u0017\u0010#\u001a\u00020\n2\u0006\u0010d\u001a\u00020cH\u0007¢\u0006\u0004\b#\u0010fJ\u0017\u0010i\u001a\u00020\n2\u0006\u0010_\u001a\u00020hH\u0007¢\u0006\u0004\bi\u0010jJ\u001d\u0010l\u001a\u00020\n2\f\u0010k\u001a\b\u0012\u0004\u0012\u00020)0\u000fH\u0007¢\u0006\u0004\bl\u0010mJ\u0017\u0010n\u001a\u00020\n2\u0006\u0010_\u001a\u00020hH\u0007¢\u0006\u0004\bn\u0010jJ\u000f\u0010o\u001a\u00020\nH\u0017¢\u0006\u0004\bo\u0010!J\u000f\u0010p\u001a\u00020\nH\u0007¢\u0006\u0004\bp\u0010!J\u0017\u0010r\u001a\u0004\u0018\u00010q2\u0006\u0010\"\u001a\u00020\u0002¢\u0006\u0004\br\u0010sJ\u001d\u0010t\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010q0\u00192\u0006\u0010\"\u001a\u00020\u0002¢\u0006\u0004\bt\u0010uJ-\u0010w\u001a\"\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\b0v0v¢\u0006\u0004\bw\u0010xJ3\u0010y\u001a(\u0012$\u0012\"\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\b0v0v0\u0019¢\u0006\u0004\by\u0010zJ7\u0010|\u001a\u00020\n2\u0006\u0010\"\u001a\u00020\u00022\n\u0010Q\u001a\u00060\u0002j\u0002`P2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\b\u0010{\u001a\u0004\u0018\u000102¢\u0006\u0004\b|\u0010}R\u0018\u0010~\u001a\u0004\u0018\u0001028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b~\u0010\u007fR\u001a\u0010\u0081\u0001\u001a\u00030\u0080\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0081\u0001\u0010\u0082\u0001RC\u0010\u0085\u0001\u001a\u000f\u0012\u0004\u0012\u000202\u0012\u0004\u0012\u00020\r0\u0083\u00012\u0014\u0010\u0084\u0001\u001a\u000f\u0012\u0004\u0012\u000202\u0012\u0004\u0012\u00020\r0\u0083\u00018\u0000@BX\u0080\u000e¢\u0006\u000f\n\u0006\b\u0085\u0001\u0010\u0086\u0001\u001a\u0005\b\u0087\u0001\u0010xR\u001a\u0010\u0089\u0001\u001a\u00030\u0088\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0089\u0001\u0010\u008a\u0001R\u001a\u0010\u008c\u0001\u001a\u00030\u008b\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u008c\u0001\u0010\u008d\u0001R\u001a\u0010\u008f\u0001\u001a\u00030\u008e\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u008f\u0001\u0010\u0090\u0001R\u001a\u0010\u0092\u0001\u001a\u00030\u0091\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0092\u0001\u0010\u0093\u0001R&\u0010\u0094\u0001\u001a\u000f\u0012\u0004\u0012\u000202\u0012\u0004\u0012\u00020\r0\u0083\u00018\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u0094\u0001\u0010\u0086\u0001RH\u0010\u0095\u0001\u001a\"\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\b0v0v8\u0006@\u0006X\u0086\u000e¢\u0006\u0017\n\u0006\b\u0095\u0001\u0010\u0086\u0001\u001a\u0005\b\u0096\u0001\u0010x\"\u0006\b\u0097\u0001\u0010\u0098\u0001R\u001a\u0010\u009a\u0001\u001a\u00030\u0099\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u009a\u0001\u0010\u009b\u0001R\u001a\u0010\u009d\u0001\u001a\u00030\u009c\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u009d\u0001\u0010\u009e\u0001R\u001a\u0010 \u0001\u001a\u00030\u009f\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b \u0001\u0010¡\u0001R*\u0010¢\u0001\u001a\u000f\u0012\u0004\u0012\u000202\u0012\u0004\u0012\u00020,0\u0083\u00018\u0006@\u0006¢\u0006\u000f\n\u0006\b¢\u0001\u0010\u0086\u0001\u001a\u0005\b£\u0001\u0010xR$\u0010¦\u0001\u001a\r ¥\u0001*\u0005\u0018\u00010¤\u00010¤\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b¦\u0001\u0010§\u0001R\u001a\u0010©\u0001\u001a\u00030¨\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b©\u0001\u0010ª\u0001R%\u0010«\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020q0v8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b«\u0001\u0010\u0086\u0001R5\u0010:\u001a \u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020q0¬\u0001j\u000f\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020q`\u00ad\u00018\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b:\u0010®\u0001R>\u0010¯\u0001\u001a#\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\b0v0\u0083\u00018\u0006@\u0006¢\u0006\u000f\n\u0006\b¯\u0001\u0010\u0086\u0001\u001a\u0005\b°\u0001\u0010xR\u001a\u0010²\u0001\u001a\u00030±\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b²\u0001\u0010³\u0001¨\u0006º\u0001"}, d2 = {"Lcom/discord/stores/StoreApplicationInteractions;", "Lcom/discord/stores/StoreV2;", "", "Lcom/discord/primitives/MessageId;", "messageId", "", "Lcom/discord/widgets/botuikit/ComponentIndex;", "componentIndex", "Lcom/discord/stores/StoreApplicationInteractions$InteractionSendState;", "sendState", "", "addInteractionStateToComponent", "(JILcom/discord/stores/StoreApplicationInteractions$InteractionSendState;)V", "Lcom/discord/models/commands/ApplicationCommandLocalSendData;", "localSendData", "", "Lcom/lytefast/flexinput/model/Attachment;", "attachments", "Lkotlin/Function0;", "onSuccess", "Lkotlin/Function1;", "Lcom/discord/utilities/error/Error;", "onFail", "handleSendApplicationCommandRequest", "(Lcom/discord/models/commands/ApplicationCommandLocalSendData;Ljava/util/List;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V", "Lrx/Observable;", "Lcom/discord/utilities/messagesend/MessageResult;", "sendApplicationCommandObservable", "(Lcom/discord/models/commands/ApplicationCommandLocalSendData;Ljava/util/List;)Lrx/Observable;", "result", "handleApplicationCommandResult", "(Lcom/discord/utilities/messagesend/MessageResult;Lcom/discord/models/commands/ApplicationCommandLocalSendData;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V", "markAllLocalApplicationCommandRequestsAsFailed", "()V", "interactionId", "handleInteractionFailure", "(Lcom/discord/models/commands/ApplicationCommandLocalSendData;Ljava/lang/Long;)V", "handleApplicationCommandRequestStateUpdate", "", "isLoading", "isFailed", "Lcom/discord/models/message/Message;", "buildApplicationCommandLocalMessage", "(Lcom/discord/models/commands/ApplicationCommandLocalSendData;ZZ)Lcom/discord/models/message/Message;", "Lcom/discord/stores/StoreApplicationInteractions$ComponentLocation;", "componentLocation", "handleComponentInteractionMessage", "(Lcom/discord/stores/StoreApplicationInteractions$ComponentLocation;)V", "upsertApplicationCommandSendData", "(Lcom/discord/models/commands/ApplicationCommandLocalSendData;)V", "", "nonce", "removeApplicationCommandSendData", "(Ljava/lang/String;)V", "loadCachedApplicationCommandSendDataSet", "handleInteractionDataFetchStart", "(J)V", "Lcom/discord/api/commands/ApplicationCommandData;", "interactionData", "handleInteractionDataFetchSuccess", "(JLcom/discord/api/commands/ApplicationCommandData;)V", "handleInteractionDataFetchFailure", "clearCache", "clearComponentInteractionSendSuccessAndFailures", "Lcom/discord/models/commands/Application;", "Lcom/discord/api/user/User;", "toUser", "(Lcom/discord/models/commands/Application;)Lcom/discord/api/user/User;", "Landroid/content/Context;", "context", "init", "(Landroid/content/Context;)V", "connectionReady", "handleConnectionReady", "(Z)V", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "handleChannelSelected", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/primitives/GuildId;", "guildId", "version", "Lcom/discord/widgets/chat/input/models/ApplicationCommandData;", "data", "sendApplicationCommand", "(JLjava/lang/Long;Ljava/lang/String;Lcom/discord/widgets/chat/input/models/ApplicationCommandData;Ljava/util/List;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V", "Lcom/discord/primitives/ApplicationId;", "applicationId", "Lcom/discord/restapi/RestAPIParams$ComponentInteractionData;", "messageFlags", "sendComponentInteraction", "(JLjava/lang/Long;JJILcom/discord/restapi/RestAPIParams$ComponentInteractionData;Ljava/lang/Long;)V", "message", "handleLocalMessageDelete", "(Lcom/discord/models/message/Message;)V", "resendApplicationCommand", "Lcom/discord/api/interaction/InteractionStateUpdate;", "interactionStateUpdate", "handleInteractionCreate", "(Lcom/discord/api/interaction/InteractionStateUpdate;)V", "handleInteractionSuccess", "Lcom/discord/api/message/Message;", "handleMessageCreate", "(Lcom/discord/api/message/Message;)V", "messagesList", "handleMessagesCreateOrLoad", "(Ljava/util/List;)V", "handleMessageUpdate", "snapshotData", "handlePreLogout", "Lcom/discord/stores/StoreApplicationInteractions$State;", "getInteractionData", "(J)Lcom/discord/stores/StoreApplicationInteractions$State;", "observeInteractionData", "(J)Lrx/Observable;", "", "getComponentInteractionData", "()Ljava/util/Map;", "observeComponentInteractionState", "()Lrx/Observable;", "messageNonce", "fetchInteractionDataIfNonExisting", "(JJJLjava/lang/String;)V", "sessionId", "Ljava/lang/String;", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "", "<set-?>", "applicationCommandLocalSendDataSet", "Ljava/util/Map;", "getApplicationCommandLocalSendDataSet$app_productionGoogleRelease", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreUser;", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "Lcom/discord/models/domain/NonceGenerator;", "nonceGenerator", "Lcom/discord/models/domain/NonceGenerator;", "Lcom/discord/stores/StoreMessages;", "storeMessages", "Lcom/discord/stores/StoreMessages;", "applicationCommandLocalSendDataSnapshot", "interactionComponentSendStateSnapshot", "getInteractionComponentSendStateSnapshot", "setInteractionComponentSendStateSnapshot", "(Ljava/util/Map;)V", "Lcom/discord/stores/StoreStream;", "storeStream", "Lcom/discord/stores/StoreStream;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/stores/StoreLocalActionComponentState;", "storeLocalActionComponentState", "Lcom/discord/stores/StoreLocalActionComponentState;", "componentInteractions", "getComponentInteractions", "Lcom/google/gson/Gson;", "kotlin.jvm.PlatformType", "gson", "Lcom/google/gson/Gson;", "Landroid/content/SharedPreferences;", "sharedPrefs", "Landroid/content/SharedPreferences;", "interactionDataSnapshot", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "Ljava/util/HashMap;", "interactionComponentSendState", "getInteractionComponentSendState", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreMessages;Lcom/discord/stores/StoreLocalActionComponentState;Lcom/discord/stores/StoreUser;Lcom/discord/utilities/time/Clock;Landroid/content/SharedPreferences;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/models/domain/NonceGenerator;)V", "Companion", "ComponentLocation", "InteractionSendState", "State", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreApplicationInteractions extends StoreV2 {
    private static final String CACHE_KEY_APPLICATION_COMMAND_SEND_DATA_SET = "CACHE_KEY_APPLICATION_COMMAND_SEND_DATA_SET";
    public static final Companion Companion = new Companion(null);
    private static final ObservationDeck.UpdateSource ComponentStateUpdate = new ObservationDeck.UpdateSource() { // from class: com.discord.stores.StoreApplicationInteractions$Companion$ComponentStateUpdate$1
    };
    public static final long TYPE_COMPONENT_INTERACTION = 3;
    private Map<String, ApplicationCommandLocalSendData> applicationCommandLocalSendDataSet;
    private Map<String, ApplicationCommandLocalSendData> applicationCommandLocalSendDataSnapshot;
    private final Clock clock;
    private final Map<String, ComponentLocation> componentInteractions;
    private final Dispatcher dispatcher;
    private final Gson gson;
    private final Map<Long, Map<Integer, InteractionSendState>> interactionComponentSendState;
    private Map<Long, ? extends Map<Integer, ? extends InteractionSendState>> interactionComponentSendStateSnapshot;
    private final HashMap<Long, State> interactionData;
    private Map<Long, ? extends State> interactionDataSnapshot;
    private final NonceGenerator nonceGenerator;
    private final ObservationDeck observationDeck;
    private final RestAPI restAPI;
    private String sessionId;
    private final SharedPreferences sharedPrefs;
    private final StoreLocalActionComponentState storeLocalActionComponentState;
    private final StoreMessages storeMessages;
    private final StoreStream storeStream;
    private final StoreUser storeUser;

    /* compiled from: StoreApplicationInteractions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u000f"}, d2 = {"Lcom/discord/stores/StoreApplicationInteractions$Companion;", "", "Lcom/discord/stores/updates/ObservationDeck$UpdateSource;", "ComponentStateUpdate", "Lcom/discord/stores/updates/ObservationDeck$UpdateSource;", "getComponentStateUpdate", "()Lcom/discord/stores/updates/ObservationDeck$UpdateSource;", "", StoreApplicationInteractions.CACHE_KEY_APPLICATION_COMMAND_SEND_DATA_SET, "Ljava/lang/String;", "", "TYPE_COMPONENT_INTERACTION", "J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public final ObservationDeck.UpdateSource getComponentStateUpdate() {
            return StoreApplicationInteractions.ComponentStateUpdate;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreApplicationInteractions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\n\u0010\n\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010\u000b\u001a\u00060\u0006j\u0002`\u0007¢\u0006\u0004\b\u001a\u0010\u001bJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\b\u001a\u00060\u0006j\u0002`\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ,\u0010\f\u001a\u00020\u00002\f\b\u0002\u0010\n\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010\u000b\u001a\u00060\u0006j\u0002`\u0007HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0011\u0010\tJ\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u001d\u0010\n\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0016\u001a\u0004\b\u0017\u0010\u0005R\u001d\u0010\u000b\u001a\u00060\u0006j\u0002`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0018\u001a\u0004\b\u0019\u0010\t¨\u0006\u001c"}, d2 = {"Lcom/discord/stores/StoreApplicationInteractions$ComponentLocation;", "", "", "Lcom/discord/primitives/MessageId;", "component1", "()J", "", "Lcom/discord/widgets/botuikit/ComponentIndex;", "component2", "()I", "messageId", "componentIndex", "copy", "(JI)Lcom/discord/stores/StoreApplicationInteractions$ComponentLocation;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getMessageId", "I", "getComponentIndex", HookHelper.constructorName, "(JI)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ComponentLocation {
        private final int componentIndex;
        private final long messageId;

        public ComponentLocation(long j, int i) {
            this.messageId = j;
            this.componentIndex = i;
        }

        public static /* synthetic */ ComponentLocation copy$default(ComponentLocation componentLocation, long j, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                j = componentLocation.messageId;
            }
            if ((i2 & 2) != 0) {
                i = componentLocation.componentIndex;
            }
            return componentLocation.copy(j, i);
        }

        public final long component1() {
            return this.messageId;
        }

        public final int component2() {
            return this.componentIndex;
        }

        public final ComponentLocation copy(long j, int i) {
            return new ComponentLocation(j, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ComponentLocation)) {
                return false;
            }
            ComponentLocation componentLocation = (ComponentLocation) obj;
            return this.messageId == componentLocation.messageId && this.componentIndex == componentLocation.componentIndex;
        }

        public final int getComponentIndex() {
            return this.componentIndex;
        }

        public final long getMessageId() {
            return this.messageId;
        }

        public int hashCode() {
            return (b.a(this.messageId) * 31) + this.componentIndex;
        }

        public String toString() {
            StringBuilder R = a.R("ComponentLocation(messageId=");
            R.append(this.messageId);
            R.append(", componentIndex=");
            return a.A(R, this.componentIndex, ")");
        }
    }

    /* compiled from: StoreApplicationInteractions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/stores/StoreApplicationInteractions$InteractionSendState;", "", HookHelper.constructorName, "()V", "Failed", "Loading", "Success", "Lcom/discord/stores/StoreApplicationInteractions$InteractionSendState$Loading;", "Lcom/discord/stores/StoreApplicationInteractions$InteractionSendState$Failed;", "Lcom/discord/stores/StoreApplicationInteractions$InteractionSendState$Success;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class InteractionSendState {

        /* compiled from: StoreApplicationInteractions.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0013\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0007\u0010\bR\u001b\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/stores/StoreApplicationInteractions$InteractionSendState$Failed;", "Lcom/discord/stores/StoreApplicationInteractions$InteractionSendState;", "", "errorMessage", "Ljava/lang/String;", "getErrorMessage", "()Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Failed extends InteractionSendState {
            private final String errorMessage;

            public Failed() {
                this(null, 1, null);
            }

            public Failed(String str) {
                super(null);
                this.errorMessage = str;
            }

            public final String getErrorMessage() {
                return this.errorMessage;
            }

            public /* synthetic */ Failed(String str, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this((i & 1) != 0 ? null : str);
            }
        }

        /* compiled from: StoreApplicationInteractions.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreApplicationInteractions$InteractionSendState$Loading;", "Lcom/discord/stores/StoreApplicationInteractions$InteractionSendState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Loading extends InteractionSendState {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        /* compiled from: StoreApplicationInteractions.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreApplicationInteractions$InteractionSendState$Success;", "Lcom/discord/stores/StoreApplicationInteractions$InteractionSendState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Success extends InteractionSendState {
            public static final Success INSTANCE = new Success();

            private Success() {
                super(null);
            }
        }

        private InteractionSendState() {
        }

        public /* synthetic */ InteractionSendState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreApplicationInteractions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/stores/StoreApplicationInteractions$State;", "", HookHelper.constructorName, "()V", "Failure", "Fetching", "Loaded", "Lcom/discord/stores/StoreApplicationInteractions$State$Fetching;", "Lcom/discord/stores/StoreApplicationInteractions$State$Loaded;", "Lcom/discord/stores/StoreApplicationInteractions$State$Failure;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class State {

        /* compiled from: StoreApplicationInteractions.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreApplicationInteractions$State$Failure;", "Lcom/discord/stores/StoreApplicationInteractions$State;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Failure extends State {
            public static final Failure INSTANCE = new Failure();

            private Failure() {
                super(null);
            }
        }

        /* compiled from: StoreApplicationInteractions.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreApplicationInteractions$State$Fetching;", "Lcom/discord/stores/StoreApplicationInteractions$State;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Fetching extends State {
            public static final Fetching INSTANCE = new Fetching();

            private Fetching() {
                super(null);
            }
        }

        /* compiled from: StoreApplicationInteractions.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/stores/StoreApplicationInteractions$State$Loaded;", "Lcom/discord/stores/StoreApplicationInteractions$State;", "Lcom/discord/api/commands/ApplicationCommandData;", "component1", "()Lcom/discord/api/commands/ApplicationCommandData;", "commandOptions", "copy", "(Lcom/discord/api/commands/ApplicationCommandData;)Lcom/discord/stores/StoreApplicationInteractions$State$Loaded;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/commands/ApplicationCommandData;", "getCommandOptions", HookHelper.constructorName, "(Lcom/discord/api/commands/ApplicationCommandData;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Loaded extends State {
            private final ApplicationCommandData commandOptions;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loaded(ApplicationCommandData applicationCommandData) {
                super(null);
                m.checkNotNullParameter(applicationCommandData, "commandOptions");
                this.commandOptions = applicationCommandData;
            }

            public static /* synthetic */ Loaded copy$default(Loaded loaded, ApplicationCommandData applicationCommandData, int i, Object obj) {
                if ((i & 1) != 0) {
                    applicationCommandData = loaded.commandOptions;
                }
                return loaded.copy(applicationCommandData);
            }

            public final ApplicationCommandData component1() {
                return this.commandOptions;
            }

            public final Loaded copy(ApplicationCommandData applicationCommandData) {
                m.checkNotNullParameter(applicationCommandData, "commandOptions");
                return new Loaded(applicationCommandData);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Loaded) && m.areEqual(this.commandOptions, ((Loaded) obj).commandOptions);
                }
                return true;
            }

            public final ApplicationCommandData getCommandOptions() {
                return this.commandOptions;
            }

            public int hashCode() {
                ApplicationCommandData applicationCommandData = this.commandOptions;
                if (applicationCommandData != null) {
                    return applicationCommandData.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(commandOptions=");
                R.append(this.commandOptions);
                R.append(")");
                return R.toString();
            }
        }

        private State() {
        }

        public /* synthetic */ State(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ StoreApplicationInteractions(StoreStream storeStream, Dispatcher dispatcher, StoreMessages storeMessages, StoreLocalActionComponentState storeLocalActionComponentState, StoreUser storeUser, Clock clock, SharedPreferences sharedPreferences, ObservationDeck observationDeck, RestAPI restAPI, NonceGenerator nonceGenerator, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(storeStream, dispatcher, storeMessages, storeLocalActionComponentState, storeUser, clock, (i & 64) != 0 ? SharedPreferencesProvider.INSTANCE.get() : sharedPreferences, (i & 128) != 0 ? ObservationDeckProvider.get() : observationDeck, (i & 256) != 0 ? RestAPI.Companion.getApi() : restAPI, (i & 512) != 0 ? new NonceGenerator() : nonceGenerator);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void addInteractionStateToComponent(long j, int i, InteractionSendState interactionSendState) {
        this.interactionComponentSendState.put(Long.valueOf(j), g0.mapOf(o.to(Integer.valueOf(i), interactionSendState)));
        markChanged(ComponentStateUpdate);
    }

    private final Message buildApplicationCommandLocalMessage(ApplicationCommandLocalSendData applicationCommandLocalSendData, boolean z2, boolean z3) {
        return LocalMessageCreatorsKt.createLocalApplicationCommandMessage(applicationCommandLocalSendData.getNonce(), applicationCommandLocalSendData.getApplicationCommandName(), applicationCommandLocalSendData.getChannelId(), UserUtils.INSTANCE.synthesizeApiUser(this.storeUser.getMe()), toUser(applicationCommandLocalSendData.getApplication()), z3, z2, applicationCommandLocalSendData.getInteractionId(), this.clock);
    }

    public static /* synthetic */ Message buildApplicationCommandLocalMessage$default(StoreApplicationInteractions storeApplicationInteractions, ApplicationCommandLocalSendData applicationCommandLocalSendData, boolean z2, boolean z3, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        if ((i & 4) != 0) {
            z3 = false;
        }
        return storeApplicationInteractions.buildApplicationCommandLocalMessage(applicationCommandLocalSendData, z2, z3);
    }

    @StoreThread
    private final void clearCache() {
        SharedPreferences.Editor edit = this.sharedPrefs.edit();
        m.checkNotNullExpressionValue(edit, "editor");
        edit.putString(CACHE_KEY_APPLICATION_COMMAND_SEND_DATA_SET, null);
        edit.apply();
        this.applicationCommandLocalSendDataSet.clear();
        markChanged();
    }

    private final void clearComponentInteractionSendSuccessAndFailures() {
        this.dispatcher.schedule(new StoreApplicationInteractions$clearComponentInteractionSendSuccessAndFailures$1(this));
    }

    @StoreThread
    private final void handleApplicationCommandRequestStateUpdate(ApplicationCommandLocalSendData applicationCommandLocalSendData, Long l) {
        upsertApplicationCommandSendData(ApplicationCommandLocalSendData.copy$default(applicationCommandLocalSendData, 0L, 0L, null, null, null, null, null, null, null, null, l, AudioAttributesCompat.FLAG_ALL, null));
    }

    public static /* synthetic */ void handleApplicationCommandRequestStateUpdate$default(StoreApplicationInteractions storeApplicationInteractions, ApplicationCommandLocalSendData applicationCommandLocalSendData, Long l, int i, Object obj) {
        if ((i & 2) != 0) {
            l = null;
        }
        storeApplicationInteractions.handleApplicationCommandRequestStateUpdate(applicationCommandLocalSendData, l);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleApplicationCommandResult(MessageResult messageResult, ApplicationCommandLocalSendData applicationCommandLocalSendData, Function0<Unit> function0, Function1<? super Error, Unit> function1) {
        ApplicationCommandLocalSendData applicationCommandLocalSendData2 = this.applicationCommandLocalSendDataSet.get(applicationCommandLocalSendData.getNonceString());
        boolean z2 = (applicationCommandLocalSendData2 != null ? applicationCommandLocalSendData2.getInteractionId() : null) == null;
        if (messageResult instanceof MessageResult.Success) {
            if (function0 != null) {
                function0.invoke();
            }
            if (z2) {
                handleApplicationCommandRequestStateUpdate$default(this, applicationCommandLocalSendData, null, 2, null);
            }
        } else if (messageResult instanceof MessageResult.UnknownFailure) {
            if (function1 != null) {
                function1.invoke(((MessageResult.UnknownFailure) messageResult).getError());
            }
            if (z2) {
                this.storeStream.handleInteractionFailure(new InteractionStateUpdate(applicationCommandLocalSendData.getInteractionId(), applicationCommandLocalSendData.getNonceString()));
            }
        } else if (z2) {
            this.storeStream.handleInteractionFailure(new InteractionStateUpdate(applicationCommandLocalSendData.getInteractionId(), applicationCommandLocalSendData.getNonceString()));
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void handleApplicationCommandResult$default(StoreApplicationInteractions storeApplicationInteractions, MessageResult messageResult, ApplicationCommandLocalSendData applicationCommandLocalSendData, Function0 function0, Function1 function1, int i, Object obj) {
        if ((i & 4) != 0) {
            function0 = null;
        }
        if ((i & 8) != 0) {
            function1 = null;
        }
        storeApplicationInteractions.handleApplicationCommandResult(messageResult, applicationCommandLocalSendData, function0, function1);
    }

    private final void handleComponentInteractionMessage(ComponentLocation componentLocation) {
        addInteractionStateToComponent(componentLocation.getMessageId(), componentLocation.getComponentIndex(), InteractionSendState.Success.INSTANCE);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleInteractionDataFetchFailure(long j) {
        this.interactionData.put(Long.valueOf(j), State.Failure.INSTANCE);
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleInteractionDataFetchStart(long j) {
        this.interactionData.put(Long.valueOf(j), State.Fetching.INSTANCE);
        markChanged();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleInteractionDataFetchSuccess(long j, ApplicationCommandData applicationCommandData) {
        this.interactionData.put(Long.valueOf(j), new State.Loaded(applicationCommandData));
        markChanged();
    }

    public static /* synthetic */ void handleInteractionFailure$default(StoreApplicationInteractions storeApplicationInteractions, ApplicationCommandLocalSendData applicationCommandLocalSendData, Long l, int i, Object obj) {
        if ((i & 2) != 0) {
            l = null;
        }
        storeApplicationInteractions.handleInteractionFailure(applicationCommandLocalSendData, l);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleSendApplicationCommandRequest(ApplicationCommandLocalSendData applicationCommandLocalSendData, List<? extends Attachment<?>> list, Function0<Unit> function0, Function1<? super Error, Unit> function1) {
        upsertApplicationCommandSendData(applicationCommandLocalSendData);
        ObservableExtensionsKt.appSubscribe(sendApplicationCommandObservable(applicationCommandLocalSendData, list), StoreApplicationInteractions.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreApplicationInteractions$handleSendApplicationCommandRequest$1(this, applicationCommandLocalSendData, function0, function1));
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void handleSendApplicationCommandRequest$default(StoreApplicationInteractions storeApplicationInteractions, ApplicationCommandLocalSendData applicationCommandLocalSendData, List list, Function0 function0, Function1 function1, int i, Object obj) {
        if ((i & 2) != 0) {
            list = null;
        }
        if ((i & 4) != 0) {
            function0 = null;
        }
        if ((i & 8) != 0) {
            function1 = null;
        }
        storeApplicationInteractions.handleSendApplicationCommandRequest(applicationCommandLocalSendData, list, function0, function1);
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0017 A[Catch: Exception -> 0x0036, TryCatch #0 {Exception -> 0x0036, blocks: (B:2:0x0000, B:4:0x000b, B:10:0x0017, B:11:0x001d, B:12:0x0033), top: B:17:0x0000 }] */
    /* JADX WARN: Removed duplicated region for block: B:11:0x001d A[Catch: Exception -> 0x0036, TryCatch #0 {Exception -> 0x0036, blocks: (B:2:0x0000, B:4:0x000b, B:10:0x0017, B:11:0x001d, B:12:0x0033), top: B:17:0x0000 }] */
    @com.discord.stores.StoreThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void loadCachedApplicationCommandSendDataSet() {
        /*
            r7 = this;
            android.content.SharedPreferences r0 = r7.sharedPrefs     // Catch: java.lang.Exception -> L36
            java.lang.String r1 = "CACHE_KEY_APPLICATION_COMMAND_SEND_DATA_SET"
            r2 = 0
            java.lang.String r0 = r0.getString(r1, r2)     // Catch: java.lang.Exception -> L36
            if (r0 == 0) goto L14
            boolean r1 = d0.g0.t.isBlank(r0)     // Catch: java.lang.Exception -> L36
            if (r1 == 0) goto L12
            goto L14
        L12:
            r1 = 0
            goto L15
        L14:
            r1 = 1
        L15:
            if (r1 == 0) goto L1d
            java.util.LinkedHashMap r0 = new java.util.LinkedHashMap     // Catch: java.lang.Exception -> L36
            r0.<init>()     // Catch: java.lang.Exception -> L36
            goto L33
        L1d:
            com.discord.stores.StoreApplicationInteractions$loadCachedApplicationCommandSendDataSet$type$1 r1 = new com.discord.stores.StoreApplicationInteractions$loadCachedApplicationCommandSendDataSet$type$1     // Catch: java.lang.Exception -> L36
            r1.<init>()     // Catch: java.lang.Exception -> L36
            java.lang.reflect.Type r1 = r1.getType()     // Catch: java.lang.Exception -> L36
            com.google.gson.Gson r2 = r7.gson     // Catch: java.lang.Exception -> L36
            java.lang.Object r0 = r2.g(r0, r1)     // Catch: java.lang.Exception -> L36
            java.lang.String r1 = "gson.fromJson(cachedDataSet, type)"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)     // Catch: java.lang.Exception -> L36
            java.util.Map r0 = (java.util.Map) r0     // Catch: java.lang.Exception -> L36
        L33:
            r7.applicationCommandLocalSendDataSet = r0     // Catch: java.lang.Exception -> L36
            goto L4a
        L36:
            r0 = move-exception
            r3 = r0
            r7.clearCache()
            java.util.Map<java.lang.String, com.discord.models.commands.ApplicationCommandLocalSendData> r0 = r7.applicationCommandLocalSendDataSet
            r0.clear()
            com.discord.app.AppLog r1 = com.discord.app.AppLog.g
            r4 = 0
            r5 = 4
            r6 = 0
            java.lang.String r2 = "Error restoring cached command send data"
            com.discord.utilities.logging.Logger.e$default(r1, r2, r3, r4, r5, r6)
        L4a:
            r7.markChanged()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreApplicationInteractions.loadCachedApplicationCommandSendDataSet():void");
    }

    @StoreThread
    private final void markAllLocalApplicationCommandRequestsAsFailed() {
        for (ApplicationCommandLocalSendData applicationCommandLocalSendData : this.applicationCommandLocalSendDataSet.values()) {
            this.storeStream.handleInteractionFailure(new InteractionStateUpdate(applicationCommandLocalSendData.getInteractionId(), applicationCommandLocalSendData.getNonceString()));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void removeApplicationCommandSendData(String str) {
        if (this.applicationCommandLocalSendDataSet.remove(str) != null) {
            markChanged();
        }
    }

    private final Observable<MessageResult> sendApplicationCommandObservable(ApplicationCommandLocalSendData applicationCommandLocalSendData, List<? extends Attachment<?>> list) {
        return this.storeMessages.sendMessage(buildApplicationCommandLocalMessage$default(this, applicationCommandLocalSendData, false, false, 6, null), applicationCommandLocalSendData, list);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ Observable sendApplicationCommandObservable$default(StoreApplicationInteractions storeApplicationInteractions, ApplicationCommandLocalSendData applicationCommandLocalSendData, List list, int i, Object obj) {
        if ((i & 2) != 0) {
            list = null;
        }
        return storeApplicationInteractions.sendApplicationCommandObservable(applicationCommandLocalSendData, list);
    }

    private final User toUser(Application application) {
        User bot = application.getBot();
        if (bot != null) {
            return bot;
        }
        long id2 = application.getId();
        String name = application.getName();
        String icon = application.getIcon();
        return new User(id2, name, icon != null ? new NullSerializable.b(icon) : new NullSerializable.a(null, 1), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 4194296);
    }

    @StoreThread
    private final void upsertApplicationCommandSendData(ApplicationCommandLocalSendData applicationCommandLocalSendData) {
        this.applicationCommandLocalSendDataSet.put(applicationCommandLocalSendData.getNonceString(), applicationCommandLocalSendData);
        markChanged();
    }

    public final void fetchInteractionDataIfNonExisting(long j, long j2, long j3, String str) {
        ApplicationCommandLocalSendData applicationCommandLocalSendData;
        State state = this.interactionData.get(Long.valueOf(j));
        if (!(state instanceof State.Fetching) && !(state instanceof State.Loaded)) {
            if ((str == null || t.isBlank(str)) || !this.applicationCommandLocalSendDataSnapshot.containsKey(str) || (applicationCommandLocalSendData = this.applicationCommandLocalSendDataSnapshot.get(str)) == null) {
                this.dispatcher.schedule(new StoreApplicationInteractions$fetchInteractionDataIfNonExisting$2(this, j));
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.getInteractionData(j2, j3), false, 1, null), StoreApplicationInteractions.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new StoreApplicationInteractions$fetchInteractionDataIfNonExisting$4(this, j), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreApplicationInteractions$fetchInteractionDataIfNonExisting$3(this, j));
                return;
            }
            this.dispatcher.schedule(new StoreApplicationInteractions$fetchInteractionDataIfNonExisting$$inlined$let$lambda$1(applicationCommandLocalSendData, this, j));
        }
    }

    public final Map<String, ApplicationCommandLocalSendData> getApplicationCommandLocalSendDataSet$app_productionGoogleRelease() {
        return this.applicationCommandLocalSendDataSet;
    }

    public final Map<Long, Map<Integer, InteractionSendState>> getComponentInteractionData() {
        return this.interactionComponentSendStateSnapshot;
    }

    public final Map<String, ComponentLocation> getComponentInteractions() {
        return this.componentInteractions;
    }

    public final Map<Long, Map<Integer, InteractionSendState>> getInteractionComponentSendState() {
        return this.interactionComponentSendState;
    }

    public final Map<Long, Map<Integer, InteractionSendState>> getInteractionComponentSendStateSnapshot() {
        return this.interactionComponentSendStateSnapshot;
    }

    public final State getInteractionData(long j) {
        return this.interactionDataSnapshot.get(Long.valueOf(j));
    }

    @StoreThread
    public final void handleChannelSelected() {
        clearComponentInteractionSendSuccessAndFailures();
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        this.sessionId = modelPayload.getSessionId();
    }

    @StoreThread
    public final void handleConnectionReady(boolean z2) {
        if (z2) {
            markAllLocalApplicationCommandRequestsAsFailed();
        }
    }

    @StoreThread
    public final void handleInteractionCreate(InteractionStateUpdate interactionStateUpdate) {
        m.checkNotNullParameter(interactionStateUpdate, "interactionStateUpdate");
        ApplicationCommandLocalSendData applicationCommandLocalSendData = this.applicationCommandLocalSendDataSet.get(interactionStateUpdate.b());
        if (applicationCommandLocalSendData != null) {
            handleApplicationCommandRequestStateUpdate(applicationCommandLocalSendData, interactionStateUpdate.a());
        }
    }

    @StoreThread
    public final void handleInteractionFailure(InteractionStateUpdate interactionStateUpdate) {
        ApplicationCommandLocalSendData applicationCommandLocalSendData;
        m.checkNotNullParameter(interactionStateUpdate, "interactionStateUpdate");
        if (this.componentInteractions.containsKey(interactionStateUpdate.b())) {
            ComponentLocation componentLocation = this.componentInteractions.get(interactionStateUpdate.b());
            if (componentLocation != null) {
                this.storeLocalActionComponentState.clearState(componentLocation.getMessageId(), Integer.valueOf(componentLocation.getComponentIndex()));
                this.componentInteractions.remove(interactionStateUpdate.b());
                addInteractionStateToComponent(componentLocation.getMessageId(), componentLocation.getComponentIndex(), new InteractionSendState.Failed(null, 1, null));
            }
        } else if (this.applicationCommandLocalSendDataSet.containsKey(interactionStateUpdate.b()) && (applicationCommandLocalSendData = this.applicationCommandLocalSendDataSet.get(interactionStateUpdate.b())) != null) {
            handleInteractionFailure(applicationCommandLocalSendData, interactionStateUpdate.a());
        }
    }

    @StoreThread
    public final void handleInteractionSuccess(InteractionStateUpdate interactionStateUpdate) {
        ComponentLocation componentLocation;
        m.checkNotNullParameter(interactionStateUpdate, "interactionStateUpdate");
        if (this.componentInteractions.containsKey(interactionStateUpdate.b()) && (componentLocation = this.componentInteractions.get(interactionStateUpdate.b())) != null) {
            this.interactionComponentSendState.remove(Long.valueOf(componentLocation.getMessageId()));
            this.componentInteractions.remove(interactionStateUpdate.b());
            markChanged(ComponentStateUpdate);
        }
    }

    @StoreThread
    public final void handleLocalMessageDelete(Message message) {
        m.checkNotNullParameter(message, "message");
        String nonce = message.getNonce();
        if (nonce != null) {
            removeApplicationCommandSendData(nonce);
        }
    }

    @StoreThread
    public final void handleMessageCreate(com.discord.api.message.Message message) {
        m.checkNotNullParameter(message, "message");
        handleMessagesCreateOrLoad(d0.t.m.listOf(new Message(message)));
    }

    @StoreThread
    public final void handleMessageUpdate(com.discord.api.message.Message message) {
        boolean z2;
        boolean z3;
        m.checkNotNullParameter(message, "message");
        Map<String, ComponentLocation> map = this.componentInteractions;
        if (!map.isEmpty()) {
            for (Map.Entry<String, ComponentLocation> entry : map.entrySet()) {
                if (entry.getValue().getMessageId() == message.o()) {
                    z3 = true;
                    continue;
                } else {
                    z3 = false;
                    continue;
                }
                if (z3) {
                    z2 = true;
                    break;
                }
            }
        }
        z2 = false;
        if (z2) {
            this.storeLocalActionComponentState.clearState(message.o(), null);
            Map<String, ComponentLocation> map2 = this.componentInteractions;
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (Map.Entry<String, ComponentLocation> entry2 : map2.entrySet()) {
                if (entry2.getValue().getMessageId() == message.o()) {
                    linkedHashMap.put(entry2.getKey(), entry2.getValue());
                }
            }
            for (String str : linkedHashMap.keySet()) {
                this.componentInteractions.remove(str);
            }
            markChanged(ComponentStateUpdate);
        }
        if (this.interactionComponentSendState.containsKey(Long.valueOf(message.o()))) {
            this.interactionComponentSendState.remove(Long.valueOf(message.o()));
            markChanged(ComponentStateUpdate);
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    @StoreThread
    public final void handleMessagesCreateOrLoad(List<Message> list) {
        m.checkNotNullParameter(list, "messagesList");
        for (Message message : list) {
            String nonce = message.getNonce();
            if (nonce == null || !this.componentInteractions.containsKey(nonce)) {
                Interaction interaction = message.getInteraction();
                ApplicationCommandLocalSendData applicationCommandLocalSendData = null;
                Long a = interaction != null ? interaction.a() : null;
                if (!(nonce == null || t.isBlank(nonce)) && this.applicationCommandLocalSendDataSet.containsKey(nonce)) {
                    applicationCommandLocalSendData = this.applicationCommandLocalSendDataSet.get(nonce);
                } else if (a != null) {
                    Iterator<T> it = this.applicationCommandLocalSendDataSet.values().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        Object next = it.next();
                        if (m.areEqual(((ApplicationCommandLocalSendData) next).getInteractionId(), a)) {
                            applicationCommandLocalSendData = next;
                            break;
                        }
                    }
                    applicationCommandLocalSendData = applicationCommandLocalSendData;
                }
                if (applicationCommandLocalSendData != null) {
                    removeApplicationCommandSendData(applicationCommandLocalSendData.getNonceString());
                    this.storeMessages.deleteLocalMessage(applicationCommandLocalSendData.getChannelId(), applicationCommandLocalSendData.getNonceString());
                }
            } else {
                ComponentLocation componentLocation = this.componentInteractions.get(nonce);
                if (componentLocation != null) {
                    handleComponentInteractionMessage(componentLocation);
                    this.componentInteractions.remove(nonce);
                } else {
                    return;
                }
            }
        }
    }

    @StoreThread
    public final void handlePreLogout() {
        this.applicationCommandLocalSendDataSet.clear();
        markChanged();
    }

    @Override // com.discord.stores.Store
    @StoreThread
    public void init(Context context) {
        m.checkNotNullParameter(context, "context");
        super.init(context);
        loadCachedApplicationCommandSendDataSet();
    }

    public final Observable<Map<Long, Map<Integer, InteractionSendState>>> observeComponentInteractionState() {
        Observable<Map<Long, Map<Integer, InteractionSendState>>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{ComponentStateUpdate}, false, null, null, new StoreApplicationInteractions$observeComponentInteractionState$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    public final Observable<State> observeInteractionData(long j) {
        Observable<State> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreApplicationInteractions$observeInteractionData$1(this, j), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    public final void resendApplicationCommand(Message message) {
        String nonce;
        ApplicationCommandLocalSendData applicationCommandLocalSendData;
        m.checkNotNullParameter(message, "message");
        Integer type = message.getType();
        if (type != null && type.intValue() == -4 && (nonce = message.getNonce()) != null && (applicationCommandLocalSendData = this.applicationCommandLocalSendDataSnapshot.get(nonce)) != null) {
            this.dispatcher.schedule(new StoreApplicationInteractions$resendApplicationCommand$1(this, message, nonce, ApplicationCommandLocalSendData.copy$default(applicationCommandLocalSendData, NonceGenerator.Companion.computeNonce$default(NonceGenerator.Companion, null, 1, null), 0L, null, null, null, null, null, null, null, null, null, 1022, null)));
        }
    }

    public final void sendApplicationCommand(long j, Long l, String str, com.discord.widgets.chat.input.models.ApplicationCommandData applicationCommandData, List<? extends Attachment<?>> list, Function0<Unit> function0, Function1<? super Error, Unit> function1) {
        m.checkNotNullParameter(applicationCommandData, "data");
        m.checkNotNullParameter(function0, "onSuccess");
        m.checkNotNullParameter(function1, "onFail");
        this.dispatcher.schedule(new StoreApplicationInteractions$sendApplicationCommand$1(this, new ApplicationCommandLocalSendData(this.nonceGenerator.nonce(), j, l, applicationCommandData.getApplicationCommand().getGuildId(), applicationCommandData.getApplication(), this.sessionId, applicationCommandData.getApplicationCommand().getName(), applicationCommandData.getApplicationCommand().getId(), applicationCommandData.getValues(), str, null, 1024, null), list, function0, function1));
    }

    public final void sendComponentInteraction(long j, Long l, long j2, long j3, int i, RestAPIParams.ComponentInteractionData componentInteractionData, Long l2) {
        m.checkNotNullParameter(componentInteractionData, "data");
        this.dispatcher.schedule(new StoreApplicationInteractions$sendComponentInteraction$1(this, j3, i, String.valueOf(this.nonceGenerator.nonce()), j, l, j2, l2, componentInteractionData));
    }

    public final void setInteractionComponentSendStateSnapshot(Map<Long, ? extends Map<Integer, ? extends InteractionSendState>> map) {
        m.checkNotNullParameter(map, "<set-?>");
        this.interactionComponentSendStateSnapshot = map;
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        super.snapshotData();
        this.interactionDataSnapshot = new HashMap(this.interactionData);
        this.interactionComponentSendStateSnapshot = new HashMap(this.interactionComponentSendState);
        HashMap hashMap = new HashMap(this.applicationCommandLocalSendDataSet);
        this.applicationCommandLocalSendDataSnapshot = hashMap;
        String m = this.gson.m(hashMap);
        SharedPreferences.Editor edit = this.sharedPrefs.edit();
        m.checkNotNullExpressionValue(edit, "editor");
        edit.putString(CACHE_KEY_APPLICATION_COMMAND_SEND_DATA_SET, m);
        edit.apply();
    }

    public StoreApplicationInteractions(StoreStream storeStream, Dispatcher dispatcher, StoreMessages storeMessages, StoreLocalActionComponentState storeLocalActionComponentState, StoreUser storeUser, Clock clock, SharedPreferences sharedPreferences, ObservationDeck observationDeck, RestAPI restAPI, NonceGenerator nonceGenerator) {
        m.checkNotNullParameter(storeStream, "storeStream");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(storeMessages, "storeMessages");
        m.checkNotNullParameter(storeLocalActionComponentState, "storeLocalActionComponentState");
        m.checkNotNullParameter(storeUser, "storeUser");
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(sharedPreferences, "sharedPrefs");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(restAPI, "restAPI");
        m.checkNotNullParameter(nonceGenerator, "nonceGenerator");
        this.storeStream = storeStream;
        this.dispatcher = dispatcher;
        this.storeMessages = storeMessages;
        this.storeLocalActionComponentState = storeLocalActionComponentState;
        this.storeUser = storeUser;
        this.clock = clock;
        this.sharedPrefs = sharedPreferences;
        this.observationDeck = observationDeck;
        this.restAPI = restAPI;
        this.nonceGenerator = nonceGenerator;
        this.componentInteractions = new LinkedHashMap();
        this.interactionComponentSendState = new LinkedHashMap();
        this.interactionComponentSendStateSnapshot = h0.emptyMap();
        this.applicationCommandLocalSendDataSnapshot = new LinkedHashMap();
        this.applicationCommandLocalSendDataSet = new LinkedHashMap();
        this.interactionDataSnapshot = h0.emptyMap();
        this.interactionData = new HashMap<>();
        e eVar = new e();
        b.a.b.a.a(eVar);
        this.gson = eVar.a();
    }

    @StoreThread
    private final void handleInteractionFailure(ApplicationCommandLocalSendData applicationCommandLocalSendData, Long l) {
        handleApplicationCommandRequestStateUpdate(applicationCommandLocalSendData, l);
    }
}
