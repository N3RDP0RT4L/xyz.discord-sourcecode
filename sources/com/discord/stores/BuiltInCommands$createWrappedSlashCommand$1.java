package com.discord.stores;

import d0.z.d.m;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreApplicationCommands.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0010\u0000\n\u0002\b\u0004\u0010\u0006\u001a\u00020\u00012\u0014\u0010\u0003\u001a\u0010\u0012\u0004\u0012\u00020\u0001\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "", "", "commandOptions", "invoke", "(Ljava/util/Map;)Ljava/lang/String;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class BuiltInCommands$createWrappedSlashCommand$1 extends o implements Function1<Map<String, ? extends Object>, String> {
    public final /* synthetic */ String $wrapper;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public BuiltInCommands$createWrappedSlashCommand$1(String str) {
        super(1);
        this.$wrapper = str;
    }

    public final String invoke(Map<String, ? extends Object> map) {
        m.checkNotNullParameter(map, "commandOptions");
        StringBuilder sb = new StringBuilder();
        sb.append(this.$wrapper);
        Object obj = map.get("message");
        if (obj == null) {
            obj = "";
        }
        sb.append(obj);
        sb.append(this.$wrapper);
        return sb.toString();
    }
}
