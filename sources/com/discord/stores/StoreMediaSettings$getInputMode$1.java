package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.rtcconnection.mediaengine.MediaEngineConnection;
import com.discord.stores.StoreMediaSettings;
import j0.k.b;
import kotlin.Metadata;
/* compiled from: StoreMediaSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "kotlin.jvm.PlatformType", "it", "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;)Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMediaSettings$getInputMode$1<T, R> implements b<StoreMediaSettings.VoiceConfiguration, MediaEngineConnection.InputMode> {
    public static final StoreMediaSettings$getInputMode$1 INSTANCE = new StoreMediaSettings$getInputMode$1();

    public final MediaEngineConnection.InputMode call(StoreMediaSettings.VoiceConfiguration voiceConfiguration) {
        return voiceConfiguration.getInputMode();
    }
}
