package com.discord.stores;

import andhook.lib.HookHelper;
import com.discord.api.guildjoinrequest.ApplicationStatus;
import com.discord.api.guildjoinrequest.GuildJoinRequest;
import com.discord.models.domain.ModelPayload;
import com.discord.models.guild.Guild;
import com.discord.stores.updates.ObservationDeck;
import com.discord.widgets.servers.member_verification.MemberVerificationSuccessDialog;
import d0.t.h0;
import d0.z.d.m;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: StoreGuildJoinRequest.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B'\u0012\u0006\u00101\u001a\u000200\u0012\u0006\u0010'\u001a\u00020&\u0012\u0006\u0010*\u001a\u00020)\u0012\u0006\u00104\u001a\u000203¢\u0006\u0004\b6\u00107J\u001b\u0010\u0006\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J#\u0010\u000b\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\n0\t0\b¢\u0006\u0004\b\u000b\u0010\fJ#\u0010\u000e\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\r0\t0\b¢\u0006\u0004\b\u000e\u0010\fJ!\u0010\u000f\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\r0\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u001b\u0010\u0011\u001a\u0004\u0018\u00010\r2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\r\u0010\u0013\u001a\u00020\u0005¢\u0006\u0004\b\u0013\u0010\u0014J\u0017\u0010\u0017\u001a\u00020\u00052\u0006\u0010\u0016\u001a\u00020\u0015H\u0007¢\u0006\u0004\b\u0017\u0010\u0018J\u001b\u0010\u0019\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0007¢\u0006\u0004\b\u0019\u0010\u0007J#\u0010\u001b\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u001a\u001a\u00020\rH\u0007¢\u0006\u0004\b\u001b\u0010\u001cJ'\u0010\u001f\u001a\u00020\u00052\n\u0010\u001e\u001a\u00060\u0002j\u0002`\u001d2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0007¢\u0006\u0004\b\u001f\u0010 J\u000f\u0010!\u001a\u00020\u0005H\u0017¢\u0006\u0004\b!\u0010\u0014R:\u0010$\u001a&\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\r0\"j\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\r`#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%R\u0016\u0010'\u001a\u00020&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R\u0016\u0010*\u001a\u00020)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010+R&\u0010,\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\n0\t8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b,\u0010-R&\u0010.\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\r0\t8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b.\u0010-R:\u0010/\u001a&\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\n0\"j\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\n`#8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b/\u0010%R\u0016\u00101\u001a\u0002008\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b1\u00102R\u0016\u00104\u001a\u0002038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b4\u00105¨\u00068"}, d2 = {"Lcom/discord/stores/StoreGuildJoinRequest;", "Lcom/discord/stores/StoreV2;", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "handleGuildJoinRequestApproved", "(J)V", "Lrx/Observable;", "", "Lcom/discord/models/guild/Guild;", "observePendingGuilds", "()Lrx/Observable;", "Lcom/discord/api/guildjoinrequest/GuildJoinRequest;", "observeGuildJoinRequests", "observeGuildJoinRequest", "(J)Lrx/Observable;", "getGuildJoinRequest", "(J)Lcom/discord/api/guildjoinrequest/GuildJoinRequest;", "fetchPendingGuilds", "()V", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "handleGuildRemove", "request", "handleGuildJoinRequestCreateOrUpdate", "(JLcom/discord/api/guildjoinrequest/GuildJoinRequest;)V", "Lcom/discord/primitives/UserId;", "userId", "handleGuildJoinRequestDelete", "(JJ)V", "snapshotData", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "guildJoinRequests", "Ljava/util/HashMap;", "Lcom/discord/stores/StoreGuilds;", "guildsStore", "Lcom/discord/stores/StoreGuilds;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "pendingGuildsSnapshot", "Ljava/util/Map;", "guildJoinRequestsSnapshot", "pendingGuilds", "Lcom/discord/stores/StoreUser;", "userStore", "Lcom/discord/stores/StoreUser;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", HookHelper.constructorName, "(Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/updates/ObservationDeck;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildJoinRequest extends StoreV2 {
    private final Dispatcher dispatcher;
    private final StoreGuilds guildsStore;
    private final ObservationDeck observationDeck;
    private final StoreUser userStore;
    private HashMap<Long, Guild> pendingGuilds = new HashMap<>();
    private Map<Long, Guild> pendingGuildsSnapshot = h0.emptyMap();
    private final HashMap<Long, GuildJoinRequest> guildJoinRequests = new HashMap<>();
    private Map<Long, GuildJoinRequest> guildJoinRequestsSnapshot = h0.emptyMap();

    public StoreGuildJoinRequest(StoreUser storeUser, StoreGuilds storeGuilds, Dispatcher dispatcher, ObservationDeck observationDeck) {
        m.checkNotNullParameter(storeUser, "userStore");
        m.checkNotNullParameter(storeGuilds, "guildsStore");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.userStore = storeUser;
        this.guildsStore = storeGuilds;
        this.dispatcher = dispatcher;
        this.observationDeck = observationDeck;
    }

    private final void handleGuildJoinRequestApproved(long j) {
        Guild guild = this.guildsStore.getGuild(j);
        if ((guild != null ? guild.getName() : null) != null) {
            MemberVerificationSuccessDialog.Companion.enqueue(j);
        }
    }

    public final void fetchPendingGuilds() {
        this.dispatcher.schedule(new StoreGuildJoinRequest$fetchPendingGuilds$1(this));
    }

    public final GuildJoinRequest getGuildJoinRequest(long j) {
        return this.guildJoinRequestsSnapshot.get(Long.valueOf(j));
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        List<GuildJoinRequest> guildJoinRequests = modelPayload.getGuildJoinRequests();
        if (guildJoinRequests != null) {
            for (GuildJoinRequest guildJoinRequest : guildJoinRequests) {
                HashMap<Long, GuildJoinRequest> hashMap = this.guildJoinRequests;
                Long valueOf = Long.valueOf(guildJoinRequest.b());
                m.checkNotNullExpressionValue(guildJoinRequest, "joinRequest");
                hashMap.put(valueOf, guildJoinRequest);
            }
        }
        markChanged();
    }

    @StoreThread
    public final void handleGuildJoinRequestCreateOrUpdate(long j, GuildJoinRequest guildJoinRequest) {
        m.checkNotNullParameter(guildJoinRequest, "request");
        if (guildJoinRequest.e() == this.userStore.getMe().getId()) {
            this.guildJoinRequests.put(Long.valueOf(j), guildJoinRequest);
            if (guildJoinRequest.a() == ApplicationStatus.APPROVED && guildJoinRequest.c() == null) {
                handleGuildJoinRequestApproved(j);
            }
            markChanged();
        }
    }

    @StoreThread
    public final void handleGuildJoinRequestDelete(long j, long j2) {
        if (j == this.userStore.getMe().getId()) {
            this.guildJoinRequests.remove(Long.valueOf(j2));
            this.pendingGuilds.remove(Long.valueOf(j2));
            markChanged();
        }
    }

    @StoreThread
    public final void handleGuildRemove(long j) {
        if (this.guildJoinRequests.remove(Long.valueOf(j)) != null) {
            markChanged();
        }
    }

    public final Observable<GuildJoinRequest> observeGuildJoinRequest(long j) {
        Observable<GuildJoinRequest> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreGuildJoinRequest$observeGuildJoinRequest$1(this, j), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    public final Observable<Map<Long, GuildJoinRequest>> observeGuildJoinRequests() {
        Observable<Map<Long, GuildJoinRequest>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreGuildJoinRequest$observeGuildJoinRequests$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    public final Observable<Map<Long, Guild>> observePendingGuilds() {
        Observable<Map<Long, Guild>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreGuildJoinRequest$observePendingGuilds$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR… }.distinctUntilChanged()");
        return q;
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        super.snapshotData();
        this.guildJoinRequestsSnapshot = new HashMap(this.guildJoinRequests);
        this.pendingGuildsSnapshot = new HashMap(this.pendingGuilds);
    }
}
