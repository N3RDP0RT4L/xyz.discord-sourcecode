package com.discord.stores;

import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreApplicationInteractions;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Collection;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreApplicationInteractions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreApplicationInteractions$sendComponentInteraction$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $applicationId;
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ int $componentIndex;
    public final /* synthetic */ RestAPIParams.ComponentInteractionData $data;
    public final /* synthetic */ Long $guildId;
    public final /* synthetic */ Long $messageFlags;
    public final /* synthetic */ long $messageId;
    public final /* synthetic */ String $nonce;
    public final /* synthetic */ StoreApplicationInteractions this$0;

    /* compiled from: StoreApplicationInteractions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Ljava/lang/Void;", "it", "", "invoke", "(Ljava/lang/Void;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreApplicationInteractions$sendComponentInteraction$1$3  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass3 extends o implements Function1<Void, Unit> {
        public static final AnonymousClass3 INSTANCE = new AnonymousClass3();

        public AnonymousClass3() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Void r1) {
            invoke2(r1);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Void r1) {
        }
    }

    /* compiled from: StoreApplicationInteractions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreApplicationInteractions$sendComponentInteraction$1$4  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass4 extends o implements Function1<Error, Unit> {

        /* compiled from: StoreApplicationInteractions.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreApplicationInteractions$sendComponentInteraction$1$4$1  reason: invalid class name */
        /* loaded from: classes.dex */
        public static final class AnonymousClass1 extends o implements Function0<Unit> {
            public final /* synthetic */ Error $it;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public AnonymousClass1(Error error) {
                super(0);
                this.$it = error;
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                StoreLocalActionComponentState storeLocalActionComponentState;
                storeLocalActionComponentState = StoreApplicationInteractions$sendComponentInteraction$1.this.this$0.storeLocalActionComponentState;
                StoreApplicationInteractions$sendComponentInteraction$1 storeApplicationInteractions$sendComponentInteraction$1 = StoreApplicationInteractions$sendComponentInteraction$1.this;
                storeLocalActionComponentState.clearState(storeApplicationInteractions$sendComponentInteraction$1.$messageId, Integer.valueOf(storeApplicationInteractions$sendComponentInteraction$1.$componentIndex));
                StoreApplicationInteractions$sendComponentInteraction$1.this.this$0.getComponentInteractions().remove(StoreApplicationInteractions$sendComponentInteraction$1.this.$nonce);
                StoreApplicationInteractions$sendComponentInteraction$1 storeApplicationInteractions$sendComponentInteraction$12 = StoreApplicationInteractions$sendComponentInteraction$1.this;
                StoreApplicationInteractions storeApplicationInteractions = storeApplicationInteractions$sendComponentInteraction$12.this$0;
                long j = storeApplicationInteractions$sendComponentInteraction$12.$messageId;
                int i = storeApplicationInteractions$sendComponentInteraction$12.$componentIndex;
                Error.Response response = this.$it.getResponse();
                m.checkNotNullExpressionValue(response, "it.response");
                storeApplicationInteractions.addInteractionStateToComponent(j, i, new StoreApplicationInteractions.InteractionSendState.Failed(response.getMessage()));
            }
        }

        public AnonymousClass4() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Error error) {
            invoke2(error);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Error error) {
            Dispatcher dispatcher;
            m.checkNotNullParameter(error, "it");
            dispatcher = StoreApplicationInteractions$sendComponentInteraction$1.this.this$0.dispatcher;
            dispatcher.schedule(new AnonymousClass1(error));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreApplicationInteractions$sendComponentInteraction$1(StoreApplicationInteractions storeApplicationInteractions, long j, int i, String str, long j2, Long l, long j3, Long l2, RestAPIParams.ComponentInteractionData componentInteractionData) {
        super(0);
        this.this$0 = storeApplicationInteractions;
        this.$messageId = j;
        this.$componentIndex = i;
        this.$nonce = str;
        this.$applicationId = j2;
        this.$guildId = l;
        this.$channelId = j3;
        this.$messageFlags = l2;
        this.$data = componentInteractionData;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        boolean z2;
        RestAPI restAPI;
        String str;
        boolean z3;
        StoreLocalActionComponentState storeLocalActionComponentState;
        boolean z4;
        boolean z5;
        Collection<StoreApplicationInteractions.ComponentLocation> values = this.this$0.getComponentInteractions().values();
        if (!(values instanceof Collection) || !values.isEmpty()) {
            for (StoreApplicationInteractions.ComponentLocation componentLocation : values) {
                if (componentLocation.getMessageId() == this.$messageId) {
                    z5 = true;
                    continue;
                } else {
                    z5 = false;
                    continue;
                }
                if (z5) {
                    z2 = true;
                    break;
                }
            }
        }
        z2 = false;
        if (z2) {
            Collection<StoreApplicationInteractions.ComponentLocation> values2 = this.this$0.getComponentInteractions().values();
            if (!(values2 instanceof Collection) || !values2.isEmpty()) {
                for (StoreApplicationInteractions.ComponentLocation componentLocation2 : values2) {
                    if (componentLocation2.getMessageId() != this.$messageId || componentLocation2.getComponentIndex() == this.$componentIndex) {
                        z4 = false;
                        continue;
                    } else {
                        z4 = true;
                        continue;
                    }
                    if (!z4) {
                        z3 = false;
                        break;
                    }
                }
            }
            z3 = true;
            if (z3) {
                storeLocalActionComponentState = this.this$0.storeLocalActionComponentState;
                storeLocalActionComponentState.clearState(this.$messageId, Integer.valueOf(this.$componentIndex));
                return;
            }
            return;
        }
        this.this$0.getComponentInteractions().put(this.$nonce, new StoreApplicationInteractions.ComponentLocation(this.$messageId, this.$componentIndex));
        this.this$0.addInteractionStateToComponent(this.$messageId, this.$componentIndex, StoreApplicationInteractions.InteractionSendState.Loading.INSTANCE);
        restAPI = this.this$0.restAPI;
        long j = this.$applicationId;
        Long l = this.$guildId;
        long j2 = this.$channelId;
        long j3 = this.$messageId;
        Long l2 = this.$messageFlags;
        RestAPIParams.ComponentInteractionData componentInteractionData = this.$data;
        str = this.this$0.sessionId;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(restAPI.sendComponentInteraction(new RestAPIParams.ComponentInteraction(3L, j2, j, l, j3, l2, componentInteractionData, str, this.$nonce)), false, 1, null), this.this$0.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new AnonymousClass4(), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, AnonymousClass3.INSTANCE);
    }
}
