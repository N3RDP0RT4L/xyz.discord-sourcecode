package com.discord.stores;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.SharedPreferences;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.g0.s;
import d0.t.n0;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: StoreGuildsNsfw.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010#\n\u0002\b\u0007\u0018\u0000 #2\u00020\u0001:\u0001#B\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0017\u0012\u0006\u0010\u001b\u001a\u00020\u001a¢\u0006\u0004\b!\u0010\"J#\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0002*\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J#\u0010\b\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002*\b\u0012\u0004\u0012\u00020\u00050\u0002H\u0002¢\u0006\u0004\b\b\u0010\u0007J\u0019\u0010\u000b\u001a\u00020\n2\n\u0010\t\u001a\u00060\u0003j\u0002`\u0004¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u000e\u001a\u00020\rH\u0017¢\u0006\u0004\b\u0010\u0010\u0011J\u0019\u0010\u0012\u001a\u00020\u000f2\n\u0010\t\u001a\u00060\u0003j\u0002`\u0004¢\u0006\u0004\b\u0012\u0010\u0013J\u0019\u0010\u0014\u001a\u00020\u000f2\n\u0010\t\u001a\u00060\u0003j\u0002`\u0004¢\u0006\u0004\b\u0014\u0010\u0013J\u000f\u0010\u0015\u001a\u00020\u000fH\u0017¢\u0006\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0018\u001a\u00020\u00178\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R\u0016\u0010\u001b\u001a\u00020\u001a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR \u0010\u001e\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u001d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR \u0010 \u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b \u0010\u001f¨\u0006$"}, d2 = {"Lcom/discord/stores/StoreGuildsNsfw;", "Lcom/discord/stores/StoreV2;", "", "", "Lcom/discord/primitives/GuildId;", "", "toStringSet", "(Ljava/util/Set;)Ljava/util/Set;", "toGuildIdSet", "guildId", "", "isGuildNsfwGateAgreed", "(J)Z", "Landroid/content/Context;", "context", "", "init", "(Landroid/content/Context;)V", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_GRANTED, "(J)V", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_DENIED, "snapshotData", "()V", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreChannels;", "", "guildIdsAllowed", "Ljava/util/Set;", "guildIdsAllowedSnapshot", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreChannels;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildsNsfw extends StoreV2 {
    private static final Companion Companion = new Companion(null);
    @Deprecated
    private static final String GUILDS_ALLOWED_KEY = "GUILDS_ALLOWED_KEY";
    private final Dispatcher dispatcher;
    private final Set<Long> guildIdsAllowed = new LinkedHashSet();
    private Set<Long> guildIdsAllowedSnapshot = n0.emptySet();
    private final StoreChannels storeChannels;

    /* compiled from: StoreGuildsNsfw.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0082\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/stores/StoreGuildsNsfw$Companion;", "", "", StoreGuildsNsfw.GUILDS_ALLOWED_KEY, "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public StoreGuildsNsfw(Dispatcher dispatcher, StoreChannels storeChannels) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(storeChannels, "storeChannels");
        this.dispatcher = dispatcher;
        this.storeChannels = storeChannels;
    }

    private final Set<Long> toGuildIdSet(Set<String> set) {
        ArrayList arrayList = new ArrayList();
        for (String str : set) {
            Long longOrNull = s.toLongOrNull(str);
            if (longOrNull != null) {
                arrayList.add(longOrNull);
            }
        }
        return u.toSet(arrayList);
    }

    private final Set<String> toStringSet(Set<Long> set) {
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(set, 10));
        for (Number number : set) {
            arrayList.add(String.valueOf(number.longValue()));
        }
        return u.toSet(arrayList);
    }

    public final void allow(long j) {
        this.dispatcher.schedule(new StoreGuildsNsfw$allow$1(this, j));
    }

    public final void deny(long j) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.takeSingleUntilTimeout$default(this.storeChannels.observeDefaultChannel(j), 0L, false, 3, null), StoreGuildsNsfw.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, StoreGuildsNsfw$deny$1.INSTANCE);
    }

    @Override // com.discord.stores.Store
    @StoreThread
    public void init(Context context) {
        Set<Long> set;
        m.checkNotNullParameter(context, "context");
        super.init(context);
        Set<String> stringSet = getPrefs().getStringSet(GUILDS_ALLOWED_KEY, n0.emptySet());
        if (stringSet == null || (set = toGuildIdSet(stringSet)) == null) {
            set = n0.emptySet();
        }
        this.guildIdsAllowed.addAll(set);
        markChanged();
    }

    public final boolean isGuildNsfwGateAgreed(long j) {
        return this.guildIdsAllowedSnapshot.contains(Long.valueOf(j));
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        super.snapshotData();
        this.guildIdsAllowedSnapshot = new HashSet(this.guildIdsAllowed);
        SharedPreferences.Editor edit = getPrefs().edit();
        m.checkNotNullExpressionValue(edit, "editor");
        edit.putStringSet(GUILDS_ALLOWED_KEY, toStringSet(this.guildIdsAllowed));
        edit.apply();
    }
}
