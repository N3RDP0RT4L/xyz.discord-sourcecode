package com.discord.stores;

import com.discord.stores.StoreAuditLog;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreAuditLog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAuditLog$toggleSelectedState$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $selectedItemId;
    public final /* synthetic */ StoreAuditLog this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreAuditLog$toggleSelectedState$1(StoreAuditLog storeAuditLog, long j) {
        super(0);
        this.this$0 = storeAuditLog;
        this.$selectedItemId = j;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreAuditLog.AuditLogState auditLogState;
        StoreAuditLog.AuditLogState auditLogState2;
        StoreAuditLog.AuditLogState auditLogState3;
        StoreAuditLog.AuditLogState auditLogState4;
        StoreAuditLog storeAuditLog = this.this$0;
        long j = this.$selectedItemId;
        auditLogState = storeAuditLog.state;
        Long selectedItemId = auditLogState.getSelectedItemId();
        if (selectedItemId != null && j == selectedItemId.longValue()) {
            auditLogState4 = this.this$0.state;
            auditLogState2 = StoreAuditLog.AuditLogState.copy$default(auditLogState4, 0L, null, null, null, null, null, null, null, null, null, false, 1919, null);
        } else {
            auditLogState3 = this.this$0.state;
            auditLogState2 = StoreAuditLog.AuditLogState.copy$default(auditLogState3, 0L, null, null, null, null, null, null, Long.valueOf(this.$selectedItemId), null, null, false, 1919, null);
        }
        storeAuditLog.state = auditLogState2;
        this.this$0.markChanged();
    }
}
