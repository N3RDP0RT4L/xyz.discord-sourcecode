package com.discord.stores;

import com.discord.models.gifpicker.dto.TrendingGifCategoriesResponseDto;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreGifPicker.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/gifpicker/dto/TrendingGifCategoriesResponseDto;", "p1", "", "invoke", "(Lcom/discord/models/gifpicker/dto/TrendingGifCategoriesResponseDto;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final /* synthetic */ class StoreGifPicker$fetchGifCategories$2 extends k implements Function1<TrendingGifCategoriesResponseDto, Unit> {
    public StoreGifPicker$fetchGifCategories$2(StoreGifPicker storeGifPicker) {
        super(1, storeGifPicker, StoreGifPicker.class, "handleFetchGifCategoriesOnNext", "handleFetchGifCategoriesOnNext(Lcom/discord/models/gifpicker/dto/TrendingGifCategoriesResponseDto;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(TrendingGifCategoriesResponseDto trendingGifCategoriesResponseDto) {
        invoke2(trendingGifCategoriesResponseDto);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(TrendingGifCategoriesResponseDto trendingGifCategoriesResponseDto) {
        m.checkNotNullParameter(trendingGifCategoriesResponseDto, "p1");
        ((StoreGifPicker) this.receiver).handleFetchGifCategoriesOnNext(trendingGifCategoriesResponseDto);
    }
}
