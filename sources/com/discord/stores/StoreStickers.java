package com.discord.stores;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.sticker.Sticker;
import com.discord.api.sticker.StickerType;
import com.discord.models.sticker.dto.ModelStickerPack;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.media.MediaFrecencyTracker;
import com.discord.utilities.persister.Persister;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.Clock;
import com.discord.utilities.time.ClockFactory;
import d0.d0.f;
import d0.t.g0;
import d0.t.h0;
import d0.t.n;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreStickers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 M2\u00020\u0001:\u0002MNB-\u0012\u0006\u0010H\u001a\u00020G\u0012\b\b\u0002\u0010E\u001a\u00020D\u0012\b\b\u0002\u0010B\u001a\u00020A\u0012\b\b\u0002\u0010:\u001a\u000209¢\u0006\u0004\bK\u0010LJ\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u001d\u0010\t\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0005¢\u0006\u0004\b\t\u0010\nJ\u001b\u0010\f\u001a\u0004\u0018\u00010\b2\n\u0010\u000b\u001a\u00060\u0006j\u0002`\u0007¢\u0006\u0004\b\f\u0010\rJ\u001d\u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u000f0\u0005¢\u0006\u0004\b\u0010\u0010\nJ\u0013\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011¢\u0006\u0004\b\u0013\u0010\u0014J\u0013\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0011¢\u0006\u0004\b\u0015\u0010\u0014J\u001f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\b0\u00162\n\u0010\u000b\u001a\u00060\u0006j\u0002`\u0007¢\u0006\u0004\b\u0017\u0010\u0018J\u0019\u0010\u0019\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00110\u0016¢\u0006\u0004\b\u0019\u0010\u001aJ\u001d\u0010\u001b\u001a\u0012\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0006j\u0002`\u000e0\u00110\u0016¢\u0006\u0004\b\u001b\u0010\u001aJ\u0019\u0010\u001c\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u00110\u0016¢\u0006\u0004\b\u001c\u0010\u001aJ\u0015\u0010\u001e\u001a\u00020\u00022\u0006\u0010\u001d\u001a\u00020\u000f¢\u0006\u0004\b\u001e\u0010\u001fJ\u0019\u0010 \u001a\u00020\u00022\n\u0010\u000b\u001a\u00060\u0006j\u0002`\u0007¢\u0006\u0004\b \u0010!J\r\u0010\"\u001a\u00020\u0002¢\u0006\u0004\b\"\u0010\u0004J!\u0010$\u001a\u00020\u00022\u0010\u0010#\u001a\f\u0012\b\u0012\u00060\u0006j\u0002`\u00070\u0011H\u0007¢\u0006\u0004\b$\u0010%J\u001d\u0010'\u001a\u00020\u00022\f\u0010&\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011H\u0007¢\u0006\u0004\b'\u0010%J\u001d\u0010)\u001a\u00020\u00022\f\u0010(\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011H\u0007¢\u0006\u0004\b)\u0010%J\u0017\u0010*\u001a\u00020\u00022\b\u0010\u001d\u001a\u0004\u0018\u00010\u000f¢\u0006\u0004\b*\u0010\u001fJ\u000f\u0010+\u001a\u00020\u0002H\u0017¢\u0006\u0004\b+\u0010\u0004J\u000f\u0010,\u001a\u00020\u0002H\u0007¢\u0006\u0004\b,\u0010\u0004J\u001f\u0010.\u001a\u00020\u00022\u0010\u0010-\u001a\f\u0012\b\u0012\u00060\u0006j\u0002`\u000e0\u0011¢\u0006\u0004\b.\u0010%R\u001c\u00101\u001a\b\u0012\u0004\u0012\u0002000/8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b1\u00102R&\u00103\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u000f0\u00058\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b3\u00104R\u0016\u00105\u001a\u00020\u00068\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b5\u00106R\u001c\u00107\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b7\u00108R\u0016\u0010:\u001a\u0002098\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b:\u0010;R\u001c\u0010<\u001a\b\u0012\u0004\u0012\u00020\u00120\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b<\u00108R\u0016\u0010=\u001a\u0002008\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b=\u0010>R&\u0010?\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u00058\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b?\u00104R\u001c\u0010@\u001a\b\u0012\u0004\u0012\u00020\u00120\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b@\u00108R\u0016\u0010B\u001a\u00020A8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bB\u0010CR\u0016\u0010E\u001a\u00020D8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bE\u0010FR\u0016\u0010H\u001a\u00020G8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bH\u0010IR&\u0010J\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u00058\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bJ\u00104¨\u0006O"}, d2 = {"Lcom/discord/stores/StoreStickers;", "Lcom/discord/stores/StoreV2;", "", "init", "()V", "", "", "Lcom/discord/primitives/StickerPackId;", "Lcom/discord/stores/StoreStickers$StickerPackState;", "getStickerPacks", "()Ljava/util/Map;", "stickerPackId", "getStickerPack", "(J)Lcom/discord/stores/StoreStickers$StickerPackState;", "Lcom/discord/primitives/StickerId;", "Lcom/discord/api/sticker/Sticker;", "getStickers", "", "Lcom/discord/models/sticker/dto/ModelStickerPack;", "getEnabledStickerPacks", "()Ljava/util/List;", "getEnabledStickers", "Lrx/Observable;", "observeStickerPack", "(J)Lrx/Observable;", "observeStickerPacks", "()Lrx/Observable;", "observeFrequentlyUsedStickerIds", "observeEnabledStickerPacks", "sticker", "onStickerUsed", "(Lcom/discord/api/sticker/Sticker;)V", "fetchStickerPack", "(J)V", "fetchEnabledStickerDirectory", "stickerPackIds", "handleNewLoadingStickerPacks", "(Ljava/util/List;)V", "newStickerPacks", "handleNewLoadedStickerPacks", "enabledPacks", "handleNewEnabledStickerDirectory", "handleFetchedSticker", "snapshotData", "handlePreLogout", "stickerIds", "handleDeletedStickers", "Lcom/discord/utilities/persister/Persister;", "Lcom/discord/utilities/media/MediaFrecencyTracker;", "frecencyCache", "Lcom/discord/utilities/persister/Persister;", "stickersSnapshot", "Ljava/util/Map;", "lastFetchedEnabledPacks", "J", "enabledStickerPacksStickersSnapshot", "Ljava/util/List;", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "enabledStickerPacks", "frecency", "Lcom/discord/utilities/media/MediaFrecencyTracker;", "stickerPacksSnapshot", "enabledStickerPacksSnapshot", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/utilities/rest/RestAPI;", "api", "Lcom/discord/utilities/rest/RestAPI;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "stickerPacks", HookHelper.constructorName, "(Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/utilities/time/Clock;)V", "Companion", "StickerPackState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStickers extends StoreV2 {
    public static final Companion Companion = new Companion(null);
    private static final long FETCH_ENABLED_STICKER_PACKS_DELAY = 300000;
    private static final int MAX_FREQUENTLY_USED_STICKERS = 20;
    private final RestAPI api;
    private final Clock clock;
    private final Dispatcher dispatcher;
    private List<ModelStickerPack> enabledStickerPacks;
    private List<ModelStickerPack> enabledStickerPacksSnapshot;
    private List<Sticker> enabledStickerPacksStickersSnapshot;
    private final MediaFrecencyTracker frecency;
    private final Persister<MediaFrecencyTracker> frecencyCache;
    private long lastFetchedEnabledPacks;
    private final ObservationDeck observationDeck;
    private Map<Long, ? extends StickerPackState> stickerPacks;
    private Map<Long, ? extends StickerPackState> stickerPacksSnapshot;
    private Map<Long, Sticker> stickersSnapshot;

    /* compiled from: StoreStickers.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/stores/StoreStickers$Companion;", "", "", "FETCH_ENABLED_STICKER_PACKS_DELAY", "J", "", "MAX_FREQUENTLY_USED_STICKERS", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreStickers.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/stores/StoreStickers$StickerPackState;", "", HookHelper.constructorName, "()V", "Loaded", "Loading", "Unknown", "Lcom/discord/stores/StoreStickers$StickerPackState$Unknown;", "Lcom/discord/stores/StoreStickers$StickerPackState$Loading;", "Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class StickerPackState {

        /* compiled from: StoreStickers.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;", "Lcom/discord/stores/StoreStickers$StickerPackState;", "Lcom/discord/models/sticker/dto/ModelStickerPack;", "component1", "()Lcom/discord/models/sticker/dto/ModelStickerPack;", "stickerPack", "copy", "(Lcom/discord/models/sticker/dto/ModelStickerPack;)Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/sticker/dto/ModelStickerPack;", "getStickerPack", HookHelper.constructorName, "(Lcom/discord/models/sticker/dto/ModelStickerPack;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Loaded extends StickerPackState {
            private final ModelStickerPack stickerPack;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loaded(ModelStickerPack modelStickerPack) {
                super(null);
                m.checkNotNullParameter(modelStickerPack, "stickerPack");
                this.stickerPack = modelStickerPack;
            }

            public static /* synthetic */ Loaded copy$default(Loaded loaded, ModelStickerPack modelStickerPack, int i, Object obj) {
                if ((i & 1) != 0) {
                    modelStickerPack = loaded.stickerPack;
                }
                return loaded.copy(modelStickerPack);
            }

            public final ModelStickerPack component1() {
                return this.stickerPack;
            }

            public final Loaded copy(ModelStickerPack modelStickerPack) {
                m.checkNotNullParameter(modelStickerPack, "stickerPack");
                return new Loaded(modelStickerPack);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Loaded) && m.areEqual(this.stickerPack, ((Loaded) obj).stickerPack);
                }
                return true;
            }

            public final ModelStickerPack getStickerPack() {
                return this.stickerPack;
            }

            public int hashCode() {
                ModelStickerPack modelStickerPack = this.stickerPack;
                if (modelStickerPack != null) {
                    return modelStickerPack.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(stickerPack=");
                R.append(this.stickerPack);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: StoreStickers.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreStickers$StickerPackState$Loading;", "Lcom/discord/stores/StoreStickers$StickerPackState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Loading extends StickerPackState {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        /* compiled from: StoreStickers.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/stores/StoreStickers$StickerPackState$Unknown;", "Lcom/discord/stores/StoreStickers$StickerPackState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Unknown extends StickerPackState {
            public static final Unknown INSTANCE = new Unknown();

            private Unknown() {
                super(null);
            }
        }

        private StickerPackState() {
        }

        public /* synthetic */ StickerPackState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ StoreStickers(Dispatcher dispatcher, RestAPI restAPI, ObservationDeck observationDeck, Clock clock, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(dispatcher, (i & 2) != 0 ? RestAPI.Companion.getApi() : restAPI, (i & 4) != 0 ? ObservationDeckProvider.get() : observationDeck, (i & 8) != 0 ? ClockFactory.get() : clock);
    }

    public final void fetchEnabledStickerDirectory() {
        if (this.lastFetchedEnabledPacks + 300000 < this.clock.currentTimeMillis()) {
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(this.api.getStickerPacks(), false, 1, null), StoreStickers.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreStickers$fetchEnabledStickerDirectory$1(this));
        }
    }

    public final void fetchStickerPack(long j) {
        this.dispatcher.schedule(new StoreStickers$fetchStickerPack$1(this, j));
    }

    public final List<ModelStickerPack> getEnabledStickerPacks() {
        return this.enabledStickerPacksSnapshot;
    }

    public final List<Sticker> getEnabledStickers() {
        return this.enabledStickerPacksStickersSnapshot;
    }

    public final StickerPackState getStickerPack(long j) {
        return this.stickerPacksSnapshot.get(Long.valueOf(j));
    }

    public final Map<Long, StickerPackState> getStickerPacks() {
        return this.stickerPacksSnapshot;
    }

    public final Map<Long, Sticker> getStickers() {
        return this.stickersSnapshot;
    }

    public final void handleDeletedStickers(List<Long> list) {
        m.checkNotNullParameter(list, "stickerIds");
        this.dispatcher.schedule(new StoreStickers$handleDeletedStickers$1(this, list));
    }

    public final void handleFetchedSticker(Sticker sticker) {
        if (sticker != null && sticker.k() == StickerType.STANDARD) {
            Long i = sticker.i();
            m.checkNotNull(i);
            fetchStickerPack(i.longValue());
        }
    }

    @StoreThread
    public final void handleNewEnabledStickerDirectory(List<ModelStickerPack> list) {
        m.checkNotNullParameter(list, "enabledPacks");
        handleNewLoadedStickerPacks(list);
        this.enabledStickerPacks = list;
        markChanged();
    }

    @StoreThread
    public final void handleNewLoadedStickerPacks(List<ModelStickerPack> list) {
        m.checkNotNullParameter(list, "newStickerPacks");
        Map<Long, ? extends StickerPackState> mutableMap = h0.toMutableMap(this.stickerPacks);
        for (ModelStickerPack modelStickerPack : list) {
            StickerPackState stickerPackState = this.stickerPacks.get(Long.valueOf(modelStickerPack.getId()));
            if (stickerPackState == null || !(stickerPackState instanceof StickerPackState.Loaded) || ((StickerPackState.Loaded) stickerPackState).getStickerPack().getStoreListing() == null) {
                mutableMap.put(Long.valueOf(modelStickerPack.getId()), new StickerPackState.Loaded(modelStickerPack));
            }
        }
        this.stickerPacks = mutableMap;
        markChanged();
    }

    @StoreThread
    public final void handleNewLoadingStickerPacks(List<Long> list) {
        m.checkNotNullParameter(list, "stickerPackIds");
        Map<Long, ? extends StickerPackState> mutableMap = h0.toMutableMap(this.stickerPacks);
        for (Number number : list) {
            mutableMap.put(Long.valueOf(number.longValue()), StickerPackState.Loading.INSTANCE);
        }
        this.stickerPacks = mutableMap;
        markChanged();
    }

    @StoreThread
    public final void handlePreLogout() {
        Persister.clear$default(this.frecencyCache, false, 1, null);
    }

    public final void init() {
        fetchEnabledStickerDirectory();
    }

    public final Observable<List<ModelStickerPack>> observeEnabledStickerPacks() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreStickers$observeEnabledStickerPacks$1(this), 14, null);
    }

    public final Observable<List<Long>> observeFrequentlyUsedStickerIds() {
        Observable F = this.frecencyCache.getObservable().F(StoreStickers$observeFrequentlyUsedStickerIds$1.INSTANCE);
        m.checkNotNullExpressionValue(F, "frecencyCache.getObserva…ckerId.toLong() }\n      }");
        return F;
    }

    public final Observable<StickerPackState> observeStickerPack(long j) {
        this.dispatcher.schedule(new StoreStickers$observeStickerPack$1(this, j));
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreStickers$observeStickerPack$2(this, j), 14, null);
    }

    public final Observable<List<StickerPackState>> observeStickerPacks() {
        return ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreStickers$observeStickerPacks$1(this), 14, null);
    }

    public final void onStickerUsed(Sticker sticker) {
        m.checkNotNullParameter(sticker, "sticker");
        this.dispatcher.schedule(new StoreStickers$onStickerUsed$1(this, sticker));
    }

    @Override // com.discord.stores.StoreV2
    @StoreThread
    public void snapshotData() {
        HashMap hashMap = new HashMap(this.stickerPacks);
        this.stickerPacksSnapshot = hashMap;
        Collection values = hashMap.values();
        ArrayList<StickerPackState.Loaded> arrayList = new ArrayList();
        for (Object obj : values) {
            if (obj instanceof StickerPackState.Loaded) {
                arrayList.add(obj);
            }
        }
        ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(arrayList, 10));
        for (StickerPackState.Loaded loaded : arrayList) {
            arrayList2.add(loaded.getStickerPack().getStickers());
        }
        List flatten = o.flatten(arrayList2);
        LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(o.collectionSizeOrDefault(flatten, 10)), 16));
        for (Object obj2 : flatten) {
            linkedHashMap.put(Long.valueOf(((Sticker) obj2).getId()), obj2);
        }
        this.stickersSnapshot = linkedHashMap;
        this.enabledStickerPacksSnapshot = new ArrayList(this.enabledStickerPacks);
        List<ModelStickerPack> list = this.enabledStickerPacks;
        ArrayList arrayList3 = new ArrayList(o.collectionSizeOrDefault(list, 10));
        for (ModelStickerPack modelStickerPack : list) {
            arrayList3.add(modelStickerPack.getStickers());
        }
        this.enabledStickerPacksStickersSnapshot = new ArrayList(o.flatten(arrayList3));
    }

    public StoreStickers(Dispatcher dispatcher, RestAPI restAPI, ObservationDeck observationDeck, Clock clock) {
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(restAPI, "api");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(clock, "clock");
        this.dispatcher = dispatcher;
        this.api = restAPI;
        this.observationDeck = observationDeck;
        this.clock = clock;
        this.stickerPacks = h0.emptyMap();
        this.stickerPacksSnapshot = h0.emptyMap();
        this.stickersSnapshot = h0.emptyMap();
        Persister<MediaFrecencyTracker> persister = new Persister<>("STICKER_HISTORY_V1", new MediaFrecencyTracker(20, 1));
        this.frecencyCache = persister;
        this.frecency = persister.get();
        this.enabledStickerPacks = n.emptyList();
        this.enabledStickerPacksSnapshot = n.emptyList();
        this.enabledStickerPacksStickersSnapshot = n.emptyList();
    }
}
