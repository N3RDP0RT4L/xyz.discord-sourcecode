package com.discord.stores;

import andhook.lib.HookHelper;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.os.PowerManager;
import b.a.q.i0;
import b.a.q.k0.g;
import b.c.a.a0.d;
import b.i.a.f.e.o.f;
import co.discord.media_engine.RtcRegion;
import co.discord.media_engine.VideoInputDeviceDescription;
import co.discord.media_engine.VideoInputDeviceFacing;
import com.discord.app.AppLog;
import com.discord.models.domain.ModelPayload;
import com.discord.models.domain.ModelRtcLatencyRegion;
import com.discord.rtcconnection.KrispOveruseDetector;
import com.discord.rtcconnection.LowMemoryDetector;
import com.discord.rtcconnection.mediaengine.MediaEngine;
import com.discord.rtcconnection.mediaengine.MediaEngineConnection;
import com.discord.stores.StoreMediaSettings;
import com.discord.utilities.collections.ListenerCollection;
import com.discord.utilities.collections.ListenerCollectionSubject;
import com.discord.utilities.lifecycle.ApplicationProvider;
import com.discord.utilities.persister.Persister;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$1;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$2;
import com.discord.utilities.systemlog.SystemLogUtils;
import com.hammerandchisel.libdiscord.Discord;
import d0.t.j;
import d0.t.k;
import d0.t.o;
import d0.t.u;
import d0.w.h.b;
import d0.w.h.c;
import d0.w.i.a.g;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlinx.coroutines.CoroutineDispatcher;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action0;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import s.a.a.n;
import s.a.k0;
import s.a.l;
import s.a.x0;
/* compiled from: StoreMediaEngine.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000æ\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \u0094\u00012\u00020\u0001:\b\u0094\u0001\u0095\u0001\u0096\u0001\u0097\u0001B&\u0012\b\u0010\u008d\u0001\u001a\u00030\u008c\u0001\u0012\b\u0010\u0085\u0001\u001a\u00030\u0084\u0001\u0012\u0007\u0010\u0080\u0001\u001a\u00020\u007f¢\u0006\u0006\b\u0092\u0001\u0010\u0093\u0001J)\u0010\u0007\u001a\u00020\u00052\u0018\u0010\u0006\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0004\u0012\u00020\u00050\u0002H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\t\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0019\u0010\r\u001a\u00020\u00052\b\u0010\f\u001a\u0004\u0018\u00010\u000bH\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u000f\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u000f\u0010\nJ\u0017\u0010\u0012\u001a\u00020\u00052\u0006\u0010\u0011\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u0012\u0010\u0013JA\u0010\u0018\u001a\u00020\u00052\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\u0010\u0016\u001a\u0004\u0018\u00010\u00152\u0018\b\u0002\u0010\u0017\u001a\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0015\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u0019\u0010\u001b\u001a\u00020\u00052\b\u0010\u001a\u001a\u0004\u0018\u00010\u0004H\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ\u000f\u0010\u001d\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u001d\u0010\nJ\u000f\u0010\u001e\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u001e\u0010\nJ\u000f\u0010\u001f\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u001f\u0010\nJ\u001b\u0010 \u001a\u0004\u0018\u00010\u0015*\b\u0012\u0004\u0012\u00020\u00040\u0003H\u0002¢\u0006\u0004\b \u0010!J\u0013\u0010$\u001a\b\u0012\u0004\u0012\u00020#0\"¢\u0006\u0004\b$\u0010%J\u0017\u0010(\u001a\u00020\u00052\u0006\u0010'\u001a\u00020&H\u0016¢\u0006\u0004\b(\u0010)J\u0015\u0010,\u001a\u00020\u00052\u0006\u0010+\u001a\u00020*¢\u0006\u0004\b,\u0010-J\u0019\u0010/\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040.0\"¢\u0006\u0004\b/\u0010%J\u0015\u00100\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00040\"¢\u0006\u0004\b0\u0010%J\u000f\u00101\u001a\u0004\u0018\u00010\u0004¢\u0006\u0004\b1\u00102J\u0017\u00103\u001a\u00020\u00052\b\u0010\u0016\u001a\u0004\u0018\u00010\u0015¢\u0006\u0004\b3\u00104J\u0013\u00106\u001a\b\u0012\u0004\u0012\u0002050\"¢\u0006\u0004\b6\u0010%J\u0013\u00108\u001a\b\u0012\u0004\u0012\u0002070\"¢\u0006\u0004\b8\u0010%J'\u00109\u001a\u00020\u00052\u0018\b\u0002\u0010\u0017\u001a\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0015\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0002¢\u0006\u0004\b9\u0010\bJ\u0015\u0010:\u001a\u0004\u0018\u00010\u0015H\u0086@ø\u0001\u0000¢\u0006\u0004\b:\u0010;J\u0015\u0010<\u001a\u0004\u0018\u00010\u0015H\u0086@ø\u0001\u0000¢\u0006\u0004\b<\u0010;J\u0015\u0010>\u001a\u00020\u00052\u0006\u0010=\u001a\u000207¢\u0006\u0004\b>\u0010?J\u000f\u0010A\u001a\u0004\u0018\u00010@¢\u0006\u0004\bA\u0010BJ\r\u0010C\u001a\u00020\u0005¢\u0006\u0004\bC\u0010\nJ\u0017\u0010E\u001a\u00020\u00052\u0006\u0010D\u001a\u000205H\u0007¢\u0006\u0004\bE\u0010FJ\u0019\u0010G\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u0082@ø\u0001\u0000¢\u0006\u0004\bG\u0010;J\u0019\u0010H\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u0082@ø\u0001\u0000¢\u0006\u0004\bH\u0010;J\r\u0010I\u001a\u000207¢\u0006\u0004\bI\u0010JJ\u0019\u0010N\u001a\u00020\u00052\n\u0010M\u001a\u00060Kj\u0002`L¢\u0006\u0004\bN\u0010OJ\u0015\u0010Q\u001a\u00020\u00052\u0006\u0010P\u001a\u000207¢\u0006\u0004\bQ\u0010?J5\u0010T\u001a\u00020\u00052\f\u0010S\u001a\b\u0012\u0004\u0012\u00020R0.2\u0018\u0010\u0006\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00150.\u0012\u0004\u0012\u00020\u00050\u0002¢\u0006\u0004\bT\u0010UJ\r\u0010V\u001a\u00020\u0005¢\u0006\u0004\bV\u0010\nR\u001f\u0010X\u001a\b\u0012\u0004\u0012\u00020W0\"8\u0006@\u0006¢\u0006\f\n\u0004\bX\u0010Y\u001a\u0004\bZ\u0010%R$\u0010]\u001a\u00020[2\u0006\u0010\\\u001a\u00020[8\u0006@BX\u0086.¢\u0006\f\n\u0004\b]\u0010^\u001a\u0004\b_\u0010`R2\u0010c\u001a\u001e\u0012\f\u0012\n b*\u0004\u0018\u00010W0W\u0012\f\u0012\n b*\u0004\u0018\u00010W0W0a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bc\u0010dR\u0018\u0010f\u001a\u0004\u0018\u00010e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bf\u0010gR\u001c\u0010i\u001a\b\u0012\u0004\u0012\u00020\u00150h8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bi\u0010jRR\u0010l\u001a>\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0004 b*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010.0. b*\u001e\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0004 b*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010.0.\u0018\u00010k0k8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bl\u0010mR\u001f\u0010p\u001a\b\u0012\u0004\u0012\u00020o0n8\u0006@\u0006¢\u0006\f\n\u0004\bp\u0010q\u001a\u0004\br\u0010sR\u0016\u0010I\u001a\u0002078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bI\u0010tR2\u0010u\u001a\u001e\u0012\f\u0012\n b*\u0004\u0018\u00010707\u0012\f\u0012\n b*\u0004\u0018\u000107070a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bu\u0010dR\u001e\u0010v\u001a\n\u0018\u00010Kj\u0004\u0018\u0001`L8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bv\u0010wR\u0016\u0010x\u001a\u00020\u00158\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bx\u0010yR\u0018\u0010\u001a\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001a\u0010zR\u0016\u0010{\u001a\u0002078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b{\u0010tR:\u0010}\u001a&\u0012\f\u0012\n b*\u0004\u0018\u00010#0# b*\u0012\u0012\f\u0012\n b*\u0004\u0018\u00010#0#\u0018\u00010|0|8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b}\u0010~R\u0019\u0010\u0080\u0001\u001a\u00020\u007f8\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0080\u0001\u0010\u0081\u0001R4\u0010\u0082\u0001\u001a\u001e\u0012\f\u0012\n b*\u0004\u0018\u00010707\u0012\f\u0012\n b*\u0004\u0018\u000107070a8\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b\u0082\u0001\u0010dR\u001d\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00040\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u0014\u0010\u0083\u0001R\u001a\u0010\u0085\u0001\u001a\u00030\u0084\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0085\u0001\u0010\u0086\u0001R0\u0010\u0087\u0001\u001a\u001a\u0012\u0006\u0012\u0004\u0018\u00010\u0004 b*\f\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0018\u00010k0k8\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b\u0087\u0001\u0010mR4\u0010\u0088\u0001\u001a\u001e\u0012\f\u0012\n b*\u0004\u0018\u00010505\u0012\f\u0012\n b*\u0004\u0018\u000105050a8\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b\u0088\u0001\u0010dR\u0019\u0010\u0089\u0001\u001a\u00020K8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u0089\u0001\u0010\u008a\u0001R\u001e\u0010\u008b\u0001\u001a\b\u0012\u0004\u0012\u0002070h8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u008b\u0001\u0010jR\u001a\u0010\u008d\u0001\u001a\u00030\u008c\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u008d\u0001\u0010\u008e\u0001R \u0010\u0090\u0001\u001a\t\u0012\u0004\u0012\u00020o0\u008f\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0090\u0001\u0010\u0091\u0001\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0098\u0001"}, d2 = {"Lcom/discord/stores/StoreMediaEngine;", "Lcom/discord/stores/Store;", "Lkotlin/Function1;", "", "Lco/discord/media_engine/VideoInputDeviceDescription;", "", "callback", "getVideoInputDevicesNative", "(Lkotlin/jvm/functions/Function1;)V", "setupMediaEngineSettingsSubscription", "()V", "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "voiceConfig", "handleVoiceConfigChanged", "(Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;)V", "handleNativeEngineInitialized", "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;", "connection", "handleNewConnection", "(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;)V", "videoInputDevices", "", "deviceGUID", "onSelected", "handleVideoInputDevices", "([Lco/discord/media_engine/VideoInputDeviceDescription;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V", "selectedVideoInputDevice", "updateSelectedVideoInputDevice", "(Lco/discord/media_engine/VideoInputDeviceDescription;)V", "enableLocalVoiceStatusListening", "disableLocalVoiceStatusListening", "restartLocalMicrophone", "pickDefaultDeviceGUID", "([Lco/discord/media_engine/VideoInputDeviceDescription;)Ljava/lang/String;", "Lrx/Observable;", "Lcom/discord/rtcconnection/KrispOveruseDetector$Status;", "onKrispStatusEvent", "()Lrx/Observable;", "Landroid/content/Context;", "context", "init", "(Landroid/content/Context;)V", "Lcom/discord/models/domain/ModelPayload;", "payload", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "", "getVideoInputDevices", "getSelectedVideoInputDevice", "getSelectedVideoInputDeviceBlocking", "()Lco/discord/media_engine/VideoInputDeviceDescription;", "selectVideoInputDevice", "(Ljava/lang/String;)V", "Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLESConfig;", "getOpenSLESConfig", "", "getIsNativeEngineInitialized", "selectDefaultVideoDevice", "selectDefaultVideoDeviceAsync", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getDefaultVideoDeviceGUID", "active", "setPttActive", "(Z)V", "Lcom/hammerandchisel/libdiscord/Discord;", "getVoiceEngineNative", "()Lcom/hammerandchisel/libdiscord/Discord;", "cycleVideoInputDevice", "openSLESConfig", "setOpenSLESConfig", "(Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLESConfig;)V", "getVideoInputDevicesNativeAsync", "awaitVideoInputDevicesNativeAsync", "hasNativeEngineEverInitialized", "()Z", "", "Lcom/discord/primitives/ChannelId;", "channelId", "handleVoiceChannelSelected", "(J)V", "audioInputEnabled", "setAudioInputEnabled", "Lcom/discord/models/domain/ModelRtcLatencyRegion;", "regionsWithIps", "getRankedRtcRegions", "(Ljava/util/List;Lkotlin/jvm/functions/Function1;)V", "handleMicrophonePermissionGranted", "Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;", "localVoiceStatus", "Lrx/Observable;", "getLocalVoiceStatus", "Lcom/discord/rtcconnection/mediaengine/MediaEngine;", "<set-?>", "mediaEngine", "Lcom/discord/rtcconnection/mediaengine/MediaEngine;", "getMediaEngine", "()Lcom/discord/rtcconnection/mediaengine/MediaEngine;", "Lrx/subjects/SerializedSubject;", "kotlin.jvm.PlatformType", "localVoiceStatusSubject", "Lrx/subjects/SerializedSubject;", "Lrx/Subscription;", "mediaEngineSettingsSubscription", "Lrx/Subscription;", "Lcom/discord/utilities/persister/Persister;", "preferredVideoInputDeviceGuidCache", "Lcom/discord/utilities/persister/Persister;", "Lrx/subjects/BehaviorSubject;", "videoInputDevicesSubject", "Lrx/subjects/BehaviorSubject;", "Lcom/discord/utilities/collections/ListenerCollection;", "Lcom/discord/stores/StoreMediaEngine$Listener;", "listeners", "Lcom/discord/utilities/collections/ListenerCollection;", "getListeners", "()Lcom/discord/utilities/collections/ListenerCollection;", "Z", "pttActiveSubject", "previousVoiceChannelId", "Ljava/lang/Long;", "preferredVideoInputDeviceGUID", "Ljava/lang/String;", "Lco/discord/media_engine/VideoInputDeviceDescription;", "hasTimedOutAwaitingDevice", "Lrx/subjects/PublishSubject;", "onKrispStatusSubject", "Lrx/subjects/PublishSubject;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "isNativeEngineInitializedSubject", "[Lco/discord/media_engine/VideoInputDeviceDescription;", "Lcom/discord/stores/StoreStream;", "storeStream", "Lcom/discord/stores/StoreStream;", "selectedVideoInputDeviceSubject", "openSLESConfigSubject", "userId", "J", "hasNativeEngineEverInitializedCache", "Lcom/discord/stores/StoreMediaSettings;", "mediaSettingsStore", "Lcom/discord/stores/StoreMediaSettings;", "Lcom/discord/utilities/collections/ListenerCollectionSubject;", "listenerSubject", "Lcom/discord/utilities/collections/ListenerCollectionSubject;", HookHelper.constructorName, "(Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;)V", "Companion", "DefaultListener", "EngineListener", "Listener", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreMediaEngine extends Store {
    private static final String DEFAULT_VIDEO_DEVICE_GUID = "";
    private static final long MAX_WAIT_FOR_DEVICES_MS = 750;
    private final Dispatcher dispatcher;
    private boolean hasTimedOutAwaitingDevice;
    private final SerializedSubject<Boolean, Boolean> isNativeEngineInitializedSubject;
    private final ListenerCollectionSubject<Listener> listenerSubject;
    private final ListenerCollection<Listener> listeners;
    private final Observable<MediaEngine.LocalVoiceStatus> localVoiceStatus;
    private final SerializedSubject<MediaEngine.LocalVoiceStatus, MediaEngine.LocalVoiceStatus> localVoiceStatusSubject;
    private MediaEngine mediaEngine;
    private Subscription mediaEngineSettingsSubscription;
    private final StoreMediaSettings mediaSettingsStore;
    private Long previousVoiceChannelId;
    private final SerializedSubject<Boolean, Boolean> pttActiveSubject;
    private final StoreStream storeStream;
    private VideoInputDeviceDescription[] videoInputDevices;
    private final BehaviorSubject<List<VideoInputDeviceDescription>> videoInputDevicesSubject;
    public static final Companion Companion = new Companion(null);
    private static final MediaEngine.LocalVoiceStatus LOCAL_VOICE_STATUS_DEFAULT = new MediaEngine.LocalVoiceStatus(-100.0f, false);
    private static final MediaEngine.OpenSLESConfig DEFAULT_OPENSLES_CONFIG = MediaEngine.OpenSLESConfig.DEFAULT;
    private String preferredVideoInputDeviceGUID = "";
    private final Persister<String> preferredVideoInputDeviceGuidCache = new Persister<>("PREFERRED_VIDEO_INPUT_DEVICE_GUID", this.preferredVideoInputDeviceGUID);
    private VideoInputDeviceDescription selectedVideoInputDevice;
    private final BehaviorSubject<VideoInputDeviceDescription> selectedVideoInputDeviceSubject = BehaviorSubject.l0(this.selectedVideoInputDevice);
    private final SerializedSubject<MediaEngine.OpenSLESConfig, MediaEngine.OpenSLESConfig> openSLESConfigSubject = new SerializedSubject<>(BehaviorSubject.k0());
    private long userId = -1;
    private boolean hasNativeEngineEverInitialized;
    private Persister<Boolean> hasNativeEngineEverInitializedCache = new Persister<>("CACHE_KEY_NATIVE_ENGINE_EVER_INITIALIZED", Boolean.valueOf(this.hasNativeEngineEverInitialized));
    private final PublishSubject<KrispOveruseDetector.Status> onKrispStatusSubject = PublishSubject.k0();

    /* compiled from: StoreMediaEngine.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007R\u0016\u0010\t\u001a\u00020\b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\t\u0010\nR\u0016\u0010\f\u001a\u00020\u000b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\f\u0010\r¨\u0006\u0010"}, d2 = {"Lcom/discord/stores/StoreMediaEngine$Companion;", "", "Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLESConfig;", "DEFAULT_OPENSLES_CONFIG", "Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLESConfig;", "", "DEFAULT_VIDEO_DEVICE_GUID", "Ljava/lang/String;", "Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;", "LOCAL_VOICE_STATUS_DEFAULT", "Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;", "", "MAX_WAIT_FOR_DEVICES_MS", "J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreMediaEngine.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\b&\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u0006\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/stores/StoreMediaEngine$DefaultListener;", "Lcom/discord/stores/StoreMediaEngine$Listener;", "", "onConnecting", "()V", "onConnected", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class DefaultListener implements Listener {
        @Override // com.discord.stores.StoreMediaEngine.Listener
        public void onConnected() {
        }

        @Override // com.discord.stores.StoreMediaEngine.Listener
        public void onConnecting() {
        }
    }

    /* compiled from: StoreMediaEngine.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u0017\u0010\u0018J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\t\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\t\u0010\u0004J\u000f\u0010\n\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\n\u0010\u0004J\u000f\u0010\u000b\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u000b\u0010\u0004R\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u001a\u0010\u0011\u001a\u00060\u000fj\u0002`\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0016\u001a\u00020\u00138B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0015¨\u0006\u0019"}, d2 = {"Lcom/discord/stores/StoreMediaEngine$EngineListener;", "Lcom/discord/rtcconnection/mediaengine/MediaEngine$c;", "", "onNativeEngineInitialized", "()V", "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;", "connection", "onNewConnection", "(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;)V", "onDestroy", "onConnecting", "onConnected", "Lcom/discord/rtcconnection/LowMemoryDetector;", "lowMemoryDetector", "Lcom/discord/rtcconnection/LowMemoryDetector;", "", "Lcom/discord/utilities/debug/DebugPrintableId;", "debugPrintableId", "J", "Landroid/content/Context;", "getContext", "()Landroid/content/Context;", "context", HookHelper.constructorName, "(Lcom/discord/stores/StoreMediaEngine;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final class EngineListener implements MediaEngine.c {
        private final LowMemoryDetector lowMemoryDetector = new LowMemoryDetector();
        private long debugPrintableId = -1;

        public EngineListener() {
        }

        private final Context getContext() {
            return ApplicationProvider.INSTANCE.get();
        }

        @Override // com.discord.rtcconnection.mediaengine.MediaEngine.c
        public void onConnected() {
            StoreMediaEngine.this.listenerSubject.notify(StoreMediaEngine$EngineListener$onConnected$1.INSTANCE);
        }

        @Override // com.discord.rtcconnection.mediaengine.MediaEngine.c
        public void onConnecting() {
            StoreMediaEngine.this.listenerSubject.notify(StoreMediaEngine$EngineListener$onConnecting$1.INSTANCE);
        }

        public void onDestroy() {
            getContext().unregisterComponentCallbacks(this.lowMemoryDetector);
            if (Build.VERSION.SDK_INT >= 29) {
                i0 i0Var = i0.n;
                synchronized (i0Var) {
                    if (i0.k) {
                        d.b1("ThermalDetector", "unregister");
                        Object systemService = ApplicationProvider.INSTANCE.get().getSystemService("power");
                        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.os.PowerManager");
                        ((PowerManager) systemService).removeThermalStatusListener(i0Var);
                        i0.k = false;
                        i0.l = false;
                        if (i0.m) {
                            ((Handler) i0.j.getValue()).removeCallbacks(i0Var);
                            i0.m = false;
                        }
                    }
                }
                SystemLogUtils.INSTANCE.getDebugPrintables$app_productionGoogleRelease().remove(this.debugPrintableId);
                this.debugPrintableId = -1L;
            }
        }

        @Override // com.discord.rtcconnection.mediaengine.MediaEngine.c
        public void onNativeEngineInitialized() {
            getContext().registerComponentCallbacks(this.lowMemoryDetector);
            if (Build.VERSION.SDK_INT >= 29) {
                i0 i0Var = i0.n;
                synchronized (i0Var) {
                    if (!i0.k) {
                        d.b1("ThermalDetector", "register");
                        Object systemService = ApplicationProvider.INSTANCE.get().getSystemService("power");
                        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.os.PowerManager");
                        ((PowerManager) systemService).addThermalStatusListener(i0Var);
                        i0.k = true;
                    }
                }
                this.debugPrintableId = SystemLogUtils.INSTANCE.getDebugPrintables$app_productionGoogleRelease().add(i0Var, "ThermalDetector");
            }
            StoreMediaEngine.this.handleNativeEngineInitialized();
        }

        @Override // com.discord.rtcconnection.mediaengine.MediaEngine.c
        public void onNewConnection(MediaEngineConnection mediaEngineConnection) {
            m.checkNotNullParameter(mediaEngineConnection, "connection");
            StoreMediaEngine.this.handleNewConnection(mediaEngineConnection);
        }
    }

    /* compiled from: StoreMediaEngine.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001J\u000f\u0010\u0003\u001a\u00020\u0002H&¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H&¢\u0006\u0004\b\u0005\u0010\u0004¨\u0006\u0006"}, d2 = {"Lcom/discord/stores/StoreMediaEngine$Listener;", "", "", "onConnecting", "()V", "onConnected", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public interface Listener {
        void onConnected();

        void onConnecting();
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            KrispOveruseDetector.Status.values();
            int[] iArr = new int[3];
            $EnumSwitchMapping$0 = iArr;
            iArr[KrispOveruseDetector.Status.FAILED.ordinal()] = 1;
            iArr[KrispOveruseDetector.Status.CPU_OVERUSE.ordinal()] = 2;
            iArr[KrispOveruseDetector.Status.VAD_CPU_OVERUSE.ordinal()] = 3;
            MediaEngineConnection.Type.values();
            int[] iArr2 = new int[2];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[MediaEngineConnection.Type.DEFAULT.ordinal()] = 1;
            iArr2[MediaEngineConnection.Type.STREAM.ordinal()] = 2;
        }
    }

    public StoreMediaEngine(StoreMediaSettings storeMediaSettings, StoreStream storeStream, Dispatcher dispatcher) {
        m.checkNotNullParameter(storeMediaSettings, "mediaSettingsStore");
        m.checkNotNullParameter(storeStream, "storeStream");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        this.mediaSettingsStore = storeMediaSettings;
        this.storeStream = storeStream;
        this.dispatcher = dispatcher;
        ListenerCollectionSubject<Listener> listenerCollectionSubject = new ListenerCollectionSubject<>();
        this.listenerSubject = listenerCollectionSubject;
        this.listeners = listenerCollectionSubject;
        SerializedSubject<MediaEngine.LocalVoiceStatus, MediaEngine.LocalVoiceStatus> serializedSubject = new SerializedSubject<>(BehaviorSubject.l0(LOCAL_VOICE_STATUS_DEFAULT));
        this.localVoiceStatusSubject = serializedSubject;
        Boolean bool = Boolean.FALSE;
        this.pttActiveSubject = new SerializedSubject<>(BehaviorSubject.l0(bool));
        VideoInputDeviceDescription[] videoInputDeviceDescriptionArr = new VideoInputDeviceDescription[0];
        this.videoInputDevices = videoInputDeviceDescriptionArr;
        this.videoInputDevicesSubject = BehaviorSubject.l0(j.asList(videoInputDeviceDescriptionArr));
        this.isNativeEngineInitializedSubject = new SerializedSubject<>(BehaviorSubject.l0(bool));
        Observable q = ObservableExtensionsKt.computationLatest(serializedSubject).q();
        final StoreMediaEngine$localVoiceStatus$1 storeMediaEngine$localVoiceStatus$1 = new StoreMediaEngine$localVoiceStatus$1(this);
        Observable u = q.u(new Action0() { // from class: com.discord.stores.StoreMediaEngine$sam$rx_functions_Action0$0
            @Override // rx.functions.Action0
            public final /* synthetic */ void call() {
                m.checkNotNullExpressionValue(Function0.this.invoke(), "invoke(...)");
            }
        });
        final StoreMediaEngine$localVoiceStatus$2 storeMediaEngine$localVoiceStatus$2 = new StoreMediaEngine$localVoiceStatus$2(this);
        Observable<MediaEngine.LocalVoiceStatus> Q = u.v(new Action0() { // from class: com.discord.stores.StoreMediaEngine$sam$rx_functions_Action0$0
            @Override // rx.functions.Action0
            public final /* synthetic */ void call() {
                m.checkNotNullExpressionValue(Function0.this.invoke(), "invoke(...)");
            }
        }).Q();
        m.checkNotNullExpressionValue(Q, "localVoiceStatusSubject\n…ening)\n          .share()");
        this.localVoiceStatus = Q;
    }

    public static final /* synthetic */ MediaEngine access$getMediaEngine$p(StoreMediaEngine storeMediaEngine) {
        MediaEngine mediaEngine = storeMediaEngine.mediaEngine;
        if (mediaEngine == null) {
            m.throwUninitializedPropertyAccessException("mediaEngine");
        }
        return mediaEngine;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void disableLocalVoiceStatusListening() {
        MediaEngine mediaEngine = this.mediaEngine;
        if (mediaEngine == null) {
            m.throwUninitializedPropertyAccessException("mediaEngine");
        }
        mediaEngine.l(null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void enableLocalVoiceStatusListening() {
        MediaEngine mediaEngine = this.mediaEngine;
        if (mediaEngine == null) {
            m.throwUninitializedPropertyAccessException("mediaEngine");
        }
        mediaEngine.l(new StoreMediaEngine$enableLocalVoiceStatusListening$1(this.localVoiceStatusSubject));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void getVideoInputDevicesNative(Function1<? super VideoInputDeviceDescription[], Unit> function1) {
        MediaEngine mediaEngine = this.mediaEngine;
        if (mediaEngine == null) {
            m.throwUninitializedPropertyAccessException("mediaEngine");
        }
        mediaEngine.j(new StoreMediaEngine$getVideoInputDevicesNative$1(function1));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void handleNativeEngineInitialized() {
        this.hasNativeEngineEverInitialized = true;
        Persister<Boolean> persister = this.hasNativeEngineEverInitializedCache;
        Boolean bool = Boolean.TRUE;
        persister.set(bool, true);
        this.isNativeEngineInitializedSubject.k.onNext(bool);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void handleNewConnection(MediaEngineConnection mediaEngineConnection) {
        setupMediaEngineSettingsSubscription();
        mediaEngineConnection.l(new MediaEngineConnection.a() { // from class: com.discord.stores.StoreMediaEngine$handleNewConnection$1
            @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.a, com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
            public void onDestroy(MediaEngineConnection mediaEngineConnection2) {
                StoreMediaSettings storeMediaSettings;
                m.checkNotNullParameter(mediaEngineConnection2, "connection");
                if (u.minus(StoreMediaEngine.this.getMediaEngine().getConnections(), mediaEngineConnection2).isEmpty()) {
                    storeMediaSettings = StoreMediaEngine.this.mediaSettingsStore;
                    storeMediaSettings.revertTemporaryDisableKrisp();
                }
            }

            @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.a, com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
            public void onKrispStatus(MediaEngineConnection mediaEngineConnection2, KrispOveruseDetector.Status status) {
                PublishSubject publishSubject;
                StoreMediaSettings storeMediaSettings;
                StoreMediaSettings storeMediaSettings2;
                m.checkNotNullParameter(mediaEngineConnection2, "connection");
                m.checkNotNullParameter(status, "status");
                AppLog.i("onKrispStatus(" + status + ')');
                publishSubject = StoreMediaEngine.this.onKrispStatusSubject;
                publishSubject.k.onNext(status);
                int ordinal = status.ordinal();
                if (ordinal == 0 || ordinal == 1) {
                    storeMediaSettings = StoreMediaEngine.this.mediaSettingsStore;
                    storeMediaSettings.setNoiseProcessing(StoreMediaSettings.NoiseProcessing.CancellationTemporarilyDisabled);
                } else if (ordinal == 2) {
                    storeMediaSettings2 = StoreMediaEngine.this.mediaSettingsStore;
                    storeMediaSettings2.setVADUseKrisp(StoreMediaSettings.VadUseKrisp.TemporarilyDisabled);
                }
            }
        });
        getVideoInputDevicesNative(new StoreMediaEngine$handleNewConnection$2(this));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void handleVideoInputDevices(VideoInputDeviceDescription[] videoInputDeviceDescriptionArr, String str, Function1<? super String, Unit> function1) {
        String str2;
        int length = videoInputDeviceDescriptionArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                i = -1;
                break;
            } else if (m.areEqual(videoInputDeviceDescriptionArr[i].getGuid(), str)) {
                break;
            } else {
                i++;
            }
        }
        boolean z2 = i >= 0;
        MediaEngine mediaEngine = this.mediaEngine;
        if (mediaEngine == null) {
            m.throwUninitializedPropertyAccessException("mediaEngine");
        }
        mediaEngine.f(-1);
        MediaEngine mediaEngine2 = this.mediaEngine;
        if (mediaEngine2 == null) {
            m.throwUninitializedPropertyAccessException("mediaEngine");
        }
        mediaEngine2.f(i);
        MediaEngine mediaEngine3 = this.mediaEngine;
        if (mediaEngine3 == null) {
            m.throwUninitializedPropertyAccessException("mediaEngine");
        }
        for (MediaEngineConnection mediaEngineConnection : mediaEngine3.getConnections()) {
            if (mediaEngineConnection.getType().ordinal() == 0) {
                mediaEngineConnection.j(z2);
            }
        }
        if (function1 != null) {
            if (!z2) {
                str = null;
            }
            function1.invoke(str);
        }
        updateSelectedVideoInputDevice(z2 ? videoInputDeviceDescriptionArr[i] : null);
        this.videoInputDevices = videoInputDeviceDescriptionArr;
        this.videoInputDevicesSubject.onNext(j.asList(videoInputDeviceDescriptionArr));
        VideoInputDeviceDescription videoInputDeviceDescription = this.selectedVideoInputDevice;
        if (videoInputDeviceDescription != null) {
            if (videoInputDeviceDescription == null || (str2 = videoInputDeviceDescription.getGuid()) == null) {
                str2 = "";
            }
            this.preferredVideoInputDeviceGUID = str2;
            String str3 = (String) Persister.set$default(this.preferredVideoInputDeviceGuidCache, str2, false, 2, null);
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void handleVideoInputDevices$default(StoreMediaEngine storeMediaEngine, VideoInputDeviceDescription[] videoInputDeviceDescriptionArr, String str, Function1 function1, int i, Object obj) {
        if ((i & 4) != 0) {
            function1 = null;
        }
        storeMediaEngine.handleVideoInputDevices(videoInputDeviceDescriptionArr, str, function1);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final synchronized void handleVoiceConfigChanged(StoreMediaSettings.VoiceConfiguration voiceConfiguration) {
        if (voiceConfiguration != null) {
            MediaEngine mediaEngine = this.mediaEngine;
            if (mediaEngine == null) {
                m.throwUninitializedPropertyAccessException("mediaEngine");
            }
            mediaEngine.d(voiceConfiguration.toMediaEngineVoiceConfig());
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final String pickDefaultDeviceGUID(VideoInputDeviceDescription[] videoInputDeviceDescriptionArr) {
        boolean z2;
        VideoInputDeviceDescription videoInputDeviceDescription;
        int length = videoInputDeviceDescriptionArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z2 = false;
                break;
            } else if (m.areEqual(videoInputDeviceDescriptionArr[i].getGuid(), this.preferredVideoInputDeviceGUID)) {
                z2 = true;
                break;
            } else {
                i++;
            }
        }
        if (z2) {
            return this.preferredVideoInputDeviceGUID;
        }
        int length2 = videoInputDeviceDescriptionArr.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length2) {
                videoInputDeviceDescription = null;
                break;
            }
            videoInputDeviceDescription = videoInputDeviceDescriptionArr[i2];
            if (videoInputDeviceDescription.getFacing() == VideoInputDeviceFacing.Front) {
                break;
            }
            i2++;
        }
        if (videoInputDeviceDescription == null) {
            videoInputDeviceDescription = (VideoInputDeviceDescription) k.firstOrNull(videoInputDeviceDescriptionArr);
        }
        if (videoInputDeviceDescription != null) {
            return videoInputDeviceDescription.getGuid();
        }
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void restartLocalMicrophone() {
        enableLocalVoiceStatusListening();
        disableLocalVoiceStatusListening();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void selectDefaultVideoDevice$default(StoreMediaEngine storeMediaEngine, Function1 function1, int i, Object obj) {
        if ((i & 1) != 0) {
            function1 = null;
        }
        storeMediaEngine.selectDefaultVideoDevice(function1);
    }

    private final synchronized void setupMediaEngineSettingsSubscription() {
        Subscription subscription = this.mediaEngineSettingsSubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        ObservableExtensionsKt.appSubscribe(this.mediaSettingsStore.getVoiceConfig(), getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new StoreMediaEngine$setupMediaEngineSettingsSubscription$3(this), (r18 & 8) != 0 ? null : StoreMediaEngine$setupMediaEngineSettingsSubscription$2.INSTANCE, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreMediaEngine$setupMediaEngineSettingsSubscription$1(this));
    }

    private final synchronized void updateSelectedVideoInputDevice(VideoInputDeviceDescription videoInputDeviceDescription) {
        this.selectedVideoInputDevice = videoInputDeviceDescription;
        this.selectedVideoInputDeviceSubject.onNext(videoInputDeviceDescription);
        this.storeStream.handleVideoInputDeviceSelected(videoInputDeviceDescription);
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0035  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x005c  */
    /* JADX WARN: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final /* synthetic */ java.lang.Object awaitVideoInputDevicesNativeAsync(kotlin.coroutines.Continuation<? super co.discord.media_engine.VideoInputDeviceDescription[]> r7) {
        /*
            r6 = this;
            boolean r0 = r7 instanceof com.discord.stores.StoreMediaEngine$awaitVideoInputDevicesNativeAsync$1
            if (r0 == 0) goto L13
            r0 = r7
            com.discord.stores.StoreMediaEngine$awaitVideoInputDevicesNativeAsync$1 r0 = (com.discord.stores.StoreMediaEngine$awaitVideoInputDevicesNativeAsync$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            com.discord.stores.StoreMediaEngine$awaitVideoInputDevicesNativeAsync$1 r0 = new com.discord.stores.StoreMediaEngine$awaitVideoInputDevicesNativeAsync$1
            r0.<init>(r6, r7)
        L18:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = d0.w.h.c.getCOROUTINE_SUSPENDED()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L35
            if (r2 != r3) goto L2d
            java.lang.Object r0 = r0.L$0
            com.discord.stores.StoreMediaEngine r0 = (com.discord.stores.StoreMediaEngine) r0
            d0.l.throwOnFailure(r7)
            goto L4c
        L2d:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r0)
            throw r7
        L35:
            d0.l.throwOnFailure(r7)
            r4 = 750(0x2ee, double:3.705E-321)
            com.discord.stores.StoreMediaEngine$awaitVideoInputDevicesNativeAsync$devices$1 r7 = new com.discord.stores.StoreMediaEngine$awaitVideoInputDevicesNativeAsync$devices$1
            r2 = 0
            r7.<init>(r6, r2)
            r0.L$0 = r6
            r0.label = r3
            java.lang.Object r7 = s.a.h.b(r4, r7, r0)
            if (r7 != r1) goto L4b
            return r1
        L4b:
            r0 = r6
        L4c:
            co.discord.media_engine.VideoInputDeviceDescription[] r7 = (co.discord.media_engine.VideoInputDeviceDescription[]) r7
            boolean r1 = r0.hasTimedOutAwaitingDevice
            r2 = 0
            if (r1 != 0) goto L57
            if (r7 != 0) goto L56
            goto L57
        L56:
            r3 = 0
        L57:
            r0.hasTimedOutAwaitingDevice = r3
            if (r7 == 0) goto L5c
            goto L5e
        L5c:
            co.discord.media_engine.VideoInputDeviceDescription[] r7 = new co.discord.media_engine.VideoInputDeviceDescription[r2]
        L5e:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreMediaEngine.awaitVideoInputDevicesNativeAsync(kotlin.coroutines.Continuation):java.lang.Object");
    }

    public final synchronized void cycleVideoInputDevice() {
        int indexOf = k.indexOf(this.videoInputDevices, this.selectedVideoInputDevice);
        if (indexOf >= 0) {
            selectVideoInputDevice(this.videoInputDevices[indexOf == k.getLastIndex(this.videoInputDevices) ? 0 : indexOf + 1].getGuid());
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0035  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.lang.Object getDefaultVideoDeviceGUID(kotlin.coroutines.Continuation<? super java.lang.String> r5) {
        /*
            r4 = this;
            boolean r0 = r5 instanceof com.discord.stores.StoreMediaEngine$getDefaultVideoDeviceGUID$1
            if (r0 == 0) goto L13
            r0 = r5
            com.discord.stores.StoreMediaEngine$getDefaultVideoDeviceGUID$1 r0 = (com.discord.stores.StoreMediaEngine$getDefaultVideoDeviceGUID$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            com.discord.stores.StoreMediaEngine$getDefaultVideoDeviceGUID$1 r0 = new com.discord.stores.StoreMediaEngine$getDefaultVideoDeviceGUID$1
            r0.<init>(r4, r5)
        L18:
            java.lang.Object r5 = r0.result
            java.lang.Object r1 = d0.w.h.c.getCOROUTINE_SUSPENDED()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L35
            if (r2 != r3) goto L2d
            java.lang.Object r0 = r0.L$0
            com.discord.stores.StoreMediaEngine r0 = (com.discord.stores.StoreMediaEngine) r0
            d0.l.throwOnFailure(r5)
            goto L44
        L2d:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r0)
            throw r5
        L35:
            d0.l.throwOnFailure(r5)
            r0.L$0 = r4
            r0.label = r3
            java.lang.Object r5 = r4.awaitVideoInputDevicesNativeAsync(r0)
            if (r5 != r1) goto L43
            return r1
        L43:
            r0 = r4
        L44:
            co.discord.media_engine.VideoInputDeviceDescription[] r5 = (co.discord.media_engine.VideoInputDeviceDescription[]) r5
            java.lang.String r5 = r0.pickDefaultDeviceGUID(r5)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreMediaEngine.getDefaultVideoDeviceGUID(kotlin.coroutines.Continuation):java.lang.Object");
    }

    public final Observable<Boolean> getIsNativeEngineInitialized() {
        return this.isNativeEngineInitializedSubject;
    }

    public final ListenerCollection<Listener> getListeners() {
        return this.listeners;
    }

    public final Observable<MediaEngine.LocalVoiceStatus> getLocalVoiceStatus() {
        return this.localVoiceStatus;
    }

    public final MediaEngine getMediaEngine() {
        MediaEngine mediaEngine = this.mediaEngine;
        if (mediaEngine == null) {
            m.throwUninitializedPropertyAccessException("mediaEngine");
        }
        return mediaEngine;
    }

    public final Observable<MediaEngine.OpenSLESConfig> getOpenSLESConfig() {
        return this.openSLESConfigSubject;
    }

    public final synchronized void getRankedRtcRegions(List<ModelRtcLatencyRegion> list, Function1<? super List<String>, Unit> function1) {
        m.checkNotNullParameter(list, "regionsWithIps");
        m.checkNotNullParameter(function1, "callback");
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
        for (ModelRtcLatencyRegion modelRtcLatencyRegion : list) {
            String region = modelRtcLatencyRegion.getRegion();
            Object[] array = modelRtcLatencyRegion.getIps().toArray(new String[0]);
            if (array != null) {
                arrayList.add(new RtcRegion(region, (String[]) array));
            } else {
                throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        Object[] array2 = arrayList.toArray(new RtcRegion[0]);
        if (array2 != null) {
            RtcRegion[] rtcRegionArr = (RtcRegion[]) array2;
            MediaEngine mediaEngine = this.mediaEngine;
            if (mediaEngine == null) {
                m.throwUninitializedPropertyAccessException("mediaEngine");
            }
            mediaEngine.b(rtcRegionArr, new StoreMediaEngine$getRankedRtcRegions$1(function1));
        } else {
            throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    public final Observable<VideoInputDeviceDescription> getSelectedVideoInputDevice() {
        Observable<VideoInputDeviceDescription> q = this.selectedVideoInputDeviceSubject.q();
        m.checkNotNullExpressionValue(q, "selectedVideoInputDevice…  .distinctUntilChanged()");
        return q;
    }

    public final VideoInputDeviceDescription getSelectedVideoInputDeviceBlocking() {
        return this.selectedVideoInputDevice;
    }

    public final Observable<List<VideoInputDeviceDescription>> getVideoInputDevices() {
        Observable<List<VideoInputDeviceDescription>> q = this.videoInputDevicesSubject.q();
        m.checkNotNullExpressionValue(q, "videoInputDevicesSubject…  .distinctUntilChanged()");
        return q;
    }

    public final Object getVideoInputDevicesNativeAsync(Continuation<? super VideoInputDeviceDescription[]> continuation) {
        l lVar = new l(b.intercepted(continuation), 1);
        lVar.A();
        getVideoInputDevicesNative(new StoreMediaEngine$getVideoInputDevicesNativeAsync$2$1(lVar));
        Object u = lVar.u();
        if (u == c.getCOROUTINE_SUSPENDED()) {
            g.probeCoroutineSuspended(continuation);
        }
        return u;
    }

    public final synchronized Discord getVoiceEngineNative() {
        MediaEngine mediaEngine;
        mediaEngine = this.mediaEngine;
        if (mediaEngine == null) {
            m.throwUninitializedPropertyAccessException("mediaEngine");
        }
        return mediaEngine.i();
    }

    public final synchronized void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "payload");
        this.userId = modelPayload.getMe().i();
    }

    public final void handleMicrophonePermissionGranted() {
        Observable<Boolean> Z = this.isNativeEngineInitializedSubject.Z(1);
        m.checkNotNullExpressionValue(Z, "isNativeEngineInitializedSubject\n        .take(1)");
        ObservableExtensionsKt.appSubscribe(Z, StoreMediaEngine.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreMediaEngine$handleMicrophonePermissionGranted$1(this));
    }

    /* JADX WARN: Code restructure failed: missing block: B:9:0x0010, code lost:
        if (r0.longValue() != 0) goto L10;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final synchronized void handleVoiceChannelSelected(long r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            java.lang.Long r0 = r5.previousVoiceChannelId     // Catch: java.lang.Throwable -> L26
            if (r0 == 0) goto L1e
            r1 = 0
            if (r0 != 0) goto La
            goto L12
        La:
            long r3 = r0.longValue()     // Catch: java.lang.Throwable -> L26
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 == 0) goto L1e
        L12:
            int r0 = (r6 > r1 ? 1 : (r6 == r1 ? 0 : -1))
            if (r0 != 0) goto L1e
            com.discord.stores.StoreMediaEngine$handleVoiceChannelSelected$1 r0 = new com.discord.stores.StoreMediaEngine$handleVoiceChannelSelected$1     // Catch: java.lang.Throwable -> L26
            r0.<init>(r5)     // Catch: java.lang.Throwable -> L26
            r5.getVideoInputDevicesNative(r0)     // Catch: java.lang.Throwable -> L26
        L1e:
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch: java.lang.Throwable -> L26
            r5.previousVoiceChannelId = r6     // Catch: java.lang.Throwable -> L26
            monitor-exit(r5)
            return
        L26:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreMediaEngine.handleVoiceChannelSelected(long):void");
    }

    public final synchronized boolean hasNativeEngineEverInitialized() {
        return this.hasNativeEngineEverInitialized;
    }

    @Override // com.discord.stores.Store
    public void init(Context context) {
        m.checkNotNullParameter(context, "context");
        super.init(context);
        this.preferredVideoInputDeviceGUID = this.preferredVideoInputDeviceGuidCache.get();
        this.hasNativeEngineEverInitialized = this.hasNativeEngineEverInitializedCache.get().booleanValue();
        SharedPreferences prefsSessionDurable = getPrefsSessionDurable();
        MediaEngine.OpenSLESConfig openSLESConfig = DEFAULT_OPENSLES_CONFIG;
        String string = prefsSessionDurable.getString("OPEN_SLES", openSLESConfig.name());
        if (string == null) {
            string = openSLESConfig.name();
        }
        m.checkNotNullExpressionValue(string, "prefsSessionDurable\n    …AULT_OPENSLES_CONFIG.name");
        MediaEngine.OpenSLESConfig valueOf = MediaEngine.OpenSLESConfig.valueOf(string);
        this.openSLESConfigSubject.k.onNext(valueOf);
        MediaEngine.b storeMediaEngine$init$echoCancellationCallback$1 = new MediaEngine.b() { // from class: com.discord.stores.StoreMediaEngine$init$echoCancellationCallback$1
            @Override // com.discord.rtcconnection.mediaengine.MediaEngine.b
            public void onEchoCancellationUpdated(MediaEngine.EchoCancellationInfo echoCancellationInfo) {
                Dispatcher dispatcher;
                m.checkNotNullParameter(echoCancellationInfo, "info");
                dispatcher = StoreMediaEngine.this.dispatcher;
                dispatcher.schedule(new StoreMediaEngine$init$echoCancellationCallback$1$onEchoCancellationUpdated$1(echoCancellationInfo));
            }
        };
        EngineListener engineListener = new EngineListener();
        ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor();
        m.checkNotNullExpressionValue(newSingleThreadExecutor, "Executors.newSingleThreadExecutor()");
        AppLog appLog = AppLog.g;
        g.a aVar = b.a.q.k0.g.c;
        b.a.q.k0.g gVar = b.a.q.k0.g.f260b;
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(engineListener, "listener");
        m.checkNotNullParameter(newSingleThreadExecutor, "singleThreadExecutorService");
        m.checkNotNullParameter(valueOf, "openSLESConfig");
        m.checkNotNullParameter(appLog, "logger");
        m.checkNotNullParameter(gVar, "echoCancellation");
        m.checkNotNullParameter(storeMediaEngine$init$echoCancellationCallback$1, "echoCancellationCallback");
        this.mediaEngine = new b.a.q.m0.c.k(context, engineListener, new b.a.q.c(newSingleThreadExecutor, false), valueOf, appLog, gVar, storeMediaEngine$init$echoCancellationCallback$1, null, null, 384);
        Observable<R> F = this.storeStream.getExperiments$app_productionGoogleRelease().observeUserExperiment("2021-05_opensl_default_enable_android", true).x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
        m.checkNotNullExpressionValue(F, "filter { it != null }.map { it!! }");
        Observable Z = F.Z(1);
        m.checkNotNullExpressionValue(Z, "storeStream.experiments.…erNull()\n        .take(1)");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.computationLatest(Z), StoreMediaEngine.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreMediaEngine$init$1(this));
    }

    public final Observable<KrispOveruseDetector.Status> onKrispStatusEvent() {
        PublishSubject<KrispOveruseDetector.Status> publishSubject = this.onKrispStatusSubject;
        m.checkNotNullExpressionValue(publishSubject, "onKrispStatusSubject");
        return publishSubject;
    }

    public final void selectDefaultVideoDevice(Function1<? super String, Unit> function1) {
        x0 x0Var = x0.j;
        CoroutineDispatcher coroutineDispatcher = k0.a;
        f.H0(x0Var, n.f3802b.H(), null, new StoreMediaEngine$selectDefaultVideoDevice$1(this, function1, null), 2, null);
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0035  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.lang.Object selectDefaultVideoDeviceAsync(kotlin.coroutines.Continuation<? super java.lang.String> r7) {
        /*
            r6 = this;
            boolean r0 = r7 instanceof com.discord.stores.StoreMediaEngine$selectDefaultVideoDeviceAsync$1
            if (r0 == 0) goto L13
            r0 = r7
            com.discord.stores.StoreMediaEngine$selectDefaultVideoDeviceAsync$1 r0 = (com.discord.stores.StoreMediaEngine$selectDefaultVideoDeviceAsync$1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            com.discord.stores.StoreMediaEngine$selectDefaultVideoDeviceAsync$1 r0 = new com.discord.stores.StoreMediaEngine$selectDefaultVideoDeviceAsync$1
            r0.<init>(r6, r7)
        L18:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = d0.w.h.c.getCOROUTINE_SUSPENDED()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L35
            if (r2 != r3) goto L2d
            java.lang.Object r0 = r0.L$0
            com.discord.stores.StoreMediaEngine r0 = (com.discord.stores.StoreMediaEngine) r0
            d0.l.throwOnFailure(r7)
            goto L44
        L2d:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r0)
            throw r7
        L35:
            d0.l.throwOnFailure(r7)
            r0.L$0 = r6
            r0.label = r3
            java.lang.Object r7 = r6.awaitVideoInputDevicesNativeAsync(r0)
            if (r7 != r1) goto L43
            return r1
        L43:
            r0 = r6
        L44:
            r1 = r7
            co.discord.media_engine.VideoInputDeviceDescription[] r1 = (co.discord.media_engine.VideoInputDeviceDescription[]) r1
            java.lang.String r7 = r0.pickDefaultDeviceGUID(r1)
            r3 = 0
            r4 = 4
            r5 = 0
            r2 = r7
            handleVideoInputDevices$default(r0, r1, r2, r3, r4, r5)
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.stores.StoreMediaEngine.selectDefaultVideoDeviceAsync(kotlin.coroutines.Continuation):java.lang.Object");
    }

    public final void selectVideoInputDevice(String str) {
        getVideoInputDevicesNative(new StoreMediaEngine$selectVideoInputDevice$1(this, str));
    }

    public final synchronized void setAudioInputEnabled(boolean z2) {
        MediaEngine mediaEngine = this.mediaEngine;
        if (mediaEngine == null) {
            m.throwUninitializedPropertyAccessException("mediaEngine");
        }
        mediaEngine.k(z2);
    }

    @SuppressLint({"ApplySharedPref"})
    public final synchronized void setOpenSLESConfig(MediaEngine.OpenSLESConfig openSLESConfig) {
        m.checkNotNullParameter(openSLESConfig, "openSLESConfig");
        this.openSLESConfigSubject.k.onNext(openSLESConfig);
        getPrefsSessionDurable().edit().putString("OPEN_SLES", openSLESConfig.name()).commit();
    }

    public final synchronized void setPttActive(boolean z2) {
        MediaEngine mediaEngine = this.mediaEngine;
        if (mediaEngine == null) {
            m.throwUninitializedPropertyAccessException("mediaEngine");
        }
        for (MediaEngineConnection mediaEngineConnection : mediaEngine.getConnections()) {
            mediaEngineConnection.q(z2);
        }
        SerializedSubject<Boolean, Boolean> serializedSubject = this.pttActiveSubject;
        serializedSubject.k.onNext(Boolean.valueOf(z2));
    }
}
