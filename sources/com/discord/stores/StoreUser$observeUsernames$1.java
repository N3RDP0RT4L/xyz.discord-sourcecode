package com.discord.stores;

import com.discord.models.user.User;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreUser.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/user/User;", "user", "", "invoke", "(Lcom/discord/models/user/User;)Ljava/lang/String;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUser$observeUsernames$1 extends o implements Function1<User, String> {
    public static final StoreUser$observeUsernames$1 INSTANCE = new StoreUser$observeUsernames$1();

    public StoreUser$observeUsernames$1() {
        super(1);
    }

    public final String invoke(User user) {
        m.checkNotNull(user);
        return user.getUsername();
    }
}
