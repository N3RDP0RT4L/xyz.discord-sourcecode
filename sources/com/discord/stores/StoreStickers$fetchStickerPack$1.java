package com.discord.stores;

import com.discord.models.sticker.dto.ModelStickerPack;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.t.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreStickers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreStickers$fetchStickerPack$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $stickerPackId;
    public final /* synthetic */ StoreStickers this$0;

    /* compiled from: StoreStickers.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/sticker/dto/ModelStickerPack;", "newPack", "", "invoke", "(Lcom/discord/models/sticker/dto/ModelStickerPack;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreStickers$fetchStickerPack$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<ModelStickerPack, Unit> {

        /* compiled from: StoreStickers.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.stores.StoreStickers$fetchStickerPack$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C02141 extends o implements Function0<Unit> {
            public final /* synthetic */ ModelStickerPack $newPack;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C02141(ModelStickerPack modelStickerPack) {
                super(0);
                this.$newPack = modelStickerPack;
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                StoreStickers$fetchStickerPack$1.this.this$0.handleNewLoadedStickerPacks(m.listOf(this.$newPack));
            }
        }

        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(ModelStickerPack modelStickerPack) {
            invoke2(modelStickerPack);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(ModelStickerPack modelStickerPack) {
            Dispatcher dispatcher;
            d0.z.d.m.checkNotNullParameter(modelStickerPack, "newPack");
            dispatcher = StoreStickers$fetchStickerPack$1.this.this$0.dispatcher;
            dispatcher.schedule(new C02141(modelStickerPack));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreStickers$fetchStickerPack$1(StoreStickers storeStickers, long j) {
        super(0);
        this.this$0 = storeStickers;
        this.$stickerPackId = j;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        RestAPI restAPI;
        this.this$0.handleNewLoadingStickerPacks(m.listOf(Long.valueOf(this.$stickerPackId)));
        restAPI = this.this$0.api;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.restSubscribeOn$default(restAPI.getStickerPack(this.$stickerPackId), false, 1, null), this.this$0.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
