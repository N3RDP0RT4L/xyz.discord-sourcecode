package com.discord.stores;

import com.discord.stores.StoreUserRelationships;
import d0.t.h0;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Map;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreUserRelationships.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u0016\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "", "Lcom/discord/primitives/UserId;", "", "Lcom/discord/primitives/RelationshipType;", "invoke", "()Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserRelationships$observe$1 extends o implements Function0<Map<Long, ? extends Integer>> {
    public final /* synthetic */ StoreUserRelationships this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreUserRelationships$observe$1(StoreUserRelationships storeUserRelationships) {
        super(0);
        this.this$0 = storeUserRelationships;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Map<Long, ? extends Integer> invoke() {
        StoreUserRelationships.UserRelationshipsState userRelationshipsState;
        StoreUserRelationships.UserRelationshipsState userRelationshipsState2;
        userRelationshipsState = this.this$0.relationshipsStateSnapshot;
        if (m.areEqual(userRelationshipsState, StoreUserRelationships.UserRelationshipsState.Unloaded.INSTANCE)) {
            return h0.emptyMap();
        }
        if (userRelationshipsState instanceof StoreUserRelationships.UserRelationshipsState.Loaded) {
            userRelationshipsState2 = this.this$0.relationshipsStateSnapshot;
            Objects.requireNonNull(userRelationshipsState2, "null cannot be cast to non-null type com.discord.stores.StoreUserRelationships.UserRelationshipsState.Loaded");
            return ((StoreUserRelationships.UserRelationshipsState.Loaded) userRelationshipsState2).getRelationships();
        }
        throw new NoWhenBranchMatchedException();
    }
}
