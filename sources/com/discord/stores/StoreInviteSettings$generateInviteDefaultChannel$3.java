package com.discord.stores;

import androidx.core.app.NotificationCompat;
import com.discord.api.channel.Channel;
import d0.t.u;
import d0.z.d.m;
import j0.k.b;
import java.util.List;
import kotlin.Metadata;
/* compiled from: StoreInviteSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\u0010\u0007\u001a\u0004\u0018\u00010\u00042\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "Lcom/discord/api/channel/Channel;", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Ljava/util/List;)Ljava/lang/Long;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreInviteSettings$generateInviteDefaultChannel$3<T, R> implements b<List<? extends Channel>, Long> {
    public static final StoreInviteSettings$generateInviteDefaultChannel$3 INSTANCE = new StoreInviteSettings$generateInviteDefaultChannel$3();

    @Override // j0.k.b
    public /* bridge */ /* synthetic */ Long call(List<? extends Channel> list) {
        return call2((List<Channel>) list);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final Long call2(List<Channel> list) {
        m.checkNotNullExpressionValue(list, "it");
        Channel channel = (Channel) u.firstOrNull((List<? extends Object>) list);
        if (channel != null) {
            return Long.valueOf(channel.h());
        }
        return null;
    }
}
