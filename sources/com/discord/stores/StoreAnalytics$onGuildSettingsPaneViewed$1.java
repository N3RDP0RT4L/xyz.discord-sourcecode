package com.discord.stores;

import com.discord.utilities.analytics.AnalyticsTracker;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreAnalytics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAnalytics$onGuildSettingsPaneViewed$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ String $pane;
    public final /* synthetic */ StoreAnalytics this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreAnalytics$onGuildSettingsPaneViewed$1(StoreAnalytics storeAnalytics, long j, String str) {
        super(0);
        this.this$0 = storeAnalytics;
        this.$guildId = j;
        this.$pane = str;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        Map<String, ? extends Object> guildAnalyticsPropertiesInternal;
        guildAnalyticsPropertiesInternal = this.this$0.getGuildAnalyticsPropertiesInternal(this.$guildId);
        AnalyticsTracker.INSTANCE.settingsPaneViewed("guild", this.$pane, guildAnalyticsPropertiesInternal);
    }
}
