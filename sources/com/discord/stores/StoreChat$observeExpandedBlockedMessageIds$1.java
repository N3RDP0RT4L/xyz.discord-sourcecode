package com.discord.stores;

import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: StoreChat.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\f\u0012\b\u0012\u00060\u0001j\u0002`\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "", "Lcom/discord/primitives/MessageId;", "invoke", "()Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreChat$observeExpandedBlockedMessageIds$1 extends o implements Function0<List<? extends Long>> {
    public final /* synthetic */ StoreChat this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreChat$observeExpandedBlockedMessageIds$1(StoreChat storeChat) {
        super(0);
        this.this$0 = storeChat;
    }

    @Override // kotlin.jvm.functions.Function0
    public final List<? extends Long> invoke() {
        return this.this$0.getExpandedBlockedMessageGroups();
    }
}
