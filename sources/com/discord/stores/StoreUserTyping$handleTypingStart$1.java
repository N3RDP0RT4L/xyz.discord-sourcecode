package com.discord.stores;

import com.discord.api.user.TypingUser;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreUserTyping.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/api/user/TypingUser;", "kotlin.jvm.PlatformType", "prevTyping", "", "invoke", "(Lcom/discord/api/user/TypingUser;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreUserTyping$handleTypingStart$1 extends o implements Function1<TypingUser, Unit> {
    public final /* synthetic */ StoreUserTyping this$0;

    /* compiled from: StoreUserTyping.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreUserTyping$handleTypingStart$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ TypingUser $prevTyping;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(TypingUser typingUser) {
            super(0);
            this.$prevTyping = typingUser;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            StoreUserTyping storeUserTyping = StoreUserTyping$handleTypingStart$1.this.this$0;
            TypingUser typingUser = this.$prevTyping;
            m.checkNotNullExpressionValue(typingUser, "prevTyping");
            storeUserTyping.handleTypingStop(typingUser);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreUserTyping$handleTypingStart$1(StoreUserTyping storeUserTyping) {
        super(1);
        this.this$0 = storeUserTyping;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(TypingUser typingUser) {
        invoke2(typingUser);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(TypingUser typingUser) {
        Dispatcher dispatcher;
        dispatcher = this.this$0.dispatcher;
        dispatcher.schedule(new AnonymousClass1(typingUser));
    }
}
