package com.discord.stores;

import com.discord.utilities.search.network.state.SearchState;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreSearch.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/search/network/state/SearchState;", "searchState", "", "invoke", "(Lcom/discord/utilities/search/network/state/SearchState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreSearch$init$6 extends o implements Function1<SearchState, Unit> {
    public final /* synthetic */ StoreSearch this$0;

    /* compiled from: StoreSearch.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.stores.StoreSearch$init$6$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ SearchState $searchState;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(SearchState searchState) {
            super(0);
            this.$searchState = searchState;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            StoreStream storeStream;
            storeStream = StoreSearch$init$6.this.this$0.stream;
            storeStream.handleSearchFinish(this.$searchState);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreSearch$init$6(StoreSearch storeSearch) {
        super(1);
        this.this$0 = storeSearch;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(SearchState searchState) {
        invoke2(searchState);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(SearchState searchState) {
        Dispatcher dispatcher;
        m.checkNotNullParameter(searchState, "searchState");
        dispatcher = this.this$0.dispatcher;
        dispatcher.schedule(new AnonymousClass1(searchState));
    }
}
