package com.discord.stores;

import android.annotation.SuppressLint;
import com.discord.models.domain.emoji.ModelEmojiCustom;
import com.discord.stores.StoreEmoji;
import com.discord.utilities.collections.ShallowPartitionMap;
import d0.t.u;
import d0.u.a;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreEmoji.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\n\u0010\u0002\u001a\u00060\u0000j\u0002`\u0001H\u000b¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/primitives/GuildId;", "guildId", "", "invoke", "(J)V", "processGuildEmojis"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreEmoji$buildUsableEmojiSet$5 extends o implements Function1<Long, Unit> {
    public final /* synthetic */ Map $allCustomEmojis;
    public final /* synthetic */ StoreEmoji.EmojiContext $emojiContext;
    public final /* synthetic */ ShallowPartitionMap $emojiIdsMap;
    public final /* synthetic */ ShallowPartitionMap $emojiNameCounts;
    public final /* synthetic */ boolean $includeUnavailableEmojis;
    public final /* synthetic */ boolean $includeUnusableEmojis;
    public final /* synthetic */ StoreEmoji$buildUsableEmojiSet$1 $isExternalEmoji$1;
    public final /* synthetic */ StoreEmoji$buildUsableEmojiSet$2 $isExternalEmojiRestricted$2;
    public final /* synthetic */ boolean $isMePremium;
    public final /* synthetic */ HashMap $usableCustomEmojis;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreEmoji$buildUsableEmojiSet$5(Map map, StoreEmoji$buildUsableEmojiSet$1 storeEmoji$buildUsableEmojiSet$1, StoreEmoji$buildUsableEmojiSet$2 storeEmoji$buildUsableEmojiSet$2, boolean z2, StoreEmoji.EmojiContext emojiContext, boolean z3, boolean z4, ShallowPartitionMap shallowPartitionMap, ShallowPartitionMap shallowPartitionMap2, HashMap hashMap) {
        super(1);
        this.$allCustomEmojis = map;
        this.$isExternalEmoji$1 = storeEmoji$buildUsableEmojiSet$1;
        this.$isExternalEmojiRestricted$2 = storeEmoji$buildUsableEmojiSet$2;
        this.$includeUnavailableEmojis = z2;
        this.$emojiContext = emojiContext;
        this.$isMePremium = z3;
        this.$includeUnusableEmojis = z4;
        this.$emojiNameCounts = shallowPartitionMap;
        this.$emojiIdsMap = shallowPartitionMap2;
        this.$usableCustomEmojis = hashMap;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Long l) {
        invoke(l.longValue());
        return Unit.a;
    }

    @SuppressLint({"DefaultLocale"})
    public final void invoke(long j) {
        ModelEmojiCustom modelEmojiCustom;
        Map map = (Map) this.$allCustomEmojis.get(Long.valueOf(j));
        if (map != null) {
            boolean invoke = this.$isExternalEmoji$1.invoke(j);
            if (!this.$isExternalEmojiRestricted$2.invoke(invoke)) {
                ArrayList arrayList = new ArrayList();
                Iterator it = map.values().iterator();
                while (true) {
                    boolean z2 = true;
                    if (!it.hasNext()) {
                        break;
                    }
                    ModelEmojiCustom modelEmojiCustom2 = (ModelEmojiCustom) it.next();
                    if (this.$includeUnavailableEmojis || modelEmojiCustom2.isAvailable()) {
                        int i = 0;
                        boolean z3 = modelEmojiCustom2.isManaged() && ((this.$emojiContext instanceof StoreEmoji.EmojiContext.Global) ^ true);
                        if (!this.$isMePremium && (modelEmojiCustom2.isAnimated() || (invoke && !z3))) {
                            z2 = false;
                        }
                        if (this.$includeUnusableEmojis || z2) {
                            Integer num = (Integer) this.$emojiNameCounts.get(modelEmojiCustom2.getName());
                            if (num != null || !z2) {
                                if (num != null) {
                                    i = num.intValue();
                                }
                                modelEmojiCustom = new ModelEmojiCustom(modelEmojiCustom2, i, z2);
                            } else {
                                modelEmojiCustom = modelEmojiCustom2;
                            }
                            arrayList.add(modelEmojiCustom);
                            StoreEmoji$buildUsableEmojiSet$4 storeEmoji$buildUsableEmojiSet$4 = StoreEmoji$buildUsableEmojiSet$4.INSTANCE;
                            ShallowPartitionMap shallowPartitionMap = this.$emojiNameCounts;
                            String name = modelEmojiCustom2.getName();
                            m.checkNotNullExpressionValue(name, "emoji.name");
                            storeEmoji$buildUsableEmojiSet$4.invoke((Map<ShallowPartitionMap, Integer>) shallowPartitionMap, (ShallowPartitionMap) name);
                            ShallowPartitionMap shallowPartitionMap2 = this.$emojiIdsMap;
                            String uniqueId = modelEmojiCustom2.getUniqueId();
                            m.checkNotNullExpressionValue(uniqueId, "emoji.uniqueId");
                            shallowPartitionMap2.put(uniqueId, modelEmojiCustom);
                        }
                    }
                }
                if (!arrayList.isEmpty()) {
                    this.$usableCustomEmojis.put(Long.valueOf(j), u.sortedWith(arrayList, new Comparator() { // from class: com.discord.stores.StoreEmoji$buildUsableEmojiSet$5$processGuildEmojis$$inlined$sortedByDescending$1
                        @Override // java.util.Comparator
                        public final int compare(T t, T t2) {
                            String name2 = ((ModelEmojiCustom) t2).getName();
                            m.checkNotNullExpressionValue(name2, "it.name");
                            Locale locale = Locale.ROOT;
                            m.checkNotNullExpressionValue(locale, "Locale.ROOT");
                            Objects.requireNonNull(name2, "null cannot be cast to non-null type java.lang.String");
                            String lowerCase = name2.toLowerCase(locale);
                            m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                            String name3 = ((ModelEmojiCustom) t).getName();
                            m.checkNotNullExpressionValue(name3, "it.name");
                            m.checkNotNullExpressionValue(locale, "Locale.ROOT");
                            Objects.requireNonNull(name3, "null cannot be cast to non-null type java.lang.String");
                            String lowerCase2 = name3.toLowerCase(locale);
                            m.checkNotNullExpressionValue(lowerCase2, "(this as java.lang.String).toLowerCase(locale)");
                            return a.compareValues(lowerCase, lowerCase2);
                        }
                    }));
                }
            }
        }
    }
}
