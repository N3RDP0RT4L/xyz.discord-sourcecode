package com.discord.stores;

import kotlin.Metadata;
/* compiled from: StoreChangeLog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\"\u0016\u0010\u0001\u001a\u00020\u00008\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0001\u0010\u0002\"\u0016\u0010\u0004\u001a\u00020\u00038\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0004\u0010\u0005¨\u0006\u0006"}, d2 = {"", "NOTICE_NAME", "Ljava/lang/String;", "", "MIN_VIEW_AGE_IN_MS", "J", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreChangeLogKt {
    private static final long MIN_VIEW_AGE_IN_MS = 432000000;
    private static final String NOTICE_NAME = "CHANGE_LOG";
}
