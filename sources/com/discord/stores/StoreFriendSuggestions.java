package com.discord.stores;

import andhook.lib.HookHelper;
import com.discord.api.friendsuggestions.FriendSuggestionDelete;
import com.discord.api.friendsuggestions.FriendSuggestionReason;
import com.discord.models.domain.ModelPayload;
import com.discord.models.friendsuggestions.FriendSuggestion;
import com.discord.models.user.CoreUser;
import com.discord.utilities.friendsuggestions.FriendSuggestionsFetcher;
import d0.t.h0;
import d0.t.u;
import d0.z.d.m;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
/* compiled from: StoreFriendSuggestions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010%\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010%\u001a\u00020$\u0012\u0006\u0010-\u001a\u00020,\u0012\u0006\u0010(\u001a\u00020'¢\u0006\u0004\b0\u00101J\u001b\u0010\u0006\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u001d\u0010\u000b\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u00050\b¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u000e\u001a\u00020\rH\u0007¢\u0006\u0004\b\u0010\u0010\u0011J\u001f\u0010\u0014\u001a\u00020\u000f2\u0010\u0010\u0013\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\u0012¢\u0006\u0004\b\u0014\u0010\u0015J\u001b\u0010\u0017\u001a\u00020\u000f2\n\u0010\u0016\u001a\u00060\u0002j\u0002`\u0003H\u0007¢\u0006\u0004\b\u0017\u0010\u0018J\u0017\u0010\u001b\u001a\u00020\u000f2\u0006\u0010\u001a\u001a\u00020\u0019H\u0007¢\u0006\u0004\b\u001b\u0010\u001cJ!\u0010\u001e\u001a\u00020\u000f2\u0010\u0010\u001d\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\u0012H\u0007¢\u0006\u0004\b\u001e\u0010\u0015J\u000f\u0010\u001f\u001a\u00020\u000fH\u0007¢\u0006\u0004\b\u001f\u0010 J\u000f\u0010!\u001a\u00020\u000fH\u0016¢\u0006\u0004\b!\u0010 R&\u0010\u0013\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u00050\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010#R\u0016\u0010%\u001a\u00020$8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b%\u0010&R\u0019\u0010(\u001a\u00020'8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010)\u001a\u0004\b*\u0010+R\u0016\u0010-\u001a\u00020,8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b-\u0010.R&\u0010/\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u00050\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b/\u0010#¨\u00062"}, d2 = {"Lcom/discord/stores/StoreFriendSuggestions;", "Lcom/discord/stores/StoreV2;", "Lcom/discord/api/friendsuggestions/FriendSuggestion;", "Lcom/discord/stores/ApiFriendSuggestion;", "apiFriendSuggestion", "Lcom/discord/models/friendsuggestions/FriendSuggestion;", "convertApiFriendSuggestion", "(Lcom/discord/api/friendsuggestions/FriendSuggestion;)Lcom/discord/models/friendsuggestions/FriendSuggestion;", "", "", "Lcom/discord/primitives/UserId;", "getFriendSuggestions", "()Ljava/util/Map;", "Lcom/discord/models/domain/ModelPayload;", "readyPayload", "", "handleConnectionOpen", "(Lcom/discord/models/domain/ModelPayload;)V", "", "suggestions", "updateFriendSuggestions", "(Ljava/util/List;)V", "friendSuggestionCreate", "handleFriendSuggestionCreate", "(Lcom/discord/api/friendsuggestions/FriendSuggestion;)V", "Lcom/discord/api/friendsuggestions/FriendSuggestionDelete;", "friendSuggestionDelete", "handleFriendSuggestionDelete", "(Lcom/discord/api/friendsuggestions/FriendSuggestionDelete;)V", "loadedSuggestions", "handleFriendSuggestionsLoaded", "handleFriendSuggestionsLoadFailure", "()V", "snapshotData", "", "Ljava/util/Map;", "Lcom/discord/stores/StoreStream;", "storeStream", "Lcom/discord/stores/StoreStream;", "Lcom/discord/utilities/friendsuggestions/FriendSuggestionsFetcher;", "friendSuggestionsFetcher", "Lcom/discord/utilities/friendsuggestions/FriendSuggestionsFetcher;", "getFriendSuggestionsFetcher", "()Lcom/discord/utilities/friendsuggestions/FriendSuggestionsFetcher;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "suggestionsSnapshot", HookHelper.constructorName, "(Lcom/discord/stores/StoreStream;Lcom/discord/stores/Dispatcher;Lcom/discord/utilities/friendsuggestions/FriendSuggestionsFetcher;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreFriendSuggestions extends StoreV2 {
    private final Dispatcher dispatcher;
    private final FriendSuggestionsFetcher friendSuggestionsFetcher;
    private final StoreStream storeStream;
    private final Map<Long, FriendSuggestion> suggestions = new HashMap();
    private Map<Long, FriendSuggestion> suggestionsSnapshot = h0.emptyMap();

    public StoreFriendSuggestions(StoreStream storeStream, Dispatcher dispatcher, FriendSuggestionsFetcher friendSuggestionsFetcher) {
        m.checkNotNullParameter(storeStream, "storeStream");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(friendSuggestionsFetcher, "friendSuggestionsFetcher");
        this.storeStream = storeStream;
        this.dispatcher = dispatcher;
        this.friendSuggestionsFetcher = friendSuggestionsFetcher;
    }

    private final FriendSuggestion convertApiFriendSuggestion(com.discord.api.friendsuggestions.FriendSuggestion friendSuggestion) {
        CoreUser coreUser = new CoreUser(friendSuggestion.b());
        FriendSuggestionReason friendSuggestionReason = (FriendSuggestionReason) u.firstOrNull((List<? extends Object>) friendSuggestion.a());
        return new FriendSuggestion(coreUser, friendSuggestionReason != null ? friendSuggestionReason.a() : null);
    }

    public final Map<Long, FriendSuggestion> getFriendSuggestions() {
        return this.suggestionsSnapshot;
    }

    public final FriendSuggestionsFetcher getFriendSuggestionsFetcher() {
        return this.friendSuggestionsFetcher;
    }

    @StoreThread
    public final void handleConnectionOpen(ModelPayload modelPayload) {
        m.checkNotNullParameter(modelPayload, "readyPayload");
        this.suggestions.clear();
        if (modelPayload.getFriendSuggestionCount() > 0) {
            this.friendSuggestionsFetcher.maybeFetch();
        }
        markChanged();
    }

    @StoreThread
    public final void handleFriendSuggestionCreate(com.discord.api.friendsuggestions.FriendSuggestion friendSuggestion) {
        m.checkNotNullParameter(friendSuggestion, "friendSuggestionCreate");
        CoreUser coreUser = new CoreUser(friendSuggestion.b());
        FriendSuggestionReason friendSuggestionReason = (FriendSuggestionReason) u.firstOrNull((List<? extends Object>) friendSuggestion.a());
        FriendSuggestion friendSuggestion2 = new FriendSuggestion(coreUser, friendSuggestionReason != null ? friendSuggestionReason.a() : null);
        this.suggestions.put(Long.valueOf(friendSuggestion2.getUser().getId()), friendSuggestion2);
        markChanged();
    }

    @StoreThread
    public final void handleFriendSuggestionDelete(FriendSuggestionDelete friendSuggestionDelete) {
        m.checkNotNullParameter(friendSuggestionDelete, "friendSuggestionDelete");
        if (this.suggestions.remove(Long.valueOf(friendSuggestionDelete.a())) != null) {
            markChanged();
        }
    }

    @StoreThread
    public final void handleFriendSuggestionsLoadFailure() {
        this.suggestions.clear();
        markChanged();
    }

    @StoreThread
    public final void handleFriendSuggestionsLoaded(List<com.discord.api.friendsuggestions.FriendSuggestion> list) {
        m.checkNotNullParameter(list, "loadedSuggestions");
        this.suggestions.clear();
        Map<Long, FriendSuggestion> map = this.suggestions;
        for (com.discord.api.friendsuggestions.FriendSuggestion friendSuggestion : list) {
            map.put(Long.valueOf(friendSuggestion.b().i()), convertApiFriendSuggestion(friendSuggestion));
        }
        markChanged();
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        super.snapshotData();
        this.suggestionsSnapshot = new HashMap(this.suggestions);
    }

    public final void updateFriendSuggestions(List<com.discord.api.friendsuggestions.FriendSuggestion> list) {
        m.checkNotNullParameter(list, "suggestions");
        this.dispatcher.schedule(new StoreFriendSuggestions$updateFriendSuggestions$1(this, list));
    }
}
