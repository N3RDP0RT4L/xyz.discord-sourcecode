package com.discord.stores;

import com.discord.analytics.generated.events.network_action.TrackNetworkActionUserLoginMfa;
import com.discord.analytics.generated.traits.TrackNetworkMetadataReceiver;
import com.discord.models.domain.auth.ModelLoginResult;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: StoreAuthentication.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/auth/ModelLoginResult;", "it", "Lcom/discord/analytics/generated/traits/TrackNetworkMetadataReceiver;", "invoke", "(Lcom/discord/models/domain/auth/ModelLoginResult;)Lcom/discord/analytics/generated/traits/TrackNetworkMetadataReceiver;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreAuthentication$authMFA$1 extends o implements Function1<ModelLoginResult, TrackNetworkMetadataReceiver> {
    public static final StoreAuthentication$authMFA$1 INSTANCE = new StoreAuthentication$authMFA$1();

    public StoreAuthentication$authMFA$1() {
        super(1);
    }

    public final TrackNetworkMetadataReceiver invoke(ModelLoginResult modelLoginResult) {
        return new TrackNetworkActionUserLoginMfa();
    }
}
