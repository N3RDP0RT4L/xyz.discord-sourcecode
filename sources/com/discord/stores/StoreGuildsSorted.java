package com.discord.stores;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.Context;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelGuildFolder;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreGuildsSorted;
import com.discord.stores.StoreStream;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.g0.t;
import d0.t.n;
import d0.t.o;
import d0.t.r;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StoreGuildsSorted.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u000289B'\u0012\u0006\u0010'\u001a\u00020&\u0012\u0006\u0010*\u001a\u00020)\u0012\u0006\u00104\u001a\u000203\u0012\u0006\u0010-\u001a\u00020,¢\u0006\u0004\b6\u00107J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u001d\u0010\n\u001a\u00020\u00042\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u0007H\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\f\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\f\u0010\u0006J\u000f\u0010\r\u001a\u00020\u0004H\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u000f\u001a\u00020\u0004H\u0003¢\u0006\u0004\b\u000f\u0010\u000eJ\u0015\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00020\u0010H\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u0013\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\u0004\b\u0013\u0010\u0014J\u0019\u0010\u0015\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0010¢\u0006\u0004\b\u0015\u0010\u0012J1\u0010\u001b\u001a&\u0012\b\u0012\u00060\u0017j\u0002`\u0018\u0012\u0004\u0012\u00020\u00190\u0016j\u0012\u0012\b\u0012\u00060\u0017j\u0002`\u0018\u0012\u0004\u0012\u00020\u0019`\u001a¢\u0006\u0004\b\u001b\u0010\u001cJ7\u0010\u001d\u001a,\u0012(\u0012&\u0012\b\u0012\u00060\u0017j\u0002`\u0018\u0012\u0004\u0012\u00020\u00190\u0016j\u0012\u0012\b\u0012\u00060\u0017j\u0002`\u0018\u0012\u0004\u0012\u00020\u0019`\u001a0\u0010¢\u0006\u0004\b\u001d\u0010\u0012J\u0017\u0010 \u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\u001eH\u0016¢\u0006\u0004\b \u0010!J\u000f\u0010\"\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\"\u0010\u000eJ\u001b\u0010#\u001a\u00020\u00042\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\u0004\b#\u0010\u000bR\u001c\u0010$\u001a\b\u0012\u0004\u0012\u00020\b0\u00078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b$\u0010%R\u0016\u0010'\u001a\u00020&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R\u0016\u0010*\u001a\u00020)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010+R\u0016\u0010-\u001a\u00020,8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b-\u0010.R\u001c\u00100\u001a\b\u0012\u0004\u0012\u00020\b0/8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b0\u0010%R\u001c\u00102\u001a\b\u0012\u0004\u0012\u0002010/8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b2\u0010%R\u0016\u00104\u001a\u0002038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b4\u00105¨\u0006:"}, d2 = {"Lcom/discord/stores/StoreGuildsSorted;", "Lcom/discord/stores/StoreV2;", "Lcom/discord/stores/StoreGuildsSorted$State;", "state", "", "handleNewState", "(Lcom/discord/stores/StoreGuildsSorted$State;)V", "", "Lcom/discord/stores/StoreGuildsSorted$Entry;", "guildPositions", "handleNewPositionsFromUser", "(Ljava/util/List;)V", "updatePositions", "ensureValidPositions", "()V", "rebuildSortedGuilds", "Lrx/Observable;", "observeStores", "()Lrx/Observable;", "getEntries", "()Ljava/util/List;", "observeEntries", "Ljava/util/LinkedHashMap;", "", "Lcom/discord/primitives/GuildId;", "Lcom/discord/models/guild/Guild;", "Lkotlin/collections/LinkedHashMap;", "getOrderedGuilds", "()Ljava/util/LinkedHashMap;", "observeOrderedGuilds", "Landroid/content/Context;", "context", "init", "(Landroid/content/Context;)V", "snapshotData", "setPositions", "entriesSnapshot", "Ljava/util/List;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/stores/Dispatcher;", "dispatcher", "Lcom/discord/stores/Dispatcher;", "Lcom/discord/stores/StoreLurking;", "lurkingGuildStore", "Lcom/discord/stores/StoreLurking;", "", "entries", "Lcom/discord/models/domain/ModelGuildFolder;", "positions", "Lcom/discord/stores/StoreGuilds;", "guildStore", "Lcom/discord/stores/StoreGuilds;", HookHelper.constructorName, "(Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/Dispatcher;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreLurking;)V", "Entry", "State", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class StoreGuildsSorted extends StoreV2 {
    private final Dispatcher dispatcher;
    private final StoreGuilds guildStore;
    private final StoreLurking lurkingGuildStore;
    private final ObservationDeck observationDeck;
    private final List<Entry> entries = new ArrayList();
    private List<? extends Entry> entriesSnapshot = n.emptyList();
    private final List<ModelGuildFolder> positions = new ArrayList();

    /* compiled from: StoreGuildsSorted.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0007\bB\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0003\u001a\u00020\u0002H&¢\u0006\u0004\b\u0003\u0010\u0004\u0082\u0001\u0002\t\n¨\u0006\u000b"}, d2 = {"Lcom/discord/stores/StoreGuildsSorted$Entry;", "", "Lcom/discord/models/domain/ModelGuildFolder;", "asModelGuildFolder", "()Lcom/discord/models/domain/ModelGuildFolder;", HookHelper.constructorName, "()V", "Folder", "SingletonGuild", "Lcom/discord/stores/StoreGuildsSorted$Entry$SingletonGuild;", "Lcom/discord/stores/StoreGuildsSorted$Entry$Folder;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class Entry {

        /* compiled from: StoreGuildsSorted.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B5\u0012\n\u0010\u0013\u001a\u00060\u0005j\u0002`\u0006\u0012\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\n0\t\u0012\b\u0010\u0015\u001a\u0004\u0018\u00010\r\u0012\b\u0010\u0016\u001a\u0004\u0018\u00010\u0010¢\u0006\u0004\b)\u0010*J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u0014\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0016\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\tHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0012\u0010\u000e\u001a\u0004\u0018\u00010\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012JF\u0010\u0017\u001a\u00020\u00002\f\b\u0002\u0010\u0013\u001a\u00060\u0005j\u0002`\u00062\u000e\b\u0002\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\n0\t2\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\r2\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u0010HÆ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0019\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u0012J\u0010\u0010\u001a\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u001a\u0010\u001f\u001a\u00020\u001e2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001cHÖ\u0003¢\u0006\u0004\b\u001f\u0010 R\u001f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\n0\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010!\u001a\u0004\b\"\u0010\fR\u001b\u0010\u0015\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010#\u001a\u0004\b$\u0010\u000fR\u001d\u0010\u0013\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010%\u001a\u0004\b&\u0010\bR\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010'\u001a\u0004\b(\u0010\u0012¨\u0006+"}, d2 = {"Lcom/discord/stores/StoreGuildsSorted$Entry$Folder;", "Lcom/discord/stores/StoreGuildsSorted$Entry;", "Lcom/discord/models/domain/ModelGuildFolder;", "asModelGuildFolder", "()Lcom/discord/models/domain/ModelGuildFolder;", "", "Lcom/discord/primitives/FolderId;", "component1", "()J", "", "Lcom/discord/models/guild/Guild;", "component2", "()Ljava/util/List;", "", "component3", "()Ljava/lang/Integer;", "", "component4", "()Ljava/lang/String;", ModelAuditLogEntry.CHANGE_KEY_ID, "guilds", ModelAuditLogEntry.CHANGE_KEY_COLOR, ModelAuditLogEntry.CHANGE_KEY_NAME, "copy", "(JLjava/util/List;Ljava/lang/Integer;Ljava/lang/String;)Lcom/discord/stores/StoreGuildsSorted$Entry$Folder;", "toString", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getGuilds", "Ljava/lang/Integer;", "getColor", "J", "getId", "Ljava/lang/String;", "getName", HookHelper.constructorName, "(JLjava/util/List;Ljava/lang/Integer;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Folder extends Entry {
            private final Integer color;
            private final List<Guild> guilds;

            /* renamed from: id  reason: collision with root package name */
            private final long f2783id;
            private final String name;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Folder(long j, List<Guild> list, Integer num, String str) {
                super(null);
                m.checkNotNullParameter(list, "guilds");
                this.f2783id = j;
                this.guilds = list;
                this.color = num;
                this.name = str;
            }

            public static /* synthetic */ Folder copy$default(Folder folder, long j, List list, Integer num, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = folder.f2783id;
                }
                long j2 = j;
                List<Guild> list2 = list;
                if ((i & 2) != 0) {
                    list2 = folder.guilds;
                }
                List list3 = list2;
                if ((i & 4) != 0) {
                    num = folder.color;
                }
                Integer num2 = num;
                if ((i & 8) != 0) {
                    str = folder.name;
                }
                return folder.copy(j2, list3, num2, str);
            }

            @Override // com.discord.stores.StoreGuildsSorted.Entry
            public ModelGuildFolder asModelGuildFolder() {
                Long valueOf = Long.valueOf(this.f2783id);
                List<Guild> list = this.guilds;
                ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
                for (Guild guild : list) {
                    arrayList.add(Long.valueOf(guild.getId()));
                }
                return new ModelGuildFolder(valueOf, arrayList, this.color, this.name);
            }

            public final long component1() {
                return this.f2783id;
            }

            public final List<Guild> component2() {
                return this.guilds;
            }

            public final Integer component3() {
                return this.color;
            }

            public final String component4() {
                return this.name;
            }

            public final Folder copy(long j, List<Guild> list, Integer num, String str) {
                m.checkNotNullParameter(list, "guilds");
                return new Folder(j, list, num, str);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Folder)) {
                    return false;
                }
                Folder folder = (Folder) obj;
                return this.f2783id == folder.f2783id && m.areEqual(this.guilds, folder.guilds) && m.areEqual(this.color, folder.color) && m.areEqual(this.name, folder.name);
            }

            public final Integer getColor() {
                return this.color;
            }

            public final List<Guild> getGuilds() {
                return this.guilds;
            }

            public final long getId() {
                return this.f2783id;
            }

            public final String getName() {
                return this.name;
            }

            public int hashCode() {
                int a = b.a(this.f2783id) * 31;
                List<Guild> list = this.guilds;
                int i = 0;
                int hashCode = (a + (list != null ? list.hashCode() : 0)) * 31;
                Integer num = this.color;
                int hashCode2 = (hashCode + (num != null ? num.hashCode() : 0)) * 31;
                String str = this.name;
                if (str != null) {
                    i = str.hashCode();
                }
                return hashCode2 + i;
            }

            public String toString() {
                StringBuilder R = a.R("Folder(id=");
                R.append(this.f2783id);
                R.append(", guilds=");
                R.append(this.guilds);
                R.append(", color=");
                R.append(this.color);
                R.append(", name=");
                return a.H(R, this.name, ")");
            }
        }

        /* compiled from: StoreGuildsSorted.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\b\u001a\u00020\u0005¢\u0006\u0004\b\u0018\u0010\u0019J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\t\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0011HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\b\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0016\u001a\u0004\b\u0017\u0010\u0007¨\u0006\u001a"}, d2 = {"Lcom/discord/stores/StoreGuildsSorted$Entry$SingletonGuild;", "Lcom/discord/stores/StoreGuildsSorted$Entry;", "Lcom/discord/models/domain/ModelGuildFolder;", "asModelGuildFolder", "()Lcom/discord/models/domain/ModelGuildFolder;", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "guild", "copy", "(Lcom/discord/models/guild/Guild;)Lcom/discord/stores/StoreGuildsSorted$Entry$SingletonGuild;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/guild/Guild;", "getGuild", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class SingletonGuild extends Entry {
            private final Guild guild;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public SingletonGuild(Guild guild) {
                super(null);
                m.checkNotNullParameter(guild, "guild");
                this.guild = guild;
            }

            public static /* synthetic */ SingletonGuild copy$default(SingletonGuild singletonGuild, Guild guild, int i, Object obj) {
                if ((i & 1) != 0) {
                    guild = singletonGuild.guild;
                }
                return singletonGuild.copy(guild);
            }

            @Override // com.discord.stores.StoreGuildsSorted.Entry
            public ModelGuildFolder asModelGuildFolder() {
                return new ModelGuildFolder(null, d0.t.m.listOf(Long.valueOf(this.guild.getId())), null, null, 8, null);
            }

            public final Guild component1() {
                return this.guild;
            }

            public final SingletonGuild copy(Guild guild) {
                m.checkNotNullParameter(guild, "guild");
                return new SingletonGuild(guild);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof SingletonGuild) && m.areEqual(this.guild, ((SingletonGuild) obj).guild);
                }
                return true;
            }

            public final Guild getGuild() {
                return this.guild;
            }

            public int hashCode() {
                Guild guild = this.guild;
                if (guild != null) {
                    return guild.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("SingletonGuild(guild=");
                R.append(this.guild);
                R.append(")");
                return R.toString();
            }
        }

        private Entry() {
        }

        public abstract ModelGuildFolder asModelGuildFolder();

        public /* synthetic */ Entry(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StoreGuildsSorted.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0082\b\u0018\u00002\u00020\u0001BC\u0012\u0010\u0010\u000f\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002\u0012\u001a\u0010\u0010\u001a\u0016\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\b\u0012\u00060\u0003j\u0002`\b0\u0007\u0012\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\f0\u000b¢\u0006\u0004\b$\u0010%J\u001a\u0010\u0005\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0006J$\u0010\t\u001a\u0016\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\b\u0012\u00060\u0003j\u0002`\b0\u0007HÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0016\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000bHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJR\u0010\u0012\u001a\u00020\u00002\u0012\b\u0002\u0010\u000f\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00022\u001c\b\u0002\u0010\u0010\u001a\u0016\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\b\u0012\u00060\u0003j\u0002`\b0\u00072\u000e\b\u0002\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\f0\u000bHÆ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0018\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u001a\u0010\u001c\u001a\u00020\u001b2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR-\u0010\u0010\u001a\u0016\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\b\u0012\u00060\u0003j\u0002`\b0\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001e\u001a\u0004\b\u001f\u0010\nR\u001f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\f0\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010 \u001a\u0004\b!\u0010\u000eR#\u0010\u000f\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\"\u001a\u0004\b#\u0010\u0006¨\u0006&"}, d2 = {"Lcom/discord/stores/StoreGuildsSorted$State;", "", "", "", "Lcom/discord/primitives/GuildId;", "component1", "()Ljava/util/Set;", "", "Lcom/discord/primitives/Timestamp;", "component2", "()Ljava/util/Map;", "", "Lcom/discord/models/domain/ModelGuildFolder;", "component3", "()Ljava/util/List;", "mutedGuilds", "joinedAt", "userSettingsGuildPositions", "copy", "(Ljava/util/Set;Ljava/util/Map;Ljava/util/List;)Lcom/discord/stores/StoreGuildsSorted$State;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getJoinedAt", "Ljava/util/List;", "getUserSettingsGuildPositions", "Ljava/util/Set;", "getMutedGuilds", HookHelper.constructorName, "(Ljava/util/Set;Ljava/util/Map;Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class State {
        private final Map<Long, Long> joinedAt;
        private final Set<Long> mutedGuilds;
        private final List<ModelGuildFolder> userSettingsGuildPositions;

        public State(Set<Long> set, Map<Long, Long> map, List<ModelGuildFolder> list) {
            m.checkNotNullParameter(set, "mutedGuilds");
            m.checkNotNullParameter(map, "joinedAt");
            m.checkNotNullParameter(list, "userSettingsGuildPositions");
            this.mutedGuilds = set;
            this.joinedAt = map;
            this.userSettingsGuildPositions = list;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ State copy$default(State state, Set set, Map map, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                set = state.mutedGuilds;
            }
            if ((i & 2) != 0) {
                map = state.joinedAt;
            }
            if ((i & 4) != 0) {
                list = state.userSettingsGuildPositions;
            }
            return state.copy(set, map, list);
        }

        public final Set<Long> component1() {
            return this.mutedGuilds;
        }

        public final Map<Long, Long> component2() {
            return this.joinedAt;
        }

        public final List<ModelGuildFolder> component3() {
            return this.userSettingsGuildPositions;
        }

        public final State copy(Set<Long> set, Map<Long, Long> map, List<ModelGuildFolder> list) {
            m.checkNotNullParameter(set, "mutedGuilds");
            m.checkNotNullParameter(map, "joinedAt");
            m.checkNotNullParameter(list, "userSettingsGuildPositions");
            return new State(set, map, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof State)) {
                return false;
            }
            State state = (State) obj;
            return m.areEqual(this.mutedGuilds, state.mutedGuilds) && m.areEqual(this.joinedAt, state.joinedAt) && m.areEqual(this.userSettingsGuildPositions, state.userSettingsGuildPositions);
        }

        public final Map<Long, Long> getJoinedAt() {
            return this.joinedAt;
        }

        public final Set<Long> getMutedGuilds() {
            return this.mutedGuilds;
        }

        public final List<ModelGuildFolder> getUserSettingsGuildPositions() {
            return this.userSettingsGuildPositions;
        }

        public int hashCode() {
            Set<Long> set = this.mutedGuilds;
            int i = 0;
            int hashCode = (set != null ? set.hashCode() : 0) * 31;
            Map<Long, Long> map = this.joinedAt;
            int hashCode2 = (hashCode + (map != null ? map.hashCode() : 0)) * 31;
            List<ModelGuildFolder> list = this.userSettingsGuildPositions;
            if (list != null) {
                i = list.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("State(mutedGuilds=");
            R.append(this.mutedGuilds);
            R.append(", joinedAt=");
            R.append(this.joinedAt);
            R.append(", userSettingsGuildPositions=");
            return a.K(R, this.userSettingsGuildPositions, ")");
        }
    }

    public StoreGuildsSorted(ObservationDeck observationDeck, Dispatcher dispatcher, StoreGuilds storeGuilds, StoreLurking storeLurking) {
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(dispatcher, "dispatcher");
        m.checkNotNullParameter(storeGuilds, "guildStore");
        m.checkNotNullParameter(storeLurking, "lurkingGuildStore");
        this.observationDeck = observationDeck;
        this.dispatcher = dispatcher;
        this.guildStore = storeGuilds;
        this.lurkingGuildStore = storeLurking;
    }

    @StoreThread
    private final void ensureValidPositions() {
        Map<Long, Guild> guildsInternal$app_productionGoogleRelease = this.guildStore.getGuildsInternal$app_productionGoogleRelease();
        Set<Long> unavailableGuildsInternal$app_productionGoogleRelease = this.guildStore.getUnavailableGuildsInternal$app_productionGoogleRelease();
        HashSet hashSet = new HashSet();
        for (ModelGuildFolder modelGuildFolder : this.positions) {
            hashSet.addAll(modelGuildFolder.getGuildIds());
        }
        for (Guild guild : guildsInternal$app_productionGoogleRelease.values()) {
            if (!hashSet.contains(Long.valueOf(guild.getId())) && !this.lurkingGuildStore.isLurking$app_productionGoogleRelease(guild)) {
                this.positions.add(0, new ModelGuildFolder(null, d0.t.m.listOf(Long.valueOf(guild.getId())), null, null, 8, null));
            }
        }
        int i = 0;
        for (Object obj : this.positions) {
            i++;
            if (i < 0) {
                n.throwIndexOverflow();
            }
            ModelGuildFolder modelGuildFolder2 = (ModelGuildFolder) obj;
            List<Long> guildIds = modelGuildFolder2.getGuildIds();
            ArrayList arrayList = new ArrayList();
            for (Object obj2 : guildIds) {
                long longValue = ((Number) obj2).longValue();
                Guild guild2 = guildsInternal$app_productionGoogleRelease.get(Long.valueOf(longValue));
                if (guildsInternal$app_productionGoogleRelease.containsKey(Long.valueOf(longValue)) && !unavailableGuildsInternal$app_productionGoogleRelease.contains(Long.valueOf(longValue)) && guild2 != null && !this.lurkingGuildStore.isLurking$app_productionGoogleRelease(guild2)) {
                    arrayList.add(obj2);
                }
            }
            this.positions.set(i, ModelGuildFolder.copy$default(modelGuildFolder2, null, arrayList, null, null, 13, null));
        }
        Set<Long> lurkingGuildIdsSync = this.lurkingGuildStore.getLurkingGuildIdsSync();
        ArrayList<Number> arrayList2 = new ArrayList();
        for (Number number : lurkingGuildIdsSync) {
            Guild guild3 = this.guildStore.getGuildsInternal$app_productionGoogleRelease().get(Long.valueOf(number.longValue()));
            Long valueOf = guild3 != null ? Long.valueOf(guild3.getId()) : null;
            if (valueOf != null) {
                arrayList2.add(valueOf);
            }
        }
        for (Number number2 : arrayList2) {
            this.positions.add(0, new ModelGuildFolder(null, d0.t.m.listOf(Long.valueOf(number2.longValue())), null, null, 8, null));
        }
        r.removeAll((List) this.positions, (Function1) StoreGuildsSorted$ensureValidPositions$6.INSTANCE);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleNewPositionsFromUser(List<? extends Entry> list) {
        this.positions.clear();
        List<ModelGuildFolder> list2 = this.positions;
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
        for (Entry entry : list) {
            arrayList.add(entry.asModelGuildFolder());
        }
        list2.addAll(arrayList);
        ensureValidPositions();
        rebuildSortedGuilds();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @StoreThread
    public final void handleNewState(State state) {
        updatePositions(state);
        ensureValidPositions();
        rebuildSortedGuilds();
    }

    private final Observable<State> observeStores() {
        StoreStream.Companion companion = StoreStream.Companion;
        Observable<State> h = Observable.h(companion.getGuilds().observeGuilds(), companion.getUserGuildSettings().observeMutedGuildIds().F(StoreGuildsSorted$observeStores$1.INSTANCE), companion.getGuilds().observeJoinedAt(), companion.getUserSettings().observeGuildFolders(), StoreGuildsSorted$observeStores$2.INSTANCE);
        m.checkNotNullExpressionValue(h, "Observable.combineLatest…lds, joinedAt, folders) }");
        return h;
    }

    @StoreThread
    private final void rebuildSortedGuilds() {
        this.entries.clear();
        for (ModelGuildFolder modelGuildFolder : this.positions) {
            Long id2 = modelGuildFolder.getId();
            if (id2 != null) {
                long longValue = id2.longValue();
                List<Long> guildIds = modelGuildFolder.getGuildIds();
                ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(guildIds, 10));
                for (Number number : guildIds) {
                    Guild guild = this.guildStore.getGuildsInternal$app_productionGoogleRelease().get(Long.valueOf(number.longValue()));
                    m.checkNotNull(guild);
                    arrayList.add(guild);
                }
                this.entries.add(new Entry.Folder(longValue, arrayList, modelGuildFolder.getColor(), modelGuildFolder.getName()));
            } else {
                Guild guild2 = this.guildStore.getGuildsInternal$app_productionGoogleRelease().get(u.first((List<? extends Object>) modelGuildFolder.getGuildIds()));
                m.checkNotNull(guild2);
                this.entries.add(new Entry.SingletonGuild(guild2));
            }
        }
        markChanged();
    }

    @StoreThread
    private final void updatePositions(final State state) {
        this.positions.clear();
        final Map<Long, Guild> guildsInternal$app_productionGoogleRelease = this.guildStore.getGuildsInternal$app_productionGoogleRelease();
        if (!state.getUserSettingsGuildPositions().isEmpty()) {
            this.positions.addAll(state.getUserSettingsGuildPositions());
            return;
        }
        List<ModelGuildFolder> list = this.positions;
        List<Number> sortedWith = u.sortedWith(guildsInternal$app_productionGoogleRelease.keySet(), new Comparator<Long>() { // from class: com.discord.stores.StoreGuildsSorted$updatePositions$1
            public final int compare(Long l, Long l2) {
                String str;
                String name;
                boolean contains = StoreGuildsSorted.State.this.getMutedGuilds().contains(l);
                if (StoreGuildsSorted.State.this.getMutedGuilds().contains(l2) != contains) {
                    return contains ? 1 : -1;
                }
                Long l3 = StoreGuildsSorted.State.this.getJoinedAt().get(l);
                long j = Long.MIN_VALUE;
                long longValue = l3 != null ? l3.longValue() : Long.MIN_VALUE;
                Long l4 = StoreGuildsSorted.State.this.getJoinedAt().get(l);
                if (l4 != null) {
                    j = l4.longValue();
                }
                if (longValue != j) {
                    return (int) (longValue - j);
                }
                Guild guild = (Guild) guildsInternal$app_productionGoogleRelease.get(l);
                String str2 = "";
                if (guild == null || (str = guild.getName()) == null) {
                    str = str2;
                }
                Guild guild2 = (Guild) guildsInternal$app_productionGoogleRelease.get(l2);
                if (!(guild2 == null || (name = guild2.getName()) == null)) {
                    str2 = name;
                }
                return t.compareTo(str, str2, false);
            }
        });
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(sortedWith, 10));
        for (Number number : sortedWith) {
            arrayList.add(new ModelGuildFolder(null, d0.t.m.listOf(Long.valueOf(number.longValue())), null, null, 8, null));
        }
        list.addAll(arrayList);
    }

    public final List<Entry> getEntries() {
        return this.entriesSnapshot;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final LinkedHashMap<Long, Guild> getOrderedGuilds() {
        LinkedHashMap<Long, Guild> linkedHashMap = new LinkedHashMap<>();
        for (Entry entry : getEntries()) {
            if (entry instanceof Entry.SingletonGuild) {
                Entry.SingletonGuild singletonGuild = (Entry.SingletonGuild) entry;
                linkedHashMap.put(Long.valueOf(singletonGuild.getGuild().getId()), singletonGuild.getGuild());
            } else if (entry instanceof Entry.Folder) {
                for (Object obj : ((Entry.Folder) entry).getGuilds()) {
                    linkedHashMap.put(Long.valueOf(((Guild) obj).getId()), obj);
                }
            }
        }
        return linkedHashMap;
    }

    @Override // com.discord.stores.Store
    public void init(Context context) {
        m.checkNotNullParameter(context, "context");
        super.init(context);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.leadingEdgeThrottle(ObservableExtensionsKt.computationLatest(observeStores()), 1L, TimeUnit.SECONDS), StoreGuildsSorted.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StoreGuildsSorted$init$1(this));
    }

    public final Observable<List<Entry>> observeEntries() {
        Observable<List<Entry>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreGuildsSorted$observeEntries$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR…  .distinctUntilChanged()");
        return q;
    }

    public final Observable<LinkedHashMap<Long, Guild>> observeOrderedGuilds() {
        Observable<LinkedHashMap<Long, Guild>> q = ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this}, false, null, null, new StoreGuildsSorted$observeOrderedGuilds$1(this), 14, null).q();
        m.checkNotNullExpressionValue(q, "observationDeck.connectR…  .distinctUntilChanged()");
        return q;
    }

    public final void setPositions(List<? extends Entry> list) {
        m.checkNotNullParameter(list, "guildPositions");
        this.dispatcher.schedule(new StoreGuildsSorted$setPositions$1(this, list));
    }

    @Override // com.discord.stores.StoreV2
    public void snapshotData() {
        super.snapshotData();
        this.entriesSnapshot = new ArrayList(this.entries);
    }
}
